from tkinter import *
from tkinter import ttk
from tkinter import messagebox
import os,time,random,datetime,pyttsx3
from concurrent.futures import ThreadPoolExecutor, ProcessPoolExecutor
engine=pyttsx3.init()
def saye(text):engine.say(text);engine.runAndWait();engine.stop()
executor=ThreadPoolExecutor(max_workers=20)
class Window(Frame):
    def __init__(self, master=None):
        Frame.__init__(self, master)
        self.master = master

# initialize tkinter
def on_closing(notice=True):
    global lo,error,rlo,di
    if not notice:
        if not os.path.exists('save_SC'):os.makedirs('save_SC')
        f=open('save_SC/lo.list','w+');f.write(repr(lo));f.close()
        try:error;e=True
        except:e=False
        try:rlo;rl=True
        except:rl=False
        if e:f=open('save_SC/error.int','w+');f.write(repr(error));f.close()
        if rl:f=open('save_SC/rlo.list','w+');f.write(repr(rlo));f.close()
        f=open('save_SC/di.dict','w+');f.write(repr(di));f.close()
        root.destroy();quit()
    if messagebox.askokcancel("退出", "你確定退出嗎？"):
        # save lo.list,error.int,rlo.list,di.dict
        if not os.path.exists('save_SC'):os.makedirs('save_SC')
        f=open('save_SC/lo.list','w+');f.write(repr(lo));f.close()
        try:error;e=True
        except:e=False
        try:rlo;rl=True
        except:rl=False
        if e:f=open('save_SC/error.int','w+');f.write(repr(error));f.close()
        if rl:f=open('save_SC/rlo.list','w+');f.write(repr(rlo));f.close()
        f=open('save_SC/di.dict','w+');f.write(repr(di));f.close()
        root.destroy();quit()
root,i = Tk(),0
app = Window(root)
f=open('setting.txt','r');loaded=f.read();f.close()
if os.path.exists('save_SC'):
    if messagebox.askokcancel("繼續", "你確定繼續上次學習嗎？"):
        rea=True
    else:rea=False
else:rea=False
if not os.path.exists('dynamic_logging'):os.makedirs('dynamic_logging')

# initialize logging file
if not os.path.exists('learn_words_list'):os.makedirs('learn_words_list')
if os.path.exists('learn_words_list/%s.txt'%loaded):
    f=open('learn_words_list/%s.txt'%loaded,'r');lo=eval(f.read());f.close()
else:print('非法運行，已退出');quit()
di,olo={},lo.copy();il=len(olo)
if rea:
    f=open('save_SC/lo.list','r');lo=eval(f.read());f.close()
    f=open('save_SC/di.dict','r');di=eval(f.read());f.close()
else:
    for a in lo:
        if a[0]not in di:di[a[0]]=a[1:]
        else:
            di[a[0]][0]='%s & %s'%(di[a[0]][0],a[1])
            di[a[0]][1]='%s & %s'%(di[a[0]][1],a[2])
            di[a[0]][2].extend(a[3])

# set window title and icon
root.wm_title("英語單詞英譯漢訓練 - %s"%loaded)
icon=PhotoImage(file='icon.png')
root.tk.call('wm','iconphoto',root._w,icon)

# set window size and position
w,h=1200,1000
root.geometry("%dx%d+%d+%d"%(w,h,(root.winfo_screenwidth()-w)/2,(root.winfo_screenheight()-h)/2))

# set status of gird positions
Grid.columnconfigure(root,0,weight=1)

# labals
la0=Label(text='英譯漢訓練',font=('Arial','15'))
la0.grid(row=0,column=0,columnspan=9,sticky='NSEW')
nn=0
def de(l,n):return[a[n]for a in l]
deo=de(lo,0)
for a in olo:
    if a[0]in deo:nn+=1
if nn==0:
    # statis data information
    l,l2=0,0
    for a in di.keys():
        l+=int(di[a][4])
        l2+=int(di[a][5])
        di[a][6]=l/(l+l2)*100
    messagebox.showinfo(title='Congratulation!',message='{}%.'.format('Congratulate! You finished your assignment! You finished %d words. Correct is %d. Error is %d. Correct-Rate is %f'%(l+l2,l,l2,l/(l+l2)*100)));on_closing(False)
la1=Label(text='還剩%d/%d個單詞'%(nn,il),font=('Arial','8'))
la1.grid(row=1,column=0,columnspan=9,sticky='NSEW')
la2=Label(text=lo[0][0],font=('Arial','72'))
la2.grid(row=2,column=0,columnspan=9,sticky='NSEW')
la3=Label(text=lo[0][2],font=('宋体','24'))
la3.grid(row=3,column=0,columnspan=9,sticky='NSEW')
la4=Label(text='隱藏定義',font=('Arial','32'))
la4.grid(row=4,column=0,columnspan=9,sticky='NSEW')
def delo(a,b):
    rep=''.join(['_'for a in range(len(b))])
    t=a.replace(' %s '%b,' %s '%rep)
    try:
        if t[:len(b)]==b:t[:len(b)]=rep
    except:pass
    try:
        if t[-len(b):]==b:t[-len(b):]=rep
    except:pass
    return t
def randomlist(a,n):
    if len(a)>n:
        t=[]
        while len(t)<n:
            for b in range(n):
                while(z:=a[random.randint(0,n-1)])not in t:t.append(z)
        return t
    else:return a
if not rea:rlo=randomlist(lo[0][3],3)
else:f=open('save_SC/rlo.list','r');rlo=eval(f.read());f.close()
la5=Label(text='\n\n'.join(['\n'.join([delo(z[0],lo[0][0]),z[1]])for z in rlo]),font=('Arial','12'),wraplengt=800)
la5.grid(row=20,columnspan=9,sticky='NSEW')

# give tests
i=0
def test():
    global lo,la2,i,il,error,rlo,di,olo;nn=0
    deo=de(lo,0)
    for a in olo:
        if a[0]in deo:nn+=1
    if nn==0:
        # statis data information
        l,l2=0,0
        for a in di.keys():
            l+=int(di[a][4])
            l2+=int(di[a][5])
            di[a][6]=l/(l+l2)*100
        messagebox.showinfo(title='Congratulation!',message='{}%.'.format('Congratulate! You finished your assignment! You finished %d words. Correct is %d. Error is %d. Correct-Rate is %f'%(l+l2,l,l2,l/(l+l2)*100)));on_closing(False)
    lo[i][4]+=1
    di[lo[i][0]][3]+=1

    lo[i][5]+=1
    di[lo[i][0]][4]+=1
    now=datetime.datetime.now()
    if not os.path.exists(pa:='dynamic_logging/%s-%s-%s-%s.txt'%(str(now.year),str(now.month),str(now.day),loaded)):f=open(pa,'w+');f.write('%s:%s:%s %s True None chinese'%(str(now.hour),str(now.minute),str(now.second),lo[i][0]));f.close()
    else:f=open(pa,'a');f.write('\n%s:%s:%s %s True None chinese'%(now.hour,now.minute,now.second,lo[i][0]));f.close()
    lo.remove(lo[0]);nn-=1
    la1.configure(text='還剩%d/%d個單詞'%(nn,il));la1.update()
    if len(lo)==0:
        # statis data information
        l,l2=0,0
        for a in di.keys():
            l+=int(di[a][4])
            l2+=int(di[a][5])
            di[a][6]=l/(l+l2)*100
        messagebox.showinfo(title='Congratulation!',message='{}%.'.format('Congratulate! You finished your assignment! You finished %d words. Correct is %d. Error is %d. Correct-Rate is %f'%(l+l2,l,l2,l/(l+l2)*100)));on_closing(False)
    la2.configure(text=lo[i][0]);la2.update()
    la3.configure(text=lo[i][2]);la3.update()
    rlo=randomlist(lo[i][3],3)
    la5.configure(text='\n\n'.join(['\n'.join([delo(z[0],lo[i][0]),z[1]])for z in rlo]));la5.update()
    # update button
    give_button()
    error=True
    executor.submit(saye,lo[i][0])
def test_fail(en0):
    global lo,la2,i,il,error,rlo,di,olo;nn=0
    deo=de(lo,0)
    for a in olo:
        if a[0]in deo:nn+=1
    if nn==0:
        # statis data information
        l,l2=0,0
        for a in di.keys():
            l+=int(di[a][4])
            l2+=int(di[a][5])
            di[a][6]=l/(l+l2)*100
        messagebox.showinfo(title='Congratulation!',message='{}%.'.format('Congratulate! You finished your assignment! You finished %d words. Correct is %d. Error is %d. Correct-Rate is %f'%(l+l2,l,l2,l/(l+l2)*100)));on_closing(False)
    lo[i][4]+=1
    di[lo[i][0]][3]+=1
    
    lo[i][6]+=1
    di[lo[i][0]][5]+=1
    if error:
        lo.insert(i+2,lo[i])
        lo.insert(i+4,lo[i])
        lo.insert(i+10,lo[i])
        lo.append(lo[i])
        la1.configure(text='還剩%d/%d個單詞'%(nn,il));la1.update()
    error=False
    # update button
    executor.submit(saye,lo[i][0])
    la4.configure(text=lo[i][1]);la2.update();now=datetime.datetime.now()
    give_button()
    if not os.path.exists(pa:='dynamic_logging/%s-%s-%s-%s.txt'%(str(now.year),str(now.month),str(now.day),loaded)):f=open(pa,'w+');f.write('%s:%s:%s %s False %s chinese'%(str(now.hour),str(now.minute),str(now.second),lo[i][0],en0));f.close()
    else:f=open(pa,'a');f.write('\n%s:%s:%s %s False %s chinese'%(now.hour,now.minute,now.second,lo[i][0],en0));f.close()
    time.sleep(1)
    la4.configure(text='隱藏含義');la2.update()

# give buttons
f=open('logging/%s.txt'%loaded,'r');zlo=eval(f.read());f.close()
bu0,bus=None,None
def give_button():
    global zlo,lo,bu0,bus
    try:zlo.remove(lo[0])
    except:pass
    random.shuffle(zlo)
    rows=[5,6,7,8,9,10,11,12]
    rows=random.sample(rows,k=8)
    if not bu0:bu0=Button(text=lo[0][1],font=('Arial','24'),command=test)
    else:bu0.configure(text=lo[0][1])
    if not bus:bus=[]
    bu0.grid(row=rows[0],columnspan=9,sticky='NSEW')
    for a in range(7):
        if a>len(bus)-1:bus.append(Button(text=zlo[a][1],font=('Arial','24'),command=lambda:test_fail(zlo[a][1])))
        else:bus[a].configure(text=zlo[a][1])
        bus[a].grid(row=rows[a+1],columnspan=9,sticky='NSEW')
give_button()

# read words

if not rea:error=True
else:f=open('save_SC/error.int','r');error=eval(f.read());f.close()
def read_words(ev=None):executor.submit(saye,lo[i][0])
def read_sentence_1(ev=None):executor.submit(saye,rlo[0][0])
def read_sentence_2(ev=None):
    if len(rlo)>1:executor.submit(saye,rlo[1][0])
def read_sentence_3(ev=None):
    if len(rlo)>2:executor.submit(saye,rlo[2][0])

executor.submit(saye,lo[0][0])
root.bind('<Control-r>',read_words)
bu1=Button(text='朗讀單詞CR',font=('Arial','32'),command=read_words)
bu1.grid(row=19,column=0,sticky='NSEW')
root.bind('<Control-a>',read_sentence_1)
bu1=Button(text='朗讀例句一CA',font=('Arial','32'),command=read_sentence_1)
bu1.grid(row=19,column=1,sticky='NSEW')
root.bind('<Control-s>',read_sentence_2)
bu2=Button(text='朗讀例句二CS',font=('Arial','32'),command=read_sentence_2)
bu2.grid(row=19,column=2,sticky='NSEW')
root.bind('<Control-d>',read_sentence_3)
bu3=Button(text='朗讀例句三CD',font=('Arial','32'),command=read_sentence_3)
bu3.grid(row=19,column=3,sticky='NSEW')

# show window
root.protocol("WM_DELETE_WINDOW", on_closing)
root.mainloop()
