from tkinter import *
import os

class Window(Frame):
    def __init__(self, master=None):
        Frame.__init__(self, master)
        self.master = master

# initialize tkinter
root = Tk()
app = Window(root)

# set window title and icon
root.wm_title("英語單詞認識、拼寫、選擇、例句、綜合、聽力、構詞法訓練器")
icon=PhotoImage(file='icon.png')
root.tk.call('wm','iconphoto',root._w,icon)

# set window size and position
w,h=800,600
root.geometry("%dx%d+%d+%d"%(w,h,(root.winfo_screenwidth()-w)/2,(root.winfo_screenheight()-h)/2))

# set status of gird positions
Grid.columnconfigure(root,0,weight=1)
Grid.rowconfigure(root,1,weight=1)

# give the list of dictionaries
ti=Label(root,text="請選擇要學習的詞典",font=("宋体",24))
ti.grid(row=0,column=0,sticky="NSEW")
lb=Listbox(root,font=("宋体",24))
lb.grid(row=1,column=0,sticky="NSEW")
ln=['大學四級詞彙','大學四級詞組','大學四級高頻詞彙','大學英語六級詞彙','大學六級詞組','英語專業四級詞彙','英語專業八級詞彙','GRE詞彙','《自由軟件，自由社會》']
for a in ln:lb.insert(END,a)

# give a button for confirm
def printSelected():
    if lb.get(ANCHOR)=='大學四級詞彙':print('歡迎開始學習');f=open('setting.txt','w+');f.write('大學四級詞彙');f.close();os.system('pyth loaded_window.py')
    if lb.get(ANCHOR)=='英語專業八級詞彙':print('歡迎開始學習');f=open('setting.txt','w+');f.write('英語專業八級詞彙');f.close();os.system('pyth loaded_window.py')
bu0=Button(root,text='確認進入學習',command=printSelected,font=("宋体",24))
bu0.grid(row=2,column=0,sticky="NSEW")
def check():os.system('pyth statis_window.py')
bu1=Button(root,text='查看統計數據',command=check,font=("Arial",24))
bu1.grid(row=3,column=0,sticky="NSEW")

'''
# lock window border
#root.resizable(width=False, height=False)
#Grid.rowconfigure(root,0,weight=1)
#ti.place(relx=0.5,anchor='n',y=8)
#ti.pack(fill=BOTH,expand=True,side=TOP)
#lb.place(relx=0.5,anchor='n',y=48,width=800,height=600-48-48)
#lb.pack(fill=BOTH,expand=True,side=TOP)
#bu0.place(relx=0.5,anchor='n',y=48+600-48-48)
#bu0.pack(fill=BOTH,expand=True,side=TOP)
'''

# show window
root.mainloop()
