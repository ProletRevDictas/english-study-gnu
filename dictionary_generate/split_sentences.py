import os
from html.parser import HTMLParser
from re import sub
from sys import stderr
from traceback import print_exc
class _DeHTMLParser(HTMLParser):
    def __init__(self):
        HTMLParser.__init__(self)
        self.__text = []

    def handle_data(self, data):
        text = data.strip()
        if len(text) > 0:
            text = sub('[ \t\r\n]+', ' ', text)
            self.__text.append(text + ' ')

    def handle_starttag(self, tag, attrs):
        if tag == 'p':
            self.__text.append('\n\n')
        elif tag == 'br':
            self.__text.append('\n')

    def handle_startendtag(self, tag, attrs):
        if tag == 'br':
            self.__text.append('\n\n')

    def text(self):
        return ''.join(self.__text).strip()
def dehtml(text):
    try:
        parser = _DeHTMLParser()
        parser.feed(text)
        parser.close()
        return parser.text()
    except:
        print_exc(file=stderr)
        return text
for a in os.walk(he:='dictionary.com'):
    for b in a[2]:
        if os.path.exists(pa:='sentences/%s.txt'%b.split('.htm')[0]):continue
        print(b)
        f=open('%s/%s'%(he,b),'r');t=f.read();f.close()
        try:t=dehtml(t.split('<!-- --> in a sentence</h2>')[1].split('<strong data-ci-target="seeLessExamples">SEE FEWER EXAMPLES</strong>')[0].replace('<span class="italic">','').replace('</span>','')).split('{display:block;}')[1].split('SEE MORE EXAMPLES')[0].strip()if t.find('<!-- --> in a sentence</h2>')!=-1 else None
        except:
            t=dehtml(t.split('<!-- --> in a sentence</h2>')[1].split('trending articles')[0]).split('.css-')[0].strip()if t.find('<!-- --> in a sentence</h2>')!=-1 else None
            print(t)
            #quit()
        if t:f=open(pa,'w+');f.write(t);f.close()
