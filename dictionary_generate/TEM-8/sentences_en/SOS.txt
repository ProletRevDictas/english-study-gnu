As Democrats mutter privately that their Senate majority is sinking beneath the waves, their leadership has sent out an SOS. The Only Way for Democrats to Win |Jonathan Alter |October 24, 2014 |DAILY BEAST 
Mooney quickly inflated his life raft, sent out an SOS signal and drifted for fourteen days before he was rescued. Victor Mooney’s Epic Adventure for His Dead Brother |Justin Jones |October 19, 2014 |DAILY BEAST 
But FEC filings show that neither the left-leaning SoS for Democracy nor the right-leaning SoS for SoS have taken off yet. The Democrats' Katherine Harris Strategy |Patricia Murphy |September 6, 2014 |DAILY BEAST 
The acronym of the embodying League of the Common Fate is SOS. This 1979 Novel Predicted Putin’s Invasion Of Crimea |Michael Weiss |May 18, 2014 |DAILY BEAST 
Gascón and the Boken family and the others of a pro “kill switch” group calling itself Secure Our Smartphones (SOS) kept pushing. Murdered for Her iPhone |Michael Daly |May 8, 2014 |DAILY BEAST 
Miss blusht—what a happy dog he was—Miss blusht crimson, and then he sighed deeply, and began eating his turbat and lobster sos. Memoirs of Mr. Charles J. Yellowplush |William Makepeace Thackeray 
A great blob of brown sos spurted on to master's chick, and myandrewed down his shert-collar and virging-white weskit. Memoirs of Mr. Charles J. Yellowplush |William Makepeace Thackeray 
I came up to tell you, sos you could get a man to help you and go right down and get him out. Three Little Women |Gabrielle E. Jackson 
Den write it all out crost de back ob Miss Jinnys letter sos I have sumpin fer ter show dat its done paid. Three Little Women |Gabrielle E. Jackson 
Yuh hadn't ought to uh done it—or else yuh oughta made a clean job of it sos't we could hang yuh proper. The Happy Family |Bertha Muzzy Bower