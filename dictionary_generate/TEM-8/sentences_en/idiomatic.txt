Full-time residents, who call themselves Yoopers, use the same name to refer to their idiomatic English. Welcome to Yooperland, A Little Slice of Finland in Michigan |Jane & Michael Stern |May 11, 2014 |DAILY BEAST 
In the meantime, she continues to speak with idiomatic brusqueness. Mamata Banerjee, India's Political Superwoman |Shoma Chaudhury |May 18, 2011 |DAILY BEAST 
“There was no absolute word in the Russian idiomatic language,” he said. Nikita Khrushchev, Talk Show Guest |Stephen Battaglio |November 20, 2010 |DAILY BEAST 
Moreover, by the time he reaches college it is too late to teach him even common, idiomatic expressiveness. The Idyl of Twin Fires |Walter Prichard Eaton 
All the truly idiomatic and national phrases are kept, and all others successively picked out and thrown away. Beacon Lights of History, Volume XIII |John Lord 
Numberless compound idiomatic phrases have also been given a place, in each case under the head of the significant word. Chambers's Twentieth Century Dictionary (part 1 of 4: A-D) |Various 
Thats idiomatic French, and means a good deal that you dont understand; I always use it to gals, especially when theyre pretty. Three in Norway |James Arthur Lees 
It was filled with jokes, beneath which were German notes explaining any difficult or idiomatic words and phrases. War in the Garden of Eden |Kermit Roosevelt