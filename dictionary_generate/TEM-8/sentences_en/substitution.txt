Some of these cutbacks can be achieved through more substitution and incremental steps. How to Watch Out for Hidden Sugar and Replace With Leaner Substitutes |Diana Le Dean |February 23, 2013 |DAILY BEAST 
No prize transfer, assignment, or substitution by Winner is permitted. The Orbitz Business Travel Survey Sweepstakes | |December 14, 2012 |DAILY BEAST 
The substitution of an effect for a cause is an old technique and trick of classical sophistry. Demonization Is No Excuse |Hussein Ibish |November 20, 2012 |DAILY BEAST 
The Prize is nontransferable and no substitution of the Prize by the Grand Prize Winner is allowed. OFFICIAL RULES OF THE IMAX® Award Contest—Deadline Extended | |October 8, 2012 |DAILY BEAST 
Yet here is the great problem with Murray's substitution of imagery for numbers in his social thought. What the Founders Would Tell Charles Murray |David Frum |February 7, 2012 |DAILY BEAST 
But as both conceptions are related somehow to the idea of cost, the substitution is never discovered. The Unsolved Riddle of Social Justice |Stephen Leacock 
This one detail—the substitution of springs for weights—has had a far-reaching effect upon organ music. The Recent Revolution in Organ Building |George Laing Miller 
Koch notes that the reading depriued arose from its substitution for the less familiar form priued. Chaucer's Works, Volume 1 (of 7) -- Romaunt of the Rose; Minor Poems |Geoffrey Chaucer 
A second element of the "accent du Midi" just referred to is the substitution of an open for a close o. Frdric Mistral |Charles Alfred Downer 
The work is much quickened by the substitution of steam power, water power, or even horse power for manual labor. The Wonder Book of Knowledge |Various