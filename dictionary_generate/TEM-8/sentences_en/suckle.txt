Things might have seemed fine at first, the baby crying, suckling, being passed around the group for love and comfort, but soon it would have been clear that the baby’s health was declining. The Beloved Mesolithic Girl - Issue 109: Excavation |Jamie Hodgkins |December 15, 2021 |Nautilus 
Some simply have a fold of skin that protects their young while they suckle. Scientists Say: Marsupial |Maria Temming |October 25, 2021 |Science News For Students 
It is Ma who suggests to Rose of Sharon that she suckle a starving man. Is There a Ma Joad for the Piketty Era? |Katie Baker |July 1, 2014 |DAILY BEAST 
When they suckle their young, they take them in their paws, and present the breast to them like a woman. Buffon's Natural History. Volume IX (of 10) |Georges Louis Leclerc de Buffon 
Men cannot give birth to a child, nor can they suckle a child; they can only procreate children, or become fathers. The Sexual Life of the Child |Albert Moll 
Man belongs to the highest class of vertebrates, the Mammals, which produce living young and suckle them. The New Stone Age in Northern Europe |John M. Tyler 
He cautiously pulled back a branch of the honey-suckle, and looked through. The Code of the Mountains |Charles Neville Buck 
In consumption, all efforts to suckle are frequently equally fatal to the mother and child. The Physical Life of Woman: |Dr. George H Napheys