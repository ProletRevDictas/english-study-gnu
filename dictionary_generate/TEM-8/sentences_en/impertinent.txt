Well, nothing except hard feelings from impertinent comments made by bewigged egocentrics with fiery tempers. James Madison’s Lesson in Delayed Great-ification |Kevin Bleyer |July 4, 2014 |DAILY BEAST 
Edmund is now 4, and is a giggly, sociable, nosy, occasionally impertinent boy. The Cost of Raising a Special Needs Son |Elizabeth Picciuto |June 11, 2014 |DAILY BEAST 
Yes, Paul brought it up in a way that was impertinent and likely a political ploy. Stop Slut-Shaming Monica Lewinsky! |Emily Shire |May 7, 2014 |DAILY BEAST 
But uncomfortable —and arguably impertinent—questions are now fair game. French President François Hollande Slams Affair Allegations |Tracy McNicoll |January 11, 2014 |DAILY BEAST 
In fact, I think it's rather impertinent of you to raise the question, don't you? How Legitimate Was My Rape? |Justin Green |August 20, 2012 |DAILY BEAST 
Of course, she should not have minded so keenly the foolish talk of an impertinent and unkind girl. The Campfire Girls of Roselawn |Margaret Penrose 
As you will see, I was unable to end my letter without a touch of impertinent irony, which proved how much in love I still was. Camille (La Dame aux Camilias) |Alexandre Dumas, fils 
He delivered to the Secretary of State a note abusive and impertinent beyond all example and all endurance. The History of England from the Accession of James II. |Thomas Babington Macaulay 
They are as impertinent as those people who stop you only to bore you; but the former are perhaps less irksome. The 'Characters' of Jean de La Bruyre |Jean de La Bruyre 
She was a little impertinent, the Duchess thought, decidedly aggressive, and not witty enough to carry it off. Marriage la mode |Mrs. Humphry Ward