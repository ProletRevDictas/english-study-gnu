“He told me to keep my eyes open super wide,” Waters told Vocative. Anonymous Model Goes Public with Allegations Against Terry Richardson |Justin Jones |March 12, 2014 |DAILY BEAST 
The vocative alone often takes a final a as in the interrogative form. The Mafulu |Robert W. Williamson 
Ahaygar; a pet term; my friend, my love: vocative of Irish tagur, love, a dear person. English As We Speak It in Ireland |P. W. Joyce 
Avourneen, my love: the vocative case of Irish muirnn, a sweetheart, a loved person. English As We Speak It in Ireland |P. W. Joyce 
Vocative, vok′a-tiv, adj. pertaining to the act of calling, applied to the grammatical case used in personal address. Chambers's Twentieth Century Dictionary (part 4 of 4: S-Z and supplements) |Various 
It is a different word altogether, and is only the subjunctive of am, in the way puss is the vocative of cat. A Handbook of the English Language |Robert Gordon Latham