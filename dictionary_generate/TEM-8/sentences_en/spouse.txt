Now, multiply that and say their spouse or significant other. Why Can’t Schools Get What the N.F.L. Has? (Ep. 431) |Stephen J. Dubner |September 10, 2020 |Freakonomics 
I’m seeing—and giving—glimpses of spouses, kids, pets—or the lack thereof. How this year’s 40 Under 40 are surviving the pandemic |jonathanvanian2015 |September 7, 2020 |Fortune 
Now, their supervisors are virtually in their living rooms, meeting spouses, babies and pets. Deep Dive: How companies and their employees are facing the future of work |Digiday |September 1, 2020 |Digiday 
The event is primarily for transmasculine and nonbinary folks, but friends, spouses and allies of any gender are also welcome. Calendar: Aug. 14-20 |Kaela Roeder |August 12, 2020 |Washington Blade 
A massive transfer of wealth is under way as male baby boomers die, leaving assets to spouses, and as more women become breadwinners. US women will take control of an additional $20 trillion in wealth this decade |John Detrixhe |July 29, 2020 |Quartz 
Who else would see a former spouse accused of underage sex and call him ‘the greatest man there is’? Fergie Dives Into Prince Andrew’s Sex Scandal |Tom Sykes |January 5, 2015 |DAILY BEAST 
Otherwise, he decides whether or not to perform a wedding based on how comfortable he feels with the spouse on the outside. Saying Yes to the Dress—Behind Bars |Caitlin Dickson |December 8, 2014 |DAILY BEAST 
I point out to both of them that Wahlberg seems the very model of a supportive spouse. Jenny McCarthy: I Am Not Anti-Vaccine |Lloyd Grove |October 24, 2014 |DAILY BEAST 
I have to say that as a military spouse, I am excited to meet Dr. Jill Biden, wife of Vice President Joe Biden. Women in the World Texas Sneak Peek |Cynthia Allum |October 20, 2014 |DAILY BEAST 
Well, that is very beneficial to the spouse who has a new job in LA, but detrimental to the one who left a job in New York. Is Alimony Anti-Feminist? |Keli Goff |August 25, 2014 |DAILY BEAST 
In the humour her spouse was then in she had better have remained silent—she told him, that he was harsh and unjust. The World Before Them |Susanna Moodie 
Why,” said his spouse, after considering a moment, “he said you had been letting him into the mysteries of the cellar. The Portsmouth Road and Its Tributaries |Charles G. Harper 
Look: Nature burns around us and rolls in the arms of Summer, and drinks in the devouring breath of her ruddy spouse. Frdric Mistral |Charles Alfred Downer 
The duties which one spouse legally owes to the other are fairly well known. Report of the Special Committee on Moral Delinquency in Children and Adolescents |Oswald Chettle Mazengarb et al. 
Have my young mistress and her august spouse already taken leave? The Dragon Painter |Mary McNeil Fenollosa