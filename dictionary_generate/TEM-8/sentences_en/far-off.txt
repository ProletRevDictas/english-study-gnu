So far, all the players seemed to be willing to wait their turn. The Golden State Preps for the ‘Red Wedding’ of Senate Races |David Freedlander |January 9, 2015 |DAILY BEAST 
As far as I can tell, this magazine spent as much time making fun of French politicians as it did of Muslims or Islam. Harry Shearer on The Dangerous Business of Satire |Lloyd Grove |January 8, 2015 |DAILY BEAST 
How far has Congress really evolved on race when in 50 years it has gone from one black senator to two? The Unbearable Whiteness of Congress |Dean Obeidallah |January 8, 2015 |DAILY BEAST 
But sources said that the evidence so far is pointing away from an ISIS connection. U.S. Spies See Al Qaeda Fingerprints on Paris Massacre |Shane Harris, Nancy A. Youssef |January 8, 2015 |DAILY BEAST 
I think the response of the French government so far has been pretty appropriate in that regard. Harry Shearer on The Dangerous Business of Satire |Lloyd Grove |January 8, 2015 |DAILY BEAST 
However, they were not seen to venture far into the surrounding deciduous forest. Summer Birds From the Yucatan Peninsula |Erwin E. Klaas 
Suddenly, however, he became aware of a small black spot far ahead in the very middle of the unencumbered track. The Joyous Adventures of Aristide Pujol |William J. Locke 
In the evening, St. Peter's and its accessories were illuminated—by far the most brilliant spectacle I ever saw. Glances at Europe |Horace Greeley 
Thus far Boston banks have received more benefits from this bank than have the other banks in this district. Readings in Money and Banking |Chester Arthur Phillips 
Sleek finds it far harder work than fortune-making; but he pursues his Will-o'-the-Wisp with untiring energy. The Pit Town Coronet, Volume I (of 3) |Charles James Wills