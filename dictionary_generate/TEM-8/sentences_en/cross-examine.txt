The Via Dolorosa ends at the Church of the Holy Sepulchre, and is marked by nine stations of the cross. Oops! Jesus’ Last Steps Are in the Wrong Place |Candida Moss |January 6, 2015 |DAILY BEAST 
If they were meaningful, we might have realized it before—surely one of these kids wore a cross, or a yarmulke, or a hijab? Harry Potter and the Torah of Terror |Candida Moss, Joel Baden |January 4, 2015 |DAILY BEAST 
The reason: activist government and unionized government often work at cross purposes. How Public Sector Unions Divide the Democrats |Daniel DiSalvo |December 29, 2014 |DAILY BEAST 
What do you get when you cross an oil company with gay rights? How Canadian Oilmen Pinkwash the Keystone Pipeline |Jay Michaelson |December 28, 2014 |DAILY BEAST 
But they refused to cross the street to help because, they told bystanders, the rules required them instead to call 911. Red Tape Is Strangling Good Samaritans |Philip K. Howard |December 27, 2014 |DAILY BEAST 
In cross-section the burrows varied from round (three inches in diameter) to oval (three inches high and four inches wide). Summer Birds From the Yucatan Peninsula |Erwin E. Klaas 
I cannot believe that God would think it necessary to come on earth as a man, and die on the Cross. God and my Neighbour |Robert Blatchford 
At Jaques Cartier they had but one batteau to cross the army over with, and were fired upon during the whole time by two frigates. The Every Day Book of History and Chronology |Joel Munsell 
Monsieur le Maire,” said he, “I should like to examine the premises, and beg that you will have the kindness to accompany me. The Joyous Adventures of Aristide Pujol |William J. Locke 
Father Salvierderra said if we repined under our crosses, then a heavier cross would be laid on us. Ramona |Helen Hunt Jackson