The latest attempt to rattle the foundations of cosmology appeared as a smattering of dots pulled upward into a cosmic sneer. Cosmologists Parry Attacks on the Vaunted Cosmological Principle |Charlie Wood |December 13, 2021 |Quanta Magazine 
A medical tech walked by the hospital room, his beady blue eyes landing on my chestnut-skinned brother with a sneer. My Brother Is Still Unvaccinated Because Our Medical System Is Ableist |Amanda Calhoun |November 8, 2021 |Time 
He observes the bodies floating away on the river, pulling on his cigarette with a sneer. Houellebecq’s Incendiary Novel Imagines France With a Muslim President |Pierre Assouline |January 9, 2015 |DAILY BEAST 
And Hitler looking like such a lout, a drunken lout, with that sort of ignorant sneer. Martin Amis Talks About Nazis, Novels, and Cute Babies |Ronald K. Fried |October 9, 2014 |DAILY BEAST 
The typical sneer was that Obama had never even run a candy store. The Conservatives' Great Black Hope |Evan Gahr |May 19, 2014 |DAILY BEAST 
The Internet might sneer at Monarch, but all press is good press, after all. Porn Company Wants Amanda Knox To Star In Adult Entertainment Film |Lizzie Crocker |February 12, 2014 |DAILY BEAST 
The serious magazines felt similarly behooved to weigh in, also largely to sneer. ‘You’ve Got to Be Kidding’: Why Adults Dismissed The Beatles in 1964 |Michael Tomasky |January 30, 2014 |DAILY BEAST 
"Reassure yourself," answered Marius, with a sneer, a greyness that was of jealous rage overspreading his face. St. Martin's Summer |Rafael Sabatini 
Amateurs will perhaps sneer at these cynical maxims, but each can learn from them what he wishes. Charles Baudelaire, His Life |Thophile Gautier 
"Doubtless you are very well off, Mr. Jock," Pauline continued, and this time the sneer in her voice was hardly veiled. A Butterfly on the Wheel |Cyril Arthur Edward Ranger Gull 
Westmacott moved a step or two forward, a swagger unmistakable in his gait, his nether-lip thrust out in a sneer. Mistress Wilding |Rafael Sabatini 
He did not look at Cash, but he felt morbidly that Cash was regarding him with that hateful sneer hidden under his beard. Cabin Fever |B. M. Bower