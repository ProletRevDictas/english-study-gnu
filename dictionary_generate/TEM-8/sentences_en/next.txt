In fact, according to F-35 program sources, the next software upgrades are not yet fully defined nor are they fully funded. Pentagon Misfires in Stealth Jet Scandal |Dave Majumdar |January 8, 2015 |DAILY BEAST 
But we are afraid and we wonder to ourselves who will be next. Mexico’s Priests Are Marked for Murder |Jason McGahan |January 7, 2015 |DAILY BEAST 
The next phase of the trial consists of vaccinating Ebola workers on the front lines. The Race for the Ebola Vaccine |Abby Haglage |January 7, 2015 |DAILY BEAST 
He is expected to spend the next few days closeted with lawyers and advisers at his home, Royal Lodge, in Windsor Great Park. From Playboy Prince to Dirty Old Man? |Tom Sykes |January 5, 2015 |DAILY BEAST 
In the next breath, however, he is decrying the press misinterpretation of his Diana script. Harry’s Daddy, and Diana’s ‘Murder’: Royal Rumors In a New Play |Tom Sykes |January 4, 2015 |DAILY BEAST 
In particular the Governor of Adinskoy offered us a guard of fifty men to the next station, if we apprehended any danger. The Life and Most Surprising Adventures of Robinson Crusoe, of York, Mariner (1801) |Daniel Defoe 
“This is a distressing predicament for these young people,” thought Mr. Pickwick, as he dressed himself next morning. The Posthumous Papers of the Pickwick Club, v. 2(of 2) |Charles Dickens 
The thought seemed to produce the dreaded object, for next moment a large hummock appeared right ahead. The Giant of the North |R.M. Ballantyne 
Impressed by the lugubrious scene, Aguinaldo yielded, and the next day peace negotiations were opened. The Philippine Islands |John Foreman 
All through the sad duties of the next four days Felipe was conscious of the undercurrent of this premonition. Ramona |Helen Hunt Jackson