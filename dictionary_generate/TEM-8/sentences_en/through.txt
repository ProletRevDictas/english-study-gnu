Fluoride first entered an American water supply through a rather inelegant technocratic scheme. Anti-Fluoriders Are The OG Anti-Vaxxers |Michael Schulson |July 27, 2016 |DAILY BEAST 
We see detoxing as a path to transcendence, a symbol of modern urban virtue and self-transformation through abstinence. How Taryn Toomey’s ‘The Class’ Became New York’s Latest Fitness Craze |Lizzie Crocker |January 9, 2015 |DAILY BEAST 
The questions going through my mind are: How on earth are there Kalashnikovs and rocket launchers in the heart of Paris? Ayaan Hirsi Ali: Our Duty Is to Keep Charlie Hebdo Alive |Ayaan Hirsi Ali |January 8, 2015 |DAILY BEAST 
Cold War fears could be manipulated through misleading art to attract readers to daunting material. How Pulp Fiction Saved Literature |Wendy Smith |January 8, 2015 |DAILY BEAST 
It opens with Huckabee's dramatic recollection of going through security at the airport. Huckabee 2016: Bend Over and Take It Like a Prisoner! |Olivia Nuzzi |January 8, 2015 |DAILY BEAST 
Before Ripperda could unclasp his lips to reply, the stranger had opened the door, and passed through it like a gliding shadow. The Pastor's Fire-side Vol. 3 of 4 |Jane Porter 
A constant sense of easy balance should be developed through poising exercises. Expressive Voice Culture |Jessie Eldridge Southwick 
This city stands upon almost two equal parts on each side the river that passes through. Gulliver's Travels |Jonathan Swift 
Nothing remarkable occurred in our march through this country. The Life and Most Surprising Adventures of Robinson Crusoe, of York, Mariner (1801) |Daniel Defoe 
Just corporeal enough to attest humanity, yet sufficiently transparent to let the celestial origin shine through. Pearls of Thought |Maturin M. Ballou