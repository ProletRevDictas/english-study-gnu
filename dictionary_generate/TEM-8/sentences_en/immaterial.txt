Regardless of where you land between — or even on — those two poles is immaterial. A fraction of Robinhood’s users are driving its runaway growth |Alex Wilhelm |February 19, 2021 |TechCrunch 
Whether Hand gets every save opportunity is immaterial — though he was a perfect 16 for 16 with Cleveland last year. Brad Hand’s arrival might end a Nationals tradition: The search for summer relief |Barry Svrluga |January 26, 2021 |Washington Post 
We literally lost only a few million dollars, which is fairly immaterial. Upstart CEO talks major IPO ‘pop,’ A.I. racial bias, and Google |rhhackettfortune |December 18, 2020 |Fortune 
We’re — you know, so I think — what I think of that is immaterial. Is it Too Late for General Motors to Go Electric? (Ep. 442) |Stephen J. Dubner |December 3, 2020 |Freakonomics 
Winning a championship, even under disputed circumstances, has a way of making everything else immaterial. For the Red Sox, rehiring Alex Cora makes sense. But it doesn’t look good. |Dave Sheinin |November 6, 2020 |Washington Post 
It is immaterial if the infidel is a combatant or a civilian. The CIA’s Wrong: Arming Rebels Works |Christopher Dickey |October 19, 2014 |DAILY BEAST 
The lack of evidence for HGH as an effective performance enhancer is just as immaterial as its illegality. Major League Baseball’s Planned Suspensions Are Already a Bust |David Roth |June 5, 2013 |DAILY BEAST 
Whether blame is assigned to the failed follower or the failed leader is immaterial. The Truth About Women in Combat |David Frum |March 1, 2013 |DAILY BEAST 
Their daily experiences are so much more powerful than ink on paper that they make the content of textbooks immaterial. Why Textbooks Don't Matter That Much |Ori Nir |February 26, 2013 |DAILY BEAST 
So as long as a lethal strike passes muster in constitutional terms, the location of the target is immaterial. White Paper Suggests U.S. Could Launch Drones Into U.S. Cities |Kal Raustiala |February 17, 2013 |DAILY BEAST 
It is immaterial to whom the transfer is made if the purpose be to prefer one creditor to another. Putnam's Handy Law Book for the Layman |Albert Sidney Bolles 
It appears also, as far as absorption goes, to be immaterial whether the ammonia is free or combined. Elements of Agricultural Chemistry |Thomas Anderson 
It is immaterial whether she acquired her estate before or after the birth of the child. Putnam's Handy Law Book for the Layman |Albert Sidney Bolles 
Nor is the fact immaterial that he need not, and would not have made the payment had he known the true state of things. Putnam's Handy Law Book for the Layman |Albert Sidney Bolles 
Like him, he disbelieved in the existence of anything immaterial, for even a human soul is formed out of matter. Beacon Lights of History, Volume I |John Lord