We suggest letting it air dry, as the material can lose its softness in commercial dryers. Best weighted blanket: Sleep like a baby with our comfy bedding picks |PopSci Commerce Team |February 12, 2021 |Popular-Science 
Some blankets can even go into the dryer—but be aware the dryer has to be large enough, and the heat setting must be low. Best heated throw blanket: Bundle up with these electric blankets |PopSci Commerce Team |February 11, 2021 |Popular-Science 
If the toys are still damp after a while in the dryer, let them air-dry overnight. Hints From Heloise: Treasured stuffed animals need a bath |Heloise Heloise |February 5, 2021 |Washington Post 
Then ten to fifteen minutes in the dryer on low should do it. How to Wash Your Ski Kit |Joe Jackson |January 30, 2021 |Outside Online 
Titanium dryers are also typically non-ionic, and when added to the internal components of a hair dryer, can improve consistency and heat distribution. The best hair dryer: Get a salon-worthy blowout at home |Carsen Joenk |January 22, 2021 |Popular-Science 
The images include a Firestone tire, a frosted cake, a nuclear cloud, and a little girl beneath a hair dryer. F-111: Death-Dealing, Pop-Art Masterpiece |Nicolaus Mills |October 15, 2014 |DAILY BEAST 
It insists on efficiency standards for household appliances so that your towels come out of the dryer refreshingly cool and damp. The Federal Government Has Violated My Right to Chainsaw |P. J. O’Rourke |April 27, 2014 |DAILY BEAST 
Finally, seats settled and hair dryer blowing, we're ready to launch into our conversation. There’s Something About Mary Lambert |Abby Haglage |September 19, 2013 |DAILY BEAST 
At some point in healthcare there must be a margin, like me and my clothes dryer. Why Do We Want Prices in Health Care? |Megan McArdle |February 27, 2013 |DAILY BEAST 
I, on the other hand, have a choice to turn my clothes dryer on at noon on the hottest day of the year, or wait until 10pm. Why Do We Want Prices in Health Care? |Megan McArdle |February 27, 2013 |DAILY BEAST 
Wash leaves carefully and put on ice either in lettuce dryer or in a cloth. The New Dr. Price Cookbook |Anonymous 
It was a twisted little smile which might have been born of the pain of stinging lids and dryer, aching throat. Once to Every Man |Larry Evans 
A sugar-cane mill is in operation, connected with which is a centrifugal sugar-dryer, the only one in the State. Petals Plucked from Sunny Climes |Sylvia Sunshine 
By degrees, Wemmick got dryer and harder as we went along, and his mouth tightened into a post-office again. Great Expectations |Charles Dickens 
Turn it on full force and it would still be a thousand times dryer than any place here. The Carter Girls' Week-End Camp |Nell Speed