Similarly, those returns are why Ilves likens travel advisers to stockbrokers. Why 2021 will be the year of the travel agent |Bailey Berg |May 13, 2021 |Washington Post 
Mark Kimsey, a Great Falls stockbroker, boosted Snyder’s bottom line two days before the deadline with a $1 million donation. McAuliffe holds fundraising lead in race for Virginia governor |Laura Vozzella, Antonio Olivo |April 16, 2021 |Washington Post 
When completed, local investors will be able to buy and sell stocks across those three countries without the currently existing hurdles, such as needing different stockbrokers in each country. Three East African countries are merging their stock markets to boost local investment |Yomi Kazeem |September 24, 2020 |Quartz 
As Jordan Belfort, a charismatic monster of a stockbroker, DiCaprio is a feral beast; the id incarnate. Why Leonardo DiCaprio, Who Wows in ‘The Wolf of Wall Street,’ Deserves to (Finally) Win An Oscar |Marlow Stern |February 17, 2014 |DAILY BEAST 
Former stockbroker Jordan Belfort once had sex on $3 million in cash. Reel Numbers: The Golden Globe Nominees Are… (Video) |The Daily Beast Video |January 12, 2014 |DAILY BEAST 
Near the beginning of Wolf, Belfort arrives on Wall Street for his first day of work as an entry-level stockbroker. Finally! ‘The Wolf of Wall Street’ Is Hollywood’s First 1990s Period Piece |Andrew Romano |December 23, 2013 |DAILY BEAST 
Pippa Middleton was snapped snogging her new boyfriend stockbroker Nico Jackson after playing a game of tennis in London. Love All! Pippa Snogs Nico in London After Tennis Match |Tom Sykes |June 11, 2013 |DAILY BEAST 
Nico is a stockbroker at Deutsche Bank, whose family run a ski shop in England. Now Pippa Snogs Her New Squeeze On The Slopes! |Tom Sykes |March 11, 2013 |DAILY BEAST 
The stockbroker had too short a go—he was carried off in his flower. The Tragic Muse |Henry James 
He wouldn't be a theatrical manager—his attire was too formal; or a stockbroker—his attire was not formal enough. Those Times And These |Irvin S. Cobb 
Then there's that bald man in the white robe--his name's Giroflet--a retired stockbroker. In the Days of My Youth |Amelia Ann Blandford Edwards 
This Brown was the son of a Scotch stockbroker living in Lambeth. Keats |Sidney Colvin 
Joseph knew no stockbroker—had only heard of them by rumour. A Book of Ghosts |Sabine Baring-Gould