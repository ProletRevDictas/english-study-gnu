I know many people who think to be an artist means you have to suffer, or at least wallow in old miseries. Mara Wilson Remembers Robin Williams: We're All His Goddamn Kids |Mara Wilson |August 18, 2014 |DAILY BEAST 
Amia, Louie's temporary girlfriend, is gone, leaving him to wallow in his heartbreak—at least for a few scenes. Louie Attempts Rape (and Explores the ‘Nice Guy’ Phenomenon) |Amy Zimmerman |June 3, 2014 |DAILY BEAST 
In our film, Emad is using a language that does not wallow in suffering and in that way he becomes a powerful inspiration. Comfortable Symbols: The Suffering Palestinian and the Good Israeli |Guy Davidi |February 24, 2013 |DAILY BEAST 
But Romney strikes me as a glass-half-full kind of guy, so let us not wallow in the negatives. Super Tuesday: Mitt Romney’s Senior Citizen Surge |Michelle Cottle |March 7, 2012 |DAILY BEAST 
The Wallow is the best known, but not the only, fire now racing through Arizona. Arizona Fires: Mormons Mobilize to Help |Terry Greene Sterling |June 11, 2011 |DAILY BEAST 
Did you not see his crooked claws when he set the bowl before you, that you might wallow in the debasing drink? Skipper Worse |Alexander Lange Kielland 
On the perfect day I have been talking about she hunted up a sunlit puddle and indulged in the first wallow of the season. The Red Cow and Her Friends |Peter McArthur 
Well, Beatrice selected a spot where a defective drain had left the ground soft and trenched it with a luxurious wallow. The Red Cow and Her Friends |Peter McArthur 
The willow tree (Welsh helygen), which grows essentially by the water-side, may be connoted with wallow. Archaic England |Harold Bayley 
But after a lowly wallow in melancholy, a sudden rise of spirits is always viewed with suspicion by a woman. A Yankee from the West |Opie Read