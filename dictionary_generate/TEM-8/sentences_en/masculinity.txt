A more rugged version of American masculinity is hard to find on screen. The Story Behind Lee Marvin’s Liberty Valance Smile |Robert Ward |January 3, 2015 |DAILY BEAST 
What does masculinity look like in a world where men and women alike can be titans? Men Need A Better Men’s Rights Movement |Nancy Kaffer |December 16, 2014 |DAILY BEAST 
However the American sexual ideal is intimately related to a certain idea of masculinity. Living Black & Gay in the ’50s |Alain Mabanckou |December 3, 2014 |DAILY BEAST 
We are looking the same, acting the same, and mimicking masculinity the same. How Straight World Stole ‘Gay’: The Last Gasp of the ‘Lumbersexual’ |Tim Teeman |November 12, 2014 |DAILY BEAST 
In other words, another treatise on masculinity from Pizzolatto. Alright ‘True Detective,’ You Got Me: Taylor Kitsch Is a Woman’s Man |Teo Bugbee |November 1, 2014 |DAILY BEAST 
Breezy freshness, strong masculinity, and almost reckless abandon in the literary texture and dramatic inventions. The Devourers |Annie Vivanti Chartres 
In spite of her aggressive masculinity, she is somehow made in her way really attractive. The Mermaid Series. Edited by H. Ellis. The best plays of the old dramatists. Thomas Dekker. Edited, with an introduction and notes by Ernest Rhys. |Thomas Dekker 
She stared without speaking at his square face, fierce with determination—at his roused, dominating masculinity. To Him That Hath |Leroy Scott 
Whistling merrily he strode on into the dim cool bar, with its heavy brown fixtures and solid atmosphere of masculinity. Death Makes A Mistake |P.F. Costello 
She preferred masculinity sublimated, so to speak—purified by the processes of art. Gray youth |Oliver Onions