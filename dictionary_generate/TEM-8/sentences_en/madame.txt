Lacey Noonan's A Gronking to Remember makes 50 Shades of Grey look like Madame Bovary in terms of its literary sophistication. ‘A Gronking to Remember’ Speed Read: 8 Naughtiest Bits |Emily Shire |January 7, 2015 |DAILY BEAST 
Madame Cézanne is ultimately about the figure in the portraits rather than the person, who remains a tantalizing enigma. Sight Unseen: Cézanne’s Mysterious Wife |Lizzie Crocker |November 19, 2014 |DAILY BEAST 
Last year, while filming the documentary “Madame Presidenta: Why Not U.S.?” What Brazil’s Dilma Rousseff Can Teach Hillary Clinton |Heather Arnet |October 29, 2014 |DAILY BEAST 
When Madame Tussauds unveiled their new waxwork of Kate, people queued up for a chance to feel “her” hair. Why Kate's Hair Matters |Tom Sykes |July 3, 2014 |DAILY BEAST 
Hollywood Madame Michelle Braun told The New York Post in 2009 that Tiger Woods paid $60,000 for four escorts he saw six times. The High Cost of An Orgasm: Is Momentary Pleasure Worth a Lifetime of Regret? |Aurora Snow |June 28, 2014 |DAILY BEAST 
Madame Ratignolle, more careful of her complexion, had twined a gauze veil about her head. The Awakening and Selected Short Stories |Kate Chopin 
Edna did not reveal so much as all this to Madame Ratignolle that summer day when they sat with faces turned to the sea. The Awakening and Selected Short Stories |Kate Chopin 
Madame de Condillac stood watching him, her face composed, her glance cold. St. Martin's Summer |Rafael Sabatini 
She took the fan from Madame Ratignolle and began to fan both herself and her companion. The Awakening and Selected Short Stories |Kate Chopin 
Beside her was a box of bonbons, which she held out at intervals to Madame Ratignolle. The Awakening and Selected Short Stories |Kate Chopin