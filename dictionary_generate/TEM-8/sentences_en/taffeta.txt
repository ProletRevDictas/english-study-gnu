In the Forum Club, there is taffeta and lace, leather and gold. Shaq, Year One |Charles P. Pierce |May 24, 2014 |DAILY BEAST 
Absurd as that taffeta dress was for a child of her age, it seemed to her an armor against all disaster. The Campfire Girls of Roselawn |Margaret Penrose 
Heres a piece of ribbon to tie it with, if you wont cut it off, said Winona, reappearing with a wide length of blue taffeta. Winona of the Camp Fire |Margaret Widdemer 
Just look at him, with an old taffeta whigmaleerie tied to his back, like Paddy from Cork, with his coat buttoned behind! The Mirror of Literature, Amusement, and Instruction |Various 
It was a blue taffeta, very stiff and rustling, trimmed with plaid taffeta and black buttons. Winona of the Camp Fire |Margaret Widdemer 
Taffeta is one of the oldest weaves known, silk under this name having been in constant use since the fourteenth century. Textiles |William H. Dooley