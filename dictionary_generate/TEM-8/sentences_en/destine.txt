Until then, I thought I was just an unlucky soul, destined to live without one of my favorite cooking tools. Don’t Fear the Broiler |Elazar Sontag |February 11, 2021 |Eater 
Truffles seemed destined to be among the short-lived majority. How a sickly squirrel offered me unexpected comfort |Pam Spritzer |February 8, 2021 |Washington Post 
That impulse — not to own the libs but to hone the libs — seems destined to endure. Bernie Sanders is often called a liberal. He’d beg to differ. Who is actually a liberal? |Graham Vyse |February 2, 2021 |Washington Post 
One of the wealthiest women in Africa, Alakija, 69, is a power broker — and may be destined for even greater heights. The Women Reshaping the C-Suite |Nick Fouriezos |January 24, 2021 |Ozy 
It's making something new from an original item destined for the trash. Hints From Heloise: Bullying can follow kids anywhere |Heloise Heloise |January 12, 2021 |Washington Post 
Why, she asked, should she wilfully destine her youth to a hopeless waste of affection, and dearth of all permanent comfort? Camilla |Fanny Burney 
Il y a sur nous une prdiction, une destine qui s'accomplit presque invitablement de pre en fils. Paris and the Parisians in 1835 (Vol. 2 of 2) |Frances Trollope 
Le dechirant et lugubre cortege se dirigeait vers la place du marche destine aux executions criminelles. Travels in France during the years 1814-1815 |Archibald Alison 
I shall, in my secret heart, destine her for you, and it is in this light I shall think of you for the future. Monsieur de Camors, Complete |Octave Feuillet 
You fus' kiss the young lady I destine faw my sultana, an' now you offeh me a briibe! John March, Southerner |George W. Cable