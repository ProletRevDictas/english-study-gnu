The program began to teeter under the weight of its own outsized expectations, questionable staffing decisions, and naivete. Send in the Marines—and the Anthropologists too? |John Kael Weston |August 23, 2013 |DAILY BEAST 
It is a moment in history worth remembering as we once more teeter on a historical precipice. Mideast War in Our Time? |Jamie Dettmer |May 31, 2013 |DAILY BEAST 
The economy will teeter, one foot over the cliff, while members of Congress soak up the recess sun or swoosh down the ski slopes. Robert Shrum: Obama Won Election & Will Win Again on Fiscal Cliff |Robert Shrum |December 7, 2012 |DAILY BEAST 
Unaware of their own strength, they teeter on the edge of the victim abyss. A Writer's International Wanderings |Susan Salter Reynolds |May 9, 2010 |DAILY BEAST 
The one-liners, ungrounded in the best of times, now teeter dangerously close to nastiness. The Banned 'Family Guy' Episode |Paul Cullum |August 13, 2009 |DAILY BEAST 
A broad and massive teeter-board was brought in, and balanced across a support about two feet high. Kings in Exile |Sir Charles George Douglas Roberts 
Instantly King descended from his pedestal, ran over to the teeter-board, and mounted it at the centre. Kings in Exile |Sir Charles George Douglas Roberts 
And when he stooped over to pick up his child he turned into a sand-snipe, and the baby turned into a little teeter-snipe. Aw-Aw-Tam Indian Nights |J. William Lloyd 
A few birds flew up from along the shore, some of them "teeter" snipe that had been feeding. The Boy Scouts' First Camp Fire |Herbert Carter 
But betwixt the pair of you you've nigh druv two old women crazy, and set the whole village a-teeter. The Brass Bound Box |Evelyn Raymond