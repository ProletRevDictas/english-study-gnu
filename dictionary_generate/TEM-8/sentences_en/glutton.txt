The last time I made it, this glutton for punishment asked his significant other to grade it. The food critic gets critiqued: What Tom Sietsema learned in a year of pandemic home cooking |Tom Sietsema |July 14, 2021 |Washington Post 
The Daily Pic: The Italian was such a glutton for order, he could have been the first conceptualist. Proof That Morandi Was Sol LeWitt's Teacher? |Blake Gopnik |September 5, 2013 |DAILY BEAST 
For while he can be as diverting as the greatest glutton for mirth desires, he has all the machinery of dejection too. Adventures and Enthusiasms |E. V. Lucas 
I see the tiny drops spurt out, lapped up by the glutton as soon as they appear. More Hunting Wasps |J. Henri Fabre 
At Glutton Mill the character of the scenery changes with the geological nature of the country. The Rivers of Great Britain: Rivers of the East Coast |Various 
Most high and mighty Czar of all flesh, ceaseless reducer of empires, unfathomable glutton in the whole realms of nature. Poems of The Third Period |Friedrich Schiller 
I was like a glutton before a table piled high with delicacies and with plenty of time to spare. The Blue Germ |Martin Swayne