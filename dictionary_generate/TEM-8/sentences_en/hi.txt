Somehow, these hi-def blurts led to collaborations with the likes of Vince Staples and Madonna, swiftly bringing Sophie’s happy maximalism to the greater pop listenership she’d always hoped to reach. Sophie pushed feel-good music toward a boundary that might not exist |Chris Richards |February 1, 2021 |Washington Post 
A formal federal program could help by offering clear guidance on hi-fi masks. Everyone should be wearing N95 masks now |Joseph G. Allen |January 26, 2021 |Washington Post 
She did the first one as a dry run a few weeks back, and it was fun to meet some Losers for the first time, as well as some loyal fans, and to say hi to some of the regulars. Style Conversational Week 1420: Back to the vocal point |Pat Myers |January 21, 2021 |Washington Post 
Getting outside and saying hi to shops’ staff and being proactive are empowering tools against anxiety. In spite of it, how to live our best winter |Tom Sommers |January 12, 2021 |Washington Blade 
You’re either in over-and-out mode, or you’re in hi-this-is-a-nice-video-recording-I-recorded-several-hours-ago mode, which is like email. To Boldly Go Where No Internet Protocol Has Gone Before |Susan D'Agostino |October 21, 2020 |Quanta Magazine 
People we pass on the street, sit next to on the subway, say hi to at work every day. ‘High Maintenance,’ Like a Good High, Is Funny and Sometimes Unsettling |Caitlin Dickson |November 11, 2014 |DAILY BEAST 
Another person regrets not saying hi to Harry at the Gramercy Hotel. The Cult of Blondie: Debbie Harry’s Very Special New York Picture Show |Tim Teeman |October 1, 2014 |DAILY BEAST 
And he said hi back and smiled and sort of patted the baby on the head and everything. The Stacks: Grateful Dead I Have Known |Ed McClanahan |August 30, 2014 |DAILY BEAST 
Robin crossed to me from across the room, got down to my level, and whispered “Hi, how are you?” Mara Wilson Remembers Robin Williams: We're All His Goddamn Kids |Mara Wilson |August 18, 2014 |DAILY BEAST 
Below is the email response I received: Hi, thank you but we respectfully decline. You Don’t Hate Feminism. You Just Don’t Understand It. |Emily Shire |July 24, 2014 |DAILY BEAST 
Hi just hasked to learn your custom hin case hi did lose hany. The Book of Anecdotes and Budget of Fun; |Various 
The former at least has been accentuated since yesterday, when your likeness to Hi struck me very painfully. Ancestors |Gertrude Atherton 
"Hi, those foolish young men are getting ready to ride over here," said White Otter. Three Sioux Scouts |Elmer Russell Gregor 
The men is fair wild wid th' drink, and th' Rough Red is beside hi'self. Blazed Trail Stories |Stewart Edward White 
But mebbe up in heaven he will think of me and wait And holler "Hi!" With the Colors |Everard Jack Appleton