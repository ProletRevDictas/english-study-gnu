Directions: Preheat your oven to 400°F. Place the potatoes, celeriac and onion in an earthenware-type baking dish. Butternut Squash, Plus Some Celeriac |The Daily Beast |November 25, 2008 |DAILY BEAST 
Fanchon's grandmother stirs up the drowsy fire; then she breaks the eggs on the black earthenware platter. Child Life In Town And Country |Anatole France 
Pour water in an earthenware jar, place the plates in it and turn the plug in a lamp socket. The Boy Mechanic, Book 2 |Various 
Putting in front of them an earthenware jar, made to the width of the mine,The Romans smoked out. The Histories of Polybius, Vol. II (of 2) |Polybius 
In front are the footlights, a row of earthenware bowls filled with oil, with a lighted wick floating in each one. Round the Wonderful World |G. E. Mitton 
As they had no earthenware vessels, they had no idea that water could be heated. Celebrated Travels and Travellers |Jules Verne