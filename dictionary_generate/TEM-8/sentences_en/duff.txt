An op-ed today by regular Press TV contributor Gordon Duff leaps to Paul's defense. Rand Paul's Biggest Liability: His Dad |David Frum |April 26, 2013 |DAILY BEAST 
His roster included Taylor Swift, Hilary Duff, Paris Hilton, and country star Dwight Yoakam. Britney Spears and Jason Trawick: He’s Her Fiancé, Manager, and Conservator |Ramin Setoodeh |September 11, 2012 |DAILY BEAST 
Like many of the competitors, neither she nor Duff is concerned about winning, only with finishing. Vermont’s Amazing, Grueling, Traumatic Race |Stuart Miller |June 22, 2011 |DAILY BEAST 
Even B-list celebrities like Hilary Duff can command more than six figures, as she did for the wedding photos she sold to OK! Was Arnie’s Mistress Cheated? |The Daily Beast |June 16, 2011 |DAILY BEAST 
Moderated By: Charlie Rose, Executive Editor and Anchor, Charlie Rose • Amy Chua, John M. Duff, Jr. Women in the World 2011 Agenda |The Daily Beast |March 4, 2011 |DAILY BEAST 
That crack down in the back lane at Edmonton, Blathers, said Mr. Duff, assisting his colleagues memory. Oliver Twist, Vol. II (of 3) |Charles Dickens 
Father Duff couldn't have stumbled on a more unhappy example for himself. My New Curate |P.A. Sheehan 
"I can learn, I reckon," said Ralph so heartily that Mr. Duff took a second look at the boy, then smiled to himself. Ralph Granger's Fortunes |William Perry Brown 
Ralph walked back to where Mr. Duff was standing at the binnacle, conning the ship. Ralph Granger's Fortunes |William Perry Brown 
At this Mr. Duff laughed outright, and the sailors nudged each other as if highly tickled. Ralph Granger's Fortunes |William Perry Brown