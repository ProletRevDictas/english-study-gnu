Especially not by the faceless boogymen that spew threats with no accountability. Sarah Silverman’s History of Pro-Woman, Liberal, and Vagina-Related Activism |Asawin Suebsaeng |October 8, 2014 |DAILY BEAST 
Look, Hice has every right to spew his hate from the pulpit to those who chose to attend his services. Jody Hice: Mr. Bigot Goes to Washington? |Dean Obeidallah |June 24, 2014 |DAILY BEAST 
As I listen to myself and the real Bert spew inanities, I feel terrible, as if I am mocking a world before its very benefactors. My Great Art-Hoax Experiment |Murray Miller |December 9, 2012 |DAILY BEAST 
Small homes often have only kerosene lamps to provide light, which spew toxins equivalent to two packs of cigarettes a day. sOccket Inventors: Being Young and Stubborn Helps Innovation |Casey Schwartz |March 9, 2012 |DAILY BEAST 
I “embrace” my “otherness,” to spew that overused phrase of early '90s identity reclamation. Norway’s New Cultural Anxiety |Keshni Kashyap |July 24, 2011 |DAILY BEAST 
And I took oath that I would never again spew a filthy expression from my mouth or do an ill thing. Tramping on Life |Harry Kemp 
So then because thou art lukewarm, and neither cold nor hot, I will spew thee out of my mouth. Walks and Words of Jesus |M. N. Olmsted 
With his war club he strikes repeated blows upon the heart of the fish, which attempts to spew him out. Myths of the Cherokee |James Mooney 
Because inferres the reason; as, I wil spew the out, because thou art nether hoat nor cald. Of the Orthographie and Congruitie of the Britan Tongue |Alexander Hume 
Because thou art neither cold nor hot, I will spew thee out of my mouth. Around The Tea-Table |T. De Witt Talmage