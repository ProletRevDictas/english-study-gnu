As a broadcast journalist himself, Itay Hod loved the ‘Today’ show. ‘Good Morning America’ vs. ‘Today’: Why I’ve Switched |Itay Hod |October 9, 2012 |DAILY BEAST 
Itay Hod attends a ceremony that left not a dry eye in the house. Broadway's Gay-Marriage Bonanza |Itay Hod |July 30, 2011 |DAILY BEAST 
He tells Itay Hod about his nightly drag transformation and his tabloid feud with Mario Lopez. Broadway's New Queen |Itay Hod |March 17, 2011 |DAILY BEAST 
In the half-burnt artists' village of Ein Hod, it consumed houses and works of art, but thankfully no lives. Israel's Inferno |Fania Oz-Salzberger |December 5, 2010 |DAILY BEAST 
Itay Hod on the stunningly lucrative spectacle that had cash-strapped mayors going to the mat. Let the Gay Games Begin |Itay Hod |October 2, 2009 |DAILY BEAST 
I speired at her whaur she had hod it, but she juist said, 'What would I be doin' hoddin't'?' A Window in Thrums |J. M. Barrie 
He kent 'at if she'd hod it, the kitchen maun be the place, but he thocht she'd gi'en it to me to hod. A Window in Thrums |J. M. Barrie 
So she had to go down cellar and bring up as much as she could in the hod. Pages for Laughing Eyes |Unknown 
Broadcloth is wiser, just as a skilled workman is wiser than a hod carrier. The Diamond Coterie |Lawrence L. Lynch 
Fanny opened the coal-hod, intending to put fresh coals on the dying fire; but, to her distress, found that the hod was empty. Betty Vivian |L. T. Meade