A he-spider hulk at 8 millimeters barely reaches half the length of small females. Invasive jorō spiders get huge and flashy — if they’re female |Susan Milius |March 30, 2022 |Science News 
When he looked back, he saw an enormous hulk of brown fur heading toward Pieciul. It’s No Fun to Wake a Sleeping Bear |jversteegh |August 11, 2021 |Outside Online 
In the near distance, the decaying hulk of RFK Stadium loomed. Speed at their fingertips: Remote-control racing takes off in the District |Joe Heim |July 2, 2021 |Washington Post 
In moments of duress, a different self manifests with acts of destruction: unleashed id in Freudian, or Incredible Hulk, terms. A Different Kind of Vietnam Story |J.T. Price |October 9, 2014 |DAILY BEAST 
Hulk has a more prominent role, and I have a more prominent role. Jeremy Renner Opens Up About Marriage, His Problems with the Media, and the Future of Hawk-Eye |Marlow Stern |September 29, 2014 |DAILY BEAST 
I just have this personal mental obsession with the Hulk being an Asian-American male icon. Model Minority Rage: Why the Hulk Should Be an Asian Guy |Arthur Chu |July 18, 2014 |DAILY BEAST 
Whenever I take a clickbait quiz to determine which of The Avengers I would be, I always game the questions to aim for the Hulk. Model Minority Rage: Why the Hulk Should Be an Asian Guy |Arthur Chu |July 18, 2014 |DAILY BEAST 
He disallowed a goal scored by Hulk that would have given Brazil a 2-1 lead. World Cup 2014 Nail-Biter: Host Country Brazil Defeats Chile on Penalty Kicks |Tunku Varadarajan |June 28, 2014 |DAILY BEAST 
But as no junk-man came, and as no one could be found to care for its now sadly battered hulk, its good riddance became a problem. The Real Latin Quarter |F. Berkeley Smith 
A few minutes before she had been a stately three-masted frigate; now she was a helpless hulk. Stories of Our Naval Heroes |Various 
You've put in good work to-night all right, and saved this old hulk from drifting into harbour. Menotah |Ernest G. Henham 
It was on such a night that a great black hulk moved like a sable monster through the waters off the coast of Cuba. Stories of Our Naval Heroes |Various 
She is a dirty commonplace hulk, packed with men in soiled clothes, no longer the radiant white ship of our vision. Round the Wonderful World |G. E. Mitton