Pornography is here to stay—a consequence of living in a free country. Rick Santorum’s War on Pornography Stirs Up the Same Tired Arguments |Kristin Battista-Frazee |March 20, 2012 |DAILY BEAST 
The Pornography of Terror By Christopher Dickey, Oct. 1, 2011 Maybe now the madness can end. Cheney Cheers Awlaki Kill | |October 2, 2011 |DAILY BEAST 
Pornography is sexual expression; human trafficking and slavery are crimes. Shame on Michele Bachmann! |Larry Flynt |July 9, 2011 |DAILY BEAST 
"Pornography has crawled out of the muck and has joined trash culture," she says. Celebrity Mistress Convention |Tricia Romano |July 11, 2010 |DAILY BEAST 
Pornography, an eminent American jurist has pointed out, is distinguished by the "leer of the sensualist." 1601 |Mark Twain