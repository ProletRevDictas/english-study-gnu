"About that little adventure of Tom Betts in the river," interrupted Jud. The Banner Boy Scouts on a Tour |George A. Warren 
To his mind, it did not appear at all unlikely that Jud Bates had mischief in hand. That Lass O' Lowrie's |Frances Hodgson Burnett 
"That as I've towd thee is nowt to th' rest on it," answered Jud in enthusiasm. That Lass O' Lowrie's |Frances Hodgson Burnett 
Jud drew Nib closer, and turned, if possible, a trifle paler. That Lass O' Lowrie's |Frances Hodgson Burnett 
Jud Elderkin made this surprising statement after he had gone to the door to take a peep at the weather. The Banner Boy Scouts Snowbound |George A. Warren