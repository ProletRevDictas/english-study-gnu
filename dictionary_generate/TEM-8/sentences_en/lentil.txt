“Liked the lentils … old-school graphics,” Ace Copy Editor Doug Norwood told me charitably about the reverse-crossword grid in Style Invitational Week 1446. Style Conversational Week 1446: Lentils entertain you |Pat Myers |July 22, 2021 |Washington Post 
I started learning to cook right out of college, when I lived in a group house with other young people who were always semi-broke, so we ate a lot of beans, lentils, rice, potatoes, and noodles. Why I Sent Myself to Vegetarian Boot Camp |mturner |July 15, 2021 |Outside Online 
It’s giving me plenty of motivation to finally work through the once-toddler-size bag of lentils in my cupboard. Sick of Those Panic-Bought Beans? Try Sprouting Them |Aliza Abarbanel |April 15, 2021 |Eater 
James Park, Eater social media managerTo be honest, I’ve never been a huge fan of lentil soup. Everything Eater Editors Have Cooked in 2021 |Eater Staff |March 26, 2021 |Eater 
James Park, Eater social media managerTo be honest I’ve never been a huge fan of lentil soup. Six Recipes That Got Us Through Another Week |Eater Staff |March 19, 2021 |Eater 
Tonight's dinner--lentil soup with ham--went in the slow cooker before work this morning. Friday Forum: How often do you cook? |Megan McArdle |January 11, 2013 |DAILY BEAST 
We have a rotating list of soups that we serve at François Payard Bakery, but my current favorite is the lentil. Fresh Picks by François Payard |François Payard |March 3, 2011 |DAILY BEAST 
If you can find it, opt for a soup with a legume base like lentil or black bean. 9 Unhealthiest Takeout Foods |Divya Gugnani |October 24, 2010 |DAILY BEAST 
Depending upon how much lamb is included, the stew can be lentil with lamb or lamb with lentils. What to Eat: One-Pot Meals for a Busy Holiday Season |Cookstr.com |December 15, 2009 |DAILY BEAST 
Lentil and Feta Cheese Salad by Nava Atlas and Fran Bigelow A few bites to transport you to the Mediterranean. What to Eat |Cookstr.com |July 14, 2009 |DAILY BEAST 
The radio was blurting either soap operas, hill-billy music, or lentil-mouthed commentators. Cue for Quiet |Thomas L. Sherred 
What are then the objections to the point as illustrated in bean, coffee-berry, seed, and wooden lentil? Froebel's Gifts |Kate Douglas Wiggin 
Within the cups lentil-shaped bodies are attached to the base and sides by elastic cords. Fungi: Their Nature and Uses |Mordecai Cubitt Cooke 
Some of these vesicles were the size of a lentil, and others as small as a millet-seed. Buffon's Natural History, Volume III (of 10) |Georges Louis Leclerc, Comte de Buffon 
At some distance from this glandular body there was a small one of the same kind, about the size of a lentil. Buffon's Natural History, Volume III (of 10) |Georges Louis Leclerc, Comte de Buffon