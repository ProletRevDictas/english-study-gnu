We cannot dither, we cannot just twiddle our thumbs, or wait and see. After Steven Sotloff Murder, Congress Demands a Vote on Obama’s ISIS War |Josh Rogin |September 2, 2014 |DAILY BEAST 
We dither, we argue, we do little, as the world moves rapidly. Obama’s Deficit Plan Falls Short |Zachary Karabell |September 19, 2011 |DAILY BEAST 
Global markets have been remarkably stable while the politicians dither, but time is running out. Debt Talks Turn Dire |Eleanor Clift |July 14, 2011 |DAILY BEAST 
But unlike the epic drama of the 2000 debacle, this result feels like the big British dither. Florida on the Thames |Tina Brown |May 7, 2010 |DAILY BEAST 
The planks at his feet had started to dither again, and practice told him that the vessel must be moving. The Hero of Panama |F. S. Brereton 
The universe goes into a cosmic dither when we slide into a berth in Hampton Rhodus. Operation Earthworm |Joe Archibald 
Imagine anyone trying to get the Old Man into a dither—and getting away with it. The Best Made Plans |Everett B. Cole 
And the more perfectly made the engine, the less will the amount of this "dither" be. The Practice and Science Of Drawing |Harold Speed 
I went all o' a dither, while I hardly knew if I were standin' on my heels or my heead. More Tales of the Ridings |Frederic Moorman