“The marvellous thing about him was that he played it extremely cool,” Mavis said. Week in Death: The Woman Who Cracked Hitler’s Codes |The Telegraph |November 17, 2013 |DAILY BEAST 
The “marvellous,” then, can be scientific, religious, or philosophical. London's Guerrilla Art |Olivia Cole |October 15, 2009 |DAILY BEAST 
Almost half the 60 works in The Age of the Marvellous have already sold and it's only been officially open a day. London's Guerrilla Art |Olivia Cole |October 15, 2009 |DAILY BEAST 
The Age of the Marvellous is curated by London-based American Joseph LaPlaca. London's Guerrilla Art |Olivia Cole |October 15, 2009 |DAILY BEAST 
Tausig possessed this repose in a technical way, and his touch was marvellous; but he never drew the tears to your eyes. Music-Study in Germany |Amy Fay 
This new exalted state was very marvellous; for while it lasted he welcomed all that was to come. The Wave |Algernon Blackwood 
Now an automobile was a marvellous dragon for Rosemary, and she could never see too many for her pleasure. Rosemary in Search of a Father |C. N. Williamson 
The quicker the climax came, the sooner would he know the marvellous joy that lay beyond the pain. The Wave |Algernon Blackwood 
How can we rely upon such evidence after nineteen hundred years, and upon a statement of facts so important and so marvellous? God and my Neighbour |Robert Blatchford