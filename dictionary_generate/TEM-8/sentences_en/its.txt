Many people have already written off ‘American Idol’ as a past-its-prime reality TV corpse. 7 Ways ‘American Idol’ Revolutionized TV |Kevin Fallon |January 14, 2014 |DAILY BEAST 
The late-in-its-run success of Breaking Bad is the perfect example of that. Why We Binge-Watch Television |Kevin Fallon |January 8, 2014 |DAILY BEAST 
One young woman had affixed Post-its to the wall reading “Welcome Home Annie!” A Tale of Thanksgiving Triumph |Michael Daly |November 28, 2013 |DAILY BEAST 
Over dessert, we look through the Post-Its full of squiggly pictures. Are the 40 Days Duo Really a Couple? |Abby Haglage |September 4, 2013 |DAILY BEAST 
When Demi Moore was hospitalized last year, friends told TMZ she had seizure-like symptoms brought on by doing whip-its. Everything You Never Wanted to Know About Whip-Its |Winston Ross |March 22, 2013 |DAILY BEAST 
Jack probably learned more about the Bible during that trip-its history and its heroes-than during all his former years. Mark Twain, A Biography, 1835-1910, Complete |Albert Bigelow Paine 
The world had nothing more to give him now except that which he had already long possessed-its honor and its love. Mark Twain, A Biography, 1835-1910, Complete |Albert Bigelow Paine 
Didn't Wolfe say that he would rather have written what's-its-name than taken Quebec? Punch, or the London Charivari, Vol. 147, December 30, 1914 |Various 
My heart just sank like a lump of what's-its-name, but my whole soul went out in sympathy for her. The Haunted Pajamas |Francis Perry Elliott 
Then Billings, who was already gasping like a jolly what's-its-name, dropped upon the arm of the chair and held his side. The Haunted Pajamas |Francis Perry Elliott