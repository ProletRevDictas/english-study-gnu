I still do find it a tremendously useful device to invent a character and have the character sing the song. Belle & Sebastian Aren’t So Shy Anymore |James Joiner |January 7, 2015 |DAILY BEAST 
By the time it concluded with a sing-a-long of “XO,” Beyoncé had done the rare thing. Bow Down, Bitches: How Beyoncé Turned an Elevator Brawl Into a Perfect Year |Kevin Fallon |December 31, 2014 |DAILY BEAST 
Yep, the song the Whos sing in How the Grinch Stole Christmas. Yes, I Like Christmas Music. Stop Laughing. |Michael Tomasky |December 24, 2014 |DAILY BEAST 
He could sing Beatles songs with as much authenticity as the Liverpool lads themselves—and sometimes with even more fervor. The Greatest Rock Voice of All Time Belonged to Joe Cocker |Ted Gioia |December 23, 2014 |DAILY BEAST 
We sing “Rudolph the Red-Nosed Reindeer” and “Jingle Bells”. Congress’ Gift That Keeps on Giving |P. J. O’Rourke |December 20, 2014 |DAILY BEAST 
Strive to speak or sing fluently without breaking the quality of tone used. Expressive Voice Culture |Jessie Eldridge Southwick 
While half of Christendom sing “On earth peace, good will toward men,” the other half sing, “On earth peace to men of good will.” Solomon and Solomonic Literature |Moncure Daniel Conway 
I suppose the hammer falls back more slowly from the string, and that makes the tone sing longer. Music-Study in Germany |Amy Fay 
He sat down and played it phrase by phrase, pausing between each measure, to let it "sing." Music-Study in Germany |Amy Fay 
Flocks of birds seemed to sing through the air, striking against the telegraph wires. A Lost Hero |Elizabeth Stuart Phelps Ward and Herbert D. Ward