But the two presenting together, as they did here at the 2008 Emmy Awards, is an out-of-this-world combination. Golden Globes Hosts Tina Fey & Amy Poehler’s Funniest Moments (Video) |Kevin Fallon |January 12, 2014 |DAILY BEAST 
But on the relative plane, Zen is this-worldly and does not deny ethics, or ontology for that matter. The Shocking Scandal at the Heart of American Zen |Jay Michaelson |November 14, 2013 |DAILY BEAST 
The bogus cry of this-raises-serious-concerns was soon heard throughout the land. The Politicization of the Boston Bombings Has Begun |Howard Kurtz |April 21, 2013 |DAILY BEAST 
If he thinks that he can get a deal he likes, I'd expect to see a c'mon-guys-we-can-do-this conciliatory exhortation. What to Look for In Tonight's State of the Union |Megan McArdle |February 12, 2013 |DAILY BEAST 
True, it can be fun to know that this-or-that athlete is Jewish. Redeeming 'Jewish Jocks' |Spencer Ackerman |October 31, 2012 |DAILY BEAST 
Rodney is thoroughly and comfortably this-worldly; Michael is—other-worldly! Jane Journeys On |Ruth Comfort Mitchell 
What has made thy heart so sore as to come and cry a-this-ons? Lizzie Leigh |Elizabeth Gaskell 
For marriage is like life in this-that it is a field of battle, and not a bed of roses. The Pocket R.L.S. |Robert Louis Stevenson 
Waal, we're ergwine ter nail up thet door ternight an' quit this-hyar place. The Code of the Mountains |Charles Neville Buck 
"I reckon Newty's got a license ter dwell in this-hyar house," she belligerently asserted. The Code of the Mountains |Charles Neville Buck