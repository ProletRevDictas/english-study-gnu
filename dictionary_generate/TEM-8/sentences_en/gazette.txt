Gays have won, Mr. Barron said in his op-ed for the Pittsburgh Post-Gazette, and Christians are now “outlaws” and “martyrs.” Do LGBTs Owe Christians an Olive Branch? Try The Other Way Around |Jay Michaelson |December 14, 2014 |DAILY BEAST 
In 2012, according to Saudi Gazette, Saudi authorities arrested 215 supposed magicians. Will Saudi Arabia Execute Guest Workers for 'Witchcraft'? |Michael Schulson |March 29, 2014 |DAILY BEAST 
The Prince arrived late last night, according to local paper the Famagusta Gazette. Prince Harry Given Four Cans Of Lager to Unwind in Cyprus |Tom Sykes |January 22, 2013 |DAILY BEAST 
On October 12, Government Gazette Notice 832 of 2012 was published. The South African "Made In" Fight |Kevin Bloom |October 30, 2012 |DAILY BEAST 
But the French critic Louis de Fourcaud, writing in the Gazette des Beaux-Arts, called it a masterpiece of characterization. The Scandal of Madame X |David McCullough |May 22, 2011 |DAILY BEAST 
Also there is a description of Bampton, which once thrilled the readers of the Tiverton Gazette. Punch, or the London Charivari, Volume 107, November 3, 1894 |Various 
In the Railway Official Gazette was a column devoted to short reviews of new books which were sent to the editor. Fifty Years of Railway Life in England, Scotland and Ireland |Joseph Tatlow 
A bend in the road near the ruined offices of the Delhi Gazette gave them a chance of increasing the pace to a gallop. The Red Year |Louis Tracy 
This operation was reported in the "Gazette des Hopitaux;" but the patient died. Repertory Of The Comedie Humaine, Complete, A -- Z |Anatole Cerfberr and Jules Franois Christophe 
In despair he went to The Daily Gazette office and proclaimed himself ready for a job. Jaffery |William J. Locke