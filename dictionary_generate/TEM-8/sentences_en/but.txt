And more I cannot explain/but you, from what I did not say/will infer what I do not say. Sor Juana: Mexico’s Most Erotic Poet and Its Most Dangerous Nun |Katie Baker |November 8, 2014 |DAILY BEAST 
And there is always the easy-but-true charge of Hollywood hypocrisy. Russell Brand’s Revolution For Morons |Michael Moynihan |November 2, 2014 |DAILY BEAST 
Sheets of bright-but-not-too-bright blue streaked with thin clouds. Native American Basketball Team in Wyoming Have Hoop Dreams Of Their Own |Robert Silverman |August 31, 2014 |DAILY BEAST 
And in those conflicts, the Predator would be all-but-useless. The Killer Drone Goes Stealthy—Just in Time for a New Cold War |Zach Rosenberg |April 16, 2014 |DAILY BEAST 
And we all remember good-but-overpraised songs like If I Had a Hammer and the treacly classic Where Have All the Flowers Gone? The Death of 'Stalin's Songbird' |Michael Moynihan |January 29, 2014 |DAILY BEAST 
When he first worked her she had the old bee-but boiler, 24 feet in diameter. Life of Richard Trevithick, Volume II (of 2) |Francis Trevithick 
Till now one with sudden hiss: "But-good Christ-just look-why, the roof's leaning—!" The Lord of the Sea |M. P. Shiel 
Our friend with his infinite variety and flexibility, we know-but can we put him in? The Pocket R.L.S. |Robert Louis Stevenson 
Eligible single gentlemen pass and repass-but there is no invitation for to inquire within or without. The Pickwick Papers |Charles Dickens 
And again there came to Honoria that ache of longing for the but-half-disclosed glory and fulness of life. The History of Sir Richard Calmady |Lucas Malet