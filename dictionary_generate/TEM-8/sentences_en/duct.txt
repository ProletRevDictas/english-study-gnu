The patch is patterned with artificial sweat ducts, similar to human skin pores. No sweat |Jennifer Chu |August 24, 2021 |MIT Technology Review 
I’d camp instead of pay for a hotel, and I wore the same puffy jacket forever, patching holes with duct tape. Young, Dumb, and Broke: Why Outdoorsy Types Suck at Money |jversteegh |July 13, 2021 |Outside Online 
When the baby is born, the luminal cells, which line the inside of the ducts, produce the proteins that comprise milk. The Cancer Custodians - Issue 102: Hidden Truths |Lina Zeldovich |June 23, 2021 |Nautilus 
The images of sprouting ducts look like blossoming trees in the spring while later they shrivel like plants do in the fall. The Cancer Custodians - Issue 102: Hidden Truths |Lina Zeldovich |June 23, 2021 |Nautilus 
The myoepithelial cells reside outside the ducts and work as muscles that squeeze the ducts to push milk out. The Cancer Custodians - Issue 102: Hidden Truths |Lina Zeldovich |June 23, 2021 |Nautilus 
With a bit of luck and duct tape, I thought we might put together enough votes to avoid a runoff. How Thad Cochran Pulled Off a Win Over Chris McDaniel (Simple, Really) |Stuart Stevens |June 30, 2014 |DAILY BEAST 
The windows were sealed shut around the edges by duct tape but still rattled when it got windy. Almost Famous: A Father's Day Story |Alex Belth |June 15, 2014 |DAILY BEAST 
He binds the little flyer with black plastic ties and seals her mouth with duct tape. The Stacks: The Searing Story of How Murder Stalked a Tiny New York Town |E. Jean Carroll |April 19, 2014 |DAILY BEAST 
Nobody was allowed in his room, where the windows were covered with black garbage bags secured with duct tape. We Already Know What Adam Lanza’s Real Motive Was at Sandy Hook |Michael Daly |November 26, 2013 |DAILY BEAST 
The large roll of duct tape was still attached and found next to her head by firefighters. Autopsies on Hannah Anderson’s Family Bring Police No Closer to a Motive |Christine Pelisek |September 24, 2013 |DAILY BEAST 
Another smaller lymph duct enters the right subclavian vein. A Civic Biology |George William Hunter 
Above I could see the end of the duct faintly in the light coming up through the open chamber door from the utility room. Greylorn |John Keith Laumer 
Having ligated the duct, he saw it swell below and become empty above the ligature. An Epitome of the History of Medicine |Roswell Park 
The duct that bears his name was discovered during his residence in Leyden or at Amsterdam. An Epitome of the History of Medicine |Roswell Park 
The ancient conception of the artery as an air-duct gave rise to the derivation from Gr. Chambers's Twentieth Century Dictionary (part 1 of 4: A-D) |Various