When I first pulled the cork, my initial impression was of green under-ripeness, but that disappeared within about 15 minutes. These 3 delicious wines are kosher for Passover and start at $12 |Dave McIntyre |March 12, 2021 |Washington Post 
If Frank Lloyd Wright had a van today, you can bet there’d be cork in it. How to Build a Green Van |Lauren Matison |March 11, 2021 |Outside Online 
As for the floor insulation, we ordered cork rolls from Manton. How to Build a Green Van |Lauren Matison |March 11, 2021 |Outside Online 
Tyler Reith of Greenhome Solutions recommended cork for its thermal, vibrational, and acoustic-insulating properties. How to Build a Green Van |Lauren Matison |March 11, 2021 |Outside Online 
Enter Workvivo, a Cork, Ireland-based employee experience startup that is backed by Zoom founder Eric Yuan and Tiger Global that has steadily grown over 200% over the past year. Inside Workvivo’s plans to take on Microsoft in the employee experience space |Mary Ann Azevedo |March 4, 2021 |TechCrunch 
In fact, Viagra was originally developed in County Cork, Ireland with the intention of improving blood flow to the heart. Viagra Promising for Muscular Dystrophy Patients |Emily Shire |May 9, 2014 |DAILY BEAST 
The floors were softened by cork tiles, the walls by Philippine mahogany paneling. The Night Vince Lombardi Lay Awake Brooding Over a 49-0 Win |W.C. Heinz |January 25, 2014 |DAILY BEAST 
Eyebrows Cressida: Imagine eyebrows drawn on a balloon with a blackened cork. Cressida Bonas vs. Cara Delevingne: A Differentiation Guide to Two Celebrity English Roses |Tom Sykes |June 26, 2013 |DAILY BEAST 
The two move from Vermont, where Fred worked in corporate training, to County Cork. Must Reads: Margot Livesey, Robert Walser and More |Wendy Smith, Malcolm Forbes, Jane Ciabattari |February 16, 2012 |DAILY BEAST 
Their nanny is from Cork, Ireland, and she is amazing and hilarious. Neil Patrick Harris Hosts the Tonys |Kevin Sessums |June 10, 2011 |DAILY BEAST 
And the deep grave weltering below you, and only a ring of cork and oilskin to keep you out of that cold home. The Chequers |James Runciman 
But no sooner was she full than the discharging tubes freed her, and she rose again and again, buoyant as a cork. The Floating Light of the Goodwin Sands |R.M. Ballantyne 
Perrott went to Cork, where the sessions were well attended and where he executed sixty more persons. Ireland Under the Tudors, Vol. II (of 3) |Richard Bagwell 
He married the eldest daughter of the Earl of Cork, by whom he had a son and a daughter. Private Letters of Edward Gibbon (1753-1794) Volume 1 (of 2) |Edward Gibbon 
Cork and Cloyne were united in the fifteenth century, and Dominic Tirrey was appointed in 1536. Ireland Under the Tudors, Vol. II (of 3) |Richard Bagwell