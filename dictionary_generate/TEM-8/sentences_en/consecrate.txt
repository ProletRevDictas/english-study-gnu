Let his countrymen consecrate the memory of the heroic general, the patriotic statesman, and the virtuous sage. A Compilation of the Messages and Papers of the Presidents |Edited by James D. Richardson 
These divinest poets consecrate the spot, and throw a reflected glory over the humblest of their companions. Our Old Home, Vol. 2 |Nathaniel Hawthorne 
For to what but to felicity should men consecrate themselves, were felicity a goddess? The City of God, Volume I |Aurelius Augustine 
Seven days shall they purge the altar and purify it; and they shall consecrate it. The Prophet Ezekiel |Arno C. Gaebelein 
It is all consecrated but one thing, and I can't consecrate that. Beth Woodburn |Maud Petitt