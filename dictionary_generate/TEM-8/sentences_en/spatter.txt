Blood spatter was reportedly found on the bed, and a Playboy magazine alongside it. Mom Recalls Horror of Unknowingly Sleeping Beside Murdered 6-Year-Old Daughter |Allison Quinn |June 11, 2021 |The Daily Beast 
His pictures are uncrowded, with some details suggested by the simplest of gestures, such as quick spatters that represent flowers. In the galleries: Artists sport their chops with prints on the cutting edge |Mark Jenkins |March 19, 2021 |Washington Post 
High sides catch the spatter--you can shallow-fry in a few inches of oil in one of these. The 2012 Holiday Kitchen Gift Guide |Megan McArdle |December 13, 2012 |DAILY BEAST 
Yells of pain mingled with the tumult that drowned the ragged, ineffective spatter of firing from the war-fleet. Cursed |George Allan England 
And faint across the creek, the road, and the fields lay the pondy smell of spatter-docks. Roof and Meadow |Dallas Lore Sharp 
The rain had begun to spatter the deck beneath them and the cool wind was working its own will with their garments. The Incendiary |W. A. (William Augustine) Leahy 
Instantly, too, three others spoke, aimed at her flash and she heard the spatter of lead against stone nearby. A Pagan of the Hills |Charles Neville Buck 
With one quick jerk, he raised his rifle, and a vivid spatter of fire followed. The Border Boys Across the Frontier |Fremont B. Deering