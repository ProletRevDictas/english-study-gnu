Already, these closures are reshaping the landscapes of our cities. Is the Government Just Going to Watch the Restaurant Industry Die? |Elazar Sontag |August 28, 2020 |Eater 
There’s so much about this pandemic—and the way its reshaping our world, for better or worse—that’s out of our control. Moms are getting fewer raises and promotions than dads while working remotely |kristenlbellstrom |August 28, 2020 |Fortune 
Here are three ways we’re using the pandemic as a springboard to reshape health care. COVID-19 has spurred rapid transformation in health care. Let’s make sure it stays that way |jakemeth |August 20, 2020 |Fortune 
At the end of the century and into the next, as e-commerce started to reshape the industry, many US retailers remained focused on building physical spaces. Retail’s future is in Asia |Marc Bain |August 20, 2020 |Quartz 
There are very few times in one’s life when you have an opportunity to reshape their habits. There are only a few moments in life when buying habits change, and a pandemic is one of them |Marc Bain |August 8, 2020 |Quartz 
World War I would reshape psychological boundaries as radically as it did geographical ones. American Dreams, 1914: Penrod by Booth Tarkington |Nathaniel Rich |February 27, 2014 |DAILY BEAST 
Already, we see how that innovation could reshape this region. Full Video and Transcript of Obama's Speech in Israel |Justin Green |March 21, 2013 |DAILY BEAST 
Republicans should not try to reshape the government of the United States from the House of Representatives. Republicans Will Lose on the Sequester |David Frum |February 20, 2013 |DAILY BEAST 
Instead of retiring from public life, he may reshape it and save the lives of thousands and ultimately tens of thousands. Obama and Bloomberg Together May Unarm the NRA |Robert Shrum |February 15, 2013 |DAILY BEAST 
Hartman came to Israel to reshape and rethink Jewish peoplehood, emphasizing modernity and its place in religion. The Tragedy And Success Of Rabbi David Hartman |Elisheva Goldberg |February 11, 2013 |DAILY BEAST 
The power to remake and reshape and rebuild planetary conditions to suit man exactly. Empire |Clifford Donald Simak 
First—roughly, in pencil: after which correct and reshape as much as you deem necessary. The Life and Letters of Lafcadio Hearn, Volume 1 |Elizabeth Bisland 
After several years of suggestion, discussion, and change, Mr. Bancroft decided to reshape the entire plan of work accordingly. The Quarterly of the Oregon Historical Society, Vol. IV |Various 
Reshape in original Camembert form, dust thickly with the crumbs and there you are. The Complete Book of Cheese |Robert Carlton Brown 
The lines of Long-thin-and-hungry seemed to shift and reshape. Punch, or the London Charivari, Vol. 93, September 24, 1887 |Various