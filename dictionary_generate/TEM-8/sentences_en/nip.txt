Rich men put their mistresses there so they can nip in and visit them on the way home. Russia’s Gold Digger Academy |Peter Pomerantsev |November 11, 2014 |DAILY BEAST 
Lebanese security agencies have been quick to try to nip what could well be a new bombing spate in the bud. ISIS May Open a Third Front in Lebanon |Jamie Dettmer |June 25, 2014 |DAILY BEAST 
They saw the light years ago and now many do a healthy nip-and-tuck business, especially on noses. The New World of Anti-Aging Dentistry |Kent Sepkowitz |June 4, 2014 |DAILY BEAST 
To the north, the icy southern peaks of the Brooks Range, the northernmost mountain range on the continent, nip at the sky. Visiting the Arctic Circle…Before It’s Irreversibly Changed |Terry Greene Sterling |April 1, 2014 |DAILY BEAST 
But did she really deserve all the nip-and-tuck hate-tweeting? Should We Give Kim Novak a Break on the Oscar Plastic Surgery Hate-Tweeting? |Lizzie Crocker |March 6, 2014 |DAILY BEAST 
Laverdire derives Saguenay from the Montagnais saki-nip, "the rushing water." The Jesuit Relations and Allied Documents, Vol. II: Acadia, 1612-1614 |Various 
Owing to the flagboat drifting, they went round Lepe buoy, and having to nip to fetch, 'Valkyrie' gained a trifle. Yachting Vol. 2 |Various. 
"I'd like a little nip of something to cure the belly-ache," he answered slily. Jack Harkaway's Boy Tinker Among The Turks |Bracebridge Hemyng 
Sheppy is now able to slip in on them and nip their heels, and they do not dare to take a chance on kicking at him. The Red Cow and Her Friends |Peter McArthur 
I knew right off it was a nip-and-tuck race, with the chances in favor of a man called Pringle getting nipped. Motor Matt's Mystery |Stanley R. Matthews