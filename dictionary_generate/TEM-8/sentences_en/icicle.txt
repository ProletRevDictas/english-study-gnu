Hike up the ice-shrouded Onion River, whose trail is accessed via the Ray Berglund Wayside, before passing through a canyon lined with towering icicles. 15 Ways to Play in the Snow This Year |Megan Michelson |November 26, 2020 |Outside Online 
Is stabbing someone with an icicle really the perfect crime? So You Are Enduring a Temporarily Paralyzing Winter Storm |Kelly Williams Brown |February 15, 2014 |DAILY BEAST 
The coldness of his body through the bag jolts me like an icicle through my heart. Inside a Home Funeral |Melissa Roberts Weidman |February 5, 2013 |DAILY BEAST 
Or for that special lady, how about a $1,595 J. Crew icicle dress? Extremely Last Minute Holiday Gift Guide |The Daily Beast |December 19, 2010 |DAILY BEAST 
The badge of the order was a ribbon, striped black, white and yellow, and the device something like an icicle. The Every Day Book of History and Chronology |Joel Munsell 
It was only a long icicle, and all Jack had to do was to touch the reindeer with its point to make them run faster and faster. Seven O'Clock Stories |Robert Gordon Anderson 
Then without warning the Icicle struck something frozen in the ice. Ghost Beyond the Gate |Mildred A. Wirt 
More than human he seemed, there under the icicle loom of the stern-post, his gray hair and beard rigid with ice. The Valor of Cappen Varra |Poul William Anderson 
As cool as an icicle, the man stretched himself out again, half on the deck and half in the cab of the launch. The Hero of Panama |F. S. Brereton