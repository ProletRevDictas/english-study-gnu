The diction is simple, the humor is soft and his subjects deal with the relatable details of daily life. Why Billy Collins Is America’s Most Popular Poet |Austen Rosenfeld |October 22, 2013 |DAILY BEAST 
Classical allusions, poetical turns of phrase, antique diction, recondite words. David's Book Club: The Souls of Black Folk |David Frum |May 5, 2013 |DAILY BEAST 
The prime minister has also reportedly paid for diction lessons to smooth out her rough Neapolitan accent. Berlusconi Announces May-December Engagement |Barbie Latza Nadeau |December 17, 2012 |DAILY BEAST 
This is yet another masterpiece, even though the tone and diction are all wrong, and the proportions totally off. Joseph Roth’s Letters Reveal a Great Forgotten Writer |Anthony Heilbut |February 10, 2012 |DAILY BEAST 
There was a British woman with a mike who sounded smarter than everyone else, due to her Oxford diction. The Day D.C. Went Sane |Howard Kurtz |October 30, 2010 |DAILY BEAST 
But give me a comprehensive idea of the place, in your own inimitable unvarnished diction. Ancestors |Gertrude Atherton 
Cibber almost new wrote the whole, and the last act was entirely his in conduct, sentiment and diction. The Fatal Dowry |Philip Massinger 
The critics say that his sublimity of diction is sometimes carried to an extreme, so that his language becomes inflated. Beacon Lights of History, Volume I |John Lord 
Tragedy is a drama in which the diction is dignified, the movement impressive, and the ending unhappy. English: Composition and Literature |W. F. (William Franklin) Webster 
He loved a correct and classic diction, and never underrated style, so long as style was not an excuse for poverty of thought. The Life of Mazzini |Bolton King