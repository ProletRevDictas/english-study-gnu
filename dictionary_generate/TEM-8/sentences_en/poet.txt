Also watching were New Roads students — including a group of third-graders who recognized the poet. Amanda Gorman learned the power of poetry early on |Los Angeles Times |February 1, 2021 |Washington Post 
Astronaut Michael Collins said the best crew for a space mission would be a philosopher, a priest and a poet. Can Astronaut Thinking Heal America at Last? |Leslie dela Vega |January 28, 2021 |Ozy 
The best-selling poet offers a new collection of verse with illustrations. Washington Post paperback bestsellers | |January 26, 2021 |Washington Post 
Gorman isn’t just an amazing poet, she’s also academically talented. Meet Amanda Gorman, The Youngest Inaugural Poet In U.S. History |Tiffani Dupree |January 20, 2021 |Essence.com 
An early version of the modern solution came, of all people, from the poet Edgar Allan Poe. The universe is 13.8 billion years old—here’s how we know |Charlie Wood |January 13, 2021 |Popular-Science 
The poet apparently collapsed in the street upon his departure from “The Horse” and died not long after. The Bars That Made America Great |Nina Strochlic |December 28, 2014 |DAILY BEAST 
A Harvard-educated poet and professor, Linsker was arrested early Sunday morning and released without bail later that day. The High-Priced Union Rep Charged With Attacking a Cop |Jacob Siegel |December 19, 2014 |DAILY BEAST 
So many were arrested in Leningrad, the poet Anna Akhmatova said, that the city “dangled like an appendage from its prisons….” When Stalin Met Lady Macbeth |Brian Moynahan |November 9, 2014 |DAILY BEAST 
Who was the most erotic poet of the late Renaissance and early Baroque, when the quatrain reached its courtly zenith? Sor Juana: Mexico’s Most Erotic Poet and Its Most Dangerous Nun |Katie Baker |November 8, 2014 |DAILY BEAST 
And, for that matter, where is our poet who could damn any of them for it? Election Day Is Scarier Than Halloween |P. J. O’Rourke |November 1, 2014 |DAILY BEAST 
Rene le Pays, a French poet, died; well known at court by his miscellanies. The Every Day Book of History and Chronology |Joel Munsell 
Richard Brathwaite, an English poet and miscellaneous writer, died. The Every Day Book of History and Chronology |Joel Munsell 
But if people will insist on patting a strange poet, they mustn't be surprised if they get a nasty bite! Punch, or the London Charivari, Volume 107, November 3, 1894 |Various 
Richard Cumberland died; eminent as a British poet, essayist, novelist and dramatic writer. The Every Day Book of History and Chronology |Joel Munsell 
Bonnell Thornton died; an English poet, essayist and miscellaneous writer, and translator of Plautus. The Every Day Book of History and Chronology |Joel Munsell