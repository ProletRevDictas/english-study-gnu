The presentation was a kind of rebuff to those who think Disney has been one-upped in the digital world by Netflix. Disney unveils huge cache of content, signaling it seeks to dominate both digital and theaters |Steven Zeitchik |December 11, 2020 |Washington Post 
Not only did she rebuff his awkward advance in person, she went home and did some sleuthing. Online Shaming Gives Creeps the Spotlight They Deserve |Samantha Allen |September 23, 2014 |DAILY BEAST 
March 2012: “In rebuff to Obama, Abbas says he will send ultimatum to Israel.” Abbas Threatens To Dismantle PA—Again |Emily L. Hauser |December 28, 2012 |DAILY BEAST 
Or we simply learn how to artfully rebuff advances by subtly threatening to blow the whistle on despicable and degrading behavior. Hounded by Harassment |Lauren Ashburn |November 4, 2011 |DAILY BEAST 
The tone constituted a rebuff, and Rita's coquetry deserted her, leaving her mortified and piqued. Dope |Sax Rohmer 
"Tell him to call," she said to Tim, who delivered her message rather awkwardly, as if expecting a rebuff. The Cromptons |Mary J. Holmes 
Nothing dismayed by his first rebuff, the audacious Fouch again intervened. The Life of Napoleon Bonaparte |William Milligan Sloane 
He was so easily rebuffed, and she was so reluctant to rebuff him. A Houseful of Girls |Sarah Tytler 
Take, for instance, the matter of a caress or an embrace—how would you react to repeated rebuff? The Mother and Her Child |William S. Sadler