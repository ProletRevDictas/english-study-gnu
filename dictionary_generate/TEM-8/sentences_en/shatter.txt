The total Union loss was 16,179 men, a number about equal to the army led by Shatter against Santiago. The Colored Regulars in the United States Army |T. G. Steward 
Between two roaring worlds where they swirl, I. Shatter them, one and both. Ulysses |James Joyce 
But the captain of the Shatter will certainly not reveal it as long as he lives. Gun running for Casement in the Easter rebellion, 1916 |Karl Spindler 
On both sides of the bow flaunted in large lettering the name Shatter II. Gun running for Casement in the Easter rebellion, 1916 |Karl Spindler 
The crew of the Shatter had apparently forgotten that they could support their demand with force of arms. Gun running for Casement in the Easter rebellion, 1916 |Karl Spindler