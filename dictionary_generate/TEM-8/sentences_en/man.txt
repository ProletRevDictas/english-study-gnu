In the first episode, an officer is shown video of himself shooting and killing a man. 'Babylon' Review: The Dumb Lives of Trigger-Happy Cops |Melissa Leon |January 9, 2015 |DAILY BEAST 
That man was Xavier Cortada, a gay man who wrote of his frustration that he and his partner of eight years were unable to marry. Jeb Bush’s Unseen Anti-Gay Marriage Emails |Jackie Kucinich |January 9, 2015 |DAILY BEAST 
It is the summit of human happiness: the surrender of man to God, of woman to man, of several women to the same man. Houellebecq’s Incendiary Novel Imagines France With a Muslim President |Pierre Assouline |January 9, 2015 |DAILY BEAST 
But no more so than the Sodexo building maintenance man or the two cops who were also killed in the crossfire. Trolls and Martyrdom: Je Ne Suis Pas Charlie |Arthur Chu |January 9, 2015 |DAILY BEAST 
He looks like a man who should have had kids, but now never will. The Muslim Cop Killed by Terrorists |Michael Daly |January 9, 2015 |DAILY BEAST 
Davy looked around and saw an old man coming toward them across the lawn. Davy and The Goblin |Charles E. Carryl 
The supernaturalist alleges that religion was revealed to man by God, and that the form of this revelation is a sacred book. God and my Neighbour |Robert Blatchford 
The most High hath created medicines out of the earth, and a wise man will not abhor them. The Bible, Douay-Rheims Version |Various 
He remembered something—the cherished pose of being a man plunged fathoms-deep in business. St. Martin's Summer |Rafael Sabatini 
On the thirteenth of the same month they bound to the stake, in order to burn alive, a man who had two religious in his house. The Philippine Islands, 1493-1898, Volume XX, 1621-1624 |Various