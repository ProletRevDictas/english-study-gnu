It’s super durable with raised sides protect the screen and an aluminum button. Smartwatch accessories to give your high-tech friends and family |PopSci Commerce Team |October 1, 2020 |Popular-Science 
Toyota's system uses an extremely beefy aluminum extrusion cage that covers the entire ceiling of the test house. Toyota’s ceiling-mounted robot is like GLaDOS for your kitchen |Ron Amadeo |October 1, 2020 |Ars Technica 
BPA-free plastic, stainless steel and aluminum construction afford this unit both durability and longevity. Coffee makers for java enthusiasts |PopSci Commerce Team |October 1, 2020 |Popular-Science 
For starters, the 5 features a 100% recycled aluminum body, whereas the 4a 5G is polycarbonate. Google now has three mid-range Pixel phones |Brian Heater |October 1, 2020 |TechCrunch 
The aluminum legs support a lot of weight, and the top is easy to clean. Car Camping Is Way Better with These 5 Pieces of Gear |Jakob Schiller |September 26, 2020 |Outside Online 
New refinements in aluminum made structures both stronger and lighter. Flight 8501 Poses Question: Are Modern Jets Too Automated to Fly? |Clive Irving |January 4, 2015 |DAILY BEAST 
According to police, Kory then attacked the victim with an aluminum tennis racket. Meet Your New ‘Hot Mugshot Guy’: Sean Kory, Fox News’ Public Enemy No. 1 |Marlow Stern |November 3, 2014 |DAILY BEAST 
I glimpse an alarming, finger-length aluminum baton in her bag. Best Career Arc Ever: From Burlesque To Bartending |Anne Berry |September 13, 2014 |DAILY BEAST 
In 2012, Li allegedly supplied the Iranians with 20,000 kilos of steel pipe and 1,300 aluminum alloy tubes. Tehran’s Chinese Missile Man |Jefferson Morley |June 9, 2014 |DAILY BEAST 
It is one of the only times I can think of when life imitates art to the very bleeding edge of an aluminum shank. Reading Prison Novels In Prison |Daniel Genis |May 24, 2014 |DAILY BEAST 
When partially exhausted the aluminum shutters are dipped into a bath of shellac. The Recent Revolution in Organ Building |George Laing Miller 
On a hunch I dropped in an aluminum alkyl, and then pushed the polymerization along with both ultraviolet and heat. The Professional Approach |Charles Leonard Harness 
These experiments did show, however, that the tablets contained an amount of aluminum corresponding to over 25 per cent. The Propaganda for Reform in Proprietary Medicines, Vol. 1 of 2 |Various 
It was enough, with some aluminum above the amount needed for the wire. Space Prison |Tom Godwin 
It was the year one hundred and fifty-two when they smelted the aluminum. Space Prison |Tom Godwin