On first sip, it seems to be assessing your worthiness to drink it, but with a few hours of air — even the next day — it unfurls flavors of orange and tangerine over blackberries and plums. At just $10, this Italian red blend is your ideal chocolate dessert pairing |Dave McIntyre |October 28, 2021 |Washington Post 
Situated along a series of narrow ridgetops within Mark Twain National Forest, the road skirts bright crimson and tangerine-hued leaves before winter arrives. The Best Scenic Drive in Every State |eriley |August 26, 2021 |Outside Online 
Pour in Campari to fill the glass and float one more tangerine slice. Celeb Chefs' Holiday Cocktails |Jacquelynn D. Powers |November 19, 2010 |DAILY BEAST 
Please note: The faux-Tangerine Dream soundtrack was not included with the computer. Apple's 10 Biggest Flops |The Daily Beast Video |June 12, 2010 |DAILY BEAST 
A close second: thinking the public has the I.Q. of a tangerine. John Edwards' Audacity of Spin |Eric Dezenhall |January 27, 2010 |DAILY BEAST 
The Kandy-Kolored Tangerine-Flake Streamline Babyby Tom Wolfe The ur-text of New Journalism still bears rereading now. This Week's Hot Reads |The Daily Beast |December 14, 2009 |DAILY BEAST 
Put the dried apricots into a saucepan with the water, and add the sugar and juices from the lemon and tangerine or orange. Ham, Green Bean Casserole, Easy Trifle |The Daily Beast |December 23, 2008 |DAILY BEAST 
A few mandarini—tangerine oranges—rolled on a plate for dessert. Sea and Sardinia |D. H. Lawrence 
He waited, palpably waited, but Arlee continued to peel a tangerine with absorption, and the question had to come from him. The Palace of Darkened Windows |Mary Hastings Bradley 
Very carefully she tore the tangerine skin into very little bits, her head bent over it. The Palace of Darkened Windows |Mary Hastings Bradley 
The ex-Marine was munching on a Lebanese tangerine and watching the Nile boats below. The Egyptian Cat Mystery |Harold Leland Goodwin 
I respect Billy, the adjutant, for his long service and the Tangerine at the back of his neck. The Strand Magazine, Volume V, Issue 27, March 1893 |Various