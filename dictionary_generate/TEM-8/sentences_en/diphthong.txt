It is curious that the close o is heard only in the infrequent diphthong óu, or as an obscured, unaccented final. Frdric Mistral |Charles Alfred Downer 
The diphthong is long when a voiced sound follows it, short before a voiceless sound. The Sounds of Spoken English |Walter Rippmann 
Listen to a foreigner's pronunciation of English words containing this diphthong; what do you notice? The Sounds of Spoken English |Walter Rippmann 
In both cases we have a laxly articulated short sound, and a diphthong in which the tongue rises towards the end. The Sounds of Spoken English |Walter Rippmann 
Notice the faulty tendency to raise the tongue too high in uttering the first part of this diphthong; see § 40. The Sounds of Spoken English |Walter Rippmann