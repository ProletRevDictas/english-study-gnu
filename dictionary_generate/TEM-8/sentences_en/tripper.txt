"I wish he was in it now," a bloodthirsty tripper muttered darkly in my ear. Adventures and Enthusiasms |E. V. Lucas 
Tripper threw up his arm to show he understood, and then lent his aid in getting up the anchor. A Chapter of Adventures |G. A. Henty 
When the chain was stowed below, and the anchor securely fastened, Tripper went aft and hauled in the main-sheet. A Chapter of Adventures |G. A. Henty 
As soon as the ugly ledge of rocks running far out under water was weathered, Tripper put down the helm. A Chapter of Adventures |G. A. Henty 
Tripper was standing beside him, and pointing at something broad away on the beam. A Chapter of Adventures |G. A. Henty