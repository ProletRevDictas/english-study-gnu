Whatever the case, she stuck with her style of “anarchic humanism” — figurative painting, not abstract — even as her style grew increasingly outdated. “A Fascinating, Sexy, Intellectually Compelling, Unregulated Global Market.” (Ep. 484) |Stephen J. Dubner |December 2, 2021 |Freakonomics 
It was the ultimate guarantor of the humanism he advanced against Nazism. The Catholic Philosopher Who Took on Hitler |John Henry Crosby |December 26, 2014 |DAILY BEAST 
For religious parents concerned that teaching humanism to kids might turn them into psychopaths, this is a very gentle book. Are You There, Nobody? It’s Me, Margaret |Candida Moss |October 12, 2014 |DAILY BEAST 
These, he insisted, were harbingers of the twin plagues of socialism and secular humanism. What I Saw at Iowa’s So-Co Circus |Ben Jacobs |August 10, 2014 |DAILY BEAST 
Gloria Steinem famously said that feminism is, at its core, humanism. Ten Reasons Women Are Losing While Gays Keep Winning |Jay Michaelson |July 6, 2014 |DAILY BEAST 
Our vehement prejudices leave us no patience for his appeal to radical humanism. How to Fight for Freedom in 2014 |James Poulos |December 29, 2013 |DAILY BEAST 
There are two radical differences between Humanism and Christianity. God and my Neighbour |Robert Blatchford 
Humanism concerns itself solely with Man, so that Man is its first and last care. God and my Neighbour |Robert Blatchford 
Even Chaucer is not so complete in his humanism, his love of all sorts and conditions of men. Giovanni Boccaccio, a Biographical Study |Edward Hutton 
It is only possible for the rational gospel of humanism, the great religion of natural sympathy, to heal the breach. Islam Her Moral And Spiritual Value |Arthur Glyn Leonard 
The opponents of humanism are wrong to combat antiquity as well; for in antiquity they have a strong ally. We Philologists, Volume 8 (of 18) |Friedrich Nietzsche