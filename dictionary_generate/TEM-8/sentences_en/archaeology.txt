In conversation, they soon discovered a mutual love of archaeology. Date Lab: They had ‘fun hair,’ fondue and a shared love of archaeology |Damona Hoffman |August 26, 2021 |Washington Post 
Neanderthals were discovered in Europe, the continent with the longest and most well-funded tradition in archaeology. The Human Family Tree, It Turns Out, Is Complicated - Issue 102: Hidden Truths |Razib Khan |June 30, 2021 |Nautilus 
Data archaeology is particularly good for historians studying cities because urban places often hold the stories of immigrants who might be otherwise difficult to trace. How technology helped archaeologists dig deeper |Annalee Newitz |April 28, 2021 |MIT Technology Review 
In recent months, a series of discoveries have captivated the world of archaeology. In the tombs of Saqqara, new discoveries are rewriting ancient Egypt’s history |Sudarsan Raghavan |April 22, 2021 |Washington Post 
Mason jars were tucked into dusty bookshelves packed with cookbooks, novels, and archaeology texts. The Search for a Ranger Who Was Lost and Never Found |Brendan Borrell |April 1, 2021 |Outside Online 
The first time I ever heard about archaeology was in the fifth grade when we learned about Richard Leakey. The Real-Life Raiders of the Lost Ark |Alex Belth |November 14, 2014 |DAILY BEAST 
I found the section of the book on forensic archaeology fascinating. The Real-Life Raiders of the Lost Ark |Alex Belth |November 14, 2014 |DAILY BEAST 
Ruth Shady isn't my anything; she is her own, completely original force in archaeology. The Real-Life Raiders of the Lost Ark |Alex Belth |November 14, 2014 |DAILY BEAST 
If you can stomach lunch—forensic archaeology can be disgusting. The Real-Life Raiders of the Lost Ark |Alex Belth |November 14, 2014 |DAILY BEAST 
Archaeology is about paying attention to things that have been or could be indetectable or invisible to others. The Real-Life Raiders of the Lost Ark |Alex Belth |November 14, 2014 |DAILY BEAST 
This lack of enjoyment in Nature, lack of interest in topography and archaeology, was probably personal to him. The Age of Erasmus |P. S. Allen 
The Canon, whose archaeology did not go back beyond St. Patrick, offered no correction. Hyacinth |George A. Birmingham 
Whatever may be the reason, such theories are not borne out by the discoveries of archaeology. The Other Side of Evolution |Alexander Patterson 
We need also to consider the vast and great civilizations which existed in remote antiquity as is now revealed by archaeology. The Other Side of Evolution |Alexander Patterson 
David Hulse, junior author, is a native of Decatur, and his interest in archaeology is as longstanding as Cambron's. Handbook of Alabama Archaeology: Part I Point Types |James W. Cambron