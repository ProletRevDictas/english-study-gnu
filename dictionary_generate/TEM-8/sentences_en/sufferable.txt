Lady Ann was offended, and seriously: was alliance with such a woman permissible or sufferable? There and Back |George MacDonald 
It was something besides the river that made the air so much more sufferable than it had been. The March Family Trilogy, Complete |William Dean Howells 
Yet no man will, at this day, pretend that the Greek of his prize ode is sufferable. Blackwood's Edinburgh Magazine -- Volume 57, No. 351, January 1845 |Various 
Such viragoes are in a measure sufferable, for they are sometimes in a good-humour. Sheppard Lee, Vol. I (of 2) |Robert Montgomery Bird 
Even Bully Pigeon was sufferable (as Paddy observed), if he was not altogether agreeable. The Three Midshipmen |W.H.G. Kingston