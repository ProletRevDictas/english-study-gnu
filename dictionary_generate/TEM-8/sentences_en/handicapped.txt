I spent four years in a prison where each handicapped convict was issued an underpaid inmate assistant. A Million Ways to Die in Prison |Daniel Genis |December 8, 2014 |DAILY BEAST 
Franco plays Benjy, the mentally handicapped—and eventually, castrated—member of the Compson clan. James Franco and Scott Haze on 'The Sound and the Fury' and Gawker 'Outing' Them As A 'Couple' |Marlow Stern |September 6, 2014 |DAILY BEAST 
He is not lingering in illness, or crippled, or handicapped. The Death of a Rodeo Cowboy |Peter Richmond |May 11, 2014 |DAILY BEAST 
We lost a million individuals in that war…and another million were handicapped. Afghanistan: Building a Vision Of Hope and Change |Lloyd Grove |April 17, 2014 |DAILY BEAST 
Saenz set an orange plastic sled at the top of a handicapped access ramp. Philip Seymour Hoffman and the Mourner’s Playground |Michael Daly |February 4, 2014 |DAILY BEAST 
My work has been greatly handicapped of late by lack of certain books to which I wanted to refer. First Plays |A. A. Milne 
But poor Lancing was seriously handicapped by the fact that he had a woman for his antagonist. The Weight of the Crown |Fred M. White 
Thus, special assistance may be given for starting in trade persons handicapped by their infirmities. English Poor Law Policy |Sidney Webb 
He was handicapped by the fact that he did not quite dare to put all his back and shoulders into the preliminary shove. The Boy Grew Older |Heywood Broun 
But as for getting tangible help from the Creator of a body handicapped by nerves like his! Red Pepper Burns |Grace S. Richmond