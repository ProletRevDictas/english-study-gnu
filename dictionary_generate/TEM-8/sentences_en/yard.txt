Others munch on the berries and other fruits that people grow in their yards. Are coyotes moving into your neighborhood? |Kathryn Hulick |September 3, 2020 |Science News For Students 
For years, Amazon has envisioned deploying drones to deliver packages to customers’ yards. Amazon is one big step closer to delivering packages by drone |Rob Verger |September 2, 2020 |Popular-Science 
Then, as now, development regulations in the city placed standalone homes with their own yards — and the residents who could afford them — above all else. Morning Report: A Century of Single Family Home Supremacy |Voice of San Diego |August 27, 2020 |Voice of San Diego 
These numbers may vary depending on your pet’s age and energy level, as well as whether you have a fenced-in yard. The right way to walk your dog |John Kennedy |August 26, 2020 |Popular-Science 
It means someone went out and put a bunch of yard signs out. Full Transcript: Sean Spicer on ‘The Carlos Watson Show’ |Daniel Malloy |August 26, 2020 |Ozy 
Jettison your lawyers as a source of prison-yard guidance, Abramoff said. Abramoff’s Advice for Virginia’s New Jailhouse Guv |Tim Mak, Jackie Kucinich |January 7, 2015 |DAILY BEAST 
Parker writes of the “black-faced doe” that he sees in the yard in his new Texas house. Will Texas Stay Texan? |David Fontana |December 29, 2014 |DAILY BEAST 
Just right for that person who needs a little creative push to do something daring in their yard. The Best Gift Books of 2014 |William O’Connor |December 12, 2014 |DAILY BEAST 
Later that day he made a call from the row of phones in the yard and reached his wife for the first time in six months. A Million Ways to Die in Prison |Daniel Genis |December 8, 2014 |DAILY BEAST 
The victim was himself dangerous, and also the strongest man in the yard. A Million Ways to Die in Prison |Daniel Genis |December 8, 2014 |DAILY BEAST 
Sol got up, slowly; took a backward step into the yard; filled his lungs, opened his mouth, made his eyes round. The Bondboy |George W. (George Washington) Ogden 
They ran side by side across the yard to a roofed flight of steps that led to the printing-office. Hilda Lessways |Arnold Bennett 
After his death crowds flocked to his grave to touch his holy monument, till the authorities caused the church yard to be shut. The Every Day Book of History and Chronology |Joel Munsell 
In the court-yard of the hotel was standing the voiture, which had come in some twenty minutes before us. Blackwood's Edinburgh Magazine, Volume 60, No. 372, October 1846 |Various 
The huge sail thrust its yard high above the fog bank, and watchers on the river side saw it. The Red Year |Louis Tracy