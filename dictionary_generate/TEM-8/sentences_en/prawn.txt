The iconic Stagecoach Inn hasn’t changed much since it opened in 1959, and the restaurant is still known for its crispy hand-breaded prawns, prime rib, and steak and beans. Eater’s Guide to Idaho’s Snake River Valley |Amanda Gabriele |November 23, 2021 |Eater 
PRAWN COCKTAILThis is also delicious if you replace the shrimp with smoked salmon or cooked crab. That '70s Food |Lydia Brownlow |April 22, 2011 |DAILY BEAST 
Similar in appearance and often used synonymously—and erroneously—the shrimp and the prawn are two different creatures. Four Things You Didn't Know About Shrimp |Jane Frye |June 24, 2009 |DAILY BEAST 
After a lunch of prawn curry and rice at the Bunder, you may want to visit the Lighthouse Hill and its vicinity. An Excerpt from Between the Assassinations |Aravind Adiga |June 10, 2009 |DAILY BEAST 
Among them are the shrimp and prawn, thin-shelled, active crustaceans common along our eastern coast. A Civic Biology |George William Hunter 
They strike the water with great force, and so send the Prawn or Shrimp quickly backwards. On the Seashore |R. Cadwallader Smith 
To move quickly, the Shrimp or Prawn merely bends his body, then straightens it. On the Seashore |R. Cadwallader Smith 
When the Prawn or Shrimp is not in a hurry, he swims slowly but surely with the little paddles, or "swimmerets." On the Seashore |R. Cadwallader Smith 
To tell a live Shrimp from a Prawn, look at the long pointed beak which juts out from the front of the head. On the Seashore |R. Cadwallader Smith