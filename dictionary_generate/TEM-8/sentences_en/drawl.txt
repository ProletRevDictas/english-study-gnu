The drawls are as grand as the morning sunrises and the evening sunsets over marshland, the swaying patches of brown and green grass cradling both mud and smaller tributaries of water. A vacation town promises rest and relaxation. The water knows the truth. |Nneka M. Okona |August 26, 2021 |Vox 
David has a Southern drawl and charm that informs his character. Dan Stevens Blows Up ‘Downton’: From Chubby-Cheeked Aristo to Lean, Mean American Psycho |Tim Teeman |September 19, 2014 |DAILY BEAST 
Still, a tight-jawed smile, wild eyes and a southern California drawl remind me of Matthew McConaughey. He Bullies Kids and Calls It News |Brandy Zadrozny |June 26, 2014 |DAILY BEAST 
She raps with a tightly-wound drawl, one that, to American ears, feels tone-deaf not musically, but socially. Stop Being So Surprised By the Rise of Iggy Azalea |Kevin Fallon |May 30, 2014 |DAILY BEAST 
Boyd is tall and thickly muscled and speaks softly in a Piedmont drawl. From PTSD to Prison: Why Veterans Become Criminals |Matthew Wolfe |July 28, 2013 |DAILY BEAST 
When the crew travels underwater, they discover a land where the mermen and merwomen speak a Southern American drawl. The Funniest ‘Futurama’ Scenes: From Bender to Zoidberg (VIDEO) |Jean Trinh |June 19, 2013 |DAILY BEAST 
The man did not live, nor could the occasion arrive, which would quicken his constitutional drawl. Ramona |Helen Hunt Jackson 
He talked freely and intimately in a low, hesitating drawl that was not unpleasant to hear. The Awakening and Selected Short Stories |Kate Chopin 
Except in long speeches she did not drawl; at times she spoke rapidly, snapping off sentences abruptly. A Hoosier Chronicle |Meredith Nicholson 
Again the note of melancholy, throbbing above the drawl––rising, indeed, into a wail. The Cruise of the Shining Light |Norman Duncan 
He seemed to affect a drawl, and the grasp of his hand was not exactly hearty. Frank Merriwell's Pursuit |Burt L. Standish