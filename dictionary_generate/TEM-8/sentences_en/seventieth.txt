It would seem, at best, an uneasy fit: Paul Newman and his seventieth birthday, this month. The Stacks: The Eyes of Winter: Paul Newman at 70 |Peter Richmond |October 11, 2014 |DAILY BEAST 
On his seventieth birthday Mr. Bright justified what he called the policy of 1846. Captains of Industry |James Parton 
Sir Peregrine himself at this time was an old man, having passed his seventieth year. Orley Farm |Anthony Trollope 
A number of more or less notable things happened in this, Mark Twain's seventieth year. Mark Twain, A Biography, 1835-1910, Complete |Albert Bigelow Paine 
His wife was a writer of ability and after reaching her seventieth year wrote a book of poems. Lyman's History of old Walla Walla County, Vol. 2 (of 2) |William Denison Lyman 
In other places he says that Mrs. Thrale's thirty-fifth year coincided with Johnson's seventieth. Autobiography, Letters and Literary Remains of Mrs. Piozzi (Thrale) (2nd ed.) (2 vols.) |Mrs. Hester Lynch Piozzi