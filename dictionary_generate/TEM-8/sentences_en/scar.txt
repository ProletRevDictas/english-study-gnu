I am one of those people whose scars are visible—both physical and emotional ones. Amanda Gorman and the Art of Being Unfinished and Imperfect |Susanna Schrobsdorff |January 24, 2021 |Time 
We’ll carry with us the scars of this long year, and of all our history. Amanda Gorman and the Art of Being Unfinished and Imperfect |Susanna Schrobsdorff |January 24, 2021 |Time 
Now, he says, gesturing to the burn scar, the towns had a fire defense in place of a liability. What the complex math of fire modeling tells us about the future of California’s forests |Amy Nordrum |January 18, 2021 |MIT Technology Review 
The victims have their scars on their bodies, family traumas, societal traumas. Historian: Today’s Authoritarian Leaders Aren’t Fascists—But They Are Part of the Same Story |Olivia B. Waxman |November 10, 2020 |Time 
You beat cancer, but many people have lifelong scars, both physical and emotional. How Cancer Shaped Justice Ruth Bader Ginsburg's Life and Work |Jamie Ducharme |September 19, 2020 |Time 
A scar marks her right wrist where the bullet hit her eight months ago. ‘There was no food, no more water lilies’ |Crystal Wells |October 31, 2014 |DAILY BEAST 
He lifted his t-shirt and showed us a long scar, running from sternum to waistband. Watching ISIS Come to Power Again |Elliot Ackerman |September 7, 2014 |DAILY BEAST 
[points to above the knee] The scar looks like a shark bite. Life After Deaths: Sean Bean on 'Game of Thrones' Paternity and 'Legends' |Jason Lynch |August 11, 2014 |DAILY BEAST 
Prinze loves Star Wars and has a big scar on his chin to prove it. ‘Star Wars Rebels’ Explores the Jedi’s Lost Years Between the Prequels and the Original Trilogy |Annaliza Savage |August 2, 2014 |DAILY BEAST 
One runner in Houston now has a permanent, hoof-shaped scar in the center of his forehead. Chicago’s Running of the Bulls |Hampton Stevens |July 26, 2014 |DAILY BEAST 
He has an ugly scar—a knife-cut—across the back of one hand; you can't mistake him if you get sight of him. Raw Gold |Bertrand W. Sinclair 
There was a scar in the shape of a cross on the man's swarthy cheek, and it glowed redly with the anger that filled him. Motor Matt's "Century" Run |Stanley R. Matthews 
He was a dark, somber looking man with a particularly ugly scar on his chin. Hooded Detective, Volume III No. 2, January, 1942 |Various 
Anyhow, when McKibben saw him after the team was stopped, there was that cross-shaped scar, plain as anything. Motor Matt's "Century" Run |Stanley R. Matthews 
Brutus was getting well, but there would always be a scar on his shoulder, where the sharp-pointed shrub had entered the flesh. The Cromptons |Mary J. Holmes