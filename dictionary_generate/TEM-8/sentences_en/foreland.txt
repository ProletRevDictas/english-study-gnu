Impelled by this most welcome breeze, we were soon round the South Foreland and off Dover, where we hove-to to land the pilot. The Cruise of the "Esmeralda" |Harry Collingwood 
And the youngest, he never was found; and the others was stone dead ashore, nigh on to the Foreland. Somehow Good |William de Morgan 
Its sea-front walk of a couple of miles or more is as fine as any that can be found from the Foreland to the Lizard. The Automobilist Abroad |M. F. (Milburg Francisco) Mansfield 
The North Foreland had been made advisedly snug for the night. Denis Dent |Ernest W. Hornung 
The North Foreland was blessed with a commander who was at his best in an emergency. Denis Dent |Ernest W. Hornung