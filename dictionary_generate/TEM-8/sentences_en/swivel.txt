It is designed with a non-slip handle, an easy “open” button for the lid, and a 360-degree swivel base that will assist your pour. The best electric kettles for a great brew |PopSci Commerce Team |October 8, 2020 |Popular-Science 
It comes in a huge variety of colors and has a lockable 36-inch swivel cord that’s designed for ease of movement in your busy food workspace. These pieces of kitchen gear make excellent gifts |PopSci Commerce Team |October 6, 2020 |Popular-Science 
Once the base is attached, you adjust the swivel plate to face the direction you want, then you mount a second, superthin magnet to the back of your phone or phone case. The Best Phone Mount for Your Car Costs $12 |Jakob Schiller |September 20, 2020 |Outside Online 
The magnet on your phone then attaches to the magnetic swivel plate, and you’re done. The Best Phone Mount for Your Car Costs $12 |Jakob Schiller |September 20, 2020 |Outside Online 
On top of the base is a small plastic swivel plate with a magnet. The Best Phone Mount for Your Car Costs $12 |Jakob Schiller |September 20, 2020 |Outside Online 
How refreshing it was to see England players swivel and dribble and sell dummies. The Group of Life |Tunku Varadarajan |June 15, 2014 |DAILY BEAST 
He sat before a wall full of badges, in a big swivel chair, his bovine features set in mistrustful concentration. The Strange and Mysterious Death of Mrs. Jerry Lee Lewis |Richard Ben Cramer |January 11, 2014 |DAILY BEAST 
Swivel Store Spice Rack  Okay, so this is an As Seen On Television product. The 2012 Holiday Kitchen Gift Guide |Megan McArdle |December 13, 2012 |DAILY BEAST 
He sits, leans back in his swivel chair and begins to discuss his SATC character, Aleksandr Petrovsky. Baryshnikov Unbound |Ross Kenneth Urken |May 17, 2009 |DAILY BEAST 
Ward sat back in his swivel chair, hooked his thumbs into the arm holes of his vest and beamed. Hooded Detective, Volume III No. 2, January, 1942 |Various 
One of the swivel guns was fired, and then came a whole broadside, sending its balls hurtling over the crowded deck of the sloop. Stories of Our Naval Heroes |Various 
Also Crozier's evidence was expected to be sensational, and to prove the swivel on which the fate of the accused man would hang. You Never Know Your Luck, Complete |Gilbert Parker 
They were of elm, with lignum vit roller sheaves, and were bound inside with iron, and had swivel eyes. Loss of the Steamship 'Titanic' |British Government 
She dropped her check book into a drawer and swung round in her swivel chair until she faced him. A Hoosier Chronicle |Meredith Nicholson