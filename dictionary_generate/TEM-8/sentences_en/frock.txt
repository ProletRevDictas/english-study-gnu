One episode finds our host in a poofy, heart-print frock that epitomizes what the kids call lovecore. The Surprisingly Complicated Pleasures of Cooking With Paris |Judy Berman |August 4, 2021 |Time 
The Biba brand exploded in 1964 with the phenomenal success of a pink and white gingham frock called “the Barbara.” Barbara Hulanicki, Queen of Fast Fashion |Lizzie Crocker |October 15, 2014 |DAILY BEAST 
She wore a sea-green, V-necked frock with a modest hint of cleavage. ‘Outnumbered’: The Making of a Fox News Hit |Lloyd Grove |October 13, 2014 |DAILY BEAST 
For this service he arrayed himself in an old-fashioned frock coat with long skirts. The Stacks: H.L. Mencken on the 1904 Baltimore Fire |H.L. Mencken |October 4, 2014 |DAILY BEAST 
There was even a free-spirited frock made of ropes, which flung about through her ritualistic dance. Gareth Pugh's Fashion Show Lacked Fashion, But Not Passion |Justin Jones |September 5, 2014 |DAILY BEAST 
During her 2011 tour of Canada, a skimpy yellow frock flew skywards on a Calgary airfield. Kate Middleton's History of Flesh-Flashing Wardrobe Malfunctions |Tom Sykes |May 29, 2014 |DAILY BEAST 
Movement to know that she was attired in appropriate costume—short frock, biped continuations and a mannish oil-skin hat. Glances at Europe |Horace Greeley 
At eighteen years of age I had my first frock coat and tall hat. Fifty Years of Railway Life in England, Scotland and Ireland |Joseph Tatlow 
Now, go and mend that deplorable frock, and if you don't dream over it, you won't waste too much of your holiday. The Daisy Chain |Charlotte Yonge 
There were eunuchs too, black frock-coated—and the chief eunuch, an important personage who ranks very high. Fifty Years of Railway Life in England, Scotland and Ireland |Joseph Tatlow 
Nothing bad (she had already acclaimed it to Amy and Jessie) could happen to her with that frock on. The Campfire Girls of Roselawn |Margaret Penrose