Hovering above the scene, commandos in helicopters were poised with automatic rifles. France Kills Charlie Hebdo Murderers |Nico Hines |January 9, 2015 |DAILY BEAST 
Even the hot Jewish women I mentioned above did something a bit more “intellectual” than pageantry: acting. Why Was Bess Myerson the First and Last Jewish Miss America? |Emily Shire |January 7, 2015 |DAILY BEAST 
Are you excited, nervous, afraid, all of the above for the new Star Wars films? Patton Oswalt on Fighting Conservatives With Satire |William O’Connor |January 6, 2015 |DAILY BEAST 
I fall back into a dream and then suddenly there is a tapping on the window just above my bed. The Story Behind Lee Marvin’s Liberty Valance Smile |Robert Ward |January 3, 2015 |DAILY BEAST 
He was born in an apartment above the grocery store owned by his immigrant parents in South Jamaica, Queens. Mario Cuomo, a Frustrating Hero to Democrats, Is Dead at 82 |Eleanor Clift |January 2, 2015 |DAILY BEAST 
It contains above eighty thousand houses, and about six hundred thousand inhabitants. Gulliver's Travels |Jonathan Swift 
He didn't need to wait—as the birds did—until an angleworm stuck his head above ground. The Tale of Grandfather Mole |Arthur Scott Bailey 
It is then we make him our friend, which sets us above the envy and contempt of wicked men. The Life and Most Surprising Adventures of Robinson Crusoe, of York, Mariner (1801) |Daniel Defoe 
We had now approached closely to the foot of the mountain-ranges, and their lofty summits were high above us in mid-air. Blackwood's Edinburgh Magazine, Volume 60, No. 372, October 1846 |Various 
Above all, he was amazed to hear me talk of a mercenary standing army in the midst of peace and among a free people. Gulliver's Travels |Jonathan Swift