Just the hard-on before you shoot unarmed members of the public. 'Babylon' Review: The Dumb Lives of Trigger-Happy Cops |Melissa Leon |January 9, 2015 |DAILY BEAST 
The gunman hardly broke stride as he nonetheless shot Merabet in the head, killing him. The Muslim Cop Killed by Terrorists |Michael Daly |January 9, 2015 |DAILY BEAST 
Three on-the-record stories from a family: a mother and her daughters who came from Phoenix. I Tried to Warn You About Sleazy Billionaire Jeffrey Epstein in 2003 |Vicky Ward |January 7, 2015 |DAILY BEAST 
You just travel light with carry-on luggage, go to cities that you love, and get to hang out with all your friends. Coffee Talk with Fred Armisen: On ‘Portlandia,’ Meeting Obama, and Taylor Swift’s Greatness |Marlow Stern |January 7, 2015 |DAILY BEAST 
It was a brick wall that we turned into the on-ramp of a highway. Coffee Talk with Fred Armisen: On ‘Portlandia,’ Meeting Obama, and Taylor Swift’s Greatness |Marlow Stern |January 7, 2015 |DAILY BEAST 
Madame Ratignolle, more careful of her complexion, had twined a gauze veil about her head. The Awakening and Selected Short Stories |Kate Chopin 
Only in the carnage of the head, the tilt of the chin, was the insolence expressed that had made her many enemies. Ancestors |Gertrude Atherton 
Old Mrs. Wurzel and the buxom but not too well-favoured heiress of the house of Grains were at the head of the table. The Pit Town Coronet, Volume I (of 3) |Charles James Wills 
She sat straight up in bed, and jerked her hands to her head, and screamed long and terribly. The Homesteader |Oscar Micheaux 
A fancy came into my head that I would entertain the king and queen with an English tune upon this instrument. Gulliver's Travels |Jonathan Swift