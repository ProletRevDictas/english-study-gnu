He was as unsociable, hoggish an old curmudgeon as ever rode a stout hack. The Funny Side of Physic |A. D. Crabtre 
And you will cosset him in them—to save his hoggish dignity487 and buttress up his heavy pride. The Fifth Queen Crowned |Ford Madox Ford 
It is no more hoggish, really, than the longing for sleep if you haven't slept for nights, or for water when one is thirsty. Thorley Weir |E. F. (Edward Frederic) Benson 
The Master sent in another, with a hoggish grunt which spoke of the energy behind it. The Croxley Master: A Great Tale Of The Prize Ring |Arthur Conan Doyle 
It was a hoggish thing to do; but he was a native and knew no reason for letting up. Fishing With The Fly |Charles F. Orvis and Others