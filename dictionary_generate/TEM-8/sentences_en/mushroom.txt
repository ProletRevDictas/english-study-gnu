Other wins have included a mushroom porridge torta in 2012 and a sticky toffee porridge in 2014. In Pursuit of the Perfect Bowl of Porridge |Clarissa Wei |September 11, 2020 |Eater 
High levels of Penicillium fungi, for example, resulted in wine with low levels of octanoic acid, a volatile compound that can give wine a mushroom flavor. How does a crop’s environment shape a food’s smell and taste? |Carolyn Beans |September 10, 2020 |Science News 
Among control bees, those with larger mushroom bodies learned faster. Pesticides can have long-term impact on bumblebee learning |Alison Pearce Stevens |May 18, 2020 |Science News For Students 
On average, bees exposed to pesticide had smaller mushroom bodies than did those in the control group. Pesticides can have long-term impact on bumblebee learning |Alison Pearce Stevens |May 18, 2020 |Science News For Students 
Having bigger mushroom bodies didn’t make it easier for those bees to learn. Pesticides can have long-term impact on bumblebee learning |Alison Pearce Stevens |May 18, 2020 |Science News For Students 
For Iraq, it was the WMDs and the mushroom clouds (and yes, they were lies, people, not intelligence failures). Can America Still Win Wars? |Michael Tomasky |October 4, 2014 |DAILY BEAST 
He even invited us to come back to the Luhansk region after the war and go mushroom picking with him. Held at Gunpoint by Ukraine Rebels |Anna Nemtsova |May 31, 2014 |DAILY BEAST 
If you could have a mushroom granola bar a half-hour before you work out, well, that would be ideal. Mushrooms Are Magic for Women Trying to Lose Weight |Liza Gates |May 22, 2014 |DAILY BEAST 
You can actually buy the same mushroom powder used in our study. Mushrooms Are Magic for Women Trying to Lose Weight |Liza Gates |May 22, 2014 |DAILY BEAST 
After determining that the bee pollen and mushroom broth were inedible, the “detox” quickly went downhill. We Were Gwyneth’s GOOP Guinea Pigs |Erin Cunningham, Olivia Nuzzi |March 30, 2014 |DAILY BEAST 
The invalid sat on the shank of a mushroom anchor, and smoked his pipe while he affected to superintend the work. The Floating Light of the Goodwin Sands |R.M. Ballantyne 
But if you let cows get at a stack they will rub against it until it looks like a monster mushroom. The Red Cow and Her Friends |Peter McArthur 
Of certain features of existing places I have made a composite, which is the "Mushroom Town" of this book. Mushroom Town |Oliver Onions 
A large species of mushroom, of the puff-ball kind, was not uncommon, nearly equal in size to a man's head. Early Western Travels 1748-1846, Volume XVI |Various 
When done, turn it out and serve a good white mushroom sauce round it. Dressed Game and Poultry la Mode |Harriet A. de Salis