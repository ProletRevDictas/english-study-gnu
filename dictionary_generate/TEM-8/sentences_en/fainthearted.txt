I never was fainthearted before; but I cannot believe such a creature cares for me.' North and South |Elizabeth Cleghorn Gaskell 
I ricommend to you, trusting to God, and putting down all misgivings and fainthearted feelin's. The Deerslayer |James Fenimore Cooper 
Sometimes I feel almost fainthearted, which is cowardly and forgetful of our calling "to fight manfully under Christ's banner." Life of John Coleridge Patteson |Charlotte M. Yonge 
By its power he keeps the whole herd of dejected, fainthearted, despairing and unsuccessful creatures, fast to life. Friedrich Nietzsche |Georg Brandes 
Only the fainthearted talk of retreating; the others do not think of it, and these are more than the majority. The Lone Ranche |Captain Mayne Reid