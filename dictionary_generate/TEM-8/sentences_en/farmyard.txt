"You had better plan to reach the farmyard at nine o'clock sharp," Jimmy Rabbit advised him. The Tale of Buster Bumblebee |Arthur Scott Bailey 
So he found himself shortly in the carriage-house, from which everything on wheels had been run outside into the farmyard. The Tale of Buster Bumblebee |Arthur Scott Bailey 
Farmyard cats—cats that knew the difference between chickens, ducklings, mice, and rats. Our Cats and All About Them |Harrison Weir 
Rhoda nodded round to all the citizens of the farmyard; and so eased her heart of its laughing bubbles. Rhoda Fleming, Complete |George Meredith 
Can you believe that I once had a well-deserved reputation in several nurseries as a farmyard imitator? The Poison Belt |Arthur Conan Doyle