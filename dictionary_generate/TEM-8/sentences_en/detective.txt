Then, the two paramedics and two city police officers and a detective walked into the room. The Accidental Attempted Murder |Eugene Robinson |September 2, 2020 |Ozy 
Only then would the detective seek other evidence like eyewitnesses or forensic data to justify arresting and charging the subject. The Bias in the Machine - Issue 89: The Dark Side |Sidney Perkowitz |August 19, 2020 |Nautilus 
He’s an epidemiologist, or disease detective, in Canada at the University of Toronto. Viral scents? Dogs sniff out coronavirus in human sweat |Sharon Oosthoek |August 19, 2020 |Science News For Students 
His team described how its detective work led to this conclusion in the January 2015 EMBO Molecular Medicine. How to find the next pandemic virus before it finds us |Lindsey Konkel |April 30, 2020 |Science News For Students 
She’s a virus detective at the University of California, Davis. How to find the next pandemic virus before it finds us |Lindsey Konkel |April 30, 2020 |Science News For Students 
On Thursday, Detective Superintendent McDonald described his account as “harrowing” and compelling. Victim: I Watched British MPs Rape and Murder Young Boys |Nico Hines |December 18, 2014 |DAILY BEAST 
Robert Morrill, a former gang detective and author of The Mexican Mafia/The Story, said there is little the authorities can do. The Mexican Mafia Is the Daddy of All Street Gangs |Seth Ferranti |December 11, 2014 |DAILY BEAST 
The Glock family feud gets messier with new court documents alleging that gun magnate Gaston had a detective follow his ex-wife. Glock Founder Puts Bullseye on Ex-Wife |Brandy Zadrozny |December 6, 2014 |DAILY BEAST 
Back at police headquarters, Chief of Police Michael Floore Sr. ran out of the detective bureau, barking into a walkie talkie. The Disappearing Cops of East St. Louis |Justin Glawe |November 26, 2014 |DAILY BEAST 
She had a simple answer to my inquiry about the calls that were never returned—from her or anyone else in the detective bureau. The Disappearing Cops of East St. Louis |Justin Glawe |November 26, 2014 |DAILY BEAST 
The door opened and a plain clothes detective entered the office. The Joyous Adventures of Aristide Pujol |William J. Locke 
So did my versatile friend, joyously confident in his powers, start on his glorious career as a private detective. The Joyous Adventures of Aristide Pujol |William J. Locke 
Detective Frey came in and saw Duggin lying dead, and he figured he'd go out and do big things. Hooded Detective, Volume III No. 2, January, 1942 |Various 
The detective went downstairs and talked with Mrs. McCarthy a few minutes, and then took his leave. The Homesteader |Oscar Micheaux 
The detective Agnes had retained, called on Baptiste's lawyers and held a lengthy consultation. The Homesteader |Oscar Micheaux