"He brought Ray Charles to the mix as an influence on rock & roll," E Street Band guitarist Steven Van Zandt once raved. Joe Cocker's Deep Live Cuts |Asawin Suebsaeng |December 22, 2014 |DAILY BEAST 
An x-ray two hours later confirms my hunch: my tibia (the big bone behind the shin) is snapped clean in two. You’re Never ‘Cured’ of an Eating Disorder |Carrie Arnold |December 20, 2014 |DAILY BEAST 
This is admittedly a loaded question, but do you feel James Earl Ray really killed Martin Luther King Jr.? Ava DuVernay on ‘Selma,’ the Racist Sony Emails, and Making Golden Globes History |Marlow Stern |December 15, 2014 |DAILY BEAST 
Thanks to CompStat and strategies added by Police Commissioner Ray Kelly, crime continued to decline. Eric Garner Was Just a Number to Them |Michael Daly |December 5, 2014 |DAILY BEAST 
“Getting out of X band is on option,” said one senior Air Force official. Pentagon Worries That Russia Can Now Outshoot U.S. Stealth Jets |Dave Majumdar |December 4, 2014 |DAILY BEAST 
He thought they were now in touch with our troops at "X" but that they had been through some hard fighting to get there. Gallipoli Diary, Volume I |Ian Hamilton 
W was a Watchman, and guarded the door; X was expensive, and so became poor. Boys and Girls Bookshelf; a Practical Plan of Character Building, Volume I (of 17) |Various 
A ray of Consciousness is passed over that impression and you re-read it, you re-awaken the record. Assimilative Memory |Marcus Dwight Larrowe (AKA Prof. A. Loisette) 
Gordon turned his pleading eyes on his old friend without a ray of concession; but for a moment he hesitated. Confidence |Henry James 
Presently a ray of inspiration dispelled the cloud from the features of the battered man. The Joyous Adventures of Aristide Pujol |William J. Locke