Shiftall, a subsidiary of Panasonic, has a bodysuit that makes you feel temperature changes via a sensor placed on the nape of the neck. The Metaverse Ideas From CES 2022 That We’re Most Excited – and Confused – About |Andrew R. Chow |January 13, 2022 |Time 
This shadow follows my every move, an uncomfortable chill on the nape of my neck. How our data encodes systematic racism |Amy Nordrum |December 10, 2020 |MIT Technology Review 
His black hair sweeps back from the crest of his high forehead and laps at the nape of his neck; his lips are pursed. Meet Alexandre Desplat, Hollywood’s Master Composer |Andrew Romano |February 11, 2014 |DAILY BEAST 
His gray hair curls at the nape, but he is as graceful as he was in 1949, his timing as immaculate. Marty Reisman: The Magical Hustler Who Saved a Classic Game |Harold Evans |December 10, 2012 |DAILY BEAST 
Oref—The homefront, although literally, the back of the neck, the nape. New Words For A New War |Gil Troy |November 19, 2012 |DAILY BEAST 
From there, the pain shoots out toward my temple and down to the nape of my neck. Presidents Can Have Migraines |Dana Goldstein |July 20, 2011 |DAILY BEAST 
If a man has hired an ox and has crushed its foot or has cut its nape, ox for ox to the owner of the ox he shall render. The Oldest Code of Laws in the World |Hammurabi, King of Babylon 
She knew that Harney liked to see its reddish edges ruffled about her forehead and breaking into little rings at the nape. Summer |Edith Wharton 
They approached in a friendly manner, “dancing and making many signs of joy, saying in their tongue Nape tondamen assuath.” The Indian in his Wigwam |Henry R. Schoolcraft 
In this connexion, I need merely allude to the production of voluptuous sensations by tickling the nape of the neck. The Sexual Life of the Child |Albert Moll 
Ajax grabbed him by the nape of the neck and seat of his uniform, nearly ruining one of the epaulets. Watch Yourself Go By |Al. G. Field