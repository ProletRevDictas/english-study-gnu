Boris Johnson, the UK’s prime minister since 2019 and the spearhead of the 2016 Brexit campaign, resigned today. UK prime minister Boris Johnson has resigned |Cassie Werber |July 7, 2022 |Quartz 
She did not spearhead initiatives to help mothers rejoin the world of work. The New Right-Wing Idol: Working Moms |Tim Teeman |July 16, 2014 |DAILY BEAST 
Putin appears to be using elite commandos—Spetsnaz—to spearhead his stealth move into Crimea and, perhaps, beyond. Russia's Special Ops Invasion of Ukraine Has Begun |Eli Lake, Anna Nemtsova |March 15, 2014 |DAILY BEAST 
Price runs his own MRM on-line magazine, The Spearhead, which both compliments and competes with AV4M. The Masculine Mystique |R. Tod Kelly |October 20, 2013 |DAILY BEAST 
He will travel to Sanford this week to help spearhead protest rallies. Sanford Mourns the Loss of Trayvon All Over Again |Jacqui Goddard |July 14, 2013 |DAILY BEAST 
Or helping spearhead the push for comprehensive immigration reform among American Christians. First Ladies of the Church |Joshua DuBois |March 20, 2013 |DAILY BEAST 
MacRae's seat, stone-marker, and aboriginal spearhead; the three lined up like the sights of a modern rifle. Raw Gold |Bertrand W. Sinclair 
See if you can strike off tiny flakes until the large flake looks like a spearhead. The Later Cave-Men |Katharine Elizabeth Dopp 
In the coffin were also a bronze spearhead and several weapons of flint—facts which all go to establish a remote date. In Search Of Gravestones Old And Curious |W.T. (William Thomas) Vincent 
On 4th June, 1915, in Gallipoli, you forced your way like a spearhead into and through line upon line of Turkish trenches. The Seventh Manchesters |S. J. Wilson 
The spearhead at the same rate would weigh about eighteen pounds twelve ounces. The Bible: what it is |Charles Bradlaugh