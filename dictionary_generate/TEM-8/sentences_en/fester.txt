We denied them loans, closed them off in housing projects, redlined their neighborhoods, and left them to fester. The Flaw in My Brother’s Keeper |Jamelle Bouie |February 28, 2014 |DAILY BEAST 
I've tried to forget the grudges, the painful memories, the resentments I allowed to fester in my heart for so long. How 'The Little Way of Ruthie Leming' Taught Me It's OK to Love My Hometown |Justin Green |April 10, 2013 |DAILY BEAST 
Low-grade insurgencies fester in other states, notably among the Karen minority. Obama Does Delicate Dance on Historic Visit to Burma |Lennox Samuels |November 19, 2012 |DAILY BEAST 
But they will leave the country rudderless, the victory will be hollow, and the problems will be left to fester. Ryan Budget Plan Sounds Good But Lacks Substance |Zachary Karabell |August 13, 2012 |DAILY BEAST 
Jewish refugees were absorbed in Israel and the West; the Palestinians were left to fester in camps. Denigrating Jewish Refugees |Lyn Julius |August 9, 2012 |DAILY BEAST 
Several of my toes commenced to blacken and fester near the tips and the nails worked loose. The Home of the Blizzard |Douglas Mawson 
The wrong done the body politic may fester unseen, but it festers on all the same. Belford's Magazine, Vol. II, No. 3, February 1889 |Various 
Only such carrion as this was left to fester upon the earth, to poison the lives of decent men and women. A Sheaf of Corn |Mary E. Mann 
The enemies of their fellows are bred, not in deserts, but in cities, where human creatures fester together in heaps. Impressions And Comments |Havelock Ellis 
There are words a man has no power or wish to say to a man, yet which must be spoken or they fester in his mind. A Book o' Nine Tales. |Arlo Bates