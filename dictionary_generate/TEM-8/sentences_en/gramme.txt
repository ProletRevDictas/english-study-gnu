Furthermore, a person with norovirus has about 70 billion viral particles per gram of stool. A Doctor Explains Why Cruise Ships Should Be Banned |Kent Sepkowitz |November 19, 2014 |DAILY BEAST 
The European formula for Fireball has even less: under one gram per kilogram of propylene glycol. Europeans Recall Fireball Whiskey Over a Sweetener Also Used in Antifreeze |Tim Mak |October 28, 2014 |DAILY BEAST 
Wax can cost a hundred dollars a gram, while buds are as cheap as $20 these days. This Is Your E-Cigarette on Drugs |Daniel Genis |July 28, 2014 |DAILY BEAST 
Food business groups argue that a gram of sugar, natural or added, is a gram of sugar—so why distinguish it? Guess Who Doesn’t Want You to Know How Much Added Sugar Is in Your Food |Tim Mak |July 19, 2014 |DAILY BEAST 
He was, he said, amazed that “a fraction of a gram of sugar had rendered [him] unconscious.” The Week in Death: Alexander Shulgrin, Who Synthesized the Drug Ecstasy |The Telegraph |June 7, 2014 |DAILY BEAST 
Recognition of the pneumococcus depends upon its morphology, the fact that it is Gram-staining, and the presence of a capsule. A Manual of Clinical Diagnosis |James Campbell Todd 
The great majority belong to the colon bacillus group, and are negative to Gram's method of staining. A Manual of Clinical Diagnosis |James Campbell Todd 
A Gram-positive stool due to cocci is suggestive of intestinal ulceration. A Manual of Clinical Diagnosis |James Campbell Todd 
Gram's method (p. 40) is a very useful aid in distinguishing certain bacteria. A Manual of Clinical Diagnosis |James Campbell Todd 
This is a minute, slender rod, which lies within and between the pus-corpuscles (Fig. 125), and is negative to Gram's stain. A Manual of Clinical Diagnosis |James Campbell Todd