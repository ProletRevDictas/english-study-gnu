That is more than enough for him and his wife, Vasiliki, but in this family-oriented society pensions often subsidise the young. Greece on the Brink of Financial Abyss as Syriza Party Weighs Default |John Psaropoulos |May 12, 2012 |DAILY BEAST 
That he was anxious we have seen, by his attempts to subsidise his literary gains by a Government office. Thackeray |Anthony Trollope 
I think, now I am rich and respectable, I shall subsidise a prize-fighter to pitch into me once a fortnight. Ravenshoe |Henry Kingsley 
No rich man has yet proposed to found, endow, or subsidise such a theatre. The Golden Butterfly |Walter Besant 
The Indian peasants groaned under the burden of taxes imposed to subsidise a horde of functionaries. The South American Republics, Part II (of 2) |Thomas C. Dawson 
Brenart, whom she could no longer subsidise, kept aloof, for mixed reasons of his own. The History of David Grieve |Mrs. Humphry Ward