Over time, these patients develop visibly enlarged salivary glands; they can also become malnourished. 5 Hidden Eating Disorders |Danielle Friedman |May 2, 2010 |DAILY BEAST 
These migrate to the salivary glands, and are carried into the blood of the person whom the mosquito bites. A Manual of Clinical Diagnosis |James Campbell Todd 
In very small amounts alcohol stimulates the secretion of the salivary and gastric glands, and thus appears to aid in digestion. A Civic Biology |George William Hunter 
This organ may perhaps be best compared with the simple salivary gland of Julus. The Works of Francis Maitland Balfour, Volume 1 |Francis Maitland Balfour 
Less well developed than the salivary organs, they are often of a very complicated structure. The Insect World |Louis Figuier 
Resting a moment, she proceeded to apply the salivary glue and adjust the twig, and then settled again to the task of sitting. Bird Lore, Volume I--1899 |Various