We are not talking about a trivial difference over policy — or even a major one. Stop trying to save the GOP. It’s hopeless. |Jennifer Rubin |February 12, 2021 |Washington Post 
Leaving your camera off for a call you don’t need to be visible for makes for a small — but not trivial — savings in carbon emissions. Remote workers are greener, but their tech still has a real carbon cost |Devin Coldewey |January 20, 2021 |TechCrunch 
It just has to be a non-trivial cost for someone who was on the margin. How Much Do We Really Care About Children? (Ep. 447) |Stephen J. Dubner |January 14, 2021 |Freakonomics 
That would be so regardless of how much the aliens shattered the beliefs people held about their own societies, whose beloved differences would look trivial by comparison to those with the Little Green Men. Why a Universal Society Is Unattainable - Issue 95: Escape |Mark W. Moffett |January 14, 2021 |Nautilus 
The axion has all the right properties, which is not trivial at all. A Prodigy Who Cracked Open the Cosmos |Claudia Dreifus |January 12, 2021 |Quanta Magazine 
And more trivial modifications like altering bodily odors and promoting a healthy lifestyle. Design Your Own Dinosaur: The Era of Custom DNA |Justin Jones |January 8, 2015 |DAILY BEAST 
Harping about a Republican war on women while wages stagnate and growth sputters is trivial and desperate. Earth to DNC: Dyspeptic Dad Still Votes, Too |Lloyd Green |November 11, 2014 |DAILY BEAST 
The most riveting stories so far deal with trivial matters that sound like deleted scenes from a George Costanza fever dream. Why D.C. Wants an Election About Nothing |Nick Gillespie |October 23, 2014 |DAILY BEAST 
Seemingly trivial facts gathered from a variety of experiences can change the course of a future narrative. Writing a Novel: Even Making It Up Requires Research |Ridley Pearson |July 16, 2014 |DAILY BEAST 
Clarification: in a country with vile, daily crimes against women it is wrong for privileged women to file trivial cases. How India’s Elites Encourage Rape |Neha Sharma |July 15, 2014 |DAILY BEAST 
Never did events of the utmost magnitude hinge on incidents so trivial to the community at large. The Red Year |Louis Tracy 
They merely used such instruments as fate offered, however trivial, however clumsy. The Wave |Algernon Blackwood 
If she ignored his note it would give undue importance to a trivial affair. The Awakening and Selected Short Stories |Kate Chopin 
They were little reasons, trivial grains of offence which through long years had accumulated into a mountain. St. Martin's Summer |Rafael Sabatini 
His secret thoughts he buried beneath a continuous mental preoccupation with the vain and the trivial. The Man from Time |Frank Belknap Long