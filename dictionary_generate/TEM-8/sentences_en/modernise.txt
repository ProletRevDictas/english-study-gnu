Again, the later remanieurs of the earliest Chansons de Geste modernise the details of these poems. Homer and His Age |Andrew Lang 
We modernise the somewhat difficult spelling, but retain the quaint language of the original. Irish Witchcraft and Demonology |St. John D. (St. John Drelincourt) Seymour 
As a result, nearly five hundred dollars was voted from the corporation funds to strengthen and modernise the "calaboose." The Daughter of Anderson Crow |George Barr McCutcheon 
Even so, it's better than treating the world like a company trading for profit, but we must modernise the rules. Sonia Married |Stephen McKenna 
We might modernise a little, so as to show the real sense, by saying 'Glevum city and Corinium city and Bath city.' Science in Arcady |Grant Allen