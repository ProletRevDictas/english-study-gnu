Having yourself immortalized with a paunch indicated you were wealthy/held high office/were involved in derring-do. The Return of the Power Paunch |Sean Macaulay |May 1, 2013 |DAILY BEAST 
If you find yourself hungering for more humorous were-action, check out the British version of Being Human on BBC America. My Favorite Horror Comedies |Marti Noxon |August 16, 2011 |DAILY BEAST 
So too, does Henry Kissinger and did the late Arthur Schlesinger Jr., but they are/were apparently another story. The 'Delusional Left' Wins |Eric Alterman |March 18, 2010 |DAILY BEAST 
There was a certain difficulty in obtaining the necessary funds without announcing precisely what they-were for. Hyacinth |George A. Birmingham 
In pattern it showed bright green flowers-that-never-were sprawling on a purple background. Roast Beef, Medium |Edna Ferber 
In Abyssinia, at the present day, blacksmiths are considered to be were-wolves, according to Winstanley. The Medicine-Men of the Apache. (1892 N 09 / 1887-1888 (pages 443-604)) |John G. Bourke 
In tropic countries we have stories of supernatural snakes, who appear in various forms, as were-snakes, shall we say? The Supernatural in Modern English Fiction |Dorothy Scarborough 
Accordingly they returned to the Court, where the 288 were-wolf became an object of the greatest curiosity to all. Legends &amp; Romances of Brittany |Lewis Spence