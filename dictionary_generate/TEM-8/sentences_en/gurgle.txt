She kept her mask up to greet him over the hiss of the grill and the gurgle of fryer oil. Does It Get Better For the Indie Fine Dining Restaurant? |Sara Sheridan |July 22, 2021 |Eater 
Laughing at intervals that low gurgle which sprang from fear, as some wild bird would start up at his approach, he plodded on. The Underworld |James C. Welsh 
The next instant he sank back with a gurgle in his throat and a knife thrust in his side. Blazed Trail Stories |Stewart Edward White 
Elsie jumped up with a little gurgle of joy and ran ahead of her mother to the 25 flower. Every Girl's Book |George F. Butler 
Then there was a loud scream that died in a weltering gurgle. Hunters Out of Space |Joseph Everidge Kelleam 
His eyes closed tight, and there was a funny gurgle in his throat. The Planet Strappers |Raymond Zinke Gallun