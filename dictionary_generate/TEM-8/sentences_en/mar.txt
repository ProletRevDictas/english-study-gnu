But Khin Mar Cho is pinning her hopes on the international community. Junta Crackdown On Burmese Press |Joshua Carroll |July 12, 2014 |DAILY BEAST 
A steel bracelet on my wrists reads CPL BRIAN L. CHEVALIER - 14 MAR 2007 – DIYALA. Memorial Days After Mourning Has Passed |Alex Horton |May 25, 2014 |DAILY BEAST 
Then came his turn as Ennis Del Mar, the gay lovelorn cowboy in Brokeback Mountain, and the rest is history. Rob Lowe: Don’t Hate Me Because I’m Beautiful |Tricia Romano |April 8, 2014 |DAILY BEAST 
Pujol was offering a choice of tasting menus that evening: Mar (Surf) or Tierra (Turf). Mexico City’s Magical Moment of Resurgence |Condé Nast Traveler |February 10, 2014 |DAILY BEAST 
The Savanna-La-Mar Hurricane then moved onto Cuba, killing more than 1,000, in total. Uncovering Jamaica’s Jewish Past |Debra A. Klein |December 1, 2013 |DAILY BEAST 
Railroads are reckless Radicals and are destined by turns to make and to mar the fortunes of many great emporiums. Glances at Europe |Horace Greeley 
But the Earls of Mar and Athol are collecting their forces, and some other nobles of the land are drawing to their party.' King Robert the Bruce |A. F. Murison 
A cigar should be handled daintily; it is a fragile, graceful creature—don't mar its beauty. Tobacco; Its History, Varieties, Culture, Manufacture and Commerce |E. R. Billings. 
One glaring color, or conspicuous article, would entirely mar the beauty of such a dress. The Ladies' Book of Etiquette, and Manual of Politeness |Florence Hartley 
Collars or sleeves, pinned over or tightly strained to meet, will entirely mar the effect of the prettiest dress. The Ladies' Book of Etiquette, and Manual of Politeness |Florence Hartley