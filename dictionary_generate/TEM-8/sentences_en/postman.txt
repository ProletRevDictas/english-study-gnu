Cheval was born in 1836 in Charmes, France, and he chose to serve his small community as a postman. The Postman Who Built a Palace in France…by Hand |Nina Strochlic |November 20, 2014 |DAILY BEAST 
And they were all viewing the uncut version, too—the one not in theaters—because of postman Guy. Kevin Smith's Marijuanaissance: On 'Tusk,' 'Falling Out' with Ben Affleck, and 20 Years of 'Clerks' |Marlow Stern |September 9, 2014 |DAILY BEAST 
The Postman cost $80 million to make; it earned $18 million at the box office. Are We in the Midst of a Kevin Costner Comeback? |Andrew Romano |January 28, 2014 |DAILY BEAST 
I went out, put the article in the mailbox, and raised the flag for the postman. Lawrence Wright: How I Write |Noah Charney |May 22, 2013 |DAILY BEAST 
Cain wrote one good book, Postman, and a shelf full of schlock. Can Pulp Win the Booker? |Allen Barra |September 7, 2011 |DAILY BEAST 
The old postman says it was insufficiently addressed, or it 'ud ha' been here by first post. Hilda Lessways |Arnold Bennett 
But the postman never came near the little cottage at the elbow in Whiffle Street, all that day. The Girls of Central High on the Stage |Gertrude W. Morrison 
It had become a standard jest with the ex-postman that she should never go anywhere away from Skyrie without her pocket-book. Dorothy at Skyrie |Evelyn Raymond 
The statement was in the form of a question, to which the ex-postman rather coldly replied: "Yes, so I have heard." Dorothy at Skyrie |Evelyn Raymond 
The steps continue and grow faint, as the postman rounds the distant corner. Prison Memoirs of an Anarchist |Alexander Berkman