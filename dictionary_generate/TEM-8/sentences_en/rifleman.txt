If Rifleman is rather a tragic character, his ally Alexander “Babay” Mozhayev is more comic. The Kremlin’s Crazy Shock Troops |Oleg Shynkarenko |May 22, 2014 |DAILY BEAST 
He was assigned as a rifleman to Fox Company, Second Battalion, Fifth Marine Regiment. On Memorial Day, Remembering 15-Year-Old Marine PFC Dan Bullock |Michael Daly |May 28, 2012 |DAILY BEAST 
The idea here is to permit each rifleman to believe that he is not a killer. Last Night's Gruesome Execution |David R. Dow |June 18, 2010 |DAILY BEAST 
The miller was glad that a rifleman from his own canton should prove the best shot, and should have won universal applause. Rudy and Babette |Hans Christian Andersen 
The rifleman kept running, seeming not to hear him in his panic. Space Prison |Tom Godwin 
Mr. Simon Uhlmann presented a bronze figure of a rifleman, as second annual prize. A report on the feasibility and advisability of some policy to inaugurate a system of rifle practice throughout the public schools of the country |George W. Wingate 
While making for this point, a sail was sighted, which proved to be the British brig-sloop "Rifleman." The Naval History of the United States |Willis J. Abbot. 
The glasses crashed one after another like gelatine balls hit by the bullets of an expert rifleman. Astounding Stories of Super-Science February 1930 |Various