Almost 80 people died, most of them drowned in their vehicles, electrocuted, or crushed under collapsed buildings. The architect making friends with flooding |Erica Gies |December 21, 2021 |MIT Technology Review 
On June 19, 1953, Ethel Rosenberg was electrocuted, having been found guilty of conspiracy to commit espionage on behalf of the Soviet Union. How Today’s World Can Help Us See That There’s More to the Ethel Rosenberg Story |Anne Sebba |June 9, 2021 |Time 
He also built shocking devices in middle school consisting of a box with two wires attached that would electrocute its user. The Year's Most Explosive Film |Marlow Stern |August 5, 2011 |DAILY BEAST 
A strong electric shock may of course electrocute a plant by killing the cells. The Romance of Plant Life |G. F. Scott Elliot 
"Thar's a cayuse in thar thet I'd plumb like ter electrocute," said Bud, who was mad clear through. Ted Strong's Motor Car |Edward C. Taylor 
We electrocute the Italian and print pictures of the wheat speculator in our magazines as an example of Success. The Root of Evil |Thomas Dixon 
And if you make any sudden moves you are liable to break a phone, electrocute yourself or choke to death. Sense of Obligation |Henry Maxwell Dempsey (AKA Harry Harrison) 
The man in the control room had frustrated the attempt to electrocute us. Spacewrecked on Venus |Neil R. Jones