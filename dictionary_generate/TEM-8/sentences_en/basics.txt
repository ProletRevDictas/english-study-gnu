The men use the dolls to practice the basics of caring for babies. How Good Dads Can Change the World |Gary Barker, PhD, Michael Kaufman |January 6, 2015 |DAILY BEAST 
Despite the obvious ongoing problems with disease and access to basics, the future of Africa is bright. Silicon Valley Sets Its Sights on Africa |Christian Borys |December 22, 2014 |DAILY BEAST 
The Spire, like most fountains, has the basics -- Pepsi, Mountain Dew, Sierra Mist, Brisk Iced Tea and SoBe Lifewater. Font of Invention | |September 18, 2014 |DAILY BEAST 
Instead, LaFrieda, Hanna-Korpi, and Dawson all see a general back-to-basics approach from hereon in. Have We Reached ‘Peak Burger’? The Crazy Fetishization of Our Most Basic Comfort Food |Brandon Presser |July 31, 2014 |DAILY BEAST 
It may be time to kick these new age diet fads and get back to basics. Helen Mirren Trains Like the Air Force; Kendall Jenner Denied Kim Kardashian at Her Shows |The Fashion Beast Team |July 29, 2014 |DAILY BEAST 
It is hovering on the borders of a region containing this planet we are to land on—a region operating on other basics. Unthinkable |Roger Phillips Graham 
In some way both sets of basics operate in either conflict or compromise. Unthinkable |Roger Phillips Graham 
With the new basics it does just that—except that it is still just one car, and yet never was just one car and never will be. Unthinkable |Roger Phillips Graham 
This Nation will not go back to the days of simply shuffling children along from grade to grade without them learning the basics. State of the Union Addresses of George W. Bush |George W. Bush 
That was one of the basics they had lost, years ago—their belief that life would arise on any planet capable of supporting it. An Empty Bottle |Mari Wolf