In 2009, when a military doctor finally diagnosed her husband with the condition, she says, “that was the big a-ha moment.” When the War Comes Home |Sara Stewart |October 16, 2014 |DAILY BEAST 
I lay there and thrashed about and all I could hear was my father and ‘ha, ha, ha’. Kate and William’s Royal Family Values |Tom Sykes |September 22, 2014 |DAILY BEAST 
From just outside the main door a distinct “ha-ha-ha” echoed up and down the concourse. Inside a Hospital for the Criminally Insane |Caitlin Dickson |September 15, 2014 |DAILY BEAST 
Lonard thinks ha-ha-ha and tells Bantam that the rights have reverted to him, which they have. Elmore Leonard’s Rocky Road to Fame and Fortune |Mike Lupica |September 13, 2014 |DAILY BEAST 
I was really surprised because Head-On ha​d already been brought to the screen six years ago. Game of Thrones’ Sibel Kekilli Discusses Shae’s Treachery at the Trial of Tyrion Lannister |Marlow Stern |May 13, 2014 |DAILY BEAST 
The old postman says it was insufficiently addressed, or it 'ud ha' been here by first post. Hilda Lessways |Arnold Bennett 
Ha—assure you we quite understand; no necessity to say another word about it. Punch, or the London Charivari, Volume 107, November 3, 1894 |Various 
This is not the first time in history when the lack of a ha'porth of tar has spoilt the ship of State. Gallipoli Diary, Volume I |Ian Hamilton 
It is the chief cereal, and the inhabitants say it originated in Ha-ram, China, nearly five thousand years ago. Our Little Korean Cousin |H. Lee M. Pike 
Sit down, Dorothy Chance, Ye'r welcome to what we ha'; not 'specting company you'll find no junkets at table. The World Before Them |Susanna Moodie