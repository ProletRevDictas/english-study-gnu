The problem is that we delude ourselves into thinking that our intentions are still selfless. The Sikh Idea of Seva Is an Antidote to Our Current Malaise |Simran Jeet Singh |July 26, 2022 |Time 
Most of us have dragged our feet and deluded ourselves for too long about the state of the world. California’s Forever Fire |by Elizabeth Weil, ProPublica, photography by Meridith Kohut for The New York Times Magazine |January 3, 2022 |ProPublica 
But we shouldn't delude ourselves that this would have "kept the election from being stolen". What if the Supreme Court Had Declined to Hear Bush v. Gore? |Megan McArdle |April 29, 2013 |DAILY BEAST 
Without sufficient growth, we should not delude ourselves about how painful this is going to be. As Austerity Cuts Loom, Greeks Strike, But More Cuts Are Demanded |Barbie Latza Nadeau |September 25, 2012 |DAILY BEAST 
To ignore those words—and those ideas—is to disrespect the organization, let alone delude oneself. Boycott Hamas — But Foster Palestinian Moderation |Gil Troy |August 17, 2012 |DAILY BEAST 
We should not delude ourselves that the middle is the golden path. It's All or Nothing in Afghanistan |Vanda Felbab-Brown |October 11, 2009 |DAILY BEAST 
Don't delude yourself with the notion that she is sitting down in sackcloth and ashes with her past! Ancestors |Gertrude Atherton 
The reader would smile to read the infatuated simple reasons with which the bishop endeavoured to delude the ignorant. Fox's Book of Martyrs |John Foxe 
At others, I yield to a fools paradise and delude myself with impossible solutions that deceive me but for an hour. The Wasted Generation |Owen Johnson 
But, in any case, there is no use to delude ourselves as to what are the real qualifications of Mr. Filipino. Where Half The World Is Waking Up |Clarence Poe 
But Isaac was formed in too simple and honest a mould to delude the two women or himself with iridescent dreams of success. McClure's Magazine, January, 1896, Vol. VI. No. 2 |Various