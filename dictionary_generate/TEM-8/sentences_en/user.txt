Routines may still be considered something of a power user feature. Amazon makes Alexa Routines shareable |Sarah Perez |September 17, 2020 |TechCrunch 
Another change will impact which groups are suggested to users. Facebook tries to clean up Groups with new policies |Sarah Perez |September 17, 2020 |TechCrunch 
The most popular branded hashtag challenges make their way to TikTok’s trending page where millions of users can see them. What you must know about TikTok for business |Connie Benton |September 17, 2020 |Search Engine Watch 
In August, Amazon launched “Gold Vault” in India, where users can buy digital gold for as little as Rs5. Amazon’s fintech push is a play to dig further into India |Ananya Bhattacharya |September 17, 2020 |Quartz 
The user experience that your website offers is ultimately what determines whether customers bounce in droves or actually stick around. How to drive digital innovation necessary during the pandemic |Nick Chasinov |September 16, 2020 |Search Engine Watch 
The digital dating sphere can prove tricky, and bruising, for the trans user. Grindr’s Trans Dating Problem |David Levesley |January 9, 2015 |DAILY BEAST 
The user fee on duck stamps goes exclusively to funding federal acquisition of wetlands as wildlife habitat. Nazis, Sunscreen, and Sea Gull Eggs: Congress in 2014 Was Hella Productive |Ben Jacobs |December 29, 2014 |DAILY BEAST 
A click sends a user to a statement, a list of passenger nationalities, emergency call-center numbers, and other information. The Presumed Crash of AirAsia Flight QZ8501 Is Nothing Like MH370 |Lennox Samuels |December 29, 2014 |DAILY BEAST 
That user's posts were being wiped completely from existence. China’s Internet Is Freer Than You Think |Brendon Hong |December 27, 2014 |DAILY BEAST 
That lapse was partly mitigated by the rise of blog­ging, which encouraged user-generated content. You Can Look It Up: The Wikipedia Story |Walter Isaacson |October 19, 2014 |DAILY BEAST 
I made the arrangement shown in the sketch to take care of the line without any effort to the user. The Boy Mechanic, Book 2 |Various 
As it is so important, and above all to the voice-user, it merits special consideration. Voice Production in Singing and Speaking |Wesley Mills 
Therefore the user of these cutters should exercise care when cutting pipe. Elements of Plumbing |Samuel Dibble 
Not that I should have done any better if I had had the myth or the novel, for I am not a good user of opportunities. George Eliot's Life, Vol. II (of 3) |George Eliot 
Some viewers may require user assistance to find fonts containing these characters. William Oughtred |Florian Cajori