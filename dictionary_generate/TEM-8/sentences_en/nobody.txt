There was nobody that I read who was like, “This is just… whatever.” ‘Archer’ Creator Adam Reed Spills Season 6 Secrets, From Surreal Plotlines to Life Post-ISIS |Marlow Stern |January 8, 2015 |DAILY BEAST 
Nobody ever says they want to become a cop so they can bust people for urinating in public or drinking alcohol on their stoop. Shot Down During the NYPD Slowdown |Michael Daly |January 7, 2015 |DAILY BEAST 
If nobody on the outside will send Teresa money, should she learn a prison hustle? How a ‘Real Housewife’ Survives Prison: ‘I Don’t See [Teresa Giudice] Having a Cakewalk Here’ |Michael Howard |January 6, 2015 |DAILY BEAST 
Unlike the characters he plays, men who came from nowhere and, as he himself puts it, “go home to nobody.” The Story Behind Lee Marvin’s Liberty Valance Smile |Robert Ward |January 3, 2015 |DAILY BEAST 
Nobody terrified audiences with a smile as well as Lee Marvin. The Story Behind Lee Marvin’s Liberty Valance Smile |Robert Ward |January 3, 2015 |DAILY BEAST 
He was aware that his act by this time, had helped nobody, had made no one happy or satisfied—not even himself. The Homesteader |Oscar Micheaux 
When you engaged yourself to the young woman you were poor and a nobody, and the step was perhaps excusable. Elster's Folly |Mrs. Henry Wood 
Nobody else wanted Hugh Egerton's society, and he began to believe that this girl sincerely did want it. Rosemary in Search of a Father |C. N. Williamson 
And since nobody answered, Jolly Robin seemed to think he had silenced Mrs. Robin—for once. The Tale of Grandfather Mole |Arthur Scott Bailey 
Please understand that nobody here had the least intention of playing a trick upon you! Punch, or the London Charivari, Volume 107, November 3, 1894 |Various