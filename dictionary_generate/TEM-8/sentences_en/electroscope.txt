At any time in the drift, an electroscope exposed outside became rapidly charged. The Home of the Blizzard |Douglas Mawson 
The electroscope will now insulate well enough to show no appreciable collapse of the leaves in one or two hours' time. On Laboratory Arts |Richard Threlfall 
The quartz electroscope is taken, and the insulating rod heated in the blow-pipe. On Laboratory Arts |Richard Threlfall 
A charge is given to the electroscope, and the time required for a given degree of collapse of the leaves noted. On Laboratory Arts |Richard Threlfall 
When a charged body is brought near the electroscope the leaves become charged similarly by induction. The New Gresham Encyclopedia |Various