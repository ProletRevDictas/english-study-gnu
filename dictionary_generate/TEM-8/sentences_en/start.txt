And not just sick in the body but in your mind, because you start obsessing. How Taryn Toomey’s ‘The Class’ Became New York’s Latest Fitness Craze |Lizzie Crocker |January 9, 2015 |DAILY BEAST 
Between 25 and 30, you’re trying to decide how much longer before you start growing a beard and calling yourself ‘Daddy. Freaking Out About Age Gaps in Gay Relationships Is Homophobic |Samantha Allen |January 9, 2015 |DAILY BEAST 
Kickstarter is one start-up platform that seems to have realized the danger. Design Your Own Dinosaur: The Era of Custom DNA |Justin Jones |January 8, 2015 |DAILY BEAST 
But maybe you have to start somewhere else — with Lamont Waltman Marvin, Monty, his father, the Chief, the old man. The Story Behind Lee Marvin’s Liberty Valance Smile |Robert Ward |January 3, 2015 |DAILY BEAST 
So Marvin had the old showbiz glamour in his life from the start. The Story Behind Lee Marvin’s Liberty Valance Smile |Robert Ward |January 3, 2015 |DAILY BEAST 
Keep closely covered with a bell glass and, in a few weeks, more or less, the baby Ferns will start to put in an appearance. How to Know the Ferns |S. Leonard Bastin 
The smoke from her kitchen fire rose white as she put in dry sumac to give it a start. The Bondboy |George W. (George Washington) Ogden 
There are sentimental children, as there are sentimental adults, who seem never happier than when the tears are ready to start. Children's Ways |James Sully 
But the day he planned to start was very cold—the mercury stood twenty-seven below zero. The Homesteader |Oscar Micheaux 
He said that you were going along, and so I thought I'd hunt you up and tell you that we'll start about seven in the morning. Raw Gold |Bertrand W. Sinclair