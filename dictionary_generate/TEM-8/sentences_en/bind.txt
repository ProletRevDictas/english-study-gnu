The Broadsheet has spent plenty of time covering the “double bind” that women face in the workplace. Do female politicians face a ‘motherhood bind’? |kristenlbellstrom |October 16, 2020 |Fortune 
This New York Times story suggests that the double bind has a sibling—I’ll call it the motherhood bind. Do female politicians face a ‘motherhood bind’? |kristenlbellstrom |October 16, 2020 |Fortune 
That’s the chicken-and-egg situation that has left indoor farms in a bind the world over until now, Teng points out. Inside Singapore’s huge bet on vertical farming |Katie McLean |October 13, 2020 |MIT Technology Review 
To accomplish this next stage of capitalism, we must shake ourselves from the binds of a false narrative, the one that pits the interests of stakeholders against the interests of shareholders. Companies must commit to equal pay to make stakeholder capitalism work |matthewheimer |September 18, 2020 |Fortune 
Many students are taking Mitsch’s advice, and that’s putting colleges in a bind. Is School Out Forever? |Daniel Malloy |August 9, 2020 |Ozy 
By announcing this meeting with such feel-good publicity, they are placing their successors in quite a bind. Plotting Nicea III Could Be Pope Francis's Masterstroke |Candida Moss |June 8, 2014 |DAILY BEAST 
On Escobar's order, Popeye took Mendoza hostage in the warden's house while Escobar tried to figure his way out of the bind. Pablo Escobar’s Private Prison Is Now Run by Monks for Senior Citizens |Jeff Campagna |June 7, 2014 |DAILY BEAST 
Its molecules bind to messenger RNA, allowing certain genes to be “turned off.” Trial Drug Reverses Alzheimer’s Disease in Mice |Elizabeth Lopatto |May 24, 2014 |DAILY BEAST 
This has put Ukrainian gay activists and their allies in a bind. The Closeted Revolution: Kiev’s Gays Keep Quiet to Deny Putin a Propaganda Win |James Kirchick |April 1, 2014 |DAILY BEAST 
Finally, Mandela understood the ties that bind the human spirit. Full Text of President Obama's Eulogy for Nelson Mandela |The Daily Beast |December 10, 2013 |DAILY BEAST 
They have a living faith in the potency of the Horse-Guards, and in the maxim that "Safe bind is sure find." Glances at Europe |Horace Greeley 
He had, however, torn the leg of one of his stockings: so he asked Amy to bind up his wounds. The Nursery, July 1873, Vol. XIV. No. 1 |Various 
Oaths taken in courts of judicature, civil or religious, and the marriage oath, bind the parties in like manner. The Ordinance of Covenanting |John Cunningham 
In one word, to the whole worship of God the soul that clings to His Covenant will cordially bind itself in his dread presence. The Ordinance of Covenanting |John Cunningham 
There is none to judge thy judgment to bind it up: thou hast no healing medicines. The Bible, Douay-Rheims Version |Various