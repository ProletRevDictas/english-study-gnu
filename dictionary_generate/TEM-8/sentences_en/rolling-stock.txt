“Gently rolling hills” roll not-so-gently under my tires, but the English countryside scenery is soporific. Biking With the Bard |Kara Cutruzzula |December 28, 2014 |DAILY BEAST 
In our headlong quest for a legally perfect society, we don’t take the time to take stock of what‘s been created so far. Red Tape Is Strangling Good Samaritans |Philip K. Howard |December 27, 2014 |DAILY BEAST 
Mullins quotes Stewart from an interview with Rolling Stone. David Gregory's 'Meet the Press' Eviction Exposed in Washingtonian Takedown |Lloyd Grove |December 23, 2014 |DAILY BEAST 
In the same house where Rolling Stone's Jackie says she was. I Was Gang Raped at a UVA Frat 30 Years Ago, and No One Did Anything |Liz Seccuro |December 16, 2014 |DAILY BEAST 
But after Rolling Stone's rape story debacle, how much momentum does the call to ban fraternities have left? Fraternities in a Post-UVA World |Samantha Allen |December 12, 2014 |DAILY BEAST 
Sol laughed out of his whiskers, with a big, loose-rolling sound, and sat on the porch without waiting to be asked. The Bondboy |George W. (George Washington) Ogden 
Neither privately owned nor government stock is entitled to voting power. Readings in Money and Banking |Chester Arthur Phillips 
I didn't take much stock in the yarn at the time, but I'm beginning to think he had it straight. Raw Gold |Bertrand W. Sinclair 
One day she had heard a man say, "If there is a drought we shall have the devil to pay with our stock before winter is over." Ramona |Helen Hunt Jackson 
Cotton exchanges reopened on November 16, and stock exchanges opened for restricted trading shortly thereafter. Readings in Money and Banking |Chester Arthur Phillips