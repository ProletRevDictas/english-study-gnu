My mother lived in a time and place where aspiration beyond housewifery and motherhood was neither expected nor admired. You Should Meet My Mother |The Daily Beast |May 8, 2009 |DAILY BEAST 
Mrs. P. My housewifery is but small; but God gave me grace to go to the true church. Fox's Book of Martyrs |John Foxe 
Three steadinesses of good womanhood: keeping a steady tongue; a steady chastity; a steady housewifery. Ancient Irish Poetry |Various 
But, with true Maryland housewifery, she must personally see to all the details of the annual flitting. Dorothy |Evelyn Raymond 
Mandy had said truly that there wasn't a thing on the farm she didn't love to do, and the gift of housewifery ran in the family. The Power and the Glory |Grace MacGowan Cooke 
It became an industrial society to help people to homes and teach practical farming, trades, and housewifery. American Missionary - Volume 50, No. 9, September, 1896 |Various