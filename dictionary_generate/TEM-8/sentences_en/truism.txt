Coaches use the line as a truism, and it often spreads to their players. It Really Is Harder To Beat A Team The Third Time In Men’s College Basketball |Jake Lourim |March 8, 2022 |FiveThirtyEight 
If one of the truisms of connection is to meet people where they are, then Kennedy has it down. How Dr. Becky Became the Millennial Parenting Whisperer |Doree Shafrir |June 26, 2021 |Time 
Our analysis also found that a longtime truism in polling — that surveys using live callers are more accurate — is no longer true. Politics Podcast: The Gold Standard For Polling Has Changed |Galen Druke |March 25, 2021 |FiveThirtyEight 
This is a truism of life, but we can’t resist manufacturing false gods. The difference between a plantation and college sports: A plantation didn’t pretend |Jerry Brewer |March 12, 2021 |Washington Post 
The amendment states but a truism that all is retained which has not been surrendered. Exclusive: GOP Senate Candidate Caught Saying States Can Nullify Laws |Ben Jacobs |July 28, 2014 |DAILY BEAST 
There is a truism bandied about that more people like to read about baseball than watch it. The Literature of Futbol: 11 Great Books About Soccer |Robert Birnbaum |June 25, 2014 |DAILY BEAST 
The religious basis of the fiercest opposition to same-sex marriage is a truism. Opposing Gay Marriage Doesn’t Make You a Crypto-Racist |Jonathan Rauch |April 24, 2014 |DAILY BEAST 
Each of us gathered there had lived the truism of all wars: what can go wrong will go wrong. War Is the New Peace: American Vets Reflect on Syria |John Kael Weston |September 10, 2013 |DAILY BEAST 
That old truism that hawks are the most capable of making peace carries a lot of weight in middle Israel. Netanyahu's Studied Ambiguity |Paul Gross |January 21, 2013 |DAILY BEAST 
In other words, it is a truism, mere equation in terms, telling nothing whatever. The Unsolved Riddle of Social Justice |Stephen Leacock 
Nevertheless, it is a truism which men are none the worse of being reminded of now and then. The Floating Light of the Goodwin Sands |R.M. Ballantyne 
That the day may begin with calm and sunshine, yet end in clouds and tempest—or vice versa—is a truism which need not be enforced. The Floating Light of the Goodwin Sands |R.M. Ballantyne 
Somehow this statement, though a truism, did not seem to fit on to previous remarks. Punch, or the London Charivari, Vol. 93, September 24, 1887 |Various 
It is a mere truism to remark that in every political question the main controversy is complicated by a number of side issues. Is Ulster Right? |Anonymous