The trouble with Gable was that he was under contract to MGM. How 'Gone With the Wind' Got Made |Helen Anders |September 10, 2014 |DAILY BEAST 
Clark Gable and Robert Taylor and Tyrone Power could make women sigh. Mickey Rooney Was Hollywood’s Golden Age Showman |Lorenza Muñoz |April 7, 2014 |DAILY BEAST 
One of her courtiers bet that Clark Gable as Rhett Butler would be her ideal, “a privateer, a romantic, and a fabulous cad.” The Sexy Side of Maggie: How Thatcher Used Her Softer Quality |Gail Sheehy |January 11, 2012 |DAILY BEAST 
Led by Clark Gable, the men aboard rise up and take back the ship. Tron: Legacy and the Attack of the Gay Villains |Jacob Bernstein |December 23, 2010 |DAILY BEAST 
He is like you would imagine a young hipster Clark Gable would be and he's got a leer on him that won't quit. Inside the Vampire Diaries Craze |Choire Sicha |November 2, 2010 |DAILY BEAST 
Tim's letter was in my pocket and the sun was still high over the gable of the mill. The Soldier of the Valley |Nelson Lloyd 
The wall finishes in a gable and the whole west wall is a true termination of the nave which lies behind. Bell's Cathedrals: A Short Account of Romsey Abbey |Thomas Perkins 
And yet you appreciate so keenly my old enamels, and your eye seeks out, in a minute, a picturesque roof or gable. The Daughters of Danaus |Mona Caird 
In a few minutes, the gable end where the chimney was built, in its turn, sank into the abyss. The Seven Cardinal Sins: Envy and Indolence |Eugne Sue 
The regular form of the buildings was rectangular, the gable sides probably being shorter than the others. Encyclopaedia Britannica, 11th Edition, Volume 4, Part 3 |Various