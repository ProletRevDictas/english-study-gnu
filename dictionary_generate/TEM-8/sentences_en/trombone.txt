Oboes, trombones and especially trumpets were all higher risk, spreading more aerosols than a person speaking. What science tells us about reducing coronavirus spread from wind instruments |Betsy Ladyzhets |August 6, 2021 |Science News 
As a teen, she studied trombone and later started a local swing band. Ethel Gabriel, trailblazing producer and executive at RCA Records, dies at 99 |Terence McArdle |April 12, 2021 |Washington Post 
There was never any one criterion for how every trombone or tenor saxophone or singer should sound. The Stacks: John Coltrane’s Mighty Musical Quest |Nat Hentoff |October 18, 2014 |DAILY BEAST 
Instead of spoofing it, Farmer Derek plays it on trombone in an open field. Viral Video of the Day: Farmer Summons Cattle With ‘Royals’ Cover |Alex Chancey |August 4, 2014 |DAILY BEAST 
While his trombone skills are decent, he certainly draws a crowd—or rather, a herd. Viral Video of the Day: Farmer Summons Cattle With ‘Royals’ Cover |Alex Chancey |August 4, 2014 |DAILY BEAST 
All you had to do was board with your submachine gun in a trombone case, as Martin McNally did at St. Louis airport in 1972. When Hijackers Ruled the American Skies |Emma Garman |June 20, 2013 |DAILY BEAST 
Nino opens a trunk and begins extracting props—balloons, a cane, and a battered old trombone. Giovanni Zoppé’s Real-Life Family Circus |Malcolm Jones |October 21, 2012 |DAILY BEAST 
At the present time he was trombone in the “Tournée Gulland,” a touring opera company. The Joyous Adventures of Aristide Pujol |William J. Locke 
A unique novelty was the Contra Trombone on the Pedal of 64 feet actual length. The Recent Revolution in Organ Building |George Laing Miller 
Within Average Jones' overstocked mind something stirred at the repetition of the words "B-flat trombone." Average Jones |Samuel Hopkins Adams 
"I can play the B-flat trombone louder as any man in the business," asserted Schlichting with proud conviction. Average Jones |Samuel Hopkins Adams 
I came back––in tights, playing a big trombone, prancing round and making an awful noise. The Cup of Fury |Rupert Hughes