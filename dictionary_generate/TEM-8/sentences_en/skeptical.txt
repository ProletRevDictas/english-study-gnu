Outsiders were skeptical, unsure how the dance would translate to a competitive format and how it would engage spectators. How break dancing made the leap from ’80s pop culture to the Olympic stage |Rick Maese |February 9, 2021 |Washington Post 
I think that I was skeptical about Liz at first, because I had not heard of her. 'We All Have to Create Our Own Universe.' Producer and Artist Zackary Drucker on Telling Nuanced Trans Stories |Suyin Haynes |February 8, 2021 |Time 
I’m skeptical I could do this without exploding the egg everywhere but, hey, you never know. How to separate eggs without the stress or mess |Becky Krystal |February 8, 2021 |Washington Post 
If you’re still skeptical, we’ve outlined a few health benefits to your new favorite green drink. The Benefits Of Drinking Celery Juice |Charli Penn |February 2, 2021 |Essence.com 
Not any thicker than normal unlined denim, I was skeptical that these pants would provide the additional warmth I was looking for. The Best Fleece-Lined Pants for the Outdoors |Wes Siler |January 15, 2021 |Outside Online 
As Ackerman pointed out, all scientists are skeptics; skeptical doubt is a part of the scientific method. Extreme Weather? Blame the End Times |Jay Michaelson |November 28, 2014 |DAILY BEAST 
I have to admit, I was skeptical about True Detective from the start. Alright ‘True Detective,’ You Got Me: Taylor Kitsch Is a Woman’s Man |Teo Bugbee |November 1, 2014 |DAILY BEAST 
Perhaps we should have been a little more skeptical about this story. Brace Yourself: October Election Surprises Surely on the Way |Matt Lewis |October 31, 2014 |DAILY BEAST 
For days the Kurds have insisted to skeptical journalists, including myself, that Kobani would hold. In the Battle for Kobani, ISIS Falls Back. But for How Long? |Jamie Dettmer |October 20, 2014 |DAILY BEAST 
Postol and Lloyd were skeptical; hexamine is a common precursor chemical for military-grade explosives. The Kardashian Look-Alike Trolling for Assad |Noah Shachtman, Michael Kennedy |October 17, 2014 |DAILY BEAST 
The other chambers, having fireplaces, I decided needed no further heat, though the plumber was mournfully skeptical. The Idyl of Twin Fires |Walter Prichard Eaton 
I have always been skeptical about such things as "soul," but when I look into the mirror—God help me! The Diary of Philip Westerly |Paul Compton 
Felipe can establish his relationship beyond the doubt of the most skeptical. Frank Merriwell's Pursuit |Burt L. Standish 
He was skeptical about Jim, but he was struck with the accuracy of the portrait of Edwards. The Calico Cat |Charles Miner Thompson 
Odin had grown skeptical of such thinking when he was a medical student. Hunters Out of Space |Joseph Everidge Kelleam