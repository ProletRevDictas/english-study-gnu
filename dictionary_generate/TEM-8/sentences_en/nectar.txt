To attract vital pollinators, males ooze nectar among the nubbins. ‘Vampire’ parasite challenges the definition of a plant |Susan Milius |September 16, 2020 |Science News For Students 
Sometimes these and other bumblebees don’t grope for nectar through the natural openings of flowers. Bumblebees may bite leaves to spur plant blooming |Susan Milius |July 2, 2020 |Science News For Students 
These animals probably don’t binge on sugary fruit and nectar that creates ethanol. Why elephants and armadillos might easily get drunk |Susan Milius |June 4, 2020 |Science News For Students 
Like honeybee workers, bumblebee workers collect pollen and nectar. Pesticides can have long-term impact on bumblebee learning |Alison Pearce Stevens |May 18, 2020 |Science News For Students 
To get enough energy, Minecraft bees would need a lot of nectar, Combes says. Minecraft’s big bees don’t exist, but giant insects once did |Carolyn Wilke |May 14, 2020 |Science News For Students 
Legend has it that Alexander the Great enjoyed ancient sno-cones as well; his were flavored with honey and nectar. An Investigation Into the Delicious Origins of Ice Cream |Andrew Romano |July 13, 2014 |DAILY BEAST 
It gives the experience of the sweetest nectar of life, pure bliss consciousness. David Lynch Discusses Transcendental Meditation in Los Angeles |Sean Macaulay |April 7, 2013 |DAILY BEAST 
Another choice is agave nectar, made from a type of cactus that grows in Mexico (yes, tequila fans, that cactus). How to Watch Out for Hidden Sugar and Replace With Leaner Substitutes |Diana Le Dean |February 23, 2013 |DAILY BEAST 
Then, as if succumbing to the charms of its nectar, the novel becomes more extravagant as it progresses. 3 Must-Read Novels |The Daily Beast |April 11, 2011 |DAILY BEAST 
Honey, which is produced by honeybees harvesting nectar from flowers, does a lot to encourage sexuality. Five Aphrodisiac Foods |Sarah Whitman-Salkin |February 9, 2010 |DAILY BEAST 
At least ten men be sides Gwynne were hovering about Dolly Boutts, like humming-birds about the nectar of a full-blown rose. Ancestors |Gertrude Atherton 
The nectar of the gods pales into nothingness when compared with a toddy such as I make, said he. The Fifth String |John Philip Sousa 
The gods themselves were fed on nectar and ambrosia, that they might not die like ordinary mortals. Beacon Lights of History, Volume I |John Lord 
Mary's eyes were fastened on the silver cups; were they brimmed with nectar of the old Greek gods that they should charm her so? God Wills It! |William Stearns Davis 
Thatcher has filled me amply with expensive urban food in this sylvan retreat—nectar and ambrosia. A Hoosier Chronicle |Meredith Nicholson