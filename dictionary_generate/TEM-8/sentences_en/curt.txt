The man who called me, a long-retired Chicago police officer, was alternately charming and curt. The Murder Chicago Didn’t Want to Solve |by Mick Dumke |February 25, 2021 |ProPublica 
Former Red Sox star Curt Schilling says his politics are keeping him out of Cooperstown. Conservative Curt Says His Politics, Not His Pitching, Kept Him Out of the Hall of Fame |Ben Jacobs |January 9, 2015 |DAILY BEAST 
Sen. Bernie Sanders, a darling of the left who identifies as a democratic socialist, was curt. Even Left-Wing Politicians Can’t Quit Israel |Tim Mak |July 30, 2014 |DAILY BEAST 
The senator also is curt on the subject of the D.C. political magazine that scores the votes of every member of Congress. Can This Ornery Socialist Spoil the Clinton Coronation? |David Freedlander |July 2, 2014 |DAILY BEAST 
Wilson, who spoke to The Daily Beast from a cab on his way back up to Harlem, is curt and cold in conversation. Bad to the Drone: Amateur Flyer Appears at Harlem Wreckage |Abby Haglage |March 13, 2014 |DAILY BEAST 
Curt Smith of Tears for Fears played himself in the fifth season episode “Shawn 2.0.” Psych’s 13 Best Musical Moments |Chancellor Agard |December 15, 2013 |DAILY BEAST 
Curt as is the cable it has yet scope to show up a little more of our great K.'s outfit. Gallipoli Diary, Volume I |Ian Hamilton 
It seemed to her that she had lived for a century since the few hours before when Madame Malmaison had given her a curt dismissal. The Weight of the Crown |Fred M. White 
"That will do," said Halyard, ungraciously, which curt phrase was apparently the usual dismissal for the nurse. In Search of the Unknown |Robert W. Chambers 
Had it not been for occasional curt, illuminative questions, Peter Gross might have thought him asleep. The Argus Pheasant |John Charles Beecham 
There came the hard, curt questions and the command: "Outside—hurry!" Space Prison |Tom Godwin