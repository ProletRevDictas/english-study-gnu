Brienne of Tarth is back on the road—a far more worldly and intimidating figure than her hapless male squire, Podrick Payne. Game of Thrones’ Ep. 5 'First of His Name' Recap: An Ode to the Women of Westeros |Andrew Romano |May 5, 2014 |DAILY BEAST 
Seemingly sad beyond consolation, the widow begs the squire to finish her off the same way. Read This and Blush: Naughty Medieval French Tales |Yunte Huang |June 13, 2013 |DAILY BEAST 
Another California undergrad, Andrew Squire, has taken a different approach to selling pot to raise cash. The Tuition Black Market |Kathleen Kingsbury |August 12, 2009 |DAILY BEAST 
Samuel Squire, bishop of St. David's died; a poetical, historical and antiquarian writer of note. The Every Day Book of History and Chronology |Joel Munsell 
"I fear, squire, hers is not a bodily affection," said Mrs. Dodd with a deep sigh. The Pit Town Coronet, Volume I (of 3) |Charles James Wills 
Not one word did Mrs. Dodd utter for many days to her husband of her momentous conversation with the squire. The Pit Town Coronet, Volume I (of 3) |Charles James Wills 
A country squire introduced his baboon, in clerical habits, to say grace. The Book of Anecdotes and Budget of Fun; |Various 
Sir Herbert's squire, Thomas du Bois, joined in his master's confident wager. King Robert the Bruce |A. F. Murison