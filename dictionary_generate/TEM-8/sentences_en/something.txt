Something like fluoride, which is too small for normal filters, yanks away that feeling of agency. Anti-Fluoriders Are The OG Anti-Vaxxers |Michael Schulson |July 27, 2016 |DAILY BEAST 
Citizens, perhaps, need to feel like they can communicate something to science. Anti-Fluoriders Are The OG Anti-Vaxxers |Michael Schulson |July 27, 2016 |DAILY BEAST 
Why would “they” want to crush him just for attempting to buy something twenty years ago? Phylicia Rashad and the Cult of Cosby Truthers |Stereo Williams |January 8, 2015 |DAILY BEAST 
But I think Steve Austin has to team up with a Japanese holdout to stop a nuclear bomb from going off or something. ‘Archer’ Creator Adam Reed Spills Season 6 Secrets, From Surreal Plotlines to Life Post-ISIS |Marlow Stern |January 8, 2015 |DAILY BEAST 
It was something ineffable and harder to define: freedom of speech. Politicians Only Love Journalists When They're Dead |Luke O’Neil |January 8, 2015 |DAILY BEAST 
He remembered something—the cherished pose of being a man plunged fathoms-deep in business. St. Martin's Summer |Rafael Sabatini 
There seems something in that also which I could spare only very reluctantly from a new Bible in the world. The Salvaging Of Civilisation |H. G. (Herbert George) Wells 
There is, perhaps, in this childish suffering often something more than the sense of being homeless and outcast. Children's Ways |James Sully 
The beauty, the mystery,—this fierce sunshine or something—stir——' She hesitated for a fraction of a second. The Wave |Algernon Blackwood 
And furthermore, I imagine something else about this—quite unlike the old Bible—I imagine all of it periodically revised. The Salvaging Of Civilisation |H. G. (Herbert George) Wells