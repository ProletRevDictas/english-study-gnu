And lest you be deceived, primary elections are no partisan monopoly. Reality Check: There Are No Swing Voters |Goldie Taylor |November 13, 2014 |DAILY BEAST 
The cops suspended the high-speed pursuit lest some innocent be killed. Did the Amber Lynn Coplin Murder Photos Sicken the Creeps of 4Chan? |Michael Daly |November 6, 2014 |DAILY BEAST 
He said it would be wise to stop taking it, lest it turn on my “genetic light switch.” Birth Control Made My Hair Fall Out, and I’m Not the Only One |Molly Oswaks |October 14, 2014 |DAILY BEAST 
Lest anyone be confused, the site clearly states:  “This is not a dating app.” Swipe Right For Sex: Mixxxer Is Tinder for the Porn Star Set |Aurora Snow |October 4, 2014 |DAILY BEAST 
Lest anyone be confused, the site clearly states: ‘This is not a dating app.’ Swipe Right For Sex: Mixxxer Is Tinder for the Porn Star Set |Aurora Snow |October 4, 2014 |DAILY BEAST 
There is always in the background of my mind dread lest help should reach the enemy before we have done with Sedd-el-Bahr. Gallipoli Diary, Volume I |Ian Hamilton 
Terror and fascination caught him; he turned away lest she should reach his secret and communicate her own. The Wave |Algernon Blackwood 
He saw them standing at street corners, watchfully staring lest they should miss the form of joy. Bella Donna |Robert Hichens 
This new-found joy I long pursued in secret, afraid lest it should be discovered and despised as a folly. Fifty Years of Railway Life in England, Scotland and Ireland |Joseph Tatlow 
Underneath these were affectionate hearts quaking with fear lest the home-coming be but a sad one after all. Ramona |Helen Hunt Jackson