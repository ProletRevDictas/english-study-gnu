The go-for-broke bidding underscores how crucial these midband frequencies are to companies trying to seize global leadership in emerging 5G technology. 5G airwave bids soar past $76 billion to set auction record |Verne Kopytoff |January 5, 2021 |Fortune 
See, no one has ever gone broke selling stuff to women who have been convinced that there’s something wrong with them. The Outré Art of Pegging |Eugene Robinson |December 14, 2020 |Ozy 
Then he moved to New York, worked as a business analyst, went broke. Football is back in Happy Valley. The coronavirus never left. |Kent Babb |October 30, 2020 |Washington Post 
You could lose everything, including that solid business, and end up broke and living in a culvert beneath a freeway. When Cuckolding’s Gone Crazy |Eugene Robinson |October 26, 2020 |Ozy 
Irma, after all, broke weather records for how powerful and sustained it was. Political rhetoric may impact your hurricane preparedness |Ula Chrobak |September 11, 2020 |Popular-Science 
The gunman hardly broke stride as he nonetheless shot Merabet in the head, killing him. The Muslim Cop Killed by Terrorists |Michael Daly |January 9, 2015 |DAILY BEAST 
“We broke off shortly after because we were more ambitious,” says Lean. The Cult of Yung Lean: ‘I’m Building An Anarchistic Society From the Ground Up’ |Marlow Stern |January 4, 2015 |DAILY BEAST 
Riots broke out in 1994, after Iranian authorities replaced a Sunni mosque in Mashad with a development project. The Dangerous Drug-Funded Secret War Between Iran and Pakistan |Umar Farooq |December 29, 2014 |DAILY BEAST 
The two sides taunted and insulted each other but with police separating them no violence broke out. NYC’s Garner Protesters vs. Pro-Cop Protesters |Jacob Siegel |December 20, 2014 |DAILY BEAST 
In October, news broke that Regal hired Morgan Stanley to explore a possible sale. The Right-Wing Billionaire Who Bowed to North Korea over ‘The Interview’ |Asawin Suebsaeng |December 19, 2014 |DAILY BEAST 
The volcanic eruptions of the mountains on the west broke down its barriers, and let its waters flow. Blackwood's Edinburgh Magazine, Volume 60, No. 372, October 1846 |Various 
The great plague of this and the subsequent year broke out at St. Giles, London. The Every Day Book of History and Chronology |Joel Munsell 
The Senora Moreno's heart broke within her, when those words passed her lips to her adored Felipe. Ramona |Helen Hunt Jackson 
A sob rose in her throat, and broke from her lips transformed into a trembling, sharp, glad cry. The Bondboy |George W. (George Washington) Ogden 
Walls End Castle, when the party broke up, returned to its normal state. The Pit Town Coronet, Volume I (of 3) |Charles James Wills