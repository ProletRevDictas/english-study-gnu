His thankfully negative result took 48 hours to come in, resulting in two days of missed camp and the all too familiar work-parenting juggle. 6 answers to parents’ COVID-19 questions as kids return to school |Sujata Gupta |August 11, 2021 |Science News 
He hopes to go to Stanford and major in computer science, but for now he has to juggle homework with his online vigilante persona. Meet Graham Smith, SnapChat’s 16-Year-Old Nemesis |Nina Strochlic |January 29, 2014 |DAILY BEAST 
Even if you think they're evil enough to juggle the jobs figures, it's probably too late to do the president any good. Why Would the BLS Bother to Cook the Books? |Megan McArdle |October 5, 2012 |DAILY BEAST 
Well, a bear can juggle and stand on a ball and he's talented, but he's not famous. Oscar de la Renta's Feud with Cathy Horyn, The Man Repeller's Empire Expands |The Daily Beast |September 17, 2012 |DAILY BEAST 
When they did, the guys suddenly had two sets of girls at the house to juggle, and neither pair knew about the other. ‘Jersey Shore’ Canceled: 11 Wildest Moments (VIDEO) |Kevin Fallon |August 31, 2012 |DAILY BEAST 
He managed to juggle a challenging workload and foster good relations among officials at various agencies. Is Gates’ Success Overstated? |Tara McKelvey |June 21, 2011 |DAILY BEAST 
A frind iv mine wanst got full iv kerosene an' attempted to juggle a polisman. Mr. Dooley Says |Finley Dunne 
Through her tears his meagre face showed as a seraph's who spoke the truth and forbade her to juggle with her soul. The Longest Journey |E. M. Forster 
He was all in white, his waistcoat had long sleeves, and every minute it seemed as if he must begin to juggle with glasses. The Life of James McNeill Whistler |Elizabeth Robins Pennell 
He couldn't even juggle one foot backward and forward without correction. The Wrong Twin |Harry Leon Wilson 
My thoughts merely revel and juggle with them, picture and legend—they are pastimes of my child-self. I, Mary MacLane |Mary MacLane