“I do not support gay marriages being recognized in Florida,” he wrote Andrew Walther of Sanford. Jeb Bush’s Unseen Anti-Gay Marriage Emails |Jackie Kucinich |January 9, 2015 |DAILY BEAST 
That man was Xavier Cortada, a gay man who wrote of his frustration that he and his partner of eight years were unable to marry. Jeb Bush’s Unseen Anti-Gay Marriage Emails |Jackie Kucinich |January 9, 2015 |DAILY BEAST 
Some gay apps, like the newer Mister, have not subscribed to the community/tribe model. Grindr’s Trans Dating Problem |David Levesley |January 9, 2015 |DAILY BEAST 
Meanwhile, in Florida, Bush was flooded with questions about whether gay marriage could possibly come to the Sunshine State. Jeb Bush’s Unseen Anti-Gay Marriage Emails |Jackie Kucinich |January 9, 2015 |DAILY BEAST 
In the 70s, this myth kept openly gay people out of teaching positions. Freaking Out About Age Gaps in Gay Relationships Is Homophobic |Samantha Allen |January 9, 2015 |DAILY BEAST 
Am I not in France—gay, delightful France—partaking of the kindness and civility of the country? Blackwood's Edinburgh Magazine, Volume 60, No. 372, October 1846 |Various 
After a moment's silence, the cavaliers both burst into a gay laugh. Blackwood's Edinburgh Magazine, Volume 60, No. 372, October 1846 |Various 
Never had Tom seen his gay and careless cousin in such guise: he was restless, silent, intense and inarticulate. The Wave |Algernon Blackwood 
If it had not been for the presence of Mademoiselle Stéphanie, it would not have been gay for Aristide. The Joyous Adventures of Aristide Pujol |William J. Locke 
The box of the diplomatic corps was just opposite us, and our gay little Mrs. F. sat in it dressed in white satin. Music-Study in Germany |Amy Fay