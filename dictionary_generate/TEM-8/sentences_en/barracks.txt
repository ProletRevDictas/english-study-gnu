Once a cadet dropped a brick from a third-story barracks window that barely missed Jackson. Stonewall Jackson, VMI’s Most Embattled Professor |S. C. Gwynne |November 29, 2014 |DAILY BEAST 
The abandoned barracks of the Liberian Army lay just beyond in the tropical thicket. The Life of a Liberian Child with Ebola |Sarah Crowe |November 5, 2014 |DAILY BEAST 
With lights flashing, the cruiser arrived at the Blooming Grove State Police barracks in Pike County. Killer Eric Frein Held in Murdered Cop’s Cuffs |Michael Daly |October 31, 2014 |DAILY BEAST 
Frein was lodged in a holding cell at Blooming Grove barracks. Killer Eric Frein Held in Murdered Cop’s Cuffs |Michael Daly |October 31, 2014 |DAILY BEAST 
Turkish media reported the airstrikes came after Kurdish separatists attacked a military barracks nearby. Whose Side Is Turkey On? |Jamie Dettmer |October 15, 2014 |DAILY BEAST 
On the night of June the 11th a red-hot cannon-ball set fire to one of the barracks which was used as a hospital. The Red Year |Louis Tracy 
From the near-by barracks troopers craned through windows, and gathered in doorways. Raw Gold |Bertrand W. Sinclair 
It is late, but MacRae knows these barracks, and doubtless he can find you a temporary sleeping place. Raw Gold |Bertrand W. Sinclair 
A troublesome intruding fellow, why can't he keep his ailments to his own barracks? Elster's Folly |Mrs. Henry Wood 
Something was done towards improving the condition of the barracks, hospitals, and schools. Journal of a Voyage to Brazil |Maria Graham