My jacket is from Old Navy, and sweaters from a motley of stores I don’t care about. Opinion | Being Isabel |Isaac Amend |June 14, 2021 |Washington Blade 
Klagsbrun’s assemblages are motley and ramshackle, and yet vital. In the galleries: A heightened homage to trees and what they can teach us |Mark Jenkins |May 7, 2021 |Washington Post 
The Post led the charge into federal court, welcoming a motley crew. To Hold the Government Accountable, We Need to Know What It’s Doing. That’s Why We’re Tracking PPP Data. |by Jeremy Kutner |March 12, 2021 |ProPublica 
Now, it’s all up to a motley crew of players to plan out how to actually get these things to people like doctors and nurses in the safest way possible. How a molecule gets to the masses |Sy Mukherjee |December 17, 2020 |Fortune 
Ultimately conquering the coronavirus pandemic will likely require a motley crew of vaccines which use different technologies. The science behind the leading COVID vaccines will lead to faster manufacturing |Sy Mukherjee |December 5, 2020 |Fortune 
A motley crew of former sailors led by Commodore Joshua Barney mounted the only real resistance to the British. The Presidential Hopeful Obsessed With the War of 1812 |Ben Jacobs |September 9, 2014 |DAILY BEAST 
Well, it was based on an amalgam of bands—Black Sabbath, AC/DC, Motley Crue, Judas Priest, and Van Halen. Rob Reiner on the State of Romcoms, ‘The Princess Bride’s’ Alternate Ending, and the Red Viper |Marlow Stern |July 27, 2014 |DAILY BEAST 
A motley crew, to be sure: Islamic states, sub-Saharan Africa, China. At the United Nations, It’s Human Rights, Putin-Style |Jay Michaelson |June 26, 2014 |DAILY BEAST 
The motley crew cast participates in an illegal cross-country car race, while doing anything to win. 10 Greatest Road Trip Movies |Marina Watts |May 21, 2014 |DAILY BEAST 
Vampire Weekend, “Giving up the Gun” The ultimate motley crew: Jake Gyllenhaal, Joe Jonas, Daft Punk, Lil Jon and RZA. Andrew Garfield in ‘We Exist’ and More Celebrities in Music Videos |Marina Watts |May 18, 2014 |DAILY BEAST 
The proclamation was posted on the door of the court house and soon a motley crowd gathered around to read it. The Courier of the Ozarks |Byron A. Dunn 
Seldom has war brought together such a motley assemblage of races as gathered on the Ridge during the siege of Delhi. The Red Year |Louis Tracy 
These little shops, which display at regular intervals their motley assortment of wares, fill me with delight. Marguerite |Anatole France 
It was a motley army, without uniforms, without banners and many without arms. The Courier of the Ozarks |Byron A. Dunn 
While waiting for the performance to begin, the motley troop encamps on the grass near the theatre. The Life &amp; Letters of Peter Ilich Tchaikovsky |Modeste Tchaikovsky