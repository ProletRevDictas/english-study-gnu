Possession of the books was the only recompense, though for all other work payment in money was made. Fifty Years of Railway Life in England, Scotland and Ireland |Joseph Tatlow 
His immediate recompense was the post of general of brigade in the Army of the Eastern Pyrenees. Napoleon's Marshals |R. P. Dunn-Pattison 
Thou shalt render them a recompense, O Lord, according to the works of their hands. The Bible, Douay-Rheims Version |Various 
Behold, I will raise them up out of the place wherein you have sold them: and I will return your recompense upon your own heads. The Bible, Douay-Rheims Version |Various 
Her descendants have been exempted from the taille (poll tax)—a mean and shameful recompense! A Philosophical Dictionary, Volume 1 (of 10) |Franois-Marie Arouet (AKA Voltaire)