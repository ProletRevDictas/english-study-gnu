He sets out, and on his journey finds a lion, a wolf, and a falcon disputing over the carcase of a horse. The Three Days' Tournament |Jessie L. Weston 
As for Homer Smith, his carcase might rot in the desert of Arizona, or anywhere, for aught he cared. The Cromptons |Mary J. Holmes 
Is the carcase of a bird really a more scientific thing than its lustrous, colored plumage and its song with its subtle tones? Urania |Camille Flammarion 
“Where the carcase is, there the eagles are gathered together,” said cook, solemnly. The Dark House |Georg Manville Fenn 
Everybody has heard of his rude taunt thereupon at Elizabeth, that 'her conditions were as crooked as her carcase.' Sir Walter Ralegh |William Stebbing