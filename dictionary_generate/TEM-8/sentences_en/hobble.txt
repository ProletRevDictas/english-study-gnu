Reduced global availability could send prices for these nutrients higher and financially hobble farm belts across a slew of countries. How Sanctions on Russia Will Hurt—and Help—the World's Economies |Simon Constable |March 7, 2022 |Time 
This, more than any one scandal, is likely to hobble the party for the next few election cycles. Paging Rose Mary Woods: Obama’s Unbelievable Missing IRS Emails |James Poulos |June 18, 2014 |DAILY BEAST 
A few days before, she had managed to stand and hobble around the ward. Surviving Syria’s Incendiary Bomb Attacks |Paul Adrian Raymond |December 11, 2013 |DAILY BEAST 
Hardly able to hobble into the room on his bruised and engorged feet, he sported black eyes. Despite Pledge, Syrian Rebels Continue to Torture |Jamie Dettmer |August 15, 2012 |DAILY BEAST 
Just the distraction that this kind of case creates can hobble even the most successful, well-run company. Antitrust Suit Could Bring Down Google |Dan Lyons |April 27, 2012 |DAILY BEAST 
When the flames began to bite on one side he could hobble around the post to the opposite side. A Virginia Scout |Hugh Pendexter 
A little longer let me live, I pray—A little longer hobble round thy door. Stories about Animals: with Pictures to Match |Francis C. Woodworth 
You get us into a precious hobble through sheer wanton foolery, and then you expect me to like it. Jack Harkaway's Boy Tinker Among The Turks |Bracebridge Hemyng 
You know about as much of a motor boat as a pig knows of the hobble skirt. Boy Scouts in the Philippines |G. Harvey Ralphson 
Our people when they are in a hobble always like to employ him, though he is somewhat dear. The Romany Rye |George Borrow