For three decades, ‘Puck’ waged war on all things holy—politicians, social mores, and the news. The Magazine That Made—and Unmade—Politicians |Anthony Haden-Guest |November 2, 2014 |DAILY BEAST 
Women have long expressed their sexuality—and the mores of the time—through their choice of undergarments. What Lies Beneath: How Lingerie Got Sexy |Raquel Laneri |June 5, 2014 |DAILY BEAST 
Cultural conservatives will put up with a certain amount of pandering to more modern mores with a nudge and a wink. The GOP and the Young |Michael Tomasky |June 3, 2013 |DAILY BEAST 
I think it was more about sexual mores, mating and dating rituals in the city, cultural anthropology. Candace Bushnell Defends ‘The Carrie Diaries’ |Ramin Setoodeh |March 25, 2013 |DAILY BEAST 
The change is not in the mores of France, but in its geopolitical and economic history. Dominique Strauss-Kahn Settles With Maid: How the Case Changed France |Christopher Dickey |December 11, 2012 |DAILY BEAST 
Quid sit Nova Francia, qualis regio, qui in e populi, quique mores. The Jesuit Relations and Allied Documents, Vol. II: Acadia, 1612-1614 |Various 
Increpaui ego, vt potui, per interpret paganicos hos mores in iam Christianis. The Jesuit Relations and Allied Documents, Vol. II: Acadia, 1612-1614 |Various 
It includes accommodation to the folkways, the mores, the conventions, and the social ritual (Sittlichkeit). Introduction to the Science of Sociology |Robert E. Park 
Professional faults arouse a much feebler response than offenses against the mores of the larger society. Introduction to the Science of Sociology |Robert E. Park 
The mores, as thus conceived, are the judgments of public opinion in regard to issues that have been settled and forgotten. Introduction to the Science of Sociology |Robert E. Park