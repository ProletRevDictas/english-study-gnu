The big headline Wednesday was that top Department of Health and Human Services spokesman Michael Caputo is taking 60 days of medical leave. The Trump administration’s politicization of coronavirus comes to a head |Aaron Blake |September 16, 2020 |Washington Post 
Residents have been advised to stay indoors, particularly those with medical conditions. California wildfires fuel a Northwest nightmare |Axios |September 11, 2020 |Axios 
She said she wasn’t surprised the number of children hospitalized beyond medical necessity hadn’t gone down in the last two years. Hundreds of Children Are Stuck in Psychiatric Hospitals Each Year Despite the State’s Promises to Find Them Homes |by Duaa Eldeib |September 11, 2020 |ProPublica 
Arthrex and Stryker routinely compete in the field of medical devices. How the Best Workplaces in Manufacturing have risen to the COVID-19 challenge |lbelanger225 |September 10, 2020 |Fortune 
That again is the league’s chief medical officer, Allen Sills. Why Can’t Schools Get What the N.F.L. Has? (Ep. 431) |Stephen J. Dubner |September 10, 2020 |Freakonomics 
The court ruled she lacked the maturity to make her own medical decisions. Should Teens Have The Right To Die? |Brandy Zadrozny |January 8, 2015 |DAILY BEAST 
Doubling down on Schedule I is, at best, a deranged way to push Americans away from “medical,” and toward recreational, use. Obama’s Pot Policy Is Refer Madness |James Poulos |January 5, 2015 |DAILY BEAST 
But medical experts say being able to take advantage of American health care almost certainly prolonged his life. Final Chapter for Accused Africa Bomber |Jamie Dettmer |January 4, 2015 |DAILY BEAST 
But the medical examiner reported that Brinsley had eaten nothing at all. Exclusive: Inside a Cop-Killer’s Final Hours |Michael Daly |December 31, 2014 |DAILY BEAST 
Houston has the largest medical center in the world, and the largest export port in the entire country. Will Texas Stay Texan? |David Fontana |December 29, 2014 |DAILY BEAST 
William Woodville died; a distinguished English physician and medical writer. The Every Day Book of History and Chronology |Joel Munsell 
William Hewson died; an eminent English anatomist, and medical author. The Every Day Book of History and Chronology |Joel Munsell 
Christopher Bennet died; a distinguished London physician, and writer on medical subjects. The Every Day Book of History and Chronology |Joel Munsell 
He wrote on law, medical jurisprudence and political economy, and translated Justinian and Broussais. The Every Day Book of History and Chronology |Joel Munsell 
William P. Dewees, a distinguished medical writer, died at Philadelphia. The Every Day Book of History and Chronology |Joel Munsell