After they disappeared back into the woods, I put my mask in my pocket, where it stayed for the rest of the hike. Mushroom Hunting at the End of the World |Danny Palumbo |November 12, 2020 |Eater 
That would likely mean a tax hike is a non-starter as would be a generous stimulus package. The vaccine jolt fades, sinking global stocks—except for this one surprising sector |Bernhard Warner |November 12, 2020 |Fortune 
My wife and I are about an hour into a hike up Atalaya Peak, a 9,000-foot mountain overlooking our hometown of Santa Fe. A Parent's Guide to Hiking with a Chatterbox |Ryan Van Bibber |November 7, 2020 |Outside Online 
Just make sure to arrive early, as this hike is a favorite among locals and the parking lot off California Highway 88 fills up fast. The Best Day Hikes on the Pacific Crest Trail |Maggy Lehmicke |November 4, 2020 |Outside Online 
We were younger then, yes, but more caught up in raising children and handling busy jobs than in training for a hike. Autumn in the Adirondacks is a mountain do |Debra Bruno |October 30, 2020 |Washington Post 
He was a quiet kid, a hiker and hunter, smart enough to graduate at the head of Harrison High. The Ballad of Johnny France |Richard Ben Cramer |January 12, 2014 |DAILY BEAST 
As a hiker, I thought Strayed was a knucklehead—but a likable one. Book Bag: Timothy Egan’s Five Favorite Travel Books |Timothy Egan |October 23, 2012 |DAILY BEAST 
A hiker told a French newspaper how he came across the bodies of four people gunned down in a forest car park in the French Alps. Hiker Recounts Grisly Murder Scene in French Alps |The Telegraph |September 12, 2012 |DAILY BEAST 
It took about 20 minutes to convince the emaciated hiker to let them carry him to the helicopter. Ray Gardner Used Autism Training To Find William LaFever |Laura Colarusso |July 14, 2012 |DAILY BEAST 
At first, they thought it was another hiker just saying hello, but as they got closer, they could see it was LaFever. Ray Gardner Used Autism Training To Find William LaFever |Laura Colarusso |July 14, 2012 |DAILY BEAST 
He was making good time, but Captain DeCastros proceeded to demonstrate that he was no mean hiker, himself. The Marooner |Charles A. Stearns 
Forbes, from the experience of a campaigner, a wilderness hiker, lifted an eyebrow of patronizing incredulity. What Will People Say? |Rupert Hughes 
At each ranger station, hotel, chalet, and permanent camp in the park will be found a "Hiker's Register" book. Glacier National Park [Montana] |United States Dept. of the Interior 
This will prove, upon investigation, to be a hiker, or maybe two or more. Legends of the Skyline Drive and the Great Valley of Virginia |Carrie Hunter Willis 
There followed something about being held up by a hitch-hiker. The Blind Spot |Austin Hall