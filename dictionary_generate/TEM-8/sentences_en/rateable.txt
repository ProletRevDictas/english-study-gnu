They could indulge this feeling by putting a very high rateable value on their property. Speculations from Political Economy |C. B. Clarke 
If A preferred raising his rateable value to 1600, B would get the fine, Government would get the increased rate. Speculations from Political Economy |C. B. Clarke 
The people should pay a rateable tax for their sheep, and an Excise for every thing which they should eat. The Olden Time Series, Vol. 6: Literary Curiosities |Henry M. Brooks 
We fear therefore that here again we can not penetrate through the rateable to the real. Domesday Book and Beyond |Frederic William Maitland 
The question whether the acreage stated in the Suffolk survey is real or rateable can not be briefly debated. Domesday Book and Beyond |Frederic William Maitland