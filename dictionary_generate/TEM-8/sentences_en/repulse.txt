How did warring, factionalized city-states on the edge of the known world repulse the first superpower? Persian Fire and Rubicon (Full) |David Frum |September 23, 2012 |DAILY BEAST 
And if I were Christie, a song like "41 Shots" would repulse me. Conservatives and Rock and Roll |Michael Tomasky |June 22, 2012 |DAILY BEAST 
The Repulse Isles are of small size; they are surrounded by rocks, which do not extend more than a quarter of a mile from them. Narrative of a Survey of the Intertropical and Western Coasts of Australia] [Volume 2 of 2] |Phillip Parker King 
Gwynne was not in the humor to repulse anybody, and assured her that she really made him feel that he had returned to his home. Ancestors |Gertrude Atherton 
The three men leaped to their feet, and seizing their arms, prepared bravely to repulse the enemies who attacked them so suddenly. The Border Rifles |Gustave Aimard 
Her repulse was a mortal offence: small minds never forget, much less pardon a rebuke to their vanity, and he inly swore revenge. Alone |Marion Harland 
In a sortie Bohemond the crafty and brave was wounded; Tancred's and Godfrey's valor ended in repulse. God Wills It! |William Stearns Davis