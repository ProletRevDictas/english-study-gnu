Many spiders avoid cannibalism by using special tactile signals that could be copied by adroit predators, thereby deceiving the arachnids, he says. Assassin bugs tap spiders to distract them before a lethal strike |Jake Buehler |October 28, 2021 |Science News 
He is adroit and dry, and balances humor with the humanity of life. Pierce Brosnan’s Life After Bond: From Action Hero to Losing His Daughter to Cancer |Tim Teeman |July 2, 2014 |DAILY BEAST 
There are many circles of influence and many adroit, if at times feuding, practitioners. James Warren: Romney Attacks on Chicago Misguided |James Warren |July 18, 2012 |DAILY BEAST 
Modern circumstances require an adroit approach to the manner in which our foreign policy is being implemented. Sen. Jim Webb: Congress Must OK Military Intervention |Sen. Jim Webb |May 18, 2012 |DAILY BEAST 
Sometimes, to be unmemorable is to be adroit; and tonight, Obama was deeply unmemorable—and very adroit. A Triumphantly Unmemorable Address |Tunku Varadarajan |January 25, 2011 |DAILY BEAST 
In Petraeus, Obama also gets an Afghan commander as adroit at handling the press as McChrystal was suicidal. Petraeus, a Master of Spin |Ellen Knickmeyer |June 23, 2010 |DAILY BEAST 
The countess-dowager was not very adroit at spelling and composition, whether French or English, as you observe. Elster's Folly |Mrs. Henry Wood 
The adroit Mohammedan knew better than to trust his sahib and himself too long on the highway. The Red Year |Louis Tracy 
One or the other was always ready with an adroit question which brought out the whole story. Motor Matt's "Century" Run |Stanley R. Matthews 
There was no applause as the little man outwitted his follower by an adroit dodge under the ticket wagon. David Lannarck, Midget |George S. Harney 
Priests the most adroit have overcharged religion with ceremonies, and practices, and mysteries. Letters To Eugenia |Paul Henri Thiry Holbach