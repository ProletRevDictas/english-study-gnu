I think a lot of it has to do with the attitude and the energy behind it and the honesty. ‘Black Dynamite’ Presents Police Brutality: The Musical |Stereo Williams |January 9, 2015 |DAILY BEAST 
Eric Garcetti succeeded Villaraigosa and has received high marks in his first year and a half on the job. The Golden State Preps for the ‘Red Wedding’ of Senate Races |David Freedlander |January 9, 2015 |DAILY BEAST 
Nor does the jet have the ability to capture high-definition video, utilize an infra-red pointer. Pentagon Misfires in Stealth Jet Scandal |Dave Majumdar |January 8, 2015 |DAILY BEAST 
Obsessive exercising and inadequate nutrition can, over time, put people at high risk for overuse injuries like stress fractures. How Skinny Is Too Skinny? Israel Bans ‘Underweight’ Models |Carrie Arnold |January 8, 2015 |DAILY BEAST 
The most recent activity had a high point of 3.6 on the Richter Scale. 26 Earthquakes Later, Fracking’s Smoking Gun Is in Texas |James Joiner |January 7, 2015 |DAILY BEAST 
Honour the physician for the need thou hast of him: for the most High hath created him. The Bible, Douay-Rheims Version |Various 
This is the first and principal point at which we can stanch the wastage of teaching energy that now goes on. The Salvaging Of Civilisation |H. G. (Herbert George) Wells 
The most High hath created medicines out of the earth, and a wise man will not abhor them. The Bible, Douay-Rheims Version |Various 
The Majesty on high has a colony and a people on earth, which otherwise is under the supremacy of the Evil One. Solomon and Solomonic Literature |Moncure Daniel Conway 
In cross-section the burrows varied from round (three inches in diameter) to oval (three inches high and four inches wide). Summer Birds From the Yucatan Peninsula |Erwin E. Klaas