Fungal keratitis, an infection of the cornea, strikes more than a million people annually, estimates suggest, and blinds around 600,000. A sailor’s story captures the impact of rising serious fungal infections |Aimee Cunningham |November 29, 2021 |Science News 
Blood was pooling beneath her cornea, forming what is known as a hyphema. Breaking Mount Everest’s Glass Ceiling |Amanda Padoan, Peter Zuckerman |March 30, 2014 |DAILY BEAST 
What is possibly lovable about the cornea—or the iris or the retina for that matter? Found: Rand Paul's Secret Papers |Kent Sepkowitz |June 21, 2010 |DAILY BEAST 
Here is the proof: even as a whippersnapper, young Rand seemed to love the cornea. Found: Rand Paul's Secret Papers |Kent Sepkowitz |June 21, 2010 |DAILY BEAST 
It is the cornea—the last redoubt of virility and independence. Found: Rand Paul's Secret Papers |Kent Sepkowitz |June 21, 2010 |DAILY BEAST 
Untouched and untouchable, not surprisingly it was the cornea that the future Dr. Paul fell in love with. Found: Rand Paul's Secret Papers |Kent Sepkowitz |June 21, 2010 |DAILY BEAST 
Anatomical changes took place in the cornea, as evidenced by a white opacity. Poisons: Their Effects and Detection |Alexander Wynter Blyth 
He was distinguished, also, as an oculist and aurist, and removed a particle of iron from the cornea by means of a magnet. An Epitome of the History of Medicine |Roswell Park 
It is no more art to use the cornea and retina for the reception of an image, than to use a lens and a piece of silvered paper. The Stones of Venice, Volume III (of 3) |John Ruskin 
Make an incision through the upper quadrant of the cornea into the anterior chamber by means of a triangular keratome. The Elements of Bacteriological Technique |John William Henry Eyre 
Without removing the needle from the cornea attach the syringe and make the injection into the anterior chamber. The Elements of Bacteriological Technique |John William Henry Eyre