For two centuries, elites across the world have undermined Haitian sovereignty and impoverished its people. Haiti Isn’t Cursed. It is Exploited. |Malaika Jabali |September 22, 2021 |Essence.com 
If we lose the genetic diversity of native plants, we’re impoverishing our landscapes and making them less resilient. Not all things green are equal |Julie Fox |April 28, 2021 |MIT Technology Review 
These twin deficits enrich our frenemies and impoverish present and future Americans. U.S. Closing Trade Deficit With Better Oil Numbers |Daniel Gross |June 5, 2013 |DAILY BEAST 
Rosner seemed to think such boycotts simply serve to impoverish our knowledge of these crucial issues. “The Invisible Men” Accused of Pinkwashing |Sigal Samuel |November 12, 2012 |DAILY BEAST 
Their basic claim is that if government spends more now, deficits will rise, and that will impoverish our grandchildren. Forget Stimulus, Let’s Talk Savings |Robert H. Frank |September 8, 2011 |DAILY BEAST 
In short, deficits undertaken to finance productive investment not only do not impoverish our grandchildren, they enrich them. Let's Start Spending |Robert H. Frank |July 20, 2010 |DAILY BEAST 
Giving does not impoverish either her ample purse or her generous heart. Ernest Linwood |Caroline Lee Hentz 
Father Castel was a madman, but a good man upon the whole; he was sorry to see me thus impoverish myself to no purpose. The Confessions of J. J. Rousseau, Complete |Jean Jacques Rousseau 
Spanish policy had devised a still more ingenious contrivance gradually to impoverish the richest families of the land. The Revolt of The Netherlands, Complete |Friedrich Schiller 
In the system we are discussing, to allow them to export crowns would be to allow them to impoverish themselves. Essays on Political Economy |Frederic Bastiat 
Sometimes nature appears to spend all her intellectual and moral wealth on the father, and almost to impoverish the sons. The Expositor's Bible: The Book of Joshua |William Garden Blaikie