There is growing evidence of how bad—and expensive—air pollution is in Lagos, Africa’s largest mega-city. Residents in Lagos are paying for the city’s worsening air pollution with their lives |Yomi Kazeem |September 25, 2020 |Quartz 
Today, shawarma is eaten in many parts of the Middle East, northern Africa, and the Mediterranean. Turn a cheap chicken dinner into a Turkish street-food getaway |SAVEUR Editors |September 25, 2020 |Popular-Science 
This includes the continents of Africa, South America and much of Asia. A dirty and growing problem: Too few toilets |Stephanie Parker |September 24, 2020 |Science News For Students 
Gellert has overseen Patagonia’s business across Europe, the Middle East, and Africa since 2014. Patagonia Just Announced a New CEO |Christopher Solomon |September 23, 2020 |Outside Online 
In January 1984, Tropical Storm Domoina cut a swath of destruction across the southern end of Africa. An Insurance Solution for the World’s Poorest |Daniel Malloy |September 21, 2020 |Ozy 
According to Pew, 14 of the 20 countries in the Middle East and North Africa have blasphemy laws. In Defense of Blasphemy |Michael Tomasky |January 9, 2015 |DAILY BEAST 
The need for an Ebola vaccine in West Africa has never been greater. The Race for the Ebola Vaccine |Abby Haglage |January 7, 2015 |DAILY BEAST 
With Ebola still raging in West Africa, the race to find a vaccine is heating up. The Race for the Ebola Vaccine |Abby Haglage |January 7, 2015 |DAILY BEAST 
While the world fixated on Ukraine and Syria, a near-genocide ripped through central Africa, to little international fanfare. The Year’s Most Forgotten Humanitarian Crisis |Nina Strochlic |January 1, 2015 |DAILY BEAST 
Egypt has a comparatively low number of HIV cases compared to the rest of Africa, with just 11,000 infected people nationwide. Sisi Is Persecuting, Prosecuting, and Publicly Shaming Egypt’s Gays |Bel Trew |December 30, 2014 |DAILY BEAST 
The winter of 1897-98 was spent by Mr. Kipling and his family, accompanied by his father, in South Africa. Kipling Stories and Poems Every Child Should Know, Book II |Rudyard Kipling 
In Africa the ova, and even adults, of Distoma hmatobium are common, accompanying "Egyptian hematuria." A Manual of Clinical Diagnosis |James Campbell Todd 
Gallinas, the noted slave factory on the west of Africa, purchased by the Liberian republic. The Every Day Book of History and Chronology |Joel Munsell 
We give an engraving of a kind of pipe used by the natives of interior Africa. Tobacco; Its History, Varieties, Culture, Manufacture and Commerce |E. R. Billings. 
Later on he went to South Africa, where in the diamond mines he met with great success and made a large fortune. Fifty Years of Railway Life in England, Scotland and Ireland |Joseph Tatlow