In the just-released USA Today/Suffolk poll, Roberts trails his opponent by five points. A Loss by Pat Roberts in Kansas? Actually, Not So Bizarre |Jeff Greenfield |October 3, 2014 |DAILY BEAST 
A just-published study in the journal Nature explored how mice reacted to a diet of artificial sweeteners. Are Artificial Sweeteners Wrecking Your Diet? |DailyBurn |September 30, 2014 |DAILY BEAST 
Pamphlets were venues for advocacy and commentary on domestic affairs, but newspapers adopted a pose of just-the-facts neutrality. How the News Business Found Its Footing |Nick Romeo |June 22, 2014 |DAILY BEAST 
(Complete CPAC Coverage) A couple of speakers later, Mike Huckabee revved up the crowd with his just-plain-folks sermonizing. On CPAC Day 2, the Libertarian Wing Takes Over |Michelle Cottle |March 8, 2014 |DAILY BEAST 
So every bottle of rowanberry schnaps you see began with thousands of hand-harvest, just-frosted rowanberries. What to Drink When it’s Cold? The Glory of Austrian Schnaps |Jordan Salcito |January 25, 2014 |DAILY BEAST 
"Sure," grinned Stanton, with all the deceptive, undauntable optimism of the Just-Awakened. Molly Make-Believe |Eleanor Hallowell Abbott 
It is served by a ministerial-looking butler and a just-ready-to-be-ordained footman. Penelope's Experiences in Scotland |Kate Douglas Wiggin 
Till now one with sudden hiss: "But-good Christ-just look-why, the roof's leaning—!" The Lord of the Sea |M. P. Shiel 
In it were included the names of his brother, of Couthon, and of Saint-Just. Historical Tales, Vol. 6 (of 15) |Charles Morris 
We hurried back for them, forgetting that we had promised ourselves a long just-us talk to bridge the months of separation. Paris Vistas |Helen Davenport Gibbons