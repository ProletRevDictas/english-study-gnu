Give a sweet savour, and a memorial of fine flour, and make a fat offering, and then give place to the physician. The Bible, Douay-Rheims Version |Various 
These poems, of a savour so exquisitely strange, cost him no more than any badly rhymed commonplace. Charles Baudelaire, His Life |Thophile Gautier 
Some fruit was boiling on a stove, giving out a fragrant savour, and Elise's eye was on it mechanically. When Valmond Came to Pontiac, Complete |Gilbert Parker 
Brother Jackson made a motion with his mouth, as though he were tasting some pleasant savour. A Charming Fellow, Volume II (of 3) |Frances Eleanor Trollope 
And as for emotions—such as anger, or religion, or fear—he would attempt none whose savour he had not tasted for himself. Blazed Trail Stories |Stewart Edward White