Cook until the underside is a deep golden brown, then flip the grispelle with tongs and brown the other side. Italy's Best Dessert Secret |Rosetta Costantino |December 9, 2010 |DAILY BEAST 
And one of the seraphims flew to me, and in his hand was a live coal, which he had taken with the tongs off the altar. The Bible, Douay-Rheims Version |Various 
The children played and carried hot stones with tongs made of sticks. The Later Cave-Men |Katharine Elizabeth Dopp 
Use the butter-knife, salt-spoon, and sugar-tongs as scrupulously when alone, as if a room full of people were watching you. The Ladies' Book of Etiquette, and Manual of Politeness |Florence Hartley 
I could not stop to retrieve the question papers with a pair of tongs—as I might, had I not been hurried. Ruth Fielding At College |Alice B. Emerson 
I took the rabbit with a pair of tongs; the others had handled their baits and pug crept round 'em and nosed the trick. It Is Never Too Late to Mend |Charles Reade