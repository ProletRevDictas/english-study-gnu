Virginia artist Dongpei He, who is a native of China, paints peonies with a precision worthy of scientific illustration, but places the pink blossoms in loosely rendered environs. In the galleries: Biennial survey celebrates Mid-Atlantic artists of many mediums |Mark Jenkins |October 29, 2021 |Washington Post 
More often than not, Peony went with green, choosing to take care of both herself and Rita. What Can Animals Teach Us About Our Morality? |Robert Herritt |April 5, 2013 |DAILY BEAST 
Her two previous novels, Snowflower and the Secret Fan and Peony in Love, were also bestsellers. The Girls of Paris, China |Sandra McElwaine |June 19, 2009 |DAILY BEAST 
Sophy stepped over the dividing line, and the two sisters walked away to the peony settlement. Country Neighbors |Alice Brown 
They saw a band of silent maidens who stood in a wilderness of blossoming peony flowers, that grew to the waters edge. Japanese Fairy Tales |Grace James 
All the peony bed was tossed about like a troubled sea, and the pink and white petals flew like foam. Japanese Fairy Tales |Grace James 
I have in mind a woman who, some years ago, invested in a rare variety of Peony. Amateur Gardencraft |Eben E. Rexford 
I was not going to be a peony flaunting among thrifty modest vetches. The Late Miss Hollingford |Rosa Mulholland