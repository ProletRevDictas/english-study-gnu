Examples include some beetles and rainforest kangaroos called pademelons. Please do not touch the Australian stinging tree |Rebecca E. Hirsch |November 25, 2020 |Science News For Students 
Near the back of the beetle, the ridges are not as tightly interlocked. The diabolical ironclad beetle is nearly unsquishable |Maria Temming |November 23, 2020 |Science News For Students 
The diabolical ironclad beetle is like a tiny tank on six legs. The diabolical ironclad beetle is nearly unsquishable |Maria Temming |November 23, 2020 |Science News For Students 
He writes of the beetles that save mimosa trees in Houston, his daughter’s death at age 38 and greeting “small nervous birds” in walks along the sea. Eighty years of memories that will stir readers’ own |Connie Schultz |November 20, 2020 |Washington Post 
Last weekend, China’s customs agency banned imports of timber from Queensland, claiming it had found a beetle infestation in one shipment. China is ramping up its other big trade war |eamonbarrett |November 4, 2020 |Fortune 
Blister rust is like having the flu; the pine beetle is like fast acting leukemia. What It Takes to Kill a Grizzly Bear |Doug Peacock |November 23, 2014 |DAILY BEAST 
In 2007, FWS reported that the beetle outbreak had affected only 16 percent of the whitebark pines. What It Takes to Kill a Grizzly Bear |Doug Peacock |November 23, 2014 |DAILY BEAST 
The Beetle was launched with a series of television commercials unlike any before them. Nationalism on Four Wheels |Clive Irving |October 18, 2014 |DAILY BEAST 
Who could turn down a seaside drive in a bright yellow Beetle? World-Famous Architects Design Dollhouses For New Charity Project |Chloë Ashby |November 1, 2013 |DAILY BEAST 
The rhino beetle—fried, stewed, grilled, or roasted—is high in calcium and protein. Cicadas, Grasshoppers, Locusts, Ants Among the Tastiest Insects |Nina Strochlic |May 14, 2013 |DAILY BEAST 
Her feet crush creeping things: there is a busy ant or blazoned beetle, with its back broken, writhing in the dust, unseen. God and my Neighbour |Robert Blatchford 
But strangest of all the dishes at the Tagal's feast was one prepared from a kind of beetle. Alila, Our Little Philippine Cousin |Mary Hazelton Wade 
Then the juice would fill up the hole the beetle had made, and the tree would go on growing as before. The Wonder Book of Knowledge |Various 
There was a toy balloon, a beetle that ran all over the room in a life-like manner, a jumping jack, and some popcorn balls. The Story of the Big Front Door |Mary Finley Leonard 
Old Mr. Ford declared he had not laughed so much in twenty years as he did at the antics of the boys and the beetle. The Story of the Big Front Door |Mary Finley Leonard