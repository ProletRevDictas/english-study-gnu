Over the edge, you hurtle forward at a speed the human body was never meant to travel at. Skiing is Love at First Swoosh |David Frum |March 16, 2013 |DAILY BEAST 
As we hurtle together to the fiscal cliff, the Republicans are getting much the worse of the media battle. Democrats Have Their Own Fiscal Delusions |David Frum |December 31, 2012 |DAILY BEAST 
Ice masses, especially ones the size of Canadian provinces, do not technically “hurtle.” Oops, My Mistake |Christopher Buckley |December 17, 2009 |DAILY BEAST 
Dimly he saw the harpoon hurtle through the spray and the sharp crack of the explosion sounded in his ear. The Boy With the U. S. Fisheries |Francis Rolt-Wheeler 
Pen thought it was quite an honor to have seen the great Mr. Hurtle, whose works he admired. A History of Pendennis, Volume 1 |William Makepeace Thackeray 
He made sure that the folds projected above his hat, and would shut out all bullets that might hurtle against the unique helmet. The Phantom of the River |Edward S. Ellis 
Through watering eyes he saw the black cloud of flying beasts hurtle up from the trees below. Deathworld |Harry Harrison 
Others said that there certainly had been a Mr. Hurtle, and that to the best of their belief he still existed. The Way We Live Now |Anthony Trollope