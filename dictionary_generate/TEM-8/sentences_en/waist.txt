I proudly wore a figurative heavyweight champion belt around my 28-inch waist. The Anatomy Of A Breast Cancer Survivor: ‘Early Detection Saved My Life’ |Charli Penn |October 6, 2020 |Essence.com 
Body fat stored around the waist also releases more blood triglycerides in response to stress hormone signals compared to hip and thigh fat. Belly Fat Linked To Higher Risk Of Premature Death, Regardless Of Your Weight |LGBTQ-Editor |October 5, 2020 |No Straight News 
To maintain that aggressive fall-line stance through steep turns, you need to pivot at the waist, moving your lower body into a turn while keeping your upper body facing downhill. 6 Easy Ways to Be a Better Skier |Heather Hansman |October 1, 2020 |Outside Online 
Keep your elbows gently bent and your hands out in front of you at about waist height. 6 Easy Ways to Be a Better Skier |Heather Hansman |October 1, 2020 |Outside Online 
The high rise and seven-inch inseam are just right, too, providing coverage without smothering your waist. Why I Live in Bike Shorts |Aleta Burchyski |September 13, 2020 |Outside Online 
She is wearing a crop top, and Andrew has his arm wrapped around her waist. Buckingham Palace Disputes Sex Allegations Against Prince ‘Randy Andy’ |Tom Sykes |January 4, 2015 |DAILY BEAST 
He was paralyzed from the waist down, a World War II veteran and had six weeks to live. A Million Ways to Die in Prison |Daniel Genis |December 8, 2014 |DAILY BEAST 
The coat, with fitted bodice, nipped-in waist, and full skirt, created a familiar silhouette for Kate. Kate Middleton, the Preggers Fashion Princess |Tom Sykes |November 14, 2014 |DAILY BEAST 
Another model, covered from head to waist in white flowers, danced. Gareth Pugh's Fashion Show Lacked Fashion, But Not Passion |Justin Jones |September 5, 2014 |DAILY BEAST 
A naked dancer held a towel around his waist as he slicks back his hair in the ladies toilets. Inside London's Underground Burlesque and Fetish Scene |Liza Foreman |August 12, 2014 |DAILY BEAST 
His boyish suspenders had been put away in favor of a belt, which was tight-drawn about his slim waist. The Bondboy |George W. (George Washington) Ogden 
He stood aside, and bending from the waist he made a sweeping gesture towards the door with the hand that held his hat. St. Martin's Summer |Rafael Sabatini 
On his loins was a lion of great fierceness, and coiled round his waist was a hissing mamba (snake). Uncanny Tales |Various 
A pair of thin trousers and a shirt hanging down outside instead of being tucked in at the waist, and his toilet is made. Alila, Our Little Philippine Cousin |Mary Hazelton Wade 
The waist, now less pinched in at the middle, looked longer without being really so. Antonio Stradivari |Horace William Petherick