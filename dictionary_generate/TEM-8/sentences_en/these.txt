Quite a part of the game, believe me!these sort of things are only toys for children. The Sorrows of Satan |Marie Corelli 
Sir Knight, wouldst know thy lady's name?These pin-pricks tell from whence I came. Suppers |Paul Pierce 
Higher still were a dark belt of stunted firs and the sandstone ledge, and above these-home. A Cumberland Vendetta |John Fox, Jr. 
They cuddn'-ta-helptit-with-all-these-socean steamers-going-over-there every-day. Mollie and the Unwiseman Abroad |John Kendrick Bangs 
If the governor was a Duke of Devonshire, all these-caprices might be pardonable; but my theory is, roast-beef before roses. Sir Brook Fossbrooke, Volume I. |Charles James Lever