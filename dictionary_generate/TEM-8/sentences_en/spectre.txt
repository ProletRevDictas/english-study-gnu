Craig is signed on for just one more Bond flick after Spectre. Exclusive: Sony Emails Reveal Studio Head Wants Idris Elba For the Next James Bond |William Boot |December 19, 2014 |DAILY BEAST 
A spectre is haunting the internet—the spectre of Open Sarcasm. The Rise and Fall of the Infamous SarcMark |Keith Houston |September 24, 2013 |DAILY BEAST 
Nonetheless, it would have been better if the Supreme Court had not raised this spectre by halting the process. What if the Supreme Court Had Declined to Hear Bush v. Gore? |Megan McArdle |April 29, 2013 |DAILY BEAST 
A specter is haunting the world,” they chant, echoing the first sentence of the Communist Manifesto: “The spectre of capitalism. In ‘Cosmopolis,’ Robert Pattinson Depicts Financial World Gone Mad |Alex Klein |August 22, 2012 |DAILY BEAST 
Alone Orlean lay trying vainly to forget something—something that stood like a spectre before her eyes. The Homesteader |Oscar Micheaux 
And when he did leave the dismal scene of this last act of his miseries, it was like the spectre of the man who had entered it. The Pastor's Fire-side Vol. 3 of 4 |Jane Porter 
Besides, there was the ever unceasing grizzly spectre of poverty dangling before Jessie's eyes. The Weight of the Crown |Fred M. White 
A thing purple and dripping with blood—ghastly—unthinkable—monstrous—a spectre of nightmare dreams! Love's Pilgrimage |Upton Sinclair 
As a public force he was no longer a human being at all—he was a deformity, a spectre conjured up to bring fright to the beholder. Love's Pilgrimage |Upton Sinclair