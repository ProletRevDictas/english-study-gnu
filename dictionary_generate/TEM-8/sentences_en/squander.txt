Fans, pundits, and even players themselves regularly squander the chance to discuss race in meaningful ways. Ex-NFL Linebacker: We Talk Around Race, Not About It |Carl Banks |October 23, 2014 |DAILY BEAST 
Yet abandoning the push for equality now would squander a rare opportunity. The Closeted Revolution: Kiev’s Gays Keep Quiet to Deny Putin a Propaganda Win |James Kirchick |April 1, 2014 |DAILY BEAST 
“I watched the government squander a ridiculous amount of money on technology,” he says. Even the Most Powerful Man in the World Is at the Mercy of the IT Guy |Jill Lawrence |March 5, 2014 |DAILY BEAST 
We need to tell our politicians to not squander our heritage. Honeywell CEO to Washington: Don’t Squander Our Heritage |Dave Cote |October 18, 2013 |DAILY BEAST 
On the other hand, if we let fear carry the day, we will squander another key moment to move forward together. Democrats Disagree on Fracking, and It's Starting to Show |Ilana Glazer |April 26, 2013 |DAILY BEAST 
Arbitrary so far as doing the right thing as trustee went, not suffering me, or any one else, to squander a shilling. Johnny Ludlow, Fourth Series |Mrs. Henry Wood 
But a belligerent should not squander on diversions strength which might be employed in the main conflict. The Political History of England - Vol. X. |William Hunt 
Then he pointed to the beautifully worked manta, “Did she squander wealth of hers on that?” The Treasure Trail |Marah Ellis Ryan 
Time is a divine inheritance that no man has a right to squander. Autobiography of Frank G. Allen, Minister of the Gospel |Frank G. Allen 
He is not content to squander his immense wealth in race-horses and champagne. American Sketches |Charles Whibley