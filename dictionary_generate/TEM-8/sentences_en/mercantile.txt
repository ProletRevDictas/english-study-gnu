Copyright renewed 1969 by August Mencken and Mercantile Safe Deposit Trust Co. The Stacks: H.L. Mencken on the 1904 Baltimore Fire |H.L. Mencken |October 4, 2014 |DAILY BEAST 
The gulf between mercantile hubs and the polities in which they are lodged is not new. UKIP’s Nativist Rebellion Against London |Lloyd Green |May 26, 2014 |DAILY BEAST 
J. Crew Mercantile, which as of late has only become a name trademarked by the company, could be the next big thing. Angelina Jolie Talks Rebellious Years in ELLE; Shape Magazine Shuns Blogger's "After" Photos |The Fashion Beast Team |May 7, 2014 |DAILY BEAST 
Statements rendered by mercantile or collection agencies to inquirers for business purposes are clearly privileged. Putnam's Handy Law Book for the Layman |Albert Sidney Bolles 
With no mercantile marine of their own to guard, they had a free field for attack in the abundant shipping of their foes. The Wonder Book of Knowledge |Various 
He had been connected with the mercantile marine of New York from an early age. The Every Day Book of History and Chronology |Joel Munsell 
Above, in the lower lofts, every conceivable human oddity was assembled in a sort of mercantile crazy quilt. The Woman Gives |Owen Johnson 
She thought it was some mercantile speculation which led me from home, and, as you may believe, I did not undeceive her. Confessions of a Thug |Philip Meadows Taylor