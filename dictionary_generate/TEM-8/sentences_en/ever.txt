And yet we keep devouring the ever-increasing array of Jewish dating apps and sites and Facebook groups--why? My Week on Jewish Tinder |Emily Shire |January 5, 2015 |DAILY BEAST 
Duke kept running for offices and losing by ever-greater margins. The Louisiana Racists Who Courted Steve Scalise |Jason Berry |January 3, 2015 |DAILY BEAST 
The bar also claims that it hosted the first-ever poetry slam 28 years ago. The Bars That Made America Great |Nina Strochlic |December 28, 2014 |DAILY BEAST 
These insights and discoveries help PepsiCo anticipate, rather than react to, an ever-changing consumer landscape. The Science of Ingredient Innovation | |December 15, 2014 |DAILY BEAST 
What she did win, though, was the ever-more intense ardor of her growing number of liberal fans. The Most Powerful Democrat in America |Michael Tomasky |December 15, 2014 |DAILY BEAST 
A long stretch of smooth ice followed, over which he glided with ever-increasing speed. The Giant of the North |R.M. Ballantyne 
The pole was, therefore, continually floating or rising and falling in steam of ever-varying pressure. Life of Richard Trevithick, Volume II (of 2) |Francis Trevithick 
No, give me deserts or precipices,—anything fixed and solid is better than this capricious, ever-changing sea. Gallipoli Diary, Volume I |Ian Hamilton 
What matter if souls and bodies are failing beneath the feet of the ever-pressing multitude! The Awakening and Selected Short Stories |Kate Chopin 
It is a known fact that the third class traffic pays for the ever-increasing luxuries of first and second class travelling. Third class in Indian railways |Mahatma Gandhi