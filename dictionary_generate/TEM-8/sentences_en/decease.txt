In the time when thou shalt end the days of thy life, and in the time of thy decease, distribute thy inheritance. The Bible, Douay-Rheims Version |Various 
At Mr. Wright's death, it came to his widow, who gave it to my brother; at whose decease, it came to me. Notes and Queries, Number 177, March 19, 1853 |Various 
While it was being disposed of, she acquainted Mr. Bumble with the old womans decease. Oliver Twist, Vol. II (of 3) |Charles Dickens 
In the month of March, the decease of Mr. Liston, the comedian, attracted public notice. The History of England in Three Volumes, Vol.III. |E. Farr and E. H. Nolan 
August witnessed the decease of the veteran anti-reformer, Sir Charles Wetherell. The History of England in Three Volumes, Vol.III. |E. Farr and E. H. Nolan