Donald, on the other hand, began imitating Gua’s barks and onomatopoeia, which may have been one reason the experiment ended in just six months. What scientists learned when they tried to raise a chimp with a human baby |PopSci Staff |September 21, 2021 |Popular-Science 
Reading his writing can be kind of like taking a psychedelic — a literary onomatopoeia. A homegrown garden of mind-altering substances |David Herzberg |July 23, 2021 |Washington Post 
He seemed as much taken aback as if he had found a tribe of Cherokees studying onomatopoeia in English verse. McClure's Magazine, Volume VI, No. 3. February 1896 |Various 
All words which were spontaneously acquired seemed to be instances of onomatopoeia. Scientific American Supplement, No. 358, November 11, 1882 |Various 
Onomatopoeia, formations of words resembling in sound that of the things denoted by them. The Nuttall Encyclopaedia |Edited by Rev. James Wood 
Mao, the term for a "cat," is obviously an example of onomatopoeia. China and the Chinese |Herbert Allen Giles 
This correspondence of sound and sense is called onomatopoeia. The Principles of English Versification |Paull Franklin Baum