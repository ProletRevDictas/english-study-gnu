I started a blog called Boo Cancer, You Suck as a safe place for me to process what I was going through. Blogger Shares and Shames Cancer in ‘Lily’ |Amy Grantham |December 9, 2014 |DAILY BEAST 
Whether the episodes suck or the episodes are great, I stand by them. The TV Superhero Guru Behind ‘The Flash’ |Jason Lynch |October 6, 2014 |DAILY BEAST 
They want Lawrence to suck it up and smile for the camera, however retroactively. Jennifer Lawrence Shouldn’t Laugh Off Her Nude Photo Hack |Samantha Allen |September 2, 2014 |DAILY BEAST 
Anyone who thinks otherwise, to employ the emotional sophistication of “Shake It Off,” can suck it. Taylor Swift’s ‘Shake It Off’ Is Disappointing |Kevin Fallon |August 19, 2014 |DAILY BEAST 
They were going to suck up this problem and turn into the solution to the other problem, which was the meat shortage. Mississippi Hippos, Teddy Bears, and Other Strange Beasts |Scott Porch |July 25, 2014 |DAILY BEAST 
Then they continue to prick the body, and, as they say, they draw off or suck out the humors until the body is left dry. The Philippine Islands, 1493-1898, Volume XX, 1621-1624 |Various 
The child, who teaches its grandmother to suck eggs, commits a venial fault in comparison. Gallipoli Diary, Volume I |Ian Hamilton 
They seize the former so dexterously by the neck that they always master them; the crocodile eggs they suck. A Woman's Journey Round the World |Ida Pfeiffer 
If the young elephant had once been used to suck with his mouth, how could he lose that habit the remainder of his life? Buffon's Natural History. Volume VII (of 10) |Georges Louis Leclerc de Buffon 
She is supposed to be a sort of spiritual vampyre, and to suck the life out of infants and young people. Ancient Faiths And Modern |Thomas Inman