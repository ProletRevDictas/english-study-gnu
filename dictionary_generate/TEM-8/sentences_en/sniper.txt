The fences are themselves covered in black sniper netting, to discourage assassins. 9/11 Mastermind Is Afraid of the Ladies |Tim Mak |December 16, 2014 |DAILY BEAST 
Eric Frein eluded Pennsylvania police for seven weeks after he allegedly killed a state trooper with a sniper shot. Killer Eric Frein Held in Murdered Cop’s Cuffs |Michael Daly |October 31, 2014 |DAILY BEAST 
Before long, she was awarded the “Top Shot” award, and poised to be a top-level sniper. The Making of Kiesza: From Navy Sharpshooter to Beauty Queen to Pop Diva |Marlow Stern |October 20, 2014 |DAILY BEAST 
“I had to go in the tunnels under [a] town to avoid sniper fire,” she said. Morgan Fairchild: Badass Foreign Policy Wonk |Asawin Suebsaeng |October 20, 2014 |DAILY BEAST 
He lists them starting with modern sniper rifles going all the way up to anti-tank and anti-aircraft missiles. Why Does the Free Syrian Army Hate Us? |Jamie Dettmer |October 3, 2014 |DAILY BEAST 
Suddenly a sniper would see some part of me showing, and would then let drive at me. Letters of Lt.-Col. George Brenton Laurie |George Brenton Laurie 
Then a German sniper with his gun climbed up on the platform. The Blot on the Kaiser's 'Scutcheon |Newell Dwight Hillis 
Poor fellow, he was killed by a sniper near St loi on April 15. The Doings of the Fifteenth Infantry Brigade |Edward Lord Gleichen 
Dyer was caught by a sniper, and Tucker was hit in the leg by a machine gun bullet. Into the Jaws of Death |Jack O'Brien 
No time for a lad of his mettle to weary of well-doing; and he knew of a sniper worth adding to his bag. Notes of a Camp-Follower on the Western Front |E. W. Hornung