That was sadly even true for Margaret Fuller, one of the leading lights of transcendentalism. Why Do Women Love Bad Men? A New Life of Margaret Fuller |Susan Cheever |March 22, 2013 |DAILY BEAST 
Your religion does not make it—its ethics are too weak, its theories too unsound, its transcendentalism is too thin. God and my Neighbour |Robert Blatchford 
The vagueness of transcendentalism is united with the materialism of nature worship, and the resulting equation is pessimism. The War Upon Religion |Rev. Francis A. Cunningham 
Here we have the root of the errors which are distinctive of dualism and the prevailing metaphysical transcendentalism. The Wonders of Life |Ernst Haeckel 
Transcendentalism, too, had just passed the noon meridian of its splendor. John Greenleaf Whittier |W. Sloane Kennedy 
The chief fountains of this tradition were Calvinism and transcendentalism. Winds Of Doctrine |George Santayana