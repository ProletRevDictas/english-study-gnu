Moreover, trucks, dust, and boomtown stress are the effects of any large-scale industrial activity. New York’s Conservative Fracking Ban |Jay Michaelson |December 20, 2014 |DAILY BEAST 
Moreover, the crippling restrictions resulted in the North partially shutting down weapons programs for lack of funds. U.S. Should Make North Korea Pay for Sony Hack |Gordon G. Chang |December 18, 2014 |DAILY BEAST 
Moreover, for America there is a fundamental imperative to act. U.S. Should Make North Korea Pay for Sony Hack |Gordon G. Chang |December 18, 2014 |DAILY BEAST 
Moreover, the exhibition begs the question: how do we come to privilege certain images? A History of Paris in 150 Photographs |Sarah Moroz |December 14, 2014 |DAILY BEAST 
Moreover, uneducated Americans have a competitive advantage because of their fluency in English. The Case for More Low-Skill Immigration |Veronique de Rugy |December 7, 2014 |DAILY BEAST 
Moreover, most of the burrows were only a few feet apart and no agonistic behavior was witnessed. Summer Birds From the Yucatan Peninsula |Erwin E. Klaas 
Moreover, Napoleon, so great in many things, was so jealous of his own glory that he could be mean beyond words. Napoleon's Marshals |R. P. Dunn-Pattison 
Moreover, the Bible teaches erroneous theories of history, theology, and science. God and my Neighbour |Robert Blatchford 
I saw the folly of imagining that I could stand a chance against a man like Moeran, and, moreover, he interested me too deeply. Uncanny Tales |Various 
Tony, moreover, had hidden himself until his letter should be answered—and she was 'lonely.' The Wave |Algernon Blackwood