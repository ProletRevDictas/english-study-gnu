He taught the people how to form and use alphabetic signs and instructed them in mathematics: he gave them their code of laws. Myths of Babylonia and Assyria |Donald A. Mackenzie 
Where obvious, typos have been corrected in the text and marked with an alphabetic footnote. Secret Societies And Subversive Movements |Nesta H. Webster 
Professor Sayce supported him, strongly inclining toward an alphabetic or linguistic, perhaps ideographic, signification. The Swastika |Thomas Wilson 
We can use her materials for sense training and lead as she does to easy mastery of the alphabetic symbols. The Montessori Method |Maria Montessori 
The privilege of modifying alphabetic form is one that has been frequently exercised. A Librarian's Open Shelf |Arthur E. Bostwick