The two met last year when Riya, 19, took up squash during a gap year after graduating from Marshall High in Falls Church. She’s been growing her hair for 17 years. On Thursday, she’ll cut it off for charity. |John Kelly |August 25, 2021 |Washington Post 
A favorite of chefs and home cooks alike, you may not have to look far to find edible squash blossoms. 7 edible flowers and how to use them |John Kennedy |July 20, 2021 |Popular-Science 
If folks think of squashes as vegetables, maybe they are vegetables. The bizarre botany that makes corn a fruit, a grain, and also (kind of) a vegetable |empire |July 8, 2021 |Popular-Science 
Also called yellow zucchini, this squash is slightly sweeter that its green cousin. How to use summer squash: a guide to this bountiful, versatile vegetable |Aaron Hutcherson |July 2, 2021 |Washington Post 
You’ll get a crunchy, crispy garnish for pasta enrobed in a thick, rich squash sauce. Cut waste and boost flavor with skin-to-seed recipes that use the whole vegetable |Joe Yonan |April 23, 2021 |Washington Post 
The salmon is presented atop a mound of sautéed vegetables: mushrooms, peppers, squash, onions, leafy greens, and herbs. Spaghetti for Breakfast?! Not So Crazy at This Idaho Farm Café |Jane & Michael Stern |August 4, 2014 |DAILY BEAST 
There is increasing evidence that Assad is working with ISIS to squash the Free Syrian Army. America's Allies Are Funding ISIS |Josh Rogin |June 14, 2014 |DAILY BEAST 
Spaghetti squash also contains specific nutrients that help convert the tryptophan in other foods you eat into serotonin. 6 Healthy Foods to Fight the Flu, Beat Stress and More |DailyBurn |February 5, 2014 |DAILY BEAST 
Headache—Take the rinds of a couple of lemons and squash it into a paste. Use These 15 Home Remedies Based On Ayurveda To Cure Menstrual Cramps, Hangovers, and Indigestion |Ari Meisel |January 21, 2014 |DAILY BEAST 
All this testing is serving to squash creativity and the excitement of learning. A Teach for America Alum On How Testing Is Hurting Our Kids |Jonathan Sheehan |October 9, 2013 |DAILY BEAST 
The Squash Maiden drew about her a rich green blanket, into which she had woven many flaunting gold trumpet-shaped flowers. Stories the Iroquois Tell Their Children |Mabel Powers 
Corn Plume looked down at the Squash Maiden sitting on her blanket at his feet. Stories the Iroquois Tell Their Children |Mabel Powers 
Ever after, corn and beans were planted in the same hill, and often a squash seed was added. Stories the Iroquois Tell Their Children |Mabel Powers 
I guess it must be squash, for he likes mam-ma's squash pies so much. Pages for Laughing Eyes |Unknown 
Squish, squash echoed the milk in the great cylinder, but never arose the sound they waited for. Tess of the d'Urbervilles |Thomas Hardy