Lee says men bombard him on Twitter with questions about how to get into the industry. Inside ‘The Sex Factor’: Where 16 Men and Women Vie For Porn Immortality |Aurora Snow |November 22, 2014 |DAILY BEAST 
Just Google “Patrick Wilson Girls backlash,” and wait for the hateful, Lena Dunham-bashing vitriol to bombard your screen. Louis C.K. Apologizes to the ‘Fat Girls’ |Amy Zimmerman |May 13, 2014 |DAILY BEAST 
Your most grating acquaintance could – and usually would – bombard you with reams of unoriginal drivel at the press of a key. Unconsidered Trifles: Found Comedy in the Age of Social Media |Tom Doran |March 30, 2013 |DAILY BEAST 
Listen, suppose they got in, suppose they start to bombard Guantanamo? JFK’s Secret White House Recordings Unveiled |Ted Widmer |September 25, 2012 |DAILY BEAST 
In other words, it's business as usual as the two sides bombard each other militarily and diplomatically. The Aborted Gaddafi Amnesty Deal |Fadel Lamen |March 9, 2011 |DAILY BEAST 
The Germans continued to bombard Ypres with large calibre shells, heaping ruins upon ruins. Ypres and the Battles of Ypres |Unknown 
Three days before Filangieri landed, the gunners in the citadel began to bombard the helpless town lying beneath them. A History of the Nineteenth Century, Year by Year |Edwin Emerson 
Sheriff Jones demanded the arms of the people, otherwise he would bombard the town. Personal Recollections of Pardee Butler |Pardee Butler 
Wolfe's first move was to occupy Point Levi, and erect batteries there, from which he could bombard the city. Battles of English History |H. B. (Hereford Brooke) George 
The 15th was fixed upon for the grand assault, and the entire fleet had orders to move up and bombard at an early hour. The Hero of Manila |Rossiter Johnson