Software began decades ago as an unserious programmers’ antonym of “hardware.” From ‘Scientist’ to ‘Spam,’ the Surprisingly Playful Origins of English Words |Ralph Keyes |April 1, 2021 |Time 
But, oddly enough, the eyes were a total and surprising antonym of this smiling mouth. The Red Debt |Everett MacDonald 
In strict usage, daily is the antonym of nightly as diurnal is of nocturnal. English Synonyms and Antonyms |James Champlin Fernald 
By and by, which was once a synonym, has become an antonym of immediately, meaning at some (perhaps remote) future time. English Synonyms and Antonyms |James Champlin Fernald 
Logically the other side of white is not white, while the antonym is the extreme black. Public Speaking |Clarence Stratton 
The direct antonym of cause is effect, while that of antecedent is consequent. English Synonyms and Antonyms |James Champlin Fernald