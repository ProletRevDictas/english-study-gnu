But the other thing that needs to be done is for us citizens to do. Ayaan Hirsi Ali: Our Duty Is to Keep Charlie Hebdo Alive |Ayaan Hirsi Ali |January 8, 2015 |DAILY BEAST 
Newspapers around Europe have also done so in solidarity with the slain. Why We Stand With Charlie Hebdo—And You Should Too |John Avlon |January 8, 2015 |DAILY BEAST 
Neither the Republican nor the Democratic party have done anything to consistently target Asian- American voters. Asian-Americans Are The New Florida |Tim Mak |January 8, 2015 |DAILY BEAST 
He made clear that he fully appreciated what the cops had done. Shot Down During the NYPD Slowdown |Michael Daly |January 7, 2015 |DAILY BEAST 
Because I was going more on about how things had already been done. Patton Oswalt on Fighting Conservatives With Satire |William O’Connor |January 6, 2015 |DAILY BEAST 
You need but will, and it is done; but if you relax your efforts, you will be ruined; for ruin and recovery are both from within. Pearls of Thought |Maturin M. Ballou 
Done, says he, why let fifty of our men advance, and flank them on each wing. The Life and Most Surprising Adventures of Robinson Crusoe, of York, Mariner (1801) |Daniel Defoe 
By the time I had done my toilette there was a tap at the door, and in another minute I was in the salle--manger. Blackwood's Edinburgh Magazine, Volume 60, No. 372, October 1846 |Various 
And is this a mere fantastic talk, or is this a thing that could be done and that ought to be done? The Salvaging Of Civilisation |H. G. (Herbert George) Wells 
If it took years to do it, you shall never stir out of this house till it is done. Checkmate |Joseph Sheridan Le Fanu