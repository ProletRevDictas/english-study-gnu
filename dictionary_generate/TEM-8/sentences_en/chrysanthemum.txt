Since it was early afternoon, Rinzler gave me a crash-course in “right drinking” over a pot of Chrysanthemum tea. Meditation Teacher Lodro Rinzler Rebrands Buddhism to Deal with Drinking and Sex |Allison Yarrow |February 11, 2012 |DAILY BEAST 
Her efforts were so successful that the newspapers referred to Twain's locks as a "chrysanthemum of white hair." America's First Modern Celebrity |Laura Skandera Trombley |March 20, 2010 |DAILY BEAST 
Be on the lookout for black beetle on Aster and Chrysanthemum. Amateur Gardencraft |Eben E. Rexford 
Since then she pulls off a chrysanthemum every morning from the plant in her mother's window, and lays it beside my plate. A Sheaf of Corn |Mary E. Mann 
The rose show of June is only surpassed by the chrysanthemum exhibition in autumn. Appletons' Popular Science Monthly, July 1899 |Various 
Other bearers followed, keeping step and carrying the regalia, consisting of chrysanthemum stalks and blossoms. Japanese Fairy World |William Elliot Griffis 
Particularly is this true of My Lady Chrysanthemum; for she well repays for any trouble that may be spent upon her. The Harmsworth Magazine, Vol. 1, 1898-1899, No. 6 |Various