He is a designer, a technologist, and a Renaissance man without peer. Elon Musk’s rocket ride |Andrew Nusca |December 2, 2020 |Fortune 
The “chain” attacks after a counterattack turned Ezio into a Renaissance John Wick. All the ‘Assassin’s Creed’ games, ranked |Elise Favis, Gene Park |November 11, 2020 |Washington Post 
The plexiglass creates a sort of terrarium of a Renaissance garden. GALA Hispanic Theater reopens with comedy — and plexiglass |Patrick Folliard |October 31, 2020 |Washington Blade 
This grandiose wine “jail” is designed out of wrought iron with solid iron bars in a Renaissance inspired arched and scrollwork design. Wine bottle holders and racks that make sophisticated gifts |PopSci Commerce Team |September 29, 2020 |Popular-Science 
Things seem to have gotten out of hand after the Renaissance. Why Mathematicians Should Stop Naming Things After Each Other - Issue 89: The Dark Side |Laura Ball |September 2, 2020 |Nautilus 
The demonic ‘anti-Santa’ enjoys an unlikely renaissance as we learn to embrace our inner pagan. Meet Krampus, the Seriously Bad Santa |Jay Michaelson |December 5, 2014 |DAILY BEAST 
Lee makes a convincing case that the loveliness of much Renaissance art is inversely related to the moral ugliness of its patrons. Great Renaissance Art Thrived Amid Filth |Nick Romeo |December 3, 2014 |DAILY BEAST 
The cardinals had such a bad reputation that the very term “cardinal” became an insult in Renaissance Rome. Great Renaissance Art Thrived Amid Filth |Nick Romeo |December 3, 2014 |DAILY BEAST 
It may not be a story the Vatican wants told, but such nasty behavior is also a part of the Renaissance. Great Renaissance Art Thrived Amid Filth |Nick Romeo |December 3, 2014 |DAILY BEAST 
But the flaws and peccadilloes of Renaissance artists like Michelangelo pale beside the misdeeds of patrons and pontiffs. Great Renaissance Art Thrived Amid Filth |Nick Romeo |December 3, 2014 |DAILY BEAST 
The organ is inclosed in a case designed by Mr. Arthur Hill after old renaissance examples. The Recent Revolution in Organ Building |George Laing Miller 
His history endeavors to show that the Felibrean renaissance was not a spontaneous springing into existence. Frdric Mistral |Charles Alfred Downer 
They affirmed their adherence to the Renaissance méridionale, and claimed equal rights for the Languedocian dialect. Frdric Mistral |Charles Alfred Downer 
During this period of the Renaissance, as Hadria afterwards called the short-lived epoch, little Martha was visited frequently. The Daughters of Danaus |Mona Caird 
The architects of the Renaissance showed great boldness in their designs. Encyclopaedia Britannica, 11th Edition, Volume 4, Part 3 |Various