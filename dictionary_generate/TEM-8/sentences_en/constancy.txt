Since color vision does rely, at least in part, on color constancy, well, that could actually solve some complicated problems. How These Rothkos Were Restored Without Touching the Canvas |Adam Rogers |May 30, 2021 |The Daily Beast 
The Harvard team planned to create an illusion—to put the “con” in color constancy. How These Rothkos Were Restored Without Touching the Canvas |Adam Rogers |May 30, 2021 |The Daily Beast 
Homeostasis says that living systems resist change and desire constancy above all else. 6 Principles for Navigating Challenges in Life |Brad Stulberg |May 6, 2021 |Outside Online 
“The key goal of regulation is not rigid constancy,” writes Sterling. 6 Principles for Navigating Challenges in Life |Brad Stulberg |May 6, 2021 |Outside Online 
In its constancy, it became all the more comforting for the legions of fans who turned to “Jeopardy!” Alex Trebek, quintessential quizmaster as ‘Jeopardy!’ host for three decades, dies at 80 |Emily Langer |November 8, 2020 |Washington Post 
Well, McConnell made Romney look like an ironman of forthright constancy. The Amazingly Two-Faced Mitch McConnell |Michael Tomasky |October 15, 2014 |DAILY BEAST 
It conveys the constancy and consistency of the alliance, a special relationship. Rick Perry's Dangerous Israel Gaffe |Bruce Riedel |November 13, 2011 |DAILY BEAST 
She left her son with one place to go: in search of a constancy, a definition he had never known. Dreams of His Mother |Stacy Schiff |May 3, 2011 |DAILY BEAST 
The preoccupation with “middle class” constancy strikes me as obsessive. What Did Velma Hart Expect? |Tunku Varadarajan |September 21, 2010 |DAILY BEAST 
The bad feelings are mainly about values, style and constancy more than policy. Day of Reckoning |The Daily Beast |June 22, 2010 |DAILY BEAST 
World-weary and sick at heart, they still struggled to sustain each other, and to meet their dreadful fate with heroic constancy. Madame Roland, Makers of History |John S. C. Abbott 
Arcite is supposed to have worn white, red, or green; but he did not wear blue, for that was the colour of constancy. Chaucer's Works, Volume 1 (of 7) -- Romaunt of the Rose; Minor Poems |Geoffrey Chaucer 
Both here and in the Squieres Tale we find the allusions to Lamech, and to blue as the colour of constancy; see notes to ll. Chaucer's Works, Volume 1 (of 7) -- Romaunt of the Rose; Minor Poems |Geoffrey Chaucer 
He was kept a long time in prison, twice racked by order of the council, and every effort was made to shake his constancy. Encyclopaedia Britannica, 11th Edition, Volume 5, Slice 2 |Various 
The constancy and danger of a twenty years' passion is a subject upon which I hardly know how to be serious. Private Letters of Edward Gibbon (1753-1794) Volume 1 (of 2) |Edward Gibbon