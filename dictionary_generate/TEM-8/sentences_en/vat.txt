Snapdragon’s technology uses a continuous production line, rather than the traditional process of making batches in big vats, so it’s easier to scale up by simply keeping production running for a longer time. Why We Can’t Make Vaccine Doses Any Faster |by Isaac Arnsdorf and Ryan Gabrielson |February 19, 2021 |ProPublica 
I slipped the skins off peaches, scooped melon with a spoon, and made gargantuan vats of applesauce. Six Months of Soft Food Set Me Free |Meghan McCarron |February 2, 2021 |Eater 
And, failing that, even bigger vats of liquid are on the way. We still don’t really know what’s inside the sun—but that could change very soon |Charlie Wood |November 30, 2020 |Popular-Science 
VAT is levied on consumption of goods and services in countries around Europe. Travel spending has vanished across Europe |Marc Bain |July 27, 2020 |Quartz 
When that garbage is diverted into a vat of energy-producing soup, it becomes a penny earned. Will Food Waste Power Your Home? |The Daily Beast |June 16, 2014 |DAILY BEAST 
It filled my head, that muttering sound, like thick oily smoke from a fat-rendering vat or an odour of noisome decay. Read ‘The King in Yellow,’ the ‘True Detective’ Reference That’s the Key to the Show |Robert W. Chambers |February 20, 2014 |DAILY BEAST 
He kills his own son in cold blood (or in a vat of hot macadamia butter, to be specific). American Dreams, 1993: The Road to Wellville by T. Coraghessan Boyle |Nathaniel Rich |October 30, 2013 |DAILY BEAST 
Burning a girl alive while her father watches or cooking a severed head into a vat of chili? Why the ‘Sons of Anarchy’ Premiere Was Gratuitous and Horrifying and Totally Awesome |Paula Szuchman |September 11, 2013 |DAILY BEAST 
And, also, there ought to be no other forms of taxation, like a national sales tax or a VAT tax or what have you. Mark Levin's Nutty Constitutional Convention Idea |Michael Tomasky |August 29, 2013 |DAILY BEAST 
But my faver says it's un-man-ly to be always kissing, and I did n't fink you'd do vat, Coppy. Kipling Stories and Poems Every Child Should Know, Book II |Rudyard Kipling 
Acid fumes escaping under the vat lids made the haze and seared the man's throat. In the Dark |Ronal Kayser 
The vat contained an acid powerful enough to destroy anything—except gold. In the Dark |Ronal Kayser 
Oof you hatn't done vat you dit, I bed you somet'ing der modor-car vould haf peen a lot oof junk. Motor Matt's Mystery |Stanley R. Matthews 
Und der feller vat hat it didn't vant it, or he vouldn't haf let it go. Motor Matt's Mystery |Stanley R. Matthews