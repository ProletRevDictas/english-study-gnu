That such a revulsion should occur in the nature of a gadabout and featherbrain like this girl, is not unnatural. Iconoclasts |James Huneker 
France will be prettily governed by a featherbrain, who has such whims. Balsamo, The Magician |Alexander Dumas 
She thought, the featherbrain, that she was entering the Leaf-cutters house! Insect Adventures |J. Henri Fabre 
I was always laughing and skipping about like a featherbrain. His Masterpiece |Emile Zola 
You I can trust; but she is a bit of a featherbrain, and one never knows what may happen. A True Friend |Adeline Sergeant