Wildlife researchers often trek through jungles and underbrush to find animals. These flying robots protect endangered wildlife |Kathryn Hulick |March 10, 2022 |Science News For Students 
The way he tells it, he sat in the cold for a few hours only to have a frenzied pair of antlers flash into view for half a second before vanishing into the frozen underbrush. Why You Should Be Watching Indoor Track |mmirhashem |January 27, 2022 |Outside Online 
Suddenly, a hungry Tyrannosaurus rex bursts from the underbrush. Explainer: The age of dinosaurs |Beth Geiger |December 3, 2021 |Science News For Students 
Sometimes it’s the understory or underbrush that’s turning bright colors. Leaf-Peeping Looks Different This Year |eriley |October 8, 2021 |Outside Online 
They’re too large to move silently, so rustling and scratching noises in the underbrush or up in the branches of young trees is a good sign that they may be nearby. How to not get pricked by a North American porcupine |Natalie Wallington |September 17, 2021 |Popular-Science 
She was already dead and laying just a quarter mile from her home, in dense underbrush where her mother used to play as a child. The Case Against Casey |Diane Dimond |May 22, 2011 |DAILY BEAST 
The road on which the Federals were marching was narrow and on each side lined with dense underbrush. The Courier of the Ozarks |Byron A. Dunn 
Benny and Violet scrambled through the underbrush to the place Jess pointed out, and investigated. The Box-Car Children |Gertrude Chandler Warner 
But the children could find no path through the underbrush that would lead to its beautiful waters. Honey-Bee |Anatole France 
Violet held great sprays of fine underbrush in place until each log was laid. The Box-Car Children |Gertrude Chandler Warner 
The path led for some distance through a thicket of alders and underbrush, from which at length it emerged into an open field. The Rival Campers |Ruel Perley Smith