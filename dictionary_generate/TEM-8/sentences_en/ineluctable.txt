Back then, today was supposed to be the official beginning of the slow and ineluctable ascent to the White House. How Bad Does the GOP Need Chris Christie? Really Bad. |Michael Tomasky |January 21, 2014 |DAILY BEAST 
He felt that in the face of every obstacle he was still the instrument of an ineluctable and incorruptible Justice. The Shadow |Arthur Stringer 
Others he seldom felt called upon to judge, but if the instance were ineluctable, he was prone to an amiable generosity. The Clarion |Samuel Hopkins Adams 
I am so bad that I am fleeing in a day or two—as I hope you will have been doing if your ineluctable fate doesn't spare you. The Letters of Henry James (volume I) |Henry James 
Nature herself is maya; natural science must perforce deal with her ineluctable quiddity. Autobiography of a YOGI |Paramhansa Yogananda 
She liked Evanthia because she had that ineluctable quality of transfiguring an act into a grandiose gesture. Command |William McFee