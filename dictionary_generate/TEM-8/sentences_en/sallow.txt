Lincoln would endure bout after bout of the hypos, until a permanent sadness settled onto his sallow face. Making Lincoln Sexy: Jerome Charyn’s Fictional President |Tom LeClair |March 6, 2014 |DAILY BEAST 
Junkies have their own look (emaciated, haunted, sallow) and their own junk names: Doolie, Cash, and Dupré. American Dreams, 1953: ‘Junky’ by William S. Burroughs |Nathaniel Rich |June 27, 2013 |DAILY BEAST 
This John is sickly and sallow, his body lacking plasticity. Caravaggio's Grand Passions |Adam Eaker |June 11, 2010 |DAILY BEAST 
Again the sallow fingers began to play with the book-covers, passing from one to another, but always slowly and gently. Bella Donna |Robert Hichens 
Now and then he touched one with his long and sallow fingers, lifted its cover, then let it drop mechanically. Bella Donna |Robert Hichens 
After a few minutes he returned, and then his sallow countenance wore a smile. Blackwood's Edinburgh Magazine, No. CCCXXXIX. January, 1844. Vol. LV. |Various 
She patted the sallow cheek of the American with her jewelled fingers, and turned aside, glancing about her. Dope |Sax Rohmer 
Then, the sallow, black-haired knave who had last night proclaimed himself as Garnache in disguise was some impostor. St. Martin's Summer |Rafael Sabatini