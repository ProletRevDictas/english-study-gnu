Multiply this at the scale of Facebook’s billions of users and across all subject matters, and it’s easy to see why simply not “recommending” some groups barely makes a dent. Facebook tries to clean up Groups with new policies |Sarah Perez |September 17, 2020 |TechCrunch 
Many of those top-of-the-line homes on wheels can put a dent in your savings as large as a condo. Can’t Afford a Sprinter? Get a Tiny Van Instead. |Emily Pennington |September 16, 2020 |Outside Online 
Yet, you want your brand to gain traction and make a dent, in spite of the dominant brands. Inbound marketing for brand awareness: Four up-to-date ways to do it |Ali Faagba |September 11, 2020 |Search Engine Watch 
The vaccine has to be effective, and there needs to be enough of it to put a dent in transmission. A third of Americans might refuse a Covid-19 vaccine. How screwed are we? |Brian Resnick |September 4, 2020 |Vox 
I won’t say it’s cured me of my insomnia completely, but it’s made enough of a dent to keep it around. The best things I bought in August |Rachel Schallom |August 30, 2020 |Fortune 
My desk still has the dent from where I hit my head against it when I heard that one. Why So Many Surgeons Are Psychos |Russell Saunders |December 17, 2014 |DAILY BEAST 
That is bound to put a dent in public confidence in the police. Idris Elba on Eric Garner, ‘Mi Mandela,’ and Selling Weed to Dave Chappelle |Marlow Stern |December 6, 2014 |DAILY BEAST 
Even with mixed reviews, the film did little to dent Clooney's reputation. Clooney: A Constant Charmer at the Altar |Tim Teeman |September 28, 2014 |DAILY BEAST 
Plane travel is extremely dehydrating, and continuously purchasing water at airport prices can put a dent in your wallet. How to Get Cheaper Tickets, Live Like a Local, and Other Great Travel Hacks |Brandon Presser |June 4, 2014 |DAILY BEAST 
Emergency benefits have just expired for some 1.3 million jobless Americans, putting a huge dent in our economy. Republicans’ Unemployment Shame |Jamelle Bouie |December 28, 2013 |DAILY BEAST 
I guess he can't make a dent on the Chinese disposition, or he'd have had Wong dead before this. Mystery Ranch |Arthur Chapman 
Please take the dent out of my side, Poly, for I am more crippled than was the Soldier. The Tin Woodman of Oz |L. Frank Baum 
The snow-covered Dent du Midi had a splendor like the face of the full moon when it is rising. Rudy and Babette |Hans Christian Andersen 
Then he would show the dent in his cheek, and pass his helmet round for all to see, as a conjurer does. The Relief of Mafeking |Filson Young 
Mr. John Dent was born about the middle of the eighteenth century. English Book Collectors |William Younger Fletcher