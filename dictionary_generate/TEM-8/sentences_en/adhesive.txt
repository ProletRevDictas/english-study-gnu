To set up the MagicMount, you first attach its plastic base to your dash with a peel-back adhesive. The Best Phone Mount for Your Car Costs $12 |Jakob Schiller |September 20, 2020 |Outside Online 
You can also slip the magnet between your phone and phone case and forgo an adhesive. The Best Phone Mount for Your Car Costs $12 |Jakob Schiller |September 20, 2020 |Outside Online 
Be aware the adhesive backing will not adhere to textured surfaces. Cork boards for organizing your home or office |PopSci Commerce Team |September 17, 2020 |Popular-Science 
This plastic case is equipped with 100 first aid resources, including six antiseptic towelettes, 12 alcohol wipes, and 30 adhesive bandages. The best first aid kits for staying safe and prepared |PopSci Commerce Team |September 4, 2020 |Popular-Science 
Worn like an adhesive bandage, this skin patch looks simple on the outside. Sweat tech alerts athletes when to rehydrate — and with what |Silke Schmidt |May 21, 2020 |Science News For Students 
Then, the merkin is applied with an adhesive; Ledermann uses a matte substance by Telesis. Hollywood's Most Private Accessory |Claire Howorth |September 20, 2010 |DAILY BEAST 
Have you ever seen 11 players with a relationship so adhesive to the ball? Who Will Win the World Cup? |Tunku Varadarajan |July 10, 2010 |DAILY BEAST 
This work fascinates children, and as the stars are adhesive, it can be done in class with very little trouble. The Story of the Thirteen Colonies |H. A. (Hlne Adeline) Guerber 
In the case of a sprained ankle a properly applied adhesive strap bandage will give no end of relief and support. The Mother and Her Child |William S. Sadler 
The tape Mac used to insure his connection had an asbestos base, with adhesive gum insinuated into the tape. Tight Squeeze |Dean Charles Ing 
The side of the box showed that it had been made adhesive, for the sake of security, to another substance. Rhoda Fleming, Complete |George Meredith 
Virgil refers to it as a "glue more adhesive than bird-lime and the pitch of Phrygian Ida." A Year in the Fields |John Burroughs