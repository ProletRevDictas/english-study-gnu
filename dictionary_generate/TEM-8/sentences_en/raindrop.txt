We should stay rain-free while the sun is up, although a couple raindrops near sunset are possible. D.C.-area forecast: Clouds increase today, with rain odds up starting tonight |A. Camden Walker |February 26, 2021 |Washington Post 
Flakes passing through this elevated mild air partially melt into icy raindrops. Sleet vs. snow: The reason behind Thursday’s icy mess |Jeffrey Halverson |February 19, 2021 |Washington Post 
That same thing happens when sunlight shines through a raindrop. Explainer: Rainbows, fogbows and their eerie cousins |Matthew Cappucci |May 1, 2020 |Science News For Students 
Rainbows develop when sunlight passes through falling raindrops. Explainer: Rainbows, fogbows and their eerie cousins |Matthew Cappucci |May 1, 2020 |Science News For Students 
A little bit of the leftover light reflects — bounces — off of the inside edge of the raindrop. Explainer: Rainbows, fogbows and their eerie cousins |Matthew Cappucci |May 1, 2020 |Science News For Students 
Although the beat of a raindrop is proverbially light, the stroke is not ineffective. Outlines of the Earth's History |Nathaniel Southgate Shaler 
Not a tree quivered, not a raindrop slipped from a blade of grass, but Guy held out his arms to clasp his long-awaited Pauline. Plashers Mead |Compton Mackenzie 
The gentle raindrop grapples with mountains of solid rock, and with never-ending persistence drags them piecemeal into the sea. Your National Parks |Enos A. Mills 
Karna silent heard this mandate but his birth could not proclaim, Like a raindrop-pelted lotus bent his humble head in shame! Maha-bharata |Anonymous 
The night was inky black, and the wind that swept down the gorge brought an occasional raindrop with it. The Wayfarers |Mary Stewart Cutting