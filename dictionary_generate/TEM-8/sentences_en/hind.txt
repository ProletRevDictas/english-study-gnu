Di Liberto says this “hind-casting” is the opposite of forecasting. Beyond crystal balls: How to make good forecasts |Avery Elizabeth Hurt |March 11, 2021 |Science News For Students 
Their uninjured companions also showed heightened sensitivity, and in both hind paws. Mice may ‘catch’ each other’s pain — and pain relief |Carolyn Wilke |January 12, 2021 |Science News 
For example, there’s a dog who was born without front legs and learned to walk on her hind legs. Your Brain Makes You a Different Person Every Day - Issue 91: The Amazing Brain |Steve Paulson |October 14, 2020 |Nautilus 
In another set of experiments, the team repeated the electrojabs on the mice’s hind legs, but at lower intensity. We Need New, Safer Ways to Treat Pain. Could Electroacupuncture Be One? |Shelly Fan |August 18, 2020 |Singularity Hub 
The tracks’ most surprising feature is that they only show hind feet. These crocodile ancestors lived a two-legged life |Carolyn Gramling |July 23, 2020 |Science News For Students 
This video shows a bear walking on its hind legs through a New Jersey neighborhood. Bear Walks Upright, ‘Apparently Kid,’ and More Viral Videos |The Daily Beast Video |August 10, 2014 |DAILY BEAST 
A taxidermic bear stands almost six feet tall on his hind legs with his mouth gaping in a never-ending silent roar. Half of This Bar Is in Slovenia, the Other Half Is in Croatia |Jeff Campagna |January 6, 2014 |DAILY BEAST 
So they took the hind legs of the animal and began to drag it the other way. The Ghosts of Israel’s Past |John Barry |September 14, 2011 |DAILY BEAST 
“They rise up on their hind legs when somebody is coming to attack their cubs,” Palin said. The 2010 Political Dictionary from A to Z |Samuel P. Jacobs |December 12, 2010 |DAILY BEAST 
Perhaps our job, post-election, is to provide a gentle but swift boot in the bee-hind of the party whose mascot is an ass. GOP Will Show No Mercy |Michael Moore |October 29, 2010 |DAILY BEAST 
For, at that moment Squinty stood up on his hind legs, as the boy had taught him, and walked over toward the big balloon basket. Squinty the Comical Pig |Richard Barnum 
Then, as Squinty remembered how he had been taught to stand up on his hind legs, he thought he would do that trick now. Squinty the Comical Pig |Richard Barnum 
Squinty dropped down on his four legs, since he found that walking on his hind ones brought him no food. Squinty the Comical Pig |Richard Barnum 
He usually seizes his prey by the flank near the hind leg, or by the throat below the jaw. Hunting the Lions |R.M. Ballantyne 
Sometimes he went on four feet and sometimes he stood up straight on his hind feet. Squinty the Comical Pig |Richard Barnum