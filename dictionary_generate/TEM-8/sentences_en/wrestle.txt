She squinted, blinked sporadically, and tilted her head, as if straining to wrestle answers from her brain. Jeopardy! Champion Julia Collins’s Brain Feels Like Mush |Sujay Kumar |November 20, 2014 |DAILY BEAST 
Regardless of how one wrestles with Noam Chomsky, one does always wrestle, leaving the bout much smarter and stronger. Noam Chomsky—Infuriating and Necessary |David Masciotra |September 28, 2014 |DAILY BEAST 
“We do have to wrestle with the fact that the army already has them on the ground,” she said. Iran Warns Obama to Stay Out of Iraq |Eli Lake |September 18, 2014 |DAILY BEAST 
A recently released app invites users to wrestle with the text of a modern classic. The ‘Slow Web’ Movement Will Save Our Brains |Joshua Rivera |June 16, 2014 |DAILY BEAST 
Erudite is trying to wrestle control of the government away from Abnegation via nefarious schemes. Exclusive: Shailene Woodley On ‘Divergent,’ J. Law, and Why She Turned Down ‘Fifty Shades of Grey’ |Marlow Stern |March 7, 2014 |DAILY BEAST 
The children never wrestle or pull each other about, either in sport or earnest. A Woman's Journey Round the World |Ida Pfeiffer 
The Widow crept noiselessly out of the room, and left her to wrestle with her grief as she could. The Atlantic Monthly, Volume 17, No. 101, March, 1866 |Various 
I had the right to expect from her the mild interest attached to those who wrestle with their misfortune. Balsamo, The Magician |Alexander Dumas 
A most trying thing it was to a man who carried the burden of the future in his soul—to have to wrestle with an obstinate stomach! Love's Pilgrimage |Upton Sinclair 
He had not a sufficient grasp upon his mighty subject—nor for that matter had he freedom to get by himself and wrestle it out. Love's Pilgrimage |Upton Sinclair