There are minor syntactic errors throughout the study, presumably due to translation. Palestinian Kids’ PTSD Could Last Generations |Russell Saunders |August 18, 2014 |DAILY BEAST 
But stress has done more than articulate or unify sequences that in their own right imply a syntactic relation. Language |Edward Sapir 
In the isolating languages the syntactic relations are expressed by the position of the words in the sentence. Language |Edward Sapir 
Hence the 16th century shows a syntactic licence and freedom which distinguishes it strikingly from that of later times. Encyclopaedia Britannica, 11th Edition, Volume 9, Slice 6 |Various 
The purposes for which the processes are used are derivation, modification, and syntactic relation. On the Evolution of Language |John Wesley Powell 
Syntactic relation is the relation of the parts of speech to each other as integral parts of a sentence. On the Evolution of Language |John Wesley Powell