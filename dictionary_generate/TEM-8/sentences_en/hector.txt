There was one card signed "Hector," but I don't know anyone named Hector! Hints From Heloise: Pop-up ads anger reader |Heloise Heloise |May 31, 2021 |Washington Post 
Outside, they killed Hector McMillan, a Canadian missionary, before joining the ranks of the fleeing rebels. ‘Argo’ in the Congo: The Ghosts of the Stanleyville Hostage Crisis |Nina Strochlic |November 23, 2014 |DAILY BEAST 
“The United States v. Hector Monsegur,” Judge Loretta Preska said. How Sabu the Hacker Rat Manipulated a Good-Hearted Judge |Michael Daly |May 28, 2014 |DAILY BEAST 
On Tuesday, it was Hector Pagan, ex-husband of Mob Wives star Renee Graziano. 'Mob Wives’ Courtroom Drama Exposes Rat, But Protects Jury |Michael Daly |March 12, 2014 |DAILY BEAST 
“Then maybe you should take up laundering yourself, Billy,” says Hector quietly. Britain’s KGB Sugar Daddy |Michael Weiss |March 7, 2014 |DAILY BEAST 
One celebrated English author has gone that far already: “Plus another thing, Hector!” Britain’s KGB Sugar Daddy |Michael Weiss |March 7, 2014 |DAILY BEAST 
He guessed that the request was connected with Hector Caron's death; and, of course, gave his consent. Mrs. Falchion, Complete |Gilbert Parker 
Justine tried hard for composure, and answered gently: "I loved my brother Hector." Mrs. Falchion, Complete |Gilbert Parker 
For Hector ran away from a single man; this hero was never known to run away at all. It Is Never Too Late to Mend |Charles Reade 
Major Hector Munro took command of the British army, and found it in a mutinous condition; desertions to the enemy were frequent. The Political History of England - Vol. X. |William Hunt 
And then, dear Hector, if you only knew how some people's tempers have altered since you went away! Autobiographical Reminiscences with Family Letters and Notes on Music |Charles Gounod