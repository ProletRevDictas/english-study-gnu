In Greek mythology, the species became associated with numerous gods. The History of the Chicken: How This Humble Bird Saved Humanity |William O’Connor |December 27, 2014 |DAILY BEAST 
The Greek embassy confirmed the death, which has barely registered by the international press. Riding Thailand’s WWII Death Railway |Liza Foreman |December 21, 2014 |DAILY BEAST 
That distant whirring sound you hear is a long-dead Greek physician spinning in his grave. Why So Many Surgeons Are Psychos |Russell Saunders |December 17, 2014 |DAILY BEAST 
In 1984, gay men were not openly accepted in Southern Greek culture. I Was Gang Raped at a UVA Frat 30 Years Ago, and No One Did Anything |Liz Seccuro |December 16, 2014 |DAILY BEAST 
The root of the word irony is in the Greek eironeia, “liar.” Up to a Point: They Made Me Write About Lena Dunham |P. J. O’Rourke |December 13, 2014 |DAILY BEAST 
John of Damascus, an important Greek theologian of the eighth century, often cited by Thomas. The Mediaeval Mind (Volume II of II) |Henry Osborn Taylor 
The Greek character will now become easy to decipher; and the evening papers may take King Otho both off the throne and on. Blackwood's Edinburgh Magazine, No. CCCXXXIX. January, 1844. Vol. LV. |Various 
Just got a telegram saying that M. Venezelos has gained a big majority in the Greek Election. Gallipoli Diary, Volume I |Ian Hamilton 
The fact that the earth was globular in form was understood by the Greek men of science. Outlines of the Earth's History |Nathaniel Southgate Shaler 
In Greek especially she was proficient, and Plato was to her more interesting than any story book. The Childhood of Distinguished Women |Selina A. Bower