Fishergate Baptist Chapel has an excellent interior, and it will accommodate about twice as many people as patronise it. Our Churches and Chapels |Atticus 
We were too apt to patronise scholarship winners, as though a scholarship was toffee given as a reward for virtue. The New Machiavelli |Herbert George Wells 
Punch is a beverage I don't patronise; it makes a man's hand shaky. Frank Fairlegh |Frank E. Smedley 
They are generally stout, piggy-faced gentlemen, who eat hearty suppers, and patronise free-and-easys. Bentley's Miscellany, Volume II |Various 
Fashion, or perhaps the hall-mark of the "gentleman" of those days, dictated that he should patronise art. Cathedral Cities of Italy |William Wiehe Collins