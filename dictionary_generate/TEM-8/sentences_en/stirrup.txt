I took my feet out of the stirrups and let the reins rest loosely on Molly’s neck. Navigating the World with a Terrible Sense of Direction |awise |July 12, 2021 |Outside Online 
But the jar threw my six-shooter where I couldn't reach it, and the carbine was jammed in the stirrup-leather on the wrong side. Raw Gold |Bertrand W. Sinclair 
So while the lady rode ahead, Richard galloped stirrup to stirrup with the Spaniard. God Wills It! |William Stearns Davis 
Even our men grew silent, and the ring of sword on stirrup seemed too loud to be natural at last. A Prince of Cornwall |Charles W. Whistler 
Louis de Valmont had been half lifted from his saddle; one foot had lost its stirrup; but Longsword sat as a tower. God Wills It! |William Stearns Davis 
"There's another pistol shot if you move," cried Seth, with one foot in the stirrup. The Light That Lures |Percy Brebner