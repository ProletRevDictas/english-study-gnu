Anatoliy slapped me on the shoulder and told me he knew it all along—all I had needed to do was change my attitude. The quest to snare—and save—the world’s largest owl |Jonathan Slaght |August 28, 2020 |Popular-Science 
Leashes that attach at the back of a dog’s shoulders only reinforce pulling, Perry says. The right way to walk your dog |John Kennedy |August 26, 2020 |Popular-Science 
My shoulders, which have irked me for months, have ceased to do so. The Gym-Free Pandemic Workout: Kettlebells, Indian Clubs, Sandbags, Oh My! |Eugene Robinson |August 25, 2020 |Ozy 
They didn’t stand shoulder to shoulder but had separate sandboxes set up. Morning Report: About Those Private Day Camps … |Voice of San Diego |August 18, 2020 |Voice of San Diego 
Todays’ POS systems offer in-built SEO tools in your point of sale system taking much of the burden off your shoulders. SEO tips for your retail store |Content Media |August 17, 2020 |Search Engine Watch 
So I just patted him kind-like on the shoulder and sat down. The Story Behind Lee Marvin’s Liberty Valance Smile |Robert Ward |January 3, 2015 |DAILY BEAST 
But Liberty is always dipping his shoulder, whirling around. The Story Behind Lee Marvin’s Liberty Valance Smile |Robert Ward |January 3, 2015 |DAILY BEAST 
“I only touched his shoulder,” the pastor told sheriffs, according to the police report. Exposed: The Gay-Bashing Pastor’s Same-Sex Assault |M.L. Nestel |December 21, 2014 |DAILY BEAST 
Her mother, pregnant at the time of the killing, was hit in the shoulder by a bullet from the same gun that killed her son. Drawing on the Memories of Syrian Women |Masha Hamilton |November 26, 2014 |DAILY BEAST 
According to Ibrahim, the police officers grabbed him by the shoulder and struck him in the face. In Jerusalem Home Demolitions, the Biblical Justice of Revenge |Creede Newton |November 25, 2014 |DAILY BEAST 
She was holding the back of her chair with one hand; her loose sleeve had slipped almost to the shoulder of her uplifted arm. The Awakening and Selected Short Stories |Kate Chopin 
I only saw the glitter of a bayonet which a Mexican thrust into his shoulder, at the very moment he was helping me up. Blackwood's Edinburgh Magazine, No. CCCXXXIX. January, 1844. Vol. LV. |Various 
For a moment Joe stood behind her, silently, looking over her shoulder at the signature of Isom Chase. The Bondboy |George W. (George Washington) Ogden 
Uncle David nodded, and waved his hand, as on entering the door he gave them a farewell smile over his shoulder. Checkmate |Joseph Sheridan Le Fanu 
Rising at once he bundled up his traps, threw the line of his small hand-sledge over his shoulder, and stepped out for home. The Giant of the North |R.M. Ballantyne