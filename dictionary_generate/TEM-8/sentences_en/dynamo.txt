Sure, it may not be crazy fast, but this pint-size dynamo is still plenty frisky. Two-door turn-ons |Joe Phillips |August 20, 2021 |Washington Blade 
The moon’s core is “really small,” says John Tarduno, a geophysicist at the University of Rochester in New York, and it’s not clear how that core could have sustained a dynamo for long before cooling. A lunar magnetic field may have lasted for only a short time |Carolyn Gramling |August 4, 2021 |Science News 
In his previous life as a superstar with the Lob City Clippers, Griffin was an offensive dynamo. Blake Griffin Was A Luxury Pickup. Now The Nets Need Him. |Louis Zatzman |June 15, 2021 |FiveThirtyEight 
Once a warm, wet world, Mars lost its magnetic field more than 4 billion years ago when its outer core cooled, shutting off the dynamo that kept the field in place. Mars Has Much More Water Than Previously Known—But There's a Catch |Jeffrey Kluger |March 16, 2021 |Time 
She isn’t the dramatic dynamo of the fashion industry’s imagination. The sound of a shifting power structure |Robin Givhan |January 13, 2021 |Washington Post 
Dynamo is a platform that gives Turkers a collective voice and, consequently, the chance to drive change. Amazon’s Turkers Kick Off the First Crowdsourced Labor Guild |Kevin Zawacki |December 3, 2014 |DAILY BEAST 
And the chrome-domed 47-year-old has been a sports commentating dynamo from Jump Street. World Cup Anchor Mike Tirico’s Bizarre History: Reports of Stalking and Sexual Harassment |Marlow Stern |July 1, 2014 |DAILY BEAST 
Wendi Murdoch, a glamorous dynamo whom I have always found extremely engaging, was the harbinger of this billionaire dating trend. Tina Brown on Sex, Decorating, and Divorce for Billionaires |Tina Brown |July 8, 2013 |DAILY BEAST 
West is a dynamo, a fearsome warrior who quotes classic Greek with a warm, Southern charm. 5 Lessons from Tuesday's Primaries |Mark McKinnon |August 25, 2010 |DAILY BEAST 
What hidden dynamo torqued his professional engine with such relentless efficiency? The Svengali of Pop Art |Annie Cohen-Solal |May 13, 2010 |DAILY BEAST 
It was the face of a man who ran his mental dynamo at top speed in defiance of nature's laws against speeding. Scattergood Baines |Clarence Budington Kelland 
Everybody but the dynamo-watch lay steeped in sleep; there was no sound. A Connecticut Yankee in King Arthur's Court, Complete |Mark Twain (Samuel Clemens) 
He caught her frail body in his great grasp, and she vibrated like a bit of wire caught up by a dynamo. Jaffery |William J. Locke 
I passed the Jefe myself on the City Hall steps, and heard him b-r-r-ring like a dynamo. The Belted Seas |Arthur Colton 
The motor and dynamo are mounted on a heavy wood base, which in turn is firmly bolted to a concrete foundation. The Boy Mechanic, Book 2 |Various