“Magical, right,” then-pitching coach Paul Menhart said, meaning the aura over the whole camp. Nothing can spoil the first day of spring training. (Though covid sure is giving it a shot.) |Thomas M. Boswell |February 18, 2021 |Washington Post 
Keeping them in the light is supposed to deplete their aura or something. Al Cohen, D.C. magic shop proprietor who knew all the tricks, dies at 94 |Matt Schudel |December 18, 2020 |Washington Post 
Despite efforts like relaunching Reebok Classics a few years ago, the brand never regained much of its cool aura, retro or otherwise. Why Adidas is finally putting Reebok up for sale |Phil Wahba |December 14, 2020 |Fortune 
Yet, even as the club became more accessible, it retained its aura of indulgence. Long Live the Room-Service Club Sandwich |Rafael Tonon |October 9, 2020 |Eater 
Perhaps above all else, Kipchoge benefited from his own aura of invincibility. Eliud Kipchoge’s Streak Comes to an End in London |Martin Fritz Huber |October 5, 2020 |Outside Online 
Most of the Atari employees I saw projected an aura of almost delirious bliss. ‘Asteroids’ & The Dawn of the Gamer Age |David Owen |November 29, 2014 |DAILY BEAST 
The friendly aura vanished, her eyes dead, voice robotic and confident she was correct. Jeopardy! Champion Julia Collins’s Brain Feels Like Mush |Sujay Kumar |November 20, 2014 |DAILY BEAST 
They shared a birthday—July 8—and Nelson always thought that bestowed some sort of personal aura on him. This Republican Loved Taxes & Modern Art |Scott Porch |November 19, 2014 |DAILY BEAST 
It gives them all aura, a collective power, an almost animal force. War Is Hell and Such Good Fun |Don Gomez |November 11, 2014 |DAILY BEAST 
He was, in fact, of average height, but he had an aura like a pope or a head of state. Ben Bradlee Was the Last of the Newspaper Giants |Tom Shales |October 22, 2014 |DAILY BEAST 
Some hidden magnetism burst from him like an aura, and his cold pasty face and light gray eyes flamed into positive beauty. Ancestors |Gertrude Atherton 
Celuy pourra savoir l'aise qu'en avons receu et recevons, qui aura cogneu les dangers et necessits o nous estions. The Jesuit Relations and Allied Documents, Vol. II: Acadia, 1612-1614 |Various 
I suppose there was an aura, a halo, some sort of effulgency about the place; for here I find you louder than the rest. The Works of Robert Louis Stevenson - Swanston Edition Vol. 25 (of 25) |Robert Louis Stevenson 
Ma foi, je serais fch de le savoir, et je crois que l'auteur aura sagement fait de ne mettre personne dans son secret. Baron d'Holbach |Max Pearson Cushing 
It might have been the aura of the two exquisite women, he thought. Mary Gray |Katharine Tynan