Hence, both the Vey and the Cherokee, the two latest coinages in the way of alphabets, are both syllabic. The Ethnology of the British Colonies and Dependencies |Robert Gordon Latham 
In prism, schism the m may have syllabic value; it then does the work usually performed by a vowel. The Sounds of Spoken English |Walter Rippmann 
In English, the syllable is generally carried by vowels; sometimes also by liquids and nasals, which are then called syllabic. The Sounds of Spoken English |Walter Rippmann 
No letters can represent the nasal intonation of this syllabic inquiry, and no words the supreme indifference of the boy's tone. Corporal Cameron |Ralph Connor 
"To Morvah," explained Enid, with a syllabic emphasis meant for one pair of ears. The Pillar of Light |Louis Tracy