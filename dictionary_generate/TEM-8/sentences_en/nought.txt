If the capital letter S were cut into two parts, and the bottom half attached to the top half, it would make a nought . Assimilative Memory |Marcus Dwight Larrowe (AKA Prof. A. Loisette) 
It was a curious chapter of accidents that brought all these well laid plans to nought. The Eve of the Revolution |Carl Becker 
Fortune had favoured me far above my expectations, and I saw nought before me but a career of distinction under my new master. Confessions of a Thug |Philip Meadows Taylor 
Between the two on the hills is the great camp which any force can hold, but nought but a great one can storm. A Prince of Cornwall |Charles W. Whistler 
After that the men of Gerent who were with them at the camp cared nought for their strange leader. A Prince of Cornwall |Charles W. Whistler