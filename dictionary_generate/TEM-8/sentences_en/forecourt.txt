One crew member waited four hours on a forecourt for a tanker to arrive so that he could guarantee getting filled up. U.K.’s gas panic-buying nightmare pushes more employers to adopt hybrid working and commuting setups |Jessica Davies |October 7, 2021 |Digiday 
The guard is first seen marching up and down the gravel forecourt, before breaking into pirouettes. Hunt To Identify Pirouetting 'Bearskin' Guardsman Who Shamed Army |Tom Sykes |September 3, 2014 |DAILY BEAST 
Colonel Sullivan turned with Uncle Ulick to the nearest window and looked out on the untidy forecourt. The Wild Geese |Stanley John Weyman 
Uncle Ulick shook his fist at a particularly importunate beggar who had ventured across the forecourt. The Wild Geese |Stanley John Weyman 
They drove to a modest suburb of the great ingenious town, and stopped at the forecourt of a small house. Mugby Junction |Charles Dickens 
There was no forecourt to the house; passers-by walked close to the windows; they could look in if they tried. Name and Fame |Adeline Sergeant 
The sound of carriage wheels on the gravelled forecourt in the front of the house suddenly awakened her drowsy senses. El Dorado |Baroness Orczy