“We always say addiction is an equal opportunity affliction,” Skipper said. Busted for Heroin: When Pilots Fly High |Caitlin Dickson |July 23, 2014 |DAILY BEAST 
“The minute they blow, you can call the airline and pull them off flight status,” Skipper said. Busted for Heroin: When Pilots Fly High |Caitlin Dickson |July 23, 2014 |DAILY BEAST 
After 48 hours at the helm, the skipper knew he needed to rest, so Lennon would have to steer for a while. How John Lennon Rediscovered His Music in Bermuda |The Telegraph |November 3, 2013 |DAILY BEAST 
Scott carried on as skipper of the station, with its 10-hour work days, while assuming the added duty of concerned brother. Scott Kelly: Gabrielle Giffords' Brother-in-Law in Space |Peter J. Boyer |February 18, 2011 |DAILY BEAST 
We do what we can when Skipper loses her lunch money, pay for college if we can, help out with the first-day-at-work suit. 'Mom, Dad—I Need $96K' |Kathleen Parker |July 10, 2009 |DAILY BEAST 
Kielland's third novel, "Skipper Worse," marked a distinct step in his development. Skipper Worse |Alexander Lange Kielland 
Skipper Worse growled a little and rubbed his head, when Sivert Gesvint pressed his hand and welcomed him with effusion. Skipper Worse |Alexander Lange Kielland 
Sarah bashfully welcomed Skipper Worse, who patted her on the head; he had known her ever since she was a small child. Skipper Worse |Alexander Lange Kielland 
Skipper Worse could not understand what they were talking about, and he became very weary. Skipper Worse |Alexander Lange Kielland 
In a moment Skipper Worse was wide awake, and began to hum, as she moved her fingers along the lines. Skipper Worse |Alexander Lange Kielland