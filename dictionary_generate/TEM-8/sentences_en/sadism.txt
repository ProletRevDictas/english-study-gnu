When you research wars, for example, you find that most soldiers didn’t fight in the name of sadism or some kind of ideology—they just didn’t want to let their friends down. Rutger Bregman on Why He Has Faith in Humanity |Ryan Krogh |May 11, 2021 |Outside Online 
Dodson’s adventures in Don Draper-land are a welcome antidote to the all-too-vivid sadism of the serial killer loose in Coronado Springs. Joe Ide’s IQ series continues with the idiosyncratic marvel ‘Smoke’ |Maureen Corrigan |February 26, 2021 |Washington Post 
Julian Fellowes can save his show by offing the target of his sadism. Just Kill Mr. Bates Already! How to Save ‘Downton Abbey’ |Andrew Romano |February 20, 2014 |DAILY BEAST 
Obama processes his sadism to the point where he gets somewhat more acceptable. Obama Has a Mean Streak and He Turned It on Romney This Week |Lloyd Grove |May 26, 2012 |DAILY BEAST 
The over-reaction, the lack of professionalism, the random sadism of this boggles the mind. UC Davis Suspends Pepper-Spray Cops (Video) | |November 20, 2011 |DAILY BEAST 
But the existence of sadism is a part of the human condition that is both universal and universally hard to acknowledge. Why Obama Is in Denial |Justin Frank |April 26, 2009 |DAILY BEAST 
But we must confront our sadism to protect ourselves from it. Why Obama Is in Denial |Justin Frank |April 26, 2009 |DAILY BEAST 
Sadism, as we know, played a prominent part in both the French and Russian revolutions. Secret Societies And Subversive Movements |Nesta H. Webster 
I suspect that under many of our professed principles there lurk elements of unconscious sadism and masochism. The Behavior of Crowds |Everett Dean Martin 
Thus in a great many quite famous works of art there will be found an element of sadism. The Complex Vision |John Cowper Powys 
Sadism is for a long time restrained by fear, education or moral sentiments. The Sexual Question |August Forel 
Sadism appears to be most often an effect of hereditary alcoholic blastophthoria. The Sexual Question |August Forel