More recently, Specialized just recalled all of its new Tarmac SL7s for a similar issue with the compression plug. Integrated Road Bikes Perform Better, but at What Cost? |agintzler |December 3, 2021 |Outside Online 
He saluted the Marines standing guard on the tarmac with his right hand—the hand with the coffee cup in it. Obama, the Coffee Salute, and the Dementia on the Right |Sally Kohn |September 25, 2014 |DAILY BEAST 
Only 101 lives were lost by the two air forces and most of those were not caused by crashes but by accidents on the tarmac. Blood and War: The Hard Truth About ‘Boots on the Ground’ |Clive Irving |September 22, 2014 |DAILY BEAST 
We all grabbed our gear, lined up, and walked across the tarmac. How I’ll End the War: My First Week Back in Afghanistan |Nick Willard |May 1, 2014 |DAILY BEAST 
Upon landing, he was reportedly spotted wandering the tarmac with only a comb in his pocket. How to Hitchhike a Plane—and Survive |Kent Sepkowitz |April 22, 2014 |DAILY BEAST 
The couple were met on the tarmac by Tony Abbott, the prime minister. New Pictures As Prince George Arrives in Australia |Tom Sykes |April 16, 2014 |DAILY BEAST 
Something was squatting on the tarmac close to the petrol tank. Berry And Co. |Dornford Yates 
Mechanics were climbing down out of cockpits and walking along down the tarmac in groups. Dave Dawson with the R.A.F |R. Sidney Bowen 
His head snapped back, his feet left the ground, and he did a beautiful backward somersault to crash down on the tarmac in a heap. Dave Dawson with the R.A.F |R. Sidney Bowen 
Half an hour later the two air aces were out on the tarmac, and ready to leave. Dave Dawson with the Commandos |R. Sidney Bowen 
He had done so, and now stood on the dark tarmac between the manifest shed and the pilot-barracks. World Beyond Pluto |C. H. Thames