On the way down he identifies trees by which needles are best to sleep on: Balsam fir is good. Pete Dexter’s Indelible Portrait of Author Norman Maclean |Pete Dexter |March 23, 2014 |DAILY BEAST 
The Daily Pic: Monika Sosnowska plants her "Fir Tree" in Central Park. A Climb-Down for Art |Blake Gopnik |November 27, 2012 |DAILY BEAST 
I took everything you taught me, flushed it down the toilet, and bought a pine fir hoping it would bring me spiritual nourishment. Jesus Made Me a Better Jew |Benyamin Cohen |December 24, 2008 |DAILY BEAST 
He saw the tips of the fir trees shimmer, and heard them whisper as the breeze turned their needles towards the light. Three More John Silence Stories |Algernon Blackwood 
I did not see any others at that time amongst the fir-trees, though no doubt a few others were there. Birds of Guernsey (1879) |Cecil Smith 
I will hear him, and I will make him flourish like a green fir tree: from me is thy fruit found. The Bible, Douay-Rheims Version |Various 
The sayling firr; this 'alludes to the ship's masts and spars being made of fir.' Chaucer's Works, Volume 1 (of 7) -- Romaunt of the Rose; Minor Poems |Geoffrey Chaucer 
In one hand he carried a peevie, a big wooden lever with an iron hook on it, such as men use in rolling fir logs. The Gold Trail |Harold Bindloss