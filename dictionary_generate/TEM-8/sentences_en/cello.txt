Classically trained in piano and cello, Fisher had also picked up the guitar as a teenager. Meet the OZY Genius Who’s Making Arts Programs More Accessible |Eugene Robinson |July 27, 2021 |Ozy 
The same goes for handling a bicycle, riding a wave, growing a garden, or playing the cello. These 7 Dog-Training Principles Work for Humans, Too |cobrien |July 14, 2021 |Outside Online 
They played, respectively, a cello, djembe drum, and floor tom drum. What Makes Music Universal - Issue 99: Universality |Kevin Berger |April 29, 2021 |Nautilus 
Yo-Yo Ma started sharing cello solos he called “Songs of Comfort” during early pandemic days. The Livestream Show Will Go On. How COVID Has Changed Live Music—Forever |Raisa Bruner |March 30, 2021 |Time 
Boynton wrote, illustrated, and directed the creation of the Jungle Night video, while Ma used his cello to bring to life the sounds of snoozing jungle creatures. Sandra Boynton and Yo-Yo Ma in Conversation With a Kid Reporter About Their New Collaboration Jungle Night |TIME for Kids |March 23, 2021 |Time 
Only one of each: one bass, one cello, like a string quintet. Meet Alexandre Desplat, Hollywood’s Master Composer |Andrew Romano |February 11, 2014 |DAILY BEAST 
In full disclosure, I play the cello, and it is my favorite instrument. Mark Kurlansky’s Book Bag: 5 Essential Music Reads |Mark Kurlansky |July 9, 2013 |DAILY BEAST 
We teach them everything from how to mic their cello to how to book a gig. Mother Falcon the 18-Piece Indie Symphonic Rock Band Taking Texas By Storm |Abby Haglage |June 2, 2013 |DAILY BEAST 
My roommate, a talented musician and mathematician, gave up the cello and took a job as an investment banker. Did My Education Cost Too Much? |Jessica Feldman |September 12, 2012 |DAILY BEAST 
The cello adds an elegant dramatic touch to a song many have found rather devoid of emotion. 10 Best and Worst “Call Me Maybe” Covers |Caleb Baer |July 10, 2012 |DAILY BEAST 
Anne-Marie lifts her right arm slowly, and strikes the low G—a long vibrating note, like the note of a 'cello. The Devourers |Annie Vivanti Chartres 
It was not simply that his 'cello was his joy and pride, but he felt it to be a recognition of his return to respectability. Black Rock |Ralph Connor 
Dr. Vereker won't have a cab; he will leave the 'cello till next time, and walk. Somehow Good |William de Morgan 
If one has a few pieces (violin, cello, bass viol, flute) to play Hungarian airs during the dinner it will please the guests. Suppers |Paul Pierce 
Short-distance travel with a 'cello is not much more agreeable. Atlantic Classics |Various