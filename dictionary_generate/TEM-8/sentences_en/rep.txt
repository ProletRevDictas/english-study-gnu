Reached Tuesday, a Sitrick Co. rep confirmed they parted ways with Epstein in April 2011. Sleazy Billionaire’s Double Life Featured Beach Parties With Stephen Hawking |M.L. Nestel |January 8, 2015 |DAILY BEAST 
“I truly believe there is a Santa Claus” said Rep. Kerry Bentivolio. Kerry Bentivolio: The Congressman Who Believes in Santa Claus |Ben Jacobs |December 24, 2014 |DAILY BEAST 
On the Democratic side, many expect former Rep. Mike McMahon to make another run at the seat. The Felon Who Wouldn’t Leave Congress |Ben Jacobs, David Freedlander |December 23, 2014 |DAILY BEAST 
As Rep. Joe Crowley of New York told reporters, “There was no right answer and no wrong answer.” ‘Cromnibus’ Passes, But Did Anyone Win? |Ben Jacobs |December 12, 2014 |DAILY BEAST 
At that time, though, my job as a rep for an Australian company made it impossible to leave Australia. Ted Hughes’s Brother on Losing Sylvia Plath |Gerald Hughes |December 2, 2014 |DAILY BEAST 
It was the era of black walnut and green rep, and they chose sets looking much alike. Girls and Women |Harriet E. Paine (AKA E. Chester} 
When Charles wanted a mistress he went far afield; in his hunt he bagged anything that came his way, from a duchess to a demi-rep. Court Beauties of Old Whitehall |W. R. H. Trowbridge 
You ought to see ithorrid old brown rep, and a carpet thats worn into white spots! Winona of the Camp Fire |Margaret Widdemer 
The weave is similar to that of ordinary rep or poplin, being a simple round-corded effect. Textiles |William H. Dooley 
Rep, Repp, rep, n. a kind of cloth having a finely corded surface. Chambers's Twentieth Century Dictionary (part 3 of 4: N-R) |Various