There’s Robert Horry, the dead-eye three-point gunner whose penchant for late-game heroics is legend. Kobe Bryant Punched A Teammate Over $100, And It Wasn’t Shaq |Jeff Pearlman |September 25, 2020 |FiveThirtyEight 
One day, he took a heroic dose of magic mushrooms and climbed to the top of a tall tree, where he was trapped by a lightning storm. The Fungal Evangelist Who Would Save the Bees - Issue 90: Something Green |Merlin Sheldrake |September 23, 2020 |Nautilus 
The Oscar-nominated movie Hotel Rwanda was inspired by Rusesabagina’s heroics. Butterfly Effect: Pandemic Shrouds Terrorist Activities |Charu Kasturi |September 3, 2020 |Ozy 
I’m not trying to make it sound heroic, our neighborhood is just a small part of the Bonny Doon community. How We Saved Our Neighborhood from a Wildfire |Dain Zaffke |September 2, 2020 |Outside Online 
Similarly heroic stories abound across all the businesses, teams and leaders included in this year’s shortlist. Shopify, Chewy and Ally are Resilience Awards finalists |Digiday Awards |September 1, 2020 |Digiday 
Sometimes they do heroic things, sometimes they wish they could. After The Fall: Introducing The Anti-Villain |Rich Goldstein |December 21, 2014 |DAILY BEAST 
Claret for boys, port for men, and brandy for heroes, according to Dr. Johnson, and Hitch went for the heroic. Alfred Hitchcock’s Fade to Black: The Great Director’s Final Days |David Freeman |December 13, 2014 |DAILY BEAST 
Hamas called the attack “heroic” and, disgustingly, tweeted out photos of the bodies. Mass Murder in the Holy City |Michael Tomasky |November 18, 2014 |DAILY BEAST 
It is this kind of abstraction that leads to more mythology, more heroic narratives, more undertones of patriotic martyrdom. War Is About More Than Heroes, Martyrs, and Patriots |Nathan Bradley Bethea |November 12, 2014 |DAILY BEAST 
Some heroic volunteers, like Spencer, have joined the fight—but not enough. Why New York’s Ebola Case Will Hurt Infected Patients Everywhere |Abby Haglage |October 24, 2014 |DAILY BEAST 
Again, she was present at the battle of Silan, where her heroic example of courage infused new life into her brother rebels. The Philippine Islands |John Foreman 
It is not likely that the inhabitants of Ivrea, who thus commemorate her heroic deed, will ever forget their Mugnaia. The Pit Town Coronet, Volume I (of 3) |Charles James Wills 
All men are not heroes, and in many countries men may become average hunters without being particularly heroic. Hunting the Lions |R.M. Ballantyne 
We shook each other by the hand, and congratulated one another mutually, as if we had done some great and heroic deed. A Woman's Journey Round the World |Ida Pfeiffer 
He was in love, and, being very young, wanted to do something insanely heroic. Uncanny Tales |Various