Machines will inject argon gas between the two canisters to provide an inert atmosphere, and the copper cask will be welded shut. The World’s First Deep Geological Nuclear Vault Will Store Radioactive Waste in Finland for 100,000 Years |Vanessa Bates Ramirez |March 25, 2022 |Singularity Hub 
As fire fell through the hatchway, the men set a cask into the cellar and spread a train of powder across the floor. The Great Fire of 1835 Helped Create Modern New York City |Daniel S. Levy |March 11, 2022 |Time 
Well, for one, maple cask bourbon whiskey after a day on the slopes. The 25 Best Winter Trips in the World |jversteegh |October 15, 2021 |Outside Online 
He uses traditional ex-bourbon casks to age the flagship Puni Gold and ex-Pedro Ximénez sherry barrels to make Sole. Six great whiskies from around the world |Liza Weisstuch |March 11, 2021 |Washington Post 
The resulting product included four single-cask variants along with finished pictures of McKidd enjoying a glass of The Macallan. The Restaurant, Flask, And Photography Worthy of The Macallan Whisky | |December 16, 2014 |DAILY BEAST 
A bottle of The Glenlivet, aged in the cask longer than Poppet and Buster put together. The Ridiculousness of Father's Day |P. J. O’Rourke |June 15, 2014 |DAILY BEAST 
I learned that day of a process called “dry cask storage” that seems to offer a safer alternative. A Fix for Indian Point's Spent Nuclear Fuel Rods |Jonathan Alter |March 23, 2011 |DAILY BEAST 
Age whiskey in a sherry cask and it takes on flavor from the wood. Coffee's Dirty Little Secret |Michael Meyer |August 18, 2009 |DAILY BEAST 
Little did Tressan dream to what a cask of gunpowder he was applying the match of his smug pertness. St. Martin's Summer |Rafael Sabatini 
Near the stream we found some felled trees and the staves of a cask. Narrative of a Survey of the Intertropical and Western Coasts of Australia] [Volume 2 of 2] |Phillip Parker King 
At this point he lost his balance, and went rolling to leeward like an empty cask. The Floating Light of the Goodwin Sands |R.M. Ballantyne 
I could see that a powerful effort was needed to keep him off the vexed question of the cask of beer, but he made it. A Thin Ghost and Others |M. R. (Montague Rhodes) James 
No rattle responded; but the despairing fact became apparent: the cask was empty! A Fortune Hunter; Or, The Old Stone Corral |John Dunloe Carteret