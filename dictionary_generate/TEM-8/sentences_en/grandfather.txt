He rarely saw him or spoke to him when he was young, and his grandfather died while he was in college. He knew his grandfather was a mob boss. But was that the whole story? |Joe Heim |February 12, 2021 |Washington Post 
During that time, his stepfather and both grandfathers died, and he had to clean out their homes, too. How downsizing expert Matt Paxton helps Americans sort through their stuff |Jura Koncius |January 21, 2021 |Washington Post 
Many, many, many people on Twitter claimed Sanders as their uncle or grandfather. The handwarming story of how Bernie Sanders got his inauguration mittens |Travis Andrews |January 21, 2021 |Washington Post 
Ilya Samsonov’s grandfather loved the game, and in time, so did he. Ilya Samsonov, the Caps’ present and future in goal, is ready for the spotlight |Samantha Pell |January 14, 2021 |Washington Post 
Her grandfather lived around the corner from my grandfather. Why Padma Lakshmi Won’t Be Running for Office |Pallabi Munsi |December 28, 2020 |Ozy 
Earl Spencer adds, “Effectively, my great-grandfather sold his children to his father-in-law.” The Real-Life ‘Downton’ Millionairesses Who Changed Britain |Tim Teeman |December 31, 2014 |DAILY BEAST 
His grandfather, a pastor, had visited the church decades before—in the 1980s—when the church was popular within the community. Beaten By His Church for Being Gay |Justin Jones |December 16, 2014 |DAILY BEAST 
His great-grandfather, David Yellin, was a prominent Zionist scholar and Israeli pioneer. Trans in the Holy Land: ‘Marzipan Flowers,’ Tal Kallai, and the Shattering of Israel’s LGBT Taboos |Itay Hod |November 4, 2014 |DAILY BEAST 
Meanwhile, his grandfather was named Marion—also the birth name of John Wayne. ‘Archer’ Season 6 Preview: Cast and Crew on Rebranding and Dropping ISIS |Marlow Stern |October 27, 2014 |DAILY BEAST 
My grandfather lived fast and large—he liked his liquor and his tobacco, and he was also an ace gambler. Those Kansas City Blues: A Family History |Katie Baker |October 24, 2014 |DAILY BEAST 
He grew very restless, for it was a long time before Grandfather Mole appeared. The Tale of Grandfather Mole |Arthur Scott Bailey 
But she told Grandfather Mole that it was all right—that she knew a person of his age ought not to go without his breakfast. The Tale of Grandfather Mole |Arthur Scott Bailey 
And he had waited so long for Grandfather Mole that he had begun to feel hungry again. The Tale of Grandfather Mole |Arthur Scott Bailey 
One thing was certain: Grandfather Mole could travel much faster through the water than he could underground. The Tale of Grandfather Mole |Arthur Scott Bailey 
Then Jimmy remembered suddenly that he had to meet Grandfather Mole over there. The Tale of Grandfather Mole |Arthur Scott Bailey