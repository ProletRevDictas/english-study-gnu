A buyer, of course, can walk away if the appraisal reveals a massive discrepancy. Politics Report: Forged Footnote 15 |Scott Lewis and Andrew Keatts |September 12, 2020 |Voice of San Diego 
The discrepancy can be chalked up to a decline in the average selling price of the products. Low-cost fitness bands see a resurgence in interest amid the pandemic |Brian Heater |September 4, 2020 |TechCrunch 
There’s going to be discrepancies if you deal with 100 of something. Full Transcript: Tomi Lahren on ‘The Carlos Watson Show’ |Daniel Malloy |August 31, 2020 |Ozy 
The PPP loan program, however, has faced discrepancies in how loans were allocated across race and gender. Why women-owned businesses anticipate a tougher recovery ahead than their male-owned counterparts |ehinchliffe |August 26, 2020 |Fortune 
Those additional terms are expected to be too small to account for the discrepancy. A measurement of positronium’s energy levels confounds scientists |Emily Conover |August 24, 2020 |Science News 
Tweedy had gone online to research a small discrepancy in a pair of Mormon texts. God vs. the Internet. And the Winner is… |Michael Schulson |November 16, 2014 |DAILY BEAST 
"The discrepancy may have been that we really don't see black or white among our colleagues," Zoll said. Fact-Checking the Sunday Shows: August 17 |PunditFact.com |August 17, 2014 |DAILY BEAST 
By no means does this discrepancy indicate that Barnard is necessarily safer for women. No Rapes On Campus? No Way. |Emily Shire |July 5, 2014 |DAILY BEAST 
But there is a discrepancy in the way masturbation is discussed in regards to men and women. C’mon, Ladies, Masturbation Isn’t Just for Bad Girls |Emily Shire |June 19, 2014 |DAILY BEAST 
He claims in his complaint that the agency never clarified the discrepancy nor did it pay Shanklin the $35,000. How Models Get Robbed on the Runway |Jennifer Sky |May 7, 2014 |DAILY BEAST 
How, then, are we to explain this extraordinary discrepancy between human power and resulting human happiness? The Unsolved Riddle of Social Justice |Stephen Leacock 
There was, of course, a ridiculous discrepancy between this latter demand and the magnitude of his fortune. Juana |Honore de Balzac 
By this method a line can be reserved for each hand, and any discrepancy in the scores at once rectified. Encyclopaedia Britannica, 11th Edition, Volume 4, Part 3 |Various 
Westphal attributes this strange discrepancy to the accidental displacing of some words in the MSS. The Modes of Ancient Greek Music |David Binning Monro 
This consideration brings the two places into such close agreement that any hypothesis involving discrepancy is most improbable. The Modes of Ancient Greek Music |David Binning Monro