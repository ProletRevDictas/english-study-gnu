Thirty-six percent were in favor and 38 percent were opposed. How the PC Police Threaten Free Speech |Nick Gillespie |January 9, 2015 |DAILY BEAST 
It was a very faithful homage to a Six Million Dollar Man episode. ‘Archer’ Creator Adam Reed Spills Season 6 Secrets, From Surreal Plotlines to Life Post-ISIS |Marlow Stern |January 8, 2015 |DAILY BEAST 
I just recently rewatched all six Star Wars movies the other day… Oh wow, from the beginning? Patton Oswalt on Fighting Conservatives With Satire |William O’Connor |January 6, 2015 |DAILY BEAST 
After the six-week training, the forces will be deployed to confront the Islamic State, officials said. Pentagon Insider on New Plan to Fight ISIS: ‘Of Course It’s Not Enough’ |Nancy A. Youssef |January 6, 2015 |DAILY BEAST 
But the program is just six weeks long, the Pentagon admitted Monday. Pentagon Insider on New Plan to Fight ISIS: ‘Of Course It’s Not Enough’ |Nancy A. Youssef |January 6, 2015 |DAILY BEAST 
I take the Extream Bells, and set down the six Changes on them thus. Tintinnalogia, or, the Art of Ringing |Richard Duckworth and Fabian Stedman 
About this time the famous Philippine painter, Juan Luna (vide p. 195), was released after six monthsʼ imprisonment as a suspect. The Philippine Islands |John Foreman 
It contains above eighty thousand houses, and about six hundred thousand inhabitants. Gulliver's Travels |Jonathan Swift 
We had six field-pieces, but we only took four, harnessed wit twice the usual number of horses. Blackwood's Edinburgh Magazine, No. CCCXXXIX. January, 1844. Vol. LV. |Various 
The Seven-score and four on the six middle Bells, the treble leading, and the tenor lying behind every change, makes good Musick. Tintinnalogia, or, the Art of Ringing |Richard Duckworth and Fabian Stedman