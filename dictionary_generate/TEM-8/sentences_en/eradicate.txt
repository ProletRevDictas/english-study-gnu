No effective treatments were discovered by the time the disease was eradicated. Smallpox used to kill millions of people every year. Here’s how humans beat it. |Kelsey Piper |February 5, 2021 |Vox 
A farmer who aggressively eradicates all the insects on their land might be setting up an invisible desert for birds passing through. A new mapping method could help humans and wildlife coexist |Philip Kiefer |February 4, 2021 |Popular-Science 
Pakistan is one of the three countries in the world where polio has not been eradicated. How the CIA’s fake vaccine program in Pakistan helped fuel the anti-vax movement |Hala Iqbal |February 1, 2021 |Vox 
Unless the coronavirus is eradicated everywhere, she said, it will remain a threat to the United States and other countries. New coronavirus variants accelerate race to make sure vaccines keep up |Carolyn Y. Johnson, Laurie McGinley, Joel Achenbach |January 26, 2021 |Washington Post 
We’ll never go back to the way things were even if the virus is eradicated. ‘We’ll never go back to the way things were’: Confessions of a producer on in-person shoots |Kayleigh Barber |January 25, 2021 |Digiday 
We need Obama to follow through on his promise to eradicate it. The West’s Female-Genital Mutilation Wake-Up Call |Charlotte Lytton |October 20, 2014 |DAILY BEAST 
It might take us centuries to eradicate the sexism that powers the harassment of women on a cultural level. Will the Internet Ever Be Safe for Women? |Samantha Allen |August 28, 2014 |DAILY BEAST 
First impressions are tough to eradicate—especially in the cutthroat world of Hollywood. Brooklyn Decker on Her ‘Horrible’ Modeling Experiences, Marriage, and Cracking Hollywood |Marlow Stern |April 11, 2014 |DAILY BEAST 
Erdogan had announced the move in a speech on Thursday, vowing to “eradicate Twitter.” Turkey’s Useless Twitter Ban |Thomas Seibert |March 21, 2014 |DAILY BEAST 
“We will eradicate Twitter”, he said during a campaign speech on Thursday. Turkey’s Useless Twitter Ban |Thomas Seibert |March 21, 2014 |DAILY BEAST 
You cannot all at once eradicate the deep-rooted customs and habits of any people, whoever they may be. The Jesuit Relations and Allied Documents, Vol. II: Acadia, 1612-1614 |Various 
The country here is infested by guerillas, whom all our efforts cannot eradicate. Under Wellington's Command |G. A. Henty 
But he could not bequeath political capacity to his colleagues, nor could he eradicate many bad traditions of long standing. Argentina |W. A. Hirst 
I wanted to eradicate those twisted ideas, and make her good qualities her ruling ones. Children of the Whirlwind |Leroy Scott 
Indeed, even in our own day it has hardly been possible to eradicate from India the custom of burning the widow of the deceased. Elements of Folk Psychology |Wilhelm Wundt