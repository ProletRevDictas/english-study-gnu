She was lying on a couch, wearing a housecoat, and didn't look up from the magazine in front of her. The Odyssey of Sam Meecham |Charles E. Fritch 
She was wearing a gay housecoat of Betty's, too tight in just the right places. Sugar Plum |Reginald Bretnor 
Her hair was uncombed and she wore a brilliant red housecoat. Hoofbeats on the Turnpike |Mildred A. Wirt 
Unshaven, in a housecoat and bare feet, with long toenails and red eyes. Little Brother |Cory Doctorow 
He was tall and fit and muscular, his brown calves flashing through the vent of his housecoat. Someone Comes to Town, Someone Leaves Town |Cory Doctorow