How lovely, than, that one Internet denizen took the time to help these forgotten statesmen find sexual satiation…with each other! Fifty Shades of Presidential FanFiction |Amy Zimmerman |August 2, 2014 |DAILY BEAST 
He seeks those things that satisfy the senses, he attempts the satiation of the lower cravings. Rudolph Eucken |Abel J. Jones 
The only ambition of this great powerful frame was to do nothing, to grovel in idleness and satiation from hour to hour. Therese Raquin |Emile Zola 
The promise of satiation, of inevitability, steeped his being in a pleasant lethargy. Cytherea |Joseph Hergesheimer 
Delicacies of fish and flesh and hitherto unheard-of fruits were served up to me to satiation. The Portal of Dreams |Charles Neville Buck 
Somewhat nettled she showed displeasure, charged him with the fickleness of satiation. Bakemono Yashiki (The Haunted House) |James S. De Benneville