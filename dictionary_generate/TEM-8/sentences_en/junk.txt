The result has been a tide of disposable, nonrecyclable plastic junk. Big Oil’s hopes are pinned on plastics. It won’t end well. |David Roberts |September 4, 2020 |Vox 
They’ll junk their plan against a certain adversary and throw something completely different at them if whatever they did the first time around didn’t work. The Raptors’ Defense Is Almost Never The Same, But It’s Always Really Good |Jared Dubin |August 17, 2020 |FiveThirtyEight 
Space junk isn’t going away anytime soon—and neither are the problems it causes. How to cast a wider net for tracking space junk |Neel Patel |August 5, 2020 |MIT Technology Review 
Given how bad the space junk problem is getting, any new solutions are more than welcome at this point. How to cast a wider net for tracking space junk |Neel Patel |August 5, 2020 |MIT Technology Review 
Mail ballots are now postage-paid, so you won’t need to dig around your junk drawer for stamps. Politics Report: What Comes Next for Sports Arena |Scott Lewis and Andrew Keatts |July 11, 2020 |Voice of San Diego 
I am not the most financially literate person (I would be hard-pressed to articulate the term “junk bond”). Can Self-Help Books Really Make a New You? |Lizzie Crocker |December 29, 2014 |DAILY BEAST 
What you see is a massive, well-intentioned, legal junk pile. Red Tape Is Strangling Good Samaritans |Philip K. Howard |December 27, 2014 |DAILY BEAST 
For those in the resource world, every ton of junk that goes into a landfill represents wasted energy. Garbage In, Power Out |The Daily Beast |November 24, 2014 |DAILY BEAST 
(Or as Gehry framed it in the Sketches documentary: “mak[ing] beauty with junk”). Frank Gehry Is Architecture’s Mad Genius |Sarah Moroz |October 27, 2014 |DAILY BEAST 
I am just so convinced that junk food and high sugar food are undermining the health of people…It caused a lot of strain. Why the Rockefellers Rejected Big Oil |Tim Mak |September 24, 2014 |DAILY BEAST 
But as no junk-man came, and as no one could be found to care for its now sadly battered hulk, its good riddance became a problem. The Real Latin Quarter |F. Berkeley Smith 
We obliged them to proceed, passed close by the junk, and then landed, and continued our excursion on foot. A Woman's Journey Round the World |Ida Pfeiffer 
After he had married her, he'd sell out this pile of junk and let somebody else haggle with the Injuns and cowpunchers. Mystery Ranch |Arthur Chapman 
Your Caroline, so enticing five hours before in this very chamber where she frisked about like an eel, is now a junk of lead. The Petty Troubles of Married Life, Complete |Honore de Balzac 
Then bit by bit, he unloaded his mind, which appeared full of little things, like a junk shop. The Belted Seas |Arthur Colton