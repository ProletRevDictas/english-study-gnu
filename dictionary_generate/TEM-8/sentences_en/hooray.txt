Wrong wrong wrong, I report happily (and hooray for my Maryland, and the other states approving same-sex marriage). Various Thoughts: Demography, Rove, Morris, the Great Nate, &c. |Michael Tomasky |November 7, 2012 |DAILY BEAST 
Well, he may be on TBS now, but NBC is still in his crosshairs…and hooray for that! 6 Funniest Moments from Conan's Debut |The Daily Beast Video |November 9, 2010 |DAILY BEAST 
Still, let's leave it like this: Hooray for you, Julianne Moore! The Campiest Movie of the Year |Choire Sicha |March 28, 2010 |DAILY BEAST 
The songs, which have creaky titles like “All that Razz” and “Hooray for What's-not-good,” are belted out with pitchy gusto. The Worst Awards in Hollywood |Sean Macaulay |March 7, 2010 |DAILY BEAST 
"Hooray," yelled Mr. Gibney, and dashed at the post which held Captain Scraggs prisoner. Captain Scraggs |Peter B. Kyne 
Upon hearing his sentence he lifted up his voice and shouted, "Hooray!" The Log of a Sea-Waif |Frank T. Bullen 
Advancing in extended order, about twenty of the enemy were challenged, and they all cheered, shouting "Hooray." The Story of the 6th Battalion, The Durham Light Infantry |Unknown 
Instead of the lowing of cattle and the bleating of sheep, was the rattle of the drum and the "hooray" of the volunteers. An Artilleryman's Diary |Jenkin Lloyd Jones 
Then all the courtiers round about murmured their congratulations, the audience that filled the theatre shouted 'Hooray!' Yellow-Cap and Other Fairy-Stories For Children |Julian Hawthorne