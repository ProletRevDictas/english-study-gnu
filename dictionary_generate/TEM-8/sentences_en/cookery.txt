Price observes a different sort of cookery in her pictures of reclining sunbathers on Brooklyn beaches, made from 2008 to 2015 and published in a 2016 book, “Stranger Lives.” In the galleries: A sizzling exhibit crackles with creativity |Mark Jenkins |July 23, 2021 |Washington Post 
In Rodney Scott’s World of BBQ, I believe Scott has set down the final word in backyard pig cookery. The Four Cookbooks That Got Me Out of My Pandemic Cooking Rut |Max Watman |June 25, 2021 |The Daily Beast 
Doubleday became the first house to hire a fulltime editor, Clara Claasen, to fill its stable with cookery authors. To Find Hope in American Cooking, James Beard Looked to the West Coast |John Birdsall |October 2, 2020 |Eater 
Ferris credits her books Louisiana Cookery (1954) and New Orleans Cuisine (1969) as exemplars of diligent reporting and research. The Tragic History of Southern Food |Jason Berry |November 12, 2014 |DAILY BEAST 
How does she find the time to write her cookery column for Waitrose magazine, that's what people will be asking. Pippa Hits The Races |Tom Sykes |March 14, 2013 |DAILY BEAST 
John Benbow has started cookery classes in his home in Clerkenwell. Gal With a Suitcase |Jolie Hunt |February 26, 2011 |DAILY BEAST 
The cookbook that has most inspired me is Escoffier: The Complete Guide to the Art of Modern Cookery. Fresh Picks |Laurent Tourondel |November 4, 2010 |DAILY BEAST 
Jamie Boswell contended that cookery was the criterion of reason; for that no animal but man did cook. The Book of Anecdotes and Budget of Fun; |Various 
Every thing was excellent in its kind, with only a little more garlic than is used in English cookery. Journal of a Voyage to Brazil |Maria Graham 
It reeked with stale tobacco-smoke, the smell of cookery, and the odors of frowsy clothes. The Gold Trail |Harold Bindloss 
This is a humorous allusion to a manner of serving up pikes which is well illustrated in the Fifteenth-Century Cookery-books, ed. Chaucer's Works, Volume 1 (of 7) -- Romaunt of the Rose; Minor Poems |Geoffrey Chaucer 
As early as 1878 payment for the attendance of the workhouse girls at a school of cookery was held to be legal. English Poor Law Policy |Sidney Webb