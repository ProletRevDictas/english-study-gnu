We keep our distance from hotheads, implore people to be reasonable, and regret various flings, outbursts, and acts of thoughtlessness. It’s Not Irrational to Party Like It’s 1999 - Issue 108: Change |Steven Pinker |November 17, 2021 |Nautilus 
Gutierrez — the hothead in the video, who has since been terminated — joined in. Arrogance and entitlement are the diseases in American policing |Petula Dvorak |April 12, 2021 |Washington Post 
A hothead who believes life itself has betrayed him is liable to take even minor perceived disloyalty as treason. Aaron Hernandez’s Terrifying Past |Michael Daly |July 22, 2013 |DAILY BEAST 
But the view of Current executives is that Olbermann was a hard-to-control hothead. Keith Olbermann’s Rage During Current TV Gig |Howard Kurtz |April 5, 2012 |DAILY BEAST 
He, along with his loyal, hothead sidekick, Jem (Jeremy Renner), are members of an elite group of criminals. Prepping for the Oscars on DVD |Marlow Stern |January 21, 2011 |DAILY BEAST 
Assange, already known to be somewhat of a hothead, rebuked CNN and King for shameful “tabloid journalism.” 15 Classic Larry King Moments |Sujay Kumar |December 15, 2010 |DAILY BEAST 
That would be Billy Poe, an ex-football star, 21, out of work, and a bit of a hothead. Men of Steel |Taylor Antrim |February 24, 2009 |DAILY BEAST 
"Sentimental old hothead," grumbled Brian, touched and pleased. Kenny |Leona Dalrymple 
A hothead like you will benefit by a period of quiet meditation. Humphrey Bold |Herbert Strang 
As he spoke he covered the wretched creature with a cloak, and placed a doubled gaberdine beneath hothead. Sweet Mace |George Manville Fenn 
The role of firebrand and hothead, drawing villainous knives on frightened boys, would not quite convince his present audience. The Varmint |Owen Johnson 
These four men, however, stood in such awe of the young hothead that they neglected to fulfil their commission. The Memoirs of the Conquistador Bernal Diaz del Castillo, Vol 1 (of 2) |Bernal Diaz del Castillo