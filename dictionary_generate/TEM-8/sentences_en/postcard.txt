The union is responding to Prospect’s claims with a postcard that is being mailed this week to 4,000 state opinion-makers. Rich Investors Stripped Millions From a Hospital Chain and Want to Leave It Behind. A Tiny State Stands in Their Way. |by Peter Elkind |February 4, 2021 |ProPublica 
They needed to pay for postcards letting people know they could vote absentee–or, in some states, to mail ballots to every voter. The Secret History of the Shadow Campaign That Saved the 2020 Election |Molly Ball |February 4, 2021 |Time 
As it turns out, while a Nobel Peace Prize nomination is a bit trickier than simply sending a guy in Norway a postcard with someone's name on it, it's not much trickier than that. You, too, can become a Nobel Peace Prize nominee |Philip Bump |February 1, 2021 |Washington Post 
For four days, we fished, ate and chilled in a postcard setting. In Big Sky country, a pandemic-era fly-fishing getaway |Carl Fincke |January 21, 2021 |Washington Post 
Atlanta’s CBS affiliate, WSB-TV, reported that Raffensperger’s son passed away in 2018 but received postcards from the New Georgia Project encouraging him to register to vote. Georgia Secretary Of State Sued Again For Purging Voters |Kirsten West Savali |December 3, 2020 |Essence.com 
Send a postcard to PostSecret and your deepest thoughts could end up on a blog. Your Sex Confession Is Her Screenplay |Samantha Allen |October 29, 2014 |DAILY BEAST 
There was a retro print postcard of her face printed on every single product. The Improbable Rise of Rita Ora: A Guide for the Modern-Day Celebrity |Emma Gannon |May 5, 2014 |DAILY BEAST 
No, the difference between the postcard and the real thing is immeasurable. Face to Face With ‘The Goldfinch,’ the Painting from Donna Tartt’s Novel |Malcolm Jones |December 1, 2013 |DAILY BEAST 
The caption reads “I figured this would be a good postcard to send home.” War Tourists Flock to Syria’s Front Lines |Ben Taub |November 2, 2013 |DAILY BEAST 
Now and then, a postcard would arrive with a curt message, typed on a manual typewriter. The Man with the President’s Ear, Arthur Schlesinger Jr. and JFK |Ted Widmer |October 27, 2013 |DAILY BEAST 
Later, a good deal later, when the picture postcard was invented, Howell did rather well out of that too. Mushroom Town |Oliver Onions 
Vonnie made good her threat and two weeks after the quarrel Peter received a picture postcard of a giant redwood. The Boy Grew Older |Heywood Broun 
Occasionally he would send me a postcard between the letters. The Escape of a Princess Pat |George Pearson 
When you are at home, please will you send me a postcard with a picture of London? Castellinaria |Henry Festing Jones 
How glad I am that I can get a pretty postcard for each of the other girls! Tabitha at Ivy Hall |Ruth Alberta Brown