Gentlemen seem to mesmerise houses—cow them with an eye, and up they come, trembling. Howards End |E. M. Forster 
The light in his blue eyes seemed to mesmerise men, to draw them, willing or unwilling, to him. With the Boer Forces |Howard C. Hillegas 
Candidates, save your money; mesmerise your opponents instead of bribing them, and you may become a patriot by a show of hands. Punch, or the London Charivari, Vol. 1, August 7, 1841 |Various 
I have had people try to mesmerise me a dozen times, and never with the least result. In the South Seas |Robert Louis Stevenson 
She said he really did mesmerise her, and that she could see in her sleep. The Notting Hill Mystery |Charles Felix