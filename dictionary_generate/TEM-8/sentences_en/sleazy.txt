Or as Joey, the sleazy, besieged used car salesman in Cadillac Man. Robin Williams, Hollywood’s Grand Jester, Is Dead at 63 |Marlow Stern |August 12, 2014 |DAILY BEAST 
Octopus is one of those sleazy and boorish Americans whose instincts prove correct. This 1979 Novel Predicted Putin’s Invasion Of Crimea |Michael Weiss |May 18, 2014 |DAILY BEAST 
And that is this sleazy rabbit hole in its most perfect summation. The B Is Back: The Benghazi Hearings Are Bullsh*t |Michael Tomasky |May 7, 2014 |DAILY BEAST 
How can she wax poetically about soiling herself at parties and not get branded as sleazy trash, a la Ke$ha? How Jennifer Lawrence Took Over Hollywood. (It’s Not Just Because of Her Charm.) |Kevin Fallon |December 20, 2013 |DAILY BEAST 
When the ship docks, the “property” is scooped up by a sleazy slave merchant (Paul Giamatti), and sold at auction. ‘12 Years a Slave,’ Starring Chiwetel Ejiofor and Michael Fassbender, Is Mesmerizing |Marlow Stern |August 31, 2013 |DAILY BEAST 
She wore wooden sabots on her feet, and upon her body a tattered, sleazy black frock. The Glory of The Coming |Irvin S. Cobb 
Finally he gave them up too, and one morning came to work wearing a flimsy, sleazy, negligee shirt. The Escape of Mr. Trimm |Irvin S. Cobb 
It was a thin, tattered, dried-fish-like thing; printed with blurred ink upon mean, sleazy paper. Pierre; or The Ambiguities |Herman Melville 
It was brief, occupying only about four pages of the small, sleazy note paper that we bought in those days of the sutlers. The Story of a Common Soldier of Army Life in the Civil War, 1861-1865 |Leander Stillwell 
Joe watched his narrow, bent shoulders under the sleazy shirt. Stubble |George Looms