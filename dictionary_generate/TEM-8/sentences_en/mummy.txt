Analyzing other nonroyal mummies from ancient Egypt may reveal how common mud shells were, who used them and why. An ancient Egyptian mummy was wrapped in an unusual mud shell |Maria Temming |February 3, 2021 |Science News 
Torchlight bags from Big Agnes offer an expandable twist on the mummy design to provide the best of both worlds. The 8 most exciting sports and outdoor products of 2020 |By Stan Horaczek and Rob Verger |December 4, 2020 |Popular-Science 
Until now, this type of procedure had mostly been seen in human mummies. See what these animal mummies are keeping under wraps |Helen Thompson |October 6, 2020 |Science News For Students 
Ancient embalmers often opened the mouths and eyes of mummies so the dead could see and communicate with the living, but previously this kind of procedure had primarily been seen in human mummies. X-rays reveal what ancient animal mummies keep under wraps |Helen Thompson |August 20, 2020 |Science News 
Ancient people in Great Britain made their own mummies, for instance. Let’s learn about mummies |Bethany Brookshire |July 8, 2020 |Science News For Students 
In recent years, away from the screen, Hurley has reinvented herself as a yummy mummy. Happy 20th Birthday, Liz Hurley’s Safety-Pin Dress |Tim Teeman |December 12, 2014 |DAILY BEAST 
Some families have one mummy, some families have one daddy, or two families. How Robin Williams’ Mrs. Doubtfire Won the Culture Wars |Tim Teeman |August 13, 2014 |DAILY BEAST 
Kashkari instead agreed with the Los Angeles Times' Matt Pearce that he looks like “the mummy from The Mummy.” Gun-toting Tea Partier Rejected by California |Olivia Nuzzi |June 4, 2014 |DAILY BEAST 
In pictures taken after the attack, she was wrapped up like a mummy. Surviving Syria’s Incendiary Bomb Attacks |Paul Adrian Raymond |December 11, 2013 |DAILY BEAST 
But our plan is to dress our son up as a mummy and take him through the neighborhoods to do his thing. ‘Hocus Pocus’ Turns 20: Meet the Voice Behind Binx the Talking Cat |Kevin Fallon |October 31, 2013 |DAILY BEAST 
Her whole soul hangs upon the lips of a beautiful baby doll that seems to be calling her his mummy. Marguerite |Anatole France 
He calls him a mummy, and describes him as being dead long ago and having lately also married. Frederick Chopin as a Man and Musician |Frederick Niecks 
He is an animated mummy, who used to fish on the Nile three thousand years ago, and catch nothing. Over the Sliprails |Henry Lawson 
His pleasant, youthful face was drawn to mummy-like wanness. The Ghost Breaker |Charles Goddard 
This was to act as the protector Khepra, of the ka or immaterial vitality of the sahu or mummy. Scarabs |Isaac Myer