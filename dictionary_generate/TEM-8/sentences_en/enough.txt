Grindr currently has twelve ‘tribes,’ and for some people this just is not enough. Grindr’s Trans Dating Problem |David Levesley |January 9, 2015 |DAILY BEAST 
The atmosphere on campuses has gotten repressive enough that comedian Chris Rock no longer plays colleges. How the PC Police Threaten Free Speech |Nick Gillespie |January 9, 2015 |DAILY BEAST 
He added: “People say he deserves his day in court… Do we have enough time?” Bill Maher: Hundreds of Millions of Muslims Support Attack on ‘Charlie Hebdo’ |Lloyd Grove |January 8, 2015 |DAILY BEAST 
But if you have a hearing and you prove that someone is mature enough, well then that state interest evaporates. Should Teens Have The Right To Die? |Brandy Zadrozny |January 8, 2015 |DAILY BEAST 
Luckily enough I have this dedicated flat that is just along from my house that I go to every day. Belle & Sebastian Aren’t So Shy Anymore |James Joiner |January 7, 2015 |DAILY BEAST 
And although we gabbled freely enough, MacRae avoided all mention of the persons of whom I most wished to hear. Raw Gold |Bertrand W. Sinclair 
The sailors sometimes use it to fry their meat, for want of butter, and find it agreeable enough. The Life and Most Surprising Adventures of Robinson Crusoe, of York, Mariner (1801) |Daniel Defoe 
Just corporeal enough to attest humanity, yet sufficiently transparent to let the celestial origin shine through. Pearls of Thought |Maturin M. Ballou 
A distinguished-looking man, evidently vested with authority, bustled forward and addressed him, civilly enough. The Red Year |Louis Tracy 
And sure enough when Sunday came, and the pencil was restored to him, he promptly showed nurse his picture. Children's Ways |James Sully