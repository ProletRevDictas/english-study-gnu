The GOP is still struggling to shed its hidebound, out-of-touch image, especially with voters not yet drawing Social Security. Why America Needs Cheney. No, the Other Cheney. |Michelle Cottle |April 23, 2014 |DAILY BEAST 
Change is a necessary part of life, even in hidebound Washington. Odd Ideas, Short Tempers, Great Abs |Michelle Cottle |January 4, 2013 |DAILY BEAST 
Is it not possible that you are not yet awake, or, God pity you, that you are hidebound in the dogmatism of your bit of thinking. The Kempton-Wace Letters |Jack London 
We are very hidebound in these conventions, we Moors; no other ways command honor. The Wolf Cub |Patrick Casey 
"But you mustn't think I'm absolutely hidebound," she went on. Mrs. Maxon Protests |Anthony Hope 
Now this young zealot was a man of imagination, hidebound only in his traditions. The Quickening |Francis Lynde 
I have never cared much about the world's opinion, even in the days when I was most hidebound in prejudice. Three Comedies |Bjrnstjerne M. Bjrnson