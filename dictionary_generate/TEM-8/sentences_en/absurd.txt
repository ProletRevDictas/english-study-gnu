That also means that they will pump out absurd amounts of heat. Nvidia’s monstrous new graphics cards crank up the power while dropping their prices |Stan Horaczek |September 9, 2020 |Popular-Science 
The fairest critique of the Bay Area has to do with its absurd housing costs. In defense of California |James Temple |September 4, 2020 |MIT Technology Review 
The sense of humor might be a little drier and more absurd, but that’s about it. A most excellent interview: The team behind Bill and Ted Face the Music |Jennifer Ouellette |August 28, 2020 |Ars Technica 
In the go-go days of 2017 and early 2018, dozens of crypto firms raised absurd amounts of money in “initial coin offerings” that sold digital tokens to all comers via the Internet. Are blockchain companies cursed with too much cash? |Jeff |August 19, 2020 |Fortune 
Thankfully the particular project that had so fully absorbed me eventually collapsed under its own absurd weight. Even small changes can help you combat workplace burnout |Kyle Hegarty |July 31, 2020 |Quartz 
Whatever the reason, and however absurd their beliefs may seem, American evangelicals are deadly serious. The Evangelical Apocalypse Is All Your Fault |Jay Michaelson |January 4, 2015 |DAILY BEAST 
And, as any good public defender would, Wolf says the allegations are absurd. The Strange Case of the Christian Zionist Terrorist |Creede Newton |December 14, 2014 |DAILY BEAST 
It is not merely morally absurd to suggest that facts do not matter; as a person of color, it is insulting and degrading. Dear White People: Well-Meaning Paternalism Is Still Racist |Chloé Valdary |December 9, 2014 |DAILY BEAST 
A lot of folks this week have responded to the absurd question, “What does Valerie Jarrett really do?” The Valerie Jarrett I Know: How She Saved the Obama Campaign and Why She’s Indispensable |Joshua DuBois |November 18, 2014 |DAILY BEAST 
That suggestion turns absurd when you consider the long list of corrupt Democrat politicians Lynch has sent to prison. For Next AG, Obama Picks a Quiet Fighter With a Heavy Punch |Michael Daly |November 8, 2014 |DAILY BEAST 
Tressan fell suddenly to groaning and wringing his hands a pathetic figure had it been less absurd. St. Martin's Summer |Rafael Sabatini 
The charges in general are quite reasonable, though I have paid one or two absurd bills. Glances at Europe |Horace Greeley 
I believe I murmured something suitable, but it was absurd to pretend to be overjoyed at the news. Uncanny Tales |Various 
Judge: If I hear any more absurd comparisons, I will give you twelve months. The Book of Anecdotes and Budget of Fun; |Various 
Absurd as that taffeta dress was for a child of her age, it seemed to her an armor against all disaster. The Campfire Girls of Roselawn |Margaret Penrose