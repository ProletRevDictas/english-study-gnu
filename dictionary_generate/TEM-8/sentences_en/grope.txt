Female activists handed out flyers depicting a hand emerging from a military uniform and stretching out to grope a frowning woman. Egyptian Military Police Beating a Female Protestor Produces Fury and an Apology |Sarah A. Topol |December 21, 2011 |DAILY BEAST 
When Winston and some of his friends followed her there and tried to grope her, Brown stabbed Winston in the chest. Is She a Killer? |Bryan Curtis |December 30, 2009 |DAILY BEAST 
Then she closed the door as quietly as possible, and clutching the handrail began to grope her way downstairs. Dope |Sax Rohmer 
After a time she managed to grope her way to her bedroom, where, turning up the light, she sank down helplessly upon the bed. Dope |Sax Rohmer 
The front room, as I entered to grope for the matches, felt as cold as a stone vault, and the air held an unusual dampness. Masterpieces of Mystery, Vol. 1 (of 4) |Various 
It is not merely that I, the fanatic, have had to grope without humour. The Napoleon of Notting Hill |Gilbert K. Chesterton 
It shot out long sinuous pseudopods that seemed to grope angrily. The Revolt of the Star Men |Raymond Gallun