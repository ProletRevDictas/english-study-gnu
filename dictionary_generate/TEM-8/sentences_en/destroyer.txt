Left-wing NGOs have denounced him as a fascist, a destroyer of democracy. Tucker Carlson praises anti-LGBTQ Hungary prime minister |Michael K. Lavers |August 7, 2021 |Washington Blade 
Broad was not, as his critics would have liked to paint him, an evil destroyer of public education. The Learning Curve: Eli Broad’s Complicated Ed Reform Legacy |Will Huntsberry |May 6, 2021 |Voice of San Diego 
You can hear the destroyer coming and dropping the depth charges into the water, it’s just a matter of time. Amazon CEO Jeff Bezos‘s successor will inherit his challenges |Jay Greene, Cat Zakrzewski |February 4, 2021 |Washington Post 
Barnes & Noble has been tagged as either the savior or the destroyer of the book industry in its 100-year history. Is it too late for Barnes & Noble? |Aaron Pressman |December 7, 2020 |Fortune 
The oil palm is a Shiva of the modern consumer economy, a great creator and a great destroyer. The Environmental Headache in Your Shampoo - Issue 90: Something Green |Anastasia Bendebury & Michael Shilo DeLay |September 16, 2020 |Nautilus 
The USS Arleigh Burke, a destroyer, and USS Philippine Sea, a cruiser, launched a total of 47 Tomahawk missiles. $70 Billion Stealth Jet Finally Flies in Its First War |Dave Majumdar |September 23, 2014 |DAILY BEAST 
He was in the forward gun turret where the destroyer hit us. JFK Letter: ‘War Is a Dirty Business’ |John F. Kennedy |November 5, 2013 |DAILY BEAST 
During a night operation in the Solomon Islands in 1943, the patrol torpedo boat he commanded was rammed by a Japanese destroyer. Three Great Men Died That Day: JFK, C.S. Lewis, and Aldous Huxley |John Garth |November 3, 2013 |DAILY BEAST 
He was not a destroyer seeking to become exceptional by killing. Kofi Awoonor, the Ghanaian Poet Killed in Westgate Mall Attack |Michael Daly |September 24, 2013 |DAILY BEAST 
Some see all significance in the grim front of the destroyer, and some in the bitter sufferers of the Lost Cause. David's Bookclub: Battle Cry of Freedom |David Frum |May 23, 2013 |DAILY BEAST 
But the rumble of distant guns told the destroyer that his short-lived hour of triumph was nearly sped. The Red Year |Louis Tracy 
Left on a picket boat with Birdie to board my destroyer to an accompaniment of various denominations of projectiles. Gallipoli Diary, Volume I |Ian Hamilton 
When the Turks saw a destroyer come bustling up at an unusual hour they said to themselves, "fee faw fum!" Gallipoli Diary, Volume I |Ian Hamilton 
Instead of being a destroyer of merchandise, this new craft was an unarmed carrier of merchandise. The Wonder Book of Knowledge |Various 
The great mission of the submarine during the European war was as a commerce destroyer. The Wonder Book of Knowledge |Various