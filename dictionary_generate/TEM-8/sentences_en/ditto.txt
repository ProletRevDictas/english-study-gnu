Ditto Virginia, but in reverse; culturally, northern Virginia is Yankee land (but with gun shops). Dems, It’s Time to Dump Dixie |Michael Tomasky |December 8, 2014 |DAILY BEAST 
When the former engaged in his drone filibuster, Cruz showed up in support; ditto for Paul when Cruz held an Obamacare filibuster. Rand Paul Beats Ted Cruz, Saves NSA From ‘Reform’ |Tim Mak, Olivia Nuzzi |November 19, 2014 |DAILY BEAST 
Ditto Ohio Gov. John Kasich, who won easily, and might parlay his success into a presidential bid. For Conservatives, Liberal Tears Taste Sweet |Matt Lewis |November 5, 2014 |DAILY BEAST 
Ditto that the Court acted (or in-acted) “without providing any explanation whatsoever.” Who Are the Judicial Activists Now? |Michael Tomasky |October 7, 2014 |DAILY BEAST 
Ditto for Nancy Writebol the other American flown back in that dramatic first wave. The CDC Was Wrong About How to Stop Ebola |Kent Sepkowitz |October 1, 2014 |DAILY BEAST 
The northern aisle is occupied below with free seats; and above, in a gallery, with ditto. Our Churches and Chapels |Atticus 
For a week past, the house had been tolerably well filled—ditto Mrs. Sutton's hands; ditto her great, heart. At Last |Marion Harland 
Sam has been got out of the way by a cooked-up story, ditto your manager. Fifty-Two Stories For Girls |Various 
And now a cold little hand was stretched out from the left bed towards her, and a cold little hand from the right bed did ditto. Betty Vivian |L. T. Meade 
Lot 176—Frock coat and vest, dress coat and vest, ditto, pair of trousers and opera hat. Happy Days |Alan Alexander Milne