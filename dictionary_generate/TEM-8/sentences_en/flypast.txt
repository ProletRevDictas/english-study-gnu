Cool video shot from the cockpit of two different planes during yesterday's jubilee flypast. Flypast Aerial Video |Tom Sykes |June 6, 2012 |DAILY BEAST 
Weather permitting, there will then be an RAF flypast and balcony appearance. Philip in Royal Prayers |Tom Sykes |June 5, 2012 |DAILY BEAST 
A balcony appearance and RAF flypast is scheduled for 3:30pm. Jubilee Pageantry in London | |June 5, 2012 |DAILY BEAST 
A Royal Navy Pilot sits inside a Lynx helicopter at RAF Odiham air base today ahead of a rehearsal for a Diamond Jubilee flypast. Royal Navy, Army and RAF Helicopter Crews Prepare Ahead Of The Diamond Jubilee | |May 18, 2012 |DAILY BEAST 
They were rewarded by a third thundering flypast of the fighter planes. And Then the Town Took Off |Richard Wilson