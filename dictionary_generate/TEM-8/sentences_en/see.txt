We see detoxing as a path to transcendence, a symbol of modern urban virtue and self-transformation through abstinence. How Taryn Toomey’s ‘The Class’ Became New York’s Latest Fitness Craze |Lizzie Crocker |January 9, 2015 |DAILY BEAST 
Harris is unlikely to see a challenge from Villaraigosa, either. The Golden State Preps for the ‘Red Wedding’ of Senate Races |David Freedlander |January 9, 2015 |DAILY BEAST 
We do see that a few European countries have them on the books: Germany, Poland, Italy, Ireland, a couple more. In Defense of Blasphemy |Michael Tomasky |January 9, 2015 |DAILY BEAST 
Several times, either because they forgot or they had a technical problem, they connected directly, and we could see them. Was Sony Hit With a Second Hack? |Shane Harris |January 8, 2015 |DAILY BEAST 
That would truly be a milestone to celebrate—until you see what that record “diversity” actually means. The Unbearable Whiteness of Congress |Dean Obeidallah |January 8, 2015 |DAILY BEAST 
And to tell the truth, she couldn't help wishing he could see, so he could make the game livelier. The Tale of Grandfather Mole |Arthur Scott Bailey 
I waited three months more, in great impatience, then sent him back to the same post, to see if there might be a reply. The Boarded-Up House |Augusta Huiell Seaman 
You see, I'd always thought of him as the boy whom Great-aunt Lucia described having seen. The Boarded-Up House |Augusta Huiell Seaman 
He shrank, as from some one who inflicted pain as a child, unwittingly, to see what the effect would be. The Wave |Algernon Blackwood 
On to Gaba Tepe just in time to see the opening, the climax and the end of the dreaded Turkish counter attack. Gallipoli Diary, Volume I |Ian Hamilton