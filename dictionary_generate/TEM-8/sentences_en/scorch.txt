One of the reasons I did that Twitter feed is that I want the truth to come out, all the truth, so I can scorch the earth. Porn Professor Hugo Schwyzer Comes Clean About His Twitter Meltdown and Life as a Fraud |Richard Abowitz |August 12, 2013 |DAILY BEAST 
And with too little bacon in the pan, not enough fat renders quickly enough and the bacon will scorch. America's Bacon Addiction |Sarah Whitman-Salkin |June 30, 2009 |DAILY BEAST 
Speculation: The scorch might have been made by radioactivity attendant upon the resurrection. 10 Reasons the Resurrection Really Happened |Jeffrey Hart |April 10, 2009 |DAILY BEAST 
If the scorch on the Shroud is the result of radiation, it could have been radiation that reconstituted the dead body. 10 Reasons the Resurrection Really Happened |Jeffrey Hart |April 10, 2009 |DAILY BEAST 
Cold words freeze people, and hot words scorch them, and bitter words make them bitter, and wrathful words make them wrathful. The Ladies' Book of Etiquette, and Manual of Politeness |Florence Hartley 
Losses, mistakes, discouragements and disappointments scorch with burning blisters the lining of our lives. Tyranny of God |Joseph Lewis 
Sometimes they scorch them off their bodies by means of a lighted stick—a kind office which Yamba performed for me. The Adventures of Louis de Rougemont |Louis de Rougemont 
There was a second dash upon the stove, and another scorch in the slip. Prudy Keeping House |Sophie May 
Even with a helper wouldn't you probably scorch the mutton or else burn yourself to death with the hot grease? The Iron Puddler |James J. Davis