Anyone who believes that life is a battlefield full of individual warriors should go out into the meadows on a spring night. What the Meadow Teaches Us - Issue 90: Something Green |Andreas Weber |September 16, 2020 |Nautilus 
Lee herself has found skeletons of possible warrior women in ancient Mongolia, a nation just north of China. Women like Mulan didn’t need to go to war in disguise |Bethany Brookshire |September 4, 2020 |Science News For Students 
This remains true if the one in green is a warrior or a lord. Can you expose the truth in these two riddles? |Claire Maldarelli |August 26, 2020 |Popular-Science 
Archaeology has uncovered evidence that in ancient societies, women could be warriors. Scientists Say: Archaeology |Bethany Brookshire |August 10, 2020 |Science News For Students 
Skeletons show signs that some women in these communities were warriors. Skeletons hint that ancient societies had women warriors |Bruce Bower |May 28, 2020 |Science News For Students 
But the proud stone lion that once stood atop the tomb, as Peristeri has often maintained, suggests a male occupant and a warrior. Is This Alexander the Great’s Tomb—or His Wife’s? |James Romm |December 12, 2014 |DAILY BEAST 
Along the way, she discovers the fearless warrior that her mother knew was inside her all along. Exclusive: The Making of Reese Witherspoon’s Golden Globe-Nominated ‘Wild’ |Marlow Stern |December 12, 2014 |DAILY BEAST 
Inhofe is not just a climate-change denier; he is a warrior for corporate-funded half-truths and outright lies. If You Think D.C. Is Awful Now, Wait Until Wednesday |Jonathan Alter |November 4, 2014 |DAILY BEAST 
Partly this is tradition as old as the history of warrior Islam. The Secret Life of an ISIS Warlord |Will Cathcart, Vazha Tavberidze, Nino Burchuladze |October 27, 2014 |DAILY BEAST 
For its part, the Wounded Warrior Project dismisses much of the criticism. Wounded Warrior Project Under Fire |Tim Mak |September 26, 2014 |DAILY BEAST 
He was a distinguished warrior under Francis I, mortally wounded at the battle of Marignan. The Every Day Book of History and Chronology |Joel Munsell 
But in the end his health gave way, and the Emperor himself wrote to Prince Eugne telling him to send the old warrior home. Napoleon's Marshals |R. P. Dunn-Pattison 
Its pages are filled with the purple gowns of kings and the scarlet trappings of the warrior. The Unsolved Riddle of Social Justice |Stephen Leacock 
Nicholson and John Lawrence were there; could they hold those warrior-tribes in subjection, or, better still, in leash? The Red Year |Louis Tracy 
The death of Harcla, the keenest and ablest warrior in141 England, did not remove the difficulties from Edward's path. King Robert the Bruce |A. F. Murison