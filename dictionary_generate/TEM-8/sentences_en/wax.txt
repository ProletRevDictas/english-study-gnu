Online multiplayer gamesThe popularity of various multiplayer games waxes and wanes. Video gaming is for everybody now. Here’s how to get back into it. |Harry Guinness |January 25, 2021 |Popular-Science 
Since the portions are wrapped in wax paper, they can be easily separated. Hints From Heloise: Getting vitamin D into your diet |Heloise Heloise |January 20, 2021 |Washington Post 
The faster the economy is waxing, the more profitable new investments become, and the more companies compete for the capital needed to fund those investments. Investing legends Carl Icahn and Jeremy Grantham see a stock market bubble |Shawn Tully |January 8, 2021 |Fortune 
Flushing out every bit of it means there’s nothing to attract dirt, and it also makes room for the wax to penetrate and bond to the chain. You Should Hot-Wax Your Bike Chains. Here's Why. |Joe Lindsey |November 6, 2020 |Outside Online 
Lay chain in the pot, on top of the wax, as flat as possible. You Should Hot-Wax Your Bike Chains. Here's Why. |Joe Lindsey |November 6, 2020 |Outside Online 
Place the flour, baking soda, baking powder, and salt on parchment or wax paper. Make ‘The Chew’s’ Carla Hall’s Sticky Toffee Pudding |Carla Hall |December 28, 2014 |DAILY BEAST 
Alastair Sim had jowls like melting candle wax, a snarl like a cornered cat and eyes cold with contempt. How Dickens and Scrooge Saved Christmas |Clive Irving |December 22, 2014 |DAILY BEAST 
Inside the wax floored examining room, I sat up on the powder blue table with my shirt off. I Shot Bin Laden |Elliot Ackerman |November 16, 2014 |DAILY BEAST 
I promised never again to wax lyrical about the fries in gravy. I Saw Nuclear Armageddon Sitting on My Desk |Clive Irving |November 10, 2014 |DAILY BEAST 
Full disclosure: I briefly worked for Torres at his current magazine, Wax Poetics. It Was All a Dream: Drama, Bullshit, and the Rebirth of The Source Magazine |Alex Suskind |October 14, 2014 |DAILY BEAST 
Her eldest daughter married in America, and was well known as a modeller in wax in New York. Women in the fine arts, from the Seventh Century B.C. to the Twentieth Century A.D. |Clara Erskine Clement 
At length only four or five flames remained, feebly wavering in their pools of melted wax. The Boarded-Up House |Augusta Huiell Seaman 
Two many-branched candelabra, holding wax lights, brilliantly illuminate the game. The Pit Town Coronet, Volume I (of 3) |Charles James Wills 
Tories will wax eloquent on "the pink miasma of revolutionary Radicalism." Punch, or the London Charivari, Volume 107, November 3, 1894 |Various 
Frulein Timm belongs to the single sisterhood, but is one of the fresh and placid kind, and as neat as wax. Music-Study in Germany |Amy Fay