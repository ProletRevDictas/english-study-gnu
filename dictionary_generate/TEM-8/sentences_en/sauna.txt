Also, don’t miss the city’s new Sky Lagoon, with its swim-up Champagne bar and fjord-view sauna and natural hot springs. The 25 Best Fall Trips in the World |jversteegh |August 9, 2021 |Outside Online 
The sauna effect is a thing of the past, as are pit zips—and NeoShell’s supple, four-way stretchiness is a far cry from stiff waterproof shells. 11 Times Polartec Revolutionized How We Dress |bsmith |August 7, 2021 |Outside Online 
Instead, she kept her weekly Thursday date at a local sauna, coolly confident that she had time on her hands. Tschüss: OZY’s Angela Merkel Retrospective |Charu Kasturi |July 29, 2021 |Ozy 
They sometimes spend more than half an hour there talking, joking and laughing, as if they were in a sauna. Locked up in the Land of Liberty: Part II |Michael K. Lavers |July 14, 2021 |Washington Blade 
She gamely gets her armpits professionally sniffed, and she joins naked, sweating audiences for sauna theater. ‘The Joy of Sweat’ will help you make peace with perspiration |Bethany Brookshire |July 13, 2021 |Science News 
The amenities include a fitness room, a sauna, a beauty parlor, and a karaoke setup. Inside the ‘Surprisingly Great’ North Korean Hacker Hotel |Michael Daly |December 20, 2014 |DAILY BEAST 
There is a breeze, and that is the only thing that differentiates it from a sauna. Whatever You Do Someone Will Die. A Short Story About Impossible Choices in Iraq |Nathan Bradley Bethea |August 31, 2014 |DAILY BEAST 
Our early-evening appointment with the sauna was canceled due to “technical reasons.” For Ukrainians on Holiday, the Carpathians Are the New Crimea |Vijai Maheshwari |July 14, 2014 |DAILY BEAST 
And I would rather work as a security guard for a sauna full of beautiful girls! Ukrainian Troops Surrender to Unarmed Pro-Russian Protesters |Anna Nemtsova |April 17, 2014 |DAILY BEAST 
Russians believe the best way to dry out from vodka saturation is with a sauna session and a beating with birch branches. The Wildest Hangover Cures From Around the World |Nina Strochlic |November 29, 2013 |DAILY BEAST 
Suyettar thought that this would be a fine chance to wash the dust from her eyes, so she let them lead her to the sauna. Mighty Mikko |Parker Fillmore 
The sauna, or bath-house, is always a separate building; and there Finnish people take extremely hot baths almost every evening. Kalevala, Volume I (of 2) |Anonymous 
There are many Finns in the community who still use the sauna, or steam bath, of their native land. North Dakota |Various 
The first building which she erected was not the dwelling-house but the Sauna or bath-house. Our Little Finnish Cousin |Clara Vostrovsky Winlow 
Slowly she marched out of the castle, across the courtyard, and over to the sauna. Mighty Mikko |Parker Fillmore