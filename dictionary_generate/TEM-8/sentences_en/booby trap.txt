But Reconcile is from a slightly different arm of Houston hip-hop—more focused on spiritual triumph over the trap. Down With the King: Christianity Isn’t Hiding in Rap’s Closet |Stereo Williams |December 28, 2014 |DAILY BEAST 
Do not fall into the trap of being swayed by political notion. Is Pope Francis Backpedaling on Gays? |Jay Michaelson |November 19, 2014 |DAILY BEAST 
You now have a growing number of candidates and elected officials who can do that without having to fall into that trap. The Republican Rainbow Coalition Is Real |Tim Mak |November 18, 2014 |DAILY BEAST 
By the time Sotloff was allowed to leave the border crossing, the trap was set. Obama Administration and Sotloff Family Battle Over Blame for Journalist’s Kidnapping |Josh Rogin |September 22, 2014 |DAILY BEAST 
In this way, the U.S. would avoid the trap of being viewed, once again, as the leader of an anti-Islamic crusade. Stop the ISIS War Before It Gets Worse! |Jeffrey Sachs, Michael Shank |September 17, 2014 |DAILY BEAST 
Sometimes the animal was caught in a trap which was nothing less than a hut of logs with a single entrance. Our Little Korean Cousin |H. Lee M. Pike 
Haggard had disappeared with the celerity of a harlequin who jumps through a trap. The Pit Town Coronet, Volume I (of 3) |Charles James Wills 
A patch of light fell clear on the side of the trap, and on Longcluse's ungloved hand as he leaned on it. Checkmate |Joseph Sheridan Le Fanu 
A trap-door had opened in the floor of his consciousness; his first, early love sheltered in his aching heart again. The Wave |Algernon Blackwood 
But if they all pick up the broadcast that this is where to get a free ride home, I'll have just another sand trap here. Fee of the Frontier |Horace Brown Fyfe