Her Highness, Queen Bey, dropped two new songs Friday morning. Beyonce’s New “7/11” and “Ring Off” Will Give You Reason to Live (And Dance) |Kevin Fallon |November 21, 2014 |DAILY BEAST 
Her Serene Highness also appears to have taken solace in extensive surgical alterations to her body over the past three years. Princess Charlene's Monaco Nightmare |Tom Sykes |September 15, 2014 |DAILY BEAST 
Her Royal Highness will no longer accompany The Duke of Cambridge on their planned engagement in Oxford today. Second Royal Baby for Kate |Tom Sykes |September 8, 2014 |DAILY BEAST 
In comparison, The Quest is an over the top confection, with about as much genuine genre integrity as Your Highness. 'The Quest' Review: Behold, a Campy 'Game of Thrones' Reality Show |Amy Zimmerman |August 1, 2014 |DAILY BEAST 
Each country plans its own celebration to receive and honor her royal highness (well, her representative baton, at least). Kiss the…Queen’s Baton? The British Empire Is Alive and Well |Debra A. Klein |May 27, 2014 |DAILY BEAST 
Mrs. Charmington hastened to spread the report that his Royal Highness was seriously smitten. The Pit Town Coronet, Volume I (of 3) |Charles James Wills 
May it please your Transparent Highness, I've found out how the needles get into the haystacks. Davy and The Goblin |Charles E. Carryl 
Doubtless I should have been entrusted with letters for your highness were not the city in some confusion owing to the fighting. The Red Year |Louis Tracy 
You are found in the library engaged in a bold flirtation with her Highness's son, Prince Boris. The Weight of the Crown |Fred M. White 
His Highness is about thirty-six years of age, short, thick set, wearing a slight moustache and his hair cropped very close. The Philippine Islands |John Foreman