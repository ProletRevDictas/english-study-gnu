These discs, titled Miracle, transpose the invisible concept of ālaya into a tangible object. Mariko Mori Rebirth at the Japan Society |Justin Jones |October 10, 2013 |DAILY BEAST 
I always thought, they would need to be an evidence to the story, and the way I would transpose it. Hedi Slimane Interview: ‘California Song’ at MOCA Los Angeles (PHOTOS) |Isabel Wilkinson |January 20, 2012 |DAILY BEAST 
I transpose; all have What harme was (but harm is monosyllabic, and the line is then bad). Chaucer's Works, Volume 1 (of 7) -- Romaunt of the Rose; Minor Poems |Geoffrey Chaucer 
I absorbed this idea almost unconsciously, and hardly know when I learned to transpose, so natural did it seem to me. Piano Mastery |Harriette Brower 
Omit e corn, for bit read bite (so too at l. 211), and transpose, otwinne bite. Selections from Early Middle English 1130-1250: Part II: Notes |Various 
She used to give me very little time in which to transpose her songs, and insisted on their being finished when she wanted them. Lazy Thoughts of a Lazy Girl |Jenny Wren 
He is a great reader, of course, and can transpose at sight, and all that sort of thing. Music-Study in Germany |Amy Fay