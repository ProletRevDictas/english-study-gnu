The body clock shifts, making it easier to stay up late and harder to wake early. Explainer: What is puberty? |Esther Landhuis |August 27, 2020 |Science News For Students 
This clock can distinguish between different colors of light. Explainer: How our eyes make sense of light |Tina Hesman Saey |July 16, 2020 |Science News For Students 
In the future, even more precise atomic clocks could provide further information about what makes the universe tick. The universe might have a fundamental clock that ticks very, very fast |Emily Conover |July 13, 2020 |Science News 
They had bedrooms next to the rooms where their animals were being deprived so they could monitor around the clock. Why Sleep Deprivation Kills |Veronique Greenwood |June 4, 2020 |Quanta Magazine 
So the merchants of bad, as we call them, are just going at us around the clock trying to sell us their wares. Reasons to Be Cheerful (Ep. 417) |Stephen J. Dubner |May 7, 2020 |Freakonomics 
France 24 is providing live, round-the-clock coverage of both scenes as they progress. LIVE Coverage of the Paris Terror Attacks | |January 9, 2015 |DAILY BEAST 
The wine cellar—one of the best in the world—survived World War II and is guarded around the clock. Inside The World’s 10 Oldest Restaurants |Justin Jones |December 20, 2014 |DAILY BEAST 
They thrive on packed schedules, they say, and take pleasure in working around the clock. How the Property Brothers Became Your Mom’s Favorite TV Stars |Kevin Fallon |November 25, 2014 |DAILY BEAST 
He becomes increasingly paranoid by the societal fixtures around him—a ticking clock, a ringing phone. ‘Interstellar’ Is Wildly Ambitious, Very Flawed, and Absolutely Worth Seeing |Marlow Stern |November 7, 2014 |DAILY BEAST 
Bill Haley had kicked rock off with “Rock Around the Clock,” but Elvis Presley made it an international phenomenon. How Rock and Roll Killed Jim Crow |Dennis McNally |October 26, 2014 |DAILY BEAST 
The night wore on, and the clock downstairs was striking the hour of two when she suddenly awakened. The Homesteader |Oscar Micheaux 
The clock struck ten, and clerks poured in faster than ever, each one in a greater perspiration than his predecessor. The Posthumous Papers of the Pickwick Club, v. 2(of 2) |Charles Dickens 
As it came near, it proved to be the clock, with a sail hoisted, and the Goblin sitting complacently in the stern. Davy and The Goblin |Charles E. Carryl 
A clock was put above the spot where the fountain stood, in April, 1852, which cost £60. Showell's Dictionary of Birmingham |Thomas T. Harman and Walter Showell 
The tower has four clock faces, pinnacles at the angles, and a steep slate roof and is 120 feet high. Showell's Dictionary of Birmingham |Thomas T. Harman and Walter Showell