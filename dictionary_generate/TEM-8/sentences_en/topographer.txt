Everyone from government topographers and fur traders to missionaries imposing their beliefs onto Indigenous communities headed across the Mississippi River toward the Midwest. Space Exploration Is Hot. What Came Before? |Josefina Salomon |August 10, 2021 |Ozy 
Morris, who spent a decade as an Atlanta topographer, didn't really like to work. The Man Who Fell for 'King Con' |Gina Piccalo |December 1, 2010 |DAILY BEAST 
The Topographer says he "describes with the language of a master, the artless scenes of uncultivated nature." On the Portraits of English Authors on Gardening, |Samuel Felton 
The old topographer informs us that in the time of Richard II. A History of the Cries of London |Charles Hindley 
Surely Thackeray, especially in the “Ballads,” mentions many places not alluded to by the new topographer. Lost Leaders |Andrew Lang 
And the feature which strikes the topographer is of course that which would naturally give the name. The River-Names of Europe |Robert Ferguson 
We pursued them through passages and halls whose confused arrangement would craze the best military topographer. Saragossa |Benito Prez Galds