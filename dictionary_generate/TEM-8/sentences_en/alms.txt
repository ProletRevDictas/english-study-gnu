Another piece might cause people to come up short is an elaborate green Burmese Buddhist alms bowl on a stand covered in gold. Hello, ‘Gorgeous’: Grit and Glamour In San Francisco |Emily Wilson |June 20, 2014 |DAILY BEAST 
“You can't give alms to the poor on one level and have your hands on their throat on another,” he said. The Clinton Global Initiative Kicks off With Tears, Impressions, and Fighting Words |Nina Strochlic |September 24, 2013 |DAILY BEAST 
A beggar asking alms under the character of a poor scholar, a gentleman put the question, Quomodo vales? The Book of Anecdotes and Budget of Fun; |Various 
But when thou dost alms, let not thy left hand know what thy right hand doth. The Bible, Douay-Rheims Version |Various 
To such a one he would say, 'Wilt thou cease to beg alms of Me in such a shameless manner? Black Diamonds |Mr Jkai 
The surplus was distributed in alms amongst the poor, a duty strenuously prescribed by their moral code. Ancient Faiths And Modern |Thomas Inman 
In point of fact, prayers are spoken of as if they were equivalent to sacrifice, alms-giving, or any other supposed virtue. Ancient Faiths And Modern |Thomas Inman