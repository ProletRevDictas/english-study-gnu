As the name implies, moustache wax is good for facial hair above the lip, but not good for beards. Best beard product for the best beard of your life |Florie Korani |August 18, 2021 |Popular-Science 
Other beard products, like beard balms and moustache wax, are designed more for styling than hydration. Best beard product for the best beard of your life |Florie Korani |August 18, 2021 |Popular-Science 
The best trimmer for beards, moustaches and every other type of body. Best beard product for the best beard of your life |Florie Korani |August 18, 2021 |Popular-Science 
He was very sincere and nice, but I saw him glance at the pink moustache across my lip. I Shot Bin Laden |Elliot Ackerman |November 16, 2014 |DAILY BEAST 
He does stop short of making Anders just another Wall Street moustache-twirler. Ted Thompson’s Debut Novel Features A 1 Percenter As Its Hero |Stefan Beck |May 6, 2014 |DAILY BEAST 
Garcia looks the part with a high-and-tight haircut, a thin moustache, suspenders, and a bow-tie. The Secret Speakeasies of Buenos Aires |Jeff Campagna |February 25, 2014 |DAILY BEAST 
He took a tremendous drink from his cup, the froth sticking to his moustache. The Fourth War: My Lunch with a Jihadi |Elliot Ackerman |January 21, 2014 |DAILY BEAST 
In came Woodrow Wilson, and ever since, we've had not a single moustache, of understanding or otherwise, in the Oval Office. Will We Ever Have a Hirsute President Again? |Michael Tomasky |January 22, 2013 |DAILY BEAST 
"The worst of it is that one can't be certain of anything," said Papa, pulling his moustache. Kipling Stories and Poems Every Child Should Know, Book II |Rudyard Kipling 
He had a red, jolly face, divided unequally by a great black moustache, and his manner was hearty. The Joyous Adventures of Aristide Pujol |William J. Locke 
His smooth brow wrinkled and his mouth tightened to a thin straight line beneath the fair "regulation" moustache. Dope |Sax Rohmer 
He drew himself up, twisted his moustache, and met her eyes—they were rather sad and tired—with the roguish mockery of his own. The Joyous Adventures of Aristide Pujol |William J. Locke 
The Comte de Lussigny twirled the tips of his moustache almost to his forehead and caught up his hat. The Joyous Adventures of Aristide Pujol |William J. Locke