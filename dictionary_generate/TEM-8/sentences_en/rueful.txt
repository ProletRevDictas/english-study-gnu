Then she paused, glanced away from the camera, and took a long, rueful breath. YouTuber Valkyrae’s skincare fiasco proves online drama doesn’t produce accountability |Nathan Grayson |October 28, 2021 |Washington Post 
Rueful laughter from both men turned to thoughtfulness in Stittsworth. The Strange, True Tale of the Old-Timey Goat Testicle-Implanting 'Governor' |Penny Lane |September 16, 2014 |DAILY BEAST 
You can only ever see it from an outsider and comment on it with the rueful wisdom of a non participant. Benedict Cumberbatch’s Revealing Reddit AMA: On Julian Assange’s Letter and Fame |Marlow Stern |October 11, 2013 |DAILY BEAST 
A discussion of a London cheese shop sends her into paroxysms of rueful joy. Michelle Forbes' Good Grief |Jace Lacob |May 22, 2011 |DAILY BEAST 
But most of them must be delivered from on the peninsula, where Bob is standing, casting a rueful eye on the whole show. On the Peninsula |Bryan Curtis |April 25, 2011 |DAILY BEAST 
“You can pick which lines can come out,” Spitzer said with a rueful smile. Eliot Spitzer's Comeback Trail |Lloyd Grove |May 10, 2010 |DAILY BEAST 
Gwynne regarded the thin sole of his house shoe with so rueful a countenance that the judge laughed outright. Ancestors |Gertrude Atherton 
Pale, lean, taciturn and somewhat deaf, he bore much resemblance to the Knight of the Rueful Countenance. Repertory Of The Comedie Humaine, Complete, A -- Z |Anatole Cerfberr and Jules Franois Christophe 
By this time he was in a wholly different mood; angry with himself, and full of rueful thought about his wife. Marriage la mode |Mrs. Humphry Ward 
At the rueful outcry, Clifford turned, just in time to see the bobbing bundle disappear in the muddy water. A Fortune Hunter; Or, The Old Stone Corral |John Dunloe Carteret 
In order to carry out my wife's orders, I had to disentangle Susan from Liosha's embrace and pack her off rueful to the nursery. Jaffery |William J. Locke