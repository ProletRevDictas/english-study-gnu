Still, those first six months with Ana helped me steady myself against the aimless floundering. The best $1,419 I ever spent: Flights for my long-distance relationship |Arthur Tarley |August 25, 2021 |Vox 
In many ways, this is how humans and other animals seem to learn, via aimless play. An endlessly changing playground teaches AIs how to multitask |Will Douglas Heaven |July 30, 2021 |MIT Technology Review 
The media frequently portray young people excluded from wage work as inactive, aimless and alienated from mainstream society. The biggest stereotypes about young, unemployed Africans |Hannah J. Dawson |June 15, 2021 |Quartz 
Hours and hours of aimless walking in a mall is my idea of uber hell. Fortune 500 secret: Consumer intelligence analytics drives every decision |Sponsored Content: NetBase Quid |June 4, 2021 |Search Engine Land 
Heartbroken and aimless, Kaestel was arrested days later for burglarizing a store. He Robbed a Taco Joint With a Toy Water Gun for $264. He Got Life in Prison. |Kate Briquelet |May 31, 2021 |The Daily Beast 
Brinsley got out of jail last July, and was desperate and aimless. Alleged Cop Killer Ismaaiyl Brinsley Had a Death Wish |M.L. Nestel |December 22, 2014 |DAILY BEAST 
The rhinoceros, again looking pretty aimless and beaten down, was made—beautifully—of papier mache. How to Catch a Depressed Gorilla, Japanese-Style |Tim Teeman |February 6, 2014 |DAILY BEAST 
The title track (again, a cover) is an aimless Bo Diddley raveup. ‘High Hopes’ Review: Bruce Springsteen Lowers the Bar |Andrew Romano |January 7, 2014 |DAILY BEAST 
Nick, once aimless with a deposition that would make Eeyore seem like Richard Simmons, is now happily dating Jess. The Return of New Girl’s Coach…and of Faith in the Still-Great Comedy |Kevin Fallon |November 6, 2013 |DAILY BEAST 
It reminds me a little of Raising Hope in that it centers on well-intentioned but aimless people who love each other a lot. TV Preview: Snap Judgments of 2012-13’s New Shows |Jace Lacob, Maria Elena Fernandez |June 12, 2012 |DAILY BEAST 
Our social life is aimless without it, we are a crowd without a common understanding. The Salvaging Of Civilisation |H. G. (Herbert George) Wells 
Beardsley was watching Arnold's fingers; there was something aimless and fretful as they pushed among the code-sealed tapes. We're Friends, Now |Henry Hasse 
He closed the door, whistling gloomily, went over to the piano and struck a few aimless chords. The Woman Gives |Owen Johnson 
They say such thoughts are sinful, but annihilation is preferable to an aimless, loveless existence. Alone |Marion Harland 
Always remember that a short anecdote well told is worth pages of aimless enumeration. English: Composition and Literature |W. F. (William Franklin) Webster