I was informed that this pagoda was formerly one of the most celebrated in China, but it has long ceased to be used. A Woman's Journey Round the World |Ida Pfeiffer 
The villages we visited, resembled more or less, that we had seen near the Half-way Pagoda. A Woman's Journey Round the World |Ida Pfeiffer 
The Herren Pagoda has three stories, with a pointed roof, and is distinguished for its external sculpture. A Woman's Journey Round the World |Ida Pfeiffer 
A single pagoda, five stories high, reminded us of the peculiar character of Chinese architecture. A Woman's Journey Round the World |Ida Pfeiffer 
But a man can go over to the Pagoda, and tell 'em 'All things are one,' and get three hundred identical opinions to agree with. The Belted Seas |Arthur Colton