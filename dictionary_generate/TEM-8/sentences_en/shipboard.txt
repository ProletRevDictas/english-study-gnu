Some data came from shipboard measurements of artificial light in the water. Night lights make even the seas bright |Carolyn Gramling |April 27, 2022 |Science News For Students 
Occasionally it does raise its head as they have BBC, MSNBC and Fox News on shipboard TV. Blogging my first overseas vacation since COVID |Peter Rosenstein |October 19, 2021 |Washington Blade 
Off the coast of Vietnam, a shipboard catastrophe cut short your chances to soar in combat. Why My Former Hero Shouldn’t Be President |Christopher Brownfield |October 7, 2008 |DAILY BEAST 
With him they were soon on the intimate terms of shipboard—terms that commit one to nothing in the future when land is reached. Bella Donna |Robert Hichens 
Although they had talked upon shipboard, this was the first time they had been en tête-à-tête. Bella Donna |Robert Hichens 
The Guards had much improved in health during their sojourn on shipboard, and were in good spirits and condition. The British Expedition to the Crimea |William Howard Russell 
Somehow there are barriers and conventionalities that one cannot break, for all the vaunted "freedom of shipboard." My Wonderful Visit |Charlie Chaplin 
He had about thirty miles to walk, and carried near five stone weight of goods, which he did not choose to put on shipboard. Yorkshire Oddities, Incidents and Strange Events |S. Baring-Gould