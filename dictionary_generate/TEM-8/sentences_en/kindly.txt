Team advanced statistics did not reflect kindly on the Nationals. The Nationals’ defensive philosophy is changing. Just look at the shift. |Jesse Dougherty |November 11, 2020 |Washington Post 
If she uses your overtures as a chance to get meaner, then don’t abandon the tactic — kindly stick to it. Carolyn Hax: Mother-in-law constantly demeans her |Carolyn Hax |November 11, 2020 |Washington Post 
“We would like to thank Zhuyun Dai from Carnegie Mellon University for kindly sharing her DeepCT retrieval results.” Could Google passage indexing be leveraging BERT? |Dawn Anderson |October 29, 2020 |Search Engine Land 
Facebook hasn’t taken kindly to the unofficial board’s public attacks. Facebook’s fight against outside oversight |Danielle Abril |October 29, 2020 |Fortune 
There’s no getting around the simple truth that the Joker is slow, to put it kindly. Nikola Jokić Is A Tortoise, And He’s Beating All Of The Hares |Chris Herring (chris.herring@fivethirtyeight.com) |September 24, 2020 |FiveThirtyEight 
We kindly inform these little jokers with the dubious jokes that they risk judicial proceedings they may not find funny at all. French Freak-Out Over Creepy Clowns |Tracy McNicoll |October 31, 2014 |DAILY BEAST 
But Bone, when we routed him out, could not promise us any more accommodation than he had so kindly given us the first night. The Stacks: H.L. Mencken on the 1904 Baltimore Fire |H.L. Mencken |October 4, 2014 |DAILY BEAST 
John kindly offered to take Hice to see that the “gay plot” has nothing to do with “recruiting” children, but with equal rights. Meet the Man Running for Congress on an Anti-Muslim Platform |Dean Obeidallah |July 24, 2014 |DAILY BEAST 
The letter kindly asked that the money be returned, but not a single cent has been transferred and nobody held to account. Creating Consequences for South Sudan’s Political Elite |Justine Fleischner |July 9, 2014 |DAILY BEAST 
The agent kindly instructs you to step aside while they investigate. Oregon Judge Grounds the Federal No-Fly List—and It’s High Time |Dean Obeidallah |June 26, 2014 |DAILY BEAST 
And if an earthly father would act thus wisely and thus kindly, "how much more your Father which is in Heaven?" God and my Neighbour |Robert Blatchford 
It must be confessed that he felt none too kindly towards Grandfather Mole. The Tale of Grandfather Mole |Arthur Scott Bailey 
The Professor thought very kindly of the dead cousin, whose money would provide for this great work. Uncanny Tales |Various 
Miss Anne smiled kindly, not dreaming of his perplexity, amused by his Southern warmth. The Joyous Adventures of Aristide Pujol |William J. Locke 
It was a sad day for Ramona and Alessandro when the kindly Hyers pulled up their tent-stakes and left the valley. Ramona |Helen Hunt Jackson