This is comedy based on a cold humor, detached, euphemistic, devoid of any generosity. Houellebecq’s Incendiary Novel Imagines France With a Muslim President |Pierre Assouline |January 9, 2015 |DAILY BEAST 
“ADD [Attention Deficit Disorder] is just a euphemistic way of saying, ‘I have limits,’” Brown writes. Can Self-Help Books Really Make a New You? |Lizzie Crocker |December 29, 2014 |DAILY BEAST 
Take it from Ben Bernanke, who keeps begging Congress (in that euphemistic Fed-speak way) to do something to help the economy. It’s Your Moment, Obama. Don’t Blow It. |Michael Tomasky |July 29, 2013 |DAILY BEAST 
Nowhere does this report, even in the most euphemistic terms possible, discuss the rage problem. The GOP’s Inept Autopsy |Michael Tomasky |March 18, 2013 |DAILY BEAST 
We may note here the euphemistic tendency to call powerful spirits by propitiatory names. The Sources and Analogues of 'A Midsummer-night's Dream' |Compiled by Frank Sidgwick 
But this is the usual attitude of the folk towards the "Good People," as indeed their euphemistic name really implies. More English Fairy Tales |Various 
It is said that on this occasion they were first called Eumenides (“the kindly”), a euphemistic variant of their real name. Encyclopaedia Britannica, 11th Edition, Volume 9, Slice 7 |Various 
The Pioneer was as euphemistic in speaking of death as was the Greek or Roman of classic times. The Life of Bret Harte |Henry Childs Merwin 
They trace their origin to the same source whence come the notions of propitiating the fairies by euphemistic names. British Goblins |Wirt Sikes