GPT-3 is getting a lot of oxygen lately because of its size, scope and capabilities. What does GPT-3 mean for the future of the legal profession? |Walter Thompson |August 28, 2020 |TechCrunch 
Unfortunately, one of things I’ve learned, because I’ve learned a lot about ventilation lately, is that the buildings, when we had the energy crisis, we went to LEAD buildings. ‘We Have the Power as a Community to Decide’ |Megan Wood |August 7, 2020 |Voice of San Diego 
Coral bleaching is happening more often and in more places lately. Let’s learn about coral reefs |Bethany Brookshire |July 22, 2020 |Science News For Students 
Los Angeles Times chief revenue officer Josh Brandau has been thinking a lot about golf lately. ‘This is a relationship business’: The in-person client meeting is beginning to make a comeback among publishers |Max Willens |July 13, 2020 |Digiday 
With that definition, you can see why loneliness may have spiked lately. Is There Really a “Loneliness Epidemic”? (Ep. 407) |Stephen J. Dubner |February 27, 2020 |Freakonomics 
On the other hand, right-wing activists have lately said, banning displays interferes with the exercise of religion. In Florida, ’Tis The Season for Satan |Jay Michaelson |December 7, 2014 |DAILY BEAST 
The first Bleachers single went to number one in alternative and all the shows have been sold out lately. The Rise of Jack Antonoff, the Taylor Swift Whisperer |Kevin Fallon |November 14, 2014 |DAILY BEAST 
Like it or not, forecasting is a “what have you done for me lately” profession. Why Is Nate Silver So Afraid of Sam Wang? |Daniel Altman |October 6, 2014 |DAILY BEAST 
For the record, Huckabee is no Johnny-come-lately to the cause of overhauling the tax system. Huckabee: ‘A Tax Is Punishment’ |Lloyd Green |September 29, 2014 |DAILY BEAST 
To my mind this is nothing compared to the flip-flops done lately by foreigners. My Coffee Klatch With Rand Paul |P. J. O’Rourke |September 27, 2014 |DAILY BEAST 
The church has lately been much enlarged, and the long-standing rebuke no more exists. Showell's Dictionary of Birmingham |Thomas T. Harman and Walter Showell 
Her sister-in-law pointed out to her that old Mr. Warrender had been very attentive lately. The Pit Town Coronet, Volume I (of 3) |Charles James Wills 
The von Fleischl has long been the standard instrument, but has lately fallen into some disfavor. A Manual of Clinical Diagnosis |James Campbell Todd 
It was her wildest dream come true; that is, it had come true, until lately. Tessa Wadsworth's Discipline |Jennie M. Drinkwater 
The ground lately in the shaft has been cleaner killas, and if any alteration, better ground. Life of Richard Trevithick, Volume II (of 2) |Francis Trevithick