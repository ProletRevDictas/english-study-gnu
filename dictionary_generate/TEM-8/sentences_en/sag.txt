And the series was implausibly shut out by both the Golden Globe and SAG Awards. ‘The Comeback’ Finale: Give Lisa Kudrow All of the Awards |Kevin Fallon |December 29, 2014 |DAILY BEAST 
What happens when the legs go, the arm tires, the eyes fade, the lungs sag? Football Great Bob Suffridge Wanders Through the End Zone of Life |Paul Hemphill |September 6, 2014 |DAILY BEAST 
I did get to meet Emma Thompson during the audition and I saw her recently at the SAG Awards and she said, “Oh, hello you!” ‘Game of Thrones’ Star Maisie Williams, aka Arya Stark, on Her Big Premiere Episode ‘Two Swords’ |Marlow Stern |April 7, 2014 |DAILY BEAST 
In fact, of all the precursor awards, the SAG is probably the best indicator of the eventual Oscar winner. The SAG Awards Best Speeches…And What They Mean for Oscar |Kevin Fallon |January 19, 2014 |DAILY BEAST 
Every SAG Best Actor winner has gone to win the Oscar stretching back to 2003. The SAG Awards Best Speeches…And What They Mean for Oscar |Kevin Fallon |January 19, 2014 |DAILY BEAST 
Misagàsà ang sag-ub nga natumba sa batu, The bamboo container went crack when it fell on the stones. A Dictionary of Cebuano Visayan |John U. Wolff 
Ikay nagsalsag sa sag-ub, You broke the bamboo tube water container into slivers. A Dictionary of Cebuano Visayan |John U. Wolff 
It would persistently sag down in spots, and each of these spots became a reservoir from which would descend an icy stream. Si Klegg, Book 2 (of 6) |John McElroy 
This would sag downwards under its own weight in a fine curve till the tip rested on the snow beneath. The Home of the Blizzard |Douglas Mawson 
Dorn Hackett sat moodily upon his low bed in a little cell of Sag Harbor jail. Belford's Magazine, Vol. II, No. 3, February 1889 |Various