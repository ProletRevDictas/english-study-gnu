Like mirrored sunglasses and beer koozies, the brainless summer entertainment is a warm-weather staple. The Hitman’s Wife’s Bodyguard Is Proof That Some Franchises Deserve to Die |Stephanie Zacharek |June 16, 2021 |Time 
This alone could be the preamble to an adequate brainless action comedy. The Hitman’s Wife’s Bodyguard Is Proof That Some Franchises Deserve to Die |Stephanie Zacharek |June 16, 2021 |Time 
It appears that simple creatures — including, now, the brainless hydra — can sleep. Sleep Evolved Before Brains. Hydras Are Living Proof. |Veronique Greenwood |May 18, 2021 |Quanta Magazine 
Further away, the scientists found a cluster made up of brainless things, such as plants and free‐living bacteria. What Is Life? Its Vast Diversity Defies Easy Definition. |Carl Zimmer |March 9, 2021 |Quanta Magazine 
Don't fancy that a man who has made millions—made it, understand—is brainless, and not well worth knowing. Dorothy at Skyrie |Evelyn Raymond 
He is utterly insignificant, rebuked, and humiliated,--even as a brainless beauty finds herself de trop in a circle of wits. Beacon Lights of History, Volume VII |John Lord 
Especially he had despised the young men as brainless cowards in regard to their views of women and conduct toward them. Rhoda Fleming, Complete |George Meredith 
I mean that the intellectual, the clever, invariably choose the insipid brainless girl. Lazy Thoughts of a Lazy Girl |Jenny Wren 
I am fit only to be the wife of an idle brainless man, with money and a title,' she said, in extreme self-contempt. Evan Harrington, Complete |George Meredith