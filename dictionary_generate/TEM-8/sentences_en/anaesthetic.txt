Ketamine has long been approved in the US as an anaesthetic. Ketamine’s promise as an antidepressant is being undermined by its lack of profit |Olivia Goldhill |August 6, 2020 |Quartz 
Corydon promptly did so, and another doctor who was to administer the anaesthetic came to her side. Love's Pilgrimage |Upton Sinclair 
In the early part of the war particularly many a young man had to face even serious operations without an anaesthetic. Health Through Will Power |James J. Walsh 
He replied that he would be ready for the operation at the time suggested, but that he would not take an anaesthetic. Health Through Will Power |James J. Walsh 
Arnald also revived the search for some anaesthetic that would produce insensibility to pain in surgical operations. A History of Science, Volume 2(of 5) |Henry Smith Williams 
The involuntary start that shook the pine cone from his hand freed Phil's nostrils of the anaesthetic. The Short Life |Francis Donovan