It lay open to the flyleaf, and there was an inscription penned in the fine handwriting that engravers try so hard to copy. The Lone Ranger Rides |Fran Striker 
Would it tire you very much to write it for me in the flyleaf of this Prayer-Book that Mr. Charnock has given me? The Three Brides |Charlotte M. Yonge 
With a silver pencil she wrote her name and address on the flyleaf of Persuasion, and gave the book to Rachel. The Voyage Out |Virginia Woolf 
I combine here the data of the two lists, calling the list on the flyleaf A and that on the lower margin B. Henry the Sixth |John Blacman 
Heber has inscribed a MS. note on the flyleaf to this effect. The Confessions of a Collector |William Carew Hazlitt