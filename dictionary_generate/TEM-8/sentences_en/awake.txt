She would go home and tell her now-husband about it and lay awake at night thinking about it. ‘Don’t wait to be perfect:’ 4 top startup tips from a unicorn’s founder and investor |Beth Kowitt |September 6, 2020 |Fortune 
To be woke is, in theory, to be awake to issues of social and racial justice. The American Fringes Go Mainstream |Nick Fouriezos |September 6, 2020 |Ozy 
Johnson was asleep when she came in, but the physician awoke her easily. Sent Home to Die |by Annie Waldman and Joshua Kaplan |September 2, 2020 |ProPublica 
While it’s unknown what kind of response Lukashenko expected when Belarus awoke to the news, the election results beggared belief to such an extreme that the country exploded. Misery in Minsk |Eugene Robinson |August 18, 2020 |Ozy 
Whenever that rat tried to rest, the scientists spun the table, nudging both rats awake and sometimes pushing them into the water. Why Sleep Deprivation Kills |Veronique Greenwood |June 4, 2020 |Quanta Magazine 
Now half-awake, we need all the help we can get in understanding our situation. American Democracy Under Threat for 250 Years |Jedediah Purdy |December 28, 2014 |DAILY BEAST 
I worked a lot of 11-7 shifts, and so had to stay awake, although most of the nights other people slept. James Patterson Goes Full ‘Fahrenheit 451’ With Burning Book Video |William O’Connor |November 25, 2014 |DAILY BEAST 
My partner Brandon and I awake at the crack of dawn for a canoe ride on the milky blue glacial waters of Lake Louise. Our Trip to The Climate War's Ground Zero |Darren Aronofsky |September 19, 2014 |DAILY BEAST 
How many times have your stories kept me awake at night wondering, like a child in the dark, what monsters lurk nearby? Stephen King, “Falling,” and My Father’s Poetry |Christopher Dickey |September 14, 2014 |DAILY BEAST 
But the moment suggests to him that if he is awake, then his reality is truly stranger and more menacing than he ever imagined. Haruki Murakami's Weird, Wonderful World |Malcolm Jones |August 15, 2014 |DAILY BEAST 
The Café tender was asleep in his chair; the porter had gone off; the sentinel alone kept awake on his post. Glances at Europe |Horace Greeley 
Finding him awake, he sat by his side and, with the earnestness of a nursery-maid, patted him off to slumber. The Joyous Adventures of Aristide Pujol |William J. Locke 
Or rather I suppose I was only half awake; but you seemed to open that door so easily that it quite startled me. Uncanny Tales |Various 
She slept lightly at first, half awake and drowsily attentive to the things about her. The Awakening and Selected Short Stories |Kate Chopin 
She lay wide awake composing a letter which was nothing like the one which she wrote next day. The Awakening and Selected Short Stories |Kate Chopin