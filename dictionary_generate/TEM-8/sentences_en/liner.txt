If you don’t think your bag can hack it on its own, slip it inside another bag, or use a sleeping bag liner for a double layer of warmth. How to stay warm while sleeping in the frigid outdoors |Alisha McDarris |February 12, 2021 |Popular-Science 
I usually won’t switch to a lightweight or liner glove until temps are well into the 40s and I’m traveling uphill. The Best Ski Gloves Are the Simplest Ones |Joe Jackson |February 10, 2021 |Outside Online 
That’s why Patagonia wisely included a thin nylon mitten that folds out of the cuff of these liner gloves and covers your fingers. My Favorite Winter Gloves for Various Activities |Jakob Schiller |February 3, 2021 |Outside Online 
If being used in extreme cold, we recommend using these as a glove liner. Best winter gloves: Our picks for touch screen gloves, ski gloves, and more |PopSci Commerce Team |February 2, 2021 |Popular-Science 
The Dakota is made from a thick, stretchy woven nylon shell, backed by a heavy bonded fleece liner, so they’re not only extremely durable but also extremely warm. The Best Fleece-Lined Pants for the Outdoors |Wes Siler |January 15, 2021 |Outside Online 
Opposite is a red-brick monastery leaning like an ocean liner in the snow. Russia’s Gold Digger Academy |Peter Pomerantsev |November 11, 2014 |DAILY BEAST 
Pick up records from that time and chances are Hentoff wrote the liner notes. The Stacks: John Coltrane’s Mighty Musical Quest |Nat Hentoff |October 18, 2014 |DAILY BEAST 
The senior Senator was, as usual, highly disciplined, avoiding the substance of some questions with one-liner talking points. Meet the Sports Host Who Could Decide the McConnell-Grimes Matchup |Jonathan Miller |October 13, 2014 |DAILY BEAST 
He purchased two first-class tickets on the France, a luxury liner passage to England, for his new girlfriend and himself. The Climax of ‘Portnoy’s Complaint’ |Robert Hofler |May 14, 2014 |DAILY BEAST 
Mary, Queen of Scots on Reign is more likely pop off a bustier than a one-liner. A Fan Night Out: The Return of Veronica Mars |Scott Bixby |March 14, 2014 |DAILY BEAST 
Northward, toward the Pole, were liner lanes in the higher levels, but here was a deserted sector. Astounding Stories, May, 1931 |Various 
Coming alongside the crushed hull of the interplanetary liner, we made an inspection of its position. Spacewrecked on Venus |Neil R. Jones 
He and the others now floated as smoothly as though under water toward a wrecked liner at the Pallas' right. The Sargasso of Space |Edmond Hamilton 
"Let's get back and let them know about it," Liggett urged, and they climbed back out of the liner. The Sargasso of Space |Edmond Hamilton 
It seemed that some months before he had been a purser on an East Indian liner. The Blot on the Kaiser's 'Scutcheon |Newell Dwight Hillis