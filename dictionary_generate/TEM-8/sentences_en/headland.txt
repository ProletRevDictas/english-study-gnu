I pointed out little shimmering fish and stared out at the deep green headland. The Quiet Ache of Children's Pandemic Birthdays |Katie Gutierrez |February 3, 2022 |Time 
You rock up to Nazaré, walk down the headlands, and you can see just how big the waves really are. What You Missed: Surfer Andrew Cotton on the Impact of HBO’s “100 Foot Wave” |Fred Dreier |December 6, 2021 |Outside Online 
Preserves like Cutler Coast Public Reserved Land connect cobble beaches and headlands with blueberry barrens, pine forests, and peatlands. The Best East Coast Road Trips |kklein |August 8, 2021 |Outside Online 
Columbus, proceeding towards Cuba, named the headland Cabo de Cruz on this day. The Every Day Book of History and Chronology |Joel Munsell 
As you know, this headland that we're on is called the Trwyn, and 'trwyn' simply means a nose or a promontory. Mushroom Town |Oliver Onions 
The night paled; the Trwyn light went out; and off the headland a seal disported itself in the icy sea. Mushroom Town |Oliver Onions 
To the south was a headland, which our skipper informed us was the north end of Amelia Island. In the Wilds of Florida |W.H.G. Kingston 
From the ancient fort on the headland to the Casa Blanca and the city beyond, it was a progression of delicious sights and sounds. Gardens of the Caribbees, v. 1/2 |Ida May Hill Starr