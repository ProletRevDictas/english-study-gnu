They used their monologues to snipe at each other, with Letterman piling on Jay just for kicks. Is Jay Leno Facing Another NBC Coup in Favor of Jimmy Fallon? |Howard Kurtz |March 4, 2013 |DAILY BEAST 
Snipe had been kicked out of his home, in the Bronx, and needed a place to crash. The Amazing Superheroes of New York City |Matthew Shaer |August 7, 2011 |DAILY BEAST 
A third stopped, and Snipe trotted forward to chat to the driver. The Amazing Superheroes of New York City |Matthew Shaer |August 7, 2011 |DAILY BEAST 
Plenty of couples snipe at each other in sometimes embarrassing ways in front of company. Why Hillary Lashed Out |Tina Brown |August 12, 2009 |DAILY BEAST 
Mr. MacCulloch also writes me word that the Solitary Snipe occasionally occurs. Birds of Guernsey (1879) |Cecil Smith 
Dottrel and yellow silk,—inside of Snipe's wing, and pale yellow silk,—hooks No. 2. The Teesdale Angler |R Lakeland 
Light Blue,—inside of Snipe's wing,—body light Drab silk,—tail and legs grizzled hackle. The Teesdale Angler |R Lakeland 
Light Blue,—inside of Snipe's wing,—light drab silk for body,—legs and tail grizzled hackle. The Teesdale Angler |R Lakeland 
The blood was pouring from the wound, and he lay motionless, with the snipe dead on the ground about six inches from his nose. The Animal Story Book |Various