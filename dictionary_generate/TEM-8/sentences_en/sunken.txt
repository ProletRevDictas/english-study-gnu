While the ISS is larger than most trash in the ocean, other experts are less worried about its immense size in comparison to other sunken junk. There’s a lot we don’t know about the International Space Station’s ocean grave |Tatyana Woodall |February 23, 2022 |Popular-Science 
This gives rise to a ring-shaped island, or smattering of islands, around the bowl of water, or lagoon, left by the sunken volcano. Scientists Say: Atoll |Maria Temming |November 15, 2021 |Science News For Students 
Most of the graves are invisible to the naked eye or revealed only as sunken rectangular shapes. African American gravesites detected near the Capital Beltway will be spared in road-widening plans |Katherine Shaver |September 9, 2021 |Washington Post 
The work is adventurous and challenging—he’d done everything from searching for a sunken helicopter to cleaning up after the British Petroleum oil spill. The Leg at the Bottom of the Sea |David Kushner |May 24, 2021 |Outside Online 
He thought sunken whale carcasses would be a good place to start. Whales get a second life as deep-sea buffets |Stephen Ornes |October 15, 2020 |Science News For Students 
He has sunken eyes and a narrow black beard speckled with gray. Heart of Darkness: Into Afghanistan’s Taliban Valley |Matt Trevithick, Daniel Seckman |November 15, 2014 |DAILY BEAST 
Now in his early thirties, his cheeks are sunken from smoking too much hash. Obama’s Deadly Informants: The Drone Spotters of Pakistan |Umar Farooq, Syed Fakhar Kakakhel |November 12, 2014 |DAILY BEAST 
There was a first-class lounge with a sunken well and cocktail bar. The Sexy Dream of the 747 |Clive Irving |October 26, 2014 |DAILY BEAST 
After he disembarked the sunken ship, Schettino told reporters that he accepts responsibility for his role in the disaster. Captain Schettino Returns to Costa Concordia Crime Scene |Barbie Latza Nadeau |February 27, 2014 |DAILY BEAST 
Then he pointed to my sunken cheeks where a couple gray whiskers poked through. The Fourth War: My Lunch with a Jihadi |Elliot Ackerman |January 21, 2014 |DAILY BEAST 
The sunken eyes, the tangled masses of raven hair, the look of exhaustion and hopeless woe. The World Before Them |Susanna Moodie 
Beyond it, temple roofs—black keels of sunken vessels—cut a sky still powdered thick with stars. The Dragon Painter |Mary McNeil Fenollosa 
And then the salt tears flowed down his sunken cheeks and formed a pool on the floor. The Whale and the Grasshopper |Seumas O'Brien 
Had the piles been hollow, or too short to reach bed-rock, it would either have sunken or tumbled. Ancestors |Gertrude Atherton 
This morning he came up, his cheeks more sunken, his eyes more hollow. Prison Memoirs of an Anarchist |Alexander Berkman