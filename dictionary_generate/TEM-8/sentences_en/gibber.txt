Whether gibber comes from geber, as Dr Johnson contends, must remain in doubt. A Select Collection of Old English Plays, Volume 10 (of 15) |Various 
Gibberish is no doubt derived from gibber, and it means idle nonsense. A Select Collection of Old English Plays, Volume 10 (of 15) |Various 
See those things that ape our species dance and gibber round the famishing, hunted wretch. Lucretia, Complete |Edward Bulwer-Lytton 
Never a word they said, never a sound from the mouths that seemed to gibber. Twelve Stories and a Dream |H. G. Wells 
If it had not been for the generous supply of "days off" that the Skipper allowed us, we should by February have begun to gibber. Adventures of a Despatch Rider |W. H. L. Watson