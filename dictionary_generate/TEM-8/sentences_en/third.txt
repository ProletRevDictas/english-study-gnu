The third suspect, an 18-year-old named Hamyd Mourad, who turned himself in, is part of the same extended family. France Mourns—and Hunts |Nico Hines, Christopher Dickey |January 8, 2015 |DAILY BEAST 
Officials have said the war to reclaim upward of a third of Iraq and a quarter of Syria from ISIS could take years. Pentagon Doesn’t Know How Many People It’s Killed in the ISIS War |Nancy A. Youssef |January 7, 2015 |DAILY BEAST 
Third parties in turn quibbled with his accounts, and he was irritated, but not overly so. I Tried to Warn You About Sleazy Billionaire Jeffrey Epstein in 2003 |Vicky Ward |January 7, 2015 |DAILY BEAST 
The third problem is the evidence of corroborating witnesses. Buckingham Palace Disputes Sex Allegations Against Prince ‘Randy Andy’ |Tom Sykes |January 4, 2015 |DAILY BEAST 
Murders are slightly down from 414 last year, but have fallen by about one—third since 2003. America’s 2014 Murder Capital |Brandy Zadrozny |January 3, 2015 |DAILY BEAST 
But the Mexican caballeros had no notion of coming up to the scratch a third time. Blackwood's Edinburgh Magazine, No. CCCXXXIX. January, 1844. Vol. LV. |Various 
This was somewhat tiresome; and, after a rather feeble attempt at a third laugh, Davy said, "I don't feel like it any more." Davy and The Goblin |Charles E. Carryl 
It occurred to him then, for the first time, that a third resource was open—he might cut the rope, and let the kite go free! The Giant of the North |R.M. Ballantyne 
On the third day after the declaration of his recall, Ripperda took his official leave, and presented his son in his new office. The Pastor's Fire-side Vol. 3 of 4 |Jane Porter 
The third boat and kite had been damaged beyond repair, but the two left were sufficient. The Giant of the North |R.M. Ballantyne