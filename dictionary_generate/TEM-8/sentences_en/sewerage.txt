As recently as 2002, Rahat, like all six of the other government-planned towns, lacked a completed sewerage system. The Negev Desert’s Vanishing Bedouin |Ben Hattem |June 16, 2014 |DAILY BEAST 
A water-borne sewerage system began to be introduced in 1906. Encyclopaedia Britannica, 11th Edition, Volume 15, Slice 4 |Various 
The valuable portions of the sewerage were utilized on adjacent vegetable farms. The Harris-Ingram Experiment |Charles E. Bolton 
All sewerage and garbage were promptly removed, and used to enrich the suburban market-gardens. The Harris-Ingram Experiment |Charles E. Bolton 
A permanent board of water and sewerage commissioners was created by an act of April 2, 1869. A History of the City of Brooklyn and Kings County Volume II |Stephen M. Ostrander 
It is now almost a trite thing to show how closely connected imperfect sewerage is with disease. The Claims of Labour |Arthur Helps