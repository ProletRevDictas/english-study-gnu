We did ThunderAnt stuff for ourselves and just put it online, and then it blossomed into something else. Coffee Talk with Fred Armisen: On ‘Portlandia,’ Meeting Obama, and Taylor Swift’s Greatness |Marlow Stern |January 7, 2015 |DAILY BEAST 
But we are afraid and we wonder to ourselves who will be next. Mexico’s Priests Are Marked for Murder |Jason McGahan |January 7, 2015 |DAILY BEAST 
What could be more important, to make sure that side of things is right before we tie ourselves to someone forever? ‘Downton Abbey’ Review: A Fire, Some Sex, and Sad, Sad Edith |Kevin Fallon |January 5, 2015 |DAILY BEAST 
Expectations, reasonable or unrealistic, remain so even if we impose them on ourselves. Why Singles Should Say ‘I Don’t’ to The Self-Marriage Movement |Tim Teeman |December 30, 2014 |DAILY BEAST 
Should we cancel gatherings, reunions, excursions, or throw ourselves into them with even more gratitude for one another? The Media's Pro-Torture Cheerleaders |Jedediah Purdy |December 10, 2014 |DAILY BEAST 
It was in the college stage that most of us made out our religion and made it real for ourselves. The Salvaging Of Civilisation |H. G. (Herbert George) Wells 
Calkilate we should do ourselves more harm than him by shooting down his people. Blackwood's Edinburgh Magazine, No. CCCXXXIX. January, 1844. Vol. LV. |Various 
We won't want much more ourselves, for we are nearly at our last landing place. Squinty the Comical Pig |Richard Barnum 
That will give us time to turn about us, and to prepare ourselves against similar unpleasant casualties. Blackwood's Edinburgh Magazine, No. CCCXXXIX. January, 1844. Vol. LV. |Various 
Oh, but surely if we have to call ourselves Wurzel-Flummery it would count as earned income. First Plays |A. A. Milne