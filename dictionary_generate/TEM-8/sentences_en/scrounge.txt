Since Holtsman did not receive much guidance or support, he had to scrounge for supplies and ran his own operations. On the Trail of Nazi Counterfeiters |Dr. Kevin C. Ruffner |September 20, 2014 |DAILY BEAST 
When you earn money, cash falls from the sky, meaning you have to scrounge on the floor for dollar bills. Kim Kardashian Isn't the Butt of Jokes Anymore |Amy Zimmerman |August 14, 2014 |DAILY BEAST 
“I have been on food stamps and had to scrounge for money,” she says. Phil Spector’s Unlikely Defender |Christine Pelisek |November 2, 2012 |DAILY BEAST 
I was able to scrounge up $9,000 and then put in $9,000 of my own, so I was in for $18,000. Billy Sammeth, the Manager Fired by Cher and Joan Rivers, Tells His Side of the Story |Kevin Sessums |February 14, 2012 |DAILY BEAST 
"Let's scrounge anything we can find that runs on gasoline or coal oil," said Al Miner. The Year When Stardust Fell |Raymond F. Jones 
I was never any good at this so Bruce used to scrounge for us. The Biography of a Rabbit |Roy Benson 
While my company was in support one day my corporal and I managed to scrounge into a pill-box away from the awful mud. 500 of the Best Cockney War Stories |Various 
One day we missed one of our fellows, a Cockney, for about two hours, and guessed he was on the "scrounge" somewhere or other. 500 of the Best Cockney War Stories |Various 
Back from a spell behind Ypres in 1915, a few of us decided to scrounge round for a hair-cut. 500 of the Best Cockney War Stories |Various