Bars and restaurants need to invest in new cups, lids, branding, training, and marketing. Bar Rescue: Pandemic Edition |jakemeth |September 15, 2020 |Fortune 
Its metal lid and body are professional-looking yet still come at an affordable, mid-range value. The best stockpots for your kitchen |PopSci Commerce Team |September 9, 2020 |Popular-Science 
Opening the lid repeatedly to grab a cold one will vastly reduce how long it stays frigid in there. The Absolute Beginner's Guide to Camp Cooking |AC Shilton |September 5, 2020 |Outside Online 
The lid not only seals well to the top of a container, it also snaps onto the bottom of a smaller container in the set—a joy for any organizational obsessive. Dishwasher-safe food storage containers that make meal prep a breeze |PopSci Commerce Team |August 6, 2020 |Popular-Science 
Society could keep a lid on such a re-emergence by keeping up their social distancing. COVID-19: When will it be safe to go out again? |Jonathan Lambert |March 27, 2020 |Science News For Students 
“Hard hat…heavy jacket…welding gloves…fish landing net…a sheet…a big Tupperware bin with a lid,” he says. He Faces Jail for Rescuing Baby Eagles |Michael Daly |November 2, 2014 |DAILY BEAST 
A Saudi Arabian television ad for Viagra shows a man struggling to push a straw through the lid of his beverage. Laughter Will Be the Legacy of Viagra |Samantha Allen |October 2, 2014 |DAILY BEAST 
Trying to keep a lid on yet more rumors about your sexuality. Dear John, It Gets Better: A Letter to Travolta |Tim Teeman |July 24, 2014 |DAILY BEAST 
“I want to use information to put a lid on that local corruption before it gets too extreme,” Simon told The Daily Beast. Who’s Watching Your Statehouse? No One. |Eleanor Clift |July 19, 2014 |DAILY BEAST 
Crumbs avoided advertising and contracted production out, thus keeping a lid on costs. The Cupcake Boom’s Sugar High Finally Crashes |Daniel Gross |July 8, 2014 |DAILY BEAST 
Taking off the lid she emptied its contents in a heap—silver and copper with one or two gold pieces intermixed—on the table. The Garret and the Garden |R.M. Ballantyne 
She glanced uneasily at Gwynne and fancied she could hear him slam the lid of his breeding upon a supercilious sputter. Ancestors |Gertrude Atherton 
Gwynne pressed the little gilt nob, and as the lid flew up Isabel cried out, with delight. Ancestors |Gertrude Atherton 
Thats music for you, chuckled the old man, raising the lid to see if the water had boiled sufficiently. The Fifth String |John Philip Sousa 
Marie lifted the lid from the stove, and a warm red glow of reflected light filled the little kitchen. The Amazing Interlude |Mary Roberts Rinehart