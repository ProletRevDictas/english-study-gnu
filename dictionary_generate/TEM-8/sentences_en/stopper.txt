Long’s Vintner Select line is only available online, but he has recently released a cabernet sauvignon and a chardonnay to national distribution, under a unique twist-off cork stopper. This $14 Italian red is a gem that invites a pairing with a pot roast or pasta |Dave McIntyre |February 12, 2021 |Washington Post 
A proprietary stopper replaces the traditional cork at the top of the bottle, allowing the bottle to interface with the system’s main piece. The best, most practical Valentine’s Day gifts for any kind of partner |PopSci Commerce Team |February 8, 2021 |Popular-Science 
If teams want someone a little bit older, they can go with Clark, a defensive stopper. The Stars Who Could Be On The Move During WNBA Free Agency |Howard Megdal |January 19, 2021 |FiveThirtyEight 
It also comes with a workout mat and a set of wheel stoppers. Gift Guide: What you need to work(out) from home |Rachel King |November 2, 2020 |Fortune 
On the bottom of the Toddy there is a filter and a stopper that keeps the coffee in the container. Gear to make every day feel like National Coffee Day |PopSci Commerce Team |September 29, 2020 |Popular-Science 
We decided to do some hitchhiking or become what the locals called an “auto-stopper.” Michael Clinton: How to Become a Globe-Trotter |Michael Clinton |March 4, 2013 |DAILY BEAST 
Spill stopper  This is a silicone lid that sits on top of your pots and prevents them from boiling over. The 2012 Holiday Kitchen Gift Guide |Megan McArdle |December 13, 2012 |DAILY BEAST 
First, it's a conversation stopper, especially in the United States. Show, Don't Tell: Why the Apartheid Analogy Falls Flat |Hussein Ibish |March 26, 2012 |DAILY BEAST 
On the show-stopper "Turn It Off," sung by a closeted missionary struggling with his sexuality. Broadway's Real-Life Mormon Missionary |Sally Atkinson |June 7, 2011 |DAILY BEAST 
This is an effortless show-stopper and the leftovers make for fabulous sandwiches, too. 5 Recipes for Holiday Cheer |Cookstr.com |December 1, 2009 |DAILY BEAST 
When we lift the stopper the fluid for a moment falls straight down through the opening. Outlines of the Earth's History |Nathaniel Southgate Shaler 
It is so heavy that an ordinary cork would soon be forced out by it, therefore an iron stopper must be screwed in. St. Nicholas, Vol. 5, No. 5, March, 1878 |Various 
If live grasshoppers, or similar bait, is desired the cork can be used unnotched to form a watertight stopper. The Boy Mechanic, Book 2 |Various 
Not infrequently one end of the handle terminated in a tobacco stopper. Chats on Household Curios |Fred W. Burgess 
If a stopper is used, the workman must see that it is taken out. Elements of Plumbing |Samuel Dibble