He hardly spares any codified religion from his angry denunciations, including Orthodoxy. Frank Schaeffer, the Atheist Who Believes in God |Nick Tabor |August 3, 2014 |DAILY BEAST 
But his picture is far richer than the grim founder worship usually found in American political orthodoxy. One U.S. Constitution Just Wasn’t Enough |Tom Arnold-Forster |July 4, 2014 |DAILY BEAST 
Paul has generated positive headlines with a pivot away from party orthodoxy in recent months. Did the RNC Just Set the Table for Rand Paul? |David Freedlander |May 14, 2014 |DAILY BEAST 
What was his argument against the moderate McMahon, who was so willing to buck his own party orthodoxy? Michael Grimm’s Unhappily Ever After |David Freedlander |April 29, 2014 |DAILY BEAST 
In short, the proposals of the 2012 Republican vice presidential nominee have already become party orthodoxy. Will Ryan Budget Doom GOP in '16? |Ben Jacobs |April 2, 2014 |DAILY BEAST 
The new creed, called the King's Book, approved by the houses of convocation, and made the standard of English orthodoxy. The Every Day Book of History and Chronology |Joel Munsell 
He has been all his life an ardent Whig, and Clay and Webster were his standards of political orthodoxy. Portrait and Biography of Parson Brownlow, The Tennessee Patriot |William Gannaway Brownlow 
By this time a sad departure from primitive orthodoxy of belief had already taken place. The Catacombs of Rome |William Henry Withrow 
Even orthodoxy must trip it on tiptoe; there was always some prejudice, some susceptibility to consider. The Daughters of Danaus |Mona Caird 
It was, however, resolutely opposed, and an outward orthodoxy rigidly kept up. Ancient Faiths And Modern |Thomas Inman