Think of this, ye who talk, not always without reason, of "factory slaves" and the meagre rewards of labor in America. Glances at Europe |Horace Greeley 
Aguinaldoʼs scholarship is too meagre for an elevated position, and his dignity and self-respect too great for an inferior one. The Philippine Islands |John Foreman 
This rather meagre information concerning him was furnished by a certain Madame Komorn. Repertory Of The Comedie Humaine, Complete, A -- Z |Anatole Cerfberr and Jules Franois Christophe 
The poor fellow's health so gave way under this meagre diet, that he died before his course of study was finished. Friend Mac Donald |Max O'Rell 
This is always repeated in one unvarying phase of the most jejune and meagre character. The Catacombs of Rome |William Henry Withrow