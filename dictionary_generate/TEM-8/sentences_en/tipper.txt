Elizabeth Dole and Tipper Gore would both have been married to former presidents; they were both well-liked. Does the Spouse Bump Exist? History Suggests First Ladies Don’t Often Tilt Elections |Mark McKinnon |April 15, 2012 |DAILY BEAST 
As Sandra Kobrin wrote on Women's eNews, women have reached "The Tipper Point." The End of Male Privilege: Women Fed Up With Bad Behavior |Leslie Bennetts |May 23, 2011 |DAILY BEAST 
We constantly ask ourselves in the writers' room: Will she end up like Hillary or Tipper? TV's Scorned Political Wife |Robert King |July 13, 2010 |DAILY BEAST 
More crassly, why are Bill and Hillary together while Al and Tipper have separated? TV's Scorned Political Wife |Robert King |July 13, 2010 |DAILY BEAST 
Tipper launched her family-values crusade with her own perfect family smiling at her back. The Way They Were |The Daily Beast |June 9, 2010 |DAILY BEAST 
I soon succeeded in making matters pleasant with Mrs Tipper again. Chambers's Journal of Popular Literature, Science, and Art |Various 
I was very soon able, with dear old Mrs Tipper's ready sanction, to give Becky a step in life. Chambers's Journal of Popular Literature, Science, and Art |Various 
As he mounted the steps to the tipper deck, he saw a brilliant light shine from the bridge. Story of Chester Lawrence |Nephi Anderson 
Dear little Mrs Tipper was a bright example of content and happiness. Chambers's Journal of Popular Literature, Science, and Art, No. 695 |Various 
It had turned out there were some hundreds lying in Mrs Tipper's name at the banker's. Chambers's Journal of Popular Literature, Science, and Art, No. 696 |Various