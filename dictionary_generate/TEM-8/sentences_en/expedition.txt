If you want to climb it, you usually start your expedition from Montemonaco. Investors continue to push global stocks into record territory |Bernhard Warner |August 24, 2020 |Fortune 
To learn about the microbes living in sub-seafloor sediments, scientists must typically go on drilling expeditions to retrieve samples of them. ‘Zombie’ Microbes Redefine Life’s Energy Limits |Jordana Cepelewicz |August 12, 2020 |Quanta Magazine 
OceanSky Cruises—based, perhaps unsurprisingly, in Sweden—is currently taking reservations for expeditions to the North Pole in the 2023-2024 season. Airships Are No Longer a Relic of the Past; You Could Ride in One by 2023 |Vanessa Bates Ramirez |August 4, 2020 |Singularity Hub 
Suffice it to say, the expedition, bolstered by complex preparation, was massive with over 35,000 soldiers from different nationalities. History of the Crusades: Origins, Politics, and Crusaders |Dattatreya Mandal |March 23, 2020 |Realm of History 
These expeditions are commonly known as the Peasants’ Crusades. History of the Crusades: Origins, Politics, and Crusaders |Dattatreya Mandal |March 23, 2020 |Realm of History 
Two years into an Arctic expedition, they were forced to abandon ship a thousand miles north of Siberia. The Best Nonfiction Books of 2014 |William O’Connor |December 14, 2014 |DAILY BEAST 
At Studio Stagetti, I shot a man with more picks and axes than I have ever seen outside an arctic expedition. Damien Hirst’s Army of Geppettos |Tim Teeman |December 2, 2014 |DAILY BEAST 
Going to the library was like a treasure hunt, an expedition. The Singular Artist of New Yorkistan |Lizzie Crocker |November 14, 2014 |DAILY BEAST 
He sees all the more reason to continue with plans for their next expedition, in June 2015. How Amelia's Plane Was Found |Michael Daly |October 30, 2014 |DAILY BEAST 
Companies across the country donated supplies and equipment to assist him with his expedition. Victor Mooney’s Epic Adventure for His Dead Brother |Justin Jones |October 19, 2014 |DAILY BEAST 
After the battle of the Pyramids he fell sick, and before the Syrian expedition, applied to return to France. Napoleon's Marshals |R. P. Dunn-Pattison 
Lawrence and Dan were told of the danger that threatened Fulton, and they determined to accompany Guitar in his expedition. The Courier of the Ozarks |Byron A. Dunn 
The wealth, which Ripperda deemed necessary for his expedition, was sewed into various parts of their muleteer garments. The Pastor's Fire-side Vol. 3 of 4 |Jane Porter 
The filibustering expedition set out in three small ships on the 28th of September. Napoleon's Marshals |R. P. Dunn-Pattison 
But scarcely had the new ambassador arrived at his destination when he heard of Bonaparte's projected expedition to Egypt. Napoleon's Marshals |R. P. Dunn-Pattison