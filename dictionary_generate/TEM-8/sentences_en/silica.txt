The team encoded different images into DNA and encapsulated each file in a small silica particle labeled with single-stranded DNA “barcodes” corresponding to its contents. The world in a mug |Anne Trafton |August 24, 2021 |MIT Technology Review 
Then remove them, brush off any remaining sticky gel, and save the leftover silica for another day. How to preserve flowers in 4 easy ways |empire |August 20, 2021 |Popular-Science 
It’s a mere speck of matter — a piece of silica crystal no bigger than a virus, levitated in a light beam. How Big Can the Quantum World Be? Physicists Probe the Limits. |Philip Ball |August 18, 2021 |Quanta Magazine 
It’s also made from a different compound, with a lower silica content, and uses a tread pattern with larger voids and fewer sipes. Everything You Need to Know About All-Terrain Tires |Wes Siler |April 6, 2021 |Outside Online 
Although they, too, lost sulfur to the surrounding solution, they kept their silica crust. Fossil mimics may be more common in ancient rocks than actual fossils |Carolyn Gramling |February 9, 2021 |Science News 
Silica is an invariable constituent of the ash, but in most plants occurs but in small quantity. Elements of Agricultural Chemistry |Thomas Anderson 
Quartz consists of pure silica, and when in large masses is one of the most indestructible rocks. Elements of Agricultural Chemistry |Thomas Anderson 
Silica and the alkalies occur generally, though not invariably, in all three. Elements of Agricultural Chemistry |Thomas Anderson 
In general, sand consists of finely broken crystals of silica or quartz. Outlines of the Earth's History |Nathaniel Southgate Shaler 
Many natural waters present us with silica in a dissolved state, although it is not soluble in pure water. The Wonder Book of Knowledge |Various