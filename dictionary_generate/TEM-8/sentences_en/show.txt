Policemen on the show joke about prison riots, bomb threats, and the shooting of unarmed civilians. 'Babylon' Review: The Dumb Lives of Trigger-Happy Cops |Melissa Leon |January 9, 2015 |DAILY BEAST 
As soon as this attack [happened], Paris citizens came together to show were are not afraid, we are Charlie Hebdo. France Kills Charlie Hebdo Murderers |Nico Hines |January 9, 2015 |DAILY BEAST 
And they might not have to wait that long to show their political heft. Asian-Americans Are The New Florida |Tim Mak |January 8, 2015 |DAILY BEAST 
Not actual CIA agents, but U.S. government personnel who have worked very closely with the CIA, and who are fans of the show. ‘Archer’ Creator Adam Reed Spills Season 6 Secrets, From Surreal Plotlines to Life Post-ISIS |Marlow Stern |January 8, 2015 |DAILY BEAST 
Earlier this week, Huckabee ended his Fox News talk show so he could spend time mulling another bid for the Republican nomination. Huckabee 2016: Bend Over and Take It Like a Prisoner! |Olivia Nuzzi |January 8, 2015 |DAILY BEAST 
None other would dare to show herself unveiled to a stranger, and a white man at that. The Red Year |Louis Tracy 
I shall show how it is possible thus to prolong life to the term set by God. The Mediaeval Mind (Volume II of II) |Henry Osborn Taylor 
Not only do children thus of themselves extend the scope of our commands, they show a disposition to make rules for themselves. Children's Ways |James Sully 
He called upon the Order to show their title-deeds, but was met with a contemptuous refusal. The Philippine Islands |John Foreman 
If any one has lost his temper, as well as his money, he takes good care not to show it; to do so here would be indeed bad form. The Pit Town Coronet, Volume I (of 3) |Charles James Wills