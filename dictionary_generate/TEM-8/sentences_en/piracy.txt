Much of the piracy appears to be coming from the US and India. “Wonder Woman 1984” foreshadows Hollywood’s giant piracy problem |Adam Epstein |January 5, 2021 |Quartz 
With governments preoccupied by the public health crisis, they’ve had fewer resources to devote to combating piracy. Hey Joe: Welcome to the New World Order |Daniel Malloy |January 3, 2021 |Ozy 
It was not designed to deal with piracy at the scale that was about to erupt. Art has been brutalized by tech’s giants. How can it survive? |Konstantin Kakaes |December 23, 2020 |MIT Technology Review 
The most common technique to limit piracy is by adding watermarks that help trace which customer originally received the movie. Movie piracy is on the rise as studios bypass theatrical releases |radmarya |October 6, 2020 |Fortune 
For years, piracy attacks on major shipping routes have been on a decline. Is Maritime Piracy Back from the Dead? |Eromo Egbejule |August 25, 2020 |Ozy 
The record business is 98 percent piracy everywhere on the planet. Quincy Jones Talks Chicago’s Mean Streets, Why Kanye West Is No Michael Jackson, and Bieber |Marlow Stern |September 25, 2014 |DAILY BEAST 
As with any new device, fears came up about privacy, security, the fear of looking like a dork, and piracy. Google Glass Is Helping Doctors Save Lives. Maybe It’s Time to Reconsider the Poorly Received Technology. |Valerie Vande Panne |March 28, 2014 |DAILY BEAST 
This also an area known for piracy, which means that military radar surveillance would have been highly active. The Botched Hunt for Malaysia Airlines Flight 370 |Clive Irving |March 12, 2014 |DAILY BEAST 
First charged with “piracy,” each member of the retinue now faces seven years in jail if found guilty of “hooliganism.” The Return of Russian Hard Power? |Michael Weiss |November 23, 2013 |DAILY BEAST 
Piracy has transformed into a well-developed business and I am part of that business. The Pirate Negotiator |Caitlin Dickson |November 14, 2013 |DAILY BEAST 
William Kidd with others executed at Execution dock, London, for piracy. The Every Day Book of History and Chronology |Joel Munsell 
It was the beginning of a policy which was to put an end to the piracy which had prevailed for centuries on those waters. Stories of Our Naval Heroes |Various 
If you give me those documents, I will show them to the Captain—but he is not the sort of man—this is mere piracy, after all! The Lord of the Sea |M. P. Shiel 
When the victim of Algerian piracy stood on the deck, dripping and indignant, and told his tale of woe, we were delighted. Humanly Speaking |Samuel McChord Crothers 
He used to make voyages from port to port, partly for commerce, but more especially for piracy. Female Warriors, Vol. I (of 2) |Ellen C. Clayton