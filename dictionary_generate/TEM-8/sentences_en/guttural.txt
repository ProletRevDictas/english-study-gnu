On the steps of city hall, Mayor David Anderson hollered a guttural “Wahhh!!!” Two Rich Men Decided to Fund a Failing City. Some People Say They Made It Worse |Alana Semuels/Kalamazoo, Michigan |November 4, 2021 |Time 
Those snorting and obnoxious guttural sounds continue to hound you all night long. Here’s an anti-snoring device that guarantees improved sleep |Quinn Gawronski |July 20, 2021 |Popular-Science 
The acceleration literally took my breath away, and the guttural exhaust rumble—which was downright primal—turned more than a few heads. Smart rides for trying times |Joe Phillips |May 21, 2021 |Washington Blade 
The researchers noted that a relatively large body size may protect guttural toads against predators. Readers ask about shrinking toads, defining distance in an expanding universe and more |Science News Staff |February 7, 2021 |Science News 
His voice would morph from a melodic baritone to a deep, guttural snarl, grinding notes to a pulp. Future Islands Frontman Samuel T. Herring on Their 11-Year Journey to Letterman and Viral Stardom |Marlow Stern |April 3, 2014 |DAILY BEAST 
She even changed the way she spoke; as a little kid, she spoke like her parents, with guttural hets and ayins. What's 'Legitimate' Israeli Fiction? |Sigal Samuel |March 29, 2013 |DAILY BEAST 
She lets out a deep, guttural laugh, the kind that sends her into a body-shaking cough away from the phone. China Machado in 'About Face': A Fashion Legend Takes On Aging |Isabel Wilkinson |July 30, 2012 |DAILY BEAST 
Queen Mary is given a heavy guttural German accent; it actually was barely perceptible (as recordings of her voice prove). History Wrong in Weinstein's New Madonna Movie ‘W.E.’ |Andrew Roberts |February 6, 2012 |DAILY BEAST 
Sobs wracked my body, and I heard a guttural cry like a wild animal come from somewhere deep within me. Why I Almost Killed Myself—And My Children |Daleen Berry |April 16, 2011 |DAILY BEAST 
He went up a few steps, and looked over the upper deck; then he called out some guttural words. Bella Donna |Robert Hichens 
"Young man," he addresses me in the artificial guttural voice he affects on solemn occasions. Prison Memoirs of an Anarchist |Alexander Berkman 
“Let our white brother come close,” called a deep guttural voice. A Virginia Scout |Hugh Pendexter 
Ah Sing entered behind them, pointed at Peter Gross, and issued a harsh, guttural command. The Argus Pheasant |John Charles Beecham 
With an alphabet of thirty-nine letters, but it is harsh and guttural. Lavengro |George Borrow