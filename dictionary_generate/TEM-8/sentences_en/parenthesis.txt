In the script, when it says, “Enter Aunt Sandy,” in parentheses it says, “Think Jennifer Coolidge.” ‘Single’ sensation: An interview with actor Michael Urie |Gregg Shapiro |December 9, 2021 |Washington Blade 
Once you’ve filled out all of your parameters, close the parentheses and hit return to pull up your result. Three spreadsheet tips to make beginners feel like pros |Sandra Gutierrez G. |October 21, 2021 |Popular-Science 
Our roundup includes the inflation-adjusted price in parentheses, the year the deal took place, and a comment on whether the acquisition paid off. How the Slack/Salesforce deal stacks up to history’s other Big Tech acquisitions |Jeff |December 3, 2020 |Fortune 
In parentheses are the number of points you earn for each case. Can You Break A Very Expensive Centrifuge? |Zach Wissner-Gross |September 18, 2020 |FiveThirtyEight 
The attack on the World Trade Center's towers (and on the Pentagon, that breathtaking parenthesis) was a brilliant act of jujitsu. Osama bin Laden: Why He Won |E.J. Graff |May 15, 2011 |DAILY BEAST 
“When you have a food label and see quite a lot of parenthesis—first tip that your food may be highly fabricated,” she says. Foods That Fudge the Facts |Kate Dailey |January 26, 2011 |DAILY BEAST 
He drops the parenthesis about the great Variety of entertaining Incidents, and he diminishes these engaging Scenes to it. Samuel Richardson's Introduction to Pamela |Samuel Richardson 
What a tendency there is to round off a narrative into falsehood; or else by parenthesis to destroy its pith and continuity. Friends in Council |Arthur Helps 
Page 365: closing parenthesis added after "particular shape" Mystic London: |Charles Maurice Davies 
Page 388: closing parenthesis added after "assumption of omniscience" Mystic London: |Charles Maurice Davies 
Let me tell you in a parenthesis that he is going to the army to join the King. The Best of the World's Classics, Restricted to Prose, Vol. VII (of X)--Continental Europe I |Various