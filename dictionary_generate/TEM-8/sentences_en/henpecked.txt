One danger is that women will be henpecked by men who now claim that unprotected sex is medically safe. The New Unsafe Sex |Tracy Quan |July 28, 2009 |DAILY BEAST 
Martha paid as little attention to the old man's injunctions as a predominant dame gives to those of a henpecked husband. The Fortunes of Nigel |Sir Walter Scott 
He still believed that she was excessively fond of him—a common delusion of husbands, especially when henpecked. Night and Morning, Complete |Edward Bulwer-Lytton 
Socrates was henpecked, and was one of the greatest philosophers in the world—but the rule does not hold good any longer. The Foolish Almanak |Anonymous 
I saw one instance where a henpecked husband "ran amok" and killed or wounded seventeen people before he himself was killed. Little Journeys to the Homes of the Great - Volume 12 |Elbert Hubbard 
I am prepared to become an awful example of a henpecked husband. Mount Royal, Volume 1 of 3 |Mary Elizabeth Braddon