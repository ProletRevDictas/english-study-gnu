My son’s nonchalant response to my coming out was one of the greatest gifts I’ve ever received. Coming Out to My Kid Helped Me Come Out to Myself |Aubrey Hirsch |June 11, 2022 |Time 
Their physical presence speaks to our aesthetic biases and nonchalant insults. The sound of a shifting power structure |Robin Givhan |January 13, 2021 |Washington Post 
These nonchalant brutalities seem at first at odds with the genteel decorum that mostly cloaks late-19th century culture. The Magazine That Made—and Unmade—Politicians |Anthony Haden-Guest |November 2, 2014 |DAILY BEAST 
On video, Raymond Santana was smug, boastful, and nonchalant by turns, vividly reenacting who did what during the rape. The Myth of the Central Park Five |Edward Conlon |October 19, 2014 |DAILY BEAST 
I so wanted to seem brave and nonchalant, but my hands began to shake and my heart accelerated. How I Got Used to Gaza Rockets |Miranda Frum |July 9, 2014 |DAILY BEAST 
The Democrats need to be similarly nonchalant about this one. Despite Their Loss In Florida’s Special Election, Democrats Shouldn’t Panic Over November |Michael Tomasky |March 12, 2014 |DAILY BEAST 
An extreme close-up of Man Ray snapped in profile (1968) hangs opposite a nonchalant-looking Andy Warhol (1965). David Bailey’s ‘Stardust’ Shows a Keen Eye for Fine Faces |Chloë Ashby |February 8, 2014 |DAILY BEAST 
Palmer is out of town, and Lovegrove and Matthews appear wonderfully nonchalant. Private Letters of Edward Gibbon (1753-1794) Volume 1 (of 2) |Edward Gibbon 
Algernon's words could not be objected to, but the tone in which they were uttered was completely nonchalant. A Charming Fellow, Volume II (of 3) |Frances Eleanor Trollope 
And, indeed, his countenance brightened at once, and he took a chair opposite to Minnie with all his old nonchalant gaiety. A Charming Fellow, Volume II (of 3) |Frances Eleanor Trollope 
"It seems they've spotted us," said Arcot in a voice he tried to make nonchalant. Islands of Space |John W Campbell 
To this Bobby returned ostentatious yawns of boredom and nonchalant lollings, for it seemed a small matter to be so fashed about. Greyfriars Bobby |Eleanor Atkinson