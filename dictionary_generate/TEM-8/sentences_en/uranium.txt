Also, large amounts of uranium would have required the presence of lots of neutrons. New Type of Stellar Explosion Could Explain the Mystery of the Milky Way’s Elements |Gary Da Costa |July 8, 2021 |Singularity Hub 
Violent explosions of massive, magnetized stars may forge most of the universe’s heavy elements, such as silver and uranium. Souped-up supernovas may produce much of the universe’s heavy elements |Mara Johnson-Groh |July 7, 2021 |Science News 
After blasting uranium with neutrons, Fermi and colleagues reported evidence of success. How matter’s hidden complexity unleashed the power of nuclear physics |Emily Conover |April 8, 2021 |Science News 
Among the provocative moves, Iran began enriching uranium at levels beyond caps outlined in the accord. The US and Iran have their first real chance to revive the nuclear deal |Alex Ward |April 2, 2021 |Vox 
In the process, heavy elements such as uranium begin to crystalize, forming “snowflakes” in the stars’ cores. Uranium ‘snowflakes’ could set off thermonuclear explosions of dead stars |Emily Conover |March 30, 2021 |Science News 
Importantly, as part of the interim plan, Iran has diluted or converted its stockpile of 20 percent enriched uranium. Iran Nuke Deal: A Matter of War or Peace |Christopher Dickey |November 23, 2014 |DAILY BEAST 
And that conglomerate also owns nearly 70 percent of the Rossing uranium mine in Namibia. McCain Helps a Business Partner of Iran |Ben Jacobs |November 13, 2014 |DAILY BEAST 
Since then, all dividend payments have been frozen and Iran receives “no uranium or revenue from the mine.” McCain Helps a Business Partner of Iran |Ben Jacobs |November 13, 2014 |DAILY BEAST 
A drawing of what was deemed a “deer pig” was also sent through the uranium decay ringer. The Oldest Cave Art May Not Be in Europe |Justin Jones |October 9, 2014 |DAILY BEAST 
He referred to this risky state of affairs as “the babushka-with-uranium-in-the-chicken-shed” problem. Fired From Los Alamos for Pushing Obama's Nuclear Agenda |Center for Public Integrity |July 31, 2014 |DAILY BEAST 
It's not controllable enough and uranium isn't something we could carry by the ton. Islands of Space |John W Campbell 
But the osmium and uranium alloyed with it are something else. The Planet Strappers |Raymond Zinke Gallun 
Radium was discovered in 1898 by M. and Madame Curie and M. Bmont, while experimenting with the uranium mineral pitchblende. Invention |Bradley A. Fiske 
From the steel-like ground they scooped a dozen tons of the dirty black uranium ore and sent it hurtling back to the Bertha. Feline Red |Robert Sampson 
They flew about a huge stack of lead containers—uranium cells secured for the long trip home. Feline Red |Robert Sampson