They know they will face either a swift backlash or deafening silence. Phylicia Rashad and the Cult of Cosby Truthers |Stereo Williams |January 8, 2015 |DAILY BEAST 
Taylor Swift – 1989 A change has, it seems, done Taylor Swift good. The 10 Best Albums of 2014: Taylor Swift, Sia, Run the Jewels, and More |Marlow Stern |December 28, 2014 |DAILY BEAST 
You talk in the book about meeting some of your idols, including Taylor Swift. Portrait of the Austin Mahone as a Teen Idol |William O’Connor |December 10, 2014 |DAILY BEAST 
When they combine it with their pop music, like Taylor Swift, it's basically targeting children and that's wrong. I Got Kicked Out Of The Victoria’s Secret Fashion Show |Nico Hines |December 3, 2014 |DAILY BEAST 
Does it matter whether Taylor Swift wants me to inflate my Internet notoriety by doing a dumb thing where I lip sync to her music? Death of the Author by Viral Infection: In Defense of Taylor Swift, Digital Doomsayer |Arthur Chu |December 3, 2014 |DAILY BEAST 
And he was gone, and out of sight on the swift galloping Benito, before Father Gaspara bethought himself. Ramona |Helen Hunt Jackson 
Dean Swift was indeed a misanthrope by theory, however he may have made exception to private life. Gulliver's Travels |Jonathan Swift 
Sanson's Atlas: a very large atlas by a French geographer in use in Swift's time. Gulliver's Travels |Jonathan Swift 
The sound of the swift beating of horse-hoofs was heard from the south, and soon three men came riding up. The Courier of the Ozarks |Byron A. Dunn 
Long before reason found the answer, instinct—swift, merciless interpreter—told him plainly. The Wave |Algernon Blackwood