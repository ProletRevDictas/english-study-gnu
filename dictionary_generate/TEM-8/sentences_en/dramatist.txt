Such contemporary dramatists as Theresa Rebeck, Craig Wright and Caleen Sinnette Jennings tried out new work for her. Folger Theatre’s Janet Griffin announces her retirement after decades of devotion to Shakespeare |Peter Marks |January 27, 2021 |Washington Post 
The bias for me, as a dramatist—to go back to the bully question—is how did we become like this? Oliver Stone on the Tyranny of Obama’s ‘Exceptional’ America |Andrew Romano |October 17, 2013 |DAILY BEAST 
The playwright Jon Fosse could avoid the curse of Henrik Ibsen to become a Norwegian dramatist Nobel laureate. Nobel Literature Prize Favorites for Dummies, According to the Bookies |Jimmy So |October 9, 2013 |DAILY BEAST 
The first question a dramatist asks is not "Is this how it really works?" Memo: The Aaron Sorkin Model of Political Discourse Doesn't Actually Work |Megan McArdle |April 23, 2013 |DAILY BEAST 
“The opposition [between Left and Right] appealed to me as a dramatist,” he writes in The Secret Knowledge. David Mamet's Right Turn |Ben Crair |May 28, 2011 |DAILY BEAST 
On the political stage, Mamet is not a dramatist; he is merely acting. David Mamet's Right Turn |Ben Crair |May 28, 2011 |DAILY BEAST 
William Kenrick, an English dramatist and miscellaneous writer, died. The Every Day Book of History and Chronology |Joel Munsell 
In the literary and dramatic world Tchaikovsky had two good friends—the dramatist Ostrovsky and Sadovsky. The Life &amp; Letters of Peter Ilich Tchaikovsky |Modeste Tchaikovsky 
Richard Brinsley Sheridan, an English dramatist, wit and orator, died. The Every Day Book of History and Chronology |Joel Munsell 
Indeed, both the young gallant himself and all his satellites can safely be put down as creations of the actor-dramatist. The Fatal Dowry |Philip Massinger 
But every scientific man can tell you a little about nature, and every dramatist can tell you a little about dramatic truth. The Autobiography of a Play |Bronson Howard