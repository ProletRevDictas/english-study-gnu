A man struck in a hail of eight bullets Sunday afternoon had collapsed in the parking lot of a 7-Eleven in Southwest Washington. After D.C. firefighter shot and wounded on the job, department expresses frustration at violence in city |Peter Hermann |November 24, 2020 |Washington Post 
That day at the park, we packed our bags while dodging peanut-size hail in a parking lot near String Lake. Grand Teton Is a Hiker's Park |Emily Pennington |November 23, 2020 |Outside Online 
Each storm can bring a suite of problems, from hail to high winds, but it’s lightning that is your number one concern. How to Survive 5 Extreme Weather Scenarios |Graham Averill |October 15, 2020 |Outside Online 
Severe thunderstorms can bring lightning and large hail on some occasions. Columbus: A Rich History of LGBTQ Diversity |LGBTQ-Editor |August 30, 2020 |No Straight News 
It’s everything from getting a cab hail to wondering what’s going to happen when you get pulled over. Full Transcript: Sean Spicer on ‘The Carlos Watson Show’ |Daniel Malloy |August 26, 2020 |Ozy 
Why call a taxi when you can hail a Lyft to pick up visiting family and friends? One of a Kind Gifts Are Only a Neighbor Away |Lawrence Ferber |December 8, 2014 |DAILY BEAST 
Elsewhere on the Internet, and often, Turkers butt heads over which tools work best, or what nation they hail from. Amazon’s Turkers Kick Off the First Crowdsourced Labor Guild |Kevin Zawacki |December 3, 2014 |DAILY BEAST 
They unleashed a hail of bullets to rival the final scene in ‘Bonnie and Clyde.’ The Cleveland Cops Who Fired 137 Shots and Cried Victim |Michael Daly |December 2, 2014 |DAILY BEAST 
Victoria and Zoe Yin, who hail from Boston, were both deemed child prodigies at young ages. Blessed or Cursed? Child Prodigies Reveal All |Justin Jones |November 17, 2014 |DAILY BEAST 
As an election tactic, it was a Hail-Mary move: a half-hour address by an aging actor of no political standing or constituency. Remembering Reagan’s Defining Speech |Stuart Stevens |October 27, 2014 |DAILY BEAST 
But hail shall be in the descent of the forest, and the city shall be made very low. The Bible, Douay-Rheims Version |Various 
Then he broke off, and when he next gave hint of his whereabouts, it was to hail us from the nearest point on the canyon rim. Raw Gold |Bertrand W. Sinclair 
Thickets were swept as with a great jagged scythe by the leaden hail which swept through them. The Courier of the Ozarks |Byron A. Dunn 
He is the true soldier who knows how to die and stand his ground in the midst of a hail of bullets. Third class in Indian railways |Mahatma Gandhi 
Slipping the cable once more, the lifeboat gallantly dashed into the thickest of the fight, and soon got within hail of the wreck. The Floating Light of the Goodwin Sands |R.M. Ballantyne