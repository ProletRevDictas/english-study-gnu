I guess the reason it needs a lot of chocolate is the actual cookie is rather dreadful. Who makes the best chocolate chip cookie? We tasted 14 top brands and ranked them. |Matt Brooks |December 4, 2020 |Washington Post 
As a glasses-noob suffering from the dreadful fog myself, I turned to the internet for answers. The best ways to stop a mask from fogging up your glasses, ranked |Sandra Gutierrez G. |November 18, 2020 |Popular-Science 
She is a threat to its isolation, its purity, and its unknowable and dreadful secrets. Gothic novels are obsessed with borders. Mexican Gothic takes full advantage. |Constance Grady |October 16, 2020 |Vox 
If there were to be any sort of silver lining to dreadful circumstances, this was it. Hammerly isn’t ready to give up on theater |Patrick Folliard |August 20, 2020 |Washington Blade 
Toronto’s offense checks in just outside the top 10 for the full season, and it’s been dreadful during the restart. The Raptors’ Defense Is Almost Never The Same, But It’s Always Really Good |Jared Dubin |August 17, 2020 |FiveThirtyEight 
Is there a more dreadful sensation than that of your stomach wringing itself out like a washcloth? Why My Norovirus Panic Makes Me Sick |Lizzie Crocker |January 5, 2015 |DAILY BEAST 
He looked, that dreadful afternoon, as if he had just come from his barber, tailor and haberdasher. The Stacks: H.L. Mencken on the 1904 Baltimore Fire |H.L. Mencken |October 4, 2014 |DAILY BEAST 
In the novel, the moral situation Frances ends up in is dreadful. Sarah Waters: Queen of the Tortured Lesbian Romance |Tim Teeman |September 30, 2014 |DAILY BEAST 
Any of the three on its own would have been dreadful enough. American Statesmanship Is Depressingly MIA on Border Kids, MH17 & Gaza |Michael Tomasky |July 21, 2014 |DAILY BEAST 
There are some hopeful elements in an otherwise dreadful day for human rights. At the United Nations, It’s Human Rights, Putin-Style |Jay Michaelson |June 26, 2014 |DAILY BEAST 
The conflict in Tom's puzzled heart sharpened that evening into dreadful edges that cut him mercilessly whichever way he turned. The Wave |Algernon Blackwood 
He could not bear to open his dreadful situation to his Uncle David, nor to kill himself, nor to defy the vengeance of Longcluse. Checkmate |Joseph Sheridan Le Fanu 
At other times they have a dreadful look of being fibs invented for the purpose of covering a fault. Children's Ways |James Sully 
Nevertheless, this world of mankind to-day seems to me to be a very sinister and dreadful world. The Salvaging Of Civilisation |H. G. (Herbert George) Wells 
She had wakened up in the night, and perceived with dreadful clearness that trouble lay in front of her. Hilda Lessways |Arnold Bennett