The flares appear to be tiny cousins of the better known solar flares. Close-up of the sun reveals ‘campfires’ |Lisa Grossman |August 28, 2020 |Science News For Students 
Weganool, a vegan cousin of wool that is primed to take off as the next big thing in sustainable fashion in Europe and beyond. How a Wasteland Shrub Is Becoming the Next Big Thing in Fashion |Daniel Malloy |August 28, 2020 |Ozy 
Suppose Dax had a cousin, Max, who was 60% husky and 40% Pomeranian. How to Design (or at Least Model) Mixed Dog Breeds |Pradeep Mutalik |July 31, 2020 |Quanta Magazine 
Also like its basketball-playing cousins, hockey will have a bit of a convoluted format upon its return. How The NHL’s New Format Changed The Stanley Cup Race |Neil Paine (neil.paine@fivethirtyeight.com) |July 29, 2020 |FiveThirtyEight 
In addition to being an economist and teacher, Sally Sadoff also happens to be my cousin. America’s Math Curriculum Doesn’t Add Up (Ep. 391) |Steven D. Levitt |October 3, 2019 |Freakonomics 
Mating with a cousin or brother is safer than risking life and limb to mate with an outsider. Mongooses, Meerkats, and Ants, Oh My! Why Some Animals Keep Mating All in the Family |Helen Thompson |December 29, 2014 |DAILY BEAST 
“A mother has lost a son,” referring to his late cousin Akai. Protesters Demand Justice For Gurley As Gap Grows Between Cops and NYC |M.L. Nestel |December 28, 2014 |DAILY BEAST 
The savvy forger with Pappy empties to fill might get his hands on some Old Weller and present it as its more illustrious cousin. The Cult of Pappy van Winkle |Eric Felten |December 3, 2014 |DAILY BEAST 
A few worries, to be sure, but not that cousin of depression and anxiety, dread. Awaiting the Grand Jury, Dread in Ferguson and America |Gene Robinson |November 16, 2014 |DAILY BEAST 
The second-to-last time we met Zalwar Khan, he brought a man he introduced as his cousin. Heart of Darkness: Into Afghanistan’s Taliban Valley |Matt Trevithick, Daniel Seckman |November 15, 2014 |DAILY BEAST 
Cousin George's position is such a happy one, that conversation is to him a thing superfluous. Physiology of The Opera |John H. Swaby (AKA "Scrici") 
From the moment that he touches the magical little hand, cousin George is eclipsed. Physiology of The Opera |John H. Swaby (AKA "Scrici") 
As a matter of fact, it was a very distant cousin of hers who had died, a Mrs. Fanshawe Collingwood, who also lived in the town. The Boarded-Up House |Augusta Huiell Seaman 
The Professor thought very kindly of the dead cousin, whose money would provide for this great work. Uncanny Tales |Various 
Never had Tom seen his gay and careless cousin in such guise: he was restless, silent, intense and inarticulate. The Wave |Algernon Blackwood