It almost mirrors the Buddhist cycle of life, death, and rebirth. The Buddhist Business of Poaching Animals for Good Karma |Brendon Hong |December 28, 2014 |DAILY BEAST 
Buddhist "I feel like my body is borrowed, and this life is very temporary." The Beyoncé Manifesto: Quotes on Nihilism and Feminism |Amy Zimmerman |December 12, 2014 |DAILY BEAST 
Then, he decided to give it all up and become a Buddhist monk. From Fashion Player to Photographer Monk |Nina Strochlic |December 3, 2014 |DAILY BEAST 
Buddhist and Hindu literature is rich with stories of disciples finally learning to surrender in this way. Is India’s Fallen ‘God-Man’ So Different From a Megachurch Pastor? |Jay Michaelson |November 21, 2014 |DAILY BEAST 
And while guru literally means “teacher,” in Hindu and Buddhist contexts, it often means much more. Is India’s Fallen ‘God-Man’ So Different From a Megachurch Pastor? |Jay Michaelson |November 21, 2014 |DAILY BEAST 
The columns in the courts are of Buddhist origin; the bell with long chain is sculptured on them in relief. A Woman's Journey Round the World |Ida Pfeiffer 
Pleasure they regarded as an evil, having a tendency to enchain man to earthly enjoyments, a peculiarly Buddhist tenet. Ancient Faiths And Modern |Thomas Inman 
The following exposition of modern Buddhist belief well deserves attention. Ancient Faiths And Modern |Thomas Inman 
As this example shows well the Buddhist veneration for memorials of the dead, I will not quote more. Ancient Faiths And Modern |Thomas Inman 
To the thoughtful reader of our christian history, this note upon Buddhist processions of images is painfully pregnant. Ancient Faiths And Modern |Thomas Inman