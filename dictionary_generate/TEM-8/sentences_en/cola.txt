Coca-Cola was a wildly popular drink and hangover remedy because, well, it contained cocaine. History's Craziest Hangover Cures |Justin Jones |December 30, 2014 |DAILY BEAST 
And, with Coca-Cola announcing the launch of a new milk product, the beverage could be back in our hands before we know it. History's Craziest Hangover Cures |Justin Jones |December 30, 2014 |DAILY BEAST 
Take James Carville, who, swigging Coc' Cola and playing the mad Cajun, spurred buttermilk-biscuit glamour to new heights. Let Us Now Praise Famous Rednecks and Their Unjustly Unsung Kin |Allison Glock |August 23, 2014 |DAILY BEAST 
Right-wing conservatives were in a tizzy over Coca Cola's new ad. Rage Against the Coke Machine |Jamelle Bouie |February 3, 2014 |DAILY BEAST 
The Coca Cola Company—as is its wont—had one of the best ads to air on Super Bowl Sunday. Rage Against the Coke Machine |Jamelle Bouie |February 3, 2014 |DAILY BEAST 
The room was full of students eating ice cream and drinking coco-cola and ice cream sodas. Tramping on Life |Harry Kemp 
Matteo was the name of this worthy pair, and the old man was called Cola and his wife was known as Sapatella. Edmund Dulacs Fairy-Book |Edmund Dulac 
It was to them that Cola di Rienzo looked for assistance and support. Rome |Mildred Anna Rosalie Tuker 
The saturnian line falls into two cola of which the first (a) contains three, the second (b) two accented syllables. The Oxford Book of Latin Verse |Various 
She turned back and saw the half-empty Pepsi-Cola bottle on the floor beside the bed table. Deadly City |Paul W. Fairman