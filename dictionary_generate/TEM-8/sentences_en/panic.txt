If the panic was back with fresh branding as QAnon, it had a new ally in Facebook. Evangelicals are looking for answers online. They’re finding QAnon instead. |Abby Ohlheiser |August 26, 2020 |MIT Technology Review 
I was having panic attacks, heart palpitations, and had developed a major sleeping issue, which ultimately led to me being prescribed Xanax. OneRepublic’s Ryan Tedder on launching a hemp-infused sparkling water brand |Rachel King |August 24, 2020 |Fortune 
His parents panicked, fearing he had been kidnapped, or worse. An Unlikely Esports Star Emerges From Pakistan |Daniel Malloy |August 21, 2020 |Ozy 
I would advise not to stop and not to panic, the situation will somehow be solved and the brand will either resist or not. Marketing strategies during COVID-19 times |Evelina Brown |July 13, 2020 |Search Engine Watch 
As the platform entertains millions of users, the spread of misinformation was creating panic among the users and misleading them regarding the seriousness of the Pandemic. How Twitter is contributing to support masses during the Coronavirus outbreak |Harry Liam |May 22, 2020 |Search Engine Watch 
Just two young kids experiencing the panic, pain, and then the miracle, of new birth. Jesus Wasn’t Born Rich. Think About It. |Gene Robinson |December 25, 2014 |DAILY BEAST 
Hence, I suspect, the panic, the lockdown, the capitulation. Pyongyang Shuffle: Hollywood In Dead Panic Over Sony Hack |James Poulos |December 19, 2014 |DAILY BEAST 
Someone was sure to capitalize on the Ebola panic, and Dr. Joseph Alton is that guy. The Sham, Scaremongering Guide to Ebola |Abby Haglage |November 20, 2014 |DAILY BEAST 
In one sentence, he asserts: “Panic is worse than complacency.” The Sham, Scaremongering Guide to Ebola |Abby Haglage |November 20, 2014 |DAILY BEAST 
Panic—and the inevitable panicking about the panic—is counterproductive. Fighting Ebola and Starvation in Sierra Leone |Abby Haglage |November 5, 2014 |DAILY BEAST 
We stood staring after the fugitives in perfect bewilderment, totally unable to explain their apparently causeless panic. Blackwood's Edinburgh Magazine, No. CCCXXXIX. January, 1844. Vol. LV. |Various 
Two artillery subalterns who had fought their way through a mob stricken with panic for the moment, soon arrived. The Red Year |Louis Tracy 
He was naturally frightfully upset about it, and a regular panic sprang up in the neighbourhood. Uncanny Tales |Various 
She should not show panic because of the mysterious noise in the loft of the abandoned Carter house. The Campfire Girls of Roselawn |Margaret Penrose 
General Wheatonʼs brigade captured Malinta, and the insurgents fled panic-stricken after having suffered severely. The Philippine Islands |John Foreman