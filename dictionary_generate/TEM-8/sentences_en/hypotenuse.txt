That’s because 65 happened to be the length of the hypotenuse for three distinct Pythagorean triples with legs that formed an arithmetic sequence. Can You Reach The Target Number? |Zach Wissner-Gross |April 1, 2022 |FiveThirtyEight 
One week later he decided that he should have cupped his hands and shouted, "You hypotenuse hussy!" The Boy Grew Older |Heywood Broun 
Mr. Isadore Binswanger inserted a toothpick between his lips and stretched his limbs out at a hypotenuse from the chair. Just Around the Corner |Fannie Hurst 
Thus we demonstrate that the surface of a regular polygon may be found by multiplying the perimeter by half the hypotenuse. Montessori Elementary Materials |Maria Montessori 
First take out the two rectangles formed on the hypotenuse; place them in the two lateral grooves, and lower the triangle. Montessori Elementary Materials |Maria Montessori 
Presently he turned again, to return cross-lots along the hypotenuse of the triangle. Sudden Jim |Clarence Budington Kelland