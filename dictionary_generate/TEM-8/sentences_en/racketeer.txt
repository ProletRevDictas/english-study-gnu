RICO is the acronym for the Racketeer-influenced and Corrupt Organizations Act. The FBI Declares War on L.A.’s Crips |Andrew Romano |June 18, 2014 |DAILY BEAST 
Mr. Duffy, it happened, was Big Bill Duffy, a jolly henchman of Owney Madden, the racketeer. Portrait of the Consummate Con Man |John Lardner |May 17, 2014 |DAILY BEAST 
He could, however, be a racketeer specializing in bookmaking, gambling, drug dealing, and assorted other crime. Whitey Bulger Trial Kicks Off in Boston |T.J. English |June 13, 2013 |DAILY BEAST 
He was a raider, a racketeer, not a leader of purposeful men. Black Man's Burden |Dallas McCord Reynolds 
Her racketeer sweetie has a long arm, however, and Nita gets hers. Murder at Bridge |Anne Austin