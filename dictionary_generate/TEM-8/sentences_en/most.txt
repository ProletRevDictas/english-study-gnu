Turkey has had more than a decade of economic boom, and is now the sixth-most-visited tourist destination in the world. The Women Battling an Islamist Strongman |Christina Asquith |December 22, 2014 |DAILY BEAST 
Since then, Abilify has risen from the fifth-most-prescribed drug to the top of the heap. Mother’s Little Anti-Psychotic Is Worth $6.9 Billion A Year |Jay Michaelson |November 9, 2014 |DAILY BEAST 
“Daughter” was the second-most Googled search term for the Louisiana race, the Washington Examiner reported. Mary Landrieu-Bill Cassidy Louisiana Senate Race Heads to a Runoff |Tim Mak |November 5, 2014 |DAILY BEAST 
The most-intact section of this image is the dark, bowl-shaped object. Clues From SpaceShipTwo’s Wreckage: Did the Crew Compartment Fail? |Clive Irving |November 2, 2014 |DAILY BEAST 
The most recent numbers place it as the seventh-most unequal among 35 OECD states. Scotland’s ‘Yes’ Campaign and the Myth of Scottish Equality |Noah Caldwell |September 18, 2014 |DAILY BEAST 
He had been down into the bottom-most pit of hell, and the sights that he had seen there had withered him up. Love's Pilgrimage |Upton Sinclair 
The top-most bud waits only through the twelve hours of a single day to open. The College, the Market, and the Court |Caroline H. Dall 
Her father had no son living, therefore she was an only child, and the most-sought-after of any maiden in that band. Red Hunters And the Animal People |Charles A. Eastman 
The noblest and most-varied scenery in the north-west Himalaya is in the catchment area of the Jhelam. The Panjab, North-West Frontier Province, and Kashmir |Sir James McCrone Douie 
The birds were filling the top-most branches, a gathering of the clans, evidently, for the day's start. Roof and Meadow |Dallas Lore Sharp