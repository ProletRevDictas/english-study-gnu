His loud, often profane way had been exchanged for a more docile demeanor. The Many Lives of H. Rap Brown |Rembert Browne |November 1, 2021 |Time 
The harder she looks, the more she sees beneath the “docile surface” of the streets. Barack Obama’s summer reading pick ‘Intimacies’ is an unsettling novel about moral dilemmas |Ron Charles |July 13, 2021 |Washington Post 
Sometimes large dogs need a heavy-duty crate even though their personality is docile. Best dog crate: These indestructible pet products for the home keep your pup cozy and safe |Irena Collaku |July 13, 2021 |Popular-Science 
She is the docile Earth-mother who generously balances and protects the environment and all its inhabitants. Gaia, the Scientist - Issue 99: Universality |Hope Jahren |April 7, 2021 |Nautilus 
They note that the bowheads, which had initially been docile, started using the sea ice to avoid harpoons. Sperm whales have a surprisingly deep—and useful—culture |Ellie Shechet |March 19, 2021 |Popular-Science 
She also features a more natural face than the one of docile serenity so often bestowed on the Queen of Heaven. The Virgin Mary Lookbook |William O’Connor |December 7, 2014 |DAILY BEAST 
For the first few years, the public was fairly docile in response to the school wars. Hunger Games Comes to New York State’s Public Schools |Zephyr Teachout |November 26, 2014 |DAILY BEAST 
The middle classes,” Satyarthi once told the BBC, want “cheap, docile labour. Kailash Satyarthi, Malala's Nobel Peace Prize Co-Winner, Is Fighting India's Child Slavery Epidemic |Dilip D’Souza |October 11, 2014 |DAILY BEAST 
The tabloids demand that Kate Middleton be as docile as Jane Seymour, whose personal motto was “Bound to obey and serve.” Why Does Anne Boleyn Obsess Us? |Lauren Elkin |April 25, 2013 |DAILY BEAST 
Spivack, meanwhile, continues to promise her American clients docile, submissive partners. The House’s Immigrant Betrayal With New Violence Against Women Act |Michelle Goldberg |May 17, 2012 |DAILY BEAST 
It is the young animals of these species which are the most social and docile and most approach man in appearance. Man And His Ancestor |Charles Morris 
It consisted in subjecting some of the docile herbivora more fully to human mastership. Man And His Ancestor |Charles Morris 
She tried to turn a docile face toward old Kano; but the deepening glory of her husband's look drew her as light draws a flower. The Dragon Painter |Mary McNeil Fenollosa 
Instead of creating men, a perfect God ought to have created only docile and submissive angels. Superstition In All Ages (1732) |Jean Meslier 
We have always been taught to think a nation sound and safe whose women were docile and domestic. The Daughters of Danaus |Mona Caird