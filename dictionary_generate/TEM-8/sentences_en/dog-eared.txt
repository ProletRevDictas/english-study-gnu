Indeed, although he works here in the old town, he lives in the new part of the city where he walks his dog in the morning. The Photographer Who Gave Up Manhattan for Marrakech |Liza Foreman |January 6, 2015 |DAILY BEAST 
Up till then I was just a dog-assed heavy, one of the posse. The Story Behind Lee Marvin’s Liberty Valance Smile |Robert Ward |January 3, 2015 |DAILY BEAST 
Hangover Rx: “The old ‘hair of the dog’ is pretty much just a myth,” says White. 5 Hangover Cures to Save You After a Few Too Many |DailyBurn |December 19, 2014 |DAILY BEAST 
His latest book is a short story collection, Even a Street Dog: Las Vegas Stories. Will Hyman Roth Return to Havana With Normalized Relations? |John L. Smith |December 18, 2014 |DAILY BEAST 
And just last May Glee aired “Old Dog, New Trick,” the first episode scripted by Colfer. Chris Colfer on Writing, Acting, and the Pain of Being A Pop Culture Trailblazer |Oliver Jones |December 15, 2014 |DAILY BEAST 
A little boy of four was moved to passionate grief at the sight of a dead dog taken from a pond. Children's Ways |James Sully 
A was an Archer, who shot at a frog; B was a Butcher, and had a great dog. Boys and Girls Bookshelf; a Practical Plan of Character Building, Volume I (of 17) |Various 
The dog stood with hanging head and tail, as if ashamed he had let so many of his enemies get away unharmed. The Courier of the Ozarks |Byron A. Dunn 
These words were uttered in a guarded whisper by a boy about seventeen years of age, to a great dog that stood by his side. The Courier of the Ozarks |Byron A. Dunn 
At the word of command, the dog crouched down, his whole body quivering with excitement. The Courier of the Ozarks |Byron A. Dunn