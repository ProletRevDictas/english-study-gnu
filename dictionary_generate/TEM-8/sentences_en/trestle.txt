There was his portmanteau on its trestle, exactly where he had seen the porter put it when he first arrived. Masterpieces of Mystery, Vol. 1 (of 4) |Various 
The two boys gazed respectfully at the bare trestle table and the raised reading-desk and the picture of St. Benedict. Sinister Street, vol. 1 |Compton Mackenzie 
Both boys narrowly missed being run down by an ore train as it was shunted out on the trestle. The Iron Boys on the Ore Boats |James R. Mears 
Ore was being shot down through the chutes into boats on each side of the great trestle. The Iron Boys on the Ore Boats |James R. Mears 
The young man stood now in the car vestibule, as the train roared over the trestle and slowed down at the station. The Code of the Mountains |Charles Neville Buck