“Discord” proceeds to envelop us in this exhaustive, mind-racking debate. The Gospel According to Thomas Jefferson (And Tolstoy and Dickens) |Samuel Fragoso |October 26, 2014 |DAILY BEAST 
The shadows may have sought to envelop Paris altogether early Wednesday. Paris Jackson, the Girl We Met at Michael’s Funeral, in Suicide Scare |Michael Daly |June 5, 2013 |DAILY BEAST 
Spontaneity should, like a sort of fog, already envelop those pages that you will write the next day. Ismail Kadare: How I Write |Noah Charney |January 31, 2013 |DAILY BEAST 
He is one of those who, when they meet Mandela, envelop him in a hug as they walk to steady the old man. Nelson Mandela Recovering in South Africa After Brief Hospital Scare |Charlene Smith |February 27, 2012 |DAILY BEAST 
Her flowing dusty-rose gown seemed to envelop her—like a chiffon pup tent held up with silver sequins. Rooney Mara, Michelle Williams, Kristen Wiig: 2012 Oscars’ Best, Worst, and Wilted |Robin Givhan |February 27, 2012 |DAILY BEAST 
The long axis of the hip-roof crystal is often so shortened that it resembles the envelop crystal of calcium oxalate. A Manual of Clinical Diagnosis |James Campbell Todd 
The shell is thick, and is surrounded by an uneven gelatinous envelop which is often stained with bile. A Manual of Clinical Diagnosis |James Campbell Todd 
I long for the precious embrace to surround me, to envelop me, to pour its soft balm into my aching soul. Prison Memoirs of an Anarchist |Alexander Berkman 
They were great rolling clouds that seemed to envelop the entire universe with their vibrance. The Holes and John Smith |Edward W. Ludwig 
The night wears on: darkness and fog envelop Paris more and more. The Bronze Eagle |Emmuska Orczy, Baroness Orczy