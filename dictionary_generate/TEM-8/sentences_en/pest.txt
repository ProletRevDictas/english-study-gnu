Then 4–1, the commissioners voted to go forward with a test of genetically modified males as pest control devices. Genetically modified mosquitoes have been OK’d for a first U.S. test flight |Susan Milius |August 22, 2020 |Science News 
He wondered if it was the consequence of decades of the dingoes’ status as a livestock pest. Culling dingoes with poison may be making them bigger |Jake Buehler |August 19, 2020 |Science News 
They also sent surveys out to pest control companies around the world. The ‘ratpocalypse’ isn’t nigh, according to service call data |Bethany Brookshire |July 14, 2020 |Science News 
When food disappears, or pest control comes, rat populations fall. Why you’re spotting more wildlife during COVID-19 |Bethany Brookshire |June 8, 2020 |Science News For Students 
That can keep pests in check and limit damage, without unintended harm to pollinators. Pesticides can have long-term impact on bumblebee learning |Alison Pearce Stevens |May 18, 2020 |Science News For Students 
Bassett sounded off on the importance of eradicating problem pest areas. Crowdsourcing NYC’s War on Rats |Kevin Zawacki |June 24, 2014 |DAILY BEAST 
Scotts Miracle Gro made a pest resistant Kentucky bluegrass in 2011 using the same technology. Plants That Glow in the Dark Spark Heated Debate |Josh Dzieza |August 18, 2013 |DAILY BEAST 
"Earworm" has been around for quite some time to refer to a pest that burrows into the ears of corn. New Words Added to Merriam-Webster Dictionary: ‘Man Cave,’ ‘Sexting,’ and More |Kory L. Stamper |August 14, 2012 |DAILY BEAST 
With one swift ninja movement, Obama swatted the pest with ease, leaving it lifeless on the ground. Obama’s 11 Most Badass Moments (Videos) |Brittany Jones-Cooper |April 25, 2012 |DAILY BEAST 
And, for a year or so after they first met, he was an incorrigible pest. Gloria Cain: Sex Accusations Make No Sense in 'Old School' Marriage |Michelle Cottle |November 15, 2011 |DAILY BEAST 
What has happened between you and the communiers, whom may the pest carry off and hell confound! The Pilgrim's Shell or Fergan the Quarryman |Eugne Sue 
It is an animal not less mischievous than it is deformed; it is the pest of man, and the torment of other animals. Buffon's Natural History. Volume VII (of 10) |Georges Louis Leclerc de Buffon 
All weapons seemed lawful against such a mere pest as this—a dog in the manger. It Is Never Too Late to Mend |Charles Reade 
They do not live in fixed abodes, but shift about in search of game or plunder, and are deemed a pest by the Santa Fe traders. The Indian in his Wigwam |Henry R. Schoolcraft 
That Arabic word, picked up at hazard from the dragoman, has acted like a talisman—the pest has actually gone! Round the Wonderful World |G. E. Mitton