He describes an agency stuck in the mainframe era that needed to come up to speed quickly to fight the threat. Democracy depends on Washington improving its tech |Adam Lashinsky |September 17, 2020 |Fortune 
NBCUniversal is looking to update traditional TV measurement and planning for an era in which advertisers want to know how many sales they received versus how many people they reached. NBCUniversal tests new measurement program to prove it can push product sales for advertisers |Tim Peterson |September 17, 2020 |Digiday 
On top of all this, the company is benefiting from consumers’ growing aversion to cash in the pandemic era. ‘Square is a beast’ |Jeff |September 16, 2020 |Fortune 
Those of us who grew up in that era are now in our forties and fifties. While We’re Rethinking Policing, It’s Time to End Gang Injunctions |Jamie Wilson |September 15, 2020 |Voice of San Diego 
Speakers during the hearing floated several ideas about helping workers in the machine learning era. When it comes to A.I., worry about ‘job churn’ instead of ‘job loss’ |jonathanvanian2015 |September 15, 2020 |Fortune 
Even in the medieval era this disparity made Christians uncomfortable. Oops! Jesus’ Last Steps Are in the Wrong Place |Candida Moss |January 6, 2015 |DAILY BEAST 
The number of dissenters though is unprecedented in the modern era. Democrats Accidentally Save Boehner From Republican Coup |Ben Jacobs, Jackie Kucinich |January 6, 2015 |DAILY BEAST 
Community policing is expensive and, in an era of budget cuts, increasingly rare. How to Solve the Policing Crisis |Keli Goff |January 5, 2015 |DAILY BEAST 
There were stories of distant strife, in Bosnia, Rwanda, and Northern Ireland, and those stories had the whiff of a different era. The Best Columns of 2014 |John Avlon, Errol Louis |December 31, 2014 |DAILY BEAST 
One of the most famous directors of this era was Shin Sang-ok (신상옥). Propaganda, Protest, and Poisonous Vipers: The Cinema War in Korea |Rich Goldstein |December 30, 2014 |DAILY BEAST 
Science teaches that man existed during the glacial epoch, which was at least fifty thousand years before the Christian era. God and my Neighbour |Robert Blatchford 
In the preceding chapter an examination has been made of the purely mechanical side of the era of machine production. The Unsolved Riddle of Social Justice |Stephen Leacock 
They embody in themselves the uppermost thought of the era that was dawning when they were written. The Unsolved Riddle of Social Justice |Stephen Leacock 
Here was a bit of a civilization of a building era, that was almost old, everything being relative. Ancestors |Gertrude Atherton 
How often she had remembered that day as an era; the beginning of the best things in her uneventful life! Tessa Wadsworth's Discipline |Jennie M. Drinkwater