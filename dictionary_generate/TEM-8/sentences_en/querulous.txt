The querulous, interconnected pamphlets printed in seventeenth-century Europe prefigure the culture of modern blogging. Social Media is So Old Even the Romans Had It |Nick Romeo |October 25, 2013 |DAILY BEAST 
Calasso reconstruction is, in Mounts' judgment, a superbly ambitious, quirky, querulous, lyrical, and finally persuasive essay. The Best of Brit Lit |Peter Stothard |April 7, 2010 |DAILY BEAST 
When the feeble, querulous mother died, Romarino was fifteen years of age. Skipper Worse |Alexander Lange Kielland 
But, though Portland was an unreasonable and querulous friend, he was a most faithful and zealous minister. The History of England from the Accession of James II. |Thomas Babington Macaulay 
This consists in extending the suffrage among the querulous and suffering part of the people. Blackwood's Edinburgh Magazine, Volume 67, Number 414, April, 1850 |Various 
The human sweetness in him was half dried up, and a misanthropy, so new and alien to him, made him querulous and captious. The Life of Mazzini |Bolton King 
Mrs Beazeley, the housekeeper, has become inert and querulous from rheumatism and the burden of added years. Newton Forster |Captain Frederick Marryat