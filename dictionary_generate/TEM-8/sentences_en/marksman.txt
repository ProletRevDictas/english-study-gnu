She loved to build model planes and ships, became an “expert marksman” at 14 and skied competitively for the United States in slalom and downhill races. Wally Funk was supposed to go to space 60 years ago. Now she’s going with Jeff Bezos. |Taylor Telford |July 1, 2021 |Washington Post 
Joseph Lobdell was born Lucy Ann Lobdell, a master trapper, hunter and self-taught marksman who left home in men’s clothes to escape a troubled marriage in his 20s. The Gender Revolution |Daniel Malloy |March 21, 2021 |Ozy 
The marksman, a lance corporal in the Coldstream Guards, was 930 yards from his target when he squeezed the trigger. The Greatest Sniper Shot in History: Six Taliban Killed With One Bullet |Nico Hines |April 1, 2014 |DAILY BEAST 
At least one former detective believes that the killer was an expert marksman, according to ABC News. Suspect's Suicide Deepens Chasen Mystery |Kate Aurthur, Claire Martin |December 2, 2010 |DAILY BEAST 
See, in the war on race I am a foot soldier, not a marksman. Duh! Of Course Fashion's Racist |Elizabeth Gates |October 14, 2009 |DAILY BEAST 
Boone was such a good marksman that he soon found he could kill his game with half a bullet and less powder. The Story of the Thirteen Colonies |H. A. (Hlne Adeline) Guerber 
I may say that a Boer even early in life is a good horseman and marksman. My Reminiscences of the Anglo-Boer War |Ben Viljoen. 
With such a marksman he would not be maimed, but killed outright. From Sand Hill to Pine |Bret Harte 
Sergeant Fugler, the best marksman in the Company, was a hard drinker, with a hobnailed liver. Wandering Heath |Sir Arthur Thomas Quiller-Couch 
It was a tantalizing sight to Hendrik, who would have liked much to have shown his marksman skill by “creasing” one. Popular Adventure Tales |Mayne Reid