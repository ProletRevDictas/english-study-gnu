Netanyahu, and even some ingenuous pundits, are bragging about how Israel and America have never been closer. Obama's Biggest Opportunity Yet |Bernard Avishai |November 23, 2012 |DAILY BEAST 
During the ingenuous apologia pro vita sua Miss Anne regarded him with her honest candour. The Joyous Adventures of Aristide Pujol |William J. Locke 
This was the most perfect specimen of the bluff, hearty, breezy, almost ingenuous Westerner that Gwynne had encountered. Ancestors |Gertrude Atherton 
"I know you wouldn't wish me to affect an interest I do not feel," said Haggard with an ingenuous smile. The Pit Town Coronet, Volume III (of 3) |Charles James Wills 
His replies were perfectly ingenuous, evincing nothing of the natural taciturnity and shyness of the Indian mind. The Indian in his Wigwam |Henry R. Schoolcraft 
Her piety was not free from puerile pleasures; for everything, even religion, was poetry to her ingenuous heart. Honorine |Honore de Balzac