In large amounts, these chemicals can cause botulism, an illness that may produce nausea, vomiting, and in severe cases, fatal paralysis of vital muscles. Can you get too much Botox? |By Matthew J. Lin/The Conversation |October 1, 2020 |Popular-Science 
Some good signs that you need one would be if you have serious fatigue, nausea, or headaches after working out. Is Gatorade actually better than water? |Sara Chodosh |September 29, 2020 |Popular-Science 
They found active gut infection in seven patients, some of whom had no nausea, diarrhea or other digestive symptoms. COVID-19 infection lingers in the gut, even after it clears the respiratory system, researchers say |Claire Zillman, reporter |September 8, 2020 |Fortune 
A trained acupuncture practitioner will target specific points to help relieve issues like back pain, headaches, and nausea. My Chronic Pain Was No Match for This Mat |Aleta Burchyski |September 4, 2020 |Outside Online 
Barrio Logan residents, just adjacent to the port, reported nausea and headaches, according to the San Diego Union-Tribune. Answers on Navy Fire’s Health Impacts Won’t Come Right Away |MacKenzie Elmer |July 14, 2020 |Voice of San Diego 
Just pay attention to potential side effects such as dizziness or nausea. Does Fasted Cardio Really Burn More Fat? |DailyBurn |August 22, 2014 |DAILY BEAST 
Yes, gridlock frustration and national debt nausea are understandable. Flushing Money Down the Tea Party Toilet |Tom Dougherty |August 19, 2014 |DAILY BEAST 
The anti-nausea medicine, added to the I.V. a little bit later, has clearly worked. The I.V. Doc Comes to Your House, Fights Hangovers, and Wins |Abby Haglage |July 20, 2014 |DAILY BEAST 
A 1975 article in the New England Journal of Medicine concluded that THC is an effective anti-nausea therapy. The Chronic Chronicles: A History of Pot |Roger Roffman |July 6, 2014 |DAILY BEAST 
If I was intensely sick, they might feel generous enough to call the medical team to give me a shot to stop the nausea. I Detoxed from Heroin in Jail |Tracey Mitchell |June 28, 2014 |DAILY BEAST 
After his interview with Eloise, the Colonel had complained of nausea and faintness, and had gone early to bed. The Cromptons |Mary J. Holmes 
He buckled dizzily with weakness and nausea, but then an invisible force jolted him upright and motionless. Restricted Tool |Malcolm B. Morehart 
The only unpleasant effect that I have noted is nausea after large doses, sixty drops or more, and this in very few patients. The Treatment of Hay Fever |George Frederick Laidlaw 
But she trod down the momentary nausea with the resolute will that had always been hers. The Highgrader |William MacLeod Raine 
Slight nausea and pain in the back, headache and a soupçon of chill. Four Years in Rebel Capitals |T. C. DeLeon