A siren’s blare woke me, so loud it sounded like it was inside the room. Near the end of life, my hospice patient had a ghostly visitor who altered his view of the world |Scott Janssen |January 2, 2021 |Washington Post 
I crept to the door: the organ broke out overhead with a blare. Read ‘The King in Yellow,’ the ‘True Detective’ Reference That’s the Key to the Show |Robert W. Chambers |February 20, 2014 |DAILY BEAST 
Yon deep bell tolls no matin—'tis the tocsin's hurried blare! Blackwood's Edinburgh Magazine, No. CCCXXXIX. January, 1844. Vol. LV. |Various 
Suddenly we heard the blare of trumpets; the slow walk burst into a gallop, and then—well, it was wonderful to see! A Connecticut Yankee in King Arthur's Court, Complete |Mark Twain (Samuel Clemens) 
Deep red—that was the colour of the Romantic school; the flourishing of trumpets and the blare of brass its note. The History of Modern Painting, Volume 1 (of 4) |Richard Muther 
A night of Nature's making when she is tired of noise and blare of color. Wayside Courtships |Hamlin Garland 
The blare of the thousand trumpets, the acclamations of a vast multitude proclaimed the thing done! A Heroine of France |Evelyn Everett-Green