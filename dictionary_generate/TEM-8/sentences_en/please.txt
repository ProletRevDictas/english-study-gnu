“Please, please do not permit this to happen here in Florida,” wrote Cris K. Smith of East Polk County. Jeb Bush’s Unseen Anti-Gay Marriage Emails |Jackie Kucinich |January 9, 2015 |DAILY BEAST 
“Please,” he laughed, handing me the map after he was finished sketching. The Life and Hard Times Of The Family A Cuban Defector Left Behind |Brin-Jonathan Butler |December 19, 2014 |DAILY BEAST 
Please, Your Excellencies, consider my case with justice and intervene on my behalf. An American Marine in Iran’s Prisons Goes on Hunger Strike |IranWire |December 18, 2014 |DAILY BEAST 
Please know she has a very special place in our collective hearts. The Mystery Death Of A Female Firefighter |Christopher Moraff |December 13, 2014 |DAILY BEAST 
I would nod, and we'd tell the St. Regis 'One more night, please.' Alfred Hitchcock’s Fade to Black: The Great Director’s Final Days |David Freeman |December 13, 2014 |DAILY BEAST 
All please noteHis wondrous height and girth; He has the longest legs and throatOf anything on earth. Boys and Girls Bookshelf; a Practical Plan of Character Building, Volume I (of 17) |Various 
Add to this, if you please, the great difficulty of obtaining from them even the words that they have. The Jesuit Relations and Allied Documents, Vol. II: Acadia, 1612-1614 |Various 
I desired the captain would please to accept this ring in return of his civilities, which he absolutely refused. Gulliver's Travels |Jonathan Swift 
Hain't I kep' in doors uv a nite, an quit chawn tobacker and smokin' segars just to please her? The Book of Anecdotes and Budget of Fun; |Various 
She would not dare to choose, and begged that Mademoiselle Reisz would please herself in her selections. The Awakening and Selected Short Stories |Kate Chopin