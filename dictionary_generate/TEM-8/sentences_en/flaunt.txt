“We live in a society where there are girls who are in love with theirself and flaunt it but the majority are insecure,” says one. Cara Delevingne Posts Protest Picture Of Her Boob (With Taped-Out Nipple) |Tom Sykes |July 14, 2014 |DAILY BEAST 
As the Middle East sinks further into chaos, the Americans and Israelis flaunt a luxury that neither can afford. Israel Needs Better War Technology |Lloyd Green |July 7, 2014 |DAILY BEAST 
Most patriotic movies flaunt their pride by having America trounce foreign countries. 13 Most Patriotic Movies Ever: ‘Act of Valor,’ ‘Top Gun’ & More (VIDEO) |Melissa Leon |July 4, 2014 |DAILY BEAST 
Do we get to see Jon Snow flaunt his skills with the ladies in Season 4? Kit Harington: Game of Thrones’ Jon Snow Learns to Fight Dirty |Marlow Stern |April 9, 2014 |DAILY BEAST 
Why so many people are so eager to flaunt their musical bona fides by loathing Coldplay. Why Is It Cool to Hate Coldplay? A First Listen of New Album ‘Ghost Stories’ |Andrew Romano |March 26, 2014 |DAILY BEAST 
No less mock will we make of them, however, and we will flaunt in their faces that we have no fear of them. The Sagas of Olaf Tryggvason and of Harald The Tyrant (Harald Haardraade) |Snorri Sturluson 
It belonged to his dream of success—when a thousand banners should flaunt in the gardens of the Tuileries. When Valmond Came to Pontiac, Complete |Gilbert Parker 
But fr'm now on ye can flaunt ye'er teeth in th' face iv anny inspictor. Mr. Dooley Says |Finley Dunne 
Beloved, have I not tended you that you should thus flaunt me?' A German Pompadour |Marie Hay 
She bloomed with alternatives—she resembled some dull garden-path which under a copious downpour has begun to flaunt with colour. The Marriages |Henry James