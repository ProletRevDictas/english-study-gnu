They tapped nearby hydrants, and in search of additional water headed to the foot of Wall Street to break through the East River’s ice and pump more. The Great Fire of 1835 Helped Create Modern New York City |Daniel S. Levy |March 11, 2022 |Time 
Then, the community of people asking questions expanded like the spray from an open fire hydrant. The Key Role a Local Newspaper Played in the Trial Over Ahmaud Arbery's Murder |Janell Ross/Brunswick, Ga. |November 30, 2021 |Time 
Non-natural stimuli in this case would be parametric stimuli like oriented bars, while natural would include both the savanna as well as something human-made like a hydrant. Are We Wired to Be Outside? - Issue 92: Frontiers |Grigori Guitchounts |November 11, 2020 |Nautilus 
Some punk city kids crack open a fire hydrant and are playing in its spray. The Craziest Moments From ‘Ghost Shark’ (VIDEO) |Kevin Fallon |August 23, 2013 |DAILY BEAST 
The water deepened quickly and was almost covering a fire hydrant within 100 yards. In Stuyvesant Town, Evidence of Hurricane Sandy’s Wrath |Matthew DeLuca |October 30, 2012 |DAILY BEAST 
The Fire Hydrant: Starting on all fours, I lifted my leg like a dog relieving himself, and then extended that leg behind me. Does Sexercise Work? |Meghan Pleticha |May 26, 2010 |DAILY BEAST 
Shoeless, he ran into the car and barreled out of the driveway before careening off a fire hydrant and then smashing into a tree. Tiger's Thanksgiving Mystery — Solved |Gerald Posner |January 24, 2010 |DAILY BEAST 
Yes, you will still have TMZ on your front lawn (or camped out by the broken fire hydrant). Tiger’s Troubles Deepen |Lisa DePaulo |December 2, 2009 |DAILY BEAST 
He stopped at a hydrant and washed the mud off the elephants' legs and gave 'em an extra feed. David Lannarck, Midget |George S. Harney 
At the hydrant on one side stood a fire-engine blowing off its useless steam. Harper's Round Table, August 20, 1895 |Various 
If waterworks are handy, connect the boiler with a hydrant and after filling the boiler, let it receive the hydrant pressure. Farm Engines and How to Run Them |James H. Stephenson 
He crossed the fire lines, found his way to the engine captain near the main hydrant. The Five Arrows |Allan Chase 
The fat seemed to make them thirsty; they had to go to the hydrant to wash it down with cold water. Bird Lore, Volume I--1899 |Various