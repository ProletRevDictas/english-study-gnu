This Congress will welcome more women than ever before at 19 percent of the House and 20 percent of the Senate. The Unbearable Whiteness of Congress |Dean Obeidallah |January 8, 2015 |DAILY BEAST 
Even internally in the House, women are not getting their fair shake. The Unbearable Whiteness of Congress |Dean Obeidallah |January 8, 2015 |DAILY BEAST 
He then provides some insight into his psyche - complete with Animal House reference. Huckabee 2016: Bend Over and Take It Like a Prisoner! |Olivia Nuzzi |January 8, 2015 |DAILY BEAST 
Even the arguably more democratic House is only at 10 percent black members. The Unbearable Whiteness of Congress |Dean Obeidallah |January 8, 2015 |DAILY BEAST 
While 19 percent of the House is female, just one woman will get to chair one of its 20 committees. The Unbearable Whiteness of Congress |Dean Obeidallah |January 8, 2015 |DAILY BEAST 
It was a decayed house of superb proportions, but of a fashion long passed away. Checkmate |Joseph Sheridan Le Fanu 
A Yankee, whose face had been mauled in a pot-house brawl, assured General Jackson that he had received his scars in battle. The Book of Anecdotes and Budget of Fun; |Various 
On the thirteenth of the same month they bound to the stake, in order to burn alive, a man who had two religious in his house. The Philippine Islands, 1493-1898, Volume XX, 1621-1624 |Various 
"Better so," was the Senora's sole reply; and she fell again into still deeper, more perplexed thought about the hidden treasure. Ramona |Helen Hunt Jackson 
Old Mrs. Wurzel and the buxom but not too well-favoured heiress of the house of Grains were at the head of the table. The Pit Town Coronet, Volume I (of 3) |Charles James Wills