“Doing more with less” is often management-gabble to justify arbitrary budget cuts. Why Old-School Airships Now Rule Our Warzones |Bill Sweetman |June 30, 2014 |DAILY BEAST 
The excitement and gabble were worse than the Dean case, or Federation, and sickened me, for they were all on the wrong track. Over the Sliprails |Henry Lawson 
But no flight of arrows rattled among the boughs, and all we heard was the gabble of excited voices. Mass' George |George Manville Fenn 
The lovely gabble of the cranes and the wild swans comes back to me whenever I think of the place. Thirty Years in Australia |Ada Cambridge 
"You must be drunk yourself to come here waking me up in the middle of the night, to hear this idle gabble," said Louis angrily. File No. 113 |Emile Gaboriau 
Two incidents alone relieved the dead level of idiocy and incomprehensible gabble. The Works of Robert Louis Stevenson - Swanston Edition Vol. XXII (of 25) |Robert Louis Stevenson