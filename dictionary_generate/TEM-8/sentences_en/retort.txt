Last week, The Lancet published a retort from a team led by Temple University biologist Enrico Bucci. How America Could Be Helping COVID Ravage the World |David Axe |May 21, 2021 |The Daily Beast 
When the body goes into the retort, the first thing to burn is its cardboard box, or “alternative container” as it’s called on the funeral bill. How to Be Better at Death (Ep. 450) |Maria Konnikova |February 4, 2021 |Freakonomics 
My grandfather’s witty retort kept coming to mind this year. Tallying up a year of loss: A lot of pounds, too many loved ones, countless connections |Jerry Brewer |December 27, 2020 |Washington Post 
The Ralph Retort, a paragon of ethical journalism websites, decided to make crowdsourcing stuff to discredit me into a project. Rage Against GamerGate’s Hate Machine: What I Got For Speaking Up |Arthur Chu |November 17, 2014 |DAILY BEAST 
“I want Ebola to leave Liberia, so I can go to school,” came the snappy retort deciphered by locals. The Life of a Liberian Child with Ebola |Sarah Crowe |November 5, 2014 |DAILY BEAST 
(To which the obvious retort was: the Christ-like thing to do would be to forgive me). Sympathy for the Devils: Scenes From the Social Conservative Collapse |Olivia Nuzzi |June 21, 2014 |DAILY BEAST 
That remark prompted a sharp retort from Russian President Vladimir Putin. Just How Extremist Are the Rebels in Syria? |Jamie Dettmer |September 17, 2013 |DAILY BEAST 
Here's a Boehner retort to Sean Hannity (via Buzzfeed's Rebecca Berg). Why I Love John Boehner |Justin Green |March 14, 2013 |DAILY BEAST 
There is nothing like a plaintive retort when your case is utterly indefensible. Blackwood's Edinburgh Magazine, No. CCCXXXIX. January, 1844. Vol. LV. |Various 
In conversing with foreigners, if they speak slightingly of the manners of your country, do not retort rudely, or resentfully. The Ladies' Book of Etiquette, and Manual of Politeness |Florence Hartley 
Probably he lays hold of the elements of experience and casts them into a seeming retort of reveries. Repertory Of The Comedie Humaine, Complete, A -- Z |Anatole Cerfberr and Jules Franois Christophe 
The whole mass is then transferred to a retort and distilled over a slow fire. A Woman's Journey Round the World |Ida Pfeiffer 
"But my rings always make tusks more beautiful," was his retort. Kari the Elephant |Dhan Gopal Mukerji