For many dogs, gnawing on things seems to help distract them. Best dog toys to keep your pet happy and healthy |Florie Korani |August 19, 2021 |Popular-Science 
These are the kinds of questions that have gnawed at me throughout my 25 years in the tech industry. The best way to grow your tech career? Treat it like an app |Ram Iyer |July 30, 2021 |TechCrunch 
What he’d “lost” by not hanging on as South Sea shares approached £1,000 gnawed at him. Today’s Crypto Fanatics Could Learn a Lot From Isaac Newton’s Money Mishaps |Thomas Levenson |June 23, 2021 |Time 
The rodents gnaw down trees to create lodges and dams, and dig channels for transporting their logs to the dams. Simple hand-built structures can help streams survive wildfires and drought |Brianna Randall |March 26, 2021 |Science News 
I think we’ve been greatly underplaying the chronic, gnawing anxiety we’ve had to keep at bay to function. Vaccinated lives: 5 health experts revel in simple pleasures |Lena H. Sun |March 10, 2021 |Washington Post 
In the end, the ethical implications of using a drug to pull statements from otherwise unwilling people began to gnaw. Would Truth Serum Work on James Holmes in the Aurora Shooting Trial? |Kent Sepkowitz |March 14, 2013 |DAILY BEAST 
Stanley Crouch on why there are so many predators “looking for some high-profile black female meat to give the gnaw.” Michelle vs. the All-American Jackass |Stanley Crouch |March 25, 2009 |DAILY BEAST 
For three generations it's been a sort of a gnaw-bone, to be dug up and chewed on when there's nothing else. David Lannarck, Midget |George S. Harney 
Then she lay down again, chuckling softly as she did when the mouse escaped, even though it was to gnaw her cheese. Country Neighbors |Alice Brown 
I began to gnaw it and play with it, and when Ned called out, "fetch it," I dropped it and ran toward him. Beautiful Joe |Marshall Saunders 
I know I am only the mouse, but I could gnaw through very strong cords. Mr. Grex of Monte Carlo |E. Phillips Oppenheim 
The animals stand in a group, and the mules gnaw at the frozen dung of former visitors. Trans-Himalaya, Vol. 2 (of 2) |Sven Hedin