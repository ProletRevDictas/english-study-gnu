The snake also suffered from kidney damage, possibly a result of water deprivation near the end of its life. X-rays reveal what ancient animal mummies keep under wraps |Helen Thompson |August 20, 2020 |Science News 
Lizard and snake genomes are usually around 2 gigabases, she says. How tuatara live so long and can withstand cool weather |Jake Buehler |August 5, 2020 |Science News 
Physicists already knew that tree snakes flatten their bodies as they leap. Flying snakes wriggle their way through the air |Emily Conover |August 4, 2020 |Science News For Students 
Over 300 years ago, microscopy pioneer Antonie van Leeuwenhoek described sperm tails swaying in a symmetric pattern, like “that of a snake or an eel.” Human sperm don’t swim the way that anyone had thought |Jack J. Lee |July 31, 2020 |Science News 
That’s just like venom glands of snakes, but it’s a first for amphibians, the researchers report July 3 in iScience. Bizarre caecilians may be the only amphibians with venomous bites |Christie Wilcox |July 3, 2020 |Science News 
For these Arabs, Iran is the raised (and loaded) head of the snake. The Nuclear Deal That Iran’s Regime Fears Most |Djavad Khadem |November 22, 2014 |DAILY BEAST 
The black mamba snake slithering towards Lakshmi is highly venomous. ‘Gods of Suburbia’: Dina Goldstein’s Arresting Photo Series on Religion vs. Consumerism |Dina Goldstein |November 8, 2014 |DAILY BEAST 
Here the snake oil quotient is a bit more evident than in the skybox seats occupied by insights made using hard science. All These AIDS ‘Cures’ Are a Fantasy—One That Can Cause Real Harm |Kent Sepkowitz |November 6, 2014 |DAILY BEAST 
Which of these foods have science to back them up, and which are nothing but snake oil? Fish Oil, Turmeric, and Ginseng, Oh My! Are ‘Brain Foods’ B.S.? |Dr. Anand Veeravagu, MD |October 10, 2014 |DAILY BEAST 
The snake was particularly kissed and touched as worshippers entered. Fighting Back With Faith: Inside the Yezidis’ Iraqi Temple |Michael Luongo |August 21, 2014 |DAILY BEAST 
He heard Mohammedans alluding to a Brahmin as a leader—so might a wolf and a snake make common alliance against a watch dog. The Red Year |Louis Tracy 
On his loins was a lion of great fierceness, and coiled round his waist was a hissing mamba (snake). Uncanny Tales |Various 
So they bore Spotted Snake away with them in the canoe, while the Dogtown gang shrieked farewells from the old landing. The Campfire Girls of Roselawn |Margaret Penrose 
Dorothy again caught the furtive glance of the woman's evil eyes, and recoiled from it as if she had trodden upon a snake. The World Before Them |Susanna Moodie 
A few hundreds in a few hungry pockets, and we run a snake through the legislature declaring that lake state property. Ancestors |Gertrude Atherton