That cynicism doesn’t seem to be motivating them to sit on the sidelines during elections. Why Younger Americans Don’t Vote More Often (*No, It’s Not Apathy) |Amelia Thomson-DeVeaux (Amelia.Thomson-DeVeaux@abc.com) |October 30, 2020 |FiveThirtyEight 
McConnell argued, with obvious cynicism, that he wanted the American people to weigh in on the president first. The Republican court plan has the support of the majority they’re concerned about: Republicans |Philip Bump |September 23, 2020 |Washington Post 
I spotlighted the Change the World list yesterday, but I will do so again today, because it is a welcome antidote to the cynicism that pervades today’s discussions today about the state of business. How AB Inbev is trying to change the world |Alan Murray |September 22, 2020 |Fortune 
The cynicism behind this statement can make many people uncomfortable. The Dark Side of Smart - Facts So Romantic |Diana Fleischman |September 15, 2020 |Nautilus 
It’s got the idealism of The West Wing married to the cynicism of House of Cards, and it somehow works. One Good Thing: A Danish drama perfect for political devotees, now on Netflix |Emily VanDerWerff |September 11, 2020 |Vox 
It creates a cynicism in us that is not the most noble of things to dwell upon. Ron Perlman's Secret Suicide Attempt |William O’Connor |October 28, 2014 |DAILY BEAST 
For them, the cowardly cynicism of the status quo will do just fine. How the Media Failed to Nail the NFL |Steve Almond |October 19, 2014 |DAILY BEAST 
But civic cynicism is not, and it means we can never be the unified country we were in World War II. Can America Still Win Wars? |Michael Tomasky |October 4, 2014 |DAILY BEAST 
The knee-jerk cynicism with which it has been greeted is dysfunctional. Paul Ryan’s New Plan Is a Good One, Especially for Black People |John McWhorter |July 30, 2014 |DAILY BEAST 
That is the height of cynicism, combined with an admission of impotence. Border Kids Crisis—Impotent Congress |John Avlon |July 10, 2014 |DAILY BEAST 
“Perhaps I div,” returned the Scot, with that touch of cynicism which is occasionally seen in his race. The Garret and the Garden |R.M. Ballantyne 
Ordinarily she was smiling with an affected cynicism, and it was plain to be seen that she respected her intellect. Ancestors |Gertrude Atherton 
Directly he entered her presence he seemed to forget his cynicism, and to become light-hearted and gay. The Everlasting Arms |Joseph Hocking 
The voice could be heard, in different modulations, but always with profound cynicism as its basis. Mystery Ranch |Arthur Chapman 
Along with his war-books, Thyrsis was reading his daily newspaper, which came to him freighted with the cynicism of the hour. Love's Pilgrimage |Upton Sinclair