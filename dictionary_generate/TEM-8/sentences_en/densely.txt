Through drying, the team says they’re able to create a fertilizer that’s just as nutrient-dense by mass as a typical conventional fertilizer. Your pee could be the golden ticket to a greener world |Ula Chrobak |November 19, 2020 |Popular-Science 
For most liquids, cooling makes them become denser and more difficult to compress. Supercooled water has been caught morphing between two forms |Emily Conover |November 19, 2020 |Science News 
That dense head gives you enough power to actually chop through larger branches. 5 Axes and Hatchets We Love for Wood Chopping |Graham Averill |November 10, 2020 |Outside Online 
Snowshoeing and cross-country skiing are easily accessible from your doorstep, as well as some adventurous backcountry terrain through dense glades. Take a Backcountry Hut Trip This Winter |Megan Michelson |November 10, 2020 |Outside Online 
Patchy areas of dense fog may develop in the early morning, but skies are generally partly cloudy. PM Update: Warm, stagnant air continues into tomorrow |A. Camden Walker |November 8, 2020 |Washington Post 
In its own weird way, by the end, The Colbert Report was as densely serialized as Lost. The End of Truthiness: Stephen Colbert’s Sublime Finale |Noel Murray |December 19, 2014 |DAILY BEAST 
Densely populated and impoverished, the community was struggling long before Ebola arrived. Meet the Liberian Girls Beating Ebola |Abby Haglage |October 29, 2014 |DAILY BEAST 
And the reasons for that suggest just how densely complicated the Mideast quagmire has become. Obama’s Arab Backers May Draw the U.S. Deep Into the Mideast Quagmire |Jamie Dettmer |September 25, 2014 |DAILY BEAST 
In the mid-20th century, the 15.5-acre island had become the most densely populated place on Earth. Japan's James Bond Villain Ghost Town |Nina Strochlic |August 7, 2014 |DAILY BEAST 
New Jersey is the most densely populated state in the nation. A Mob-Defying Former Mayor Knows Why New Jersey Is So Corrupt |Burt Ross |February 13, 2014 |DAILY BEAST 
"I believe that is what they call it," Gordon answered, gazing back at her with his densely clouded blue eyes. Confidence |Henry James 
It is of an exceedingly hard, densely compact nature; from its hardness difficult to work, but susceptible of a very high polish. Asbestos |Robert H. Jones 
Thus they made their way in good array down the Rue Saint-Antoine, which was densely crowded with men, women, and children. The Merrie Tales Of Jacques Tournebroche |Anatole France 
This part is really the city; beyond is a suburb laid out in gardens densely inhabited. Ways of War and Peace |Delia Austrian 
They are densely ignorant though they may be fluent talkers. The Eugenic Marriage, Vol. 3 (of 4) |W. Grant Hague