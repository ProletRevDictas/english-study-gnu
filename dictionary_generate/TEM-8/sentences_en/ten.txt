Ten of the callers identified the man as Jason Polanco of the Bronx. Shot Down During the NYPD Slowdown |Michael Daly |January 7, 2015 |DAILY BEAST 
As played by Omundson, King Richard is effeminate, sincere, and ten times funnier than everyone else. ‘Galavant’: A Drunken, Horny Musical Fairy Tale |Melissa Leon |January 5, 2015 |DAILY BEAST 
“Wait…” Suddenly a huge, graceful black marlin leaps out of the water, sending a shower of water ten feet high. The Story Behind Lee Marvin’s Liberty Valance Smile |Robert Ward |January 3, 2015 |DAILY BEAST 
I first saw Marvin when I was ten years old, living with my parents in Arlington, Virginia. The Story Behind Lee Marvin’s Liberty Valance Smile |Robert Ward |January 3, 2015 |DAILY BEAST 
I did a ten minute scene in his class: the guy who had gangrene in his leg in The Snows of Kilimanjaro. The Story Behind Lee Marvin’s Liberty Valance Smile |Robert Ward |January 3, 2015 |DAILY BEAST 
In less than ten minutes, the bivouac was broken up, and our little army on the march. Blackwood's Edinburgh Magazine, No. CCCXXXIX. January, 1844. Vol. LV. |Various 
Ten minutes later, veiled and cloaked, she stepped out alone into the garden. Hilda Lessways |Arnold Bennett 
The clock struck ten, and clerks poured in faster than ever, each one in a greater perspiration than his predecessor. The Posthumous Papers of the Pickwick Club, v. 2(of 2) |Charles Dickens 
Strathland would bundle me out in ten minutes if anything happened to Jack. Ancestors |Gertrude Atherton 
There is cause for alarm when they bring one hundred and ten ships into these seas without any means of resistance on our part. The Philippine Islands, 1493-1898, Volume XX, 1621-1624 |Various