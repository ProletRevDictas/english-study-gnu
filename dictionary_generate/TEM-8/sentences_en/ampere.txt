Currents are measured in units known as amperes, or amps, for short. Explainer: Understanding electricity |Trisha Muro |December 10, 2021 |Science News For Students 
Jean Marie Ampere, famed as a mathematician and natural philosopher, died. The Every Day Book of History and Chronology |Joel Munsell 
Ampere—Amperes are units by which the rate of flow of electrical current (electrons) is measured. Electricity for the 4-H Scientist |Eric B. Wilson 
An ampere is 6.3 billion electrons passing one point in a circuit, in one second. Electricity for the 4-H Scientist |Eric B. Wilson 
Show how to figure the wattage that a circuit protected by a 15 ampere fuse can handle. Electricity for the 4-H Scientist |Eric B. Wilson 
The coulomb is defined as the quantity of electricity delivered by a current of one ampere during one second. Physics |Willis Eugene Tower