In an arms races to prove they are holier than thou, rabbis add more and more prohibitions and prerequisites. Why Ultra-Orthodox Jewish Babies Keep Getting Herpes |Emily Shire |July 29, 2014 |DAILY BEAST 
Three times,” he says angrily, “thou shalt betray me ere the cock crows. The Stacks: The Judas Priest Teen Suicide Trial |Ivan Solotaroff |June 28, 2014 |DAILY BEAST 
Officials say thou-sands of International school boys may have crossed paths with William Vahey. Pedophile Teacher Drugged, Abused 90 Students From Managua to London |Nico Hines |April 25, 2014 |DAILY BEAST 
Make thee an ark of gopher wood; rooms shalt thou make in the ark, and shalt pitch it within and without with pitch. The Story of Noah's Ark From the Bible’s Book of Genesis |The Daily Beast |March 24, 2014 |DAILY BEAST 
Go forth of the ark, thou, and thy wife, and thy sons, and thy sons' wives with thee. The Story of Noah's Ark From the Bible’s Book of Genesis |The Daily Beast |March 24, 2014 |DAILY BEAST 
Honour the physician for the need thou hast of him: for the most High hath created him. The Bible, Douay-Rheims Version |Various 
Solely over one man therein thou hast quite absolute control. Pearls of Thought |Maturin M. Ballou 
For it is better that thy children should ask of thee, than that thou look toward the hands of thy children. The Bible, Douay-Rheims Version |Various 
In the time when thou shalt end the days of thy life, and in the time of thy decease, distribute thy inheritance. The Bible, Douay-Rheims Version |Various 
Forget it not: for there is no returning, and thou shalt do him no good, and shalt hurt thyself. The Bible, Douay-Rheims Version |Various