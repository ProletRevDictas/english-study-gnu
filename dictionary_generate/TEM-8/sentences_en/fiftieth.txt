As I was lifting myself up for the fiftieth time, he said, “Now get in the water and make a sugar cookie.” Inside Seal Team Six by Don Mann Excerpt |Don Mann |December 4, 2011 |DAILY BEAST 
He even bought a convertible sports car for his fiftieth birthday. A Man Who Could Be Me |Bruce Feiler |April 27, 2010 |DAILY BEAST 
After passing his fiftieth year an individual should abstain from venesection. Old-Time Makers of Medicine |James J. Walsh 
I should like to see you without it for the fiftieth part of a second. The Tragic Muse |Henry James 
A pocket-book, with which he presented his father on his fiftieth birthday, brought out his unqualified praise. There and Back |George MacDonald 
Prince Paskiewitch completed the fiftieth year of his service in the Russian army. Harper's New Monthly Magazine, No. VII, December 1850, Vol. II |Various 
Mark Twain's fiftieth birthday was one of the pleasantly observed events of that year. Mark Twain, A Biography, 1835-1910, Complete |Albert Bigelow Paine