她必須離開位於菲律賓中部農業小鎮1月(Januiay)的簡陋小屋，前往一個茂密的灌木叢地區，步行10分鐘即可到達。 由於COVID-19，菲律賓仍然沒有完全重新開放學校。 這對孩子們有什麼影響? 乍得·德·古茲曼，2021年12月2日 
 房子沒了，雅各布斯在果園和灌木叢下耕耘，這裡曾是社區的綠洲。 在住房危機期間，他拆除了窮人居住的汽車旅館。 城市領導人什麼都沒做。 |由Anjeanette Damon拍攝，David Calvert攝影，特別為ProPublica, 2021年11月12日| 
 這款輕量級修剪機不到三磅重，有一個符合人體工程學的手柄和強大的1200轉/分鐘電機，可以塑造和剪切任何灌木。 最好的樹籬修剪器:用這些強大的修剪器保持你的後院整潔弗洛裡·科拉尼，2021年6月24日 
 與聚集在橋下的蝙蝠或為夥伴梳理皮毛的狒狒相比，即使是最善於社交的雌性長頸鹿也常常看起來像是碰巧在同一叢灌木叢中四處覓食。 擁有更多的朋友可以幫助雌性長頸鹿活得更久|蘇珊·米利烏斯| 2021年2月25日|科學新聞 
 它坐落在一片長滿野草和灌木的不起眼的土地上。 Gosta Peterson的《波西米亞狂想曲:揭開攝影師60年代的秘密》|Lizzie Crocker | 2014年9月10日|DAILY BEAST 
 他們粉刷了他的房子，維護了他的院子，更換了草皮，安裝了人造草皮，種植和移動了灌木。 《胡佛的秘密檔案》|羅納德·凱斯勒| 2011年8月2日|每日野獸 
 基普從灌木叢中掙脫出來，穿過開闊的草坪，朝那個人躺在地上的地方跑去。 戴兜帽的偵探，第三卷第二冊，1942年1月|各種 
 沒有一片灌木或花園打破這地方灰色的單調。 《從一輛汽車看英國的公路和小道》|托馬斯·d·墨菲 
 她在那幢有淡紫色灌木和低矮鐵柵欄的白房子裡，上上樓下住了二十五年。 泰莎·沃茲沃斯的紀律|珍妮·m·德林克沃特 
 它所佔據的場地現在是一個公共花園，有灌木和鮮花。 《從一輛汽車看英國的公路和小道》|托馬斯·d·墨菲 
 他不能為此目的使用灌木和觀賞樹木，也不能為此目的砍伐站立的木材。 普特南為外行人寫的簡易法律書|阿爾伯特·西德尼·博爾斯