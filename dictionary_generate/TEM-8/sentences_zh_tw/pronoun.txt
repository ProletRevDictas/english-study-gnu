他們憤怒地說，他們今天可能是在對一場騷亂做出反應，但明天他們就會因為你使用了錯誤的代詞而封鎖你。 右翼給硅谷的信息:“言論自由只屬於我，而不是你”David French | 2021年1月16日|時間 
 她和她的丈夫根據家族傳統小心翼翼地給他們的孩子起了名字，現在其中一個想要一個新名字和新的代詞，這在哈蘇里聽來是錯誤的。 一個母親不可能無所不知|Terri Schlichenmeyer | 2020年10月9日|華盛頓刀刃報 
 它可以消除代詞的歧義，翻譯，推斷，類比，甚至執行一些常識性的推理和算術。 歡迎來到下一層扯皮-第89期:陰暗面|Raphaël Millière | 2020年9月9日|鸚鵡螺 
 這是一項識別文檔中所有專有名稱的任務，並找出文本中的哪些代詞指的是哪些人或哪些組織。 人工智能能理解詩歌嗎? 2020年8月18日|財富 
 因為捉鬼敢死隊是一個團體，他們被描述為代詞they。 你能糾正這些語法錯誤的歌詞嗎? |布里吉德·沃爾什| 2020年8月5日| Z之後的一切 
 哦，隨便用一個代詞就能搞出天堂和地獄。 承認擁有的壞處|Samantha Harvey | 2014年12月14日|DAILY BEAST 
 當他大聲說出標題時，他特別強調了代詞。 比爾·布拉頓能解決白思豪的紐約警察困境嗎? 邁克爾·戴利| 2013年12月5日|DAILY BEAST 
 有UP和US, ut(一個古老的名字，第一個(和最後一個)音)，do, WE(最有趣的代詞)和WO，是woe。 全國拼字遊戲日:一首詩讓你知道所有101個雙字母單詞|David Bukszpan | 2013年4月13日|DAILY BEAST 
 這個稱呼是有意為之的:《模擬人生》是最早一批有大量女性玩家的遊戲之一。 《模擬城市》比你聰明(即使你是城市規劃師)|Josh Dzieza | 2013年2月26日|DAILY BEAST 
 他問道:“‘我的’這個代詞有什麼魔力，讓我們有理由推翻公正的真理的決定?” 為什麼偏袒是美德:反對公平的案例|Stephen T. Asma | 2012年12月7日|DAILY BEAST 
 主語代詞，當不強調時，不表達，而是從動詞的結束處理解。 查爾斯·阿爾弗雷德·唐納 
 關係賓代詞經常作為人稱代詞重複使用，這樣動詞的賓語就表示了兩次。 查爾斯·阿爾弗雷德·唐納 
 第三個人稱代詞- he, she, it -在所有情況下，它的指代都特別不確定。 英語:寫作與文學|W。 韋伯斯特 
 這就是我隨意使用人稱代詞的理由，不是為了突出人，而是為了強調現實。 敘利亞的銀鈴。 美國尼爾森 
 代詞的析取形式有時也保留在動詞和形容詞之前。 住在棚屋裡的印第安人亨利·r·斯庫爾克拉夫特