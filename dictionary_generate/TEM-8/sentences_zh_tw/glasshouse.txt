1948年，當這座溫室遺址被挖掘時，只發現了一些小碎片和墨綠色的水滴。 早期詹姆斯敦商品和工業畫冊| 保羅·哈德遜 
 在去那裡的路上，在一個巨大的溫室周圍可以看到一圈老式的紫杉樹籬。 倫敦附近的自然|理查德·傑弗瑞 
 黛西說那是玻璃屋街的一個地方，她對它沒有太大的好感。 《邪惡街》第二卷|康普頓·麥肯齊 
 整個溫室都被一棵美麗的藤蔓撐成拱形。 《極樂與其他故事》凱瑟琳·曼斯菲爾德 
 一個流傳已久的傳說是在詹姆斯敦的玻璃溫室製造珠子，但這並沒有考古證據。 來自歷史和技術博物館|伊沃·諾埃爾·休謨