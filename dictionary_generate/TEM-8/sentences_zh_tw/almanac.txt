他出現在年曆、報紙和煽情小說的木刻插圖中。 撒旦恐慌的革命根源仍然在美國政治中被援引|Zara Anishanslin | 2022年8月5日|華盛頓郵報 
 這種變化是由占星年鑑等非科學出版物推動的。 冥王星不再是一顆行星了——真的是這樣嗎? 麗莎·格羅斯曼2021年10月28日|學生科學新聞 
 從本質上說，他為曆書提供了基礎，這些曆書出版後將成為美國最受歡迎和最準確的歷書之一。 美國對太空外星人和不明飛行物的擔憂比你想象的要早|戈登·弗雷澤| 2021年6月25日|華盛頓郵報 
 到目前為止，這個較小的值甚至被國家標準與技術協會(National Institute of Standards and Technology) CODATA推薦物理常數列表(核與原子化學家和物理學家的官方年鑑)的官方值所採用。 測量大自然組成部分的突破-如此浪漫的事實|蘇博德·帕蒂爾| 2021年1月8日|鸚鵡螺 
 《年鑑》還告訴我們，如果你有任何拆除計劃，這將是一個好時機。 停止把你的瘋狂歸咎於月亮|Janelle Dumalaon | 2014年6月20日|DAILY BEAST 
 另一方面，科爾納基似乎對《美國政治年鑑》(the Almanac of American Politics)裡的那種奧秘更感興趣。 Steve Kornacki, MSNBC取代Chris Hayes的聰明之人，只想有用|David Freedlander | 2013年3月21日|DAILY BEAST 
 他的《曆書》比利德比特的《曆書》或與他同時代的其他計算器的《曆書》更受歡迎。 《美國有色人種的狀況、地位、移民和命運》馬丁·r·德拉尼 
 當印第安人聽到這些教訓的時候，最好對他的嚮導說，他不要他的歷書。 《哲學詞典》第一卷(10卷)|弗朗索瓦-瑪麗·阿魯埃(伏爾泰) 
 讓我們以1749年聖恩年的一本流行年鑑為例。 家庭古玩聊天|弗雷德·w·伯吉斯 
 他貪婪的頭腦從來沒有閒著過。1732年，他開始出版著名的《窮理查年鑑》。 蒸汽鋼鐵和電力|詹姆斯w斯蒂爾 
 慶祝的狂熱變得如此強烈，以至於人們經常查閱年鑑。 《暴雪之家》|道格拉斯·莫森