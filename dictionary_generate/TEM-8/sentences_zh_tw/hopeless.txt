當你處於壓力之下，感到絕望時，這些衝突更有可能演變成暴力。 當警察停止工作時，市長能做些什麼? | 2020年9月3日|ProPublica 
 無助，絕望，沮喪，所有你能想到的英語中悲傷的情緒。 播客:一項有135年曆史的法律如何讓印度關閉互聯網Anthony Green |， 2020年9月2日MIT科技評論| 
 雖然看到這一切可能會讓人感到絕望，但請記住，你仍然可以為確保自己和你愛的人的健康負責。 美國疾病控制與預防中心新的COVID-19檢測指南可能會使大流行變得更糟 
 這封信沒有得到任何正式的回應，但其中一名支持民主的經濟學家Fouad Abdelmoumni說，他在該機構高層的朋友告訴他，這封信毫無希望，並敦促他放棄這件事。 在NSO內部，以色列十億美元的間諜軟件巨頭|Tate Ryan-Mosley | 2020年8月19日|MIT技術評論 
 儘管公投結果如此，但作為“歐洲最後一個獨裁國家”，情況並非完全沒有希望。 白俄羅斯選舉:歐洲“最後的獨裁者”宣稱獲勝，有爭議的結果引發大規模騷亂lgbtq編輯2020年8月12日沒有直接新聞 
 社會本身必須改變，從我們絕望的處境中走出來。 沒有上帝，沒有警察，沒有主人|詹姆斯·普洛斯| 2015年1月1日|每日野獸 
 這些方陣從盧旺達湧出，就像從波斯尼亞和現在從敘利亞湧出一樣，毫無希望。 發明“種族滅絕”一詞的人|Nina Strochlic | 2014年11月19日|DAILY BEAST 
 在裡面，似乎沒有希望了，因為眼前的每張椅子都坐滿了人，有十幾個人在地板上睡著了。 堆疊:H.L.門肯在1904年巴爾的摩大火上 門肯| 2014年10月4日|每日野獸 
 任何向一隻可愛的涉禽開槍的人都是無可救藥的缺陷，在我看來，是進化上的錯誤。 卡爾·海森能拯救佛羅里達州嗎? Malcolm Jones 2014年9月19日|DAILY BEAST 
 《經濟學人》雜誌2000年的封面將其稱為“無望的大陸”，配上一張笑容可掬、全副武裝的士兵的照片。 我是如何迷上非洲的(並寫了一本關於它的驚悚小說)|託德·莫斯| 2014年9月9日|每日野獸 
 這些確定性的衝突在他身體的每一個角落都留下了絕望的混亂。 阿爾傑農·布萊克伍德 
 當這個無望的願望穿過他的靈魂時，鐵也隨之進入，但並沒有消失。 牧師的爐邊卷3 4 |簡·波特 
 當他望著遠處一隻正從他身邊飛走的鳥時，他的態度是一種無可救藥的順從。 《覺醒與短篇小說集》|凱特·肖邦 
 奧地利御前大臣指出，繼續戰鬥是沒有希望的，因為他既沒有給養，也沒有彈藥。 拿破崙的那幾個小元帥| R。 p . Dunn-Pattison 
 這種生活狀況不適合她，她只看到一種可怕的、無望的無聊。 《覺醒與短篇小說集》|凱特·肖邦