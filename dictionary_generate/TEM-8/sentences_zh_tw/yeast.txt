這種化合物釋放出揮發性芳香，因為酵母酶甚至在瓶蓋蓋上之後也會斷開鍵。 作物生長的環境是如何塑造食物的氣味和味道的? 2020年9月10日|科學新聞 
 至少兩個世紀以前，人們就已經認識到酵母在葡萄酒發酵中的重要性。 作物生長的環境是如何塑造食物的氣味和味道的? 2020年9月10日|科學新聞 
 這些發現表明酵母是從多細胞祖先獨立進化而來的單細胞生活方式。 通過失去基因，生命往往進化得更復雜|Viviane Callier | 2020年9月1日|量子雜誌 
 後來，對酵母基因組的更全面的分析表明，基因丟失在酵母門中是普遍存在的。 通過失去基因，生命往往進化得更復雜|Viviane Callier | 2020年9月1日|量子雜誌 
 事實上，她說，酵母菌已經失去了參與不同代謝途徑的各種基因，並從多種細菌中重新獲得了它們。 通過失去基因，生命往往進化得更復雜|Viviane Callier | 2020年9月1日|量子雜誌 
 麥芽、酵母和水經過完全相同的烹飪、發酵和蒸餾。 威士忌酒桶對口感有多大影響? 2014年12月10日|DAILY BEAST 
 他們是葡萄酒，草本，並使人聯想到酵母發酵劑。 葡萄酒勢利者，有一種啤酒為你|Jordan Salcito | 2014年4月5日|每日野獸 
 在酒精含量達到15%後的發酵過程中，酵母開始產生組胺。 是的，女人能釀好酒|Jordan Salcito | 2014年3月22日|DAILY BEAST 
 另一種療法是Kvass，這是一種酒精含量較低的飲料，由幹黑麥麵包與糖和酵母浸泡而成。 世界各地最瘋狂的宿醉療法|Nina Strochlic | 2013年11月29日|DAILY BEAST 
 製作麵糰時，將麵粉、酵母、鹽和糖混合在一個大碗裡。 在你的烤架上做不尋常的披薩|Lydia Brownlow | 2012年6月15日|DAILY BEAST 
 酵母細胞光滑，無色，高度折射，球形或卵形細胞。 臨床診斷手冊|詹姆斯·坎貝爾·託德 
 它的缺點是與麵包一起引入了數量不定的乳酸和大量的酵母細胞。 臨床診斷手冊|詹姆斯·坎貝爾·託德 
 顯微鏡下通常可見沙黴素、細菌和大量的酵母細胞。 臨床診斷手冊|詹姆斯·坎貝爾·託德 
 晚餐已經結束; 男孩們衝進操場; 無論是酵母餃子還是鹹牛肉，都沒有卡在他們的喉嚨裡。 迪格比Heathcote | W.H.G. 金斯敦 
 “發酵的酵母總是酸的，我的孩子，”麵包師若有所思地歪著頭回答說。 《朝聖者的貝殼》或者《採石人費根》歐格尼·蘇