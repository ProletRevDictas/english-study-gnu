烤龍蝦上的甘藍紋勺麵包和海鮮一樣美味，甜煙燻的“強尼香料”(Johnny’s spice)有20種食材，就像更香的老海灣(Old Bay)。 我們因為流行病失去了強尼的半殼。 20年過去了，它值得我們舉杯告別。 2021年1月15日|華盛頓郵報 
 烤麵包，偶爾攪拌，直到香味，2到4分鐘。 恢復身體和精神的芳香雞湯|Olga Massov | 2021年1月7日|華盛頓郵報 
 在一個乾燥的小煎鍋中，用中火加熱，烘烤開心果，不斷攪拌，直到發出香味，大約3到5分鐘。 填滿山羊奶酪的無花果是你需要的低難度、高味道的開胃菜|Ellie Krieger | 2020年12月17日|華盛頓郵報 
 正如園藝學家所知，玫瑰無論叫什麼名字，都由數百種不同的揮發物和各種各樣的香味組成。 過去是什麼味道? - 93期:先驅|Ann-Sophie Barwich | 2020年12月9日|鸚鵡螺 
 把核桃放在有邊的烤盤上，放入烤箱烤8到10分鐘，或者烤到有香味，中途搖晃一下。 這些有嚼勁、含有棗和核桃的巧克力棒被稱為“上帝的食物”是有充分理由的 
 一千年的芬芳長生不老藥如何穿越大陸和時間，成為肯塔基賽馬的招牌飲料。 經典薄荷的故事起源Julep |Dane Huckelbridge | 2014年5月3日|DAILY BEAST 
 現在乳白色的玉米粥用勺子舀到盤子上，再用勺子舀上最香的深褐色燉雞肉。 兩隻雞，一把舊吉他，和一群陌生人:一場改變巴西生活的盛宴 
 即使在今天，在許多農村地區，人們仍在食用狗肉——委婉地稱之為“香肉”。 中國的狗染熱:曾經被迴避的寵物狗現在被擁抱了 
 空氣中充滿了這個地方的氣息，芳香而潮溼，充滿活力。 哥倫布日的奇特遺產勞倫斯·伯格林| 2011年10月10日|每日野獸 
 再來一勺多汁的莎莎醬，配上香濃的番茄和辣椒。 我們喜歡但不應該吃的食物莉迪亞·布朗洛| 2011年7月15日|每日野獸 
 當她端著泡沫芬芳的飲料回來時，他正站在那裡，雙手插在口袋裡，凝視著下面的城市。 祖先|格特魯德阿瑟頓 
 這些雪茄通常經過壓制，燃燒良好，留下深色的菸灰，並散發出芳香的氣味。 菸草; 它的歷史、品種、文化、生產和商業。 r·比林斯。 
 世界上最美麗、最芬芳的菸草田就在敘利亞。 菸草; 它的歷史、品種、文化、生產和商業。 r·比林斯。 
 在房子裡，在道路上，在街道上，馬尼拉的芳香總是隨風飄蕩。 菸草; 它的歷史、品種、文化、生產和商業。 r·比林斯。 
 傑西一邊說，一邊把狗放在一張苔蘚床上，開始精力充沛地撿起一堆堆芳香的針。 Box-Car Children |格特魯德·錢德勒·華納