在她所在學校的門廳裡，沒有戴面具的校長站在那裡，向聚集在7月29日開放日的數百個家庭致意。 儘管最近新冠肺炎病例激增，但我孩子的學校不會恢復戴口罩。 以下是我選擇做的事情。 | 2021年8月12日|ProPublica 
 確保門廳的燈具是時髦的，讓你屏住呼吸。 妥善準備你的房子以最|Justin Noble | 2021年6月24日|華盛頓刀鋒 
 兩個小時後，當我在幼兒園的門廳裡跪下來，用雙臂抱住那些咯咯笑的嬰兒時，我已經決定不再擔心了。 16個月後，我去看望了我的孫輩，意識到疫情對我的改變有多大 
 我到達的那天，門廳裡空無一人，只有一尊美麗的甘尼薩(Ganesha)雕塑。甘尼薩是印度教的神，被視為藝術的守護神。 Netflix vs莫迪和印度電影靈魂之戰Konstantin Kakaes, 2021年3月24日，MIT科技評論 
 我最接近的一次是在門廳裡，我去取最近點的三份外賣中的一份，在招待臺附近量了我的體溫。 回到1789年的新鮮理由，華盛頓特區最古老的餐廳之一|Tom Sietsema | 2021年2月19日|華盛頓郵報 
 門廳外的小房間佈滿了塗鴉，為人們提供了避雨的地方，滂沱大雨可能會出乎意料地、惡意地襲來。 剛果被遺忘的殖民之旅|Nina Strochlic | 2014年12月18日|DAILY BEAST 
 在他的表演中，查理·辛站在門廳裡，打翻了一個裝滿支票的桶，而不是冰水。 來自“混蛋”史蒂夫-奧凱文·扎瓦基的智慧2014年8月21日 
 每次我們去銀行的大廳，他都在談論這件事。 金·戈登:索尼克青年後的單飛，以及為什麼她認同“女孩”|安德魯·羅馬諾| 2014年4月10日|每日野獸 
 奧巴馬總統走進來時，我正坐在一個叫做“外交接待室”的門廳裡。 被婚姻顧問奧巴馬教訓了一頓| 
 到目前為止，最令人印象深刻的藝術品是門廳裡埃尼斯的青銅半身像。 特雷沃恩·馬丁的死就像恩尼斯·考斯比的死，艾利森·塞繆爾，2013年7月14日，每日野獸 
 在這種情況下，門廳就像一個舞廳，在客人和音樂還沒有到來之前，就被清理出來準備跳舞了。 《悲劇的繆斯》亨利·詹姆斯 
 她領著他上樓，穿過門廳，經過拉爾菲的房間，經過小客房，來到他們的臥室。 第一個|赫伯特·d·卡斯爾 
 不一會兒，我們就穿過了小小的門廳，站在了藝術家的聖殿裡。 鋼琴大師|哈里特·布羅爾 
 這個敏感的人在門廳裡停了一會兒，匆匆掃了一眼圓形的大房間。 《敏感的人》保羅·威廉·安德森 
 他的出租車顛簸著停了下來，雷吉向司機扔了一張賬單，然後跳下車，衝進了他住的大樓的門廳。 死亡犯了一個錯誤 科斯特洛