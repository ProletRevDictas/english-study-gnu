2003年，中國恢復了頒獎典禮的轉播，不過審查機構偶爾會刪減政治敏感人物或評論的畫面。 中國抵制奧斯卡將政治與遏制好萊塢主導地位的努力結合起來|Lyric Li, Steven Zeitchik | 2021年4月22日|華盛頓郵報 
 審查制度的缺失使得這些服務能夠講述一種新的故事——真實的印度故事，而不是被審查委員會所接受的故事。 Netflix vs莫迪和印度電影靈魂之戰Konstantin Kakaes, 2021年3月24日，MIT科技評論 
 Someswar Bhowmik寫道，到1920年，印度有幾個地區審查委員會，其成員被告知要注意“敏感問題”和“禁止的場景”。 Netflix vs莫迪和印度電影靈魂之戰Konstantin Kakaes, 2021年3月24日，MIT科技評論 
 這部電影通過了審查，但暴徒損毀公共財產，毆打人們，投擲燃燒瓶。 Netflix vs莫迪和印度電影靈魂之戰Konstantin Kakaes, 2021年3月24日，MIT科技評論 
 與此同時，自營出版商總是不得不小心翼翼地對待他們發佈的內容，否則就有可能被認為是非法或不恰當的審查機構盯上。 新規約束中國蓬勃發展的自助出版空間|廖麗塔| 2021年2月1日|TechCrunch 
 電影公司似乎對結果很滿意——儘管仍然選擇在許多外國地區審查死亡順序。 獨家:索尼電子郵件稱國務院在“採訪”中祝福暗殺金正恩威廉·布特2014年12月17日|每日野獸 
 不過，俄羅斯當局是否有可能審查互聯網，讓俄羅斯讀者無法閱讀《Meduza》? 俄羅斯最自由的網站現在在拉脫維亞|Anna Nemtsova | 2014年11月29日|DAILY BEAST 
 對中國大陸有關抗議活動的新聞進行審查的企圖適得其反。 中國遊客在香港自拍抗議|Brendon Hong | 2014年10月23日|DAILY BEAST 
 活動人士仍然必須自己訪問該網站，逃避他們本國對互聯網的審查或監控。 新網絡平臺眾包人權|Josh Rogin | 2014年7月10日|DAILY BEAST 
 該公司是否應該審查圍繞此類槍支照片的對話，禁止談論出售或價格? 人們在Instagram上賣槍… 而且大多是合法的|Brian Ries | 2013年10月22日|DAILY BEAST 
 我不太在意你如何審查或選擇學生的閱讀、談話和思考。 文明的拯救H。 (赫伯特·喬治)威爾斯 
 比利時人Van de Water的那篇論文，是關於通過否定自我審查來昇華潛意識的。 戴兜帽的偵探，第三卷第二冊，1942年1月|各種 
 電影審查者把他的角色主要解釋為一個引導父母。 兒童和青少年道德犯罪問題特別委員會的報告|Oswald Chettle Mazengarb等人。 
 審查人員的職責不包括確保他的裁決得到遵守。 兒童和青少年道德犯罪問題特別委員會的報告|Oswald Chettle Mazengarb等人。 
 他之所以被任命為審查官，完全是因為他與威爾士攝政王王子(regent Prince of Wales)有過一段酒交。 粗口史|朱利安·沙曼