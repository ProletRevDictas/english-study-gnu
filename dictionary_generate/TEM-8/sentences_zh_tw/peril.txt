日託中心長期以來利潤微薄，大流行使它們陷入了財務危機。 華盛頓特區日託中心的員工批評該市的疫苗計劃優先考慮學校員工，而不是他們 
 赤裸的野心本週我將寫《普里蒂》和奧黛麗·曼森(Audrey Munson)，她是美國電影史上被遺忘的人物，也是名人危險的悲慘例子。 記住在華盛頓上映的有史以來(不是)最醜聞的電影|約翰·凱利| 2021年1月10日|華盛頓郵報 
 每隔幾道食譜，馬丁就會停下來詳細講述海上生活的細節，並強調路易斯安那州海岸線面臨的危險。 隨著顧客和海岸線的消失，新奧爾良的一名廚師為她的社區而戰 
 已故藝術家的追隨者反過來幫助揭露音樂產業的危險，希望推動變革。 在幾位年輕的說唱歌手英年早逝之後，粉絲們決心將他們的遺產延續下去 
 由航空運輸支撐的至少4600萬個工作崗位岌岌可危。 達美航空和意大利航空將恢復美國和歐洲之間的無隔離航班 
 我們把尋求正義和尋求真相分開，後果自負。 比爾·白思豪的茶黨問題|威爾·凱恩| 2014年12月30日|每日野獸 
 事實是有分量和質量的，我們忽視它們或濫用它們，後果自負。 關於弗格森事件的事實，該死的|道格·麥金太爾| 2014年12月3日|每日野獸 
 我剛從越南迴來，迷失了方向，也許在尋找一種等同於戰爭的危險，但目標是生活的方向。 如何殺死一隻灰熊|道格·皮科克| 2014年11月23日|每日野獸 
 如今，偷獵行為日益猖獗，野生動物保護面臨危險。 埃博拉可能對非洲野生動物造成致命打擊Brandon Presser | 2014年11月3日|每日野獸 
 瑞克必須引導他剛出生的女兒朱迪絲穿過這個危險的世界。 《行屍走肉》的盧克·天行者:裡克·格萊姆斯是完美的現代神話英雄|Regina Lizik | 2014年10月28日|DAILY BEAST 
 在犧牲和危險的場景中，我一定渴望那種自我奉獻的激動人心的快感! 牧師的爐邊卷3 4 |簡·波特 
 他們冒著生命危險，毫不遲疑地滑下山坡。 阿里斯蒂德·普約爾的歡樂冒險|威廉·j·洛克 
 這使我有些發燒，因為他死在城外鄉下的一個花園裡，他的財產正處於極大的危險之中。 菲律賓群島，1493-1898，卷XX, 1621-1624 
 林恩不是膽小的弱者，在危急時刻坐下來徒勞地哭泣。 純金|伯特蘭·w·辛克萊 
 羅蘭夫人清楚地看到並深刻地感受到了她和她的朋友所面臨的危險。 羅蘭夫人，歷史的創造者|約翰S. C.阿伯特