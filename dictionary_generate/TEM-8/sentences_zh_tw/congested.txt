軟件將用完全安全、無縫和有組織的東西取代我們危險、擁擠、綿延的道路。 自動駕駛汽車是一個紅鯡魚-第92期:前沿|安東尼·湯森| 2020年10月21日|鸚鵡鸚鵡 
 這就好像一個人可以在擁擠的市中心高速行駛，永遠不會碰到紅綠燈一樣。 第一次實現了室溫超導Niall Firth, 2020年10月14日，MIT技術評論 
 對一些人來說，這一改變是由於員工擔心可能出現的問題，比如投票擁擠或選民之間需要額外的時間以滿足冠狀病毒的限制，這些問題可能會在大流行期間試圖投票時突然出現。 “美國最複雜的選舉日”:為什麼一些廣告公司今年採取了一種新的方法來抽時間投票Kristina Monllos |020年10月8日|Digiday 
 儘管如此，由於飛機的運力僅為30%左右，旅行者有空間也有理由考慮在這個假期季節乘坐飛機，而這個季節通常是一年中最擁擠的時候。 打算坐飛機度假? 預訂前要知道的6件事|matthewheimer | 2020年9月19日|財富 
 下面的三輛車都很舒適，由相當省油的發動機提供動力，這意味著我可以輕鬆通過擁擠的休息區，而不必為加油而緊張。 2020年8月22日，喬·菲利普斯，華盛頓刀刃 
 就在幾英尺外，在一間組織有序但擁擠不堪的臥室裡，板條箱、玻璃缸和其他圍欄從地板堆到天花板。 城市畜牧業怪異的地下世界|Dale Eisinger | 2014年5月19日|每日野獸 
 印度的蘇格蘭印度以擁擠的交通和城市的人群而聞名。 2014年3月30日每日野獸 
 印度以城市擁擠的交通和人群而聞名。 繁茂的地方:印度的蘇格蘭|通庫瓦拉達拉揚| 2014年3月25日|每日野獸 
 正如首爾的支持者所宣稱的那樣，它完全現代化，但同時也高度擁擠，美學貧瘠。 城市領導人喜歡密度，但大多數城市居民不同意|喬爾·科特金| 2013年9月16日|每日野獸 
 在美國和其他地方，當被問到這個問題時，人們通常會說，他們更喜歡住在不那麼密集、不那麼擁擠的地方。 城市領導人喜歡密度，但大多數城市居民不同意|喬爾·科特金| 2013年9月16日|每日野獸 
 在我的記憶中，除了倫敦，沒有哪個地方的街道比這裡更擁堵了。 《從一輛汽車看英國的公路和小道》|托馬斯·d·墨菲 
 穿過英格蘭銀行附近的倫敦橋是所有橋中最擁擠的。 《從一輛汽車看英國的公路和小道》|托馬斯·d·墨菲 
 每個地下室都很擁擠，頂層已經完全廢棄。 潘趣，或倫敦查裡瓦里，第147卷，1914年11月4日|各種 
 倫敦的濟貧院因為“蜂擁而入的是最底層和最難管理的窮人階層”而變得擁擠不堪。 英國窮人法政策|西德尼·韋伯 
 臨近午夜時分，隨著點票結果的陸續送來，國家自由黨變得異常擁擠。 新馬基雅維利|赫伯特·喬治·威爾斯