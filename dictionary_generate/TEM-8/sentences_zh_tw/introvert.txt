無論你是一個喜歡招待客人的出色廚師，還是像我一樣，一個喜歡吃爆米花和冰淇淋的內向者，你都受到了比爾德的影響。 詹姆斯·比爾德傳記，美味的盛宴|凱西·沃爾夫| 2020年10月22日|華盛頓刀鋒報 
 此外，就像很少有人是百分百的外向或百分百的內向一樣，我們也很少有人是百分百的平等主義者或百分百的偏執者。 相信怪物:大衛·利文斯通·史密斯論次人類——如此浪漫的事實|埃裡克·施維茨格貝爾| 2020年9月11日|鸚鵡螺 
 這和同時成為一個極端外向和極端內向的人一樣沒有意義。 相信怪物:大衛·利文斯通·史密斯論次人類——如此浪漫的事實|埃裡克·施維茨格貝爾| 2020年9月11日|鸚鵡螺 
 13歲的高瑟姆五(Gowtham V)在普維達姆就讀，他的母親告訴我，他是個內向的人。 學校教孩子如何在未來的惡劣氣候中生存|Charu Kasturi | 2020年8月7日|Ozy 
 是的，內向者和外向者在許多重要的方面確實不同，但事實上，他們常常在不知不覺中相互融合。 5個你可能誤用的心理學術語(第334頁重播)|Stephen J. Dubner | 2020年1月9日|魔鬼經濟學 
 現在47歲的瑪麗娜•貝盧斯科尼似乎要從政了——如果這位羞於上鏡的內向人士真的想當總統的話。 貝盧斯科尼的女兒會接管意大利嗎? 2014年5月10日|DAILY BEAST 
 作為一個天生內向的人，她完全滿足於自己相對的默默無聞。 《Vogue》創意總監格蕾絲·柯丁頓的回憶錄沒有什麼啟示|羅賓·吉夫漢| 2012年11月20日|每日野獸 
 Nemertine蠕蟲的頭狀長鼻或前內向長鼻的範圍很廣。 大英百科全書，第11版，第11卷，第5片|各種 
 你對他的印象是內省的還是內向的還是外向的? 沃倫委員會(26卷9):聽證會第九卷(15卷)|總統的刺殺肯尼迪總統委員會 
 此外，內向的人不會像他那樣在小事上對妻子發脾氣。 沃倫委員會(26卷9):聽證會第九卷(15卷)|總統的刺殺肯尼迪總統委員會 
 標記者收起了他們的標記，陷入了失態——戲班正式開始了。 《軍騾和其他戰爭素描》亨利·a·卡斯爾 
 主要維護者不僅僅變成了內向者。 《大時代》|弗裡茨·路透·雷伯