雖然一些研究發現，牛也喜歡和草原土撥鼠一起吃草，但齧齒動物和有蹄類動物之間的關係在某些方面是變化無常的，這一點我們還沒有完全理解。 拯救美國最瀕危哺乳動物|Ula Chrobak | 2021年3月5日|大眾科學 
 無論是好是壞，這種善變的心態已經主導了聯盟十年之久。 字母哥的耐心是送給密爾沃基的禮物。 別搞砸了，巴克斯。 傑裡·布魯爾2020年12月16日|華盛頓郵報 
 在如今變化無常的NBA氛圍下，很難想象雙方會達成和解，尤其是哈登準備讓局面變得更糟。 詹姆斯·哈登混淆了球員授權和自我破壞，傑瑞·布魯爾，2020年12月10日，華盛頓郵報 
 長期以來，Facebook一直允許開發者使用其API平臺，但對於開發者如何使用這些數據，Facebook的態度常常是善變的。 硅谷在Facebook的霸淩策略引起監管機構注意前幾年就已經開始擔心它了 
 對於一次性的現場活動，你需要的是儘可能簡單的設置——你不會想要一個變化無常的Wi-Fi路由器，因為它很難處理大量的客人。 如何為你所有的朋友和家人直播一個事件|哈里·吉尼斯| 2020年11月25日|大眾科學 
 滑雪勝地是一門生意，而且是一門變化無常的生意——一個糟糕的雪季意味著糟糕的收入。 太陽能滑雪纜車|每日野獸| 2014年11月24日|每日野獸 
 我們這些住在這裡的人都是善變的，注意力都是短暫的。 如果你能抓住他:班克西入侵紐約的重演|Alex Suskind | 2014年11月14日|DAILY BEAST 
 這就指向了白宮本身的罪責。 在拋棄他的高級助手之前，奧巴馬應該照照鏡子|Leslie H. Gelb | 2014年11月2日|DAILY BEAST 
 至少，善變且眼光敏銳的觀影者得到了一個振奮人心的診斷:比以往任何時候都更健康。 夏季大片已經死了嗎? 凱文·法倫2014年7月14日每日野獸 
 而銷售，這些微妙的小數字，如此依賴於善變的公眾的需求和奇想，正是因為這個原因而受阻。 鄉巴人天堂:小批量波旁威士忌的歷史丹·哈克布里奇| 2014年3月29日|每日野獸 
 這位新君主以其引人注目的個性和英俊的外表，立刻俘獲了他那善變的南方臣民的心。 拿破崙的那幾個小元帥| R。 p . Dunn-Pattison 
 不過，他對她的挑剔是完全正確的，因為財富，就像其他善變的玉一樣，如果不斷地濫用，就更有可能是真實的。 《坑鎮王冠》第一卷(共3卷)|查爾斯·詹姆斯·威爾斯 
 他是一個亡命之徒，被追殺，被鄙視，靠一個善變女人的反覆無常來維持自己的生命。 紅年|路易斯·特雷西 
 她對後果毫不在意，因此受到啟發，向善變的人氣女神求愛。 祖先|格特魯德阿瑟頓 
 女人指責男人反覆無常，男人則反駁女人善變。 Jean de La Bruyre的“人物”|Jean de La Bruyre