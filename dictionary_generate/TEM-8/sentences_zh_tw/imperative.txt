因此，對搜索環境及其所有細微差別有最全面的瞭解是推動成功和做出最明智的決策的必要條件。 解決機構搜索情報差距|Ian O 'Rourke和Stephen Davis | 2021年2月9日|搜索引擎觀察 
 重塑社區安全，撤除警察經費，直面“執法”最糟糕的行為——這些都不是新的道德義務。 “撤資警察”的真正含義|Simon Balto | 2021年2月9日|華盛頓郵報 
 我喜歡他們在《鎖墓》中的表現，以及他們在敘事中的重要性。 第九作者Tamsyn Muir如何搞怪太空歌劇|Constance Grady | 2021年2月5日|Vox 
 當務之急是，在所有美國人都能接種冠狀病毒疫苗之前的幾個月裡，迅速發現並糾正前任政府的缺陷，以挽救生命。 眾議院民主黨人就COVID-19疫情對OSHA、肉類工廠展開調查 
 對Covid-19疫苗的不信任有很多根源，但感覺有必要把它們放在巴基斯坦近代史的背景下。 中央情報局在巴基斯坦的假疫苗項目如何幫助推動反疫苗運動|Hala Iqbal | 2021年2月1日|Vox 
 此外，對美國來說，有一項基本的必要行動。 美國應該讓朝鮮為索尼黑客事件付出代價Gordon G. Chang | 2014年12月18日 
 對裡德來說，當務之急是確認34名地區法院提名人選中儘可能多的人選。 如果美國沒有司法部長會怎樣? |埃莉諾·克里夫特| 2014年11月14日 
 毫無疑問，正是這位羅斯福仍然相信，我們必須使美國“在一代人的時間裡相當激進”。 肯·伯恩斯的《羅斯福家族》好但有缺陷|哈維·j·凱| 2014年9月14日|每日野獸 
 許多教訓和評論都是命令式的，但不是全部。 Mike Leach處理動機殺人犯Geronimo |James A. Warren | 2014年8月17日|DAILY BEAST 
 這些都沒有削弱我對猶太復國主義的信念，也沒有削弱我對以色列作為猶太人家園和避難所的必要性的信念。 不要指責以色列種族隔離|Benjamin Pogrund | 2014年7月17日|每日野獸 
 她的目光從他的臉上移到海灣那邊去了，海灣那鏗鏘的低語傳到她的耳中，彷彿是一種慈愛而又迫切的懇求。 《覺醒與短篇小說集》|凱特·肖邦 
 大自然，永遠充滿活力和使命感，盡她最大的努力來補救“人對人的不人道”所造成的疾病。 看一眼歐洲|賀拉斯格里利 
 因為是瑪莎媽媽而不是她的女兒聽從了塞西爾夫人的命令:“過來!” Skyrie的桃樂茜|伊芙琳·雷蒙德 
 在接待您的那一天，除了最重要的職責之外，別讓任何事情來召喚您。 《女士禮儀手冊和禮貌手冊》|弗洛倫斯·哈特利 
 去聖奧爾本斯的時間只有一個小時，比我們原計劃的要短得多，但由於出發晚了，我們必須繼續前進。 《從一輛汽車看英國的公路和小道》|托馬斯·d·墨菲