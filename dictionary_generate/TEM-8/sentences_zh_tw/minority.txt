希望加里森能幫助奧斯汀改善這些問題和其他問題，因為五角大樓需要提升合格的少數族裔到更多的權力職位，並將極端分子排除在軍中。 五角大樓正在採取重大步驟來處理其多樣性問題 
 少數持不同看法的共和黨人可能會在下一輪初選中存活下來，但他們沒有能力讓88%到95%的共和黨人離開。 不要試圖拯救共和黨。 這是絕望的。 詹妮弗·魯賓2021年2月12日|華盛頓郵報 
 雖然我認為這沒有錯，但我也認為共和黨中少數人的界限已經變得更加清晰。 國會大廈暴動後，美國發生了怎樣的變化? |邁卡·科恩(micah.cohen@fivethirtyeight.com) | 2021年2月11日|FiveThirtyEight 
 雖然要知道這些措施是否會改善少數族裔社區的生活還為時過早，但它們代表著解決全球一個長期問題的努力。 今天墨西哥的環境種族主義問題根植於歷史|傑森·波特| 2021年2月11日|華盛頓郵報 
 這意味著，來自資源不足的種族和少數民族社區的老年人能夠預約就診的人數要少得多。 美國分發COVID-19疫苗的努力出現了一個大問題? 可憐的互聯網接入。 Tamra Burns Loeb, Et Al./The Conversation | 2021年2月10日|大眾科學 
 拉丁裔是美國增長最快的少數群體，但在國會的代表人數更少。 國會不可忍受的白色|Dean Obeidallah | 2015年1月8日|DAILY BEAST 
 對我的朋友來說，只有一小部分的JSwipe配對變成了對話，沒有一個變成了約會。 我在猶太Tinder上的一週|Emily Shire | 2015年1月5日|DAILY BEAST 
 少數族裔社區對警察的兩大抱怨乍一看似乎自相矛盾。 紐約警局減速的零點|巴提亞·昂加爾·薩根| 2015年1月1日|每日野獸 
 退伍軍人也只是人口中的一小部分，他們為更大的群體服務。 老兵視角:紐約警察和市政廳之間的冷戰|馬特·加拉格| 2014年12月29日|每日野獸 
 在那裡，許多少數族裔家長支持湯姆·托克拉森，他支持教育改革議程。 公共部門工會如何分化民主黨|Daniel DiSalvo | 2014年12月29日|DAILY BEAST 
 當大多數人向一個方向靠攏的時候，有一個活躍的少數人希望娜娜建立一個獨立的王國。 紅年|路易斯·特雷西 
 儘管這個黨可能只是少數忠實信徒中的一小部分，但其成員足以使這個組織繼續下去。 普特南為外行人寫的簡易法律書|阿爾伯特·西德尼·博爾斯 
 關於這一經濟狀況問題，《少數民族報告》的看法比較溫和。 英格蘭、蘇格蘭和愛爾蘭五十年的鐵路生活|約瑟夫·塔特洛 
 美國有一些偉大的英國領導人站在他們一邊，但他們絕對是少數。 神聖的遺產:弗吉尼亞的生活|多蘿西M.湯培 
 但是，那些願意讓他擁有他認為必要的一半兵力的人，只是少數。 從詹姆斯二世即位開始的英國曆史。 |托馬斯·賓頓麥考利