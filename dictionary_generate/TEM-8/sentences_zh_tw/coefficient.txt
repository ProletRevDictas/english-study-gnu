Gardam的反例使用了一個最簡單的數字系統來計算它的係數，一個只有兩個“小時”的時鐘算術。 數學家反駁80年前的代數猜想|艾麗卡·卡拉賴希| 2021年4月12日|量子雜誌 
 當你要解的線性系統有大量係數為零的變量時，它們通常也很有用。 新算法打破求解線性方程的速度限制Kevin Hartnett, 2021年3月8日，量子雜誌 
 阻力系數衡量的是汽車的空氣動力學程度，或者從本質上說，它的形狀如何影響它在空氣中移動的方式，這個數字越低越好。 這輛野生電動汽車幾乎不需要充電，因為它部分依靠陽光運行。Vanessa Bates Ramirez, 2020年12月9日 
 第一個維度是取30億個數字的一個子集，把它們加在一起，或者乘以某個係數。 縮小大數據的計算機科學家艾利森·惠滕| 2020年12月7日|量子雜誌 
 把想要變成無窮大的部分塞進係數-一個固定的數字-在和的前面。 數學“騙局”如何拯救粒子物理學|查理伍德| 2020年9月17日|量子雜誌 
 (b)同樣得到接觸30分鐘時的苯酚係數。 細菌學技術的基本原理|約翰·威廉·亨利·愛 
 當然，你想知道這個係數是如何被求出來的，以及你如何確定它是正確的。 《科學美國人副刊》第二十一卷 1886年3月6日，531號 
 我希望我已經遵守了我的諾言，並且清楚地說明了離心力系數是如何用這種簡單的方法求出來的。 《科學美國人副刊》第二十一卷 1886年3月6日，531號 
 如何找到係數，通過它可以計算出在任何情況下所施加的離心力。 《科學美國人副刊》第二十一卷 1886年3月6日，531號 
 在高溫下，電阻一般增大，但溫度係數是不規則的。 科學美國人增刊，第1082期，1896年9月26日|各種