讓加拿大來製作一部如此溫柔又有趣的電視劇吧，它讓美國人再次愛上了家庭情景喜劇。 舒適電視的新類別:當你沒有朋友和辦公室時看的16部節目 
 從1985年開始，情景喜劇《黃金女孩》(Golden Girls)連續播放了7年，片中比阿·阿瑟(Bea Arthur)飾演多蘿西(Dorothy)，她尊重與兩個成年子女之間的界限，同時每天照顧自己的母親。 是時候結束電視上貧窮的空巢媽媽的比喻了 
 從上世紀50年代的《我愛露西》到2010年代的《摩登家庭》，每集都講述了家庭情景喜劇的新十年。 萬達視覺帶來了創意可以在不斷擴張的特許經營時代存活下來的希望 
 整部劇似乎都在向伊麗莎白·奧爾森致敬，因為她的姐姐瑪麗-凱特和阿什莉·奧爾森在情景喜劇片場中長大。 萬達視覺帶來了創意可以在不斷擴張的特許經營時代存活下來的希望 
 漫威超級英雄電影宇宙的下一階段——有史以來最昂貴、最賣座的電影系列——將從一部電視情景喜劇開始。 迪士尼在很大程度上依賴於漫威非常奇怪的新劇亞當·愛潑斯坦的推出，2021年1月14日，Quartz 
 今年早些時候，考斯比正在為NBC製作一部情景喜劇，還在為Netflix製作一部單口喜劇特別節目。 世界如何轉向比爾·考斯比:一天一天的記錄|斯科特門廊| 2014年12月1日|每日野獸 
 最後我在洛杉磯拍了一部劇，HBO說他們喜歡這部劇，問我能不能把它拍成情景喜劇。 史蒂芬·麥錢特談《你好女士》電影，妮可·基德曼的客串，以及《辦公室》的遺產 
 比爾·考斯比(Bill Cosby)似乎只出現在兩種情況中:一種是深受喜愛的情景喜劇中聖潔的居家男人，另一種是墮落、墮落的惡棍。 簡訊:比爾·考斯比不是克里夫·赫克斯特布爾|蒂姆·提曼| 2014年11月20日|每日野獸 
 一個單口喜劇演員在一部自封的情景喜劇中圍繞著他對虛無的沉思? 約翰·穆蘭尼是下一個宋飛嗎? 凱文·法倫2014年10月5日 
 “一個普通的電視情景喜劇，一個百老匯演出和一個豐富的電影角色，”裡弗斯說。 瓊·裡弗斯:“死亡就像整容”蒂姆·提曼| 2014年9月4日|每日野獸 
 他們面面相覷，就像情景喜劇里約會結束時一對瞪大眼睛的孩子。 製造商|科裡·多克託羅