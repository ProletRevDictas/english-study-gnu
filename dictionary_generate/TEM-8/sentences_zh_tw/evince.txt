Annabelle餐廳的新版本——整隻小雞配上柑橘皮和溫暖的香料——也贏得了掌聲，因為同桌的人都不願意分享。 2021秋季餐飲指南，Tom Sietsema, 2021年10月6日，華盛頓郵報 
 專輯的每一部分，從合作到伴隨部分曲目的“音樂視頻”，都展現了定義科學和創作過程的實驗和探索。 口語專輯“實驗詞”編織與理由押韻|艾娜·阿貝爾| 2021年9月29日|科學新聞 
 洛克的帖子是否是贊助內容的問題表明了一個盲點，這個盲點困擾著包括YouTube、Facebook、Instagram和現在的TikTok在內的平臺。 隨著廣告和付費影響力之間的灰色地帶不斷擴大，Mozilla呼籲TikTok加強對政治視頻的控制 
 去年秋天，當人們在多倫多國際電影節(Toronto International Film Festival)上第一次看到大衛·奧伊洛(David Oyelowo)的《水人》(The Water Man)時，有些人對這位演員選擇它作為導演處女作表示驚訝。 觀眾喜歡家庭電影。 好萊塢可能正在趕上這一潮流。 Ann Hornaday | 2021年5月7日|華盛頓郵報 
 他對自己的過去也沒有表現出太多的興趣，他說:“我對自己並沒有那麼好奇。” 如何寫骯髒的戰爭|菲爾克萊| 2013年7月10日|每日野獸 
 對國營計劃的偏愛似乎表明對政策問題缺乏理解。 什麼是公共選擇? 2009年10月20日|DAILY BEAST 
 以下是《菸草植物》中菸草罐的警句，體現了許多“品味、智慧和獨創性”。 菸草; 它的歷史、品種、文化、生產和商業。 r·比林斯。 
 避免個人的言論; 這表明他們缺乏判斷力、品味、善良和禮貌。 《女士禮儀手冊和禮貌手冊》|弗洛倫斯·哈特利 
 一位熱心的評論家曾宣稱它是現代作品中最具天才的作品之一。 作為一個人和音樂家的弗雷德裡克·肖邦&弗雷德裡克·尼克 
 它們表現出一種前進的狀態，周圍的環境對它非常有利。 住在棚屋裡的印第安人亨利·r·斯庫爾克拉夫特 
 她莊嚴的誓言，她明智的重複，她的整個措詞，都表明了這種普遍的傾向。 女性聖經傳記，卷一|弗朗西斯·奧古斯都·考克斯