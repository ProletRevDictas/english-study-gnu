例如，公民權利的分離可能是出於好意，但可能導致局勢惡化，因為那些支持政治極端主義的人試圖將他們的信仰變成受保護的言論。 民主黨改革230條款的計劃如何可能適得其反 
 “那個網站的整個基礎設施從一開始就是煽動，”監測網絡極端主義的賽德情報集團(SITE Intelligence Group)執行董事麗塔·卡茨(Rita Katz)說。 唐納德的主人說出了為什麼他最終關閉了充滿仇恨的網站|克雷格·蒂姆伯格，德魯·哈維爾| 2021年2月5日|華盛頓郵報 
 他們的部分工作人員通常都是以前激進的人，他們站在打擊右翼極端主義的第一線。右翼極端主義是一個日益嚴重的威脅，受到人們的關注，但專家認為，長期以來一直被忽視。 國會暴動後，絕望的家庭求助於那些“消滅”極端分子的組織 
 絕大多數人參軍是出於正確的原因，但即使極端主義在社會上普遍抬頭，它也會在軍隊中抬頭。 美國軍隊需要打擊自身內部的極端主義。 詹姆斯·斯塔夫裡迪斯，2021年2月5日 
 換句話說，我們有證據表明，一名國會議員煽動了極端主義甚至是嗜血欲，導致了國會大廈的未遂叛亂。 共和黨的瑪喬麗·泰勒·格林問題正在失去控制，艾倫·布萊克，2021年1月27日，華盛頓郵報 
 駐莫斯科的總編輯加利娜·季姆琴科在發表了一篇有關烏克蘭的文章後，因“極端主義”被解僱。 俄羅斯最自由的網站現在在拉脫維亞|Anna Nemtsova | 2014年11月29日|DAILY BEAST 
 擊敗伊斯蘭極端主義需要硬實力和軟實力的應對。 為什麼基地組織如此強大? 華盛頓(真的)不知道|Bruce Riedel | 2014年11月9日|每日野獸 
 這些人所能做的就是牢牢地控制住公眾對宗教極端主義的厭惡。 Karen Armstrong的新規則:宗教與暴力無關|Patricia Pearson | 2014年10月29日|DAILY BEAST 
 正如阿薩夫所說，“這是拒絕極端主義的一種方式，讓人們不害怕。” 2014年10月29日，《每日野獸》 
 皮尤研究中心的數據顯示，超過五分之三的美國人非常擔心伊斯蘭極端主義的興起。 愛荷華州領跑者麥克·哈克比接受《每日野獸》採訪，勞埃德·格林2014年9月22日 
 由於這種極端主義，他的聲音註定是一個人在荒野中哭泣的聲音。 異見先知|奧托·海勒 
 我們的外交戰略是團結全世界加入到打擊極端主義的鬥爭中來。 小布什的國情諮文|小布什 
 我們還反對以德黑蘭政權為代表的極端主義勢力。 小布什的國情諮文|小布什 
 我們還在聖地反對極端主義勢力，在那裡我們有了新的希望。 小布什的國情諮文|小布什 
 正是從這種熱情和極端中，我們聽到了女人本性的一個關鍵音符——她的忠誠。 希臘女人|米切爾卡羅爾