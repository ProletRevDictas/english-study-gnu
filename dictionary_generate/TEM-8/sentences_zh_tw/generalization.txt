這是一個有風險的概括，但斯蒂爾和他的同事們指出了文獻中的一些其他線索，表明這可能是一種常見現象。 每週一次力量訓練背後的數據Alex Hutchinson, 2021年2月2日|Outside Online 
 這一假設是基於對太陽系內部演化的幾項概括。 火星上的隆隆聲燃起了地下岩漿流的希望|羅賓·喬治·安德魯斯| 2021年2月1日|量子雜誌 
 雖然在一堆客戶數據中發現和定義模式是人類的天性，也是優秀的數據科學，但過多的分類會導致寬泛的概括，可能會忽略重要的行為和觀點。 把你的活動帶到更深的層次，破壞和平|Max Braun | 2020年12月24日|搜索引擎觀察 
 在動物王國裡，關於泛化和創造力的例子可能還有很多，當然我們也是其中的一員。 人，女人，男人，相機，電視-第93期:先驅|Adithya Rajagopalan | 2020年12月2日|鸚鵡螺 
 他稱他的方案為“雙單形”表示，因為自然界的左手和右手粒子各構成一個單形——一個三角形的推廣。 所有粒子和力的新地圖|Natalie Wolchover, Samuel Velasco和Lucy read - ikkanda | 2020年10月22日|量子雜誌 
 Glassdoor可以輕鬆地根據20條評論的樣本規模對公司進行概括。 如果Cosi想要盈利，它需要增加工資|Daniel Gross | 2013年8月22日|DAILY BEAST 
 這不僅僅是一種心理學上的概括，而是一種存在主義的觀點。 為什麼偏袒是美德:反對公平的案例|Stephen T. Asma | 2012年12月7日|DAILY BEAST 
 之後有人來找我，跟我說，‘哦，我不同意這種說法，’‘我認為那是一種泛化。 《好漢兩個半》聯合主創Lee Aronsohn的女性喜劇抨擊|Tricia Romano | 2012年4月4日|DAILY BEAST 
 坦率地說，你認為15歲的孩子不可能像我這樣寫作的概括(眾多推論之一)是錯誤的。 邁克爾·沃爾夫是孩子丹尼爾·朱特| 2010年6月22日|每日野獸 
 “當人們說印刷業已經消亡時，我很難過，因為這是對事物現狀的不公平概括，”他說。 MTV《好人先生》|伊妮德·葡萄牙| 2010年4月26日|每日野獸 
 E. Mitior的病例數量非常有限，無法進一步推廣。 癲癇的性質和治療的統計調查|亞歷山大·休斯·貝內特 
 過分強調細微的細節而忽視概括的藝術是不行的。 歷史的燈塔之光，第一卷|約翰·洛德 
 但正是這個悖論引出了關於數的性質的推廣的真正原理。 邏輯系統:推理與歸納|約翰·斯圖亞特·密爾 
 按照我們的分佈，前者屬於泛化謬誤，後者屬於推理謬誤。 邏輯系統:推理與歸納|約翰·斯圖亞特·密爾 
 最容易從歷史中概括出來的經驗法則並不等於這個。 邏輯系統:推理與歸納|約翰·斯圖亞特·密爾