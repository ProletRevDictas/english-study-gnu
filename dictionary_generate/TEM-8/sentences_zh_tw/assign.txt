它的設計目的是收聽有多個與會者的會議，分析討論模式，生成信息豐富的摘要，並分配會後的行動項目。 為追求普遍問題的務實解決方案Francesca Fanshawe | 2021年3月31日|MIT技術評論 
 這種隨機的雙盲對照試驗隨機分配患者接受一種藥物或一種安慰劑，不向參與者或醫生透露誰接受哪種藥物。 關節炎藥物託珠單抗似乎無助於抗擊COVID-19 
 這種人工智能可以仔細研究宇航員的症狀，然後建議進行醫療測試，做出診斷並安排治療。 在火星任務中存活下來需要規劃和大量創新瑪麗亞·特明| 2020年10月22日|學生科學新聞 
 所以我的工作不僅僅是清潔，而是在美國航空公司的交通控制中心擔任客艙服務的調度人員，為每一架來港的飛機分配清潔人員。 我從5年的午夜清潔飛機中學到的|matthewheimer | 2020年8月30日|財富 
 理想情況下，火星飛船應該配備人工智能，可以考慮宇航員的症狀，建議醫療測試，做出診斷並安排治療。 宇航員在危險的火星之旅中需要什麼? 2020年7月15日|科學新聞 
 現在克里姆林宮將指派更多忠誠的人來統治該地區，其中大部分是軍事領導人。 衰退? 貶值? 通貨膨脹? 普京告訴俄羅斯堅持到底。 Anna Nemtsova | 2014年12月4日|DAILY BEAST 
 當我們賦予另一個個人或社會群體一種原始的“不是我”的地位時，它會——也確實會——把我們帶向一條破壞性的道路。 弗格森，移民和“我們Vs.他們”，2014年11月27日 
 其他人則可以辯論並指責“誰輸了伊拉克”。 伊朗是伊拉克最大的輸家阿基·佩裡茨| 2014年6月15日|每日野獸 
 芮妮·理查森知道，她可能永遠無法為兒子的死負責——她已經為此抗爭過了。 軍事基地槍擊事件揭示了心理健康辯論|凱特琳·迪克森| 2014年2月9日|每日野獸 
 女孩們被引導閱讀了幾頁，直到她們被要求給男孩分配一系列預先確定的形容詞。 來看看Lulu:一款讓女孩匿名給男孩打分的應用|Isabel Wilkinson | 2013年8月22日|DAILY BEAST 
 俄羅斯的計劃早已眾所周知; 但是，新的印刷藝術的實踐可能賦予它們新的特徵。 布萊克伍德愛丁堡雜誌，No。 CCCXXXIX。 1844年1月。 卷,LV。 |各種 
 約翰·史密斯，他們中最好的農民，帶著多麼誠實的驕傲走到前面，給每個人分配了自己的位置! Skyrie的桃樂茜|伊芙琳·雷蒙德 
 如果承租人死亡，他的遺囑執行人或管理人可以轉讓他的剩餘期限。 普特南為外行人寫的簡易法律書|阿爾伯特·西德尼·博爾斯 
 如承租人在未被禁止的情況下可轉讓或分租，出租人也可放棄其在出租房屋中的權益。 普特南為外行人寫的簡易法律書|阿爾伯特·西德尼·博爾斯 
 如果有人請你吃你不想吃的菜，你可以謝絕，但不要說明任何理由。 《女士禮儀手冊和禮貌手冊》|弗洛倫斯·哈特利