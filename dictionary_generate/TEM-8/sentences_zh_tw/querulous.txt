17世紀歐洲印製的那些充滿抱怨、相互聯繫的小冊子，預示著現代博客文化的發展。 社交媒體如此古老，連羅馬人都有|尼克·羅密歐| 2013年10月25日|每日野獸 
 在馬爾斯看來，卡拉索的重建是一篇極具野心、古怪、牢騷滿腹、抒情而最終具有說服力的文章。 Peter Stothard | 2010年4月7日|每日野獸 
 當虛弱、愛抱怨的母親去世時，羅馬裡諾才15歲。 更糟糕的是亞歷山大·蘭格·基蘭德 
 但是，儘管波特蘭是一個不講道理、愛抱怨的朋友，他卻是一個最忠實、最熱心的牧師。 從詹姆斯二世即位開始的英國曆史。 |托馬斯·賓頓麥考利 
 這就是擴大那些愛發牢騷和受苦受難的人的選舉權。 布萊克伍德的《愛丁堡雜誌》，第67卷，第414期，1850年4月 
 他身上的人類的甜蜜已經乾涸了一半，一種對他來說是那麼新鮮和陌生的厭世情緒，使他愛抱怨，愛挑剔。 《馬茲尼的一生》|波頓王 
 女管家比茲利太太因患風溼病和年事已高，變得遲鈍而愛發牢騷。 牛頓·福斯特，弗雷德裡克·馬里亞特上尉