他們在一次採訪中說，在她提出指控後，獄警和獄友一直對她說恐嚇的話，讓她感到不安全。 移民和海關執法局驅逐了一名指控獄警性侵的女子，而聯邦調查局仍在調查此事|羅米·克里爾| 2020年9月15日|ProPublica 
 在過去的6個月裡，埃克內利戈達的妻子桑婭(Sandya)說，她認為這起案件的目擊者受到了恐嚇，對她的威脅和對她家人的監視都有所增加。 這10名記者失蹤了，而COVID正在妨礙調查 
 讓移民感到不受歡迎，讓他們感到害怕，讓他們受到恐嚇，讓他們無法瞭解和行使自己的權利，包括投票權。 美國新公民是增長最快的投票群體之一。 但今年不是。 郭愛玲| 2020年8月31日|FiveThirtyEight 
 例如，警察不能恐嚇證人，不能在調查過程中撒謊，也不能參加“有瀆職行為模式”的執法團伙。 薩克拉門託報道:瓊斯、COVID-19和遠程投票的諷刺|薩拉·利比和傑西·馬克思| 2020年8月28日|聖地亞哥之聲 
 他們很容易被嚇倒，因為害怕報復，害怕失去工作，害怕沒有人會聽或相信他們而保持沉默。 臨時工對所謂的性騷擾進行了反擊，並表示他們會因此受到報復 
 如今，政客和首席執行官們經常用這種顏色來贏得尊重和恐嚇。 猩紅是新的黑色|Raquel Laneri | 2014年8月31日|DAILY BEAST 
 當ISIS斬首一名美國記者時，它的意圖是恐嚇和挑釁美國。 奧巴馬vs ISIS:這一次是私人恩怨，2014年8月22日，每日野獸 
 一位在茲瓦拉的飛行員朋友指出，只要“兩架阿帕奇”武裝直升機就能迫使民兵停火。 不是美國讓利比亞變成了災難，而是今天，Ann Marlowe | 2014年8月3日|每日野獸 
 週日，易洛魁族後衛在下半場的高潮中利用他們恐嚇和猛擊加拿大人。 在發明這項運動一千年後，易洛魁人是長曲棍球的新超級大國 
 你知道，你有一個政府用它的工具恐嚇人民。 保守黨的黑色希望|Evan Gahr | 2014年5月19日|DAILY BEAST 
 有一次，一個著名的職業決鬥者認為他可以恐嚇他。 拿破崙的那幾個小元帥| R。 p . Dunn-Pattison 
 如果這些人影去找敵人的首領，他們可能會嚇到他——到這裡來，他們會嚇到我。 加里波利日記第二卷|伊恩·漢密爾頓 
 為了恐嚇他們，鄧莫爾發表了一些宣言，並威脅要解放奴隸反抗他們的主人。 《美國的保皇黨和他們的時代》，卷一，2 |埃格頓·瑞爾森 
 她有一種微弱的希望，希望這個房間能嚇住這個西方女孩，但她看到的不是恐嚇，而是狂喜。 蝴蝶屋|瑪麗e威爾金斯弗里曼 
 他們試圖恐嚇他——讓他退縮; 每個人都在敦促對方立即採取個人暴力行動。 南北|伊麗莎白·克萊霍恩·蓋斯凱爾