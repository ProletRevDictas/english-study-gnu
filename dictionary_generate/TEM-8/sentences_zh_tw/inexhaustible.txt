就像納博科夫的語言一樣，這部作品的物質內容，其幾乎無窮無盡的美，給人以巨大的愉悅。 看Jasper Johns:一個開創性的藝術家的職業生涯在兩個城市被慶祝和照亮|Sebastian Smee | 2021年9月29日|華盛頓郵報 
 看似取之不盡、用之不竭的時間資源終於用完了。 美國在阿富汗的時間已經到了。 我們對他們人民的虧欠是沒有期限的|Brian Castner | 2021年9月9日|時間 
 在這一交易中流動的鉅額資金是一種腐敗力量，它吸引了似乎源源不斷的參與者，不管警察關了多少人。 精英巴爾的摩警察變成了罪犯邁克爾·弗萊徹| 2021年3月26日|華盛頓郵報 
 事實上，我是影片中所記錄的無盡慷慨的幸運接受者之一。 我的朋友，羅傑·埃伯特:普利策獎得主湯姆·謝爾斯的感人紀錄片《生命本身 
 有經驗的飛行員並不是用之不竭的。 370航班只是一個開始:為什麼亞洲沒有做好航空安全|Clive Irving | 2014年3月13日|每日野獸 
 這是一本取之不盡用之不竭的書，任何對這種形式感興趣的人都必須擁有。 Phillip Lopate的書包:作文傳統|Phillip Lopate | 2013年2月5日|DAILY BEAST 
 她的魯莽、創造性和用之不竭的精力使她走完了剩下的路。 埃爾莎·麥克斯韋，立王者|馬克·布勞德| 2012年11月1日|每日野獸 
 什麼樣的比喻，什麼樣的公式，可以描述那些年的無窮無盡的運動? 波普藝術的斯文加利|安妮·科恩-索拉| 2010年5月13日|每日野獸 
 有一天，當阿里斯蒂德正在談論女人這個無窮無盡的話題時，我把他拉了上來。 阿里斯蒂德·普約爾的歡樂冒險|威廉·j·洛克 
 他的罵人話千變萬化，活靈活現，說不完，渾濁的溪水很容易流。 英格蘭、蘇格蘭和愛爾蘭五十年的鐵路生活|約瑟夫·塔特洛 
 鮑比正在展開一張摺疊的紙，那是她從她那用不完的口袋裡掏出的。 舞臺上的中央高中女孩|格特魯德·w·莫里森 
 每次經過巴西的小樹林時，我都能看到新的花朵和植物，以及似乎取之不盡用之不竭的豐富植被。 巴西航海日記|瑪麗亞·格雷厄姆 
 土地是我們的——那裡有取之不盡用之不竭的資源; 讓我們去佔有它吧。 《美國有色人種的狀況、地位、移民和命運》馬丁·r·德拉尼