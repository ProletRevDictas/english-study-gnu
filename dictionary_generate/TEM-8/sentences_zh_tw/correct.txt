提交兩個問題中的任何一個的正確答案，你都有可能在下一個專欄中獲得大喊答題機會。 你能像老闆一樣穿過馬路嗎? 扎克·威斯納-格羅斯，2021年2月12日| 
 你手機上的自動更正功能或Gmail組合框都是機器學習技術的完美例子。 在數字營銷中使用機器學習的五種方法|Birbahadur Kathayat | 2021年2月12日|搜索引擎觀察 
 無論視頻是完全從某個來源獲得的，還是通過社交媒體偶然發現的，都必須採取正確的步驟，以確保視頻未被修改，並在正確的背景下呈現。 這是事實核查員納丁·阿賈卡、艾麗斯·塞繆爾斯、莎拉·卡蘭於2021年2月11日在《華盛頓郵報》發佈的項目 
 如果它知道所有其他玩具的名字，它可能會選擇正確的玩具，因為它猜不熟悉的單詞一定是不熟悉的玩具。 你的狗真的聰明嗎? 取決於它的內存。 |作者Jan Hoole/The Conversation | 2021年2月8日|大眾科學 
 他們敦促用戶更新他們的硬件和軟件，以避免補丁可以輕鬆糾正的安全問題。 你應該儘快更新你的iPhone和Chrome瀏覽器，Stan Horaczek, 2021年2月8日，| 
 由於空氣稀薄，正確和不正確的空速之間的差距非常小，只有50英里每小時。 惡劣天氣導致亞航8501墜毀? 克萊夫·歐文2014年12月29日|DAILY BEAST 
 保守派要求“放松管制”是行不通的，因為自由派認為大多數美國人需要清潔的水是正確的。 繁文縟節扼殺了好心人|菲利普·k·霍華德| 2014年12月27日|每日野獸 
 參議院的報告為許多需要糾正的問題提供了充足的證據。 為什麼我們在9/11之後恐慌，忽視了我們所知道的應對安全威脅的一切? Deborah Pearlstein | 2014年12月18日|DAILY BEAST 
 Breitbart迫使她更正了一小部分故事，但這樣的政治迫害會讓每個受害者畏縮不前。 右翼的強姦噴子vs莉娜·鄧漢姆|艾米麗·夏爾| 2014年12月10日|每日野獸 
 事實上，在連續20場比賽中，她按下鍵盤的時候，有92%的答對率。 冒險! 冠軍茱莉亞·柯林斯的大腦感覺像Mush |Sujay Kumar | 2014年11月20日|DAILY BEAST 
 對於電報來說，拼寫並不重要; 辦公室裡的人會糾正你的錯誤——如果他們不糾正，你可以推給他們。 信心|亨利·詹姆斯 
 我非常驚訝地發現，我有多少小的醜陋的習慣，我都沒有意識到我必須改正。 音樂-德國留學|艾米·費伊 
 這絕不是加爾納什的意思; 不過，既然這是他的心裡話，他就不去糾正馬呂斯了。 聖馬丁之夏，拉斐爾·薩巴蒂尼 
 對於感知能力較好的兒童來說，向正確的側面視圖的過渡可能要走得更遠。 兒童之路|詹姆斯·薩利 
 1881年，這些人物的外觀被塗上了正確的顏色。 《肖威爾伯明翰詞典》|托馬斯·t·哈曼和沃爾特·肖威爾