參數H衡量的是它們在保持最小接觸的情況下分散的程度，而它的倒數，內部程度，則衡量的是它們擠在一起的程度。 慶祝約翰·霍頓·康威的俏皮魔法|普拉迪普·穆塔利克| 2020年10月15日|量子雜誌 
 獵人和獵物之間的關係可以代表一種互惠的關係，充滿了心理意義和精神重量。 切斷獵人-獵物關係的精神代價——如此浪漫的事實|威廉·巴克納| 2020年10月14日|鸚鵡螺 
 所以你所要做的就是找到一個完美的戰爭遊戲的概率，然後計算這個數字的倒數。 第五次謎語族之戰|扎克·威斯納·格羅斯| 2020年9月4日|FiveThirtyEight 
 這些文本的主題從食物，到電影，到書籍，到家庭，所有這些都是對話和積極的，是相互的，我相信，是相互的。 阿拉斯加司法部長向年輕同事發送數百條“不舒服的”短信後無薪休假，安克雷奇每日新聞，2020年8月25日 
 北京方面可能通過對蘋果實施對等制裁進行報復。 華為說，美國無法“碾壓”它。 特朗普正在餓死它，而不是2020年8月20日 
 他的訪問還包括回敬邀請，教皇計劃在5月底進行後續邀請。 2014年3月26日，奧巴馬訪問羅馬，希望利用教皇方濟各的聲望 
 有時我做得少，有時我做得多，但總是相互尊重。 肯塔基州最優秀的反英雄:Walton Goggins在《正義團》的變色龍反派|Allen Barra | 2014年2月11日|DAILY BEAST 
 歡欣鼓舞的一個原因是，臨時協議把我們和他們的強硬派聚集在一起，相互沮喪。 美國毛拉的神話|James Kirchick | 2013年12月14日|每日野獸 
 安全網植根於人類互惠交換的本能。 當工作消失|Megan McArdle | 2013年6月14日|DAILY BEAST 
 不要再和以色列抱得更緊而不尋求相應的行動。 歐洲與消失的兩國選項|尼克·威特尼，丹尼爾·列維| 2013年5月9日|每日野獸 
 從前僕人和主人之間的相互聯繫很牢固，現在卻完全消失了。 《女士禮儀手冊和禮貌手冊》|弗洛倫斯·哈特利 
 在他的簡單信條中，如果一個女孩接受了一個男人，讓他親吻她，戴上他的戒指，這就是一種相互的愛情。 《神奇插曲》瑪麗·羅伯茨·萊因哈特 
 他們的思想和品味驚人地一致，相互的同情和相互的尊重，很快使他們成為了朋友。 二氧化鈾|卡米爾·弗拉馬利翁出版社 
 圖69 b為外力的多邊形，69 c為倒數的一半。 大英百科全書，第11版，第4卷，第3部分|各種 
 當相互的愛在我的血管裡燃燒時，我的嘴唇將變成紫色，我的吻將是火焰! 《朝聖者的貝殼》或者《採石人費根》歐格尼·蘇