當我有能力感到憤怒或沮喪時，我就會時不時地來來回回。 全球氣候峰會的麻煩|abarronian | 2021年11月17日|Outside Online 
 即使是被自己的選擇束縛住的游擊隊員，也可能對他們所目睹的一切感到沮喪。 選舉直播更新:辯論委員會表示將改變結構，以確保更“有序的討論”|約翰·瓦格納，費莉西亞·桑梅茲，艾米·B·王| 2020年9月30日|華盛頓郵報 
 僅僅在兩年前，由於糟糕的經濟數據和令人沮喪的反對黨，他還很得意。 共和黨現在需要的是一場良好的內部鬥爭|James Poulos | 2014年11月6日|每日野獸 
 鏡頭對準美國球迷的鏡頭顯示了他們沮喪的面容，緊蹙的眉頭。 把它! 美國隊進入第二輪，2014年6月26日，Tunku Varadarajan 
 當被冷落的球隊的球迷會憤怒，或沮喪，或兩者兼而有之時，威爾曼將在錦標賽的選擇之後崩潰。 2014年3月16日，Matt Gallagher，每日野獸 
 這樣的努力重新激發了一場直到最近都似乎萎靡不振的運動。 政治平等的動力幫助女性獲勝|萊斯利·貝內特| 2012年1月19日|每日野獸 
 這是民主黨基層有多沮喪的一個衡量標準，因為他們的成員不確定奧巴馬是否有這麼多潛力。 奧巴馬終於準備好咆哮|埃裡克·阿爾特曼| 2010年9月8日|每日野獸 
 他表現得垂頭喪氣，心灰意冷，如果他能說話，就會問這一切是什麼意思。 《歐扎克的信使》拜倫·a·鄧恩 
 這時，拉比克懷著恐懼、焦慮和不信任的心情，慢慢地跟著高貝爾先生走進了客店。 聖馬丁之夏，拉斐爾·薩巴蒂尼 
 現在他顯然已被辛勞弄得精疲力竭，又因失望而精神萎靡。 布萊克伍德愛丁堡雜誌，No。 CCCXXXIX。 1844年1月。 卷,LV。 |各種 
 我簡直說不出我為什麼要寫這張語無倫次的便條; 只是我很沮喪，很想和你說話。 獨自|馬里昂哈蘭 
 對一個迷途的流浪者，尤其是對一個垂頭喪氣的女人來說，這樣的偉大不是崇高，而是可怕的。 陸上|約翰威廉德森林