像康威和沃爾伯特一樣，他把自己描述性的名字放在了作品的標題中，而不僅僅是正文。 為什麼數學家應該停止用彼此的名字命名事物-第89期:陰暗面|勞拉·波爾| 2020年9月2日|鸚鵡螺號 
 每個領域都有藝術術語，但當這些術語是描述性的，它們更容易記憶。 為什麼數學家應該停止用彼此的名字命名事物-第89期:陰暗面|勞拉·波爾| 2020年9月2日|鸚鵡螺號 
 歐幾里得的《幾何原本》中充滿了常見的描述性名稱，儘管他借鑑了許多不同的人的發現。 為什麼數學家應該停止用彼此的名字命名事物-第89期:陰暗面|勞拉·波爾| 2020年9月2日|鸚鵡螺號 
 到目前為止，描述性的alt文本應該是所有內容團隊的最佳實踐。 五種SEO內容類型的力量和增長您的業務到2020年|Jim Yu | 2020年6月17日|搜索引擎觀察 
 他們中的許多人——我認為這要麼是誤導，要麼不是很有描述性——稱之為“旁觀者效應”，但這並沒有告訴你旁觀者效應是增加還是減少報告。 5個你可能誤用的心理學術語(第334頁重播)|Stephen J. Dubner | 2020年1月9日|魔鬼經濟學 
 她避免了詳盡的描述，因為她反對基於某些小說的缺陷來譴責所有的小說。 小說的誕生|尼克·羅密歐| 2014年11月27日|每日野獸 
 當你把它寄出去或帶回來的時候，你要加入描述性的文字，讓消費者看到你的作品。 模糊的界限在紐約素描本博物館|丹尼爾·吉尼斯| 2014年11月1日|每日野獸 
 所以我們都應該把“職業母親”作為一個描述性形容詞來表示。 新右翼偶像:職業媽媽蒂姆·提曼| 2014年7月16日|每日野獸 
 它的名字是描述性的:它們是伽馬射線的極強烈爆發，伽馬射線是光的最高能量形式。 Matthew R. Francis | 2014年6月1日|每日野獸 
 它是描述性的，規範性的，明晰性的。 馬來西亞為何隱瞞MH370報告? 克萊夫·歐文2014年4月28日 
 他模仿泰奧弗拉斯的《人物》是一部公認的傑出作品，描繪了那個時代的禮儀。 《每日曆史與年表》|喬爾·蒙塞爾 
 阿明太太看著他，想起了那個把他和盧克索所有人區別開來的描述性短語。 貝拉唐娜|羅伯特希肯斯 
 其中一幅描繪洪水氾濫前歷史的畫是拉麥用弓箭射該隱。 註釋與查詢，178號，1853年3月26日|各種各樣 
 這些事情超出了我的知識範圍，用無知來形容也許更貼切。 羅伯特·路易斯·史蒂文森的作品-斯旺斯頓版第25卷(25分)|羅伯特·路易斯·史蒂文森 
 順便說一句——因為他確實給《每日公報》寄了描述性的文章——他並不是出於公事而出去的。 威廉·j·洛克