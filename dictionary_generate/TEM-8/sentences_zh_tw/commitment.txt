這就是為什麼在數據保護法中公開承諾政府是如此重要。 播客:COVID-19正在幫助巴西變成一個監控國家|安東尼·格林| 2020年9月16日MIT技術評論| 
 斯卡拉內轉而關注他對參議院超級多數規則的支持，並質疑他支持自由派法官的承諾。 預告片:第一個州走到最後|大衛·韋格| 2020年9月15日|華盛頓郵報 
 我們需要做的是，將企業對社會承擔更廣泛責任的承諾與支持這種責任的權力結構相匹配。 50年後，米爾頓·弗裡德曼的股東主義已不復存在 
 我們讚揚所有HEI參與者致力於為所有人提供包容性護理。 Chase Brexton在HRC醫療保健平等指數中獲得認可|Philip Van Slooten | 2020年9月11日|Washington Blade 
 我們已經從旅行中解脫出來，簽下了一群令人印象深刻的ceo，包括萬豪的阿恩·索倫森、Land O 'Lakes的貝絲·福特、塔吉特的布萊恩·康奈爾、Guadian Life的迪安娜·穆里根和通用電氣的拉里·卡爾普。 亞當·拉辛斯基| 2020年9月10日|《財富 
 但是，馬里奧·科莫為公共生活帶來的品質——同情、正直、堅守原則——在今天仍然稀缺。 科莫總統會是一頭獅子|喬納森·奧特| 2015年1月2日|每日野獸 
 婚姻是一種契約，是一種承諾——嫁給自己是荒謬的，因為你已經嫁給了自己。 為什麼單身人士應該對自婚運動說“我不”|蒂姆·蒂曼| 2014年12月30日|每日野獸 
 我們需要一個真正的承諾來製作這部電影，以便讓它繼續下去。 索尼的“菠蘿快遞2戲劇”內部:洩露的電子郵件揭示了關於斯通納喜劇續集的爭鬥威廉·Boot 2014年12月21日 
 這是她正在努力的一件私人事情，看到她的投入程度，我也感到非常投入。 安吉麗娜·朱莉的新繆斯:傑克·奧康奈爾的崛起，二戰史詩《堅不可摧》的主演Marlow Stern | 2014年12月10日 
 “看到他做出這樣的承諾，以及他想確保自己的孩子接受洗禮的事實，就足以說明問題，”李說。 老邁克爾·布朗的洗禮和弗格森的火的洗禮Justin glwe | 2014年11月27日|每日野獸 
 他向我詳細地敘述了考文垂的事情，以及治安官們對紐蓋特的承諾。 愛德華·吉本的私人信件(1753-1794)第一卷(2卷)|愛德華·吉本 
 我還在我的小牢房裡等待著什麼到來，還沒有聽到太多關於承諾的消息，但我希望它會及時到來。 監獄與祈禱:還是愛的勞動|伊麗莎白·萊德·惠頓 
 據報告，他在整個旅程和履行承諾時表現得非常冷靜和得體。 《犯罪編年史》或者《新紐蓋特日曆》 v. 1/2 |Camden Pelham 
 於是我又被關進監裡，在那裡被關了六個月，直到我的服刑期滿。 喬治·福克斯|喬治·福克斯 
 對另一個人承諾是一種勇敢的行為，難怪我們有時會退縮。 這是愛，魯爾·l·豪