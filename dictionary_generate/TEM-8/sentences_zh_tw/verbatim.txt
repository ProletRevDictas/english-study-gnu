真理部的職員負責把老大哥的壞話儘可能地寫下來，一字不差地抄下他嘴裡說出來的話，這對溫斯頓似乎不公平。 以下是根據我對“奧威爾式”用法的瞭解，我認為在奧威爾的書中發生了什麼 
 這是我希望人們能夠完全瞭解的電影之一。 “情人搖滾”——史蒂夫·麥奎因向雷鬼致敬的音樂背後的故事 
 至少有三份聯邦文件，包括一份2019年的報告，幾乎一字不漏地重複了項目申請中關於參觀、創造就業和創收的聲明。 《麋鹿、遊客和煤炭地區失蹤的工作》作者:R.G.鄧洛普，肯塔基州調查報道中心|，2020年10月22日| 
 你可能想要犧牲流暢的可讀性，以獲得逐字逐句的長尾關鍵詞。 五個優秀的建議優化搜索引擎必應-谷歌|Justin Staples | 2020年10月16日|搜索引擎觀察 
 卡洪河谷聯盟的官員在把一些學生帶回校園後，幾乎一字不差地告訴了我同樣的事情。 學習曲線:學校現在是一種解毒劑，很少有家庭可以進入|Will Huntsberry, 2020年9月24日|聖地亞哥之聲 
 我有可靠的消息，這些引用即使不是一字不差，也是百分之百準確。 忘記的決議; 嘗試一些宣言|凱文·布萊耶| 2015年1月1日|每日野獸 
 但最重要的是，他喜歡蒙提·派森劇團，經常一字不差地重複他們滑稽的小品。 當加里·賴特遇到喬治·哈里森:織夢人，約翰和洋子，和更多|加里·賴特| 2014年9月29日 
 他把它們塞進口袋，後來幾乎一字不差地複製到他的小說裡。 《美國夢，1933:寂寞小姐》作者:納撒尼爾·韋斯特2013年4月29日 
 這些和其他著名的革命修辭作品幾乎一字不差地重複了劇中的臺詞。 誰是真正的卡託? David Frum | 2012年12月20日|DAILY BEAST 
 看看奧巴馬在這一點上的破壞性廣告，普通民眾逐字逐句地閱讀羅姆尼的解釋。 關於米特稅收的新興理論:2009年|邁克爾·托馬斯基| 2012年7月18日|每日野獸 
 這些談話我幾乎可以一字不差地複述出來，因為根據我一貫的習慣，我把他所說的一切都記了下來。 三個約翰·沉默的故事|阿爾傑農·布萊克伍德 
 他的主要嚮導是凱撒大帝，他經常一字不差地引用愷撒的話。 斯特拉波的地理，卷三|斯特拉波 
 他(斯托普福德)一走，就在前廳裡把它寫了下來，我可以把它看作是一字不差的。 加里波利日記第二卷|伊恩·漢密爾頓 
 他的社論是《信使報》的領袖，逐字逐句地發表。 《印第安納州紀事報》梅雷迪思·尼克爾森 
 德魯裡不能聲稱自己逐字記錄了沃恩教授的講話，但他盡力提供了內容。 Etidorhpa或地球的盡頭。 |約翰Uri勞埃德