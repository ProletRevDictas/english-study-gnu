我們看到，這種自由根本不是免費的——有時，它的到來要付出危險的代價。 麗貝卡·霍爾的去世是一個關於種族身份的複雜、感人的故事，也是聖丹斯電影節的傑出作品 
 因為很多這樣的科技公司都沒有工會，大多數領導或參與抗議的工人都不是勞工活動人士，而是對自己所處的危險處境做出反應的工人。 COVID-19帶來的挑戰迫使許多工人罷工。 勞工運動的利益會持續嗎? 阿比蓋爾·艾布拉姆斯，2021年1月17日 
 以下是一些虛構的遊戲模式，讓獵鬼變得更加危險和荒謬。 你是“相位恐懼症”專家嗎? 這裡有一些保持恐懼新鮮感的替代規則。 Elise Favis | 2021年1月11日|華盛頓郵報 
 兩人都很瞭解自己的球員，並在危機時刻保持樂觀。 皮特·卡羅爾知道7勝9負可能是一件大事的開始，Les Carpenter, 2020年12月18日，華盛頓郵報 
 司法部正在起訴谷歌，但政府有權監督接受審判的大型科技公司。谷歌的訴訟發生在帕克斯頓尤其危險的時候。 德克薩斯州領導共和黨總檢察長對谷歌發起新的反壟斷訴訟，目標是其廣告帝國託尼·羅姆，2020年12月16日 
 諷刺作家處在一個危險的位置——把教條和虛偽串在一起，在需要它的保護的同時與當權派對抗。 哈利·希勒談《危險的諷刺生意》|勞埃德·格羅夫| 2015年1月8日|每日野獸 
 這是一條棘手而危險的道路，但沒有現實的選擇。 打敗ISIS只有一個辦法:與阿薩德和伊朗合作|Leslie H. Gelb | 2014年10月18日|每日野獸 
 但是，如果“我們的地中海”結束了，這可能是移民們進行危險跨越的悲慘的一天。 歐洲救援人員正在引誘移民走向死亡? 芭比·拉扎·納多2014年9月7日 
 不幸的是，這可能會傳遞出一個善意但危險的信息。 “精靈，你自由了”:自殺不是解放羅素·桑德斯| 2014年8月12日|每日野獸 
 《被迫害者》一書為現代美國日益被剝奪公民權的基督徒所面臨的危險處境提供了一面鏡子。 “被迫害”是基督教右翼偏執的夢|坎迪達·莫斯| 2014年7月22日|每日野獸 
 這條路是為了運木材而修的，有六英里的路都是彎彎曲曲的。 雷蒙娜，海倫·亨特·傑克遜 
 我認為，這反而是哲學攻擊的更危險、更清楚和更好的對象。 註釋與查詢，177號，1853年3月19日|各種各樣 
 在他們危險的旅途中，麻疹的發作增加了他們的不適。 理查德·特里維西克的生平，卷二(2)|弗朗西斯·特里維西克 
 此外，考慮到漫長而危險的航程，當我們結帳時，船員們將得到獎金。 更糟糕的是亞歷山大·蘭格·基蘭德 
 他只能在一年中大部分時間裡航海是危險和困難的緯度上才能進入海洋。 從詹姆斯二世即位開始的英國曆史。 |托馬斯·賓頓麥考利