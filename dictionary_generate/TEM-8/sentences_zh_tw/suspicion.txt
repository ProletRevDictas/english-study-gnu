反疫苗運動的基礎是對權威的懷疑，首先是對醫療權威的懷疑。 偽科學家是如何逍遙法外的——如此浪漫的事實Stuart Firestein | 2020年8月28日|鸚鵡螺 
 對該項目的一些更普遍的反對意見可能更多地與對政府和營利性企業的懷疑有關，而不是與蚊子的生物學有關。 轉基因蚊子已獲准在美國進行首次試飛。蘇珊·米利烏斯，科學新聞，2020年8月22日 
 第二天，又有兩人因為同樣的嫌疑被毆打，政府關閉了國家互聯網，以阻止謠言的傳播。 2020年8月19日，MIT科技評論| 
 事實上，儘管算法的寫作模式有些奇怪，偶爾會出現錯誤，但在他在Hacker News上的第一篇文章下發表評論的幾十人中，只有三、四人懷疑這篇文章可能是由算法生成的。 一個大學生偽造的人工智能博客愚弄了成千上萬人。 這就是他的成功之道。 2020年8月14日，MIT科技評論| 
 麥迪遜-威斯康星大學(University of Madison-Wisconsin)經濟學家Menzie Chinn表示，7月就業報告只是證實了他對經濟復甦開始趨於穩定的懷疑。 經濟學家擔心沒有更多的失業援助會發生什麼|Amelia Thomson-DeVeaux | 2020年8月11日|FiveThirtyEight 
 喬爾神父Román薩拉查2013年死於車禍; 他的死亡被裁定為意外事故，但謀殺的嫌疑仍然存在。 墨西哥的牧師被標記為謀殺|傑森·麥加漢| 2015年1月7日|每日野獸 
 那些在國外服役的人被懷疑受到了歐洲外交的影響。 美國大使館一直是待售的|威廉·奧康納| 2015年1月2日|每日野獸 
 在羅姆尼的世界裡，超級政治行動委員會受到了真正的懷疑和懷疑。 “為羅姆尼做好準備”是業餘時間|Tim Mak | 2014年12月23日|DAILY BEAST 
 塞納可能覺得自己遠離了懷疑，部分原因是他與當地警方的友好關係。 讓孩子和狗做愛的父親|約翰·l·史密斯| 2014年12月16日|每日野獸 
 人們的懷疑集中在與努斯拉陣線關係密切的卡塔爾政府上。 一名26歲女子是ISIS最後一名美國人質沙恩·哈里斯2014年11月17日|每日野獸 
 任何人都可以擁有悲劇演員的肖像而不會引起懷疑或評論。 《覺醒與短篇小說集》|凱特·肖邦 
 他把這一系列都背下來了，絲毫沒有懷疑自己是在背。 同化記憶|馬庫斯·德懷特·拉羅(又名A. Loisette教授) 
 希爾達急不可耐地轉過頭去; 他們的目光相遇了片刻，帶著懷疑、挑戰和敵意。 希爾達·萊斯韋，阿諾德·貝內特 
 她望著他，心裡突然產生了一種懷疑——懷疑他懷疑他們。 聖馬丁之夏，拉斐爾·薩巴蒂尼 
 懷疑、猜疑、憤怒模糊了視線; 痛苦打破了客觀的觀念。 阿爾傑農·布萊克伍德