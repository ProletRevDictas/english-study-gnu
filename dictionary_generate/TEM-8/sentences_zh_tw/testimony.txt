該案的一名關鍵證人提供的證詞被專家認定為虛假的。 特朗普呼籲恢復法律與秩序，芝加哥最高檢察官能否戰勝對犯罪軟弱的指控? |作者:米克·杜姆克| 2020年9月4日| 
 一些人質疑，為什麼有精神健康問題的患者很少能得到打折的通行證，儘管他們的醫生提供了證明。 MTS經常否決醫生為殘疾人降低票價的命令Lisa Halverstadt 2020年8月31日|聖地亞哥之聲 
 他在證詞中指出，他所研究的模型在25%的情況下對一個人的種族的假設是錯誤的。 解釋:政治數據庫對你瞭解多少? 2020年8月31日，MIT科技評論| 
 美國郵政拒絕置評，並讓《財富》雜誌參考德喬伊的證詞。 美國郵政的延誤威脅到婦女獲得避孕措施ehinchliffe 
 這一決定是在8月18日的一場虛擬公開聽證會上進行了大約兩個小時的有爭議的證詞之後做出的。 轉基因蚊子已獲准在美國進行首次試飛。蘇珊·米利烏斯，科學新聞，2020年8月22日 
 我們什麼時候才能停止相信傑，那個馬里蘭州所依賴的證詞的人? Adnan殺了她! 不，是傑乾的! 連續劇的不確定，真實到現實的結局|Emily Shire | 2014年12月18日|DAILY BEAST 
 根據海軍罪案調查處首席調查員的法庭證詞，裡面有各種手機，甚至還有珍貴的信件。 海軍“英雄”拉皮條給一名艾滋病毒陽性青少年 Nestel | 2014年12月11日|每日野獸 
 既然我們有了話題標籤，誰還需要法庭，更不用說受害者的證詞了? 閉嘴，Lady Gaga的強姦不是關於你|艾米·齊默爾曼| 2014年12月4日|每日野獸 
 陪審團秘密開會數月，收集包括潘塔列奧在內的證人的證詞。 加納殺手未被起訴，紐約會成為下一個弗格森嗎? Jacob Siegel 2014年12月3日|DAILY BEAST 
 他們所有的證詞都被法庭接受了，其中大部分指責都是他們的船長。 Costa Concordia的Randy魯莽船長出庭|芭比·拉扎·納多| 2014年12月2日|每日野獸 
 所有各方都證明了他的工作的價值和他卓越的才能。 《每日曆史與年表》|喬爾·蒙塞爾 
 在這一點上，我有不同看法和無可指責的證人的證詞。 看一眼歐洲|賀拉斯格里利 
 華盛頓體力勞動者學校和霍華德學院可以證明他的勤奮和愛國精神。 《每日曆史與年表》|喬爾·蒙塞爾 
 威靈頓本人也證實了蘇爾特的美德，認為他在法警中僅次於馬斯納。 拿破崙的那幾個小元帥| R。 p . Dunn-Pattison 
 很少有人知道——事實上可能只有一個人，那就是她的丈夫——也沒有人意識到那份虛假證詞給她帶來了多大的損失。 homestead |Oscar Micheaux