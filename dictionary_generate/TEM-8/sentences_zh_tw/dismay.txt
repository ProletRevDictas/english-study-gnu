Facebook等平臺的收入將因廣告收入的損失而受到衝擊，這些平臺的反應是憤怒和沮喪，而選擇退出意味著營銷人員將更難有效地瞄準消費者。 深度跳水:2020年夏天如何迫使品牌營銷向更好的方向改變|jim cooper | 2020年9月14日|Digiday 
 只有在《多世界解釋》一章中，他才表達了自己的沮喪。 量子力學的多種含義指南——如此浪漫的事實|Sabine Hossenfelder | 2020年9月3日|鸚鵡鸚鵡 
 你離華爾街越遠，對經濟狀況的沮喪感就越深。 道指的大型科技股改頭換面能否最終推動該藍籌股指數突破3萬點大關? 伯恩哈德·華納，2020年8月25日 
 加拿大作家瑪格麗特·阿特伍德在2003年的經典小說《羚羊與秧雞》中提到了一個“到處都是沮喪，卻沒有足夠的救護車”的時代——這是對我們當前困境的預測。 科幻小說探索了冠狀病毒大流行揭示的相互聯繫Mayurika Chakravorty, 2020年8月9日 
 令許多公共衛生專家沮喪的是，即使在冠狀病毒大流行肆虐之際，這在美國仍是一個有待辯論的問題。 為什麼科學家說戴口罩不應該是有爭議的|Tina Hesman Saey | 2020年6月26日|科學新聞 
 所有接受採訪的官員都對直接向該集團支付款項的可能性表示失望。 為什麼人道主義者與ISIS對話|Joshua Hersh | 2014年10月24日|DAILY BEAST 
 對於成熟的觀察者來說，青少年焦慮、朦朧的頭腦是持續困惑和沮喪的來源。 誰發明了“青少年”? Nina Strochlic | 2014年3月14日 
 還有一個16歲左右的男孩(令我沮喪的是，他沒有參與節目)戴著一頂鋁箔做的帽子。 2014年3月2日，金酸莓獎後臺，表彰好萊塢最差電影 
 除了對生活的令人困惑的本質感到沮喪地舉手投降，我們還能做什麼呢? 死亡集中營合作者的自白:克勞德·蘭茲曼的“最後的不公正”吉米·索| 2014年2月7日|每日野獸 
 當大多數人對這個消息的反應都是懷疑和沮喪的時候，約瑟夫卻謙卑地接受了這個消息。 聖誕節真正的禮物是生命、愛和上帝的神秘|Joshua DuBois | 2013年12月25日|DAILY BEAST 
 李斯特看著它，讓她又害怕又沮喪的是，他不耐煩地喊道:“不，我不要聽!” 音樂-德國留學|艾米·費伊 
 現在，他從單純的悔恨，經過沮喪，變成了對自己的諾言的徹底懺悔。 聖馬丁之夏，拉斐爾·薩巴蒂尼 
 但即使是這樣的責備也可能太過分了，有一次我發現我很沮喪。 英格蘭、蘇格蘭和愛爾蘭五十年的鐵路生活|約瑟夫·塔特洛 
 他嚇了一跳，臉上流露出那麼多的憂慮和沮喪，我茫然地盯著他。 離奇的故事|各種 
 兩個人迅速、急切地交換著眼色，帶著疑問，流露出沮喪的神情。 三個約翰·沉默的故事|阿爾傑農·布萊克伍德