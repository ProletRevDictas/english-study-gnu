我們已經證明了自己——所有的數字都表明我們現在比電視和紙媒加起來還要大——現在是時候弄清楚我們是怎麼做到的了。 “行業不可避免的成熟”:桌面廣告攔截已經過了它的頂峰|Lara O' reilly | 2020年8月20日|Digiday 
 這些變化仍然會降低郵件的速度，包括媒體郵件(Media mail)的投遞速度，因為許多書籍和基於光盤的內容的包裹通常都很薄，可以很容易地塞進家裡或公司的郵資印刷的郵箱。 慢速的郵件投遞是獨立書店現在最不需要的東西 
 Mighty Buildings的住宅與3d打印住宅在兩個方面有所不同。 這些時髦的房子是3D打印的，它們適合在你的後院|Vanessa Bates Ramirez | 2020年8月13日|奇點中心 
 像SubRosa和Omelet這樣的商店之前已經冒險進入雜誌出版領域，分別發行了兩年一度的紙質雜誌《La Petit Mort》和季刊《Wake Up》。 “讓我們把它發佈到世界上”:為什麼代碼和理論正在創建自己的思想領導出版物Decode |克里斯蒂娜·蒙洛斯| 2020年8月6日|Digiday 
 這棟400平方米，兩層樓高的房子花了45天打印出來——在當時，這似乎快得驚人。 這個小房子是3D打印的，漂浮的，將持續100年以上 
 他們躲在巴黎東北部的一家印刷廠裡，把一名工作人員扣為人質。 法國擊斃《查理週刊》兇手|尼克·海恩斯| 2015年1月9日|每日野獸 
 它還要求，如果廣告對模型進行了數字更改，則必須打印免責聲明。 怎樣的瘦才算太瘦? 以色列禁止“體重過輕”模特|Carrie Arnold | 2015年1月8日|DAILY BEAST 
 在《雪橇旅行》(Sleigh Ride)中，敘述者正在描繪一個如此完美的場景，以至於它可以出現在柯里耶(Currier)和艾夫斯(Ives)的標誌性版畫上。 最令人困惑的聖誕歌詞解釋(視頻)|凱文·法倫| 2014年12月24日|每日野獸 
 斯克羅吉仍然和我們在一起，不僅出現在出版物中，還體現在各地厭世者的冷漠心和自私算計中。 狄更斯和斯克羅吉如何拯救聖誕節|克萊夫·歐文| 2014年12月22日|每日野獸 
 Mokbar的Esther Choi說，她做過名為gam ja jun的韓國土豆煎餅，還有PRINT的Charles Rodriguez。 我吃土豆煎餅直到我被挖出來|Emily Shire | 2014年12月17日|每日野獸 
 這種新的印刷聯繫是在四五代人的一生中成長起來的，它正在經歷不斷的變化。 文明的拯救H。 (赫伯特·喬治)威爾斯 
 鋼邊馬蹄印在鬆軟的壤土上清晰可見，就像剛下過雪的鹿皮鞋印一樣。 純金|伯特蘭·w·辛克萊 
 泥牆的壁龕裡放著一幅廉價的聖母像，一根蠟燭剛剛熄滅。 雷蒙娜，海倫·亨特·傑克遜 
 直到印章被固定在羊皮紙上，釋放他的授權書公佈於眾，他才得以休息。 布萊克伍德愛丁堡雜誌，No。 CCCXXXIX。 1844年1月。 卷,LV。 |各種 
 本電子書已從1767年出版的原始印刷版進行了轉錄。 對牧師馬斯基林先生最近出版的一本小冊子的評論