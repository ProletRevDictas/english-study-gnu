這是可以理解的，人們缺乏同理心或遠見，意識到父母有一系列特殊的挑戰。 硅谷有權利的工人的抱怨亞當·拉辛斯基| 2020年9月8日|財富 
 我希望這種對人類同理心的推動能持續到現在。 今年40位40歲以下的人是如何在大流行中倖存下來的|jonathanvanian2015 | 2020年9月7日|財富 
 相反，她建議以同理心對待反面具者。 在COVID-19大流行期間，你應該對拒絕戴口罩的人說什麼? 沒有直人新聞 
 疫情後對員工安全的關注不僅僅是因為CEO們的同情。 利益相關者資本主義不是一個選擇，Alan Murray, 2020年8月24日，財富 
 這與該公司強烈的同理心有很大關係。 第一次管理者的同理心領導指南|Celeste Mora | 2020年8月20日|Quartz 
 你寫了很多關於名人的文章，而且很有同情心。 達芙妮·莫金談莉娜·杜漢姆，書評和自省|明迪·法拉比| 2014年12月26日|每日野獸 
 “我很同情男性，也很同情他們所承受的壓力。” Beyoncé宣言:虛無主義和女權主義語錄|艾米·齊默爾曼| 2014年12月12日|每日野獸 
 因此，這本書有一個吸引人的雙重“同理心”，這個詞出現在所有四個部分中。 理查德·福特的巧妙生存指南:弗蘭克·巴斯科姆的迴歸|湯姆·勒克萊爾| 2014年11月4日|每日野獸 
 這些場景引發了演員和工作人員的親密評論，誰的視角更能引起共鳴，或者感覺更真實。 為什麼你婚姻幸福，卻有外遇|Ryan Selzer | 2014年11月2日|DAILY BEAST 
 但研究表明，白人對黑人的同理心更少。 聖路易斯縣的問題:白人能理解黑人嗎? 2014年8月23日|DAILY BEAST 
 也許是所謂的“天生”機械師，他們對機械的理解是一種我們從未懷疑過的同理心。 意外飛行|弗洛伊德·l·華萊士 
 在這些簡單的事情之外還有心靈感應，心靈遙感，移情.... 地下人送禮物|弗雷德裡克·布朗 
 但我也贏得了20年代，記得嗎，那時候我也不知道什麼是同理心。 《詛咒星球》哈里·哈里森 
 一些定居者對海豚有高度的同理心，但羅斯自己的接觸能力相對較弱。 Out of Time |安德烈·愛麗絲·諾頓 
 他想起了吉莉亞，想起了夢中的移情作用。 Earthsmith |彌爾頓較小