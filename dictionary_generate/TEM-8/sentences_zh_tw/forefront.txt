位於柏林的政府運營的羅伯特·科赫公共衛生研究所(Robert Koch Institute for public health research)一直站在該國強有力的大流行應對行動的前沿，領導著疫苗的研究，並競相推出大量的測試樣本。 2020年8月19日，《麻省理工科技評論 
 長期以來，《蘋果日報》一直站在香港民主運動的前沿，密切報道抗議活動，並通過一貫嚴肅的新聞作風對政府官員進行監督。 北京正在著手拆除香港唯一不受其控制的報紙之一，2020年8月10日，Quartz 
 有這些在科學和工程發現前沿的決賽選手，我知道我們得到了很好的照顧。 對於青少年來說，大問題可能會導致有意義的研究|卡羅琳·威爾克| 2020年7月28日|學生科學新聞 
 然而，尤其是兒童保育問題，並沒有經常出現在政治辯論的前沿。 為什麼政客們花了這麼長時間才把兒童保育危機視為一場危機|克萊爾·馬龍(clare.malone@fivethirtyeight.com) | 2020年7月16日|FiveThirtyEight 
 幫助起草這項政策的活動人士和社區成員多年來也一直站在刑事司法問題的最前沿。 智能街燈計劃實施多年，委員會將制定監控規則傑西·馬克思2020年7月9日聖地亞哥之聲 
 沃爾特雖然是個瘋子，但他卻走在印刷藝術的最前沿。 蒂姆·伯頓談《大眼睛》，他對恐怖的品味，以及《陰間大法師》續集 
 他們說，有許多“志願者”，奧塞梯人和車臣人在訓練他們的第一線。 東烏克蘭:回到蘇聯|傑米·德特默| 2014年11月19日|每日野獸 
 但現在，他們應該為了國家，把自己的利益放在首位。 巴西迪爾瑪·羅塞夫能教希拉里·克林頓|Heather Arnet | 2014年10月29日|每日野獸 
 據今天戰鬥在第一線的醫生說，他們仍然如此。 血液是埃博拉病毒的武器和弱點Abby Haglage 2014年10月26日 
 你有沒有因為把跨性別問題放在音樂的前沿而感到壓力，比如《Against Me》! Laura Jane Grace的變性朋克叛亂|Melissa Leon | 2014年10月10日|DAILY BEAST 
 在當時所有的著作中，神學的興趣都是最重要的。 德國文化的過去與現在|歐內斯特·貝爾福特·巴克斯 
 他們一起向炮房的門口跑去，前面那根冒著煙的發亮的木頭尖。 薩滿|羅伯特·謝伊 
 他出身於著名的紐約休伊特家族，該家族的成員一直走在進步的最前沿。 如何成為成功的發明家|古德溫·b·史密斯 
 總的來說，學校是空氣清新運動的前沿，尤其是公立學校。 一個女孩的學生時代和之後|珍妮特·馬克斯 
 從人群的最前面，一個穿深紅色長袍的人向船跑去。 地球上的巨人斯特納·聖保羅·米克