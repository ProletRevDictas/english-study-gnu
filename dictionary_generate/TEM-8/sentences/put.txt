To put it rather uncharitably, the USPHS practiced a major dental experiment on a city full of unconsenting subjects. Anti-Fluoriders Are The OG Anti-Vaxxers |Michael Schulson |July 27, 2016 |DAILY BEAST 

Kennedy: "Mankind must put an end to war — or war will put an end to mankind." Huckabee 2016: Bend Over and Take It Like a Prisoner! |Olivia Nuzzi |January 8, 2015 |DAILY BEAST 

Obsessive exercising and inadequate nutrition can, over time, put people at high risk for overuse injuries like stress fractures. How Skinny Is Too Skinny? Israel Bans ‘Underweight’ Models |Carrie Arnold |January 8, 2015 |DAILY BEAST 

When I put their allegations to Epstein, he denied them and went into overdrive. I Tried to Warn You About Sleazy Billionaire Jeffrey Epstein in 2003 |Vicky Ward |January 7, 2015 |DAILY BEAST 

We did ThunderAnt stuff for ourselves and just put it online, and then it blossomed into something else. Coffee Talk with Fred Armisen: On ‘Portlandia,’ Meeting Obama, and Taylor Swift’s Greatness |Marlow Stern |January 7, 2015 |DAILY BEAST 

He was voluble in his declarations that they would “put the screws” to Ollie on the charge of perjury. The Bondboy |George W. (George Washington) Ogden 

Each day she resolved, "To-morrow I will tell Felipe;" and when to-morrow came, she put it off again. Ramona |Helen Hunt Jackson 

This is the place where the Muscovite criminals are banished to, if they are not put to death. The Life and Most Surprising Adventures of Robinson Crusoe, of York, Mariner (1801) |Daniel Defoe 

Let them open their minds to us, let them put upon permanent record the significance of all their intrigues and manœuvres. The Salvaging Of Civilisation |H. G. (Herbert George) Wells 

Before the spinet a bench was placed about four feet below the keys, and I was put upon the bench. Gulliver's Travels |Jonathan Swift