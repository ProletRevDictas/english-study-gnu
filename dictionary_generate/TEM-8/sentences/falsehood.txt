Once falsehoods move to encrypted messaging apps like Telegram and WhatsApp, it becomes significantly harder to monitor and put a stop them. Among Latino immigrants, false vaccine claims are spreading as fast as the virus |Teo Armus |February 11, 2021 |Washington Post 

Every falsehood eventually comes back to haunt Carlson, a fellow who pontificates every night on the country’s affairs. In attacking Ocasio-Cortez, Tucker Carlson maxes out on hypocrisy |Erik Wemple |February 4, 2021 |Washington Post 

Were she president, one assumes that Rubio would strongly advocate robust coverage of her falsehoods and an exploration of conspiracy theories she might offer that could lead to dangerous situations. The Republican conspiracy-theory crisis, in one tweet |Philip Bump |February 3, 2021 |Washington Post 

Such falsehoods will be a major impediment to the vaccine campaign. The key to combating conspiracy theories about coronavirus vaccines |Cameron Givens |February 1, 2021 |Washington Post 

Advocacy groups push for Marjorie Taylor Greene’s resignation over report that she spread falsehoods about school shootingsOn Tuesday, CNN’s KFile published the findings of its review of hundreds of posts and comments on Greene’s Facebook page. Rep. Marjorie Taylor Greene’s endorsement of conspiracy theories, violence sparks calls for her resignation — again |Reis Thebault |January 27, 2021 |Washington Post 

In our digital world falsehood flies first class via hyperlinks, Tweets, texts, and The View. The Facts About Ferguson Matter, Dammit |Doug McIntyre |December 3, 2014 |DAILY BEAST 

Instead, Clapper responded with an egregious falsehood, if not an outright lie. Ron Wyden and Rand Paul, the Senate's NSA-Busting ‘Ben Franklin Caucus’ |Eleanor Clift |June 14, 2014 |DAILY BEAST 

But later that day, NBC, CBS, and the Los Angeles Times all reported the same “falsehood,” also citing law-enforcement sources. Boston Marathon Bombing Media Errors Pile Up, as Does the Outrage |Michael Moynihan |April 18, 2013 |DAILY BEAST 

But no matter, because journalists and partisans—and even The Onion—jumped on the Post for reporting a “total falsehood.” Boston Marathon Bombing Media Errors Pile Up, as Does the Outrage |Michael Moynihan |April 18, 2013 |DAILY BEAST 

Presented with such a beautiful falsehood, the rest of humanity gratefully embraced the illusion. What Karl Rove Learned From Jorge Luis Borges |Alec Nevala-Lee |November 20, 2012 |DAILY BEAST 

For her to testify that she did not love—and had never loved Jean Baptiste, he knew would be a deliberate falsehood. The Homesteader |Oscar Micheaux 

In the ordinary compliments of civilized life, there is no intention to deceive, and consequently no falsehood. The Ladies' Book of Etiquette, and Manual of Politeness |Florence Hartley 

This thing called Secession originated in falsehood, theft and perjury. Portrait and Biography of Parson Brownlow, The Tennessee Patriot |William Gannaway Brownlow 

Then after he had encouraged them, he shewed withal the falsehood of the Gentiles, and their breach of oaths. The Bible, Douay-Rheims Version |Various 

If he finds no out-of-the-way truths, he will identify himself with no very burning falsehood. The Pocket R.L.S. |Robert Louis Stevenson