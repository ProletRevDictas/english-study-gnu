Stuart Scheller expressed “contempt” that the defense secretary, chairman of the Joint Chiefs of Staff and commandant of the Marine Corps executed the president’s directive to withdraw from Afghanistan. The Kabul evacuation illuminated a dangerous strain of thought in the military about civilian control |James Hohmann |September 1, 2021 |Washington Post 

Bridging the gap between the two, Yeager went on to train 19 astronauts while he was commandant of the Air Force’s Aerospace Research Pilot School from 1962 to 1966. Chuck Yeager has died at 97, but the legacy of his record-breaking flight lives on |Rob Verger |December 8, 2020 |Popular-Science 

“In the camp no-one knows themselves,” muses the monstrous commandant. How Hitch & Amis Discovered Evil In My House |Peter Foges |September 28, 2014 |DAILY BEAST 

We believe that we may be close to such an impasse, and we want to present the Commandant with options. Gagging the Corps: A Marine Commandant’s War on Newsprint |David Abrams |February 26, 2014 |DAILY BEAST 

He is a graduate of the Kakul academy and was also its commandant at one point in his career. The New Head of Pakistan’s Army Holds the Country’s Most Important Job |Bruce Riedel |November 30, 2013 |DAILY BEAST 

He was previously deputy commandant for combat development and integration. Why Was Firefighter-Marine Reserve Maj. Jason Brezler Betrayed? |Michael Daly |November 19, 2013 |DAILY BEAST 

He also wrote to the Commandant Coast Guards, identifying obvious vulnerabilities. When India Failed in the Mumbai Terrorist Attacks |Emma Garman |November 2, 2013 |DAILY BEAST 

The Commandant was to take over the offices, staff, and functions of the late Civil Governor. The Philippine Islands |John Foreman 

October 15, 1612, he was formally appointed commandant in New France. The Jesuit Relations and Allied Documents, Vol. II: Acadia, 1612-1614 |Various 

As his reward the Minister of War promoted him colonel and commandant of the second regiment of horse artillery. Napoleon's Marshals |R. P. Dunn-Pattison 

Randolph himself was very sorely bested, but he succeeded in killing the commandant; whereupon the garrison gave in. King Robert the Bruce |A. F. Murison 

The projection of land fixed upon for the site of a town, was named after the commandant (Captain Barlow). Narrative of a Survey of the Intertropical and Western Coasts of Australia] [Volume 2 of 2] |Phillip Parker King