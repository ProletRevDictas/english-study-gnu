Movies like this conjure a world in which everything is a little senseless and absurd. M. Night Shyamalan returns with Old, a floppy but haunting thriller about aging |Alissa Wilkinson |July 23, 2021 |Vox 

Performed by a common soldier fresh from the field of battle, this song exposes the senseless cruelty and needless death of warfare and makes clear the inescapable fear felt by young, American men during the Vietnam era. ’1776’ — not ‘Hamilton’ — is the musical that best portrays the Founders |Zachary Clary |July 1, 2021 |Washington Post 

If you’re hearing danger in a strange noise late at night, or looking at a world event and thinking that there must be more to it than what we’re being told, you’re just doing what your brain has evolved to do as a way to make sense of the senseless. Why Does the QAnon Conspiracy Thrive Despite All its Unfulfilled Prophecies? |Mike Rothschild |June 30, 2021 |Time 

It is their most sincere hope that someone will come forward and cooperate with authorities so that the perpetrator of these senseless crimes can be brought to justice. Questions Swirl After Powerful Lawyer’s Wife, Son Are Killed Amid Fatal Boat Party Probe |Justin Rohrlich |June 10, 2021 |The Daily Beast 

Ultimately, the pleasures of “Life’s Edge” derive from its willingness to sit with the ambiguities it introduces, instead of pretending to conclusively transform the senseless into the sensible. The surprisingly elusive definition of ‘life’ |Jacob Brogan |March 26, 2021 |Washington Post 

Or will we simply see more senseless bloodshed and another generation of Palestinians defer their dreams of a homeland? In the Middle East, the Two-State Solution Is Dead |Dean Obeidallah |January 2, 2015 |DAILY BEAST 

Bethea is trying to conjure why all the senseless killing of both his family member and the cops as well. Protesters Demand Justice For Gurley As Gap Grows Between Cops and NYC |M.L. Nestel |December 28, 2014 |DAILY BEAST 

Senseless bureaucracy is part of what spawned the Tea Party. Red Tape Is Strangling Good Samaritans |Philip K. Howard |December 27, 2014 |DAILY BEAST 

The plot was a string of anecdotes from the senseless shootings of friends that Brinsley knew. Alleged Cop Killer’s Blood-Soaked Screenplay |M.L. Nestel |December 24, 2014 |DAILY BEAST 

It reflects both the anger provoked by the senseless violence two days earlier, and the growing confidence of the opposition. How Havel Inspired the Velvet Revolution |Michael Zantovsky |December 6, 2014 |DAILY BEAST 

They shall be all proved together to be senseless and foolish: the doctrine of their vanity is wood. The Bible, Douay-Rheims Version |Various 

The piper was taken away senseless, but my brother would not suffer either Joe or Alley to be disturbed till breakfast was ready. The Book of Anecdotes and Budget of Fun; |Various 

Overpowered with anguish, the light suddenly dropped from her hand, and she fell senseless upon the floor. Madame Roland, Makers of History |John S. C. Abbott 

I would rather have undergone a hundred runaways than one week with that old woman muttering her Dutch over my senseless form. The Soldier of the Valley |Nelson Lloyd 

Her prophets are senseless, men without faith: her priests have polluted the sanctuary, they have acted unjustly against the law. The Bible, Douay-Rheims Version |Various