Grindr currently has twelve ‘tribes,’ and for some people this just is not enough. Grindr’s Trans Dating Problem |David Levesley |January 9, 2015 |DAILY BEAST 

When twelve people are killed by violence, whoever they are, for whatever reason, that is a tragedy and a waste. Trolls and Martyrdom: Je Ne Suis Pas Charlie |Arthur Chu |January 9, 2015 |DAILY BEAST 

Twelve-year-old dance prodigy Maddie Ziegler has suffered the wrath of Dance Moms tyrant Abby Lee Miller. See Burly Shia LaBeouf Interpretive Cage Fight Lil Sia in the Singer’s Fantastic New Music Video |Marlow Stern |January 7, 2015 |DAILY BEAST 

When it was announced that Jourdan Dunn would be the first black model to cover British Vogue in twelve years it made me sad. One Vogue Cover Doesn’t Solve Fashion’s Big Race Problem |Danielle Belton |January 2, 2015 |DAILY BEAST 

I arrive at twelve-twenty-five and the secretaries are in a tizzy. Alfred Hitchcock’s Fade to Black: The Great Director’s Final Days |David Freeman |December 13, 2014 |DAILY BEAST 

It was close upon twelve o'clock, and the "Rooms" had been open to the public for two hours. Rosemary in Search of a Father |C. N. Williamson 

At twelve, or fifteen, or sixteen, or twenty it was decided that they should stop learning. The Salvaging Of Civilisation |H. G. (Herbert George) Wells 

In the next chapter he gives his twelve disciples authority over demons. Solomon and Solomonic Literature |Moncure Daniel Conway 

With twelve hundred foes around us, we had plenty to occupy all our thoughts and attention. Blackwood's Edinburgh Magazine, No. CCCXXXIX. January, 1844. Vol. LV. |Various 

We had been twelve hours in the saddle, and had ridden over nearly a hundred miles of ground. Blackwood's Edinburgh Magazine, No. CCCXXXIX. January, 1844. Vol. LV. |Various