There shall also be a standard width of dyed cloth, russet and haberject, namely two ells within the selvedges. Why beer serving sizes are so weird |Mike Rampton/Mel Magazine |October 20, 2021 |Popular-Science 

Cemeteries have also been bulldozed, leaving bone fragments protruding from the russet earth. How Beijing Is Redefining What It Means to Be Chinese, from Xinjiang to Inner Mongolia |CHARLIE CAMPBELL/SHANGHAI |July 12, 2021 |Time 

After testing both russets and Yukon Golds, I can confidently say russets are the way to go. How to make crispy air-fryer fries with no fuss and very little muss |Daniela Galarza |June 17, 2021 |Washington Post 

He was wearing a fleece gilet zipped over his russet sweater. The Country Gentleman of Physics - Issue 100: Outsiders |Michael Brooks |May 12, 2021 |Nautilus 

Winds and rain could easily turn the desert back into a blank russet canvas. The famous Nazca lines aren’t mysterious—but they are ingenious |Sara Kiley Watson |January 3, 2021 |Popular-Science 

A maple spread friendly arms at one corner, a lordly tree that would blaze crimson and russet-brown when October came again. The Hidden Places |Bertrand W. Sinclair 

The St. Bernard laid his silver and russet head on her skirt. Mary Gray |Katharine Tynan 

Tertiary Colours are three only, citrine, russet, and olive. Field's Chromatography |George Field 

Inside was an elderly lady, and in her arms was a russet Pekingese. A Boswell of Baghdad |E. V. Lucas 

The third circle shows how slate, citrine and russet are made. Color Value |C. R. Clifford