According to the theory behind the Weibel instability, the two sets of plasma break into filaments as they stream by one another, like two hands with fingers interlaced. Giant lasers help re-create supernovas’ explosive, mysterious physics |Emily Conover |November 12, 2020 |Science News 

It may be in the halos around galaxies or, as another paper recently suggested, in filaments stretching between galaxies. Meet the disk-shaped halo of hot gas you currently live in |Kate Baggaley |October 21, 2020 |Popular-Science 

Each magnificent wing is covered in billions of tiny filaments, each a tenth of the width of a human hair. Imitation Is the Sincerest Form of Environmentalism - Issue 90: Something Green |Anastasia Bendebury & Michael Shilo DeLay |October 7, 2020 |Nautilus 

Give Dan Rhodes a small sample of a novel polymer, and he’ll figure out how to extrude it into a filament, and how to fine-tune the process to see whether the material can be made to work in high-speed manufacturing. Unmade in America |Tate Ryan-Mosley |August 14, 2020 |MIT Technology Review 

The team actually gathered data from the filament back in 2014 during a single eight-hour stretch, but the data sat waiting as the radio astronomy community spent years figuring out how to improve the calibration of LOFAR’s measurements. The Hidden Magnetic Universe Begins to Come Into View |Natalie Wolchover |July 2, 2020 |Quanta Magazine 

The library in Williamsburg itself is illuminated with antique filament bulbs and everything inside is of the past or a nod to it. Blurred Lines at NY Sketchbook Museum |Daniel Genis |November 1, 2014 |DAILY BEAST 

The B voltage gives the plate a positive charge to attract electrons from the filament. Whatever Happened to the "B" Battery? |Megan McArdle |December 21, 2012 |DAILY BEAST 

When you connect the A battery, the filament of the tube is heated to release negatively charged electrons. Whatever Happened to the "B" Battery? |Megan McArdle |December 21, 2012 |DAILY BEAST 

Electrons travel through the partial vacuum inside the tube, flowing from the filament to the positively charged plate. Whatever Happened to the "B" Battery? |Megan McArdle |December 21, 2012 |DAILY BEAST 

Many tubes also have small structures, known as grids, between the filament and the plate. Whatever Happened to the "B" Battery? |Megan McArdle |December 21, 2012 |DAILY BEAST 

In the gill filament the blood comes into contact with the free oxygen of the water bathing the gills. A Civic Biology |George William Hunter 

Something has been previously said of the difficulties attending the making of the filament for the incandescent light. Steam Steel and Electricity |James W. Steele 

The large hairs have their root, and even part of the filament, enclosed in a small membraneous vessel or capsule. The Book of Curiosities |I. Platts 

When a sufficient current was passed through the filament, it glowed with a dazzling lustre. Heroes of the Telegraph |J. Munro 

At one side stand the warps, very tall and interesting to see, with their lines of delicate filament and high tiers of bobbins. Making Both Ends Meet |Sue Ainslie Clark and Edith Wyatt