Less gathers on the side where the nanowires touch the larger electrode. Will bacterial ‘wires’ one day power your phone? |Alison Pearce Stevens |September 2, 2020 |Science News For Students 

I have never been to the Matzo Ball, but I gather the vibes are different. The Craziest Date Night for Single Jews, Where Mistletoe Is Ditched for Shots |Emily Shire |December 26, 2014 |DAILY BEAST 

Cabinet ministers of the day gather to review the names and the allegations. The Castration of Alan Turing, Britain’s Code-Breaking WWII Hero |Clive Irving |November 29, 2014 |DAILY BEAST 

And who else would let them gather dust in some drawer for nearly 50 years? Elvis Costello, Marcus Mumford, and Others Crowdsource A Dylan Album |Malcolm Jones |November 16, 2014 |DAILY BEAST 

The land involved is sacred to them and used to gather acorns for religious ceremonies. McCain Helps a Business Partner of Iran |Ben Jacobs |November 13, 2014 |DAILY BEAST 

They gather and sleep in open fields, surrounded by nature and the stillness of the night. London’s Pagan Counterculture Kings |Justin Jones |October 12, 2014 |DAILY BEAST 

She would sometimes gather them passionately to her heart; she would sometimes forget them. The Awakening and Selected Short Stories |Kate Chopin 

She waited for the material pictures which she thought would gather and blaze before her imagination. The Awakening and Selected Short Stories |Kate Chopin 

I may be tempted to postpone my retirement, and for a while longer to continue to gather the golden harvest that ripens round me. Checkmate |Joseph Sheridan Le Fanu 

Draw near to me, ye unlearned, and gather yourselves together into the hours of discipline. The Bible, Douay-Rheims Version |Various 

She saw two fiery eyes; she saw the tiger gather himself preparatory to springing. The Soldier of the Valley |Nelson Lloyd