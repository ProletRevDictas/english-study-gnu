It’s the most precaution Valderruten has taken before a first date, and it’s a sign of how much the singles scene has changed in the past year. What’s sexy in a pandemic? Caution. |Lisa Bonos |February 12, 2021 |Washington Post 

The fact that I was even thinking that was a sign of how you start doubting your whole reality. Deepfake porn is ruining women’s lives. Now the law may finally ban it |Karen Hao |February 12, 2021 |MIT Technology Review 

The stormy pattern shows no signs of stopping, with four more chances for wintry precipitation over the next week. Snow expected tonight, mainly south of D.C., before possible ice on Saturday |Jason Samenow, Wes Junker |February 11, 2021 |Washington Post 

Senior aides to the impeachment managers’ team claim they are seeing signs that some Republicans may be wavering and might be convinced to vote for a conviction. I was skeptical of Democrats pursuing a second impeachment. I was wrong. |Karen Tumulty |February 11, 2021 |Washington Post 

In a true sign that the Politico of 2021 is not the Politico of, say, 2008, the Playbook crew didn’t publish the story 30 seconds after asking for comment. Politico loses scoop to People |Erik Wemple |February 11, 2021 |Washington Post 

They are always suspended over a precipice, dangling by a slender thread that shows every sign of snapping. How the PC Police Threaten Free Speech |Nick Gillespie |January 9, 2015 |DAILY BEAST 

It was hard not to take it as a sign, a personal comment on my own Jewish dating failings. My Week on Jewish Tinder |Emily Shire |January 5, 2015 |DAILY BEAST 

If he did, it could be a sign that our politicians are ready to resume genuine policy-making across party lines. Christie Blames Parents for Bad Economy |Monica Potts |January 3, 2015 |DAILY BEAST 

President Harry Truman kept a sign on his desk that read: “The Buck Stops Here.” The ‘No Child’ Rewrite Threatens Your Kids’ Future |Jonah Edelman |January 3, 2015 |DAILY BEAST 

Even then, most of us doubted he would show up and actually sign the papers allowing him to enter the 1992 New Hampshire primary. President Cuomo Would’ve Been a Lion |Jonathan Alter |January 2, 2015 |DAILY BEAST 

Its continued presence in pulmonary tuberculosis is, however, a grave prognostic sign, even when the physical signs are slight. A Manual of Clinical Diagnosis |James Campbell Todd 

Idly his pen traced upon the paper in front of him a large X, the sign of the unknown quantity. Uncanny Tales |Various 

Here they are seldom abundant, but their constant presence is the most reliable urinary sign of the disease. A Manual of Clinical Diagnosis |James Campbell Todd 

Waxy casts are found in most advanced cases of nephritis, where they are an unfavorable sign. A Manual of Clinical Diagnosis |James Campbell Todd 

Scouts reported that Porter still occupied his camp, and showed no sign of moving. The Courier of the Ozarks |Byron A. Dunn