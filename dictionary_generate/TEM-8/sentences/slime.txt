Affected sea stars at such sites may just melt into a puddle of slime. Choked by bacteria, some starfish are turning to goo |Erin Garcia de Jesus |February 8, 2021 |Science News For Students 

Such microbes thrive when there are high levels of organic matter in warm water and create a low oxygen environment that can make sea stars melt in a puddle of slime. Some bacteria are suffocating sea stars, turning the animals to goo |Erin Garcia de Jesus |January 20, 2021 |Science News 

Instead of the traditional narration from Jim Nantz and Tony Romo, there were graphics aplenty, including that slime in the end zone and on the first down markers. The future of football-watching has arrived, with streams for everyone (and slime for the kids) |Ben Strauss |January 11, 2021 |Washington Post 

It was fun to see what sort of magnets they were going to use or how much slime was going to be on it. Inside ‘Save Yourselves!’—a socially distant comedy for our times |radmarya |October 1, 2020 |Fortune 

That suggests that light from the slime may help trigger the production of more light. This tube worm’s glowing slime may help sustain its own shine |Carolyn Wilke |June 5, 2020 |Science News For Students 

The pair riff on pink slime, the New World Order, and even the War on Christmas™. Kirk Cameron Saves Christmas from Abominable Killjoys (Other Christians) |Brandy Zadrozny |November 14, 2014 |DAILY BEAST 

Or hear stories about something called “pink slime” infiltrating their Quarter Pounders. Do You Want Wine With Those Fries? |Abby Haglage |October 1, 2013 |DAILY BEAST 

Because the movement of the giant vessel was so slow, the only way to mark the rotation was by watching the slime line rise. The Raising of the Concordia |Barbie Latza Nadeau |September 17, 2013 |DAILY BEAST 

The Consumerist declared that a “new circle in hell” had opened for these “slime.” 12-12-12 Concert Ticket Scalpers: The Hurricane Sandy Benefit Spoilers |Winston Ross |December 13, 2012 |DAILY BEAST 

A GOP professional laments the “slime and dirt and muck attached not only to the two candidates but also to the party itself.” Romney and Gingrich Set the GOP on a Path Toward Self-Destruction |John Batchelor |January 30, 2012 |DAILY BEAST 

Edna looked at her feet, and noticed the sand and slime between her brown toes. The Awakening and Selected Short Stories |Kate Chopin 

The greasy surface, dotted here and there with specks of vegetable, resembles a pool of stagnant water covered with green slime. Prison Memoirs of an Anarchist |Alexander Berkman 

So she leant over—down, nearer, closer, until her fingers curved over the stone amid the moisture and green slime. Menotah |Ernest G. Henham 

Not far distant Winton lay stretched along a fir-shadowed rock, the slime-green base of which was washed by the lipping waves. Menotah |Ernest G. Henham 

They were slippery with river slime and the light boat climbed up on them, driving them down under the water. Stories of Our Naval Heroes |Various