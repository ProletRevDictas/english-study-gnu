In response, it dilates the blood vessels near the surface of our skin, allowing the blood to release more heat into the surrounding air. What extreme heat means for the future of the Summer Olympics |Claire Maldarelli |July 23, 2021 |Popular-Science 

It can also dilate its nostrils to boost its trunk’s carrying capacity while snorting up water, researchers report online June 2 in the Journal of the Royal Society Interface. Newly recognized tricks help elephants suck up huge amounts of water |Sid Perkins |June 3, 2021 |Science News 

For instance, CGRP causes blood vessels to dilate in a variety of systems including the intestines. New drugs that block a brain chemical are game changers for some migraine sufferers |Karen J. Bannan |March 22, 2021 |Science News 

Heat dilates blood vessels that surround the lumbar region of your spine, increasing the flow of oxygen to your back muscles and stimulating sensory receptors in your skin to help heal damaged tissue and relieve discomfort. The best heated chairs for pain relief and maximum comfort |PopSci Commerce Team |January 6, 2021 |Popular-Science 

With close attention, our pupils dilate and contract over and over. Can’t remember? Maybe you multitask too much between screens |Alison Pearce Stevens |December 14, 2020 |Science News For Students 

This will make your pupils dilate, making you more attractive. Russia’s Gold Digger Academy |Peter Pomerantsev |November 11, 2014 |DAILY BEAST 

Kettlewell used to dilate on the great sacrificial feast of charity. The English Church in the Eighteenth Century |Charles J. Abbey and John H. Overton 

Another room had been added to the cabin—and the fragrant smell of cedar made her nostrils dilate. The Trail of the Lonesome Pine |John Fox, Jr. 

In that September morning his soul seemed to dilate with every breath he drew. The Child of Pleasure |Gabriele D'Annunzio 

Little need to dilate on the situation as it appeared to Mrs Iver! Tristram of Blent |Anthony Hope 

Frequently the nerves dilate the blood vessels of the skin, thus helping the sweat glands to secrete, by giving them more blood. A Civic Biology |George William Hunter