He owns a vast array of businesses, including coal mines, resort hotels and agricultural interests, many of them regulated by the state agencies that report to him. This Billionaire Governor’s Coal Company Might Get a Big Break From His Own Regulators |by Ken Ward Jr. |September 17, 2020 |ProPublica 

There are few hotels near the club, the former official said, so it was vital to have a space available for agents and equipment. Trump’s businesses charged Secret Service more than $1.1 million, including for rooms in club shuttered for pandemic |David Fahrenthold, Josh Dawsey |September 17, 2020 |Washington Post 

The Housing Commission is set to vote this Friday on the proposal to buy Residence Inn hotels in Mission Valley and Kearny Mesa. Morning Report: The Dreaded Purple Tier |Voice of San Diego |September 16, 2020 |Voice of San Diego 

He said those who made reservations in other hotels will have to contact the hotels directly to cancel their reservations. D.C. Mid-Atlantic Leather Weekend 2021 cancelled due to COVID |Lou Chibbaro Jr. |September 15, 2020 |Washington Blade 

I didn’t want to get caught, so I suggested we go back to his hotel. Making Poly Less of a Pain in the Ass |Eugene Robinson |September 14, 2020 |Ozy 

Flesh encircled him at the main pool of the Paradise Hotel and Residences at Boca. Powerful Congressman Writes About ‘Fleshy Breasts’ |Asawin Suebsaeng |January 7, 2015 |DAILY BEAST 

Scalise has called the talk, which he delivered in a hotel outside New Orleans, “a mistake I regret.” The Louisiana Racists Who Courted Steve Scalise |Jason Berry |January 3, 2015 |DAILY BEAST 

The added charge for access to hotel Wi-Fi is not only exploitative but increasingly irrelevant. How ‘Ethical’ Hotel Chain Marriott Gouges Guests in the Name of Wi-Fi Security |Kyle Chayka |December 31, 2014 |DAILY BEAST 

“Personal hotspots can get speeds of up to 60 Mb/s down, whereas hotel Wi-Fi can be as slow as 1.5 Mb/s,” Sesar said. How ‘Ethical’ Hotel Chain Marriott Gouges Guests in the Name of Wi-Fi Security |Kyle Chayka |December 31, 2014 |DAILY BEAST 

If someone wants to ensure a direct and secure connection, no entity, whether a hotel or otherwise, should be able to block it. How ‘Ethical’ Hotel Chain Marriott Gouges Guests in the Name of Wi-Fi Security |Kyle Chayka |December 31, 2014 |DAILY BEAST 

“This house must have been the hotel of some distinguished family, Baron; it is nobly proportioned,” said David Arden. Checkmate |Joseph Sheridan Le Fanu 

There he gave orders for the car to be put into running condition for the following morning, and returned to the hotel. The Joyous Adventures of Aristide Pujol |William J. Locke 

Outside the hotel he came upon the two sisters sitting on a bench and drinking coffee. The Joyous Adventures of Aristide Pujol |William J. Locke 

I turned round, thrust my purse into the lap of the nearest, and with a light heart led the lady back to the hotel. Blackwood's Edinburgh Magazine, Volume 60, No. 372, October 1846 |Various 

When he returned to the hotel he kissed his incongruous room-mate with the gentleness of a woman. The Joyous Adventures of Aristide Pujol |William J. Locke