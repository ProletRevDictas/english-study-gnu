The moon was like a wonderful white lantern in the purple sky; there was but a smoulder of stars. Villa Rubein and Other Stories |John Galsworthy 

It was strewn in a zigzag line, was lighted at one end, and allowed to smoulder away to the other. Trans-Himalaya, Vol. 2 (of 2) |Sven Hedin 

Quite large logs could be slipped in and they would lie there and smoulder, lasting sometimes all night. A Little Girl in Old Salem |Amanda Minnie Douglas 

Guillaume told me that he had known fires to smoulder on in grassy glens for weeks together. Wanderings in Patagonia |Julius Beerbohm 

We lit our fire against its side, so that it soon began to smoulder and gave out a great heat. The Backwoodsman |Various