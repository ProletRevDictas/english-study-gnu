“Demand for its flagship S Series smartphones did little to revive its smartphone sales globally,” Gupta said. This smartphone maker’s sales dropped the most due to COVID-19 |jonathanvanian2015 |August 25, 2020 |Fortune 

Mullenweg is also the CEO of Automattic, a company that offers commercial versions of the WordPress software outside of the flagship app. Why Apple let WordPress walk but continues to fight Fortnite’s Epic Games |rhhackettfortune |August 25, 2020 |Fortune 

Meanwhile, Louis Bacon’s Moore Capital, which last year decided to eject the remaining external investors from its flagship funds after a long barren stretch, notched up a 25 percent gain in seven months through July. Hedge Fund ‘Pirates’ Set Sail Again |Daniel Malloy |August 21, 2020 |Ozy 

Experts at Citizen Lab, a research group at the University of Toronto, analyzed the link and pointed to Pegasus, NSO’s flagship product. The man who built a spyware empire says it’s time to come out of the shadows |Bobbie Johnson |August 19, 2020 |MIT Technology Review 

Like most other flagship smartphones these days, the Note20 Ultra is packed with cameras. Samsung Note20 Ultra review: Why this big phone works for the COVID era |Aaron Pressman |August 18, 2020 |Fortune 

From its founding in 1914, The New Republic has been the flagship and forum of American liberalism. Facebook Prince Purges The New Republic: Inside the Destruction of a 100-Year-Old Magazine |Lloyd Grove |December 5, 2014 |DAILY BEAST 

Sure, some ignored the protests, and made their pilgrimages to Apple, Chanel, and Louis Vuitton flagship stores. Chinese Tourists Are Taking Hong Kong Protest Selfies |Brendon Hong |October 23, 2014 |DAILY BEAST 

An interview with Mel Greig will be shown on the flagship BBC2 news program Newsnight tonight. Kate Prank Call DJ Tells of Death Threats |Tom Sykes |October 13, 2014 |DAILY BEAST 

Flagship series Mad Men just finished the first half of its final season. ‘Halt and Catch Fire’ and AMC’s Push to Reset Dramas |Andrew Romano |May 30, 2014 |DAILY BEAST 

Visually, these works blend perfectly throughout the three floors of the New York flagship store on Madison Avenue. Don’t Just Look at This Art, Sit on It |Justin Jones |March 5, 2014 |DAILY BEAST 

The lectern, as the pulpit-stand in English churches is called, was fashioned of oak taken from Nelson's flagship, the Victory. British Highways And Byways From A Motor Car |Thomas D. Murphy 

The San Domingo (70 guns) blew up; the flagship Phœnix (80 guns), and three other ships of 70 guns, were taken. Private Letters of Edward Gibbon (1753-1794) Volume 1 (of 2) |Edward Gibbon 

Oh, what a dreadful time there was on Perry's flagship during those sad two hours. Stories of Our Naval Heroes |Various 

Farragut chose the sloop-of-war Hartford for his flagship and sailed merrily away for the mighty river. Stories of Our Naval Heroes |Various 

He started out with his flagship, named the Reina Cristina, straight for the Olympia, which he hoped to cut in two. Stories of Our Naval Heroes |Various