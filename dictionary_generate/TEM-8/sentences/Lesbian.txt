The last of the nation’s lesbian bars might not make it to the other side of the pandemic. Is the Government Just Going to Watch the Restaurant Industry Die? |Elazar Sontag |August 28, 2020 |Eater 

Growing up lesbian and loving to wear boys clothes, I never read of anyone like me in stories of how women got the right to vote. History offers hope in the midst of rage |Kathi Wolfe |August 28, 2020 |Washington Blade 

Corado told the Washington Blade one of the two women making the threats identified herself as the grandmother of a female teenage Casa Ruby client who identifies as a lesbian. D.C. police investigating threats against Casa Ruby |Lou Chibbaro Jr. |August 26, 2020 |Washington Blade 

She was the executive producer of the first HBO comedy special featuring a lesbian performer, Suzanne Westenhoefer, and was nominated for a Chloe Award. Power couple working to elect Biden |Peter Rosenstein |August 20, 2020 |Washington Blade 

Labeled “Girlstown” in the 90s due to a vibrant lesbian population, Andersonville is a thriving LGBTQ community that’s nestled between Edgewater and Uptown. Chicago: A Midwestern Jewel for the LGBTQ Community |LGBTQ-Editor |July 11, 2020 |No Straight News 

It said: “Tonie Tobias, Information Technology, President of GLEN, Gay and Lesbian Employee Network.” How A Company’s Support of Gay Employees Helped One of Them To Come Out | |December 24, 2014 |DAILY BEAST 

She told a story of a fellow employee who identifies as a butch dyke (a lesbian who takes on a more masculine identity). The LGBT Center That Changed Our Lives |Justin Jones |December 22, 2014 |DAILY BEAST 

And who better to do that with than the actress who is playing the object of said (alleged) lesbian affection in the flick? Inside the Lifetime Whitney Houston Movie’s Lesbian Lover Storyline |Kevin Fallon |December 16, 2014 |DAILY BEAST 

And as bad as it might be for gay or lesbian people, the discrimination is markedly worse for transgender people. State of LGBT Rights: Married on Sunday, but Fired on Monday |Gene Robinson |December 14, 2014 |DAILY BEAST 

Much more common is the African American lesbian couple in the Deep South raising children at or below the poverty line. State of LGBT Rights: Married on Sunday, but Fired on Monday |Gene Robinson |December 14, 2014 |DAILY BEAST 

The style of carving of the cymatium with its astragal should be the Lesbian. Ten Books on Architecture |Vitruvius 

Therefore, in forming our judgment of human affairs, we must apply a "Lesbian rule," instead of one that is inflexible. Blackwood's Edinburgh Magazine, No. CCCXXVIII. February, 1843. Vol. LIII. |Various 

Radical women's groups, even 'lesbian separatists' who wanted to abolish men altogether! Little Brother |Cory Doctorow 

She too, loved the handsome Lesbian and refused to leave him despite the brilliant offers made to her on all sides. An Egyptian Princess, Complete |Georg Ebers 

The same day a Persian embassy set out for Memphis on board one of the Lesbian vessels. An Egyptian Princess, Complete |Georg Ebers