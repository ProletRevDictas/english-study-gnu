Restrictions on overtime pay under DeJoy may have prevented full-time workers at some facilities from adding hours to pick up some of the slack. Poorly Protected Postal Workers Are Catching COVID-19 by the Thousands. It’s One More Threat to Voting by Mail. |by Maryam Jameel and Ryan McCarthy |September 18, 2020 |ProPublica 

Where money and technology fail, though, it inevitably falls to government policies — and government subsidies — to pick up the slack. Climate Change Will Force a New American Migration |by Abrahm Lustgarten, photography by Meridith Kohut |September 15, 2020 |ProPublica 

When the BLS lists workers by full- or part-time status, they also track whether that status is due to economic reasons — like a slowdown in that industry or a slack job market — or noneconomic reasons. The Easy Part Of The Economic Recovery Might Be Over |Amelia Thomson-DeVeaux |September 4, 2020 |FiveThirtyEight 

Underlings gave Clark, who has worked at Amazon during almost all of his career, that moniker after he told them that early in his tenure he would hide in the shadows at warehouses seeking to catch lazy workers slacking off who he could fire. Who is Dave Clark, the new chief of Amazon’s giant retail business? |Aaron Pressman |August 22, 2020 |Fortune 

With Gaspar’s low fundraising total, for instance, the conservative Lincoln Club that supports her might consider picking up the slack to help her stay competitive. Politics Report: Bry, Lawson-Remer Dominate Fundraising |Scott Lewis and Andrew Keatts |August 8, 2020 |Voice of San Diego 

He knew I was a Chicago guy, and he cut me absolutely no slack. Bill Murray’s Words of Wisdom: On Comedy, the Greatness of In-N-Out, and Searching For Great Love |Marlow Stern |October 10, 2014 |DAILY BEAST 

To the contrary: since the 2011 ouster of Gaddafi, the world has cut Libya a lot of slack. It’s Not the USA that Made Libya the Disaster it is Today |Ann Marlowe |August 3, 2014 |DAILY BEAST 

Answering a question from fellow Foxer Geraldo Rivera—does the right-leaning network cut President Obama enough slack? Bill O’Reilly Disses His Fellow Fox Newsers |Lloyd Grove |June 19, 2014 |DAILY BEAST 

Other women can often be the worst at cutting any slack towards the love interest in a sex scandal. How Monica Lewinsky Changed the Media |Tina Brown |May 9, 2014 |DAILY BEAST 

At the same time, I cut myself slack, because my creative reach went beyond my skill level. How ‘Transcendence’ Director Wally Pfister Became Christopher Nolan’s Secret Weapon |Andrew Romano |April 17, 2014 |DAILY BEAST 

An amount of slack in the chain caused the balls to knock on passing this roller before entering the pump bottom. Life of Richard Trevithick, Volume II (of 2) |Francis Trevithick 

I went into the dugout indescribably slack; hardly energy to struggle against the heat and the myriads of flies. Gallipoli Diary, Volume I |Ian Hamilton 

Lost Sister had fashioned a rude litter out of rawhide and two saplings, slack between the poles so the girl could not roll out. A Virginia Scout |Hugh Pendexter 

His crew soon produced from the slack of their frocks pieces of plug, which they passed on board in exchange for our eggs. Famous Adventures And Prison Escapes of the Civil War |Various 

At length I gave up the contest, led him with a slack rein, and pulled no longer. Blackwood's Edinburgh Magazine, Volume 67, Number 414, April, 1850 |Various