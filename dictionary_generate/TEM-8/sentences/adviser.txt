Austin is surrounding himself with senior advisers for many of his top priorities, including one for China policy. The Pentagon is taking a major step to deal with its diversity problems |Alex Ward |February 12, 2021 |Vox 

Alabama can try again to execute Smith once it institutes procedures for allowing spiritual advisers to accompany inmates. Supreme Court says Alabama cannot execute inmate without his pastor present |Robert Barnes |February 12, 2021 |Washington Post 

As the concept for the “In Your Shoes” meetups took shape, Brumberg contacted Cory Grewell, a professor of literature and drama club adviser at Patrick Henry, a 40-minute drive from Georgetown. An innovative Georgetown lab looks to theater to quell political fires |Peter Marks |February 12, 2021 |Washington Post 

The brains of older adults seem to have more aperiodic activity than those of younger adults, Voytek found in a 2015 study with his doctoral adviser Robert Knight, a professor of neuroscience at Berkeley. Brain’s ‘Background Noise’ May Hold Clues to Persistent Mysteries |Elizabeth Landau |February 8, 2021 |Quanta Magazine 

Last month, as his position soared above $47 million, the user was unmasked as Keith Gill, a 34-year-old certified financial adviser in Massachusetts. How the rich got richer: Reddit trading frenzy benefited Wall Street elite |Douglas MacMillan, Yeganeh Torbati |February 8, 2021 |Washington Post 

On the following Monday, it was arranged by my Resident Adviser that I would meet with the dean of students, Robert Canevari. I Was Gang Raped at a UVA Frat 30 Years Ago, and No One Did Anything |Liz Seccuro |December 16, 2014 |DAILY BEAST 

Whitaker is not only a close friend of the president and the first lady but of senior adviser Valerie Jarrett, as well. Obama’s Golf Buddy May Be a ‘Hostile Witness’ in Chicago Corruption Case |Ben Jacobs |December 3, 2014 |DAILY BEAST 

General Brent Scowcroft, national security adviser to the first President Bush, shuffled head-down through the mob. Kissy-Face The Nation: Washington’s Power Elite Smooch Bob Schieffer |Lloyd Grove |November 18, 2014 |DAILY BEAST 

He also appointed a very experienced intelligence chief, Ajit Doval as his national security adviser. ICYMI: India-Pakistan Head for Nuke War |Bruce Riedel |October 20, 2014 |DAILY BEAST 

But when the information was brought up with the White House, senior adviser Karl Rove told them to “let these sleeping dogs lie.” Insiders Blame Rove for Covering Up Iraq’s Real WMD |Eli Lake |October 16, 2014 |DAILY BEAST 

His services as witness and expert adviser were in great request by railway companies. Fifty Years of Railway Life in England, Scotland and Ireland |Joseph Tatlow 

Henry Burns smiled most affably, as though the squire had been his dearest friend and adviser. The Rival Campers |Ruel Perley Smith 

He lived in a house belonging to her, the hotel de Soissons; she made him her supreme adviser. Catherine de' Medici |Honore de Balzac 

The sheik, panic-struck, came in and made submission, revealing the treachery of the ranee's paramour and adviser. The History of England in Three Volumes, Vol.III. |E. Farr and E. H. Nolan 

In this latter work his success was very moderate, but he became unconsciously an intimate friend and adviser of the Czar. The Life of Napoleon Bonaparte |William Milligan Sloane