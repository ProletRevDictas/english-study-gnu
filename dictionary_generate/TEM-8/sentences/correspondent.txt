The question may have seemed gimmicky to some, but as NBC News political correspondent Sahil Kapur noted, familiarity with local agricultural concerns has been important to Iowa voters in the past. Joni Ernst didn’t know the price of soybeans. Here’s why that could cost her. |Benjamin Rosenberg |October 16, 2020 |Vox 

Pottinger had served as a Marine intelligence officer and worked in China as a correspondent for The Wall Street Journal. Inside the Fall of the CDC |by James Bandler, Patricia Callahan, Sebastian Rotella and Kirsten Berg |October 15, 2020 |ProPublica 

Conversation editor Nabeelah Shabbir at The English-language Correspondent is tasked with driving interaction between its over 50,000 paying members and the site’s five full-time journalists, who it calls correspondents, and freelancers. ‘It’s on the writers’: How The Correspondent drives interaction between members and its journalists |Lucinda Southern |October 14, 2020 |Digiday 

Banks have hired thousands of employees to beef up anti-money laundering and financial crime teams, and some withdrew from certain countries and dropped correspondent-banking ties with hundreds of smaller lenders. Global bank stocks tumble following ‘FinCEN files’ revelations |Bernhard Warner |September 21, 2020 |Fortune 

Ginger Thompson, senior reporter, has been promoted to chief of correspondents, reporting to Managing Editor Robin Fields. ProPublica Announces Six Staff Promotions, Creates New Masthead Team |by ProPublica |September 8, 2020 |ProPublica 

The correspondent does a stand-up next to a burning pile of heroin and gets a taste of its effect. BBC Reporter Gets High On The Job |Jack Holmes, The Daily Beast Video |December 23, 2014 |DAILY BEAST 

That good fortune meant CNN had the only TV correspondent on the scene. CNN's Overnight Sydney Star |Lloyd Grove |December 16, 2014 |DAILY BEAST 

There she met Janet Flanner, who would become a famed New Yorker correspondent “Genet”—for three decades. The Bookstore That Bewitched Mick Jagger, John Lennon, and Greta Garbo |Felice Picano |December 16, 2014 |DAILY BEAST 

Booker plans to spend his Thanksgiving dinner with CBS correspondent Gayle King and their families. Talking Tofurky With Newly Vegan Cory Booker |Vlad Chituc |November 26, 2014 |DAILY BEAST 

Every artist-correspondent and writer-correspondent who could possibly get permission to be there, was there. Blood in the Sand: When James Jones Wrote a Grunt’s View of D-Day |James Jones |November 15, 2014 |DAILY BEAST 

He was long a correspondent of the National Intelligencer and other papers, residing in Virginia. The Every Day Book of History and Chronology |Joel Munsell 

Such is the opinion of this Correspondent to the Times, and it is doubtless the opinion of a fair and just majority. Punch, or the London Charivari, Volume 107, November 3, 1894 |Various 

Your correspondent Erica gives us some quotations and epitaphs, in which the metaphor of an Inn is applied both to life and death. Notes and Queries, Number 177, March 19, 1853 |Various 

The success of his imitation of Coleridge's style is proved by the indignation of your correspondent. Notes and Queries, Number 177, March 19, 1853 |Various 

We accept with thanks the polite offer made by our Correspondent in his postscript. Notes and Queries, Number 177, March 19, 1853 |Various