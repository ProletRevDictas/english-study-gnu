News teams streamed into Butte County for months after the blaze, telling and retelling the gut-wrenching tales of those who survived and those who didn’t. A New Book Examines What We Lost in the Camp Fire |klindsey |August 24, 2021 |Outside Online 

We know this—and we have known it for decades—because it’s a story Hollywood keeps retelling. Netflix Horror Noir Brand New Cherry Flavor Tastes Terrible |Judy Berman |August 6, 2021 |Time 

Family lore, told and retold, can be a fuzzy thing, but some memories about his father, like their first time fishing together, remained spectacularly vivid and personal. The backstory for ‘A River Runs Through It’ has arrived, 45 years later |Nick Ehli |June 3, 2021 |Washington Post 

At no point in time did we want to remake the older films or to retell a story that has been told a bunch before. Here’s why the new ‘Mortal Kombat’ movie stars a new, original lead character |Gene Park |April 21, 2021 |Washington Post 

Elder’s journey will be retold this week because of his appearance at Augusta National. Lee Elder made history at the Masters. He made an impact in Washington. |Barry Svrluga |April 8, 2021 |Washington Post 

Of course, the only reason we retell the story is precisely the data did corroborate Einstein's theory. How Do We Know a Theory is Correct? |David Frum |April 18, 2013 |DAILY BEAST 

The Eichmann trial was as an opportunity to retell the story of the Holocaust to Israelis—and to the world. New York Needs a Terror Trial |Karen Greenberg |February 12, 2010 |DAILY BEAST 

Then she had to retell, perhaps for the hundredth time, the story of the hand-bag and the linen chest, but her eyes closed. The Song of Songs |Hermann Sudermann 

Many of his poems describe country life in New England; others retell old stories of pioneer days. Story Hour Readings: Seventh Year |E.C. Hartwell 

These have been so perfectly told by great writers that to retell them would seem absurd. A Book of Myths |Jean Lang 

The tale is too long to retell here but it is undeniably thrilling and good reading. Castles and Chateaux of Old Burgundy |Francis Miltoun 

To the Teacher: Have the children retell the story of the picture. Stories Pictures Tell |Flora Carpenter