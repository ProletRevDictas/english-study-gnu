Even though upward of one-third of the US population may have already been exposed to the virus, we don’t fully know who has had it because there are so many asymptomatic cases, and because of gaps in testing. Covid-19 vaccines are great — but you still need to wear a mask for now |Umair Irfan |February 9, 2021 |Vox 

A wispy, spirit-like version of herself falls away from her body as her outstretched arm and eyes point upward. Peloton makes toning your glutes feel spiritual. But should Jesus be part of the experience? |Michelle Boorstein |February 5, 2021 |Washington Post 

If case counts in an area begin to tick upward, public-health departments should be ready to respond quickly with vaccination and testing campaigns, Popescu says. We May Never Eliminate COVID-19. But We Can Learn to Live With It |Jamie Ducharme |February 4, 2021 |Time 

In the lab box tests, spiders had to add strand after strand before researchers saw the first upward lurch. How a tiny spider uses silk to lift prey 50 times its own weight |Susan Milius |February 3, 2021 |Science News 

She has been depositing upward of $250 a month for the last six months into both of my daughters' accounts. Carolyn Hax: Hesitant to plunge into the online dating pool |Carolyn Hax |February 1, 2021 |Washington Post 

Officials have said the war to reclaim upward of a third of Iraq and a quarter of Syria from ISIS could take years. Pentagon Doesn’t Know How Many People It’s Killed in the ISIS War |Nancy A. Youssef |January 7, 2015 |DAILY BEAST 

Their clear priorities : faster economic growth and promoting upward mobility for the middle and working classes. Time to Bring Back the Truman Democrats |Joel Kotkin |December 21, 2014 |DAILY BEAST 

For fees ranging upward of $100, the officiant is the person who makes a prison marriage happen. Saying Yes to the Dress—Behind Bars |Caitlin Dickson |December 8, 2014 |DAILY BEAST 

Constitutional arguments aside, there do seem to be some better ways to create conditions for upward mobility among newcomers. Legal but Still Poor: The Economic Consequences of Amnesty |Joel Kotkin |November 21, 2014 |DAILY BEAST 

Unlike a normal ship, the bow slopes upward from the water up to the deck. Can the Navy's $12 Billion Stealth Destroyer Stay Afloat? |Dave Majumdar |October 22, 2014 |DAILY BEAST 

Both of the orator's hands swung upward and outward, and he looked intently at the ceiling. The Soldier of the Valley |Nelson Lloyd 

The unsupported pump-rods fell downwards, setting in upward motion the column of water in the plunger-pole pumps. Life of Richard Trevithick, Volume II (of 2) |Francis Trevithick 

As the organist closes his pedal, the indicator key again moves upward into its normal position. The Recent Revolution in Organ Building |George Laing Miller 

Even Henrietta stopped eating, looked upward at the dusty ceiling, and listened for a repetition of the sound. The Campfire Girls of Roselawn |Margaret Penrose 

Pallid, Sir Lucien Pyne lay by the ebony chair glaring horribly upward. Dope |Sax Rohmer