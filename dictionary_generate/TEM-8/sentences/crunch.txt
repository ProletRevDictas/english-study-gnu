There was, however, that pleasant, satisfying crunch, like taking a chunk out of a perfectly crisp apple, and that miniscule bit of toasted, sometimes sweet, sometimes salty, flavor mixed in with a slurry of desiccated rice matter. The Rise and Fall of the Rice Cake, America’s One-Time Favorite Health Snack |Brenna Houck |September 17, 2020 |Eater 

Stepien earlier had slashed spending on television ads, fearing a cash crunch that could leave the campaign in financial trouble in its final 30 days. Trump and his campaign try to allay concerns about trailing Biden in television ads |Michael Scherer, Josh Dawsey |September 17, 2020 |Washington Post 

Ziff Davis, which owns publications including PC Mag and Mashable, saw the strong performance they’ve seen on products such as laptops continue, even amid an economic crunch, vp of partner development Jessica Spira said. Publishers prep for back-to- school bump as traditional shopping models and items shift due to uncertain return this fall |Max Willens |July 29, 2020 |Digiday 

These hundred-million-dollar machines usually run on hundreds of thousands of processors, occupy warehouse floors, gobble up copious amounts of energy, and crunch numbers at an ungodly pace. The World’s New Fastest Supercomputer Is an Exascale Machine for AI |Jason Dorrier |June 25, 2020 |Singularity Hub 

Doritos are prototyped in a lab by material scientists looking at different dimensions of like crunch and torsion and all these other sort of mechanical properties. The Future of Meat (Ep. 367 Rebroadcast) |Stephen J. Dubner |August 29, 2019 |Freakonomics 

The low crunch of packed dirt against rubber tire was overwhelmed by the ragged explosions of automatic gunfire. A Belgian Prince, Gorillas, Guerrillas & the Future of the Congo |Nina Strochlic |November 6, 2014 |DAILY BEAST 

The stories are told through the eyes of two fishing guides, Crunch and Des, with a spare grace and humor that stand strong. Book Bag: Overlooked Classic Books From the Sunshine State |Randy Wayne White |September 30, 2014 |DAILY BEAST 

The tomatoes' crunch and tang add new dimensions of delight. The Ultimate Southern Cheeseburger Created in South Carolina |Jane & Michael Stern |August 10, 2014 |DAILY BEAST 

The dogs have been fried in soybean oil until their exterior skin begins to develop a sensuous crunch. The Jersey Shore’s Biggest Weiners Are at Jimmy Buff’s |Jane & Michael Stern |June 15, 2014 |DAILY BEAST 

Look for at least three grams of fiber and three grams of protein for a more satisfying crunch. How to Buy Gluten-Free Without Getting Duped |DailyBurn |April 12, 2014 |DAILY BEAST 

The shouts of teamsters and the crack of whips punctuated the crunch of wheels as our wagons swiftly swung again into stockade. The Way of a Man |Emerson Hough 

A crunch of footsteps could be heard outside, cautiously approaching the barn door. Motor Matt's Mystery |Stanley R. Matthews 

The lawyer took a cup, sugared it, and drank it, after having crumbled into it a little cake which was too hard to crunch. The Works of Guy de Maupassant, Volume VIII. |Guy de Maupassant 

There was a sickening crunch of bone as giant fangs closed on the face of the struggling figure, and Mog, the sullen, was no more. Warrior of the Dawn |Howard Carleton Browne 

I can feel my bones crunch in their big mouths and see them lick their chops after they have eaten us. Billy Whiskers' Adventures |Frances Trego Montgomery