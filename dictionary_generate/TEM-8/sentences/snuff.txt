He could imagine himself an Islamist avenger like that masked monster in black who appears in the ISIS snuff videos. The Muslim Convert Behind America’s First Workplace Beheading |Michael Daly |September 27, 2014 |DAILY BEAST 

He packed a large pinch of the snuff against his bottom gum. Short Stories from The Daily Beast: Four Hundred Grand |Elliot Ackerman |July 6, 2014 |DAILY BEAST 

Even as Valle sat in court this week, the 364 various groups on DFN included one called Cannibalism, Snuff with 835 members. Cannibal Cop’s Dark Fetishes Detailed in Grisly Trial Testimony |Michael Daly |February 27, 2013 |DAILY BEAST 

Look in my face while I snuff the sidle of evening, Talk honestly, for no one else hears you, and I stay only a minute longer. A Eulogy for Marie Colvin |Katrina Heron |March 14, 2012 |DAILY BEAST 

Pelosi and Reid will work to snuff out any legislation that could force Obama to take unpopular stands. Freshmen Hit the Hill |Samuel P. Jacobs |January 4, 2011 |DAILY BEAST 

Here are pretty goings on—a pinch of your snuff, Perker, my boy—never were such times, eh? The Posthumous Papers of the Pickwick Club, v. 2(of 2) |Charles Dickens 

Joe,” said Mr. Wardle, after an unsuccessful search in all his pockets, “is my snuff-box on the sofa? The Posthumous Papers of the Pickwick Club, v. 2(of 2) |Charles Dickens 

Some of the tribes inhabiting the district of the lower Amazon indulge in snuff-taking. Tobacco; Its History, Varieties, Culture, Manufacture and Commerce |E. R. Billings. 

Portuguese snuff seemed to be in favor and was delicately perfumed. Tobacco; Its History, Varieties, Culture, Manufacture and Commerce |E. R. Billings. 

Some varieties of snuff were named after the scents employed in flavoring them. Tobacco; Its History, Varieties, Culture, Manufacture and Commerce |E. R. Billings.