Such systems need only a trickle of power to smell smoke or detect rising heat, he says. Trees power this alarm system for remote forest fires |Stephen Ornes |October 16, 2020 |Science News For Students 

Before that, the water just wasn’t there—or if it was, perhaps it was a trickle or a stream. How the truth was murdered |Abby Ohlheiser |October 7, 2020 |MIT Technology Review 

The cash spigot that had been flowing to startups throughout Magic Leap’s lifetime slowed to a trickle this year when the coronavirus pandemic struck. Magic Leap tried to create an alternate reality. Its founder was already in one |Verne Kopytoff |September 26, 2020 |Fortune 

A plant satiated on NPK fertilizers reaches for the exudate tap with leafy hands and turns it down to a trickle. Junk Food Is Bad For Plants, Too - Issue 90: Something Green |Anne Bikl&#233; & David R. Montgomery |September 23, 2020 |Nautilus 

Since the onset of the pandemic, each day has brought a slow trickle of restaurant closures, but now, they’re coming in waves. Is the Government Just Going to Watch the Restaurant Industry Die? |Elazar Sontag |August 28, 2020 |Eater 

Throughout the late 1960s and 1970s, many old Nazis managed discreetly to trickle back to what they regarded as the Fatherland. Hitler’s Henchmen in Arabia |Guy Walters |December 7, 2014 |DAILY BEAST 

In the meantime, much of the book is already available online, and scholarly criticism has already started to trickle in. Jesus Christ, Baby Daddy? |Candida Moss |November 12, 2014 |DAILY BEAST 

Months after the president stepped in to save the Yazidis from genocide, the airstrikes have slowed to a trickle. Yazidis Face Genocide by ISIS After U.S. Turns Away |Josh Rogin |November 4, 2014 |DAILY BEAST 

A small trickle of donations from friends and family, handled by a church in Indiana, was his main source of funding. ISIS Thugs Behead Peter Kassig |Nick Schwellenbach |October 4, 2014 |DAILY BEAST 

In days of yore, blood on screen was to be feared: think the trickle of blood signaling defilement in old vampire movies. Sex, Blood and Maroon 5: Pop Culture’s Wounds Run Deep |Lizzie Crocker |October 3, 2014 |DAILY BEAST 

I laved his pain-twisted face with the cool water and let a few drops trickle into his open mouth. Raw Gold |Bertrand W. Sinclair 

The trickle of water sounded very pleasant to all the children as they lay down once more to drink. The Box-Car Children |Gertrude Chandler Warner 

Beardsley savored the thought tastily, and let it trickle away, and the look of glee on his cherubic face was gone. We're Friends, Now |Henry Hasse 

He looked down at his hand, where two long red scratches oozed a trickle of blood. The Medici Boots |Pearl Norton Swet 

These drops of water trickle to the floor, and occasionally the exuded white matter falls. The Indian in his Wigwam |Henry R. Schoolcraft