This is a great chance to step out of the supporting-someone-else role and shine on her own. Meet Cathy Russell, Obama’s New Champion for Women |Eleanor Clift |March 21, 2013 |DAILY BEAST 

And there are also a few obvious-to-everybody-else explanations for the tendency. The Impressive Hypocrisy of the GOP |Eric Alterman |April 9, 2009 |DAILY BEAST 

Everything else-even endowments given by private persons a few years before the Act was passed-was swept away. Is Ulster Right? |Anonymous 

He wandered else-whither, and came to something afterwards, poor Spaen. History Of Friedrich II. of Prussia, Vol. VII. (of XXI.) |Thomas Carlyle 

You deliberately set out to marry, or else-how tie some emotional cable onto me. Highways in Hiding |George Oliver Smith 

This way shalt thou leave, other-else thou shalt go it on thy foot, for wit thou well thy horse shall be slain. Le Morte D'Arthur, Volume II (of II) |Thomas Malory 

Many of the original settlers have died; yet, like people else-where, their offspring outnumber those deceased. The Papers And Writings Of Abraham Lincoln, Volume Six |Abraham Lincoln