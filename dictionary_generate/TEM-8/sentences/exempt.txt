In his ruling, McKenna said the city needed to either license the units or formally exempt the housing authority from inspections through legislation. She Was Sued Over Rent She Didn’t Owe. It Took Seven Court Dates to Prove She Was Right. |by Danielle Ohl, Capital Gazette, and Talia Buford and Beena Raghavendran, ProPublica |August 25, 2020 |ProPublica 

Travel to and from Maryland and Virginia is exempt from the mayor’s order. Delaware returned to D.C. ‘high-risk states’ list |Lou Chibbaro Jr. |August 24, 2020 |Washington Blade 

Neighboring cities, like Carlsbad, Escondido and Oceanside, have long argued that emails should be exempt from the state’s two-year minimum, so they also destroy emails on an earlier time table. North County Report: Why We’re Suing Solana Beach |Kayla Jimenez |August 19, 2020 |Voice of San Diego 

There is evidence, for example, that many departments buy their technology using federal grants or nonprofit gifts, which are exempt from certain disclosure laws. There is a crisis of face recognition and policing in the US |Tate Ryan-Mosley |August 14, 2020 |MIT Technology Review 

State law requires local agencies to hang onto public records for at least two years, but many have argued that emails should be exempt. We’re Suing for COVID-19 Data |Scott Lewis and Jesse Marx |August 14, 2020 |Voice of San Diego 

Surely, Hollywood should not be exempt from such a standard. It’s Not Just Cosby: Hollywood’s Long List of Male Scumbags |Asawin Suebsaeng |November 19, 2014 |DAILY BEAST 

Individuals and businesses could exempt themselves from anti-discrimination laws by proffering religious objections to them. RFRA Madness: What’s Next for Anti-Democratic ‘Religious Exemptions’ |Jay Michaelson |November 16, 2014 |DAILY BEAST 

State and federal laws generally exempt religious institutions from having to perform gay marriages. Refusing to Marry Same-Sex Couples Isn’t Religious Freedom, It’s Just Discrimination |Sally Kohn |October 23, 2014 |DAILY BEAST 

But having reached something of a compromise, the IRS approved the school as a tax-exempt nonprofit in March 2002. At This Creepy Libertarian Charter School, Kids Must Swear ‘to Be Obedient to Those in Authority’ |ProPublica |October 15, 2014 |DAILY BEAST 

The groups, nonprofits exempt from paying taxes, are not required to disclose their donors in Kansas and most other states. Millions in Dark Money Has Taken Over the Airwaves in Kansas |Center for Public Integrity |October 9, 2014 |DAILY BEAST 

He had discovered that the all-glorious boast of Spain was not exempt from the infirmities of common men. The Pastor's Fire-side Vol. 3 of 4 |Jane Porter 

The federal Bankruptcy Act prescribes what property passes to the trustee and also what is exempt. Putnam's Handy Law Book for the Layman |Albert Sidney Bolles 

Although exempt from concupiscence and "full of grace," she was so distrustful of herself as if she were in continual danger. Mary, Help of Christians |Various 

Is she more exempt from egotism, does she dislike others less, and has she fewer worldly affections? The 'Characters' of Jean de La Bruyre |Jean de La Bruyre 

The interests of the savings deposits are in like manner exempt from the Income Tax and from any future tax coming in its place. Social Comptabilism, Cheque and Clearing Service &amp; Proposed Law |Ernest Solvay