The minute you start stimulating the brain, you are going to be changing people’s minds. People are concerned about tech tinkering with our minds |Laura Sanders |February 11, 2021 |Science News For Students 

“I started learning patients’ minute-renewal schedules,” Winford said in an interview. Lacking a Lifeline: How a federal effort to help low-income Americans pay their phone bills failed amid the pandemic |Tony Romm |February 9, 2021 |Washington Post 

Its maximum print speed is 35 pages per minute and features auto-duplex printing and a color touchscreen display that will connect your scans to Google Drive, Dropbox, Facebook, OneDrive, and more. Best all-in-one printer: Upgrade your home office with these multitasking machines |Carsen Joenk |February 8, 2021 |Popular-Science 

One problem with Fake Famous is that, clocking in at under 90 minutes, it barely gives viewers a sense of what the subjects are like as people. What HBO’s Fake Famous Doesn’t Understand About Young People and Influencer Culture |Judy Berman |February 4, 2021 |Time 

Regulators and politicians have questioned their growth and data collection, and their power over the most minute aspects of people’s lives. With Bezos out as Amazon CEO, Zuckerberg is the last man standing |Elizabeth Dwoskin |February 3, 2021 |Washington Post 

Whatever happened overtook them both within a minute or so of that altitude change request, and they were never heard from again. Flight 8501 Poses Question: Are Modern Jets Too Automated to Fly? |Clive Irving |January 4, 2015 |DAILY BEAST 

“The play contains one five minute scene about James Hewitt,” Conway says. Harry’s Daddy, and Diana’s ‘Murder’: Royal Rumors In a New Play |Tom Sykes |January 4, 2015 |DAILY BEAST 

I did a ten minute scene in his class: the guy who had gangrene in his leg in The Snows of Kilimanjaro. The Story Behind Lee Marvin’s Liberty Valance Smile |Robert Ward |January 3, 2015 |DAILY BEAST 

Could you talk a minute about the notion of being an unreliable narrator? Daphne Merkin on Lena Dunham, Book Criticism, and Self-Examination |Mindy Farabee |December 26, 2014 |DAILY BEAST 

“The beginning of that piece is one minute of cellos and violas,” he says. ‘Mozart in the Jungle’: Inside Amazon’s Brave New World of Sex, Drugs, and Classical Music |Kevin Fallon |December 23, 2014 |DAILY BEAST 

After a minute's pause, while he stood painfully silent, she resumed in great emotion. The Pastor's Fire-side Vol. 3 of 4 |Jane Porter 

I assure you, no matter how beautifully we play any piece, the minute Liszt plays it, you would scarcely recognize it! Music-Study in Germany |Amy Fay 

By the time I had done my toilette there was a tap at the door, and in another minute I was in the salle--manger. Blackwood's Edinburgh Magazine, Volume 60, No. 372, October 1846 |Various 

The remaining one struggled for another half-minute, and flared up in one last, desperate effort. The Boarded-Up House |Augusta Huiell Seaman 

Words are often everywhere as the minute-hands of the soul, more important than even the hour-hands of action. Pearls of Thought |Maturin M. Ballou