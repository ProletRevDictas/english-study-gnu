Engineers must first spend several days setting up each stadium that will use the system. A robot referee can really keep its ‘eye’ on the ball |Kathryn Hulick |August 20, 2020 |Science News For Students 

The district has also spent more than $100 million in recent years upgrading and creating fields and stadiums with bond money. The Learning Curve: The Case for Outdoor School in San Diego |Will Huntsberry |July 30, 2020 |Voice of San Diego 

So while stadiums might have been quiet on opening day, in 2020, social media certainly wasn’t. As live sports roar back onto screens, brands capture a social-media lift |Twitter |July 30, 2020 |Digiday 

According to a collection of stadium profiles at Clem’s Baseball, most fans were never more than 60 feet above the playing surface at the old Tiger Stadium in Detroit. MLB’s Newest Ballpark Is A Shift Away From Retro-Era Stadiums |Travis Sawchik |July 16, 2020 |FiveThirtyEight 

The idea is to stand up a temporary 15,000-seat stadium immediately on the parking lot while everything else is cooking. Politics Report: What Comes Next for Sports Arena |Scott Lewis and Andrew Keatts |July 11, 2020 |Voice of San Diego 

In fact, he's not even high on the list of NFL players one jerks off too during halftime at Gillette Stadium. ‘A Gronking to Remember’ Speed Read: 8 Naughtiest Bits |Emily Shire |January 7, 2015 |DAILY BEAST 

The Dallas Cowboys sell out their state-of-the art football stadium. Will Texas Stay Texan? |David Fontana |December 29, 2014 |DAILY BEAST 

It was around noon that Brinsley chucked the phone behind a radiator at the basketball stadium and went off the grid. Alleged Cop Killer Ismaaiyl Brinsley Had a Death Wish |M.L. Nestel |December 22, 2014 |DAILY BEAST 

The flag that was unveiled at Yankee Stadium 19 days after 9/11 was a different, much larger one. Brad Meltzer's Passion For Reuniting America With Its Historic Objects |Oliver Jones |November 7, 2014 |DAILY BEAST 

A winning team may pack the stadium, but you need that packed stadium to get top recruits and sustain victories. How The University of Wisconsin Badgers Are Bucking the Big Ten Ticket Flop |Brian Weidy |October 31, 2014 |DAILY BEAST 

Mithridates discharged an arrow from the angle of the roof, and supposed that it fell a little beyond the distance of a stadium. The Geography of Strabo, Volume III (of 3) |Strabo 

It was a quadrangular pyramid of baked brick, a stadium in height, and each of the sides a stadium in length. The Geography of Strabo, Volume III (of 3) |Strabo 

For the river, which is a stadium in breadth, flows through the middle of the city, and the garden is on the side of the river. The Geography of Strabo, Volume III (of 3) |Strabo 

One of the servants and flatterers of Agathocles, whose name was Philo, came out to the stadium still flustered with wine. The Histories of Polybius, Vol. II (of 2) |Polybius 

As soon as they had got the king, the Macedonians placed him on a horse and conducted him to the stadium. The Histories of Polybius, Vol. II (of 2) |Polybius