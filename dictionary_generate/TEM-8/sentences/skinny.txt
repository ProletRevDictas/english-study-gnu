On the morning conference call with the House Democratic caucus Pelosi rejected the notion of a slimmed-down, or “skinny,” bill, such as the $300 billion measure Democrats blocked last week in the Senate. Frustrated House Democrats push for action on new economic relief bill |Erica Werner |September 15, 2020 |Washington Post 

The Republican’s “skinny” stimulus package did not include funding for a second round of $1,200 stimulus checks—a proposal that for months Republican and Democratic leaders supported. GOP $500 billion aid package blocked by Senate Democrats—here’s what it means for unemployment benefits and stimulus checks |Lance Lambert |September 10, 2020 |Fortune 

A big ECB meeting, jobless claims numbers and a largely meaningless Senate vote on a “skinny” stimulus spending package. Global markets dip as investors again sour on tech stocks |Bernhard Warner |September 10, 2020 |Fortune 

In addition, the Republican’s skinny stimulus does not provide additional funding for state and local governments. ‘Skinny stimulus’: Senate to vote on $500 billion package. What it does and doesn’t include |Lance Lambert |September 9, 2020 |Fortune 

There are over a dozen races worth watching today, but here’s the skinny on the most consequential. What You Need To Know About Today’s Elections In Kansas, Michigan And Missouri |Nathaniel Rakich (nathaniel.rakich@fivethirtyeight.com) |August 4, 2020 |FiveThirtyEight 

In the most recent image posted by her mother, Kristina is wearing a black vest over her skinny shoulders. Is 9-Year-Old Russian Model Kristina Pimenova Too Sexualized? |Anna Nemtsova |December 12, 2014 |DAILY BEAST 

The whims of fashionistas have brought us bellbottoms and skinny jeans and everything in between. 100 Years of Beauty Styles in 1 Minute |Jack Holmes, The Daily Beast Video |December 3, 2014 |DAILY BEAST 

Instead, break out a form-fitting garment — think skinny jeans or a curve-hugging dress. 12 Thanksgiving Weight Loss Tips That Actually Work |DailyBurn |November 27, 2014 |DAILY BEAST 

The man with the skinny tie tells me that the comic book is transported in a manila envelope. The Holy Grail of Comic Books Hid in Plain Site at New York Comic Con |Sujay Kumar |October 14, 2014 |DAILY BEAST 

Skinny tie tells me that it was found in a storage locker in 2011 and auctioned for $2.1 million. The Holy Grail of Comic Books Hid in Plain Site at New York Comic Con |Sujay Kumar |October 14, 2014 |DAILY BEAST 

“Why, to look at her, I think that skinny woman would dare anything,” remarked Amy. The Campfire Girls of Roselawn |Margaret Penrose 

She was thin, skinny, dark-haired, and possessed of great physical strength in the form of endurance. Three More John Silence Stories |Algernon Blackwood 

Burke pulled a packet of bills from his pocket, slipped them to Lefty's skinny hand. Hooded Detective, Volume III No. 2, January, 1942 |Various 

At this the door flew open and a skinny woman appeared, her homespun frock pendent with tow-headed urchins. Famous Adventures And Prison Escapes of the Civil War |Various 

Her back was turned to Nita, her clawlike, skinny hands were diving into the chest of gold. They Looked and Loved |Mrs. Alex McVeigh Miller