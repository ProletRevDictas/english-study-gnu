Schwarze believes that “every aspect of science has a home in what we do as geologists and geophysicists.” The Women Reshaping the C-Suite |Nick Fouriezos |January 24, 2021 |Ozy 

We do not yet understand everything we have seen, but geologists will be poring over this data for years to come, using it to tease out clues to the history of the Martian environment at this location. InSight’s heat probe has failed on Mars. Is the mission a failure? |Neel Patel |January 20, 2021 |MIT Technology Review 

Many others, particularly geologists, were unimpressed, hostile, even horrified. How the Earth-shaking theory of plate tectonics was born |Carolyn Gramling |January 13, 2021 |Science News 

Cruelly, the first was a geologist who flew on Apollo 17, only to arrive on the moon and realize he was allergic to the very thing he studied. Imagine traveling to the moon only to realize you’re allergic to it. One astronaut did. |PopSci Staff |November 25, 2020 |Popular-Science 

It allowed Darwin to propose his theory of evolution, geologists to carbon-date the true age of Earth, and physicists to simulate the expansion of the universe. Humanity is stuck in short-term thinking. Here’s how we escape. |Katie McLean |October 21, 2020 |MIT Technology Review 

My background as a geologist is a major influence on this book, as it is on many of my books. The Man Who Made America: Simone Winchester Talks New Book |Eric Herschthal |October 17, 2013 |DAILY BEAST 

He lived with her for twenty years, never telling her his real identity as a renowned geologist. The Man Who Made America: Simone Winchester Talks New Book |Eric Herschthal |October 17, 2013 |DAILY BEAST 

Bass says he “kind of identifies with” Richard, the geologist who anchors the novel. Faulkner of Oil Country: Rick Bass Talks New Novel |Jane Ciabattari |August 22, 2013 |DAILY BEAST 

The geologist David R. Montgomery set out to write a “straightforward refutation of creationism.” How Noah’s Flood Spurred Science: David R. Montgomery’s ‘The Rocks Don’t Lie’ |David Sessions |August 28, 2012 |DAILY BEAST 

Geologist David R. Montgomery set out to disprove the Christian creationist account of the great flood. How Noah’s Flood Spurred Science: David R. Montgomery’s ‘The Rocks Don’t Lie’ |David Sessions |August 28, 2012 |DAILY BEAST 

But how does our infidel geologist set about his work of proving that the earth has any given age, say a thousand million years? Gospel Philosophy |J. H. Ward 

Thus in the Laurentian Lakes above Ontario the geologist finds evidence that the drainage lines have again and again been changed. Outlines of the Earth's History |Nathaniel Southgate Shaler 

Mitchell, a North Carolina geologist, was killed by a fall into the Caney river, while engaged alone in scientific explorations. The Every Day Book of History and Chronology |Joel Munsell 

I freed myself to speak to the geologist who seemed eager to be on his way. Jane Journeys On |Ruth Comfort Mitchell 

I may not look quite so forlorn as the geologist did, but I feel it. Jane Journeys On |Ruth Comfort Mitchell