Mustard is working to supplant — or at least augment — some of that pricey coaching with the launch of a new mobile app designed to analyze an athlete’s mechanics and offer corrective tips to help them improve. Mustard raises $1.7M to improve athletic mechanics with AI |Brian Heater |September 4, 2020 |TechCrunch 

In addition to the vortex work, he and his students have been busy exploring “topological mechanics,” which involves teasing out strange, quantum-like properties in systems composed of a large number of identical rotating objects. An Unexpected Twist Lights Up the Secrets of Turbulence |David H. Freedman |September 3, 2020 |Quanta Magazine 

Baggott’s obsession with quantum mechanics is not driven by public demand. Your Guide to the Many Meanings of Quantum Mechanics - Facts So Romantic |Sabine Hossenfelder |September 3, 2020 |Nautilus 

Sabine Hossenfelder is a Research Fellow at the Frankfurt Institute for Advanced Studies where she works on modifications of general relativity, phenomenological quantum gravity, and the foundations of quantum mechanics. Your Guide to the Many Meanings of Quantum Mechanics - Facts So Romantic |Sabine Hossenfelder |September 3, 2020 |Nautilus 

Afterward, he is willing to rely on brute mechanics to outplay enemies, by bringing in a completely unorthodox chain of moves which give him the advantage. An Unlikely Esports Star Emerges From Pakistan |Daniel Malloy |August 21, 2020 |Ozy 

A car mechanic who goes by the name “Big Perm” said he noticed a change in the neighborhood. Ground Zero of the NYPD Slowdown |Batya Ungar-Sargon |January 1, 2015 |DAILY BEAST 

One trucker did shout an obscenity, and a musclebound mechanic told them to go and do something useful like study. Mexican Protesters Look to Start a New Revolution |Jason McGahan |November 21, 2014 |DAILY BEAST 

Sure, the story makes no sense, but look how tight that shooting mechanic is! The Cake Is a Lie: Sexism Isn’t a Boss Gamer Girls Can Beat |Emily V Gordon |July 8, 2014 |DAILY BEAST 

It plugs into the same port that a mechanic uses to check the computer, and it works on most cars made after 1996. Testing Automatic Link, the FitBit for Your Car |Jamie Todd Rubin |July 8, 2014 |DAILY BEAST 

Even the most sketch-ball, scheming car mechanic knows how much those brake pads cost. ‘Code Black’: An M.D. on How to Fix Our Emergency Room Crisis |Ryan McGarry |June 20, 2014 |DAILY BEAST 

You will find these two traits in every grade of Scotch life—in tradesman, mechanic, and peasant. Friend Mac Donald |Max O'Rell 

Mr. Parsons lost no time; and, skilled mechanic that he was, commenced his work at once. The Pit Town Coronet, Volume II (of 3) |Charles James Wills 

All the money saved by this skillful mechanic and his thrifty wife was gone, and this girl had to go to work. The Leaven in a Great City |Lillian William Betts 

Besides being the best boss mechanic in the West, he was a first-class fighting man, with a clear head and nerve to burn. The Wreckers |Francis Lynde 

I left the master-mechanic at the door of a Greek eat-shop that he patronized and went on up to the Bullard. The Wreckers |Francis Lynde