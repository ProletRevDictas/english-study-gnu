The Singapore launch was pegged for the first half of this year, pushed back due to coronavirus. ‘We’re about hiring journalists’: Insider Inc. launches third global news hub in Singapore |Lucinda Southern |September 17, 2020 |Digiday 

The University of Washington’s Institute on Health Metrics estimates that there will be nearly 413,000 deaths by the end of the year. Trump blames blue states for the coronavirus death toll — but most recent deaths have been in red states |Philip Bump |September 16, 2020 |Washington Post 

To best compare today’s StarTech to its past versions, I look at its metrics on September 30 each year from 2015 to 2019, and those numbers as of September 15 of this year. Will tech stocks stumble or slide? What the fundamentals tell us |Shawn Tully |September 16, 2020 |Fortune 

No chief executive “should sit in their chair for dozens of years,” she says. Why one of the world’s few female bank CEOs decided to step down |Claire Zillman, reporter |September 16, 2020 |Fortune 

Today’s announcements won’t hit the Pro, which got an update earlier in the year. Apple just announced a new iPad, iPad Air, and Apple Watch Series 6 |Stan Horazek |September 15, 2020 |Popular-Science 

However, more than 20 players on the ballot this year were probably worthy of being enshrined in Cooperstown. Conservative Curt Says His Politics, Not His Pitching, Kept Him Out of the Hall of Fame |Ben Jacobs |January 9, 2015 |DAILY BEAST 

The influential al Qaeda propagandist, who was born in New Mexico, died in a U.S. drone strike later that year. France Kills Charlie Hebdo Murderers |Nico Hines |January 9, 2015 |DAILY BEAST 

Eric Garcetti succeeded Villaraigosa and has received high marks in his first year and a half on the job. The Golden State Preps for the ‘Red Wedding’ of Senate Races |David Freedlander |January 9, 2015 |DAILY BEAST 

Grindr introduced the feature themselves in October the same year and called it ‘tribes.’ Grindr’s Trans Dating Problem |David Levesley |January 9, 2015 |DAILY BEAST 

In the last year, her fusion exercise class has attracted a cult following and become de rigueur among the celebrity set. How Taryn Toomey’s ‘The Class’ Became New York’s Latest Fitness Craze |Lizzie Crocker |January 9, 2015 |DAILY BEAST 

In the year of misery, of agony and suffering in general he had endured, he had settled upon one theory. The Homesteader |Oscar Micheaux 

The great plague of this and the subsequent year broke out at St. Giles, London. The Every Day Book of History and Chronology |Joel Munsell 

Twice a year the formal invitation was sent out by the old nobleman to his only son, and to his two nephews. The Pit Town Coronet, Volume I (of 3) |Charles James Wills 

After about the forty-fifth year it becomes gradually less; after seventy-five years it is about one-half the amount given. A Manual of Clinical Diagnosis |James Campbell Todd 

The clink of the stone-masons' chisels had resounded year after year from morning till night. The Pit Town Coronet, Volume I (of 3) |Charles James Wills