The human Murphy did a lot of work early in his career on Komodo dragons and later published a book on reptile play behavior, using observations from the National Zoo’s Komodo dragons. The National Zoo’s wild naming inspiration, from Billy Joel to the Golden Girls |John Kelly |January 25, 2021 |Washington Post 

With the “massive smell of reptile” in there they may steer clear, he says. Monitor lizards’ huge burrow systems can shelter hundreds of small animals |Jake Buehler |January 19, 2021 |Science News 

For decades, the invasive reptiles have slithered up trees to feast upon the forest birds of Guam. These snakes wiggle up smooth poles by turning their bodies into ‘lassoes’ |Kate Baggaley |January 13, 2021 |Popular-Science 

The next year, while conducting reptile and amphibian surveys in roughly the same region, Fulgence found three more of the spiders hiding in similar leaf retreats. These spiders may sew leaves into fake shelters to lure frogs to their doom |Jake Buehler |January 4, 2021 |Science News 

The reptiles weren’t hurt, just so cold that they couldn’t move and lost their grip. From Elvis worms to the Milky Way’s edge, these science stories sparked joy in 2020 |Erika Engelhaupt |December 17, 2020 |Science News 

The new film ignored all previous sequels and put the giant reptile at the center of a cold war nuclear standoff. A Comprehensive History of Toho’s Original Kaiju (and Atomic Allegory) Godzilla |Rich Goldstein |May 18, 2014 |DAILY BEAST 

Even reptile experts are puzzled, Christine Pelisek reports. Python Murder Mystery: How Did a Snake Kill Two Young Boys? |Christine Pelisek |August 7, 2013 |DAILY BEAST 

A reptile hatching from an egg must not cry out for its mother, or else it will be readily detected by predators and eaten. Why Do We Cry? |Michael Trimble |January 10, 2013 |DAILY BEAST 

In 1915 Thorpe started also playing and coaching the “reptile sport” of professional football in Canton, Ohio. The Most Wonderful Athlete in the World: Jim Thorpe’s Story |Kate Buford |August 5, 2012 |DAILY BEAST 

Pedestrians would take turtles for walks and let the reptile set the pace. Sage Advice from Famous Dads |Bruce Feiler |June 19, 2010 |DAILY BEAST 

While searching the field Lawrence noticed some white object crawling along like a large reptile. The Courier of the Ozarks |Byron A. Dunn 

If you value your life, and that of your mother and her husband, avoid him as you would some venomous reptile. The Doctor of Pimlico |William Le Queux 

It is much more distinguished and honorable to be a reptile than a dog, dont you think, Soldier? A Horse's Tale |Mark Twain 

The reptile only seemed to await a motion on Clifford's part to strike like a flash of lightning. A Fortune Hunter; Or, The Old Stone Corral |John Dunloe Carteret 

He left the guards at the fringes of his engineers' forest and rode the eight-legged reptile recklessly among the huge trunks. The Envoy, Her |Horace Brown Fyfe