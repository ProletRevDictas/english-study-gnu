Cubans are cursed whether they find a means of escape or remain. The Life and Hard Times Of The Family A Cuban Defector Left Behind |Brin-Jonathan Butler |December 19, 2014 |DAILY BEAST 

“When Gil cursed, he cursed in certain ways that I could respect it,” Herc says. ‘The Prince of Chocolate City’: When Gil Scott-Heron Became A Music Icon |Marcus Baram |November 15, 2014 |DAILY BEAST 

In other words, gay people are cursed with deep-seated disorder and are to be treated with compassion. Catholic University’s Harvey Milk Ban Reflects A Church In Transition |Jay Michaelson |October 3, 2014 |DAILY BEAST 

Since then, the group has been cursed with outsized expectations. Interpol on the Arrogance of Believing Their Own Myth and Life After Carlos D. |Melissa Leon |September 8, 2014 |DAILY BEAST 

This infuriated his grandfather, who cursed Barry and never spoke to him again. Sebastian Barry’s Quarrel With Irish History |J.P. O’Malley |May 7, 2014 |DAILY BEAST 

Woe to the man that first did teach the cursed steel to bite in his own flesh, and make way to the living spirit. Pearls of Thought |Maturin M. Ballou 

When the man turned bad on his hands, Jahweh was angry, and cursed him and his seed for thousands of years. God and my Neighbour |Robert Blatchford 

He cursed himself inwardly for a fool and a dolt—the more pitiable because he accounted himself cunning above others. St. Martin's Summer |Rafael Sabatini 

One pipe after another have the cursed French shot away from my mouth. Tobacco; Its History, Varieties, Culture, Manufacture and Commerce |E. R. Billings. 

And was it not in imitation of some such practices, as that which he attempted, that Goliath cursed David by his gods? The Ordinance of Covenanting |John Cunningham