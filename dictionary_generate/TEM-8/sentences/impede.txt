As outcomes, elimination and endemic transmission may be opposites, but thinking that we need to choose one or the other is a mistake that can impede public understanding of how to manage the pandemic. We'll Probably Never Eliminate COVID-19 from the U.S. It's Still Worth Trying |Gavin Yamey |February 25, 2021 |Time 

The proposals weren’t enacted in any systemic way, but Levi says they underscore an ongoing worry that the typical regulations around reimbursement and cross-state practicing impede the expansion of telehealth. The doctor will Zoom you now |Katie McLean |February 24, 2021 |MIT Technology Review 

These systems typically use elastic closures around the ankle areas to keep wind and moisture out of your dog’s coat without impeding their dexterity. Best dog coats: Pet clothes to keep any dog warm |PopSci Commerce Team |February 17, 2021 |Popular-Science 

And, racial gaps also impeded access to testing and affected quality of care. One big hiccup in US efforts to distribute COVID-19 vaccines? Poor internet access. |By Tamra Burns Loeb, Et Al./The Conversation |February 10, 2021 |Popular-Science 

We have provided them thousands of pages of documents already and are not in any way impeding who they speak with. VMI resists letting investigators interview cadets, faculty without its lawyers present, report says |Ian Shapira |February 9, 2021 |Washington Post 

Will Bridgegate impede Chris Christie's chances of seeking higher office? Chris Christie’s YOLO Attitude for 2016 |Olivia Nuzzi |May 15, 2014 |DAILY BEAST 

If following this diet stresses you out or interferes with your sleep patterns, it could also impede weight loss. Does Intermittent Fasting Really Work? |DailyBurn |March 6, 2014 |DAILY BEAST 

Moreover, they have been allowed to impede the realization of a crucial American national security interest. Israel and Palestine Vs. ‘Blood and Magic’ |Hussein Ibish, Saliba Sarsar |September 17, 2013 |DAILY BEAST 

That record will also severely impede his effectiveness in his portfolio. Make Democrats Own Their Hagel-Flop |David Frum |February 25, 2013 |DAILY BEAST 

Raise the rates, and you impede efficiency-enhancing transactions - and thereby harm everybody's economic welfare. The Right Way to Tax Mitt Romney, Part 2 |David Frum |January 24, 2012 |DAILY BEAST 

Many persons impede their execution by not keeping the thumb independent enough of the rest of the hand. Music-Study in Germany |Amy Fay 

This mob of men and boys were good-natured, but very curious, and it gathered so close as to impede the progress of the ponies. Our Little Korean Cousin |H. Lee M. Pike 

Hence the furrows are not ploughed close together, for the roots when crowded impede the sprouting of the plant. The Geography of Strabo, Volume III (of 3) |Strabo 

All along the journey little absolutely unnecessary villages kept bobbing up to impede the progress of the train. Molly Make-Believe |Eleanor Hallowell Abbott 

But you could do no good with so few men—they are more than ten to one, I hear—and your men would but impede us. The Devil-Tree of El Dorado |Frank Aubrey