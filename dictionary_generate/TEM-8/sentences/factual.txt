Fox News, to pick the most powerful of the organs cited by Eshoo and McNerney, has a long history of resisting pressure to produce factual news reports. Hey, Democrats: Hands off Fox News’s cable carriers |Erik Wemple |February 24, 2021 |Washington Post 

He pleaded guilty last year to one count of misconduct in office for making “intentional misrepresentations and factual omissions” in Webster’s application for certification. The first state to pass a law protecting police accused of misconduct may also be the first to repeal it. |Ovetta Wiggins |February 9, 2021 |Washington Post 

Yes, science accumulates factual knowledge, but it is at its best when it generates new and better questions. How Pseudoscientists Get Away With It - Facts So Romantic |Stuart Firestein |August 28, 2020 |Nautilus 

In ideologically charged situations, one’s prejudices end up affecting one’s factual beliefs. Coronavirus Responses Highlight How Humans Are Hardwired To Dismiss Facts That Don’t Fit Their Worldview |LGBTQ-Editor |July 2, 2020 |No Straight News 

Our ancestors evolved in small groups, where cooperation and persuasion had at least as much to do with reproductive success as holding accurate factual beliefs about the world. Coronavirus Responses Highlight How Humans Are Hardwired To Dismiss Facts That Don’t Fit Their Worldview |LGBTQ-Editor |July 2, 2020 |No Straight News 

To make it work almost everything else about these shows has to seem factual which is why many look like a weird Celebrity Sims. ‘Empire’ Review: Hip-Hop Musical Chairs with an Insane Soap Opera Twist |Judnick Mayard |January 8, 2015 |DAILY BEAST 

Highly educated people will disagree, and no amount of factual information will necessarily decide the issue. Catholic University’s Harvey Milk Ban Reflects A Church In Transition |Jay Michaelson |October 3, 2014 |DAILY BEAST 

I wonder who is putting up the ads and how factual they are? The Sleazy War on the Humane Society |Center for Public Integrity |August 18, 2014 |DAILY BEAST 

We live in the world of Zero Dark Thirty; factual accuracy has supplanted fantasy technology. Writing a Novel: Even Making It Up Requires Research |Ridley Pearson |July 16, 2014 |DAILY BEAST 

But both of these statements are factual, and Republicans will spin them hard today and tomorrow. Despite Their Loss In Florida’s Special Election, Democrats Shouldn’t Panic Over November |Michael Tomasky |March 12, 2014 |DAILY BEAST 

I only wish to avoid vulgar exaggeration, to keep within the bounds of the factual. Frederick Chopin as a Man and Musician |Frederick Niecks 

Factual material, however disguised, often shines through its fictional background. Indirection |Everett B. Cole 

The last of the internal senses is that of factual memory, the power which retains the judgments made by the faculty preceding. A History of Mediaeval Jewish Philosophy |Isaac Husik 

Logically, any factual proposition is a hypothetical proposition when it is made the basis of any inference. Essays in Experimental Logic |John Dewey 

The better element meets the better element, and he makes factual, intelligent reports. The Five Arrows |Allan Chase