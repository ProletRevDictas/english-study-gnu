Despite plentiful rice, corn and wheat, China remains dependent on imported soybeans and is facing a pork shortage. Why Is China Cracking Down on Food Waste? |Daniel Malloy |September 3, 2020 |Ozy 

But, now, with August wrapping up, my mind swirls again to thoughts about how an ear of corn in my home could also help me savor summer for just a while longer. Summer Is Forever With This Corn-on-the-Cob Chair |Emma Orlow |August 28, 2020 |Eater 

Do not underestimate this seat in the shape of a corn-on-the-cob by Third Drawer Down, an Australian home goods company. Summer Is Forever With This Corn-on-the-Cob Chair |Emma Orlow |August 28, 2020 |Eater 

The corn is then washed and peeled, and left to dry for 12 hours before it’s ground with stones made from volcanic rock. How Tamales Are Made at One of New York City’s Favorite Puebla Tamal Shops |Eater Video |August 27, 2020 |Eater 

You’ve got surplus corn and you’ve got a demand for easy, convenient sweetener in the food sector. How the Supermarket Helped America Win the Cold War (Ep. 386 Rebroadcast) |Stephen J. Dubner |August 6, 2020 |Freakonomics 

For the uninitiated, the film is set on a future Earth whose crops (save corn) have been wiped out by a mysterious blight. Neil deGrasse Tyson Breaks Down ‘Interstellar’: Black Holes, Time Dilations, and Massive Waves |Marlow Stern |November 11, 2014 |DAILY BEAST 

I certainly found it very helpful when I realized we were going to have to grow our own corn. Christopher Nolan Uncut: On ‘Interstellar,’ Ben Affleck’s Batman, and the Future of Mankind |Marlow Stern |November 10, 2014 |DAILY BEAST 

That must have been some corn for her to remember it 50 years later! Joan Rivers: The Playboy Bunnies Weren’t Sluts! |Patty Farmer |November 7, 2014 |DAILY BEAST 

Hilbert was a flashy man who helicoptered five miles daily over corn fields to and from the office. The Very Rich Should Divorce Very Quietly |Vicky Ward |November 6, 2014 |DAILY BEAST 

He is perfectly capable of introducing a bill requiring all cars to run on corn stalks instead of gasoline. Up to a Point: In Defense of Lobbyists |P. J. O’Rourke |October 25, 2014 |DAILY BEAST 

The man with the automobile, the corn-cure, and the baby grew to be legendary in the villages of Provence. The Joyous Adventures of Aristide Pujol |William J. Locke 

I should judge that a peck of corn is about the average product of a day's work through all this region. Glances at Europe |Horace Greeley 

The Vine appears at intervals, but is not general through this region: Indian Corn is also rare, and appears in small patches. Glances at Europe |Horace Greeley 

Then, kindly and gently, the boy took Squinty over to the place where the corn crib was built on to the barn. Squinty the Comical Pig |Richard Barnum 

If he has made up his mind that I'm stealing corn nothing I could say would change his opinion. The Tale of Grandfather Mole |Arthur Scott Bailey