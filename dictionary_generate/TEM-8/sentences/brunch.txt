As flaky as your friend who cancels brunch plans on you only after you’ve already sat down and ordered a mimosa. Popeyes’ new fish sandwich is a muted sequel to the chicken sandwich blockbuster of 2019 |Emily Heil |February 12, 2021 |Washington Post 

Multi-course lunch and brunch menus will also be available for $22 per person. Restaurant Week returns with focus on dining at home |Evan Caplan |January 20, 2021 |Washington Blade 

We have the ability to apply our deposit to any future event, whether that’s a 12-person brunch or an 80-person dinner. 4 tips for planning a wedding during COVID |Rachel Schallom |January 18, 2021 |Fortune 

We’ll have to wait a while to join the ladies at brunch and find out. A ‘Sex and the City’ reboot without Samantha? Fans aren’t pleased, but it can still be fabulous. |Lisa Bonos |January 12, 2021 |Washington Post 

In scandalizing the brunch crew, she pushed the national conversation around women and sex forward a generation. Sex and the City Is Nothing Without Samantha Jones |Judy Berman |January 11, 2021 |Time 

My father had visited the States years before we moved here and was totally taken by this American concept of brunch. How Aasif Mandvi Became Jon Stewart’s Favorite Jihadi |Dean Obeidallah |November 16, 2014 |DAILY BEAST 

In Paris, you can go to the Brooklyn Café, or have brunch à la Brooklyn. How Brooklyn Invaded Paris—Next Stop, the World |Brandon Presser |October 23, 2014 |DAILY BEAST 

Brunch is a catalyst, brunch is the enforcer of different-rules-for-the-weekend. Don’t Diss the Beauty of Brunch: Defending Our Favorite Meal |Tim Teeman |October 15, 2014 |DAILY BEAST 

Oh dear, the New York Times has pronounced brunch as done, over, declaring, “Brunch is for jerks.” Don’t Diss the Beauty of Brunch: Defending Our Favorite Meal |Tim Teeman |October 15, 2014 |DAILY BEAST 

A New York Times article says brunch is over-rated and for “jerks.” Don’t Diss the Beauty of Brunch: Defending Our Favorite Meal |Tim Teeman |October 15, 2014 |DAILY BEAST 

At brunch he kept his eyes open, and before too long Panek came into the dining room for his lunch. Man of Many Minds |E. Everett Evans 

Rick and Scotty slept late the following morning and were awakened for brunch by Dr. Miller. The Blue Ghost Mystery |Harold Leland Goodwin 

He took the neck of her brunch coat in his fist and jerked downward. Ten From Infinity |Paul W. Fairman