The asset cap has hindered Wells Fargo’s ability to keep up with its competitors among major banks. Wells Fargo cuts jobs as the pandemic and penalties for past scandals take their toll |reymashayekhi |August 21, 2020 |Fortune 

Both of which, it said, may hinder its ability to effectively target and measure ads. How the world’s biggest media companies fared through the ongoing crisis in Q2 |Lara O'Reilly |August 12, 2020 |Digiday 

Itchy as producers are about getting back to work, they are wary of the rising number of coronavirus cases that could compromise that return, which is already hindered by the changes being made to facilitate it. TV, streaming show makers ease back into production despite coronavirus concerns — and insurance costs |Tim Peterson |August 11, 2020 |Digiday 

Now, the city’s democracy activists fear that Apple is again actively hindering the protest movement. Hong Kong’s protest movement keeps getting stymied by Apple |Mary Hui |July 14, 2020 |Quartz 

It also takes time and effort to develop data-sharing mechanisms such as systems that can store test results anonymously without hindering their accessibility. Why South Asia’s COVID-19 Numbers Are So Low (For Now) |Puja Changoiwala |June 23, 2020 |Quanta Magazine 

Graham told The Daily Beast on Monday it might do more to hinder the president than to help him. Republicans Offer Obama ISIS War Authorization He Doesn’t Want |Josh Rogin |September 9, 2014 |DAILY BEAST 

Simply stated, the harshness of the elements conspires to help, rather than hinder, the lucky few. How to Hitchhike a Plane—and Survive |Kent Sepkowitz |April 22, 2014 |DAILY BEAST 

Quora Q: Does drinking water during meals help or hinder the digestive system? Quora Q: Does Drinking Water During Meals Help or Hinder the Digestive System? |Quora Contributor |January 30, 2014 |DAILY BEAST 

Still, in the end, what will likely hinder Hidary is that he is not Bloombergian enough. Jack Hidary Waits to Make His Push in New York City Mayoral Race |David Freedlander |August 14, 2013 |DAILY BEAST 

He chastises Israel for segregationist policies but it is his one-nation allies that hinder integration efforts. Recognizing Israel's Arab Achievements |Robert Cherry |March 8, 2013 |DAILY BEAST 

This alone could hinder the execution of his appointment, for in other things he has excellent qualifications for the dignity. The Philippine Islands, 1493-1898, Volume XX, 1621-1624 |Various 

Don't tell me; I know: you don't want me to go, and take every mean advantage to hinder me. The Book of Anecdotes and Budget of Fun; |Various 

Who was he, indeed, that he should claim the right to thwart another's happiness, hinder another's best self-realisation? The Wave |Algernon Blackwood 

He doubted whether it lay in his power now to hinder anything, but in any case he would not seek to do so. The Wave |Algernon Blackwood 

But Soult was possessed of a crafty caution which seldom if ever allowed his ambition to hinder the success his ability deserved. Napoleon's Marshals |R. P. Dunn-Pattison