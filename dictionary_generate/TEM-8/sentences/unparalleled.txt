Part of this grief reflects her unparalleled status as a feminist icon and pioneer for women in the legal profession and beyond. Ruth Bader Ginsburg Forged A New Place For Women In The Law And Society |LGBTQ-Editor |September 23, 2020 |No Straight News 

His firm, Merrill Lynch, brought stock investing to an unparalleled number of regular people. Robinhood’s speedy rise is shaking up the brokerage market |John Detrixhe |September 19, 2020 |Quartz 

The members of the monitoring board have unparalleled insight into clinical studies. Some scientists downplay significance of AstraZeneca’s COVID-19 vaccine trial halt |Claire Zillman, reporter |September 9, 2020 |Fortune 

Opening stores in India represents an “unparalleled” opportunity for Apple, since it will be able to “develop roots” in the country, he said. Apple’s iPhone sales have lagged in India for years. It’s only now unleashing its branding firepower |Grady McGregor |September 6, 2020 |Fortune 

This arrangement has given American corporations unparalleled freedom to swap contractors, minimize tax burdens, and make things using inventory someone else pays to insure and maintain. Unmade in America |Tate Ryan-Mosley |August 14, 2020 |MIT Technology Review 

Weeks retained an unparalleled legal team, which included bitter political rivals Hamilton and Burr. New York’s Most Tragic Ghost Loves Minimalist Swedish Fashion |Nina Strochlic |January 8, 2015 |DAILY BEAST 

That makes for unparalleled wines, but also for the dangerous temptation to label every variety as unique. Napa’s Earthquake Is Not The Only Thing Shaking The Vineyards |Clive Irving |August 31, 2014 |DAILY BEAST 

Their losing streak is unparalleled in professional American sports. 100 Years of Wrigley Field: Are the Chicago Cubs Horrible Because of the Ballpark? |Luke Epplin |March 28, 2014 |DAILY BEAST 

Funny, engaging, and reliably controversial, she proved her unparalleled worth. Rosie O’Donnell’s Explosive Return to ‘The View’ Makes Us Miss Her More |Kevin Fallon |February 7, 2014 |DAILY BEAST 

Bitcoin lets users make transactions with comparatively unparalleled anonymity. A Bitcoin Boss’s Bust Spotlights the Cryptocurrency’s Link to Online Drug Sales |Betsy Woodruff |January 28, 2014 |DAILY BEAST 

The sugar of Cuba is the finest in the world; but in Cuba, slavery is unparalleled in its horrors. Blackwood's Edinburgh Magazine, No. CCCXXXIX. January, 1844. Vol. LV. |Various 

Such a lengthened period between licensing and publication is probably unparalleled in literary history. The Catacombs of Rome |William Henry Withrow 

As for Lyndaraxa, her repeated and unparalleled treachery can only be justified by the extreme imbecility of her lovers. The Works Of John Dryden, Volume 4 (of 18) |John Dryden 

Now the contestants were in clear view, and a race followed unparalleled in the annals of war. Famous Adventures And Prison Escapes of the Civil War |Various 

The writers of the period abound in notices of the unparalleled growth of trade and commerce. The Influence and Development of English Gilds |Francis Aiden Hibbert