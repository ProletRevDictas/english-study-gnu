The fourth artist, Andrew Hladky, burrows deeply into fable, inspired by a dystopian novel. In the galleries: Personal and political perspectives on the past |Mark Jenkins |February 19, 2021 |Washington Post 

The book’s sequel, “The Spirit of Music,” is a kind of action-adventure fable involving Victor, Michael, and a number of other friends and teachers. The tale of a bass player, sonic epiphanies and a quest to save ‘real music’ |Ben Ratliff |February 12, 2021 |Washington Post 

In her book, Jaffe, a longtime labor journalist, says large corporations specifically conjured this fable in order to pay workers less and give them fewer benefits. Why You Don't Feel as Fulfilled From Your Job as You Think You Should |Eliana Dockterman |January 25, 2021 |Time 

For a writer who would become most renowned for his nonfiction—he won the National Book Award for Arctic Dreams in 1986—it was his short stories and fables and trickster tales that I most cherished, learned from, stole from. Remembering My Friend Barry Lopez |Bob Shacochis |January 5, 2021 |Outside Online 

Like the fable of the city mouse and the country mouse, a city coyote may feel very uncomfortable in the country, and vice versa, guesses Javier Monzon. Are coyotes moving into your neighborhood? |Kathryn Hulick |September 3, 2020 |Science News For Students 

The story of fluoridation reads like a postmodern fable, and the moral is clear: a scientific discovery might seem like a boon. Anti-Fluoriders Are The OG Anti-Vaxxers |Michael Schulson |July 27, 2016 |DAILY BEAST 

It is a fable about an elderly woman, “Grandy,” who has suffered an unnamed loss. Book Bag: Reading Your Way Out Of Grief |Anna Whiston-Donaldson |October 16, 2014 |DAILY BEAST 

The fable tells us that if policymakers foster competition and cut taxes, the rest will pretty much work itself out. What’s At Stake In The Tocqueville/Piketty Debate |James Poulos |April 27, 2014 |DAILY BEAST 

D.H. Lawrence wrestled with the discontent of well-off people in his dark fable, “The Rocking-Horse Winner.” What Tolstoy Teaches Us About Insider Trading |Liesl Schillinger |June 2, 2013 |DAILY BEAST 

His hilarious parody-fable, “A Dissertation Upon Roast Pig,” traces the supposed genesis of that culinary delicacy. Phillip Lopate’s Book Bag: The Essay Tradition |Phillip Lopate |February 5, 2013 |DAILY BEAST 

You know the fable about the dog who dropped his meat in the water, trying to snap at its reflection? Rosemary in Search of a Father |C. N. Williamson 

But whatever may be the origin of this fable, the assigning of it to Napoleon is in itself a singular circumstance. Notes and Queries, Number 178, March 26, 1853 |Various 

An allusion to the fable in sop about the earthern and brazen pots being dashed together. Chaucer's Works, Volume 1 (of 7) -- Romaunt of the Rose; Minor Poems |Geoffrey Chaucer 

The two versions of this fable are also instances of the relative capabilities of the French and the English four-stress lines. Aesop Dress'd |Bernard Mandeville 

This fable is only one among many others that were narrated with a view to curbing the propensities of blaspheming swearers. A Cursory History of Swearing |Julian Sharman