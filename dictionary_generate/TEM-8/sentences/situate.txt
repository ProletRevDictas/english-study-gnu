Ever since Arthur Conan Doyle situated his sleuth Sherlock Holmes at 221B Baker Street, Victorian London has been the ur-setting for detective stories. Two historical mystery novels plunge readers into the past while keeping them guessing |Clare McHugh |February 8, 2021 |Washington Post 

The Blue Jays are a classic, upwardly mobile franchise — situated in a major media market, coming off a playoff appearance in 2020 and hungry to make the leap from good to great. The Blue Jays gamble on George Springer, hoping to go from a good team to a great one |Dave Sheinin |January 20, 2021 |Washington Post 

Time meant situating ourselves as part of a larger web of life. Humans Have Rights and So Should Nature - Issue 94: Evolving |Grant Wilson |January 6, 2021 |Nautilus 

They situate the writer at home in a story in which she is usually a voyeur. Harvard students told a lurid tale of murder. Was it true? |Marin Cogan |December 11, 2020 |Washington Post 

The absence of the pay-TV distribution deals would appear to situate the companies’ streamers as separate from, and incremental to, their linear businesses. ‘Burn the boats’: TV networks playing with fire in streaming pivots |Tim Peterson |December 9, 2020 |Digiday 

These details situate the scene in time, but also in place, eliding any conflict between the two. Painting Pontius Pilate's Palestine |Blake Gopnik |March 6, 2013 |DAILY BEAST 

It is a long log and frame building, situate on the south side of the road, with a porch extending along its entire frontage. The Old Pike |Thomas B. Searight 

It was constructed of stone, evidently of some antiquity, and situate in a dull remote street. Harper's New Monthly Magazine, No. VII, December 1850, Vol. II |Various 

The Palace has Gardens delightfully situate by the side of the Elbe. The Memoirs of Charles-Lewis, Baron de Pollnitz, Volume I |Karl Ludwig von Pllnitz 

From the pools we went down the hollow in which they are situate, and followed the course of the aqueduct. Letters from Palestine |J. D. Paxton 

Stoke is situate in the county of Stafford, and has a great porcelain manufactory founded by Wedgwood. Memoirs of the Duchesse de Dino v.1/3, 1831-1835 |Dorothy Duchesse de Dino