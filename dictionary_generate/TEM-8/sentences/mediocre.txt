It also doesn’t help that the characters are mediocre at best, with only Sadness — a troubled spirit Marianne tries to help throughout her journey — inspiring a small measure of interest as the two gain one another’s trust. ‘The Medium’ review: A disjointed, unfulfilling puzzle horror game |Elise Favis |February 1, 2021 |Washington Post 

There at the Capitol lies the worst of us, as well as our mediocre selves. Ghosts of our unsettled past |Robin Givhan |January 26, 2021 |Washington Post 

It means that even small or mediocre projects have their value, because they give creators experience, and maybe a paycheck, so they can stick around and work another day. Art has been brutalized by tech’s giants. How can it survive? |Konstantin Kakaes |December 23, 2020 |MIT Technology Review 

People began throwing together mediocre content, inserting as many links into it as possible, and shopping it around to any website that would publish it. Six strategies guaranteed to help you build more backlinks to your website |Izabelle Hundrev |December 10, 2020 |Search Engine Watch 

Results might have been challenged if a team with the 11th to 16th best record in the sport — such mediocre clubs have never been allowed in October before — had somehow gotten hot and grabbed a crown. No asterisk required: These Dodgers were historically great — and legitimate champions |Thomas M. Boswell |October 29, 2020 |Washington Post 

To call them mediocre, uninspiring, and stale would be overly generous. Latinos Aren’t a ‘Cheap Date’ for Democrats Anymore |Ruben Navarrette Jr. |November 11, 2014 |DAILY BEAST 

Jeremy Lin exploded onto the stage as the breakout star of one in a long line of mediocre New York Knicks seasons in 2011-12. Ellen Anaconda, Mike Tyson TV Battle, and More Viral Videos |Jack Holmes |September 14, 2014 |DAILY BEAST 

He also took cobbling lessons, eventually becoming good enough to do a mediocre job replacing a pair of soles. Adam Sandler Talks Getting Fired From ‘SNL,’ Bad Reviews, and His Desire to Play a Villain |Marlow Stern |September 12, 2014 |DAILY BEAST 

One will not know until the next round—the quarterfinals—when this mediocre Brazil team will once again flirt with defeat. World Cup 2014 Nail-Biter: Host Country Brazil Defeats Chile on Penalty Kicks |Tunku Varadarajan |June 28, 2014 |DAILY BEAST 

But it is doggedly, almost deliberately mediocre in every way. ‘Jersey Boys’ Proves Clint Eastwood is Hollywood’s Most Overrated Director |Andrew Romano |June 20, 2014 |DAILY BEAST 

Although a capable specialist, as regards general culture and intelligence Ilia Petrovich had only a mediocre equipment. The Life &amp; Letters of Peter Ilich Tchaikovsky |Modeste Tchaikovsky 

For even an indifferent photographer is in closer harmony with nature than a mediocre poet.' The Talking Horse |F. Anstey 

He no longer perceives the defects of his work—does not know that it is mediocre or bad. The Child of Pleasure |Gabriele D'Annunzio 

The resources offered by this country to sportsmen were mediocre in the extreme. Celebrated Travels and Travellers |Jules Verne 

Never praise heartily, that is the sign of an intelligence not mediocre. How to Fail in Literature |Andrew Lang