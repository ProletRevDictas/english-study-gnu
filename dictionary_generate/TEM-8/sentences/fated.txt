Country First was more than a slogan for John McCain’s ill-fated 2008 presidential bid. Cindy McCain Opens Up — On Her Terms |Philip Elliott |April 27, 2021 |Time 

There was the ill-fated reelection campaign of Senator Kay Hagan in North Carolina, who lost to Republican Thom Tillis. Latinos Aren’t a ‘Cheap Date’ for Democrats Anymore |Ruben Navarrette Jr. |November 11, 2014 |DAILY BEAST 

Fated to die in the end like all the others he describes himself as “the saddest man in the world… infinitely sad.” How Hitch & Amis Discovered Evil In My House |Peter Foges |September 28, 2014 |DAILY BEAST 

Perry has associated with Barbour since at least 2012, when Barbour served on his ill-fated but memorable presidential campaign. Tea Party Unloads on 'Complete Imbecile' Rick Perry |Olivia Nuzzi |September 4, 2014 |DAILY BEAST 

This appears to glorify a crime,” said Cheek, “as does the apparent emphasis on their illegal and ill-fated invasion. The Never-Ending Falklands War: In Buenos Aires, A Museum's Selective History |Michael Luongo |August 30, 2014 |DAILY BEAST 

Republicans cast her ill-fated health care policy task force as a “shadow government.” Everyone Sees the Hillary They Want to See |Eleanor Clift |June 17, 2014 |DAILY BEAST 

"It is ill-fated;" and Alessandro blamed himself for having forgotten her only association with the name. Ramona |Helen Hunt Jackson 

How little did she realize the long drawn-out agony that was even then beginning for her sisters in that ill-fated entrenchment! The Red Year |Louis Tracy 

Then we were again in motion, and the ill-fated town of Karinjah, now a heap of smouldering ruins, was soon far behind us. Confessions of a Thug |Philip Meadows Taylor 

Strange calumnies still rest upon this queen, all of whose actions were fated to be misjudged. Catherine de' Medici |Honore de Balzac 

I was one of the 109 Union officers who passed through the tunnel, and one of the ill-fated 48 that were retaken. Famous Adventures And Prison Escapes of the Civil War |Various