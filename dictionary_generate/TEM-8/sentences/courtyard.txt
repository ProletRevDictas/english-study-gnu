I’ve also met a porter in our apartment complex who every night at 7 for months sprinted around our central courtyard carrying aloft an American flag. Every Thanksgiving, my mental health strategy is recalling memorable, personal moments |Bob Brody |November 23, 2020 |Washington Post 

Less than a mile away, more than 200 people gathered to watch the game in an apartment building’s courtyard. Football is back in Happy Valley. The coronavirus never left. |Kent Babb |October 30, 2020 |Washington Post 

The presidential debate will take place in the Samson Pavilion, which features a massive steel roof and soaring 80-foot-high indoor courtyard. The Startling Reach and Disparate Impact of Cleveland Clinic’s Private Police Force |by David Armstrong |September 28, 2020 |ProPublica 

We have previously, for example, written about why the city may have done lease-to-own deals for this building and a previous, eerily similar one, Civic Center Plaza, which is right across the courtyard from City Hall. Politics Report: Alleged Footnote 15 |Scott Lewis and Andrew Keatts |September 5, 2020 |Voice of San Diego 

Through a tiny window, I saw people stripped naked in the courtyard. Misery in Minsk |Eugene Robinson |August 18, 2020 |Ozy 

After our helicopter crashed in the courtyard, Jimbo and I rushed inside the house. I Shot Bin Laden |Elliot Ackerman |November 16, 2014 |DAILY BEAST 

She slow-motion jogs through the courtyard, paints his portrait, and carves his name into a tree. Taylor Swift’s ‘Blank Space’: Hell Hath No Fury Like A Tay-Tay Scorned |Sujay Kumar |November 10, 2014 |DAILY BEAST 

I entered the metal gates to an open courtyard full of inmates eagerly waiting for their own visitors. Cocaine, Politicians and Wives: Inside the World’s Most Bizarre Prison |Jason Batansky |October 12, 2014 |DAILY BEAST 

I was there at one of the first gigs by Julian Casablancas and the Voidz at SXSW, in Cedar Street Courtyard. Julian Casablancas Enters the Void: On the Strokes’ Friction, Why He Left NYC, and Starting Over |Marlow Stern |October 9, 2014 |DAILY BEAST 

Pivoting on his heels, he casually strolled out toward a nearby walled courtyard. Inside a Hospital for the Criminally Insane |Caitlin Dickson |September 15, 2014 |DAILY BEAST 

The building, which has five storeys, stands on three sides of a square courtyard, and faces into Edmund Street. Showell's Dictionary of Birmingham |Thomas T. Harman and Walter Showell 

They found the village inn to be a series of low, small buildings built on three sides of a courtyard. Our Little Korean Cousin |H. Lee M. Pike 

Outside in the courtyard the fire was kept burning, beside which two watchmen sat all night smoking and telling stories. Our Little Korean Cousin |H. Lee M. Pike 

A favorite of king and courtier, its use was alike common in the palace and the courtyard. Tobacco; Its History, Varieties, Culture, Manufacture and Commerce |E. R. Billings. 

One side of the courtyard blazed in sunshine, the other lay cool and grey in shadow. The Joyous Adventures of Aristide Pujol |William J. Locke