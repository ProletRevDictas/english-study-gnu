Memories may seem like amorphous wisps inside a mind, but they have strong neurobiological underpinnings. A Critical Immune Protein Helps the Brain Link Memories, and Could Combat Aging |Shelly Fan |May 31, 2022 |Singularity Hub 

Bumpy rides down La Ruta del Lechón, a stretch of highway known for its roast pork spits, wisps of smoke rising to the sky. A pernil-style pork tenderloin that conjures the smells of a Puerto Rican kitchen |Daniela Galarza |April 8, 2021 |Washington Post 

You could hear wisps of music and smell the earth, freshly turned over by spring worms, and there was a hint of something sugary in the air, something new. Before You Have That Bonfire of the Stretchy Pants, Consider Some Pandemic Wardrobe Habits You Should Keep |Susanna Schrobsdorff |April 4, 2021 |Time 

You wouldn’t think sound could travel very well on Mars—what with the planet’s bare wisp of an atmosphere, which is just 1% the thickness of ours. Listen to the Sounds of NASA's Perseverance Rover Driving on Mars |Jeffrey Kluger |March 18, 2021 |Time 

A thin man with a wisp of a goatee beard, he struggles with a stutter to explain what happened to him that day. Photographs Expose Russian-Trained Killers in Kiev |Jamie Dettmer |March 30, 2014 |DAILY BEAST 

Sleek finds it far harder work than fortune-making; but he pursues his Will-o'-the-Wisp with untiring energy. The Pit Town Coronet, Volume I (of 3) |Charles James Wills 

Rosemary flitted about like a will o' the wisp, and finally went to the window, where she stood looking wistfully out. Rosemary in Search of a Father |C. N. Williamson 

As she left the wood she saw a big hay-stack, as firm and shapely of outline as a house, not a loose wisp anywhere. Ancestors |Gertrude Atherton 

A wisp of wheat was knotted round her neck for a necklace, and a perfect sheaf of it in her hair. Music-Study in Germany |Amy Fay 

I was told that subsequent to that matter my will-o'-the-wisp was coming on here positively. The Weight of the Crown |Fred M. White