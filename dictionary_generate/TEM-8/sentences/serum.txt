In May, Moderna reported that serum from people given the half dose of the existing vaccine neutralized SARS-CoV-2 variants at levels similar to or higher than those against the non-variant strain. What You Need to Know About COVID-19 Booster Shots |Alice Park |July 15, 2021 |Time 

Its intense hydrating benefits – and unique ability to hold many times its weight in water – make it a popular active ingredient in many hydrating serums and masks. ABC’s of Skin Health: Get to Know the Skincare Ingredients That Go Into the Science of Great Skin and How They Help Melanated Skin Types |Steven Psyllos |June 25, 2021 |Essence.com 

However, all the supersoldiers except for Isaiah eventually died from the serum. All Your Falcon and the Winter Soldier Finale Questions, Answered |Eliana Dockterman |April 23, 2021 |Time 

Enter “clean beauty”— images of effortlessly glowing people, makeup products that double as magical skin serums, and lists of supposedly ethical qualities ranging from cruelty-free to fragrance-free to formaldehyde-free. Are ‘clean’ cosmetics better for us than other beauty products? |Sara Kiley Watson |March 17, 2021 |Popular-Science 

Our top-selling products are serums, toners, essences, which makes a lot of sense because people are in their homes and have more time to dedicate to their skincare routines. Singapore-based Raena gets $9M Series A for its pivot to skincare and beauty-focused social commerce |Catherine Shu |February 26, 2021 |TechCrunch 

If he contracts the disease, there will be no ‘secret serum’ waiting for him. Fighting Ebola With Nothing but Hope |Abby Haglage |August 27, 2014 |DAILY BEAST 

A Washington Post blog asks: “Why do two white Americans get the Ebola serum while hundreds of Africans die?” Why the White Americans Got the ‘Secret’ Ebola Serum |Michael Daly |August 8, 2014 |DAILY BEAST 

The New Republic demands: “Why did two U.S. missionaries get an Ebola serum while Africans are left to die?” Why the White Americans Got the ‘Secret’ Ebola Serum |Michael Daly |August 8, 2014 |DAILY BEAST 

Never mind that there seem to have been no more than eight doses of the serum in existence. Why the White Americans Got the ‘Secret’ Ebola Serum |Michael Daly |August 8, 2014 |DAILY BEAST 

The serum failed to neutralize the virus in subsequent tests and seemed to offer little protection in animal experiments. Infected Ebola Doctor Kent Brantly Is an Endangered Hero |Michael Daly |August 3, 2014 |DAILY BEAST 

It is thinner than that of chronic bronchitis, and upon standing separates into three layers of pus, mucus, and frothy serum. A Manual of Clinical Diagnosis |James Campbell Todd 

It is especially useful with cultures upon serum media, but is applicable also to the sputum. A Manual of Clinical Diagnosis |James Campbell Todd 

The clear, straw-colored fluid which is left after separation of the coagulum is called blood-serum. A Manual of Clinical Diagnosis |James Campbell Todd 

One drop of the serum is then added to nine drops of normal salt solution, making a dilution of 1:10. A Manual of Clinical Diagnosis |James Campbell Todd 

In serum therapy antitoxins are artificially excited into being in the blood of beasts. Essays In Pastoral Medicine |Austin Malley