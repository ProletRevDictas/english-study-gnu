Popkin said the deputy appears to have unsuccessfully tried to use his Taser to stop the assailant, who continued to attack with the piece of wood. Video shows fatal shooting of man by Montgomery County sheriff’s deputy |Ian Duncan, Dan Morse |February 8, 2021 |Washington Post 

Karau ended up representing himself in General District Court — and the alleged assailant was convicted of assault. Judge faults Fairfax County prosecutors for failing to notify victim of trial |Justin Jouvenal |February 5, 2021 |Washington Post 

The assailants belonged to the Front de Libération du Québec, or FLQ, a militant group that called for the independence of their Francophone province in eastern Canada. James Cross, British diplomat kidnapped by Quebec separatists, dies at 99 of covid-19 |Emily Langer |January 21, 2021 |Washington Post 

Video of the encounter soon spread on the Internet, prompting people to speculate online about the alleged assailant’s identity. Bethesda bike-trail assailant pleads guilty to assault in flier-snatching case |Dan Morse |December 16, 2020 |Washington Post 

Noble left his vehicle and the assailant began chasing him in broad daylight, while continuing to shoot at him. Rapper Mo3 Shot And Killed In Dallas |Joe Colucci |November 12, 2020 |Essence.com 

Somebody yanks Chan and elbows him and he is momentarily distracted trying to apprehend his assailant. Protesters Slimed This Good Samaritan Cop |Michael Daly |December 16, 2014 |DAILY BEAST 

The rationale in those cases, he said, is that DNA should be used to identify the assailant. How the U.S. Ended Up With 400,000 Untested Rape Kits |Caitlin Dickson |September 23, 2014 |DAILY BEAST 

Thankfully, Linda was not injured and the assailant, a white male, was arrested. 13 Years After 9/11, Anti-Muslim Bigotry Is Worse Than Ever |Dean Obeidallah |September 11, 2014 |DAILY BEAST 

My assailant will remain unpunished, and life on this campus will continue its course as if nothing had happened. Harvard’s Biggest Problem With Sexual Assault Is Harvard Itself |Emily Shire |April 4, 2014 |DAILY BEAST 

There was that time Nancy Kerrigan was whacked in the knee by a baton-wielding assailant in 1994. Sotnikova Beat Kim Yu-Na? Figure Skating Is Probably Corrupt (But We Knew That) |Kevin Fallon |February 21, 2014 |DAILY BEAST 

In the distance he saw his late assailant running hard; the coach had disappeared. St. Martin's Summer |Rafael Sabatini 

So long as he had to deal with a single assailant he saw no need to move from so excellent a position. St. Martin's Summer |Rafael Sabatini 

If Mr. Adams fell where he was struck, the assailant must have had that door directly before him. The Circular Study |Anna Katharine Green 

Now if he saw the tragedy from this point, he saw it over the assailant's shoulder, instead of face to face. The Circular Study |Anna Katharine Green 

A second curse, and the assailant ghost-like was gliding amongst the orchard trees. God Wills It! |William Stearns Davis