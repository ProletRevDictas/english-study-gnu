By the 1980s, China was extensively testing and implementing insecticide nets around the country as a prevention method, well before they were standard practice. China becomes the largest country to officially eradicate malaria |Monroe Hammond |July 1, 2021 |Popular-Science 

In plants, it functions as an insecticide — poisoning insects that try to eat the plant. Scientists Say: Nicotine |Bethany Brookshire |June 7, 2021 |Science News For Students 

Tick activity is expected to be higher than usual this year, and some health experts recommend wearing clothing that’s been treated with permethrin, an insecticide. Cicadas, plants and too much sun: How to keep your pet safe from summer hazards |Angela Haupt |June 3, 2021 |Washington Post 

In fact, another study found that sparrows eating seeds coated with a neonicotinoid insecticide commonly used in agriculture lost weight and then delayed their migration. Here’s the real story behind Alfred Hitchcock’s ‘The Birds’ |Tom McNamara |December 3, 2020 |Popular-Science 

First, researchers need to test the new version of the insecticide against pesticide-resistant mosquitoes. Heating deltamethrin may help it kill pesticide-resistant mosquitoes |Maria Temming |October 19, 2020 |Science News 

The disease remains a global scourge despite rapid advances in providing insecticide nets and spraying homes, mostly with DDT. The Robot That Could Kill Malaria |Eleanor Clift |May 8, 2014 |DAILY BEAST 

Thanks to the widespread adoption of such seeds, farmers have been spraying their crops with insecticide less frequently. Farmers Turn to Pesticide as Insects Resist Genetically Modified Crops |Daniel Gross |May 22, 2013 |DAILY BEAST 

Sometimes a druggist will remember selling some poison to kill a dog or as an insecticide. Hooded Detective, Volume III No. 2, January, 1942 |Various 

There are very few data at hand concerning the discovery of the insecticide properties of pyrethrum. Scientific American Supplement No. 299 |Various 

Perches are best cleaned by washing with some liquid insecticide, and then allowing them to dry in the sun. Making a Poultry House |Mary Roberts Conover 

The experiments prove that all insects having open mouth parts are peculiarly susceptible to this popular insecticide. Scientific American, Volume 40, No. 13, March 29, 1879 |Various 

Get ready in advance for prompt action against it by laying in a supply of the insecticide at the beginning of the summer. ABC of Gardening |Eben Eugene Rexford