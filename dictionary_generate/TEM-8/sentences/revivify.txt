In the twelfth century the old system of wer and bt is already vanishing, though an antiquarian lawyer may yet try to revivify it. Domesday Book and Beyond |Frederic William Maitland 

The thought that he could revivify her by the very strength of his overflowing love took him forward a step. The Secret of the Storm Country |Grace Miller White 

Prof. Church has in this story sought to revivify that most interesting period, the last days of the Roman Republic. Historic Boys |Elbridge Streeter Brooks 

After all, how can we know anything of a nation's present or future without some attempt to revivify its past? Penelope's Irish Experiences |Kate Douglas Wiggin 

It is a food too strong for ordinary men, and which, when it does not revivify, smothers. The Paris Sketch Book of Mr. M. A. Titmarsh |William Makepeace Thackeray