Early on, the company was “fairly agnostic,” Jain says, waiting to see whether machines or people could better fact-check the world. He’s Fighting QAnon With Sunlight |Nick Fouriezos |September 6, 2020 |Ozy 

Consumers are increasingly agnostic about whether they buy online or locally. A Corona Xmas: Why physical stores will power online shopping this holiday season |Greg Sterling |September 4, 2020 |Search Engine Land 

Met’s general manager, Jeffrey Kightlinger, said the agency is “agnostic” about the project. The Water Authority Is Resurrecting Its Pipe Dream – Again |MacKenzie Elmer |September 1, 2020 |Voice of San Diego 

The sequence is device agnostic when a user is logged in through their account, which means that shifting between devices doesn’t affect that strategy, it even enhances the experience. Five great display and video advertising tactics to increase relevance and revenue in a cookie-less world |Anastasia-Yvoni Spiliopoulou |August 24, 2020 |Search Engine Watch 

We take an agnostic approach to ad tech and wherever there’s commercial growth — then that’s when we’ll look to expand. ‘We’ll get briefs we couldn’t access before’: Inside Channel 4’s push for programmatic advertisers |Seb Joseph |August 11, 2020 |Digiday 

She is agnostic and a firm supporter for gay rights and birth control. The World Would Go to Hell Without Nuns |William O’Connor |September 4, 2014 |DAILY BEAST 

In general, MBAs are agnostic about how cost cutting can be achieved. Burger King Invades Canada to Save His Faltering Kingdom |Daniel Gross |August 26, 2014 |DAILY BEAST 

According to Pew, close to two-thirds of those who identify as atheist or agnostic are men. Are Atheists the New Mormons? |Michael Schulson |April 18, 2014 |DAILY BEAST 

For the record, I believe in God but am an agnostic about therapy. Anthony Weiner and Other Democrats in Sex Scandals Don’t Mention God |Dean Obeidallah |July 25, 2013 |DAILY BEAST 

Street savvy but compassionate, mystical but agnostic and above all, brilliantly idiosyncratic, Fly is a rambling poet of sorts. This Week’s Hot Reads: June 10, 2013 |Mythili Rao |June 10, 2013 |DAILY BEAST 

He had by this time become what would now be called an agnostic. The English Utilitarians, Volume II (of 3) |Leslie Stephen 

For the Agnostic, no more than the Atheist, can attach no intelligible meaning to "God." Theism or Atheism |Chapman Cohen 

True, she said, checked for a moment, but one is not truly agnostic when ones mother has had faith. The Wasted Generation |Owen Johnson 

This statement, coming from a leading agnostic, was welcome to the theologians. The Book of Life: Vol. I Mind and Body; Vol. II Love and Society |Upton Sinclair 

A story of modern life and thought, being a study of two opposite types—the Christian and the Agnostic. Battles of English History |H. B. (Hereford Brooke) George