The trial in which he was convicted by the Inquisition for “vehement suspicion of heresy” exerted a chilling effect on progress in deciphering the laws governing the cosmos. Galileo the Science Publicist - Issue 103: Healthy Communication |Mario Livio |July 14, 2021 |Nautilus 

Yes, I know this is heresy, especially the part about investment bankers. AT&T dividend cut in WarnerMedia-Discovery deal is a debacle for shareholders |Allan Sloan |May 21, 2021 |Washington Post 

Days later, Bridenstine took this heresy further when he suggested SpaceX's Falcon Heavy rocket could boost humans to the Moon. So long Senator Shelby: Key architect of SLS rocket won’t seek reelection |Eric Berger |February 9, 2021 |Ars Technica 

That might sound like heresy coming from a veteran journalist, and Cose knows it. Some free-speech norms are in danger. Maybe that’s a good thing. |Kenneth Mack |November 6, 2020 |Washington Post 

A person close to the family told the German broadcaster Deutsche Welle that he was initially arrested for heresy. Trust Iran? It Just Hanged a Man Who Doubted ‘Jonah and the Whale’ |IranWire |September 30, 2014 |DAILY BEAST 

His move to the Tribune would be followed by a move to a suburban manse—“Heresy!” The Stacks: John Schulian’s Classic Profile of Newspaper Columnist Mike Royko |John Schulian |January 5, 2014 |DAILY BEAST 

A few hundred years later, Belgian cartographer Gerard Mercator was charged with heresy. The Secret, Contentious History of Maps |Kevin Canfield |November 30, 2013 |DAILY BEAST 

But by doing his job, he may have committed unforgivable heresy in a Santorum-leaning party. Lessons From the Fiscal Cliff: the Political Fallout |Robert Shrum |January 4, 2013 |DAILY BEAST 

The find led to the discovery of other similar lakes and floods, but was initially disbelieved because it was “geological heresy.” How Noah’s Flood Spurred Science: David R. Montgomery’s ‘The Rocks Don’t Lie’ |David Sessions |August 28, 2012 |DAILY BEAST 

Joan Boughton, a widow, was burned for heresy; said to be the first female martyr of England. The Every Day Book of History and Chronology |Joel Munsell 

Probably this is due to the long association of intellectuality and science with heresy. Solomon and Solomonic Literature |Moncure Daniel Conway 

John Penry, an English controversial writer, executed for heresy against the episcopacy. The Every Day Book of History and Chronology |Joel Munsell 

Constantine (the Great) called the first council of Nice to determine on the Arian heresy. The Every Day Book of History and Chronology |Joel Munsell 

Third cumenical council assembled at Ephesus, to execute the decree of pope Celestine as to the heresy of Nestorius. The Every Day Book of History and Chronology |Joel Munsell