For Roger, whose research also looks at Tudor quarantine measures that could be classified as “social distancing” today, the current environment has made him think more about how authorities handled outbreaks in the 16th century. COVID-19 Is Prompting Wealthy People to Move Out of Cities. The Plague Had the Same Effect Hundreds of Years Ago |Suyin Haynes |August 21, 2020 |Time 

On Friday, she sacked Roger Goodell, basically asking: “Hey Commissioner, ever hear of double-jeopardy?” The $44 Million Teflon Don of the NFL |Mike Barnicle |November 30, 2014 |DAILY BEAST 

When Roger first heard what happened and saw the tape he was shocked, truly shocked, and outraged. The $44 Million Teflon Don of the NFL |Mike Barnicle |November 30, 2014 |DAILY BEAST 

To this day, Bush media maven Roger Ailes adamantly denies that he or the campaign had any role in the Willie Horton mug shot ad. Want President Hillary? Then Primary Her |Jeff Greenfield |November 24, 2014 |DAILY BEAST 

A local radio personality named Roger Fredinburg remembers getting a call to see if he wanted to host a show at the new station. The Godfather of Right-Wing Radio |Caitlin Dickson |November 23, 2014 |DAILY BEAST 

The beloved Mr. Roger's premiered in 1968, opening a door to television that didn't speak down to children. ‘Sesame Street’ Is Middle-Aged and Awesome |Emily Shire |November 10, 2014 |DAILY BEAST 

It lacks convincingness perhaps from the fact that Thomass theology is so largely philosophy, as Roger Bacon said. The Mediaeval Mind (Volume II of II) |Henry Osborn Taylor 

Roger Bacon, an eminently learned monk of the Franciscan order, died, aged 80. The Every Day Book of History and Chronology |Joel Munsell 

Only think of Roger Williams sharing in the proceeds of a slave sale. The Condition, Elevation, Emigration, and Destiny of the Colored People of the United States |Martin R. Delany 

Roger Jones, an American military officer of distinction, died at Washington. The Every Day Book of History and Chronology |Joel Munsell 

Two queens of the snobocracy will entertain us at romping in the hay, with Sir Roger de Coverley to follow. The Pit Town Coronet, Volume II (of 3) |Charles James Wills