Elsewhere, our deals roundup includes big discounts on Apple's AirPods Pro and iPad Air, several deals on Amazon Fire HD tablets, a big Xbox digital game sale, and more. Apple’s new MagSafe wireless charger is down to a new low price today |Ars Staff |February 11, 2021 |Ars Technica 

If you know you like a particular style of game, like strategy games or platformers, look for roundups that describe the top choices available. Video gaming is for everybody now. Here’s how to get back into it. |Harry Guinness |January 25, 2021 |Popular-Science 

As 2020 comes to a much-anticipated end, here’s our annual roundup of the top 10 Blade stories of the year by web traffic. Top 10 Blade stories by web traffic in 2020 |Staff reports |December 30, 2020 |Washington Blade 

Here, from the editorial staff of Fortune, is a roundup of the best tips and tricks for going the distance. Tried and true tricks for getting over jet lag |Rachel King |December 27, 2020 |Fortune 

Sign up for the Future Perfect newsletter and we’ll send you a roundup of ideas and solutions for tackling the world’s biggest challenges — and how to get better at doing good. AI has cracked a problem that stumped biologists for 50 years. It’s a huge deal. |Sigal Samuel |December 3, 2020 |Vox 

Read here for yourself a roundup of how his statement was playing in real time on September 13, 2012. Who Made Benghazi ‘Political’? |Michael Tomasky |May 13, 2014 |DAILY BEAST 

Ippudo, East Village, Midtown West No NYC ramen roundup would be complete without an Ippudo shout-out. Underground Ramen’s Mainstream Moment |Sara Sayed |May 12, 2014 |DAILY BEAST 

What was supposed to be a cattle roundup looked more like a military exercise. Gun-Toting Ranchers Defeat Feds |John L. Smith |April 16, 2014 |DAILY BEAST 

Here, a roundup of the best responses from cops who are down with getting high. Can You Smoke Weed if Your Friend Is a Cop? |The Daily Beast |September 10, 2013 |DAILY BEAST 

(Such seeds were of course from Roundup Ready plants, since they had survived the spraying of Roundup). Supreme Court Supports Monsanto in Patent Dispute |Paul Campos |May 16, 2013 |DAILY BEAST 

There are some unweaned calves, and a few unbranded yearlings that will just about pay the cost of their roundup. David Lannarck, Midget |George S. Harney 

So Blink, though he was counted a good man on roundup, was left pretty much alone when in camp. The Happy Family |Bertha Muzzy Bower 

The "rough string," as the bad horses were called, was corralled, and the men made merry with the roundup crew. The Happy Family |Bertha Muzzy Bower 

On this roundup, however, he was not often amiable and he was nearly always rumbling to himself. The Happy Family |Bertha Muzzy Bower 

For the second time the Flying U roundup was brought to an involuntary pause because of its cook. The Happy Family |Bertha Muzzy Bower