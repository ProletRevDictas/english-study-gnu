Anencephaly – absence of parts of the brain and skull – is neither treatable nor curable with modern medicine. I'm a Doctor Who Cares for Newborns. I’m Nervous After the End of Roe |Rachel Fleishman |July 8, 2022 |Time 

At this point, lupus is not curable, but it is manageable, and a big part of how to do that is through lifestyle habits like exercise, sleep, stress management, and diet. How Changing Your Diet Could Have a Major Impact on Managing Lupus Symptoms |Elizabeth Millard |June 29, 2022 |Time 

But it remains a curable illness, and one that does not require course after course of antibiotics. Predator Doctors Take Advantage of Patients With ‘Chronic Lyme’ Scam |Russell Saunders |September 19, 2014 |DAILY BEAST 

Homosexuality must be curable, it argues, since the Torah would not forbid something which is impossible to avoid. Gay Orthodox Jews Sue Over Therapy That Claims to ‘Cure’ Them |Zoë Blackler |November 27, 2012 |DAILY BEAST 

Diabesity is nearly 100 percent preventable, treatable, and very often curable. The Real Fight Over Health Care Should Be Against Diabetes and Obesity |Mark Hyman, MD |March 23, 2012 |DAILY BEAST 

Gangrene is not curable by current medical intervention once past a certain point in its progression, except by amputation. Can Meditation Cure Disease? |Maureen Seaberg |December 25, 2010 |DAILY BEAST 

How curable it is—caught soon enough—and how deadly it is if ignored. Carly's Cancer Warning |Dana Goldstein |November 20, 2009 |DAILY BEAST 

It has been stated before that no attempt would be made in this paper to prove that epilepsy was curable by therapeutic means. A Statistical Inquiry Into the Nature and Treatment of Epilepsy |Alexander Hughes Bennett 

Cydalise probably accompanied Montes to Brazil, the only place where this horrible ailment is curable. Repertory Of The Comedie Humaine, Complete, A -- Z |Anatole Cerfberr and Jules Franois Christophe 

I wrote at once to the vet, telling him to telegraph "Curable" or "Hopeless," and to act accordingly. Animal Ghosts |Elliott O'Donnell 

She points out the necessity of a just discrimination between what is curable in the body politic and what has to be endured. George Eliot |Mathilde Blind 

Dropsies in the chest either with or without anasarcous limbs, are much more curable than those of the belly. An Account of the Foxglove and some of its Medical Uses |William Withering