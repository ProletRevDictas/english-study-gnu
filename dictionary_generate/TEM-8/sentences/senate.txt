It meant that, in New Jersey, the sitting president of the state senate will probably be replaced by a guy who spent on his entire campaign less than a third of what McAuliffe spent on fines to the Board of Elections alone. Another problem for Democrats on Tuesday? Little to no ticket-splitting. |Philip Bump |November 3, 2021 |Washington Post 

He was set to testify before the state senate, where a bill to finally repeal the 50a secrecy law was being considered. “City Hall Put the Kibosh on That”: The Inside Story of How de Blasio Promised, Then Thwarted NYPD Accountability |by Eric Umansky and Jake Pearson |June 11, 2021 |ProPublica 

On March 19, the faculty senate on March 19 passed a resolution urging the university to strike the names. Uproar erupts at U. of Richmond over building names with ties to racism |Nick Anderson |March 26, 2021 |Washington Post 

This is going to be the Game of Thrones of U.S. Senate races. The Golden State Preps for the ‘Red Wedding’ of Senate Races |David Freedlander |January 9, 2015 |DAILY BEAST 

This Congress will welcome more women than ever before at 19 percent of the House and 20 percent of the Senate. The Unbearable Whiteness of Congress |Dean Obeidallah |January 8, 2015 |DAILY BEAST 

AIDS insanity:  When running for the US Senate in 1992, Huckabee called for a quarantine of people who had AIDS. The Devil in Mike Huckabee |Dean Obeidallah |January 6, 2015 |DAILY BEAST 

It was a Senate floor soap opera over none other than a soap-opera producer. U.S. Embassies Have Always Been for Sale |William O’Connor |January 2, 2015 |DAILY BEAST 

He seemed by all appearances perfectly happy to let the Republicans control the state senate. Mario Cuomo: An OK Governor, but a Far Better Person |Michael Tomasky |January 2, 2015 |DAILY BEAST 

He was afterwards a member of the Massachusetts senate, and much esteemed as a physician and a patriot. The Every Day Book of History and Chronology |Joel Munsell 

The conservative senate sent a deputation to Bonaparte, expressing their desire that he would accept the title of emperor. The Every Day Book of History and Chronology |Joel Munsell 

This power may be exercised, either through treaties made by the president and senate, or through statutes enacted by congress. Putnam's Handy Law Book for the Layman |Albert Sidney Bolles 

In due course the news came that the date of voting in the Senate for or against the retention of the Islands was fixed. The Philippine Islands |John Foreman 

That will fill in while you are waiting a chance for Congress—you must be seven years in the country for that—nine for the Senate. Ancestors |Gertrude Atherton