This waver goes up to 400 degrees Fahrenheit with instant heat recovery to apply an even temperature to your tresses. Best curling iron: Hair styling tools to create the curls you seek no matter your hair type |Irena Collaku |August 12, 2021 |Popular-Science 

But no sooner was the 40-year-old activist out of U.S. hands than he began to waver. Has Hillary Clinton Salvaged Deal to Bring Chen to U.S. Temporarily? |Howard Kurtz |May 4, 2012 |DAILY BEAST 

The leading tendency here is not toward openness: People waver between frank fascism and latent xenophobia. J. M.G. Le Clézio on His Birthplace |Jean-Marie Gustave Le Clézio |April 6, 2011 |DAILY BEAST 

He would waver…all of that is to me a measure of the fact that he was troubled and it is pretty clear what he was troubled by. How Spitzer Could Have Survived |Allan Dodds Frank |March 8, 2010 |DAILY BEAST 

But that had no appreciable effect on military performance until the top leadership itself began to waver and retreat. The Decade's First Revolution? |Gary Sick |January 2, 2010 |DAILY BEAST 

He steeled himself, for he had had his experience of woman's wiles; and his faith in masculine supremacy as a habit did not waver. Ancestors |Gertrude Atherton 

He was rather gratified than otherwise to hear that Mr. Puffin had begun to waver in his ideas about celibacy. The Pit Town Coronet, Volume II (of 3) |Charles James Wills 

Lindsay seemed to waver; her glance went near enough to him to show her that his face had a red tinge of embarrassment. Hilda |Sarah Jeanette Duncan 

She drew back from me a little as I came; but her eyes did not waver from mine, and these lured me forward. The Works of Robert Louis Stevenson, Volume XXI |Robert Louis Stevenson 

But now, to my surprise and horror, when I looked into the eye of my monitor, my own eye would not waver nor admit subjection! The Way of a Man |Emerson Hough