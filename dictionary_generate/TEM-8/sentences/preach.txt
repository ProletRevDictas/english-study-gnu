While analytics will preach that you must shoot the three and post-up moves are virtually extinct from today’s game, the Lakers have gotten back to the finals doing the exact opposite. The NBA Finals Burst the Analytics Bubble |Joshua Eferighe |September 29, 2020 |Ozy 

So if I’m going to preach that, I better be practicing it every day. Adam Grant’s Philosophy of Giving |Joshua Eferighe |September 25, 2020 |Ozy 

The fact that diversity execs aren’t just preaching to the converted internally now is evidence of this. ‘It’s all been plan, plan, plan mode:’ Agencies have big ideas for greater diversity, but more action is needed |Seb Joseph |September 15, 2020 |Digiday 

Both issues are of acute concern for a president who preaches the need for self-sufficiency. Why Is China Cracking Down on Food Waste? |Daniel Malloy |September 3, 2020 |Ozy 

At Coffee Creek, his evangelical church outside Oklahoma City, he had preached on racial justice for the past three weeks. Evangelicals are looking for answers online. They’re finding QAnon instead. |Abby Ohlheiser |August 26, 2020 |MIT Technology Review 

At Christianity Today, Peter Chin claims Christians should preach peace instead of bogging down in the particulars of race. No Gods, No Cops, No Masters |James Poulos |January 1, 2015 |DAILY BEAST 

Priests often preach support for the regime to their congregations, many of whom loudly dissent. In One Corner of Syria, Christmas Spirit Somehow Manages to Survive |Peter Schwartzstein |December 25, 2014 |DAILY BEAST 

In letters to Theo, Vincent would preach to younger brother the virtues of life. Decoding Vincent Van Gogh’s Tempestuous, Fragile Mind |Nick Mafi |December 7, 2014 |DAILY BEAST 

Sex and the City fans were right behind her, ready to preach. Confessions of a Rom-Com Writer: Liz Tuccillo Talks ‘Sex and the City,’ ‘Take Care,’ and More |Kevin Fallon |December 5, 2014 |DAILY BEAST 

We should partner with them to get the message across, have them at the table, and listen rather than preach. The New Face of HIV Is Gay & Young |Adebisi Alimi |December 1, 2014 |DAILY BEAST 

When I am an old maid I am going to mount the platform and preach the training of the voice in childhood. Ancestors |Gertrude Atherton 

But it is emulating Mrs. Partington and her mop to attempt to preach down a (p. 206) world. Tobacco; Its History, Varieties, Culture, Manufacture and Commerce |E. R. Billings. 

"I don't allow a parson's wife to preach to me about my duty, or to interfere wi' my family matters," said Rushmere, dryly. The World Before Them |Susanna Moodie 

Many of us think it would be well if we could find a reliable man who could work and who could preach during the hours of rest. Skipper Worse |Alexander Lange Kielland 

Arise and go to Ninive, the great city, and preach in it: For the wickedness thereof is come up before me. The Bible, Douay-Rheims Version |Various