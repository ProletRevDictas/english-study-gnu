Each molecule of salt contains a pair of ions, atoms with an electrical charge. Working up a sweat may one day power up a device |Carolyn Wilke |June 29, 2020 |Science News For Students 

The company plans to get around this by instead shuttling the ions themselves around the system. A New Startup Intends to Build the World’s First Large-Scale Quantum Computer |Edd Gent |June 22, 2020 |Singularity Hub 

The other was a reformulation of the material that carries ions between the battery’s cathode and its anode. New Record-Crushing Battery Lasts 1.2 Million Miles in Electric Cars |Vanessa Bates Ramirez |June 11, 2020 |Singularity Hub 

Those ions can trigger the mucus to emit bursts of blue light, the team reports. This tube worm’s glowing slime may help sustain its own shine |Carolyn Wilke |June 5, 2020 |Science News For Students 

Lithium salts provide the ions that move through the new electrolyte. Batteries should not burst into flames |Carolyn Wilke |April 16, 2020 |Science News For Students 

I have previously drawn attention to the cargo aboard Flight 370, which included a large consignment of lithium-ion batteries. The Flight 370 Zombie Theory Rises From the Dead |Clive Irving |June 26, 2014 |DAILY BEAST 

Last week it made five recommendations directed at the testing and certification of lithium-ion batteries. NTSB Doesn’t Think the Boeing 787 Dreamliner Is Safe Enough to Fly |Clive Irving |May 28, 2014 |DAILY BEAST 

Lithium-ion batteries will further increase underwater performance. Tomorrow’s Stealthy Subs Could Sink America’s Navy |Bill Sweetman |May 12, 2014 |DAILY BEAST 

How can this saga of the lithium-ion batteries be tied to the fate of Flight 370? Passenger Flights Must Stop Carrying Lithium-Ion Batteries as Cargo |Clive Irving |May 5, 2014 |DAILY BEAST 

A month after that announcement, unaccustomed daylight was forced upon the lithium-ion battery industry. Passenger Flights Must Stop Carrying Lithium-Ion Batteries as Cargo |Clive Irving |May 5, 2014 |DAILY BEAST 

A merry, happy time the children had, and on reaching Ion the little ones were ready for their supper and bed. Elsie's Vacation and After Events |Martha Finley 

At Ion Grandma Elsie lay quietly sleeping, her three daughters watching over her with tenderest care and solicitude. Elsie's Vacation and After Events |Martha Finley 

In other words, five hydrogen ions passed to the right, while one chloride ion passed to the left. The Elements of Qualitative Chemical Analysis, vol. 1, parts 1 and 2. |Julius Stieglitz 

That is, different concentrations of hydrogen-ion or of hydroxide-ion are required to change their colors. The Elements of Qualitative Chemical Analysis, vol. 1, parts 1 and 2. |Julius Stieglitz 

The second table gives, similarly, the concentrations of hydroxide-ion required to produce the changes of tint indicated. The Elements of Qualitative Chemical Analysis, vol. 1, parts 1 and 2. |Julius Stieglitz