One day I know he’ll be gone,risen early like the Buddha out of a dream,taking his special knowledge into the world. Five poems about the mind |Cynthia Miller, Paula Bohince, Anthony Anaxagorou, Tishani Doshi, Zeina Hashem Beck |August 25, 2021 |MIT Technology Review 

At times, he was like a serene Buddha, simply smiling at it all. Nam June Paik predicted the Internet, YouTube and Instagram. But he was more interesting as an artist than as a prophet. |Sebastian Smee |July 30, 2021 |Washington Post 

It shows an 18th-century wooden sculpture of a seated Buddha, which Paik purchased at an antique store. Nam June Paik predicted the Internet, YouTube and Instagram. But he was more interesting as an artist than as a prophet. |Sebastian Smee |July 30, 2021 |Washington Post 

She admits she thought her father “should be like Buddha, or Morrie Schwartz from Tuesdays With Morrie, or any number of stoic philosophers who embrace their final days with a pure heart, conviction of the world’s oneness flowing from their lips.” Kate Fagan quit her job to care for her dying father. The experience brought unexpected rewards. |Ada Calhoun |June 4, 2021 |Washington Post 

Exiled back to Earth, Monkey and his armies battle the forces of Heaven successfully until the Buddha finally imprisons him under a mountain. The action-packed saga ‘Monkey King: Journey to the West’ gets a modern take |Michael Dirda |March 3, 2021 |Washington Post 

“My children are traumatised,” Than Dar told a group of reporters in front a large reclining Buddha. Hope and Change? Burma Kills a Journalist Before Obama Arrives |Joshua Carroll |November 11, 2014 |DAILY BEAST 

It was the first time I witnessed his balanced mix of strength and kindness, or what could be dubbed his “Buddha nature.” The Buddhist Punk Reforming Drug Rehab |Stephen Krcmar |June 16, 2014 |DAILY BEAST 

But the Buddha condemned both extreme luxury and extreme poverty as obstacles to enlightenment. Complaining Like It’s 1999: ‘Fight Club,’ ‘American Beauty,’ and the Revolt of the Cubicle Drone |Arthur Chu |June 3, 2014 |DAILY BEAST 

The couch is too deep, and he is growing heavier; he will be Buddha-like in girth at some point soon. The Stacks: The True Greatness of Muhammad Ali |Peter Richmond |February 23, 2014 |DAILY BEAST 

This is one of the meanings of the famous Zen koan, “If you see the Buddha on the road, kill him.” The Shocking Scandal at the Heart of American Zen |Jay Michaelson |November 14, 2013 |DAILY BEAST 

In return, each of the priests placed an image of Buddha on a tree-root, turning it into an altar. Our Little Korean Cousin |H. Lee M. Pike 

Upon one of these monuments an upright figure of the deity Buddha is sculptured in a standing position. A Woman's Journey Round the World |Ida Pfeiffer 

No ancient teacher enjoined the duties based on an immutable morality with more force than Confucius, Buddha, and Epictetus. Beacon Lights of History, Volume I |John Lord 

The Life of Buddha by Asvaghosha is a poetical romance of nearly ten thousand lines. Beacon Lights of History, Volume I |John Lord 

Buddha travels slowly to the sacred city of Benares, converting by the way even Brahmans themselves. Beacon Lights of History, Volume I |John Lord