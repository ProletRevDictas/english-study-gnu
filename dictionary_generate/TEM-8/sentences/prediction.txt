Unlike S2S, SubX operates in nearly real time, allowing forecasters to see how their subseasonal predictions pan out as weather develops. Improved three-week weather forecasts could save lives from disaster |Alexandra Witze |August 27, 2020 |Science News 

At its heart is the reward prediction error, and even if you haven’t heard of it, you’ve experienced it. This Is How Your Brain Responds to Social Influence |Shelly Fan |August 25, 2020 |Singularity Hub 

Tech predictions—they’re fun to make, but can either be eerily prescient or wildly off-base. When cell phones were a “hot new industry” |Karen Yuan |August 20, 2020 |Fortune 

That information helps scientists make “very confident” predictions over how different amounts of sea level rise would creep into areas like the Midway District, Barnard said. Nobody’s Talking About the Sports Arena Flood Zone |MacKenzie Elmer |August 19, 2020 |Voice of San Diego 

To do that accurately, the algorithm needs to analyze every type of engagement in the book, recognizing the patterns and making predictions about future behaviors. How to optimize for the Instagram algorithm in 2020 |Julia Miashkova |August 19, 2020 |Search Engine Watch 

He vividly remembers Shirley Tilghman, then the president of Princeton, asking for his prediction. Meet the One Numbers-Cruncher Who Foresees Democrats Holding the Senate |Linda Killian |September 16, 2014 |DAILY BEAST 

Another prediction he got wrong involved the 2012 congressional election. Meet the One Numbers-Cruncher Who Foresees Democrats Holding the Senate |Linda Killian |September 16, 2014 |DAILY BEAST 

Jackson had another prediction: protests will continue tonight. Uneasy Peace in Ferguson in Danger After Slain Brown Named as Suspect in Robbery |Justin Glawe |August 15, 2014 |DAILY BEAST 

As with his Serbian prediction, Paul was absolutely correct when it came to Spain: Germany lost, 1-0. The Amazing Tale of Paul the Psychic Octopus: Germany’s World Cup Soothsayer |Emily Shire |July 12, 2014 |DAILY BEAST 

He soon invents the “Efram Daniels Expulsion Index (EDEI) … a hybrid futures and prediction market.” In a New Novel, Apathetic Teenagers Usher in the Apocalypse |Elliot Ackerman |June 9, 2014 |DAILY BEAST 

They burnt the chosen city of holiness, and made the streets thereof desolate according to the prediction of Jeremias. The Bible, Douay-Rheims Version |Various 

During two years following, this prediction might well have appeared to moderate minded men entirely justified. The Eve of the Revolution |Carl Becker 

Tony was gay, light-hearted as usual, belying Mrs. Haughstone's ominous prediction. The Wave |Algernon Blackwood 

Notwithstanding Mrs. Biggs's prediction that she would not sleep a wink, Eloise did sleep fairly well. The Cromptons |Mary J. Holmes 

An Introduction, maestoso, followed by something mystical (Kents Prediction). The Life &amp; Letters of Peter Ilich Tchaikovsky |Modeste Tchaikovsky