She tried to use an app on her phone to translate, but a minute later the man steered into the lot of another motel. Sex-trafficked kids are crime victims. In Las Vegas, they still go to jail. |Jessica Contrera |August 26, 2021 |Washington Post 

The most recent, “Rip Crew,” centers on the exploits of Valentine Pescatore, a former Border Patrol agent asked to look into a mysterious massacre of migrant women in a motel room. Revisiting “The Year of the Spy” |by Stephen Engelberg |July 28, 2021 |ProPublica 

The drive is mostly grain silos and fast food signs and a bunch of tiny towns that are pretty much the same, all equipped with a highway-side motel that’d make a good place to get shot. The ballad of the Chowchilla bus kidnapping |Kaleb Horton |July 23, 2021 |Vox 

His mother was a model, and his father worked in motel and restaurant management. Robert Downey Sr., provocative underground filmmaker, dies at 85 |Harrison Smith |July 8, 2021 |Washington Post 

He has entry and exit records at the airport nearest to me, and told me what motel he stayed at when he was trying to find me in San Francisco. Why it’s so hard to make tech more diverse |Wudan Yan |June 30, 2021 |MIT Technology Review 

Such a woman would be much more difficult to catch unawares than a teenage escort trapped in a motel room. Indiana Serial Killer’s Confession Was Just the Start |Michael Daly |October 21, 2014 |DAILY BEAST 

Cunningham and a male friend went to the motel and entered the room. Indiana Serial Killer’s Confession Was Just the Start |Michael Daly |October 21, 2014 |DAILY BEAST 

We were finishing steaks at a motel dining room, washing them down with beer, when the waitress could stand it no longer. Football Great Bob Suffridge Wanders Through the End Zone of Life |Paul Hemphill |September 6, 2014 |DAILY BEAST 

He is said by investigators to have acknowledged going with Simonson to a motel in Rochester, Minnesota. How ‘MrHandcuffs’ Ended Up With Two Corpses in Suitcases |Michael Daly |June 30, 2014 |DAILY BEAST 

The woman left the motel and went to a Panorama City 7-Eleven, where she called the police. More Bad News for Uber: Driver Arrested in Los Angeles Rape Case |Olivia Nuzzi |June 4, 2014 |DAILY BEAST 

I dressed hastily, and as I pulled my clothing on I took a slow dig at the other cabins in the motel. Highways in Hiding |George Oliver Smith 

They opened up the motel so we would have a place to spend the night in the lobby and some of the rooms. The Biography of a Rabbit |Roy Benson 

Actually, the spotel business isn't much different from running a plain, ordinary motel back on Highway 101 in California. The Love of Frank Nineteen |David Carpenter Knight 

Like I said, the spotel business isn't so different from the motel game back in California. The Love of Frank Nineteen |David Carpenter Knight 

My impression was that he didn't have money to pay for the trip or the motel or anything. Warren Commission (8 of 26): Hearings Vol. VIII (of 15) |The President's Commission on the Assassination of President Kennedy