The base four-cylinder turbo is dandy, especially for daily commutes. Sport sedans make spirited return |Joe Phillips |March 19, 2021 |Washington Blade 

These handy-dandy contraptions disperse hot water vapor to a fiber pad, allowing you to clean your floors with minimal effort, chemicals, and mess. Best steam mop: For gleaming, sanitized floors |PopSci Commerce Team |March 18, 2021 |Popular-Science 

Using a long-handled lighter, WD-40 customers have reported that it makes a dandy mini-flame thrower. 20 ingenious uses for WD-40 |By Bill Heavey/Field & Stream |February 11, 2021 |Popular-Science 

The market value of your now-nine-year security would be only about $9,174, according to my handy-dandy online bond calculator. The Fed Saved the Economy but Is Threatening Trillions of Dollars Worth of Middle-Class Retirement |by Allan Sloan |October 21, 2020 |ProPublica 

That’s all fine and dandy, but a lack of a schedule can be confusing for our animals, especially when they usually have free reign of the house from 9 to 5 every day. How to help your pet with separation anxiety |Sara Kiley Watson |August 26, 2020 |Popular-Science 

And this big box that encloses him is only an exaggeration of his regular nerd-dandy clothes. The Stacks: Pauline Kael's Talking Heads Obsession |Pauline Kael |November 22, 2014 |DAILY BEAST 

As well as being less flirtatious, Capaldi is far less of a dandy. Doctor Who’s ‘Deep Breath’: The 2,000-Year-Old Time Lord Grows Up |Nico Hines |August 8, 2014 |DAILY BEAST 

Yankee Doodle Dandy, from 1942, tells the story of the man who practically wrote the soundtrack to American patriotism. 13 Most Patriotic Movies Ever: ‘Act of Valor,’ ‘Top Gun’ & More (VIDEO) |Melissa Leon |July 4, 2014 |DAILY BEAST 

Gary Hume dressed as a Mexican dandy and sold tequila slammers. Joshua Compston Was Once the Wunderkind of the British Art World…and Now He’s Been Practically Forgotten |Anthony Haden-Guest |January 17, 2014 |DAILY BEAST 

Baudelaire, very much the well-dressed young dandy, was watching from his carriage across the street. Baudelaire’s Femme Fatale Muse |James MacManus |May 7, 2013 |DAILY BEAST 

The poor dandy showed a pair of straight coat-tails instanter, and the whole table joined in a "tremenjous" roar. The Book of Anecdotes and Budget of Fun; |Various 

The tobacco-box, during the reign of Elizabeth, was no unimportant part of a dandy's outfit; sometimes a pouch or bag was used. Tobacco; Its History, Varieties, Culture, Manufacture and Commerce |E. R. Billings. 

The creature kept up a deafening squealing, while out of the bush rushed Dandy, the farmers dog. The Girls of Central High on the Stage |Gertrude W. Morrison 

Dandy seemed much surprised to discover that he had locked his teeth on the wrong individual! The Girls of Central High on the Stage |Gertrude W. Morrison 

Boat after boat came up and made fast astern of the dandy vessel, and soon the decks were crowded with merry groups. The Chequers |James Runciman