The unique ability of humans to pry open the Earth and excavate the mass carbon graves of millions of years ago has created an energy surplus unheard of in the billions of years prior. Dawn of the Heliocene - Issue 90: Something Green |Summer Praetorius |September 16, 2020 |Nautilus 

Crucially, each flare would excavate some protons from the neutron star’s surface. Neutrinos could reveal how fast radio bursts are launched |Lisa Grossman |September 16, 2020 |Science News 

Five council circles have been found among 22 ancestral Wichita sites excavated along an eight-kilometer stretch of the Little Arkansas and Smoky Hill rivers, around 230 kilometers north of the newly surveyed site. Drones find signs of a Native American ‘Great Settlement’ beneath a Kansas pasture |Bruce Bower |September 10, 2020 |Science News 

Makers of the earthwork may have removed soil from the pits to construct mounds inside its borders, as has been observed at excavated council circles in the region. Drones find signs of a Native American ‘Great Settlement’ beneath a Kansas pasture |Bruce Bower |September 10, 2020 |Science News 

Previous research has been done on Stonehenge’s acoustics, but was incomplete, says archaeologist Timothy Darvill of Bournemouth University in England who has excavated at Stonehenge but did not participate in the new research. Stonehenge enhanced sounds like voices or music for people inside the monument |Bruce Bower |August 31, 2020 |Science News 

There have been attempts to excavate the remains, and many have been removed over the past half-decade. A WWII Battle Frozen in Time |Nina Strochlic |May 14, 2014 |DAILY BEAST 

In the years since, Alford has worked hard to excavate what she really feels. JFK’s Intern-Mistress Mimi Alford Confesses, ‘I Did Love Him’ |Leslie Bennetts |February 10, 2012 |DAILY BEAST 

However, de-miners crawling on their bellies to identify, excavate, and destroy mines remain the default modus operandi. Diana Landmine Conspiracies Return |Martyn Gregory |June 6, 2010 |DAILY BEAST 

To excavate a hole under either of the four would have required more time than we believed we had to spare. A Virginia Scout |Hugh Pendexter 

I looked among the dead, to see if I could find any iron implement with which to excavate the wall, or to break the chains. Rule of the Monk |Giuseppe Garibaldi 

Then the birds are able to obtain a foothold and to excavate with the bill, while clinging to the edge of the hole. A Bird Calendar for Northern India |Douglas Dewar 

Before the locks could be built it became necessary to excavate down to bed rock. The Panama Canal |Frederic Jennings Haskin 

This has operated to excavate numerous and extensive caves into the coast. Summary Narrative of an Exploratory Expedition to the Sources of the Mississippi River, in 1820 |Henry Rowe Schoolcraft