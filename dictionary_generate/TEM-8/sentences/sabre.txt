The grasp on the sabre would tighten; the quiet eyes would flash. Stonewall Jackson, VMI’s Most Embattled Professor |S. C. Gwynne |November 29, 2014 |DAILY BEAST 

In time we will know if this was nothing more than sabre rattling or an all out war for control of the GOP. The Gettysburg of the GOP Civil War |Dean Obeidallah |February 15, 2014 |DAILY BEAST 

But these resentments, They are the rocket fuel That lives In the tip of my sabre. The Poetry of Charlie Sheen |Michael Solomon |March 1, 2011 |DAILY BEAST 

And then the printers, manufactured by new Dunder Mifflin parent company Sabre, began to catch on fire. Axe The Office! |Jace Lacob |June 29, 2010 |DAILY BEAST 

He held the sabre lower, but the point was kept unwaveringly at the chest of his enemy; his teeth were set. The Pit Town Coronet, Volume I (of 3) |Charles James Wills 

Well, we must try our luck with a regulation sabre; they can't well refuse it; ours is the stronger and bigger man. The Pit Town Coronet, Volume I (of 3) |Charles James Wills 

His open brow lowered, and his fingers instinctively began playing with the hilt of his sabre. Blackwood's Edinburgh Magazine, No. CCCXXXIX. January, 1844. Vol. LV. |Various 

Where sabre, lance, and bayonet, right soon would turn and flee! Blackwood's Edinburgh Magazine, No. CCCXXXIX. January, 1844. Vol. LV. |Various 

At Moskowa his cuirassiers, sabre in hand, drove the Russians out of the great redoubt, but Grouchy himself was seriously wounded. Napoleon's Marshals |R. P. Dunn-Pattison