The company had also said it would plant grasses that better hold in place the sandy riverbank and add a layer of rocks to lessen erosion. New Engineering Report Finds Privately Built Border Wall Will Fail |by Jeremy Schwartz and Perla Trevizo |September 2, 2020 |ProPublica 

Doing small things like this can help to lessen some of the friction that comes along with asking others to engage with your posts. Five content promotion strategies SaaS marketers should implement today |Izabelle Hundrev |August 28, 2020 |Search Engine Watch 

Playing fantasy sports helped lessen the sting of not having his own startup. The Economics of Sports Gambling (Ep. 388 Rebroadcast) |Stephen J. Dubner |August 20, 2020 |Freakonomics 

Minnesota, by contrast, lets in certain family members, clad in personal protective gear, for up to three hours a day to monitor care and lessen loneliness. No Visitors Leading to Despair and Isolation in Senior Care Homes |Jared Whitlock |July 28, 2020 |Voice of San Diego 

Among 86 infected women, the drug helped lessen the amount of extra oxygen needed in 96 percent of pregnant women and 89 percent of women who had newly given birth. Remdesivir may work even better against COVID-19 than we thought |Tina Hesman Saey |July 13, 2020 |Science News 

Cooperating with Assad is also the only feasible way, at present, to lessen the humanitarian nightmare in Syria. There’s Only One Way to Beat ISIS: Work With Assad and Iran |Leslie H. Gelb |October 18, 2014 |DAILY BEAST 

We ask that you lessen her pain and therefore lessen the pain of all humanity. Hallucinating Away a Heroin Addiction |Abby Haglage |May 4, 2014 |DAILY BEAST 

Each 6000kg sculpture is lowered to the seabed where it is drilled into the substrate to lessen the effects of turbulent weather. Artist Jason deCaires Taylor’s Underwater Sculptures Are a Sight to Sea |Justin Jones |April 7, 2014 |DAILY BEAST 

But getting cyclists to don a hard-shelled helmet, which can lessen the risks of serious injury, has been a global challenge. Helmet Haute Couture: The Invisible Helmet Revolutionizing Bike Safety |Nina Strochlic |December 19, 2013 |DAILY BEAST 

The company hopes the change will lessen the weight load for not only the pilots, but the planes. Airline Pilots Ditch Charts and Paperwork for iPads to Save Money and Fuel |Miranda Green |June 28, 2013 |DAILY BEAST 

Alone the supreme Self in him looked calmly on, seeming to lessen the part that trembled and knew fear. The Wave |Algernon Blackwood 

The recognition did not lessen the reality, the poignancy of the revelation by any suggestion or promise of instability. The Awakening and Selected Short Stories |Kate Chopin 

The wind now began to lessen; and, for fear of being becalmed, I was anxious to get an offing. Narrative of a Survey of the Intertropical and Western Coasts of Australia] [Volume 2 of 2] |Phillip Parker King 

If fortune frowns he may lessen his crop but never his attachment for the plant. Tobacco; Its History, Varieties, Culture, Manufacture and Commerce |E. R. Billings. 

The crop supplied more tobacco than was needed, but no one man would cease to plant it, or lessen his crop for the general good. Tobacco; Its History, Varieties, Culture, Manufacture and Commerce |E. R. Billings.