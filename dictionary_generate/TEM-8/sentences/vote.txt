During this year’s Democratic primaries, it took days and sometimes weeks for the bulk of votes to get counted. Election live updates: Trump returns to Wisconsin; Biden to face live audience at town hall |Colby Itkowitz, Felicia Sonmez, John Wagner |September 17, 2020 |Washington Post 

Democrats also won a majority of votes in Senate races in 2016, but again, Republicans secured a majority. America needs a democratic revolution |Matthew Yglesias |September 17, 2020 |Vox 

Yeah, but we don’t know where these freaking votes came from. William Barr is the poster child for politicized law enforcement officials |Philip Bump |September 17, 2020 |Washington Post 

Still, the vote is significant because it indicates the breadth of congressional support for tearing down the PACER paywall. Bill to tear down federal courts’ paywall gains momentum in Congress |Timothy B. Lee |September 16, 2020 |Ars Technica 

McDonald’s appealed, and last December, the labor board reversed the judge’s decision and authorized the settlement, with Emanuel again casting the deciding vote in a 2-1 opinion. “Cover Up”: House Democrats Subpoena Documents That NLRB Refused to Share in Ethics Investigation |by Ian MacDougall |September 15, 2020 |ProPublica 

Weiss is likely to get confirmed even as Warren and a handful of other progressive Democrats vote no. Sen. Warren’s Main Street Crusade to Pressure Clinton |Eleanor Clift |January 8, 2015 |DAILY BEAST 

Asian-Americans may vote for Democrats now, but they are a highly persuadable—and growing—part of the electorate. Asian-Americans Are The New Florida |Tim Mak |January 8, 2015 |DAILY BEAST 

In 1992, Republican George H.W. Bush won the Asian-American vote by 24 points. Asian-Americans Are The New Florida |Tim Mak |January 8, 2015 |DAILY BEAST 

By 2012, Democratic President Barack Obama owned the Asian-American vote, winning it by 47 percentage points. Asian-Americans Are The New Florida |Tim Mak |January 8, 2015 |DAILY BEAST 

But after winning 55 percent of the white vote, Duke had a database of supporters some politicians coveted. The Price of Steve Scalise’s Silence |Jason Berry |January 7, 2015 |DAILY BEAST 

The bill to remove the civil disabilities of the Jews rejected in the British parliament by a vote of 288 to 165. The Every Day Book of History and Chronology |Joel Munsell 

They can, and they will, vote themselves and their friends or adherents into the good jobs and the high places. The Unsolved Riddle of Social Justice |Stephen Leacock 

Only a creditor who owns a demand or provable claim can vote at creditors' meetings. Putnam's Handy Law Book for the Layman |Albert Sidney Bolles 

If a portion of a creditor's debt is secured and a portion is unsecured, he may vote on the unsecured portion. Putnam's Handy Law Book for the Layman |Albert Sidney Bolles 

An appeal by a member of a subordinate lodge from a vote of expulsion does not abate by his death while the appeal is pending. Putnam's Handy Law Book for the Layman |Albert Sidney Bolles