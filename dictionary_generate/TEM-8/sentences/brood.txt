That chafed—not so much the business of being so junior a member of the sibling brood, but, as he reached his teens, at the privileges age afforded his siblings and the ones it denied him. Four Civilian Astronauts. Three Days in Orbit. One Giant Leap. Meet the Inspiration4 Crew |Jeffrey Kluger |August 10, 2021 |Time 

Last week, you analyzed two broods of cicadas, with periods of A and B years, that had just emerged in the same season. Can You Win The Penalty Shootout? |Zach Wissner-Gross |July 16, 2021 |FiveThirtyEight 

Both 13 and 17 are prime numbers — and relatively prime with one another — which means these broods are rarely in phase with other predators or each other. Can You Solve This Astronomical Enigma? |Zach Wissner-Gross |July 9, 2021 |FiveThirtyEight 

Her quirky, bubbly personality is a sharp contrast from the otherwise dark, brooding atmosphere of “Mass Effect 2.” Every ‘Mass Effect’ squadmate, ranked from a storytelling perspective |Jhaan Elker |June 4, 2021 |Washington Post 

Kritsky hopes that by the time the next major cicada explosion emerges in 2024—a brood in northern Illinois that emerges on a 13-year cycle—he’ll have figured out a way to use artificial intelligence to do the painstaking work. The Brood X cicadas are here — and yes, there’s an app for that |Tanya Basu |June 3, 2021 |MIT Technology Review 

The new trail is slated for February, which happens to be around the time the eagles will likely be starting another brood. He Faces Jail for Rescuing Baby Eagles |Michael Daly |November 2, 2014 |DAILY BEAST 

After all, the small congregation— about 40 strong —is comprised almost entirely of the Phelps brood. This Man Is The Future of Westboro Baptist Church |Caitlin Dickson |March 24, 2014 |DAILY BEAST 

The key is how much we can brood, and what is meant by brooding—is it to daydream, or is it to agonize over every detail? ‘True Detective,’ Obsessive-Compulsive Noir, and ‘Twin Peaks’ |Jimmy So |March 14, 2014 |DAILY BEAST 

An uncle and his family resided in another house and his aunt and her brood in a third. A Young Chef Travels to Calabria, Italy, and Learns the Old Ways of Cooking |Curtis Stone |November 28, 2013 |DAILY BEAST 

Why he went after the Anderson brood is especially puzzling. Hannah Anderson Captor a ‘Trusted Friend,’ Cat Lover |Christine Pelisek |August 13, 2013 |DAILY BEAST 

He returned in ten minutes or so, having sat for that period behind a neighbouring tree to brood over his circumstances. Hunting the Lions |R.M. Ballantyne 

What can be prettier than a brood of chickens with a good motherly hen, like the one in this picture! The Nursery, July 1873, Vol. XIV. No. 1 |Various 

What will you be, some day, when Posey lays eggs, and brings out a brood of little chickens? The Nursery, July 1873, Vol. XIV. No. 1 |Various 

A portly woman, whom Isabel knew to be the mother of a brood, was far more anxious to please. Ancestors |Gertrude Atherton 

Finally only one duckling remained in the middle of the river, probably at once the strongest and most foolish of the brood. Gold-Seeking on the Dalton Trail |Arthur R. Thompson