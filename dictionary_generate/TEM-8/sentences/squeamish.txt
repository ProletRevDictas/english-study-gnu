As a former vegetarian, I’m still squeamish about eating living things. I Tried Lab-Grown Fish Maw. Here's Why It Could Help Save Our Oceans |Amy Gunia/Hong Kong |September 17, 2021 |Time 

If you’re still a bit squeamish, you could follow Wynne’s recipes for her preserves and apply the water-bath method for processing them. I found an old-school technique to conquer my jam fears. Then I tried to figure out if it’s safe. |Charlotte Druckman |August 26, 2021 |Washington Post 

The second is that I am infamous for being un-squeamish in dining. Gene Weingarten: Would you like some Brood X with your Dom Pérignon? |Gene Weingarten |May 21, 2021 |Washington Post 

Before visiting the Roselands’ ranch, Virginia was squeamish about the idea of shooting an animal, and nervous that her experience at gun ranges wouldn’t translate to safety in the field. How Non-Native Pheasants Protect American Biodiversity |Wes Siler |April 1, 2021 |Outside Online 

If you’re someone who’s a bit squeamish about the feeling of raw eggs in your hand, perhaps it’s not for you. How to separate eggs without the stress or mess |Becky Krystal |February 8, 2021 |Washington Post 

None were squeamish about organising power to pursue their object. One U.S. Constitution Just Wasn’t Enough |Tom Arnold-Forster |July 4, 2014 |DAILY BEAST 

Katy Perry as a squeamish conservative is a little hard to swallow. Katy Perry and Miley Cyrus Kiss, Break Up, Make Up |Amy Zimmerman |March 7, 2014 |DAILY BEAST 

Partisans are seldom over-squeamish about their choice of allies. Michael Scheuer’s Meltdown |David Frum |January 3, 2014 |DAILY BEAST 

Linda Kasabian, a more recent and squeamish recruit, was left to stand guard at the gate. The Making of a Monster: Charles Manson’s Childhood |Wendy Smith |August 3, 2013 |DAILY BEAST 

Plenty of non-conservatives are squeamish about 20-plus-week abortions. Rep. Trent Franks: Just Another Idiot When It Comes to Abortion |Michelle Cottle |June 14, 2013 |DAILY BEAST 

Yet so squeamish did he become when once the official mantle had descended upon his shoulders, that even the exclamations “lud!” A Cursory History of Swearing |Julian Sharman 

Even for the most squeamish the discomforts of the voyage lay behind. The Highgrader |William MacLeod Raine 

Had you not been so over squeamish you might have changed the children, and made your own son the heir of the Moncton. The Monctons: A Novel, Volume I |Susanna Moodie 

Those who are responsible are squeamish as to the appearance of delicacy in the conduct of a young girl. The Wayfarers |Mary Stewart Cutting 

True humanity consists not in a squeamish ear, but in listening to the story of human suffering and endeavoring to relieve it. The Anti-Slavery Examiner, Part 3 of 4 |American Anti-Slavery Society