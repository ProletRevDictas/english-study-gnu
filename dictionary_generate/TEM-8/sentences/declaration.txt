Which, of course, I recognize that at no point in volume one do they make any explicitly romantic declarations. How Gideon the Ninth author Tamsyn Muir queers the space opera |Constance Grady |February 5, 2021 |Vox 

Now, the tides have turned toward quieter declarations of identity, particularly as young people embrace more fluid expressions of sexuality and gender. T.J. Osborne Is Ready to Tell His Story |Sam Lansky |February 3, 2021 |Time 

He has not made similar declarations about employee vaccines, but emphasized the value of employee vaccinations in a video address Monday. Schools Want COVID-19 Vaccines, But Not Necessarily Mandates |Ashly McGlone |January 27, 2021 |Voice of San Diego 

We’ve also been hearing declarations that a “science of reading” proves that employing phonics in a particular war is the best and right path to teach young children how to read. Is there really a ‘science of reading’ that tells us exactly how to teach kids to read? |Valerie Strauss |January 26, 2021 |Washington Post 

The declaration makes it illegal for US citizens to own Xiaomi stock. US declares Xiaomi a “Communist Chinese military company,” bans investments |Ron Amadeo |January 15, 2021 |Ars Technica 

Apart from the video, the Saraya Al-Khorasani group has made no official declaration that it is linked to Taghavi. What an Iranian Funeral Tells Us About the Wars in Iraq |IranWire |January 6, 2015 |DAILY BEAST 

A declaration of candidacy signed by Cuomo was in the trunk of his car. Mario Cuomo, a Frustrating Hero to Democrats, Is Dead at 82 |Eleanor Clift |January 2, 2015 |DAILY BEAST 

Thanks to that meddling Franklin and the other editors, Jefferson thought his Declaration had been “mangled.” Forget the Resolutions; Try a Few Declarations |Kevin Bleyer |January 1, 2015 |DAILY BEAST 

The tone of the declaration is radically different from “A few sentences.” How Havel Inspired the Velvet Revolution |Michael Zantovsky |December 6, 2014 |DAILY BEAST 

The declaration adopted by the meeting was a bold step, but it did not a revolution make. How Havel Inspired the Velvet Revolution |Michael Zantovsky |December 6, 2014 |DAILY BEAST 

So he bore down on the solemn declaration that she stood face to face with a prison term for perjury. The Bondboy |George W. (George Washington) Ogden 

Huxley quotes with satirical gusto Dr. Wace's declaration as to the word "Infidel." God and my Neighbour |Robert Blatchford 

On the third day after the declaration of his recall, Ripperda took his official leave, and presented his son in his new office. The Pastor's Fire-side Vol. 3 of 4 |Jane Porter 

Through what ages has that declaration, not to be denied, ascended to cold and cruel skies? Solomon and Solomonic Literature |Moncure Daniel Conway 

The declaration of war, or cessation thereof, used to be proclaimed in the market by the High Bailiff. Showell's Dictionary of Birmingham |Thomas T. Harman and Walter Showell