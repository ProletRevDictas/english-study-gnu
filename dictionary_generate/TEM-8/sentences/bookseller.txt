Under Bezos’s stewardship, Amazon evolved from an upstart online bookseller into one of the world’s most popular Internet marketplaces able to quickly deliver a vast catalogue of products and services. Jeff Bezos stepping down as Amazon CEO, transitioning to executive chair role |Jay Greene, Tony Romm |February 4, 2021 |Washington Post 

Jassy has long been viewed as a potential successor to Bezos, despite not being part of Amazon’s retail operations, the company’s core business since its launch as an online bookseller in 1995. Who is Amazon’s new CEO, Andy Jassy? |Marc Bain |February 3, 2021 |Quartz 

To try to get to the bottom of this, Feingold and Svorenčík undertook an astonishingly systematic and exhaustive preliminary survey spanning some 27 countries and their libraries, private owners, and booksellers. Who Said Nobody Read Isaac Newton? - Issue 95: Escape |Caleb Scharf |January 20, 2021 |Nautilus 

It was also selling well at competing bookseller Barnes & Noble, where it ranked second among the company’s top 100 titles. George Orwell’s “1984” is topping Amazon’s best sellers |Karen Ho |January 13, 2021 |Quartz 

The Hard Tomorrow is available from its publisher, Drawn & Quarterly, through Bookshop, and through your local bookseller. One Good Thing: The future is uncertain. This graphic novel gave me hope anyway. |Alissa Wilkinson |January 1, 2021 |Vox 

The following month Suzuki returned with Major Yoshimi Taniguchi, by then a bookseller. The Week in Death: The Last to Surrender |The Telegraph |January 19, 2014 |DAILY BEAST 

The onetime bookseller now dominates retail across the market. Everything You Need to Know About Jeff Bezos |Nina Strochlic |August 5, 2013 |DAILY BEAST 

I work as a bookseller, so I have a dozen of these in my back pocket at all times. How I Write Family Edition: Emma Straub & Peter Straub |Noah Charney |August 29, 2012 |DAILY BEAST 

Bookseller Roxanne Coady has worried about her son for 21 years. 10 Books for My Son the Graduate |Roxanne Coady |June 5, 2012 |DAILY BEAST 

As a bookseller I naturally think the solution to everything is books. 10 Books for My Son the Graduate |Roxanne Coady |June 5, 2012 |DAILY BEAST 

He was a bookseller, but better known as a translator of the German contributor to the Gentleman's Magazine, &c. The Every Day Book of History and Chronology |Joel Munsell 

Joseph Towers died; a printer, bookseller, and afterwards a preacher with the title of LL. The Every Day Book of History and Chronology |Joel Munsell 

One bookseller's shop, where books are extravagantly dear, exists in the low town, and one other in the ascent to the upper. Journal of a Voyage to Brazil |Maria Graham 

There is not one bookseller in Pernambuco, and the population of its different parishes amounts to 70,000 souls! Journal of a Voyage to Brazil |Maria Graham 

Andrew Millar, the most distinguished bookseller of his times, died in London. The Every Day Book of History and Chronology |Joel Munsell