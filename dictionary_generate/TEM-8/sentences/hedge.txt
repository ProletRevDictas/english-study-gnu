In 2018, Citi was targeted by ValueAct, an activist hedge fund that has pushed for shakeups at other companies. Citi CEO Jane Fraser’s biggest challenge, in one chart |John Detrixhe |September 10, 2020 |Quartz 

In January 2019, he formally set up a hedge fund called Next Alpha, which today, he says, has about $30 million under management. Can an A.I. hedge fund beat the market? |Jeremy Kahn |August 25, 2020 |Fortune 

This has nurtured a renaissance for many macro hedge funds, with some notching up gains not seen since their 1990s heyday. Hedge Fund ‘Pirates’ Set Sail Again |Daniel Malloy |August 21, 2020 |Ozy 

The average global macro fund is flat this year, according to data from Aurum Fund Management, a firm that invests in hedge funds. Hedge Fund ‘Pirates’ Set Sail Again |Daniel Malloy |August 21, 2020 |Ozy 

The hedge fund publishes reports on companies that it says has misled investors, while making a bet on the stock price falling. The world’s best known short seller says Tesla traders are feeding on testosterone |John Detrixhe |July 30, 2020 |Quartz 

All the money from cuts to public education can go toward more tax breaks for hedge-fund types. Hunger Games Comes to New York State’s Public Schools |Zephyr Teachout |November 26, 2014 |DAILY BEAST 

Third, the destruction: These hedge-fund managers want to eliminate all limits and oversight of charter schools. Hunger Games Comes to New York State’s Public Schools |Zephyr Teachout |November 26, 2014 |DAILY BEAST 

The center-right hedge fund clique known as Third Way, and associated Blue Dogs and hangers on. One of the Midterms’ Little-Noticed Big Losers: The NRA |Cliff Schecter |November 10, 2014 |DAILY BEAST 

Dunham makes fun of herself only so that she can then hedge and embrace an authoritative role. Time to Grow Up, Lena Dunham |Emily Shire |October 10, 2014 |DAILY BEAST 

In 1998, when the hedge fund Long Term Capital Management blew up, the New York Fed helped organize a $3.65 billion bailout. The Incredible 'Wussiness' Of The Fed Vs Goldman Sachs—Caught On Tape |Daniel Gross |September 26, 2014 |DAILY BEAST 

Where there is no hedge, the possession shall be spoiled: and where there is no wife, he mourneth that is in want. The Bible, Douay-Rheims Version |Various 

I will take away the hedge thereof, and it shall be wasted: I will break down the wall thereof, and it shall be trodden down. The Bible, Douay-Rheims Version |Various 

We cut over the fields at the back with him between usstraight as the crow fliesthrough hedge and ditch. Oliver Twist, Vol. II (of 3) |Charles Dickens 

Into the houses, and behind every garden fence and hedge, the retreating Federals gathered. The Courier of the Ozarks |Byron A. Dunn 

They crashed against Sir Edward Bruce's division, which received them 'like a dense hedge' or 'wood.' King Robert the Bruce |A. F. Murison