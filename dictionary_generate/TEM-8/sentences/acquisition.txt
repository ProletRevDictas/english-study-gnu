With a lack of scripted content finished on time, reality television and acquisitions of already produced international shows have been used to fill the gap. After years of ‘too much TV,’ the pandemic means there’s now barely enough |Aric Jenkins |August 27, 2020 |Fortune 

Microsoft is the only company to publicly confirm acquisition talks. Walmart joins Microsoft in effort to buy TikTok |Verne Kopytoff |August 27, 2020 |Fortune 

Benioff hinted that Salesforce is unlikely to go on an acquisition spree. Salesforce shares soar 13% on strong growth despite the pandemic |jonathanvanian2015 |August 25, 2020 |Fortune 

LinkedIn’s registered user base has grown from 400 million people at the time the acquisition closed in December 2016 to 706 million people as of July 2020. Microsoft’s hands-off handling of LinkedIn offers model for potential TikTok acquisition |Tim Peterson |August 24, 2020 |Digiday 

Former House speaker Paul Ryan is starting a special purpose acquisition company that will seek to raise about $300 million, the Wall Street Journal reports, citing sources. Paul Ryan jumps onto the SPAC train |Lucinda Shen |August 21, 2020 |Fortune 

The user fee on duck stamps goes exclusively to funding federal acquisition of wetlands as wildlife habitat. Nazis, Sunscreen, and Sea Gull Eggs: Congress in 2014 Was Hella Productive |Ben Jacobs |December 29, 2014 |DAILY BEAST 

“EOTS is a poster child for one of the ills of the acquisition process,” the official said. Newest U.S. Stealth Fighter ‘10 Years Behind’ Older Jets |Dave Majumdar |December 26, 2014 |DAILY BEAST 

ISIS had broadly advertised its acquisition of a broad range of U.S.-made weapons during its rampage across Iraq. ISIS Video: America’s Air Dropped Weapons Now in Our Hands |Josh Rogin |October 21, 2014 |DAILY BEAST 

Last year, the company raised funding at a $2 billion valuation and shot down a $3 billion acquisition offer from Facebook. ‘The Snappening’ Is Real: 90,000 Private Photos and 9,000 Hacked Snapchat Videos Leak Online |Marlow Stern |October 13, 2014 |DAILY BEAST 

Whoever pulled the trigger (so to speak) on this acquisition may have just been caught up in the moment. Why Does My Kids’ Elementary School Need a Tank? |Andy Hinds |September 13, 2014 |DAILY BEAST 

In brief, Chumru abused the English with such an air that he was regarded by the rebels as quite an acquisition. The Red Year |Louis Tracy 

It is skirted by houses and gardens and is a valuable acquisition to the town. Narrative of a Survey of the Intertropical and Western Coasts of Australia] [Volume 2 of 2] |Phillip Parker King 

John Bones, lawyer, age twenty-six, was a recent acquisition to Coldriver village. Scattergood Baines |Clarence Budington Kelland 

And his mind at least was happy in its new sense of expansion and acquisition, its increasing and developing powers. Ancestors |Gertrude Atherton 

It is when studies requiring abstruse thought are reached that the facility in acquisition of the savage races comes to an end. Man And His Ancestor |Charles Morris