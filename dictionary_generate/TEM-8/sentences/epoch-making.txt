Everyone out there who says, “Charlie Hebdo provoked,” is making the same fundamental error. Ayaan Hirsi Ali: Our Duty Is to Keep Charlie Hebdo Alive |Ayaan Hirsi Ali |January 8, 2015 |DAILY BEAST 

As far as I can tell, this magazine spent as much time making fun of French politicians as it did of Muslims or Islam. Harry Shearer on The Dangerous Business of Satire |Lloyd Grove |January 8, 2015 |DAILY BEAST 

The pulps brought new readers to serious fiction, making it less intimidating with alluring art and low prices. How Pulp Fiction Saved Literature |Wendy Smith |January 8, 2015 |DAILY BEAST 

This is not making the 228,000 residents of Irving, Texas feel very relaxed. 26 Earthquakes Later, Fracking’s Smoking Gun Is in Texas |James Joiner |January 7, 2015 |DAILY BEAST 

First, they allow Paul to siphon off attention from whichever potential candidate is making news. Rand Paul’s Passive-Aggressive Trolling Campaign |Olivia Nuzzi |January 6, 2015 |DAILY BEAST 

She looked so sweet when she said it, standing and smiling there in the middle of the floor, the door-way making a frame for her. Music-Study in Germany |Amy Fay 

Sleek finds it far harder work than fortune-making; but he pursues his Will-o'-the-Wisp with untiring energy. The Pit Town Coronet, Volume I (of 3) |Charles James Wills 

Besides these, twenty thousand Indians are under the care of secular priests—making a total of two hundred and five thousand. The Philippine Islands, 1493-1898, Volume XX, 1621-1624 |Various 

Robert Fitzgerald received a patent in England for making salt water fresh. The Every Day Book of History and Chronology |Joel Munsell 

So intelligent were her methods that she doubtless had great influence in making the memory of his art enduring. Women in the fine arts, from the Seventh Century B.C. to the Twentieth Century A.D. |Clara Erskine Clement