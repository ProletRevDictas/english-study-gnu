It all seemed do-able, since he had already scoped out the area, found tunnels, and had caught star-nosed moles there before. How to hunt for star-nosed moles (and their holes) |Kenneth Catania |September 15, 2020 |Popular-Science 

The team has been led by one of the most unusual stars, in an offense that produces a most unusual shot chart. Everything Should Be On The Table For The Houston Rockets. Even James Harden’s Future. |Chris Herring (chris.herring@fivethirtyeight.com) |September 14, 2020 |FiveThirtyEight 

With 700 million users, Alipay is another rising star in the search landscape. Are search engines dead in China? |Ada Luo |September 14, 2020 |Search Engine Watch 

King Richard, a biopic about Venus and Serena Williams’ father starring Will Smith, shifted from November 25, 2020, to November 19, 2021. How the coronavirus outbreak is roiling the film and entertainment industries |Alissa Wilkinson |September 11, 2020 |Vox 

We can directly link it to the gravitational influence from the three stars that are in the center of the disk. A strange dusty disk could hide a planet betwixt three stars |Paola Rosa-Aquino |September 11, 2020 |Popular-Science 

Former Red Sox star Curt Schilling says his politics are keeping him out of Cooperstown. Conservative Curt Says His Politics, Not His Pitching, Kept Him Out of the Hall of Fame |Ben Jacobs |January 9, 2015 |DAILY BEAST 

It was seen by a small delegation of star-struck prelates and dignitaries who later described the film as “moving.” Pope Francis Has the Pleasure of Meeting Angelina Jolie for a Few Seconds |Barbie Latza Nadeau |January 8, 2015 |DAILY BEAST 

I just recently rewatched all six Star Wars movies the other day… Oh wow, from the beginning? Patton Oswalt on Fighting Conservatives With Satire |William O’Connor |January 6, 2015 |DAILY BEAST 

The star announces he is to marry his 27-year-old boyfriend. Meet Stephen Fry’s Future Husband (Who Is Less Than Half His Age) |Tom Sykes |January 6, 2015 |DAILY BEAST 

Real Housewives of New Jersey star Teresa Giudice turned herself in to serve a 15-month sentence for bankruptcy fraud. How a ‘Real Housewife’ Survives Prison: ‘I Don’t See [Teresa Giudice] Having a Cakewalk Here’ |Michael Howard |January 6, 2015 |DAILY BEAST 

I had no idea who they were, as the Grand Duke was in morning costume, and had no star or decoration to distinguish him. Music-Study in Germany |Amy Fay 

When rapidly deposited, as by artificial precipitation, triple phosphate often takes feathery, star- or leaf-like forms. A Manual of Clinical Diagnosis |James Campbell Todd 

She liked him less than ever, nevertheless wished that he were her brother and the rising star in American politics. Ancestors |Gertrude Atherton 

According to a weekly paper not only is Constance Binney a famous screen star, but she is also a first-class ukelele player. Punch, or the London Charivari, Volume 158, April 28, 1920 |Various 

I do not quite agree with the 'Star' as to the Fire-crest not being "very uncommon," though it occasionally occurs. Birds of Guernsey (1879) |Cecil Smith