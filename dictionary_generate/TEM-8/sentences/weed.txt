They had superhuman speed and could run up my arm and jump into the weeds before I could react. How to hunt for star-nosed moles (and their holes) |Kenneth Catania |September 15, 2020 |Popular-Science 

Ask any farmer or gardener and they’ll confirm that a few hours of hauling dirt, pulling weeds, and pushing seeds into soil is as exhausting as a gnarly mountain-bike ride. A 15-Week Exercise Plan for Kids and Families |Krista Langlois |September 12, 2020 |Outside Online 

Like cleaning out that garden, it will take some work, some knowledge, and the right tools thought the dirt and weeds, in this case, are purely digital. How to teach an old blog new SEO tricks |Tom Pick |August 27, 2020 |Search Engine Watch 

Marijuana Moment’s newsletter aggregates both US and international legal developments in the weed world, and links to related stories about cannabis culture, business, and science. The best cannabis industry newsletters, sites, and podcasts |Jenni Avins |August 13, 2020 |Quartz 

This competition is a trial-by-fire for mitochondria and weeds out even the slightest mismatch. Sex Is Driven by the Impetus to Change - Issue 88: Love & Sex |Jill Neimark |August 12, 2020 |Nautilus 

The pale, baby-faced, red-cheeked rapper is furiously puffing away at a hastily-made blunt crammed with low-grade weed. The Cult of Yung Lean: ‘I’m Building An Anarchistic Society From the Ground Up’ |Marlow Stern |January 4, 2015 |DAILY BEAST 

Antoine himself had recently been arrested on a six-year-old warrant for a dime bag of weed. Ground Zero of the NYPD Slowdown |Batya Ungar-Sargon |January 1, 2015 |DAILY BEAST 

Maurice, a 22-year-old father, says the cops planted weed on him after he was arrested once. Ground Zero of the NYPD Slowdown |Batya Ungar-Sargon |January 1, 2015 |DAILY BEAST 

There was a lot of weed, he snorted a ton of coke, was guzzling Bloody Marys. The Unbelievable (True) Story of the World’s Most Infamous Hash Smuggler |Marlow Stern |November 14, 2014 |DAILY BEAST 

As our correspondent discovers, a little weed can go a long way. Meet the Julia Child of Weed |Justin Jones |November 13, 2014 |DAILY BEAST 

They are so rich in harmony, so weird, so wild, that when you hear them you are like a sea-weed cast upon the bosom of the ocean. Music-Study in Germany |Amy Fay 

The weed growing over every water, and at the bank of the river, shall be pulled up before all grass. The Bible, Douay-Rheims Version |Various 

Lamb fills his case, and lights this the ne plus ultra of a soothing weed. The Pit Town Coronet, Volume I (of 3) |Charles James Wills 

We should infer also from some of the early stage plays, that the "players" used the weed even when acting their parts. Tobacco; Its History, Varieties, Culture, Manufacture and Commerce |E. R. Billings. 

In no part of the world is smoking so common as in South America; here all classes and all ages use the weed. Tobacco; Its History, Varieties, Culture, Manufacture and Commerce |E. R. Billings.