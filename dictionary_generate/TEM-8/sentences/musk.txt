Although a musk ox looks like a dirty dust mop on four tiny hooves, it’s formidable. Headbutts hurt the brain, even for a musk ox |Laura Sanders |May 25, 2022 |Science News 

Marble Fruit has notes of pear and nectarine, plus jasmine warm musk and is unisex. The Best New Launches from Food52, L.L. Bean, and More |Jillian Lucas |July 9, 2021 |The Daily Beast 

For example, “New York City” has notes of jasmine, grapefruit, water lilies, musk, and concrete to evoke springtime in Central Park and crowded department stores. The best graduation gifts for her: Fun picks for middle school, high school, and college girls |Carsen Joenk |April 16, 2021 |Popular-Science 

Nothing could better describe the differences between Branson and Musk than these two events. Tycoons in Space: One in Orbit and One Still Grounded |Clive Irving |October 5, 2014 |DAILY BEAST 

Richard Branson and Elon Musk have taken very different paths in their race to the heavens. Tycoons in Space: One in Orbit and One Still Grounded |Clive Irving |October 5, 2014 |DAILY BEAST 

Musk is there, too, having taken a three-year lease at Spaceport for testing reusable rockets. Tycoons in Space: One in Orbit and One Still Grounded |Clive Irving |October 5, 2014 |DAILY BEAST 

Like Ford, Musk started by building a high-end sports car that would be a toy for the ultra-rich. From the Model T to the Model S |The Daily Beast |September 24, 2014 |DAILY BEAST 

At first blush, Henry Ford, the founder of Ford, and Elon Musk, the founder of Tesla Motors, would seem to have little in common. From the Model T to the Model S |The Daily Beast |September 24, 2014 |DAILY BEAST 

The spotted fawn, the musk-deer, gazelles and antelopes, all seemed to answer the call of the music. Kari the Elephant |Dhan Gopal Mukerji 

Having done this, add a quarter of an ounce of essence of lemon; half-a-drachm of musk, and half-a-drachm of oil of thyme. The Ladies' Book of Etiquette, and Manual of Politeness |Florence Hartley 

After the boiling has ceased, but before it is cold, add one gill of spirits of wine, and a grain of musk. The Ladies' Book of Etiquette, and Manual of Politeness |Florence Hartley 

Muske-catthe civet-cat; applied as a term of contempt to a fop, as being a person perfumed with musk. The Fatal Dowry |Philip Massinger 

THE generality of naturalists are of opinion that the perfume called civet, or musk, is furnished only by one species of animals. Buffon's Natural History. Volume VII (of 10) |Georges Louis Leclerc de Buffon