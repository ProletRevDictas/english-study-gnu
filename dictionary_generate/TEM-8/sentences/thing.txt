That will probably be a theme for the whole year, but it is good timing for us to reset, focus on our game, get some key guys back and get other guys back who are nursing some things. For first time in three weeks, Capitals could be at full strength Sunday |Samantha Pell |February 11, 2021 |Washington Post 

That almost cost me my life, but it’s also the things that saved my life. Ryan Newman returns to Daytona 500, with no memory of 2020 crash and ‘therefore no fear’ |Cindy Boren |February 11, 2021 |Washington Post 

“Probably more can be done than just masks and distancing because we are doing all those things and if that alone was enough, we wouldn’t be in this situation,” he said. The Caps are dealing with an unexpected break. They hope to use it to recover and reset. |Samantha Pell |February 10, 2021 |Washington Post 

If you’ve been struggling to give your business the boost it deserves, implementing these strategies will go a long way in turning things around. A small business’ step-by-step guide to dominating local search in 2021 |Joseph Dyson |February 10, 2021 |Search Engine Watch 

To say that a certain percentage of our population is not able to do these things in this day and age is not morally right, it’s not fair, and it is has to be dealt with. Lacking a Lifeline: How a federal effort to help low-income Americans pay their phone bills failed amid the pandemic |Tony Romm |February 9, 2021 |Washington Post 

There is no such thing as speech so hateful or offensive it somehow “justifies” or “legitimizes” the use of violence. Trolls and Martyrdom: Je Ne Suis Pas Charlie |Arthur Chu |January 9, 2015 |DAILY BEAST 

“They sure took the Sony thing seriously,” Attkisson said dryly. Ex-CBS Reporter Sharyl Attkisson’s Battle Royale With the Feds |Lloyd Grove |January 9, 2015 |DAILY BEAST 

But the other thing that needs to be done is for us citizens to do. Ayaan Hirsi Ali: Our Duty Is to Keep Charlie Hebdo Alive |Ayaan Hirsi Ali |January 8, 2015 |DAILY BEAST 

No one seems to know who that is—or why they would want to do such a thing. Phylicia Rashad and the Cult of Cosby Truthers |Stereo Williams |January 8, 2015 |DAILY BEAST 

When I was in Holland, this is the kind of thing people feared. Ayaan Hirsi Ali: Our Duty Is to Keep Charlie Hebdo Alive |Ayaan Hirsi Ali |January 8, 2015 |DAILY BEAST 

"There's just one thing I'd like to ask, if you don't mind," said Cynthia, coming suddenly out of a brown study. The Boarded-Up House |Augusta Huiell Seaman 

That it is a reasonable and proper thing to ask our statesmen and politicians: what is going to happen to the world? The Salvaging Of Civilisation |H. G. (Herbert George) Wells 

Of course the expression of this value is modified and characterized by the nature of the thing spoken of. Expressive Voice Culture |Jessie Eldridge Southwick 

It is only just to say, that the officers exhibited a degree of courage far beyond any thing we had expected from them. Blackwood's Edinburgh Magazine, No. CCCXXXIX. January, 1844. Vol. LV. |Various 

And is this a mere fantastic talk, or is this a thing that could be done and that ought to be done? The Salvaging Of Civilisation |H. G. (Herbert George) Wells