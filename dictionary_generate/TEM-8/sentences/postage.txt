She has passed legislation improving young people’s access to the vote, eliminated postage fees on mail ballots and many others. Politics Report: Shirley Weber’s Shoes to Fill |Scott Lewis |December 26, 2020 |Voice of San Diego 

E-delivered ballots require voters to provide their own envelope and affix postage. Maryland voting guide: What to know about early voting, mail-in ballots |Erin Cox |October 29, 2020 |Washington Post 

In a statement, the department listed several other steps it has taken beyond the minimum required by law to increase access, including providing prepaid postage for returning mail ballots. Pennsylvania’s New Vote-by-Mail Law Expands Access for Everyone Except the Poor |by Jonathan Lai, Samantha Melamed and Michaelle Bond, The Philadelphia Inquirer |October 22, 2020 |ProPublica 

Of the five, Colorado and Utah ask voters to pay postage to mail their votes. Making mail-in voting free is a quick way to raise voter turnout |Annalisa Merelli |September 23, 2020 |Quartz 

If not, contact your local post office and ask what its policy is on ballot postage. Reporting Recipe: How to Report on Voting by Mail |by Rachel Glickhouse and Jessica Huseman |September 2, 2020 |ProPublica 

“Honoring predator Harvey Milk on a U.S. postage stamp is disturbing to say the least,” reads the press release. Fringe Factor: Keep Harvey Milk Off Our Mail! |Caitlin Dickson |June 1, 2014 |DAILY BEAST 

Bob Larson was raised in McCook, Nebraska, a postage-stamp town of around 7,000 people and 23 churches. My $295 Skype Exorcism |Scott Bixby |February 6, 2014 |DAILY BEAST 

She was relentless in her efforts to have my father honored with a United States postage stamp. How Betty Shabazz Persevered After Her Husband, Malcolm X, Was Killed |Ilyasah Shabazz |February 2, 2013 |DAILY BEAST 

There is no limit on the amount of mail one can send a voter, and candidates do not get a special discount on postage. Is Super PACs’ Influence on the 2012 Presidential Election Overhyped? |Ben Jacobs |February 16, 2012 |DAILY BEAST 

Then Harris walked onto the postage stamp-sized stage, flashed that beauty queen smile and urged her crowd to hunker down. The Nail-Biter Race in California |Gina Piccalo |November 3, 2010 |DAILY BEAST 

At that time, the postage on letters from that region was very high, sometimes as much as fifty or sixty cents, or even a dollar. The Boarded-Up House |Augusta Huiell Seaman 

The stamps, in remote districts, would frequently require more in postage to obtain than the value of the tax. The Eve of the Revolution |Carl Becker 

The contract becomes complete when the policy is put in the mail, postage prepaid, for delivery in due course to the insured. Putnam's Handy Law Book for the Layman |Albert Sidney Bolles 

On the upper right hand corner of your envelope, put your postage-stamp. The Ladies' Book of Etiquette, and Manual of Politeness |Florence Hartley 

If the enquiry refers to matters interesting only to yourself, enclose a postage-stamp for the reply. The Ladies' Book of Etiquette, and Manual of Politeness |Florence Hartley