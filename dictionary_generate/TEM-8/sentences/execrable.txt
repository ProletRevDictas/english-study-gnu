That “Plan A” has failed, a result of over confidence, bad intelligence, worse generalship, execrable logistics, and terrible on-the-ground leadership. The Ukraine War Is Becoming Putin's Vietnam |James Stavridis |June 9, 2022 |Time 

As disappointing as such a glib argument would be from a better-made series, it renders Cherry Flavor fully execrable. Netflix Horror Noir Brand New Cherry Flavor Tastes Terrible |Judy Berman |August 6, 2021 |Time 

Those execrable three words are murmured by sorrowful elected officials in times of tragedy, but rarely burdened with any actual plans or action. The ‘Commander in Grief’ steps up to the microphone |Monica Hesse |March 12, 2021 |Washington Post 

Added to the mix are a supporting cast of conjured “demons” including mid-20th writers and chat show regulars James Baldwin, Truman Capote, and the execrable Ayn Rand. Virtual theater continues with hopes for return to live audiences in fall |Patrick Folliard |March 10, 2021 |Washington Blade 

Anything, for example, to take our minds off the execrable “dining experience.” Your iPod (Most Likely) Won’t Bring Down the Plane |Clive Irving |October 31, 2013 |DAILY BEAST 

So I'm not criticizing her, and I'm certainly not defending DW Griffith's execrable opinions. The Economic History of Stereotypes |Megan McArdle |June 3, 2013 |DAILY BEAST 

If Kennedy displayed execrable judgment during the war, he did nothing to redeem it after the conflict had ended. “The Patriarch”: Joseph Kennedy Sr.’s Outsized Life |Jacob Heilbrunn |November 21, 2012 |DAILY BEAST 

The road was execrable; full of holes, pits, and puddles, in which our poor beasts often sank above their knees. A Woman's Journey Round the World |Ida Pfeiffer 

I wouldnt dwess in such execrable taste for any sum you could mentionno, sir! The Girls of Central High on the Stage |Gertrude W. Morrison 

He urged upon the Assembly the adoption of immediate and energetic measures to arrest these execrable deeds of lawless violence. Madame Roland, Makers of History |John S. C. Abbott 

As I said before, the provisions were execrable; the remnants of the first cabin were sent to us poor wretches. A Woman's Journey Round the World |Ida Pfeiffer 

I cannot contemplate such loveliness and associate it with the execrable sin which calls down vengeance upon this house. The Circular Study |Anna Katharine Green