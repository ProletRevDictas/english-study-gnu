The rules appear to have had a noticeable positive impact, as only one player has tested positive over the past two weeks, a steep drop from January’s rash of cases leaguewide. The NBA’s week of controversies show how hard life is outside of the bubble |Ben Golliver |February 12, 2021 |Washington Post 

It caused fever, then a rash, which over the course of a few days developed into the skin-covering lumps that are the disease’s trademark. Smallpox used to kill millions of people every year. Here’s how humans beat it. |Kelsey Piper |February 5, 2021 |Vox 

For Grantham, that divergence tells us more about the raging market fever than the rash of new SPACs. Investing legends Carl Icahn and Jeremy Grantham see a stock market bubble |Shawn Tully |January 8, 2021 |Fortune 

In the context of the rash of restaurant closures, however, thousands of workers lost their jobs, whether outright layoffs or temporary furloughs. Restaurants, bars put creativity on the menu in 2020 |Evan Caplan |December 30, 2020 |Washington Blade 

This reaction, Parikh noted, is not believed to be connected to the other types of rashes caused by the coronavirus, including the unusual frostbite-like patches that have been observed on people’s toes and sometimes fingers. Covid may cause rashes and swelling. That doesn’t mean you’re ‘allergic,’ experts say. |Allyson Chiu |December 10, 2020 |Washington Post 

And it is not clear that there have been a rash of lawsuits from outraged parents over aggressive Christmastime greetings. A Field General in the War on Christmas |David Freedlander |December 24, 2014 |DAILY BEAST 

The results of that rash decision, the most dire of which has been the rise of ISIS, are now plain for us to see. ‘America in Retreat’: Why Neo-Isolationism Exploded Under Obama and What We Can Do About It |James Kirchick |December 1, 2014 |DAILY BEAST 

His judgments are not rash or driven by insecurity, fear, and a longing for the past. The Walking Dead’s Luke Skywalker: Rick Grimes Is the Perfect Modern-Day Mythical Hero |Regina Lizik |October 28, 2014 |DAILY BEAST 

A rash of crimes against gay and trans* people point to a possible brewing class resentment in the “hip” parts of Brooklyn. Is Brooklyn Becoming Unsafe for Gays? It Depends On Which Ones |Jay Michaelson |October 18, 2014 |DAILY BEAST 

Most have been straightforward cases where the child came in with the characteristic rash. Predator Doctors Take Advantage of Patients With ‘Chronic Lyme’ Scam |Russell Saunders |September 19, 2014 |DAILY BEAST 

Hence Napoleon was driven more and more to trust to the advice of the rash, unstable King of Naples. Napoleon's Marshals |R. P. Dunn-Pattison 

Garnache need not plague himself with vexation that his rash temper alone had wrought his ruin now. St. Martin's Summer |Rafael Sabatini 

Should he hire a horse and kill the animal by rash driving, he would be liable for its value. Putnam's Handy Law Book for the Layman |Albert Sidney Bolles 

But glorious as his conduct was, his rash impetuosity more than once seriously compromised Napoleon's plans. Napoleon's Marshals |R. P. Dunn-Pattison 

He was a mere boy, who, in a rash skirmish with some of our hussars, was wounded severely and taken prisoner. Blackwood's Edinburgh Magazine, No. CCCXXXIX. January, 1844. Vol. LV. |Various