The man stuffs the slip in his wallet, where it’s soon forgotten. Easy interventions like revamping forms help people show up to court |Sujata Gupta |October 8, 2020 |Science News 

Your stay comes with a slip on Lake Glenville, where you can launch the property’s complimentary canoe, kayak, and SUPs. Our Favorite Hipcamp in Every State |Alison Van Houten |October 1, 2020 |Outside Online 

When it was delayed until late August, military officials did not cite a reason for the schedule slip. Delta IV Heavy rocket delayed again, raising concerns of aging infrastructure |Eric Berger |September 30, 2020 |Ars Technica 

I’m using an iPhone 11 Pro, but based on the rigidity of the tension on the swivel, I have no doubt that people with larger phones—like the 11 Pro Max—won’t see any slips either. The Best Phone Mount for Your Car Costs $12 |Jakob Schiller |September 20, 2020 |Outside Online 

There are other drawbacks to this as well—doing burpees and other similar movements on a hard surface can hurt you, not to mention that accumulated sweat on wooden or ceramic floors pose a serious slip hazard. Working out at home? Here’s how to keep your house from smelling like a gym. |Harry Guinness |September 3, 2020 |Popular-Science 

Block 3F is slated for release in 2019, but who knows how much that will slip? New U.S. Stealth Jet Can’t Fire Its Gun Until 2019 |Dave Majumdar |December 31, 2014 |DAILY BEAST 

Less than a minute into her big break, Slate let slip a highly audible F-bomb instead of the scripted “freaking.” The Curious Little Shell That Restarted Jenny Slate’s Career |Luke Hopping |December 15, 2014 |DAILY BEAST 

I know that Detroit is losing market share in auto sales, but how did they let the Motown sound slip out of their hands? The Best Albums of 2014 |Ted Gioia |December 13, 2014 |DAILY BEAST 

Both Time and CNN reinstated Zakaria after determining the slip-up was “an isolated incident.” Can Fareed Zakaria Survive A Plagiarism Firestorm? |Lloyd Grove |November 12, 2014 |DAILY BEAST 

To his fellow survivors and to the audience, this delusion indicates another slip on a downward spiral. The Walking Dead’s Luke Skywalker: Rick Grimes Is the Perfect Modern-Day Mythical Hero |Regina Lizik |October 28, 2014 |DAILY BEAST 

Q was a Queen, who wore a silk slip; R was a Robber, and wanted a whip. Boys and Girls Bookshelf; a Practical Plan of Character Building, Volume I (of 17) |Various 

So my mother begged me to slip into the Rooms, with what was left, and try to get something back. Rosemary in Search of a Father |C. N. Williamson 

What the economist does is to slip out of the difficulty altogether by begging the whole question. The Unsolved Riddle of Social Justice |Stephen Leacock 

Only then did I own that by hook or by crook—and mostly by crook, I was forced to suspect—they had purposely given me the slip. Raw Gold |Bertrand W. Sinclair 

It's easy for a prisoner t' slip a note to a friend that happens t' be mountin' guard. Raw Gold |Bertrand W. Sinclair