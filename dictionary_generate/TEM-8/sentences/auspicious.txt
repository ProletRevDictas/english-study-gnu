To those in the business, the timing of these trends and the steady uptick in astrological interest feels auspicious. High-Tech Astrology Apps Claim to Be More Personalized Than Ever. Gen Z-ers Are Turning Out to Be Believers |Raisa Bruner |July 23, 2021 |Time 

I went to France at an auspicious time to be making this private discovery. The Pandemic Made Us Strangers to Ourselves. Will We Have Learned Anything When It's Over? |Sherry Turkle |March 16, 2021 |Time 

It was fairly auspicious timing, as those things go, falling the same day seven public health departments launched a joint shelter-in-place order in its native California. Chorus brings a social layer to meditation |Brian Heater |February 18, 2021 |TechCrunch 

There were plenty of technical issues at first, leading to a less than auspicious first impression. Rollables are the new foldables |Brian Heater |January 12, 2021 |TechCrunch 

She takes over the position at an auspicious time for the newspaper. ‘Unstoppable innovator’: The meteoric rise of Meredith Kopit Levien, the next New York Times CEO |Steven Perlberg |August 19, 2020 |Digiday 

But even though 2014 is only halfway over, it already seems like an auspicious year for the “other” parent. Is 2014 ‘Year of the Dad’? |Andy Hinds |June 9, 2014 |DAILY BEAST 

The timing is auspicious for such a move with the Tiananmen anniversary fast approaching. Why China Hates This New D.C. Street Name |David Keyes |June 3, 2014 |DAILY BEAST 

It was an auspicious time for a 21-year-old woman to publicly document her sexual escapades and humiliations. Is This Dildo-Licking, Dominatrix-Loving Vogue Blogger the New Face of Feminism? |Lizzie Crocker |May 22, 2014 |DAILY BEAST 

Yet a screaming headline on the Drudge Report—SHARPTON WAS FBI MOB RAT—was hardly an auspicious way to begin a momentous week. Al Sharpton: I’m No Snitch |Lloyd Grove |April 8, 2014 |DAILY BEAST 

That heritage is probably just as auspicious as is proprietorship of The Tonight Show. Jimmy Fallon’s Brilliant ‘Tonight Show’ Debut |Tom Shales |February 18, 2014 |DAILY BEAST 

We soon found opportunity for another deed of charity not dissimilar to this, though its result was more auspicious. The Jesuit Relations and Allied Documents, Vol. II: Acadia, 1612-1614 |Various 

Augustine skilfully seized the auspicious moment; she threw herself into her husband's arms, and pointed to the portrait. At the Sign of the Cat and Racket |Honore de Balzac 

He entered upon his government under auspicious circumstances. The History of England in Three Volumes, Vol.III. |E. Farr and E. H. Nolan 

It was at this auspicious moment that Meade's division advanced alone to pierce the Rebel line. The Boys of '61 |Charles Carleton Coffin. 

It was an auspicious moment,—a golden opportunity, such as does not often come to military commanders. The Boys of '61 |Charles Carleton Coffin.