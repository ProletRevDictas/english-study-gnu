Once again, the invisibility from the government over this population will make Puerto Rico’s path towards cultural competence education and acceptance of the diversity its citizens harder. Puerto Rico: Number one in hate crimes |Alberto J. Valentín |February 2, 2021 |Washington Blade 

The competence of the office is a fair question, but the fraud accusation is absurd. Rick Scott’s zombie claim about voter fraud in 2018 |Salvador Rizzo |February 2, 2021 |Washington Post 

They also offer a sense of mastery and competence that can give them an advantage over more passive forms of entertainment like movies or books. Stressed out? Video games can help—if you follow these tips. |Stan Horazek |January 20, 2021 |Popular-Science 

With most skills, the progression from competence to excellence comes from building upon each new lesson that you learn. Why we’re creating a new content experience for SMX |Henry Powderly |January 13, 2021 |Search Engine Land 

From a humble novice, skill learners progressed to the advanced beginner stage, then on to a sort of midpoint of competence, before climbing further to proficiency, finally summiting at expertise. How I Learned to Surf in Middle Age |Tom Vanderbilt |January 5, 2021 |Outside Online 

If that state is to be further armed with new laws, its competence will be even more on the line. David Cameron's Plan to Fight ISIS Will Likely Involve Racial Profiling |Clive Irving |September 2, 2014 |DAILY BEAST 

When you look at Mona Lisa, what you see is a woman of confidence and competence and compassion. The Life of Lisa Gherardini del Giocondo, the (Most Likely) Real 'Mona Lisa' |Justin Jones |August 9, 2014 |DAILY BEAST 

Voters prize gubernatorial competence above gubernatorial ideology. Memo to the 2016 GOP: Winning Your Home State Matters |Lloyd Green |May 5, 2014 |DAILY BEAST 

Whatever your views on capital punishment, the incident raises questions of basic competence. Mary Fallin’s Killer Fiasco |Michelle Cottle |May 1, 2014 |DAILY BEAST 

But when did they become the litmus test of competence in office? 100 Years of Right (And Left) Moves |Robert Shrum |March 31, 2014 |DAILY BEAST 

Nine-tenths of those who have a competence know what income they have, and are careful not to spend more. Glances at Europe |Horace Greeley 

He is a business man of great competence, and I think he ought to be able to do much to get things on to a ship-shape footing. Gallipoli Diary, Volume I |Ian Hamilton 

There are prairie farmers who would consider what he is leaving behind him a competence. Winston of the Prairie |Harold Bindloss 

At the same time he began lending money on short time, and by speculating with the poorer class he acquired a certain competence. Repertory Of The Comedie Humaine, Complete, A -- Z |Anatole Cerfberr and Jules Franois Christophe 

Page 229 Chapter X a comma was inserted in the phrase 'he would secure the competence he had yearned for, for so many years'. The Pit Town Coronet, Volume II (of 3) |Charles James Wills