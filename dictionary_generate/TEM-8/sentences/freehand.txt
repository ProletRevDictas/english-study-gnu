The main annotation tools are up in the top left corner—you can draw freehand scribbles, create shapes, and drop text on your picture. How to edit screenshots on any device |David Nield |August 24, 2021 |Popular-Science 

Grinding is a really weird thing, when you grind a blade freehand. The Knife Master |Melissa Vaughn, Brendan Vaughn |October 11, 2010 |DAILY BEAST 

Some chefs freehand it, but see the previous note about scars. The Chef's Secret Weapon |Chad Ward |November 3, 2009 |DAILY BEAST 

Pencils for mechanical drawing should be sharpened with a chisel point, and those for freehand work with a round point. An Introduction to Machine Drawing and Design |David Allan Low 

During each year there is regular instruction in freehand drawing, the last year being from life. The Brochure Series of Architectural Illustration, Volume 01, No. 06, June 1895 |Various 

May I ask,” said the teacher, “to have explained the system by which the supposedly freehand drawing in this book has been done? Emmy Lou |George Madden Martin 

By art teaching I hasten to say that I do not mean giving children lessons in freehand drawing and perspective. A Treatise on Parents and Children |George Bernard Shaw 

Mr. Nasmyth was a good freehand draughtsman, and he sketched the Rocket as it stood on the line. Scientific American Supplement, No. 460, October 25, 1884 |Various