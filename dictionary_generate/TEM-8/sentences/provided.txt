“We met the smuggler in the train station; he came to speak with us about the services he provided,” Yazbek says. Ghost Ships of the Mediterranean |Barbie Latza Nadeau |January 6, 2015 |DAILY BEAST 

Can they determine that individual citizens should not have access to rights provided by the Constitution? The Back Alley, Low Blow-Ridden Fight to Stop Gay Marriage in Florida Is Finally Over |Jay Michaelson |January 5, 2015 |DAILY BEAST 

During two years in Iranian custody, Abdolhamid provided crucial details of how Jundullah operated. The Dangerous Drug-Funded Secret War Between Iran and Pakistan |Umar Farooq |December 29, 2014 |DAILY BEAST 

At its peak, his business made as much as $30,000 a year—provided he worked the entire month of December. Kerry Bentivolio: The Congressman Who Believes in Santa Claus |Ben Jacobs |December 24, 2014 |DAILY BEAST 

The bill also provided $64 billion in war funding through the Overseas Contingency Operations account. Merry Christmas, Defense Contractors! |Veronique de Rugy |December 22, 2014 |DAILY BEAST 

Judged from this point of view only, the elasticity provided by the new law is doubtless adequate. Readings in Money and Banking |Chester Arthur Phillips 

He replied that he had no objections, provided she did not encumber the carriage with bandboxes, which were his utter abhorrence. The Book of Anecdotes and Budget of Fun; |Various 

The governor of the fortress was provided with a safe residence in Egypt, and an annual pension of 75,000 piasters. The Every Day Book of History and Chronology |Joel Munsell 

It is true that the parents so provided think more of the twenty-five francs than they do of the foundling. The Joyous Adventures of Aristide Pujol |William J. Locke 

There are two forms of elasticity, one of quantity and the other of quality, both provided for in the act. Readings in Money and Banking |Chester Arthur Phillips