Finally, Dallas Mavericks owner Mark Cuban, who had instructed his organization not to play the national anthem before home games at the start of the season, was compelled by the league office to reverse course. The NBA’s week of controversies show how hard life is outside of the bubble |Ben Golliver |February 12, 2021 |Washington Post 

That compelled the Lions to ensure there was a trade agreement in place by then. Lions agree to trade Matthew Stafford to Rams for Jared Goff, draft picks |Mark Maske |January 31, 2021 |Washington Post 

So you end up with a system that everyone knows is flawed but feels compelled to use anyway. Testing Whether Fast Kids Make Future Champions |Alex Hutchinson |January 29, 2021 |Outside Online 

There also was something that really compelled us about her withholding a memory from herself that was too chaotic for her admit to herself—and that finally flooding back to her in her final moments. 'Death is a Turning Point': Search Party's Creators Discuss Season 4 and the Shocking Finale Twist |Andrew R. Chow |January 28, 2021 |Time 

“The need to protect the honor of this body compelled me to proceed,” Bell said. Virginia senator who called U.S. Capitol rioters ‘patriots’ is censured |Gregory S. Schneider |January 27, 2021 |Washington Post 

It is “an act of force to compel our enemy to do our will” by spilling blood, and lots of it. How Clausewitz Invented Modern War |James A. Warren |November 24, 2014 |DAILY BEAST 

And the truth that language changes over time does not compel us to endorse any particular change. Go Ahead, End With a Preposition: Grammar Rules We All Can Live With |Nick Romeo |November 3, 2014 |DAILY BEAST 

Which brings us to the images that compel our attention today. When a Picture Is Too Powerful |Jeff Greenfield |September 15, 2014 |DAILY BEAST 

“The government cannot compel a nonbeliever to take an oath that affirms the existence of a supreme being,” Miller added. U.S. Air Force: Swear to God—or Get Out |Dave Majumdar |September 8, 2014 |DAILY BEAST 

Owen will have the power to compel the production of witnesses and documents from the British security and intelligence services. Brits Investigate Assassination of the Spy Who Warned Us About Putin |Nico Hines |July 22, 2014 |DAILY BEAST 

They will try to compel you to confession; and, though you are blameless, you will suffer the cruelest ordeal of transgression. The Pastor's Fire-side Vol. 3 of 4 |Jane Porter 

If there had been a loss, something like that amount, then the courts would compel him to pay. Putnam's Handy Law Book for the Layman |Albert Sidney Bolles 

B could compel him to do so, and the expense must be borne by A because his deed of warranty required him to give a clear title. Putnam's Handy Law Book for the Layman |Albert Sidney Bolles 

Oppressive laws compel me to pay a portion of my hard earnings to support them in their pride and indolence. Madame Roland, Makers of History |John S. C. Abbott 

A defective acknowledgment by a married woman is worthless, nor will any court compel her to make another one. Putnam's Handy Law Book for the Layman |Albert Sidney Bolles