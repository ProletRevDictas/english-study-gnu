World-class outdoor destinations like Vermont don’t just materialize out of the ether. The Key to Vermont’s Outdoor Recreation Economy? Unfettered Altruism. |kklein |August 18, 2021 |Outside Online 

Surprisingly, although anesthesia with ether was first used in surgery at Massachusetts General Hospital in 1846, the specific processes as to how general anesthetics act at multiple sites to produce anesthetic action remain a mystery. New Research Shows How Dopamine Plays a Key Role in Consciousness |Barbara Jacquelyn Sahakian |August 12, 2021 |Singularity Hub 

“So this physical file of her fingerprints had just gone into the ether,” Little said. Three days after becoming a U.S. citizen, she’ll run to become a U.S. Olympian |Adam Kilgore |June 25, 2021 |Washington Post 

Scores of crypto assets, from dogecoin to ether, surged as bitcoin climbed to a record of more than $64,000, up from about $7,000 a year ago, according to CoinDesk data. Coinbase’s IPO is great for its business as bitcoin hits record high |John Detrixhe |April 15, 2021 |Quartz 

Then the band crackled to life, a voice riding a high-frequency wave out of the ether. Inside the Summit-Obsessed World of Ham Radio |Chase Brush |March 14, 2021 |Outside Online 

We would lack a human face as our symbol; we would exist in the ether of ideas with no concrete stake in the ground to tether us. 128 Years Old and Still a Looker: Happy Birthday to Lady Liberty |Elizabeth Mitchell |October 28, 2014 |DAILY BEAST 

A week does not pass without another set of economic numbers blasting through the ether. Don’t Trust the Economic Numbers That Govern Our World |Zachary Karabell |February 27, 2014 |DAILY BEAST 

Users post a photo, add a question, and send it off into the ether to be answered. This Week’s Hot Apps: Jelly, Confide, Yahoo News Digest, Lumosity Mobile |Brian Ries |January 10, 2014 |DAILY BEAST 

“It almost feels sometimes that Matilda is out in the ether,” she says. ‘Matilda’ Star Mara Wilson Reviews ‘Matilda the Musical’ |Ramin Setoodeh |April 16, 2013 |DAILY BEAST 

But that particular sword of Damocles has floated off into the ether. Obama’s Social Security Gambit |Michael Tomasky |April 9, 2013 |DAILY BEAST 

Their heads might have been turned upside-down, so absolutely did they tread upon blue ether. The Awakening and Selected Short Stories |Kate Chopin 

The metal is then removed, and washed successively with very dilute sodium hydroxid solution, alcohol, and ether. A Manual of Clinical Diagnosis |James Campbell Todd 

Suddenly something cluttered up the airways—some sort of interference—and the mystery of the ether died away. The Campfire Girls of Roselawn |Margaret Penrose 

It has the odour of thyme, is sparingly soluble in water, but very soluble in alcohol, ether and in alkaline solutions. Encyclopaedia Britannica, 11th Edition, Volume 5, Slice 2 |Various 

It is very slightly soluble in water, but readily dissolves in alcohol and ether. Encyclopaedia Britannica, 11th Edition, Volume 5, Slice 2 |Various