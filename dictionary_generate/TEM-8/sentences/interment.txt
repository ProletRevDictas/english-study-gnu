Radiocarbon-dated samples from the earthen floor near the woman’s remains date her interment to around 19,200 years ago. A body burned inside a hut 20,000 years ago signaled shifting views of death |Bruce Bower |February 16, 2021 |Science News 

They will remain at half-staff until sunset on the day of his interment. Virginia state senator dies of covid-19 |Laura Vozzella |January 2, 2021 |Washington Post 

After the interment, the department radio dispatcher announced over the air that German was gone, but never to be forgotten. The Teen Love Letters that Led to a Tragic Murder-Suicide in Florida |Michael Daly |March 30, 2014 |DAILY BEAST 

At press time, none had responded to requests to arrange for his disposal or interment. Chasen Murderer's Secret Past |A. L. Bardach |December 15, 2010 |DAILY BEAST 

Moments later, Cardinal Cushing stepped forward to read the prayers of interment. My Life with the Kennedys |Archbishop Hannan, Nancy Collins |June 1, 2010 |DAILY BEAST 

Could you be ready Wednesday night [December 4],” she asked, “for the re-interment of the bodies of our two babies at Arlington? My Life with the Kennedys |Archbishop Hannan, Nancy Collins |June 1, 2010 |DAILY BEAST 

The first interment in the cemetery of Pere la Chaise; it was laid out and prepared by order of Bonaparte. The Every Day Book of History and Chronology |Joel Munsell 

The date was fixed for the interment with military pomp, and immense crowds came out to witness the imposing procession. The Philippine Islands |John Foreman 

As I was wondering at this marvellous spectacle Urania told me this was their usual mode of interment and resurrection. Urania |Camille Flammarion 

Certainely!And from this prison twas the sonnes requestThat his deare father might interment haue. The Fatal Dowry |Philip Massinger 

He remained until the interment was completed, when he returned home with those who attended the funeral. Stories about Animals: with Pictures to Match |Francis C. Woodworth