In a typical haloscope, we generate this magnetic field using a big electromagnet called a superconducting solenoid. Scientists Hunt for an Elusive Particle to Unlock the Mystery of Dark Matter |Ben McAllister |July 31, 2022 |Singularity Hub 

The i4 and iX employ induction in their motors to create electromagnets. The new electric BMW i4 is a nimble driving machine—with a cinematic soundtrack |Dan Carney |October 15, 2021 |Popular-Science 

In conventional tokamaks they are provided by enormous electromagnets made from superconducting wires that need to be cryogenically cooled. New Reactor Design Could Produce First Ever Energy-Positive Fusion Reaction |Edd Gent |October 5, 2020 |Singularity Hub 

Now allow the thread to unwind, and as the watch revolves, pass it back and forth near a powerful electromagnet. The Boy Mechanic, Book 2 |Various 

It should be made of a good grade of steel, tempered and then magnetized by means of a powerful electromagnet. The Boy Mechanic, Book 2 |Various 

A minute later, it reappeared with a large electromagnet and a relux plate, to which were attached a huge pair of silver busbars. Islands of Space |John W Campbell 

We can also wrap wires around this circular iron and make an electromagnet of it. Electricity for the 4-H Scientist |Eric B. Wilson 

This is done by shaping the stationary magnet around the path of the rotating electromagnet. Electricity for the 4-H Scientist |Eric B. Wilson