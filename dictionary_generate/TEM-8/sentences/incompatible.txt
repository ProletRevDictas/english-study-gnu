Violence is incompatible with the exercise of democratic rights and freedoms. Woman dies after shooting in U.S. Capitol; D.C. National Guard activated after mob breaches building |Washington Post Staff |January 7, 2021 |Washington Post 

The law prohibits employers from firing, demoting or otherwise retaliating against workers who refuse to take part in activities they believe are incompatible with public health and safety mandates. Workers are getting fired and penalized for reporting COVID safety violations |lbelanger225 |October 23, 2020 |Fortune 

Additionally, the amount of data being collected by companies was exploding, but they usually spread it across many storage facilities and kept it in incompatible formats. Meet Snowflake, one of the buzziest tech IPOs ever |Aaron Pressman |September 15, 2020 |Fortune 

One section of the ordinance, which regulates behavior around elections, campaign finance and lobbying, bans so-called “incompatible activities.” Barrios Was Paid by Union While Working for Council President |Andrew Keatts and Jesse Marx |September 2, 2020 |Voice of San Diego 

To meet global demand, producers have continued to burn coal at a rate that’s incompatible with international climate goals and a stable climate. The coal industry is finally closing more plants than it’s building |Michael J. Coren |August 6, 2020 |Quartz 

Interesting that those who sat in judgment of him found those two sets of beliefs to be incompatible. In Defense of Blasphemy |Michael Tomasky |January 9, 2015 |DAILY BEAST 

But the site is incompatible with special screen reading software that would make it accessible to blind readers. TEDx Talks Have a Disability Problem—but This Incredible Young Woman Is Working to Change That |Nina Strochlic |November 5, 2014 |DAILY BEAST 

She was a New Woman: She demanded the vote but also a life in which being married and having a career were not incompatible. Wonder Woman’s Creation Story Is Wilder Than You Could Ever Imagine |Tom Arnold-Forster |November 3, 2014 |DAILY BEAST 

Many jazz fans were puzzled by this finding, which seems incompatible with the lackluster sales figures in the genre. Jazz (The Music of Coffee and Donuts) Has Respect, But It Needs Love |Ted Gioia |June 15, 2014 |DAILY BEAST 

Indeed, the idea that an elected official should act exactly as voters demand is incompatible with the way our government works. Voting for Slavery? Jim Wheeler Gets Into Hot Water |Jamelle Bouie |October 30, 2013 |DAILY BEAST 

Frankly, we must say that this is inconceivably incompatible with Señor Paternoʼs clear intelligence. The Philippine Islands |John Foreman 

Glory is not incompatible with youth, and the hero of the 26th February may become the hero of the 9th January. Journal of a Voyage to Brazil |Maria Graham 

Surely they must possess certain merits which do not harmonise together and certain virtues which are incompatible. The 'Characters' of Jean de La Bruyre |Jean de La Bruyre 

Certainly, the existence of such old ruins of the middle-ages is incompatible with the grandeurs of modern Paris. Catherine de' Medici |Honore de Balzac 

You have seen, Madam, in my preceding letter, the incompatible and contradictory ideas which this religion gives us of the Deity. Letters To Eugenia |Paul Henri Thiry Holbach