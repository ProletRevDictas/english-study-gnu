If we did, we wouldn’t be watching the eeriest of parallels unfold at the end of another miscalculated American war. No-No Boy’s Moving Musical Ode to the Fall of Afghanistan |Julian Saporiti |August 25, 2021 |The Daily Beast 

Yale economics professor Ebonya Washington co-authored a 2018 paper, “The Mommy Effect,” which asserts that women miscalculate the difficulty of balancing work and raising a family when they make decisions about their education. The Looming Unemployment Benefits Cliff |Toyloy Brown III |August 9, 2021 |Ozy 

For his part Israeli Prime Minister Binyamin Netanyahu severely miscalculated by failing to recognize that while the Palestinians are down they are not yet out. Israel-Palestine is a State of Permanent Conflict Punctuated by Periodic Carnage. Only the Watching World Can Stop It |Mouin Rabbani |May 13, 2021 |Time 

Richard has lost close friends—military pilots—because they simply miscalculated how far their blades were from the mountainside. What We Know About the Deadly Alaskan Heli Crash |Marc Peruzzi |April 10, 2021 |Outside Online 

They also showed that if researchers only took into account the transmission dynamics of symptomatic individuals, they would likely miscalculate R0. Chasing the Elusive Numbers That Define Epidemics |Jordana Cepelewicz |March 22, 2021 |Quanta Magazine 

And it might even cause Georgians to miscalculate American military support against Russia and foolishly provoke Moscow. Obama's Latest Foreign Slip |Leslie H. Gelb |August 17, 2009 |DAILY BEAST 

A beginner like you might miscalculate the weight, and think what a terrible smash up we'd have then! In Hostile Red |Joseph Altsheler 

Too many of us--the President among the rest, I fear--miscalculate the distance between contingency and desire. Marse Henry (Vol. 2) |Henry Watterson 

It seems to me that those who take such a view quite miscalculate the force of the affection that a man feels for his country. Modern Eloquence: Vol II, After-Dinner Speeches E-O |Various 

Even the oldest sailors are apt to miscalculate the time likely to elapse before the wind can touch them. The Lieutenant and Commander |Basil Hall 

I may miscalculate the time and wait until I am too small to climb upon the ring. The Girl in the Golden Atom |Raymond King Cummings