Nitrogen oxide gases produced by traffic are a powerful precursor to cities’ elevated ozone levels, which can damage the lungs and trigger respiratory ailments. What the pandemic can teach us about ways to reduce air pollution |Carolyn Gramling |January 4, 2021 |Science News 

Current sachets use a few layers of plastic for strength and one of aluminum oxide, to provide a barrier against the liquid oozing out. Graphene gets real: Meet the entrepreneurs bringing the wonder substance to market |David Meyer |December 13, 2020 |Fortune 

D’Arcy notes that one group has combined bricks with metal oxide nanoparticles to help filter pollution out of the air, and another has created bricks that can conduct electricity by incorporating electrodes made from carbon nanomaterials. Scientists Found a Way to Turn Bricks Into Batteries |Edd Gent |August 17, 2020 |Singularity Hub 

The researchers found that the peak drop occurred in April, when globally averaged CO2 emissions and nitrogen oxides fell by roughly 30 percent from baseline, mostly due to reduced driving. Emissions dropped during the COVID-19 pandemic. The climate impact won’t last |Jonathan Lambert |August 7, 2020 |Science News 

The pair then isolated two bacterial species that, when present together, generate manganese oxide. Scientists stumbled across the first known manganese-fueled bacteria |Carolyn Beans |July 21, 2020 |Science News 

Nitric oxide helps to promote the flow of oxygen, so it may improve your endurance and fight fatigue during your sweat session. 6 Healthy Foods to Fight the Flu, Beat Stress and More |DailyBurn |February 5, 2014 |DAILY BEAST 

Campbell is quoted saying “Nitrous oxide can explode on its own.” Branson’s Galactic Obstacles: Tom Bower Puts a Damper on Virgin’s Space Flight Dreams |Clive Irving |January 30, 2014 |DAILY BEAST 

Nitrous oxide, otherwise known as laughing gas, gives one an exhilarating feeling while operating as an anesthetic. Gina Gershon’s Trip to Heaven in the Dentist’s Chair |Gina Gershon |October 23, 2012 |DAILY BEAST 

Tragic indeed are those whip-it rumors about Demi Moore, for the allure of nitrous oxide is hardly a mystery to me. Demi Moore’s Poignant Fall |Tracy Quan |February 15, 2012 |DAILY BEAST 

The zirconium alloy will react with water to produce hydrogen and oxide, but it also produces heat that has to be removed. Japan Nuclear Crisis: What Is a Full Meltdown? |Josh Dzieza |March 15, 2011 |DAILY BEAST 

Reddish and yellowish sandy clay, coloured by oxide of iron, and used as pigments by the natives. Narrative of a Survey of the Intertropical and Western Coasts of Australia] [Volume 2 of 2] |Phillip Parker King 

Blew in two faces & got good looking ore seamed with a black incrustation, oxide of something, but what could not determine. Cabin Fever |B. M. Bower 

The properties of ammoniacal solutions of silver oxide are in entire agreement with this conception. The Elements of Qualitative Chemical Analysis, vol. 1, parts 1 and 2. |Julius Stieglitz 

It is, in fact, on account of this property, that potassium oxide is decomposed by water. The Elements of Qualitative Chemical Analysis, vol. 1, parts 1 and 2. |Julius Stieglitz 

The oxide, K2O, is decomposed by neutralizing hydrogen ions formed by the primary ionization of water. The Elements of Qualitative Chemical Analysis, vol. 1, parts 1 and 2. |Julius Stieglitz