O daughter of Babylon… Happy shall he be, that taketh and dasheth thy little ones against the stones. Up to a Point: Shrugging Our Way Back to War in Iraq |P. J. O’Rourke |August 16, 2014 |DAILY BEAST 

Go forth of the ark, thou, and thy wife, and thy sons, and thy sons' wives with thee. The Story of Noah's Ark From the Bible’s Book of Genesis |The Daily Beast |March 24, 2014 |DAILY BEAST 

Sounds like loving thy neighbor is easier preached than practiced. Christian Monks Square Off at One of Jerusalem’s Holiest Sites |Nina Strochlic |July 4, 2013 |DAILY BEAST 

Although there is certainly overlap between Birthright Israel and Know Thy Heritage, Rabie hesitates to make the comparison. A Palestinian Birthright Trip? |Anna Lekas Miller |April 3, 2013 |DAILY BEAST 

“This is not a vacation,” Know Thy Heritage founder Rateb Rabie stressed in several interviews. A Palestinian Birthright Trip? |Anna Lekas Miller |April 3, 2013 |DAILY BEAST 

Turn away from sin and order thy hands aright, and cleanse thy heart from all offence. The Bible, Douay-Rheims Version |Various 

For it is better that thy children should ask of thee, than that thou look toward the hands of thy children. The Bible, Douay-Rheims Version |Various 

And for fear of being ill spoken of weep bitterly for a day, and then comfort thyself in thy sadness. The Bible, Douay-Rheims Version |Various 

In the time when thou shalt end the days of thy life, and in the time of thy decease, distribute thy inheritance. The Bible, Douay-Rheims Version |Various 

Give not up thy heart to sadness, but drive it from thee: and remember the latter end. The Bible, Douay-Rheims Version |Various