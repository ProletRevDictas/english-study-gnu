There's a whole community out there rushing to find documents, disks, and hard drives from the '80s and '90s before they're savaged by time and bit rot. “Deleted” Nintendo floppy recovered 26 years later, full of Earthbound secrets |Sam Machkovech |June 4, 2021 |Ars Technica 

A great country cannot allow people to come in and savage it, have no consequences, and then wait for the next attack. As Colonial Pipeline recovers from cyberattack, leaders point to a ‘wake-up call’ for U.S. energy infrastructure |Aaron Gregg, Sean Sullivan, Stephanie Hunt |May 13, 2021 |Washington Post 

I asked Savage to listen to “Sho Z-Pod Dupa” by DakhaBrakha to see where it fell on his universal scale. What Makes Music Universal - Issue 99: Universality |Kevin Berger |April 29, 2021 |Nautilus 

On this level, Mankiewicz’s film is a masterwork of subversion, a precursor to films that savaged the American love affair with normalcy—“The Graduate,” “Blue Velvet,” “American Beauty,” and “Fight Club” among them. ‘All About Eve’ at 70 |Tom Joudrey |September 25, 2020 |Washington Blade 

Stephanopoulos is a TV newsman, and Savage is a sex columnist. Politics Report: A Poll and a Court Ruling in Key Council Race |Scott Lewis |August 22, 2020 |Voice of San Diego 

Bolstered by the momentum of Savage, Masters continued to accumulate up-and-coming conservative talent. The Godfather of Right-Wing Radio |Caitlin Dickson |November 23, 2014 |DAILY BEAST 

After two years, the dispute ended with an arbitration ruling in favor of Savage. The Godfather of Right-Wing Radio |Caitlin Dickson |November 23, 2014 |DAILY BEAST 

In a 2009 profile of the right-wing firebrand, The New Yorker called Savage “a heretic among heretics.” The Godfather of Right-Wing Radio |Caitlin Dickson |November 23, 2014 |DAILY BEAST 

In the midst of the Michael Savage drama, the Talk Radio Network empire entered into another major lawsuit. The Godfather of Right-Wing Radio |Caitlin Dickson |November 23, 2014 |DAILY BEAST 

Savage noted that “HIV/AIDS forced us to start talking about what people are doing in bed.” The ‘Back Door’ Is Having Its Pop Culture Moment |Gabriella Paiella |September 27, 2014 |DAILY BEAST 

Under so many savage blows, the labouring mountains brought forth Turks. Gallipoli Diary, Volume I |Ian Hamilton 

It makes out of the savage raw material which is our basal mental stuff, a citizen. The Salvaging Of Civilisation |H. G. (Herbert George) Wells 

Yet a child coming under the humanising influences of culture soon gets far away from the level of the savage. Children's Ways |James Sully 

A primitive savage makes a bow and arrow in a day: it takes him a fortnight to make a bark canoe. The Unsolved Riddle of Social Justice |Stephen Leacock 

Savage troopers urged their horses into the water and slashed cowering women with their sabers. The Red Year |Louis Tracy