Antifa does exist as a loose-knit ideology with some adherents who have engaged in violence. Five falsehoods spurring Republican concern about the election |Philip Bump |October 15, 2020 |Washington Post 

It’s a little surprising to hear you say that Miller has a “firm ideology” because he built his reputation as a troll, and we don’t typically think of trolls as serious ideologues. Stephen Miller, explained |Sean Illing |October 7, 2020 |Vox 

I always wonder with these alt-right types if they began as trolls and picked up a coherent ideology later or if it’s the other way around. Stephen Miller, explained |Sean Illing |October 7, 2020 |Vox 

The ideologies of the party were less hard and fast 40 years ago. How Hatred Came To Dominate American Politics |Lee Drutman (drutman@newamerica.org) |October 5, 2020 |FiveThirtyEight 

Meanwhile, proposals have been raised year after year — on the basis of fiscal concerns, ideology or both — to limit the value of federal employment benefits or shift more of the cost to employees. Starting Thursday, most federal employees are eligible for paid parental leave |Eric Yoder |October 1, 2020 |Washington Post 

The man behind the desk is a fictional character—a ferocious patriot exposing the limits of rigid ideology. The End of Truthiness: Stephen Colbert’s Sublime Finale |Noel Murray |December 19, 2014 |DAILY BEAST 

His ideology is just so strong and so powerful that it clouds his vision for common sense and objectiveness. The Sydney Astrologer Turned Islamic Radical |Jacob Siegel |December 16, 2014 |DAILY BEAST 

ALEC attracted corporations that saw an opportunity to push an agenda, regardless of ideology. The Left’s Answer to ALEC |Ben Jacobs |December 15, 2014 |DAILY BEAST 

ALEC echoed the ideology of Charles Wilson, the first Defense Secretary in the Eisenhower administration. The Left’s Answer to ALEC |Ben Jacobs |December 15, 2014 |DAILY BEAST 

There were no obvious leaders; no single ideology or organization held sway over the crowd. Eric Garner Protests: ‘It’s Like Vietnam’ |Abby Haglage, Caitlin Dickson, Jacob Siegel, Chris Allbritton |December 5, 2014 |DAILY BEAST 

In such changes we see the collapse of "bourgeois ideology." The Practice and Theory of Bolshevism |Bertrand Russell 

And the ideology has been carried to the extreme of calling the whole world an idea, a phantasmagoria. The Positive Outcome of Philosophy |Joseph Dietzgen 

They borrowed his phraseology and his distinctive idioms without adopting his ideology. An Autobiography |Igor Stravinsky 

To Liebknecht all that Carlyle has said about heroes is contrary to ideology and inversion of the truth. The Future Belongs to the People |Karl Liebknecht 

Michael has told me that he very soon felt that there wasn't much ideology or thought, foundation. Warren Commission (2 of 26): Hearings Vol. II (of 15) |The President's Commission on the Assassination of President Kennedy