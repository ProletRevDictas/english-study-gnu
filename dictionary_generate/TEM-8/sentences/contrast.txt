By contrast, no song from SSA has ever hit the 300 million mark. The Biggest Challenge for Apple and Spotify in North Africa: YouTube |Eromo Egbejule |September 17, 2020 |Ozy 

Inhibited children, in contrast, avoided chances to make friends in new situations and to stand out academically or socially in school. ‘The Origins of You’ explores how kids develop into their adult selves |Bruce Bower |September 16, 2020 |Science News 

By contrast, the “deoptimized” coronavirus has several hundred genetic changes. Synthetic biologists have created a slow-growing version of the coronavirus to give as a vaccine |David Rotman |September 16, 2020 |MIT Technology Review 

In contrast, Biden did not travel Tuesday to South Florida, where there are signs he is struggling with the Cuban American community. Biden visits Florida as Democrats worry about his standing in the state |Sean Sullivan |September 15, 2020 |Washington Post 

By contrast, our death rate is roughly 58 per 100,000 Americans, more than five times Germany’s per capita toll. America Is About to Lose Its 200,000th Life to Coronavirus. How Many More Have to Die? |by Stephen Engelberg |September 14, 2020 |ProPublica 

“After the New York mentality, it is the ultimate contrast to see people making things by hand,” he said. The Photographer Who Gave Up Manhattan for Marrakech |Liza Foreman |January 6, 2015 |DAILY BEAST 

In contrast, Boehner's leadership team filed into his ceremonial office and greeted the teary newly-elected Speaker with hugs. Democrats Accidentally Save Boehner From Republican Coup |Ben Jacobs, Jackie Kucinich |January 6, 2015 |DAILY BEAST 

By contrast, John McCain, the eventual GOP nominee, had raised approximately $12.7 million in the first quarter of 2007 alone. Can Huckabee Convert the GOP’s Moneymen? |Lloyd Green |January 4, 2015 |DAILY BEAST 

In contrast to Paul, Huckabee has never palled around with Al Sharpton. Can Huckabee Convert the GOP’s Moneymen? |Lloyd Green |January 4, 2015 |DAILY BEAST 

By contrast, a gun will allow a pilot to attack hostile forces that are less than 300 feet from friendly ground forces. New U.S. Stealth Jet Can’t Fire Its Gun Until 2019 |Dave Majumdar |December 31, 2014 |DAILY BEAST 

But the contrast thus presented is one that has acquired a new meaning in the age in which we live. The Unsolved Riddle of Social Justice |Stephen Leacock 

In contrast to the Widal, it begins to fade about the end of the second week, and soon thereafter entirely disappears. A Manual of Clinical Diagnosis |James Campbell Todd 

This contrast implies a great wrong somewhere, and for which somebody must be responsible. Glances at Europe |Horace Greeley 

The contrast between the open street and the enclosed stuffiness of the dim and crowded interior was overwhelming. Hilda Lessways |Arnold Bennett 

His life had been the strangest contrast to the calm countenance which I saw so tranquilly listen to its own tale. Blackwood's Edinburgh Magazine, No. CCCXXXIX. January, 1844. Vol. LV. |Various