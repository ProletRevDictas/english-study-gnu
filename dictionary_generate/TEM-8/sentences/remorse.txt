At some point, Republicans need to ask themselves whether such conversations and tough words are enough — especially considering how little remorse Greene has demonstrated. The GOP’s Marjorie Taylor Greene problem is spinning out of control |Aaron Blake |January 27, 2021 |Washington Post 

Hutchison said he feels some remorse over receiving the vaccine ahead of a few of his firefighters who were not available that day. Trying not to waste a single drop of coronavirus vaccine |Lenny Bernstein, Lola Fadulu |January 7, 2021 |Washington Post 

Studies have also shown that when women choose to plead guilty or show remorse, they are more likely to see reduced charges and sentences, especially if their behavior contrasts with defiant male defendants. Why It’s Important To See Women As Capable … Of Terrible Atrocities |LGBTQ-Editor |November 21, 2020 |No Straight News 

I used to assume that drivers in bike crashes lacked remorse and were likely people who might have made jokes about “scoring points” for hitting someone on a bike. I Hit a Cyclist with My Car |Brooke Warren |October 28, 2020 |Outside Online 

In the first display of remorse either of them have made publicly over the fraud, Giannulli told the judge earlier Friday that he “deeply” regrets the harm that his actions have caused his daughters, wife and others. Lori Loughlin gets two months in prison after judge accepts plea deal in college bribery scandal |radmarya |August 21, 2020 |Fortune 

The time for remorse was when my husband was yelling to breathe! ‘I Can’t Breathe!’ ‘I Can’t Breathe!’ A Moral Indictment of Cop Culture |Michael Daly |December 4, 2014 |DAILY BEAST 

Because primal violence is justified by religious belief, “the offenders have no remorse, no fear, and are extremely confident.” Why ISIS Keeps Running Circles Around Us, Just Like Al Qaeda Did Before 9/11 |Christopher Dickey |September 11, 2014 |DAILY BEAST 

Sobriety brought a new, kinder, and gentler Womack, who often expressed remorse and regret over his past offenses. Bobby Womack’s Sexual Democracy: The Late Soul Legend Preached Mutual Pleasure |David Masciotra |June 29, 2014 |DAILY BEAST 

At the time of his extortion conviction, van der Sloot showed no remorse for the blackmail. Wedding Bells for Joran van der Sloot |Andrea Zarate, Barbie Latza Nadeau |June 13, 2014 |DAILY BEAST 

Had he any remorse, he would have implicated those who conspired with him to commit the greatest Ponzi scheme in history. Why Trust What Bernie Madoff Says About JP Morgan Now? |Burt Ross |February 21, 2014 |DAILY BEAST 

You have, year after year, without the slightest hesitation or remorse, sucked its life-blood from it. Blackwood's Edinburgh Magazine, No. CCCXXXIX. January, 1844. Vol. LV. |Various 

But among the conflicting sensations which assailed her, there was neither shame nor remorse. The Awakening and Selected Short Stories |Kate Chopin 

If the critic repents his evil deeds, it is because something has happened to awake his remorse. God and my Neighbour |Robert Blatchford 

He didn't know there was a grave, but something weighed him down with unspeakable remorse. The Cromptons |Mary J. Holmes 

He realised by Sir Hugh's manner that he regretted his recent action and was now overcome by remorse. The Doctor of Pimlico |William Le Queux