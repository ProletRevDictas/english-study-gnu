The challenge is that data like this isn’t always accurate as a result of how its been procured. ‘There’s a degree of assumption’: Subway tests addressable media plans using non-addressable data |Seb Joseph |February 5, 2021 |Digiday 

The original Palantir subscription in San Diego was procured by the Sheriff’s Department in 2013 for the San Diego Law Enforcement Coordination Center. Local Law Enforcement Quiet on Relationships With ‘Predictive Policing’ Company |Jesse Marx |February 2, 2021 |Voice of San Diego 

Despite months of advance notice that vaccines were coming, they failed to manage public expectations about how long it would take to procure the vials of vaccine to administer. Vaccine rollout gets ‘below an F’ grade |Robert McCartney |February 1, 2021 |Washington Post 

Don’t log the evidence you find and don’t use any items that can help you procure said evidence. Are you a ‘Phasmophobia’ pro? Here are some alternate rules to keep the scares fresh. |Elise Favis |January 11, 2021 |Washington Post 

Administered by the Defense Logistics Agency, the 1033 program is one of several ways law enforcement agencies procure military grade equipment. New Studies Say Demilitarizing Police Departments Does Not Increase Crime |Kirsten West Savali |December 11, 2020 |Essence.com 

They never procure them without exertion, and they never indulge in them without apprehension. Why Kids Are Making Us Crazy |James Poulos |March 30, 2014 |DAILY BEAST 

The name of the group, paradoxically, is Procure Saber, which in Portuguese means, Seek to Know. Brazil’s Rich Ban Biographies Via Arcane Law |Mac Margolis |November 21, 2013 |DAILY BEAST 

What about people who had attempted to farm, but been stymied by their inability to procure government loans? How a Discrimination Settlement Turned into a Bonanza for Fraudsters |Megan McArdle |April 26, 2013 |DAILY BEAST 

The narrator tries all manner of despicable tricks to procure her return. David's Book Club: The Fugitive |David Frum |January 13, 2013 |DAILY BEAST 

Bounderby was indeed born poor, but to loving parents, who sacrificed to procure him an education and a start in life. David's Bookclub: Hard Times |David Frum |September 24, 2012 |DAILY BEAST 

With every allusion that Ramona made to the saints' statues, Alessandro's desire to procure one for her deepened. Ramona |Helen Hunt Jackson 

Thereupon the generals hastened round the town to procure funds, and appeased the Visayos with a distribution of 1,800 pesos. The Philippine Islands |John Foreman 

His wife would have access to good society, and would enjoy every luxury that wealth could procure. Dope |Sax Rohmer 

Secondly, Randolph prayed for safe conducts for Bruce's envoys, presently to be sent to procure reconciliation with the Church. King Robert the Bruce |A. F. Murison 

She was even allowed, through her friends, to procure a piano-forte, which afforded her many hours of recreation. Madame Roland, Makers of History |John S. C. Abbott