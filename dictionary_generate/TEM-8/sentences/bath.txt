Facing muggy race conditions in Qatar of 88 degrees Fahrenheit with 75 percent humidity, the Canadian 50K racewalker spent ten minutes in an ice bath shortly before the race, then donned an ice towel while waiting for the start. How the World’s Best Athletes Handle Brutal Heat |Alex Hutchinson |March 10, 2021 |Outside Online 

There’s this stigma toward fathers that we shouldn’t be as loving or sing to our children or get them to bed or give them baths. These Mothers Wanted to Care for Their Kids and Keep Their Jobs. Now They're Suing After Being Fired |Eliana Dockterman |March 3, 2021 |Time 

How hot the bath—and the bathroom—should beThe meta analysis from 2019 zeroed in on 104 to 109 degrees Fahrenheit as a temperature range that improves sleep quality. To take the most relaxing bath ever, add some healthy tips |Rachel Feltman |January 3, 2021 |Popular-Science 

No matter what shape your skin is in, sealing in the moisture of a bath is always a good thing. To take the most relaxing bath ever, add some healthy tips |Rachel Feltman |January 3, 2021 |Popular-Science 

If you’re wondering how long to stay in the bath for, it’s really up to your personal preference. To take the most relaxing bath ever, add some healthy tips |Rachel Feltman |January 3, 2021 |Popular-Science 

Then I see all those couples quarreling in Bed, Bath, and Beyond. How to Make It Through Thanksgiving Alive |Lizzie Crocker |November 26, 2014 |DAILY BEAST 

You can not see anything in there, until it magically emerges in the developing bath. What Does Your Shrink’s Decor Say About You? |Oliver Jones |September 22, 2014 |DAILY BEAST 

It was now safely back in its cage, although the chancellor sometimes let it play in the bath, he said. Why 7 Times 8 Tripped Up the UK Chancellor |Tim Teeman |July 4, 2014 |DAILY BEAST 

Wikipedia has an entire entry on medicinal clays, though many are of the mud-bath variety. You Probably Shouldn’t Try to Lose 20 Pounds by Eating Clay |Kent Sepkowitz |June 24, 2014 |DAILY BEAST 

Just yesterday I claimed a local deli, a small poodle and a Bed, Bath and Beyond. Bill O’Reilly, Muslim-Hunter |Dean Obeidallah |June 5, 2014 |DAILY BEAST 

But some one, perhaps it was Robert, thought of a bath at that mystic hour and under that mystic moon. The Awakening and Selected Short Stories |Kate Chopin 

Then said Nqong from his bath in the salt-pan, "Come and ask me about it to-morrow, because I'm going to wash." Kipling Stories and Poems Every Child Should Know, Book II |Rudyard Kipling 

When partially exhausted the aluminum shutters are dipped into a bath of shellac. The Recent Revolution in Organ Building |George Laing Miller 

Enjoying the keen physical pleasure of it, he thought what a wholly delightful thing was a hot bath after a day's hard hunting. Uncanny Tales |Various 

With a desperate effort of the will he hurled himself out of the bath and threw open the window. Uncanny Tales |Various