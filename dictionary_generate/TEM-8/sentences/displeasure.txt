As long as they could endure the misery of 80-hour workweeks, along with the lash of their boss’s displeasure, the company must have exceeded their wildest expectations. Tesla’s wild ride, steered by Elon Musk |Jon Gertner |August 27, 2021 |Washington Post 

That began a multiyear cycle in which Noel, he claimed, was largely ignored by Paul and misled by other Klutch officials when he initially expressed his displeasure with the agency and mused about leaving. Knicks’ Nerlens Noel sues NBA agent Rich Paul, alleging ‘indifference’ cost him millions |Des Bieler |August 25, 2021 |Washington Post 

Many users started calling the new one the “Uncanny Valley Girl” to express their displeasure. The voices of women in tech are still being erased |Mar Hicks |August 3, 2021 |MIT Technology Review 

Privacy advocates have always criticized the model, and more and more regular people have become aware of the issue, some expressing their displeasure by downloading ad blockers. Google is totally changing how ads track people around the Internet. Here’s what you need to know. |Gerrit De Vynck |June 24, 2021 |Washington Post 

A group of more than 20 employees sent a letter to magazine management voicing their concerns about McCammond’s past statements and announced their displeasure in a statement published on Twitter on March 8. Alexi McCammond won’t take Teen Vogue job after criticism of old anti-Asian tweets |Jeremy Barr |March 18, 2021 |Washington Post 

Lawmakers were open about their displeasure with Pierson, who appeared aloof as she testified before them Tuesday morning. Why Secret Service Chief Julia Pierson Was Shown the Door |Tim Mak |October 2, 2014 |DAILY BEAST 

“We have to get past the initial experience of displeasure in order to recognize the longer-term benefits,” he says. 4 Science-Backed Ways to Motivate Yourself to Work Out |DailyBurn |September 13, 2014 |DAILY BEAST 

Earthquakes, plagues, and floods were clear evidence of divine displeasure. How the News Business Found Its Footing |Nick Romeo |June 22, 2014 |DAILY BEAST 

But beyond the rank and file, some important personalities have used Twitter to voice their displeasure. Syria’s Jihadist Twitter Wars |Bill Roggio |February 16, 2014 |DAILY BEAST 

Some disenchanted Americans gave vent to a racial displeasure over this incomprehensibly exotic Miss America. Miss America, Meet India’s ‘Dark’ Side |Tunku Varadarajan |September 17, 2013 |DAILY BEAST 

"It is a pity that father had not kept in his displeasure until after the busy time was over," she said, in her simplicity. The World Before Them |Susanna Moodie 

The porker grunted her displeasure, and Patrick did some grunting, too; but he was not easily scarednor would he be shaken off. The Girls of Central High on the Stage |Gertrude W. Morrison 

In after years his skill in handling legislatures was often remarked upon with displeasure. Scattergood Baines |Clarence Budington Kelland 

Pious men saw in this stroke, so sudden and so terrible, the plain signs of the divine displeasure. The History of England from the Accession of James II. |Thomas Babington Macaulay 

Mrs Mason's displeasure seemed a distant thing; his going away was the present distress. Ruth |Elizabeth Cleghorn Gaskell