Ricardou rented the small plot of land surrounding the house back to relatives of the cheesemaking family he purchased the place from so they could continue to graze their Abondance dairy cows there, as they’d done for centuries. When specialty cheesemaking becomes a quarantine pastime |By Kat Craddock/Saveur |December 1, 2020 |Popular-Science 

The water will allow his cattle to graze the land in a more intensive way. Here's What a Carbon Offset Actually Looks Like |Tim Neville |October 27, 2020 |Outside Online 

Paul Ensor, a fourth-generation rancher, recently spent more than $100,000 to fence off from grazing cattle a “wee stream” of the glacial river that runs along his acreage. Allbirds is stepping up for the planet—by treading lightly on it |sheilamarikar |September 21, 2020 |Fortune 

Casana directed aerial sweeps over grazing land at the cattle ranch, where ancient structures had likely suffered minimal damage. Drones find signs of a Native American ‘Great Settlement’ beneath a Kansas pasture |Bruce Bower |September 10, 2020 |Science News 

With more otters and thus fewer kelp-grazing urchins, kelp forests can thrive, storing carbon and sheltering salmon, ling cod and other fishes. Bringing sea otters back to the Pacific coast pays off, but not for everyone |Jonathan Lambert |June 11, 2020 |Science News 

Low-hanging clouds nestle among the layers of mountains; horses and cows graze in the middle distance. Will Coffee Rust Hurt Starbucks? |Nina Strochlic |June 8, 2014 |DAILY BEAST 

One of the officers suffered a minor graze wound to his head. L.A.'s Rogue Ex-Cop Posted Crazed Facebook Manifesto |Christine Pelisek |February 7, 2013 |DAILY BEAST 

Their regional rivals are the nomadic Missiriya tribe, who come down from the north into Abyei so their cattle can graze. Sudan's New Game-Changer |John Avlon |March 12, 2011 |DAILY BEAST 

Many of us had been hit by the balls, but a bruise or a graze of the skin was the worst consequence that had ensued. Blackwood's Edinburgh Magazine, No. CCCXXXIX. January, 1844. Vol. LV. |Various 

"I call you," the policeman said, and stripping the saddle and bridle from his sweaty horse, turned him loose to graze. Raw Gold |Bertrand W. Sinclair 

After lunch the girls strolled around a bit, leaving their mounts to graze lazily. The Outdoor Girls in the Saddle |Laura Lee Hope 

Then hit blows the snow off en around, en stock can graze thar until near Christmas. David Lannarck, Midget |George S. Harney 

Some of the animals suffered so with thirst that they could not graze, and uttered doleful whinneys of distress. Overland |John William De Forest