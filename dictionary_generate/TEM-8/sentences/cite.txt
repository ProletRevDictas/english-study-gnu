In particular, you cite deep tech, digital health and personalised healthcare. Does early-stage health tech need more ‘patient’ capital? |Steve O'Hear |September 17, 2020 |TechCrunch 

Even Germany, an oft-cited example of coronavirus response excellence in Europe, has slowly seen its daily case count edge up, with nearly 2,000 infections — a doubling from August 1. The new Covid-19 case surge in Europe, explained |Julia Belluz |September 17, 2020 |Vox 

On Thursday, Crown Publisher David Drake cited the scale of Obama’s ambition to write a book that captures the experiences of being president and offers an inspiring story for young people. The first volume of Barack Obama’s long-awaited memoir finally has a release date |Rachel King |September 17, 2020 |Fortune 

So the government had tried to delay the implementation of LGPD until May next year, citing reasons such as businesses not being able to prepare for the law during the pandemic. Podcast: COVID-19 is helping turn Brazil into a surveillance state |Anthony Green |September 16, 2020 |MIT Technology Review 

Privacy was the reason cited by Google but advertisers quickly called the motives into question and complained about the loss of transparency into a large amount of ad spend. How much does Google’s new search term filtering affect ad spend transparency? Here’s how to find out |Frederick Vallaeys |September 16, 2020 |Search Engine Land 

The advisor would cite reasonable-sounding sources like haltabuse.org and the FBI. Cover-Ups and Concern Trolls: Actually, It's About Ethics in Suicide Journalism |Arthur Chu |January 3, 2015 |DAILY BEAST 

They also used the powers of their separate agencies to cite waste haulers for spilling sludge along roadways. Two Texas Regulators Tried to Enforce the Rules. They Were Fired. |David Hasemyer, InsideClimate News |December 9, 2014 |DAILY BEAST 

The forums and message boards all cite “waking up to loose strands on your pillow” as a real indicator of significant hair loss. Birth Control Made My Hair Fall Out, and I’m Not the Only One |Molly Oswaks |October 14, 2014 |DAILY BEAST 

Still fewer cite “personal reasons,” “moving in with a partner,” and a “growing family.” From a Broken Lease, a Dream NYC Home |Lizzie Crocker |September 17, 2014 |DAILY BEAST 

People, alas, continue to cite it as if it had some validity in either fact or theory. Ron Rosenbaum on Hitler, Hollywood, and Quantifying Evil |William O’Connor |July 26, 2014 |DAILY BEAST 

"'Cite can do it as well as I; it is really her business," she explained to Edna, who apologized for interrupting her. The Awakening and Selected Short Stories |Kate Chopin 

We might cite the initials of many more of those who found themselves, not without some mutual surprise, side by side in one room. Camille (La Dame aux Camilias) |Alexandre Dumas, fils 

Instead of selecting general examples of ape actions, we may cite some of the doings of this intelligent creature. Man And His Ancestor |Charles Morris 

You cite the case of some who are admirable tea-party oracles, but who cannot utter half a dozen sentences in the tribune. The Petty Troubles of Married Life, Complete |Honore de Balzac 

I could cite a hundred examples of the astounding spirit that such men displayed. The Relief of Mafeking |Filson Young