In the last decade, breakthroughs in our understanding of the aging process and promising early results from age reversal experiments in animals have dragged longevity research out of the academic backwaters and firmly into the mainstream. How Long Can We Live? New Research Says the Human Lifespan Tops Out at 150 |Edd Gent |June 7, 2021 |Singularity Hub 

Until the mid-20th century, these regions were the nation’s economic backwaters, “colonial” producers of raw materials that lagged far behind the rest of the nation in wealth and income. Political power keeps shifting from the Rust Belt to the Sun Belt. Here’s why. |Keith Orejel |May 20, 2021 |Washington Post 

After years in the backwaters of venture capital, edtech had a booming 2020. Edtech stocks are getting hammered but VCs keep writing checks |Natasha Mascarenhas |May 14, 2021 |TechCrunch 

It’s fair to say that microfluidics became a scientific backwater. Are you ready to be a techno-optimist again? |Katie McLean |February 24, 2021 |MIT Technology Review 

The trip was billed as a celebration of the policy change that set in motion Shenzhen’s extraordinary transformation from rural backwater to global manufacturing colossus. Xi Jinping’s new economic strategy for China: ‘Dual circulation’ or doublespeak? |claychandler |October 15, 2020 |Fortune 

When you talk about midtown Manhattan as being a commercial backwater, I find it mind boggling. When New York City Hit Its Stride |Allen Barra |July 17, 2014 |DAILY BEAST 

They both think that Los Angeles, long maligned as a culinary backwater, is the best food city in America. Why Los Angeles Is the Best Food Town in America |Andrew Romano |November 16, 2013 |DAILY BEAST 

North Korea is an economic wreck and a technological backwater. What Can We Do About North Korea? |David Frum |April 5, 2013 |DAILY BEAST 

The president is likely headed to a bureaucratic backwater as his famed office is renovated. President Obama Eyes New Oval Office While the White House Undergoes Renovations |Lauren Ashburn |February 3, 2013 |DAILY BEAST 

In contrast, says Aftergood, “security has traditionally been a backwater that hires former military personnel and muscle men.” Intelligence Community Aims to Get Tough on Leaks |Eli Lake |June 21, 2012 |DAILY BEAST 

The rugged pioneer community had become, I suddenly saw, a rural backwater. The Idyl of Twin Fires |Walter Prichard Eaton 

Once swung out of that backwater they had been swept away, powerless to know where they went, to guess what was their destination. The Hidden Places |Bertrand W. Sinclair 

At last only fourteen of the English were left alive and they got hopelessly penned in a backwater. Round the Wonderful World |G. E. Mitton 

Why, you both look as you did that night the backwater of the South Fork came into our cabin. The Three Partners |Bret Harte 

The blue heron rose heavily from the backwater, and winged his slow flight high above the trees. Creatures of the Night |Alfred W. Rees