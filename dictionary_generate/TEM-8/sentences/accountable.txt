Our stories focus on holding the powerful accountable in service of the public. Are You Participating in a Vaccine Trial? Are You Running One? We’d Like to Hear About It. |by Caroline Chen, Ryan Gabrielson and Isaac Arnsdorf |September 17, 2020 |ProPublica 

That being said, if you have the potential for a trigger, and you’re not honest with yourself about there being doubt, and you end up being wrong — then you do run the risk of being held accountable to that. Don’t Blame The Refs For All Of These Replay Reviews |Jared Dubin |September 17, 2020 |FiveThirtyEight 

Thank you to the Reporters Committee for Freedom of the Press for joining us in this effort to hold the administration accountable and to get answers to these important questions. Washington Blade sues Trump administration |Staff reports |September 15, 2020 |Washington Blade 

He also argued that the Lausanne, Switzerland–based organization should be held accountable for misinforming hosts about the real risks. Want to Host the Olympics? Plan to Go Over Budget |Fiona Zublin |September 14, 2020 |Ozy 

A PBC has an obligation to state a public purpose beyond profit, to fulfill that purpose as part of the responsibilities of its directors, and to be accountable for so doing. 50 years later, Milton Friedman’s shareholder doctrine is dead |jakemeth |September 13, 2020 |Fortune 

Then we all have to do our part to engage the officers and our community, and hold everyone accountable in the process. How to Solve the Policing Crisis |Keli Goff |January 5, 2015 |DAILY BEAST 

When companies do bad things they ought to be held accountable for them. Why Do ‘Progressives’ Want to Ban Uber and AirBnB? |Adam Thierer, Christopher Koopman |December 30, 2014 |DAILY BEAST 

It is freedom, and accountable both legally and socially to the free choices of others around them. Red Tape Is Strangling Good Samaritans |Philip K. Howard |December 27, 2014 |DAILY BEAST 

The American people need to rise up and hold their elected officials accountable. When Will We See a #Millennial Congress? |Linda Killian |December 26, 2014 |DAILY BEAST 

I hope that America is going to hold Cuba accountable in public opinion. Cuban Hip-Hop Was Born in Alamar |Daniel Levin |December 26, 2014 |DAILY BEAST 

They did not feel accountable for their deeds or thoughts or words to an irresistible Power working for righteousness or truth. Beacon Lights of History, Volume I |John Lord 

They held us accountable for the conduct of those who had left, and vented the malignity of their unfeeling hearts upon us. A Narrative of the Shipwreck, Captivity and Sufferings of Horace Holden and Benj. H. Nute |Horace Holden 

By a train of events for which she is not accountable the question has become of importance to you. Tristram of Blent |Anthony Hope 

Sometimes we can hardly be held accountable for what we do; especially when our sense of justice is sorely taxed. Three Little Women |Gabrielle E. Jackson 

Commanding officers accountable for proper training of organizations; field efficiency; team-work. Manual of Military Training |James A. Moss