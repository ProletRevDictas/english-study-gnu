For me, it doesn’t get much more elegant than being able to get a total-body workout with one tool. The Gym-Free Pandemic Workout: Kettlebells, Indian Clubs, Sandbags, Oh My! |Eugene Robinson |August 25, 2020 |Ozy 

The new method skirts the traditional mathematical slog by directly computing “intersection numbers,” which some hope could eventually lead to a more elegant description of the subatomic world. The Mathematical Structure of Particle Collisions Comes Into View |Charlie Wood |August 20, 2020 |Quanta Magazine 

He had been strolling, too, when he had lost track of his surroundings and strayed into a neighborhood of elegant columned houses. The first murder |Katie McLean |August 19, 2020 |MIT Technology Review 

In recent decades it has taken on a more elegant guise, first with physical robots in production plants, and more recently with software automation entering most offices. The Global Work Crisis: Automation, the Case Against Jobs, and What to Do About It |Peter Xing |August 6, 2020 |Singularity Hub 

It will be populated by elegant, minimalist 3D-printed smart homes called Minka houses, created by noted geriatrician Bill Thomas. How Covid-19 will end “big box” senior living |Lila MacLellan |July 26, 2020 |Quartz 

I did a piece for Elle about the effort to remake her into an elegant presence fashion-wise. Daphne Merkin on Lena Dunham, Book Criticism, and Self-Examination |Mindy Farabee |December 26, 2014 |DAILY BEAST 

It was the last really elegant shop of it kind left in Manhattan. The Bookstore That Bewitched Mick Jagger, John Lennon, and Greta Garbo |Felice Picano |December 16, 2014 |DAILY BEAST 

Perhaps, like Hawking searching for his elegant equation, filmmakers will never find the answer. Why Can’t Movies Capture Genius? |Clive Irving |December 14, 2014 |DAILY BEAST 

They seem to have service for eight of these elegant blue-and-white plates. Alfred Hitchcock’s Fade to Black: The Great Director’s Final Days |David Freeman |December 13, 2014 |DAILY BEAST 

But many spirits experts have long lauded Japanese whiskies as formidable—even the most elegant—drams. Watch Out, Scotland! Japanese Whisky Is on the Rise |Kayleigh Kulp |November 16, 2014 |DAILY BEAST 

But it greatly equalizes and strengthens the fingers, and makes your execution smooth and elegant. Music-Study in Germany |Amy Fay 

Mrs. Pell was a very elegant and accomplished woman; her manners were the theme of universal admiration in our neighbourhood. The Posthumous Papers of the Pickwick Club, v. 2(of 2) |Charles Dickens 

Neither are there any terraces and verandahs adorned with elegant trellis-work and flowers, as there are in other warm countries. A Woman's Journey Round the World |Ida Pfeiffer 

He knew that this elegant city, resplendent and glorious in the sheen of the setting sun, would soon be a living hell. The Red Year |Louis Tracy 

The snuff-boxes of this period were very elegant and were decorated with elaborate paintings or set with gems. Tobacco; Its History, Varieties, Culture, Manufacture and Commerce |E. R. Billings.