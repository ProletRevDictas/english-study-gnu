As the philosopher Thomas Nagel noted, it must “be like” something to be a bat, but what that is we cannot even imagine—because we cannot imagine what it would be like to observe the world through a kind of sonar. What would it be like to be a conscious AI? We might never know. |Will Douglas Heaven |August 25, 2021 |MIT Technology Review 

It’s similar to how a bat uses sonar to “see” its surroundings. Rhinos, camels and bone-crushing dogs once roamed Nebraska |Alison Pearce Stevens |May 13, 2021 |Science News For Students 

That was the only way to explain the thermocline, a zone of rapidly decreasing temperature that separates warm surface waters from the frigid deep ocean, which affected naval sonar. A new book explores how military funding shaped the science of oceanography |Alka Tripathy-Lang |April 16, 2021 |Science News 

The scientific goals of the expeditions are always secondary, although splurging for the sonar mapping system turns out to be key in verifying their world-record-holding status. A journey to the bottom of the oceans — all five of them |Lucinda Robb |December 18, 2020 |Washington Post 

Uncrewed ships equipped with sonar spot suspicious objects from different angles, and an extended telescopic arm wields a nail gun to attach an explosive charge. The 100 greatest innovations of 2020 |Popular Science Staff |December 2, 2020 |Popular-Science 

It had advanced sonar—aimed not at the ocean floor but at other subs and shipping—and drew a blank. If They Are Ever Found, Flight 370’s Black Boxes Should Go to the U.S. |Clive Irving |April 8, 2014 |DAILY BEAST 

They are loaded with state-of-the art sonar and radar equipment. Was MH370 Carrying Killer Cargo? |Clive Irving |March 21, 2014 |DAILY BEAST 

Or how about powerful sonar blasts the U.S. Navy is using in the waters off southern California? Fishy Mystery: Are Beached Oarfish Trying to Tell Us Something? |Kevin Bailey |October 23, 2013 |DAILY BEAST 

Before that, in December 2011, it won a $691 million U.S. Navy subcontract for “combat and sonar systems” for submarines. Assad Supplier Finmeccanica Did Business With the Pentagon |Aram Roston |July 9, 2012 |DAILY BEAST 

He was talking about the sonar beacon linked to the black box of Air France Flight 447. The Myth of the Black Box |Clive Irving |June 7, 2009 |DAILY BEAST 

The tyme of hir purificatioun was sonar then the Leviticall law appointes. The Works of John Knox, Vol. 1 (of 6) |John Knox 

The sonar equipment showed what kind of rock it was—iron and basalt. The Minus Woman |Russell Robert Winterbotham 

Diciendo y haciendo, el pcaro sac del bolsillo cuatro piezas de plata y las hizo sonar. A First Spanish Reader |Erwin W. Roessler and Alfred Remy 

Thus the sonar waves would appear to be striking no obstacle—and no echo would return to the sonarscopes on the search craft! Tom Swift and the Electronic Hydrolung |Victor Appleton 

An invisible sub—one that sonar pulses would seem to pass right through, as if nothing were there! Tom Swift and the Electronic Hydrolung |Victor Appleton