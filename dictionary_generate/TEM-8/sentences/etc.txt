Will you work with Jenny Slate, John Daly, John Mulaney, etc. moving forward? The Zany Shades of Nick Kroll |Abby Haglage |December 15, 2014 |DAILY BEAST 

Others on the RSD forum would deconstruct his approach: “too aggressive,” “too many questions,” “nervousness in your voice,” etc. School Shooters Love This Pickup Artist Website |Brandy Zadrozny |December 5, 2014 |DAILY BEAST 

“Sanctuary for all, community for all, etc, etc.” So he was on the road to community, you know? ‘Walking Dead’ Showrunner Scott Gimple Teases ‘Darker, Weirder’ Times Ahead |Melissa Leon |December 2, 2014 |DAILY BEAST 

Every other kind of influence-buying, such as spending to achieve influence, gratitude, access, etc., is just fine. Undo Citizens United? We’d Only Scratch the Surface |Jedediah Purdy |November 12, 2014 |DAILY BEAST 

But from there we had Watergate, stagflation, oil embargos, eroding American power in the world, growing income inequality, etc. We've Been on the Wrong Track Since 1972 |Dean Obeidallah |November 7, 2014 |DAILY BEAST 

Serrano closes by answering certain questions about prebends, curacies, etc. The Philippine Islands, 1493-1898, Volume XX, 1621-1624 |Various 

Other orchestra leaders are always writing and begging him to lend them his copies of Oratorios, etc. Music-Study in Germany |Amy Fay 

It may be a forerunner or successor, the cause or consequence, or a contemporaneous fact, etc. Assimilative Memory |Marcus Dwight Larrowe (AKA Prof. A. Loisette) 

(a) Poor nutrition, which usually accompanies unsanitary conditions, poor and insufficient food, etc. A Manual of Clinical Diagnosis |James Campbell Todd 

Another week was spent fighting over running powers, facilities, etc., and I was in the witness box again. Fifty Years of Railway Life in England, Scotland and Ireland |Joseph Tatlow