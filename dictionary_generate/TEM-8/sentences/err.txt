Walthour did say Rodriguez had erred in his decision when he claimed Walthour had submitted a formal affidavit concerning his wife’s health. Ruling on Murder Case by Judge Suffering From Dementia Will Stand, Court Says |by Joe Sexton |March 5, 2021 |ProPublica 

Above all, he valued his freedom — the freedom to explore, the freedom to err, the freedom to create. Isadore Singer Transcended Mathematical Boundaries |Daniel S. Freed |March 4, 2021 |Quanta Magazine 

We’re going to err on the side of caution, and we’re going to bring very few people back to the office at a time. Publishers look to allay employee fears over a return to the office by offering additional flexibility |Jessica Davies |February 10, 2021 |Digiday 

So, with transmission rates still high in many parts of the world, including the United States, and large swaths of the population still unvaccinated, it’s best to err on the side of caution. What do COVID-19 vaccines mean for daily life in the months ahead? |Erin Garcia de Jesus |January 29, 2021 |Science News 

The appeal filed Monday said Marchant had erred in several ways, including by accepting actions the General Assembly took last summer. Residents ask high court to block removal of Richmond’s Robert E. Lee statue as Virginia prepares to act |Gregory S. Schneider |January 26, 2021 |Washington Post 

No one likes it when their sandcastle is knocked over, but his reaction is a bit, err, extreme. Was Baby Jesus A Holy Terror? |Candida Moss |December 21, 2014 |DAILY BEAST 

They tend to err heavily on the side of the government where kidnapping is concerned. Mexican Journalists Speak Out on Reporter Murders |Jason McGahan |June 17, 2014 |DAILY BEAST 

The bottom line is agencies almost always err on the side of caution. Should Woody Allen Have Been Allowed to Adopt? |Keli Goff |February 7, 2014 |DAILY BEAST 

In other words, the vast majority of Americans seem to agree with the American Mullahs, err, “hard-liners.” The Myth of the American Mullah |James Kirchick |December 14, 2013 |DAILY BEAST 

Err, yes, of course Newt Gingrich loved the number of debates. Thanks, Newt, But We Should Have Fewer Debates |Justin Green |April 4, 2013 |DAILY BEAST 

And they that call this people blessed, shall cause them to err: and they that are called blessed, shall be thrown down, headlong. The Bible, Douay-Rheims Version |Various 

Why hast thou made us to err, O Lord, from thy ways: why hast thou hardened our heart, that we should not fear thee? The Bible, Douay-Rheims Version |Various 

Whatever his own personal blunders, it was impossible for Joseph Addison to err in a point of literary judgment. A Cursory History of Swearing |Julian Sharman 

Is not your style so simple, frank, and direct that a wayfaring girl can read it and not err therein? Penelope's Experiences in Scotland |Kate Douglas Wiggin 

It is under the abiding influence of the Holy Ghost, and therefore cannot err in matters of faith. My Religion |Leo Tolstoy