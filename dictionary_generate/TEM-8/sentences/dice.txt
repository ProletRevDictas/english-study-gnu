There are various ways you can slice and dice the data, most obviously by considering the effects of age and sex. The Data Behind a Once-a-Week Strength Routine |Alex Hutchinson |February 2, 2021 |Outside Online 

Add the remaining oil to the empty pot, followed by the onions and the diced vegetables and cook, stirring occasionally, softened slightly, about 4 minutes. Hearty winter vegetables are at the root of this red wine chicken stew |Ellie Krieger |January 21, 2021 |Washington Post 

Turn up the heat to medium-high, and toss in the diced pineapple, cooking for two more minutes until the pineapple is heated through and slightly soft. How to Cook with Plant-Based Beef |Christine Byrne |January 16, 2021 |Outside Online 

Unfortunately, finding a clique of 128 dice is a particularly thorny problem. Computer Search Settles 90-Year-Old Math Problem |Kevin Hartnett |August 19, 2020 |Quanta Magazine 

At that point, you can rule out all the configurations of 128 dice that involve that unworkable starting configuration of 12 tiles. Computer Search Settles 90-Year-Old Math Problem |Kevin Hartnett |August 19, 2020 |Quanta Magazine 

So why are so many wealthy Florida business owners lining up to roll the dice? Ganjapreneurs Line Up to Get Florida High |Abby Haglage |July 30, 2014 |DAILY BEAST 

Instead, Obama, like Jon Stewart, uses jokes that slice and dice his targets by name. The Daily Show President: Obama's Edgy Humor |Dean Obeidallah |July 5, 2014 |DAILY BEAST 

Healthy individuals had lucked out while their sick relatives simple ended up with a bad genetic roll of the dice. The Genetic Heroes That Could Cure the Sick |Carrie Arnold |July 1, 2014 |DAILY BEAST 

We learning how to dice an onion into perfect, tiny cubes (the secret is not to remove the root when you peel the skin). Thai Curry Therapy At London’s L’Atelier Des Chefs |Emma Woolf |February 3, 2014 |DAILY BEAST 

Dice had just finished a stand-up show in Westbury, and his manager phoned to tell him Woody Allen wanted a meeting. Andrew Dice Clay on ‘Blue Jasmine,’ His Alleged Misogyny, and More |Marlow Stern |July 25, 2013 |DAILY BEAST 

Because they used a heavier ball, roulette looked about the same as on Earth, and the same went for the dice games. Fee of the Frontier |Horace Brown Fyfe 

Anyway, the dice were sure loaded against a certain party he knew. Hooded Detective, Volume III No. 2, January, 1942 |Various 

By a strange mania this prince spends his time sitting before a table, on which are placed six dice and a dice-box. Superstition In All Ages (1732) |Jean Meslier 

True the Prophet—on whom be peace—forbids dice; but Allah will be compassionate, and I have some about me. God Wills It! |William Stearns Davis 

And Mary, opening her eyes, now saw Zubair and the chief standing by the rock, and shaking the dice in the hollows of their hands. God Wills It! |William Stearns Davis