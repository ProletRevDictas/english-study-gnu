The thing that I believe I got the advantage is the teammates because he left the team, and all the teammates that helped me get prepared for him know him very well. As Kamaru Usman edges toward UFC greatness, his former teammate wants to stop him |Glynn A. Hill |February 12, 2021 |Washington Post 

We listened to people, and there are a lot of people who tried to stand up for what they believed in and weren’t really heard. The NBA’s week of controversies show how hard life is outside of the bubble |Ben Golliver |February 12, 2021 |Washington Post 

Those who want to wrap themselves in the flag and believe the song conveys what it means to be an American — “O’er the land of the free and the home of the brave” — can absolutely believe that. The pregame national anthem — in all its roiling contradictions — still has something to offer |Barry Svrluga |February 11, 2021 |Washington Post 

Those squads are often hailed as reason to believe there is another way, but you’re talking about four exceptional teams over three decades. The NFL’s top QBs are waking up to their power, following Tom Brady and LeBron James |Jerry Brewer |February 11, 2021 |Washington Post 

To know that I would be waiting for 450 days or something like that, I wouldn’t believe it one bit, but here we are. For this college football team, covid means the season starts in February — with Senior Day |Glynn A. Hill |February 11, 2021 |Washington Post 

People watch night soaps because the genre allows them to believe in a world where people just react off their baser instincts. ‘Empire’ Review: Hip-Hop Musical Chairs with an Insane Soap Opera Twist |Judnick Mayard |January 8, 2015 |DAILY BEAST 

The death toll, which experts believe has been significantly undercut by secret burials, stands at 7,905. The Race for the Ebola Vaccine |Abby Haglage |January 7, 2015 |DAILY BEAST 

Three quarters of those people believe the end of the world is nigh. The Evangelical Apocalypse Is All Your Fault |Jay Michaelson |January 4, 2015 |DAILY BEAST 

I believe in the power of institutions—Congress, public policy, certain ideas about politics—that last for a long time. Thank Congress, Not LBJ for Great Society |Julian Zelizer, Scott Porch |January 4, 2015 |DAILY BEAST 

What they believe impacts economic policy, foreign policy, education policy, environmental policy, you name it. The Evangelical Apocalypse Is All Your Fault |Jay Michaelson |January 4, 2015 |DAILY BEAST 

He did believe you, more or less, and what you said fell in with his own impressions—strange impressions that they were, poor man! Confidence |Henry James 

I believe that these are ideal characters constructed from still more ancient legends and traditions. God and my Neighbour |Robert Blatchford 

"I verily believe they're gone to look at my button," cried Davy, beginning to laugh, in spite of his fears. Davy and The Goblin |Charles E. Carryl 

I cannot believe that God would think it necessary to come on earth as a man, and die on the Cross. God and my Neighbour |Robert Blatchford 

I cannot believe that a good God would create or tolerate a Devil, nor that he would allow the Devil to tempt man. God and my Neighbour |Robert Blatchford