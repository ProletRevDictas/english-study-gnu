The concertina wire marking the edge of his homeland triggers Vanishvili’s border cells to fire every time he sees it, and in turn, that firing may trigger the despair associated with that place. How border walls trick the human brain and psyche |Jessica Wapner |October 2, 2020 |Popular-Science 

In the first movement of his Fifth Symphony, Beethoven set up a battle between hope and despair. Beethoven’s 5th Symphony is a lesson in finding hope in adversity |Charlie Harding |September 11, 2020 |Vox 

It’s my job, I’ve begun to think, to make sure that people in this “climate generation” don’t get swallowed up in an ocean of despair. Generation Z is ‘traumatized’ by climate change—and they’re the key to fighting it |matthewheimer |August 19, 2020 |Fortune 

This discrepancy can create a feeling of despair when it comes to online reputation management, but this only means that you need to become more proactive about getting reviews from your customers. Online reputation management: Seven steps to success |Aleh Barysevich |June 3, 2020 |Search Engine Watch 

America enters the final months of the 2020 campaign in a state of despair and disrepair. As Minneapolis Burns, Trump’s Presidency Is Sinking Deeper Into Crisis. And Yet, He May Still Be Re-Elected |LGBTQ-Editor |June 2, 2020 |No Straight News 

“Light trumps darkness, hope beats despair, grace wins over sin, love defeats hate, life conquers death,” the cardinal said. 'Please Don't Die!': The Frantic Battle to Save Murdered Cops |Michael Daly |December 22, 2014 |DAILY BEAST 

Three months of despair were ignited in suburban Missouri when officer Darren Wilson was told he would walk free. Raging Protesters Set Ferguson on Fire |Justin Glawe |November 25, 2014 |DAILY BEAST 

It may be nothing other than anger and despair, at this point. Ferguson Protesters Harass Black Police, Call for Darren Wilson’s Death |Justin Glawe |November 21, 2014 |DAILY BEAST 

I highly doubt that anyone not already in a state of despair would look to war as an antidote to Godlessness. There Are Only Atheists in Fox Holes |Michael Carson |October 5, 2014 |DAILY BEAST 

Future urbanization does not need to pose a choice between rural hopelessness and urban despair. Welcome to the Billion-Man Slum |Joel Kotkin |August 25, 2014 |DAILY BEAST 

Davy read this over two or three times, in the greatest perplexity, and then gave it up in despair. Davy and The Goblin |Charles E. Carryl 

Prud'hon, in humiliation and despair, lived in a solitude almost complete. Women in the fine arts, from the Seventh Century B.C. to the Twentieth Century A.D. |Clara Erskine Clement 

The falling dew, and the howling wind raised him not from that bed of lonely despair. The Pastor's Fire-side Vol. 3 of 4 |Jane Porter 

But the '34 port was so good that he revoked twice, to the indignation and despair of his unhappy brother and partner. The Pit Town Coronet, Volume I (of 3) |Charles James Wills 

As they got lower and lower down the hill, her wretchedness and disquiet became acute, to the point of a wild despair. Hilda Lessways |Arnold Bennett