To give back and support their community—Hushin’s third pillar—the team holds a raffle each year and then uses the money to support a cause they believe in. Meet the YouTubers Who Are Rebranding Hunting |kklein |August 25, 2021 |Outside Online 

Maryland is offering state employees $100, while Lancaster, California, is trying to encourage teens to get inoculated by entering their names in a raffle for college scholarships worth up to $10,000. The incredible incentives being offered to Americans to get vaccinated |Isabelle Brocas |May 25, 2021 |Quartz 

People who get vaccinated can enter their names in a raffle, with the victor getting to choose from a range of rides that includes a Chevrolet Camaro and a Nissan Altima. Beer, bouquets and free rounds at a gun range: How local governments promote vaccines |Reis Thebault, Paulina Firozi |May 6, 2021 |Washington Post 

Inspiration 4’s other two travelers will be selected through a raffle and an entrepreneurial contest. This is how (almost) anyone can train to be an astronaut |Neel Patel |March 24, 2021 |MIT Technology Review 

People who signed up for the raffle had to attest to being less than six and a half feet tall and under 250 pounds. This is how (almost) anyone can train to be an astronaut |Neel Patel |March 24, 2021 |MIT Technology Review 

The winners will be drawn on January 8, which makes the raffle tickets the perfect Christmas gift. Pope Francis Raffles Off His Swag to Help the Poor |Barbie Latza Nadeau |November 18, 2014 |DAILY BEAST 

Engineers now field questions on Reddit and laboratories raffle off multiday tours to Twitter followers. NASA’s ‘Curiosity’: To Twitter and Beyond |Josh Dzieza |August 28, 2012 |DAILY BEAST 

And when I took a ticket for a raffle, I hardly counted upon winning this particularly gaudy sofa-cushion. Punch, or the London Charivari, Vol. 93, September 24, 1887 |Various 

When I came to the library, the master of the raffle told me it was against all rule to refund a subscription.' Camilla |Fanny Burney 

Ultimately Christian laid down his shilling, the raffle began, and the dice went round. Return of the Native |Thomas Hardy 

These are the class of gambling practices of which the church bazaar or raffle may be taken as the type. The Theory of the Leisure Class |Thorstein Veblen 

Raffle, raf′l, n. a kind of sale by chance or lottery in which the price is subscribed equally by all who hope to win. Chambers's Twentieth Century Dictionary (part 3 of 4: N-R) |Various