Household refuse was thrown into gutters, and human waste ended up in waterways and local cesspits. Did Cars Rescue Our Cities From Horses? - Issue 108: Change |Brandon Keim |November 10, 2021 |Nautilus 

Archaeogeneticist Susanna Sabin and her colleagues found DNA from human gut-dwelling microbes in samples from a 600-year-old household cesspit in Jerusalem and a 700-year-old public toilet in Riga, Latvia. Archaeologists delved into medieval cesspits to study old gut microbiomes |Kiona N. Smith |October 6, 2020 |Ars Technica 

Various substitutes have been tried for the cesspit, which retain the principle of the hand removal of excreta. Scientific American Supplement, No. 421, January 26, 1884 |Various 

There should be a ventilated trap placed on the pipe leading from the watercloset to the cesspit. Scientific American Supplement, No. 421, January 26, 1884 |Various 

Where a cesspit is unavoidable, perhaps the best and least offensive system for emptying it is the pneumatic system. Scientific American Supplement, No. 421, January 26, 1884 |Various