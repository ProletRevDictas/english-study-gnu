It’s got this increasingly authoritarian tendency, which is consolidating data and increasing surveillance, but it still has a strong civil society and a court system that’s pushing back. Podcast: COVID-19 is helping turn Brazil into a surveillance state |Anthony Green |September 16, 2020 |MIT Technology Review 

That accusation carries all the more weight under Hong Kong’s newly authoritarian reality, where provoking by unlawful means “hatred” toward the government can amount to a crime of collusion with external elements, punishable by lengthy prison terms. Beijing’s push for mass Covid testing in Hong Kong commits the cardinal sin of public health |Mary Hui |August 25, 2020 |Quartz 

Recently, though, the country has veered down a more authoritarian path. Brazil is sliding into techno-authoritarianism |Tate Ryan-Mosley |August 19, 2020 |MIT Technology Review 

To do that, they must be clear about what their values are and why they differ from those of authoritarian regimes. How China surveils the world |Tate Ryan-Mosley |August 19, 2020 |MIT Technology Review 

An LGBTQ activist in Belarus on Monday said she remains optimistic that nationwide protests will ultimately oust her country’s authoritarian president. Belarus LGBTQ activist joins anti-government protests |Michael K. Lavers |August 18, 2020 |Washington Blade 

Sony Pictures Entertainment has put out some of the most countercultural, anti-authoritarian movies of the past century. Sony: Hollywood’s Most Subversive Studio Under Attack |Marlow Stern |December 23, 2014 |DAILY BEAST 

And yes, that authoritarian, patriarchal guru-power structure, invariably, always leads to abuses. Is India’s Fallen ‘God-Man’ So Different From a Megachurch Pastor? |Jay Michaelson |November 21, 2014 |DAILY BEAST 

Perhaps that name should not be felt so ruefully today, despite the reversion to authoritarian control in Egypt. You Say You Want to Name a Revolution? |Jack DuVall |October 5, 2014 |DAILY BEAST 

Rivers continued on her political, authoritarian monologue by describing what kind of tyrant she would be. What Joan Rivers Said She Would Do If She Were Dictator of America |Asawin Suebsaeng |September 5, 2014 |DAILY BEAST 

The crisis in neighbouring Ukraine has rattled Alexander Lukashenko's authoritarian regime. The Daily Beast’s Best Longreads, June 14, 2014 |The Daily Beast |June 14, 2014 |DAILY BEAST 

Borders are no defense for the penetration of information even in highly controlled or authoritarian societies. Shock and Awe |Harlan K. Ullman 

We are aware of the strength that lies in narrowness and secretly covet the simplification and order of an authoritarian church. Nine O'Clock Talks |Frederic B. Kellogg 

The earlier Liberalism had to deal with authoritarian government in church and state. Liberalism |L. T. Hobhouse 

Soldiers occasionally use the authoritarian style to demand instant obedience. Sequential Problem Solving |Fredric Lozo 

The authoritarian model is useful for situations requiring immediate compliance by a subordinate. Sequential Problem Solving |Fredric Lozo