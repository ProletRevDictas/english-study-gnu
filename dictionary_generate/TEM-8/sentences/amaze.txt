They still had the $81 million that has to amaze everybody on all sides of the issue. The Luxury Homes That Torture and Your Tax Dollars Built |Michael Daly |December 12, 2014 |DAILY BEAST 

Daniel Radcliffe may not be Harry Potter anymore but he continues to amaze us with magic. Harry Potter Raps, The Catcalls Heard ‘Round the World and More Viral Videos |Alex Chancey |November 2, 2014 |DAILY BEAST 

Does the number of stars that came out of Angels in the Outfield amaze you? Joseph Gordon-Levitt on ‘Sin City’ and Why He Considers Himself a Male Feminist |Marlow Stern |August 14, 2014 |DAILY BEAST 

“These words astonish me as much as they amaze you,” he said. The Iranian Islamic Fundamentalist’s Handbook on Sex in the West |IranWire |July 25, 2014 |DAILY BEAST 

There's always pressure, whether it's a new film or whether it's a sequel, to entertain and amaze an audience. James Cameron on How to Find Flight MH370, Climate Change, Leonardo DiCaprio, and More |Marlow Stern |April 12, 2014 |DAILY BEAST 

His powerful logic would surprise,Amaze, and much delight: He proved that dimness of the eyesWas hurtful to the sight. The Book of Humorous Verse |Various 

Three strides took Andrews to the spot, and there he halted in amaze with a little exclamation of astonishment. The Tigress |Anne Warner 

"Madam, I am not disputing your word," cried poor Jack, in amaze at her angry vehemence. A Dreadful Temptation |Mrs. Alex. McVeigh Miller 

They did not know they were glimpsing the first outcroppings of a genius that would one day amaze and entertain the nations. Mark Twain, A Biography, 1835-1910, Complete |Albert Bigelow Paine 

She hid them quickly in the folds of her cloak, and all the while the expression of amaze and fear on her face did not abate. High Noon |Anonymous