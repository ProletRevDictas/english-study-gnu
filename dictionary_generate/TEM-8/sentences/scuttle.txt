By one count, 20 industry lobbyists were in the halls trying to scuttle SB 962 as it came to a vote nine days later. Murdered for Her iPhone |Michael Daly |May 8, 2014 |DAILY BEAST 

An ostentatious display of Japanese military might could scuttle those negotiations. Japan Prepares to Shoot North Korean Missiles Out of the Sky |Angela Erika Kubo, Jake Adelstein |April 10, 2014 |DAILY BEAST 

Bryk came in third, but he notes with pride that he bested the coal scuttle helmet man in his home county. Brooklyn’s Lazy Carpetbagger Sets His Sights on an Alaska Senate Seat |David Freedlander |December 4, 2013 |DAILY BEAST 

Any disagreement we had with them was criticized as an attempt to scuttle the building of the memorial. The Eisenhower Family Objects to the Eisenhower Monument |David Frum |March 21, 2013 |DAILY BEAST 

Marco Rubio, on the other hand, led the GOP effort to scuttle the thing on abortion-related grounds. Your "Changing" Republican Party |Michael Tomasky |December 4, 2012 |DAILY BEAST 

And Robinson heard him scuttle about and hastily convene small boys and dispatch them down the road to look at an honest man. It Is Never Too Late to Mend |Charles Reade 

In a minute or two, a black head was seen to rise slowly and fearfully out of the fore-scuttle, then it disappeared. Newton Forster |Captain Frederick Marryat 

The water then flowed in from the top through the deck scuttle forward of the collision bulkhead. Loss of the Steamship 'Titanic' |British Government 

It was by this scuttle that access was obtained to all the decks below C down to the peak tank top on the orlop deck. Loss of the Steamship 'Titanic' |British Government 

The fore-scuttle hatch was closed to keep everything dark before the bridge. Loss of the Steamship 'Titanic' |British Government