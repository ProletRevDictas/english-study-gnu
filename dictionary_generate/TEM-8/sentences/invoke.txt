He invoked his trustworthiness as a pastor, narrated facts about the vaccine over the nursing home’s public announcement system and reminded staff that nothing had happened to him or the others who had been vaccinated. Most nursing home workers don’t want the vaccine. Here’s what facilities are doing about it. |Rachel Chason, Rebecca Tan, Jenna Portnoy, Erin Cox |January 27, 2021 |Washington Post 

Yet our country has failed to invoke the Defense Production Act to produce enough masks for health-care workers and other essential workers. Everyone should be wearing N95 masks now |Joseph G. Allen |January 26, 2021 |Washington Post 

O’Connell is fond of invoking instincts in explaining how rituals unfold. In the animal kingdom, rituals that connect, renew and heal |Barbara King |January 22, 2021 |Washington Post 

If the case were moved to arbitration, the company could invoke immunity there as well, experts say. The Nursing Home Didn’t Send Her to the Hospital, and She Died |by Sean Campbell |January 8, 2021 |ProPublica 

In his victory speech, Warnock, 51, invoked Georgia’s history of racial oppression and voter suppression by hailing his vote from his mother. Ebenezer Baptist: MLK’s church makes new history with Warnock victory |DeNeen L. Brown |January 6, 2021 |Washington Post 

Before you invoke images of a nation enjoying more indolence than industry, there is an uncomfortable statistic to digest. Obama’s Extravagant Summer Break? More Like, America’s Vacation-Deficit Disorder |Clive Irving |August 10, 2014 |DAILY BEAST 

Alas, I must invoke Marx, because it is a question of limited resources. Reading Prison Novels In Prison |Daniel Genis |May 24, 2014 |DAILY BEAST 

Do I, a law professor, get to invoke the privilege when I write a piece for The Daily Beast? Democracy Demands a Journalist-Source Shield Law |Geoffrey R. Stone |April 15, 2014 |DAILY BEAST 

Does a personal blogger writing on Facebook get to invoke the privilege? Democracy Demands a Journalist-Source Shield Law |Geoffrey R. Stone |April 15, 2014 |DAILY BEAST 

Perhaps; but why do these films invoke Greek history at all, if they aim only at visual fantasy? ‘300’ Is a Misleading, Muscle-Bound Travesty of Ancient History |James Romm |March 13, 2014 |DAILY BEAST 

I round the threshold wandering here,Vainly the tempest and the rain invoke,That they may keep my lady prisoner. The Poems of Giacomo Leopardi |Giacomo Leopardi 

How far off and faint seem the years of that dead crime my brother would invoke for the punishment of this sweet soul! The Circular Study |Anna Katharine Green 

Dan waited for him to invoke deity with the asthmatic wheeziness to which mirth reduced his vocal apparatus. A Hoosier Chronicle |Meredith Nicholson 

Both read the same Bible, and pray to the same God; and each invoke his aid against the other. Key-Notes of American Liberty |Various 

But what liberty can he invoke—he who has disavowed and injured all liberties? The Arena |Various