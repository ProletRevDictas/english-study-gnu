A number of ad hoc initiatives currently do this work, but it’s a patchwork and insufficient system. Pay Restaurants to Feed Those in Need |Amanda Kludt |November 23, 2020 |Eater 

It adds process, and checks and balances, to what is currently an ad hoc authority. Representatives propose bill limiting presidential internet ‘kill switch’ |Devin Coldewey |October 22, 2020 |TechCrunch 

Williams’ case is a signal to stop the ad hoc adoption of facial recognition before an injustice occurs that cannot be undone. The Bias in the Machine - Issue 89: The Dark Side |Sidney Perkowitz |August 19, 2020 |Nautilus 

When a report of abuse comes in, an ad hoc team of up to 10 NSO employees is assembled to investigate. Inside NSO, Israel’s billion-dollar spyware giant |Tate Ryan-Mosley |August 19, 2020 |MIT Technology Review 

Technology has offered a ready solution for some types of ad hoc conversations during the pandemic. Remote workers want to re-create those watercooler moments, virtually |Tanya Basu |August 17, 2020 |MIT Technology Review 

A lot of the culture around movies in the sci-fi/fantasy genre is about deconstructing them ad nauseam. Patton Oswalt on Fighting Conservatives With Satire |William O’Connor |January 6, 2015 |DAILY BEAST 

Yes, publicizing tragedy gets clicks, gets ad revenue, gets notoriety, and can be done for all the wrong reasons. Cover-Ups and Concern Trolls: Actually, It's About Ethics in Suicide Journalism |Arthur Chu |January 3, 2015 |DAILY BEAST 

When it came to Android, however, it was ad-supported but free. Lost For Thousands of Strokes: 'Desert Golfing' Is 'Angry Birds' as Modern Art |Alec Kubas-Meyer |January 2, 2015 |DAILY BEAST 

Late former governors of NY, TX starred in a 1994 snack chip ad. Mario Cuomo, Ann Richards Concede to Doritos |The Daily Beast Video |January 2, 2015 |DAILY BEAST 

But an ad-supported version of Desert Golfing was impossible. Lost For Thousands of Strokes: 'Desert Golfing' Is 'Angry Birds' as Modern Art |Alec Kubas-Meyer |January 2, 2015 |DAILY BEAST 

"I supposed you 'adn't, as 'e ain't 'ere, with yer ma," replied the young person. Rosemary in Search of a Father |C. N. Williamson 

Postrem quid nobis effectum hactenus, seu potis quid attentatum sit ad diuinam gloriam. The Jesuit Relations and Allied Documents, Vol. II: Acadia, 1612-1614 |Various 

Cert ante hoc tempus vix vnquam Gallis vacatum fuit conuertdis incolarum ad Christum animis. The Jesuit Relations and Allied Documents, Vol. II: Acadia, 1612-1614 |Various 

I don't blame him for killin' the cuss, not a bit; I'd have shot any man livin' that 'ad taken a good horse o' mine up that trail. Ramona |Helen Hunt Jackson 

In hoc Isthmo portus regalis est, vbi nunc degimus, ad gradum latitudinis quadragesimum quartum cum besse. The Jesuit Relations and Allied Documents, Vol. II: Acadia, 1612-1614 |Various