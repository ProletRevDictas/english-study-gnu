While the finds from the genetic study are largely supported by established narratives and historic records of the transatlantic slave trade, there were also inconsistencies. Why so many African Americans have Nigerian ancestry |Uwagbale Edward-Ekpu |August 10, 2020 |Quartz 

Lawmakers, in fact, passed the Fugitive Slave Act two years after statehood, creating a legal mechanism for the return of slaves to their owners. Sacramento Report: Reparations Bill Marches Forward |Sara Libby and Jesse Marx |July 31, 2020 |Voice of San Diego 

For the first half of the 19th century, the Senate was a bulwark for the South, with an equal balance of slave and free states despite the growing Northern population advantage. The Senate Has Always Favored Smaller States. It Just Didn’t Help Republicans Until Now. |Lee Drutman (drutman@newamerica.org) |July 29, 2020 |FiveThirtyEight 

The Republican Party was the party of Lincoln, the emancipated slaves. Should America (and FIFA) Pay Reparations? (Ep. 426) |Stephen J. Dubner |July 16, 2020 |Freakonomics 

According to various estimates, almost 17,000 people among the peak 50,000 population were slaves – who not only worked in households but also in the harbor and storage facilities. Ostia Antica: Reconstruction and History of The Harbor City of Ancient Rome |Dattatreya Mandal |April 14, 2020 |Realm of History 

By giving an artistic veto to a madman, we submit to the mindset of a slave. The Sony Hack and America’s Craven Capitulation To Terror |David Keyes |December 19, 2014 |DAILY BEAST 

In the Bible, Moses does kill a guy—the Egyptian slave master who is beating an Israelite to death. Christian Bale: One Man's Moses Is Another Man's Terrorist |Candida Moss, Joel Baden |December 7, 2014 |DAILY BEAST 

This year McQueen picked up three Oscars (including best picture) for his third motion picture 12 Years A Slave. Has the Turner Prize Gone Soft? |David Levesley |December 2, 2014 |DAILY BEAST 

For the next hour and half I was the willing sex slave of a semi-professional Master I had met through a friend. Whip It: Secrets of a Dominatrix |Justin Jones |November 25, 2014 |DAILY BEAST 

Within the black community itself, the slave mentality is still being battled. Herbie Hancock Holds Forth |David Yaffe |November 8, 2014 |DAILY BEAST 

Gallinas, the noted slave factory on the west of Africa, purchased by the Liberian republic. The Every Day Book of History and Chronology |Joel Munsell 

If he is indifferent to my happiness, and unjust to the woman I love, I will no longer work like a slave for him. The World Before Them |Susanna Moodie 

Although the number of slaves in the Brazils is very great, there is nowhere such a thing as a slave-market. A Woman's Journey Round the World |Ida Pfeiffer 

In Brazil, the slave-trade exists in full force; in Cuba, it is unmitigated in its extent and horrors. Blackwood's Edinburgh Magazine, No. CCCXXXIX. January, 1844. Vol. LV. |Various 

In 1813, some disputes arose between the court of Rio and England on account of the slave trade. Journal of a Voyage to Brazil |Maria Graham