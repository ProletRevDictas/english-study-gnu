A handful of places responded to our public records request with a sheaf of court documents and told us to figure out the totals on our own. Cities Spend Millions On Police Misconduct Every Year. Here’s Why It’s So Difficult to Hold Departments Accountable. |Amelia Thomson-DeVeaux (Amelia.Thomson-DeVeaux@abc.com) |February 22, 2021 |FiveThirtyEight 

Instead of merely giving me a few data points to learn from, The Exchange wound up collecting sheafs of interesting data from upstart companies with big Q3 performance. Here’s how fast a few dozen startups grew in Q3 2020 |Alex Wilhelm |October 23, 2020 |TechCrunch 

We would get a sheaf of papers and pencils and listen to the tapes. Going Public With the Nixon Tapes |Scott Porch |August 7, 2014 |DAILY BEAST 

It was months after the assignment when, at last, I presented to Murcko a thick sheaf of pages. The Strange and Mysterious Death of Mrs. Jerry Lee Lewis |Richard Ben Cramer |January 11, 2014 |DAILY BEAST 

She started what she called her "lecture" with a fat sheaf of papers in front of her, from which she proceeded to read. A First Taste of Documenta |Blake Gopnik |June 6, 2012 |DAILY BEAST 

No, I don't have a sheaf of short stories buried in a drawer. How "Patriots" Changed Me |David Frum |May 18, 2012 |DAILY BEAST 

In one hand she carried a sheaf of Clinton literature; in the other she was lugging a stack of large yard signs. The Boys on the Bus |Ben Crair |November 3, 2008 |DAILY BEAST 

Rarely, sodium urate occurs in crystalline form—slender prisms, arranged in fan- or sheaf-like structures (Fig. 32). A Manual of Clinical Diagnosis |James Campbell Todd 

A wisp of wheat was knotted round her neck for a necklace, and a perfect sheaf of it in her hair. Music-Study in Germany |Amy Fay 

Ward busied himself with a sheaf of morning mail and miscellaneous police circulars. Hooded Detective, Volume III No. 2, January, 1942 |Various 

Her hat was covered with poppies and wild azalea, and she had a sheaf of buttercups and "blue eyes" in her belt. Ancestors |Gertrude Atherton 

The two contestants mounted new horses and sat face to face; behind each stood an attendant with the sheaf of reed lances. God Wills It! |William Stearns Davis