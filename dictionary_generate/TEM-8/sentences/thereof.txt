Accumulated unused sick days could be converted to cash or paid time off, or a combination thereof. How We Found Pricey Provisions in New Jersey Police Contracts |by Agnes Chang, Jeff Kao and Agnel Philip, ProPublica, and Andrew Ford, Asbury Park Press |February 8, 2021 |ProPublica 

An issue of trustTrust in Google — or lack thereof — is at the core of most complaints about the Privacy Sandbox process. Why Google’s approach to replacing the cookie is drawing antitrust scrutiny |Kate Kaye |February 2, 2021 |Digiday 

He can solve those problems with his mind, his arm, his legs or some combination thereof. The NFL’s conference title games will feature an endangered species: The pocket passer |Adam Kilgore |January 21, 2021 |Washington Post 

A beard, or lack thereof, can say a lot about a person’s style and sensibilities. The best beard trimmer: Shape your facial hair with ease |Carsen Joenk |January 19, 2021 |Popular-Science 

A San Diego County group that reviews cases of possible police misconduct is pressing the Sheriff’s Department over its body-worn camera policy, or lack thereof. Morning Report: Review Board Scolds Sheriff Over Body Cameras |Voice of San Diego |November 23, 2020 |Voice of San Diego 

Smiles, or lack thereof, did not bring down the Ancient Regime, of course. The French Court’s Royal Ban on Smiles |William O’Connor |December 14, 2014 |DAILY BEAST 

U.S. schools will receive funding and “customers” regardless of their merits (or lack thereof). How a GOP Senate Can Help the Poor |Veronique de Rugy |November 23, 2014 |DAILY BEAST 

Albert Camus used violence as a means of exploring meaning, or lack thereof, in his existential novels. Is ‘Satisfaction’ a Love Story That’s Too Real About Sex and Marriage? |David Masciotra |September 19, 2014 |DAILY BEAST 

Both stumbled in their first presidential campaigns over issues of their service, or lack thereof, in the fight against communism. How Obama's Shallow Worldview Failed Us |Stuart Stevens |August 29, 2014 |DAILY BEAST 

As we all know, it requires Congress not to make any law “prohibiting the free exercise thereof.” Gay Marriage Vs. the First Amendment |James Poulos |August 22, 2014 |DAILY BEAST 

I have only set down part of this Peal, which is sufficient to shew the course and method thereof. Tintinnalogia, or, the Art of Ringing |Richard Duckworth and Fabian Stedman 

The eye admireth at the beauty of the whiteness thereof, and the heart is astonished at the shower thereof. The Bible, Douay-Rheims Version |Various 

Let them that sail on the sea, tell the dangers thereof: and when we hear with our ears, we shall admire. The Bible, Douay-Rheims Version |Various 

They burnt the chosen city of holiness, and made the streets thereof desolate according to the prediction of Jeremias. The Bible, Douay-Rheims Version |Various 

Let us go up to Juda, and rouse it up, and draw it away to us, and make the son of Tabeel king in the midst thereof. The Bible, Douay-Rheims Version |Various