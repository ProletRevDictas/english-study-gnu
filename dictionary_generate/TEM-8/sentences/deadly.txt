These aren’t the kinds of choices restaurateurs should be making during a deadly pandemic. Is the Government Just Going to Watch the Restaurant Industry Die? |Elazar Sontag |August 28, 2020 |Eater 

It referenced the deadly 2007 Rice Fire, which started when a Sycamore tree limb hit a power line near Fallbrook. Watchdog Warns: SDG&E’s Tree-Trimming Plan Could Worsen Wildfires |MacKenzie Elmer |August 24, 2020 |Voice of San Diego 

At least one real-world field study supports the idea that transmissible vaccines can be both safe and effective at eradicating a deadly disease in wildlife. Can Vaccines for Wildlife Prevent Human Pandemics? |Rodrigo Pérez Ortega |August 24, 2020 |Quanta Magazine 

Odorless and tasteless, the powder could be mixed into bits of meat and scattered across the landscape as deadly bait for dingoes to snatch up. Culling dingoes with poison may be making them bigger |Jake Buehler |August 19, 2020 |Science News 

In a 2016 study, six of eight cats recovered from an infection with the deadly form of the feline coronavirus after treatment with the drug, Pedersen and colleagues reported in PLOS Pathogens. How two coronavirus drugs for cats might help humans fight COVID-19 |Erin Garcia de Jesus |August 11, 2020 |Science News 

The comedian responded to the deadly attack on a French satirical magazine by renewing his recent criticisms of the Islamic faith. Bill Maher: Hundreds of Millions of Muslims Support Attack on ‘Charlie Hebdo’ |Lloyd Grove |January 8, 2015 |DAILY BEAST 

Gunshots rang out in Paris this morning on a second day of deadly violence that has stunned the French capital. France Mourns—and Hunts |Nico Hines, Christopher Dickey |January 8, 2015 |DAILY BEAST 

Shortly after dawn, there was another outbreak of deadly force. France Mourns—and Hunts |Nico Hines, Christopher Dickey |January 8, 2015 |DAILY BEAST 

Whatever the reason, and however absurd their beliefs may seem, American evangelicals are deadly serious. The Evangelical Apocalypse Is All Your Fault |Jay Michaelson |January 4, 2015 |DAILY BEAST 

They now claim that Amanda actually wielded the knife and struck the deadly blow, increasing her sentence to 28-and-a-half years. Amanda Knox: A Mother’s Obsession |Nina Darnton |November 26, 2014 |DAILY BEAST 

There is a fascination in serpents, and there is one far more deadly—who has not felt it? Blackwood's Edinburgh Magazine, Volume 60, No. 372, October 1846 |Various 

This time they were not met by the cavalrymen alone, but the cannon belched forth its deadly charge of canister in their faces. The Courier of the Ozarks |Byron A. Dunn 

I wish Vicky saw through her; she has so much influence over Jack, and such deadly powers of ridicule. Ancestors |Gertrude Atherton 

The weeks lengthened into months, and the holidays came; but just before the holidays Black Sheep fell into deadly sin. Kipling Stories and Poems Every Child Should Know, Book II |Rudyard Kipling 

Economy with the good old soul was a cardinal virtue, waste a deadly sin. Fifty Years of Railway Life in England, Scotland and Ireland |Joseph Tatlow