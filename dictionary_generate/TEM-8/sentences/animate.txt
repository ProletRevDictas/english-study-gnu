A new RSA animated short featuring his talk is a critical reminder of how powerfully our tools shape our experience—and how easily that feedback loop can go wrong. What Two Billion People Pay Attention to Is Still in the Hands of a Few Companies |Jason Dorrier |September 20, 2020 |Singularity Hub 

Piracy has long been an issue in China—Disney’s 1998 animated version of Mulan also suffered in the China box office in part because of mass piracy before its official release. Disney tailored ‘Mulan’ for China. It still ‘never had a chance’ at the mainland box office |Naomi Xu Elegant |September 20, 2020 |Fortune 

Notably, all of these films, most of which are animated, come from after the “Eisner era” of Disney films, known as the “renaissance” that began with 1989’s The Little Mermaid under former Disney CEO Michael Eisner. Toward a queer Disney canon |Emily VanDerWerff |September 4, 2020 |Vox 

Projects already in production include a nature docu-series and an animated series about inspiring women. Prince Harry and Meghan have signed a wide-ranging Netflix production deal |radmarya |September 2, 2020 |Fortune 

After physical production shut down in March, TV networks and streamers sought out shows that could be produced remotely, such as animated fare and documentary series that rely on archival footage and interviews shot over Zoom. ‘Covid-proof production’: TV networks, streamers seek out show formats that can adapt to another shooting shutdown |Tim Peterson |August 14, 2020 |Digiday 

It is the economic questions—about the gap between rich and poor—that animate the party now. Andrew Cuomo Can't Ignore It Now: He's Weak Even at Home |David Freedlander |September 10, 2014 |DAILY BEAST 

Once, the humanist idea used to animate the very core of the university. Can Higher Education Really Save Our Humanity? |James Poulos |February 1, 2014 |DAILY BEAST 

Investors clearly believe in the value of patents and the inventions they animate. New Calculations of U.S. GDP Finally Take Research and Development Into Account |Robert Shapiro |August 2, 2013 |DAILY BEAST 

His fingers were rubbing back and forth on the photo, as if he was trying to animate his baby back to life. A Personal Plea for Gun Control |Joshua DuBois |April 10, 2013 |DAILY BEAST 

I admire his vision in his domestic affairs and the deeply Jewish values that seem to animate him. Obama: Come Visit The Settlements |Samuel Lebens |March 21, 2013 |DAILY BEAST 

The birds, moreover, were singing merrily, and all Nature seemed animate and gay. Travels through the South of France and the Interior of Provinces of Provence and Languedoc in the Years 1807 and 1808 |Lt-Col. Pinkney 

That which would have disheartened and disarmed other men, seemed only to animate him with all Macbeth's wild courage of despair. Great Men and Famous Women. Vol. 4 of 8 |Various 

A new spirit would animate the community, from which we might hope the most happy results. Thoughts on Educational Topics and Institutions |George S. Boutwell 

But it was enough for that white-clad figure to stand revealed in the thickest of the carnage to animate the men to heroic effort. A Heroine of France |Evelyn Everett-Green 

Hear from the grave, great Taliessin, hearThey breathe a soul to animate thy clay. The Ontario Readers: The High School Reader, 1886 |Ministry of Education