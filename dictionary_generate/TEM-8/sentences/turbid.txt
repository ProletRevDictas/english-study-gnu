It’s a terrific and slightly odd moment, one that briefly jolts this mostly turbid film to life. Rebecca Hall Shows Some Bristling Energy in the Otherwise Turbid Ghost Thriller The Night House |Stephanie Zacharek |August 20, 2021 |Time 

For those living an aquatic life, small eyes might be related to the fact that they live in turbid water. What hundreds of pickled frog carcasses can tell us about their enormous eyes |María Paula Rubiano A. |October 1, 2020 |Popular-Science 

Pettay recently analyzed its genome, and concluded that the alga is descended from a zooxanthellae native to a hot, shallow, often turbid region of the Indo-Pacific Ocean near Thailand. When Evolution Is Infectious - Issue 90: Something Green |Moises Velasquez-Manoff |September 30, 2020 |Nautilus 

The tops of the hills were laden with thunder-clouds, and the turbid atmosphere laboured with the stifling Sirocco. The Pastor's Fire-side Vol. 3 of 4 |Jane Porter 

His expletives were varied, vivid and inexhaustible, and the turbid stream was easily set flowing. Fifty Years of Railway Life in England, Scotland and Ireland |Joseph Tatlow 

In all but the smaller lakelets these turbid waters lay down all their sediment before they attain the outlet of the basin. Outlines of the Earth's History |Nathaniel Southgate Shaler 

The alcoholic solution of the tar became turbid on diluting with water. Some Constituents of the Poison Ivy Plant: (Rhus Toxicodendron) |William Anderson Syme 

Its waters were red and turbid; its banks well timbered, with a rich, prolific soil. The Expeditions of Zebulon Montgomery Pike, Volume II (of 3) |Elliott Coues