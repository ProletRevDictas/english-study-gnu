And Mangum swears that his knowledge of Esperanto better prepared him to learn Spanish. Do You Speak Klingon? |Stephen Brown |June 14, 2009 |DAILY BEAST 

Esperanto fell well short of Zamenhof's goal of a universal second language, but it was not a complete failure. Do You Speak Klingon? |Stephen Brown |June 14, 2009 |DAILY BEAST 

Nowhere is this pipe dream more obvious than in the history of Esperanto, one of the world's most well-known invented languages. Do You Speak Klingon? |Stephen Brown |June 14, 2009 |DAILY BEAST 

To this day, many Esperanto enthusiasts, including Doug Mangum of Greensboro, N.C., share in this frustration. Do You Speak Klingon? |Stephen Brown |June 14, 2009 |DAILY BEAST 

It is curious to notice that the apparently innocent invention of Esperanto receives support from the same quarter. Secret Societies And Subversive Movements |Nesta H. Webster 

Here is a leaflet about the Panama Exposition published in Esperanto. Esperanto: Hearings before the Committee on Education |Richard Bartholdt and A. Christen 

Here is a bookseller in Paris issuing a catalogue entirely in Esperanto. Esperanto: Hearings before the Committee on Education |Richard Bartholdt and A. Christen 

For instance, in Paris and Barcelona there are many thousands who understand Esperanto. Esperanto: Hearings before the Committee on Education |Richard Bartholdt and A. Christen 

The town of Antwerp publishes an illustrated guide of the town in Esperanto. Esperanto: Hearings before the Committee on Education |Richard Bartholdt and A. Christen