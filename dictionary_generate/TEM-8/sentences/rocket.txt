That mission, the first uncrewed flight test of our powerful Space Launch System rocket and Orion spacecraft, is just a little more than a year away from launch. NASA just announced in a blog post that SLS will cost 30% more |Eric Berger |August 28, 2020 |Ars Technica 

When it flies, it will do so with six of the same engines, on top of a giant rocket booster called Super Heavy. SpaceX flew a prototype of its Starship vehicle for the first time |Neel Patel |August 5, 2020 |MIT Technology Review 

To take off from the Martian surface and return home, astronauts will need liquid oxygen rocket fuel. NASA’s Perseverance rover will seek signs of past life on Mars |Lisa Grossman |July 28, 2020 |Science News 

Large industrial 3D printers can whip up rockets and houses. GE Will 3D Print the Bases of Wind Turbines Taller Than Seattle’s Space Needle |Jason Dorrier |June 21, 2020 |Singularity Hub 

At 394 feet tall by 30 feet wide, the rocket outsizes all those previously used in spaceflight, including the Saturn V used in NASA’s Apollo program. SpaceX Wants to Build Floating Spaceports for Daily Starship Launches |Vanessa Bates Ramirez |June 19, 2020 |Singularity Hub 

The questions going through my mind are: How on earth are there Kalashnikovs and rocket launchers in the heart of Paris? Ayaan Hirsi Ali: Our Duty Is to Keep Charlie Hebdo Alive |Ayaan Hirsi Ali |January 8, 2015 |DAILY BEAST 

These people that work for the BOP are not rocket scientists. How a ‘Real Housewife’ Survives Prison: ‘I Don’t See [Teresa Giudice] Having a Cakewalk Here’ |Michael Howard |January 6, 2015 |DAILY BEAST 

Her brothers formed a group to rescue people after a rocket attack. Drawing on the Memories of Syrian Women |Masha Hamilton |November 26, 2014 |DAILY BEAST 

It is adopting technology—in rocket propulsion, composite construction, and aerodynamic refinements—already in use elsewhere. Can Anyone Make Space Safe for Civilians? |Clive Irving |November 4, 2014 |DAILY BEAST 

That would require the rocket to run for 55 to 60 seconds without a glitch. Can Anyone Make Space Safe for Civilians? |Clive Irving |November 4, 2014 |DAILY BEAST 

General Pio del Pilar slept in the city every night, ready to give the rocket-signal for revolt. The Philippine Islands |John Foreman 

It was in conjunction with Mr. Booth that my father constructed the 'Rocket' engine. Life of Richard Trevithick, Volume II (of 2) |Francis Trevithick 

The day “Rocket” was much older, and got a good share of the Isle of Wight traffic. The Portsmouth Road and Its Tributaries |Charles G. Harper 

Francis Falconer, who died at Petersfield about 1874, drove the day “Rocket” all the time it ran. The Portsmouth Road and Its Tributaries |Charles G. Harper 

The Portsmouth Road has known no through coach since his “Rocket” was discontinued. The Portsmouth Road and Its Tributaries |Charles G. Harper