They were then forced to disband because they lacked the statutory minimum of MPs, and AfD lawmakers suddenly found themselves stripped of their privileges. Is the Slide of European Populism Permanent? |Charu Kasturi |October 15, 2020 |Ozy 

To fix this, the report suggests creating a statutory red line, defining a seller as dominant if it has a market share of 30% or more and a buyer as dominant with just a 25% share. House panel proposes largest antitrust reforms in decades to rein in Big Tech |kdunn6 |October 7, 2020 |Fortune 

If you’re willing to do that, it’s fine to wait until the statutory deadlines to request and submit your ballot. If You Wait Until Your State’s Deadline To Mail Your Ballot, You May Be Too Late |Nathaniel Rakich (nathaniel.rakich@fivethirtyeight.com) |August 25, 2020 |FiveThirtyEight 

It does not require CSS to endorse any same-sex marriages, but merely to certify that families meet Pennsylvania’s statutory criteria. Supreme Court sets Nov. 4 to hear if Catholic agency can reject LGBTQ parents |Chris Johnson |August 19, 2020 |Washington Blade 

Judges who subscribe to textualism believe they should evaluate only the words of a statute enacted by Congress and not consider evidence outside the statutory language. The Supreme Court Decision To Grant Protections To LGBT Workers Is An Important Expansion Of The Civil Rights Act |LGBTQ-Editor |June 18, 2020 |No Straight News 

The statutory tax rate in the U.S. is 35 percent compared to 15 percent for Canada. Burger King Invades Canada to Save His Faltering Kingdom |Daniel Gross |August 26, 2014 |DAILY BEAST 

But the Texas governor ignored statutory procedure and acted unilaterally. Yes, Rick Perry Could End Up Doing Time |Dean Obeidallah |August 22, 2014 |DAILY BEAST 

But in the short term, statutory authority for the program expires September 30. Dysfunctional Congress Prepares to Claim Another Victim: Injured Veterans |Tim Mak |July 23, 2014 |DAILY BEAST 

“The statutory issue depends on whether ISIS is covered by the AUMF,” he said. ‘Drone Memo’ Doesn’t Apply to America’s New Terror War |Eli Lake |June 24, 2014 |DAILY BEAST 

Naomi says statutory rape is “when the person force you against your will and you too small.” Liberia’s Child Prostitutes |Clair MacDougall |May 19, 2014 |DAILY BEAST 

The marked feature of this period is the paucity of statutory enactment affecting relief. English Poor Law Policy |Sidney Webb 

Statutory powers were obtained for the provision of hospitals in the Metropolis by combinations of boards of guardians. English Poor Law Policy |Sidney Webb 

The foregoing rules with the table were laid before Parliament in the usual way, and so received the required statutory sanction. Loss of the Steamship 'Titanic' |British Government 

It is sufficient to say of these answers that they all suggested a large extension of the statutory requirements. Loss of the Steamship 'Titanic' |British Government 

These crimes being statutory, must turn altogether on the language of the Act of Congress. The Old Pike |Thomas B. Searight