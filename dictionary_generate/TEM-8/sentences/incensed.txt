They were so incensed that they resigned and founded another journal, October, which remains an influential voice among old-guard supporters of the avant-garde. Artist Lynda Benglis became controversial in an instant, but her career has thrived for decades |Philip Kennicott |August 26, 2021 |Washington Post 

You can first identify leakage points by turning on your kitchen and bathroom exhaust fans, creating a slight pressure differential between indoors and outdoors, then holding up a lit incense stick to potential problem areas. How to stop your house’s expensive drafts — and save the planet |Sarah Kaplan |January 27, 2021 |Washington Post 

They voted with their feet, like the Fox News viewers incensed by the network's accurate 2020 election coverage who switched to Newsmax. The Trailer: What's next for the Republican Party |David Weigel |January 19, 2021 |Washington Post 

There’s a humorous element to seeing fans angrily tweet at royal family accounts, incensed by what they have just witnessed O’Connor or Olivia Colman’s Elizabeth enact on-screen. As ‘The Crown’ tackles recent history with Princess Diana, the show hits a nerve |Sonia Rao |November 19, 2020 |Washington Post 

Despite the 2008 ruling in District of Columbia v Heller which established a right to bear arms for self-defence, conservatives have become increasingly incensed at state and local laws governing gun ownership. How Presidents Have Shaped The US Supreme Court – And Why The Choice Of Its Next Justice Is So Crucial |LGBTQ-Editor |September 23, 2020 |No Straight News 

In a Hot Springs, Arkansas, stud-poker game, a player named Burke became justly incensed one evening because he could not win. Portrait of the Consummate Con Man |John Lardner |May 17, 2014 |DAILY BEAST 

Her incensed mother contacted a lawyer, hoping to block publication; Polanski had not made her sign a release. The Roman Polanski ‘Girl’ Speaks Out: Speedreading Samantha Geimer’s Memoir |Thomas Flynn |September 12, 2013 |DAILY BEAST 

The family, villagers, and the Taliban were even incensed by a shampoo commercial, featuring Humira, which played on television. The World’s Bravest Singers |Ron Moreau & Sami Yousafzai |July 18, 2013 |DAILY BEAST 

It incensed the students, to the extent that they actually shouted me down. The Pointlessness of Some Disaster Charity After the Indian Floods |Dilip D’Souza |June 26, 2013 |DAILY BEAST 

Snipes became further incensed when he learned that Ryan Reynolds and Jessica Biel had been hired to co-star in the film. Out of Jail, Does Wesley Snipes Have a Second Act? |Allison Samuels |June 7, 2013 |DAILY BEAST 

But there's no denying that I behaved disgracefully to—you know—and Dr. Ashton has good reason to be incensed. Elster's Folly |Mrs. Henry Wood 

It may be, of course, that the responsibility was thrown on the lady in order to restrain the hand of the incensed king. King Robert the Bruce |A. F. Murison 

But mother and Maxim bravely defended Stenka, and I was deeply incensed at father, who despotically terminated the discussion. Prison Memoirs of an Anarchist |Alexander Berkman 

M. d'Infreville, justly incensed against me, had not left me a sou, and my mother and I became terribly poor. The Seven Cardinal Sins: Envy and Indolence |Eugne Sue 

This action incensed the emperor, who immediately dispatched an army of eighty thousand men against the city. Hebraic Literature; Translations from the Talmud, Midrashim and Kabbala |Various