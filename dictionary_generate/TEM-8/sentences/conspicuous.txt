“Eyes Wide Shut” meets abandoned mansion horror flick for the conspicuous consumption set French Laundry Launches $850-Per-Person Indoor Dining ‘Experience’ Fit for a Bond Villain |Eve Batey |September 3, 2020 |Eater 

Mounting your monitor on an arm also makes your space easier to clean, and the result is an undeniably sleek and integrated look without the bulky, conspicuous factory base. The best monitor arms for desk-mounting your display |PopSci Commerce Team |August 26, 2020 |Popular-Science 

Despite these conspicuous failures, climate change too is now being described with the rhetoric of warfare. Why female leaders are faring better than ‘wartime presidents’ against COVID-19 |matthewheimer |August 20, 2020 |Fortune 

In terms of design, the skull core of the equipment was made from single sheet bronze, while the conspicuous apex was made separately and then tightly riveted to the main helmet. Know Your Ancient Greek Helmets: From Attic to Phrygian |Dattatreya Mandal |May 19, 2020 |Realm of History 

Used primarily for ventilation purposes, this conspicuous aperture also allowed the entry of rainwater, which was then collected on the floor-based cavity known as the impluvium and then passed on to the underground cisterns for household usage. The Roman Domus (House): Architecture and Reconstruction |Dattatreya Mandal |April 8, 2020 |Realm of History 

The system is truck-mounted and road-mobile, as are the big and conspicuous radars that stood next to it on display. How China Will Track—and Kill—America’s Newest Stealth Jets |Bill Sweetman |December 2, 2014 |DAILY BEAST 

For Shaftel this kind of licentious behavior amounts to “conspicuous consumption disguised as urbanity.” Don’t Diss the Beauty of Brunch: Defending Our Favorite Meal |Tim Teeman |October 15, 2014 |DAILY BEAST 

Any list of his conspicuous qualities turns out to be a recitation of opposites. Will the Real Jim Palmer Please Stand Up |Tom Boswell |September 27, 2014 |DAILY BEAST 

As recently as a few years ago, buying and driving a hybrid was an exercise in conspicuous consumption. Hybrid Cars Are Niche No More |The Daily Beast |June 6, 2014 |DAILY BEAST 

It was a dizzying time, and Shaquille handled an array of new situations with conspicuous aplomb. Shaq, Year One |Charles P. Pierce |May 24, 2014 |DAILY BEAST 

The doctrine of international free trade, albeit the most conspicuous of its applications, was but one case under the general law. The Unsolved Riddle of Social Justice |Stephen Leacock 

Here was none of the old-time stiffness of Capheaton, and there was a conspicuous absence of dead masters and their pupils. Ancestors |Gertrude Atherton 

The movements which its active motion causes among the corpuscles render it conspicuous. A Manual of Clinical Diagnosis |James Campbell Todd 

The more conspicuous characteristics of the blood in various diseases have been mentioned in previous sections. A Manual of Clinical Diagnosis |James Campbell Todd 

Although all varieties are increased, the characteristic and conspicuous cell is the myelocyte. A Manual of Clinical Diagnosis |James Campbell Todd