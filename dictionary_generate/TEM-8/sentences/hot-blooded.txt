Gay marriage was the hot-button fight on the left and right. Jeb Bush’s Unseen Anti-Gay Marriage Emails |Jackie Kucinich |January 9, 2015 |DAILY BEAST 

Everybody is trapped in an elevator together and tempers run a little hot. ‘Archer’ Creator Adam Reed Spills Season 6 Secrets, From Surreal Plotlines to Life Post-ISIS |Marlow Stern |January 8, 2015 |DAILY BEAST 

Even the hot Jewish women I mentioned above did something a bit more “intellectual” than pageantry: acting. Why Was Bess Myerson the First and Last Jewish Miss America? |Emily Shire |January 7, 2015 |DAILY BEAST 

There was deep brown flesh, and bronze flesh, and pallid white flesh, and flesh turned red from the hot sun. Powerful Congressman Writes About ‘Fleshy Breasts’ |Asawin Suebsaeng |January 7, 2015 |DAILY BEAST 

Many Jewish women have been accepted as conventional, mainstream hot. Why Was Bess Myerson the First and Last Jewish Miss America? |Emily Shire |January 7, 2015 |DAILY BEAST 

In the drawing-room things went on much as they always do in country drawing-rooms in the hot weather. The Pit Town Coronet, Volume I (of 3) |Charles James Wills 

“You appear to feel it so,” rejoined Mr. Pickwick, smiling at the clerk, who was literally red-hot. The Posthumous Papers of the Pickwick Club, v. 2(of 2) |Charles Dickens 

Nearly half the regiment ran to secure their picketed horses, armed themselves in hot haste, and galloped to the gaol. The Red Year |Louis Tracy 

News came that the rebels were plundering the British quarters, and the infantry went there in hot haste. The Red Year |Louis Tracy 

From Canada on the north, to Texas on the south, the hot winds had laid the land seemingly bare. The Homesteader |Oscar Micheaux