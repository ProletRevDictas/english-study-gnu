I would conjecture that 90% of ad disapprovals occur from honest advertisers who don't even realize one of their products is prohibited or there is an issue on the site. Google’s three-strikes ad policy isn’t the problem, it’s policy application that worries advertisers |George Nguyen |July 23, 2021 |Search Engine Land 

Under this formative cloud of disapproval and a lifetime of pressure to conform to entrenched standards of beauty and behavior, Roosevelt evolved into a fierce, politically savvy intellectual. How Greenwich Village shaped Eleanor Roosevelt |Karen Tucker |June 4, 2021 |Washington Post 

In reaction to the recent arrests, prominent human rights groups have expressed their disapproval of LGBTQ human rights abuses in Ghana. State Department urges Ghana to protect LGBTQ rights after activists arrest |Prince Chingarande |May 25, 2021 |Washington Blade 

Either way, the community the sneaker drew its inspiration from was excluded from a chance to buy it, incurring the disapproval of some in South Africa. Why South Africans are upset Adidas isn’t selling this one shoe to them |Marc Bain |May 23, 2021 |Quartz 

Complicating matters further is the fact that a disapproval resolution like Sanders’s, though privileged, remains in the purview of the Senate Foreign Relations Committee for up to 10 days. Sen. Bernie Sanders to introduce resolution of disapproval on $735 million U.S. arms sale to Israel |Jacqueline Alemany, Karoun Demirjian |May 20, 2021 |Washington Post 

While critics felt the season was inspired, some fans voiced their disapproval. ‘Archer’ Season 6 Preview: Cast and Crew on Rebranding and Dropping ISIS |Marlow Stern |October 27, 2014 |DAILY BEAST 

Mustering a coalition of disapproval for the World Cup should be much easier than it would be for an Olympiad. Best Way to Punish Putin? No World Cup |Tunku Varadarajan |July 20, 2014 |DAILY BEAST 

That unexpected rejection of gender equality within the church won negative headlines and widespread disapproval in Britain. Church of England Gets Female Bishops and a Conservative Backlash |Nico Hines |July 14, 2014 |DAILY BEAST 

Hardline media have roared their disapproval and prominent conservative politicians have joined in. The Kiss That Sent Iran Crazy and an Actress to Be Flogged in Public |IranWire |May 23, 2014 |DAILY BEAST 

Even the individual mandate elicits just narrow disapproval, 51% to 47%. Democrats Must Run on Obamacare in November |Robert Shrum |March 17, 2014 |DAILY BEAST 

Its culture however was looked upon with the same disapproval by Charles II. Tobacco; Its History, Varieties, Culture, Manufacture and Commerce |E. R. Billings. 

Gwynne's disapproval vanished as he shook hands with the blooming young matron and met her bright laughing eyes. Ancestors |Gertrude Atherton 

This canto never would have converted Boileau from his disapproval of the "merveilleux chrétien." Frdric Mistral |Charles Alfred Downer 

Disapproval has been expressed of many of the broadcast serials and suggestive love songs. Report of the Special Committee on Moral Delinquency in Children and Adolescents |Oswald Chettle Mazengarb et al. 

Somehow Harvey seemed, those days, to present a constant figure of disapproval. The Amazing Interlude |Mary Roberts Rinehart