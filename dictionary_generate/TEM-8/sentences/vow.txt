We thank the Council for their action today, as we all reaffirm our vow that hate has no place in the District of Columbia. D.C. Council approves bill banning LGBTQ panic defense |Lou Chibbaro Jr. |December 16, 2020 |Washington Blade 

Encouraged by a friend, I wrote and asked if, while she stayed with us, the justice might consider officiating over a brief ceremony to renew our marriage vows. The Notorious RBG |Ted Osius |September 19, 2020 |Washington Blade 

As major drug companies prepare a public vow to adhere to the most rigorous vaccine safety standards, watch for a fringe-driven conversation against vaccines to accelerate. American Fringes: How the Media Makes it Worse |Nick Fouriezos |September 7, 2020 |Ozy 

Katelyn Stanis, owner of vow writing company Wedding Words, had to cut her own guest list down from 100 to 15. How to update your guests about your pandemic wedding plans |Brooke Henderson |August 29, 2020 |Fortune 

In addition to being a part of the strange club of 2020 brides, Stanis knows what it’s like to be on both sides of the bridal party––after all, she’s a professional wedding vow and speechwriter. How to turn down a wedding invitation during the coronavirus pandemic |Brooke Henderson |August 23, 2020 |Fortune 

Kenyatta made a Bush-like vow to continue the war on Al-Shabab, saying, “We will not flinch.” Al-Shabab’s Anti-Christian Slaughter |Margot Kiser |December 3, 2014 |DAILY BEAST 

Under unusually blue skies in Beijing, the American and Chinese presidents vow cooperation to reduce greenhouse gas emissions. Obama and Xi Jinping Say They’ll Work Together to Save Environment |Ben Leung |November 12, 2014 |DAILY BEAST 

He made the vow on June 30, and I remember thinking at the time how strange it seemed. Immigration Reform? Not Until Hillary |Michael Tomasky |September 8, 2014 |DAILY BEAST 

Somewhat coyly, Skidmore admits that “Richard was to break this solemn vow in spectacular style.” Three Dicks: Cheney, Nixon, Richard III and the Art of Reputation Rehab |Clive Irving |July 27, 2014 |DAILY BEAST 

Hamas spokesmen stand by the hospital gates and denounce the attack on Al Shejaiya as a massacre and vow to fight on. Palestinians Fleeing Israeli Bombardment in Gaza Have ‘Nowhere Left to Run’ |Jesse Rosenfeld |July 20, 2014 |DAILY BEAST 

My mother opposed her vow to his; not to suffer her child to leave her, till the time of her being professed. The Pastor's Fire-side Vol. 3 of 4 |Jane Porter 

The swearing of an oath always brings under obligation to God, and therefore always includes the making of a vow. The Ordinance of Covenanting |John Cunningham 

I have made a vow never to be hanged in the beginning of a revolution, nor to be shot in the beginning of a war. Blackwood's Edinburgh Magazine, No. CCCXXXIX. January, 1844. Vol. LV. |Various 

The apprehension that God will punish for not making fulfilment to him accompanies equally the oath and the vow. The Ordinance of Covenanting |John Cunningham 

In like manner, the Nazarite separated himself from certain things, not merely in reality, but likewise by vow. The Ordinance of Covenanting |John Cunningham