The recent tech stock tear has increased the wealth of Bezos, the richest man in the world, to $205 billion, making him the first person in history to be worth more than $200 billion. MacKenzie Scott is now the wealthiest woman in the world |ehinchliffe |September 2, 2020 |Fortune 

The anti-fray design means you won’t have to deal with all the wear and tear that comes with regular use. The best ankle weights for a sculpted lower body |PopSci Commerce Team |September 1, 2020 |Popular-Science 

Laugh at “Like Crazy” and don’t be surprised if there’s a tear or two. Learning to parent your parent |Terri Schlichenmeyer |August 21, 2020 |Washington Blade 

Then with my wife almost in tears, he threatened her with jail if she refused to sign. An Officer’s Word Shouldn’t Be the Last Word |Andrew Taylor |August 19, 2020 |Voice of San Diego 

Tech shares have been on a tear this year as the Covid-19 pandemic drove more people online, lifting the fortunes of the companies’ founders and putting the industry under increased scrutiny. Elon Musk adds another $8 billion to become world’s fourth-richest person |Verne Kopytoff |August 18, 2020 |Fortune 

Sam watches her fall apart, tear herself apart and is desperate. Grief: The Real Monster in The Babadook |Tim Teeman |December 19, 2014 |DAILY BEAST 

And then he went on a tear in early 2013, creating one provocation after another, seemingly every day for more than two months. Kim Jong Un’s Kid Gloves Are Now Off |Gordon G. Chang |December 17, 2014 |DAILY BEAST 

As Kate was driven away, she appeared to wipe a tear from her eye. Tearful Kate Weeps After Meeting Mother Whose Baby Died |Tom Sykes |November 25, 2014 |DAILY BEAST 

Many of those gathering in the run-up to the grand jury decision wore hockey and tear gas masks to conceal their identity. Justice Was Served in Ferguson—This Isn’t Jim Crow America |Ron Christie |November 25, 2014 |DAILY BEAST 

In other news, black and white pin-up shots are now officially less valuable than life-size Tiger Beat tear outs. Justin Bieber Isn’t Even 21, Yet Makes More Money Than Meryl Streep |Amy Zimmerman |November 25, 2014 |DAILY BEAST 

Louis turned at the exclamation, and looked on the faithful servant; but no tear was in his eye, no sound on his lip. The Pastor's Fire-side Vol. 3 of 4 |Jane Porter 

By what word is the relation between “pendulum” and “a smile and tear” described? Assimilative Memory |Marcus Dwight Larrowe (AKA Prof. A. Loisette) 

In the tear-stained story of humanity there has never been aught to surpass the thrilling record of Cawnpore. The Red Year |Louis Tracy 

In a short time you will be able, in the language of Dr. Johnson, “to tear out the heart of any book.” Assimilative Memory |Marcus Dwight Larrowe (AKA Prof. A. Loisette) 

The horizon, however, was lowering and hazy, and the sun had not force enough to tear the murky veil asunder. A Woman's Journey Round the World |Ida Pfeiffer