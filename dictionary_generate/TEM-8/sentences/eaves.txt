Every roof includes long eaves to protect peasants from the frequent rains. The Road to Cinco de Mayo |Tania Lara |May 5, 2014 |DAILY BEAST 

All the writing is done at home, in my bedroom, up under the eaves of the house. How I Write: Jane Goodall |Noah Charney |April 9, 2014 |DAILY BEAST 

Eaves, who has been a cruise passenger in the past, says it is his “duty to the future” to be a catalyst for change. How Much Are the ‘Costa Concordia’ Passengers Entitled to Win—and Who Is Accountable for the Shipwreck? |Barbie Latza Nadeau |February 8, 2012 |DAILY BEAST 

Your old homestead is the loveliest place around, with its deep eaves and dormer-windows and vines. Tessa Wadsworth's Discipline |Jennie M. Drinkwater 

Husky crows awake in the pine trees, and doves under the temple eaves. The Dragon Painter |Mary McNeil Fenollosa 

They climbed a ladder and found the barn-swallow's nest plastered under the eaves of the barn. Seven O'Clock Stories |Robert Gordon Anderson 

So still was the place that the caged cricket hanging from the eaves of Um's distant room beat time like an elfin metronome. The Dragon Painter |Mary McNeil Fenollosa 

The gallery above is an open eaves gallery like those in north Italy. Encyclopaedia Britannica, 11th Edition, Volume 5, Slice 2 |Various