She’s since taken a diplomatic approach to this absolute farce, apologizing to her fans and sponsors, explaining her side to reporters, and doing everything she can to make sure she will get to race again. It’s Insane to Boot Sha’Carri Richardson from the Olympics for Marijuana |Corbin Smith |July 2, 2021 |The Daily Beast 

“It’s a clown-car farce,” says Terry Goddard, a former Democratic attorney general of Arizona. The mess in Maricopa |Dan Zak |May 21, 2021 |Washington Post 

Calling it a “philanthropic investment” is a farce — the donor gets every penny back. The movement to privatize public schools marches on during coronavirus pandemic |Valerie Strauss |May 20, 2021 |Washington Post 

If Another Round had been presented as a farce, a trifle, it might, paradoxically enough, carry more weight. Mads Mikkelson Can Do Anything. But He's Stuck in the Boring Oscar-Nominated Another Round |Stephanie Zacharek |March 19, 2021 |Time 

Coward, one of the slyest wits of his century, took the idea of pining for a loved one, drained it of all potential sentimentality, and fashioned it into a lively, sophisticated farce. The Ghost of Noël Coward Is Nowhere to Be Found in the Dull Blithe Spirit |Stephanie Zacharek |February 19, 2021 |Time 

The idea that the whole spectacle was merely an extended judicial farce is simply false. Iraqi Insurgents Circulate the Lie That They Killed the Judge in Saddam’s Trial |Michael Newton |June 28, 2014 |DAILY BEAST 

The government in Kiev has denounced the plebiscite as a “criminal farce.” Inside Putin's Rigged Ukraine Election |Jamie Dettmer |May 12, 2014 |DAILY BEAST 

The Muppets Most Wanted might be the best puppet-led musical mystery caper farce that Hollywood has ever produced. ‘Muppets Most Wanted’ Is a Perfect (Utterly Silly) Muppet Movie |Kevin Fallon |March 21, 2014 |DAILY BEAST 

Unless, of course, everything he told us is “a big farce” to lead challengers off the scent. ESPN’s Bracket Champion Shares His March Madness Secrets |Ben Teitelbaum |March 18, 2014 |DAILY BEAST 

History repeats itself,” Marx famously wrote, “first as tragedy, second as farce. They’ve Always Been Watching You |Nick Gillespie |January 9, 2014 |DAILY BEAST 

Here was one cause of his disinclination to meet his wife—having to keep up the farce of Dr. Ashton's action. Elster's Folly |Mrs. Henry Wood 

Another celebrated spiritual farce was enacted in 1810, entitled "The Sampford Ghost." Second Edition of A Discovery Concerning Ghosts |George Cruikshank 

After all, she could return his present when the farce was over, and she was in a mood to have the world poured into her lap. Ancestors |Gertrude Atherton 

He had told Ruby he would neither give his clothes nor money to the farce, and he prided himself on never going back on his word. The Cromptons |Mary J. Holmes 

That done, there being nothing pleasant but the foolery of the farce, we went home. Diary of Samuel Pepys, Complete |Samuel Pepys