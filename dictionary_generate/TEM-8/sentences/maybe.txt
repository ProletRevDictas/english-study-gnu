Palm-sized disks, they’re the same weight as styrofoam with a scant sprinkle of flavor crystals, salt or maybe cinnamon, dusting the top and coating the crevices between each grain of puffy rice. The Rise and Fall of the Rice Cake, America’s One-Time Favorite Health Snack |Brenna Houck |September 17, 2020 |Eater 

They started off the investigation, trying to brush it off as maybe it’s just another case. Tijuana authorities criticized over handling of transgender woman’s murder |Michael K. Lavers |September 17, 2020 |Washington Blade 

There’s a few new sensors, and the software has made strides, but maybe the best new feature is the line of new one-piece watchbands Apple calls the Solo Loop. Apple Watch Series 6 first impressions: A stretchy addition looks great |Aaron Pressman |September 17, 2020 |Fortune 

Between the 1970s and today, for example, the number of wild Atlantic salmon out there has been cut in half or maybe more, going from 8 to 10 million to just 3 to 4 million. This Startup Is Growing Sushi-Grade Salmon From Cells in a Lab |Vanessa Bates Ramirez |September 16, 2020 |Singularity Hub 

That’s good when you control the game script but maybe not against tougher opponents. We Knew A Football Team Would Win In Week 1. But Maybe Not ‘Football Team.’ |Sara Ziegler (sara.ziegler@fivethirtyeight.com) |September 14, 2020 |FiveThirtyEight 

“Our first few months we had maybe one client and then we went on The Tyra Banks Show,” says James. Career-Minded Women Turn to Male Escorts For No-Strings Fun and (Maybe) Sex |Aurora Snow |January 3, 2015 |DAILY BEAST 

Except for maybe his brainiac wife… but she could do better anyway. All Your Internet Boyfriends Are Taken: Gosling, Cumberbatch, and now Joseph Gordon-Levitt |Melissa Leon |January 3, 2015 |DAILY BEAST 

But maybe you have to start somewhere else — with Lamont Waltman Marvin, Monty, his father, the Chief, the old man. The Story Behind Lee Marvin’s Liberty Valance Smile |Robert Ward |January 3, 2015 |DAILY BEAST 

I was out, maybe in the Great Barrier Reef catching black marlin. The Story Behind Lee Marvin’s Liberty Valance Smile |Robert Ward |January 3, 2015 |DAILY BEAST 

Who knew that “we shall overcome” meant “we, the few, shall book covers every decade or so, maybe, sometimes, if we are in style.” One Vogue Cover Doesn’t Solve Fashion’s Big Race Problem |Danielle Belton |January 2, 2015 |DAILY BEAST 

Maybe it didn't feel good to be on the hurricane deck of a good horse once more! Raw Gold |Bertrand W. Sinclair 

Maybe you never stopped to think that none of our family have necks—so far as you can notice. The Tale of Grandfather Mole |Arthur Scott Bailey 

Few knew—in fact maybe only one other, and that was her husband—or appreciated how much that false testimony had cost her. The Homesteader |Oscar Micheaux 

Some time this summer we are going to get up a nice crowd and sail as far as Bar Harbor—maybe. The Campfire Girls of Roselawn |Margaret Penrose 

“Well, Hen knows how to kill snakes, but maybe she is a poor judge of character,” laughed Amy. The Campfire Girls of Roselawn |Margaret Penrose