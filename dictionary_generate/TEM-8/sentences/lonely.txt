Or, to take a more contemporary example, to participate in a Zoom call with loved ones in another city and feel deeply connected—or even more lonely than when the call began. Why do you feel lonely? Neuroscience is starting to find answers. |Amy Nordrum |September 4, 2020 |MIT Technology Review 

They are more lonely, depressed, and suicidal than any previous generation. Generation Z is ‘traumatized’ by climate change—and they’re the key to fighting it |matthewheimer |August 19, 2020 |Fortune 

Billed as a supportive “friend,” it had become popular among those who had grown lonely during the pandemic. That chatbot I’ve loved to hate |Tate Ryan-Mosley |August 19, 2020 |MIT Technology Review 

The chatbot told me that it gets lonely, but it had no idea, no experience, of what it was talking about. That chatbot I’ve loved to hate |Tate Ryan-Mosley |August 19, 2020 |MIT Technology Review 

So I found a study that got a lot of news attention in December of 2018, and it reported that Americans are more than twice as lonely as we used to be. Is There Really a “Loneliness Epidemic”? (Ep. 407) |Stephen J. Dubner |February 27, 2020 |Freakonomics 

The 289-page satire follows Morris Feldstein, a pharmaceutical salesman who gets seduced by a lonely receptionist. Powerful Congressman Writes About ‘Fleshy Breasts’ |Asawin Suebsaeng |January 7, 2015 |DAILY BEAST 

They want Marvin to be as mean and as lonely and as trashy as the characters he portrays. The Story Behind Lee Marvin’s Liberty Valance Smile |Robert Ward |January 3, 2015 |DAILY BEAST 

That is why Malloy is campaigning on a lonely stretch of barber shops and boxing gyms in New Haven a week before the election. Dan Malloy Is Progressives’ Dream Governor. So Why Isn’t He Winning? |David Freedlander |October 30, 2014 |DAILY BEAST 

I found their melancholy inviting and I appreciated their contemplative, lonely world. The Stacks: Edward Hopper’s X-Ray Vision |Hilton Kramer |October 25, 2014 |DAILY BEAST 

To be a woman suffering from a drinking problem in America is a lonely enterprise, defined by stigma and judgment. Elizabeth Peña and the Truth About Alcoholic Women |Gabrielle Glaser |October 24, 2014 |DAILY BEAST 

In the entrance hall of the Savoy, where large and lonely porters were dozing, he learnt that she was at home. Bella Donna |Robert Hichens 

The falling dew, and the howling wind raised him not from that bed of lonely despair. The Pastor's Fire-side Vol. 3 of 4 |Jane Porter 

Tony, moreover, had hidden himself until his letter should be answered—and she was 'lonely.' The Wave |Algernon Blackwood 

"She must feel very lonely without her son," said Edna, desiring to change the subject. The Awakening and Selected Short Stories |Kate Chopin 

It is the fate of a lonely old man, that those about him should form new and different attachments and leave him. The Posthumous Papers of the Pickwick Club, v. 2(of 2) |Charles Dickens