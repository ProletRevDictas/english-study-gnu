Some environmental groups, however, have criticized Newsom for his approach to the oil industry, saying he has failed to live up to his rhetoric. Gov. Gavin Newsom Says California Is Cracking Down on Oil Spills. But Our Reporting Shows Many Are Still Flowing. |by Janet Wilson, The Desert Sun |September 24, 2020 |ProPublica 

So there are clear discrepancies here between the rhetoric and the action. If China plans to go carbon neutral by 2060, why is it building so many coal plants? |James Temple |September 23, 2020 |MIT Technology Review 

This can certainly change, particularly after a close race drenched with vituperative rhetoric. Most Americans understand that we probably won’t see a winner on election night |Philip Bump |September 23, 2020 |Washington Post 

This prompted congressional investigations, lawsuits, a lot of political rhetoric, and even more public worry about whether the disruptions pose a threat to what will likely be the most mail-reliant election in history. How The Post Office Became A Political Football |Kaleigh Rogers (kaleigh.rogers@fivethirtyeight.com) |September 21, 2020 |FiveThirtyEight 

This resulting chaos is reflected in divisive rhetoric and burning cities, in militarized citizenry and dictatorial echo chambers. The American Fringes Go Mainstream |Nick Fouriezos |September 6, 2020 |Ozy 

“You try to always scratch where the itch is,” Huckabee said about his campaigning and rhetoric in the 2008 primary. Why This Liberal Hearts Huckabee |Sally Kohn |January 6, 2015 |DAILY BEAST 

He has struck a promising tone these last few days with his rhetoric about trying to “see each other.” Memo to Cops: Criticisms Aren’t Attacks |Michael Tomasky |December 28, 2014 |DAILY BEAST 

Francis is well into his seventies, looks it, has a mild demeanor and soft speaking style; but his rhetoric is electrifying. How Pope Francis Became the World’s BFF |Jason Berry |December 21, 2014 |DAILY BEAST 

In return, Cuban rhetoric wholeheartedly blamed the United States for crippling their economy. Cuba Is A Kleptocracy, Not Communist |Romina Ruiz-Goiriena |December 19, 2014 |DAILY BEAST 

I saw it first hand during the conflict in Gaza this summer when friendships ended as the conflict and the rhetoric heated up. Muslims & Jews Unite vs. Abercrombie & Fitch |Dean Obeidallah |December 16, 2014 |DAILY BEAST 

He went as far as rhetoric, at school, and was then put in a bank by his aunt, Jacqueline Collin. Repertory Of The Comedie Humaine, Complete, A -- Z |Anatole Cerfberr and Jules Franois Christophe 

It is only,” replies the friar, “to grace and adorn my speech; it is the colour of a Ciceronian rhetoric. A Cursory History of Swearing |Julian Sharman 

In Athens, rhetoric, mathematics, and natural history supplanted rhapsodies and speculations on God and Providence. Beacon Lights of History, Volume I |John Lord 

Rhetoric became connected with dialectics, and in Greece, Sicily, and Italy both were extensively cultivated. Beacon Lights of History, Volume I |John Lord 

The men were mystified, but wine and rhetoric had fired them, and they cheered him—no one knew why. When Valmond Came to Pontiac, Complete |Gilbert Parker