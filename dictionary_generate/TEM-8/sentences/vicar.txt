I heard a story the other day of a neighbor of mine, who was at a dinner party with the local vicar, who is an extremely nice man. Rediscovering Richard Dawkins: An Interview |J.P. O’Malley |September 23, 2013 |DAILY BEAST 

This woman attacked the vicar throughout the dinner party, for not being Christian enough. Rediscovering Richard Dawkins: An Interview |J.P. O’Malley |September 23, 2013 |DAILY BEAST 

It's too early to say how the new vicar of Rome will fare in these hostile times. ‘Viva Papa Francisco!’ Brazil Celebrates Pope’s Trip |Mac Margolis |July 23, 2013 |DAILY BEAST 

After all of the adventures at Hogwarts, Rowling may be saying, all you want to do is snuggle up with a cup of tea and a vicar. Writing For Teens Vs. Adults: Rowling As Case Study |Seth Lerer |October 3, 2012 |DAILY BEAST 

When Barack Obama tried to shush "loose talk of war," he got as much traction as a vicar giving a sermon during a soccer riot. Please Shut Up |Gershom Gorenberg |March 12, 2012 |DAILY BEAST 

But Mrs. Dodd, the present vicar's wife, retained the precious prerogative of choosing the book to be read at the monthly Dorcas. The Pit Town Coronet, Volume I (of 3) |Charles James Wills 

The living (value £250) is in the gift of trustees, and is now held by the Rev. M. Parker, Vicar. Showell's Dictionary of Birmingham |Thomas T. Harman and Walter Showell 

Conny stepped smilingly forward, and proceeded to affix the band around the vicar's massive throat. The Pit Town Coronet, Volume I (of 3) |Charles James Wills 

The vicar's wife, still slightly discomposed, launched out into some parochial matter she had wished to mention to him. Uncanny Tales |Various 

He was released soon after Christmas, and another Vicar filleth his place. Showell's Dictionary of Birmingham |Thomas T. Harman and Walter Showell