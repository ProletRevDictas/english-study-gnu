That dovetails with other signs of excessive market risk-taking, stoked in part by the Federal Reserve’s easy monetary policy. Who will pop the GameStop bubble? |Roya Wolverson |February 1, 2021 |Quartz 

He might not have intended for his supporters to storm the building, but he recklessly stoked a throng of people who did just that — and five deaths occurred. Pelosi once again plays politics with impeachment |Marc Thiessen |January 12, 2021 |Washington Post 

After months of stoking anger about alleged election fraud, one of America’s largest talk-radio companies has decided on an abrupt change of direction. Talk-radio owner orders conservative hosts to temper election fraud rhetoric |Paul Farhi |January 11, 2021 |Washington Post 

Fifty years after this passenger-train service debuted to connect New York and New Orleans, the arrival of the Crescent can still stoke a sense of excitement at its 31 stops, including Tuscaloosa. Aboard Amtrak’s Crescent, surprising comfort and welcome seclusion on a slow train to Mississippi |Scott Butterworth |January 1, 2021 |Washington Post 

Fears about our health, finances and family, all stoked by covid, have really done a number on the psyche. Hints From Heloise: Tips for mental health |Heloise Heloise |December 26, 2020 |Washington Post 

Comments like that are designed to stoke the fires of fan-passion—and it works beautifully. All Your Internet Boyfriends Are Taken: Gosling, Cumberbatch, and now Joseph Gordon-Levitt |Melissa Leon |January 3, 2015 |DAILY BEAST 

So much of the fear the media tries to stoke in me is fear of the oppressed underdog lashing out. Of Gamers, Gates, and Disco Demolition: The Roots of Reactionary Rage |Arthur Chu |October 16, 2014 |DAILY BEAST 

But also just as the news media plays to or even inflames such fears to drive ratings, Republicans stoke fear to drive votes. Ebola Scare-Mongerer Rand Paul Wants You to Think You’re Going to Die |Sally Kohn |October 12, 2014 |DAILY BEAST 

There are people who intentionally stoke the flames of hate against our community. 13 Years After 9/11, Anti-Muslim Bigotry Is Worse Than Ever |Dean Obeidallah |September 11, 2014 |DAILY BEAST 

For Live Another Day, did you make a concerted effort to not stoke those fires? ‘24: Live Another Day’ Showrunners on the Finale, the Dangers of Drones, and Jack Bauer’s Future |Marlow Stern |July 15, 2014 |DAILY BEAST 

Then, the most care-free creature in the world, he stole down the stone passage into the wilderness of Beverly Stoke. The Joyous Adventures of Aristide Pujol |William J. Locke 

So Aristide, in his childlike way, found remarkable happiness in Beverly Stoke. The Joyous Adventures of Aristide Pujol |William J. Locke 

Tree is less vigorous than Stoke and more subject to blight. Northern Nut Growers Association Thirty-Fourth Annual Report 1943 |Various 

Considerable litigation occurred at various periods with reference to the parochial possessions, especially those at Severn Stoke. Notes and Queries for Worcestershire |John Noake 

Day was breaking, but the toilers down in the depths of the stoke hole could not see the coming of the day. The Iron Boys on the Ore Boats |James R. Mears