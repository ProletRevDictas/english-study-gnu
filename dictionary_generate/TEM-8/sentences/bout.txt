Putin is certainly unlikely to be dislodged by this bout of unrest. 'The Whole System Needs Changing.' The Russia Protests Are About More Than Just Alexei Navalny |Madeline Roache |February 5, 2021 |Time 

Depending on location, the shot was only 72 percent to 57 percent effective against moderate to severe bouts of the illness. One-shot COVID-19 vaccine is effective against severe disease |Tina Hesman Saey |January 29, 2021 |Science News 

Now as an adult, making and eating this dish provides an anchor to my Trinbagonian heritage and alleviates the bouts of homesickness that are most acute during these punishing winter months. Trinidad-style aloo and channa infuses an Indian classic with Caribbean flavor |Brigid Washington |January 22, 2021 |Washington Post 

As her only child, Antara embraces the responsibility of caring for her with a determination threaded with resentment and even bouts of suspicion. ‘Burnt Sugar,’ a challenging Booker Prize finalist, is hard to take, but harder to shake off |Ron Charles |January 19, 2021 |Washington Post 

My brother, who had a bout with pneumonia in the past year, did not leave Boston. Tallying up a year of loss: A lot of pounds, too many loved ones, countless connections |Jerry Brewer |December 27, 2020 |Washington Post 

A “simple bout of flu” is incapable of mutating into an Ebola infection. The Sham, Scaremongering Guide to Ebola |Abby Haglage |November 20, 2014 |DAILY BEAST 

Regardless of how one wrestles with Noam Chomsky, one does always wrestle, leaving the bout much smarter and stronger. Noam Chomsky—Infuriating and Necessary |David Masciotra |September 28, 2014 |DAILY BEAST 

Omran managed to finish high school despite his bout with cancer and two wars in three years. Beating Cancer & Dodging Israel's Bombs |Itay Hod |September 1, 2014 |DAILY BEAST 

Then he pauses and looks up, doing a quick bout of surveying in his head. John Lithgow and Alfred Molina's 'Love Is Strange' Is the Love Story of the Year |Kevin Fallon |August 20, 2014 |DAILY BEAST 

A bout with a life-threatening—though not life-ending—illness is always good awards bait. 'SNL' Star Kate McKinnon's Big, 'Awesome,' Emmy-Nominated Year |Kevin Fallon |August 19, 2014 |DAILY BEAST 

I don't b'lieve nothin', and try to keep a close mouth 'bout what I do b'lieve. The Cromptons |Mary J. Holmes 

Whales, sir—a whole school of 'em—off the star-b'd quarter 'bout five miles away—big ones! Fifty Contemporary One-Act Plays |Various 

Sometimes dey'd pull up at de sho' en take a res' b'fo' dey started acrost, so by de talk I got to know all 'bout de killin'. Adventures of Huckleberry Finn, Complete |Mark Twain (Samuel Clemens) 

Strange to see what a deal of money is flung to them both upon the stage between every bout. Diary of Samuel Pepys, Complete |Samuel Pepys 

But you take a man dat's got 'bout five million chillen runnin' roun' de house, en it's diffunt. Adventures of Huckleberry Finn, Complete |Mark Twain (Samuel Clemens)