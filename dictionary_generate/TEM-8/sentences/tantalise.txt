Once a day a small ration was doled out--pitifully small--enough to tantalise appetite, but not to still hunger. The Gaunt Gray Wolf |Dillon Wallace 

The red wine in the tall glasses, the cakes and fruit, tantalise a hungry man who stares at them through the glass. The Lure of Old London |Sophie Cole 

He could pull an oar with the strongest, and on the baseball field could tantalise a crack batsman with cranky balls. The Long Patrol |H. A. Cody 

The obstacles in his way, however, seemed to increase as circumstances combined to fret and tantalise his hopes. Banked Fires |E. W. (Ethel Winifred) Savi 

Of course the deer or antelopes sprang out of the shrubbery or scoured across the plain only to tantalise them. The Hunters' Feast |Mayne Reid