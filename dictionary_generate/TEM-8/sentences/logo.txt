The brand logo turned out to feature a graceful archer on horseback, in a Tatar national costume, poised to shoot his arrow. Rebranding The Land of Mongol Warriors & Ivan The Terrible |Anna Nemtsova |December 25, 2014 |DAILY BEAST 

An older bro wore a red bow tie and a yarmulke emblazoned with the “TEAM MITCH” logo as he stared down at his smartphone intently. Mitch’s Brotastic Victory Bash |Olivia Nuzzi |November 5, 2014 |DAILY BEAST 

“Font, logo, edge finish, surface finish … everything is different from ours,” said Sung Hwang, the general manager. Patients Screwed in Spine Surgery ‘Scam’ |The Center for Investigative Reporting |November 3, 2014 |DAILY BEAST 

And this was the first time that after seeing that Marvel logo, you were introduced to all new people. The Leaner, Meaner Season 2 of ‘Marvel’s Agents of S.H.I.E.L.D.’ |Jason Lynch |September 22, 2014 |DAILY BEAST 

I realized when he sat down that he had made his T-shirt logo with a pen. A Gay American Artist in Kaiser’s Berlin |Sarah Bay Williams |August 10, 2014 |DAILY BEAST 

Besides, she generally called him 'Logo,' as all his friends did. The Diva's Ruby |F. Marion Crawford 

I told him that I would not do anything to make trouble between you and Logo. The Diva's Ruby |F. Marion Crawford 

So's Logo, for that matter, but she doesn't think a great deal of Greeks. The Diva's Ruby |F. Marion Crawford 

If the girl's the party, Logo beats the band for brass, that's all I can say!' The Diva's Ruby |F. Marion Crawford 

Uncaptioned illustrations are decorative Headpieces or the publisher's logo on the Title page. A History of Art for Beginners and Students |Clara Erskine Clement