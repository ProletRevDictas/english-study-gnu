Running back Derrius Guice was released after he was arrested on domestic violence-related charges, including allegedly strangling his girlfriend until she was unconscious. Washington football timeline: From Ron Rivera hiring to playoff exit |Sam Fortier |January 12, 2021 |Washington Post 

Ninety-five percent of all life on Earth died—strangled by an atmosphere so low in oxygen. Why Birds Can Fly Over Mount Everest - Issue 94: Evolving |Walter Murch |December 30, 2020 |Nautilus 

He strangled her with a dog leash in the basement den of his parents’ home, then stolen his mother’s car. My Friend, the Murderer |Eugene Robinson |November 24, 2020 |Ozy 

The move strangled the flow of traffic to Springer’s websites, so it relented, giving Google a temporary license to use snippets as it wished. Why Google’s $1 billion deal with news publishers isn’t the end of their war |David Meyer |October 1, 2020 |Fortune 

“Dennis was so intoxicated and violent he tried to strangle my mom,” she said. Her Stepfather Admitted to Sexually Abusing Her. That Wasn’t Enough to Keep Her Safe. |by Nadia Sussman |September 18, 2020 |ProPublica 

As you can see on your screens, this young soldier is trying to strangle me with the barrel of his carbine. This 1979 Novel Predicted Putin’s Invasion Of Crimea |Michael Weiss |May 18, 2014 |DAILY BEAST 

If he refuses to strangle his own baby in the crib, Republicans are happy to retaliate. The GOP Is Threatening Murder-Suicide With New Shutdown Warnings |Kirsten Powers |September 19, 2013 |DAILY BEAST 

They drove to his home in South Los Angeles and had sex but shortly thereafter, he attempted to strangle her with a shoelace. Los Angeles Police Pin Old Murders of Three Women on Dead Serial Killer |Christine Pelisek |August 3, 2012 |DAILY BEAST 

Bulger attempted to strangle McIntyre with a rope and, when that failed, he shot McIntyre in the head multiple times. Whitey Bulger's Victim Speaks Out Against FBI |Rikki Klieman |November 18, 2011 |DAILY BEAST 

"Mark tried to strangle me last night," my friend blurted out. Husbands Who Bring the War Home |Stacy Bannerman |September 25, 2010 |DAILY BEAST 

Yet, try as I would to strangle the idea, all through the evening the same horrible, unaccountable notion clung to me. Uncanny Tales |Various 

Wherefore it was but natural that President Castle's experts found it impossible to strangle the bill in committee. Scattergood Baines |Clarence Budington Kelland 

Bow traps are placed along the tracks of civets, ichneumons, and rodents, which snap and strangle them. Man And His Ancestor |Charles Morris 

He was of the type of those who strangle serpents while yet in the cradle. Critical Miscellanies (Vol. 2 of 3) |John Morley 

He was an angel on earth—my God, if ever I lay me hands on that woman, I'll strangle her. The Daffodil Mystery |Edgar Wallace