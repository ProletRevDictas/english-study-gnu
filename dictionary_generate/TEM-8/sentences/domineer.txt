Is it not evident that the desire to domineer over men is the essence of their profession? Superstition In All Ages (1732) |Jean Meslier 

You will domineer over her, and desire to have your own way. An Old Man's Love |Anthony Trollope 

They looked upon the people not as the flock of God, but only as their own to spoil, misuse and domineer over. The Prophet Ezekiel |Arno C. Gaebelein 

The Osseous is inclined to dominate and often to domineer over his mate and over his family in general. How to Analyze People on Sight |Elsie Lincoln Benedict and Ralph Paine Benedict 

However, that's just where the disadvantage comes in—he's too much inclined to domineer. The Boss of Taroomba |E. W. Hornung