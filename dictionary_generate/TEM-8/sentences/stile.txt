There was also a statement by a parent, Francis Stile, whose two daughters attended the school. The First Modern School Shooter Feels Responsible for the Rest |Michael Daly |May 30, 2014 |DAILY BEAST 

As there was a stile near, leading into a field, they all got over the stile, and thus passed the geese. The Nursery, July 1873, Vol. XIV. No. 1 |Various 

As she proceeded up the lane, she paused at the stile where she and Gilbert had held their last conversation. The World Before Them |Susanna Moodie 

Thinking it might be only kind to step into the clerk's, he crossed the stile and went in without ceremony by the open back-door. Elster's Folly |Mrs. Henry Wood 

After dinner we all went to the Church stile, and there eat and drank, and I was as merry as I could counterfeit myself to be. Diary of Samuel Pepys, Complete |Samuel Pepys 

He left some money when he went away, and one of the children saw him cross the stile into the next field. A Thin Ghost and Others |M. R. (Montague Rhodes) James