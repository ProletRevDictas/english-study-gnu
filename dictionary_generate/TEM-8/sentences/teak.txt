Choose from a bio ethanol or propane power, and a table made of teak or composite concrete for a look that’s just right for your outdoor decor. The 8 Best Outdoor Fire Pits for S’mores, Cuddling, and More |Korin Miller |August 27, 2021 |The Daily Beast 

While it may seem like you’re dropping a pretty penny, your patio setup will last longer if it’s made of iron, aluminum, or teak. Best patio furniture: outdoor seating that suits your space |Billy Cadden |July 20, 2021 |Popular-Science 

Here, table, highboy chairs, and screen—mahogany, cherry, teak—looked nervous and disproportioned. Marco Roth’s Book Bag: The Anti-Memoir Memoir |Marco Roth |October 1, 2013 |DAILY BEAST 

Hilda stabbed a great crisp fallen teak leaf with her parasol, and spent the grimness of this in twirling it. Hilda |Sarah Jeanette Duncan 

From under the teak table Umballa drew forth two heavy bags of silver coin. The Adventures of Kathlyn |Harold MacGrath 

African teak, let me note, is not teak properly so called, but the timber of the Oldfieldia Africana. The Desert World |Arthur Mangin 

One of the pleasures of being "on the road to Mandalay" was to see the-- "Elephints a-pilin' teak In the sludgy, squdgy creek" Where Half The World Is Waking Up |Clarence Poe 

In Siam the elephants are much used in managing the immense rafts of teak trees that are floated down the rivers for export. Where Half The World Is Waking Up |Clarence Poe