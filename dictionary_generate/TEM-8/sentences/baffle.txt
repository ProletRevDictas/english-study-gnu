To their surprise, however, one snake “formed its body in a ring around the baffle and then started to wiggle up,” Savidge says. These snakes wiggle up smooth poles by turning their bodies into ‘lassoes’ |Kate Baggaley |January 13, 2021 |Popular-Science 

One of these ways involved tests to see whether a wide pipe, or baffle, around a pole could prevent predators from reaching a starling nest box at the top. Brown tree snakes use their tails as lassos to climb wide trees |Maria Temming |January 11, 2021 |Science News 

With their help, I’d made progress toward a prototype for my tiny satellites, inventing and testing precision-pointing hardware and software, and perfecting the design of the onboard telescope and its protective baffle. My satellite would fit in a small suitcase. |Katie McLean |December 18, 2020 |MIT Technology Review 

These baffles “hug” the microphone and block out external noise while focusing on the source and enhancing the quality of final audio, making them an exceptional tool for recording vocals, kick drums, guitar amps and more. The best recording isolation shields for tracking audio in any space |PopSci Commerce Team |September 25, 2020 |Popular-Science 

The glass is lined with custom baffles contoured to block daytime glare. Apple’s ‘most ambitious’ new store is a departure from its signature design |claychandler |September 8, 2020 |Fortune 

This hidden Eden continues to baffle geologists with its oculus of volcanic stone. What Made Mexico’s Most Mysterious Beach? |Brandon Presser |October 14, 2014 |DAILY BEAST 

That decision continues to baffle some law enforcement officials nearly two decades later. New Revelations in JonBenet’s Unsolved Death |Carol McKinley |October 25, 2013 |DAILY BEAST 

With only two young witnesses, no clear motive, and no identified suspect, the case continues to baffle authorities. Alps Murder Victims Pasts Probed by Cops |Barbie Latza Nadeau |September 22, 2012 |DAILY BEAST 

It was no good to set broad goals for agencies: those goals tended to contradict each other—or else to baffle the agency itself. James Q. Wilson, 1931-2012 |David Frum |March 3, 2012 |DAILY BEAST 

The singer-actress admits the debate continues to offend and baffle her. Jill Scott’s Soul Searching |Allison Samuels |June 22, 2011 |DAILY BEAST 

It was Carmena, every nerve of her loyal nature on the alert to baffle this pursuer of Alessandro and Ramona. Ramona |Helen Hunt Jackson 

We went from office to office, where everything seemed designed to baffle suitors conducting their own cases. Reminiscences of Charles Bradlaugh |George W. Foote 

Its limitless deeps reveal themselves to us, and yet baffle our gaze: close themselves against research, but open to conjecture. Toilers of the Sea |Victor Hugo 

Thanks to the darkness and the water, you baffle the hounds, both animal and human. The Duty of Disobedience to the Fugitive Slave Act |Lydia Maria Child 

Both could blight the promise of the harvest, baffle the plans of their enemies, or wither the health of their victims. Witch, Warlock, and Magician |William Henry Davenport Adams