Perry’s latest, “Eddie’s Boy,” circles back to that early masterpiece, bringing a long-gestating narrative to an elegant and satisfying conclusion. ‘Eddie’s Boy,’ by Thomas Perry, continues the saga of a retired hit man who can’t escape his past |Bill Sheehan |November 29, 2020 |Washington Post 

Plus, he’s a harpist who not only regularly performs classical masterpieces but also excels in jazz, from gentle to almost avant garde. A New Vision for America |Daniel Malloy |November 29, 2020 |Ozy 

Crispy kale against silky onion, stretchy cheese and a toothsome crust makes for a textural masterpiece. 6 golden recipes featuring caramelized onions |Kari Sonde |October 29, 2020 |Washington Post 

One of these designers, Lafalaise Dion, is the brains behind the cowrie shell pieces sprinkled throughout the cinematic masterpiece. Lafalaise Dion Takes ESSENCE On A Personal Journey |Nandi Howard |September 4, 2020 |Essence.com 

The electric crepe maker from Chefman is a 12-inch, multifunctional masterpiece. Crepe pans you’ll use for breakfast, lunch, and dinner |PopSci Commerce Team |September 3, 2020 |Popular-Science 

The masterpiece is huge, but structurally flawed and terribly vulnerable to seismic activity. Florence Preps ‘David’ for the Big One |Barbie Latza Nadeau |December 25, 2014 |DAILY BEAST 

In the masterpiece Mr. Turner, Mike Leigh translates the soul of his characters on screen. Mike Leigh Is the Master Filmmaker Who Hates Hollywood |Nico Hines |October 14, 2014 |DAILY BEAST 

The loftiest noses among the winemakers inhale the finest of scents from the tasting glass and proclaim another masterpiece. Napa’s Earthquake Is Not The Only Thing Shaking The Vineyards |Clive Irving |August 31, 2014 |DAILY BEAST 

Two decades on from his masterpiece on anti-Americanism abroad, Whit Stillman remains obsessed with native-expat dynamics. Whit Stillman on the 20th Anniversary of ‘Barcelona’, His New Amazon Series, and the Myth of the Ugly Expat |Michael Weiss |August 10, 2014 |DAILY BEAST 

Think twice about filming that two-minute masterpiece and making yourself the next Paris Hilton (if you're lucky). So You Want to be a Porn Star? Inside the Sex Tape Phenomenon |Aurora Snow |July 19, 2014 |DAILY BEAST 

A few days later, during the general advance, he captured Naarden, the masterpiece of the great engineer Cohorn. Napoleon's Marshals |R. P. Dunn-Pattison 

Another “bon garçon”—a painter whose enthusiasm for his art knew no bounds—craved to produce a masterpiece. The Real Latin Quarter |F. Berkeley Smith 

This defence was considered at the time a masterpiece of bravery; the Indians being estimated four to one of the English. The Every Day Book of History and Chronology |Joel Munsell 

A marble lattice-work, six feet high, surrounding the two sarcophagi, is a masterpiece of art. A Woman's Journey Round the World |Ida Pfeiffer 

According to my usual opinion, I believe I could go over that book and leave a masterpiece by blotting and no ulterior art. The Works of Robert Louis Stevenson - Swanston Edition Vol. 25 (of 25) |Robert Louis Stevenson