Loeffler dug into his past sermons and church affiliations, most prominently noting that a church with which he was affiliated decades ago had invited Fidel Castro to speak. Four takeaways from Democrats’ big wins in the Georgia Senate runoffs |Amber Phillips |January 6, 2021 |Washington Post 

The ad is spliced with snippets of Warnock yelling during sermons. Raphael Warnock’s campaign for the moral high ground |Clyde McGrady |January 2, 2021 |Washington Post 

The play presents itself as a time-transcending sermon of sorts, as Hamer addresses the modern audience through the gaze of her civil rights era perspective. Arena Stage returns to live performances with an outdoor production of the rousing ‘Fannie Lou Hamer, Speak on It!’ |Thomas Floyd |October 30, 2020 |Washington Post 

He says the couple didn’t appreciate his most recent sermon, which urged Christians to call out and challenge racism anywhere they saw it, including in their own church. Evangelicals are looking for answers online. They’re finding QAnon instead. |Abby Ohlheiser |August 26, 2020 |MIT Technology Review 

Every day a new sermon was delivered to me with a bow on it. When Jesus Freaks Go Bad |Eugene Robinson |August 6, 2020 |Ozy 

A Gaylard Williams Sunday sermon (which lasted for 45 minutes on average) was something to behold. Exposed: The Gay-Bashing Pastor’s Same-Sex Assault |M.L. Nestel |December 21, 2014 |DAILY BEAST 

King says in a sermon a month later at Tabernacle Baptist Church in Selma, Alabama. Tavis Smiley Humanely Chronicles MLK’s Sad Last Year |Scott Porch |October 16, 2014 |DAILY BEAST 

In Germany, a prominent Muslim imam gave a sermon asking Allah to kill all of the “Zionist Jews.” Europe’s Jews Punished for Israel’s War |Eli Lake |July 25, 2014 |DAILY BEAST 

“I agree with what the sermon was and what it was about,” she said. Artist Gives Celebs the ‘Natural’ Look |Justin Jones |July 24, 2014 |DAILY BEAST 

Your sermon this Sunday morning: Is the power of prayer enough to overcome the German defense? Argentina Doesn’t Have A Prayer Today. Lucky Them? |Kevin Bleyer |July 13, 2014 |DAILY BEAST 

I felt just the same when I was married myself; but it's nothing to preaching one's first sermon. The Pit Town Coronet, Volume I (of 3) |Charles James Wills 

The author of the life of St. Francis Xavier, asserts, that "by one sermon he converted ten thousand persons in a desert island." The Book of Anecdotes and Budget of Fun; |Various 

A clergyman observed in his sermon, that this was unpardonable, as people did it with their eyes open. The Book of Anecdotes and Budget of Fun; |Various 

Many years ago, while a clergyman on the coast of Cornwall was in the midst of his sermon, the alarm was given, A wreck! The Book of Anecdotes and Budget of Fun; |Various 

In the churchyard, under a great tree, still standing, John Wesley preached his last open-air sermon. British Highways And Byways From A Motor Car |Thomas D. Murphy