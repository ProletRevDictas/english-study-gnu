They also say the agency is ineffectual when it comes to its other main area of oversight, antitrust and competition, or ensuring market fairness. Kill Your Algorithm: Listen to the new podcast featuring tales from a more fearsome FTC |Kate Kaye |October 22, 2021 |Digiday 

“The reasons for the delay were varied and complex and included fierce competition over finite resources, bitter interpersonal rivalries, and ineffectual scientific management,” the 2019 study in Physics Today noted. Insights into the Nazis’ failed nuclear program may lie within this 2-inch-tall uranium cube |Caroline Anders |August 25, 2021 |Washington Post 

When Superman and Batman doffed their costumes to become Clark Kent or Bruce Wayne, they revealed personae as ineffectual as Don Diego, the ennui-ridden, apolitical dandy who delights in playing parlor tricks with his handkerchief. Zorro at 100: Why the original swashbuckler is still the quintessential American action hero |Michael Sragow |January 1, 2021 |Washington Post 

This model is meaningfully distinct from the constituency statutes in some states that seek to strengthen stakeholder interests, but that stakeholder advocates condemn as ineffectual. 50 years later, Milton Friedman’s shareholder doctrine is dead |jakemeth |September 13, 2020 |Fortune 

The Obama administration has pulled together a coalition as ineffectual as it is unwilling. There’s Only One Way to Beat ISIS: Work With Assad and Iran |Leslie H. Gelb |October 18, 2014 |DAILY BEAST 

The end result only confirmed its image as disorganized and ineffectual. Boko Haram Steps Up Its War on Kids |Nina Strochlic |May 12, 2014 |DAILY BEAST 

Why, then, are we led to believe that her conniving ways are so ineffectual and misdirected? The Abused Wives of Westeros: A Song of Feminism in ‘Game of Thrones’ |Amy Zimmerman |April 30, 2014 |DAILY BEAST 

On Thrones, power, politics, money, and force constantly trump goodness, in each of its ineffectual forms. Game of Thrones’ ‘Breaker of Chains’: The Most Cynical Show on TV Bares Its Fangs Once More |Andrew Romano |April 21, 2014 |DAILY BEAST 

But the White House is sounding more and more defensive and ineffectual. The Second Oldest Profession is Here to Stay |Christopher Dickey |October 25, 2013 |DAILY BEAST 

Who can understand its nature, its operations, the sufficiency which is not sufficient, and the efficacy which is ineffectual. A Philosophical Dictionary, Volume 1 (of 10) |Franois-Marie Arouet (AKA Voltaire) 

We had heard the roar of her guns, and the quick, ineffectual firing from Fort McAllister. The Boys of '61 |Charles Carleton Coffin. 

Gaolers and soldiers, utterly taken aback by this sudden onslaught, made but ineffectual resistance. Jack Harkaway's Boy Tinker Among The Turks |Bracebridge Hemyng 

In some places an ineffectual resistance was made, and several lives lost on both sides. Fox's Book of Martyrs |John Foxe 

A rift in the opposition was started, and an attempt to close it by a conference two days later was ineffectual. The Political History of England - Vol. X. |William Hunt