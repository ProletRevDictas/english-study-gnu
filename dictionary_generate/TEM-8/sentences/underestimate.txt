This is also likely an underestimate of Filipino cases, since many speak English. Filipino Residents Have Been Hit Hard by COVID – But No One Knows Just How Hard |Maya Srikrishnan |February 1, 2021 |Voice of San Diego 

To read the charts below, consider the 0% line as the actual traffic, and any number over 0% as an overestimate by the tool, and anything under as an underestimate. Study: Comparing the data from 8 SEO tools |Jeff Baker |October 7, 2020 |Search Engine Land 

Instead, the country has seen at least 193,000 deaths, a figure that is probably an underestimate. Trump contradicts CDC director on vaccine; Biden says Americans shouldn’t trust Trump |Colby Itkowitz, Felicia Sonmez, John Wagner |September 16, 2020 |Washington Post 

The team noticed similar underestimates in models of the 2019 heat waves in Europe. Climate change drove Australian wildfires to extremes |Carolyn Gramling |April 29, 2020 |Science News For Students 

States also vary in how and where they are performing tests, and some count only proven cases and not also presumptive ones, leading to significant underestimates of the death toll. Failure To Count COVID-19 Nursing Home Deaths Could Dramatically Skew US Numbers |LGBTQ-Editor |April 27, 2020 |No Straight News 

Democrats would be mistaken to underestimate Mike Huckabee, perhaps the strongest Republican presidential contender. Why This Liberal Hearts Huckabee |Sally Kohn |January 6, 2015 |DAILY BEAST 

She really becomes the image of someone you might underestimate or take for granted. How the Circus Got a Social Conscience |Justin Jones |November 7, 2014 |DAILY BEAST 

I think people also underestimate how much we really do write it all in six days. How Aidy Bryant Stealthily Became Your Favorite ‘Saturday Night Live’ Star |Kevin Fallon |October 31, 2014 |DAILY BEAST 

Yet such rankings vastly underestimate the power now being wielded by the San Francisco region. Battle of the Upstarts: Houston vs. San Francisco Bay |Joel Kotkin |October 5, 2014 |DAILY BEAST 

These numbers, although startling, are thought to “vastly underestimate” the reality. CDC Director: First U.S. Ebola Patient ‘Critically Ill’ |Abby Haglage |September 30, 2014 |DAILY BEAST 

Lawyers always underestimate the legal knowledge of an intelligent layman. Encyclopaedia Britannica, 11th Edition, Volume 5, Slice 2 |Various 

"Do not underestimate my intelligence, I understand you," he laughed. The Secret Witness |George Gibbs 

It would however be a serious error to underestimate the value of the earlier work in plant histology. Makers of British Botany; a collection of biographies by living botanists |Various 

It is a pity that the critic is unable to contend with him on such a point without appearing to underestimate that work. Picture and Text |Henry James 

These men represented the batting strength of Place, and Ken, though he did not in the least underestimate them, had no fear. The Young Pitcher |Zane Grey