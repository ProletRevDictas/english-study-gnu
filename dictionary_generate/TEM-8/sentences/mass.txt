The photo shows a large and dense mass of smoke all over the US' West Coast, all the way down from Oregon to Southern California. West Coast wildfire smoke is visible from outer space |María Paula Rubiano A. |September 16, 2020 |Popular-Science 

Exceptional continental record of biotic recovery after the Cretaceous-Paleogene mass extinction. Dawn of the Heliocene - Issue 90: Something Green |Summer Praetorius |September 16, 2020 |Nautilus 

The ambition of the Green Deal will not pan out without mass production. Europe’s leaders want to create a ‘new Bauhaus’ as part of its Green Deal. But what does that even mean? |David Meyer |September 16, 2020 |Fortune 

Unlike many creatures, it survived the massive volcanic eruptions in what’s now Siberia that upset the chemistry of Earth’s atmosphere and oceans and probably triggered the Permian mass extinction about 252 million years ago. Ancient Lystrosaurus tusks may show the oldest signs of a hibernation-like state |Susan Milius |September 16, 2020 |Science News 

Each of its knobby blooms bursts from the soil as a skirted mass of tiny same-sex nubbins. ‘Vampire’ parasite challenges the definition of a plant |Susan Milius |September 16, 2020 |Science News For Students 

Their bodies were later found incinerated and buried in mass graves outside of town. Why Mexicans Are Enraged by Obama’s Big Tuesday Meeting |Ruben Navarrette Jr. |January 6, 2015 |DAILY BEAST 

Beyond the huge American flag that hung over the street, the mile-long mass of cops ended. Funeral Protest Is Too Much for NYPD Union Boss |Michael Daly |January 5, 2015 |DAILY BEAST 

“The United States had gone to war declaring it must destroy an active weapons of mass destruction program,” the Times reported. Political Memes That Absolutely Must Die in 2015 |Asawin Suebsaeng |January 1, 2015 |DAILY BEAST 

Google itself has taken a break and put plans for mass production on hold. You Were Wrong About Miley & Bitcoin: 2014’s Failed Predictions |Nina Strochlic |December 31, 2014 |DAILY BEAST 

A colleague overheard two conservative Mass. lawmakers talking about what “the gays” could do. The Real Story Behind the Fight for Marriage Equality |E.J. Graff |December 30, 2014 |DAILY BEAST 

Charred beams and blackened walls showed stark and gaunt in the glow of a smoldering mass of wreckage. The Red Year |Louis Tracy 

But hitherto, before these new ideas began to spread in our community, the mass of men and women definitely settled down. The Salvaging Of Civilisation |H. G. (Herbert George) Wells 

The Turks were no longer in mass but extended in several lines, less than a pace between each man. Gallipoli Diary, Volume I |Ian Hamilton 

Thus among the huge mass of accumulated commodities the simplest wants would go unsatisfied. The Unsolved Riddle of Social Justice |Stephen Leacock 

Edward Winslow died; one of the first settlers of Plymouth colony, Mass., and afterwards its governor. The Every Day Book of History and Chronology |Joel Munsell