They come in a variety of flavors, including original tamarind, watermelon, and sour lime, and with just enough citrus and spice to make this candy the gateway to micheladas. The Ultimate Guide to Mexican Snacks |Bill Esparza |September 17, 2020 |Eater 

The Mediterranean Sea is the gateway to some of the world’s most important maritime trade routes, including the Suez Canal and the Red Sea. Butterfly Effect: The Next U.S.-Russia Conflict Theater Could be the Mediterranean |Charu Kasturi |September 17, 2020 |Ozy 

JavaScript-related partial indexing was my “gateway drug” into indexing problems. How to earn your place in Google’s index in 2020 |Bartosz Góralewicz |September 14, 2020 |Search Engine Land 

And, intriguingly, the circular enclosures appear to have been accessed not via doors or gateways but through small openings in “porthole stones.” An Ancient Site with Human Skulls on Display - Issue 89: The Dark Side |Jo Marchant |September 2, 2020 |Nautilus 

For example, Olly’s multivitamin is a big seller, and can be a gateway product leading the consumer to be more open to trying the brand’s probiotics. Deep Dive: How to master Amazon advertising in the new normal |Digiday |July 29, 2020 |Digiday 

This made AIMS the gateway for all communications to and from the flight deck. MH370 Debris Is Lost Forever, Can the Plane Be Found Without It? |Clive Irving |September 7, 2014 |DAILY BEAST 

Plus it is clear that plastic surgery is a gateway drug for those both so inclined and so well-heeled. The New World of Anti-Aging Dentistry |Kent Sepkowitz |June 4, 2014 |DAILY BEAST 

But I do like the notion of same-sex marriage as a liberation gateway drug. Were Christians Right About Gay Marriage All Along? |Jay Michaelson |May 27, 2014 |DAILY BEAST 

“For the girls who have never bought a business book, I think this can be the gateway drug,” Amoruso says. The ‘Nasty Gal’ Invasion: Sophia Amoruso Wants to Create an Army of #GIRLBOSSes |Alison Baitz |May 8, 2014 |DAILY BEAST 

Bettles, Alaska has been a gateway for intrepid travelers who want to explore the pristine wilderness of the Arctic Circle. Visiting the Arctic Circle…Before It’s Irreversibly Changed |Terry Greene Sterling |April 1, 2014 |DAILY BEAST 

A fellow rudely clad—a hybrid between man-at-arms and lackey—lounged on a musket to confront them in the gateway. St. Martin's Summer |Rafael Sabatini 

He pointed out the gateway, and the boy slipped off into the darkness, his bare feet soundless and mysterious on the sand. The Wave |Algernon Blackwood 

Galway is the gateway to Connemara, and Connemara is one of the best places under the sun for a healthy and enjoyable holiday. Fifty Years of Railway Life in England, Scotland and Ireland |Joseph Tatlow 

You first enter a large fore-court, at the extremity of which a colossal gateway leads into the inner courts. A Woman's Journey Round the World |Ida Pfeiffer 

A single winding footpath leads to the grim old gateway, and we rang the bell many times before the custodian admitted us. British Highways And Byways From A Motor Car |Thomas D. Murphy