What 15 months in a federal correction institution will be like, according to a man who counsels to-be inmates. How a ‘Real Housewife’ Survives Prison: ‘I Don’t See [Teresa Giudice] Having a Cakewalk Here’ |Michael Howard |January 6, 2015 |DAILY BEAST 

But the current pontiff, for reasons one might fully understand, declined to meet the would-be papal assassin. Pope-Shooter Ali Agca’s Very Weird Vatican Visit |Barbie Latza Nadeau |December 29, 2014 |DAILY BEAST 

The would-be pope killer loves to be in front of the cameras, and the press in Italy is happy to oblige. Pope-Shooter Ali Agca’s Very Weird Vatican Visit |Barbie Latza Nadeau |December 29, 2014 |DAILY BEAST 

Apparently, Shakespeare coined 1,700 words, from the frequently used (excitement) to the should-be-more frequently used (spewed). Biking With the Bard |Kara Cutruzzula |December 28, 2014 |DAILY BEAST 

In the years to come, Wha became a legendary starting out spot for various soon-to-be rock stars. The Bars That Made America Great |Nina Strochlic |December 28, 2014 |DAILY BEAST 

"Buy something for your wife that-is-to-be," he said to his grand-nephew, as he handed him the folded paper. The Pit Town Coronet, Volume I (of 3) |Charles James Wills 

Everything showed a rapid flight; even the would-be dinner of the guerrillas was found half cooked. The Courier of the Ozarks |Byron A. Dunn 

Next night at dinner I proposed Sir Alister's health, and we all drank to him and his "bride-to-be." Uncanny Tales |Various 

While a one-step was in full swing some would-be wag suddenly turned off all the lights. Uncanny Tales |Various 

And hand-painting it allus seemed to me, is really elocution in oils; for a be-yutiful picture is a silent talker. The Soldier of the Valley |Nelson Lloyd