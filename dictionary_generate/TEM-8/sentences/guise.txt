Mariana Flores, a 33-year-old contractor who also says Roemer lured her to his house under the guise of a work offer in 2011, then touched and kissed against her will, was one of the women pushed over the edge by Roemer’s denial. Dozens of Women Accused Famous Intellectual Andrés Roemer of Sexual Abuse. They Came Together to Make the World Listen |Meaghan Beatley |June 1, 2021 |Time 

Arizona follows Georgia, Texas and Florida in enacting voting restrictions under the guise of “election integrity” over the last few weeks. Arizona Just Became the Latest State to Approve Mail Voting Restrictions. Here’s What to Know |Sanya Mansoor |May 12, 2021 |Time 

Even Ben Long sits, to the far right, in the guise of Doubting Thomas. Following a trail of frescoes in the mountains of North Carolina |Barbara Noe Kennedy |May 7, 2021 |Washington Post 

An entire group — the theropods — walked on two legs and still do in their avian guises. ‘First Steps’ shows how bipedalism led humans down a strange evolutionary path |Riley Black |April 14, 2021 |Science News 

For years, the San Diego City Council has been approving the grant applications, effectively buying a wide range of tools under the guise of terrorism preparedness, without any meaningful vetting. Local Law Enforcement Quiet on Relationships With ‘Predictive Policing’ Company |Jesse Marx |February 2, 2021 |Voice of San Diego 

In the respectable guise of religious liberty, the zombie-like Culture War soldiers on. RFRA Madness: What’s Next for Anti-Democratic ‘Religious Exemptions’ |Jay Michaelson |November 16, 2014 |DAILY BEAST 

Campaigns often exchange outrageous attacks but to do so in the guise of a government mailer is quite unusual. Grimes Campaign “Exploring Legal Options” Against McConnell |Ben Jacobs |October 31, 2014 |DAILY BEAST 

And under the guise of exercise there exists a community that welcomes them with open arms. #ButtSchool - How Porn Stars Work Out: Pop Physique Promises the Perfect Derriere |Aurora Snow |August 23, 2014 |DAILY BEAST 

During the chaos, many old grudges were settled, sometimes under the guise of overdue justice. The ‘GOT’ Red Viper and Mountain Duel, and a History of Medieval Trial by Combat |Steven Isaac |June 3, 2014 |DAILY BEAST 

These changes are being made under the guise of fixing a country on the verge of collapse. Australia Wants to Open the Great Barrier Reef to Dumping |Kirsten Alexander |June 2, 2014 |DAILY BEAST 

Never had Tom seen his gay and careless cousin in such guise: he was restless, silent, intense and inarticulate. The Wave |Algernon Blackwood 

Even the air has its strange denizens in the guise of huge beetles and vampire-winged flying foxes. The Red Year |Louis Tracy 

Under the guise of apparent indifference his mind kept the Canadian under constant observation. Three More John Silence Stories |Algernon Blackwood 

Beyond the door he could see something in the guise of a foreman printer with a damp news sheet in his hand. The Weight of the Crown |Fred M. White 

The latter appointed to the vacant estates and positions members of her house—that of Guise. Women of Modern France |Hugo P. Thieme