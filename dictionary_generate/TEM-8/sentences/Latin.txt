It dates to 1740s Britain and of course was written originally in Latin (“Adeste Fideles”). Yes, I Like Christmas Music. Stop Laughing. |Michael Tomasky |December 24, 2014 |DAILY BEAST 

BOGOTÁ, Colombia — Most Latin Americans celebrated the rapprochement between the United States and Cuba. Venezuela Says Goodbye to Its Lil Friend, While the Rest of the Continent Cheers |Catalina Lobo-Guererro |December 20, 2014 |DAILY BEAST 

Similar stories plague many parts of Latin America, Africa, and Eastern Asia. Promoting Girls’ Education Isn’t Enough: Malala Can Do More |Paula Kweskin |December 9, 2014 |DAILY BEAST 

In fact, beer prices in Panama are about 36 percent lower than anywhere else in Latin America. House of the Witch: The Renegade Craft Brewers of Panama |Jeff Campagna |November 30, 2014 |DAILY BEAST 

Panamanians are by far the biggest beer consumers in Latin America, but not when it comes to the good stuff. House of the Witch: The Renegade Craft Brewers of Panama |Jeff Campagna |November 30, 2014 |DAILY BEAST 

He could go and live over in the Latin Quarter—that 's the desire of his heart—and think of nothing but old bottles. Confidence |Henry James 

So far we have not made great progress in securing Europe's Latin-American trade. Readings in Money and Banking |Chester Arthur Phillips 

Every monumental inscription should be in Latin; for that being a dead language, it will always live. The Book of Anecdotes and Budget of Fun; |Various 

Several uneducated business men are said to have written to the Dean asking the Latin for what they think of the new Budget. Punch, or the London Charivari, Volume 158, April 28, 1920 |Various 

The descriptions of allegorical personages in this poem are clearly imitated from similar descriptions in Latin poets. Chaucer's Works, Volume 1 (of 7) -- Romaunt of the Rose; Minor Poems |Geoffrey Chaucer