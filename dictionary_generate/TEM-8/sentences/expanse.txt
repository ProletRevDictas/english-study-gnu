I looked around at the expanse of dunes below me, their proportions and distances distorted by the harsh sun. I Tried to Climb the Largest Sand Dune in North America |Emily Pennington |September 22, 2020 |Outside Online 

Finally, I looked out my window into a beautiful little valley with what seemed to be the perfect expanse of wetland. How to hunt for star-nosed moles (and their holes) |Kenneth Catania |September 15, 2020 |Popular-Science 

These strikes caused dozens of fires from Santa Cruz to Half Moon Bay, a forested, mountainous expanse, and came to be known as the CZU Lightning Complex fire. How We Saved Our Neighborhood from a Wildfire |Dain Zaffke |September 2, 2020 |Outside Online 

Meanwhile, SkyCool Systems of Mountain View, California, has developed essentially high-tech mirrors that can cast heat into the cold expanses of space, taking advantage of a natural phenomenon known as radiative cooling. Air conditioning technology is the great missed opportunity in the fight against climate change |James Temple |September 1, 2020 |MIT Technology Review 

Advocates decry the amount, saying it’s too low compared with the profit SDG&E makes off the vast expanse of electric poles, wires and natural gas lines each year. The City Is Walking a Fine Line in Demanding Millions From Its Next Power Provider |MacKenzie Elmer |August 7, 2020 |Voice of San Diego 

Kim Novak runs away from James Stewart, across an expanse of field. Alfred Hitchcock’s Fade to Black: The Great Director’s Final Days |David Freeman |December 13, 2014 |DAILY BEAST 

The party sequence in Notorious begins with a wide shot from high above the top of the stairs, all glittering expanse below. Alfred Hitchcock’s Fade to Black: The Great Director’s Final Days |David Freeman |December 13, 2014 |DAILY BEAST 

Tokyo Bay is “a black expanse where gulls wheeled above drifting shoals of white Styrofoam.” American Dreams: Did William Gibson’s ‘Neuromancer’ Blueprint Our Reality? |Nathaniel Rich |October 5, 2014 |DAILY BEAST 

Using UpToDate as the ur-text, though, gets into a vast expanse of gray zone, with some assertions evidence-based but many not. How Wikipedia Is Like Your Doctor |Kent Sepkowitz |May 31, 2014 |DAILY BEAST 

To the north lay the wide expanse of Samburu land, and its mighty Ewaso Ngiro river. Borana Joins the Fight to Save Kenya’s Rhinos…and Wants You to Help Too |Joanna Eede |February 18, 2014 |DAILY BEAST 

When it cleared, the valley was a solid expanse of white, and the stars shone out as if in an Arctic sky. Ramona |Helen Hunt Jackson 

The expanse, apparently so limitless, open to her view, invited her fancy to a range equally boundless. Madame Roland, Makers of History |John S. C. Abbott 

Here a palace with low portals extended its ponderous expanse; it was the palace of King Loc. Honey-Bee |Anatole France 

This in turn will go through its process of retreat until the former expanse of waters disappears. Outlines of the Earth's History |Nathaniel Southgate Shaler 

What more natural term, then, to apply to a spot of land standing alone in the midst of an expanse of water than an eye of land? Notes and Queries, Number 194, July 16, 1853 |Various