Scientists may develop new ways to treat diseases, but drugs and implants could also be used to control our thoughts and emotions, or even change who we are — the stuff of dystopian novels. Where do we draw the line between life and death? |Nancy Shute |September 6, 2020 |Science News 

Unlike engineering, the solution to the brain isn’t more channels or more implants. Neuralink’s Wildly Anticipated New Brain Implant: the Hype vs. the Science |Shelly Fan |September 1, 2020 |Singularity Hub 

Rather than removing the entire implant, it could potentially be useful to leave the threads inside the brain and only remove the top cap—the Link device that contains the processing chip. Neuralink’s Wildly Anticipated New Brain Implant: the Hype vs. the Science |Shelly Fan |September 1, 2020 |Singularity Hub 

The electrodes of the implant itself are “sewn” into the brain with automated robotic surgery, relying on brain imaging techniques to avoid blood vessels and reduce brain bleeding. Neuralink’s Wildly Anticipated New Brain Implant: the Hype vs. the Science |Shelly Fan |September 1, 2020 |Singularity Hub 

Researchers gave mixed reviews of Elon Musk’s colorful demonstration on Friday of Neuralink, his sci-fi startup that’s developing a brain implant. Neurologists aren’t so sure about Elon Musk’s Neuralink brain implant startup |dzanemorris |August 31, 2020 |Fortune 

I can see the implant in there, and see where the muscle is snatching that implant up. Azealia Banks Opens Up About Her Journey from Stripping to Rap Stardom |Marlow Stern |November 17, 2014 |DAILY BEAST 

McDonough helped create an implant prototype, but in the end, “nothing happened to it,” Williams said in court testimony. Patients Screwed in Spine Surgery ‘Scam’ |The Center for Investigative Reporting |November 3, 2014 |DAILY BEAST 

After he removed the plastic implant, her skin cleared up completely. Why Your Doctor Feels Like a 'Beaten Dog' |Daniela Drake |September 11, 2014 |DAILY BEAST 

Food critic and travel writer Lucy Knisley added a comic discussing her personal experiences with the birth control implant. ‘Oh Joy Sex Toy’: The Internet’s Most Radical Sex-Fueled Comic Strip |Rich Goldstein |May 10, 2014 |DAILY BEAST 

October 2012 saw her follow the surgery by spending an additional $16,000 on a rhinoplasty and a chin implant. ‘Teen Mom’ Farrah Abraham’s Rocky Path to Sex-Tape Stardom. Also, Sex Tapes Are Still a Thing? |Melissa Leon |April 11, 2013 |DAILY BEAST 

How could we avoid receiving, in our infancy, whatever impressions and opinions our teachers and relations chose to implant in us? Letters To Eugenia |Paul Henri Thiry Holbach 

She had made an effort to keep her children from harmful influences and to implant in them a hate for these things. Maezli |Johanna Spyri 

In their cases the mistress has no bad impressions to efface and she can implant her own modes in virgin soil. The Expert Maid-Servant |Cristine Terhune Herrick 

Nor is it a slight proof of the mighty power of love that it can thus implant fear in the breast of a sailor. Bentley's Miscellany, Volume II |Various 

If he have sense and abilities, they ought rather to guard his bosom from so contemptible an inmate, than implant it there. The Mysterious Wanderer, Vol. I |Sophia Reeve