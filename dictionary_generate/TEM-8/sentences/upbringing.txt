Based on my upbringing, if I am invited to a wedding, I always send a gift. Miss Manners: Exec’s disgusting habit needs to move off-screen |Judith Martin, Nicholas Martin, Jacobina Martin |December 2, 2020 |Washington Post 

When you get older and you start to see the way other families live, you ask questions about your upbringing. The Best Money Advice Lamorne Morris Ever Got … From Zooey Deschanel |Daniel Malloy |November 27, 2020 |Ozy 

Behavioral genetics looks at where individual differences come from—whether from family upbringing and life experience, or genetic factors. Why We Judge People Based on Their Relatives - Facts So Romantic |Diana Fleischman |November 11, 2020 |Nautilus 

Perhaps because of his lefty upbringing, he became the only editor to sign a union card during an unsuccessful effort to organize the magazine. Daniel Menaker, author and celebrated editor at the New Yorker and Random House, dies at 79 |Harrison Smith |October 29, 2020 |Washington Post 

Our job as parents is to take care of their health, spiritual and moral upbringing. How parents can add purpose and structure to their kids’ online learning |Tara Chklovski |August 20, 2020 |Quartz 

Following her upbringing at Chartwell, the Churchill family home in Kent, Mary Soames, according to Emma Soames, had “a good war.” Churchill’s Secret Treasures for Sale: A British PM’s Life on the Auction Block |Tom Teodorczuk |December 8, 2014 |DAILY BEAST 

None of this was expected considering his pampered upbringing. From Fashion Player to Photographer Monk |Nina Strochlic |December 3, 2014 |DAILY BEAST 

At the same time though, I am a white male from a suburban upper-middle-class upbringing. Ferguson Tensions in Black and White |Seth Ferranti |November 21, 2014 |DAILY BEAST 

Considering your multiethnic upbringing, what are some ways in which you look at this country differently than other players? The Chicago Bulls’ Joakim Noah Sounds Off on Weed, the Weather, and Winning |Bill Schulz |October 19, 2014 |DAILY BEAST 

Despite a difficult upbringing, Moran avoids depressing and Dickensian-ish themes. Join Caitlin Moran’s Riotous Feminist Revolution |Lizzie Crocker |September 29, 2014 |DAILY BEAST 

A satisfactory home life can be attained only by the co-operation of both parents in the upbringing of their children. Report of the Special Committee on Moral Delinquency in Children and Adolescents |Oswald Chettle Mazengarb et al. 

But that is hard to carry out, for the gentleman in Holstein who decides about our upbringing wants me to study for many years. Maezli |Johanna Spyri 

He so far forsook the strait "Manchester School" of his upbringing as to support Macdonald's campaign for protection in 1878. The Canadian Dominion |Oscar D. Skelton 

The home is responsible for the upbringing of healthy, intelligent children. Euthenics, the science of controllable environment |Ellen H. Richards 

Do you know that all these were squeezed out of your dying father by greedy priests, to pay for your upbringing in the cloisters? The White Company |Arthur Conan Doyle