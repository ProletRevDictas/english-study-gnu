In that ambiguity and with a sliding abacus of nuance, both political parties wrestled with their responses to the fallout and calculations about the impact this would have on the electorate. A Leaked Supreme Court Draft Threatens to Make Every Race About Abortion |Philip Elliott |May 3, 2022 |TIme 

I may as well have given them an abacus and asked them to do algebra. My Kids Navigated Our Road Trip—It Was an Adventure |thodgson |July 12, 2021 |Outside Online 

This is like the difference between building a stick-in-the-ground sundial versus a comparatively more complex accounting tool like an abacus, as one head of quantum research at a major Wall Street bank put it to me. Google and China duke it out over ‘quantum supremacy’ |rhhackettfortune |December 8, 2020 |Fortune 

Generally speaking, the abacus is more impressive, or at least more potentially useful. Google and China duke it out over ‘quantum supremacy’ |rhhackettfortune |December 8, 2020 |Fortune 

Thus, Goldman found them a willing buyer for the junk piled into Abacus. Goldman's Dirty Customers |John Carney |April 21, 2010 |DAILY BEAST 

But Abacus and similar deals were already sucking money out of Rhineland, according to a person familiar with the matter. Goldman's Dirty Customers |John Carney |April 21, 2010 |DAILY BEAST 

Every Asian capital is like a giant abacus, constantly calculating the relative power of competing states. Does Obama Care About Asia? |Michael Fullilove |March 19, 2010 |DAILY BEAST 

The height of the abacus is one seventh of the height of the capital. Ten Books on Architecture |Vitruvius 

The flowers on the four sides are to be made as large as the height of the abacus. Ten Books on Architecture |Vitruvius 

The abacus has a width equivalent to the thickness of the bottom of a column. Ten Books on Architecture |Vitruvius 

It is further distinguished by the use of the zero, which enabled the computer to dispense with the columns of the Abacus. The Earliest Arithmetics in English |Anonymous 

Our next idea would be to put a conical shaped stone beneath this abacus, to support its outer edge, as at b. The Stones of Venice, Volume I (of 3) |John Ruskin