This includes navigating influencers trying to sell them products and teaching them to look for references when information is provided to them. A feminine wash for teens? Angry parents and gynecologists are on a social media crusade. |Abigail Higgins |February 12, 2021 |Washington Post 

It also comes with a stand so you can use your mannequin as a reference for drawing mid-air poses. Must-have art supplies to let your inner creative shine |Sandra Gutierrez G. |February 11, 2021 |Popular-Science 

You never know when you’ll need it for reference or to show to someone. Replacing pieces of flatware or china can be a challenge. Here’s how to track them down. |Laura Daily |February 11, 2021 |Washington Post 

Illumina’s sequencers read through each sample’s code and compare each letter to a reference sequence, looking for significant changes. Why Opening Restaurants Is Exactly What the Coronavirus Wants Us to Do |Caroline Chen |February 6, 2021 |ProPublica 

There are frequent, if general, references to forces bigger than one’s self. Peloton makes toning your glutes feel spiritual. But should Jesus be part of the experience? |Michelle Boorstein |February 5, 2021 |Washington Post 

In 2011 LGBT media outlet Queerty took the app to task for allegedly deleting accounts that made reference to being trans. Grindr’s Trans Dating Problem |David Levesley |January 9, 2015 |DAILY BEAST 

He then provides some insight into his psyche - complete with Animal House reference. Huckabee 2016: Bend Over and Take It Like a Prisoner! |Olivia Nuzzi |January 8, 2015 |DAILY BEAST 

There is reference after reference to the “black community,” “black worth ethic,” and adherence to the “black value system.” Reverend Jeremiah Wright Was Worse Than Scalise |Ron Christie |January 2, 2015 |DAILY BEAST 

Indeed, designers frequently reference each other in their shows—and the press never fails to notice. The Big Business of Fashion Counterfeits |Lizzie Crocker |December 24, 2014 |DAILY BEAST 

On his Instagram account (which has since been taken down), Brinsley made one reference to burning an American flag. Alleged Cop Killer Ismaaiyl Brinsley Had a Death Wish |M.L. Nestel |December 22, 2014 |DAILY BEAST 

These Eskimos were very fond of kite-flying, for its own sake, without reference to utility! The Giant of the North |R.M. Ballantyne 

Had he not meant the Fleet to shove in K. must have made some reference to the second Division, surely. Gallipoli Diary, Volume I |Ian Hamilton 

For convenience of reference I now give the figure Alphabet tabulated. Assimilative Memory |Marcus Dwight Larrowe (AKA Prof. A. Loisette) 

She made no reference, for instance, to the fact that they might be frequently alone together now. The Wave |Algernon Blackwood 

Take the memories of members of the learned professions—they are usually only reference memories. Assimilative Memory |Marcus Dwight Larrowe (AKA Prof. A. Loisette)