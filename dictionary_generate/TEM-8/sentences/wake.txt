In the wake of the coronavirus pandemic, this number can certainly be expected to increase, as this spring’s primaries have indicated. The biggest threat to mail-in voting isn’t security—it’s politics |matthewheimer |August 24, 2020 |Fortune 

A separate study I conducted of newspaper coverage in the wake of the 2016 election found that about a third of news stories and op-eds argued that Clinton lost because of her focus on identity politics. How Clinton’s Loss Paved The Way For Biden |Seth Masket |August 20, 2020 |FiveThirtyEight 

That’s according to a working paper, published by the National Bureau of Economic Research in May, that looks at how violent crimes by veterans increased after overseas deployment began in the wake of the attacks on the Twin Towers. The Hidden Costs of War: Vet Crimes |Nick Fouriezos |August 19, 2020 |Ozy 

The US’s declining stature in the wake of the pandemic is accelerating two global political trends that have emerged in the last five years. Covid-19 and the geopolitics of American decline |Katie McLean |August 19, 2020 |MIT Technology Review 

Supply chains across industries are going through an unprecedented global disruption in the wake of the Covid-19 pandemic which has shuttered airports, seaports and hampered the movement of goods and people around the world. Africa is tackling its supply chain deficit with a US-backed research center in Ghana |Yinka Adegoke |July 27, 2020 |Quartz 

That is why The Daily Beast stands with Charlie Hebdo and published their controversial covers in the wake of the attack. Why We Stand With Charlie Hebdo—And You Should Too |John Avlon |January 8, 2015 |DAILY BEAST 

In the wake of this turmoil, the New York Post reported that the police had stopped policing. Ground Zero of the NYPD Slowdown |Batya Ungar-Sargon |January 1, 2015 |DAILY BEAST 

However, we have just had a necessary wake-up call that all is not as secure as we believed. A Gift to the Jihadis: The Unseen Airport Security Threat |Clive Irving |December 27, 2014 |DAILY BEAST 

The newly free country struggled to maintain order in the wake of independence, but it was woefully unprepared. The Congo's Forgotten Colonial Getaway |Nina Strochlic |December 18, 2014 |DAILY BEAST 

In the wake of the verdicts in Ferguson and New York City, many of us are still sore with emotion. The Stacks: A Chicken Dinner That Mends Your Heart |Pete Dexter |December 7, 2014 |DAILY BEAST 

The latter gentleman was wondering whether he had fallen into a dream that he should wake up from in the morning. Elster's Folly |Mrs. Henry Wood 

And a little later we, too, left the post, following in the dusty wake of the paymaster's wagon and its mounted escort. Raw Gold |Bertrand W. Sinclair 

As the impressive progress continued the revellers ceased their revels and followed in the wake of Aristide. The Joyous Adventures of Aristide Pujol |William J. Locke 

Woman—Thou beest a sound sleeper—Wake up, and see to thy bairn, and I will gie thee both a good breakfast. The World Before Them |Susanna Moodie 

Again his rage touched my admiration; but I got him away before he made enough noise to wake the whole Camp. Three More John Silence Stories |Algernon Blackwood