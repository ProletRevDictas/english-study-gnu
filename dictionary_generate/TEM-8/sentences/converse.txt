She speaks of cooking and mothering and her beloved Converse sneakers. The sound of a shifting power structure |Robin Givhan |January 13, 2021 |Washington Post 

These aren’t just self-help or how-to guides about launching your startup for the umpteenth time or networking with someone you really have no interest in conversing with ever again. The 10 best business books of 2020 |Rachel King |December 5, 2020 |Fortune 

They just happen to be voicing their support — and conversing with the rest of the community — online. In the wake of COVID, sports fans have gone social |Twitter |October 22, 2020 |Digiday 

Each wedding professional has a system for converting the client from Instagram to a live conversation, but not until they feel a valid relationship after conversing online. Inside ‘Insta-Booking’: How couples are hiring wedding vendors via Instagram |Rachel King |September 6, 2020 |Fortune 

She was alert, conversing with her children and even eating a little food. Sent Home to Die |by Annie Waldman and Joshua Kaplan |September 2, 2020 |ProPublica 

And the converse, that the more likely an event, the less sure we are, also is true. A Doctor Explains Why Cruise Ships Should Be Banned |Kent Sepkowitz |November 19, 2014 |DAILY BEAST 

Which leads to the converse problem of treating children for disorders they do not have. Daydreaming Is Not a Disorder |Russell Saunders |October 4, 2014 |DAILY BEAST 

Cobain regularly wore Converse sneakers, and was wearing a pair when his body was found on April 8,1994. Inside Kurt Cobain’s $450M empire |CNBC |April 7, 2014 |DAILY BEAST 

Throughout its history Converse has maintained strong ties to street culture. How Sneaker Culture Conquered the World |William O’Connor |March 16, 2014 |DAILY BEAST 

Yet somehow, despite the divergent approaches and subjects, the captured characters converse with ease. The Portrait Gallery Face-Off: Fresh, Famous, and Quirky Faces Win the Day |Chloë Ashby |November 17, 2013 |DAILY BEAST 

When a man converses with himself, he is sure that he does not converse with an enemy. The Life and Most Surprising Adventures of Robinson Crusoe, of York, Mariner (1801) |Daniel Defoe 

The children of sinners become children of abominations, and they that converse near the houses of the ungodly. The Bible, Douay-Rheims Version |Various 

There he was found by old Makitok, and for some time the giant and the wizard held converse together. The Giant of the North |R.M. Ballantyne 

You see it for yourself, no Englishman ever shall suspect me, when we shall converse, of being other than a Briton. The Pit Town Coronet, Volume I (of 3) |Charles James Wills 

But surely this point of view is the very converse of the teachings of common sense. The Unsolved Riddle of Social Justice |Stephen Leacock