That night, Faris saw a woman near her bawl and wide-eyed grown-ups run. A girl’s reaction to the Nationals Park shooting made the world look at D.C.’s gun violence problem |Theresa Vargas |July 21, 2021 |Washington Post 

In fact, she doesn't seem happy till she gets it and I suspect that if I missed it any morning she would bawl for it. The Red Cow and Her Friends |Peter McArthur 

At first she wouldn't allow any one but me to milk her and would bawl if I attended to any of the other cows first. The Red Cow and Her Friends |Peter McArthur 

They sometimes, on particular occasions, would sing or bawl out something like a rude tune; but we could not understand it. A Narrative of the Shipwreck, Captivity and Sufferings of Horace Holden and Benj. H. Nute |Horace Holden 

We would jibe one another, laugh at a fellow to his chagrin, and when we were angry bawl each other out unmercifully. The Iron Puddler |James J. Davis 

Sharp and lively, I mean; not bawl, and answer over your back—most part impudence, and nothing else—and then out of hearing. Rhoda Fleming, Complete |George Meredith