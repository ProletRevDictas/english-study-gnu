This matter is, in the Indian dialectic of beauty, nonnegotiable. Miss America, Meet India’s ‘Dark’ Side |Tunku Varadarajan |September 17, 2013 |DAILY BEAST 

His (mis)reading of the Megilla power dialectic meant tragedy for all. Purim Perils: His View Is His Own |Rabbi Daniel Landes |February 18, 2013 |DAILY BEAST 

He had five-year plans and seven-year plans by the bushel-full, and he never lost faith in the dialectic. Rich Man's Revolutionary |Michael Tomasky |October 13, 2011 |DAILY BEAST 

Islam is 1,400 years old; fascism entered the dialectic only with Benito Mussolini. Why the Mosque Scares the Right |M.J. Akbar |August 14, 2010 |DAILY BEAST 

They are the yin and the yang of the whole film and they dance the dialectic to perfection. Polanski's Brilliant Comeback |Simon Schama |February 18, 2010 |DAILY BEAST 

One other illustration of this keen childish dialectic when face to face with the accuser deserves to be touched on. Children's Ways |James Sully 

As in the later days of Greece, rhetoric and dialectic are the most powerful of the arts. The New Society |Walther Rathenau 

In the Anglican doctorPage 119 it employs the dialectic and metaphysics of Aristotle. Colleges in America |John Marshall Barker 

The latter is a composition of the literary German with dialectic forms, and his rhythms are halting, his ideas one-sided. The History of Yiddish Literature in the Nineteenth Century |Leo Wiener 

He wrote extensively not only on medicine, but on philosophy, his writings taking throughout a more or less dialectic character. An Epitome of the History of Medicine |Roswell Park