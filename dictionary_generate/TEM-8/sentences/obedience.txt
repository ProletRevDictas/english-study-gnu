Perhaps most important, obedience to procedural norms is a sign of judicial humility. Texas’s radical anti-abortion law, explained |Ian Millhiser |September 2, 2021 |Vox 

They can safely keep a mischievous puppy from chasing other animals or gnawing on the living-room furniture, provide an exercise space as a dog run, or help with obedience or anti-anxiety training. Best dog pen: Keep your pup safe and happy in these versatile containers |Irena Collaku |August 20, 2021 |Popular-Science 

That lack of obedience stressed out other owners and their pooches, and it proved vexing and occasionally fatal to wildlife. I Was a Bad Dog Owner. Don’t Be Like Me. |Kate Siber |May 11, 2021 |Outside Online 

The Texans need to rebuild more than they need to make some point about obedience. Deshaun Watson is taking a stand against disingenuous NFL owners. It could change the league. |Jerry Brewer |February 1, 2021 |Washington Post 

When adhering to simple safety measures can save tens of thousands of American lives, wearing a mask is not an act of blind obedience, it is an act of patriotism. How we can encourage people to wear masks — for others’ sake |Shai Davidai |December 24, 2020 |Vox 

Such brutality will likely inspire fear and obedience among the overwhelmingly moderate Sunnis of Iraq, but not enthusiasm. How Iran and America Can Beat ISIS Together |Ben Van Heuvelen |June 21, 2014 |DAILY BEAST 

“They stressed rules and obedience, Francis is emphatic about mercy,” Berry says. The Seedy Side of Sainthood: Was John Paul II Canonized Too Fast? |Barbie Latza Nadeau |April 17, 2014 |DAILY BEAST 

“The first website that really helped me understand what obedience to Allah was,” Loewen wrote. Terry Lee Loewen, the Mellow Kansas Man Who Dreamed of Jihad |Michael Daly |December 16, 2013 |DAILY BEAST 

A timeless fairytale of true love and magical transformation would be reduced to a boring exercise in memorization and obedience. China’s Schools Teaches Kids to Take Tests, Obey the State, and Not Much More |Junheng Li |November 30, 2013 |DAILY BEAST 

We knew that obedience was immediate, complete, and without question. The Sinister Side of Homeschooling |Michelle Goldberg |September 20, 2013 |DAILY BEAST 

And as she hesitated between obedience to one and duty toward the other, her life, her love and future was in the balance. The Homesteader |Oscar Micheaux 

They threw down their weapons with sullen obedience and the first great step towards the re-conquest of India was taken. The Red Year |Louis Tracy 

The legal framework of the State and of obedience to the law in which industrial society is set threatens to break asunder. The Unsolved Riddle of Social Justice |Stephen Leacock 

On the part of the believer, his faith and imperfect obedience, though necessary, are not a condition. The Ordinance of Covenanting |John Cunningham 

The old dog stuck to her like a burr, and she had not the heart to take up a stick to enforce obedience. The World Before Them |Susanna Moodie