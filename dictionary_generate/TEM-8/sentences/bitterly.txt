Then put a simple salad of bitter greens on the side, to cut through the richness of the rest of the meal, and you’re done. Turn to duck confit for a holiday treat with long-lasting benefits |Mary Beth Albright |November 20, 2020 |Washington Post 

Let the drink infuse over the heat for 10 to 15 minutes, then add the brandy and bitters. This cocktail can keep you warm around the fire pit — and you can make it there, too |M. Carrie Allan |November 20, 2020 |Washington Post 

Something bitter and strong can do wonders before a heavy dinner. Thanksgiving appetizer recipes and easy cocktails to start the feast off right |Daniela Galarza, Becky Krystal |November 11, 2020 |Washington Post 

How the simple art of cutting fruit can be an act of loveBut sweet, sometimes bitter, sometimes sour oranges can add so much to a cooked dish. Harness the bright sweetness of oranges in these 7 recipes |Kari Sonde |November 9, 2020 |Washington Post 

The bitter debates in Maryland a few years ago over allowing gambling now seem quaint. Election 2020: Winners and losers from Virginia, Maryland and D.C. |Robert McCartney |November 6, 2020 |Washington Post 

There are a few good ones, Antoine says, but he complained bitterly of a lack of responsiveness. Ground Zero of the NYPD Slowdown |Batya Ungar-Sargon |January 1, 2015 |DAILY BEAST 

The one and indivisible capital of Israel has not been this bitterly divided since 1967. Mass Murder in the Holy City |Michael Tomasky |November 18, 2014 |DAILY BEAST 

ISIS and al Qaeda bitterly split earlier this year, and have since attacked one another on occasions. Al Qaeda Plotters in Syria ‘Went Dark,’ U.S. Spies Say |Eli Lake |September 24, 2014 |DAILY BEAST 

A bitterly partisan public discourse also developed in 18th-century England. How the News Business Found Its Footing |Nick Romeo |June 22, 2014 |DAILY BEAST 

Officials are bitterly divided over two diametrically opposed strategies. Inspectors Uncover UK Schools Pushing Radical Islamic Agendas |Nico Hines |June 10, 2014 |DAILY BEAST 

And for fear of being ill spoken of weep bitterly for a day, and then comfort thyself in thy sadness. The Bible, Douay-Rheims Version |Various 

He failed to see that this man had suffered bitterly through his evil machinations. The Homesteader |Oscar Micheaux 

Ten or twelve added years had slipped by, and it did not seem human that she should continue to feel bitterly toward me. The Boarded-Up House |Augusta Huiell Seaman 

We did not talk much about the past at dinner, except—ah me, how bitterly we regretted our 10 per cent. Gallipoli Diary, Volume I |Ian Hamilton 

Punch went out and wept bitterly with Judy, into whose fair head he had driven some ideas of the meaning of separation. Kipling Stories and Poems Every Child Should Know, Book II |Rudyard Kipling