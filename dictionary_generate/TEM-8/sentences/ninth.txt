Ryser told the board that she once asked her daughter, now a ninth-grader, if the girl felt safe at school. As school shootings surge, a sixth-grader tucks his dad’s gun in his backpack |John Woodrow Cox, Steven Rich |June 24, 2021 |Washington Post 

In 2019, the school de-tracked geometry, the math class most ninth-graders take. Can honors and regular students learn math together? A new approach argues yes. |Laura Meckler |June 4, 2021 |Washington Post 

Wander Suero was perfect in the eighth, but Austin Voth surrendered a solo homer to Omar Narváez in the ninth. The Nationals’ bats remain absent, and one Max Scherzer miscue yields a fourth straight loss |Gene Wang |May 30, 2021 |Washington Post 

Eight Liberty players are north of 40 percent from deep, and a ninth, Rebecca Allen, is a skilled perimeter shooter whose shooting percentage is likely to revert to the mean in the coming games. The New York Liberty Are Shooting Lights Out — And They Could Get Even Better |Howard Megdal |May 26, 2021 |FiveThirtyEight 

That’s not a bad place to start, a road map for Martinez from the sixth through the ninth, with plenty of mix-and-match options. Brad Hand’s arrival might end a Nationals tradition: The search for summer relief |Barry Svrluga |January 26, 2021 |Washington Post 

The Eighty-ninth Congress was potentially more fertile ground for the broad range of controversial programs on his dream agenda. Thank Congress, Not LBJ for Great Society |Julian Zelizer, Scott Porch |January 4, 2015 |DAILY BEAST 

That makes New York the ninth state to require such coverage. The Insurance Company Promised a Gender Reassignment. Then They Made a Mistake. |James Joiner |December 29, 2014 |DAILY BEAST 

I know the verse because Mrs. Bertalan used to have us do it in ninth-grade choir. Yes, I Like Christmas Music. Stop Laughing. |Michael Tomasky |December 24, 2014 |DAILY BEAST 

Maybe all journeys should be imagined as a single day, short as a trip to the corner or long as a life in its ninth decade. The Daily Beast’s Best Longreads, Oct 13-19, 2014 |William Boot |October 19, 2014 |DAILY BEAST 

All of the objects are believed to have been buried between the mid-ninth and 10th centuries. How To Strike Viking Gold |Lizzie Crocker |October 18, 2014 |DAILY BEAST 

He directed the Ninth Symphony, and played twice himself with orchestral accompaniments. Music-Study in Germany |Amy Fay 

Methodius and Cyril, who were sent missionaries to the Sclavonians in the ninth century. Blackwood's Edinburgh Magazine, Volume 60, No. 372, October 1846 |Various 

Its most important compound is water, of which it forms one-ninth, the other eight-ninths consisting of oxygen. Elements of Agricultural Chemistry |Thomas Anderson 

Now the king sat in the winter house, in the ninth month: and there was a hearth before him full of burning coals. The Bible, Douay-Rheims Version |Various 

The cathedral is one of the oldest in England, having been mainly built in the Ninth Century. British Highways And Byways From A Motor Car |Thomas D. Murphy