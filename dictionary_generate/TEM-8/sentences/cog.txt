Without some help from Washington, there’s no way that number doesn’t continue to climb — right as those same workers are crucial cogs to vaccine distribution. State Budgets Are In Tatters. Republicans in Washington Say Too Bad |Philip Elliott |February 4, 2021 |Time 

In a sign of the times, “Instagram editor” is now an official title at The Washington Post and a key cog in the newspaper publisher’s subscription business. How The Washington Post’s new Instagram editor will try to boost its subscription business |Sara Guaglione |February 2, 2021 |Digiday 

How well they fit in and make that transition from startup to big company cog, will go a long way in determining the success of this transaction in the long run. SAP is buying Berlin business process automation startup Signavio |Ron Miller |January 27, 2021 |TechCrunch 

Where you can literally see the cogs turning and they get to a position, or they come up with an explanation. The BBC’s Katty Kay Shuts Down Men Who ‘Tell Me to Shut Up’ |Eugene Robinson |January 23, 2021 |Ozy 

On Android, from Settings go to Accessibility and Text-to-speech output, then tap the cog next to Preferred engine and choose Install voice data. How to convert text articles into audio (and why you might want to) |David Nield |January 5, 2021 |Popular-Science 

A poor cog in the machine, she seemed to feel no more disgrace than a blood-diamond miner. Is Bed Bath & Beyond a ‘Palace of Lies’? An Investigation. |Rachel Krantz |February 27, 2013 |DAILY BEAST 

You might be a cog at a trading desk, compensated with nothing but money. Let Them Say F--k |Reihan Salam |August 1, 2010 |DAILY BEAST 

The apples are heaped on all sides, and are first crushed between wooden cog-wheels and caught in tubs. Harper's Young People, November 30, 1880 |Various 

Nay,” returned Mr. Archer with a smile, “no man can put complete reliance in blind fate; he must still cog the dice. The Works of Robert Louis Stevenson, Volume XXI |Robert Louis Stevenson 

Each one is a cog in the vast organization and one slip may disrupt the whole arrangement. Private Peat |Harold R. Peat 

Back in the East, things have been settled for so long that a man's only a cog in a machine. Hidden Gold |Wilder Anthony 

Here, though, was evidence either that the War Lord was running out of metal or that his system had slipped a cog. The Glory of The Coming |Irvin S. Cobb