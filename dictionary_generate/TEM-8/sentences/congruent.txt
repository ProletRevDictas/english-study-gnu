Last week, you looked at a very large regular polygon — that is, a polygon all of whose sides and angles were congruent. Can You Climb Your Way To Victory? |Zach Wissner-Gross |September 24, 2021 |FiveThirtyEight 

His parents took the news with disappointment and sadness because his truth isn’t congruent with their own. Anne Lamott’s Advice Could Stop You From Drowning in Cynicism |Susanna Schrobsdorff |September 23, 2021 |Time 

In this way, all positive integers have been “covered,” and the numbers occupying the same bucket are considered “congruent” to each other. Mathematicians Find a New Class of Digitally Delicate Primes |Steve Nadis |March 30, 2021 |Quanta Magazine 

He proved that for two shapes to be “scissors congruent” — meaning they can be cut up and reassembled as each other — they must have the same Dehn invariant. Undergraduates Hunt for Special Tetrahedra That Fit Together |Kevin Hartnett |February 9, 2021 |Quanta Magazine 

In 1965 Jean-Pierre Sydler proved that any two shapes with the same volume and the same Dehn invariant are scissors congruent. Undergraduates Hunt for Special Tetrahedra That Fit Together |Kevin Hartnett |February 9, 2021 |Quanta Magazine 

That solution is not congruent with the Cato Institute philosophy. Defend Capitalism? |David Frum |July 18, 2012 |DAILY BEAST 

And yet he insists, in every single interview, that his teachings are entirely congruent with KKL policies. Thinking Outside the Blue Box |Ishay Rosen-Zvi |April 9, 2012 |DAILY BEAST 

Each boasts its full complement of saints, whose congruent catalogues are equally wearisome in length. The Soul of the Far East |Percival Lowell 

Some objects when thought of are congruent to our existing state of activity. Human Nature and Conduct |John Dewey 

They are not congruent and can never be in the actualized universe. The Mystery of Space |Robert T. Browne 

Genera; almost completely congruent; the Nymphalideous genera can be based on the structure of the larv, 444. Studies in the Theory of Descent (Volumes 1 and 2) |August Weismann 

The identity of quality between congruent segments is generally of this character. The Concept of Nature |Alfred North Whitehead