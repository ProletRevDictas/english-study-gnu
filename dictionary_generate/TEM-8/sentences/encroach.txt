As she saw Mehta lurking nearby, she asked Mehta not to encroach. A divisive Jets reporter, accused of bullying, loses his place on the beat |Ben Strauss |December 11, 2020 |Washington Post 

That allows you to zip away from any cars that you think might be encroaching into your space, but it also means that you can scare yourself if you twist it too much. I rode an electric motorcycle for the first time. Here’s what I learned. |Rob Verger |December 10, 2020 |Popular-Science 

Confirmed cases of Lyme in the US have doubled since the ’90s, when housing developments increasingly encroached into rural areas and created patchy forest remnants. The fight to stop the next pandemic starts in the jungles of Borneo |Brian Barth |December 2, 2020 |Popular-Science 

That reality has been driven home repeatedly for legacy brands that have watched emerging direct-to-consumer brands encroach on their territory with innovative new models for connecting with and advertising to consumers. Marketers have successfully pivoted to first-party data, but struggle to deploy it |Adstra |December 2, 2020 |Digiday 

Nearby coastal states like Oregon and Washington are also seeing record blazes, some of which are encroaching on major population centers. “Unprecedented”: What’s behind the California, Oregon, and Washington wildfires |Umair Irfan |September 11, 2020 |Vox 

In the east, the Chinese army continues periodically to encroach on Indian territory. Farewell to Manmohan Singh, India’s Puppet Prime Minister |Kapil Komireddi |January 5, 2014 |DAILY BEAST 

Any attempt to encroach on it, even by an iota, will ultimately lead to our enslavement by a federal tyranny. Gun-Control Foes Misunderstand the Intent of the Second Amendment |Jack Schwartz |December 18, 2012 |DAILY BEAST 

How does this symbolize constraints on girls and women, and encroach on our right to be simply as we are, at any given moment? Ashley Judd Slaps Media in the Face for Speculation Over Her ‘Puffy’ Appearance |Ashley Judd |April 9, 2012 |DAILY BEAST 

He got really mad when they started to encroach on his personal life. Why Manny Ramirez Hates Fans |Will Doig |March 9, 2009 |DAILY BEAST 

The goods manager was not aggressive, and it was sometimes thought that Mathieson inclined to encroach upon his territory. Fifty Years of Railway Life in England, Scotland and Ireland |Joseph Tatlow 

He was suddenly ashamed, humbled, feeling in her love a quality upon which no shameful circumstance could encroach. Fidelity |Susan Glaspell 

Give Great Britain time to encroach and fortify upon all our frontiers? The Diplomatic Correspondence of the American Revolution, Vol. IV (of 12) |Various 

As the tent was found to encroach partly on the same crevasse, it may be imagined that we did not dally long over the meal. The Home of the Blizzard |Douglas Mawson 

Vines threatened to engulf the circling street of sepulchers in greenery and bloom, and grass to encroach on the flower plots. Greyfriars Bobby |Eleanor Atkinson