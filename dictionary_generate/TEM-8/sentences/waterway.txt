The location would be good, especially if the Port and the California Coastal Commission permit the developer to cut a new waterway from the bay to the tower’s base. San Diego’s Sky-High Dreams Keep Getting Quashed |Randy Dotinga |February 2, 2021 |Voice of San Diego 

Another 12 percent has been burned, and almost 80 percent has piled up on land or in waterways. Chemists are reimagining recycling to keep plastics out of landfills |Maria Temming |January 27, 2021 |Science News 

In their natural habitat, hippos spend the long dry season crowded into waterways that have shrunk to puddles. Invasion of the hippos: Colombia is running out of time to tackle Pablo Escobar’s wildest legacy |Sarah Kaplan |January 11, 2021 |Washington Post 

Numerous global gatherings have celebrated the Whanganui River and other waterways that are now “persons.” Humans Have Rights and So Should Nature - Issue 94: Evolving |Grant Wilson |January 6, 2021 |Nautilus 

He and his team found 6PPD-quinone at toxic concentrations in previously collected samples of roadway runoff and contaminated waterways collected around Seattle, San Francisco, and Los Angeles. Salmon are dying off and your car tires might be to blame |Kate Baggaley |December 4, 2020 |Popular-Science 

In the 1880s, the body of an unknown young woman was found floating in the Paris waterway. Brooklyn’s Museum of Death: Inside Morbid Anatomy’s House of Intriguing Horrors |Nina Strochlic |July 10, 2014 |DAILY BEAST 

To accompany this fine duo, we order sausage made in Hancock, just the other side of the Keweenaw Waterway. Welcome to Yooperland, A Little Slice of Finland in Michigan |Jane & Michael Stern |May 11, 2014 |DAILY BEAST 

Sewage running from their base has long polluted the waterway, but the authorities never paid attention. United Nations Still Denies its Troops Brought Cholera to Haiti |Jonathan M. Katz |April 4, 2012 |DAILY BEAST 

This waterway was recently discovered, and by it there is a better route to Terrenate than was formerly followed. The Philippine Islands, 1493-1898, Volume XX, 1621-1624 |Various 

The narrowness of the Isthmus naturally suggested the cutting of a waterway through it. The Wonder Book of Knowledge |Various 

With its expansions, the narrow and deep Arrow lakes, it is an important waterway in the Kootenay region. Encyclopaedia Britannica, 11th Edition, Volume 5, Slice 2 |Various 

The black blur made by the wagon stockade and a tent or so was visible against the lighter line of the waterway of the Platte. The Way of a Man |Emerson Hough 

This narrow waterway that passes down between Asia on the one side and Africa on the other is stimulating to the imagination. Round the Wonderful World |G. E. Mitton