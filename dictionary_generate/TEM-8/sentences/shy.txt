Just shy of a month from election day, with the earliest mail-in ballots beginning to be counted, Florida has done it again. A crashed voter registration website is Floridians’ latest obstacle to the right to vote |Sara Morrison |October 9, 2020 |Vox 

Stewart, who matter-of-factly declared that she wanted to win four championships in college, then did so, never shies away from acknowledging her own level of play. The Seattle Storm’s Dynamic Duo Is On The Brink Of Another Title |Howard Megdal |October 6, 2020 |FiveThirtyEight 

Symbolically, I rang the bell two days shy of ringing in 2020 — entering a new year having successfully completed treatment. The Anatomy Of A Breast Cancer Survivor: ‘Early Detection Saved My Life’ |Charli Penn |October 6, 2020 |Essence.com 

Some allies of Peters have sent warnings to Democratic leaders that if they do not shore up what should be an easy victory, they could fall shy of the majority. Republicans face major head winds in final stretch to maintain Senate majority |Rachael Bade, Paul Kane |October 5, 2020 |Washington Post 

He came up two assists shy of a triple-double and, at 9-of-16, was efficient shooting the ball. Jimmy Butler Outplayed LeBron James And Bought Miami Some Breathing Room |Chris Herring (chris.herring@fivethirtyeight.com) |October 5, 2020 |FiveThirtyEight 

His peers remember him as a bright man who spoke softly and occasionally came across as a bit shy. Mexico’s Priests Are Marked for Murder |Jason McGahan |January 7, 2015 |DAILY BEAST 

Sabrine was the outgoing, sociable type, and had many friends, while Ziad was shy and a little more introverted. A Sunni-Shia Love Story Imperiled by al Qaeda |Ruth Michaelson |December 26, 2014 |DAILY BEAST 

When Tonie Tobias started at Delta in 1996 she was shy and closeted. How A Company’s Support of Gay Employees Helped One of Them To Come Out | |December 24, 2014 |DAILY BEAST 

A Spaniard by birth, Victor Serna left home shy of his 14th birthday and entered the monastery to become a Marist brother. Obama’s One Hand Clap With Castro |Doug McIntyre |December 24, 2014 |DAILY BEAST 

Not ones to shy away from a fight, the Sailor Senshi defend their leader to the death. ‘Sailor Moon’ Is an Oasis for Superheroes Who Can Save the Universe in Heels |Rich Goldstein |November 26, 2014 |DAILY BEAST 

Possibly, he would not shy at such monstrosities after twenty miles of a lathering ride. The Red Year |Louis Tracy 

He gave me some instructions, but I was too confused to understand them, and too shy to ask questions. Fifty Years of Railway Life in England, Scotland and Ireland |Joseph Tatlow 

But Mrs. Charmington was already on the wane, and as he had no wish to be her hero now he rather fought shy of her. The Pit Town Coronet, Volume I (of 3) |Charles James Wills 

After this Aristide learned much of her simple history, which she, at first, had been too shy to reveal. The Joyous Adventures of Aristide Pujol |William J. Locke 

Rosemary, shy but happy, began giving out the toys, diving with both hands at once into the baskets which the fairy father held. Rosemary in Search of a Father |C. N. Williamson