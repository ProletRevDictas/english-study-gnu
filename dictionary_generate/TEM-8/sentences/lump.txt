You can lump unemployment policies into two broad categories—the US-style efforts that provide payments to people who have lost their jobs, and policies popular in Europe that provide money to companies to keep workers on the payroll. Job markets in the US and Europe are surprisingly similar |Dan Kopf |September 16, 2020 |Quartz 

Unlike spoons, spurtles allegedly don’t drag and prevent lumps. In Pursuit of the Perfect Bowl of Porridge |Clarissa Wei |September 11, 2020 |Eater 

That arrangement might look something like paying a lump sum, Shevchuck suggests, but Moran argues “in my experience, arrangements to collect money from employees who have departed are inherently problematic.” As Trump’s payroll tax holiday kicks in, here’s what employers and employees need to know |Anne Sraders |September 1, 2020 |Fortune 

Upon closer inspection of a big lump of bones in the creature’s belly, Motani’s team discovered that the last thing the ichthyosaur ate was the body of a thalattosaur, sans head and tail. This ichthyosaur died after devouring a creature nearly as long as itself |Maria Temming |August 20, 2020 |Science News 

A pair of changes being rolled out to Apple’s operating systems has publishers lumping the device maker into that group as well. ‘A shady move’: Apple News+ Safari change automatically redirecting traffic to itself infuriates publishers |Max Willens |August 12, 2020 |Digiday 

French President François Hollande is telling the French people they should “not lump them together.” France’s Wave of Crazy-Terror Christmas Attacks |Christopher Dickey |December 24, 2014 |DAILY BEAST 

If the Americans are going to lump them together with ISIS, maybe best to join forces. ISIS and Al Qaeda Ready to Gang Up on Obama's Rebels |Jamie Dettmer |November 11, 2014 |DAILY BEAST 

The one-time anti-bullying champion let his attorney seek to lump the victim together with the victimizer. Ray Rice Should Have Remembered His 'Kindness' Anti-Bullying Wristband |Michael Daly |September 10, 2014 |DAILY BEAST 

One morning in late December, Sclove told me she awoke to discover a lump on her lower spine. Is Sex Assault a Crime in the Ivy League? |Olivia Nuzzi |May 10, 2014 |DAILY BEAST 

The lump, it turned out, was the result of a dislocated vertebrae. Is Sex Assault a Crime in the Ivy League? |Olivia Nuzzi |May 10, 2014 |DAILY BEAST 

Besides a few crumbs, it contained a small lump of narwhal blubber and a little packet. The Giant of the North |R.M. Ballantyne 

Now Isaias had ordered that they should take a lump of figs, and lay it as a plaster upon the wound, and that he should be healed. The Bible, Douay-Rheims Version |Various 

He gulped back the lump in his throat; his trembling nerves became as steel. The Courier of the Ozarks |Byron A. Dunn 

Furs should be kept in a box, alone, and in summer carefully packed, with a quantity of lump camphor to protect from moths. The Ladies' Book of Etiquette, and Manual of Politeness |Florence Hartley 

There was a lump in Perry's throat at that moment, and he stopped his rocking and turned to the fire, so his back was toward me. The Soldier of the Valley |Nelson Lloyd