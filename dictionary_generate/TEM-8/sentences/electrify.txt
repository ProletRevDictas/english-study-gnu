Millions of consumers have electrified their cars and appliances, and many cities and states are electrifying public services such as public transportation — but these activities still draw most of their charge from dirty power plants. One policy that could challenge a century of fossil fuel dominance |Rebecca Leber |August 25, 2021 |Vox 

Here’s what to know about how these super-heavy electric garbage trucks work, and why electrifying refuse vehicles in cities like New York isn’t a stinky idea. Electric garbage trucks are the quiet, clean titans of waste collection |Rob Verger |August 18, 2021 |Popular-Science 

It’s a film filled with electrifying footage of some of Ailey’s masterworks, giving us a rare opportunity to revel in the sheer visual poetry of his style. Dance icon gets luminous treatment in ‘Ailey’ doc |John Paul King |July 28, 2021 |Washington Blade 

Resources like these are in high demand for building technologies like new batteries, which will be crucial to electrifying our society. The Battle for the Ocean Floor |Sean Culligan |July 14, 2021 |Ozy 

Some states are considering incentives or even requirements to nudge fleets to electrify. Why the grid is ready for fleets of electric trucks |Casey Crownhart |July 9, 2021 |MIT Technology Review 

Directing her to disrobe, she would poach her in herbs in a hot tub and then literally electrify her. The Sexy Side of Maggie: How Thatcher Used Her Softer Quality |Gail Sheehy |January 11, 2012 |DAILY BEAST 

When he came out on the stage the applause was tremendous, and enough in itself to excite and electrify one. Music-Study in Germany |Amy Fay 

It would produce a splendid effect on the populace and would electrify the soldiers. Punch, Or The London Charivari, Volume 152, March 21, 1917 |Various 

To have been able to electrify his audience with the news of some startling discovery would have been pure joy for Asaph. Cy Whittaker's Place |Joseph C. Lincoln 

Electrify a smooth glass tube with a rubber, and hold a small feather at a short distance from it. Endless Amusement |Unknown 

The evils p. 267are so linked together that a shock given to any one would electrify the whole mass of evil. The Claims of Labour |Arthur Helps