The higher polynomials grow in degree, the more unwieldy they become. Mathematicians Resurrect Hilbert’s 13th Problem |Stephen Ornes |January 14, 2021 |Quanta Magazine 

That seems relatively straightforward, but throw local laws into the mix and the standards become a lot more unwieldy. Can CBD help you chill? Here’s what we know so far. |Purbita Saha |January 4, 2021 |Popular-Science 

Every election cycle, critics lament how unwieldy, ugly, or downright confusing the voting form is. Why it’s so hard to fix America’s poorly-designed election ballot |Anne Quito |October 20, 2020 |Quartz 

Investors flew from the Bay Area to Florida and signed strict non-disclosure agreements so they could strap on unwieldy prototypes. Magic Leap tried to create an alternate reality. Its founder was already in one |Verne Kopytoff |September 26, 2020 |Fortune 

I suspect Fitbit learned its lesson about unwieldy watch designs with the Ionic. Fitbit Sense review |Brian Heater |September 24, 2020 |TechCrunch 

But any biographer of the novel faces a problem more fundamental than compressing between two covers a vast and unwieldy subject. The Birth of the Novel |Nick Romeo |November 27, 2014 |DAILY BEAST 

Lachs writes with clarity and concision—admirable concision, considering how unwieldy university press offerings tend to be. America’s Meddlers Are Our Worst Enemies |Stefan Beck |October 3, 2014 |DAILY BEAST 

Turn all those unwieldy tabs into one manageable list with OneTab. Surf Better With These 9 Killer Google Chrome Extensions |Brian Ries |December 11, 2013 |DAILY BEAST 

The Wii U Gamepad, although it looks unwieldy at first, is actually great to use. Which Video Game System Is King? |Alec Kubas-Meyer |November 26, 2013 |DAILY BEAST 

Unwieldy trash piles or not, the coming treasure hunts are sure to be interesting ones. As Japan’s Tsunami Debris Arrives, Can U.S. West Coast Handle It? |Winston Ross |September 7, 2012 |DAILY BEAST 

But he found the weapon unwieldy, and he returned to his hotel a sadder man than he left it. The Pit Town Coronet, Volume I (of 3) |Charles James Wills 

His body had grown unwieldy from years and lack of exercise, and his clothes were old-fashioned and, generally, dusty. Ancestors |Gertrude Atherton 

His gunners pelted the unwieldy budgerows with round shot until they began to sink. The Red Year |Louis Tracy 

The women, likewise, are very tall, but too muscular—they might even be termed unwieldy. A Woman's Journey Round the World |Ida Pfeiffer 

The Castilians would doubtless have resented the dismemberment of the unwieldy body of which they formed the head. The History of England from the Accession of James II. |Thomas Babington Macaulay