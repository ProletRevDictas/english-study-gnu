Some side effects are expected and manageable, such as redness or swelling at the site of the injection, fever, aching muscles or joints, headaches or fatigue. Here’s what pausing the AstraZeneca-Oxford coronavirus vaccine trial really means |Aimee Cunningham |September 9, 2020 |Science News 

Year-round breaks, like the perfect swells at Praia de Santa Bárbara, attract surfers looking to avoid mainland Portugal’s crowds. 27 Epic Trips to Start Planning Now |The Editors |September 8, 2020 |Outside Online 

This could happen with a vaccine, leading to a swell of refusal among his political opponents. A third of Americans might refuse a Covid-19 vaccine. How screwed are we? |Brian Resnick |September 4, 2020 |Vox 

In a several years the entire network is expected to swell to 12,000 satellites, with a possible expansion to 42,000. Satellite mega-constellations risk ruining astronomy forever |Neel Patel |September 2, 2020 |MIT Technology Review 

If there’s a big swell, the dramatic ending of The Firebird, you know, you’re right in the middle of it. Conducting the Mathematical Orchestra From the Middle |Rachel Crowell |September 2, 2020 |Quanta Magazine 

It stands to reason the controversy will swell after the release. Kirk Cameron Saves Christmas from Abominable Killjoys (Other Christians) |Brandy Zadrozny |November 14, 2014 |DAILY BEAST 

But when I see these sorts of lies in Iranian media, my eyes swell up with tears and I start shaking. Iran Says Take Off the Veil—and Be Raped |IranWire |June 9, 2014 |DAILY BEAST 

I kept holding out hope that he would say, ‘You know, this is pretty swell here and the work is fulfilling.’ ‘The Good Wife’s Christine Baranski on Life After Will Gardner’s Death |Jason Lynch |April 21, 2014 |DAILY BEAST 

Internal organs like the spleen swell up and become as hard as sausages. Already Deadly in Africa, Could Ebola Hit America Next? |Scott Bixby |April 5, 2014 |DAILY BEAST 

“It would be a swell joke on tout-le-monde if you & Fife & I spent the summer at Juan-les-Pins,” she wrote. The Perils of Being a Hemingway Wife |Nico Hines |February 23, 2014 |DAILY BEAST 

Not only did he provide sub-octave and super-octave couplers freely, but he even added a Swell Sub-quint to Great coupler! The Recent Revolution in Organ Building |George Laing Miller 

It is not exactly so, but is still very different to the gradual swell on the other Cremona instruments. Violins and Violin Makers |Joseph Pearce 

The introduction of the balanced Swell pedal (Walcker, 1863) has greatly increased the tonal resources of the organ. The Recent Revolution in Organ Building |George Laing Miller 

The position of the swell shutters is brought under the control of the organist's fingers as well as his feet. The Recent Revolution in Organ Building |George Laing Miller 

To this general swell pedal (and its corresponding indicator key) any or all of the other swell pedals may be coupled at will. The Recent Revolution in Organ Building |George Laing Miller