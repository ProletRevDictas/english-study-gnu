An infection progresses to disease when the cells in the body become damaged by the invading germs, which can lead to symptoms of an illness appearing. Vaccine terms explained: Efficacy vs. effectiveness, herd immunity and others |Allyson Chiu |February 4, 2021 |Washington Post 

Still others have worried that the forum has been invaded, surveilled or compromised by the Wall Street giants they pledged to fight all along. As GameStop stock crumbles, newbie traders reckon with heavy losses |Drew Harwell |February 2, 2021 |Washington Post 

Two weeks ago, rioters invaded the Capitol, the first time such a thing had happened since British forces tried to burn it down. Inauguration Day was a milestone, but it’s not the destination |Monica Hesse |January 20, 2021 |Washington Post 

These vigilantes understand that they should never invade the capitol again now that they’ve been threatened with a peaceful night at their hotels. A Week After the U.S. Capitol Attack, Many Involved Are Still Walking Free Despite Online Efforts to Identify Them |Megan McCluskey |January 13, 2021 |Time 

The scientists looked for antibodies and immune cells that could recognize and react to different fragments of the virus, particularly the spike-shaped protein on the surface of SARS-CoV-2 that helps it invade host cells. COVID-19 immunity could be long term |Kate Baggaley |January 8, 2021 |Popular-Science 

When they invade new territory, populations are low, and the queen has limited mate options. Mongooses, Meerkats, and Ants, Oh My! Why Some Animals Keep Mating All in the Family |Helen Thompson |December 29, 2014 |DAILY BEAST 

On August 9, 1969, Manson sent four of his disciples to invade the home of film director Roman Polanski, who was away on a shoot. Gay Talese on Charlie Manson’s Home on the Range |Gay Talese |October 31, 2014 |DAILY BEAST 

As they once again invade the safety of the prison that the group calls home, Rick is forced to take up the proverbial sword. The Walking Dead’s Luke Skywalker: Rick Grimes Is the Perfect Modern-Day Mythical Hero |Regina Lizik |October 28, 2014 |DAILY BEAST 

Why do celebrities complain about their privacy being invaded when they invade their own so readily? Celebrities, STFU About Your ‘Privacy’ |Tim Teeman |September 24, 2014 |DAILY BEAST 

But then the conversation turns to the question occupying all minds in Ukraine: Will Putin invade? In War-Torn Ukraine, Savva Libkin's Delicious Recipes for Survival |Anna Nemtsova |August 12, 2014 |DAILY BEAST 

Mine host will no doubt be wiser inPg 50 this particular as motorists more and more invade the country. British Highways And Byways From A Motor Car |Thomas D. Murphy 

When blown sands invade a forest and the deposit is rapidly accumulated, the trees are often buried in an undecayed condition. Outlines of the Earth's History |Nathaniel Southgate Shaler 

Invade the land; it belongs to him whose strength or skill obtains possession of it. A Philosophical Dictionary, Volume 1 (of 10) |Franois-Marie Arouet (AKA Voltaire) 

Children, therefore, who invade the premises of a person without any right are trespassers like older people. Putnam's Handy Law Book for the Layman |Albert Sidney Bolles 

Bonaparte joined his great army on the Vistula, destined to invade Moscow. The Every Day Book of History and Chronology |Joel Munsell