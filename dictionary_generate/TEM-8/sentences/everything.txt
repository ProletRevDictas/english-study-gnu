To make it work almost everything else about these shows has to seem factual which is why many look like a weird Celebrity Sims. ‘Empire’ Review: Hip-Hop Musical Chairs with an Insane Soap Opera Twist |Judnick Mayard |January 8, 2015 |DAILY BEAST 

And so he looked at her, smiled, and offered a polite “Is everything okay?” Powerful Congressman Writes About ‘Fleshy Breasts’ |Asawin Suebsaeng |January 7, 2015 |DAILY BEAST 

Note: UNICOR uses its inmates for everything from call center operators to human demolishers of old computers. How a ‘Real Housewife’ Survives Prison: ‘I Don’t See [Teresa Giudice] Having a Cakewalk Here’ |Michael Howard |January 6, 2015 |DAILY BEAST 

“The government just wanted to catch the big fish [in the Juarez cartel] and they ignored everything in between,” Lozoya said. An Informant, a Missing American, and Juarez’s House of Death: Inside the 12-Year Cold Case of David Castro |Bill Conroy |January 6, 2015 |DAILY BEAST 

Do as Tumblr has done and scrub her last words off the Internet—erase everything she wanted the world to hear. Cover-Ups and Concern Trolls: Actually, It's About Ethics in Suicide Journalism |Arthur Chu |January 3, 2015 |DAILY BEAST 

So after a few minutes I remarked to him, "Everything tastes very sweet out of this spoon!" Music-Study in Germany |Amy Fay 

He wanted to tell her that if she called her father, it would mean the end of everything for them, but he withheld this. The Homesteader |Oscar Micheaux 

Everything is topsy-turvy in Europe according to our moral ideas, and they don't have what we call "men" over here. Music-Study in Germany |Amy Fay 

She never realized that the reserve of her own character had much, perhaps everything, to do with this. The Awakening and Selected Short Stories |Kate Chopin 

I should like it to be used for Maude; and mind, I'll see to everything; you need not give yourself any trouble at all. Elster's Folly |Mrs. Henry Wood