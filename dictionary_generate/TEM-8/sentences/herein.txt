Herein lies the great dilemma then for the advocates of amnesty. Legal but Still Poor: The Economic Consequences of Amnesty |Joel Kotkin |November 21, 2014 |DAILY BEAST 

There are 17 processes herein, which collectively take four years for a craftsman to master. Behind the Wheel of the Bespoke Bentley |Zoe Settle |October 27, 2014 |DAILY BEAST 

Clarence X accepted the obvious truths herein, but could not truck with the association of Fard with god. Word Is Bond: An Ex-Con Explains the 5 Percenters |Daniel Genis |April 12, 2014 |DAILY BEAST 

Herein lies the paradigm shift to a just and sustainable peace and an integration of Israel into the region. It's Time to Think Creatively About Getting Settlers on the Side of Peace |Dan Goldenblatt |October 18, 2013 |DAILY BEAST 

Just the opposite, they want to strengthen Torah…and herein lies the great danger. Who’s Afraid Of Ruth Calderon? |Sigal Samuel |February 14, 2013 |DAILY BEAST 

Herein he found an empty stall that was dark enough not to be seen, and still afforded sufficient light to read in. The Homesteader |Oscar Micheaux 

Will your Majesty order what shall be most suitable for this particular, and for whatever else is mentioned herein. The Philippine Islands, 1493-1898, Volume XX, 1621-1624 |Various 

An English writer could very appropriately call this a cloud of smoke as he has another scene herein described. Tobacco; Its History, Varieties, Culture, Manufacture and Commerce |E. R. Billings. 

Herein is discovered the difference between violins of the three great masters named and others of the same name. Violins and Violin Makers |Joseph Pearce 

At the commencement of the period herein spoken of string-toned stops as we know them to-day scarcely existed. The Recent Revolution in Organ Building |George Laing Miller