The pandemic has served to highlight the two Americas King spoke of, but the pandemic has also exacerbated the gap, largely through something sociologists call “opportunity hoarding” — or the accumulation of resources at the exclusion of others. COVID-19 Reminded Us Of Just How Unequal America Is |Neil Lewis Jr. (nlewisjr@cornell.edu) |March 29, 2021 |FiveThirtyEight 

“This inequity is due to hoarding of doses by rich nations.” Bringing COVID-19 vaccines to much of world is hard |Jonathan Lambert |March 16, 2021 |Science News For Students 

The mask debacle was about officials trying to prevent the hoarding of N95s when hospitals needed them. How To Convince People The Johnson & Johnson Vaccine Is As Good As The Others |Chadwick Matlin (chadwick.matlin@fivethirtyeight.com) |March 10, 2021 |FiveThirtyEight 

The policy will hopefully prevent hoarding and help more people get their first and second doses on time. Ramping up COVID-19 vaccine production is harder than it seems |Kat Eschner |February 19, 2021 |Popular-Science 

This hoarding by rich countries means that people in the poorest countries will be waiting many, many months, and likely years, before they can get a Covid-19 vaccine dose. Don’t hate the vaccine tourists, hate the vaccine game |Jen Kirby |February 12, 2021 |Vox 

Infomania, they say, is more subtly crippling than physical hoarding. I’m a Digital Hoarder |Lizzie Crocker |December 17, 2014 |DAILY BEAST 

Panicked, I reached out to hoarding experts, who often refer to any kind of obsessive digital collecting as “infomania.” I’m a Digital Hoarder |Lizzie Crocker |December 17, 2014 |DAILY BEAST 

Perhaps I should be more understanding, now that my own hoarding tendencies are flaring up. I’m a Digital Hoarder |Lizzie Crocker |December 17, 2014 |DAILY BEAST 

But in the Digital Age, we're at risk of a new type of hoarding that is equally problematic. I’m a Digital Hoarder |Lizzie Crocker |December 17, 2014 |DAILY BEAST 

The financial system is awash with money, yet the Federal Reserve accuses both consumers and institutions of hoarding it. How Young People Are Destroying Liberty |James Poulos |October 11, 2014 |DAILY BEAST 

She could not see the word Putney posted on a hoarding without a stirring of the spirit and a beating of the heart. The Creators |May Sinclair 

When the vernal or autumnal storms delay to break, they are gathering strength; hoarding up their fury for more sure destruction. Toilers of the Sea |Victor Hugo 

And until the very eve of victory, we treated Handitch not so much as a battlefield as a hoarding. The New Machiavelli |Herbert George Wells 

It looks out upon you—the word again, not the quality—from every hoarding. American Sketches |Charles Whibley 

Then, lest he become a miser hoarding gold and spending it not, Sweep at last bethought him of a kindly plan. The Green Forest Fairy Book |Loretta Ellen Brady