Robotics will continue to advance its capabilities, and will take over more human jobs as it does so, but it’s unlikely we’ll hit a dramatic inflection point that could be described as a “revolution.” Is the Pandemic Spurring a Robot Revolution? |Marius Robles |November 30, 2020 |Singularity Hub 

“It’s almost exponential when you compare the curves in the spring and the curves in the summer with the inflection of the curve where we are right now,” Fauci said. Will covid-19 make this the Turkey Day without political fights? |Petula Dvorak |November 23, 2020 |Washington Post 

Withiam says an inflection point could also arrive if an industry giant like Blizzard Entertainment decided to use a tool like Flow. The blockchain industry faces a moment of truth as high-profile projects go live |Jeff |October 21, 2020 |Fortune 

We’re at a very serious inflection point in the history of this country. Elijah Cummings has a message for voters ‘from beyond the grave’ |Nicole Goodkind |October 21, 2020 |Fortune 

Companies like Starz are already near that inflection point, and Disney et al. Major media reorganizations aim to bridge the divide to companies’ streaming futures |Tim Peterson |October 14, 2020 |Digiday 

Recognizing that things have truly changed has always been difficult for those living through inflection points in history. Our Ruling Ideology Is Denial |Eugene Linden |May 12, 2014 |DAILY BEAST 

The obvious inflection point in the show occurs with the jump from icons of the 40s and 50s, to those of the 60s and 70s. The 100 Coolest Americans Gather at the National Portrait Gallery |William O’Connor |February 7, 2014 |DAILY BEAST 

For a president who believes in playing the long game, this was an inflection point. Congress Cooperates, Obama Pushes Hard, and Closing Gitmo Has a Chance |Daniel Klaidman |December 12, 2013 |DAILY BEAST 

In each role he seemed to be behaving, not acting; every gesture and inflection was instinctive. River Phoenix’s Fatal Halloween, 20 Years On |Andrew Romano |October 31, 2013 |DAILY BEAST 

With the spoken word, we use our tone, inflection and volume to question, exclaim and convey our feelings. The Rise and Fall of the Infamous SarcMark |Keith Houston |September 24, 2013 |DAILY BEAST 

Practise gliding in the form of inflection, or slide, from one extreme of pitch to another. Expressive Voice Culture |Jessie Eldridge Southwick 

"The old king" and "this one" they say with an inflection of voice anything but flattering to the latter. Lippincott's Magazine of Popular Literature and Science |Various 

The name signifies little thunder, being a compound from Annimikee, thunder, and the diminutive inflection in us. The Indian in his Wigwam |Henry R. Schoolcraft 

The distinction between the active and passive voice, in the Odjibwa language, is formed by the inflection ego. The Indian in his Wigwam |Henry R. Schoolcraft 

Perhaps it was the derisive inflection on "book agent" that woke Albert. Wayside Courtships |Hamlin Garland