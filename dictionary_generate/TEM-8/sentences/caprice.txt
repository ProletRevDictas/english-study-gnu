It makes you realize how insignificant we humans are, how little our designs count compared with the caprices of nature. The torment of hope in the time of the plague |Max Boot |December 20, 2021 |Washington Post 

Men's probable actions are calculated by the law of reason; but their performance is usually the result of caprice. The Pastor's Fire-side Vol. 3 of 4 |Jane Porter 

He was an outlaw, hunted and despised, depending for his life on the caprice of a fickle-minded woman. The Red Year |Louis Tracy 

The entertainment upon such occasions, may vary with the taste of the hostess, or the caprice of her guests. The Ladies' Book of Etiquette, and Manual of Politeness |Florence Hartley 

Now and then, from caprice, one was liberated; but the innocent and the guilty fell alike. Madame Roland, Makers of History |John S. C. Abbott 

All this gave him hope, and he knew, that when caprice permitted, she would be unrivalled as a companion. Ancestors |Gertrude Atherton