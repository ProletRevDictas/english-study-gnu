I’ve spent a lot of time since in utter disbelief at how wrong those assumptions turned out to be. Rep. Watson Coleman: I’m 75. I had cancer. I got covid-19 because my GOP colleagues dismiss facts. |Bonnie Watson Coleman |January 12, 2021 |Washington Post 

The contrast shook Washington’s attorney general, Karl Racine, who seemed to be almost in disbelief on CNN Wednesday evening. Capitol Rioters Planned for Weeks in Plain Sight. The Police Weren’t Ready. |by Logan Jaffe, Lydia DePillis, Isaac Arnsdorf and J. David McSwane |January 7, 2021 |ProPublica 

Haley Stevens, a Democratic representative of Michigan, expressed her disbelief. Congress members under siege in Capitol tweet their shock and frustration |Danielle Abril |January 6, 2021 |Fortune 

Fields lifted his hands to his helmet in disbelief after a miscommunication with his receiver led to Clemson’s game-sealing interception. Justin Fields outduels Trevor Lawrence as Ohio State upsets Clemson in playoff semifinal |Emily Giambalvo, Kareem Copeland |January 2, 2021 |Washington Post 

In online chat groups and forums, political rage and disbelief metastasizes into calls for violence. D.C. is becoming a protest battleground. In a polarized nation, experts say that’s unlikely to change. |Marissa Lang |January 1, 2021 |Washington Post 

The disbelief was evident in article after article, with one conservative site using “President Pinocchio” in its headline. Obama Is Right on Race. The Media Is Wrong. |Keli Goff |December 29, 2014 |DAILY BEAST 

But, even given the necessary suspension of disbelief, does it work? Daphne Merkin on Lena Dunham, Book Criticism, and Self-Examination |Mindy Farabee |December 26, 2014 |DAILY BEAST 

In fact, enjoying the show takes an extreme dedication to suspending disbelief. I Want to See Your Spreadsheets, Baby: MTV’s ‘Are You the One?’ Is a Mathematical Orgy |Brandy Zadrozny |December 9, 2014 |DAILY BEAST 

This may be precisely the point: that fiction at its best is a sphere of suspended belief as much as suspended disbelief. The Birth of the Novel |Nick Romeo |November 27, 2014 |DAILY BEAST 

Daniels laughs at himself, once again shaking his head in disbelief. Jeff Daniels Defends Aaron Sorkin and the ‘Dumb and Dumber’ Toilet Scene |Kevin Fallon |November 7, 2014 |DAILY BEAST 

He hid himself in Assouan with belief for a companion, he came back and found that companion had been but a masquerader—disbelief. The Wave |Algernon Blackwood 

So atheism, and the disbelief of the existence of the soul after death, characterized that materialism. Beacon Lights of History, Volume I |John Lord 

When a liar speaks the truth he finds his punishment in the general disbelief. Hebraic Literature; Translations from the Talmud, Midrashim and Kabbala |Various 

With or amongst the Romanists to leave the shore is an act of disbelief which must be atoned for by penance or punishment. Ancient Faiths And Modern |Thomas Inman 

I stared at him in disbelief and said, "Oh, Mr. Spardleton, this is no time to play games with me." The Professional Approach |Charles Leonard Harness