Porter pointed out that Celgene had a structure set up where executive compensation was tied to sales goals for Revlimid. Bristol Myers Squibb and Celgene got a beating in Congress on drug prices—but only Congress can fix the problem |Sy Mukherjee |October 1, 2020 |Fortune 

Yet in the past two years, these stunning exchanges at congressional hearings have themselves gained plenty of attention beyond Capitol Hill — especially when Porter pulls out what one person on Twitter dubbed “her mighty whiteboard of truth.” ‘Mighty whiteboard of truth’: The weapon Rep. Katie Porter used to school a pharma exec — and plenty others |Teo Armus |October 1, 2020 |Washington Post 

My favorite Porter hit, “Anything Goes,” was performed live at my request. Escape to Indy for a weekend getaway |Bill Malcolm |September 4, 2020 |Washington Blade 

Gehl and Porter argue that the political industry has essentially co-opted the media, which spreads their messages for free. America’s Hidden Duopoly (Ep. 356 Rebroadcast) |Stephen J. Dubner |September 3, 2020 |Freakonomics 

We should note that most of the ideas Gehl and Porter are presenting here are not all that novel if you follow election reform even a little bit. America’s Hidden Duopoly (Ep. 356 Rebroadcast) |Stephen J. Dubner |September 3, 2020 |Freakonomics 

Coded references to risqué and sexual matters were catnip to the lyricists Lorenz Hart and Cole Porter. When Broadway Musicals Were Dark And Subversive |Laurence Maslon |December 16, 2014 |DAILY BEAST 

But while that level of ‘haute couture’ is building the market, we want to be seen as more prêt-à-porter. William, Kate, and Jay Z’s Favorite Art Star: Alexander Gilkes' World of Rock Stars and Royalty |Tim Teeman |December 10, 2014 |DAILY BEAST 

Porter was convicted and shortly after sentenced to death by a judge who compared him to a shark in a feeding frenzy. Wrongly Imprisoned for 15 Years Thanks to an Innocence Project |Jacob Siegel |November 13, 2014 |DAILY BEAST 

The police found six eyewitnesses who reported seeing Anthony Porter at the pool that night and named him as the killer. Wrongly Imprisoned for 15 Years Thanks to an Innocence Project |Jacob Siegel |November 13, 2014 |DAILY BEAST 

At that time, with the eyewitnesses all pointing at Porter, the case seemed open and shut. Wrongly Imprisoned for 15 Years Thanks to an Innocence Project |Jacob Siegel |November 13, 2014 |DAILY BEAST 

The Café tender was asleep in his chair; the porter had gone off; the sentinel alone kept awake on his post. Glances at Europe |Horace Greeley 

To talk German was beyond the sweep of my dizziest ambition, but an Italian runner or porter instantly presented himself. Glances at Europe |Horace Greeley 

Arrived at the dépôt, I discharged my porter, sat down and waited for the place to open, with ample leisure for reflection. Glances at Europe |Horace Greeley 

Poindexter ordered his men to fall in, and they followed Porter, but at a more leisurely gait. The Courier of the Ozarks |Byron A. Dunn 

Porter and Poindexter are within eleven miles of the place, and Duffield expects to be attacked by morning. The Courier of the Ozarks |Byron A. Dunn