It called for him to write a poem using a simile or metaphor. A former cop created a program to help Baltimore kids. Now, she’s hoping to give them more: a permanent safe haven. |Theresa Vargas |August 25, 2021 |Washington Post 

He surprises readers with similes that are sobering, in the middle of laughter. Author recalls beatings, discrimination in memoir |Terri Schlichenmeyer |June 3, 2021 |Washington Blade 

The matron expressed her entire concurrence in this intelligible simile, and the beadle went on. Oliver Twist, Vol. II (of 3) |Charles Dickens 

The modern simile is that of a donkey between two bundles of hay. Chaucer's Works, Volume 1 (of 7) -- Romaunt of the Rose; Minor Poems |Geoffrey Chaucer 

No one can say to himself, “I will now make a good simile,” and straightway fulfill his promise. English: Composition and Literature |W. F. (William Franklin) Webster 

A simile is an expressed comparison between unlike things that have some common quality. English: Composition and Literature |W. F. (William Franklin) Webster 

Here the simile seems to be as unlike as possible, for the lot could fall only upon one. Blackwood's Edinburgh Magazine, Volume 67, Number 414, April, 1850 |Various