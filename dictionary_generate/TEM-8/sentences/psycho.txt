The new year’s first blockbuster novel is “The Push,” by Ashley Audrain, a psychological suspense tale about a mother’s fears that her pre-school-age daughter may be a psycho killer. Why is ‘The Push’ so popular? Perhaps because it plays into a mother’s worst fears. |Maureen Corrigan |January 21, 2021 |Washington Post 

When, in succession, he made Vertigo (1958), North by Northwest (1959), Psycho (1960), and The Birds (1963). Alfred Hitchcock’s Fade to Black: The Great Director’s Final Days |David Freeman |December 13, 2014 |DAILY BEAST 

In Psycho a psychiatrist (the young Simon Oakland) tells us in clinical terms what we've seen. Alfred Hitchcock’s Fade to Black: The Great Director’s Final Days |David Freeman |December 13, 2014 |DAILY BEAST 

To bolster my case I told him we should actually call it Pursuito, like Vertigo or Psycho. Alfred Hitchcock’s Fade to Black: The Great Director’s Final Days |David Freeman |December 13, 2014 |DAILY BEAST 

He is the author of Broken Glass, Memoirs of a Porcupine, and African Psycho, among others. Living Black & Gay in the ’50s |Alain Mabanckou |December 3, 2014 |DAILY BEAST 

Because there was plenty more psycho and much more drama on the way. Hell Hath No Fury Like Valerie Trierweiler, the French President’s Ex |Lizzie Crocker |November 28, 2014 |DAILY BEAST 

But there is a wide gulf between that and concluding that all psycho-biological phenomena are hallucinations. Urania |Camille Flammarion 

That caution about 'heightened psycho-physiological effects,' that we were never able to understand! Hunter Patrol |Henry Beam Piper and John J. McGuire 

We want to work out a substitute for Beta that will keep the flavor of the drink without the psycho-physiological effects. Hunter Patrol |Henry Beam Piper and John J. McGuire 

When Dr. Martin first introduced him into the psycho-recovery room his resolution almost vanished. The Memory of Mars |Raymond F. Jones 

If we are to do our work properly, we must base it completely upon modern psycho physical fundamentals. Criminal Psychology |Hans Gross