I told him, I thought if I kept making enough excuses, he would probably get the clue. Junior Staffer Says Top Alaska Official Told Her to Keep Allegations of Misconduct Secret |by Kyle Hopkins, Anchorage Daily News |November 18, 2020 |ProPublica 

Coach Bill Belichick has spoken publicly about the team’s approach to the salary cap in recent years leaving the Patriots strapped this season for cap room, leading to follow-up questions about whether Belichick was making excuses. The Patriots’ season isn’t fixed, but at least they avoided losing to the Jets |Mark Maske |November 10, 2020 |Washington Post 

In the latest edition of our Confessions series, in which we trade anonymity for candor, we hear from one agency exec who says that the “times are tough” excuse isn’t cutting it anymore. ‘How much do we want to get screwed?’: Confessions of an agency exec on lack of payment due to coronavirus |Kristina Monllos |September 24, 2020 |Digiday 

The same was true of states like Louisiana and Texas, which still required voters to provide an excuse to vote absentee. There Have Been 38 Statewide Elections During The Pandemic. Here’s How They Went. |Nathaniel Rakich (nathaniel.rakich@fivethirtyeight.com) |August 3, 2020 |FiveThirtyEight 

As a result, only 19 percent of Americans believed that voters should need an excuse other than the pandemic to vote absentee. Americans Mostly Support Voting By Mail |Nathaniel Rakich (nathaniel.rakich@fivethirtyeight.com) |July 24, 2020 |FiveThirtyEight 

Why are Palestinians granted a license of bloodlust as an excusable remedy for their suffering? There Is No Moral Equivalent to the Murder of Three Israeli Teenagers |Thane Rosenbaum |July 2, 2014 |DAILY BEAST 

And what, exactly, would an excusable yearning be as opposed to an “inexcusable” one? Berlusconi Exits, and an Era of Sexist Buffoonery Is Over |Lawrence Osborne |November 17, 2011 |DAILY BEAST 

When you engaged yourself to the young woman you were poor and a nobody, and the step was perhaps excusable. Elster's Folly |Mrs. Henry Wood 

And certainly a Pastor is excusable who fails to do things of which he has no knowledge. The Jesuit Relations and Allied Documents, Vol. II: Acadia, 1612-1614 |Various 

Et de verit vn Pasteur est excusable qui manque faire chose dont il n'a connoissance. The Jesuit Relations and Allied Documents, Vol. II: Acadia, 1612-1614 |Various 

He is excusable; for how can a man whose digestion is just beginning understand that people could anywhere die of starvation. The 'Characters' of Jean de La Bruyre |Jean de La Bruyre 

This weakness was excusable, for the forest was growing very dark—lonely it always was—and full of strange sounds. Menotah |Ernest G. Henham