Apparently, Shakespeare coined 1,700 words, from the frequently used (excitement) to the should-be-more frequently used (spewed). Biking With the Bard |Kara Cutruzzula |December 28, 2014 |DAILY BEAST 

I never had to go through the should-I-stay-at-home conversation. The NYT’s Game of Thrones: How the Top Journalism Job Was Dangled at Guardian Editor |Lloyd Grove |May 16, 2014 |DAILY BEAST 

Most thought-provoking for me, however, in this should-have-been story is the moral at the end. Why Can't Our Politics Improve Like Our Medicine? |David Frum |February 23, 2013 |DAILY BEAST 

The problem is that Scott is unpopular—awkward, should-we-be-seen-with-him-in-public unpopular. 10 GOP Endorsements That Still Matter in 2012’s Presidential Election |John Avlon |December 7, 2011 |DAILY BEAST 

Less obvious: To what degree can/should advanced countries act unilaterally without waiting for a grand global bargain? The Ridiculous Global-Warming Freakout |Tunku Varadarajan |November 16, 2010 |DAILY BEAST 

They have a model of the man-as-he-should-be to which they mold him, in spite of himself and without his knowledge. Folkways |William Graham Sumner 

The type of the man-as-he-should-be varies by fashion, and this type exerts a great selection in the education of the young. Folkways |William Graham Sumner 

In mediæval society there were strongly defined ideals of the man-as-he-should-be. Folkways |William Graham Sumner 

Is the ideal of the man-as-he-should-be to be found, for us, in the "common man," or in the highest product of our culture? Folkways |William Graham Sumner 

But do you not see that though the King should-300- favour us, yet Amneris's rage would be beyond all bounds? Operas Every Child Should Know |Mary Schell Hoke Bacon