You know, what the law is looking at the antitrust statutes. America’s Hidden Duopoly (Ep. 356 Rebroadcast) |Stephen J. Dubner |September 3, 2020 |Freakonomics 

Our piece revealing that San Diego Police are ticketing people for what they say is seditious language using a 102-year-old city statute misstated the location in which one resident received a ticket. Morning Report: SANDAG Head Wades Into Supes Race |Voice of San Diego |August 14, 2020 |Voice of San Diego 

Some state criminal statutes could also allow San Diego’s district attorney to press charges. Audit Finds Sweetwater Officials Deliberately Manipulated Finances |Will Huntsberry |June 23, 2020 |Voice of San Diego 

It was the first substantial alteration to the statute in a century. Agencies Are Updating Policies to Comply With New Use-of-Force Standards |Jesse Marx |June 22, 2020 |Voice of San Diego 

Therefore, when the statute refers to sex, and that is the basis upon which the plaintiffs were discriminated against, originalism is not a factor. The Supreme Court Decision To Grant Protections To LGBT Workers Is An Important Expansion Of The Civil Rights Act |LGBTQ-Editor |June 18, 2020 |No Straight News 

Another bombshell: There is no statute of limitations on rape in the Commonwealth of Virginia. I Was Gang Raped at a UVA Frat 30 Years Ago, and No One Did Anything |Liz Seccuro |December 16, 2014 |DAILY BEAST 

Pennsylvania, where the assault is alleged to have taken place, has a 12-year statute of limitations on sexual assault. No Wonder Cosby's Keeping Quiet: He Could Still Be Prosecuted |Jay Michaelson |November 23, 2014 |DAILY BEAST 

According to the memo, Miller then asked what the statute of limitations in Illinois was and to define what the allegations meant. Chicago Priests Raped and Pillaged for 50 Years |Barbie Latza Nadeau |November 7, 2014 |DAILY BEAST 

It helped too that crime was no longer at the top of voter concerns, but the statute of limitations apparently has run out. And Here Come 2014’s Willie Hortons |Eleanor Clift |November 2, 2014 |DAILY BEAST 

In 2007, a Dorset man brought a lawn statute featuring a recognizable Egyptian headdress to an expert for evaluation. 7 Historically Significant Artifacts Rescued by Happenstance |The Daily Beast |October 24, 2014 |DAILY BEAST 

A member of parliament took occasion to make his maiden speech, on a question respecting the execution of a particular statute. The Book of Anecdotes and Budget of Fun; |Various 

Statute law or statutes mean the laws enacted by the state legislature and by the federal congress. Putnam's Handy Law Book for the Layman |Albert Sidney Bolles 

A claim barred by the statute of limitations is not provable, nor is a contingent liability. Putnam's Handy Law Book for the Layman |Albert Sidney Bolles 

Benefit societies may be purely voluntary associations or incorporated either by statute or charter. Putnam's Handy Law Book for the Layman |Albert Sidney Bolles 

Gainful corporations have no such power unless it has been granted by their charter or by statute. Putnam's Handy Law Book for the Layman |Albert Sidney Bolles