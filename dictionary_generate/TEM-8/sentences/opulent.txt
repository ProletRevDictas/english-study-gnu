Habré, who after losing power allegedly left Chad with more than $11 million, took exile in Senegal and lived in what were described as opulent conditions. Hissène Habré, former leader of Chad convicted of crimes against humanity, dies at 79 |Emily Langer |August 24, 2021 |Washington Post 

His latest mixes of WizDaWizard’s “Don Dada’s” and Wam SpinThaBin’s “Risk Taker” both feel opulent and jewel-like as they accelerate with weird stealth. Florida’s ‘fast’ rap remixes are speeding up another endless summer |Chris Richards |July 22, 2021 |Washington Post 

Shadow and Bone’s opulent settings and production design work against it, because the story isn’t rich enough to fill it. Netflix’s Shadow and Bone is a muddled, joyless checklist of fantasy tropes |Constance Grady |April 23, 2021 |Vox 

The Brooklyn native went the opulent route with his cannabis venture, Monogram, a nod to the print often used for luxury items. Some Feel-Good News About Marijuana’s Golden Age |Joshua Eferighe |April 20, 2021 |Ozy 

He appears to have found an even more opulent home in the streaming space. Daniel Craig’s Netflix payday dwarfs his James Bond earnings |Adam Epstein |April 7, 2021 |Quartz 

In his opulent maroon suit, Dickens flaunts his fame and fortune with so little subtlety he makes Kanye West appear modest. The Gospel According to Thomas Jefferson (And Tolstoy and Dickens) |Samuel Fragoso |October 26, 2014 |DAILY BEAST 

“It was so opulent that no one ever thought it would sink, then boom—it was gone,” says Conway. The Ghost Hotels of the Catskills |Brandon Presser |August 25, 2014 |DAILY BEAST 

The star is the cream itself, as opulent as crème fraiche, with vanilla, chocolate, strawberry, etc., serving as a sort of halo. The Secret to This Ice Cream: Pampered Cows |Jane & Michael Stern |May 18, 2014 |DAILY BEAST 

Tocqueville underscores: “The few opulent citizens of a democracy constitute no exception to this rule.” What’s At Stake In The Tocqueville/Piketty Debate |James Poulos |April 27, 2014 |DAILY BEAST 

Visitors enter the train and snake through its corridors and opulent interiors. All Aboard the Orient Express: Looking Back at the Golden Age of Train Travel |Sarah Moroz |April 15, 2014 |DAILY BEAST 

Samuel Jessup died; an opulent English grazier, of pilltaking notoriety. The Every Day Book of History and Chronology |Joel Munsell 

The least opulent in the Academy were the first to reject his offers, and to prefer liberty to pensions and honors. A Philosophical Dictionary, Volume 1 (of 10) |Franois-Marie Arouet (AKA Voltaire) 

Susannah paused before an opulent bush bearing roses red almost to a purple tinge. The Rake's Progress |Marjorie Bowen 

Yet there was nothing ostentatious, or which seemed inconsistent with the degree of an opulent burgher. The Fortunes of Nigel |Sir Walter Scott 

The lower classes were freer, more industrious,   and more opulent. Mary Wollstonecraft |Elizabeth Robins Pennell