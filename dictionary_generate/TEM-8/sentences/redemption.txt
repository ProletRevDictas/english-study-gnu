Like him, Buckley said, extremists are worthy of redemption and capable of change. After Capitol riots, desperate families turn to groups that ‘deprogram’ extremists |Paulina Villegas, Hannah Knowles |February 5, 2021 |Washington Post 

Some victims scramble together the money, Smith said, by buying gift cards and sending their tormentors the redemption codes that can be turned into cash. With children stuck at home during coronavirus shutdowns, online sexual predators can swoop in |Dan Morse |February 5, 2021 |Washington Post 

In Jones’s telling, sin and redemption are both personal and communal. ‘How the One-Armed Sister Sweeps Her House,’ by Cherie Jones, is a stunning debut |Hamilton Cain |February 2, 2021 |Washington Post 

You apologize, keep your head low, maybe resign to spend more time with your family and come back with a book deal, cable news contract or pet cause that offers redemption or credibility. Why Republicans Can’t Win on QAnon-Supporting Marjorie Taylor Greene |Philip Elliott |January 28, 2021 |Time 

Failing to convert the most efficient shot in basketball was Toronto’s failing to start the season, and it has become their redemption since. The Toronto Raptors Can’t Seem To Finish Their Drives |Louis Zatzman |January 27, 2021 |FiveThirtyEight 

Even if Loertscher eventually achieves legal redemption, she says the damage has already been done. States Slap Pregnant Women With Harsher Jail Sentences |Emily Shire |December 12, 2014 |DAILY BEAST 

It was about the hope and longing for redemption and reconciliation that lies somewhere within each of us. During Advent, Lots of Waiting, But Not Enough Hope |Gene Robinson |December 7, 2014 |DAILY BEAST 

Excerpted from Rebel Yell: The Violence, Passion, and Redemption of Stonewall Jackson by S.C. Gwynne. Stonewall Jackson, VMI’s Most Embattled Professor |S. C. Gwynne |November 29, 2014 |DAILY BEAST 

Throughout her life, she faced public ridicule, legal persecution and, eventually, redemption through a PhD in clinical sexology. Whip It: Secrets of a Dominatrix |Justin Jones |November 25, 2014 |DAILY BEAST 

Giving prisoners a chance at redemption is a good idea; but we were too careless, and innocent people suffered because it it. Want President Hillary? Then Primary Her |Jeff Greenfield |November 24, 2014 |DAILY BEAST 

At the same time no gold is paid out in redemption of notes, nor is it allowed to be exported. Readings in Money and Banking |Chester Arthur Phillips 

Hence, at the end of the war, the provision for redemption of Bank of England notes will work automatically. Readings in Money and Banking |Chester Arthur Phillips 

Under the figure of the deliverance from the Babylonish captivity, the church is invited to rejoice for her redemption from sin. The Bible, Douay-Rheims Version |Various 

It would be impossible, as well as impious, for men to imitate the making of the Covenant of Redemption, or of that of Works. The Ordinance of Covenanting |John Cunningham 

Come to our aid then; the morning of our Redemption from degradation, adorns the horizon. The Condition, Elevation, Emigration, and Destiny of the Colored People of the United States |Martin R. Delany