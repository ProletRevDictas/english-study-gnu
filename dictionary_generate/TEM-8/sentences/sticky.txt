For most insects, the sticky, slingshot ride straight into a frog’s mouth spells the end. Some beetles can be eaten by a frog, then walk out the other end |Jonathan Lambert |September 4, 2020 |Science News For Students 

The dietitian left her recommendations for Johnson on sticky notes and filed them electronically. Sent Home to Die |by Annie Waldman and Joshua Kaplan |September 2, 2020 |ProPublica 

Sugiura confirmed that the beetles were actively escaping from the frog’s digestive tract by using sticky wax to fix some beetles’ legs together. Water beetles can live on after being eaten and excreted by a frog |Jonathan Lambert |August 3, 2020 |Science News 

When threatened, these ocean creatures ooze a sticky mucus that can glow blue for days. This tube worm’s glowing slime may help sustain its own shine |Carolyn Wilke |June 5, 2020 |Science News For Students 

So far, they have reported that the lungs are filled with sticky mucus. How to cope as COVID-19 imposes social distancing |Sheila Mulrooney Eldred |March 23, 2020 |Science News For Students 

Paddle8 already has a “sticky collector base who are addicted to the site,” he says. William, Kate, and Jay Z’s Favorite Art Star: Alexander Gilkes' World of Rock Stars and Royalty |Tim Teeman |December 10, 2014 |DAILY BEAST 

Empty cigarette packs, liquor bottles, containers sticky with food residue covered the floor. Gary, Indiana Is a Serial Killer’s Playground |Justin Glawe |October 22, 2014 |DAILY BEAST 

She is equally desirous of Levine, as animalistic and eager to consume him while sticky with sanguine fluid. Sex, Blood and Maroon 5: Pop Culture’s Wounds Run Deep |Lizzie Crocker |October 3, 2014 |DAILY BEAST 

That was cold, and I was covered in sticky blood in winter in England, on a castle wall. Life After Deaths: Sean Bean on 'Game of Thrones' Paternity and 'Legends' |Jason Lynch |August 11, 2014 |DAILY BEAST 

Each of them was coated in something resembling a gray, sticky batter. Life Under Air Strikes: Children Under Fire Will Never Forget — or Forgive |Clive Irving |August 3, 2014 |DAILY BEAST 

How could this be done so that our clothing would be made water-tight and yet not be sticky in summer or stiff in winter? The Wonder Book of Knowledge |Various 

The sheets of rubber from which the uppers and soles are cut are at this stage of the work plastic and very sticky. The Wonder Book of Knowledge |Various 

Nothing more innocent-appearing yet more villainously sticky have I ever before encountered. Mrs. Raffles |John Kendrick Bangs 

"These, madam," said I, handing her a small plush bag into which I had poured the "salvage" taken from my sticky palms. Mrs. Raffles |John Kendrick Bangs 

Presently fingers came in contact with certain matter, which was thick and sticky to the touch. Menotah |Ernest G. Henham