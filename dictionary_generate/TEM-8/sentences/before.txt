Before anti-vaxxers, there were anti-fluoriders: a group who spread fear about the anti-tooth decay agent added to drinking water. Anti-Fluoriders Are The OG Anti-Vaxxers |Michael Schulson |July 27, 2016 |DAILY BEAST 

In secret, before the referendum, the council went ahead and fluoridated the water anyway. Anti-Fluoriders Are The OG Anti-Vaxxers |Michael Schulson |July 27, 2016 |DAILY BEAST 

Just the hard-on before you shoot unarmed members of the public. 'Babylon' Review: The Dumb Lives of Trigger-Happy Cops |Melissa Leon |January 9, 2015 |DAILY BEAST 

Was there an investigation of people at DOJ before they arrived at that conclusion? Ex-CBS Reporter Sharyl Attkisson’s Battle Royale With the Feds |Lloyd Grove |January 9, 2015 |DAILY BEAST 

Between 25 and 30, you’re trying to decide how much longer before you start growing a beard and calling yourself ‘Daddy. Freaking Out About Age Gaps in Gay Relationships Is Homophobic |Samantha Allen |January 9, 2015 |DAILY BEAST 

And he was gone, and out of sight on the swift galloping Benito, before Father Gaspara bethought himself. Ramona |Helen Hunt Jackson 

Descending the Alps to the east or south into Piedmont, a new world lies around and before you. Glances at Europe |Horace Greeley 

Before Ripperda could unclasp his lips to reply, the stranger had opened the door, and passed through it like a gliding shadow. The Pastor's Fire-side Vol. 3 of 4 |Jane Porter 

Louis stood firm, though pale and respectful, before the resentful gaze of Elizabeth. The Pastor's Fire-side Vol. 3 of 4 |Jane Porter 

Babylas raised his pale face; he knew what was coming; it had come so many times before. St. Martin's Summer |Rafael Sabatini