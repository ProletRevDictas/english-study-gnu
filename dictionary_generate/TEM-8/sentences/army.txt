Four years later, Stearns and an army of other grass-roots environmentalists won a court injunction that prevented logging and all other extraction at the forest for the next 17 years. A day’s drive from Chicago, exploring a very different Illinois |Carson Vaughan |February 12, 2021 |Washington Post 

By the following day, thousands of frustrated women and angry parents had joined the chorus, along with an army of social-media-savvy gynecologists who chimed in with tweets, TikTok videos and YouTube videos in opposition to the new line. A feminine wash for teens? Angry parents and gynecologists are on a social media crusade. |Abigail Higgins |February 12, 2021 |Washington Post 

Advocates like Salud y Bienestar’s Martinez say the bulk of the work falls on trusted community members, like her small army of more than 70 volunteers. Among Latino immigrants, false vaccine claims are spreading as fast as the virus |Teo Armus |February 11, 2021 |Washington Post 

His brash, outrageous and often comical style began to revolutionize American political culture as an army of “Dittoheads” flocked to “Rush Rooms” set up in restaurants nationwide to eat lunch and hear their hero. Rush Limbaugh is ailing. And so is the conservative talk radio industry. |Paul Farhi |February 9, 2021 |Washington Post 

A central part of the GameStop story was the army of Reddit day-traders enabled by zero-commission sites that are effectively funded by high-frequency trading shops. How the government might react to GameStop |Felix Salmon |February 5, 2021 |Axios 

Fry had previously confirmed the news to his army of followers on Twitter. Meet Stephen Fry’s Future Husband (Who Is Less Than Half His Age) |Tom Sykes |January 6, 2015 |DAILY BEAST 

Fatima says they were initially happy when Ziad joined the army, but that feeling has utterly faded. A Sunni-Shia Love Story Imperiled by al Qaeda |Ruth Michaelson |December 26, 2014 |DAILY BEAST 

Sabrine says that if Ziad returns, she will make him leave the army. A Sunni-Shia Love Story Imperiled by al Qaeda |Ruth Michaelson |December 26, 2014 |DAILY BEAST 

The army has since conducted a brutal wave of jailings against activists and journalists. Behind Bars for the Holidays: 11 Political Prisoners We Want to See Free In 2015 |Movements.Org |December 25, 2014 |DAILY BEAST 

We are, essentially, an army of guinea pigs millions strong. You’re Never ‘Cured’ of an Eating Disorder |Carrie Arnold |December 20, 2014 |DAILY BEAST 

In less than ten minutes, the bivouac was broken up, and our little army on the march. Blackwood's Edinburgh Magazine, No. CCCXXXIX. January, 1844. Vol. LV. |Various 

If you have any thoughts of influencing me or my men to join the regular Confederate army, you may as well give up the idea. The Courier of the Ozarks |Byron A. Dunn 

Above all, he was amazed to hear me talk of a mercenary standing army in the midst of peace and among a free people. Gulliver's Travels |Jonathan Swift 

Nogués and his brave lads have done their bit indeed for the glory of the Army of France. Gallipoli Diary, Volume I |Ian Hamilton 

He will tell you about the success he had in America; it quite makes up for the defeat of the British army in the Revolution. Confidence |Henry James