Because it’s a biological product, it can’t be patented or sold for a profit. Why we don’t know if convalescent plasma works to treat Covid-19 |Katherine Ellen Foley |August 28, 2020 |Quartz 

In the early days, it was very important to not publish so that we could get all of our patents, which is ultimately what value here is built on. Will a Covid-19 Vaccine Change the Future of Medical Research? (Ep. 430) |Stephen J. Dubner |August 27, 2020 |Freakonomics 

That September, he filed patent application 143,805, “Art of Compiling Statistics.” Punching in |Tate Ryan-Mosley |August 19, 2020 |MIT Technology Review 

The patent does not guarantee that Cansino’s vaccine will ultimately prove successful. China just issued its first COVID vaccine patent. What it means for the vaccine race |Grady McGregor |August 18, 2020 |Fortune 

The patent application reflects a high level of technical sophistication. The U.S. Postal Service is seeking a patent for voting by phone |Jeff |August 17, 2020 |Fortune 

Having received a patent on the technology in 1986, Hull founded 3D Systems to commercialize his discoveries. Pioneers in Printing |The Daily Beast |October 21, 2014 |DAILY BEAST 

Last week, the U.S. Patent and Trademark Office said the Redskins name and logo should not have trademark protection. The Native Americans Who Voted for ‘The Fighting Sioux’ |Evan Weiner |June 26, 2014 |DAILY BEAST 

Snyder is appealing the decision by the U.S. Patent and Trademark Office, and the team seems confident that it will win again. Amanda Blackhorse Is ‘Confident’ Snyder Will Lose His Redskins Appeal |Robert Silverman |June 25, 2014 |DAILY BEAST 

“Tesla will not initiate patent lawsuits against anyone who, in good faith, wants to use our technology,” he wrote. Tesla’s Radical Patent Move is a Plot to Take Over the Road |Daniel Gross |June 15, 2014 |DAILY BEAST 

The description in its entirety begins to read more like the storyline in Her than a real-life patent. Apple Wants to Make Your Head Into an App |Abby Haglage |February 19, 2014 |DAILY BEAST 

Robert Fitzgerald received a patent in England for making salt water fresh. The Every Day Book of History and Chronology |Joel Munsell 

I only draw your attention to the facts; which have been sufficiently patent to the world, whatever Lord Hartledon may think. Elster's Folly |Mrs. Henry Wood 

Papier maché buttons came in with Henry Clay's patent in 1778. Showell's Dictionary of Birmingham |Thomas T. Harman and Walter Showell 

Ellis's patent boot studs to save the sole, and the Euknemida, or concave-convex fastening springs, are the latest novelties. Showell's Dictionary of Birmingham |Thomas T. Harman and Walter Showell 

Before this patent was granted he had, however, given up the use of weights altogether and relied entirely upon springs. The Recent Revolution in Organ Building |George Laing Miller