If you can’t find some peaceful and harmonious accommodation with the news industry — and by extension the political sphere — things start getting really, very, uncomfortable. ‘We’re getting more used to the uncertainty’: BBC Global News chief on ad-funded news |Lucinda Southern |September 10, 2020 |Digiday 

Though connected to the waterfront by a wooden gangway and to a nearby shopping mall by an underground tunnel, the sphere appears to bob in the marina like, well, an apple. Apple’s ‘most ambitious’ new store is a departure from its signature design |claychandler |September 8, 2020 |Fortune 

They don’t form spheres and aren’t big enough to keep other objects out of their way. Scientists Say: Asteroid, meteor and meteorite |Bethany Brookshire |September 7, 2020 |Science News For Students 

Rather, the blob is self-contained, a roiling, lumpy sphere that leaves the water around it mostly still. An Unexpected Twist Lights Up the Secrets of Turbulence |David H. Freedman |September 3, 2020 |Quanta Magazine 

If he can get that feeling going that people feel safe and secure in their own economic sphere, then he’s going to be in good shape. Full Transcript: Sean Spicer on ‘The Carlos Watson Show’ |Daniel Malloy |August 26, 2020 |Ozy 

The digital dating sphere can prove tricky, and bruising, for the trans user. Grindr’s Trans Dating Problem |David Levesley |January 9, 2015 |DAILY BEAST 

This may be precisely the point: that fiction at its best is a sphere of suspended belief as much as suspended disbelief. The Birth of the Novel |Nick Romeo |November 27, 2014 |DAILY BEAST 

In almost every sphere of life, the trend is to trade in ownership for access. How Young People Are Destroying Liberty |James Poulos |October 11, 2014 |DAILY BEAST 

At that point, a sphere lit up, resembling the landing of the UFO in E.T., and the overheard lights descended on the stage. I'm Not Country or Pop. I'm Just Pure Garth Brooks. |David Masciotra |September 10, 2014 |DAILY BEAST 

Why tolerate toxicity in a powerful sphere of modern life that has the potential to—and does—benefit so many? Zelda Williams Is the Latest to Leave Twitter Because of Ugly Attacks |Tauriq Moosa |August 14, 2014 |DAILY BEAST 

If ever the fusion of two human beings into one has been accomplished on this sphere it was surely in their union. The Awakening and Selected Short Stories |Kate Chopin 

The belated moon stole up from its lair, hovered above the sky-line, a gaudy orange sphere in the haze of smoke. Raw Gold |Bertrand W. Sinclair 

But in 1811 he was recalled to Paris to receive orders before starting on a new sphere of duty. Napoleon's Marshals |R. P. Dunn-Pattison 

Hence in the house, the sphere of the Genius is no longer the hearth but the marriage-bed (lectus genialis). The Religion of Ancient Rome |Cyril Bailey 

Second, geology, which takes account of all those actions which in process of time have been developed in our own sphere. Outlines of the Earth's History |Nathaniel Southgate Shaler