For years, the poor adjective has been relegated to outcast status. Hate the word ‘moist’? Get over it — the alternatives are worse. |Emily Heil |January 19, 2021 |Washington Post 

Heather Matarazzo’s breakthrough performance at 13 as middle school outcast Dawn Weiner in Todd Solondz’s 1995 counter-culture classic, “Welcome to the Dollhouse,” made her a touchstone for a whole generation of traumatized teenagers. For Heather Matarazzo, ‘Equal’ is still a cause worth fighting for |John Paul King |October 20, 2020 |Washington Blade 

After I left, Avalon recorded a song called “Orphans of God,” which I thought was interesting that they were singing it because I was definitely an outcast to them. Former Avalon singer on coming out, getting ousted and where he is today |Joey DiGuglielmo |October 20, 2020 |Washington Blade 

Men were forced to abandon their children, relationships and marriages failed, generations were changed and the emotional health of the outcast individual suffered. While We’re Rethinking Policing, It’s Time to End Gang Injunctions |Jamie Wilson |September 15, 2020 |Voice of San Diego 

Although dogged by controversy and legal difficulties, he gained a cult following as an “emo rapper,” capturing the imaginations of the outcast and depressed with a new alternative rap style. We Hear Dead People: Our Favorite Posthumous Hip-Hop Albums. Ever |Joshua Eferighe |August 31, 2020 |Ozy 

It is no wonder that he constantly preached about our welcome of the stranger and our compassion for the outcast. LGBT Asylum Seekers Need America More Than Ever |Gene Robinson |June 29, 2014 |DAILY BEAST 

Outcast by his family, fired from his job and on the run, he says his life is in pieces. Al-Sisi’s Egypt Is Worse For Gays Than The Muslim Brotherhood |Bel Trew |June 28, 2014 |DAILY BEAST 

“When you leave Lampo, you become an ‘outcast’ regardless of the reasons,” another former employee said. Spies, Cash, and Fear: Inside Christian Money Guru Dave Ramsey’s Social Media Witch Hunt |Matthew Paul Turner |May 29, 2014 |DAILY BEAST 

This tension between outcast and overlord is at the heart of our sweeping change into a tech-driven, spiritually infused economy. Can Heritage Foundation Posterboy Bono Save the GOP? |James Poulos |March 15, 2014 |DAILY BEAST 

Then Mr. Wilde told Vance he could go; and he went, shambling like an outcast of the slums. Read ‘The King in Yellow,’ the ‘True Detective’ Reference That’s the Key to the Show |Robert W. Chambers |February 20, 2014 |DAILY BEAST 

There is, perhaps, in this childish suffering often something more than the sense of being homeless and outcast. Children's Ways |James Sully 

The tattered outcast dozes on his bench while the chariot of the wealthy is drawn by. The Unsolved Riddle of Social Justice |Stephen Leacock 

The poem entitled The Outcast expresses this feeling of mysterious remorse and unending and unavailing expiation. Studies in Folk-Song and Popular Poetry |Alfred M. Williams 

One was Enid Vane's sweet childish face, as she thrust her shilling with the hole in it into the little outcast's hand. A Life Sentence |Adeline Sergeant 

I humbly returned thanks to God for the privilege of ministering to the wants of this his outcast, despised and persecuted image. The Duty of Disobedience to the Fugitive Slave Act |Lydia Maria Child