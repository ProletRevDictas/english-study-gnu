The first significant climb on the course is alternatively referred to as Vault or Cemetery Hill—a reminder that before it became a public park, this land served as the private burial grounds for the Van Cortlandt family. The Eternal Magic of Van Cortlandt Park |Martin Fritz Huber |September 28, 2020 |Outside Online 

“Jewish custom insists on prompt burial…a consideration of particular relevance in hot climates,” the authoritative Encyclopaedia Judaica explains. In Death, As In Life, Ruth Bader Ginsburg Balanced Being American And Jewish |LGBTQ-Editor |September 25, 2020 |No Straight News 

Funerals can be delayed when the death falls on the Sabbath – a day of rest in the Jewish faith when no burials are performed – or on a Jewish holiday. In Death, As In Life, Ruth Bader Ginsburg Balanced Being American And Jewish |LGBTQ-Editor |September 25, 2020 |No Straight News 

She already owned a burial plot that had a tombstone engraved with her name and birth date. Dawn of the Heliocene - Issue 90: Something Green |Summer Praetorius |September 16, 2020 |Nautilus 

Congress enacted it in 1990 to protect and safely relocate native burial sites. Border Report: Kumeyaay Band Sues to Stop Border Wall Construction |Maya Srikrishnan |August 17, 2020 |Voice of San Diego 

It was a traditional burial—the kind that the government is now battling—that led to the first outbreak. Jail Threats for Sierra Leone Ebola Victims’ Families |Abby Haglage |December 10, 2014 |DAILY BEAST 

As the burial team arrived to remove the body, he began making small movements and was found to be still alive. What It’s Like to Wake Up Dead |Dr. Anand Veeravagu, MD, Tej Azad |November 21, 2014 |DAILY BEAST 

Unfortunately, neither of our teams had pinpointed the pig's burial site. Knowing Where the Bodies Are Buried: An Excerpt From 'Lives in Ruins' |Marilyn Johnson |November 14, 2014 |DAILY BEAST 

“At one point they were going to perform a burial ceremony with the ashes,” he says. Rage Against the Ebola Crematorium |Abby Haglage |November 11, 2014 |DAILY BEAST 

"Cremation is not necessary to have safe and dignified burial," Tarik Jasarevic tells me. Rage Against the Ebola Crematorium |Abby Haglage |November 11, 2014 |DAILY BEAST 

Before the breath could have been well out of his body, they hoisted him up and carried him away to burial. Hunting the Lions |R.M. Ballantyne 

An undertaker waited on a gentleman, with the bill for the burial of his wife, amounting to 67l. The Book of Anecdotes and Budget of Fun; |Various 

When all these ceremonies were finished, they carried him for burial to an isolated island, far from the mainland. The Jesuit Relations and Allied Documents, Vol. II: Acadia, 1612-1614 |Various 

They attend to the burial of the poor, and of the bones of those who are hanged, which duty they see to once each year. The Philippine Islands, 1493-1898, Volume XX, 1621-1624 |Various 

The burial of 3,000 Turks by armistice at Anzac seems to have been carried out without a hitch. Gallipoli Diary, Volume I |Ian Hamilton