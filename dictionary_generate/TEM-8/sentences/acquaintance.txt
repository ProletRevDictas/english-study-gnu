As Coupang CEO Bom Kim sought to scale his business over the summer, Pham, previously considering an early retirement following his stint at Uber, was introduced by a mutual acquaintance, making their first meeting over video chat. Uber’s former CTO is mapping out growth and delivery routes in his new gig |Lucinda Shen |October 28, 2020 |Fortune 

That’s why a picture of a new baby from a long-ago acquaintance will vault to the top of your Facebook feed, even if you haven’t seen any other posts by that person for years. Facebook’s new tool to stop fake news is a game changer—if the company would only use it |Jeff |October 18, 2020 |Fortune 

The slowing down of the glaciers is a private plan by a glaciologist of my acquaintance who doesn’t want to get into the geo-engineering wars. Kim Stanley Robinson Holds Out Hope - Issue 90: Something Green |Liz Greene |October 7, 2020 |Nautilus 

I don’t want bloodshed or violence, and it hurts me to watch acquaintances and friends being beaten, and now I hear they’re getting ready to start shooting people. Misery in Minsk |Eugene Robinson |August 18, 2020 |Ozy 

So it seems like the answer is just to have a bunch of casual acquaintances to whom you can’t really feel anything terrible. What Does Covid-19 Mean for Cities (and Marriages)? (Ep. 410) |Stephen J. Dubner |March 26, 2020 |Freakonomics 

Should old acquaintance be forgot, just remember a few of the resolutions the Founding Fathers (would have) made this year. Forget the Resolutions; Try a Few Declarations |Kevin Bleyer |January 1, 2015 |DAILY BEAST 

Another acquaintance described Seevakumaran as “a creep,” who would “constantly hit on women.” School Shooters Love This Pickup Artist Website |Brandy Zadrozny |December 5, 2014 |DAILY BEAST 

He insulted a female poet of his acquaintance by remarking “that she and her family were Jews.” Borges Had A Genius For Literature But Not Love Or Much Else |Allen Barra |October 24, 2014 |DAILY BEAST 

Not long ago, I mentioned the Victims of Communism Memorial to an acquaintance. Communism's Victims Deserve a Museum |James Kirchick |August 25, 2014 |DAILY BEAST 

In the early 2000s, an acquaintance told Sun about the possibility of doing business in Ethiopia. 'Made in China' Now Being Made in Africa |Brendon Hong |August 23, 2014 |DAILY BEAST 

A child begins to make acquaintance with the images of things when set before a mirror. Children's Ways |James Sully 

He made the acquaintance of some courtiers, who felt or affected an interest in learning and in learned men. Tobacco; Its History, Varieties, Culture, Manufacture and Commerce |E. R. Billings. 

By its operation Gordon Wright, the most sensible man of our acquaintance, is reduced to the level of infancy! Confidence |Henry James 

It was the Town Crier, with whom, as with a brother artist, he had picked acquaintance the day before. The Joyous Adventures of Aristide Pujol |William J. Locke 

For Lettice—the tender woman of his first acquaintance—had obviously experienced a moment of reaction. The Wave |Algernon Blackwood