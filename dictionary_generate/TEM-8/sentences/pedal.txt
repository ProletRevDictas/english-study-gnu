This gives the car a quicker response to the accelerator pedal, it turns up the volume on the Enterprise-at-warp effect, and it turns the instrument panel orange as a reminder of the serious business at hand. Ford’s electric Mustang Mach-E is an important leap into the future |Dan Carney |February 12, 2021 |Popular-Science 

We rented road bikes from Lahaina’s West Maui Cycles, a full-service and friendly shop, for an 18-mile pedal to lovely Napili Bay and back. A Maui vacation in three acts |Alex Pulaski |February 12, 2021 |Washington Post 

There’s also one-pedal driving, which automatically applies the brakes just by lifting your foot off the accelerator. Top rides for 2021 |Joe Phillips |January 15, 2021 |Washington Blade 

We have to continue to put our foot on the pedal, we have to continue to push the issue because we’re not that type of team where we can just get comfortable, flip on a switch and just compete. At last, Wizards get their first win by cruising to a rout of the Timberwolves |Ava Wallace |January 2, 2021 |Washington Post 

The next day, pedal 32 miles south along Highway 101 to Carpinteria State Beach, the closest place to pitch a tent near the infamous Rincon Point. Your New Adventure Travel Bucket List |The Editors |December 21, 2020 |Outside Online 

I strain and push and pedal and wonder, “When will this end?” Biking With the Bard |Kara Cutruzzula |December 28, 2014 |DAILY BEAST 

I push down on the pedal with my right leg and instead of propelling myself forward, I topple over sideways. You’re Never ‘Cured’ of an Eating Disorder |Carrie Arnold |December 20, 2014 |DAILY BEAST 

And when the AHA is reintroduced in parliament, as it inevitably will be, he can soft-pedal. The Uganda Ruling is Good For Everyone But Gays |Jay Michaelson |August 1, 2014 |DAILY BEAST 

He'd pedal a few yards, then the handle bars would get away from him. The Stacks: A Dog Dies, a Boy Grows Up |Pete Dexter |June 21, 2014 |DAILY BEAST 

And together, they can successfully pedal a bike down a rather busy city street. Chubby Korean Baby Dance, Goat Riding Guy Riding Bike, and More Viral Videos |The Daily Beast Video |June 1, 2014 |DAILY BEAST 

That is one of the places that when the pianists come to, they get their foot hard on to the pedal and hold on to it—Herr Gott! Music-Study in Germany |Amy Fay 

I had played that study to Tausig, and he found no fault with my use of the pedal; so I sat down thinking I could do it right. Music-Study in Germany |Amy Fay 

The pedal keys were almost invariably straight and the pedal boards flat. The Recent Revolution in Organ Building |George Laing Miller 

It frequently leads to a player upsetting his Pedal combination when he has no desire to do so. The Recent Revolution in Organ Building |George Laing Miller 

Metal buttons or pistons located on the toe piece of the pedal-board were introduced by the ingenious Casavant of Canada. The Recent Revolution in Organ Building |George Laing Miller