The threatening walls of this tumbledown abode seemed to have been decorated with hieroglyphics. At the Sign of the Cat and Racket |Honore de Balzac 

Hieroglyphics of thought were there, too mysterious for the common eye to interpret. Ernest Linwood |Caroline Lee Hentz 

We do not propose that he shall decipher the hieroglyphics of algebra and geometry. Astronomy for Amateurs |Camille Flammarion 

The Japanese cannot pocket it any more than he can thrill to short Saxon words or we can thrill to Chinese hieroglyphics. Revolution and Other Essays |Jack London 

Some of them, Mr. Davies thinks, were impressed with rude hieroglyphics, symbolical of Ceridiven. The Mysteries of All Nations |James Grant