The core of GameStop's quick cash-in problem was the SEC's insider trading rules, which define a specific "trading window" schedule outlining when company insiders can and can't trade on quarterly earnings results that haven't yet been made public. Why didn’t GameStop sell some of its inflated stock during the bubble? |Kyle Orland |February 11, 2021 |Ars Technica 

The inclusion of both films in that race were something of a surprise – especially the latter, given that Sia’s acclaimed cinematic debut is still mostly unseen by all but a few industry insiders. Nominees (and 2 winners) for 2021 Golden Globes announced |Troy Masters |February 3, 2021 |Washington Blade 

A security team within JPMorgan Chase ran special ops to look for insider threats by monitoring the bank’s employees. Local Law Enforcement Quiet on Relationships With ‘Predictive Policing’ Company |Jesse Marx |February 2, 2021 |Voice of San Diego 

All military personnel, including members of the National Guard, have undergone a background investigation, are subject to continuous evaluation and are enrolled in an insider threat program. The Boogaloo Bois Have Guns, Criminal Records and Military Training. Now They Want to Overthrow the Government. |by A.C. Thompson, ProPublica, and Lila Hassan and Karim Hajj, FRONTLINE |February 1, 2021 |ProPublica 

At the time, three major city leases were about to expire, and Filner and other insiders had concluded the city needed a comprehensive real estate strategy. How a Volunteer Helped Get the City Into Its Biggest Real Estate Debacle |Lisa Halverstadt |January 29, 2021 |Voice of San Diego 

The family behind Sotto Sotto says that they plan to rebuild, but an insider tells me it may be a while. The Fiery Death of Sotto Sotto, Toronto’s Celebrity Hotspot |Shinan Govani |December 30, 2014 |DAILY BEAST 

A palace insider however insisted to the Daily Beast today that the Queen was not about to abdicate. Could The Queen Abdicate on Christmas Day? |Tom Sykes |December 17, 2014 |DAILY BEAST 

A network insider insisted: “No expletives were uttered by Mr Mason in the recording of his rant.” UK Reporter’s Anti-Banker Rant Goes Viral |Nico Hines |November 13, 2014 |DAILY BEAST 

But one former company insider says knockoff screws were mixed in with real ones. Patients Screwed in Spine Surgery ‘Scam’ |The Center for Investigative Reporting |November 3, 2014 |DAILY BEAST 

When I asked a tea-party insider recently which Senate races were most important to his movement, he named Sasse first. Why You Can’t Tell the Truth About Race |Michael Tomasky |November 3, 2014 |DAILY BEAST 

If the insider keeps his eyes wide open and waits long enough his chance will come. Stories of Our Naval Heroes |Various 

Hereafter, for at least seven years, the Federation was an "insider" in the national government. A History of Trade Unionism in the United States |Selig Perlman 

A triangular opening faced towards the bows of the ship, so that the insider commanded a complete view forward. Moby Dick; or The Whale |Herman Melville 

But then, you know, the insider sometimes has a better chance than the outsider. Stories of Our Naval Heroes |Various 

The disputants know instinctively that an outsider can see the difficulty better than an insider. The home |Charlotte Perkins Gilman