If a student’s grades slip or they misbehave, they can lose tens of thousands of dollars of their scholarship. “I Finally Got to the Mountaintop and I Failed” |by Bob Fernandez, The Philadelphia Inquirer, and Charlotte Keith, Spotlight PA |June 10, 2021 |ProPublica 

As anyone over the age of, say, 8 should know, other children misbehaving doesn’t give you the green light to misbehave yourself. What does it mean that Republicans keep blaming the left for Jan. 6? |Philip Bump |May 28, 2021 |Washington Post 

So next time your child misbehaves, think of Elizabeth Tegumiar and simply walk away. I Was Constantly Arguing With My Child. Then I Learned the “TEAM” Method of Calmer Parenting |Michaeleen Doucleff |March 6, 2021 |Time 

If Facebook misbehaved, the trust would retract the company’s access to its members’ data. How data trusts can protect privacy |Katie McLean |February 24, 2021 |MIT Technology Review 

In the case of GameStop, both the cynics and sentimentalists have been misbehaving. Bitcoin, stocks and crude take off as the markets brace for a wave of stimulus checks |Bernhard Warner |February 8, 2021 |Fortune 

The discussion of race in the league just serves to distract from why players misbehave. Ex-NFL Linebacker: We Talk Around Race, Not About It |Carl Banks |October 23, 2014 |DAILY BEAST 

He is, you sense, trying not to misbehave, while remaining human and not becoming a Royal cyborg. Prince Harry Should Be King: The Royal Family’s Ace Card |Tim Teeman |June 27, 2014 |DAILY BEAST 

And yet it is the privilege of the prince and the sultan to misbehave. How the Sultan of Brunei Violated His Sharia Law With Me |Jillian Lauren |May 6, 2014 |DAILY BEAST 

A boy might be excused for that so long as he didn't misbehave. Warrior Gap |Charles King 

To allow the child to misbehave without instantly making it unpleasantly conscious of the fact would be to spoil it. A Treatise on Parents and Children |George Bernard Shaw 

But my dear, you don't meant to say that all bachelor clergymen misbehave themselves. Barchester Towers |Anthony Trollope 

But if servants misbehave themselves, or leave their places, not being regularly discharged, they ought to be amerced or punished. Everybody's Business is Nobody's Business |Daniel Defoe 

If he does not misbehave and keeps out of a lawsuit, he rarely comes in contact with his rulers. A Wayfarer in China |Elizabeth Kendall