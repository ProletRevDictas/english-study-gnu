My younger, straighter-than-an-arrow son was stopped and arrested in two separate jurisdictions a few years ago. What Would Happen if I Got in White Cop’s Face? |Goldie Taylor |December 30, 2014 |DAILY BEAST 

A half-an-hour earlier they had been caught in the middle of a mortar barrage in a skirmish with separatists. Shakeup In the Ukraine Rebel High Command |Jamie Dettmer |August 15, 2014 |DAILY BEAST 

The full translation follows: In the region of Torez, a AN-26 was just shot down, crashing behind the “progress” mine. Latest News on Malaysian Airliner Reportedly Shot Down Over Ukraine |The Daily Beast |July 17, 2014 |DAILY BEAST 

In those accounts, neither Buzzelli nor the eager-for-an-angle reporters dubbed the man a World Trade Center “surfer.” The Search for the Elusive 9/11 Surfer: Pasquale Buzzelli’s Story on Discovery Channel |Matthew DeLuca |September 12, 2012 |DAILY BEAST 

Or is he just a straight-as-an-arrow Eagle Scout, far more Clark Kent than Superman? Calvin Coolidge’s Bone-Dry Humor Is a Lesson in Laughs for Candidates |Mark Katz |February 20, 2012 |DAILY BEAST 

They had searched Mortlake House for Alice, and that vain quest had not wasted more than half-an-hour. Checkmate |Joseph Sheridan Le Fanu 

On the first day's journey toward Chang-an-sa the party made good progress. Our Little Korean Cousin |H. Lee M. Pike 

Passing through the gate, our friends found themselves at once in the midst of the Chang-an-sa monastery buildings. Our Little Korean Cousin |H. Lee M. Pike 

It was the merest baby—half-an-ounce, perhaps—and it fell from the hook into the herbage some yards from the stream. Uncanny Tales |Various 

In half-an-hour he was sound asleep, and a dead silence reigned in Azalea Lodge. The Pit Town Coronet, Volume II (of 3) |Charles James Wills