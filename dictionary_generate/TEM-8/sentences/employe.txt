Wenceslas Steinbock was at first an apprentice and afterwards an employe of the firm. Repertory Of The Comedie Humaine, Complete, A -- Z |Anatole Cerfberr and Jules Franois Christophe 

Miles could not continue to pay her large sums of money, since he was really only an employe of Flora's. Murder at Bridge |Anne Austin 

Nor could an employer discipline an employe for joining a union or inducing others to join. A History of Trade Unionism in the United States |Selig Perlman 

I think the only serious problem we have to meet is whether we shall take away the common law right from the employe. Proceedings, Third National Conference Workmen's Compensation for Industrial Accidents |Various 

First: Shall we prepare a bill that is compulsory upon the part of the employer and optional as to the employe? Proceedings, Third National Conference Workmen's Compensation for Industrial Accidents |Various