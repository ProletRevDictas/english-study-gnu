It began as two Senate bills to create vouchers and expand charters. Betsy DeVos is gone — but ‘DeVosism’ sure isn’t. Look at what Florida, New Hampshire and other states are doing. |Valerie Strauss |February 5, 2021 |Washington Post 

Residents can apply for a voucher to cover about half the cost of an at-home composting unit but that doesn’t help you get it to farmers who want it. Environment Report: Local Farms Want Your Table Scraps |MacKenzie Elmer |January 25, 2021 |Voice of San Diego 

He anticipates an expansion of the low-income housing tax credit as well as housing vouchers being made an entitlement. Experts predict what the 2021 housing market will bring |Kathy Orton |January 11, 2021 |Washington Post 

Passengers found themselves having to stay on hold with customer-service representatives to get refunds while vouchers were offered automatically, making it easier to choose that option. Stiffed by an airline for a flight you couldn’t take? This U.K. watchdog is on the case |kdunn6 |December 16, 2020 |Fortune 

The airline canceled his flights and offered only a voucher — a common problem after the covid-19 outbreak. What consumers learned about travel complaints in 2020 |Christopher Elliott |December 2, 2020 |Washington Post 

He again turns Medicare into a voucher program, a position he had to stifle in 2012, because Romney did not approve. Why Paul Ryan’s Star Dimmed |Howard Kurtz |March 21, 2013 |DAILY BEAST 

A year ago, Candidate Ryan called for voucher care instead of Medicare for Americans who were then 55 and under. GOP Meltdown: Paul Ryan Doubles Down On His Losing Southern Strategy |Lloyd Green |March 10, 2013 |DAILY BEAST 

The ticket or voucher for travel will not be replaced if lost, mutilated, or stolen. The Orbitz Business Travel Survey Sweepstakes | |December 14, 2012 |DAILY BEAST 

The Medicare drug benefit began in 2006 with a voucher approach. David Brooks Defends the Ryan Plan |David Frum |October 11, 2012 |DAILY BEAST 

And in a direct shot at Paul Ryan, Obama vowed not to turn Medicare into a voucher program. Obama’s DNC Speech: A Sober Call to Arms |Howard Kurtz |September 7, 2012 |DAILY BEAST 

The first step in providing for a complete postage record is to make a voucher for postage required. Cyclopedia of Commerce, Accountancy, Business Administration, v. 1 |Various 

The cashier should require a voucher before supplying stamps or the money with which to buy them. Cyclopedia of Commerce, Accountancy, Business Administration, v. 1 |Various 

A special voucher form showing the postage on hand, and the number of stamps of each denomination required, is shown in Fig. 15. Cyclopedia of Commerce, Accountancy, Business Administration, v. 1 |Various 

He supplies no numbered voucher, and cannot possibly tell at which tables some six or seven hundred diners will be seated. The Terms of Surrender |Louis Tracy 

Will the reader consent to their Dialogue, which is dullish, but singular to have in an authentic form, with Nicolai as voucher? History of Friedrich II. of Prussia, Vol. XVIII. (of XXI.) |Thomas Carlyle