We got more comfortable with Microsoft Teams and things like that. How Hallmark is handling pandemic Valentine’s Day |Luke Winkie |February 12, 2021 |Vox 

They are used to creating a musical work without revision, and so they are comfortable with the notion that an answer can precede a question. The tale of a bass player, sonic epiphanies and a quest to save ‘real music’ |Ben Ratliff |February 12, 2021 |Washington Post 

It isn’t always pain free, but it is easier and more comfortable and more spiritual. Marie Mongan, champion of hypnobirthing, dies at 86 |Olesia Plokhii |February 11, 2021 |Washington Post 

One is very comfortable watching sex with me, while the other will immediately walk out of the room. Watching edgier TV with your kids during the pandemic? You’re not alone. |Bonnie Miller Rubin |February 11, 2021 |Washington Post 

The plush fabric is comfortable, available in various colors, and can be machine washed. Best heated throw blanket: Bundle up with these electric blankets |PopSci Commerce Team |February 11, 2021 |Popular-Science 

I need to resist my urge to talk them into my truth, just so I can feel more comfortable and secure. In 2015, Let’s Try for More Compassion |Gene Robinson |January 4, 2015 |DAILY BEAST 

His goal: to make the perfect (and absolutely comfortable) high-heel, with the help from Nike CEO Mark Parker. What, and Who, You'll Be Wearing in 2015 |Justin Jones |December 27, 2014 |DAILY BEAST 

But she was less comfortable with it before she was an established name in fashion. The Big Business of Fashion Counterfeits |Lizzie Crocker |December 24, 2014 |DAILY BEAST 

The resulting Wool Runners were comfortable, eco-friendly, machine-washable, and super cute—and sold out almost immediately. The Coolest Fashion Innovations of 2014 |Raquel Laneri |December 18, 2014 |DAILY BEAST 

I do not recall what sort of aeroplane Mr. Hughes had at the time; however, it was quite comfortable, as I recall. Alfred Hitchcock’s Fade to Black: The Great Director’s Final Days |David Freeman |December 13, 2014 |DAILY BEAST 

Behold a dumpy, comfortable British paterfamilias in a light flannel suit and a faded sun hat. God and my Neighbour |Robert Blatchford 

"She used to be so well—so bright," said Angela, who also appeared to have the desire to say something kind and comfortable. Confidence |Henry James 

Not but that a cabriolet is a good vehicle of its sort: I know of few more comfortable. Blackwood's Edinburgh Magazine, Volume 60, No. 372, October 1846 |Various 

The lazy giant was sprawling on the most comfortable of the sofas; the pair were alone in the dainty little drawing-room. The Pit Town Coronet, Volume I (of 3) |Charles James Wills 

However, the fires were stirred up, and things made as comfortable as circumstances would admit of. Hunting the Lions |R.M. Ballantyne