The redundancy of a roundup article doesn’t come from the list of people contributing to it, but from the very topic, you will choose for it. Simple guide to creating an expert roundup post that drives website traffic |Ivan Ambrocio |November 26, 2020 |Search Engine Watch 

Dutch law requires employers to obtain approval from the UWV for planned redundancies. Uber refused permission to dismiss 11 staff at its EMEA HQ |Natasha Lomas |November 23, 2020 |TechCrunch 

Reducing staff working hours, either as a mandatory or voluntary measure, became a go-to move for many agencies at the onset of the pandemic in the spring, as a way to cut costs and prevent redundancies and furloughs. ‘People have had permission to experiment’: Pandemic expedites rethink on 9-to-5 work structures |Jessica Davies |November 23, 2020 |Digiday 

That will be followed by four weeks of public comment, while the development of a so-called synthetic sensor to add redundancy will take 20 to 24 months, he said. Boeing shares soar after European regulator appears to give the green light to 737 Max |Bernhard Warner |October 16, 2020 |Fortune 

The cuts are expected to be finished by 2022, and include 1,500 employees who have already taken volunteer redundancy this year, he said. Shell announces big layoffs as the price of its net-zero restructuring plan becomes clearer |kdunn6 |September 30, 2020 |Fortune 

Redundancy in general remains an issue for Wolcott: “white-boned,” “pale-moon,” “bulk-sized,” “streaming cataract,” “forlorn rue.” The Obligation to be Interesting: James Wolcott’s “Critical Mass” |William Giraldi |October 24, 2013 |DAILY BEAST 

The stakes are so great that you would think the people who own and run them would invest heavily in redundancy. NASDAQ Goes Down, but the Scary Part Is Any Lack of Sensible Explanation |Daniel Gross |August 22, 2013 |DAILY BEAST 

This is almost a third of the entire redundancy costs of the hundred or more staff sacked when News of the World closed. Bad Times at The Times: James Harding Steps Down |Peter Jukes |December 13, 2012 |DAILY BEAST 

Liz Mackean has since taken voluntary redundancy of the BBC, and will leave next March. Will the Jimmy Savile Scandal Tarnish the BBC? |Peter Jukes |October 19, 2012 |DAILY BEAST 

Markets usually have a lot of redundancy built into them--multiple payers, multiple suppliers. The Underappreciated Peril of Government Benefits | |September 25, 2012 |DAILY BEAST 

The French continually offers redundancy of subject or complement, but not with the relative. Frdric Mistral |Charles Alfred Downer 

In the fifth edition, Richardson seems chiefly concerned with redundancy, but he also diminishes some of the praise. Samuel Richardson's Introduction to Pamela |Samuel Richardson 

Exuberantly ornamented, it never oversteps the thin line which separates richness from redundancy. The Rivers of Great Britain: Rivers of the East Coast |Various 

The redundancy of unmarried young women should set people thinking on the causes for so much enforced celibacy. Chambers's Journal of Popular Literature, Science, and Art |Various 

This redundancy and repetition do not constitute the direct, forward-moving style we should like to impress on the children. Literature in the Elementary School |Porter Lander MacClintock