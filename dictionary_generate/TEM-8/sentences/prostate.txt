As a prostate cancer survivor myself, I had no idea of this statistic when I went through my experience five years ago. Celebrate first lady’s visit to Whitman-Walker, then act |Ted Miller |February 4, 2021 |Washington Blade 

The classic example here is prostate cancer, which could be a very slow-growing cancer that may not hurt the patient, but having a big surgery or big radiation therapy would hurt the patient. How to Fix the Incentives in Cancer Research (Ep. 449) |Stephen J. Dubner |January 28, 2021 |Freakonomics 

Miller ended his long tenure as Senate president in 2019 due to a stage 4 prostate cancer diagnosis, and was replaced by Ferguson. Former Md. Senate president Mike Miller dies |Philip Van Slooten |January 16, 2021 |Washington Blade 

I’m here with a small team led by Eileen Hall, Stan Hall’s 34-year-old half-Scottish, half-Ecuadoran daughter, who continued her father’s quest to understand the true history and power of the cave after he died of prostate cancer in 2008. Journey to the Center of the Earth |David Kushner |December 28, 2020 |Outside Online 

Movember was initially focused on prostate cancer, which will affect one in nine men in their lifetime. Movember is giving men a sense of community during a time of isolation |Alexandra Ossola |October 6, 2020 |Quartz 

Now, preventive protocols—like mammograms, colonoscopies, and Prostate-Specific Antigen tests—are being questioned. Clinton Doc: This Is How We’ll Fix Health Care |Daniela Drake |June 12, 2014 |DAILY BEAST 

Kathryn Wilson, et al. “Coffee Consumption and Prostate Cancer Risk and Progression in the Health Professionals Follow-up Study.” Can Coffee Save Your Life? |Anneli Rufus |October 28, 2011 |DAILY BEAST 

Xu Lai then moved his blog to another host, renaming it "Qian Leixian Still Wants to Speak"—or the Prostate Is Still Enflamed. The Blogger Who Got Stabbed |Richard Bernstein |February 18, 2009 |DAILY BEAST 

If the disease continue, cystitis and its consequent train of symptoms ensue (see Bladder and Prostate Diseases). Encyclopaedia Britannica, 11th Edition, Volume 3, Slice 7 |Various 

Many and many a man at this age finds his sexual power declining and cannot understand it—Enlarged Prostate Gland. Manhood Perfectly Restored |Unknown