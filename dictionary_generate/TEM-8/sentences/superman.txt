After the Second World War, the Ukrainian-born writer Vasily Grossman once argued that “the superman is born of the despair of the weak, not from the triumph of the strong.” Eric Greitens’ Embarrassing Fantasies of Political Violence |Phil Klay |June 25, 2022 |Time 

If you endo on your bike or superman on your skis, your Icon should be fine. This Kitsbow Shirt Is the Best Damn Flannel Ever Made |jeremyr |November 1, 2021 |Outside Online 

Long is a superman of sorts, the result of a genetic experiment that lets him live for hundreds of years. Artificial general intelligence: Are we close, and does it even make sense to try? |Will Heaven |October 15, 2020 |MIT Technology Review 

Ashtain Rothchild, a New York City-based trainer who also works with fitness startup Mirror, recommends activities including glute bridges, bent-over rows, and banded superman pulls. It’s time to add video games to your workout routine |John Kennedy |September 30, 2020 |Popular-Science 

Can you imagine Superman being handed over to a writer just a notch above amateur? Wonder Woman Takes a Big Step Back |Hugh Ryan |December 16, 2014 |DAILY BEAST 

What this essentially means is that superman-wannabe white people do not see my people as their equal. Dear White People: Well-Meaning Paternalism Is Still Racist |Chloé Valdary |December 9, 2014 |DAILY BEAST 

That all-American iconography has always been so potent in the Superman myth. Christopher Nolan Uncut: On ‘Interstellar,’ Ben Affleck’s Batman, and the Future of Mankind |Marlow Stern |November 10, 2014 |DAILY BEAST 

When she joins Batman and Superman in the Justice Society of America, she does so as secretary. Wonder Woman’s Creation Story Is Wilder Than You Could Ever Imagine |Tom Arnold-Forster |November 3, 2014 |DAILY BEAST 

Marston wrote that Wonder Woman needed “all the strength of Superman plus all the allure of a good and beautiful woman.” Wonder Woman’s Creation Story Is Wilder Than You Could Ever Imagine |Tom Arnold-Forster |November 3, 2014 |DAILY BEAST 

I dreamed for you a triumphal march of powerful harmonies, a genius, a superman, such as only you deserve. Fifty Contemporary One-Act Plays |Various 

To-day the Kaiser claims to have won the victory of "a superman." The Blot on the Kaiser's 'Scutcheon |Newell Dwight Hillis 

These lashes are the vision of the superman, of the one who rightfully possesses strength, happiness, and liberty. Contemporary Russian Novelists |Serge Persky 

It is not beyond the imagination to believe that your Mekstrom Superman might live three times our frail four-score and ten. Highways in Hiding |George Oliver Smith 

And then it came to me that what I really wanted second of all was to possess a body of Mekstrom Flesh, to be a physical superman. Highways in Hiding |George Oliver Smith