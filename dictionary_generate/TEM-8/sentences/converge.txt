Over the past five to 10 years, converging trends … have driven investors to hard assets such as mineral resources. In 2020, Everything That Glitters Is Gold |Charu Kasturi |September 8, 2020 |Ozy 

The pace of infections in the country is now converging with those of neighboring Denmark and Norway, where governments are telling citizens to use face masks for the first time. Europe is at a turning point as COVID cases spike, and fragile governments feel the heat |Bernhard Warner |August 20, 2020 |Fortune 

One final reason for why voters converge on a couple of players each year may be that voters have more information on the candidates than ever before. MVP Voting Has Never Been More Boring |Owen Phillips |August 14, 2020 |FiveThirtyEight 

Even here, converging exponential technologies are paving the way for massive implications in both human health and industry shifts. How AI Will Make Drug Discovery Low-Cost, Ultra-Fast, and Personalized |Peter H. Diamandis, MD |July 23, 2020 |Singularity Hub 

Through A360, I provide my members with context and clarity about how converging exponential technologies will transform every industry. How AI Will Make Drug Discovery Low-Cost, Ultra-Fast, and Personalized |Peter H. Diamandis, MD |July 23, 2020 |Singularity Hub 

They came from all over the city, by the thousands, to converge on the square. Eric Garner Protests: ‘It’s Like Vietnam’ |Abby Haglage, Caitlin Dickson, Jacob Siegel, Chris Allbritton |December 5, 2014 |DAILY BEAST 

Three police officials from three different cities converge to solve the case. Colin Farrell Officially Confirmed For ‘True Detective’ Season 2: ‘I’m So Excited’ |Marlow Stern |September 21, 2014 |DAILY BEAST 

Teams from across the globe converge on the host nation in something of an unarmed, athletic Crusade. Why Americans Should Love the World Cup |Sean Wilsey |June 12, 2014 |DAILY BEAST 

Where the two worldviews converge is that power comes from the individual. At American Enterprise Institute, NeoCons Say ‘Hello, Dalai’ |Eleanor Clift |February 21, 2014 |DAILY BEAST 

Why, then, did over 90 world leaders converge on South Africa last week? Goodbye, Madiba |Mark Gevisser |December 15, 2013 |DAILY BEAST 

The elements of the latter are conscious of themselves as belonging together, because their interests converge at one point. Introduction to the Science of Sociology |Robert E. Park 

Two tall Zulus were stalking along a path which should converge with ours a little way ahead. A Frontier Mystery |Bertram Mitford 

It may also happen that the patient does not converge sufficiently, merely because accommodation is absent. Schweigger on Squint |C. Schweigger 

Rays may diverge, that is, spread out; converge, or point toward each other; or they may be parallel with each other. Practical Mechanics for Boys |J. S. Zerbe 

The flashes and the shots increased in rapidity, and then both seemed to converge rapidly towards a common centre. In Hostile Red |Joseph Altsheler