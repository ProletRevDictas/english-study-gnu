Those studies are scheduled for completion over about the next year and a half. ‘Trained Immunity’ Offers Hope in Fight Against Coronavirus |Esther Landhuis |September 14, 2020 |Quanta Magazine 

The study tallied activity in more than a dozen different cryptocurrencies. Eastern Europe leads the world in cryptocurrency adoption—legal and otherwise |dzanemorris |September 14, 2020 |Fortune 

More recently, studies have reported on what the infection might do to the heart. College athletes show signs of possible heart injury after COVID-19 |Aimee Cunningham |September 11, 2020 |Science News 

That’s according to a new study published in Science Advances. Political rhetoric may impact your hurricane preparedness |Ula Chrobak |September 11, 2020 |Popular-Science 

The study, published Friday in the journal Environmental Research Letters, found this association in both rural counties in Louisiana and highly populated communities in New York. New Research Shows Disproportionate Rate of Coronavirus Deaths in Polluted Areas |by Lylla Younes, ProPublica, and Sara Sneath |September 11, 2020 |ProPublica 

She completed a yoga teacher-training program and, in the spring of 2008, went on a retreat in Peru to study with shamans. How Taryn Toomey’s ‘The Class’ Became New York’s Latest Fitness Craze |Lizzie Crocker |January 9, 2015 |DAILY BEAST 

In fact, in a recent study of their users internationally, it was the lowest priority for most. Grindr’s Trans Dating Problem |David Levesley |January 9, 2015 |DAILY BEAST 

But in the case of black women, another study found no lack of interest. The Unbearable Whiteness of Congress |Dean Obeidallah |January 8, 2015 |DAILY BEAST 

Indeed, study after study affirms the benefits of involved fatherhood for women and children. How Good Dads Can Change the World |Gary Barker, PhD, Michael Kaufman |January 6, 2015 |DAILY BEAST 

A recent U.S. study found men get a “daddy bonus” —employers seem to like men who have children and their salaries show it. How Good Dads Can Change the World |Gary Barker, PhD, Michael Kaufman |January 6, 2015 |DAILY BEAST 

"There's just one thing I'd like to ask, if you don't mind," said Cynthia, coming suddenly out of a brown study. The Boarded-Up House |Augusta Huiell Seaman 

His lordship retired shortly to his study, Hetton and Mr. Haggard betook themselves to the billiard-room. The Pit Town Coronet, Volume I (of 3) |Charles James Wills 

She began the study of drawing at the age of thirty, and her first attempt in oils was made seven years later. Women in the fine arts, from the Seventh Century B.C. to the Twentieth Century A.D. |Clara Erskine Clement 

In practice we find a good deal of technical study comes into the college stage. The Salvaging Of Civilisation |H. G. (Herbert George) Wells 

Its backbone should be the study of biology and its substance should be the threshing out of the burning questions of our day. The Salvaging Of Civilisation |H. G. (Herbert George) Wells