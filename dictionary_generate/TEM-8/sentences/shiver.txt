By correlating a seismic event on one side of the world with the shiver it produces on the other, scientists infer what happened in between. Seismic ‘Telescope’ Reveals a Titanic, Tree-Like Plume Feeding Earth’s Volcanoes |Jason Dorrier |October 17, 2021 |Singularity Hub 

Threats to Taiwan, the self-governing island only slightly bigger than Maryland, are sending shivers through the global tech industry. Why threats to Taiwan are a nightmare for tech |Ina Fried |April 9, 2021 |Axios 

There’s a hint of feminism in both books, and they’ll both give you shivers for months after you finish them. Best books of 2020 |Terri Schlichenmeyer |January 10, 2021 |Washington Blade 

It’s hard not to feel a shiver of apprehension for her as she walks, alone, down a long country road to knock on the door of a man who’s been avoiding her interview requests for years. Harvard students told a lurid tale of murder. Was it true? |Marin Cogan |December 11, 2020 |Washington Post 

Lauren Graham does beautiful things with her voice through the whole show, but the way she says, “Hey there, birthday girl,” with a hopeful, hesitant shiver in her tone, is a killer. Every episode of Gilmore Girls, ranked |Constance Grady |October 6, 2020 |Vox 

You know, a novel comes not from a decision but a frisson, a sort of shiver that goes through you. Martin Amis Talks About Nazis, Novels, and Cute Babies |Ronald K. Fried |October 9, 2014 |DAILY BEAST 

When pressed on who he thought killed Kennedy, Nixon “would shiver and say, ‘Texas,’” said Stone. Roger Stone’s New Book ‘Solves’ JFK Assassination: Johnson Did It! |David Freedlander |May 14, 2013 |DAILY BEAST 

I remember being shocked that Shiver sold in 38 countries, because I thought it was such a particularly “me” story. Maggie Stiefvater Talks New Novel ‘The Raven Boys,’ Fast Cars, and YA Fiction |Doug Stanton |September 28, 2012 |DAILY BEAST 

The friction of those little strings still makes my skin shiver, but when in Rome… head to Malika. Gal With a Suitcase |Jolie Hunt |February 26, 2011 |DAILY BEAST 

For Americans of a certain age, these words, even in our cynical time, yield a shiver of nostalgia, but also of purpose. Remembering the Wordsmith |Richard J. Tofel |November 1, 2010 |DAILY BEAST 

Presently he began to shiver so, with some sort of a chill, that I took off my coat and wrapped it round him. The Boarded-Up House |Augusta Huiell Seaman 

Still a-shiver at dawn, I saddled up and loped for the crest of the nearest divide to get the benefit of the first sun-rays. Raw Gold |Bertrand W. Sinclair 

Alfaretta's face assumed a look of great solemnity and a shiver of real fear ran over her. Dorothy at Skyrie |Evelyn Raymond 

It sent a shiver through me, and even old Piegan stood aghast at the malevolent determination of the man. Raw Gold |Bertrand W. Sinclair 

To his lifes end Tchaikovsky could never recall this hour without a shiver of horror. The Life &amp; Letters of Peter Ilich Tchaikovsky |Modeste Tchaikovsky