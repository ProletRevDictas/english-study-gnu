The pandemic has shaken up plans for everyone, and weddings are no exception. A guide to giving gifts for postponed and shrunken weddings |Brooke Henderson |August 20, 2020 |Fortune 

There are exceptions, of course, and Wade cited the Minnesota Lynx as a team that “really gets up in your grill.” The Pace Of Play Has Never Been Faster In The WNBA |Howard Megdal |August 6, 2020 |FiveThirtyEight 

Disasters always tend to hit the most vulnerable among us hardest, and the pandemic is no exception. Morning Report: With Building Folly, City Real Estate Director Out |Voice of San Diego |August 4, 2020 |Voice of San Diego 

A notable exception was Wisconsin, where 75 percent of votes were cast absentee despite nothing being mailed to them. There Have Been 38 Statewide Elections During The Pandemic. Here’s How They Went. |Nathaniel Rakich (nathaniel.rakich@fivethirtyeight.com) |August 3, 2020 |FiveThirtyEight 

Although the SARS-CoV-2 virus is most often fatal in patients who are elderly or have chronic medical conditions such as diabetes, heart disease or high blood pressure, exceptions that bring down apparently healthy young people are commonplace. Our Genes May Explain Severity of COVID-19 and Other Infections |Monique Brouillette |July 27, 2020 |Quanta Magazine 

The same Pediatrics journal notes that 17 states have some form of exception to the standard parental consent requirement. Should Teens Have The Right To Die? |Brandy Zadrozny |January 8, 2015 |DAILY BEAST 

With the exception of New Hampshire, Paul has not demonstrated potential enthusiasm in the early primary states. GOP Won’t Forgive Rand for Cop Critique |Lloyd Green |December 23, 2014 |DAILY BEAST 

None of her last five movies (with the exception of an Ice Age sequel she voiced) has grossed more than $50 million. The Biggest Bombs of 2014: ‘Sex Tape,’ Mariah Carey’s Vocals, ‘How I Met Your Mother’ and More |Kevin Fallon |December 19, 2014 |DAILY BEAST 

No crimes were committed by Sony with the possible exception of all those Adam Sandler movies they insist on making. The Disaster Story That Hollywood Had Coming |Doug McIntyre |December 17, 2014 |DAILY BEAST 

Mary Soames is an exception to the rule that gilded offspring endure life rather than enjoy it. Churchill’s Secret Treasures for Sale: A British PM’s Life on the Auction Block |Tom Teodorczuk |December 8, 2014 |DAILY BEAST 

Dean Swift was indeed a misanthrope by theory, however he may have made exception to private life. Gulliver's Travels |Jonathan Swift 

The first steam rolling mill, with the exception of the one at Soho, was put up at Bradley ironworks. Showell's Dictionary of Birmingham |Thomas T. Harman and Walter Showell 

All the banks in the city of New York without exception, and by common consent, stop specie payments. The Every Day Book of History and Chronology |Joel Munsell 

In fact, Monte Irvin had made a success of every affair in life with the lamentable exception of his marriage. Dope |Sax Rohmer 

The cereals and grasses form an exception to this rule, for in them it is an abundant and important element. Elements of Agricultural Chemistry |Thomas Anderson