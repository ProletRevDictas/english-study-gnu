Selin can’t pretend that she knows what a poster of Einstein means to every American college student because she knows the same poster means something different in Turkey. The true love story in Elif Batuman’s The Idiot is a love affair with language |Constance Grady |September 11, 2020 |Vox 

It remains to be seen how Nreal will live up to its promise, secure users at scale and move beyond being a mere poster child for tech giants’ mixed reality ambitions. Qualcomm-powered Chinese XR startup Nreal raises $40 million |Rita Liao |September 4, 2020 |TechCrunch 

When we talk about mental health in the workplace, it’s no longer enough to have HR put up a few posters and host occasional awareness-raising events — and this is especially so given the current pandemic. Deep Dive: How companies and their employees are facing the future of work |Digiday |September 1, 2020 |Digiday 

You’re going to see mass opportunity for fraud, because that is the model that is a poster child for what they want to bring to other states. Full Transcript: Tomi Lahren on ‘The Carlos Watson Show’ |Daniel Malloy |August 31, 2020 |Ozy 

A hyena “would just go up to a cub and grab it by the skull and crush it,” says Brown, who presented the work in a poster at the Ecological Society of America’s 2020 meeting held virtually the week of August 3. Female hyenas kill off cubs in their own clans |Carolyn Wilke |August 25, 2020 |Science News 

“EOTS is a poster child for one of the ills of the acquisition process,” the official said. Newest U.S. Stealth Fighter ‘10 Years Behind’ Older Jets |Dave Majumdar |December 26, 2014 |DAILY BEAST 

But by the time a critical wanted poster sent via fax arrived, more than two hours elapsed. Alleged Cop Killer Ismaaiyl Brinsley Had a Death Wish |M.L. Nestel |December 22, 2014 |DAILY BEAST 

Before I could apologize to his mother he ran back out to the living room with a poster of his dad and opened it up for me to see. The Life and Hard Times Of The Family A Cuban Defector Left Behind |Brin-Jonathan Butler |December 19, 2014 |DAILY BEAST 

They have made Keystone XL the poster child of their climate-change efforts. Why the Keystone XL Pipeline May Not Be Built |Robert Bryce |November 19, 2014 |DAILY BEAST 

A life-sized poster of Par Gyi greeted guests at the entrance. Hope and Change? Burma Kills a Journalist Before Obama Arrives |Joshua Carroll |November 11, 2014 |DAILY BEAST 

Sitting on the edge of the huge curtained four-poster bed, he ponders on the events of the evening. Uncanny Tales |Various 

There was still money in her purse, and her next temptation presented itself in the shape of a matinee poster. The Awakening and Selected Short Stories |Kate Chopin 

The director may consider the performance as an animated poster which moves rapidly from design to design. Fifty Contemporary One-Act Plays |Various 

Also, one boomed and boosted his own particular emotions, celebrating their merits in the language of the circus-poster. Love's Pilgrimage |Upton Sinclair 

The name "Students' Hostel," written on a large poster placed at the gate, attracted my attention and I rang the doorbell. Ways of War and Peace |Delia Austrian