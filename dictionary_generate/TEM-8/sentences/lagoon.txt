It is also soon to be the Thermal of the Thermal Beach Club, which will feature an artificial 20-acre surf lagoon with custom waves, created by PerfectSwell wave technology. Postcard From Thermal: Surviving the Climate Gap in Eastern Coachella Valley |by Elizabeth Weil and Mauricio Rodríguez Pons |August 17, 2021 |ProPublica 

Viviane Menezes, a marine scientist at the Woods Hole Oceanographic Institute in Massachusetts, has described the Red Sea as being like a “big lagoon” with “everything connected.” A Rusting Oil Tanker Off the Coast of Yemen Is an Environmental Catastrophe Waiting to Happen. Can Anyone Prevent It? |Joseph Hincks |May 14, 2021 |Time 

Before the pandemic, around 700 massive ships entered the lagoon each year. Venice Has Banned Cruise Ships but That Won’t Stop Them |Barbie Latza Nadeau |April 23, 2021 |The Daily Beast 

Turneffe Flats sits on Turneffe Atoll, a 300-square-mile series of hundreds of palm-fringed islands, endless mangroves, clear lagoons, and unbroken reefs — the very picture of pristine Caribbean beauty. With covid protocols, a Caribbean fly-fishing haven is back in business |Chris Santella |April 2, 2021 |Washington Post 

The smallest fossil could fit on a pinky nail, and has no bones or teeth—it’s just soft tissue imprinted in the bottom of a muddy lagoon. These fossilized lamprey hatchlings disprove an age-old evolutionary theory |Philip Kiefer |March 11, 2021 |Popular-Science 

No one knows exactly why 29-year-old Iranian costume design student Mahtab Savoji turned up dead in the Venice lagoon last week. Iranian Found In Venice Lagoon, Alleged Victim Of Botched Sex Game |Barbie Latza Nadeau |February 5, 2014 |DAILY BEAST 

I wind up driving into a lagoon of some kind and presumably drowning. Grand Theft Auto V Review: The Best Game Ever?! |Winston Ross |September 17, 2013 |DAILY BEAST 

A one-legged torso wearing only a stiletto-heeled boot was found floating in a Venice lagoon. Headless Torso Found in Venice Lagoon |Barbie Latza Nadeau |June 2, 2013 |DAILY BEAST 

She was the Brooke Shields we fell in love with in Blue Lagoon—but of age. Charlie Sheen On ‘Anger Management’, Lindsay Lohan, Partying & More |Marlow Stern |January 16, 2013 |DAILY BEAST 

He called out something about his fish, and soon after passed out of sight into the lagoon. Three More John Silence Stories |Algernon Blackwood 

"It's across the lagoon," Dr. Silence cried, but this time in full tones that paid no tribute to caution. Three More John Silence Stories |Algernon Blackwood 

And an answering cry sounded across the lagoon—thin, wailing, piteous. Three More John Silence Stories |Algernon Blackwood 

She looked long away from me across the lagoon and at last sighed, like one who has drunk deeply, and turned to me. The New Machiavelli |Herbert George Wells 

Making our way in the direction pointed out, we saw before us a creek falling into the lagoon. In the Wilds of Florida |W.H.G. Kingston