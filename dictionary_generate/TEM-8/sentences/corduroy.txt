As the weather grew cooler, my son discovered a corduroy jacket that had once belonged to my father. In this tough year as a parent, I found a story of hope |Holly McGhee |May 31, 2021 |Washington Post 

His slate corduroy trousers were slightly short in the leg—he is a tall, slim man. The Country Gentleman of Physics - Issue 100: Outsiders |Michael Brooks |May 12, 2021 |Nautilus 

These butterflies require a 12-foot swath of fresh corduroy for their fluttering. Cross-Country Skiers Have Ruined My Dog Walks |Sundog |April 3, 2021 |Outside Online 

Once you learn to trust the high edge angles, you’ll leave deep tracks in fresh corduroy. Long-Term Review: The Best Skis, Period |Heather Schultz and Marc Peruzzi |March 4, 2021 |Outside Online 

Your skis should be there to support you, not turn into a chattery mess the minute you venture off the corduroy. These New Skis Have Gone Too Far |Kimberly Beekman |February 4, 2021 |Outside Online 

But my all-time most memorable item of clothes was a pair of green corduroy trousers I wore for nine months of art school. Allan Gurganus: How I Write |Noah Charney |October 16, 2013 |DAILY BEAST 

An elderly man in a yellow corduroy jacket stood patiently by, waiting to speak. Twin Protests |Jacob Silverman |November 16, 2012 |DAILY BEAST 

This kid came in who was wearing corduroy pants, even though it was hot. James Van Der Beek on Katie Holmes and the Secrets of ‘Dawson’s Creek’ |Ramin Setoodeh |April 10, 2012 |DAILY BEAST 

He wears the same corduroy pants that Uncle Ben gave him on his twenty-first birthday. David Lannarck, Midget |George S. Harney 

When only a few planks are used the term "corduroy the bank" is used (see Fig. 37). Elements of Plumbing |Samuel Dibble 

The blazed trail gave way to the corduroy road, and the pack horse to the oxcart or the stage. The Canadian Dominion |Oscar D. Skelton 

Taking his knife, the boy slit the leg of the corduroy trousers, and then carefully rolled the woolen sock down. The Ranger Boys and the Border Smugglers |Claude A. Labelle 

They were all dressed out in new light-blue capotes and corduroy trousers, which they tied at the knee with beadwork garters. Hudson Bay |R.M. Ballantyne