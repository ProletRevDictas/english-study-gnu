In terms of horror, “The Medium” has some good scares but they are sandwiched between boring stretches where I mostly just walked through long hallways, or climbed obstacles, feeling lost. ‘The Medium’ review: A disjointed, unfulfilling puzzle horror game |Elise Favis |February 1, 2021 |Washington Post 

She has been able to keep the coronavirus at bay, but two weeks ago, she had a scare that forced her to close and get everyone tested after a child became ill. Essential workers get lost in the vaccine scrum as states prioritize the elderly |Lena H. Sun, Isaac Stanley-Becker, Akilah Johnson |February 1, 2021 |Washington Post 

An artist and critic argues attention is a scare resource and should be directed where it can provide the most value. Washington Post paperback bestsellers | |January 26, 2021 |Washington Post 

The real flagship SoC is the Snapdragon 888, so Qualcomm's use of "flagship" here definitely belongs in scare quotes. Qualcomm repackages last year’s flagship SoC as the “Snapdragon 870” |Ron Amadeo |January 19, 2021 |Ars Technica 

The health scare comes amid a contentious divorce with his wife, Nicole Young, who is reportedly seeking $2 million a month in temporary spousal support and another $5 million in attorney fees. Dr. Dre says he’s ‘doing great’ at the hospital after suffering a reported brain aneurysm |Timothy Bella |January 6, 2021 |Washington Post 

“Cultures” Versus “White Girls” As you can probably sense from my scare quotes, you can never be too careful these days. Solange Smacks Jay Z, Legolas Slaps Bieber, and the Biggest Celebrity Feuds of the Year |Amy Zimmerman |December 24, 2014 |DAILY BEAST 

Mrs. Deshales ordered an ambulance, which managed to scare off Wahlberg and his pals. Mark Wahlberg’s Pardon Plea: A Look Back At His Troubling, Violent, and Racist Rap Sheet |Marlow Stern |December 7, 2014 |DAILY BEAST 

As we have seen, it is not just the fact that they scare people. Does Free Speech Cover Murder Fantasies? The Supreme Court’s Definition of a ‘Threat’ |Geoffrey R. Stone |December 1, 2014 |DAILY BEAST 

Keep the scare quotes around gay “marriage,” or at least put an asterisk after it. RFRA Madness: What’s Next for Anti-Democratic ‘Religious Exemptions’ |Jay Michaelson |November 16, 2014 |DAILY BEAST 

An HIV scare, Rand Paul talking points, and a (maybe) proposition. My Bizarre Night With James Deen, Libertarian Porn Star |Emily Shire |November 12, 2014 |DAILY BEAST 

The submarine scare is full on; the beastly things have frightened us more than all the Turks and all their German guns. Gallipoli Diary, Volume I |Ian Hamilton 

Then the other girls followed; but, just as Nelly got on, Robert Wood shook the plank, and tried to scare her. The Nursery, August 1873, Vol. XIV. No. 2 |Various 

London was growing quiet, so that the shrieks of the late newsboys with the evening scare could be distinctly heard there. The Weight of the Crown |Fred M. White 

At night the train is increased by the addition of a torch-bearer, to scare off the wild beasts by the glare of his torch. A Woman's Journey Round the World |Ida Pfeiffer 

I'm half afraid to spend another night in the sleeper after the scare we got last night. The Outdoor Girls in the Saddle |Laura Lee Hope