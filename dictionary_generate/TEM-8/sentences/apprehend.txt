Minimal police investigation into McCoy’s death meant a dearth of evidence, so that when two suspects were apprehended and finally prosecuted three years later, both men were ultimately acquitted. How an unsolved murder and a public housing crisis led to Candyman |Aja Romano |August 27, 2021 |Vox 

It says that when Arlington Police initially attempted to apprehend Price, “he jumped off of a platform and ran into a subway tunnel towards the Pentagon Metro Station.” Suspect charged in murder of trans woman in PG County apartment |Lou Chibbaro Jr. |July 22, 2021 |Washington Blade 

In 2020, 26 percent of migrants apprehended by Border Patrol had been caught more than once, compared to 7 percent the previous year. The Texas GOP’s border wall fantasy |Nicole Narea |July 9, 2021 |Vox 

“The individual who appeared to possibly be under the influence was apprehended and removed from the roadway,” Ly said. Person on Metro tracks prompts brief service suspension on three rail lines |Justin George |July 8, 2021 |Washington Post 

Officers from the Animal Welfare League of Alexandria “apprehended the snake” on Sunday after a caller said it was found near the 400 block of Gibbon Street, the organization said. Rattlesnake found in Old Town Alexandria |Martin Weil |June 16, 2021 |Washington Post 

Somebody yanks Chan and elbows him and he is momentarily distracted trying to apprehend his assailant. Protesters Slimed This Good Samaritan Cop |Michael Daly |December 16, 2014 |DAILY BEAST 

In the script I worked on, a man pursues a woman in order to apprehend her husband. Alfred Hitchcock’s Fade to Black: The Great Director’s Final Days |David Freeman |December 13, 2014 |DAILY BEAST 

As the OSS pieced together the Operation Bernhard network, it made plans to apprehend those participants not already in custody. On the Trail of Nazi Counterfeiters |Dr. Kevin C. Ruffner |September 20, 2014 |DAILY BEAST 

Boya Dee live-tweeted the moments after the attack when police arrived to apprehend the suspects—injuring both. Rapper Live-Tweets London 'Terror' Attack |Brian Ries |May 22, 2013 |DAILY BEAST 

Finally, even if the court did decide to pursue charges, it would be unable to apprehend wanted suspects. Who's Afraid Of The ICC? |Mark Leon Goldberg |November 28, 2012 |DAILY BEAST 

She began to look with her own eyes; to see and to apprehend the deeper undercurrents of life. The Awakening and Selected Short Stories |Kate Chopin 

Angels have a constitution which distinguishes them from man, yet with him they apprehend the authority of the one moral law. The Ordinance of Covenanting |John Cunningham 

Though Richard could not fail to apprehend the implied dismissal, he was minded at first to disregard it. Mistress Wilding |Rafael Sabatini 

He had his uncle's revolver with him, but there was little reason to apprehend danger from wild beasts. Gold-Seeking on the Dalton Trail |Arthur R. Thompson 

A God who enjoys a power which nothing in the world can resist, can He apprehend that His intentions could be thwarted? Superstition In All Ages (1732) |Jean Meslier