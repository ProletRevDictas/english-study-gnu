Compounding the issue is the fact that the Muslim community is also stratified on caste lines, in ways that mirror the Hindu system. With Religious Tensions Worsening in India, Understanding Caste Is More Urgent Than Ever |Suprakash Majumdar |April 6, 2022 |Time 

So I think what we really want to pay attention to, and we will be doing this increasingly, is thinking about better ways of grouping and stratifying individuals and populations. How the Human Genome Project revolutionized understanding of our DNA |Tina Hesman Saey |February 9, 2022 |Science News 

Access to home ownership is also remarkably stratified based on race. Netflix’s Marriage or Mortgage Dances Around the Dark Reality of Americans’ Finances |Annabel Gutterman |March 10, 2021 |Time 

These rebundling attempts show how stratified the streaming ecosystem is becoming. How the future of TV was reshaped by 2020 |Tim Peterson |December 16, 2020 |Digiday 

Another important consideration is how a survey is weighted or stratified to accurately represent population groups with differing voting patterns. How The Washington Post’s polling average works |Scott Clement, Emily Guskin |October 16, 2020 |Washington Post 

Society will stratify itself according to the laws of social gravitation. A Mortal Antipathy |Oliver Wendell Holmes, Sr. 

A gas, as explained, is of such a character that it remains fixed and will not stratify or condense. Motors |James Slough Zerbe 

It will stratify, and force itself onward through the adjacent and opposing atmosphere, and in a right line. The Philosophy of the Weather |Thomas Belden Butler 

But already the Mississippi Valley was beginning to stratify, both socially and geographically. The Frontier in American History |Frederick Jackson Turner