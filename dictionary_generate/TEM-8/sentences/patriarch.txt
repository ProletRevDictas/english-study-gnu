The recent Trumpworld legal shakeup isn’t confined to the patriarch of the family. Trumpworld’s Star Lawyers Exit as Storm Clouds Gather |Asawin Suebsaeng, Maxwell Tani, Roger Sollenberger, Adam Rawnsley |September 23, 2021 |The Daily Beast 

The practice of confiscating child support payments from the poor persists in part because some policymakers believe that welfare should be considered a loan, to be repaid by the patriarch of the family. These Single Moms Are Forced to Choose: Reveal Their Sexual Histories or Forfeit Welfare |by Eli Hager, photography by Adria Malcolm, special to ProPublica |September 17, 2021 |ProPublica 

Always a shady, controversial figure, the patriarch was posthumously revealed to have plundered his employees’ pension funds to avoid bankruptcy. What’s the Point of Trying to Understand Ghislaine Maxwell? |Judy Berman |June 24, 2021 |Time 

Three members of the Ebrahimi family, whose patriarch made a fortune at the software firm Quark, collectively converted $65 million into Roths in 2010 and 2011. Lord of the Roths: How Tech Mogul Peter Thiel Turned a Retirement Account for the Middle Class Into a $5 Billion Tax-Free Piggy Bank |by Justin Elliott, Patricia Callahan and James Bandler |June 24, 2021 |ProPublica 

Hana Alomair’s Whispers, Netflix’s first Saudi thriller, revolves around the death of a patriarch whose secrets spill out days before the launch of his company’s new app. The Next Ava DuVernays |Sean Culligan |April 25, 2021 |Ozy 

The talk radio phenom and Fox News staple has identified himself as a longtime listener of the TRN patriarch. The Godfather of Right-Wing Radio |Caitlin Dickson |November 23, 2014 |DAILY BEAST 

Marston declared that women should rule the world but remained a patriarch. Wonder Woman’s Creation Story Is Wilder Than You Could Ever Imagine |Tom Arnold-Forster |November 3, 2014 |DAILY BEAST 

The patriarch, Josiah, had fought with the 42nd Wisconsin Infantry, marching all the way to Kentucky to battle the Confederates. Those Kansas City Blues: A Family History |Katie Baker |October 24, 2014 |DAILY BEAST 

But it was Sean Connery who was the first choice to voice the Disney patriarch with the tragic fate. ‘The Lion King’ Turns 20: Every Crazy, Weird Fact About the Disney Classic |Kevin Fallon |June 24, 2014 |DAILY BEAST 

In Moscow, Patriarch Kirill addressed an audience that included Russian President Vladimir Putin. Blood, Faith and Fatalism in Divided Ukraine |Anna Nemtsova |April 21, 2014 |DAILY BEAST 

I had those words in my thoughts four years ago, when I cut him down from the branch of the Patriarch. Blackwood's Edinburgh Magazine, No. CCCXXXIX. January, 1844. Vol. LV. |Various 

It is thus that I have seen it stated in the credentials granted to the said Fleuche, first Patriarch of those lands. The Jesuit Relations and Allied Documents, Vol. II: Acadia, 1612-1614 |Various 

The Patriarch went to him, and, with the help of an interpreter, did for him what pertained to his office as a good Pastor. The Jesuit Relations and Allied Documents, Vol. II: Acadia, 1612-1614 |Various 

They boast of having descended from Abraham through Ishmael, believing that this patriarch built Mecca and died there. A Philosophical Dictionary, Volume 1 (of 10) |Franois-Marie Arouet (AKA Voltaire) 

The sufferings of the patriarch Job form the subject of a few of these scriptural illustrations. The Catacombs of Rome |William Henry Withrow