Just 40 minutes from Anchorage, this ski town at the base of Alyeska Resort has a wild, unpretentious feel. The Best Ski Towns to Live in or Visit Right Now |eriley |February 4, 2022 |Outside Online 

Favoring mellow miles over speedy strides, this unpretentious ­workhorse—a waterproof version of Brooks’s bestselling neutral trainer—gets refreshed with more of the brand’s softest EVA midsole. The Best Road and Trail Running Shoes of 2022 |jversteegh |October 26, 2021 |Outside Online 

An aerospace engineer by training, Calandrelli is young, deliberately unpretentious, and wildly enthusiastic—and at the time, she was nine months pregnant. Wonder woman |Christina Couch, SM ’15 |April 28, 2021 |MIT Technology Review 

Xi’s crackdown gave a boost to New World wines—think Napa Valley, Chile, and Australia—as China’s image-conscious middle class looked for premium, but unpretentious, bottles. Australia’s ‘approachable’ wine won over China’s middle class. Then came the tariffs |eamonbarrett |December 7, 2020 |Fortune 

When you are not walking, you drive a Volvo—a solid, unpretentious vehicle. The first murder |Katie McLean |August 19, 2020 |MIT Technology Review 

We try to be as unpretentious a band as possible and enjoy writing fun pop songs. Future Islands Frontman Samuel T. Herring on Their 11-Year Journey to Letterman and Viral Stardom |Marlow Stern |April 3, 2014 |DAILY BEAST 

He pulls the reader in with his unpretentious, laconic style, and with his refusal to shy away from acknowledging his own flaws. Seeking Reconciliation with a Terrorist: A Jewish Journey |Lisa Goldman |September 11, 2013 |DAILY BEAST 

“[Ellen did] a crisp and unpretentious job,” wrote Tom Shales in The Washington Post. Ellen DeGeneres Is the Perfect Choice to Host the 2014 Oscars |Kevin Fallon |August 2, 2013 |DAILY BEAST 

On the other hand, she thought he was an unpretentious software genius. Why ‘Arrested Development’s’ Fourth Season Is a Bust: Contracts and More |Marlow Stern |May 28, 2013 |DAILY BEAST 

Bergoglio is unpretentious and an advocate for the disadvantaged. The New Pope Is an Advocate for the Poor |Mac Margolis |March 13, 2013 |DAILY BEAST 

A quiet, unpretentious old border town is Hereford, pleasantly located on the banks of the always beautiful Wye. British Highways And Byways From A Motor Car |Thomas D. Murphy 

The houses of the well-off were unpretentious outside, but were fitted inside with comfort and even elegance. The Towns of Roman Britain |James Oliver Bevan 

Even the most unpretentious houses in Pompeii have in them the remains of staircases (Fig. 44). The Private Life of the Romans |Harold Whetstone Johnston 

Sit down, sister; we are very proud and very happy that you have accepted our unpretentious invitation. Maupassant Original Short Stories (180), Complete |Guy de Maupassant 

Something also it owed to its unpretentious yet practical and utilitarian character. Art in England |Dutton Cook