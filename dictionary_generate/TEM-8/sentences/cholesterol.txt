Others are a softer mix of cholesterol, fats, calcium, and other substances, and these mixed plaques are more dangerous and likely to rupture. There’s New Evidence on Heart Health in Endurance Athletes |mmirhashem |August 7, 2021 |Outside Online 

Statin drugs are commonly prescribed to lower cholesterol, and they may help improve coronavirus outcomes in patients who had been taking them prior to hospitalization, according to a new study published in the journal Plos One. COVID cases are plaguing the Olympics even before they’ve begun |Lauren Leffer |July 19, 2021 |Popular-Science 

More than one in three American adults have high cholesterol, which can lead to serious health problems like heart disease and stroke. One CRISPR Treatment Lowered Cholesterol in Monkeys by 60 Percent |Vanessa Bates Ramirez |May 26, 2021 |Singularity Hub 

I set aside a small amount of the sauce before adding the shrimp for my sister who is watching her cholesterol. Six Recipes That Got Us Through Another Week |Eater Staff |March 19, 2021 |Eater 

Bile is in fact produced by the liver from cholesterol and then stored in the gallbladder. Your liver does more than you give it credit for |By Marie-Pierre Hasne/The Conversation |March 17, 2021 |Popular-Science 

Or should you swear it off in the name of better cholesterol? Bulletproof Coffee and the Case for Butter as a Health Food |DailyBurn |December 27, 2014 |DAILY BEAST 

What happens when someone with an eating disorder history is diagnosed with high cholesterol? You’re Never ‘Cured’ of an Eating Disorder |Carrie Arnold |December 20, 2014 |DAILY BEAST 

At 96 percent water, cukes have no saturated fat or cholesterol, and are very high in vitamin K, vitamin B6 and iron. 10 Ways to Stay Hydrated (That Aren’t Water) |DailyBurn |July 25, 2014 |DAILY BEAST 

And at the end of the study there was no difference in the LDL-cholesterol levels between the two groups! The AHA’s Absurd Saturated Fat Obsession |Dr. Barbara H. Roberts |June 3, 2014 |DAILY BEAST 

In fact, half of the people who have cardiac events have “ideal” levels of LDL cholesterol. The AHA’s Absurd Saturated Fat Obsession |Dr. Barbara H. Roberts |June 3, 2014 |DAILY BEAST 

Cholesterol was a little below normal in the four cases examined. Scurvy Past and Present |Alfred Fabian Hess 

Cholesterol is frequently found in animal fats, and phytosterol is a very similar substance present in vegetable fats. The Handbook of Soap Manufacture |W. H. Simmons 

The cholesterol in linseed or fish oil, which of course may be present in the soap, also give this reaction. Soap-Making Manual |E. G. Thomssen