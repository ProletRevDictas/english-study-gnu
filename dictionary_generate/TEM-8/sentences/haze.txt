Though 2021 began in a haze of uncertainty, we’re now some five months in, and for many of us—finally—things are looking brighter. The Best Movies of 2021 So Far |Stephanie Zacharek |May 25, 2021 |Time 

It is one of the first trees to bloom in spring, when its charcoal gray bark is smothered in a haze of tiny rose-pink blossoms that emerge directly from the wood. The gardener’s guide to redbuds |Adrian Higgins |March 4, 2021 |Washington Post 

The thin haze of clouds parted as we entered a valley deep in the Monashee Mountains of southeast British Columbia. After the Crash, They Said I Was Fine. I Wasn't. |Erin Tierney |February 4, 2021 |Outside Online 

As I type this, smoky air from the Sierra Nevada mountains is pouring into San Francisco, casting a deep orange haze across the city, as well as much of the American west. How to breathe easier in America’s Smoke Belt |Michael J. Coren |September 9, 2020 |Quartz 

Look closely and you can see that the haze is divided into dozens of layers. These Images Expose the Dark Side of the Solar System - Issue 89: The Dark Side |Corey S. Powell |August 26, 2020 |Nautilus 

“I personally think that his performance is unbelievable,” says Haze. James Franco and Scott Haze on 'The Sound and the Fury' and Gawker 'Outing' Them As A 'Couple' |Marlow Stern |September 6, 2014 |DAILY BEAST 

Not according to Haze, who is decidedly Zen about his Method approach to Ballard. Scott Haze on Playing a Necrophiliac in ‘Child of God’ and Naked Paintballing with James Franco |Melissa Leon |August 3, 2014 |DAILY BEAST 

The model Ireland Baldwin is less ambiguous but even more defiant when it comes to her relationship with the rapper Angel Haze. Cara Delevingne, Ireland Baldwin, and How Sexually Uninhibited Models Are Bucking the Male Gaze |Amanda Marcotte |July 2, 2014 |DAILY BEAST 

With the country sinking ever further into a prescription drug-induced haze, one state has decided to fight back. Prescription Drugs More Deadly Than Car Accidents, Guns, and Suicide |Charlotte Lytton |May 25, 2014 |DAILY BEAST 

After lunch, I entered a mental haze: reaction times dropped 12% and short-term memory 11%. How to Use Your Lunch Hour for Better Productivity, Without Ever Taking a Bite |Gregory Ferenstein |April 23, 2014 |DAILY BEAST 

The belated moon stole up from its lair, hovered above the sky-line, a gaudy orange sphere in the haze of smoke. Raw Gold |Bertrand W. Sinclair 

At ten o'clock a very thick haze spread over the land and so enveloped it that nothing could be distinguished. Narrative of a Survey of the Intertropical and Western Coasts of Australia] [Volume 2 of 2] |Phillip Parker King 

Clouds of haze moved around, and when the moon came up she seemed to be glowering from her shroud. The Chequers |James Runciman 

A pallid haze breathes wanly on the surface of the impassive sky. The Dragon Painter |Mary McNeil Fenollosa 

The upper edge of the sun was just visible above the horizon, gleaming through the haze like a speck of ruddy fire. The Floating Light of the Goodwin Sands |R.M. Ballantyne