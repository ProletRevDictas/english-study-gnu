As the ice receded, we devised more means of survival and comfort—stone dwellings, ploughs, wheeled vehicles. Anthropocene: Human-Made Materials Now Weigh as Much as All Living Biomass, Say Scientists |Jan Zalasiewicz |December 24, 2020 |Singularity Hub 

I order a swing-top bottle of German beer, and then Erik and I plough through a couple liters of red wine. Exploring the Darker Side of James Joyce’s Trieste |Jeff Campagna |January 13, 2014 |DAILY BEAST 

Existing methodology can involve plough-like, armor-plated machines, handheld metal detectors and sniffer dogs. Diana Landmine Conspiracies Return |Martyn Gregory |June 6, 2010 |DAILY BEAST 

E was an Esquire, with pride on his brow; F was a Farmer, and followed the plough. Boys and Girls Bookshelf; a Practical Plan of Character Building, Volume I (of 17) |Various 

But I will plough one more field this week; though, I know not why it is, my thoughts go against it even now. Ramona |Helen Hunt Jackson 

Since words have different meanings, we may sometimes find that a pair of words exemplify all three Laws, as plough and sword. Assimilative Memory |Marcus Dwight Larrowe (AKA Prof. A. Loisette) 

Day after day while the spring ploughing went on, the strange pair followed the plough together. The Tale of Grandfather Mole |Arthur Scott Bailey 

There was also one at the Plough and Harrow, and several may stil be found in the neighbourhood. Showell's Dictionary of Birmingham |Thomas T. Harman and Walter Showell