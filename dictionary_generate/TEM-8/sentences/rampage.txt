They were even escorted down the steps of the Capitol when they were finished with their rampage. Delusional Insurrectionists Just Won’t Stop |Shani Parrish |March 5, 2021 |Essence.com 

As traders on Reddit’s WallStreetBets channel extol each other to send GameStop shares into the stratosphere, questions are growing about whether the rampage has crossed a line into stock manipulation. Good luck proving Reddit traders did anything illegal by pumping GameStop |John Detrixhe |January 28, 2021 |Quartz 

Though the rioters had planned their rampage on the Capitol on the dark web and social media, the Capitol Police offered little resistance to their entering the building. Storming of the Capitol should give you nightmares |Kathi Wolfe |January 14, 2021 |Washington Blade 

Many who took part in the rampage at the Capitol came equipped with powerful symbols emblazoned on flags, banners and t-shirts. Flags, Hate Symbols and QAnon Shirts: Decoding the Capitol Riot |Francesca Trianni |January 11, 2021 |Time 

The rampage came after workers at the Narasapura plant were underpaid for months. Apple’s first iPhone supplier in India stretched its workers too far—and they snapped |Ananya Bhattacharya |December 21, 2020 |Quartz 

Earlier that day, officials say, Stone went on a bloody rampage killing six of his kin and wreaking havoc in three small towns. Hunt for Iraq Vet After Killing Spree |M.L. Nestel |December 16, 2014 |DAILY BEAST 

Following his rampage, Courture-Rouleau was shot and killed. Terrorist Ends Canada’s Innocence |Tim Mak |October 22, 2014 |DAILY BEAST 

ISIS had broadly advertised its acquisition of a broad range of U.S.-made weapons during its rampage across Iraq. ISIS Video: America’s Air Dropped Weapons Now in Our Hands |Josh Rogin |October 21, 2014 |DAILY BEAST 

As the opening rampage stretches into hours, a question arises: Where is help? Westgate's Chilling Security Video Reveals Shopping Mall Bloodbath |Nina Strochlic |September 15, 2014 |DAILY BEAST 

After the rampage, Demetrius Hewlin, 16, Daniel Parmertor, 16, and Russell King Jr., 17, lay dead. How To Plan A Jailbreak |Seth Ferranti |September 13, 2014 |DAILY BEAST 

These are the days when the cattle become discontented with their pasture and begin to go on a rampage. The Red Cow and Her Friends |Peter McArthur 

Buckley ought to have killed that bull long ago—that's the second time he's gone on a rampage. The Boy Land Boomer |Ralph Bonehill 

One night a bear got in there and made such an awful noise that we thought the Indians were on a rampage. Old Rail Fence Corners |Various 

The next time she starts in on any such rampage, just pick her up and carry her out, as any naughty child should be carried. Peggy Stewart at School |Gabrielle E. Jackson 

Out of the sun they had come like so many crazed hornets on the rampage. Dave Dawson on the Russian Front |R. Sidney Bowen