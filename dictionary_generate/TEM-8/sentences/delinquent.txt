Over 11,200 San Diegans behind on their water bill owe more than $1,000, which accounts for 16 percent of the total delinquent bills. San Diegans Are Drowning in Water Debt During COVID-19 |MacKenzie Elmer |January 27, 2021 |Voice of San Diego 

If they overzealously collect on delinquent loans, the public relations and regulatory cost could be high—at a time when the monetary value of trying to collect on many of those loans is at an all-time low. Bankers were the villains of the last recession. They can be heroes in this one |matthewheimer |December 11, 2020 |Fortune 

As McDonald noted in a 2008 Brookings op-ed, local ward bosses, “who knew their neighbors intimately, dispensed jobs and favors for votes” and political machines “paid supporters’ taxes in states that disenfranchised tax delinquents.” The 2020 Election Set a Record for Voter Turnout. But Why Is It Normal for So Many Americans to Sit Out Elections? |Olivia B. Waxman |November 5, 2020 |Time 

Until this year, lenders booked credit costs, called “provisions,” mostly based on the length of time loans were delinquent. How JPMorgan Chase is proceeding with extreme caution—and still making plenty of money |Shawn Tully |October 14, 2020 |Fortune 

What’s remarkable is that so far, JPMorgan is seeing a tiny fraction of its loans go delinquent. How JPMorgan Chase is proceeding with extreme caution—and still making plenty of money |Shawn Tully |October 14, 2020 |Fortune 

I was also the front for a juvenile delinquent roaming the streets of New York City and using me as a parental alibi. Where Cellphone Numbers Go to Die |Jon Methven |August 16, 2014 |DAILY BEAST 

It also traces his days as a juvenile delinquent, and gradual rise up the R&B charts. ‘Get On Up’ Star Chadwick Boseman on Becoming James Brown—With A Little Help From Mick Jagger |Marlow Stern |August 4, 2014 |DAILY BEAST 

When I was little, I was a kind of juvenile delinquent, but my father stayed on me. Shaq, Year One |Charles P. Pierce |May 24, 2014 |DAILY BEAST 

Now, the juvenile delinquent and artist formerly known as Marky Mark is at the top of the Hollywood food chain. Mark Wahlberg Discusses ‘Lone Survivor,’ Kate Moss in Playboy, the Red Sox, and More |Marlow Stern |December 23, 2013 |DAILY BEAST 

Once someone asked him what J.D. stood for, and he famously told them, “juvenile delinquent.” ‘Salinger,’ the Documentary on Reclusive Author J.D. Salinger, Premieres at Telluride |Marlow Stern |September 2, 2013 |DAILY BEAST 

The reason is that the pre-delinquent is not attracted by such forms of recreation or healthy pleasure. Report of the Special Committee on Moral Delinquency in Children and Adolescents |Oswald Chettle Mazengarb et al. 

It is a curious and amusing fact that the great smuggler (p. 267) and real delinquent was Napoleon himself. The Life of Napoleon Bonaparte |William Milligan Sloane 

The hubbub increased behind him; and several peace-officers of the Royal Household came up to apprehend the delinquent. The Fortunes of Nigel |Sir Walter Scott 

These parsons are used to being victimised and are known not to be too harsh upon the delinquent. Punch, or the London Charivari, Vol. 147, September 2nd, 1914 |Various 

Non-delinquent or non-sequestrated private patronage and the obligation of tithes were retained. Encyclopaedia Britannica, 11th Edition, Volume 9, Slice 4 |Various