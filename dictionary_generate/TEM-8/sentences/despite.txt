Despite the strong language, however, the neither the JPO nor Lockheed could dispute a single fact in either Daily Beast report. Pentagon Misfires in Stealth Jet Scandal |Dave Majumdar |January 8, 2015 |DAILY BEAST 

NEW ORLEANS — John Boehner was reelected House Speaker yesterday by his Republican colleagues despite some dissenting members. The Price of Steve Scalise’s Silence |Jason Berry |January 7, 2015 |DAILY BEAST 

Its biggest asset, of course, is the steely Atwell, who never asks you to feel sorry for Carter despite all the sexism around her. Marvel’s ‘Agent Carter’ Stomps on the Patriarchy |Melissa Leon |January 7, 2015 |DAILY BEAST 

The rebels though seemed somewhat chastened by the result despite more than doubling the anti-Boehner votes from two years ago. Democrats Accidentally Save Boehner From Republican Coup |Ben Jacobs, Jackie Kucinich |January 6, 2015 |DAILY BEAST 

The copilot on Flight 8501 was Remi Emmanuel Piesel, 46, who despite his age had just 2,275 hours of flying experience. Annoying Airport Delays Might Prevent You From Becoming the Next AirAsia 8501 |Clive Irving |January 6, 2015 |DAILY BEAST 

The conclusion is reached that, despite these drawbacks, the Jesuit mission in Canada has made a hopeful beginning. The Jesuit Relations and Allied Documents, Vol. II: Acadia, 1612-1614 |Various 

Some critics feel that, despite much that is desirable in her work, the soul is lacking in the women she paints. Women in the fine arts, from the Seventh Century B.C. to the Twentieth Century A.D. |Clara Erskine Clement 

Hunter-Weston despite his heavy losses will be advancing to-morrow which should divert pressure from you. Gallipoli Diary, Volume I |Ian Hamilton 

Long Jack was a prominent, but despite his joviality, it seems to me a pathetic figure. Fifty Years of Railway Life in England, Scotland and Ireland |Joseph Tatlow 

Despite the arrogant manner of his address, Garnache felt prepossessed in the newcomer's favour. St. Martin's Summer |Rafael Sabatini