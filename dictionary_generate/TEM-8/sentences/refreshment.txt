Given the difference in time zones, some participants were eating meals on screen, while others were sipping cocktails and some simply sat without refreshments. Miss Manners: Save me from these interminable video ‘parties’ |Judith Martin, Nicholas Martin, Jacobina Martin |February 1, 2021 |Washington Post 

These cups can elevate any refreshment, so here’s a list of some of our favorites available today. Stainless steel cups for any beverage |PopSci Commerce Team |January 14, 2021 |Popular-Science 

Also, juicy fruit, decent acidity for refreshment, and a smile to enhance our conversation or dinner. This $11 Bordeaux begs for pizza or burgers and will put a smile on your face |Dave McIntyre |December 18, 2020 |Washington Post 

There’s a pop-up cocktail table too, ensuring that refreshments are close at hand while cheering for your favorite equestrians. The first Rolls-Royce SUV has tricks that might actually justify its price tag |Dan Carney |October 5, 2020 |Popular-Science 

There were no intermissions at performances, or refreshments available. Why Vienna opera singers are ready to risk their lives to perform in a pandemic |Julia Belluz |September 30, 2020 |Vox 

And when I passed the refreshment stand on the way out of the theater, I could not help but think of Skittles. Hollywood, Shootings, and ‘2 Guns’: When Is Stylized Violence Obscene? |Michael Daly |July 30, 2013 |DAILY BEAST 

The life of the gallery is dependent on the renewal and refreshment of its artists and dealers. 50 Years of Fabulous Art |Paul Laster |September 14, 2010 |DAILY BEAST 

You must be greatly in want of some refreshment, for the wretched posadas on the road cannot have offered you any thing eatable. Blackwood's Edinburgh Magazine, Volume 60, No. 372, October 1846 |Various 

Aristide stood gossiping until the Mayor invited him to take a place at the table and consume liquid refreshment. The Joyous Adventures of Aristide Pujol |William J. Locke 

However, she was not seeking refreshment or help from any source, either external or from within. The Awakening and Selected Short Stories |Kate Chopin 

By noon they had reached Voiron, and here, at a quiet hostelry, they descended to pause awhile for rest and refreshment. St. Martin's Summer |Rafael Sabatini 

She bade Marius call Fortunio, and then dismissed the courier, bidding her captain see to his refreshment. St. Martin's Summer |Rafael Sabatini