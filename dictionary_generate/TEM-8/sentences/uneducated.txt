In recent years, many analysts have unfairly caricatured the so-called “surprise” electoral victories of populist leaders as the result of uneducated voters brainwashed by disinformation. The World Should Be Worried About a Dictator’s Son's Apparent Win in the Philippines |Jonathan Corpus Ong |May 10, 2022 |Time 

I’m putting my foot in mouth every fourth word because we’re talking about things that have to do with deaf and hearing culture and I am so uneducated. On ARRAY’s 10th Anniversary, Ava DuVernay Reflects On A Decade of Disruption & Her Own Unconscious Bias |Brande Victorian |December 10, 2021 |Essence.com 

Moreover, the risks embedded in crypto assets might end up largely falling in the laps of uneducated investors who get swept up at the tail end of speculative frenzies. The Future of Crypto Is Bright, But Governments Must Help Manage the Risks |Eswar Prasad |October 22, 2021 |Time 

I am always sensitive, I always call my brother because I want to make sure I say the right things…I know I am uneducated, but I am full of love. “I can be part of the change,” LGBTQ ally Country artist, Miranda Lambert |Staff reports |August 6, 2021 |Washington Blade 

Moreover, uneducated Americans have a competitive advantage because of their fluency in English. The Case for More Low-Skill Immigration |Veronique de Rugy |December 7, 2014 |DAILY BEAST 

Out West, in a tepee, on a reservation, alcohol, drug abuse, drain on society, poverty, uneducated—beaten down. A Millennium After Inventing the Game, the Iroquois Are Lacrosse’s New Superpower |Evin Demirel |July 21, 2014 |DAILY BEAST 

The violence and uneducated anger that my husband endured will not be the world in which my son grows up. A Message of Hope in Response to the Attack on Prabhjot Singh |Manmeet Kaur |September 25, 2013 |DAILY BEAST 

Maybe it's because we're educated and many uneducated white people don't understand what really happened. Daily Beast Readers on the Zimmerman Verdict |Melissa Fares |July 16, 2013 |DAILY BEAST 

The gun-control debate is not a new one, and the voters who care about it are not uneducated on the subject. Hillary Was Right: Obama’s Inexperience Sank Gun Deal |Stuart Stevens |April 19, 2013 |DAILY BEAST 

Children, like uneducated adults, have been known to take a spectacle on the stage of a theatre too seriously. Children's Ways |James Sully 

Several uneducated business men are said to have written to the Dean asking the Latin for what they think of the new Budget. Punch, or the London Charivari, Volume 158, April 28, 1920 |Various 

To converse with an entirely uneducated person upon literature, interlarding your remarks with quotations, is ill-bred. The Ladies' Book of Etiquette, and Manual of Politeness |Florence Hartley 

Such provisions prevented many uneducated Negroes from participating in elections. Hallowed Heritage: The Life of Virginia |Dorothy M. Torpey 

The village schoolmasters, uneducated themselves, and mostly unpaid, make but a feeble impression. Robert Moffat |David J. Deane