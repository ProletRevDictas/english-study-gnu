I thought I caught sight of one recently when Richard Barrera, a school board member for San Diego Unified School District, said online learning had opened up new possibilities for students in lower-income areas. When a Calculus Class Abruptly Became Ceramics at Lincoln High |Scott Lewis |September 16, 2020 |Voice of San Diego 

The startup’s main value proposition is that it makes it easier and cheaper for businesses to analyze data they’ve shelved away on the cloud—including the massive datasets needed to train machine learning algorithms. Snowflake’s IPO is a bet on companies using AI for everything |Nicolás Rivero |September 16, 2020 |Quartz 

When schools began transitioning to remote learning, students’ needs changed, too. How to drive digital innovation necessary during the pandemic |Nick Chasinov |September 16, 2020 |Search Engine Watch 

The app provides an endless scrollable feed of short, user-generated videos, surfaced by a machine learning system that tracks what you watch and interact with. TikTok’s enormous value isn’t just in its algorithm |Adam Epstein |September 15, 2020 |Quartz 

Indeed, there may be no way to stop the rise of machine learning, but there’s still time to prepare. When it comes to A.I., worry about ‘job churn’ instead of ‘job loss’ |jonathanvanian2015 |September 15, 2020 |Fortune 

What is most troubling is our – and I do mean “our” and not “their” – never treating these situations as learning opportunities. In 2015, Let’s Try for More Compassion |Gene Robinson |January 4, 2015 |DAILY BEAST 

In the absence of typical classrooms and curriculums, West Africans have opted for alternate methods of learning and education. The Radio Battle to Educate Ebola’s Kids |Abby Haglage |December 11, 2014 |DAILY BEAST 

These tests prod and poke the children, creating lots of anxiety and taking away from the joy of learning. Hunger Games Comes to New York State’s Public Schools |Zephyr Teachout |November 26, 2014 |DAILY BEAST 

He is honest about his religious doubts, but he is committed to learning more about God. The Good Wife’s Religion Politics: Voters Have No Faith in Alicia's Atheism |Regina Lizik |November 24, 2014 |DAILY BEAST 

And Glenn, Tara, and Rosita spend the episode, um, learning to fish. The Walking Dead’s ‘Crossed’: The Stage Is Now Set for a Bloody, Deadly Midseason Finale |Melissa Leon |November 24, 2014 |DAILY BEAST 

All our intelligent students will insist upon learning what they can of these discussions and forming opinions for themselves. The Salvaging Of Civilisation |H. G. (Herbert George) Wells 

Thomas Cooper, an English prelate, died; highly commended for his great learning and eloquence. The Every Day Book of History and Chronology |Joel Munsell 

He was a patriot of the noblest and most extensive views, and justly celebrated as a man of learning, eloquent and refined. The Every Day Book of History and Chronology |Joel Munsell 

It has only been a rare and exceptional class hitherto that has gone on learning throughout life. The Salvaging Of Civilisation |H. G. (Herbert George) Wells 

At twelve, or fifteen, or sixteen, or twenty it was decided that they should stop learning. The Salvaging Of Civilisation |H. G. (Herbert George) Wells