Easton’s premium EC90AX wheelset is roadie light at just 1,470 grams yet stiff enough for the rigors of gravel racing. New Gravel Bike Accessories for a Smoother Ride |Josh Patterson |September 28, 2020 |Outside Online 

Researchers use racing as a testing ground for auto technologies “because it’s so extreme and binary,” Peak said. Self-driving cars will hit the Indianapolis Motor Speedway in a landmark A.I. race |jonathanvanian2015 |September 19, 2020 |Fortune 

Last year, in the second quarter, F1 shared some $335 million in profits with the racing franchises. How data helped keep Red Bull’s F1 team on track during the pandemic |Jeremy Kahn |September 19, 2020 |Fortune 

Those adjustments are tested in the wind tunnel prior to racing. How data helped keep Red Bull’s F1 team on track during the pandemic |Jeremy Kahn |September 19, 2020 |Fortune 

Stepping down as CEO in 2017, Slootman followed his passion into the world of regatta sailboat racing known as Pac52 for the high-tech 52-foot long sailing vessels involved. Meet Snowflake, one of the buzziest tech IPOs ever |Aaron Pressman |September 15, 2020 |Fortune 

They were racing toward the corner of Tompkins and Myrtle avenues with Johnson at the wheel when another call came over the radio. 'Please Don't Die!': The Frantic Battle to Save Murdered Cops |Michael Daly |December 22, 2014 |DAILY BEAST 

Today, former TNR writers and the rest of the media establishment are racing to denounce Hughes. The Rise and Fall of Chris Hughes and Sean Eldridge, America’s Worst Gay Power Couple |James Kirchick |December 9, 2014 |DAILY BEAST 

Jolly, who entered the racing world when she was eight years old, remembers being taunted as a kid. The Moms of Monster Jam Drive Trucks, Buck Macho Culture |Eliza Krigman |November 22, 2014 |DAILY BEAST 

Her smile flattened out, and I could see her mind racing as she thought over what, if anything, this would mean for us. I Shot Bin Laden |Elliot Ackerman |November 16, 2014 |DAILY BEAST 

She had previously been married to a racing-car driver who had been killed, leaving her with one child. Gay Talese on Charlie Manson’s Home on the Range |Gay Talese |October 31, 2014 |DAILY BEAST 

It was at this parliament that the famous acts against horse racing and deceitful gaming were passed. The Every Day Book of History and Chronology |Joel Munsell 

Two Battalions racing due North along the coast and foothills with levelled bayonets. Gallipoli Diary, Volume I |Ian Hamilton 

Lucy Warrender, ever ready for mischief, feigned an intense interest in racing matters, but failed to draw Lord Hetton. The Pit Town Coronet, Volume I (of 3) |Charles James Wills 

Eudora was racing now through the briers, and weeds, and palmetto stumps, and dragging Mandy Ann with her. The Cromptons |Mary J. Holmes 

As the canoe drew near the girls saw a wild mob of children, both boys and girls, racing toward the broken landing. The Campfire Girls of Roselawn |Margaret Penrose