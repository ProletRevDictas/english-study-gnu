On the other hand, it’s also obvious that the pandemic has sent them into a basement where they won’t stay for long. After soaring, then plunging, earnings are heading for a ‘new normal.’ But where is that, exactly? |Shawn Tully |October 13, 2020 |Fortune 

Another seller in Arlington did $24,000 of work over a six-month period, including finishing a partially finished basement, and netted $65,000 over asking price and $115,000 over the comparable homes. What to know before selling your home in fall, winter 2020 |Khalil Alexander El-Ghoul |October 11, 2020 |Washington Blade 

You’d rather not risk itIt’s easy to disregard the notion of paranormal activity in broad daylight, but everything changes when you head into a dark basement. Why do we see ghosts? |Jake Bittle |October 6, 2020 |Popular-Science 

So, if you’re planning on storing wines for a year or more, it’s best to put them in a designated location with consistent temperature, such as a deep basement or wine fridge, where they’re out of the way. Why right now is the time to start aging your wine collection |Rachel King |October 4, 2020 |Fortune 

You don’t really question the ways it goes from Walter trying to figure out what to do about one drug dealer he has tied up in his basement to Walter using a car key fob to trigger a trunk-mounted machine gun that will destroy his enemies. Saturday Night Live will not save you |Emily VanDerWerff |October 2, 2020 |Vox