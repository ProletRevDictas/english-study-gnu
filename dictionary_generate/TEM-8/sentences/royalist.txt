Upon his death, his nine-year-old son was left with what remained of his royalist supporters. England’s Greatest Knight Puts ‘Game of Thrones’ to Shame |William O’Connor |December 9, 2014 |DAILY BEAST 

The Royalist is urgently contacting Russian Tatler to see this extraordinary document with our own eyes. How To Fire Your Maid, By A Russian Oligarch |Tom Sykes |October 1, 2014 |DAILY BEAST 

Kate is less than 12 weeks pregnant, a source confirmed to the Royalist. Second Royal Baby for Kate |Tom Sykes |September 8, 2014 |DAILY BEAST 

The Royalist has always maintained that Harry's red hair is a Spencer, not a Hewitt trait. Prince Harry Definitely Not James Hewitt's Son According To DNA Test |Tom Sykes |September 6, 2014 |DAILY BEAST 

No pictures of him cavorting naked in hotel rooms have yet been received by the Royalist. Newly Single Harry Parties In Miami! |Tom Sykes |May 1, 2014 |DAILY BEAST 

Once—twice, the chorus of that old English Royalist song rose up out of the grove. Raw Gold |Bertrand W. Sinclair 

Accordingly he set off by land, receiving a promise of protection from the royalist commander, but no escort. Napoleon's Marshals |R. P. Dunn-Pattison 

The royalist expedition against Quiberon, assisted by English munitions and money, terminated disastrously for the royal cause. The Every Day Book of History and Chronology |Joel Munsell 

He was a deputy sitting on the right and upholding the Royalist platform of Divine Right. Repertory Of The Comedie Humaine, Complete, A -- Z |Anatole Cerfberr and Jules Franois Christophe 

He was the fourth son of a good bourgeoisie family of Autun, a faithful prelate, an obstinate Royalist and a man of intelligence. Repertory Of The Comedie Humaine, Complete, A -- Z |Anatole Cerfberr and Jules Franois Christophe