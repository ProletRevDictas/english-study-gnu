Because of their reliance on snowfall for their natal dens, wolverines are a very climate-vulnerable species, says Chestnut, and the warming climate is shrinking their viable habitats. Mount Rainier’s first wolverine mama in a century is a sign of the species’ comeback |Hannah Seo |August 28, 2020 |Popular-Science 

Add the election, the Census, stimulus checks and a new reliance on home deliveries, and mail feels perhaps more important than ever. Info About Local Post Office Operations Is Conflicting and Hard to Come By |Ashly McGlone and Kate Nucci |August 27, 2020 |Voice of San Diego 

We could also see a greater reliance on social signals from Facebook and Twitter. What the commoditization of search engine technology with GPT-3 means for Google and SEO |Manick Bhan |August 21, 2020 |Search Engine Watch 

Scammers are exploiting remote workers’ increased reliance on digital tools, says Evan Reiser, an ex-Twitter product manager who now heads Abnormal. Don’t click that Zoom invitation! |rhhackettfortune |August 19, 2020 |Fortune 

Severin Borenstein, an energy economist at the University of California, Berkeley, who serves on the governing board of the Independent System Operator, says the state’s growing reliance on renewables “definitely” played a role in the blackouts. Here’s how to keep California’s grid from buckling under the heat |James Temple |August 18, 2020 |MIT Technology Review 

Christianity spawned the monastic movement, originally in Egypt, that insisted upon peace, self-reliance, education and charity. Karen Armstrong’s New Rule: Religion Isn’t Responsible for Violence |Patricia Pearson |October 29, 2014 |DAILY BEAST 

Petit says that somebody once told him they understood his reliance on his eyesight, his sense of touch, even his sense of smell. Philippe Petit’s Moment of Concern Walking the WTC Tightrope |Anthony Haden-Guest |August 8, 2014 |DAILY BEAST 

My big gripe with the old TV shows was their reliance on predictable formulas. Five Lessons the Faltering Music Industry Could Learn From TV |Ted Gioia |August 3, 2014 |DAILY BEAST 

Age of Ultron will also address our reliance on technology and the fear of where it could lead. ‘Avengers: Age of Ultron’ Unmasked: Robert Downey Jr. and Co. Speak at Comic-Con |Annaliza Savage |July 27, 2014 |DAILY BEAST 

Even so, writers of suspense fiction vary in their reliance on and dedication to fact. Writing a Novel: Even Making It Up Requires Research |Ridley Pearson |July 16, 2014 |DAILY BEAST 

What reliance could repose upon a house, divided against itself—not safe from the extravagance and pillage of its own members? Blackwood's Edinburgh Magazine, No. CCCXXXIX. January, 1844. Vol. LV. |Various 

As we learn more and more of the laws of Nature we put less and less reliance on the effect of prayer. God and my Neighbour |Robert Blatchford 

Modern times, we are convinced, have witnessed but few instances of such a masterly policy, combined with signal self-reliance. Blackwood's Edinburgh Magazine, No. CCCXXXIX. January, 1844. Vol. LV. |Various 

Indeed, individual cases show a virtual lack of self-reliance. Report of the Special Committee on Moral Delinquency in Children and Adolescents |Oswald Chettle Mazengarb et al. 

To this day in Servia the popular forms of swearing express dependence and reliance upon the powers of nature. A Cursory History of Swearing |Julian Sharman