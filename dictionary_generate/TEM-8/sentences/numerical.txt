The program converts the words for supermarket inventory items into numerical data so neural networks can process them. How to make A.I. smarter |jonathanvanian2015 |September 21, 2020 |Fortune 

Over time, as a neural network is trained on additional text, it groups words according to numerical scores measuring how frequently the words appear near each other. How to make A.I. smarter |jonathanvanian2015 |September 21, 2020 |Fortune 

The report doesn’t give a reason for why hospitals chose not to participate in the survey, but it points out those that did not participate did not receive a numerical score assessing their LGBTQ policies. HRC examines hospital policies, impact of COVID on LGBTQ people |Lou Chibbaro Jr. |September 16, 2020 |Washington Blade 

As an editor, I’ve long had mixed feelings about the journalistic tradition of marking particular chronological or numerical milestones. America Is About to Lose Its 200,000th Life to Coronavirus. How Many More Have to Die? |by Stephen Engelberg |September 14, 2020 |ProPublica 

For each of the questions below, provide a numerical range with the bottom end below your best guess and the high end above your best guess. A tool to practice overcoming “the mother of all biases”: overconfidence |Walter Frick |September 6, 2020 |Quartz 

A numerical data set helps physicians and other doctors know when to order scans of any kind. Are Routine Scans Causing Cancer? |Dale Eisinger |September 17, 2014 |DAILY BEAST 

Shares of the youth-oriented social-media company push past an important numerical barrier. Facebook Stock Pushes Above $40 |Sarah Langs |August 23, 2013 |DAILY BEAST 

He has no illusions about the status of his numerical designation. Literary Bond Superior to Movie Version |Allen Barra |November 11, 2012 |DAILY BEAST 

While there are several numerical pathways to electoral victory for either man, the math is harder for Romney without Ohio. Mitt Romney Needs Ohio to Win the Election |Mark McKinnon |October 24, 2012 |DAILY BEAST 

In other words, the bulk of the serious numerical evidence is that Virginia isn't even a swing state. New Batch of Polls: The Land Is Still Sliding! |Michael Tomasky |August 7, 2012 |DAILY BEAST 

As “t” stands for 1, and o and y are vowels, and have no figure value, the numerical value of Toy must be 1. Assimilative Memory |Marcus Dwight Larrowe (AKA Prof. A. Loisette) 

Good clothes, quiet demeanour, and numerical smallness are the striking characteristics. Our Churches and Chapels |Atticus 

It was itself an appeal to the discontent of the numerical majority, not invested with a share in the government. The Loyalists of America and Their Times, Vol. 1 of 2 |Egerton Ryerson 

It was the only one of Napoleon's great engagements in which he admitted his numerical superiority to his enemy. The Life of Napoleon Bonaparte |William Milligan Sloane 

Guichen was joined by a Spanish fleet which gave him a great numerical superiority. The Political History of England - Vol. X. |William Hunt