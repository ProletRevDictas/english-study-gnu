One of those sharks seemed to be in its own little watery world, doing its own weird thing that had little-to-nothing to do with the actual planned choreography. The Weeknd’s Super Bowl performance sparked a bounty of memes, as the halftime show usually does |Travis Andrews |February 8, 2021 |Washington Post 

No choreography, music licensing, famous actors — possibly even no director. The 5 worst Super Bowl commercials, from Dolly Parton’s betrayal to that awkward Oatly jingle |Maura Judkis, Sonia Rao |February 8, 2021 |Washington Post 

That original video consisted of relatively easy group choreography set to the song’s catchy hook. How one hit song won The Weeknd a Super Bowl halftime show |Aja Romano |February 5, 2021 |Vox 

To plot out the choreography of this complex dance, two separate groups seized upon a theorem devised by mathematician Leonhard Euler way back in the 18th century. How the Earth-shaking theory of plate tectonics was born |Carolyn Gramling |January 13, 2021 |Science News 

The staging and the careful choreography of the scenes, with fireworks and synchronized dance, enhanced the storytelling. How ‘Bridgerton’ flipped the script on ‘The Duke and I’ |Vanessa Riley |January 12, 2021 |Washington Post 

Backup dancers materialize, sparking a torrent of eye-catching choreography. The Making of Kiesza: From Navy Sharpshooter to Beauty Queen to Pop Diva |Marlow Stern |October 20, 2014 |DAILY BEAST 

The revival swept in glam costumes, elegant choreography, and cheeky fun, sometimes weaving in social commentary. Best Career Arc Ever: From Burlesque To Bartending |Anne Berry |September 13, 2014 |DAILY BEAST 

Apparently, the sexually suggestive choreography and aggressive twerking was viewed as a threat to the Latin American nation. Miley Cyrus Goes To War Against the Dominican Republic Government |Asawin Suebsaeng |September 3, 2014 |DAILY BEAST 

OK Go Use Their Illusion Alt-pop band OK Go is no stranger to single-take music videos with impressive choreography. Duck-pocalypse, Animated Mormons, and More Viral Videos |Alex Chancey |June 22, 2014 |DAILY BEAST 

Sure, her gee-golly choreography is unbearable cute, but watch her eyes. 7 Impossibly Cute Shirley Temple Moments (VIDEOS) |Kevin Fallon |February 11, 2014 |DAILY BEAST 

Fokine created the choreography of L'Oiseau de Feu section by section, as the music was handed to him. An Autobiography |Igor Stravinsky 

Besides creating the decorative setting and the beautiful costumes, he inspired the choreography even to the slightest movements. An Autobiography |Igor Stravinsky 

The music of that dance, clear and well defined, demanded a corresponding choreography—simple and easy to understand. An Autobiography |Igor Stravinsky 

The admirably successful choreography of these three ballets came from Bronislava Nijinska's inexhaustible talent. An Autobiography |Igor Stravinsky 

It is hardly surprising in these circumstances that the choreography of Le Baiser de la Fe left me cold. An Autobiography |Igor Stravinsky