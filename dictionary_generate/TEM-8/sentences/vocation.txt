Over the course of three deployments to Afghanistan and Iraq, during which she flew 89 combat missions and then served as “a ground-based problem solver,” McGrath had to reckon with the violent nature of her vocation. After breaking military barriers, Amy McGrath came to question the mission |Elizabeth Samet |August 27, 2021 |Washington Post 

For the few women who were in athletics, sports were seen as more a hobby than a vocation. Time For The End Of The Teen Gymnast |Dvora Meyers |July 27, 2021 |FiveThirtyEight 

The Jameses say that of all the vocations they’ve studied, professional athletes are among the most enthusiastic about sleep and even napping. Nap Time for Everyone! (Ep. 468) |Stephen J. Dubner |July 8, 2021 |Freakonomics 

Though he eventually lost the girl, he gained a vocation, and, in 2009, at the age of 20, he joined American Medical Response. The Broken Front Line |by Ava Kofman, photography by Kendrick Brinson and David Walter Banks |April 7, 2021 |ProPublica 

Unfortunately the church has routinely forgotten its vocation. What is the Point of Celebrating Easter During a Pandemic? |N.T. Wright |April 2, 2021 |Time 

“The golden age of Parisian smiles nurtured, and was nurtured by, the rise of dentistry as a vocation,” writes Jones. The French Court’s Royal Ban on Smiles |William O’Connor |December 14, 2014 |DAILY BEAST 

It was only once he directed and starred in his own short film that he decided to pursue acting as a vocation. Renaissance Man Jared Leto Defies Categorization |The Daily Beast |December 8, 2014 |DAILY BEAST 

He was highly esteemed in the fashion world, but his real vocation was fulfilled in the evening hours. When Downtown Was Cool: Mario Batali, Simon Doonan, Wynton Marsalis Remember the Good Old Days |The Daily Beast |April 10, 2014 |DAILY BEAST 

History has no shortage of vocation-induced tragi-ironic deaths. Bible Passages that Could Get You Killed |Candida Moss |February 18, 2014 |DAILY BEAST 

In short, this Austin native needs to make some hard decisions about his vocation. Do Blues Musicians Need to be Really, Really Old? |Ted Gioia |September 22, 2013 |DAILY BEAST 

And everything else is certainly in keeping with our dwelling and our vocation in life, that is, poverty. The Jesuit Relations and Allied Documents, Vol. II: Acadia, 1612-1614 |Various 

We engaged an experienced hack-driver, who combined with his vocation the qualities of a well informed guide as well. British Highways And Byways From A Motor Car |Thomas D. Murphy 

Has it never occurred to you, my dear Miss Warrender, that it might be your vocation, your natural aim in life. The Pit Town Coronet, Volume II (of 3) |Charles James Wills 

A divinity presided over bakers, another over ovens,--every vocation and every household transaction had its presiding deities. Beacon Lights of History, Volume I |John Lord 

Another and more serious question for the clergyman is that of the vocation in life of those who are weak mentally. Essays In Pastoral Medicine |Austin Malley