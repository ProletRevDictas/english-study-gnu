About a century later, the sport was adapted for purely human use in Britain, with athletes running from town to town, steeple to steeple, while surmounting the obstacles without the assistance of a horse. The wet and wild history of steeplechase, Olympic track and field’s splashiest event |Matt Bonesteel |June 25, 2021 |Washington Post 

There were creeks for fresh water and floodwrack for firewood and the woods so thick you could practically sleep within sight of a church steeple or police station and no one would know. Sebastian Junger on Walking America’s Railroads |Sebastian Junger |May 19, 2021 |Outside Online 

The tallest building in the town where I grew up might have been three stories—the church steeple probably was the tallest thing. Charles Frazier in the Fast Lane |Malcolm Jones |October 14, 2011 |DAILY BEAST 

From the top of the Church steeple, the highest point in this prefabricated settlement, I could only gawk at the carnage below. The Extinction Parade: An Original Zombie Story by Max Brooks |Max Brooks |January 14, 2011 |DAILY BEAST 

Note to Wall Street tourists: If you need to pee, look for the steeple. Ringing in the New Year on Wall Street |Beau Willimon |January 5, 2010 |DAILY BEAST 

They walked along the track, picking out a church steeple here and there, forgetting for a moment the object of their search. The Box-Car Children |Gertrude Chandler Warner 

Before mounting to his bed in the steeple, he went to offer a pious prayer to the Lady of Le Puy. The Merrie Tales Of Jacques Tournebroche |Anatole France 

It has a very churchly look, and if the steeple were at the other end, it would be equally orthodox. Our Churches and Chapels |Atticus 

The original design includes a beautiful steeple, surmounted with pinnacles; but want of funds precludes its erection. Our Churches and Chapels |Atticus 

It is near the western edge of the village of Greenough, the gilt cupola of whose eminent steeple is noted by far-passing ships. The Belted Seas |Arthur Colton