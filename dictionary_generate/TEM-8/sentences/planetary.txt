If your main concern is planetary health, plant-based foods are the better choice. Is Fast Food Healthier When It's Plant-Based? |Christine Byrne |February 3, 2021 |Outside Online 

Certain destinations like the moon and Mars have always been at the forefront of planetary scientists’ minds, especially as we’ve learned more about the history of water on both bodies. We’re living in a golden age of sample return missions |Neel Patel |February 2, 2021 |MIT Technology Review 

Careful crater counting suggests it could be as old as 1 million years, or as youthful as 53,000 years — yesterday, in planetary terms. Rumbles on Mars Raise Hopes of Underground Magma Flows |Robin George Andrews |February 1, 2021 |Quanta Magazine 

Two hundred light years away sits a planetary system unlike any other yet discovered. These 6 exoplanets somehow orbit their star in perfect rhythm |Charlie Wood |January 27, 2021 |Popular-Science 

The drive for infinite economic growth is leading to a planetary ecological crisis. Big Tech’s attention economy can be reformed. Here’s how. |Gideon Lichfield |January 10, 2021 |MIT Technology Review 

Some of those plans for planetary defense sound like they were ripped right out of a Michael Bay movie. U.S.—Russia Tensions Nuke Plans For Real-Life ‘Armageddon’ Asteroid Defense |Asawin Suebsaeng |August 4, 2014 |DAILY BEAST 

The leading neocons competed with each other to come up with the most grandiose vision of Middle East and planetary restructuring. Oliver Stone and Peter Kuznick On How Obama Should Handle the Crisis In Syria |Oliver Stone, Peter Kuznick |October 15, 2013 |DAILY BEAST 

Higher incomes lead to greater demand on planetary resources and more interdependence. Big Idea: The Time for Divided Nations Is Over |Ian Goldin |August 1, 2013 |DAILY BEAST 

“The Martian Chroniclers”Burkhard Bilger, The New Yorker A new era in planetary exploration. The Week’s Best Longreads for April 20, 2013 |David Sessions |April 20, 2013 |DAILY BEAST 

The Daily Pic: Michael Benson's planetary art hovers between sci-fi and astronomy. Isaac Asimov or Carl Sagan? |Blake Gopnik |February 11, 2013 |DAILY BEAST 

Being only halfway in control of his own planetary system was no state to be found in by the first interstellar visitors. Fee of the Frontier |Horace Brown Fyfe 

Still, it will be healthier if we push out of this planetary system before someone else pushes in. Fee of the Frontier |Horace Brown Fyfe 

In the stages of concentration the planetary nebulæ might well repeat those through which the greater solar mass proceeded. Outlines of the Earth's History |Nathaniel Southgate Shaler 

In an orbit made elliptical by the planetary attraction the sun necessarily occupies one of the foci of the ellipse. Outlines of the Earth's History |Nathaniel Southgate Shaler 

The conditions under which this vivifying tide is received have their origin in the planetary motion. Outlines of the Earth's History |Nathaniel Southgate Shaler