This caused a minor uproar, as my husband slipped it into a conversation about something else. Carolyn Hax: Husband didn’t share his dad’s cancer diagnosis for a year |Carolyn Hax |January 26, 2021 |Washington Post 

Some users had thought the planned changes would let Facebook read WhatsApp messages, which caused an uproar. WhatsApp delays privacy policy changes after users defect to rivals Signal and Telegram |Jonathan Vanian |January 15, 2021 |Fortune 

Yet Olevskiy’s appearance on the show drew him publicly into the uproar. Radio Free Europe fires a prominent Russian journalist — and the Kremlin smirks |Paul Farhi |December 16, 2020 |Washington Post 

Last week, HBO Max announced — to an uproar — that every major theatrical release Warner Brothers had planned for 2021 would be available on HBO Max too. ‘Marketing at maximum intensity’: Publishers ready for rocketing ad spending from streamers in 2021 |Max Willens |December 9, 2020 |Digiday 

“When the first studies started coming out about antibody responses to SARS-CoV-2, everyone was in an uproar about the response being potentially defective,” says Nina Luning Prak, an immunologist at the University of Pennsylvania. Immunity to COVID-19 may persist six months or more |Erin Garcia de Jesus |November 24, 2020 |Science News 

There was a bit of an uproar from Tolkien purists about her being included in... ‘No Regrets’: Peter Jackson Says Goodbye to Middle-Earth |Alex Suskind |December 4, 2014 |DAILY BEAST 

But the killers clearly failed to anticipate the uproar that would follow. Mexico’s First Lady of Murder Is on the Lam |Michael Daly |October 29, 2014 |DAILY BEAST 

There was frenzied uproar when she participated in a literacy program to encourage kids to read. Porn Stars Are People Too, Dammit: Lisa Ann’s Notre Dame Date and the Trolling of David Gregory |Aurora Snow |October 25, 2014 |DAILY BEAST 

While Kilmeade walked back the comment the next day after an uproar, he did not apologize. The Fox News Apology Tour |Dean Obeidallah |October 1, 2014 |DAILY BEAST 

These comments incited an uproar among Iroquois fans believing Kessenich had disrespected their tradition. A Millennium After Inventing the Game, the Iroquois Are Lacrosse’s New Superpower |Evin Demirel |July 21, 2014 |DAILY BEAST 

His largesses were abundant, and the uproar of vehement thanksgiving, was ever on the watch from the venal multitude. The Pastor's Fire-side Vol. 3 of 4 |Jane Porter 

It had come on to rain, and the raw dampness mingled itself with the dusky uproar of the Strand. Confidence |Henry James 

I thought we were in for an encore performance, but gradually the uproar died away, and by midnight all was quiet. Gallipoli Diary, Volume I |Ian Hamilton 

Hell below was in an uproar to meet thee at thy coming, it stirred up the giants for thee. The Bible, Douay-Rheims Version |Various 

Above the uproar of the reeling earth the shriek of the train sounded in his deafened ears. A Lost Hero |Elizabeth Stuart Phelps Ward and Herbert D. Ward