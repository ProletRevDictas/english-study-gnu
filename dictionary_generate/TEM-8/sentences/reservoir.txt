The unit’s reservoir holds enough water for 90 seconds of use. Water flossers that get between your teeth |PopSci Commerce Team |August 27, 2020 |Popular-Science 

From 2009 to 2018, researchers measured the carbon content of springwater fed by the Velino aquifer, which is near the epicenter of the 2009 L’Aquila quake and sits atop a reservoir of CO2 in Earth’s crust. Carbon dioxide from Earth’s mantle may trigger some Italian earthquakes |Maria Temming |August 26, 2020 |Science News 

Although salty water can be an extreme environment, the presence of an ocean suggests there might be more of these briny water reservoirs located elsewhere on the dwarf planet, raising hopes Ceres was once a habitable world—and might still be. The dwarf planet Ceres might be home to an underground ocean of water |Neel Patel |August 11, 2020 |MIT Technology Review 

Tensions further arose when satellite images revealed a growing reservoir behind the GERD, angering Egypt and Sudan that had demanded that Ethiopia should not start filling the dam without an agreement. What Egypt, Sudan and Ethiopia must overcome to all benefit from the Grand Renaissance Dam |Addisu Lashitew |July 22, 2020 |Quartz 

We also need rain to fill the reservoirs that provide drinking water. Let’s learn about rain |Bethany Brookshire |June 17, 2020 |Science News For Students 

He finished second in 2008 behind John McCain, and maintains a reservoir of good will among Republican social conservatives. Can Huckabee Convert the GOP’s Moneymen? |Lloyd Green |January 4, 2015 |DAILY BEAST 

That had to give them an enormous reservoir of moral strength and solace. Hitler’s Hail Mary |James A. Warren |December 20, 2014 |DAILY BEAST 

“The reservoir for filovirus has remained a huge mystery,” Bruenn said in 2010. Ebola's Roots Are 50 Times Older Than Mankind. And That Could Be the Key to Stopping It. |Michael Daly |October 20, 2014 |DAILY BEAST 

Reservoir Dogs did fantastic internationally, so everyone was waiting for my new movie. The Secrets of ‘Pulp Fiction’: 20 Things You Didn’t Know About the Movie on Its 20th Anniversary |Marlow Stern |October 19, 2014 |DAILY BEAST 

The animal reservoir for SARS is bats, whereas the reservoir for MERS is primarily camels. Is Middle East Respiratory Syndrome (MERS) the Next SARS? |Dr. Anand Veeravagu, MD, Tej Azad |May 3, 2014 |DAILY BEAST 

The bag, being blown up, forms a wind reservoir and the amount of tone can be regulated by the pressure of the arm. The Recent Revolution in Organ Building |George Laing Miller 

Then some genius steadied the wind pressure by pumping air into a reservoir partly filled with water. The Recent Revolution in Organ Building |George Laing Miller 

A big tank that the city used to have for a reservoir had been bought by a sugar company and turned into a storage for molasses. The Sunbridge Girls at Six Star Ranch |Eleanor H. (Eleanor Hodgman) Porter 

He was not writing yet; he was filling up his soul with the thing, making it a reservoir of impressions. Love's Pilgrimage |Upton Sinclair 

And always the usual work of the suction-pumps went on, those pumps now fixed to this great reservoir of millions. The Nabob |Alphonse Daudet