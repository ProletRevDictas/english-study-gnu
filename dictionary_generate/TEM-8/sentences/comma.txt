I wanted it to be a dramedy, although I’d call it more of a comma. How ‘Freaks and Geeks’ went from misfit dramedy to cult classic, as told by its cast and creators: ‘People just like it so much that it thrusts itself from the grave’ |Sonia Rao |January 27, 2021 |Washington Post 

Fake ones use more generic words like “vacation,” “family” and “experience” accompanied by a lot of commas and exclamation marks. How to spot fake reviews on travel sites |Roy Furchgott |October 29, 2020 |Washington Post 

The analysis found characteristics strongly suggestive of bots — such as double commas and dangling commas that often appear with automatic scripts — though at least some of the accounts were being operated by humans. Pro-Trump youth group enlists teens in secretive campaign likened to a ‘troll farm,’ prompting rebuke by Facebook and Twitter |Isaac Stanley-Becker |September 15, 2020 |Washington Post 

Cut down on any unnecessary characters like commas and spaces. Why site speed is critical for your SEO success and how to make it happen |Anthony Gaenzle |September 4, 2020 |Search Engine Watch 

Those little tiny objects are made up of unintelligible commas, spaces, numbers and all sorts of nebulous characters. SEO on a shoestring budget: What small business owners can do to win |Ali Faagba |June 4, 2020 |Search Engine Watch 

Can it be exported to Excel, or a comma-separated file, for instance? Self-Tracking for N00bz |Jamie Todd Rubin |July 24, 2014 |DAILY BEAST 

It was more like punctuation, a real life comma that emphasizes the constant pressures of our daily schedule. Why Men Shouldn’t Wait To Have Kids |Conor P. Williams |March 8, 2014 |DAILY BEAST 

Sort of a combination grammar and punctuation problem, is what I call the “however comma splice.” Ben Yagoda: How I Not Write Bad |Noah Charney |February 13, 2013 |DAILY BEAST 

Power is the subject, and the execution is precise—even if this book will make you miss the comma terribly. Nicholson Baker, Katie Kitamura, and This Week’s Hot Reads: July 30, 2012 |Jimmy So |July 30, 2012 |DAILY BEAST 

Is it a kind of punctuation, part comma, part full stop, part interrogatory mark? Does Mitt Romney Know How to Laugh? |Michael Tomasky |May 21, 2012 |DAILY BEAST 

It was bordered by trees for almost its entire length on both sides, and it was shaped like a enormous, elongated comma. The Campfire Girls of Roselawn |Margaret Penrose 

Page 229 Chapter X a comma was inserted in the phrase 'he would secure the competence he had yearned for, for so many years'. The Pit Town Coronet, Volume II (of 3) |Charles James Wills 

It was done to please him; for I omitted neither accent, nor comma, nor the minutest tittle of all he had marked down. The Best of the World's Classics, Restricted to Prose, Vol. VII (of X)--Continental Europe I |Various 

Lucas bore it patiently; he didn't want his great-grandchildren and Elaine's shooting it out over a matter of a misplaced comma. Space Viking |Henry Beam Piper 

And even if nobody read it through, not even a reviewer, I should have to without skipping a word or a comma. Balloons |Elizabeth Bibesco