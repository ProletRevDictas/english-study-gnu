Those who were at 45 to 50 hours before are down to 24 to 32 hours per week, Cooper said, and he has not seen any extra hiring to make up the lost time. Info About Local Post Office Operations Is Conflicting and Hard to Come By |Ashly McGlone and Kate Nucci |August 27, 2020 |Voice of San Diego 

This feature will help sites that do not have more exposure or access to Google to hopefully rectify the issue faster and thus reduce any lost Google traffic due to a false positive security issue. Google tests reporting security issue false positives in Search Console |Barry Schwartz |August 26, 2020 |Search Engine Land 

Federal regulators say much of the lost money came from people emptying their pockets for security scans. Travelers left nearly a million dollars at airport checkpoints last year |Rachel Schallom |August 22, 2020 |Fortune 

Some Airbnb guests argued the refund process was complicated while hosts complained that the company didn’t do more to compensate them for their lost income. Airbnb’s 3 biggest challenges on its road to an IPO |Danielle Abril |August 20, 2020 |Fortune 

“Nobody wants that, but we may not have a choice, and the lost time that could’ve been used to build up these systems will become really apparent,” she says. The U.S. largely wasted time bought by COVID-19 lockdowns. Now what? |Jonathan Lambert |July 1, 2020 |Science News 

So in that sense we have gotten close to the families that have lost loved ones, be it from one side or the other. Mexico’s Priests Are Marked for Murder |Jason McGahan |January 7, 2015 |DAILY BEAST 

After four or five months of casual interaction, they realized they both had lost a young parent to cancer. Everyone at This Dinner Party Has Lost Someone |Samantha Levine |January 6, 2015 |DAILY BEAST 

He was not originally so uninhibited, however, as can now be seen in his “lost” novel, Skylight. The Lost Novel of Nobel-Winner José Saramago |Charles Shafaieh |January 5, 2015 |DAILY BEAST 

“The origin of Brokpas is lost in antiquity,” a research article from the University of Delhi notes. The Himalayas’ Hidden Aryans |Nina Strochlic |January 3, 2015 |DAILY BEAST 

He lost his bid for a fourth term to George Pataki that year. President Cuomo Would’ve Been a Lion |Jonathan Alter |January 2, 2015 |DAILY BEAST 

The patache was never seen again, and there is not much doubt that it was lost with all hands on board. The Philippine Islands, 1493-1898, Volume XX, 1621-1624 |Various 

This vessel, loaded with supplies, went ashore and was lost; and one hundred and twenty Japanese and three Dutchmen were drowned. The Philippine Islands, 1493-1898, Volume XX, 1621-1624 |Various 

They held the compound against repeated assaults, and lost several men in hand-to-hand fighting. The Red Year |Louis Tracy 

How much of the imagination, how much of the intellect, evaporates and is lost while we seek to embody it in words! Pearls of Thought |Maturin M. Ballou 

Kum Kale has been a brilliant bit of work, though I fear we have lost nearly a quarter of our effectives. Gallipoli Diary, Volume I |Ian Hamilton