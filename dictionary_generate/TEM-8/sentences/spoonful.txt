A spoonful of typical soil could easily contain billions of microbes, and Antarctic soils from other regions host at least a few thousand per gram. Missing Antarctic microbes raise thorny questions about the search for aliens |Elise Cutts |July 20, 2021 |Science News 

Add a large spoonful of icing to the top of the layer, then spread the icing to the edges of the cake. How to Eat Delmarvelously |jversteegh |July 8, 2021 |Outside Online 

I’ve even topped it with a few spoonfuls of plain yogurt for breakfast. Rhubarb shines in this upside-down cornmeal cake |Aaron Hutcherson |June 3, 2021 |Washington Post 

Each spoonful is a delightful reminder that spring, like other times of year, is also soup season. Proof spring is soup season, too: This bright bowl packed with peas, leeks, spinach and herbs |Ellie Krieger |April 8, 2021 |Washington Post 

By tasting, and then swallowing, a lava-hot spoonful of bechamel for a pot pie filling. 20 Days of Turkey |Meghan McCarron |December 23, 2020 |Eater 

Warm milk mixed with a spoonful of fireplace ashes seemed to also be popular among 19th century England. History's Craziest Hangover Cures |Justin Jones |December 30, 2014 |DAILY BEAST 

But the complaints about the current administration did sometimes come with a spoonful of sugar, or at least a cornball of humor. What I Saw at Iowa’s So-Co Circus |Ben Jacobs |August 10, 2014 |DAILY BEAST 

She smacks her lips in satisfaction, then slurps another spoonful of orange broth. South Korea's Extreme Eating Fetishists |Lizzie Crocker |January 31, 2014 |DAILY BEAST 

Saving Mr. Banks is about as aggressively sweet as a spoonful of sugar. ‘Saving Mr. Banks’ Is This Oscar Season’s Breath of Fresh Air |Kevin Fallon |December 12, 2013 |DAILY BEAST 

For each portion, set a breast on the bottom of a warm plate and set a large spoonful of avocado chutney alongside it. Daniel Boulud Reveals His 4 Favorite Recipes From His New Cookbook |Daniel Boulud |October 15, 2013 |DAILY BEAST 

A dessert-spoonful of brandy may be added, if required to keep a long time. The Ladies' Book of Etiquette, and Manual of Politeness |Florence Hartley 

Sirdeller stretched out his hand and very deliberately drank a table-spoonful or two of his milk. The Double Four |E. Phillips Oppenheim 

Here we sat late, and so home to bed, having got my Lady Batten to give me a spoonful of honey for my cold. Diary of Samuel Pepys, Complete |Samuel Pepys 

Young Joe tried to smile, with a slice of chicken in one hand and a spoonful of preserves in the other, and a mouthful of both. The Rival Campers |Ruel Perley Smith 

I looked through the window as my last spoonful of dessert was eaten, and saw the sky breaking into blue. The Idyl of Twin Fires |Walter Prichard Eaton