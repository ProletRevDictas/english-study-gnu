Every call, all over the country, men and women, all said the same thing: Impeach him. The New Cruzians Are Ready to Make Life Hell for Mitch McConnell |Patricia Murphy |November 17, 2014 |DAILY BEAST 

In 2013, for example, Clovis said that it would be difficult to impeach the President “because he claims to be black.” The Far-Right Radio Host Who Could Deliver the Senate to the GOP |Ben Jacobs |October 6, 2014 |DAILY BEAST 

Within the House Judiciary Committee, six Republicans voted with 21 Democrats to impeach the president. Four Decades of Declining Trust in D.C. |Lloyd Green |August 11, 2014 |DAILY BEAST 

Liberal Democrats wanted to impeach President George W. Bush, but Pelosi took it off the table. Pelosi to Boehner: I Quashed Impeachment, and So Can You |Eleanor Clift |August 1, 2014 |DAILY BEAST 

But GOP candidates are making it clear to voters: We will move to impeach Obama. Don’t Be Fooled: The GOP Wants Impeachment |David Freedlander |August 1, 2014 |DAILY BEAST 

He would impeach all his partners, acknowledge his errors, and promise once more to reform. Blackwood's Edinburgh Magazine, No. CCCXXXIX. January, 1844. Vol. LV. |Various 

Let those who are greater, and wiser, and purer than Washington, impeach him. The Right of American Slavery |True Worthy Hoit 

If any president refuse to lend the executive arm of the government to the enforcement of the law, it can impeach the president. The Negro Problem |Booker T. Washington, et al. 

If, when it has passed a law, any Court shall refuse to obey its behests, it can impeach the judges. The Negro Problem |Booker T. Washington, et al. 

Do not think, however, that in making this observation I intend to impeach the character of Philip van Artevelde himself. Blackwood's Edinburgh Magazine, Volume 57, No. 356, June, 1845 |Various