The harbor dredging has been insufficient in recent years and the city has been looking for other ways to restore its eroding coastline. North County Report: Oceanside Moves Ahead on Groins Without Mayor’s Support |Kayla Jimenez |August 18, 2021 |Voice of San Diego 

The Portuguese had heard stories about Benin that compelled them to breach their customary adherence to coastal harbors and take the risk of venturing into the delta. How the language of the Edo people of Nigeria made its way into Portuguese creole |Uwagbale Edward-Ekpu |July 23, 2021 |Quartz 

It was midmorning on a Tuesday and the harbor bath was filled with locals splashing about. Denmark Is Open to Americans—Here’s What It’s Like to Visit Right Now |Mary Holland |July 2, 2021 |The Daily Beast 

Across the harbor, Amass restaurant from acclaimed chef Matt Orlando, also created a pop-up making fried chicken during the pandemic. Denmark Is Open to Americans—Here’s What It’s Like to Visit Right Now |Mary Holland |July 2, 2021 |The Daily Beast 

It is a harbor offering them safety and affirmation amid choppy waters. ‘Here We Can Express Ourselves With Freedom.’ In Puerto Rico, A Trans Collective Is Reimagining Family Values |Alejandra Rosa |June 29, 2021 |Time 

“If Charleston harbor needs improvement, let the commerce of Charleston bear the burden,” he said. Steve Scalise Shows There’s a Fine Line Between Confederate & Southern |Lloyd Green |January 2, 2015 |DAILY BEAST 

It is, Zelden said, “unthinkable” that Scalise would harbor these views. No. 3 Republican Admits Talking to White Supremacist Conference |Tim Mak |December 30, 2014 |DAILY BEAST 

Rights activists like Boledi, the Iranian Baluch dissident living in Sweden, harbor some of the same concerns. The Dangerous Drug-Funded Secret War Between Iran and Pakistan |Umar Farooq |December 29, 2014 |DAILY BEAST 

I harbor a rock ‘n’ roll fantasy, just like anybody, and I welcomed the challenge. Michael C. Hall on Going Drag for ‘Hedwig and the Angry Inch’ and Exorcising ‘Dexter’ |Marlow Stern |December 4, 2014 |DAILY BEAST 

He continued to harbor core conservative beliefs, but started to believe they could be achieved “through liberal structures.” Canada’s Subversive Sock Puppet: Ed the Sock Isn’t Afraid to Say Anything |Soraya Roberts |November 13, 2014 |DAILY BEAST 

This he did, erecting at the harbor a beautiful cross bearing the arms of France. The Jesuit Relations and Allied Documents, Vol. II: Acadia, 1612-1614 |Various 

Whenever he heard of one coming into harbor, he hastened to the shore, and closely watched the disembarking. Ramona |Helen Hunt Jackson 

She had all her life long been accustomed to harbor thoughts and emotions which never voiced themselves. The Awakening and Selected Short Stories |Kate Chopin 

Some time this summer we are going to get up a nice crowd and sail as far as Bar Harbor—maybe. The Campfire Girls of Roselawn |Margaret Penrose 

A crowd watched the ship towed, for safe-keeping, under the guns of the Romney in the harbor. The Eve of the Revolution |Carl Becker