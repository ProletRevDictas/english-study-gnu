“One of the things that NASA is really interested in knowing is whether or not there could be life in the subsurface oceans of the icy moons, like Europa and Enceladus,” says Batalha, of UC Santa Cruz. Life on Earth may have begun in hostile hot springs |Jack J. Lee |September 24, 2020 |Science News 

This is why you can spread salt on an icy sidewalk and the ice will melt. Build ice towers with bottled water and ice |Bethany Brookshire |September 16, 2020 |Science News For Students 

Even in its thinnest spots, the icy crust is probably at least a mile thick, meaning that anything alive must be swimming around in inky darkness. These Images Expose the Dark Side of the Solar System - Issue 89: The Dark Side |Corey S. Powell |August 26, 2020 |Nautilus 

Now he’s devoting some of his attention and expertise to other icy bodies in the solar system where, someday, history may repeat itself. He Found ‘Islands of Fertility’ Beneath Antarctica’s Ice |Steve Nadis |July 20, 2020 |Quanta Magazine 

Afterward, they put the icy mix into a chamber where the air pressure was very low. Silk can be molded into strong medical implants |Sid Perkins |April 2, 2020 |Science News For Students 

These brave souls took an icy dip in the ocean to ring in 2015 and raise money for charity. Diving Into 2015 With Polar Bear Plunge Extremists |James Joiner |January 1, 2015 |DAILY BEAST 

But below the surfaces of many of his films, rude, angry sex simmered; cool, icy blondes were tied up, handcuffed, humiliated. Alfred Hitchcock’s Fade to Black: The Great Director’s Final Days |David Freeman |December 13, 2014 |DAILY BEAST 

Tall, dark, and handsome Ben Affleck against icy blonde Rosamund Pike. David Fincher’s Backseat Feminism |Teo Bugbee |October 9, 2014 |DAILY BEAST 

His vibe was icy as he bluntly remarked, “What are you doing here?” When Gary Wright Met George Harrison: Dream Weaver, John and Yoko, and More |Gary Wright |September 29, 2014 |DAILY BEAST 

But many others will be happy just to get a hot shower before bundling up in their icy apartments. In Ukraine, Winter Is Coming |Anna Nemtsova |September 23, 2014 |DAILY BEAST 

The third line runs thus: “How they tinkle, tinkle, tinkle in the icy air of night.” Assimilative Memory |Marcus Dwight Larrowe (AKA Prof. A. Loisette) 

But the sun was getting warmer and the icy street would soon be slushy and the skates would cut through. The Girls of Central High on the Stage |Gertrude W. Morrison 

As the icy waters closed over him, he struck out boldly for the spot where he had last beheld the struggling youth. The Mystery at Putnam Hall |Arthur M. Winfield 

Slowly but surely Reff Ritter came up out of the icy water, his teeth chattering loudly. The Mystery at Putnam Hall |Arthur M. Winfield 

Their queen alone beheld his approach without a tremor; she turned on him the icy glance of her green eyes. Honey-Bee |Anatole France