In one recent study, a Stanford team made 34 modifications to the yeast’s DNA to chemically assemble a molecule with widespread effects on human muscles, glands, and tissue. Molecular Farming Means the Next Vaccine Could Be Edible and Grown in a Plant |Shelly Fan |August 17, 2021 |Singularity Hub 

Dawson discovered that her glands were barely producing cortisol, a hormone critical to vital body functions. Long-COVID-19 Patients Are Getting Diagnosed With Little-Known Illnesses Like POTS |Cindy Loose / Kaiser Health News |May 27, 2021 |Time 

He and others recently showed where the saliva glands get some of the major proteins they put into human saliva. Cool Jobs: Saliva offers a spitting image of our health |Kathiann Kowalski |May 6, 2021 |Science News For Students 

The researchers think a large, protrusible pheromone gland with lots of surface area could be a workaround, more efficiently dispersing pheromones to be detected by the antennae of would-be suitors. This praying mantis inflates a strange pheromone gland to lure mates |Jake Buehler |April 26, 2021 |Science News 

“I can easily see something like that being the precursor of the protrusible gland,” says Rodrigues. This praying mantis inflates a strange pheromone gland to lure mates |Jake Buehler |April 26, 2021 |Science News 

Others who, because of a tumor in the pituitary gland, may overproduce HGH develop a different problem: gigantism. Is HGH, Allegedly Alex Rodriguez’s Drug of Choice, Really So Bad? |Kent Sepkowitz |August 1, 2013 |DAILY BEAST 

Carrey sings: "You're a big, big man with a little bitty gland, so you need something bigger with a hairpin trigger." "Hee-Haw": Jim Carrey Mocks Gun Lovers & Charlton Heston In Spoof |Anna Klassen |March 25, 2013 |DAILY BEAST 

HGH is a hormone normally made in tiny amounts by the pituitary gland in all of us. Where Were the Doctors to Testify at Roger Clemens’s Perjury Trial? |Kent Sepkowitz |June 20, 2012 |DAILY BEAST 

To decimate the malformation, destroying the offending oil gland once it morphs into something with a head. My Odyssey Into Extreme Dermatology |Elizabeth Hayt |April 15, 2009 |DAILY BEAST 

The thymus gland attains a considerable development in the embryo and shrinks away to the merest vestige in the adult. Man And His Ancestor |Charles Morris 

The strong probability is that this gland belongs in the same category with other embryonic survivals yet to be pointed out. Man And His Ancestor |Charles Morris 

It is nearly always accompanied by a distinct hypertrophy of the thymus gland. Essays In Pastoral Medicine |Austin Malley 

I want a ball bat to club every country jake doctor that looks me over and asks about my pituitary gland. David Lannarck, Midget |George S. Harney 

The mucus secreted from gland cells in this lining makes a slippery surface so that the food may slip down easily. A Civic Biology |George William Hunter