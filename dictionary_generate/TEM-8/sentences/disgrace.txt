The “honey trap” would then be snapped shut with the offer to become a spy or face disgrace and ruin. Revisiting “The Year of the Spy” |by Stephen Engelberg |July 28, 2021 |ProPublica 

The first happened in October 2017, when Harvey Weinstein, head of the Weinstein Company, was revealed to be a serial sexual predator and forced to retire in disgrace. How In the Heights went from a student musical to one of the summer’s biggest movies |Constance Grady |June 11, 2021 |Vox 

Regardless, by the end of The Hundred and One Dalmatians, the Dearlys and their dogs do defeat Cruella, leading her to flee England in disgrace. The unstoppable, villainous glamour of Cruella de Vil |Constance Grady |May 31, 2021 |Vox 

This re-institutionalization of the old and mentally ill was a disgrace that proved deadly during the pandemic. History's Lesson for Activists Who Want to Defund the Police |Sarah E. Ryan |April 20, 2021 |Time 

It is adoration and judgment, celebrity and imminent disgrace, the highest honor and profound loss of face, pressed close against each other. Hideki Matsuyama’s draining, pressure-packed win makes this Masters memorable |Thomas M. Boswell |April 12, 2021 |Washington Post 

Years later, my brother still believes that being a girl is a disgrace, just like most of the local boys think nowadays. What It Feels Like For a Girl in Iraq |Noor |November 6, 2014 |DAILY BEAST 

I was made to believe that being a girl was such a disgrace and I was something really awful. What It Feels Like For a Girl in Iraq |Noor |November 6, 2014 |DAILY BEAST 

This is a national disgrace, and if we don't do something about it, we will all pay a terrible, terrible price. Is America a Police State? For Many, Yes |Gene Robinson |September 1, 2014 |DAILY BEAST 

The fight seemed to break up after the failed punch, and Bieber had to leave the restaurant in disgrace. An Unlikely Hero Blooms in Ibiza: Orlando Bloom Sort of Punches Justin Bieber |Amy Zimmerman |July 30, 2014 |DAILY BEAST 

Hillary Clinton would have been, too, or forced to resign in disgrace. Beirut Barracks vs. Benghazi |Michael Tomasky |May 9, 2014 |DAILY BEAST 

The Marshals were inclined to attribute their disgrace to the ill-will of Berthier and not to the temper of Napoleon. Napoleon's Marshals |R. P. Dunn-Pattison 

I nursed him through several attacks of delirium tremens, and was always in fear that he would get out and disgrace us. Ancestors |Gertrude Atherton 

Hitherto we have honoured his drafts, and kept your name and his free from disgrace. Blackwood's Edinburgh Magazine, No. CCCXXXIX. January, 1844. Vol. LV. |Various 

"You spoke of disgrace," she observed gently, swaying her fan before her by its silken cord. Elster's Folly |Mrs. Henry Wood 

But glorious as was his success, his impetuosity soon brought him into further disgrace. Napoleon's Marshals |R. P. Dunn-Pattison