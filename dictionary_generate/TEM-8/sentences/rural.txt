The DFC confirmed a $5 million equity investment in Copia Global, a Kenya-based e-commerce and logistics startup that focuses on under-served consumers in rural areas. The Trump administration is investing in African e-commerce startups focused on rural areas |Yomi Kazeem |September 10, 2020 |Quartz 

The true total is likely higher, as the country’s testing capacities are limited, especially in rural areas. Scientists are trying to explain why so few Kenyans are dying of coronavirus |Olivia Goldhill |September 9, 2020 |Quartz 

Yadav moved from his rural home in the eastern state of Bihar a decade ago in search of a job that would allow him to send his two kids to school. India’s megacities aren’t prepared for a wave of climate migrants |Manavi Kapur |September 3, 2020 |Quartz 

And, two rural customers – Fallbrook and Rainbow – are in the process of leaving the Water Authority because they’re fed up with its rising rates. The Water Authority Is Resurrecting Its Pipe Dream – Again |MacKenzie Elmer |September 1, 2020 |Voice of San Diego 

Eastern Washington is largely rural and conservative, but state politics are dominated by the urbanized coastal region. Vote by mail doesn’t really change much, election-wise |John Timmer |August 27, 2020 |Ars Technica 

These are young fathers, rural farmers, usually growing banana or coffee or subsistence crops. How Good Dads Can Change the World |Gary Barker, PhD, Michael Kaufman |January 6, 2015 |DAILY BEAST 

For the first time in American history, rural America has been losing population. Will Texas Stay Texan? |David Fontana |December 29, 2014 |DAILY BEAST 

She is also head of the Sabancı Foundation, which conducts female-empowerment programs for women in rural Turkey. The Women Battling an Islamist Strongman |Christina Asquith |December 22, 2014 |DAILY BEAST 

Rural churches were deserted, and the connection between the land and the bounty of harvests was gone. How Dickens and Scrooge Saved Christmas |Clive Irving |December 22, 2014 |DAILY BEAST 

“In almost all rural areas of Switzerland, it is customary to eat cats and dogs,” she says. Will the Swiss Quit Cooking their Kittens and Puppies? |Barbie Latza Nadeau |November 30, 2014 |DAILY BEAST 

There are many more good dwellings on this plain than in the rural portion of Lower Italy. Glances at Europe |Horace Greeley 

The hospitals in the capital were crowded with wounded soldiers, brought in at great risk from the rural districts. The Philippine Islands |John Foreman 

Here again we have the landscape of Lorraine and the eternal and infinitely varied theme of rural labour. Bastien Lepage |Fr. Crastre 

This rule however does not apply to travelers walking along a rural highway. Putnam's Handy Law Book for the Layman |Albert Sidney Bolles 

Then it is sung softly like the farmhand quartettes do in the rural melodrama outside the old homestead in harvest time. The Real Latin Quarter |F. Berkeley Smith