For example, imagine you’ve got six vertices, each connected to every other vertex by edges. Undergraduate Math Student Pushes Frontier of Graph Theory |Kevin Hartnett |November 30, 2020 |Quanta Magazine 

The second is the average number of different cells sharing each vertex. Scientists Uncover the Universal Geometry of Geology |Joshua Sokol |November 19, 2020 |Quanta Magazine 

Then count up and average the number of vertices on all the bits of paper. Scientists Uncover the Universal Geometry of Geology |Joshua Sokol |November 19, 2020 |Quanta Magazine 

Mathematicians are particularly interested in understanding how many vertices and edges they can contain before different kinds of substructures emerge within them. Disorder Persists in Larger Graphs, New Math Proof Finds |Kevin Hartnett |November 4, 2020 |Quanta Magazine 

This means you’re far from being guaranteed a monochromatic clique of 5 vertices, which means the Ramsey number for this example is larger than 10 vertices. Disorder Persists in Larger Graphs, New Math Proof Finds |Kevin Hartnett |November 4, 2020 |Quanta Magazine 

The vertex is orange-coloured, with a black line in the middle. Narrative of a Survey of the Intertropical and Western Coasts of Australia] [Volume 2 of 2] |Phillip Parker King 

Its shape varies from triangular to orbicular, the mouth of the animal forming the vertex of the triangle. An Introduction to Entomology: Vol. III (of 4) |William Kirby 

In D. Aloeus and its affinities, they are arranged in a triangle, whose vertex is towards the head. An Introduction to Entomology: Vol. III (of 4) |William Kirby 

Those parts which lie on the outside of the posterior half of the eyes, between which the Frons and Vertex intervene. An Introduction to Entomology: Vol. III (of 4) |William Kirby 

The point midway between the vertex V and center of curvature C is called the principal focus, F. Physics |Willis Eugene Tower