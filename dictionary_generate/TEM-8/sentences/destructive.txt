Most such flares develop when two neutron stars collide or there is some other destructive cosmic event. Exploding neutron star proves to be energy standout of the cosmos |Lisa Grossman |February 12, 2021 |Science News For Students 

I was so angry at the time, and I knew I needed to channel these self-destructive qualities and turn that negative into a positive. Republican Rep. Nancy Mace on the Capitol insurrection: ‘We need to rebuild our party’ |KK Ottesen |February 9, 2021 |Washington Post 

However, as you mentioned, there are already four deaths in Washington of which the situation is less violent and destructive than that in Hong Kong. Pro-China propaganda campaign exploits U.S. divisions in videos emphasizing Capitol attack |Craig Timberg, Eva Dou |February 4, 2021 |Washington Post 

So it isn’t surprising that too many are now grappling with serious depression, becoming self-destructive, or losing motivation for school. Dear Struggling Parents, It's Not Just You. This Is Hard |Susanna Schrobsdorff |January 31, 2021 |Time 

We are learning the hard way about their destructive effects on society and democracy. 'We Need a Fundamental Reset.' Shoshana Zuboff on Building an Internet That Lets Democracy Flourish |Billy Perrigo |January 22, 2021 |Time 

Anger often manifests in withholders as another self-destructive but more socially acceptable feeling or behavior, like anxiety. Can Self-Help Books Really Make a New You? |Lizzie Crocker |December 29, 2014 |DAILY BEAST 

It has a presence, it remains potentially destructive, but all we can do is attempt to marshal it. Grief: The Real Monster in The Babadook |Tim Teeman |December 19, 2014 |DAILY BEAST 

To break her self-destructive cycle and heal, she decides to hike 1,100 miles of the Pacific Crest Trail solo. Exclusive: The Making of Reese Witherspoon’s Golden Globe-Nominated ‘Wild’ |Marlow Stern |December 12, 2014 |DAILY BEAST 

When we assign a primitive “not me” status to another individual or social group, it can—and does—take us down a destructive path. Ferguson, Immigration, and ‘Us Vs. Them’ |Gene Robinson |November 27, 2014 |DAILY BEAST 

This structure is particularly destructive for children in low-income families. How a GOP Senate Can Help the Poor |Veronique de Rugy |November 23, 2014 |DAILY BEAST 

A tremendous tornado passed over the city of Natchez, very destructive to life and property. The Every Day Book of History and Chronology |Joel Munsell 

A destructive tornado swept over a portion of Lapeer county, Michigan. The Every Day Book of History and Chronology |Joel Munsell 

The planter passes entire nights, provided with lights, clearing the buds just opening, of these destructive insects. Tobacco; Its History, Varieties, Culture, Manufacture and Commerce |E. R. Billings. 

There is another effect from hurricanes which is even more destructive to life than that caused by the direct action of the wind. Outlines of the Earth's History |Nathaniel Southgate Shaler 

By that time, however, the Scots had completed another destructive raid. King Robert the Bruce |A. F. Murison