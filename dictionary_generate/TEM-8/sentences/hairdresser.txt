He hung out with Ashton Kutcher, dabbled in Kabbalah and often traveled in a signature Range Rover with an entourage that included his personal hairdresser and surf coach. The details of WeWork’s unraveling are stranger than fiction |Allison Stewart |July 22, 2021 |Washington Post 

Aspiring as a girl to be a hairdresser, she had enrolled in La Scala’s dance school at age 9 or 10 only on the insistence of family friends who observed in her a natural elegance. Carla Fracci, a reigning star of 20th-century ballet, dies at 84 |Emily Langer |May 30, 2021 |Washington Post 

I started to write things, such as "Mary Jones, address, phone number and my hairdresser." Hints From Heloise: Dating-site match was charming at first |Heloise Heloise |April 23, 2021 |Washington Post 

It was my mother’s hairdresser who started me out with a few reddish-brown highlights. Some young women embraced their gray hair during the pandemic. They might not go back. |Maura Judkis |April 12, 2021 |Washington Post 

Neewer’s ring light is a favorite of makeup artists and hairdressers, especially those who use Instagram live, where photoshop and other editing tricks aren’t possible. Ring lights that will make your selfies pop |PopSci Commerce Team |January 6, 2021 |Popular-Science 

"This is a girl who's mom was a hairdresser," the friend said. Beyoncé Debuts A New Blonde Bob |Allison Samuels |August 16, 2013 |DAILY BEAST 

People she never met, including a hairdresser who claimed her as a customer, were quoted as friends of hers, Kelley says. Jill Kelley Says Paula Broadwell Tried to ‘Blackmail’ Her |Howard Kurtz |January 22, 2013 |DAILY BEAST 

Her hairdresser in Malibu filled her in, and after studying every episode she was “hooked, line and sinker,” and signed on. ‘A Certain Age’—Shirley MacLaine Rattles Downton Abbey |Sandra McElwaine |December 27, 2012 |DAILY BEAST 

However, to find the proper doctor, dentist and hairdresser took some time. Isabel Allende’s Favorite Bookstore: Book Passage, Corte Madera, Calif. |Isabel Allende |November 8, 2012 |DAILY BEAST 

I believe that one should have a personal doctor, a dentist, a hairdresser, and, of course, a trusted bookstore. Isabel Allende’s Favorite Bookstore: Book Passage, Corte Madera, Calif. |Isabel Allende |November 8, 2012 |DAILY BEAST 

That should at least have saved you a bill with your hairdresser. The International Monthly Magazine, Volume 5, No. 1, January, 1852 |Various 

James Daniel, wig-maker and hairdresser also operates on the teeth, a business so necessary in this city. The Old Furniture Book |N. Hudson Moore 

She then told Nancy that she worked in a hairdresser's shop down Broadway, "mostly fixing nails." The Devourers |Annie Vivanti Chartres 

They went to a hairdresser, who cut their very thick hair and tied it with broad black ribbon. Betty Vivian |L. T. Meade 

For some time after the statue had ceased to give signs of life, the hairdresser remained gaping, incapable of thought or action. The Tinted Venus |F. Anstey