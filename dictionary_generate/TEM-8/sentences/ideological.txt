We also gave a bump for female candidates, who powered Democrats’ 2018 success, and de-emphasized a score praising ideological moderation, which OZY considered less important in a base-driven election. Our Forecast: Biden Has a Commanding (But Not Certain) Lead |Daniel Malloy |September 10, 2020 |Ozy 

Enter an aesthetic and ideological movement known as solarpunk. Solarpunk Is Growing a Gorgeous New World in the Cracks of the Old One |Carin Ism |September 6, 2020 |Singularity Hub 

The number who lost for ideological reasons is five at most, and three were due to scandals that are probably best thought of as one-off occurrences. Ed Markey Won, But It’s Still Been A Rough Year For Incumbents |Nathaniel Rakich (nathaniel.rakich@fivethirtyeight.com) |September 2, 2020 |FiveThirtyEight 

So without a clear ideological contrast, Kennedy has arguably struggled to articulate why he feels that Markey doesn’t deserve another term. Today’s Elections In Massachusetts Are Another Big Test For The Progressive Movement |Nathaniel Rakich (nathaniel.rakich@fivethirtyeight.com) |September 1, 2020 |FiveThirtyEight 

He has an ideological belief that the intellectual property system is a wonderful mechanism that is necessary for innovation and prosperity. Oxford’s COVID vaccine deal with AstraZeneca raises concerns about access and pricing |lbelanger225 |August 24, 2020 |Fortune 

The video showing cops piling on Eric Garner in Staten Island for refusing arrest elicited outrage across ideological lines. The GOP and Police Unions: A Love Story |Eleanor Clift |December 12, 2014 |DAILY BEAST 

Mired in ideological warfare, America faces her most formidable opponent yet— herself. The Post-Brown and Garner Question: Who ‘Deserves’ to Die? |Goldie Taylor |December 9, 2014 |DAILY BEAST 

The main point is this: Trying to win Southern seats is not worth the ideological cost for Democrats. Dems, It’s Time to Dump Dixie |Michael Tomasky |December 8, 2014 |DAILY BEAST 

We cannot qualify it based on ideological notions or concepts important only at one time in history. Is Pope Francis Backpedaling on Gays? |Jay Michaelson |November 19, 2014 |DAILY BEAST 

He is the ideological leader of the anti-American Rodina party. Think Putin’s Bad? Wait for the Next Guy |Anna Nemtsova |November 14, 2014 |DAILY BEAST 

As a rule, the socialist stands almost alone in combating this ideological interpretation of history and of social evolution. Violence and the Labor Movement |Robert Hunter 

At any rate, almost universally, the pattern is of a person who is not acting out of any ideological grounds. Warren Commission (5 of 26): Hearings Vol. V (of 15) |The President's Commission on the Assassination of President Kennedy 

But you also got the idea that he enjoyed this paper for its ideological content? Warren Commission (8 of 26): Hearings Vol. VIII (of 15) |The President's Commission on the Assassination of President Kennedy 

It is self-evident that one cannot build materialistic doctrines on foundations so ideological. Landmarks of Scientific Socialism |Friedrich Engels 

You know how these ideological cliques form in a government—or any other organization. Oomphel in the Sky |Henry Beam Piper