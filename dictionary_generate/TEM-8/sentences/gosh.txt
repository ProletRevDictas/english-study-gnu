I thought: gosh, he really has grown, although he still seems young. Justin Bieber's Abs Cannot Save Him |Tim Teeman |September 10, 2014 |DAILY BEAST 

Gosh, if I could bring back anyone else—it would take all day! How I Write: Jane Goodall |Noah Charney |April 9, 2014 |DAILY BEAST 

Oh gosh, just when I think I find a favorite I get to the script and there's something else. 'Girls' Costume Designer Jenn Rogien Talks Season 3 Style |Erin Cunningham |January 12, 2014 |DAILY BEAST 

“Oh my gosh, you have no idea, it was terrifying,” she says. ‘Nuke Mom’ Marisa Sketo Kirsh on Her Vindication |Michael Daly |December 5, 2013 |DAILY BEAST 

Oh gosh, it was so important to me, because I honestly mean no disrespect to any church. Diablo Cody’s Personal (and Directorial) Paradise |Kevin Fallon |October 18, 2013 |DAILY BEAST 

"I'll be gosh-darned ef that ain't the damnedest," said he, slowly. Earth's Enigmas |Charles G. D. Roberts 

"Gosh, you sure made a footprint there," he said wonderingly. The Stutterer |R.R. Merliss 

I aint got no scores to settle with you, but I have with that little demon, an by gosh shell know it, when Ive done with her! Three Little Women |Gabrielle E. Jackson 

"Ef they carry her off again it will be over my dead body, b'gosh," he murmured more than once. The Boy Land Boomer |Ralph Bonehill 

The Gosh Utes take rabbits in nets made of flax-twine, about three feet wide and of considerable length. The Works of Hubert Howe Bancroft, Volume 1 |Hubert Howe Bancroft