County Supervisor Nathan Fletcher said Thursday the county expects to begin vaccinating teachers, police and other essential workers in the next few weeks following calls to prioritize educators and law enforcement. Morning Report: Not Much COVID Enforcement Happening Despite Crackdown |Voice of San Diego |February 12, 2021 |Voice of San Diego 

Chicago, meanwhile, recently announced a program aimed at vaccinating people in high-need neighborhoods. Chicago thinks Zocdoc can help solve its vaccine chaos |Lindsay Muscato |February 12, 2021 |MIT Technology Review 

Yet they also appear to be getting vaccinated at very low rates. Among Latino immigrants, false vaccine claims are spreading as fast as the virus |Teo Armus |February 11, 2021 |Washington Post 

People are also being vaccinated where they worship in North Carolina, Connecticut and Michigan. Churches pair up with clinics to deliver coronavirus vaccine to those who need it most |Jenna Portnoy |February 10, 2021 |Washington Post 

As more vaccine is developed, other groups will be vaccinated. Hints From Heloise: CDC suggests coronavirus vaccinations first for select groups |Heloise Heloise |February 9, 2021 |Washington Post 

From a pediatric ICU in Melbourne, Australia, to an elevator in Brooklyn, we see just how harmful refusing to vaccinate can be. Hey Anti-Vaxxers, Watch NOVA: Vaccines--Calling the Shots |Russell Saunders |September 11, 2014 |DAILY BEAST 

One look at those numbers is all it takes to realize how absurd the decision not to vaccinate is. The $1 Billion Reason to Vaccinate |Russell Saunders |June 12, 2014 |DAILY BEAST 

The parents who refused to vaccinate their kids are the reason behind the measles resurrecting themselves in New York. ICYMI: Best of The Beast This Week |The Daily Beast |March 16, 2014 |DAILY BEAST 

Refusing to vaccinate your children means you are contributing to a worsening public health crisis. Thanks, Anti-Vaxxers. You Just Brought Back Measles in NYC. |Russell Saunders |March 13, 2014 |DAILY BEAST 

And then this: I always ask if the children are vaccinated, or if the parents intend to vaccinate once the child is born. Pediatrician: Vaccinate Your Kids—Or Get Out of My Office |Russell Saunders |January 30, 2014 |DAILY BEAST 

If small-pox be prevailing, it is proper to vaccinate all who have not been vaccinated within three or four years. The Physical Life of Woman: |Dr. George H Napheys 

There was a panic at that time about small-pox, and the doctor came one day to vaccinate everybody in the house. Aunt Madge's Story |Sophie May 

He apparently had been lying in wait for us, and he begged me to come to his house and vaccinate his infant son. At the Court of the Amr |John Alfred Gray 

Lymph was taken with them so that his wife could vaccinate him if it should become necessary. The Life of Mrs. Robert Louis Stevenson |Nellie van De Grift Sanchez 

Purple had engaged to vaccinate a child on a certain day, but for some reason the vaccination was not done. A System of Practical Medicine by American Authors, Vol. I |Various