Today the danger of being terrorized by technocracy threatens every country in the world. We're Dangerously Close to Giving Big Tech Control Of Our Thoughts |Susie Alegre |June 29, 2022 |Time 

The benefit of technocracy is that it avoids the petty mercenary self-interest of industry players. The Technocratic Dilemma |Megan McArdle |February 4, 2013 |DAILY BEAST 

Their shared faith in technocracy is not the only reason for that. Ask the Blogger: How About That Election? |Megan McArdle |November 5, 2012 |DAILY BEAST 

Last moment arrival of material from various authors thrust the Technocracy article out of this issue. Futuria Fantasia, Fall 1939 |Ray Bradbury 

We will, tho, in the Winter Edition, give you a few facts and predictions made by Technocracy. Futuria Fantasia, Fall 1939 |Ray Bradbury 

Under Technocracy people will be classified in a set of probably 100 industrial sequences, according to their work. Futuria Fantasia, Summer 1939 |Ray Bradbury 

When you write an introductory article to a generally new audience on Technocracy, you have to start from the ground up. Futuria Fantasia, Summer 1939 |Ray Bradbury 

Technocracy is not an organization that wants to overthrow the American government, but only an org. Futuria Fantasia, Summer 1939 |Ray Bradbury