Sheskey shot Blake while holding onto his shirt after officers unsuccessfully used a Taser on him, the Wisconsin Justice Department said. Prosecutors charge teen in fatal shooting of two protestors and wounding of a third in Kenosha |kdunn6 |August 28, 2020 |Fortune 

That’s made her a favorite among LGBTQ people, many of whom still wear “For the People” shirts from her presidential campaign. Harris as VP pick brings diversity, LGBTQ ally to Biden ticket |Chris Johnson |August 11, 2020 |Washington Blade 

The self-described “personal air sanitizer” sprays chlorine dioxide gas from a badge that customers are encouraged to wear on their backpacks or shirts. Environment Report: One Way to Force Companies to Emit Less Carbon |MacKenzie Elmer |August 10, 2020 |Voice of San Diego 

That may be because there hadn’t been enough sweat on the runners’ shirts to fully wet the supercapacitor. Working up a sweat may one day power up a device |Carolyn Wilke |June 29, 2020 |Science News For Students 

Some of the researchers strapped a capacitor onto their shirts and ran. Working up a sweat may one day power up a device |Carolyn Wilke |June 29, 2020 |Science News For Students 

Prices are relatively inexpensive and come in at around 135 euros for a shirt or 35 euros for hand woven boxers. The Photographer Who Gave Up Manhattan for Marrakech |Liza Foreman |January 6, 2015 |DAILY BEAST 

The kids had a gift for him too, a tee shirt with ‘Baseball Spoken Here’ stenciled across the front. Mario Cuomo, Always Moving Us Toward the Light |Mike Barnicle |January 4, 2015 |DAILY BEAST 

Marvin takes off his T-shirt and dives into his swimming pool. The Story Behind Lee Marvin’s Liberty Valance Smile |Robert Ward |January 3, 2015 |DAILY BEAST 

There was a handy distraction in the Che t-shirt the tourist was wearing while celebrating the death. The Life and Hard Times Of The Family A Cuban Defector Left Behind |Brin-Jonathan Butler |December 19, 2014 |DAILY BEAST 

He was standing on the corner and wearing only a T-shirt and jeans, and this was 11:30 at night and it was really cold. Hunt for Iraq Vet After Killing Spree |M.L. Nestel |December 16, 2014 |DAILY BEAST 

If you use it wisely, it may be Ulysses' hauberk; if you reject it, the shirt of Nessus were a cooler winding-sheet! The Pastor's Fire-side Vol. 3 of 4 |Jane Porter 

His hat was pushed back from his forehead, the collar of his blue flannel shirt was open. The Bondboy |George W. (George Washington) Ogden 

He reached down inside my shirt, with a none too gentle hand, and relieved me of the belt that held the money. Raw Gold |Bertrand W. Sinclair 

A pair of thin trousers and a shirt hanging down outside instead of being tucked in at the waist, and his toilet is made. Alila, Our Little Philippine Cousin |Mary Hazelton Wade 

To me the national affectation of piety and holiness resembles a white shirt put on over a dirty skin. God and my Neighbour |Robert Blatchford