At its very basic, it’s pageantry, masquerade, and glitz, and awards are given in various categories that exhibit “realness.” Celebrating and understanding Ballroom culture |Terri Schlichenmeyer |March 31, 2022 |Washington Blade 

“If near-misses masquerade as successes, then organizations and their members will only learn to continue taking the risks that produced the near-miss outcome until a tragedy occurs,” the researchers wrote. The lab leak hypothesis — true or not — should teach us a lesson |Umair Irfan |August 27, 2021 |Vox 

They refused to question Modi, and allowed him to use a national disaster to bolster his image, consolidate power, stifle dissent and masquerade grandstanding as governance. It Isn't Just Modi. India’s Compliant Media Must Also Take Responsibility for the COVID-19 Crisis |Debasish Roy Chowdhury |May 3, 2021 |Time 

It builds to a masquerade ball, in which Not Harry and the women don masks and commence flirting. You Really Don't Want to Watch Fox’s ‘I Wanna Marry “Harry”’ |Jason Lynch |May 20, 2014 |DAILY BEAST 

They masquerade as intellectual contests, but are really just showcases for rhetorical cleverness and public charisma. My Debate With an ‘Intelligent Design’ Theorist |Karl W. Giberson |April 21, 2014 |DAILY BEAST 

He was supposed to come back to “masquerade the relationship,” but no such luck. Vegan Strippers Let It All Hang Out |Kelly Williams Brown |March 29, 2014 |DAILY BEAST 

Hough would have blended in seamlessly at the “Disco Africa”-themed Halloweek masquerade party held in Milan that same night. Why It’s Time to End Blackface, Finally |Soraya Roberts |October 31, 2013 |DAILY BEAST 

He escorts her to masquerade parties, takes her sailing on his yacht, force-feeds her oysters. Speed Read: 12 Naughty Bits From ‘50 Shades Darker’ |Lizzie Crocker |May 4, 2012 |DAILY BEAST 

"I'm in a big hurry to get to a masquerade," Black Hood said as he opened the door of the taxi. Hooded Detective, Volume III No. 2, January, 1942 |Various 

It is you that have injured her by allowing her to masquerade as a man—a little thing like that, with nobody to advise her. A Little Union Scout |Joel Chandler Harris 

All the shabbiest tinsel and trappings of secular music passed across the trestles of this religious masquerade. Autobiographical Reminiscences with Family Letters and Notes on Music |Charles Gounod 

Between times I'm making a dress and cap for the masquerade dance. Patchwork |Anna Balmer Myers 

It was a game, but he rejoiced in it as a girl does in her first masquerade. Riders of the Silences |John Frederick