Segregating dispensaries deprives small businesses of a ready income stream from the foot traffic dispensaries already generate. Myths and Shame Shouldn’t Guide Cannabis Regulations |John Bertsch |September 8, 2020 |Voice of San Diego 

The state and county boards contended that Hutchins had not yet been deprived of the right to vote. Hundreds of Thousands of Nursing Home Residents May Not Be Able to Vote in November Because of the Pandemic |by Ryan McCarthy and Jack Gillum |August 26, 2020 |ProPublica 

We know that if children miss out on their education, particularly those in more deprived areas, that will have a lasting negative impact on their health and their life chances. Boris Johnson is urging parents to send their children back to school this fall |kdunn6 |August 24, 2020 |Fortune 

Responding to my query, he wrote that “facial recognition should not be used to deprive people of liberty.” The Bias in the Machine - Issue 89: The Dark Side |Sidney Perkowitz |August 19, 2020 |Nautilus 

Here was Genius, which had invested considerable time and money in developing its lyrics database, allegedly being deprived of traffic and revenue — an argument that many aggrieved publishers identify with. Genius not looking so smart after Google escapes liability for ‘misappropriating’ lyrics |Greg Sterling |August 11, 2020 |Search Engine Land 

“Scorched earth,” historically, means destroying land to deprive the encroaching enemy of its use. Israel Creates ‘No Man’s Land’ in Gaza, Shrinking Strip by 40 Percent |Jesse Rosenfeld |July 28, 2014 |DAILY BEAST 

It would also deprive a lot of kids like Henry of the opportunity to present their asylum claims. The Immigrant Kids Suing America |Caitlin Dickson |July 11, 2014 |DAILY BEAST 

But in reality all diets are basically the same—deprive the body of nutrients and it will begin to consume itself. My Week At An Austrian Fat Camp |Owen Matthews |October 27, 2013 |DAILY BEAST 

But in principle you could deprive humans of all kind of things to see what happened. Rediscovering Richard Dawkins: An Interview |J.P. O’Malley |September 23, 2013 |DAILY BEAST 

Is the market an inert force to be manipulated and exploited, to deprive it of hard-earned cash? 5 Ways to Fix Book Publishing |Anis Shivani |July 12, 2013 |DAILY BEAST 

Robert, emperor of Germany, died, just as a powerful combination had been formed to deprive him of the crown. The Every Day Book of History and Chronology |Joel Munsell 

Well, then, by meriting happiness, I will take revenge upon the injustice which would deprive me of it. Madame Roland, Makers of History |John S. C. Abbott 

In this case, the idea of a greater good determines him to deprive himself of one less desirable. Superstition In All Ages (1732) |Jean Meslier 

He will naturally resent it; it would deprive us of his friendship or regard: nay it would, perhaps, make him hate us. My Ten Years' Imprisonment |Silvio Pellico 

Does not tyranny deprive princes of true power, the love of the people, in which is safety? Superstition In All Ages (1732) |Jean Meslier