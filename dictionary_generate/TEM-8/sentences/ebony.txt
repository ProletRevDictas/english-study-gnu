The pieces, purchased by my grandparents on their honeymoon to Mexico in 1949, were slender, made of ebony and ivory. We Taught Computers To Play Chess — And Then They Left Us Behind |Oliver Roeder |January 25, 2022 |FiveThirtyEight 

I returned to the front desk with my selection, and a few minutes later, a handler appeared with Luna, a 6-month-old with an ebony coat, almond eyes and a toddler’s irrepressible energy. In Tennessee, the Old Friends Senior Dog Sanctuary welcomes two-legged visitors |Andrea Sachs |October 29, 2021 |Washington Post 

Sinking your spoon into the plastic cup delivers ebony and ivory in perfect harmony. Taqueria Xochi serves mouthwatering Mexican food from a tiny U Street storefront |Tom Sietsema |December 18, 2020 |Washington Post 

Soon thereafter she gave birth to a little daughter who was as white as snow, as red as blood, and her hair as black as ebony. In New Brothers Grimm 'Snow White', The Prince Doesn't Save Her |The Brothers Grimm |November 30, 2014 |DAILY BEAST 

“I remember saying, ‘Oh my God, Mom, you have to move,’” Ebony remembers. Killed by Donald Sterling’s Racism |Michael Daly |May 14, 2014 |DAILY BEAST 

Afterward, Ebony went to the apartment where her mother had made such a valiant stand. Killed by Donald Sterling’s Racism |Michael Daly |May 14, 2014 |DAILY BEAST 

Ebony had been 12 when her mother was suddenly deprived of her livelihood. Killed by Donald Sterling’s Racism |Michael Daly |May 14, 2014 |DAILY BEAST 

That did not preclude Ebony Jones from speaking about her mother. Killed by Donald Sterling’s Racism |Michael Daly |May 14, 2014 |DAILY BEAST 

Stradiuarius made a few instruments inlaid with ebony and ivory round the edges. Violins and Violin Makers |Joseph Pearce 

Pallid, Sir Lucien Pyne lay by the ebony chair glaring horribly upward. Dope |Sax Rohmer 

It had spread, an ebony patch, equally about the bole of the tree, so that the sun must have been immediately overhead. Dope |Sax Rohmer 

Before going down he locked them up in a small ebony cabinet which stood against the wall. Elster's Folly |Mrs. Henry Wood 

On coming out his skin would be shining like ebony, and he would squeal with pleasure as I rubbed water down his back. Kari the Elephant |Dhan Gopal Mukerji