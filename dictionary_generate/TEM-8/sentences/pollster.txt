By last month, that percentage had been cut nearly in half, to only 18 percent — the lowest of the seven metrics the pollsters measured. A brutal, isolating year leads to baffling battles between good and evil |Philip Bump |February 4, 2021 |Washington Post 

The Preference Survey mirrors a typical political poll, with Miller as the pollster. Georgia runoffs: A data scientist, using a blend of poll and betting numbers, sees odds favoring the Dems |Shawn Tully |January 5, 2021 |Fortune 

They also assess why many pollsters are sitting out the Georgia Senate runoffs and take some time to answer listener questions. Why Many Pollsters Are Sitting Out The Georgia Runoffs |Galen Druke |December 29, 2020 |FiveThirtyEight 

Other pollsters haven't seen that, though all of them find a majority of Republicans agreeing with the president. The Trailer: Why the GOP is talking about China, from Georgia to the courtroom |David Weigel |December 10, 2020 |Washington Post 

Murray, the founding director of Monmouth University’s Polling Institute, ran into many of the same challenges in the Midwest and Florida that other pollsters did. Politics Podcast: How To Make Polls Better |Galen Druke |December 4, 2020 |FiveThirtyEight 

“It takes a lot of Democrats to elect a Republican in one of these places,” said John McLaughlin, a Republican pollster. Return of the Northeastern Republican |David Freedlander |November 4, 2014 |DAILY BEAST 

“What you worry about in a race like this is voters not voting,” says Republican pollster Neil Newhouse. With Ernst and Gardner, Republicans Think They’ve Found the Formula |Eleanor Clift |November 4, 2014 |DAILY BEAST 

“I would advise any candidate to assess their viability and not just do a token run,” said Celinda Lake, a Democratic pollster. Does Team Hillary Want a Democratic Challenge? |David Freedlander |September 25, 2014 |DAILY BEAST 

“Voters in Kansas are searching for an alternative to both parties,” says Orman pollster David Beattie. The Kansas Independent Who Could Control the Senate |John Avlon |September 6, 2014 |DAILY BEAST 

As pollster John Zogby has written, the president has already alienated many young voters for a number of reasons. Hillary's Got a Millennial Problem |Nick Gillespie |August 28, 2014 |DAILY BEAST