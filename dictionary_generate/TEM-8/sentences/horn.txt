Another car behind it in line was behaving “obnoxiously,” honking its horn and blasting music, he said. Those Ticketed for Seditious Language Say Their Only Crime Was Talking Back |Kate Nucci |September 9, 2020 |Voice of San Diego 

Carolina Garcia, 35, director of original series at NetflixMy parents always say, “Toma el toro por las astas,” which means, “Grab the bull by the horns.” The advice that helped this year’s 40 under 40 find their own path |kdunn6 |September 3, 2020 |Fortune 

He took pictures of the lizard and measured the size and shape of its body parts, such as the length of its nose-horn and head. A nose-horned dragon lizard lost to science for over 100 years has been found |Dyna Rochmyaningsih |June 9, 2020 |Science News 

To transform a horse into a unicorn, you could try adding a horn from a related animal, Paul Knoepfler says. What would it take to make a unicorn? |Carolyn Wilke |March 4, 2020 |Science News For Students 

So in a horse, “you might be able to … add a few different genes that would result in a horn sprouting on their head,” he says. What would it take to make a unicorn? |Carolyn Wilke |March 4, 2020 |Science News For Students 

So I asked the driver to honk the horn, which he does, and Rod looks over. The Story Behind Lee Marvin’s Liberty Valance Smile |Robert Ward |January 3, 2015 |DAILY BEAST 

Reportedly, George Custer wore a Stetson into Little Big Horn. My Love Letter to the Stetson |Mark McKinnon |December 24, 2014 |DAILY BEAST 

A car parked at a red light honked its horn in rhythm with the chant as the crowd passed in front of it. ‘They Let Him Off?’ Scenes from NYC in Disbelief |Jacob Siegel |December 4, 2014 |DAILY BEAST 

He told the court he called the retired captain to see exactly where he lived so he knew when to sound the cruise ship horn. The Costa Concordia’s Randy Reckless Captain Takes the Stand |Barbie Latza Nadeau |December 2, 2014 |DAILY BEAST 

Available at Jonathan Adler Whisky Tumbler Set—Light Horn, $65  This is drinking at its most fashionable. The Daily Beast’s 2014 Holiday Gift Guide: For the Angelina Jolie in Your Life |Allison McNearney |November 29, 2014 |DAILY BEAST 

Before the outlaw can comply with this small request the horn sounds again. Physiology of The Opera |John H. Swaby (AKA "Scrici") 

Also our six-shooters reposed in their scabbards, the four belts hooked over the horn of MacRae's saddle. Raw Gold |Bertrand W. Sinclair 

There's one called ze, on top of a hill shaped almost like a horn; she showed me a picture of it. Rosemary in Search of a Father |C. N. Williamson 

The second barrel was discharged with no better result, except that a splinter of its horn was knocked off. Hunting the Lions |R.M. Ballantyne 

A huge string game-bag was slung over his back, and in an antelope's horn or a crane's bill bullets were carried. Our Little Korean Cousin |H. Lee M. Pike