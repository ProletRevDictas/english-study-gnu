Geologically speaking, we are still standing at the base of this horizon—different paths stretching out in front of us. Dawn of the Heliocene - Issue 90: Something Green |Summer Praetorius |September 16, 2020 |Nautilus 

Compressing consumers’ path-to-purchase is the holy grail of advertising and marketing. The race to frictionless consumer journeys is expanding beyond marketplaces |acuityads |September 10, 2020 |Digiday 

Hong and his colleagues were trying to figure out whether all urban areas follow the same path as they grow, a concept they borrowed from biology. What it takes for a city to jump into the knowledge economy |Karen Ho |September 9, 2020 |Quartz 

The riding is similar to Moab, but with more variety, from slickrock paths to high-alpine singletrack. 27 Epic Trips to Start Planning Now |The Editors |September 8, 2020 |Outside Online 

That would really put us as a country, across the board, on a better path. A third of Americans might refuse a Covid-19 vaccine. How screwed are we? |Brian Resnick |September 4, 2020 |Vox 

We see detoxing as a path to transcendence, a symbol of modern urban virtue and self-transformation through abstinence. How Taryn Toomey’s ‘The Class’ Became New York’s Latest Fitness Craze |Lizzie Crocker |January 9, 2015 |DAILY BEAST 

The anti-crime cops began searching the likely path of flight. Shot Down During the NYPD Slowdown |Michael Daly |January 7, 2015 |DAILY BEAST 

Senhor José remains stationary, but this lengthy series of clauses propels the reader along an unmarked path. The Lost Novel of Nobel-Winner José Saramago |Charles Shafaieh |January 5, 2015 |DAILY BEAST 

The flight path remained close to the Indonesian archipelago, well within what is the normal reach of air traffic control radar. Did Bad Weather Bring Down AirAsia 8501? |Clive Irving |December 29, 2014 |DAILY BEAST 

My trip takes the reverse path, and I begin by assessing the depth of my Shakespeare knowledge in his birthplace. Biking With the Bard |Kara Cutruzzula |December 28, 2014 |DAILY BEAST 

"But I can't stop to argue about it now;" and, saying this, he turned into a side path, and disappeared in the wood. Davy and The Goblin |Charles E. Carryl 

I presume this path does not extend many miles without meeting impediments. Glances at Europe |Horace Greeley 

She had been walking alone with her arms hanging limp, letting her white skirts trail along the dewy path. The Awakening and Selected Short Stories |Kate Chopin 

Suddenly his quick eye lit on something in the gravel path and his heart gave a great leap. The Joyous Adventures of Aristide Pujol |William J. Locke 

Squinty went this way and that through the woods, but he could not find the path that led to his pen. Squinty the Comical Pig |Richard Barnum