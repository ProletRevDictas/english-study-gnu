The actual wheels then bolt onto these spacers, effectively spreading the tires wider. Six ways to make your ATV even more rugged |By Tyler Freel/Outdoor Life |September 17, 2020 |Popular-Science 

For a typical virus, herd immunity occurs at a 70 to 90 percent spread within a population. The problem with Trump’s ‘herd mentality’ line isn’t the verbal flub. It’s the mass death. |Philip Bump |September 16, 2020 |Washington Post 

Growing evidence indicates that proximity to other people is among the easiest ways for coronavirus to spread, especially indoors. There’s now even more evidence that restaurant dining sharply increases COVID spread |Sy Mukherjee |September 11, 2020 |Fortune 

The spread of the sampling distribution is based on the estimated total survey error of the poll. The Forecast: The Methodology Behind Our 2020 Election Model |Daniel Malloy |September 10, 2020 |Ozy 

Microsoft’s News app has a “book mode” that spreads articles across the two screens like two facing pages in a book. Review of the Microsoft Surface Duo folding phone: Very pretty but just how useful is it? |Aaron Pressman |September 10, 2020 |Fortune 

Before anti-vaxxers, there were anti-fluoriders: a group who spread fear about the anti-tooth decay agent added to drinking water. Anti-Fluoriders Are The OG Anti-Vaxxers |Michael Schulson |July 27, 2016 |DAILY BEAST 

Their immediate response tells an important truth about a police slowdown that has spread throughout New York City in recent days. Shot Down During the NYPD Slowdown |Michael Daly |January 7, 2015 |DAILY BEAST 

Sprawled on chaise lounges with their knees high in the air and their legs spread wide. Powerful Congressman Writes About ‘Fleshy Breasts’ |Asawin Suebsaeng |January 7, 2015 |DAILY BEAST 

Groups like the Crips and MS-13 have spread from coast to coast, and even abroad. The Daily Beast’s Best Longreads, Dec 29-Jan 4, 2014 |William Boot |January 4, 2015 |DAILY BEAST 

But news of the classes is spread mainly by word of mouth, and participants bring along their friends and families. Iran’s Becoming a Footloose Nation as Dance Lessons Spread |IranWire |January 2, 2015 |DAILY BEAST 

But hitherto, before these new ideas began to spread in our community, the mass of men and women definitely settled down. The Salvaging Of Civilisation |H. G. (Herbert George) Wells 

The rapid spread of the revolt was not a whit less marvelous than its lack of method or cohesion. The Red Year |Louis Tracy 

The rebellion spread to their district, and many of the natives on and about the estate were eager to join in the movement. The Philippine Islands |John Foreman 

The spread of the holy gospel and uninterrupted preaching went on until the return of the ambassador. The Philippine Islands, 1493-1898, Volume XX, 1621-1624 |Various 

A smile of beatitude spread over his enormous countenance during the process. The Pit Town Coronet, Volume I (of 3) |Charles James Wills