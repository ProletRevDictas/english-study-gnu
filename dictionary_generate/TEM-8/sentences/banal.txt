Especially in the later images, some, at first, appear as banal as a stranger’s family photo album. At the Smithsonian, a photographic portrait of East Baltimore, decades before the dawn of the selfie era |Kelsey Ables |August 26, 2021 |Washington Post 

So it is no huge surprise that No Sudden Move — a rippling, brilliant story about trusting nobody — has some seemingly banal but actually fascinating economic and cultural underpinnings pulsing in its historical backdrop. The fascinating, horrifying history behind Steven Soderbergh’s new heist movie |Alissa Wilkinson |July 1, 2021 |Vox 

He has a politician’s recollection for obscure names and banal anecdotes, a relentless forward propulsion and limitless confidence, a firm grasp of the political upsides and downsides of any situation. Chris Matthews’ new book catalogues his front-row seat to history |Charlotte Alter |June 11, 2021 |Washington Post 

Many are consumed by disagreements over banal things like chores, though some have managed to have impacts that last beyond their expiration dates—Zuccotti Park, for example, produced a growing national consciousness about income inequality. The Intersection Where George Floyd Died Has Become a Strange, Sacred Place. Will Its Legacy Endure? |Janell Ross/Minneapolis |April 16, 2021 |Time 

Chang’s secret power is making the seemingly banal gripping, much to the pleasure of her readers. 12 Asian Writers to Watch |Kate Bartlett |April 8, 2021 |Ozy