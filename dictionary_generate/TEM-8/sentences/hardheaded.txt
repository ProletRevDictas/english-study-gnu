As I’ve said, he’s a hardheaded guy who’s not susceptible to a lot of creative fluttering. Two Simple Rules for Progressing at Anything |cobrien |July 21, 2021 |Outside Online 

While welcome, this is actually a hardheaded and highly practical move. CVS Quits Tobacco to Become a Medical Giant |Daniel Gross |February 5, 2014 |DAILY BEAST 

The hardheaded politician devoted to step-by- step progress was transformed in death into the consummate liberal idealist. How Jackie Kennedy Invented the Camelot Legend After JFK’s Death |James Piereson |November 12, 2013 |DAILY BEAST 

But hardheaded operatives like Karl Rove could shift their resources to Senate and House contests. GOP Civil War Is Coming as Mitt Romney Campaign Flails in Video’s Wake |Robert Shrum |September 18, 2012 |DAILY BEAST 

And so does the cause of decency in the hardheaded world of international affairs. Ronald Reagan’s Lessons for the Chen Guangcheng Case |Walter Reich |May 6, 2012 |DAILY BEAST 

For the hardheaded, distributing Qurans would have another benefit. The U.S. Military Should Hand Out Qurans in Afghanistan as a Good-Will Gesture |Richard Miniter |March 1, 2012 |DAILY BEAST 

Like all romancers, she is a little terrified at seeing one of her wildest conceits admitted by the hardheaded world. Song of the Lark |Willa Cather 

And she was glad to see that the hardheaded old miller was not much impressed by the man, either. Ruth Fielding on the St. Lawrence |Alice B. Emerson 

The son of the hardheaded father came out at a crisis; and not too highhandedly: he could hear an opposite argument to the end. The Amazing Marriage, Complete |George Meredith 

He was thorough, practical, and after he had cut his mountain teeth in the Peace River disaster, a hardheaded man at his work. The Daughter of a Magnate |Frank H. Spearman 

The man of science like the man in the street has to face hardheaded facts that cannot be blinked and explain them as best he can. Ulysses |James Joyce