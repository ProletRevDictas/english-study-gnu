Councilman Keith Blackburn from Carlsbad, County Supervisor Jim Desmond, El Cajon Mayor Bill Wells, Oceanside Councilman Jack Feller and Vista Mayor Judy Ritter joined Jones in voting against the proposal. Politics Report: Audit Day at SANDAG |Scott Lewis and Andrew Keatts |September 19, 2020 |Voice of San Diego 

A Union-Tribune reporter broke down those plans in Vista, Poway, Oceanside and Escondido, where officials intend to open campuses to small groups of English-learners and other students who are most in need. North County Report: Schools Are Reopening for Students Most in Need |Kayla Jimenez |September 16, 2020 |Voice of San Diego 

Werner’s version of events differs from what Vista jail employees have said in depositions. Longtime Sheriff’s Employee Contradicts Official Account of Jail Death |Kelly Davis |September 3, 2020 |Voice of San Diego 

In Vista, Mayor Judy Ritter has also aligned with the North County and rural area representatives. Politics Report: What Comes Next for Sports Arena |Scott Lewis and Andrew Keatts |July 11, 2020 |Voice of San Diego 

March and Ash has dispensaries in San Diego, Vista and the city of Imperial, and one of its principals thinks the checkpoints could actually serve a purpose. Businesses Say Border Patrol Is Seizing Legal Cannabis Between San Diego, Imperial |Jesse Marx and Maya Srikrishnan |June 29, 2020 |Voice of San Diego 

It quickly becomes way too apparent that this is not a scenic romantic vista, but rather code for Harding's own nether regions. Fifty Shades of Presidential FanFiction |Amy Zimmerman |August 2, 2014 |DAILY BEAST 

At that demarcation between inside and out, the vista is most expansive. Geoff Dyer at Sea: Unmoored but on Target |Melissa Holbrook Pierson |May 21, 2014 |DAILY BEAST 

Their vista of the snow-capped Andes suggests the Bavarian alps and the view from Berchtesgaden. Holocaust Horrors Haunt the Films ‘Ida’ And ‘The German Doctor’ |Jack Schwartz |May 12, 2014 |DAILY BEAST 

Others boast colorful tiles, captain windows to take in the vista, and fitted doors that give the appearance of a hobbit dwelling. New Mexico’s Amazing Man-Made Caves |Nina Strochlic |December 12, 2013 |DAILY BEAST 

However, a cold beer nestled in your backpack is the perfect treat when you finally make it to that vista or waterfall. Nine Amazing Places To Skinny Dip Around The World |Erin Cunningham |September 21, 2013 |DAILY BEAST 

But the death of the Queen, reported early in October 1290, again opened up a vista of hope. King Robert the Bruce |A. F. Murison 

It was barely four o'clock, and the sun came down a long vista of blue islands that led out to the open sea and Finland. Three More John Silence Stories |Algernon Blackwood 

Behind the tide of humanity rolling in from the burning district, at the end of every street, was a vista of flame and smoke. Ancestors |Gertrude Atherton 

And the possibility opened out such a vista of disasters that Alice was almost moved to tears. Punch, or the London Charivari, Vol. 108, April 6, 1895 |Various 

A late car roared down the long vista and fled, retreating in softening rumbles. The Woman Gives |Owen Johnson