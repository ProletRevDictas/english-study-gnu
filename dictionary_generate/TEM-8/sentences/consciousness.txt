QAnon, left unchecked for years, has tapped into the mainstream consciousness and is now involving people who may not even realize they’re being manipulated by QAnon-driven initiatives. Facebook tries to clean up Groups with new policies |Sarah Perez |September 17, 2020 |TechCrunch 

I’ve been defending this view on the grounds that it offers the best solution to the hard problem of consciousness. The Universe Knows Right from Wrong - Issue 89: The Dark Side |Philip Goff |September 9, 2020 |Nautilus 

Lewis-Williams argues that altered states of consciousness were likely still important as a way of journeying between these different cosmic realms. An Ancient Site with Human Skulls on Display - Issue 89: The Dark Side |Jo Marchant |September 2, 2020 |Nautilus 

Over the past decade, the NBA has increasingly been at the forefront of social consciousness among sports leagues. Why A Strike For Racial Justice Started With The Milwaukee Bucks And The NBA |Neil Paine (neil.paine@fivethirtyeight.com) |August 27, 2020 |FiveThirtyEight 

You open your eyes, come to your senses, and slide from dream state to consciousness. The Global Work Crisis: Automation, the Case Against Jobs, and What to Do About It |Peter Xing |August 6, 2020 |Singularity Hub 

As soon as the criminal left the shop, the victim snapped back into consciousness and tried to chase after him. Thief Hypnotizes Shopkeeper, Then Robs Him |Nico Hines |December 5, 2014 |DAILY BEAST 

However we strain to distract ourselves, our consciousness of death heightens our awareness of evil. McConaughey’s ‘Stand’—And Ours |James Poulos |December 5, 2014 |DAILY BEAST 

However, this has reduced consciousness of risk among young people, making the message of fear meaningless. The New Face of HIV Is Gay & Young |Adebisi Alimi |December 1, 2014 |DAILY BEAST 

And, as a result, an interesting and important text has burst out of the archives and into public consciousness. Jesus Christ, Baby Daddy? |Candida Moss |November 12, 2014 |DAILY BEAST 

Your acid experiences also seem to dovetail with expanding your musical consciousness. Herbie Hancock Holds Forth |David Yaffe |November 8, 2014 |DAILY BEAST 

The controlling center of consciousness is the extreme limit of the nares anteri. Expressive Voice Culture |Jessie Eldridge Southwick 

A ray of Consciousness is passed over that impression and you re-read it, you re-awaken the record. Assimilative Memory |Marcus Dwight Larrowe (AKA Prof. A. Loisette) 

Mine should be of pure steel; I have ordered her out of my consciousness these last weeks at the point of the bayonet. Ancestors |Gertrude Atherton 

A trap-door had opened in the floor of his consciousness; his first, early love sheltered in his aching heart again. The Wave |Algernon Blackwood 

I watched over you till you recovered consciousness of your own accord, and now—now I am here to guide you safely back to the inn. Three More John Silence Stories |Algernon Blackwood