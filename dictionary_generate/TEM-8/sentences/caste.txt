Her very presence on a sabha stage — an elite space historically dominated by upper-caste Brahmins — is a “political act,” Pillai tells me. No Dirty Dancing: India’s Classical Dancers Break Caste Taboos |Charu Kasturi |December 4, 2020 |Ozy 

“They don’t bring up caste, but they can easily identify us,” Kaila says, rattling off all of the ways he can be outed as potentially being Dalit, including the fact that he has darker skin. India’s engineers have thrived in Silicon Valley. So has its caste system. |Nitasha Tiku |October 28, 2020 |Washington Post 

Right now, the truth is that we are living in precarious times – we always have been, to one degree or another, depending on where you are in the disparity that is this American caste system. For Heather Matarazzo, ‘Equal’ is still a cause worth fighting for |John Paul King |October 20, 2020 |Washington Blade 

Intersections of gender, class, age and caste play a significant role in pushing women into exploitative situations. Three Women: Stories Of Indian Trafficked Brides |LGBTQ-Editor |October 5, 2020 |No Straight News 

My wife and I learnt a great deal about race, caste, and social boundaries. We won’t have a true economic recovery until we tackle the racial wealth gap |matthewheimer |September 1, 2020 |Fortune 

Such teaching is revolutionary in still-caste-divided India. Is India’s Fallen ‘God-Man’ So Different From a Megachurch Pastor? |Jay Michaelson |November 21, 2014 |DAILY BEAST 

The upper bureaucracy has evolved into a privileged and cossetted caste. Watch What You Say, The New Liberal Power Elite Won’t Tolerate Dissent |Joel Kotkin |June 7, 2014 |DAILY BEAST 

If we say yes to Telangana, why not say yes to caste-based states? India’s Newest State Telangana Is Bosnia Redux |Kranti Rai |March 22, 2014 |DAILY BEAST 

After all, caste is the most resistant feature of our politics, so why not just make it the basis for states formation? India’s Newest State Telangana Is Bosnia Redux |Kranti Rai |March 22, 2014 |DAILY BEAST 

Just last week, at another exclusive do, Larry Summers delivered another shocking address to another slice of the master caste. The Race Is On in Silicon Valley to Escape Real America |James Poulos |November 30, 2013 |DAILY BEAST 

Yet how came it that even a low-caste mongrel of a Lascar should offer such an overt insult to a Brahmin! The Red Year |Louis Tracy 

Caste is a thing you should be very careful of in these days, so the best thing is to ask for the Bear-Garden straightaway. Punch, or the London Charivari, Volume 158, April 28, 1920 |Various 

Until a few years ago the quarter known as the, Parian was the flourishing centre of the half-caste traders. The Philippine Islands |John Foreman 

Several American officers were present on the occasion, accompanied by a Spanish half-caste who acted as their interpreter. The Philippine Islands |John Foreman 

The labour of the spade and of the loom, and the petty gains of trade, he contemptuously abandoned to men of a lower caste. The History of England from the Accession of James II. |Thomas Babington Macaulay