Another was accused in a lawsuit of threatening to jail women if they didn’t have sex with him. Sacramento Report: Jones, COVID-19 and the Irony of Remote Voting |Sara Libby and Jesse Marx |August 28, 2020 |Voice of San Diego 

Maybe only a federal effort to establish standards and regulate compliance to them would be necessary before we no longer have a Robert Williams, a member of any minority group, or any citizen unjustly experience a night in jail or worse. The Bias in the Machine - Issue 89: The Dark Side |Sidney Perkowitz |August 19, 2020 |Nautilus 

DHS agents in Portland have tear-gassed protesters and pulled individuals into unmarked vans, and some of those people were banned from attending any more protests as a condition of being released from jail. What Happened In Portland Shows Just How Fragile Our Democracy Is |Maggie Koerth (maggie.koerth-baker@fivethirtyeight.com) |August 5, 2020 |FiveThirtyEight 

Anyone who violated the law would be subject to a fine of $100-$500 – the equivalent of $1,700-$8,500 today – or a jail term of up to 150 days. ‘Keep Your Mouth Shut’: Why San Diego Banned ‘Seditious’ Talk in 1918 |Randy Dotinga |August 4, 2020 |Voice of San Diego 

The officers then proceeded to grab his groceries, dump them to the ground, put him in handcuffs and take him to jail, where he’d spend the night for the crime of occupying multiple seats on a transit facility. The Next Major Reform Prosecutor Could Well Be a ‘Survivor’ |Joshua Eferighe |August 4, 2020 |Ozy 

As this list shows, punishments typically run to a short-ish jail sentence and/or a moderately hefty fine. In Defense of Blasphemy |Michael Tomasky |January 9, 2015 |DAILY BEAST 

Violators face up to nine months in jail or as much as $10,000 in fines. Judge: Rehoming Kids Is Trafficking |Tina Traster |December 30, 2014 |DAILY BEAST 

There are millions of stories that end with black boys in jail cells. Bobby Shmurda and Rap’s Ultimate Hoop Dream |Rawiya Kameir |December 23, 2014 |DAILY BEAST 

Brinsley got out of jail last July, and was desperate and aimless. Alleged Cop Killer Ismaaiyl Brinsley Had a Death Wish |M.L. Nestel |December 22, 2014 |DAILY BEAST 

Some of them, including Kurnosova, escaped the country as they faced a possible jail term for their opposition activity. Russians Plot Exiled Government in Kiev |Anna Nemtsova |December 16, 2014 |DAILY BEAST 

Why have I never heard until the day before yesterday of your suffering yourself to be cooped up in jail? The Posthumous Papers of the Pickwick Club, v. 2(of 2) |Charles Dickens 

They say that if he gets a judgment against you, Elder, he will put you in jail, and all that; but of course that couldn't be. The Homesteader |Oscar Micheaux 

Casey was arrested and conveyed to jail under great popular excitement. The Every Day Book of History and Chronology |Joel Munsell 

For these people, under the older dispensation, there was nothing but the poorhouse, the jail or starvation by the roadside. The Unsolved Riddle of Social Justice |Stephen Leacock 

On the Mabolo road there is a Leper Hospital, and the ruins of a partly well-built jail which was never completed. The Philippine Islands |John Foreman