When they report, players and other personnel will undergo an intake screening that includes a temperature check, a coronavirus test via nasal swab, and a rapid antibody test. MLB, players’ union agree to health and safety protocols, clearing way for spring training to start |Chelsea Janes |February 9, 2021 |Washington Post 

Most people’s intake already falls within these ranges, so striking the perfect balance of macros day after day isn’t something you should overthink. There Are No Rules for Healthy Eating |Christine Byrne |January 22, 2021 |Outside Online 

There, as documented earlier this year by The Washington Post, he went through an intake process that included a mandatory mental health therapy session. To stay or to go? |Hannah Dreier |December 26, 2020 |Washington Post 

A few years later you’d be the manager, but at first you worked intake and sat at a desk at the entrance. Dark spaces on the map |Katie McLean |December 18, 2020 |MIT Technology Review 

This is one reason it’s hard to get people to care about something as invisible as sodium intake. Salt: The dietary danger that’s easy to ignore |Karen Sandstrom |December 11, 2020 |Washington Post 

Similarly, the results of this study should not drastically increase your intake of Indian food. Fish Oil, Turmeric, and Ginseng, Oh My! Are ‘Brain Foods’ B.S.? |Dr. Anand Veeravagu, MD |October 10, 2014 |DAILY BEAST 

Prisoners are given uniforms, towels and basic hygiene kits upon intake, but forced to purchase just about everything else. ‘Progressive Jail’ Is a 21st-Century Hell, Inmates Complain |Sarah Shourd |September 29, 2014 |DAILY BEAST 

After all, without food intake for eight to 12 hours, you are in a fasted state. Does Fasted Cardio Really Burn More Fat? |DailyBurn |August 22, 2014 |DAILY BEAST 

My dad in a kind of manic phase, feeling really euphoric and excited and like [sharp breath intake] kind of high. Molly Shannon on ‘Life After Beth,’ Turning 50, and ‘Never Been Kissed’ |Melissa Leon |August 17, 2014 |DAILY BEAST 

Drink It Down Most beverages (non-alcoholic, of course) will help contribute to your daily water intake. 10 Ways to Stay Hydrated (That Aren’t Water) |DailyBurn |July 25, 2014 |DAILY BEAST 

A low, whistling sound, the simultaneous sharp intake of breath through the nostrils of forty men, filled the room. The Argus Pheasant |John Charles Beecham 

In the colder seasons warm air is fed to air intake of carbureter through the warm air elbow "F" (see cut). Marvel Carbureter and Heat Control |Anonymous 

The food intake correspondingly decreased during those periods. The Propaganda for Reform in Proprietary Medicines, Vol. 1 of 2 |Various 

A few turns of the intake valves of their helmets accomplished this, and they soon felt much better. The Revolt of the Star Men |Raymond Gallun 

The conditions with the intake and exhaust port fully opened are clearly shown at Fig. 8, C. Aviation Engines |Victor Wilfred Pag