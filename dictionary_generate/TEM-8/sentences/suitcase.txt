We sent gentlemen forth with wirelesses in suitcases and instructions to blow up certain bridges. Forgotten spies who fought the Nazis in the Middle East |Jonathan Kirsch |February 12, 2021 |Washington Post 

The Democrats are still going to have suitcases full of votes. The Clandestine Efforts to Keep Georgia’s Old-School Conservatives in the Fold |Nick Fouriezos |January 1, 2021 |Ozy 

A satellite the size of a small suitcase, ASTERIA was designed to demonstrate the technology needed for a tiny telescope to search for exoplanets by detecting the minuscule dip in a star’s light when an orbiting planet passes in front of it. My satellite would fit in a small suitcase. |Katie McLean |December 18, 2020 |MIT Technology Review 

What Giuliani and others claim happened is that observers were cleared from the room and that ballots hidden in suitcases were then brought out to be counted without oversight. Giuliani boasts about finally providing evidence of fraud (which doesn’t appear to be evidence of fraud) |Philip Bump |December 4, 2020 |Washington Post 

The sole is also flexible enough to bend in half, so the shoe packs we ll in a carry-on suitcase. Outside's 12 Days of Deals |The Editors |December 1, 2020 |Outside Online 

Her business started in a suitcase, where she kept her supplies. Inside A Finishing School for Transwomen |Sharon Adarlo |December 27, 2014 |DAILY BEAST 

One woman dying on his floor, another rotting in a suitcase: An alleged killer stuns the financial community in Hong Kong. Hong Kong’s High-Flying British Psycho Killer Suspect |Nico Hines, Tom Sykes |November 3, 2014 |DAILY BEAST 

Police said the woman in the suitcase had died on the previous day. Hong Kong’s High-Flying British Psycho Killer Suspect |Nico Hines, Tom Sykes |November 3, 2014 |DAILY BEAST 

The suitcase of stunners that Amal revealed throughout the weekend proves that she has one amazing sense of style. Screw George Clooney—Amal Alamuddin Is the Catch |Allison McNearney |September 30, 2014 |DAILY BEAST 

Excuse me, I have to get the keffiyeh out of my dusty suitcase and pack a kilt. Up to a Point: A Free Scotland Would Be a Hilarious Disaster |P. J. O’Rourke |September 13, 2014 |DAILY BEAST 

Marie was packing a suitcase and meditating upon the scorching letter she meant to write. Cabin Fever |B. M. Bower 

Lowell handed Miss Scovill's suitcase to the silent Wong, who had slipped out behind the women. Mystery Ranch |Arthur Chapman 

I suddenly tossed my suitcase into the barn, and began a tour of inspection over my thirty acres. The Idyl of Twin Fires |Walter Prichard Eaton 

If you wish them to do so, they will get your complete outfit, so you need not bring anything with you but a suitcase. In Africa |John T. McCutcheon 

Slipping naturally into the most conventional groove either of word or deed, Cornelia eyed the suitcase inquisitively. Molly Make-Believe |Eleanor Hallowell Abbott