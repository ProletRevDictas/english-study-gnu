Police in London announced an investigation into allegations that Charles’ charity the Prince’s Foundation was involved in a “cash for honors” deal in which a Saudi citizen was offered a knighthood in exchange for donations. Queen Elizabeth II Has COVID-19. Here's What to Know |Eloise Barry |February 21, 2022 |Time 

In addition to protecting his head and indicating his knighthood, the mask also allowed Kylo to feel more like his grandfather and idol, Darth Vader. Star Wars: How The Knights of Ren Are Different From The Sith |noreply@blogger.com (Unknown) |October 17, 2021 |TechCrunch 

At the end of his life, after the AFI dinner, the queen offered, and he accepted, a knighthood. Alfred Hitchcock’s Fade to Black: The Great Director’s Final Days |David Freeman |December 13, 2014 |DAILY BEAST 

After his knighthood, I stop by to see him and call him Sir Alfred. Alfred Hitchcock’s Fade to Black: The Great Director’s Final Days |David Freeman |December 13, 2014 |DAILY BEAST 

Knighted by the Queen—honorary knighthood by the Queen, I should say. Rudy Giuliani on His 9/11 Bluff, the Museum Controversy and the Rise of ISIS |Josh Robin |September 11, 2014 |DAILY BEAST 

Prime Minister David Cameron responded by saying the victory should be marked in truly British style: with a knighthood. Andy Murray Survived Dunblane School Massacre Before Winning Wimbledon |Nico Hines |July 9, 2013 |DAILY BEAST 

“I hear that a new order of Knighthood is on the tapis,” the letter reads. The Oxford English Dictionary: The Original Crowdsourcer |Josh Dzieza |April 29, 2013 |DAILY BEAST 

He then received the honour of knighthood but had retired from active service and become a director of his company. Fifty Years of Railway Life in England, Scotland and Ireland |Joseph Tatlow 

Thereupon the Prince went to Westminster Abbey and conferred knighthood upon his companions. King Robert the Bruce |A. F. Murison 

On his first absence after receiving knighthood he is taken prisoner by the Lady of Malehaut, who detains him in her castle. The Three Days' Tournament |Jessie L. Weston 

"Gilbert," said the nobleman, who was puffing himself up at the coach window, in his handsome red sash of the order of knighthood. Balsamo, The Magician |Alexander Dumas 

"I trust it will be found keen enough to satisfy any who question now my knighthood," came back the hot retort. God Wills It! |William Stearns Davis