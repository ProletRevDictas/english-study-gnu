We discovered that, for this year’s 40 Under 40, being more productive often means planning ahead and churning through tasks as quickly as possible—but also sometimes just shutting everything off and taking a break. 17 extremely useful productivity tips from this year’s 40 Under 40 |Maria Aspan |September 6, 2020 |Fortune 

They also overhauled their catching position with the addition of the productive Austin Nola from the Seattle Mariners and Jason Castro from the Los Angeles Angels. The Padres Are Going For It |Travis Sawchik |September 2, 2020 |FiveThirtyEight 

The tragedies did lead to some new legislation and some more productive conversations with Cal Fire. They Know How to Prevent Megafires. Why Won’t Anybody Listen? |by Elizabeth Weil |August 28, 2020 |ProPublica 

For many companies, especially those in services and technology, remote work has not been the disaster they might have expected, as their workforces have proven surprisingly productive. I’m a physician and a CEO. Why I won’t bring my employees back to the office before Labor Day 2021 |matthewheimer |August 26, 2020 |Fortune 

Such results could inform future efforts to make Holsteins hornless but no less productive. Biotechnology Could Change the Cattle Industry. Will It Succeed? |Dyllan Furness |August 16, 2020 |Singularity Hub 

“Small groups of like-minded Turkers [can] come together and start taking productive action,” Bernstein said. Amazon’s Turkers Kick Off the First Crowdsourced Labor Guild |Kevin Zawacki |December 3, 2014 |DAILY BEAST 

Remember—American workers work longer hours and are more productive today than they were in 1975. It’s Always Black Friday for Clerks |Michael Tomasky |November 28, 2014 |DAILY BEAST 

Human rights groups, however, said the measures were “draconian” and counter-productive. To Stop ISIS, Britain Is Set to Stop Free Speech |Nico Hines |November 25, 2014 |DAILY BEAST 

But today, many landfills are actually quite productive places. Garbage In, Power Out |The Daily Beast |November 24, 2014 |DAILY BEAST 

Next, Borlaug helped develop more productive and drought-resistant strains of rice that became adapted widely in Asia. Growth Stocks |The Daily Beast |October 17, 2014 |DAILY BEAST 

Great as is the destruction of war, not even five years of it have broken the productive machine. The Unsolved Riddle of Social Justice |Stephen Leacock 

It may very well be that an eight-hour day will prove, presently if not immediately, to be more productive than one of ten. The Unsolved Riddle of Social Justice |Stephen Leacock 

A ten-hour day, speaking in general terms and leaving out individual exceptions, is probably more productive than a day of twelve. The Unsolved Riddle of Social Justice |Stephen Leacock 

An attempt to impose an imitation on a practised judge is always productive of an unpleasant result. Violins and Violin Makers |Joseph Pearce 

An abundance of limestone makes the soil exceptionally fertile and productive. Hallowed Heritage: The Life of Virginia |Dorothy M. Torpey