Charlie Sykes, a longtime conservative commentator and editor-at-large for the Bulwark, described coverage of critical race theory as a kind of “shark attack politics” relying on a limited set of local anecdotes to tantalize viewers. Critical race theory is the hottest topic on Fox News. And it’s only getting hotter. |Jeremy Barr |June 24, 2021 |Washington Post 

In recent years, film, television, and book deals have become tantalizing secondary revenue sources for high-profile reporters looking for a lucrative paycheck on top of a journalist’s salary. How Infighting Killed the NY Times’ Chance at a GameStop Movie Deal |Lachlan Cartwright, Maxwell Tani |June 17, 2021 |The Daily Beast 

Every time he stepped on the floor, though, his skill set tantalized, and when the season restarted in the bubble, Porter’s star shone brighter than ever. Michael Porter Jr.’s Breakout Is Real |Jared Dubin |May 5, 2021 |FiveThirtyEight 

Only time will tell if that changes in 2021 — but in the first few weeks of the season at least, Buxton has tantalized everyone again with flashes of the stellar player Minnesota has been hoping he would become for years. This Is The Byron Buxton We Were Promised |Neil Paine (neil.paine@fivethirtyeight.com) |April 13, 2021 |FiveThirtyEight 

The park’s tantalizing waterfalls and the strong spring current of the Merced River have also attributed to the high fatality numbers—17 people drowned. These Are the 5 Deadliest National Parks |Emily Pennington |April 8, 2021 |Outside Online 

There was just enough to tantalize the poor brutes without filling their stomachs. Popular Adventure Tales |Mayne Reid 

Details had vanished eons ago, but something still remained to tantalize imagination. Shock Treatment |Stanley Mullen 

Trying to tantalize their victim, the Apaches made thrusts at Ensign Dave, and then leaped nimbly back. Dave Darrin on Mediterranean Service |H. Irving Hancock 

Therefore we call it to tantalize a person to offer him a thing he longs for, and then to draw it away from him. The Nursery, No. 107, November, 1875, Vol. XVIII. |Various 

This singular punishment inflicted upon Tantalus gave rise to the expression “to tantalize.” Myths of Greece and Rome |H. A. Guerber