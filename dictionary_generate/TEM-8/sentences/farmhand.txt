It was ugly, cantankerous, simple enough for any farmhand to understand and fix, and indomitable. The Wonderful, Horrible Life of Henry Ford |Richard Snow |May 14, 2013 |DAILY BEAST 

My father was placed as a farmhand in exchange for room and board. My Father, The Inglourious Basterd |Kim Masters |August 9, 2009 |DAILY BEAST 

Then it is sung softly like the farmhand quartettes do in the rural melodrama outside the old homestead in harvest time. The Real Latin Quarter |F. Berkeley Smith 

He had always worked as a farmhand, and had acquired but little in the way of an education. The Land of Lure |Elliott Smith 

A whiskey-crazed farmhand is walking bare footed up and down the middle of the road defying the world. A Son of the Middle Border |Hamlin Garland 

But Pete, jewel of a farmhand though he was, possessed one serious flaw: he would have a periodical spree. Doctor Jones' Picnic |S. E. Chapman 

He knew the love of the people of The Place for Lad, and he wondered at this invitation to a farmhand to thrash the dog publicly. Lad: A Dog |Albert Payson Terhune