One night he listened to a friendly old man slowly die alone in the next cell after a bout of torture. Islamist Terrorism Is Not Done With Us, Warns Former al Qaeda Hostage Theo Padnos |Karl Vick |February 25, 2021 |Time 

These were people who had just escaped the most horrific abuse and torture, and violation from their own families. Witness to the horrors in Chechnya |John Paul King |February 25, 2021 |Washington Blade 

She wrote her resident’s thesis on political disappearances and torture cases archived by the Human Rights Foundation of Turkey. The 'Badass Chief of Staff' of Turkey's Opposition Faces Years in Jail After Challenging Erdogan's Power. She's Not Backing Down |Joseph Hincks / Istanbul |February 24, 2021 |Time 

The same month, President Buhari signed into law the Anti-Torture Act, which criminalized torture. The Nigerian Government Has Pledged to #EndSARS and Reform the Police. This Isn't the First Time They've Made That Promise |Andrew R. Chow |October 28, 2020 |Time 

Many of them live in secrecy for fear of torture and execution. Inside the strange new world of being a deepfake actor |Karen Hao |October 9, 2020 |MIT Technology Review 

But Chechen leader Kadyrov does not think that Committee Against Torture was needed in Chechnya. Putin’s Favorite Acolyte Terrorizes Human Rights Activists |Anna Nemtsova |December 14, 2014 |DAILY BEAST 

Torture, the UVa rape, police violence—we hunger for the facts, and then twist them. On Torture, Chuck Johnson & Sondheim |James Poulos |December 13, 2014 |DAILY BEAST 

He also wrote, “Torture is not a thing that we can tolerate.” The Luxury Homes That Torture and Your Tax Dollars Built |Michael Daly |December 12, 2014 |DAILY BEAST 

White House Must Decide Who Will Be Named in the CIA ‘Torture Report’ The Best of the Beast, Aug 9-10 | |August 9, 2014 |DAILY BEAST 

Looking for a place to go, Alyokhina called her friends at a local human rights center, the Committee Against Torture. Pussy Riot Roars Out of Prison |Anna Nemtsova |December 23, 2013 |DAILY BEAST 

Torture may change your mind, as shame shall change your body. The Proud Prince |Justin Huntly McCarthy 

Torture indescribable has made of me a writhing, moaning, helpless creature for the past few minutes. The Sorrows of Satan |Marie Corelli 

Torture was still employed in capital cases to force confession even in Holland and France. Boswell's Correspondence with the Honourable Andrew Erskine, and His Journal of a Tour to Corsica |James Boswell 

Hercules in all the extremity of his Torture does not fall foul upon Religion. A Short View of the Immorality, and Profaneness of the English Stage |Jeremy Collier 

Torture was, therefore, at once employed to discover the hidden treasures. The Best of the World's Classics, Restricted to Prose, Vol. X (of X) - America - II, Index |Various