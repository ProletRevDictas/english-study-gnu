There are four photos there of representative presidential candidates. Today’s GOP: Still Cool With Racist Pandering? |Michael Tomasky |January 7, 2015 |DAILY BEAST 

Four weeks after the injections, all 20 of the participants had developed the antibodies needed to stave off the infection. The Race for the Ebola Vaccine |Abby Haglage |January 7, 2015 |DAILY BEAST 

After four or five months of casual interaction, they realized they both had lost a young parent to cancer. Everyone at This Dinner Party Has Lost Someone |Samantha Levine |January 6, 2015 |DAILY BEAST 

The injunction, she argued, only applies to these four plaintiffs—not to anyone else. The Back Alley, Low Blow-Ridden Fight to Stop Gay Marriage in Florida Is Finally Over |Jay Michaelson |January 5, 2015 |DAILY BEAST 

Each CAP, also known as an “orbit,” consists on four aircraft. Exclusive: U.S. Drone Fleet at ‘Breaking Point,’ Air Force Says |Dave Majumdar |January 5, 2015 |DAILY BEAST 

A little boy of four was moved to passionate grief at the sight of a dead dog taken from a pond. Children's Ways |James Sully 

In cross-section the burrows varied from round (three inches in diameter) to oval (three inches high and four inches wide). Summer Birds From the Yucatan Peninsula |Erwin E. Klaas 

Before the spinet a bench was placed about four feet below the keys, and I was put upon the bench. Gulliver's Travels |Jonathan Swift 

We had six field-pieces, but we only took four, harnessed wit twice the usual number of horses. Blackwood's Edinburgh Magazine, No. CCCXXXIX. January, 1844. Vol. LV. |Various 

The Seven-score and four on the six middle Bells, the treble leading, and the tenor lying behind every change, makes good Musick. Tintinnalogia, or, the Art of Ringing |Richard Duckworth and Fabian Stedman