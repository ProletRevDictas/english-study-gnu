If shots are transported during a blizzard, the vaccine convoy could follow 30 minutes behind a salt truck. Now comes the hardest part: Getting a coronavirus vaccine from loading dock to upper arm |Lena H. Sun, Frances Stead Sellers |November 23, 2020 |Washington Post 

A love story set against the backdrop of the Irish War of Independence, the film opens with an Irish Republican Army ambush of a police convoy carrying two of their captured members, and was released in America as River of Unrest. OZY Highlights: Tool Up for a Better Tomorrow |Tracy Moran |November 22, 2020 |Ozy 

We’re social animals, and serving the greatest number in your social convoy guarantees your continued existence. Sex in Front of Strangers |Eugene Robinson |November 15, 2020 |Ozy 

His strolling is interrupted when he receives a call from Peter Parker asking him to help supervise a prison convoy. ‘Marvel’s Spider-Man: Miles Morales’: Like its movie counterparts, a confident and entertaining spectacle |Christopher Byrd |November 6, 2020 |Washington Post 

Suspects’ identities remain unclear in deadly convoy attack. These 10 journalists have been killed and deserve justice immediately |Brett Haensel |November 2, 2020 |Fortune 

At the head of this five-mile convoy was Ivan Smith, a 22-year-old Rhodesian. ‘Argo’ in the Congo: The Ghosts of the Stanleyville Hostage Crisis |Nina Strochlic |November 23, 2014 |DAILY BEAST 

As soon as the convoy left, Jimbo came out of the double doors. I Shot Bin Laden |Elliot Ackerman |November 16, 2014 |DAILY BEAST 

But one convoy alone, according to Lysenko, consisted of 32 tanks, 16 D-30 howitzers and 30 KamAZ heavy trucks. Putin Is Lying on Ukraine—and the West Can’t Stop Him |Jamie Dettmer |November 14, 2014 |DAILY BEAST 

The convoy included more than 40 tankers and trucks--19 of those were each towing a 122mm howitzer. Thousands of Putin’s Troops Now in Ukraine, Analysts Say |Shane Harris |November 11, 2014 |DAILY BEAST 

On Tuesday, the group witnessed a convoy of 43 unmarked green military trucks with tarpaulin covers moving towards Donetsk. Thousands of Putin’s Troops Now in Ukraine, Analysts Say |Shane Harris |November 11, 2014 |DAILY BEAST 

It was risky work to leave the valuable convoy for an instant, but Malcolm felt that he must probe this mystery. The Red Year |Louis Tracy 

You must proceed, with your convoy and escort, till you regain the high-road, then take the first quarters you can find. Blackwood's Edinburgh Magazine, Volume 67, Number 414, April, 1850 |Various 

At the same time the Napoleon, with a convoy of steamers and transports, rose well into sight on our starboard quarter. The British Expedition to the Crimea |William Howard Russell 

Gingham merely knew that a convoy was going up; and intended to go in company, for the sake of the guard. Blackwood's Edinburgh Magazine, Volume 67, Number 414, April, 1850 |Various 

Mr Chesterfield now pushed forward with a party by the lane towards the 410 ford, the convoy and the rest of the escort following. Blackwood's Edinburgh Magazine, Volume 67, Number 414, April, 1850 |Various