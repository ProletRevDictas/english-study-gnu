If anyone were to believe that this was meant just metaphorically, Prokopios even goes on to write that those who spent time with the emperor late into the night would see a ghost instead of his bodily form. What the QAnon of the 6th Century Teaches Us About Conspiracies |Roland Betancourt |February 3, 2021 |Time 

It appears the past four years have exposed this nation as an emperor with no clothes. What Should Americans Expect in 2021? |Tracy Moran |January 1, 2021 |Ozy 

At the bottom of the world, hundreds of thousands of emperor penguins emerge from the sea each April to trek over 50 miles to their inland colonies. Math of the Penguins |Susan D'Agostino |August 17, 2020 |Quanta Magazine 

Patches of penguin poop spotted in new high-resolution satellite images of Antarctica reveal a handful of small, previously overlooked emperor penguin colonies. Penguin poop spotted from space ups the tally of emperor penguin colonies |Carolyn Gramling |August 4, 2020 |Science News 

As opposed to the crowd-pleasing projects sanctioned by the emperor, the monument was actually commissioned by the Roman Senate, on the occasion of Augustus’ return to Rome in 13 BC. Reconstruction of Ara Pacis – The ‘Altar of Peace’ in Rome |Dattatreya Mandal |April 25, 2020 |Realm of History 

At times, Mario Cuomo seemed to have the humility of a Jesuit and the goals of an emperor. Mario Cuomo, Always Moving Us Toward the Light |Mike Barnicle |January 4, 2015 |DAILY BEAST 

That is, of course, unless one has something new to say about the French emperor. Napoleon Was a Dynamite Dictator |J.P. O’Malley |November 7, 2014 |DAILY BEAST 

How her role became more “maternal rather than marital,” and branding Hawking an “all-powerful emperor” and “masterly puppeteer.” The Other Side of Stephen Hawking: Strippers, Aliens, and Disturbing Abuse Claims |Marlow Stern |November 6, 2014 |DAILY BEAST 

The problem, as many an emperor could confirm, is that culminating points are easiest to identify in retrospect. War! What Is It Good For? A Lot |Nick Romeo |August 13, 2014 |DAILY BEAST 

He also eventually comes around, killing the Emperor and saving his son, Luke. The 13 Coolest Movie Dads: ‘Taken,’ ‘Star Wars,’ ‘Die Hard,’ and More |Marlow Stern |June 15, 2014 |DAILY BEAST 

However, on reaching Spain, the magic of the Emperor's personality soon restored the vigour and prestige of the French arms. Napoleon's Marshals |R. P. Dunn-Pattison 

In passing to her own chamber she met the Emperor, and, in the agitation of her maternal fears, told him all that had passed. The Pastor's Fire-side Vol. 3 of 4 |Jane Porter 

The first rail road opened in Brazil, the emperor and empress being present at the inauguration. The Every Day Book of History and Chronology |Joel Munsell 

At the end of the campaign the Emperor justly rewarded his lieutenant by creating him Prince of Wagram. Napoleon's Marshals |R. P. Dunn-Pattison 

The emperor Nicholas of Russia declared, by ukase, his purpose to assist Austria. The Every Day Book of History and Chronology |Joel Munsell