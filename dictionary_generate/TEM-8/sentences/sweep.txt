Unless it’s a clean sweep by the Democrats, none of this will happen. What Wall Street needs from the 2020 election |Bernhard Warner |September 30, 2020 |Fortune 

The chytrid pandemic was selecting frogs based on their microbes—a selective sweep in which amphibians harboring one microbial community survived above all others. When Evolution Is Infectious - Issue 90: Something Green |Moises Velasquez-Manoff |September 30, 2020 |Nautilus 

This gravel handlebar keeps the controls in the same neutral position as a standard road handlebar and creates sweep below the controls for a wider, more confident hand position when riding in the drops. New Gravel Bike Accessories for a Smoother Ride |Josh Patterson |September 28, 2020 |Outside Online 

There’s also no evidence that a single sweep of the virus through the population would lead to herd immunity, says Sten Vermund, dean of the Yale School of Public Health. Herd immunity alone won’t stop COVID-19. Here’s why. |Kate Baggaley |September 2, 2020 |Popular-Science 

Although several players have gotten close, and several others seem like they’ve played for every single team in a league, there are surprisingly few who have made a clean sweep of a given division. The Summer Of Playoffs |Sarah Shachat |August 18, 2020 |FiveThirtyEight 

Decorative yes, but a daily handbag that will sweep through the closets of women worldwide? Handbags: The More You Pay, The Smaller They Shrink |Elizabeth Landers |December 29, 2014 |DAILY BEAST 

In the long sweep of LGBT equality, it could have stood as a seminal moment. Is Gay Marriage Going Away in 2016? |David Freedlander |December 4, 2014 |DAILY BEAST 

The “24-hour news cycle” just makes them harder to sweep under the rug and ignore. I Blame People Who Blame the Media: Robert McCulloch’s Tone-Deaf Speech |Arthur Chu |November 25, 2014 |DAILY BEAST 

It was a street-sweep, and violence had broken out, and the government was cracking down. Jon Stewart Talks ‘Rosewater’ and the ‘Chickensh-t’ Democrats’ Midterm Massacre |Marlow Stern |November 9, 2014 |DAILY BEAST 

The remains were shipped to Fiji just as the war was about to sweep the region. How Amelia's Plane Was Found |Michael Daly |October 30, 2014 |DAILY BEAST 

To talk German was beyond the sweep of my dizziest ambition, but an Italian runner or porter instantly presented himself. Glances at Europe |Horace Greeley 

He walked up the sweep of sandy drive to the hotel and went through the big glass doors. The Wave |Algernon Blackwood 

Thenceforth, it ebbed, though it raged madly for a while in the effort to sweep away the obstruction. The Red Year |Louis Tracy 

And then if the ghost of a chimney-sweep were to appear—and why not the spirit of a sweep as well as anybody else? Second Edition of A Discovery Concerning Ghosts |George Cruikshank 

It is otherwise with the people who dwell upon the land over which these atmospheric convulsions sweep. Outlines of the Earth's History |Nathaniel Southgate Shaler