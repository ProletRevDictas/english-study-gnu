A mouthful of this noxious mess and the offender succumbs or hotfoots it to a different plant. Junk Food Is Bad For Plants, Too - Issue 90: Something Green |Anne Bikl&#233; & David R. Montgomery |September 23, 2020 |Nautilus 

Several hours had elapsed since he had entered hotfoot to see her; and the day was beginning to wane. The Long Night |Stanley Weyman 

Well for the king that they had no change of steeds, but had ridden hotfoot after him from the battlefield. Wulfric the Weapon Thane |Charles W. Whistler 

I have shown merely whither circumstantial evidence leads us when we go hotfoot after a theory. The Boy Scouts Book of Stories |Various 

Guessing what had happened, Operator Green dashed out hotfoot in pursuit. The Grammar School Boys of Gridley |H. Irving Hancock 

Returning hotfoot, she found Finn immovable beside the mouth of the cave, a formidable sentry. Jan |A. J. Dawson