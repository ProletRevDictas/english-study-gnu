One of the many enthusiastic blurbs describes it as “gut-wrenchingly funny.” We need comic novels more than ever. So where are they? |Ron Charles |May 27, 2021 |Washington Post 

For a novice thriller writer, Abrams plucked blurbs from nine heavy-hitters, a literary baseball starting lineup, including Nora Roberts, Scott Turow, Lee Child and Michael Connelly. And in her spare time, Stacey Abrams wrote a thriller |Karen Heller |May 6, 2021 |Washington Post 

I get the sense that we’re looking at a list of magazine blurbs. Which Food Publication Won the 2020 Holiday Cookie Bake-off? |Eater Staff |December 4, 2020 |Eater 

In other words, a good answer might well be found in a single passage or paragraph in an otherwise broad topic document, or random blurb page without much focus at all. Could Google passage indexing be leveraging BERT? |Dawn Anderson |October 29, 2020 |Search Engine Land 

A blurb could look something like “The Trail Runner XT allows you to comfortably walk 20-25 miles a day as you trek through the beautiful landscape of the Appalachian trail”. Align your content with customer intent via zero click search |Ron Cierniakoski |October 7, 2020 |Search Engine Watch 

Even the patron saint of teenage girls, Judy Blume, is featured on the back cover with a blurb for the book. Time to Grow Up, Lena Dunham |Emily Shire |October 10, 2014 |DAILY BEAST 

I first read the book in galleys some months ago; the finished edition carries a blurb from me on the back cover. Who Was the Real Cato? |David Frum |December 20, 2012 |DAILY BEAST 

He helped John McMillian and me get our first book contract, for The Radical Reader, and then wrote a generous blurb. Howard Zinn at 90: Defending the People’s Historian |Timothy Patrick McCarthy |August 27, 2012 |DAILY BEAST 

Shteyngart sat down with The Daily Beast to discuss the book, the art of the blurb, and, yes, teaching James Franco. Hipster Novelist Gary Shteyngart on Mentoring James Franco |Gregory Gilderman, The Daily Beast Video |July 23, 2010 |DAILY BEAST 

He shares his secret formula for writing a good blurb–and his selfish motive for blurbing so frequently. Hipster Novelist Gary Shteyngart on Mentoring James Franco |Gregory Gilderman, The Daily Beast Video |July 23, 2010 |DAILY BEAST 

Front matter consisting of a blurb and a list of other publications by the author has been moved to the end of the text. Storm Over Warlock |Andre Norton 

It—it's just the sort of thing we call a 'blurb,' Miss West! Kenny |Leona Dalrymple 

Matson read that blurb in an official press release and laughed cynically. Assassin |Jesse Franklin Bone 

Herman had the wild thought that they were blurb writers whose jobs had gone to their heads. Freudian Slip |Franklin Abel 

Modern Library blurb: "mail complete list of titles" left as is. Candide |Voltaire