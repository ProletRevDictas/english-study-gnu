Obama, whose policies were more moderate than his lofty campaign rhetoric, sought to reassure the establishment and reconcile with the Clinton wing. The Return of the Regulators |by Jesse Eisinger |February 2, 2021 |ProPublica 

Lenders also can request access to real-time API data feeds to view performance of their borrowers and reconcile transaction data. Valon closes on $50M a16z-led Series A to grow mobile-first mortgage servicing platform |Mary Ann Azevedo |February 2, 2021 |TechCrunch 

It’s a process that has been around since the 1970s and was meant to make it easier for the House and the Senate to reconcile differences in budget and tax bills. The two ways a pandemic relief bill could pass, explained |Peter Stevenson |February 2, 2021 |Washington Post 

It is not an obvious threshold, and it confused school officials trying to reconcile state and local government guidance. State: School Districts Like San Diego Unified Cannot Reopen Now |Ashly McGlone |January 26, 2021 |Voice of San Diego 

Bringing about moderate democracy and sound governance in Hong Kong, while reconciling Beijing and Hong Kong’s interests, is at the very least a Herculean effort. The Only Path Ahead for Hong Kong Is Reform |Brian Wong |January 13, 2021 |Time 

America presents two contradictory narratives that it struggles to reconcile. Will Texas Stay Texan? |David Fontana |December 29, 2014 |DAILY BEAST 

Reconcile is a rapper from Houston, a city with a rich hip-hop legacy. Down With the King: Christianity Isn’t Hiding in Rap’s Closet |Stereo Williams |December 28, 2014 |DAILY BEAST 

But Reconcile is from a slightly different arm of Houston hip-hop—more focused on spiritual triumph over the trap. Down With the King: Christianity Isn’t Hiding in Rap’s Closet |Stereo Williams |December 28, 2014 |DAILY BEAST 

Efforts to reconcile these differences have been delayed and the issue remains disputed. ‘Practically Speaking, Iraq Has Broken Apart’ |Eli Lake |June 16, 2014 |DAILY BEAST 

First Lady Mellie (Bellamy Young) and Fitz reconcile—because of the whole rape thing—and we learn the son is actually his. The Explosive ‘Scandal’ Finale Was Its Best Episode Yet |Kevin Fallon |April 18, 2014 |DAILY BEAST 

I cannot reconcile the idea of a tender Heavenly Father with the known horrors of war, slavery, pestilence, and insanity. God and my Neighbour |Robert Blatchford 

But, of course, all that is impossible, and the thing is to reconcile them to the inevitable things they have to face. Mystery Ranch |Arthur Chapman 

Here, then, is sufficient to reconcile the women to Mahomet, who has not used them so hardly as he is said to have done. A Philosophical Dictionary, Volume 1 (of 10) |Franois-Marie Arouet (AKA Voltaire) 

But how are we to reconcile improbable facts related in a contradictory manner? A Philosophical Dictionary, Volume 1 (of 10) |Franois-Marie Arouet (AKA Voltaire) 

So Corydon had to reconcile herself to a house with a stove, and a stove-pipe that went through a hole in the wall! Love's Pilgrimage |Upton Sinclair