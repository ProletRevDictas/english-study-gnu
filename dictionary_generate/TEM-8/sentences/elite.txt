The program’s detractors argue that the initiative is little more than a handout to wealthy elites. Opportunity Zones haven’t fully reached their potential, but don’t write them off yet |jakemeth |September 16, 2020 |Fortune 

An annual rite of recent autumns has been to wonder whether Green Bay Packers quarterback Aaron Rodgers truly belongs among the game’s elite signal-callers anymore. Aaron Rodgers Is Playing Like Aaron Rodgers Again |Neil Paine (neil.paine@fivethirtyeight.com) |September 16, 2020 |FiveThirtyEight 

Researchers have suggested that these structures were places for ritual ceremonies, houses of social elites or protection from attackers. Drones find signs of a Native American ‘Great Settlement’ beneath a Kansas pasture |Bruce Bower |September 10, 2020 |Science News 

We want to make elite personalized coaching accessible to all. Mustard raises $1.7M to improve athletic mechanics with AI |Brian Heater |September 4, 2020 |TechCrunch 

Sure, there are those moments in elite sports when the ball bounces off the post and off a defender’s foot into the goal, but still, luck is not supernatural. ‘The Dream Architects’: Inside the making of gaming’s biggest franchises |Rachel King |September 1, 2020 |Fortune 

And they all travel affordably, busting the myth that travel is only for the elite. ‘We Out Here’: Inside the New Black Travel Movement |Charlise Ferguson |January 4, 2015 |DAILY BEAST 

They then become members of the ultra elite Unit 121, granted premium housing and a well-stocked cupboard. Inside the ‘Surprisingly Great’ North Korean Hacker Hotel |Michael Daly |December 20, 2014 |DAILY BEAST 

"Our Siberian girls are admired more by Asian countries than by the West," Elite Stars school director Tatyana Fetisova told me. Is 9-Year-Old Russian Model Kristina Pimenova Too Sexualized? |Anna Nemtsova |December 12, 2014 |DAILY BEAST 

One of five top agencies in Novosibirsk, Elite Stars, recently was producing over 200 models every two to three months. Is 9-Year-Old Russian Model Kristina Pimenova Too Sexualized? |Anna Nemtsova |December 12, 2014 |DAILY BEAST 

Muslim leaders have accused the ruling elite of carrying out what they see as religious persecution. Al-Shabab’s Anti-Christian Slaughter |Margot Kiser |December 3, 2014 |DAILY BEAST 

The wealthy elite sons and daughters of the hacienda studied at institutions of higher learning abroad. The Haciendas of Mexico |Paul Alexander Bartlett 

None of these elite were making any effort to approach the buffet or the portable bar at the other side of the room. "And That's How It Was, Officer" |Ralph Sholto 

On this much smaller sheet the elite type makes a better appearance with letters of this kind. How to Write Letters (Formerly The Book of Letters) |Mary Owens Crowther 

Here every Sunday exquisite music is rendered, and here come the elite to worship and to add liberal gifts. The Harris-Ingram Experiment |Charles E. Bolton 

Only the financially elite could afford to belong and play upon its tame nine-hole course. I Walked in Arden |Jack Crawford