Even though you all can’t see each other in person right now, video calling makes it relatively easy to forge new bonds during these times. Networking 101: Why Working Together Creates More Opportunity Than Working Apart |Shantel Holder |September 4, 2020 |Essence.com 

Coinbase is the company that will help forge this future and bring crypto into the mainstream. Coinbase shuffles board ahead of rumored IPO, Marc Andreessen joins as observer |Jeff |August 31, 2020 |Fortune 

A career forged making bold bets on global economic trends was faltering badly, taking the fun out of it for the 48-year-old — and his few remaining investors. Hedge Fund ‘Pirates’ Set Sail Again |Daniel Malloy |August 21, 2020 |Ozy 

Airbnb is forging ahead with an initial public offering, despite a host of challenges. Airbnb’s 3 biggest challenges on its road to an IPO |Danielle Abril |August 20, 2020 |Fortune 

In these experiments, physicists used the LHC to smash protons together and observe the particles forged in the collisions. This is the first known particle with four of the same kind of quark |Maria Temming |July 7, 2020 |Science News 

If we wondered where a forger would get the materials to forge a text like this, we need look no further than eBay. Dismembering History: The Shady Online Trade in Ancient Texts |Candida Moss |November 23, 2014 |DAILY BEAST 

Failing to forge lasting stability would leave us, this author and his like-minded aides, to call for a Transitional Council. The Nuclear Deal That Iran’s Regime Fears Most |Djavad Khadem |November 22, 2014 |DAILY BEAST 

Like Tomas and Ebba, sometimes the best way to handle the situation is to put your head down and forge ahead. ‘Force Majeure’ and the Swedish Family Vacation From Hell |Alex Suskind |October 27, 2014 |DAILY BEAST 

You were commended after the avian flu pandemic for your ability to forge such close friendships with international leaders. Meet America’s New Top Ebola Fighter |Abby Haglage |September 26, 2014 |DAILY BEAST 

And he would transform the electronics market that Edison had helped forge. From Edison to Jobs |The Daily Beast |September 25, 2014 |DAILY BEAST 

It was a pretty house, stood a little apart from the forge, and was called Rock Villa. Fifty Years of Railway Life in England, Scotland and Ireland |Joseph Tatlow 

Business men and some professional are the only ones that forge steadily ahead; with precious few exceptions. Ancestors |Gertrude Atherton 

A temporary forge had been set up, and soldiers in leather aprons were working over the fire. The Amazing Interlude |Mary Roberts Rinehart 

She was making two hooks for her kitchen wall, for she was clever at the forge, and could shoe a horse if she were let to do so. When Valmond Came to Pontiac, Complete |Gilbert Parker 

In the dull glare of the forge fire knelt Parpon, rocking back and forth beside the body. When Valmond Came to Pontiac, Complete |Gilbert Parker