Only time will tell whether the latest in the long and sordid history of youth abuse scandals will lead Texas to make real changes, redirecting resources away from prisons and into community-based rehabilitation and prevention programs. For more than a century, Texas youth prisons have fostered abuse |Bill Bush |October 25, 2021 |Washington Post 

A Democratic Attorney General a few years earlier resigned in a sordid sex scandal. You Think Washington Is Dysfunctional? Look at Your State Capital |Philip Elliott |October 22, 2021 |Time 

Jane was far from alone in what prosecutors describe as a sordid web of abuse and misconduct. The Horrifying Testimony R. Kelly Needs Jurors to Forget |Pilar Melendez |August 25, 2021 |The Daily Beast 

Alan Taylor, the distinguished University of Virginia historian, has spent his career upending the conventional story in favor of the more sordid and useful truth. The young United States’ manifest uncertainty |Colin Woodard |June 25, 2021 |Washington Post 

That some of his cartoons from the 1950s are unacceptable to modern audiences, however, is perhaps less surprising than his sordid personal life. 12 Crazy True Stories About Children’s Authors |Kate Bartlett |April 1, 2021 |Ozy 

The U.K. tabloids, as is their wont, have branded her “shameless,” “sordid,” and “the scourge of society.” The X Factor of Sex Invades Britain: Rebecca More’s ‘Sex Tour’ Enrages UK Politicians |Marlow Stern |October 20, 2014 |DAILY BEAST 

Their relationship was messy and sordid and full of lies and jealousy and betrayal and backstabbing. How to Get Away With Gayness: Shonda Rhimes Kills TV’s Sex Stereotypes |Kevin Fallon |September 25, 2014 |DAILY BEAST 

Other micro-countries have more sordid, even criminal, histories. So You Want to Rule a Kingdom? A Wacky History of One-Man Nations |Nina Strochlic |July 17, 2014 |DAILY BEAST 

The sordid story of a female co-founder stripped of her title because she was harassed. The Daily Beast’s Best Longreads, July 6, 2014 |The Daily Beast |July 5, 2014 |DAILY BEAST 

Are there larger lessons to be learned from this whole sordid tale? Somaly Mam And The Cult of Glamourized Victimhood |Amanda Marcotte |May 29, 2014 |DAILY BEAST 

When shall fond woman cease to give—when shall mean and sordid man be satisfied with something less than all she has to grant? Blackwood's Edinburgh Magazine, No. CCCXXXIX. January, 1844. Vol. LV. |Various 

The expression fitted best the cruder, more sordid method of gaining possession of this woman. Bella Donna |Robert Hichens 

By the light of the sordid knowledge that she had revealed to him he paid her back full tale. Kipling Stories and Poems Every Child Should Know, Book II |Rudyard Kipling 

With this political subjection one is reluctant to associate a more sordid kind of obligation. King Robert the Bruce |A. F. Murison 

It was amid such sordid troubles that Jess evolved the idea for her play. The Girls of Central High on the Stage |Gertrude W. Morrison