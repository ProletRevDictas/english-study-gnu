Simply put, it’s the range where the most successful women in tennis reside. How Serena Williams Could Finally Break The Grand Slam Record |Amy Lundy |February 10, 2021 |FiveThirtyEight 

A former Obama administration official, he resides with his husband in the Logan Circle neighborhood. Celebrate first lady’s visit to Whitman-Walker, then act |Ted Miller |February 4, 2021 |Washington Blade 

The number could actually be much higher because the “30,000 who have left the Republican Party reside in just a few states that report voter registration data, and information about voters switching between parties, on a weekly basis.” Shrinking the GOP, one state at a time |Jennifer Rubin |February 2, 2021 |Washington Post 

In northwest Amazonia reside 20 or so tribes, or language groups, known collectively as the Tukanoans. Why a Universal Society Is Unattainable - Issue 95: Escape |Mark W. Moffett |January 14, 2021 |Nautilus 

Laura Walsh resides in Ocean Beach and is the policy coordinator for the Surfrider Foundation’s San Diego County chapter. Seawalls Are Not the Answer to Coastal Bluff Erosion |Laura Walsh |January 11, 2021 |Voice of San Diego 

The more than 50,000 who reside in West Point live mostly in shacks made of zinc with rusted tin roofs. Meet the Liberian Girls Beating Ebola |Abby Haglage |October 29, 2014 |DAILY BEAST 

McCain said that ISIS held American-made Howitzer cannons had come within range of Erbil, where U.S. personnel reside. After Underestimating ISIS, Obama Scrambles for Plan to Defeat Them |Josh Rogin |August 9, 2014 |DAILY BEAST 

The vehicles were approaching Erbil, the capital city of Iraqi Kurdistan, were many U.S. diplomatic and military personnel reside. McCain Calls Obama's 'Pinprick' Iraq Strikes 'Meaningless' and 'Almost Worse Than Nothing' |Josh Rogin |August 8, 2014 |DAILY BEAST 

The Registry regularly produces tables of how many people officially reside in each settlement. Inventing Settlers to Scuttle Peace Talks |Gershom Gorenberg |July 29, 2013 |DAILY BEAST 

They reside in countries as diverse as Norway, Jordan, Honduras, Singapore, Denmark, and Brazil. Are You Reading My Emails? Former State Dept. Official Asks the NSA. |John Kael Weston |July 23, 2013 |DAILY BEAST 

We will suppose a case for argument: In this city reside, two colored families, of three sons and three daughters each. The Condition, Elevation, Emigration, and Destiny of the Colored People of the United States |Martin R. Delany 

Yet sheep-folds imply that their Arab owners would occasionally reside near its ruins. Gospel Philosophy |J. H. Ward 

She was a niece of William Pitt, and abandoned civilized society to reside among Arabs, over whom she acquired great command. The Every Day Book of History and Chronology |Joel Munsell 

Both deities are said to have continued to reside in these waters down to the present day. A Woman's Journey Round the World |Ida Pfeiffer 

I also give and bequeath to my beloved wife the dwelling-house and lot on which I now reside. Putnam's Handy Law Book for the Layman |Albert Sidney Bolles