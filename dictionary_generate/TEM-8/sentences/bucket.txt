While McInnes’ chopper carries its water in a bucket, that’s not the only way to do it. How aerial firefighters battle blazes from the skies |Rob Verger |August 27, 2020 |Popular-Science 

Kobe and Shaq butted heads, did their own things, got buckets and won three rings. LeBron And AD Dominate Like Kobe And Shaq. But Can They Win Like The Classic Lakers? |Robert O'Connell |August 24, 2020 |FiveThirtyEight 

These are in the low-touch, high-growth bucket which is a good thing for us. ‘It’s less dire than it seemed to be’: How The Wall Street Journal’s digital ads business has weathered the downturn |Lucinda Southern |August 20, 2020 |Digiday 

A centrifuge simulates gravity through centri­fugal force — the effect that keeps water in the bottom of a bucket when you swing it over your head. What will astronauts need to survive the dangerous journey to Mars? |Maria Temming |July 15, 2020 |Science News 

Another problem in this third bucket — it’s a big bucket — is when the person who designed the intervention and masterminded the initial trial can no longer be so involved once the program scales up to multiple locations. Policymaking Is Not a Science (Yet) (Ep. 405) |Stephen J. Dubner |February 13, 2020 |Freakonomics 

An 18-year-old Swedish rapper/Internet meme has inspired legions of impressionable teens to get based in bucket hats. The Cult of Yung Lean: ‘I’m Building An Anarchistic Society From the Ground Up’ |Marlow Stern |January 4, 2015 |DAILY BEAST 

Early one morning I was passing out hot water, when a man showed me a bucket of blood from his slashed wrists and asked for help. A Million Ways to Die in Prison |Daniel Genis |December 8, 2014 |DAILY BEAST 

Her solution: a bucket list of influential people and places to visit and photograph. Annie Leibovitz Talks About ‘Pilgrimage,’ Susan Sontag, Vogue & More |Justin Jones |November 20, 2014 |DAILY BEAST 

Somehow, their message has gone from lunch-bucket concerns to a date with Girls. Earth to DNC: Dyspeptic Dad Still Votes, Too |Lloyd Green |November 11, 2014 |DAILY BEAST 

It was on a hike to the Grand Canyon at age 18 that Shattuck penned her first bucket list. From Baltimore Ravens Cheerleader to Mrs. Robinson |Brandy Zadrozny |November 6, 2014 |DAILY BEAST 

A fellow was dropt down in the bucket, and soon bawled out from the bottom, "I have found the punch-ladle, so wind me up." The Book of Anecdotes and Budget of Fun; |Various 

A broken broom, covered with very ancient cobwebs, lay under one manger, and the remnants of a stable-bucket under another. The Portsmouth Road and Its Tributaries |Charles G. Harper 

When the bucket came up full of water, the top was all yellow with dandelions. The Nursery, August 1873, Vol. XIV. No. 2 |Various 

If he has stolen a watering bucket or a harrow, he shall pay three shekels of silver. The Oldest Code of Laws in the World |Hammurabi, King of Babylon 

With a bucket of water and a broomstick he beat out the fire, and went for a run to warm up. Love's Pilgrimage |Upton Sinclair