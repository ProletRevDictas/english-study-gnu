The startup is coming out of “stealth mode” today but it has been around for a couple of years already and has signed on a number of large healthcare companies as customers — for example, the wholesale drug giant AmerisourceBergen. Infinitus emerges from stealth with $21.4M for ‘voice RPA’ aimed at healthcare companies |Ingrid Lunden |February 4, 2021 |TechCrunch 

That’s why we recently stopped our wholesale business and moved to smaller collections. The TIME 2030 Committee Offers 8 Solutions for a More Equitable and Sustainable Future |TIME Staff |January 22, 2021 |Time 

They have also asked for a wholesale change to how we provide benefits to seasonal employees—an approach that has been in place at the resort, and across our industry, for decades. Why Ski Patrollers Are Picketing at Two Vail Resorts |Scott Yorko |January 20, 2021 |Outside Online 

Our factory has the capacity to produce up to 7 million masks a week, helping us keep up with the demand of our direct-to-consumer, wholesale, and collaborations projects. This female-founded face mask brand had a wait-list of more than 40,000 customers |Rachel King |January 17, 2021 |Fortune 

Kriegsman’s startup initially launched as a retail-as-a-service company, before shuttering its physical stores in 2019 to focus on its online marketplace, which helps broker wholesale orders between brands and retailers. Despite hungry VCs, DTC brands are rethinking their fundraising approach |Anna Hensel |January 8, 2021 |Digiday 

To be sure, the election was not an embrace of the GOP platform, but it was a wholesale rejection of Barack Obama. Earth to DNC: Dyspeptic Dad Still Votes, Too |Lloyd Green |November 11, 2014 |DAILY BEAST 

In March the Colorado native, who sells wholesale farm products, decided to purchase an abandoned jail in his hometown of Brush. Colorado Man Buys Former Prison, Hoping to Transform It Into a Pot Factory |Abby Haglage |July 31, 2014 |DAILY BEAST 

Crown was working at a company called Valley Wholesale until he was recently laid off. Idaho Woman Who Gave Birth on Highway: ‘I Had to Pull My Pants Down to Get the Baby Out’ |Dale Eisinger |July 10, 2014 |DAILY BEAST 

Laughing at the wholesale denial of free speech is not the voice of moderation. How America’s Nuclear Deal Sold Out Iran’s Liberals |David Keyes & Ahmad Batebi |December 3, 2013 |DAILY BEAST 

Michael Kors Sues Costco: Michael Kors is suing wholesale warehouse club Costco for false advertising. A.P.C. Releases Kanye West Collection; Max Azria Faces Insurmountable Debt |The Fashion Beast Team |July 15, 2013 |DAILY BEAST 

The money went into the pockets of the Admiralty clerks and paymasters, who thrived on wholesale and shameless peculation. The Portsmouth Road and Its Tributaries |Charles G. Harper 

Again he got up and went inside, where he wrote a letter to a certain wholesale house with whom his account was large. Scattergood Baines |Clarence Budington Kelland 

This woman was married in 1821, by Jacques Collin's sister, to the head clerk of a rich, wholesale hardware merchant. Repertory Of The Comedie Humaine, Complete, A -- Z |Anatole Cerfberr and Jules Franois Christophe 

This is the broadest street in London and was opened by wholesale condemnation of private property. British Highways And Byways From A Motor Car |Thomas D. Murphy 

Nevertheless, the fisheries supply additional employment to processing and wholesale employees. Hallowed Heritage: The Life of Virginia |Dorothy M. Torpey