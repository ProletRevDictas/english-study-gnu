After the visit in Israel and the UAE last week, Kushner and his delegation traveled to Bahrain. Behind the scenes of the U.S.-brokered Israel-Bahrain agreement |Barak Ravid |September 11, 2020 |Axios 

I polled the members of San Diego’s state delegation on their desire for a special session. Sacramento Report: Bipartisan Support for a Special Session |Sara Libby |September 11, 2020 |Voice of San Diego 

In fact, none of the previous three attorneys general took a monthlong leave that required the delegation of their authority to another state employee, according to the Department of Law. Alaska’s Attorney General on Unpaid Leave After Sending Hundreds of “Uncomfortable” Texts to a Young Colleague |by Kyle Hopkins, Anchorage Daily News |August 25, 2020 |ProPublica 

Their intention was to highlight the injustice and illegitimacy of the official delegation, and they hoped to take their places on the convention floor. Why Conventions Still Matter |Julia Azari |August 17, 2020 |FiveThirtyEight 

School leaders pushed their local legislative delegations to restore the money. How Los Angeles and San Diego Unified Started Driving State Education Policy |Will Huntsberry |July 29, 2020 |Voice of San Diego 

It was seen by a small delegation of star-struck prelates and dignitaries who later described the film as “moving.” Pope Francis Has the Pleasure of Meeting Angelina Jolie for a Few Seconds |Barbie Latza Nadeau |January 8, 2015 |DAILY BEAST 

But officials gave the delegation no specific commitments and made no concrete promises of increased U.S. assistance. Yazidis Face Genocide by ISIS After U.S. Turns Away |Josh Rogin |November 4, 2014 |DAILY BEAST 

The legal question is whether this constitutes an unconstitutional delegation of legislative power to a private entity. The Supreme Court Is Weighing Corporate Power Yet Again |Zephyr Teachout |October 17, 2014 |DAILY BEAST 

There could only be one Wisconsin delegation, so the Republican National Committee would have to choose between the two factions. The GOP’s Last Identity Crisis Remade U.S. Politics |Michael Wolraich |July 24, 2014 |DAILY BEAST 

A few days later, a Half-Breed delegation arrived in Washington, begging for his assistance. The GOP’s Last Identity Crisis Remade U.S. Politics |Michael Wolraich |July 24, 2014 |DAILY BEAST 

Sir, a consumer's delegation wishes to speak with you about the new Birthday Quotas. The Great Potlatch Riots |Allen Kim Lang 

"Hand these around that delegation, Soldier," he said, shoving a stack of Schedules 1219B across his desk toward the girl. The Great Potlatch Riots |Allen Kim Lang 

Urged by a delegation of music-loving consumers, the tubist raised his ravaged horn. The Great Potlatch Riots |Allen Kim Lang 

When I got home from the village a couple of evenings ago a bareheaded delegation met me at the road gate with bad news. The Red Cow and Her Friends |Peter McArthur 

Shortly after the house was opened a delegation of boys appeared asking for the use of the large room for a boys' club. The Leaven in a Great City |Lillian William Betts