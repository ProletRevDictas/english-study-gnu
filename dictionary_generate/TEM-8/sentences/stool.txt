Since the first weeks of the pandemic, however, scientists in China have said infectious virus in the stool of patients may also play a role in transmission. COVID-19 infection lingers in the gut, even after it clears the respiratory system, researchers say |Claire Zillman, reporter |September 8, 2020 |Fortune 

A February study of 73 patients hospitalized with the coronavirus in China’s Guangdong province found more than half tested positive for the virus in their stool. COVID-19 infection lingers in the gut, even after it clears the respiratory system, researchers say |Claire Zillman, reporter |September 8, 2020 |Fortune 

The Hong Kong scientists studied stool samples from 15 patients to better understand the virus’s activity in the gastrointestinal tract. COVID-19 infection lingers in the gut, even after it clears the respiratory system, researchers say |Claire Zillman, reporter |September 8, 2020 |Fortune 

I needed to know more about the woman behind the corn stool, so I emailed Abi Crompton, the creative director and founder of Third Drawer. Summer Is Forever With This Corn-on-the-Cob Chair |Emma Orlow |August 28, 2020 |Eater 

I love how the stool features a big bite of kernels missing, suggesting that it is almost too delicious to not purchase. Summer Is Forever With This Corn-on-the-Cob Chair |Emma Orlow |August 28, 2020 |Eater 

He noticed her in the crowd while he was sitting on his stool between rounds. The Life and Hard Times Of The Family A Cuban Defector Left Behind |Brin-Jonathan Butler |December 19, 2014 |DAILY BEAST 

Furthermore, a person with norovirus has about 70 billion viral particles per gram of stool. A Doctor Explains Why Cruise Ships Should Be Banned |Kent Sepkowitz |November 19, 2014 |DAILY BEAST 

I try to catch the eye of this third boy, but he plops down onto a stool and avoids my gaze. Magical Gardens for the Blind, Deaf, and Disabled |Elizabeth Picciuto |October 22, 2014 |DAILY BEAST 

Long wisps fall across her forehead as she sits very straight on her stool, her narrow shoulder blades drawn back elegantly. The Stacks: The Searing Story of How Murder Stalked a Tiny New York Town |E. Jean Carroll |April 19, 2014 |DAILY BEAST 

Everman had his last drink and left for the night when a friend grabbed me by the arm, yanking me off my stool. He Left Nirvana Because He Had Cooler Things to Do. Like Going to Iraq. |Jacob Siegel |April 12, 2014 |DAILY BEAST 

Tony's stool was nearer to the bass keys of the piano, while the sofa Lettice lay upon had certainly been drawn up towards him. The Wave |Algernon Blackwood 

If, now, the patient cough or strain as if at stool, the contents of the stomach will usually be forced out through the tube. A Manual of Clinical Diagnosis |James Campbell Todd 

When bleeding piles are absent, blood-streaks upon such a stool point to carcinoma. A Manual of Clinical Diagnosis |James Campbell Todd 

When the mucus is small in amount and intimately mixed with the stool, the trouble is probably in the small intestine. A Manual of Clinical Diagnosis |James Campbell Todd 

A Gram-positive stool due to cocci is suggestive of intestinal ulceration. A Manual of Clinical Diagnosis |James Campbell Todd