This issue of underutilized existing housing is complex and there’s not much hard data, so it’s a natural fit for us to work on bringing clarity and new ideas to this conversation. Vacancy Tax Study Is Giving City Officials Déjà Vu |Lisa Halverstadt and Andrew Keatts |February 10, 2021 |Voice of San Diego 

The most complex global and societal issues can be broken down into smaller solvable tech challenges. The rise of the activist developer |Walter Thompson |February 9, 2021 |TechCrunch 

Throughout the game Mono navigates disturbing environments — like drab apartment complexes or a hospital overrun with sentient mannequins — each filled with furniture two sizes too large. ‘Little Nightmares II’ made me dread every moment. And I loved it. |Elise Favis |February 9, 2021 |Washington Post 

“It’s not just proteins, it’s a complex, emotional product,” says Aleph chief executive Didier Toubia. Raising the steaks: First 3-D-printed rib-eye is unveiled |Laura Reiley |February 9, 2021 |Washington Post 

Per the agreement, organizations are limited to 75 players and 75 staff at their complexes, and are encouraged to spread them out as much as possible. MLB, players’ union agree to health and safety protocols, clearing way for spring training to start |Chelsea Janes |February 9, 2021 |Washington Post 

It was a complex task they were asked to do, and every cultural and experiential advantage would be required. Why Did We Panic After 9/11 and Ignore All We Knew About Responding to Security Threats? |Deborah Pearlstein |December 18, 2014 |DAILY BEAST 

However, their presence shows that Mars could have a more complex and evolving chemical story. Methane on Mars: Life or Just Gas? |Matthew R. Francis |December 17, 2014 |DAILY BEAST 

KSM enters the complex through a “Sally Port,” a series of gates designed to allow just one vehicle in at a time. 9/11 Mastermind Is Afraid of the Ladies |Tim Mak |December 16, 2014 |DAILY BEAST 

We are a huge, complex, diverse country still offering freedom, opportunity and hope. Dick Cheney vs. ‘Unbroken’ |Mike Barnicle |December 15, 2014 |DAILY BEAST 

If Congress struggles to keep the lights on, how could it deal with issues as complex as police brutality? Sharpton Recalls Civil Rights Struggle in DC March Against Police Violence |Ben Jacobs |December 13, 2014 |DAILY BEAST 

The act of the Covenanting Society is complex, and is the aggregate of the actings of all who compose it. The Ordinance of Covenanting |John Cunningham 

He knows when a sentiment is simple and when it is complex, when the heart is a dupe of the mind and when of the senses. Repertory Of The Comedie Humaine, Complete, A -- Z |Anatole Cerfberr and Jules Franois Christophe 

The solution of the problem, if complex in all clinical affections, is especially so in epilepsy. A Statistical Inquiry Into the Nature and Treatment of Epilepsy |Alexander Hughes Bennett 

Bud did not say anything; your efficient chauffeur reserves his eloquence for something more complex than a dead engine. Cabin Fever |B. M. Bower 

It is the most complex of all, as the lines contain internal rimes. Chaucer's Works, Volume 1 (of 7) -- Romaunt of the Rose; Minor Poems |Geoffrey Chaucer