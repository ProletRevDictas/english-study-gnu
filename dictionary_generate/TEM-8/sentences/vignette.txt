Probably the most charming movie of the year, The Truffle Hunters unfolds as a series of vignettes documenting the lives of several older men and their dogs. 11 compelling documentaries to watch for this fall |Alissa Wilkinson |October 9, 2020 |Vox 

The film dramatizes the runaway consequences of this profit strategy—ranging from mental health issues to ideological radicalization—with periodic vignettes of a fictional family struggling to navigate their digital landscape. Does Social Media Poison Everything? - Facts So Romantic |Scott Koenig |October 6, 2020 |Nautilus 

For example, it declines to acknowledge that fake news is disproportionately shared by people who are older and more conservative, and the dramatized vignettes about polarization feature a vague movement called the “extreme center.” Does Social Media Poison Everything? - Facts So Romantic |Scott Koenig |October 6, 2020 |Nautilus 

Eve Harrington’s origins story is a humble tale of hardscrabble survival, anchored by vignettes of farm life in Wisconsin, a grueling stint as a secretary in a brewery, and an ill-starred marriage to a now-perished war-hero. ‘All About Eve’ at 70 |Tom Joudrey |September 25, 2020 |Washington Blade 

In reality, most home offices are less picture-perfect, despite what design catalogs or enviable vignettes on Instagram suggest. A peek inside home offices around the world |Anne Quito |September 20, 2020 |Quartz 

The healthcare vignette provides us a textbook example of how the GOP has retreated into policy fantasyland. When Conservatives Cry Wolf |Michael Tomasky |February 10, 2014 |DAILY BEAST 

The costumes and settings are worthy of a full-length feature, and the creepy possessiveness of the song adds to the vignette. Arctic Monkeys, Foxes & More Best Music Videos of the Week (VIDEO) |Victoria Kezra |August 16, 2013 |DAILY BEAST 

Rereading that review I linked to above, I opened it with a vignette that is still clear as a bell in my mind's eye. Jack Germond and the Old Days |Michael Tomasky |August 14, 2013 |DAILY BEAST 

In the second act, a trio of ballet dancers from the New York City Ballet will appear in a vignette dedicated to cotton candy. Inside Will Cotton's Candy World |Isabel Wilkinson |November 2, 2011 |DAILY BEAST 

Yet another vignette has the Hope-wrapped Bündchen breaking the news that the mother-in-law is moving in. Too Sexy for Brazil |Mac Margolis |October 6, 2011 |DAILY BEAST 

The magazines sketch us a lively article, the newspapers vignette us, step by step, a royal tour. Blackwood's Edinburgh Magazine, No. CCCXXXIX. January, 1844. Vol. LV. |Various 

In memory of the late lamented general the present five-peso bank notes bear his vignette. The Philippine Islands |John Foreman 

See also the vignette on title page, copied from an alabaster slab in the Collegio Romano, originally from the Catacombs. The Catacombs of Rome |William Henry Withrow 

He inquired in what style I wished to be taken, whether full-length, half-length, or vignette. ' The Talking Horse |F. Anstey 

In some instances they partake much more of the character of a vignette than a tradesmans mark. Printers' Marks |William Roberts