However the talks which led to the joint statement on establishing full diplomatic relations took just 29 days. Behind the scenes of the U.S.-brokered Israel-Bahrain agreement |Barak Ravid |September 11, 2020 |Axios 

The United States normalizes diplomatic relations with China. A brief history of US-China espionage entanglements |Konstantin Kakaes |September 3, 2020 |MIT Technology Review 

On Monday, Kushner told reporters that establishing diplomatic ties with Israel was in Saudi Arabia’s interests, even though such public comments only discomfit Riyadh. Butterfly Effect: In a Trump 2.0, MBZ Could Be the New MBS |Charu Kasturi |August 20, 2020 |Ozy 

Since then, an internal struggle has been under way for control of the country — a struggle that has turned into a proxy war involving Russia and Turkey, with military, financial or diplomatic support divided among at least seven other countries. Turning a Blind Eye Internationally Will Cost Us |Tracy Moran |August 7, 2020 |Ozy 

Among these campaigns, the Sixth Crusade is noteworthy because it actually allowed the namesake Kingdom of Jerusalem to retain control of significant parts of Jerusalem for 15 years through diplomatic efforts, as opposed to military successes. History of the Crusades: Origins, Politics, and Crusaders |Dattatreya Mandal |March 23, 2020 |Realm of History 

“People are generally diplomatic,” says Steinbrick of regulars dealing with the surge of new faces. How to Survive the New Year ‘Gympocalypse’ |Tim Teeman |January 6, 2015 |DAILY BEAST 

Fred Logevall at Cornell won the Pulitzer Prize and is a diplomatic historian; he just started a book on Kennedy. Thank Congress, Not LBJ for Great Society |Julian Zelizer, Scott Porch |January 4, 2015 |DAILY BEAST 

Right now it looks like the diplomatic equivalent of one hand clapping. Obama’s One Hand Clap With Castro |Doug McIntyre |December 24, 2014 |DAILY BEAST 

President Obama defends his decision to normalize ties with Cuba and defends his diplomatic record. Fact-Checking the Sunday Shows: Dec 21 |PunditFact.com |December 21, 2014 |DAILY BEAST 

Given the potential for a cyber tit-for-tat to escalate, Obama has even more incentive to find a diplomatic solution. Obama Could Hit China to Punish North Korea |Shane Harris, Tim Mak |December 20, 2014 |DAILY BEAST 

Uncle David had none of that small diplomatic genius that helps to make a good attorney. Checkmate |Joseph Sheridan Le Fanu 

The box of the diplomatic corps was just opposite us, and our gay little Mrs. F. sat in it dressed in white satin. Music-Study in Germany |Amy Fay 

The diplomatic section shall negotiate with the foreign cabinets the recognition of belligerency and Philippine independence. The Philippine Islands |John Foreman 

This pretty, resolute, sharp girl had suddenly become an important piece in the great game of diplomatic chess. The Weight of the Crown |Fred M. White 

The different motions or interferences of the members of the diplomatic body scarcely concern this period. Journal of a Voyage to Brazil |Maria Graham