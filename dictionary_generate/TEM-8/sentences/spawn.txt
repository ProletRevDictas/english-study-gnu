There is a misconception among locals that the small masu found in winter—which they call pestrushka— are a different species entirely from the larger fish—called sima— that come in summer to spawn. The quest to snare—and save—the world’s largest owl |Jonathan Slaght |August 28, 2020 |Popular-Science 

The brand took off, becoming a poster child of the e-commerce movement that would spawn many a “Warby Parker of X”s. Warby Parker raises $245 million for a $3 billion valuation |Lucinda Shen |August 27, 2020 |Fortune 

Since his Carnegie Mellon team won the DARPA Urban Challenge in 2007 — the race that spawned today’s self-driving movement — he has been one of the industry’s central players, first co-founding Google’s self-driving unit and later leading it. Last Unicorn Standing: Can This Self-Driving Innovator Survive? |Charu Kasturi |August 27, 2020 |Ozy 

Now, these communities are counting on art to keep their economies going amid the crisis spawned by the coronavirus pandemic. Can the Arts Save Rural America From the Recession? |Charu Kasturi |August 16, 2020 |Ozy 

In recent years, she adds, Tbilisi has spawned a new crop of free-thinking youngsters planning their own startups. A Pandemic Tourism Pivot From Cool to Wellness |Dan Peleschuk |August 11, 2020 |Ozy 

Well known for his gimmicks, Daylyt entered the stage in a Spawn costume that could stop traffic at Comic Con. America’s Poets: Battle Rap Gets Real |Rich Goldstein |July 15, 2014 |DAILY BEAST 

So why is it that this new flurry of celebrity spawn are able to create their own identities while so many others have failed? The Famous Parents Modeling Club |Erin Cunningham |May 28, 2014 |DAILY BEAST 

The same, however, historically cannot be said for famous spawn turned high-fashion models. The Famous Parents Modeling Club |Erin Cunningham |May 28, 2014 |DAILY BEAST 

That is not to say that the focus on Cohle and Hart does not spawn two compelling performances. ‘True Detective,’ Obsessive-Compulsive Noir, and ‘Twin Peaks’ |Jimmy So |March 14, 2014 |DAILY BEAST 

Replaying sections will spawn enemies in the same place, but what happens then changes from time to time. ‘Killzone: Shadow Fall’ Review: Oh My God, This PlayStation 4 Game Is Beautiful |Alec Kubas-Meyer |November 19, 2013 |DAILY BEAST 

They run up into fresh water to spawn, and in the process are scooped out by the basket-load. The Cradle of Mankind |W.A. Wigram 

Those in that trough right behind you are just hatching, they're from the first batch of spawn in the early spring run. The Boy With the U. S. Fisheries |Francis Rolt-Wheeler 

I opened a big-bellied one indeed, and found it full of spawn. The Natural History of Selborne, Vol. 1 |Gilbert White 

The salmon go over a hundred miles up to the McCloud River to spawn, and will jump or leap up small falls or rapids in their way. Stories of California |Ella M. Sexton 

Worse than all, too, the common trout deteriorated, for they had fed on the spawn of the salmo eriox. The Rivers of Great Britain: Rivers of the East Coast |Various