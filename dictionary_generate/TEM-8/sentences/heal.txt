They haven’t even begun to pull the knife out, much less heal the wound. How Laurence Fishburne Gave Voice To ‘The Autobiography Of Malcolm X’ |Joi-Marie McKenzie |September 17, 2020 |Essence.com 

If the injury led to death, the bones would not have healed. Women like Mulan didn’t need to go to war in disguise |Bethany Brookshire |September 4, 2020 |Science News For Students 

We need to do a lot better job listening to what’s going on and how we can be helpful and heal. Full Transcript: Sean Spicer on ‘The Carlos Watson Show’ |Daniel Malloy |August 26, 2020 |Ozy 

While it still may have been possible for Nurkić to return had the 2019-20 season continued apace, the coronavirus-induced suspension of play gave him even more time to heal and work himself back into shape. With A Healthy Jusuf Nurkić, The Trail Blazers Are On The Cusp Of The Playoffs |Jared Dubin |August 11, 2020 |FiveThirtyEight 

However, despite the fact the Grizzlies will likely get Justise Winslow back on the floor now that his back injury has had time to heal, FiveThirtyEight is pessimistic about their chances of holding onto their spot. Who’s Who In The NBA Bubble: The Teams Just Along For The Ride |Jared Dubin |July 20, 2020 |FiveThirtyEight 

To break her self-destructive cycle and heal, she decides to hike 1,100 miles of the Pacific Crest Trail solo. Exclusive: The Making of Reese Witherspoon’s Golden Globe-Nominated ‘Wild’ |Marlow Stern |December 12, 2014 |DAILY BEAST 

We had decided we would make a film together because we both agreed about the importance of art as a way to heal. Blogger Shares and Shames Cancer in ‘Lily’ |Amy Grantham |December 9, 2014 |DAILY BEAST 

Heal STL was destroyed, as were several other shops and offices contained in the building that went up in smoke Monday night. The Baptism of Michael Brown Sr. and Ferguson’s Baptism by Fire |Justin Glawe |November 27, 2014 |DAILY BEAST 

Then Heal STL was burned down Monday like a moribund body for cremation. The Baptism of Michael Brown Sr. and Ferguson’s Baptism by Fire |Justin Glawe |November 27, 2014 |DAILY BEAST 

A bunch of old, white, rock titans come together with young, white, X Factor hotties to persuade Britain to heal Africa. Do They Know It’s Time to Stop Band Aid? |Tom Sykes |November 22, 2014 |DAILY BEAST 

One day as Mrs. Worthington stood beside her child she felt that God wanted to heal her. The value of a praying mother |Isabel C. Byrum 

He sat stunned before the amazing revelation of how little time and distance had done to heal his hurt. Cabin Fever |B. M. Bower 

Rest in our quiet family affection will soon heal you of this fever, for it is nothing else. Camille (La Dame aux Camilias) |Alexandre Dumas, fils 

Enter my service, and you shall be rich and happy; my favors shall heal those wounded limbs. Catherine de' Medici |Honore de Balzac 

Those are happiest whose wounds heal soonest,—to whom a life-time grief is unknown. Alone |Marion Harland