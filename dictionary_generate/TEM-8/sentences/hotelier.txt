The former hotelier became an opposition leader in exile and a thorn in Kagame’s side. She escaped the genocide in Rwanda. Now, 27 years later, she can’t escape its politics. |Petula Dvorak |February 25, 2021 |Washington Post 

At the end of the film, as the hotelier, his wife and son are rescued and about to be evacuated to Belgium, they find the orphaned Kanimba and her younger sister in a refugee camp and adopt them. She escaped the genocide in Rwanda. Now, 27 years later, she can’t escape its politics. |Petula Dvorak |February 25, 2021 |Washington Post 

Loaning money to shopkeepers, landlords and hoteliers in places such as Times Square or SoHo used to be considered almost a sure thing. Mounting commercial real estate losses threaten banks, recovery |David Lynch |November 11, 2020 |Washington Post 

Rate Spotlight is the pricing intelligence tool that enables hoteliers to understand how their rates compare to competitors’ in their markets. Tripadvisor launches new listings, reputation and data products to help hotels and restaurants gain insights, boost visibility |Greg Sterling |October 29, 2020 |Search Engine Land 

Shmueli Levin, a hotelier from Davos, Switzerland, learned about Elli’s Kosher Kitchen last July. The Newest Fusion Cuisine: Kosherati |Fiona Zublin |September 16, 2020 |Ozy 

The principals of design firm Roman and Williams discuss their collaboration with the late hotelier. Ace Hotel Founder Alex Calderwood’s Greatest Legacy |Jessica Dawson |November 20, 2013 |DAILY BEAST 

The hotelier alerted authorities who sent a Coast Guard ship to meet the incoming boat. Italy’s Shipwrecked Syrians Fare Better Than Most Migrants |Barbie Latza Nadeau |October 3, 2013 |DAILY BEAST 

But focusing on in-store experience—and bringing in a French hotelier to fix it—misses the forest for the trees. Why Best Buy Is Tanking |Alex Klein |August 21, 2012 |DAILY BEAST 

Gaza City hotelier Jawdat Al Khodary said much the same thing. The State of Gaza |Kathleen Peratis |May 15, 2012 |DAILY BEAST