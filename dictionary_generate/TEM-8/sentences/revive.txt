The vivid footage of the assault on the Capitol revived “horrible memories,” Daines said later. Republican senators show emotion, but little evidence of changed minds |Seung Min Kim, Karoun Demirjian |February 11, 2021 |Washington Post 

Another rare earths company may soon be going public in the US, presenting new opportunities for private investment in the domestic critical metals industry—a sector that Washington has signaled strong interest in reviving. The US is taking steps towards breaking China’s rare earths monopoly |Mary Hui |February 5, 2021 |Quartz 

EA Sports announced Tuesday that it is reviving its college football video game series, a beloved franchise that was discontinued in 2013 after EA and the NCAA were taken to court over the unpaid use of player likenesses. EA Sports revives college football franchise as courts mull NCAA’s stance on amateurism |Mike Hume, Rick Maese |February 2, 2021 |Washington Post 

Experts hope this will revive ecosystems and safeguard the diversity of Earth’s species. Can people protect as much space as nature needs? |Jonathan Lambert |January 21, 2021 |Science News For Students 

Axios went as far as to name “reviving local journalism” as one of the 10 promises to readers that CEO Jim VandeHei made earlier this month in the company’s new Bill of Rights. ‘Connect the dots’: Why publishers are investing in local media to round out big national stories |Kayleigh Barber |January 20, 2021 |Digiday 

Scholar-activists Larry Lessig and Zephyr Teachout have recently been working to revive it. Undo Citizens United? We’d Only Scratch the Surface |Jedediah Purdy |November 12, 2014 |DAILY BEAST 

Mamoon and his second wife, Liana, hope it will revive his reputation, and “prompt the reissuing of his books in forty languages.” A Novel About a Novelist ‘Like’ Naipaul |Edward Platt |November 6, 2014 |DAILY BEAST 

A great chef who has fought to revive the old spirit says he fears history may repeat itself. In War-Torn Ukraine, Savva Libkin's Delicious Recipes for Survival |Anna Nemtsova |August 12, 2014 |DAILY BEAST 

My friends, hurting from a night of rum-infused revelry, opt for Revive. The I.V. Doc Comes to Your House, Fights Hangovers, and Wins |Abby Haglage |July 20, 2014 |DAILY BEAST 

I ordered Revive and now I am about to close my second deal today! The I.V. Doc Comes to Your House, Fights Hangovers, and Wins |Abby Haglage |July 20, 2014 |DAILY BEAST 

HE ordered a lunch which he thought the girl would like, with wine to revive the faculties that he knew must be failing. Rosemary in Search of a Father |C. N. Williamson 

First Impressions are usually vivid but the power to revive them is weak—a poor memory. Assimilative Memory |Marcus Dwight Larrowe (AKA Prof. A. Loisette) 

First Impressions are usually weak but the power to revive them is strong—still a poor memory. Assimilative Memory |Marcus Dwight Larrowe (AKA Prof. A. Loisette) 

First Impressions on all subjects are strong and the power to revive them is strong—a first-class memory. Assimilative Memory |Marcus Dwight Larrowe (AKA Prof. A. Loisette) 

Thus the facts help us devise the number phrase, and the phrase helps revive the facts. Assimilative Memory |Marcus Dwight Larrowe (AKA Prof. A. Loisette)