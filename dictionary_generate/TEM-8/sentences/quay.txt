There, right on the quay on lot 36, were ship’s Captain Joseph Lazou and Marie Louise Balivet, passenger number 178. Deported From France as Convicts, These Women Helped Build New Orleans |Joan DeJean |April 26, 2022 |Time 

As Chan’s co-presenter, Quay, put it in his presentation, which covered similar ground, no “innocent” virologist would commit such oversights. They called it a conspiracy theory. But Alina Chan tweeted life into the idea that the virus came from a lab. |Antonio Regalado |June 25, 2021 |MIT Technology Review 

Steve Garth, who works in Circular Quay, was inside the Cartier jewelry store near the café when the siege began. Jihadi Siege in Sydney Ends in Gunfight |Courtney Subramanian, Lennox Samuels, Chris Allbritton |December 15, 2014 |DAILY BEAST 

The Mob ran wild, using the local Teamsters to run casinos in Las Vegas and bombing buildings along the River Quay. Those Kansas City Blues: A Family History |Katie Baker |October 24, 2014 |DAILY BEAST 

On my last day but one I crossed to the Giudecca and ran into him on the quay. My Biennale Favorites |Anthony Haden-Guest |June 8, 2009 |DAILY BEAST 

In 1634 he also prohibited the landing of tobacco any where except at the quay near the custom house in London. Tobacco; Its History, Varieties, Culture, Manufacture and Commerce |E. R. Billings. 

It also authorised the construction and maintenance, as p. 150part of such railways, of any pier, quay or jetty. Fifty Years of Railway Life in England, Scotland and Ireland |Joseph Tatlow 

When Skipper Worse reached the market quay he met with a sad disappointment. Skipper Worse |Alexander Lange Kielland 

In 1821 and the years following, one of them ran a little shop on the quay des Grands-Augustins, and purchased Lousteau's books. Repertory Of The Comedie Humaine, Complete, A -- Z |Anatole Cerfberr and Jules Franois Christophe 

The destroyers were still coaling, and a small cargo was being taken off the boat at the quay. The Amazing Interlude |Mary Roberts Rinehart