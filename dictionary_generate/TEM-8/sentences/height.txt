A surfer cares about wave heights, not the jostling of water molecules. How Mathematical ‘Hocus-Pocus’ Saved Particle Physics |Charlie Wood |September 17, 2020 |Quanta Magazine 

The difference between the ice cube and the final tower height was zero every time. Build ice towers with bottled water and ice |Bethany Brookshire |September 16, 2020 |Science News For Students 

The plan would raise the height limit not just for the Sports Arena area but the entire Midway corridor down to Old Town. Politics Report: Forged Footnote 15 |Scott Lewis and Andrew Keatts |September 12, 2020 |Voice of San Diego 

Falling ad prices on TV earlier this spring amid the height of the coronavirus crisis caught the attention of a number of direct-to-consumer brands, as previously reported by Digiday. ‘Significant shift’: With a new national TV spot, Zocdoc is changing its advertising strategy to be more offline |Kristina Monllos |September 10, 2020 |Digiday 

The last time it reached such heights was January 2018, when a massive volatility shock sent the index into a correction. The stock market had its worst day in months, but no one is quite sure why |Dion Rabouin |September 4, 2020 |Axios 

This is a Hollywood director at the height of his powers creating original, wildly ambitious epics. Oscars 2015: The Daily Beast’s Picks, From Scarlett Johansson to ‘Boyhood’ |Marlow Stern |January 6, 2015 |DAILY BEAST 

During the height of his disenchantment, he visited his hometown where an old friend gave him some liquid acid. DJ Spooky Wants You To Question Everything You Know About Music, Technology, and Philosophy |Oliver Jones |December 27, 2014 |DAILY BEAST 

At the height of the Soviet Union, the proletariat universally understood everything their government said was a work of fiction. The Facts About Ferguson Matter, Dammit |Doug McIntyre |December 3, 2014 |DAILY BEAST 

Recipients in a cryobank can peruse donor files and see hair color, eye color, race, height, IQ, and so on. Have Sperm, Will Travel: The ‘Natural Inseminators’ Helping Women Avoid the Sperm Bank |Elizabeth Picciuto |November 29, 2014 |DAILY BEAST 

From the height of 700 feet, a lush uniform green obscured the destruction unfolding below him. ‘Argo’ in the Congo: The Ghosts of the Stanleyville Hostage Crisis |Nina Strochlic |November 23, 2014 |DAILY BEAST 

All please noteHis wondrous height and girth; He has the longest legs and throatOf anything on earth. Boys and Girls Bookshelf; a Practical Plan of Character Building, Volume I (of 17) |Various 

And now, escaped from her room, even at this height there came upon him again the hot sluggishness of London. Bella Donna |Robert Hichens 

The height of the tower from the level of the street is 105 feet, the slated towers over the lateral pediments being smaller. Showell's Dictionary of Birmingham |Thomas T. Harman and Walter Showell 

The purpose of these larger windows is the effectual lighting of the Boardroom, which is of the height of two storeys. Showell's Dictionary of Birmingham |Thomas T. Harman and Walter Showell 

She was barely five feet five, but she ranked with tall women, her height as unchallenged as the chiselling of her profile. Ancestors |Gertrude Atherton