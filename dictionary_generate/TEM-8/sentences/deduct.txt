I wonder whether answering this means I can deduct the cost of 11 years of youth hockey as a business expense. Carolyn Hax: He’s a great guy, except for the bigotry |Carolyn Hax |December 11, 2020 |Washington Post 

The IRS ruled in November that if a PPP borrower “reasonably expects to receive forgiveness,” then the borrower “may not deduct those expenses” in the year they were paid. The IRS effectively canceled the tax break that made PPP loans so valuable |Geoffrey Colvin |December 7, 2020 |Fortune 

Specifically, owning your own home means that you are often able to deduct interest and property taxes paid each year, which can result in significant tax savings over the course of the time that you own your home. High housing prices and low mortgage rates |Jeff Hammerberg |November 22, 2020 |Washington Blade 

Meanwhile, payments are conducted automatically and deducted from virtual wallets. Buy now, pay later: How COVID-19 is aiding the payment model |Rachel King |August 31, 2020 |Fortune 

A judge will deduct points based on the degree of any bending. A robot referee can really keep its ‘eye’ on the ball |Kathryn Hulick |August 20, 2020 |Science News For Students 

Blumenauer proposes to change the law to allow dispensaries to deduct expenses and thus retain more of their earnings. Tax Cuts for Pot Law-Breakers? |David Frum |September 13, 2013 |DAILY BEAST 

Or rather, they allowed it, but they wouldn't let you deduct any payments into an already overfunded plan. How the IRS Wrecked Your Pension |Megan McArdle |May 9, 2013 |DAILY BEAST 

That's because they can deduct it--and you don't have to pay taxes on it. Our Tax Code is Too Complicated. Here's How to Simplify It. |Megan McArdle |April 15, 2013 |DAILY BEAST 

You still deduct the full cost of the jet; you just do so a little more slowly. "Tax Breaks for Corporate Jets": The Non-Issue at the Heart of the Presidential Agenda |Megan McArdle |April 10, 2013 |DAILY BEAST 

After all, if the tax break goes away, these companies can still buy the free lunch; they just can't deduct it from their taxes. There's No Such Thing as a Free Lunch in Taxland |Megan McArdle |April 9, 2013 |DAILY BEAST 

Those who are watching us cannot possibly deduct anything from the presence of General Maxgregor at your aunt's reception. The Weight of the Crown |Fred M. White 

Deduct the hauling of materials—a considerable item—which could be done by the farmers themselves at odd times. The Hills and the Vale |Richard Jefferies 

Deduct British and foreign tons employed in the colonial trade, viz. Blackwood's Edinburgh Magazine, Volume 64, No. 393, July 1848 |Various 

Deduct the cost of crushing the quartz, (for it is found only in quartz,) and there is left—how much? The Atlantic Monthly, Volume 14, No. 86, December, 1864 |Various 

I won't deduct for it; I look to you to make it up handsomely by keeping the expenses down. Our Mutual Friend |Charles Dickens