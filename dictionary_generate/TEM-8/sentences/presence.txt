Someone passing through the lobby wouldn’t notice its presence. The Big Corporate Rescue and the America That’s Too Small to Save |by Lydia DePillis, Justin Elliott and Paul Kiel |September 12, 2020 |ProPublica 

We think that the presence of a planet between these rings is needed to explain why the disc was torn apart. A strange dusty disk could hide a planet betwixt three stars |Paola Rosa-Aquino |September 11, 2020 |Popular-Science 

This holds true for businesses because they can register their presence on YouTube to showcase their resources. How businesses can use YouTube to tackle the COVID-19 business crisis |Catherrine Garcia |September 7, 2020 |Search Engine Watch 

The mere presence of a young patrons committee, however, doesn’t mean millennials will come, and some nonprofits still find it challenging to engage with the audience. How nonprofits are catering to millennials and rethinking the charity gala for younger generations |Rachel King |September 7, 2020 |Fortune 

Critics of symmetry-based tests, though, say that the presence of packing and cracking does not prove that the governing party has intentionally gerrymandered the state’s districts. How next-gen computer generated maps detect partisan gerrymandering |Sujata Gupta |September 7, 2020 |Science News 

The well, ghost or no ghost, is certainly a piece of history with a bold presence. New York’s Most Tragic Ghost Loves Minimalist Swedish Fashion |Nina Strochlic |January 8, 2015 |DAILY BEAST 

“There is a heavy security presence but nothing has changed,” agrees Father Javier. Mexico’s Priests Are Marked for Murder |Jason McGahan |January 7, 2015 |DAILY BEAST 

He appeared to understand however belatedly that he was in the presence of another kind of greatness. Shot Down During the NYPD Slowdown |Michael Daly |January 7, 2015 |DAILY BEAST 

In general, their presence is meant to throw you off your game by design. Brie Larson’s Hollywood Transformation |Marlow Stern |December 29, 2014 |DAILY BEAST 

No one who lives in an American city requires a flashlight to detect the presence of immigrants or the challenges they face daily. The 2014 Novel of the Year |Nathaniel Rich |December 29, 2014 |DAILY BEAST 

Recognition of the pneumococcus depends upon its morphology, the fact that it is Gram-staining, and the presence of a capsule. A Manual of Clinical Diagnosis |James Campbell Todd 

His presence, also, always graced Walls End Castle at the regulation periods. The Pit Town Coronet, Volume I (of 3) |Charles James Wills 

Most characteristic is the presence of Curschmann's spirals, Charcot-Leyden crystals, and eosinophilic leukocytes. A Manual of Clinical Diagnosis |James Campbell Todd 

For two consecutive seasons he lived in the sunlight of Mademoiselle Duvigne's presence. The Awakening and Selected Short Stories |Kate Chopin 

This, however, should not be accepted as proving the presence of bile without further tests. A Manual of Clinical Diagnosis |James Campbell Todd