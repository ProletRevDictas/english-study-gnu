The Friars sit eleventh in the latest AP poll and project as a No. How Far Can Luck Take A Team? Watch Providence To Find Out. |Josh Planos |March 10, 2022 |FiveThirtyEight 

It is not known when or if the queen will meet her eleventh great grandchild. Meghan and Harry Welcome Baby Daughter, Named for Queen and Diana |Tom Sykes |June 6, 2021 |The Daily Beast 

Eleventh place is quite the crowning achievement for the little isthmus—especially considering the low quality offerings. House of the Witch: The Renegade Craft Brewers of Panama |Jeff Campagna |November 30, 2014 |DAILY BEAST 

Pointing to the elevator bank, they say in unison, "Eleventh floor." Backstage at the Razzie Awards, Honoring Hollywood’s Worst Films |David Eckstein |March 2, 2014 |DAILY BEAST 

All this has created an outpouring of national sentiment and a predictable flurry of eleventh-hour government initiatives. Former Miss Venezuela Murdered In Roadside Attack |Mac Margolis |January 9, 2014 |DAILY BEAST 

She won an Emmy in 1963 for an episode of The Eleventh Hour. The Deaths You Missed This Year |Malcolm Jones, Jimmy So, Michael Moynihan, Caitlin Dickson |December 30, 2013 |DAILY BEAST 

No more eleventh-hour miracles—the inevitable finally has to happen. ‘Homeland’ Finale Shocker: A Death in the Family |Andrew Romano |December 16, 2013 |DAILY BEAST 

On the eleventh it was washed, and a small grain of gold of the weight of one-half real was obtained. The Philippine Islands, 1493-1898, Volume XX, 1621-1624 |Various 

Given in Paris the 27th day of November, in the year of grace 1608, and of our reign the eleventh. The Jesuit Relations and Allied Documents, Vol. II: Acadia, 1612-1614 |Various 

Before turning up the eleventh card he paused for the fraction of a second. The Joyous Adventures of Aristide Pujol |William J. Locke 

And in the eleventh year of Sedecias, in the fourth month, the fifth day of the month, the city was opened. The Bible, Douay-Rheims Version |Various 

All things considered it was a wonderful achievement when, on the evening of the eleventh day, they began their last march. The Red Year |Louis Tracy