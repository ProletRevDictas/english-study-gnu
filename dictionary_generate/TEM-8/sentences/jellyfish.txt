Scientists have used viruses to insert a green fluorescent protein found in jellyfish into mouse brains, causing neurons to light up during learning. How technology can let us see and manipulate memories |Joshua Sariñana |August 25, 2021 |MIT Technology Review 

After the rocket touched back down on the landing pad, three parachutes erupted from the capsule and hovered above it like gigantic blue and red jellyfish as the vehicle landed in a puff of dust. Blue Origin brought the first official tourists to space |Claire Maldarelli |July 20, 2021 |Popular-Science 

He’s out on the beach over a ball, near those wooden picket beach fences, on a downslope toward the waves and jellyfish. At a breezy PGA, normalcy beckoned — in the galleries, if not the top of the leader board |Chuck Culpepper |May 21, 2021 |Washington Post 

That’s because, among animals, jellyfish are evolutionarily about as far away as you can get from mammals. Sleep Evolved Before Brains. Hydras Are Living Proof. |Veronique Greenwood |May 18, 2021 |Quanta Magazine 

The evidence for sleep in creatures with minimal nervous systems seemed to reach a new high about five years ago with studies of jellyfish. Sleep Evolved Before Brains. Hydras Are Living Proof. |Veronique Greenwood |May 18, 2021 |Quanta Magazine 

His first feature film, Jellyfish Eyes, debuted last year and was set in a town near a threatening nuclear power plant. Takashi Murakami’s Art From Disaster |Justin Jones |November 28, 2014 |DAILY BEAST 

In April, Murakami released his film Jellyfish Eyes, inspired by “a manga called GeGeGe no Kitaro” from the 1960s. Harper's Bazaar Stars Takashi Murakami's Monsters |Lori-Lee Emshey |November 5, 2013 |DAILY BEAST 

Fish are too smart, big, fast, and numerous for jellyfish to come anywhere close to getting an upper hand. Beware at the Beach, the Jellyfish Rule the Seas and It’s Our Fault |Lisa-ann Gershwin |June 20, 2013 |DAILY BEAST 

Does the abundance of jellyfish in the ocean pose a threat to us? Beware at the Beach, the Jellyfish Rule the Seas and It’s Our Fault |Lisa-ann Gershwin |June 20, 2013 |DAILY BEAST 

And so, quietly, notch by notch, jellyfish continue to inherit damaged ecosystems. Beware at the Beach, the Jellyfish Rule the Seas and It’s Our Fault |Lisa-ann Gershwin |June 20, 2013 |DAILY BEAST 

"He is a fair sample of some of the human jellyfish I have found hidden away in odd corners on this coast," stated Captain Mayo. Blow The Man Down |Holman Day 

The children's mother told them that the whale is the largest of all animals, and that it lives on little jellyfish. Stories of California |Ella M. Sexton 

By the time he was twenty-seven the only living thing that could be said to have served him as a model was the jellyfish. Where the Pavement Ends |John Russell 

Now the jellyfish pursues a most amiable theory of life, being harmless, humorous, and decorative. Where the Pavement Ends |John Russell 

Perhaps because, after all, no man ever quite achieves complete resemblance to a jellyfish. Where the Pavement Ends |John Russell