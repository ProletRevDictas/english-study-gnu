Cawthorn angered his congressional colleagues with his salacious orgy comment, and the lingerie photos were a particularly damaging surprise for voters, experts say, released less than a month before the primary. Why Madison Cawthorn Lost His Race |Abby Vesoulis |May 18, 2022 |TIme 

Allegations were even thrown around that the house was the site of an orgy or two. Inside the Little Green House on K Street, D.C.’s Most Scandalous Private Club |Allison McNearney |February 27, 2022 |The Daily Beast 

The NRF insists that they’re OK with the mandate in the abstract, but that they would prefer it start in February, after the holiday season and the yearly orgy of shopping it creates. Kyrie Irving Is Not the Real Villain Here |Corbin Smith |December 20, 2021 |The Daily Beast 

Indeed, Bannon, whose film Occupy Unmasked claimed to expose an orgy of criminality at the heart of the protest, nevertheless took up positions about the abandonment of the working class that mirrored the movement’s tone. Some Say Occupy Wall Street Did Nothing. It Changed Us More Than We Think |James A. Anderson |November 15, 2021 |Time 

Beginning in the 1990s, Democrats and Republicans showered benefits on the financial sector, paving the way for an orgy of subprime lending and financial engineering that inflated housing prices to dizzying heights. We know about Amazon’s sins. Do we care? |James Kwak |March 19, 2021 |Washington Post 

In honor of A Good Old Fashioned Orgy, in theaters Friday, we offer the best movie group-sex scenes. Shame, Eyes Wide Shut, American Psycho & Hollywood's Best Orgy Scenes |Marlow Stern |September 1, 2011 |DAILY BEAST 

The New Orleans Orgy started while a local radio station was broadcasting some of this new dance-music. Hunter Patrol |Henry Beam Piper and John J. McGuire 

Orgy, or′ji, n. any drunken or riotous rite or revelry, esp. Chambers's Twentieth Century Dictionary (part 3 of 4: N-R) |Various 

The Orgy is famous for the dash and abandon with which it is painted. The Standard Galleries - Holland |Esther Singleton