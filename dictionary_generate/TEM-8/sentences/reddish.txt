This elephant-sized creature had shaggy reddish-brown fur and long, curving tusks. Will the woolly mammoth return? |Kathryn Hulick |September 23, 2021 |Science News For Students 

Their heads also protrude a bit higher due to the slight differences in the positioning of their vertebrates—allowing a better look at the gold ring encircling their reddish-brown eyes. This rainbow-scaled lizard lived anonymously in the Andes—until now |Grace Wade |September 6, 2021 |Popular-Science 

A gregarious man with fading hair and a reddish beard, Charlie often helped Mami with housekeeping and other chores. Paradise lost: Remembering the popular Rehoboth men’s guest house |James Sears |September 2, 2021 |Washington Blade 

By the 1790s, British entrepreneurs were adding palm oil to soap for its reddish-orange color and violetlike scent. The fascinating and complex rise of palm oil |Purbita Saha |June 29, 2021 |Popular-Science 

It has a reddish tint, hanging out in the constellation Pisces. Skywatch: What’s happening in the heavens in January |Blaine P. Friedlander Jr. |December 26, 2020 |Washington Post 

Instead, they are blobs of reddish stars without any particular structure. The Care and Feeding of Baby Giant Galaxies |Matthew R. Francis |May 25, 2014 |DAILY BEAST 

Her finely penciled lips are a coppery brown, playing off her salmon-colored suit and her reddish hair. Shirley MacLaine on ‘Bernie,’ ‘Downton Abbey,’ and Her Lifetime Achievments |Lorenza Muñoz |April 25, 2012 |DAILY BEAST 

A few years later, Goudeau met Carr, a slender woman with a mane of long reddish-brown hair, at a Phoenix nightclub. Arizona’s Serial-Killer Saga |Terry Greene Sterling |October 20, 2011 |DAILY BEAST 

Pedestrians dodge streams of reddish liquid in the streets, said to be pollution from tanneries. The Perils of Karachi |Steve Inskeep |October 14, 2011 |DAILY BEAST 

The hair is more reddish these days, and he likes to tuck under a black calf-skin hat. Robert Redford on Sundance and His Future |Nicole LaPorte |January 22, 2011 |DAILY BEAST 

Microscopically, they are yellow or reddish-brown crystals, which differ greatly in size and shape. A Manual of Clinical Diagnosis |James Campbell Todd 

Malarial parasites stain characteristically: the cytoplasm, sky-blue; the chromatin, reddish-purple. A Manual of Clinical Diagnosis |James Campbell Todd 

The surface is covered by a shallow, reddish-coloured soil, producing a variety of shrubs and plants. Narrative of a Survey of the Intertropical and Western Coasts of Australia] [Volume 2 of 2] |Phillip Parker King 

She had the reddish hair of the Binns and the pearl-blue eyes of the Rummelsbergers from over the mountains. The Soldier of the Valley |Nelson Lloyd 

The cliffs of Red Point partake of a reddish tinge and appear to be disposed nearly in horizontal strata. Narrative of a Survey of the Intertropical and Western Coasts of Australia] [Volume 2 of 2] |Phillip Parker King