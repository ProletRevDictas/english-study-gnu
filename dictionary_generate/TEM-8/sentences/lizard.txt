They are more likely to bite lizards called skinks, which are poor transmitters of the bacteria, researchers report January 28 in PLOS Biology. The animals that ticks bite in the U.S. South can impact Lyme disease spread |Aimee Cunningham |February 5, 2021 |Science News 

At night, the lizards retreat upward, gripping blades of grass or other plants for safety. A new chameleon species may be the world’s tiniest reptile |Jonathan Lambert |February 4, 2021 |Science News 

The animal was a mosasaur, an extinct, marine reptile related to snakes and monitor lizards. This ancient sea reptile had a slicing bite like no other |Jake Buehler |February 2, 2021 |Science News 

In 1971, paleontologist Robert Bakker argued for a landbound dinosaur, based on the resemblance of its nostrils to those of terrestrial lizards. 50 years ago, scientists made the case for a landlubbing Brontosaurus |Bethany Brookshire |January 29, 2021 |Science News 

Until recently, it wasn’t clear where monitor lizards lay their eggs. Monitor lizards’ huge burrow systems can shelter hundreds of small animals |Jake Buehler |January 19, 2021 |Science News 

Another set of hackers that goes by the name the Lizard Squad told the Washington Post that they helped with the Sony hack. U.S. Spies Say They Tracked ‘Sony Hackers’ For Years |Shane Harris |January 2, 2015 |DAILY BEAST 

And some members of the Lizard Squad are now claiming that they were never trying to poison the network. The Attack on the Hidden Internet |Marc Rogers |December 29, 2014 |DAILY BEAST 

There is a lot of residual concern that Lizard Squad was able to get even this far. The Attack on the Hidden Internet |Marc Rogers |December 29, 2014 |DAILY BEAST 

But the Lizard Squad was able to get one thing done: Piss off Anonymous, the best known of the hacktivist collectives. The Attack on the Hidden Internet |Marc Rogers |December 29, 2014 |DAILY BEAST 

It almost makes you wonder if Lizard Squad did this just to annoy Anonymous and the other earnest champions of privacy. The Attack on the Hidden Internet |Marc Rogers |December 29, 2014 |DAILY BEAST 

The woman reminds one of a red lizard—a salamander—her “svelte” body seemingly boneless in its gown of clinging scales. The Real Latin Quarter |F. Berkeley Smith 

Respecting this remarkable Lizard, Mr. Cunningham's journal contains the following remarks. Narrative of a Survey of the Intertropical and Western Coasts of Australia] [Volume 2 of 2] |Phillip Parker King 

I should have been in her fond embrace—now I shared the company of the rat, the lizard, and the scorpion. Confessions of a Thug |Philip Meadows Taylor 

They were defensive, anti-personal weapons to be employed as he had done against the lizard in the arena. Star Born |Andre Norton 

The man turned his head quickly; the lizard dodged under the rail; and old Kate awoke with a start. The Shepherd of the Hills |Harold Bell Wright