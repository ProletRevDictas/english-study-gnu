Chief administrative officer Helen Robbins-Meyer, the county’s top bureaucrat, cautioned officials against making that the norm. Welcome to a Dem-Controlled County |Jesse Marx and Maya Srikrishnan |November 4, 2020 |Voice of San Diego 

Karma Ura is a bespectacled, self-effacing man of many achievements—a scholar, writer, painter, and bureaucrat. How “gross national happiness” helped Bhutan keep covid-19 at bay |Katie McLean |October 21, 2020 |MIT Technology Review 

Generally, they’ve campaigned on a message that county officials are reactive and take their cues from the top bureaucrats rather than the other way around. Dems Want Control of the County – Here’s What They Say They’d Do With it |Jesse Marx |October 7, 2020 |Voice of San Diego 

Michell’s announcement ensures that the city’s next mayor will choose the city’s next top bureaucrat. City COO Kris Michell Resigns |Lisa Halverstadt |September 21, 2020 |Voice of San Diego 

For Election Administrators, Death Threats Have Become Part of the JobIn a polarized society, the bureaucrats who operate the machinery of democracy are taking flak from all sides. Electionland 2020: DeJoy Under Fire, Election Administrators, Pandemic Voting and More |by Rachel Glickhouse |August 21, 2020 |ProPublica 

He concentrates on a handful of characters that includes a doctor, a bureaucrat, a criminal, a priest, and a journalist. Albert Camus, Our Existential Epidemiologist |Malcolm Jones |October 17, 2014 |DAILY BEAST 

The EU needs another Greece or Portugal dragging down the euro like the EU needs another bureaucrat in Brussels. Up to a Point: A Free Scotland Would Be a Hilarious Disaster |P. J. O’Rourke |September 13, 2014 |DAILY BEAST 

After all, there will always be a bureaucrat, politician, or judge eager to set the limits on what is unacceptably offensive. Can a Tweet Put You in Prison? It Certainly Will in the UK |Michael Moynihan |January 23, 2014 |DAILY BEAST 

Faisal Khan, a 28-year-old government bureaucrat in Peshawar, says he would get fired or worse if he came out. Pakistan’s Gay Community Quietly Breaking Barriers |Ron Moreau, Sami Yousafzai |October 30, 2013 |DAILY BEAST 

And he spent virtually all his career as a government bureaucrat—as an official at the Treasury Department and the New York Fed. Larry Summers’s Connection to Wall Street Should Surprise No One |Daniel Gross |September 13, 2013 |DAILY BEAST 

His former associates regarded him as a renegade; independent observers found in him an energetic and arbitrary bureaucrat. Mr. Punch's History of Modern England Vol. IV of IV. |Charles L. Graves 

The wealthy and the educated know how to placate the bureaucrat and get what they want. The Political Future of India |Lajpat Rai 

What had become of the lesson in decorum which should have been taught to this vulgar little bureaucrat? The Bronze Eagle |Emmuska Orczy, Baroness Orczy 

What does your worker think when he sees the bureaucrat living in luxury while his wage is a comparatively meager one? Revolution |Dallas McCord Reynolds 

The new Czar was greatly influenced by his former tutor, the reactionary bureaucrat Pobiedonostzev. Bolshevism |John Spargo