When companies don’t do what they say, we see mass exoduses and even lawsuits, as has recently been the case at Pinterest and Carta. The need for true equity in equity compensation |Walter Thompson |October 16, 2020 |TechCrunch 

The exodus isn’t necessarily good news, according to Batt and other experts. Investors Extracted $400 Million From a Hospital Chain That Sometimes Couldn’t Pay for Medical Supplies or Gas for Ambulances |by Peter Elkind with Doris Burke |September 30, 2020 |ProPublica 

Unsurprisingly, the mass exodus of the well-to-do inspired contempt among those who didn’t have the option of motoring out to their second home on Long Island or in the Catskills. The Eternal Magic of Van Cortlandt Park |Martin Fritz Huber |September 28, 2020 |Outside Online 

So the Florida Legislature created a state-run company to insure properties itself, preventing both an exodus and an economic collapse by essentially pretending that the climate vulnerabilities didn’t exist. Climate Change Will Force a New American Migration |by Abrahm Lustgarten, photography by Meridith Kohut |September 15, 2020 |ProPublica 

Indeed, with early evidence of an exodus from cities to suburbs amid shutdowns, new homebuyers may well be game to buy a fixer-upper. These 2 home improvement stocks are up 30% this year. They’re still a buy |Anne Sraders |August 19, 2020 |Fortune 

Maybe even a mass exodus of Venezuelan women looking to get breast implants done in Colombia. Venezuela Now Has Toilet Paper but No Breast Implants |Jason Batansky |September 16, 2014 |DAILY BEAST 

But the day before the final exodus, Christians were informed jizya was no longer an option. ISIS Robs Christians Fleeing Its Edict in Mosul: Convert, Leave, or Die |Andrew Slater |July 22, 2014 |DAILY BEAST 

If the bill were to become law, Vinnichenko predicts “a mass exodus” of LGBT families, including her own. Homophobia in Russia Is Taking a Kafkaesque Turn |Jay Michaelson |June 9, 2014 |DAILY BEAST 

For New Yorkers, the mass exodus tends to have one destination in mind: The Hamptons. The Hamptons Hot List: Where to Eat, What to Drink, and How to Get Your Beach on This Summer |Brandon Presser |May 24, 2014 |DAILY BEAST 

But does the talent exodus signal trouble at the paper or only a changed media landscape? The New York Times Brain Drain |Lloyd Grove |November 13, 2013 |DAILY BEAST 

There was no crowding or impeding haste in their dumb exodus. The Three Partners |Bret Harte 

From the city there was reported exodus of men whose names were enrolled for military service. Recollections of Thirty-nine Years in the Army |Charles Alexander Gordon 

We saw the blonde behind the wheel and Uncle Peter seated beside her, evidently still protesting the hasty exodus. "And That's How It Was, Officer" |Ralph Sholto 

One hears a frightful lot of nonsense about the Rural Exodus and the degeneration wrought by town life upon our population. Tono Bungay |H. G. Wells 

In a big pastoral exodus like the present, it is simply impossible to keep strays out of moving herds. Wells Brothers |Andy Adams