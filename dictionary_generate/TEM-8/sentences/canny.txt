Schumer’s aw-shucks humblebrags conceal a canny, ambitious pol. The Great Kibitzer: How Chuck Schumer Got the Senate Moving Again |Molly Ball |September 2, 2021 |Time 

It’s a canny parody of how Western media has focused on these concerns amid claims that K-pop stars are forbidden to date, have grueling training schedules, and face exacting pressure from their labels and rabid fans alike. The K-Pop Episode of Dave Is Cross-Cultural Collaboration Done Right |Andrew R. Chow |June 17, 2021 |Time 

Despite his lovey-dovey talk and reassurances about the bliss and safety that await her, Bilel is clearly a cunning viper engaged in his own dangerous fraud, thus further marking Profile as a canny, multifaceted snapshot of digital deception. Tracing an ISIS Fighter’s Terrifying Seduction of a Female Journalist |Nick Schager |May 14, 2021 |The Daily Beast 

Other portrayals of nature are rendered fantastic partly by the canny use of color. In the galleries: Artists sport their chops with prints on the cutting edge |Mark Jenkins |March 19, 2021 |Washington Post 

That’s what the canny retail investors spotted when they decided to target GameStop. GameStop plunges again as the retail rally fades |Bernhard Warner |February 2, 2021 |Fortune 

Ever canny if uninspiring, John Boehner admitted as much in his recent remarks. What Republicans Need Right Now Is a Good Internal Fight |James Poulos |November 6, 2014 |DAILY BEAST 

But he was a canny political operator, far less ideological and more coldly pragmatic than proponents liked to admit. From The Square Deal to The New Deal: The Overlapping Political Identities of TR and FDR |John Avlon |September 9, 2014 |DAILY BEAST 

A wavering, but canny Wehrmacht General Dietrich von Choltitz finally surrendered it on August 25. Who Liberated Paris in August 1944? | |August 24, 2014 |DAILY BEAST 

That is admirable, and Preserve is clever, or at least canny. Blake Lively Gets Her GOOP On |Tim Teeman |July 23, 2014 |DAILY BEAST 

All of it is so canny we can only wonder why no one had said these things before. Geoff Dyer at Sea: Unmoored but on Target |Melissa Holbrook Pierson |May 21, 2014 |DAILY BEAST 

Keen and canny, they drive a close bargain but, scrupulous and conscientious, fulfil it faithfully. Fifty Years of Railway Life in England, Scotland and Ireland |Joseph Tatlow 

He is a steady lad,’ your father said, ‘and a canny goer; and I doubt not he will come safe, and be well liked where he goes. The Works of Robert Louis Stevenson - Swanston Edition Vol. 10 (of 25) |Robert Louis Stevenson 

After three years, during which he served me very faithfully, I transferred him to a Frenchman, Mr. Canny by name.' A Fantasy of Far Japan |Baron Kencho Suyematsu 

The august name of Wilkins's was in its essence so exclusive that vast numbers of fairly canny provincials had never heard of it. The Regent |E. Arnold Bennett 

It had been mischievously started by Muriel and smilingly declined by three canny freshmen. Marjorie Dean College Freshman |Pauline Lester