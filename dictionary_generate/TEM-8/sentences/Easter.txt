Most see Easter as an escapist fantasy, holding out the mirage-like hope of “heaven” hereafter. What is the Point of Celebrating Easter During a Pandemic? |N.T. Wright |April 2, 2021 |Time 

In addition to Christmas, it’s a big part of Easter celebrations, where it’s a common after-meal or tea-time sweet. How I Tried (and Failed) And Tried Again to Master the Goan Bebinca Cake |Joanna Lobo |April 1, 2021 |Eater 

I didn’t grow up celebrating a holiday that falls around the spring equinox, not Nowruz, not Passover, not Holi, nor Easter. A one-pan salmon dinner with minty peas, orange and fennel |Daniela Galarza |April 1, 2021 |Washington Post 

We’re also in the middle of Passover and approaching Easter, and celebratory gatherings could result in further spread. Is the U.S. Entering a Fourth Wave of COVID-19? |Tara Law |March 31, 2021 |Time 

Castel said she’s worried about another surge this spring, when people travel longer distances to be with family and friends for Passover, Easter and spring break amid emerging variants and a mostly unvaccinated public. Americans have started leaving home even more than before the pandemic, cellphone data shows |Katherine Shaver |March 11, 2021 |Washington Post 

At the same time, the Easter Elchies House began to deteriorate. Ester Elchies, The Estate Built By Whiskey | |December 10, 2014 |DAILY BEAST 

The land at Easter Elchies was the ideal place for Reid to set up his business. Ester Elchies, The Estate Built By Whiskey | |December 10, 2014 |DAILY BEAST 

When I reached Easter Elchies House, it was almost midnight, well, well, well passed the time that I was expected to arrive. A Whisky Connoisseur Remembers That First Sip of The Macallan | |December 10, 2014 |DAILY BEAST 

And I was lucky enough to receive an invitation to stay at Easter Elchies House, the spiritual home at The Macallan. A Whisky Connoisseur Remembers That First Sip of The Macallan | |December 10, 2014 |DAILY BEAST 

The company soon embarked on a refurbishment of Easter Elchies, opening to the public in 1985. Ester Elchies, The Estate Built By Whiskey | |December 10, 2014 |DAILY BEAST 

The doctor had been spending Easter at Cannes, and the dowager had devoutly prayed that he might not yet return. Elster's Folly |Mrs. Henry Wood 

The devotional reading of the story is a most natural and helpful observance of the Easter season. His Last Week |William E. Barton 

That morning, it was the first Sunday after Easter, the Duchess rode out of the castle on her great sorrel horse, while on? Honey-Bee |Anatole France 

I often console myself with thoughts of Easter, spring, and the summer holidays. The Life &amp; Letters of Peter Ilich Tchaikovsky |Modeste Tchaikovsky 

After the reform bill had been read a second time, the lords broke up for the Easter recess. The History of England in Three Volumes, Vol.III. |E. Farr and E. H. Nolan