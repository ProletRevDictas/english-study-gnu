It may be hard to find more mournful meaning in a mere number. Monday was a summer landmark: The last sunrise before 6:30 a.m. |Martin Weil |August 24, 2021 |Washington Post 

The Samaritaine department store—once grand, but recently shuttered at the time of the movie’s filming—had its own supporting role, looming over the proceedings like a mournful gray ghost. Annette Is Gorgeous to Look at But All the Wrong Kinds of Weird |Stephanie Zacharek |August 6, 2021 |Time 

About Endlessness, meditative, mournful and subtly celebratory, reminds us to cherish the in-between. The Best Movies of 2021 So Far |Stephanie Zacharek |May 25, 2021 |Time 

It’s meditative, mournful and gently funny, and celebratory, too, but in a muted way. Roy Andersson's Quietly Gorgeous About Endlessness Explores Questions for Which There Are No Answers |Stephanie Zacharek |April 29, 2021 |Time 

The plaintiveness of his surrender is both funny and mournful. In the Oscar-Nominated The Man Who Sold His Skin, a Refugee Stakes His Future on a Tattoo |Stephanie Zacharek |April 9, 2021 |Time 

The familiar and mournful theme song, “Suicide Is Painless,” filled the room. Almost Famous: A Father's Day Story |Alex Belth |June 15, 2014 |DAILY BEAST 

These aren't stomping tunes, but tender and mournful folk songs, a bespoke genre. Why No Oscar Love For 'Inside Llewyn Davis'? |Tim Teeman |January 20, 2014 |DAILY BEAST 

The proper melodies for putting Hebrew poems to music were Russian, and mostly mournful. For My Money, I'll Take the Al-Kuwaitis |Gershom Gorenberg |May 6, 2013 |DAILY BEAST 

"Nothing here but Oxy and coal," says one of the subjects of Sean Dunne's mournful documentary Oxyana. How Drugs Ruined This Small Town |David Frum |April 29, 2013 |DAILY BEAST 

It followed one of the saddest and most profoundly mournful images of defeat. Election Night 2012: Fashion of Jubilation And Mourning |Robin Givhan |November 7, 2012 |DAILY BEAST 

Felipe watched over her as a lover might; her great mournful eyes followed his every motion. Ramona |Helen Hunt Jackson 

A red moon hung above the mournful hills, and the stars shone in their myriads. The Wave |Algernon Blackwood 

A cold and mournful wind blew down the street, ruffling the darkened river. The Wave |Algernon Blackwood 

They jeered and sounded mournful notes without promise, devoid even of hope. The Awakening and Selected Short Stories |Kate Chopin 

Half an hour later the island was silent as the grave, but for the mournful voices of the wind as it sighed up from the sea. Three More John Silence Stories |Algernon Blackwood