Its most popular post was a story about a colony of humans living in elevator shafts. A GPT-3 bot posted comments on Reddit for a week and no one noticed |Niall Firth |October 8, 2020 |MIT Technology Review 

They come in green or pink, and have a waterproof rubber exterior with a stretchy, nylon shaft. Clothing and accessories that make great gifts |PopSci Commerce Team |October 6, 2020 |Popular-Science 

Select a dead, dry hardwood stave for your bow, and pick some shoots or sucker growth for the arrow shafts. This essential survival tool can save your life 10 different ways |By Tim MacWelch/Outdoor Life |September 15, 2020 |Popular-Science 

The shafts surround the site of a Neolithic village called Durrington Walls. Underground mega-monument found near Stonehenge |Avery Elizabeth Hurt |August 11, 2020 |Science News For Students 

Notches and wear at the bottom of the bone points indicate that they were attached to thin shafts. Clues to the earliest known bow-and-arrow hunting outside Africa have been found |Bruce Bower |June 12, 2020 |Science News 

Wandering around Tribeca, you may stumble upon a decrepit elevator shaft that's full of curiosities. New York’s Tiniest—and Weirdest—Museum |Nina Strochlic |May 29, 2014 |DAILY BEAST 

A man had killed himself by jumping out the window at the top, while a woman had done so by jumping down the elevator shaft. Weird Washington Monument History |William O’Connor |May 12, 2014 |DAILY BEAST 

In one tragic incident in 1965, a man named Bob Restall passed out in the shaft and fell into the water. Treasure Hunt to Discover Oak Island’s Mysterious Booty |Nina Strochlic |February 27, 2014 |DAILY BEAST 

Any celebration of these findings was quickly quashed as the shaft continued to flood and delay the work. Treasure Hunt to Discover Oak Island’s Mysterious Booty |Nina Strochlic |February 27, 2014 |DAILY BEAST 

For decades, explorers struggled to cap an endless flood of water that prevented access to the shaft. Treasure Hunt to Discover Oak Island’s Mysterious Booty |Nina Strochlic |February 27, 2014 |DAILY BEAST 

His arm was drawn around the drum, and finally his whole body was drawn over the shaft, at a fearful rate. The Book of Anecdotes and Budget of Fun; |Various 

"I did n't fly off the handle," said the screw, twirling huskily at the end of the screw-shaft. Kipling Stories and Poems Every Child Should Know, Book II |Rudyard Kipling 

It was placed immediately over the shaft and pump-rods, requiring no engine-beam. Life of Richard Trevithick, Volume II (of 2) |Francis Trevithick 

The case was fixed over the engine-shaft on two beams of timber from wall to wall. Life of Richard Trevithick, Volume II (of 2) |Francis Trevithick 

The pole-case was fixed to strong beams immediately over the pump-shaft. Life of Richard Trevithick, Volume II (of 2) |Francis Trevithick