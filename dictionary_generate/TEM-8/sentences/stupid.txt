If all “stupid grids” were replaced by smart grids, it would allow cities, for example, to manage production, storage, distribution and consumption of energy and to cut peaks in energy demand that would reduce CO2 emissions dramatically. Use today’s tech solutions to meet the climate crisis and do it profitably |Walter Thompson |February 12, 2021 |TechCrunch 

I’m just going to make this as completely stupid as I want to be. How Gideon the Ninth author Tamsyn Muir queers the space opera |Constance Grady |February 5, 2021 |Vox 

Her philosophy, onstage and off, is that not everybody is stupid, not only her views are right, and if we listen to those we disagree with instead of rolling our eyes, we might get somewhere. Sarah Silverman just wants to make things right |Geoff Edgers |February 4, 2021 |Washington Post 

What that story missed is that professional investors don’t underperform the market because they’re stupid. What happens after the GameStop bubble bursts? |Peter Kafka |February 2, 2021 |Vox 

Like, this thing will get published and then some Wyomingite will ride in on a horse and be all like, “Just kidding, we eat chicken wings at our Super Bowl parties, you stupid East Coast rube.” The 2021 Super Bowl food map is a deep dive into America’s weird culinary underbelly |Matt Bonesteel |February 2, 2021 |Washington Post 

Later, his turn as a lothario in the box office hit Crazy Stupid Love made him even more swoon-worthy. Rob Lowe: Don’t Hate Me Because I’m Beautiful |Tricia Romano |April 8, 2014 |DAILY BEAST 

Contrary to What Stupid Republicans Think… A completely different U.S. foreign policy may not be the answer. P.J. O’Rourke on Foreign Policy and France, Hold the Swiss |P. J. O’Rourke |January 17, 2014 |DAILY BEAST 

As expected, initial reports were met with cries of “Stupid!” Will Valve’s New Steam Controller Revolutionize Video Game Play? |Alec Kubas-Meyer |September 27, 2013 |DAILY BEAST 

On Louisiana Gov. Bobby Jindal, who said Republicans are acting like the Stupid Party? Donald Trump, Still Wacky, Rips Republicans at CPAC |Lauren Ashburn |March 15, 2013 |DAILY BEAST 

They did not pander to social-conservative populists, and no one would accuse them of representing the Party of Stupid. GOP Needs More Northeast Republicans to Save the Party |John Avlon |January 30, 2013 |DAILY BEAST 

Stupid things puns—made one myself then, though—just like me. Frank Fairlegh |Frank E. Smedley 

Stupid you were when you stole things out of my book—could you not guess that I might have read my own books? Creditors; Pariah |August Strindberg 

Stupid you were when you thought yourself cleverer than me, and when you thought that I could be lured into becoming a thief. Creditors; Pariah |August Strindberg 

Stupid you were when you thought balance could be restored by giving the world two thieves instead of one. Creditors; Pariah |August Strindberg 

Stupid Polish Majesty has his natural envies, jealousies, of a Brandenburg waxing over his head at this rate. History of Friedrich II. of Prussia, Vol. XV. (of XXI.) |Thomas Carlyle