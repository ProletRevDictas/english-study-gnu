I still very much appreciate the feel of Oklahoma, the sort of warmth of it, but I also know that some of that warmth masked a very ugly history that’s now being revealed. Can Anita Hill Forgive Joe Biden … and Work With Him? |Pallabi Munsi |September 14, 2020 |Ozy 

Preserving the quality of the content and presentations, maximizing networking opportunities and preserving as much of the feel of the expo hall as possible were among the top priorities. ‘Layer of data and efficiency’: How TechCrunch took Disrupt virtual — and grew for its tenth anniversary |Max Willens |September 11, 2020 |Digiday 

This should help you make a decision whether or not you may feel comfortable sending your child to day care. Is it safe to return to day care? 7 experts weigh in |Brooke Henderson |July 23, 2020 |Fortune 

The result is a health care experience that feels a lot more like what we would all expect for our loved ones in a time of need. Why this health care startup felt launching early during a pandemic was the best business strategy |Rachel King |July 20, 2020 |Fortune 

Year after year I’ve spent innumerable hours scouring the internet attempting to find a present that feels unique and intimate even when my partner and I are thousands of miles apart. 8 Gifts to Bring Back the Fire in Long-Distance Relationships |Tracy Moran |July 10, 2020 |Ozy 

Citizens, perhaps, need to feel like they can communicate something to science. Anti-Fluoriders Are The OG Anti-Vaxxers |Michael Schulson |July 27, 2016 |DAILY BEAST 

How do you feel about Archer and the gang abandoning the cartel and returning to the office? ‘Archer’ Creator Adam Reed Spills Season 6 Secrets, From Surreal Plotlines to Life Post-ISIS |Marlow Stern |January 8, 2015 |DAILY BEAST 

For someone with anorexia, self-starvation makes them feel better. How Skinny Is Too Skinny? Israel Bans ‘Underweight’ Models |Carrie Arnold |January 8, 2015 |DAILY BEAST 

Its biggest asset, of course, is the steely Atwell, who never asks you to feel sorry for Carter despite all the sexism around her. Marvel’s ‘Agent Carter’ Stomps on the Patriarchy |Melissa Leon |January 7, 2015 |DAILY BEAST 

This is not making the 228,000 residents of Irving, Texas feel very relaxed. 26 Earthquakes Later, Fracking’s Smoking Gun Is in Texas |James Joiner |January 7, 2015 |DAILY BEAST 

After all, may not even John Burns be human; may not Mr. Chamberlain himself have a heart that can feel for another? God and my Neighbour |Robert Blatchford 

“You appear to feel it so,” rejoined Mr. Pickwick, smiling at the clerk, who was literally red-hot. The Posthumous Papers of the Pickwick Club, v. 2(of 2) |Charles Dickens 

It was such a magnificent sum that Sol did not feel like taking the familiarity with it of mentioning it aloud. The Bondboy |George W. (George Washington) Ogden 

And he had waited so long for Grandfather Mole that he had begun to feel hungry again. The Tale of Grandfather Mole |Arthur Scott Bailey 

They feel that the system has few advantages to offer in return for the cost it entails upon them. Readings in Money and Banking |Chester Arthur Phillips