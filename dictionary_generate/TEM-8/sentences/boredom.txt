Podcasts, music, and other audio can transport listeners and enhance enjoyment in environments where the increased focus is required or where boredom must be kept at bay. Best headphones: Get good-quality audio equipment at your budget |PopSci Commerce Team |December 17, 2020 |Popular-Science 

As soaring stock markets and boredom have taken over the psyches of those locked in at home, would-be Robinhood challengers are also taking advantage of the times. Welcome to the investing wars |Lucinda Shen |December 15, 2020 |Fortune 

Before the pandemic, podcasts helped me stave off boredom on long-haul flights and kept me company during sleepless nights when my body was in one time zone and my mind was in another. With travel podcasts, explore the world through your ear buds |Andrea Sachs |December 11, 2020 |Washington Post 

Forced into partnership, in a silent pact, we agreed to compete—to treat our schoolwork like a video game and establish a high score for it, staving off boredom and distracting ourselves from the torments that arrived at the hands of our peers. Fiction: Quiet earth philosophy |Katie McLean |October 21, 2020 |MIT Technology Review 

It’s well-documented that boredom can lead to mistakes or inattention. Two new books explore Mars — and what it means to be human |Lisa Grossman |July 15, 2020 |Science News 

One needs not have served with the IDF to know true boredom, after all. ‘Zero Motivation’: the Funny Side of the IDF |Melissa Leon |December 8, 2014 |DAILY BEAST 

“Boredom is something that a person can experience anywhere,” she says. ‘Zero Motivation’: the Funny Side of the IDF |Melissa Leon |December 8, 2014 |DAILY BEAST 

These include tips on how to avoid boredom, loneliness, frustration, and anxiety. Zen, Motorcycles, And The Cult of Tech: How Robert Pirsig’s Classic Anticipated the Future |Nathaniel Rich |August 31, 2014 |DAILY BEAST 

But the prevailing emotion that day, even among us awardees, was a bemused sense of boredom, restlessness and insatiability. The Medal of Honor Disgrace |Brian Van Reet |March 26, 2014 |DAILY BEAST 

When Charlotte and Amerigo resume their affair, their behavior seems motivated less by passion than boredom. Henry James’s 1904 Sordid Little Sex Farce |Nathaniel Rich |January 30, 2014 |DAILY BEAST 

The whole scene breathed boredom, the man embarrassed by the consciousness of his nullity, the woman tired of her dismal visitor. Camille (La Dame aux Camilias) |Alexandre Dumas, fils 

Victor d'Arlan examined his fingernails and registered aristocratic boredom. We're Friends, Now |Henry Hasse 

When her brilliant little face was in repose, it had a new look of fatigue and boredom. Happy House |Betsey Riddle, Freifrau von Hutten zum Stolzenberg 

Blood and, more than that, a desperate boredom fell upon the light touch. The Boy Grew Older |Heywood Broun 

Life knocked at the door and tore him from his artist's dreams to a dissolute existence of alternating pleasure and boredom. The Precipice |Ivan Goncharov