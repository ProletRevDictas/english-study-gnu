Hunting in many cases is not achievable in these areas either because the lands aren’t open to hunting or the housing is such that firearms and bows aren’t allowed to be discharged. Four wild animals that are thriving in cities |By Ryan Chelius/Outdoor Life |February 9, 2021 |Popular-Science 

By 1804, the Tlingit had procured firearms, shot, gunpowder, and even cannons from American and British traders. Lost Alaskan Indigenous fort rediscovered after 200 years |Kiona N. Smith |February 5, 2021 |Ars Technica 

Background checks, and sales of firearms and ammunition, have been increasing pace for months. Fearing violence and political uncertainty, Americans are buying millions more firearms |Hannah Denham, Andrew Ba Tran |February 4, 2021 |Washington Post 

Greenwalt, the police spokesman, told reporters Wednesday that Narumanchi had “numerous guns on him” but didn’t offer specifics on the types of firearms. Terminally ill doctor killed pediatrician and himself after hours-long hostage standoff in Austin, police say |Andrea Salcedo, Derek Hawkins |January 27, 2021 |Washington Post 

Though the number of firearms found decreased — 3,257 in 2020 compared with a record 4,432 in 2019 — the rate per passenger was the highest since the agency was founded in 2001. Rate of TSA gun recoveries doubled in 2020 compared with a year earlier |Justin Wm. Moyer, Luz Lazo |January 26, 2021 |Washington Post 

Grimes' allies are rallying behind her ability to handle a firearm. Alison Lundergan Grimes’s New TV Ad Is One Big Gun Gaffe |Tim Mak |September 16, 2014 |DAILY BEAST 

INTERACTIVE: Hover over markers for more from each range on their age and firearm restrictions for children. Where Kids as Young as 5 Learn to Shoot Automatic Weapons |Brandy Zadrozny |September 10, 2014 |DAILY BEAST 

I spent a surreal evening watching all 85 episodes and gathered the best of the worst big ideas from the firearm luminaries. Gun Training Could’ve Prevented Newtown and 7 More of the Craziest Ideas From NRA YouTube Stars |Brandy Zadrozny |July 25, 2014 |DAILY BEAST 

The newest pro-gun propaganda: Make firearm education compulsory in schools and create gun-required zones. NRA Hipster: Give All Kids a Gun |Brandy Zadrozny |July 23, 2014 |DAILY BEAST 

He noted that possession of a loaded firearm is not even classified as a violent crime under Illinois state law. Obama, Why Aren’t You in Chiraq? |Michael Daly |July 9, 2014 |DAILY BEAST 

He scanned the smaller firearm, and then, instead of returning it to the Sioux, deliberately shoved it into his hip-pocket. Two Boys in Wyoming |Edward S. Ellis 

Yet with a panic at his heart he knew that it was the sharp crack of a firearm. Riders of the Silences |John Frederick 

Mr. Ridd recently despatched himself with a firearm for the following reasons, set forth in a letter that he left behind. The Fiend's Delight |Dod Grile 

Trask now searched Jarrow's cabin in the hope of finding some sort of firearm, but there was neither pistol nor rifle. Isle o' Dreams |Frederick F. Moore 

Washington strove also, but by the end of the siege was still unable, to provide for his men some form of regulation firearm. The Siege of Boston |Allen French