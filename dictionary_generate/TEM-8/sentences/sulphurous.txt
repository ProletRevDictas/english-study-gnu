There was a terrible, sulphurous smell, like rotten eggs, and a tremendous pressure against my chest. Swallowed by a Hippo |Justin Green |May 6, 2013 |DAILY BEAST 

Nor did the voluble and sulphurous orders to halt that a patrol-ship flashed north. Astounding Stories, May, 1931 |Various 

On the superheated surface white streaks ran like lightning in a sulphurous cloud. Balsamo, The Magician |Alexander Dumas 

A distant but threatening thunder murmur broke from the heart of a bank of sulphurous clouds beating closely over the south. Menotah |Ernest G. Henham 

This refrigerator is like those which we employ in our sulphurous anhydride frigorific apparatus. Scientific American Supplement No. 299 |Various 

Thus far the value of the products obtained by absorption of sulphurous acid has not been equal to the cost of producing them. Scientific American Supplement No. 299 |Various