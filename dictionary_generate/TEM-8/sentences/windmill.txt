Especially a romantic one, who isn’t unhinged or tilting at windmills. Christopher Lloyd is still playing characters who are unhinged — and larger than life |Karen Heller |August 26, 2021 |Washington Post 

Colleagues have cautioned him against chasing windmills in a quest that McConnell himself describes as “a little bit quixotic.” The quest to learn if our brain’s mutations affect mental health |Roxanne Khamsi |August 25, 2021 |MIT Technology Review 

There’s even up, in Yorkshire, a working windmill that actually produces flour. Should Traffic Lights Be Abolished? (Ep. 454) |Stephen J. Dubner |March 11, 2021 |Freakonomics 

Adding to the citizens’ misery are rolling electrical blackouts, possibly related to the fact that the state legislature has banned all sources of electricity except windmills and 9-volt batteries. Dave Barry’s Year in Review 2020 |Dave Barry |December 27, 2020 |Washington Post 

Tim Hwang recognizes he is tilting at windmills here, trillion-dollar windmills. Does Advertising Actually Work? (Part 2: Digital) (Ep. 441) |Stephen J. Dubner |November 26, 2020 |Freakonomics 

Up next: a windmill prototype, which is a few months from being finished. Why the Clintons Love Sierra Leone’s Boy Genius |Nina Strochlic |September 26, 2013 |DAILY BEAST 

But the American Dream is such a pretty windmill to chase that it's not a problem to get new arrivals to join in the pursuit. Why I Love the Mosque |Elizabeth Wurtzel |August 4, 2010 |DAILY BEAST 

They are, across the board, quixotic characters hacking at the windmill of language. Do You Speak Klingon? |Stephen Brown |June 14, 2009 |DAILY BEAST 

We are of as much consequence to an army, as wind to a windmill: the wings can't be put in motion without us. The Battle of Hexham; |George Colman 

And on a slight rise, but so concealed from him by the willows that only the great wings showed, stood the windmill. The Amazing Interlude |Mary Roberts Rinehart 

The mills were presumably driven for the most part by water, though we have a reference to a windmill as early as the year 833. Encyclopaedia Britannica, 11th Edition, Volume 4, Part 3 |Various 

The windmill shown in the sketch is one that will always face the wind, and it never requires adjustment. The Boy Mechanic, Book 2 |Various 

With your colors to wear, I shall have the honor of breaking a lance against the biggest windmill in the world. In Search of the Unknown |Robert W. Chambers