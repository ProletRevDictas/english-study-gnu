Before his swift relief by senior officers on Friday, Scheller held battalion command. The Botched Afghanistan Withdrawal Exposes a Dangerous Fault Line in Our Democracy |Elliot Ackerman |August 31, 2021 |Time 

At Camp Lejeune, the grunts of an experimental infantry battalion tested gear, including the Drone 40, to see what the Corps could learn, and if the Corps could adapt it for use in the future. The US Marines are testing flying, remote-controlled grenades |Rob Verger |July 13, 2021 |Popular-Science 

Starship, with its 100-ton capacity, can land a battalion of robots. The Profound Potential of Elon Musk’s New Rocket - Issue 100: Outsiders |Robert Zubrin |May 12, 2021 |Nautilus 

The parks’ beloved dining scene had largely been collapsed into an app-based meal-pickup service, and a battalion of Disney characters wandered around carrying signs reminding guests to wear their masks over their nose and mouth. For the first wave of vaccinated vacationers, venturing into the not-quite-open-world was quiet, cheaper — and a relief |Ashley Fetters |April 9, 2021 |Washington Post 

Michael Regner, the battalion commander who reported to Bronzi, also was relieved of command. Deadly Marine Corps disaster at sea was ‘tragic’ and ‘preventable,’ investigation finds |Dan Lamothe |March 26, 2021 |Washington Post 

A battalion of riot police armed with shotguns  arrived on the scene. Honoring The Late John Doar, A Nearly Forgotten Hero Of The Civil Rights Era |Gary May |November 15, 2014 |DAILY BEAST 

Whether Ukrainian authorities have actually investigated the claims against the battalion's commander and its officers is unclear. Corruption Eats Away at Ukraine Military |Charles McPhedran |October 21, 2014 |DAILY BEAST 

Sergei is not the only soldier who has complained about the conduct of the commander and the officers in the 12th Battalion. Corruption Eats Away at Ukraine Military |Charles McPhedran |October 21, 2014 |DAILY BEAST 

Fighter Sergei was hopeful that judicial investigations have begun into the 12th Battalion when I first met him in September. Corruption Eats Away at Ukraine Military |Charles McPhedran |October 21, 2014 |DAILY BEAST 

The battalion also is short of ammunition, Sergei says, but he insists he would still like to return to battle. Corruption Eats Away at Ukraine Military |Charles McPhedran |October 21, 2014 |DAILY BEAST 

But one battalion was isolated on a spur, from which there seemed no way of escape save under a scorching flank fire. Napoleon's Marshals |R. P. Dunn-Pattison 

Two-thirds of each Battalion were sound asleep in pools of mud and water—like corpses half buried! Gallipoli Diary, Volume I |Ian Hamilton 

On my way back to the beach I saw the Plymouth Battalion as it marched in from the front line. Gallipoli Diary, Volume I |Ian Hamilton 

Lannes enlisted in the second battalion of the volunteers of Gers, and was at once elected sub-lieutenant by his fellow-citizens. Napoleon's Marshals |R. P. Dunn-Pattison 

Finally a second battalion of infantry arrived under the command of Colonel Stotsenberg, who was very popular with his men. The Philippine Islands |John Foreman