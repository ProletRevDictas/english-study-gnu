Frost and Central Florida finished 13-0 and mastered Auburn in a New Year’s Six game. Once obvious and thrilling, three college football coaching hires have lost their luster |Chuck Culpepper |November 30, 2020 |Washington Post 

Lows range through the 30s with frost likely in our colder suburbs. D.C.-area forecast: After up to 4 inches, rain eases this afternoon; skies brighten Friday through the weekend |David Streit |November 12, 2020 |Washington Post 

Low temperatures dip into the mid- to upper 30s, but thanks to the well-stirred atmosphere, we shouldn’t have widespread frosts or freezes. D.C.-area forecast: Raindrops end this morning, then it gradually clears with breezy conditions |A. Camden Walker |October 30, 2020 |Washington Post 

The top Brexit negotiators — for the EU, Michel Barnier, and for the UK, David Frost — said they would talk early next week, though Frost told Barnier not to come to London unless the EU has a new plan, according to the Guardian. The EU and the UK still haven’t reached a post-Brexit agreement. What’s next? |Jen Kirby |October 16, 2020 |Vox 

The category is projected to be worth $50 billion by 2025, according to Frost and Sullivan, a marketing consulting firm. Everything You Need to Know About Period Tracking |Christine Yu |September 6, 2020 |Outside Online 

The lyrical declamation has inspired luminaries ranging from Kurt Vonnegut to Robert Frost. James Franco and Scott Haze on 'The Sound and the Fury' and Gawker 'Outing' Them As A 'Couple' |Marlow Stern |September 6, 2014 |DAILY BEAST 

And, eventually, who repented – famously on television during a remarkable series of interviews with David Frost. Three Dicks: Cheney, Nixon, Richard III and the Art of Reputation Rehab |Clive Irving |July 27, 2014 |DAILY BEAST 

You'd put a scarf across your nose and mouth and when you breathed through it, it would get all white with frost. Gordie Howe Hockey’s Greatest War Horse |W.C. Heinz |May 31, 2014 |DAILY BEAST 

Whereas Lane Frost more or less lived the life that embodied the ideal. The Death of a Rodeo Cowboy |Peter Richmond |May 11, 2014 |DAILY BEAST 

In 1987, Lane Frost won the championship of bullriding, and won the biggest buckle you can win. The Death of a Rodeo Cowboy |Peter Richmond |May 11, 2014 |DAILY BEAST 

In these archipelagos the waters being shallow, the frost was quite intense enough to cool them to the bottom. The Giant of the North |R.M. Ballantyne 

He shall pour frost as salt upon the earth: and when it freezeth, it shall become like the tops of thistles. The Bible, Douay-Rheims Version |Various 

Moreover, though a land of frost, it is very windy, the wind being nearly always a cold one. The Jesuit Relations and Allied Documents, Vol. II: Acadia, 1612-1614 |Various 

The day had been intensely cold, with a biting north-east wind and black frost. The World Before Them |Susanna Moodie 

Before morning old Jack Frost snapped his fingers and the whole world was encased in ice. The Girls of Central High on the Stage |Gertrude W. Morrison