He has her pegged as a posh, silly Englishwoman before they’ve even spoken. Amazon’s Divine Period Romance The Pursuit of Love Gives Classic Social Satire a Modern Twist |Judy Berman |July 30, 2021 |Time 

He soon met an Englishwoman named Valerie Traverso, who was already pregnant with a child by her husband, Michael Macias. Britain’s Most Notorious Islamist Once Worked at Strip Clubs and Peep Shows in London |Nico Hines |May 8, 2014 |DAILY BEAST 

But the photographer Cecil Beaton said Maxime was the only truly chic Englishwoman of her generation. Remembering YSL's Muse |Alice Cavanagh |November 7, 2011 |DAILY BEAST 

To think,” said the younger Englishwoman to her sister, “of this wee mite travelling about in an open motor! The Joyous Adventures of Aristide Pujol |William J. Locke 

Miss Thangue sat forward with the frank curiosity of the Englishwoman when inspecting a foreign specimen. Ancestors |Gertrude Atherton 

She spoke calmly, with the pleasantly modulated voice of a well-bred Englishwoman. The Red Year |Louis Tracy 

Fortunately Marie found in a young Englishwoman a treasure of knowledge, intelligence, and kindheartedness. The Seven Cardinal Sins: Envy and Indolence |Eugne Sue 

He lay in the Englishwoman's gentle arms—a little brown bundle of flexile limbs and cotton night-shirt. With Edged Tools |Henry Seton Merriman