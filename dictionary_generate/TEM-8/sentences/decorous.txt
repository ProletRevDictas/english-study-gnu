Not for Rodriguez the decorous, red-carpet likes of Gwynnie, Sandra Bullock, or Tom Cruise, but “hot criminal” Jeremy Meeks. Meet the PR Guru for the ‘Hot Convict,’ the Octomom, and Every Other D-List Trainwreck |Erin Cunningham |July 17, 2014 |DAILY BEAST 

In more decorous terms, Manchin and Huntsman came to much the same conclusion. Jon Huntsman and Joe Manchin’s No Labels Bromance |David Freedlander |January 15, 2013 |DAILY BEAST 

A decorous group of nine panelists presented their positions one at a time, following distinctly un-Israeli rules of etiquette. Israel’s New Election Discourse |Don Futterman |January 8, 2013 |DAILY BEAST 

I never knew him to take his time, squander words to be merely decorous. Christopher Hitchens Eulogized by Roya Hakakian |Roya Hakakian |December 16, 2011 |DAILY BEAST 

As president, Obama must be more decorous, of course, than merely mocking the man. Obama's Finally Ready to Rumble |Eric Alterman |September 8, 2010 |DAILY BEAST 

He remembered how decorous and dignified was the Mogul court when Britain paid honor to an ancient dynasty. The Red Year |Louis Tracy 

His visions of a merry riot were all fled, and he was listening with the eagerness of a decorous Sunday-school child. The Chequers |James Runciman 

Half of the people had taken their seats when he began; there was a hasty scramble, and a decorous, half-checked smile. Hilda |Sarah Jeanette Duncan 

The past has come down to us cloaked and shrouded, and attended by its decorous retinue of mutes and bearers. A Cursory History of Swearing |Julian Sharman 

Let us turn into the British Museum and see sensible, decorous Boxing-day there. Mystic London: |Charles Maurice Davies