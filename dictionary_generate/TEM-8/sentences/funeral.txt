López told the Blade her mother and Huerta’s brothers were able to attend the funeral. Tijuana authorities criticized over handling of transgender woman’s murder |Michael K. Lavers |September 17, 2020 |Washington Blade 

Horrifically, her brother died while she was at the funeral service for her father. The Last Bar Standing? |Eugene Robinson |September 15, 2020 |Ozy 

From Monday, all social gatherings of more than six people will become illegal across England, not including gatherings at workplaces, schools, weddings, funerals, and organized team sports. As the U.K.’s coronavirus testing system struggles, the health secretary blames too many ‘inappropriate’ tests |David Meyer |September 9, 2020 |Fortune 

The Dodgers, who had broken the league’s color barrier by signing Jackie Robinson in 1947, were the last holdouts, and the team’s front office figured King’s funeral would be over by the time its game began. Why A Strike For Racial Justice Started With The Milwaukee Bucks And The NBA |Neil Paine (neil.paine@fivethirtyeight.com) |August 27, 2020 |FiveThirtyEight 

Neither she nor her parents could attend the small funeral for fear of the virus. Coronavirus Hit Latinos Harder Thanks to a Perfect Storm of Disparities |Maya Srikrishnan |August 12, 2020 |Voice of San Diego 

Yet even after the funeral protest, de Blasio was booed and heckled while addressing a new class of recruits as well. We Need Our Police to Be Better Than This |Nick Gillespie |December 31, 2014 |DAILY BEAST 

Those who are not working on Sunday will almost certainly attend the funeral for Liu. Cop Families Boo De Blasio at NYPD Graduation |Michael Daly |December 30, 2014 |DAILY BEAST 

The truest words at the funeral were those of young Jaden, as quoted by the governor. Choking Back Tears, Thousands of Cops Honor Fallen Officer Ramos |Michael Daly |December 28, 2014 |DAILY BEAST 

Castro actually flew up to Montreal to be a pallbearer at the 2000 funeral of a beloved Canadian Prime Minister, Pierre Trudeau. Canada ♥ Cuba Just Got Complicated |Shinan Govani |December 22, 2014 |DAILY BEAST 

As far as he is concerned, they're preparing his obituary and he doesn't care to attend the funeral. Alfred Hitchcock’s Fade to Black: The Great Director’s Final Days |David Freeman |December 13, 2014 |DAILY BEAST 

He had his beautiful robe under him (for it was summer), and was preparing for his funeral oration. The Jesuit Relations and Allied Documents, Vol. II: Acadia, 1612-1614 |Various 

When the funeral was over, and they returned to their desolate home, at the sight of the empty cradle Ramona broke down. Ramona |Helen Hunt Jackson 

When the last scarlo is burned out a funeral march is played and all disperse to their homes. The Pit Town Coronet, Volume I (of 3) |Charles James Wills 

His funeral, I need not say, was attended by railway men from all parts of the kingdom. Fifty Years of Railway Life in England, Scotland and Ireland |Joseph Tatlow 

But, Mandy Ann held her back and whispered, "Can't you done 'have yerself at yer mammy's funeral an' we the only mourners?" The Cromptons |Mary J. Holmes