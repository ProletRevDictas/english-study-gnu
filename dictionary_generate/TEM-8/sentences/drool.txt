It’s basically a long flexible arm that increases your chucking power—plus it grips the slimy ball for you, so you don’t have to touch a bunch of drool. What to give your very good dog this holiday season |Sara Chodosh |November 17, 2021 |Popular-Science 

So people will drool at handsome photos of him in BuzzFeed posts. ‘Graceland’ Star Aaron Tveit Is TV’s Next Big Heartthrob |Kevin Fallon |June 3, 2013 |DAILY BEAST 

My friends all seemed to drool over churlish boy-band types. Sugar Daddy Dating Sites: Helen Croydon on Her Guilty Fantasy |Helen Croydon |May 11, 2013 |DAILY BEAST 

Add social media to a collection of pictures of finely honed muscles and you have a drool-worthy match in made in heaven. The Olympics or Soft Porn? Female, Gay Fans Gawking at Male Athletes |Tricia Romano |August 3, 2012 |DAILY BEAST 

Having an enormous kitchen that would make even the most pampered chefs drool helped. A Hollywood Host Takes on QVC |Nicole LaPorte |November 29, 2009 |DAILY BEAST 

On Friday, Verizon Wireless began selling the Motorola Droid, the most drool-worthy Android phone to date. Google's Drool-Worthy Smartphone Software |Nicholas Ciarelli |November 8, 2009 |DAILY BEAST 

Everybody does go batty that's high-brow and studies and all that drool. Under the Law |Edwina Stanton Babcock 

I've heard Taylor drool about his pet guest—lady in black, strangled in attic by jealous husband. The Gray Mask |Wadsworth Camp 

Stewards, three days out, are not in the habit of falling in love with their charges (Maundering and Drool notwithstanding). The Voice in the Fog |Harold MacGrath 

He did not drool his words, hanging one with doubtful hesitation upon another, but blew them out like a mouthful of smoke. A Yankee from the West |Opie Read 

Well, give me madmen who drool spittle, and foam at the mouth, and shriek obscene blasphemies. He Walked Around the Horses |Henry Beam Piper