That Republicans are running ads attacking expanded government health care as socialism while offering nothing serious of their own amid a pandemic says it all. A disgusting GOP attack ad shows what’s really at stake in Georgia |Greg Sargent |December 18, 2020 |Washington Post 

Moderates blame liberals for promoting socialism and proposals to “defund the police.” Democrats, nearing a moment of triumph, still feel anxious and divided |Sean Sullivan |November 10, 2020 |Washington Post 

In one of three ads unveiled Thursday, participants in a roundtable discussion tout Trump’s stewardship of the economy, with one woman describing him as “the only barrier between us and socialism.” Election live updates: Trump returns to Wisconsin; Biden to face live audience at town hall |Colby Itkowitz, Felicia Sonmez, John Wagner |September 17, 2020 |Washington Post 

This may sound like nomenclatural hair-splitting to you — the difference between a “social democracy” and “democratic socialism” — but Sachs argues otherwise. Does Anyone Really Know What Socialism Is? (Ep. 408 Rebroadcast) |Stephen J. Dubner |September 17, 2020 |Freakonomics 

She believed in the hopeful socialism of the early 20th century, when people who thought that equality was within their grasp if only they could undermine the capitalist system. Suffragette City: San Diego’s Crucial Role in Getting Women the Vote |Randy Dotinga |August 6, 2020 |Voice of San Diego 

He expected European capitalism to evolve spontaneously into a market socialism of worker-owned cooperatives. American Democracy Under Threat for 250 Years |Jedediah Purdy |December 28, 2014 |DAILY BEAST 

So a United States senator has no idea what "national socialism" means. Dole, Nazis, and Desperation in Kansas |Michael Tomasky |September 26, 2014 |DAILY BEAST 

But at any rate, we have to change course because our country is headed for national socialism. Dole, Nazis, and Desperation in Kansas |Michael Tomasky |September 26, 2014 |DAILY BEAST 

These, he insisted, were harbingers of the twin plagues of socialism and secular humanism. What I Saw at Iowa’s So-Co Circus |Ben Jacobs |August 10, 2014 |DAILY BEAST 

“Welfarism,” as Goldwater constantly called welfare, was the path to socialism. Barry Goldwater, Father of the Tea Party |Nicolaus Mills |July 16, 2014 |DAILY BEAST 

Now many people think that we are moving in the direction of world socialism to-day. The Salvaging Of Civilisation |H. G. (Herbert George) Wells 

Many people when presented with the argument above, would settle it at once with the word "socialism." The Unsolved Riddle of Social Justice |Stephen Leacock 

Thus seen, socialism appeared as the very antithesis of law and order, of love and chastity, and of religion itself. The Unsolved Riddle of Social Justice |Stephen Leacock 

It remained for the age of machinery and power to bring forth another and a vastly more potent socialism. The Unsolved Riddle of Social Justice |Stephen Leacock 

Socialism, like every other impassioned human effort, will flourish best under martyrdom. The Unsolved Riddle of Social Justice |Stephen Leacock