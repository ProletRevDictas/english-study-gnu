Because tech stocks have been soaring to new highs lately, investors should have no illusions about the cost of the shares they’re considering buying. Investors riding high on Apple and Tesla stock splits could get clipped, data shows |rhhackettfortune |August 31, 2020 |Fortune 

To give my students the illusion of eye contact, I learned to stare at the green light on my MacBook Air. That chatbot I’ve loved to hate |Tate Ryan-Mosley |August 19, 2020 |MIT Technology Review 

Several more recent studies conducted on individuals living in war and earthquake zones mirror Sosis’ finding that rituals give participants a sense — or a comforting illusion — of control over the uncontrollable. Why do we miss the rituals put on hold by the COVID-19 pandemic? |Sujata Gupta |August 14, 2020 |Science News 

The team wanted the site to look as Mars-like as possible, no factories, footprints or foliage to break the illusion. To rehearse Perseverance’s mission, scientists pretended to be a Mars rover |Lisa Grossman |July 29, 2020 |Science News 

We should be looking at these if we have any illusions of sending kids back to school. How To Make Indoor Air Safer |Kaleigh Rogers (kaleigh.rogers@fivethirtyeight.com) |July 20, 2020 |FiveThirtyEight 

Traditional coach seats gave the illusion of comfortable padding but were angular, not reflecting body shapes. Flying Coach Is the New Hell: How Airlines Engineer You Out of Room |Clive Irving |November 25, 2014 |DAILY BEAST 

And we are under no illusion that this state of affairs is confined to one battalion. Corruption Eats Away at Ukraine Military |Charles McPhedran |October 21, 2014 |DAILY BEAST 

You said, “freedom of speech is an illusion” and “freedom of assembly is an illusion.” Julian Casablancas Enters the Void: On the Strokes’ Friction, Why He Left NYC, and Starting Over |Marlow Stern |October 9, 2014 |DAILY BEAST 

Democracy is an illusion, freedom of speech is an illusion, freedom of assembly is an illusion. Julian Casablancas Enters the Void: On the Strokes’ Friction, Why He Left NYC, and Starting Over |Marlow Stern |October 9, 2014 |DAILY BEAST 

On the surface, In Situ appears less disruptive than its alternative, but this is only an illusion. Our Trip to The Climate War's Ground Zero |Darren Aronofsky |September 19, 2014 |DAILY BEAST 

But the growing crops are too cleanly and carefully weeded and too uniformly good to protract the illusion. Glances at Europe |Horace Greeley 

All this I admit to be the fever of the mind—a waking dream—an illusion to which mesmerism or magic is but a frivolity. Blackwood's Edinburgh Magazine, No. CCCXXXIX. January, 1844. Vol. LV. |Various 

The burning atmosphere, the motionless air caused doubtless the optical illusion. The Wave |Algernon Blackwood 

Nor wilt thou rest forever, weary heart.The last illusion is destroyed,That I eternal thought. The Poems of Giacomo Leopardi |Giacomo Leopardi 

From the first entrance, to the last cry of triumph or despair, the illusion was perfect. The Life &amp; Letters of Peter Ilich Tchaikovsky |Modeste Tchaikovsky