I cherish this little holiday listening ritual, but it still felt too new, too insignificant to mention back in the autumn of 2018 when Cowell — who died Thursday at 79 — invited me into his Maryland home to talk about his music. Stanley Cowell navigated the vastness of jazz a few notes at a time |Chris Richards |December 18, 2020 |Washington Post 

That said, many a gamer will cherish the idea of playing the latest in this venerable series day one, so pre-ordering a copy is a possibility if none of the other games really ring their bell. Gift Guide: Games on every platform to get you through the long, COVID winter |Devin Coldewey |December 3, 2020 |TechCrunch 

To pay for it, she took aim at a tax break cherished by the private equity industry. Susan Collins Backed Down From a Fight with Private Equity. Now They’re Underwriting Her Reelection. |by Justin Elliott, ProPublica, and Theodoric Meyer, Politico |October 29, 2020 |ProPublica 

After much soul searching, lengthy discussions and extensive evaluations of our long-term goals, my family and I decided this was the right time to pass our responsibility and cherished stewardship. The Utah Jazz will have a new owner for the first time in 35 years |Ben Golliver |October 28, 2020 |Washington Post 

Many of you are my sources, subjects, friends, confidantes, dinner companions, onstage dueling partners, cherished critics, and more. A goodbye after 19 years |Adam Lashinsky |October 5, 2020 |Fortune 

Special praise goes to Kudrow for the way she broadened the scope of Valerie Cherish in Season 2. ‘The Comeback’ Finale: Give Lisa Kudrow All of the Awards |Kevin Fallon |December 29, 2014 |DAILY BEAST 

But alas, a snub is yet another of the many indignities Valerie Cherish shall endure. 15 Enraging Golden Globe TV Snubs and Surprises: Amy Poehler, 'Mad Men' & More |Kevin Fallon |December 11, 2014 |DAILY BEAST 

The goal offered ecstasy to free-kick aficionados, who have had little to cherish at this World Cup. Brazil and Colombia Bring the Ugly Game |Tunku Varadarajan |July 4, 2014 |DAILY BEAST 

They seem to cherish a strange, irrational notion that something in the very flow of time will cure all ills. Alex Haley’s 1965 Playboy Interview with Rev. Martin Luther King Jr. |Alex Haley |January 19, 2014 |DAILY BEAST 

It's because some hearing people cherish those experiences so much and want to know that others share them. This Is What It Is Like To Be Deaf From Birth |Quora Contributor |December 23, 2013 |DAILY BEAST 

This contempt for the masses they cherish until they have to descend from Parnassus and enter the public service. Skipper Worse |Alexander Lange Kielland 

Had Mr. Wilding been other than she now learnt he was, he would surely not cherish an attachment for a person so utterly unworthy. Mistress Wilding |Rafael Sabatini 

She was careful to cherish in herself an openness to noble impressions and to the high poetry of nature and life. Marriage la mode |Mrs. Humphry Ward 

Kitty Tynan had certainly enough imagination to make her cherish a mystery. You Never Know Your Luck, Complete |Gilbert Parker 

The wolf is capable of strong attachments, and has been known to cherish the memory of a friend for a great length of time. Stories about Animals: with Pictures to Match |Francis C. Woodworth