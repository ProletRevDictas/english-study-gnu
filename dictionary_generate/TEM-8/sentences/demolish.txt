LUCA’s competitors may have survived some time as fossils, but the planet’s tectonic churn has long since demolished its earliest rocks. Your ancestors might have been Martians |Charlie Wood |February 12, 2021 |Popular-Science 

In 2014 I wrote about a rusty metal box that Anne Smith and Malcolm Bertoni found when they demolished a brick barbecue in their Cleveland Park backyard. A thrift shop photo album is full of mystery photos of a District family |John Kelly |January 27, 2021 |Washington Post 

Epic is purchasing the 980,000-square-foot Cary Towne Center, which sits on 87 acres, from developers who had previously announced plans to demolish the mall and build a new, mixed-use development. Fortnite’s parent company is turning a North Carolina mall into its headquarters |Karen Ho |January 7, 2021 |Quartz 

Greig said his family, which has owned the property for the past two decades, can barely afford to demolish the home, let alone move it back on the property line. How Famous Surfers and Wealthy Homeowners Are Endangering Hawaii’s Beaches |by Sophie Cocke, Honolulu Star-Advertiser |December 5, 2020 |ProPublica 

That demolished Delaware’s record for Democratic primary turnout, set in 2008, when Barack Obama defeated Hillary Clinton in the race for the small state’s delegates. Trump contradicts CDC director on vaccine; Biden says Americans shouldn’t trust Trump |Colby Itkowitz, Felicia Sonmez, John Wagner |September 16, 2020 |Washington Post 

Eri Hayward is cheerful, even as she drops verbal bombs that demolish mainstream conceptions about being transgender. Thank God! To the Church, This Transgender Woman Is Just a Skank |Emily Shire |October 22, 2014 |DAILY BEAST 

It will stir things up, but it will not begin to demolish the group. The Case of the Yakking Yakuza |Jake Adelstein |September 16, 2014 |DAILY BEAST 

But if the Arab states mustered the will, they could demolish ISIS, as history has shown. How a Real Air War Could Demolish ISIS |Clive Irving |August 23, 2014 |DAILY BEAST 

They need to demolish their homes and round them up, the way they do it to our children. The Seeds of the Next Intifada |Jesse Rosenfeld |July 7, 2014 |DAILY BEAST 

The sale has hit some snags, but the lease also gives El-Gamel the right to demolish the building. Why the Ground Zero Mosque Is Worth Saving |Michael Daly |April 11, 2014 |DAILY BEAST 

I did not demolish my hut of pearl shells, but left it standing exactly as it had been during the past two and a half years. The Adventures of Louis de Rougemont |Louis de Rougemont 

We then proceeded to demolish everything in sight except the boxes. The Opened Shutters |Clara Louise Burnham 

Hans raged and made as if to demolish the heart, and Gretchen, and indeed the whole dyke, but then he thought of something better. Seeing Things at Night |Heywood Broun 

They did not, however, demolish mine; on the contrary, they supplied another and a very curious link in the chain of evidence. Secret Societies And Subversive Movements |Nesta H. Webster 

These scavengers demolish an incredible amount of meat and blubber in a short time. The Home of the Blizzard |Douglas Mawson