Altogether, the team watched the brain activity of 39 people as they grappled with sticking with their choice or succumbing to peer pressure. This Is How Your Brain Responds to Social Influence |Shelly Fan |August 25, 2020 |Singularity Hub 

Put an Angus bull on a tropical pasture and “he’s probably going to last maybe a month before he succumbs to the environment,” says Oatley, while a Nelore bull carrying Angus sperm would have no problem with the climate. Biotechnology Could Change the Cattle Industry. Will It Succeed? |Dyllan Furness |August 16, 2020 |Singularity Hub 

Benedict herself succumbed to heart trouble several years later, in 1948. Gender Is What You Make of It - Issue 88: Love & Sex |Charles King |August 5, 2020 |Nautilus 

Instead of succumbing to the frog’s digestive juices, an eaten Regimbartia attenuata traverses the amphibian’s throat, swims through the stomach, slides along the intestines and climbs out the frog’s butt, alive and well. Water beetles can live on after being eaten and excreted by a frog |Jonathan Lambert |August 3, 2020 |Science News 

In the 19th century, soldiers sent to Haiti by Napoleon Bonaparte to quash rebellion succumbed to yellow fever, leading to Haitian independence and Napoleon’s sale of the territory of Louisiana to the United States. History reveals how societies survive plagues |Nancy Shute |June 14, 2020 |Science News 

Salia was the third patient to be treated in Nebraska, but the first to succumb to the disease. Was Flying Hero Doctor With Ebola to the U.S. the Wrong Call? |Abby Haglage |November 17, 2014 |DAILY BEAST 

Because we are surrounded by a world that demands we submit, succumb, and believe in nothing. The NRA’s Multimillion-Dollar New Ad Campaign Is Despicable |Michael Daly |September 8, 2014 |DAILY BEAST 

In the 21st century, however, we suppress the magic of it and succumb to the fear of it. Twin Disasters Turn 2014 Into the Year of Flying Dangerously |Clive Irving |July 19, 2014 |DAILY BEAST 

We've already seen Don resist temptation, and succumb to it, and resist it again. Mad Men’s Dramatic Déjà Vu: ‘Time Zones’ Feels Redundant |Andrew Romano |April 14, 2014 |DAILY BEAST 

Depending on the strain, anywhere from 50% to 90% of patients succumb within two weeks of infection. Already Deadly in Africa, Could Ebola Hit America Next? |Scott Bixby |April 5, 2014 |DAILY BEAST 

And yet we must go on in one direction or the other or else succumb to sheer lassitude and overpowering drowsiness. Mount Everest the Reconnaissance, 1921 |Charles Kenneth Howard-Bury 

In short, was she or was she not the sort of woman to succumb to his attack? The Child of Pleasure |Gabriele D'Annunzio 

The people of the expedition must either conquer or succumb. The Tiger Hunter |Mayne Reid 

By the time we had begun our examination Mary began to succumb to her mother's suggestions, and began to feel a trifle indisposed. The Mother and Her Child |William S. Sadler 

In the fort of Attock, Captain Herbert held out for a while, but in the end was forced to succumb. A History of the Nineteenth Century, Year by Year |Edwin Emerson