He wears a mask in hospitals, he says, not for himself but for others. In 160 words, Trump reveals how little he cares about the pandemic |Philip Bump |September 17, 2020 |Washington Post 

When Paul grew ill, Ryan, a hospital social worker, knew something was up. A Welcome Lifeline |Washington Regional Transplant Community |September 17, 2020 |Washington Blade 

By “equal visitation,” the statement was referring to a hospital policy that allows same-sex partners to have the same visitation privileges as legal family members to visit someone in the hospital. HRC examines hospital policies, impact of COVID on LGBTQ people |Lou Chibbaro Jr. |September 16, 2020 |Washington Blade 

Recommending against civilians wearing high-quality masks meant more high-quality masks could be sent to hospitals facing a flood of infected patients. Parsing Trump’s baffling, head-slapping comments on mask-wearing |Philip Bump |September 16, 2020 |Washington Post 

Federal regulators focus on this statistic to evaluate — and sometimes penalize — transplant programs, giving hospitals across the country a reputational and financial incentive to game it. ProPublica Investigation on Newark Hospital Transplant Team Wins Deadline Club Award |by ProPublica |September 15, 2020 |ProPublica 

At St. Barnabas Hospital, Pellerano was listed in stable condition with wounds to his chest and arm. Shot Down During the NYPD Slowdown |Michael Daly |January 7, 2015 |DAILY BEAST 

When the father arrived at the hospital, he was told that Andrew Dossi was in surgery, but the wounds were not life-threatening. Shot Down During the NYPD Slowdown |Michael Daly |January 7, 2015 |DAILY BEAST 

By the time the ambulance arrived, over 10 minutes later, it was too late—Mills died soon after arriving at the hospital. Red Tape Is Strangling Good Samaritans |Philip K. Howard |December 27, 2014 |DAILY BEAST 

At Woodhull Hospital, the Bed-Stuy ambulance crew kept doing all they could as they wheeled Ramos into the emergency room. 'Please Don't Die!': The Frantic Battle to Save Murdered Cops |Michael Daly |December 22, 2014 |DAILY BEAST 

Both officers were rushed to Woodhull Hospital where they were pronounced dead. Alleged Cop Killer Ismaaiyl Brinsley Had a Death Wish |M.L. Nestel |December 22, 2014 |DAILY BEAST 

On the night of June the 11th a red-hot cannon-ball set fire to one of the barracks which was used as a hospital. The Red Year |Louis Tracy 

At the pier at "W" were several big lighters filled with wounded who were about to be towed out to Hospital ships. Gallipoli Diary, Volume I |Ian Hamilton 

One or two priests of that order live in the hospital, and two others, lay brethren, act as nurses. The Philippine Islands, 1493-1898, Volume XX, 1621-1624 |Various 

The hospital is in charge of a steward appointed by the governor, and is administered by the Order of St. Francis. The Philippine Islands, 1493-1898, Volume XX, 1621-1624 |Various 

Half the army was in hospital from want of proper nourishment and commonsense sanitation. Napoleon's Marshals |R. P. Dunn-Pattison