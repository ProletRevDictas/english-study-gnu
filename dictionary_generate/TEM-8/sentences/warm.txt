Saturday wintry mix potentialSaturday brings the potential for snow and ice to the region as a low-pressure area forms off the coast of the Carolinas, and relatively warm air rides up and over a dome of cold air. Snow expected tonight, mainly south of D.C., before possible ice on Saturday |Jason Samenow, Wes Junker |February 11, 2021 |Washington Post 

The blanket’s heat can be adjusted to accommodate warm, low, medium, and high heat settings. Best heated throw blanket: Bundle up with these electric blankets |PopSci Commerce Team |February 11, 2021 |Popular-Science 

That means we’re not talking about an Earth-like world but a warm gas planet five to seven times larger than Earth. There’s a tantalizing sign of a habitable-zone planet in Alpha Centauri |Neel Patel |February 10, 2021 |MIT Technology Review 

They’re perfect for a day on the mountain and stay warm between two-and-a-half to six hours. Best hand warmers: Block the chill during your favorite winter activities |PopSci Commerce Team |February 10, 2021 |Popular-Science 

In seconds, the heating elements warm up the vest, which can be adjusted according to your comfort. Best heated vest: Beat the cold weather with the right winter gear |PopSci Commerce Team |February 9, 2021 |Popular-Science 

Warm milk mixed with a spoonful of fireplace ashes seemed to also be popular among 19th century England. History's Craziest Hangover Cures |Justin Jones |December 30, 2014 |DAILY BEAST 

It was doubtless a warm reunion with his family, who are featured in The Cuban Wives. Of Cuban Spies, a Baby, and a Filmmaker: The Strange Tale of the Cuban Five |Nina Strochlic |December 28, 2014 |DAILY BEAST 

Serve with the warm sauce and your choice of ice cream, whipped cream, or yogurt. Make ‘The Chew’s’ Carla Hall’s Sticky Toffee Pudding |Carla Hall |December 28, 2014 |DAILY BEAST 

Dinner was a baroque affair, on the beach, a warm breeze gently blowing. Canada ♥ Cuba Just Got Complicated |Shinan Govani |December 22, 2014 |DAILY BEAST 

In the House, Republicans passed the budget by 219-206 with the warm-hearted help of 57 Democrats. Congress’ Gift That Keeps on Giving |P. J. O’Rourke |December 20, 2014 |DAILY BEAST 

This has a warm though a thin soil, which must be highly favorable to the Vine to induce so exclusive a devotion to it. Glances at Europe |Horace Greeley 

It was very warm, and for a while they did nothing but exchange remarks about the heat, the sun, the glare. The Awakening and Selected Short Stories |Kate Chopin 

Decomposition sets in rapidly, especially in warm weather, and greatly interferes with all the examinations. A Manual of Clinical Diagnosis |James Campbell Todd 

She may be as chaste as unsunned snow, she is certainly as cold: but for warm, inspiring virtue! The Pastor's Fire-side Vol. 3 of 4 |Jane Porter 

Why, he ordered his chamber-maid to bring him some soap and warm water, that he might wash the sour krout off his hands. The Book of Anecdotes and Budget of Fun; |Various