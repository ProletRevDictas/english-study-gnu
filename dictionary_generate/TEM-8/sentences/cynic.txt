The cynics, meanwhile, shorted GME to heights rarely seen in the past decade—to well north of 100% of the stock’s total float. Bitcoin, stocks and crude take off as the markets brace for a wave of stimulus checks |Bernhard Warner |February 8, 2021 |Fortune 

In Wilde’s day, the cynic and sentimentalist characters made for good theater. Bitcoin, stocks and crude take off as the markets brace for a wave of stimulus checks |Bernhard Warner |February 8, 2021 |Fortune 

A cynic would say this is a way for the company to drive product adoption until it has a large, installed base of users. Facebook launches free ‘paid online events’ for SMBs and others |Greg Sterling |August 14, 2020 |Search Engine Land 

The world, unfortunately, or some people—cynics—the cynical blogosphere at least—still sees fitness equipment as rote weight loss. Peloton CEO John Foley Talks Cheaper Bikes and Making the Most of Staying Home |Eben Shapiro |May 21, 2020 |Time 

A cynic might wonder if the police were telling the whole truth. 5 Psychology Terms You’re Probably Misusing (Ep. 334 Rebroadcast) |Stephen J. Dubner |January 9, 2020 |Freakonomics 

You, dear reader and refusenik, will likely be called a cynic or a sad sack by friends. The Refuseniks Hiding From ‘Happy New Year’ |Lizzie Crocker |December 31, 2014 |DAILY BEAST 

A cynic might say that the report is like the movie Clue, perfectly set up for a multiplicity of endings. New York’s Conservative Fracking Ban |Jay Michaelson |December 20, 2014 |DAILY BEAST 

Though he made it back to the top by dint of talent and hard work, he remained a deep-dyed cynic for the rest of his life. The Stacks: Robin Williams, More Than A Shtick Figure |Joe Morgenstern |August 16, 2014 |DAILY BEAST 

Putin, after all, is not the only cynic on center stage in the Ukraine crisis. Putin Can Take Ukraine Without an Invasion, and Probably Will |Jamie Dettmer |April 12, 2014 |DAILY BEAST 

Likely, Harold would have accepted the short shrift with his usual cynic's grace. The Stacks: Harold Conrad Was Many Things, But He Was Never, Ever Dull |Mark Jacobson |March 8, 2014 |DAILY BEAST 

A cynic was Blondet, with little regard for glory undefiled. Repertory Of The Comedie Humaine, Complete, A -- Z |Anatole Cerfberr and Jules Franois Christophe 

Could I have been led to believe that the vile mockery of the cynic was applicable to one in my forlorn and desperate situation? My Ten Years' Imprisonment |Silvio Pellico 

I've met him; he's a bad-tempered hypochondriac, a cynic at heart, and a man whose word is never doubted. In Search of the Unknown |Robert W. Chambers 

The word cynic, too, comes from the name given to certain Greek philosophers who despised pleasure. Stories That Words Tell Us |Elizabeth O'Neill 

Suppose he is a cynic, it is to his interest to govern well. The Napoleon of Notting Hill |Gilbert K. Chesterton