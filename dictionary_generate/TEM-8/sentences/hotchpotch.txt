Whole volumes of that great hotchpotch of criticism are lost in the sandbanks of my treacherous memory for ever. Boon, The Mind of the Race, The Wild Asses of the Devil, and The Last Trump; |Herbert George Wells 

The intellectual basis had been lulled to sleep by that hotchpotch of convention and largeness that we call the Victorian Era. G. K. Chesterton, A Critical Study |Julius West 

The nature of the hotchpotch will be understood from a recital of some of its contents, in their chronological order. The World's Greatest Books - Volume 15 - Science |Various 

Many a poisonous hotchpotch hath evolved in our cellars: many an indescribable thing hath there been done. Thus Spake Zarathustra |Friedrich Nietzsche 

As old Simon said, his wife knew no rival in the art of preparing hotchpotch. The Underground City |Jules Verne