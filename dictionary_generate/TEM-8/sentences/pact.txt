We need aggressive government policies and trade pacts to push or pull clean technologies into the marketplace and support the development of the tools we don’t yet have or are far too expensive today. The pandemic taught us how not to deal with climate change |James Temple |January 1, 2021 |MIT Technology Review 

Kenya’s Building Bridges Initiative, which seeks sweeping amendments to the 2010 Constitution, is driven by a pact between president Uhuru Kenyatta and a seasoned opposition stalwart turned government ally, Raila Odinga. Kenyans should reject the latest round of proposed constitutional changes |Westen K Shilaho |December 7, 2020 |Quartz 

We made a pact to one another from the start that we would not skimp on any ingredients, which is something that happens too often in the industry. The skin care brand striving to make medical-grade topicals both more luxe and accessible |Rachel King |November 29, 2020 |Fortune 

The new pact does not include India, Asia’s second-largest economy. 15 countries signed the world’s biggest trade deal—but China is the clear winner |Naomi Xu Elegant |November 16, 2020 |Fortune 

On Wednesday, the United States is set to become the only nation to officially withdraw from an international pact aimed at slowing climate change. The U.S. will leave the Paris climate accord on Nov. 4. But voters will decide for how long. |Steven Mufson, Brady Dennis |October 30, 2020 |Washington Post 

The pact covered two months, September and October, but “may be extended by the parties,” the filing states. Exclusive: Did This Manhattan Firm Help Shield a Russian Fund From Sanctions? |Bill Conroy |November 10, 2014 |DAILY BEAST 

At the same time, the Warsaw Pact threat was disintegrating. How the Pentagon Strangles Its Most Advanced Stealth Warplanes |Bill Sweetman |October 13, 2014 |DAILY BEAST 

The British, he said, were too weak to survive and would have to settle for a pact with Hitler. Blood and War: The Hard Truth About ‘Boots on the Ground’ |Clive Irving |September 22, 2014 |DAILY BEAST 

Check out the app Pact, in which a community of fellow users will literally pay you to stick to your schedule. 4 Science-Backed Ways to Motivate Yourself to Work Out |DailyBurn |September 13, 2014 |DAILY BEAST 

When you reach your goal, you get paid out of a common pool funded by yourself and other pact-breakers. 4 Science-Backed Ways to Motivate Yourself to Work Out |DailyBurn |September 13, 2014 |DAILY BEAST 

The Federal Pact of 1815 had undone Napoleon's comparatively liberal constitution. The Life of Mazzini |Bolton King 

There was no majestic vision of a people rising in its own spontaneous might and deciding its destinies in a great national pact. The Life of Mazzini |Bolton King 

"A pact, my brother," said the man in the hunting-suit, extending his hand. The Secret Witness |George Gibbs 

The conditions which existed before the pact of Konopisht were no more. The Secret Witness |George Gibbs 

But in spite of this appeal and of a pact signed with the blood of the writer, no Satanic apparitions were forthcoming. Secret Societies And Subversive Movements |Nesta H. Webster