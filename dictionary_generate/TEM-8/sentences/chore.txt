The I-cook-you-clean rule is about fair distribution of chores. Carolyn Hax: How to improve on ‘I cook, you clean’ without stirring the pot |Carolyn Hax |February 12, 2021 |Washington Post 

The quotidian cycle of pandemic living had become overwhelming — and not just in an ugh, chores way. ‘Oh, we’re still in this.’ The pandemic wall is here. |Maura Judkis |February 9, 2021 |Washington Post 

We were constantly fighting over chores, meals, pickups and drop-offs, teacher's conferences and the like. Carolyn Hax: He dropped the bomb, and now he’s surprised about the fallout |Carolyn Hax |February 8, 2021 |Washington Post 

How I learned to stop worrying and love ice cream for dinnerA common joke is that the chore of pandemic cooking has reduced us to eating piles of slop. Six Months of Soft Food Set Me Free |Meghan McCarron |February 2, 2021 |Eater 

Nowadays, Calvario spends most of his days doing household chores, tending to his little sister while his mother and stepfather are at work or running errands. A steady stream of Latino students was arriving on college campuses. Then the pandemic hit. |Danielle Douglas-Gabriel, Hannah Natanson, John D. Harden |January 31, 2021 |Washington Post 

Make the chore a lot more fun with a super cute toothbrush holder. The Daily Beast’s 2014 Holiday Gift Guide: For the Blue Ivy in Your Life |Allison McNearney |November 29, 2014 |DAILY BEAST 

Instead of a chore, choose something you love so you absolutely look forward to it. Q&A With Designer Rachel Roy |Cynthia Allum |November 3, 2014 |DAILY BEAST 

That there was a way to tell the story and not have been leaden—not be a chore or an ordeal. Steve Coogan Makes His Bid For Some Serious, Dramatic Roles |Andrew Romano |November 29, 2013 |DAILY BEAST 

The act of keeping faith is a demanding chore, one that quarrels with the instinct to despair rather than continue the struggle. Defeating the Arab Spring Syndrome of Self-Defeat |Talal Alyan |October 15, 2013 |DAILY BEAST 

But for it to double its user base from the current level will be quite a chore, and may take several years. Gaming the Twitter IPO |Daniel Gross |October 4, 2013 |DAILY BEAST 

Things were not so bad until the pigs grew up, but now I dread feeding them more than any chore on the place. The Red Cow and Her Friends |Peter McArthur 

For me it was quite a chore to cut and carry up wood enough to keep our somewhat open upper room cosey and comfortable. Mary and I |Stephen Return Riggs 

When the collection reached ten thousand words and upward, it began to be quite a chore to make a new copy. Mary and I |Stephen Return Riggs 

Then I do put the hairpins in, to make them look like a water-wheel that the chore boy does build in the brook. The Story of Opal |Opal Whiteley 

The chore boy does have objects to my drawing pictures on his poker-chips that he does hide in the barn. The Story of Opal |Opal Whiteley