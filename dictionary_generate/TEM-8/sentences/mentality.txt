It’s not hard to see how a mentality that revolves around seeing what already works and replicating it came to dominate our industry. Taking your SEO content beyond the acquisition |Mordy Oberstein |February 2, 2021 |Search Engine Watch 

Duvernay-Tardif entered the job with what he described as a “medical student mentality,” focused on the treatment of patients. His team is going to the Super Bowl. He’s staying on the coronavirus front lines. |Adam Kilgore |February 1, 2021 |Washington Post 

That means pondering what you’d do if certain events happen with an if-this, then-that mentality. Stay calm under pressure with lessons learned in the world’s most stressful careers |Rob Verger |January 8, 2021 |Popular-Science 

You wonder if one had gone in and we’d been in front if the mentality of the last few minutes wouldn’t have been different. How do you lose to a team by 44 and then beat the same team the next day? Ask Army. |John Feinstein |January 6, 2021 |Washington Post 

In the regular season, the NFL could maintain its next-man-up mentality and plow ahead. The Cleveland Browns were an NFL feel-good story. Then the coronavirus got jealous. |Jerry Brewer |January 6, 2021 |Washington Post 

“After the New York mentality, it is the ultimate contrast to see people making things by hand,” he said. The Photographer Who Gave Up Manhattan for Marrakech |Liza Foreman |January 6, 2015 |DAILY BEAST 

Within the black community itself, the slave mentality is still being battled. Herbie Hancock Holds Forth |David Yaffe |November 8, 2014 |DAILY BEAST 

For Republicans, the sitzkreig mentality will only grow after the midterms, even and especially if they take the Senate. Why D.C. Wants an Election About Nothing |Nick Gillespie |October 23, 2014 |DAILY BEAST 

So does his comment about treason, which plugs into the mentality of those accusing the President of sedition and disloyalty. Paranoia Crept into American Political Life a Long Time Ago |Lewis Beale |October 19, 2014 |DAILY BEAST 

Many found this to echo a Stepford Wife mentality of women: Women like stories and language, not impersonal, cold, manly numbers! Girls Love Science. We Tell Them Not To. |Tauriq Moosa |July 17, 2014 |DAILY BEAST 

The high pitch represents mentality, the esthetic phases of beauty, and much brilliancy. Expressive Voice Culture |Jessie Eldridge Southwick 

And as bronze reflects the light, her mentality seemed to reflect all the cold lights in her nature. Bella Donna |Robert Hichens 

Supposed religious vocations, especially when of sudden development, are sometimes no more than an index of disturbed mentality. Essays In Pastoral Medicine |Austin Malley 

Hesitatingly she came forward, and Houston's dulled mentality at last took cognizance that a hand was extended slightly. The White Desert |Courtney Ryley Cooper 

But mentality and spirit cannot be bought—only labour and dexterity. The New Society |Walther Rathenau