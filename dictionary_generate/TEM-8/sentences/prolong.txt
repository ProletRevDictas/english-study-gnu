Unnecessarily prolong its impact upon children’s education, health and emotional well-being, and equitable access to opportunities for development and social success. Keeping older folks out of COVID-19 vaccine trials hurts everyone |Kat Eschner |October 1, 2020 |Popular-Science 

Pulling funding from the organization could do great damage and potentially prolong the ill-effects of the pandemic. The World Health Organization is leaning on big tech to navigate the COVID-19 pandemic |Nicole Goodkind |October 1, 2020 |Fortune 

Digital eye strain is defined as a group of vision-related problems that result from prolonged use of cell phone, computer, e-reader, or tablet, to name a few. Why dark mode web designs are gaining popularity |Amanda Jerelyn |September 30, 2020 |Search Engine Watch 

A plasma donation involves repeated and prolonged contact with staff — in the reception area, in waiting rooms, during physical exams in closed spaces and when blood is drawn. Foreign Masks, Fear and a Fake Certification: Staff at CSL Plasma Say Conditions at Donation Centers Aren’t Safe |by J. David McSwane |September 21, 2020 |ProPublica 

Amazon is also planning to host its Prime Day shopping event in the fourth quarter this year, rather than the third, so as not to affect service to its customers who are relying on its service throughout the prolonged coronavirus crisis. How the world’s biggest media companies fared through the ongoing crisis in Q2 |Lara O'Reilly |August 12, 2020 |Digiday 

We smile weakly, not wanting to let them down or prolong the conversation. The Malaysian Air Tragedy Reawakens a Primal Fear |Kelly Williams Brown |July 19, 2014 |DAILY BEAST 

When he finally became president, Nixon walked away from that war to prolong a futile one half a world away. 100 Years of Right (And Left) Moves |Robert Shrum |March 31, 2014 |DAILY BEAST 

Drugs are becoming more powerful with prescription painkillers used to enhance effect and prolong a deleterious pleasure. Heroin: America’s Silent Assassin |Dr. Anand Veeravagu, MD, Robert M. Lober, MD, PhD |February 3, 2014 |DAILY BEAST 

What the U.S. is doing now can only prolong Syrian and regional agonies. Face the Assad Reality In Syria |Frank G. Wisner, Leslie H. Gelb |January 26, 2014 |DAILY BEAST 

In Congress, Radel has been a strong conservative and Tea Party favorite who voted to prolong the government shutdown in October. Hip-Hop Conservative Rep. Trey Radel Charged With Cocaine Possession |Ben Jacobs |November 20, 2013 |DAILY BEAST 

I shall show how it is possible thus to prolong life to the term set by God. The Mediaeval Mind (Volume II of II) |Henry Osborn Taylor 

However, a sojourn in a milder climate might prolong his life for a few months; so he advised Algeria. Bastien Lepage |Fr. Crastre 

It is ill-bred, and looks as if you were unaccustomed to such pleasures, and so desirous to prolong each one. The Ladies' Book of Etiquette, and Manual of Politeness |Florence Hartley 

But Colt tried to prolong the contest by bringing up a voter an hour. The History of England from the Accession of James II. |Thomas Babington Macaulay 

The others would bring enough for a winter grubstake, and would prolong their freedom and their independence just that much. Cabin Fever |B. M. Bower