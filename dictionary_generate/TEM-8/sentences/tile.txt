The total volume is the sum of the individual volumes of all the tiles used in the triangulation. A Mathematician’s Unanticipated Journey Through the Physical World |Kevin Hartnett |December 16, 2020 |Quanta Magazine 

“The tiles make a huge impact, and they definitely define the room,” she says. Captivated by bold tile on social media? Here’s what to consider before committing to the look. |Elizabeth Mayhew |October 29, 2020 |Washington Post 

Assistant will now be possible to add as a tile on Tobii’s eye-tracking tablets and mobile apps, which present a large customizable grid of commonly used items that the user can look at to activate. Google Assistant comes to gaze-powered accessible devices |Devin Coldewey |October 6, 2020 |TechCrunch 

In 2014, a patient’s wife filed suit after soaked ceiling tiles fell and struck her in the head while she was sitting in the hospital lobby. Investors Extracted $400 Million From a Hospital Chain That Sometimes Couldn’t Pay for Medical Supplies or Gas for Ambulances |by Peter Elkind with Doris Burke |September 30, 2020 |ProPublica 

Within that space, there are infinitely many ways of placing infinitely many tiles. Computer Search Settles 90-Year-Old Math Problem |Kevin Hartnett |August 19, 2020 |Quanta Magazine 

Tile work in the bathrooms, furniture, and artwork on the walls all flowed together and carried his creative touch. When Gary Wright Met George Harrison: Dream Weaver, John and Yoko, and More |Gary Wright |September 29, 2014 |DAILY BEAST 

Revered and dutiful, he fought (and was injured) in World War II, and succeeded to the tile in 1953. For Sale: The $3M British Mountain—With Aristocratic Family Feud Included |Tom Sykes |August 24, 2014 |DAILY BEAST 

As I reach the berm of sand, tile and stucco that marked a kind of front line, bodies are being piled on carts in the street. Who Is Behind Gaza's Mass Execution? |Jesse Rosenfeld |August 1, 2014 |DAILY BEAST 

The second he does, her giggle sends the Queen of Spades cascading to the brown tile floor below. Hallucinating Away a Heroin Addiction |Abby Haglage |May 4, 2014 |DAILY BEAST 

She was standing out of the way, over against the gray tile wall. Jimmy Breslin on JFK’s Assassination: Two Classic Columns |Jimmy Breslin |November 22, 2013 |DAILY BEAST 

It is in the Elizabethan style, with half-timber frame and sagging tile roof. British Highways And Byways From A Motor Car |Thomas D. Murphy 

A variety of paving tile called "oven tiles" is of similar material to the ordinary red brick, and in size is 10 or 12 in. Encyclopaedia Britannica, 11th Edition, Volume 4, Part 3 |Various 

Gutters also of tile ran along the eaves to conduct the water into cisterns, if it was needed for domestic purposes. The Private Life of the Romans |Harold Whetstone Johnston 

The roofs are of tile, for the winters on the hills are too severe to permit the flat, terraced roofs of Algiers or Bona. Lippincott's Magazine of Popular Literature and Science, Volume 11, No. 24, March, 1873 |Various 

This stopper sometimes is of tile, sometimes a plug of paper or burlap. Elements of Plumbing |Samuel Dibble