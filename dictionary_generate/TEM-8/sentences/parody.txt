When he suggested the phrase, I’d assumed he was planning an “apologies to” parody of the famed illustrations by Sir John Tenniel from the 1865 “Alice’s Adventures in Wonderland.” Style Conversational Week 1418: Just inkin’ about tomorrow |Pat Myers |January 7, 2021 |Washington Post 

Meanwhile, a good number of people have been banned from the platform for posting vulgar content, as well as some parody accounts and those that posted pictures of feces. Parler, the “free speech” social network, explained |Rebecca Heilweil |January 7, 2021 |Vox 

During Fairbanks’s previous run as the parody hero of contemporary action comedies like “His Picture in the Papers,” fans came to think of him as “Doug,” a tribute to his offhand elegance — like Fred Astaire’s, a triumph of talent and willpower. Zorro at 100: Why the original swashbuckler is still the quintessential American action hero |Michael Sragow |January 1, 2021 |Washington Post 

The writers were masters of parody, so much so that it became something of a badge of honor to be so featured. Hulu brings back that irreverent magic with trailer for Animaniacs reboot |Jennifer Ouellette |October 22, 2020 |Ars Technica 

At this point, labeling a Democrat a “communist” is almost worthy of parody. GOP senator falsely claims opponent was endorsed by Communist Party |Glenn Kessler |October 6, 2020 |Washington Post 

In 2011, the rapper-turned-self parody debuted his first collection, Dw Kanye West. Kanye West and Kim Kardashian’s Balmain Campaign: High Fashion Meets Low Culture |Amy Zimmerman |December 23, 2014 |DAILY BEAST 

But then, this show has always been more than just the parody of right-wing cable punditry it was originally made out to be. The End of Truthiness: Stephen Colbert’s Sublime Finale |Noel Murray |December 19, 2014 |DAILY BEAST 

As part of the writing team on Blazing Saddles, he gave its parody of the Western a sharper political edge. How Richard Pryor Beat Bill Cosby and Transformed America |David Yaffe, Scott Saul |December 10, 2014 |DAILY BEAST 

As of yesterday, three of their films were nominated for Best Parody at the 2015 AVN Awards. Inside the Greatest Porn Parody Factory: From ‘Game of Bones’ to ‘The Humper Games’ |Gabriella Paiella |November 28, 2014 |DAILY BEAST 

He seems fleshless, bloodless; he might almost be a black man's parody of how a clean-cut white man moves. The Stacks: Pauline Kael's Talking Heads Obsession |Pauline Kael |November 22, 2014 |DAILY BEAST 

It is twice blessed—it blesses him who earns, and those who give, to parody the words of Shakspeare. Journal of a Voyage to Brazil |Maria Graham 

A mockery of a government—a disgrace to the office pretended to be held—a parody on the position assumed. The Condition, Elevation, Emigration, and Destiny of the Colored People of the United States |Martin R. Delany 

As to Moussorgskys music, it may go to the devil for all I care: it is the commonest, lowest parody of music. The Life &amp; Letters of Peter Ilich Tchaikovsky |Modeste Tchaikovsky 

To equip a dull, respectable person with wings would be but to make a parody of an angel. The Pocket R.L.S. |Robert Louis Stevenson 

With the perfume of the roses into the open window came the stench of this hideous parody, as if in mockery. A Year in the Fields |John Burroughs