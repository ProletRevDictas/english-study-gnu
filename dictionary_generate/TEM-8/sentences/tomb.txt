After death, grave robbers broke into their tomb at least twice, and now archaeologists have pieced together some fragments of the story—mostly the postmortem chapters. Ancient embalmers used mud to hold a damaged mummy together |Kiona N. Smith |February 4, 2021 |Ars Technica 

The treasure-filled room was one of four associated with the tomb of Tutankhamen. What the mummy’s curse reveals about your brain |Kathryn Hulick |January 14, 2021 |Science News For Students 

To some people, however, the fact that a person entered a tomb and then died soon after seemed too strange to be a coincidence. What the mummy’s curse reveals about your brain |Kathryn Hulick |January 14, 2021 |Science News For Students 

This was six weeks after opening and entering the actual burial chamber of the tomb. What the mummy’s curse reveals about your brain |Kathryn Hulick |January 14, 2021 |Science News For Students 

Some scientifically minded people questioned whether toxic mold or bacteria in the tomb might have led to the deaths. What the mummy’s curse reveals about your brain |Kathryn Hulick |January 14, 2021 |Science News For Students 

In addition to visiting the tomb of John Paul, who died of natural causes in 2005, Agca asked to see his successor, Pope Francis. Pope-Shooter Ali Agca’s Very Weird Vatican Visit |Barbie Latza Nadeau |December 29, 2014 |DAILY BEAST 

He has put flowers on the tomb of John Paul II,” said Vatican spokesman Federico Lombardi, “I think that is enough. Pope-Shooter Ali Agca’s Very Weird Vatican Visit |Barbie Latza Nadeau |December 29, 2014 |DAILY BEAST 

But the proud stone lion that once stood atop the tomb, as Peristeri has often maintained, suggests a male occupant and a warrior. Is This Alexander the Great’s Tomb—or His Wife’s? |James Romm |December 12, 2014 |DAILY BEAST 

The tomb, though much smaller than the palace, is similarly a vision of ornate twists, arches, and peaks. The Postman Who Built a Palace in France…by Hand |Nina Strochlic |November 20, 2014 |DAILY BEAST 

Greece has high hopes that the giant tomb now being excavated at Amphipolis contains one of these ancient Macedonian leaders. Amphipolis Tomb Yields Amazing Finds But Mysteries Linger |James Romm |October 17, 2014 |DAILY BEAST 

Taking half a dozen men with him, and compelling the woman to act as guide, he went to the tomb in the dark. The Red Year |Louis Tracy 

D'Israeli loved the long pipe in his youth, but in middle age pronounced it 'the tomb of love.' Tobacco; Its History, Varieties, Culture, Manufacture and Commerce |E. R. Billings. 

Over the tomb is placed a herse of iron, furnished with stands for holding lighted candles or torches. Notes and Queries, Number 177, March 19, 1853 |Various 

And it seemed to Tom that his cousin came upon them out of the heart of a dream, out of the earth, out of a sandy tomb. The Wave |Algernon Blackwood 

Thebes was a single, enormous tomb; his past lay buried there; from the solemn, mournful, desolate hills he had escaped. The Wave |Algernon Blackwood