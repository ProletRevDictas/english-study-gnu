Clue’s cycle analysis starts to get at the macro-level insights I crave and provides the data points to start piecing together the puzzle. Everything You Need to Know About Period Tracking |Christine Yu |September 6, 2020 |Outside Online 

These areas have already been linked in other experiments to the sensation of “wanting” or “craving” something. Why do you feel lonely? Neuroscience is starting to find answers. |Amy Nordrum |September 4, 2020 |MIT Technology Review 

We’ve been craving the experience of standing outside, enjoying the sweet scent of charcoal smoke mixed with pine trees, holding a beer in one hand and tongs in the other, and that slightly charred flavor you can only get from cooking over fire. 5 Portable Grills for All Your Outdoor Cooking Needs |Amy Marturana Winderl |September 2, 2020 |Outside Online 

Maybe it’s a sign of my advancing age, or maybe I just crave a bit of structure in a hectic world full of uncertainty. The Matador SEG42 Offers Unrivaled Gear Organization |Graham Averill |August 25, 2020 |Outside Online 

Politicians and parties still crave these narratives, though, especially when they lose. How Clinton’s Loss Paved The Way For Biden |Seth Masket |August 20, 2020 |FiveThirtyEight 

White Southerners crave an innocent past, a personal distance from the sins of their ancestors. The Tragic History of Southern Food |Jason Berry |November 12, 2014 |DAILY BEAST 

Its only failure was that it lacked the right kind of losing-it drama Oscar night watchers crave. Why Does Everyone Hate Lea Michele? |Tim Teeman |October 9, 2014 |DAILY BEAST 

The mad tend to crave it, many of the sane crave it, but the wise worry about its long-term side effects. David Mitchell’s ‘The Bone Clocks’ Is Fun But Mostly Empty Calories |William O’Connor |September 14, 2014 |DAILY BEAST 

The irony in it all is that our bodies need, if not crave, Vitamin D—and more than a chewable tablet. Is the Facekini the Future of Beachwear? |Justin Jones |August 23, 2014 |DAILY BEAST 

But no matter that difference, we crave to own and co-opt, rather than necessarily understand, it. Memory Porn: America’s Obscene Anniversary Obsession |Tim Teeman |June 17, 2014 |DAILY BEAST 

Yet to crave forgiveness would be to confess—to tell all I know—the whole awful truth! The Doctor of Pimlico |William Le Queux 

Some offend because they crave popularity or want to do what their friends are doing. Report of the Special Committee on Moral Delinquency in Children and Adolescents |Oswald Chettle Mazengarb et al. 

As I have great sorrows, I will confide in you; dreadful disappointment, for which I will crave a smile. Balsamo, The Magician |Alexander Dumas 

She added with the same low, soothing melody in her voice that his ear had learned to crave, And I, too—I love you. The Woman Gives |Owen Johnson 

We are remote, and can speake but seldom, and therefore crave leave to speake the more at once. The Loyalists of America and Their Times, Vol. 1 of 2 |Egerton Ryerson