The most northwest habitation in Canada — roughly 80 miles above the Arctic Circle — the town sits at the heart of the Vuntut Gwitchin First Nation. Indigenous chief Dana Tizya-Tramm is fighting climate change for his people and for the planet |Tik Root |October 29, 2021 |Washington Post 

After a federal judge ruled that the city’s so-called vehicle habitation law was too vague in 2018, the city passed yet another law restoring its ability to prohibit homeless people from living in cars on city streets. Morning Report: Only Redistricting Commissioner from D6 Resigns |Voice of San Diego |October 14, 2021 |Voice of San Diego 

Three quarters of those forest islands—which were likely built out of the flat plain by humans—contain unambiguous signs of human habitation, from the remains of fires to ditches and ceramics. This wild grassland in the Amazon isn’t as untouched as it seems |Philip Kiefer |October 7, 2021 |Popular-Science 

Even northern Canada, where no snakes live now, has snake fossils from a warmer era—a trend that may repeat itself, as global temperatures rise, reopening vast tracts of land to snake habitation. A guide to all the places with no snakes |empire |August 13, 2021 |Popular-Science 

These include habitation and life support systems, solar power, and shielding from radiation and micrometeorite impacts. Here’s what China wants from its next space station |Neel V. Patel |April 30, 2021 |MIT Technology Review 

Apparently campaign staffers and volunteers had been living in the building considered unsafe for human habitation. Republican Slobs Stick Together |Ben Jacobs |January 30, 2014 |DAILY BEAST 

And one strong gust of wind could blow the whole edifice of human habitation away. Following the Great Iditarod |Justin Green |April 26, 2013 |DAILY BEAST 

The recent Cancerian theme of creativity bleeds into the domestic sector, opening the door on imaginative modes of habitation. Horoscopes for June 12-18, 2011 |Starsky + Cox |June 12, 2011 |DAILY BEAST 

Commenons, comme l'on dict, de chez nous, de la maison et habitation; puis nous sortirons dehors. The Jesuit Relations and Allied Documents, Vol. II: Acadia, 1612-1614 |Various 

Et au partir de l tira ledit Sieur au Port Royal lieu de son habitation. The Jesuit Relations and Allied Documents, Vol. II: Acadia, 1612-1614 |Various 

It would not have been an unsightly spot if the marks of the habitation of poor and careless folk had been wiped away. The Campfire Girls of Roselawn |Margaret Penrose 

And Asor shall be a habitation for dragons, desolate for ever: no man shall abide there, nor son of man inhabit it. The Bible, Douay-Rheims Version |Various 

I have, therefore, thought it expedient to enliven with paintings the whole habitation of the saint. The Catacombs of Rome |William Henry Withrow