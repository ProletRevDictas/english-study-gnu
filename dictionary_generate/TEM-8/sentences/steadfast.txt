They were, he implied on more than one occasion, patriotic Americans who deserved recognition for their steadfast devotion to the Republic. Long before QAnon, Ronald Reagan and the GOP purged John Birch extremists from the party |Erick Trickey |January 15, 2021 |Washington Post 

You lean on your social workers who remain steadfast in their belief that everything’s all sunshine and roses. The happiness and heartbreak of a daughter’s first fishing trip |By Jonathon Klein/Field & Stream |December 30, 2020 |Popular-Science 

Astronomers long believed the pair to be our steadfast orbiting companions, like moons of the Milky Way. The New History of the Milky Way |Charlie Wood |December 15, 2020 |Quanta Magazine 

Corbat called himself “a steadfast believer in term limits” in explaining the timing in a LinkedIn post. How Jane Fraser broke banking’s highest glass ceiling |Claire Zillman, reporter |October 19, 2020 |Fortune 

Rhodes-Johnson remains steadfast in her belief that she did all she could for Stewart all those years ago. Her Stepfather Admitted to Sexually Abusing Her. That Wasn’t Enough to Keep Her Safe. |by Nadia Sussman |September 18, 2020 |ProPublica 

I envy my refusenik friends their steadfast commitments to stay in, and contentment in doing so. The Refuseniks Hiding From ‘Happy New Year’ |Lizzie Crocker |December 31, 2014 |DAILY BEAST 

“A steadfast soldier of the law,” Felsman had said of his friend and comrade. Killer Eric Frein Held in Murdered Cop’s Cuffs |Michael Daly |October 31, 2014 |DAILY BEAST 

He remained steadfast in his belief in the promise of the conservative social network. ReaganBook Is the Latest Conservative #Fail |Gideon Resnick |July 31, 2014 |DAILY BEAST 

To date, the league has been steadfast in backing Snyder as well. It’s Official: ‘Redskins’ Is Racist, but Will the Team or NFL Listen? |Robert Silverman |June 18, 2014 |DAILY BEAST 

In the months following the death of our newborn daughter, I had remained steadfast in my faith, devout and prayerful. How Losing My Daughter Changed My Faith |Kyle Cupp |June 15, 2014 |DAILY BEAST 

The Afghan was true to his salt, and their own retainers, who had come with them from Lucknow, remained steadfast at this crisis. The Red Year |Louis Tracy 

At this part of Lorenzo's narrative, a cry, unutterable in words, burst from the engloomed but steadfast bosom of his auditor. The Pastor's Fire-side Vol. 3 of 4 |Jane Porter 

And these steadfast qualities Tom absorbed unconsciously through his very skin. The Wave |Algernon Blackwood 

The driving power that forced an older self upon her had weakened before the steadfast love he bore her. The Wave |Algernon Blackwood 

Her dress and bearing gave the impression of solid wellbeing, and steadfast purpose. Skipper Worse |Alexander Lange Kielland