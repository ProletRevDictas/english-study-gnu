Its four electric motors had a record-beating efficiency of 97%, far ahead of the miserable 27% of standard thermal engines. Use today’s tech solutions to meet the climate crisis and do it profitably |Walter Thompson |February 12, 2021 |TechCrunch 

It came rushing back, how miserable I was just hours before I unfurled my picnic blanket on the damp grass and we worked our way through the pleasantries of a first date. Love, Loneliness, and the Chicken in My Freezer |Elazar Sontag |February 9, 2021 |Eater 

Hollye Kirkcaldy, director at Sparro House, a remote-working creative agency, attempted to return to work full-time after having her first child, but admitted the experience made her miserable. ‘It’s possible if done right’: Pandemic accelerates demand for flexible senior roles |Jessica Davies |February 3, 2021 |Digiday 

I prayed and prayed the break would end and now that it has ended I feel even more miserable. Carolyn Hax: He asked to ‘take a break’; now he wants to take up where they left off |Carolyn Hax |January 19, 2021 |Washington Post 

Coming off a miserable 2020 season, United would have to ensure an adequate supporting cast. Mesut Özil playing for D.C. United seems unlikely. But it’s not impossible. |Steven Goff |January 14, 2021 |Washington Post 

Despite being one of the most powerful men in the world, the king looks miserable. The French Court’s Royal Ban on Smiles |William O’Connor |December 14, 2014 |DAILY BEAST 

The young people in Girls are miserable, peevish, depressed, hate their bodies, themselves, their life, and each other. Up to a Point: They Made Me Write About Lena Dunham |P. J. O’Rourke |December 13, 2014 |DAILY BEAST 

Like Donal, he requested to go to boarding school to escape his homelife; also like Donal, he was initially miserable there. Colm Toibin Describes The Creation Of His Quiet Masterpiece ‘Nora Webster’ |Jennie Yabroff |November 3, 2014 |DAILY BEAST 

Millions of children in India endure miserable and difficult lives. Stopping the Small Hands of Slavery |Meenakshi Ganguly |October 13, 2014 |DAILY BEAST 

I was going along OK, but looking back, I was filled with anger and took it out on my first wife and made her life miserable. David Lynch on Transcendental Meditation, ‘Twin Peaks,’ and Collaborating With Kanye West |Marlow Stern |October 6, 2014 |DAILY BEAST 

U was an Usurer, a miserable elf; V was a Vintner, who drank all himself. Boys and Girls Bookshelf; a Practical Plan of Character Building, Volume I (of 17) |Various 

Madame and myself had just been regretting that we should have to pass the evening in this miserable hole of a town. Blackwood's Edinburgh Magazine, Volume 60, No. 372, October 1846 |Various 

Or, if I escaped these dangers for a day or two, what could I expect but a miserable death of cold and hunger? Gulliver's Travels |Jonathan Swift 

All the miserable stratagems they had been guilty of to win him; the dishonest plotting and planning. Elster's Folly |Mrs. Henry Wood 

Eight weary years have passed, and we have reached a miserable day in the month of November. Blackwood's Edinburgh Magazine, No. CCCXXXIX. January, 1844. Vol. LV. |Various