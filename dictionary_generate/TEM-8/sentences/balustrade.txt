They had been hanging over the low balustrade, engaged in a heart-to-heart talk with two pretty Quartier brunettes. The Real Latin Quarter |F. Berkeley Smith 

Narrow winding staircases in the interior lead to the top, upon which a small platform, with a balustrade a foot high, is erected. A Woman's Journey Round the World |Ida Pfeiffer 

Nearer came the feet, and I peered between the interstices of the screening balustrade. Valley of the Croen |Lee Tarbell 

The terrace was walled with creamy stone, and railed about by a heavy balustrade of white magnesian limestone. A Fortune Hunter; Or, The Old Stone Corral |John Dunloe Carteret 

The hideous figure of Peter the Hermit re-appeared above the balustrade. The Pilgrim's Shell or Fergan the Quarryman |Eugne Sue