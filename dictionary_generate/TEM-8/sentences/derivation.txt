The determination of micro intents by analyzing the SERPs allows the derivation of certain types of content that users expect for a search term. How to use 12 micro intents for SEO and content journey mapping |Olaf Kopp |July 18, 2022 |Search Engine Land 

I also had the nerve to find minor gaps in certain derivations that just didn’t sit right with me. How I Escaped My Troubles Through Science - Issue 104: Harmony |Subodh Patil |August 25, 2021 |Nautilus 

The French adopt the same derivation, calling it "asbeste" (minèral filamenteux et incombustible). Asbestos |Robert H. Jones 

Its similarity with the numerous derivatives of the verb damno have probably obscured the true derivation of the word. A Cursory History of Swearing |Julian Sharman 

His method is hence inductive,--the derivation of certain principles from a sum of given facts and phenomena. Beacon Lights of History, Volume I |John Lord 

In a word, the term contains a series of expressive innuendos by its etymological derivation. Essays In Pastoral Medicine |Austin Malley 

Another etymological example sometimes cited is the derivation of the English uncle from the Latin avus. A System of Logic: Ratiocinative and Inductive |John Stuart Mill