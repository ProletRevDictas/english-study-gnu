I realized that I’ve been thinking about this for at least the last three years. Christian Puglisi Is Closing His Influential Copenhagen Restaurants. COVID Is Only Partly to Blame |Rafael Tonon |September 17, 2020 |Eater 

“So much of this is realizing where we are, accepting where we are, and not anchoring our judgments of ourselves to what we were able to accomplish a year ago, but what does it look like today,” Rayl added. ‘Integrators’ and ‘separators’: How managers are helping the two types of remote workers survive the pandemic |Jen Wieczner |September 16, 2020 |Fortune 

Once I got to learn those techniques, I got to realize, I know how to do that. Tom Colicchio Hopes (and Fears) COVID-19 Will Change the Restaurant Industry |Pallabi Munsi |September 16, 2020 |Ozy 

This past October, as Jacob Holm and Eva Rotenberg were thumbing through a paper they’d posted a few months earlier, they realized they had been sitting on something big. A New Algorithm for Graph Crossings, Hiding in Plain Sight |Stephanie DeMarco |September 15, 2020 |Quanta Magazine 

The researchers realized that one variable was the source of the parasites. ‘Trained Immunity’ Offers Hope in Fight Against Coronavirus |Esther Landhuis |September 14, 2020 |Quanta Magazine 

We realise the Sea King fleet is ageing and welcome the investment in new aircraft. Is The Real Reason William is Quitting Flying His New American Boss? |Tom Sykes |March 29, 2013 |DAILY BEAST 

More likely, investors realise the ‘knock-on’ effects from a Cypriot default are literally incalculable. Cyprus is Imploding, So Why Aren't Markets Freaking Out? |Megan McArdle |March 22, 2013 |DAILY BEAST 

Another said he didn't realise who Pippa was because she introduced herself by her full name, Philippa. Pippa Middleton Joins Big Fat Belgian Hunt |Tom Sykes |January 15, 2013 |DAILY BEAST 

And I admire Rembrandt too much not to realise the beauty that can be derived from frequenting the synagogue. David's Bookclub: Sodom and Gomorrah |David Frum |September 29, 2012 |DAILY BEAST 

They don't realise that for a man it isn't just something quite nice that's occasionally optional, like flower arranging. I'm Not The Sort of Man Who Goes To Prostitutes |Louis Bernières |October 18, 2008 |DAILY BEAST 

His heart now beat high with hope, for he believed that he was about to realise his ancient dream. Hunting the Lions |R.M. Ballantyne 

As he spoke he gesticulated slightly, and no second glance was needed to realise that he was a thorough-going cosmopolitan. The Doctor of Pimlico |William Le Queux 

If only they both come to realise it in their normal waking states his Double will cease these nocturnal excursions. Three More John Silence Stories |Algernon Blackwood 

He was enough of an artist to realise that nothing was out of place, that it was a home to rejoice in, to be proud of. The Everlasting Arms |Joseph Hocking 

This year, for the first time, I have begun to realise that I am rather lonely here, in spite of many friends. The Life &amp; Letters of Peter Ilich Tchaikovsky |Modeste Tchaikovsky