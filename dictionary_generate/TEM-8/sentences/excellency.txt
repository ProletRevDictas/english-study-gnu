Now the correspondents brazenly asked the president for permission to “dedicate it to your excellency.” How a Racist Newspaper Defeated Lincoln in New York in the 1864 Election |Harold Holzer |May 2, 2013 |DAILY BEAST 

Quite how a full-scale civil war can avoid being “politicized,” His Excellency failed to enlighten the chamber. The U.N. Session on Censuring Syria Brings Out the World’s Thugs |Andrew Roberts |August 11, 2012 |DAILY BEAST 

His excellency took one end of the table, and an aide-de-camp the other: I was seated between M. and Madame do Rego. Journal of a Voyage to Brazil |Maria Graham 

His excellency bowed slightly, the parvenu humbled himself lower than the earth, then they chatted for a moment. The Nabob |Alphonse Daudet 

We have a quantity of mushrooms in the second park, and his excellency amuses himself sometimes by gathering them. The Nabob |Alphonse Daudet 

It is clearly understood between us, Excellency, that you will give me five hundred ounces? The Border Rifles |Gustave Aimard 

When you've reported yourself, if his Excellency remains silent, and takes no notice, bolt. Blackwood's Edinburgh Magazine, Volume 67, Number 414, April, 1850 |Various