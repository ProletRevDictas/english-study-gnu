My youngest son was especially glum as flight delays and a missed connection left him stranded in the airport in Charlottesville, North Carolina, while his brother’s bachelor party went on without him. Epic delays and flight cancellations: How the US airline industry can get back up to speed |Teresa Carr/Undark |January 12, 2022 |Popular-Science 

They included Lupoi, who looked sheepishly glum as he nodded to a woman who is apparently his wife amongst the spectators. Mafia’s Cocaine-in-a-Can Bust |Michael Daly |February 12, 2014 |DAILY BEAST 

It makes for rather glum reading—not least because it was printed in 1816. Britain’s Weight Crisis Almost Hits U.S. Proportions |Dan Jones |February 21, 2013 |DAILY BEAST 

For Derrida, in 1965, as often, the start of the summer was rather glum. Derrida’s ‘Of Grammatology’ and the Birth of Deconstruction |Benoît Peeters |December 21, 2012 |DAILY BEAST 

Meanwhile, several Wall Street forecasters and other firms have lowered their estimates to the same glum ballpark. Why Wall Street Is Projecting Slower Growth |Matthew Zeitlin |December 6, 2012 |DAILY BEAST 

So why are economic forecasters so glum about the fourth quarter? Why Wall Street Is Projecting Slower Growth |Matthew Zeitlin |December 6, 2012 |DAILY BEAST 

He sat glum and thoughtful, his mind in unproductive travail, until the captain was announced. St. Martin's Summer |Rafael Sabatini 

Why, I could talk for eight days without taking breath, and I am by nature a glum, silent man. First Plays |A. A. Milne 

Tessa is thinking of glum things to say to me, do sit down and say something funny. Tessa Wadsworth's Discipline |Jennie M. Drinkwater 

The thud of a bar dropped in place punctuated the evening's experience with a glum finality. Valley of the Croen |Lee Tarbell 

I thought so because he passed us as we were coming home and was looking very glum. Elsie's Vacation and After Events |Martha Finley