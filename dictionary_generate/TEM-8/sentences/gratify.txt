I’m gratified by this entire experience, including knowing that this donation will go directly to helping future lawyers gain valuable experience and to fight for justice for others. Law firm that won settlement in Md. HBCU case donating $12.5 million in fees |Pamela Wood |November 19, 2021 |Washington Post 

The moment was indeed worth an ovation, for the turn of events is gratifying and — should the productions gain further momentum and extend again — a remarkable change of fortune. Broadway is off to a shaky start, but a last-minute reprieve for two acclaimed plays lifts spirits |Peter Marks |November 4, 2021 |Washington Post 

Community organizers who have fought for a decade to end oil extraction in urban neighborhoods were gratified by the large penalty, and said they hope it will be collected. We Reported on How California Rarely Cracks Down on Oil Companies. Now Regulators Have Fined One Company $1.5 Million. |by Janet Wilson, The Desert Sun |June 2, 2021 |ProPublica 

The women all provide similar accounts, alleging the star quarterback contacted them via Instagram over the past year to arrange massages in which he then attempted to get them to gratify him sexually. As more Deshaun Watson accusers go public, their attorney rails against quarterback’s response |Will Hobson |April 15, 2021 |Washington Post 

Yet, aside from the top-shelf quality of its sights, sounds and performances, what makes Aretha so gratifying to watch is how fully Parks delivers on the promise to illuminate Franklin’s genius. Genius: Aretha Is a Lavish, Lively Showstopper That Hits a Few False Notes |Judy Berman |March 19, 2021 |Time 

Most of the meddlers in our lives do it to gratify their own egos or because they mistakenly believe they are helping. America’s Meddlers Are Our Worst Enemies |Stefan Beck |October 3, 2014 |DAILY BEAST 

He carried tales, told lies, and tried to make trouble, for no reason but to gratify his inclinations. Fifty Years of Railway Life in England, Scotland and Ireland |Joseph Tatlow 

In the autumn of this year I was able to gratify my taste for travel by a longer excursion than usual. Fifty Years of Railway Life in England, Scotland and Ireland |Joseph Tatlow 

Of refined tastes, including a penchant for blue china, being a thriving bachelor, he was able to gratify them. Fifty Years of Railway Life in England, Scotland and Ireland |Joseph Tatlow 

Norman's face was a sight, as he stood holding Aubrey in his arms, to gratify the child's impatience. The Daisy Chain |Charlotte Yonge 

In short, he may gratify his every whim and fancy, without a pang of reposing conscience, or the least jostle of his self-respect. The Pocket R.L.S. |Robert Louis Stevenson