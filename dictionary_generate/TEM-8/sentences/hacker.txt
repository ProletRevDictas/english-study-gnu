The hackers typically threaten to publish the victim’s files if the ransom isn’t paid. Elon Musk confirms Tesla was target of foiled ransomware attack |Kirsten Korosec |August 28, 2020 |TechCrunch 

An investigation determined that while the first call had been genuine, the second had come from scammers who’d paid a hacker for Xu’s number, admissions status, and request for financial aid. Inside China’s unexpected quest to protect data privacy |Tate Ryan-Mosley |August 19, 2020 |MIT Technology Review 

Western intelligence services allege that Russia has already deployed a team of hackers known as Cozy Bear to extract vaccine secrets from UK and US servers. Every country wants a covid-19 vaccine. Who will get it first? |Katie McLean |August 13, 2020 |MIT Technology Review 

Russia has tried various tactics to get in front of the competition, with hackers in the country reportedly trying to steal vaccine data from the United States, Great Britain and Canada. Here’s what we know about Russia’s unverified coronavirus vaccine |Tina Hesman Saey |August 11, 2020 |Science News 

On top of improving the efficiency, the team has tackled some of the potential side-channel attacks that a hacker could use to compromise the secure link. China Takes Another Step Towards Uncrackable Quantum Communication |Edd Gent |June 15, 2020 |Singularity Hub 

After a bunch of tough talk, this round of the hacker-on-hacker fight nevered materialized. The Attack on the Hidden Internet |Marc Rogers |December 29, 2014 |DAILY BEAST 

Until recently, the hacker collective known as Lizard Squad was all but unknown. The Attack on the Hidden Internet |Marc Rogers |December 29, 2014 |DAILY BEAST 

Surely only an advanced hacker could mastermind such a fiendish act — right? A Female Writer’s New Milestone: Her First Death Threat |Annie Gaus |December 1, 2014 |DAILY BEAST 

In the real world, he said, a hacker is more likely interested in stealing records he can sell than in harming a patient. How Your Pacemaker Will Get Hacked |Kaiser Health News |November 17, 2014 |DAILY BEAST 

Four days later, 4.6 million Snapchat usernames and phone numbers were compromised and leaked online by a hacker. ‘The Snappening’ Is Real: 90,000 Private Photos and 9,000 Hacked Snapchat Videos Leak Online |Marlow Stern |October 13, 2014 |DAILY BEAST 

A teenage hacker-turned-hero pits himself against the government to fight for his basic freedoms. Little Brother |Cory Doctorow 

The funding was kicked off by some tech millionaires who couldn't believe that a bunch of hacker kids had kicked the DHS's ass. Little Brother |Cory Doctorow 

Any complex system is sport for a hacker; a side effect of this is the hacker's natural affinity for problems involving security. Little Brother |Cory Doctorow 

However, the motivations of a hacker are typically as simple as Im an engineer because I like to design things. Little Brother |Cory Doctorow 

Colonel Hacker again admonished me to go home, and keep no more meetings. George Fox |George Fox