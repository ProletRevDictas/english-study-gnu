While CBO wonks are famously brilliant or daft—depending on how you want to use their assessments—they are not political and resist any invitation to be so. Democrats Head Toward Piecemeal Infrastructure Votes As Coalition Frays |Philip Elliott |September 28, 2021 |Time 

He said he hoped to sell the mountain to  ‘some daft Russian’ who wanted to ‘show off’. For Sale: The $3M British Mountain—With Aristocratic Family Feud Included |Tom Sykes |August 24, 2014 |DAILY BEAST 

Daft Punk amplified the crowd in a way that changed my life and they never even left the booth. DJ Steve Aoki: To Cake or Not To Cake |Steve Aoki |August 8, 2014 |DAILY BEAST 

Vampire Weekend, “Giving up the Gun” The ultimate motley crew: Jake Gyllenhaal, Joe Jonas, Daft Punk, Lil Jon and RZA. Andrew Garfield in ‘We Exist’ and More Celebrities in Music Videos |Marina Watts |May 18, 2014 |DAILY BEAST 

Like, you had the Daft Punk record and then the Bruno Mars record and new records just come out. Chromeo’s Dave 1 on ‘White Women’ and Bringing Back the Funk |Melissa Leon |May 12, 2014 |DAILY BEAST 

The idea of using an incredibly salty, orange-hued shell as a delivery vehicle for salty stuffings seemed a little daft. Domino’s Fried-Chicken Pizza Means We’ve Hit Peak Food Trolling |Daniel Gross |April 16, 2014 |DAILY BEAST 

I was near daft with fear when I saw Leeby wasna there either. A Window in Thrums |J. M. Barrie 

And Dan leaned over and whispered, "See, she's gone daft, like the rest!" The Transformation of Job |Frederick Vining Fisher 

Ah, she is a bonny thing, but ye ken she is a wee bit daft, puir lassie! Graham's Magazine Vol XXXII No. 6 June 1848 |Various 

There's a sea called Sargasso, and if I told you half the things about it, you'd think me daft. The Wind Bloweth |Brian Oswald Donn-Byrne 

A man must be daft that takes his wifes death so hard it eeny most kills him, and he stays single all the rest of his life. Wheat and Huckleberries |Charlotte Marion (White) Vaile