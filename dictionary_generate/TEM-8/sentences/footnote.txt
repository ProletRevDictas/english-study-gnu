That’s probably enough to ensure it will be at least a footnote in history. Zendaya and John David Washington deserve a better film than Malcolm & Marie |Alissa Wilkinson |February 5, 2021 |Vox 

The unfortunate footnote of any Gonzaga season is that for some, it means little unless Few wins a national championship. Gonzaga Is Dominating With Speed And Defensive Disruption |Josh Planos |January 26, 2021 |FiveThirtyEight 

That includes long-standing features like footnotes, citations and changelogs—none of which are available to those attempting to correct falsehoods on social media platforms. What if the Web Looked More Like Wikipedia? |Alex Fitzpatrick |January 15, 2021 |Time 

Cleveland wound up as the footnote of this four-team trade, but it did well to grab the 22-year-old Allen as a long-term option at center. James Harden deal shows Nets are all in, both for a title and as a personality experiment |Ben Golliver |January 14, 2021 |Washington Post 

Instead of budget footnotes, journalists were trying to find safety. The Breach of the Capitol Spooked Us — As It Should Have |Philip Elliott |January 7, 2021 |Time 

Her decision was based on a tiny footnote written by U.S. Supreme Court Justice John Stevens in 2005. Judge Could Smash Marijuana Law |Abby Haglage |November 4, 2014 |DAILY BEAST 

When I wrote the novel about the Gulag, House of Meetings, the name Stalin only appears in a footnote very early on. Martin Amis Talks About Nazis, Novels, and Cute Babies |Ronald K. Fried |October 9, 2014 |DAILY BEAST 

Mike Tyson Mysteries is also another footnote in one of the unlikeliest second acts in history. Mike Tyson Reinvents Himself (Again) as a Cartoon TV Detective on ‘Mike Tyson Mysteries’ |Annaliza Savage |July 30, 2014 |DAILY BEAST 

By the time "decisions need to be made" about 2016, Christie said, "I think this will be a footnote." Chris Christie’s YOLO Attitude for 2016 |Olivia Nuzzi |May 15, 2014 |DAILY BEAST 

To put things into perspective, had Gore won his home state, Florida would have been relegated to a footnote. Memo to the 2016 GOP: Winning Your Home State Matters |Lloyd Green |May 5, 2014 |DAILY BEAST 

Footnote 2: Tatham says that the tobacco plant is peculiarly adapted for an agricultural comparison of climates. Tobacco; Its History, Varieties, Culture, Manufacture and Commerce |E. R. Billings. 

Footnote 23: Monardes wrote upon it only from the small account he had of it from the Brazilians. Tobacco; Its History, Varieties, Culture, Manufacture and Commerce |E. R. Billings. 

Footnote 48: Tobacco has been able to survive such attacks as these—nay, has raised up a host of defenders as well as opponents. Tobacco; Its History, Varieties, Culture, Manufacture and Commerce |E. R. Billings. 

Footnote 76: Hughes, in his History of Barbadoes, says that the common people call the worm kitifonia. Tobacco; Its History, Varieties, Culture, Manufacture and Commerce |E. R. Billings. 

Footnote 81: Florida tobacco is noted for the white rust found on the leaves. Tobacco; Its History, Varieties, Culture, Manufacture and Commerce |E. R. Billings.