Though they’re inaudible to human ears, whose range bottoms out at 20 Hz, the interval creates some fairly insidious side effects. Why do we see ghosts? |Jake Bittle |October 6, 2020 |Popular-Science 

Ginsburg delivered her dissent aloud from the bench, accusing her colleagues of either ignoring or failing to comprehend the “insidious” nature of pay discrimination, and called on Congress to act. How A Conservative 6-3 Majority Would Reshape The Supreme Court |Amelia Thomson-DeVeaux (Amelia.Thomson-DeVeaux@abc.com) |September 28, 2020 |FiveThirtyEight 

The most insidious problem, however, is the varroa mite, appropriately named Varroa destructor. The Fungal Evangelist Who Would Save the Bees - Issue 90: Something Green |Merlin Sheldrake |September 23, 2020 |Nautilus 

Indeed, air conditioning represents one of the most insidious challenges of climate change, and one of the most difficult technological problems to fix. Air conditioning technology is the great missed opportunity in the fight against climate change |James Temple |September 1, 2020 |MIT Technology Review 

Such a situation is particularly insidious because the pay gap she experiences would often go undetected. How to be a fair-pay CEO |matthewheimer |August 25, 2020 |Fortune 

Is it anti-Semitism, or are less insidious cultural forces at work? Why Was Bess Myerson the First and Last Jewish Miss America? |Emily Shire |January 7, 2015 |DAILY BEAST 

But its far more insidious role was revealed, whether it was gun policies or voter suppression. Mike Brown Dies, a Generation Comes Alive |Roland S. Martin |November 25, 2014 |DAILY BEAST 

The act of erasure through mis- or under-representation is an insidious one. It Ain't Easy Being Bisexual on TV |Amy Zimmerman |August 14, 2014 |DAILY BEAST 

Its insidious reach enters into medical offices and chokes off the free-speech rights of the people trying to work there. Pediatricians Have the Right to Ask About Guns |Russell Saunders |July 30, 2014 |DAILY BEAST 

But the commentary that McCarthy gets from her so-called supporters is often just as cruel, but somehow more insidious. The Trials of ‘Tammy’: Stop Policing Melissa McCarthy’s Body |Teo Bugbee |July 7, 2014 |DAILY BEAST 

Play-writing is a luxury to a journalist, as insidious as golf and much more expensive in time and money. First Plays |A. A. Milne 

Each cachet contained three decigrams of malourea, the insidious drug notorious under its trade name of Veronal. Dope |Sax Rohmer 

The state of affairs which has come about was uncertain in origin, insidious in growth, and has developed over a wide field. Report of the Special Committee on Moral Delinquency in Children and Adolescents |Oswald Chettle Mazengarb et al. 

She believed he sought her, and she must needs fight an insidious liking for him. Repertory Of The Comedie Humaine, Complete, A -- Z |Anatole Cerfberr and Jules Franois Christophe 

This is a thing I would despise in anybody else; but he is so jolly insidious 240 and ingratiating! The Works of Robert Louis Stevenson - Swanston Edition Vol. 25 (of 25) |Robert Louis Stevenson