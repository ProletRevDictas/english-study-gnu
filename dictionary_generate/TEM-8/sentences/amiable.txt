In these scenes the film is fully in joke mode, poking amiable fun at the cheesy Quebecois-ness of the Dion family, here rechristened Dieu. ‘Aline,’ Cannes’ Unauthorized Celine Dion Biopic, Is Like a Will Ferrell Movie Without the Jokes |Caspar Salmon |July 14, 2021 |The Daily Beast 

Bright and amiable, with long brown hair and wraparound shades, Sebastian lived in Santa Rosa Beach and enjoyed the usual Florida-boy fare—swimming, snorkeling, and hanging with his buddies, who nicknamed themselves the Tribe. The Leg at the Bottom of the Sea |David Kushner |May 24, 2021 |Outside Online 

For the most part, the group is an active, amiable one in which linguaphiles help one another with translator minutia. Who should translate Amanda Gorman’s work? That question is ricocheting around the translation industry. |Sindya Bhanoo |March 25, 2021 |Washington Post 

The article is an amiable part of a special issue on spring travel, but an army of trolls, angry Boulderites, and Front Range truthers took to their devices to rip it to shreds. A Park Slope Transplant Wrote a Story About Boulder... |Tracy Ross |March 11, 2021 |Outside Online 

Coming to America was an amiable modern fairytale, a delightful picture that might have seemed conventional in 1988 but looks better with age. Coming 2 America Has Its Modest Charms, But It's Mostly a Reminder of How Great Its Predecessor Is |Stephanie Zacharek |March 5, 2021 |Time 

According to owner Argiros, the answer is amiable discretion. Inside New York’s Most Powerful Diner |Tom Teodorczuk |October 31, 2014 |DAILY BEAST 

Why, they might even switch to amiable, Texas-accented, 77-year-old Schieffer, the television equivalent of comfort food. Jon Stewart and 'Meet The Press' Would Have Been One Unhappy Marriage |Lloyd Grove |October 9, 2014 |DAILY BEAST 

Moore is famously amiable and social-media savvy, and he takes a gentler and more sophisticated tone than his predecessor. Southern Baptists Take Baby Steps Away from the Culture Wars |Ruth Graham |June 14, 2014 |DAILY BEAST 

An amiable interviewee, he was more than willing to chat about acting technique, his wide range of screen roles, and much more. The Great Character Actor: Guy Pearce on His Brilliant Career, From ‘Priscilla’ to ‘The Rover’ |Richard Porton |May 23, 2014 |DAILY BEAST 

A knock at the door interrupted him, and his face resumed its amiable expression. Read ‘The King in Yellow,’ the ‘True Detective’ Reference That’s the Key to the Show |Robert W. Chambers |February 20, 2014 |DAILY BEAST 

Neantmoins, et nostre abord cette soire, et le lendemain matin nostre descente fut fort amiable et pacifique. The Jesuit Relations and Allied Documents, Vol. II: Acadia, 1612-1614 |Various 

He was a manly young fellow, a sportsman and renowned at cricket, and she was amiable and pretty, a little blonde beauty. Fifty Years of Railway Life in England, Scotland and Ireland |Joseph Tatlow 

She was very amiable during the walk back, and raved much over Edna's appearance in her bathing suit. The Awakening and Selected Short Stories |Kate Chopin 

You will do well to send a note to your amiable friend Carr; it may save him a useless journey; for at my table he shall not sit. Elster's Folly |Mrs. Henry Wood 

I felt quite sure that Mr Lockhart would find her, he is such an obliging and amiable man, as well as clever. The Garret and the Garden |R.M. Ballantyne