Gulls loved to feed on grasshoppers, and JFK had a good grasshopper hatch. Four wild animals that are thriving in cities |By Ryan Chelius/Outdoor Life |February 9, 2021 |Popular-Science 

So, in Mexico — chapulines is what they’re called — you can get a taco filled with grasshoppers. The Downside of Disgust (Ep. 448) |Stephen J. Dubner |January 21, 2021 |Freakonomics 

Even if a determined grasshopper sets its sights on a sensor, the air curtain would theoretically blast it off course. How the autonomous vehicle engineers at Ford are helping to make it safer to drive |By Ford Built for America |December 3, 2020 |Popular-Science 

Experiments with grasshoppers and spiders, for example, have shown that the time of the day at which heating occurs can tip the ecological balance. Nights are warming faster than days. Here’s what that means for the planet. |Ula Chrobak |October 9, 2020 |Popular-Science 

Back 220 million years ago, giant grasshoppers flitted about. Minecraft’s big bees don’t exist, but giant insects once did |Carolyn Wilke |May 14, 2020 |Science News For Students 

Like Bieber, Assaf was discovered when he was still knee-high to a grasshopper. The Next Arab Idol: Palestine's Boy Wonder and Stereotype Buster |Maysoon Zayid |May 22, 2013 |DAILY BEAST 

If the future reneges, people may decide that they might as well be a grasshopper, since the ant gets just as screwed. Our Demographic Decline |Megan McArdle |December 4, 2012 |DAILY BEAST 

In some tellings, the grasshopper dies; in some, the ant saves him. America Hits the Reset Button |Kurt Andersen |July 28, 2009 |DAILY BEAST 

At last the sermon commenced, and Llewellyn, who had imprisoned a grasshopper in a paper cage, suddenly let it hop out. Eric, or Little by Little |Frederic W. Farrar 

He nearly betrayed himself once by shouting from his window at a boy who was torturing a grasshopper. The Life of Mazzini |Bolton King 

The grasshopper-lark chirps all night in the height of summer. The Natural History of Selborne, Vol. 1 |Gilbert White 

She felt as awkward as a limping grasshopper in a crowd of butterflies. Prudy Keeping House |Sophie May 

Then the grasshopper talks and is followed by others, each giving his view of life from his own individual standpoint. Little Fishers: and their Nets |Pansy