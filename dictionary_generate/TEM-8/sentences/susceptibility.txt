Those variants aren’t necessarily the genetic tweaks that lead to more severe disease, but they flag that one or more genes in the region might be responsible for increasing susceptibility to the coronavirus. Neandertal genes in people today may raise risk of severe COVID-19 |Tina Hesman Saey |October 2, 2020 |Science News 

One of the biggest problems facing quantum computers is their susceptibility to disturbances, which can easily introduce errors into their calculations. IBM Plans to Have a 1,000-Qubit Quantum Computer by 2023 |Edd Gent |September 25, 2020 |Singularity Hub 

Scientists have speculated other factors influence susceptibility, including pre-existing levels of inflammation and immunity, the amount of virus that starts an infection, and patients’ genetic makeup. Covid-19 scientists flag key immune function as a turning point in life threatening cases |kdunn6 |September 25, 2020 |Fortune 

In May, they tangled over children’s susceptibility to the coronavirus. Fauci finally loses his patience with Rand Paul |Aaron Blake |September 23, 2020 |Washington Post 

However, in an ongoing pandemic with no guarantee that a vaccine will be available anytime soon, the heterogeneity of susceptibility has real implications for the disease’s herd immunity threshold. The Tricky Math of Herd Immunity for COVID-19 |Kevin Hartnett |June 30, 2020 |Quanta Magazine 

Arsenic can also cause cardiovascular disease, which African-Americans have greater genetic susceptibility for, she said. The Congressman Fighting for More Arsenic in Drinking Water |Tim Mak |July 7, 2014 |DAILY BEAST 

One, creative fields differ in many respects, ranging from cost of production to susceptibility to digital copying. What Would IP Free Industries Look Like? Probably Not Like Restaurants. |Megan McArdle |October 16, 2012 |DAILY BEAST 

The susceptibility of otherwise sane folks to End Times enthusiasms is odd but perhaps a bit predictable. A Reality Check for the Doomsday Unhinged |John Avlon |May 21, 2011 |DAILY BEAST 

Our new hero has found that his capacity for love and his susceptibility to loss are more powerful than his hunger for tail. Has Bond Lost His Balls? |Matthew Oshinsky |November 3, 2008 |DAILY BEAST 

There are others who disclose a special susceptibility to the more simple effects of pathos. Children's Ways |James Sully 

Even orthodoxy must trip it on tiptoe; there was always some prejudice, some susceptibility to consider. The Daughters of Danaus |Mona Caird 

These were terrific moments; I had already felt them, but never with such intense susceptibility as then. My Ten Years' Imprisonment |Silvio Pellico 

She dwelt on the ancestral causes that gave him a nature of exceptional and dangerous susceptibility. Lady Byron Vindicated |Harriet Beecher Stowe 

Or there may be mental weakness and neurotic susceptibility in regard to a special class of impressions. Scientific American Supplement No. 299 |Various