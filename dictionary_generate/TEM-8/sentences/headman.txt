The district headman is the deputy of the tribal ward headman to whom he is immediately responsible. The Philippine Islands |John Foreman 

We retraced our steps a few miles up the valley to a village ruled over by a friendly woman, the widow of the late headman. Mount Everest the Reconnaissance, 1921 |Charles Kenneth Howard-Bury 

The people saw him and welcomed him back, making him their Headman, and giving him the place of honor in their gatherings. A Treasury of Eskimo Tales |Clara Kern Bayliss 

Orth'ris began rowlin' his eyes an' crackin' his fingers an' dancin' a step-dance for to impress the Headman. Soldier Stories |Rudyard Kipling 

The headman rolled his eye where Kim was chatting to a girl in blue as she laid crackling thorns on a fire. Kim |Rudyard Kipling