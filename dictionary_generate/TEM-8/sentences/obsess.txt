Before my injury, I obsessed over whether I ate healthy enough. Six Months of Soft Food Set Me Free |Meghan McCarron |February 2, 2021 |Eater 

His fans are obsessed with their love story, and Akins has fueled interest by starring in several of Rhett’s videos, notably the swoony, six-time-platinum ballad, “Die a Happy Man.” How the wives of country music stars created their own powerful — sometimes controversial — Instagram empires |Emily Yahr |February 1, 2021 |Washington Post 

I was obsessed with Wu-Tang and A Tribe Called Quest, all these albums came out when I was turning 13, 14. How ‘Freaks and Geeks’ went from misfit dramedy to cult classic, as told by its cast and creators: ‘People just like it so much that it thrusts itself from the grave’ |Sonia Rao |January 27, 2021 |Washington Post 

This isn’t some major redesign but we’re talking about minor tweaks that the designers surely obsessed over but that the users may not even really notice. Google refreshes its mobile search experience |Frederic Lardinois |January 22, 2021 |TechCrunch 

He was a man obsessed not just with his own legacy, but the ability of all people to create their own stories and histories. The best presidents in video games |Gene Park |January 20, 2021 |Washington Post 

The professor known as Wittgenstein (and Iyer) obsess about the notion of thought itself on almost every page. Lars Iyer’s ‘Wittgenstein Jr.’ Plumbs the Deep Fun of Philosophical Fiction |Drew Smith |October 1, 2014 |DAILY BEAST 

Do we obsess over celebrities because we want to be perfect? Face It—We Rubes Will Never Live Like Gwyneth and Jennifer Aniston |Rachel Bertsche |July 2, 2014 |DAILY BEAST 

Or do we want to be perfect because we obsess over celebrities? Face It—We Rubes Will Never Live Like Gwyneth and Jennifer Aniston |Rachel Bertsche |July 2, 2014 |DAILY BEAST 

If all goes as planned, Alig soon will face his fans, those who blog about him, follow his tweets, and obsess over Party Monster. The Party Monster Lives For the Applause: Michael Alig’s Second Act |Caitlin Dickson |February 28, 2014 |DAILY BEAST 

I think we obsess over a lot of the same details in a situation. ‘Portlandia’ Duo Fred Armisen and Carrie Brownstein Discuss the Secrets to the Cult Show’s Success |Marlow Stern |February 27, 2014 |DAILY BEAST 

If the world continued to confine and obsess him, he hated the world, and gasped for freedom. Winds Of Doctrine |George Santayana 

His virility would obsess him to such an extent that there certainly would be moments of posturing and swagger. Pieces of Hate |Heywood Broun 

In fact, Courtrey, burning with the new desire that was beginning to obsess him, was working out a new design. Tharon of Lost Valley |Vingie E. Roe 

Let the subtle fear that the task is impossible obsess the thought, paralyze the nerve, and no hope is left. The Meaning of Faith |Harry Emerson Fosdick 

The old chimera of turning the cold woman to warmth through his own passion began to obsess him. Dangerous Days |Mary Roberts Rinehart