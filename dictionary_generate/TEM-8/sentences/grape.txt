If you haven’t tried a wine made from bobal, an obscure grape variety indigenous to Spain, here’s your chance. This $14 Italian red is a gem that invites a pairing with a pot roast or pasta |Dave McIntyre |February 12, 2021 |Washington Post 

Nothing else tastes quite like it, and the fact Prevost actually makes this wine from red grapes suggests some delicious unknowable alchemy. You Should Be Drinking Grower Champagne |Jordan Michelman |February 11, 2021 |Eater 

Casa Marrone Appassimento is made with organic grapes, dried in the sun to concentrate the flavors and the sugars before pressing. This sauvignon blanc tastes like a splurge, but doesn’t cost like one |Dave McIntyre |January 2, 2021 |Washington Post 

The market trend in recent years toward natural wines favors less manipulation of the grapes in the vineyard and the juice in the cellar. Making champagne with little to no added sugar is tricky. A respected producer is doing it right. |Dave McIntyre |December 18, 2020 |Washington Post 

For example, New World wine labels list the type of grape pressed into the bottle, while Old World labels tend to state the region where the grape was grown instead. Australia’s ‘approachable’ wine won over China’s middle class. Then came the tariffs |eamonbarrett |December 7, 2020 |Fortune 

For a long while Zinfandel was the mystery grape, apparently sui generis except that nobody knew where it came from. Napa’s Earthquake Is Not The Only Thing Shaking The Vineyards |Clive Irving |August 31, 2014 |DAILY BEAST 

Many of the grape growers and wine makers are on site to answer questions. America’s Best Summer Food Festivals |Lonely Planet |July 5, 2014 |DAILY BEAST 

On it are balanced a plate of eggs and toast, an open quart jar of grape jelly, and a beer mug full to the brim with orange juice. The Ridiculousness of Father's Day |P. J. O’Rourke |June 15, 2014 |DAILY BEAST 

The most famous white wine grape on the island is the Assyrtiko. Book a Room for Two in a Santorini Cave |Joanna Eede |June 10, 2014 |DAILY BEAST 

Our Kiddush prayers were done with gefilte fish and grape juice instead of wine. A Jewish Ex-Con Recalls Keeping Kosher with the Faithful in Prison |Daniel Genis |May 11, 2014 |DAILY BEAST 

The nine barricaded the outer gates and placed in the best positions guns loaded with grape. The Red Year |Louis Tracy 

I prayed for her before the temple, and unto the very end I will seek after her, and she flourished as a grape soon ripe. The Bible, Douay-Rheims Version |Various 

But every one shall die for his own iniquity: every man that shall eat the sour grape, his teeth shall be set on edge. The Bible, Douay-Rheims Version |Various 

In those days they shall say no more: The fathers have eaten a sour grape, and the teeth of the children are set on edge. The Bible, Douay-Rheims Version |Various 

Grape-shot and bullets sang the death-song of many a brave fellow, but Nicholson was untouched. The Red Year |Louis Tracy