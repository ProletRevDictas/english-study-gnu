That means cops with dubious records sometimes get bounced around police agencies. Sacramento Report: Jones, COVID-19 and the Irony of Remote Voting |Sara Libby and Jesse Marx |August 28, 2020 |Voice of San Diego 

Diamandis compares it to a cop walking the beat, but for a biological threat—that is, it requires a more targeted approach than searching for one criminal. Why Covaxx thinks it has a COVID-19 vaccine game changer on its hands |Sy Mukherjee |August 27, 2020 |Fortune 

However, private security agents still face far less public scrutiny than the average cop. Defund the Police? Here Come the Private Security Patrols |Nick Fouriezos |August 27, 2020 |Ozy 

Earlier this month, Kate Nucci reported that cops since 2013 had issued at least 83 tickets for seditious language. Morning Report: SDPD Says It Will Stop Seditious Language Tickets |Voice of San Diego |August 17, 2020 |Voice of San Diego 

When Jennifer Strong and I started reporting on the use of face recognition technology by police for our new podcast, “In Machines We Trust,” we knew these AI-powered systems were being adopted by cops all over the US and in other countries. There is a crisis of face recognition and policing in the US |Tate Ryan-Mosley |August 14, 2020 |MIT Technology Review 

Smith attended both funerals as a cop and as the husband of Police Officer Moira Smith, who died on 9/11. The Muslim Cop Killed by Terrorists |Michael Daly |January 9, 2015 |DAILY BEAST 

Lucas said that he himself nonetheless hopes to become a cop. Shot Down During the NYPD Slowdown |Michael Daly |January 7, 2015 |DAILY BEAST 

Nobody ever says they want to become a cop so they can bust people for urinating in public or drinking alcohol on their stoop. Shot Down During the NYPD Slowdown |Michael Daly |January 7, 2015 |DAILY BEAST 

The NOPD fired Knight in 1973 for stealing lumber from a construction site as an off-duty cop. The Louisiana Racists Who Courted Steve Scalise |Jason Berry |January 3, 2015 |DAILY BEAST 

They selected an “easy mark” who turned out to be an off-duty NYC Housing Authority cop named James Carragher. His First Day Out Of Jail After 40 Years: Adjusting To Life Outside |Justin Rohrlich |January 3, 2015 |DAILY BEAST 

Once he had been a young cop, determined to work his way up in the police force. Hooded Detective, Volume III No. 2, January, 1942 |Various 

In his efforts to clear himself, the young cop had taken half a dozen lead slugs from underworld guns into his body. Hooded Detective, Volume III No. 2, January, 1942 |Various 

It was the Hermit's vast store of scientific knowledge that brought the half-dead cop back to health. Hooded Detective, Volume III No. 2, January, 1942 |Various 

The sight of a traffic cop made him dodge around a corner that threw him off his course. Hooded Detective, Volume III No. 2, January, 1942 |Various 

He struck just one wild haymaker of a blow that cleared the head of the cop by nearly a foot. Hooded Detective, Volume III No. 2, January, 1942 |Various