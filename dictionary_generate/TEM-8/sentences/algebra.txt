The definitions sprawled across many areas of math, from algebra to topology to geometry. Building the Mathematical Library of the Future |Kevin Hartnett |October 1, 2020 |Quanta Magazine 

You could also have worked through a little algebra before plugging anything in. The Fifth Battle For Riddler Nation |Zach Wissner-Gross |September 4, 2020 |FiveThirtyEight 

When I was an undergraduate I was really attracted to abstract algebra. Conducting the Mathematical Orchestra From the Middle |Rachel Crowell |September 2, 2020 |Quanta Magazine 

The simpler objects are arrays of numbers called matrices, the core element of linear algebra. The ‘Useless’ Perspective That Transformed Mathematics |Kevin Hartnett |June 9, 2020 |Quanta Magazine 

Many people see algebra as methods and rules, but you can take any algebraic expression and see it visually. America’s Math Curriculum Doesn’t Add Up (Ep. 391) |Steven D. Levitt |October 3, 2019 |Freakonomics 

What did those darned Muslims give us other than grammar and algebra? Deconstructing David Brat’s ‘Scholarship’ |Candida Moss |June 12, 2014 |DAILY BEAST 

Nevertheless, it was required, and at least it was more fun than studying algebra or photosynthesis. The Financial Case for Dodgeball: Why America Needs Gym Class |Mark McKinnon |April 28, 2014 |DAILY BEAST 

That same year Forever 21 was forced to stop selling tops that read “Allergic to Algebra.” The Rise of Sexist Fashion, From Plain Jane Homme to Disney |Soraya Roberts |May 9, 2013 |DAILY BEAST 

The arc of Hathahate is like one of those U-ish parabolas from algebra class. The Cult of Hathahaters: Will It Hurt Anne Hathaway’s Oscar Chances? |Kevin Fallon |January 20, 2013 |DAILY BEAST 

To make the algebra work, each day had to have been twenty-two hours in length. How Long Is a Year? Is the Earth Slowing Down? And Other Questions About Time |Neil Shubin |January 6, 2013 |DAILY BEAST 

It fell into the three more or less isolated subjects of arithmetic, algebra and Euclid. The Salvaging Of Civilisation |H. G. (Herbert George) Wells 

Dinah had left her slate on 10 a chair, and dropped her algebra on the carpet, at the sound of Norahs voice below the window. Tessa Wadsworth's Discipline |Jennie M. Drinkwater 

Old Faithful had worked half-way through the algebra and was busy solving simultaneous equations whilst sitting on the War Loan. Punch, or the London Charivari, Vol. 152, May 16, 1917. |Various 

We do not propose that he shall decipher the hieroglyphics of algebra and geometry. Astronomy for Amateurs |Camille Flammarion 

General Grant never saw an algebra nor any mathematical work until he went to West Point. The Blue and The Gray |A. R. White