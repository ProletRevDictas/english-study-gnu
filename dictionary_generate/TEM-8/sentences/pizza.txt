Cooking frozen pizzas, roasted veggies, chicken nuggets, and whole, toasted sandwiches are a snap. Best toaster: Get perfectly golden slices every time |PopSci Commerce Team |February 10, 2021 |Popular-Science 

Employees can also check-in to a flight, book a taxi or order a pizza for a meeting themselves using their smartphone. How businesses are looking to replicate home-like atmosphere in future office layouts |Jessica Davies |February 4, 2021 |Digiday 

This is your 30-minute pizza, and you can do what you like to make you happy. Turn that old bread into pizza in less than 30 minutes |By Farideh Sadeghin/Saveur |January 29, 2021 |Popular-Science 

Simply remove the leftover pizza, and put the box in the bin. Unmasking the pandemic’s pollution problem |Stephen Ornes |January 28, 2021 |Science News For Students 

GREAT VALUEBright cherry and spice aromas announce this sangiovese as a pleasing quaffer or a good partner for casual meals, such as burgers or pizza. A refreshing $14 Tuscan red seeks burgers or pizza for a delicious match |Dave McIntyre |January 22, 2021 |Washington Post 

I am with a few friends at a pizza spot in D.C., on Mass Ave., just about 10 blocks from the White House. The Day I Used Eric Garner’s Voice |Joshua DuBois |December 5, 2014 |DAILY BEAST 

Therefore, he started hiring vendors like a “papusa lady” and a pizza guy to come and cook up made-to-order snacks. L.A.’s Cool-Kid Backyard Concert Series—Kensington Presents |Sara Lieberman |October 17, 2014 |DAILY BEAST 

“There were moments when I was just really tempted to have a slice of pizza or a cheeseburger,” he says. Nick Jonas Is All Grown Up, Clutching His Penis and Everything |Kevin Fallon |October 8, 2014 |DAILY BEAST 

“I would love to eat a pizza like that,” she says, testing Robin. ‘Wetlands,’ About A Bodily Fluid-Obsessed German Teen, Is the Year's Raunchiest Film |Marlow Stern |August 29, 2014 |DAILY BEAST 

Roll out pizza dough, add sauce, cheese, pepperoni, and bake at 375 for 6-8 minutes. Epic Meal Empire’s Meat Monstrosities: From the Bacon Spider to the Cinnabattleship |Harley Morenstein |July 26, 2014 |DAILY BEAST 

The bag went over my head quickly and was cinched so tight at the throat that I choked and threw up my freegan pizza. Little Brother |Cory Doctorow 

It seemed most everyone was rushing to deflate the pizza bubble and end our love affair with the anchovy. The Land of Look Behind |Paul Cameron Brown 

A pig's head stuffed with not the familiar apple but instead each tusk hollowed bulging with pizza. The Land of Look Behind |Paul Cameron Brown 

It was all the rage to be Italian and boast of one's prowess in demolishing mounds of pizza. The Land of Look Behind |Paul Cameron Brown 

A boar's head contoured in the recognizable shape but with tusks only made of pizza was a favourite alternative. The Land of Look Behind |Paul Cameron Brown