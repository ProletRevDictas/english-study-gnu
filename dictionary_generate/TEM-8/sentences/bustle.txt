When I decided to stop reading, the bustle in the pod had stopped and the sleepiness that had once permeated me had completely disappeared. Locked up in the Land of Liberty: Part III |Yariel Valdés González |July 21, 2021 |Washington Blade 

I could make out in the distance the airport with its usual bustle of airplanes. Locked up in the Land of Liberty: Part I |Yariel Valdés González |July 7, 2021 |Washington Blade 

It remains a fascinating place to walk the streets, people watching and experiencing the bustle. Who Will Be Iran's Next President and What Does It Mean for the Region |Ian Bremmer |May 29, 2021 |Time 

While visiting a friend at WNYC studios, she said the hustle and bustle of a pre-broadcast newsroom made her feel like she was at home. ‘Always a straight shooter’: How Campbell Brown is working to close the trust gap between publishers and Facebook |Kayleigh Barber |April 23, 2021 |Digiday 

She realized she had been ignoring a lot of needs amid the bustle of her life. Being Single Was Just a Part of Their Lives Before the Pandemic. Then It Became the Defining One |Belinda Luscombe |April 17, 2021 |Time 

After nine months, McKeever gradually began to introduce Roger to the bustle and noise of New York. Central Park’s Carriages Saved This Horse |Michael Daly |April 24, 2014 |DAILY BEAST 

There is much purposeful hustle and bustle but tasks go uncompleted; confusion reigns. Inside The EuroMaidan’s Circle Of Trust |Jamie Dettmer |March 10, 2014 |DAILY BEAST 

Since the film is set in the 19th century, Jones was outfitted in a series of Victorian era gowns, replete with bodice and bustle. Felicity Jones Is Bound for Stardom |Marlow Stern |December 29, 2013 |DAILY BEAST 

But they numbered just two dozen in all and barely disturbed the workday bustle of the national capital. Coming Clean on the Dirty War: José Efraín Rios Montt Goes to Trial |Mac Margolis |March 29, 2013 |DAILY BEAST 

The bustle of the newsroom is a mere backdrop for self-involved characters to give talky speeches and taunt each other. Aaron Sorkin’s Cable Crackup: Why His HBO Series Is a Snooze |Howard Kurtz |June 22, 2012 |DAILY BEAST 

There was no dog with her, and in the bustle that followed, I forgot to seek further for the solution of those two fiery lights. Uncanny Tales |Various 

Accustomed to the bustle and hurry of a soldier's life, he was too old to acquire the tastes of a life of tranquillity. Napoleon's Marshals |R. P. Dunn-Pattison 

You will forget me in the bustle of your career, monsieur; but I shall always hold your memory very dear and very gratefully. St. Martin's Summer |Rafael Sabatini 

Those were busy days in the history of the “Anchor,” and the constant stream of poorer wayfarers added to the bustle. The Portsmouth Road and Its Tributaries |Charles G. Harper 

Miss Carrington heard the bustle in the class, so she sat up and looked out over the room with asperity. The Girls of Central High on the Stage |Gertrude W. Morrison