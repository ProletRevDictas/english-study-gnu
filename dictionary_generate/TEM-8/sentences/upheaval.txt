She had made the varsity basketball team, one of the few bright spots during all the upheaval. The stimulus relieved short-term pain, but eviction’s impact is a long haul |Kyle Swenson |February 8, 2021 |Washington Post 

Amid the management upheaval, the company’s fortunes sagged. The GameStop stock craze is about a populist uprising against Wall Street. But it’s more complicated than that. |David J. Lynch |February 1, 2021 |Washington Post 

The coronavirus pandemic has made strategic planning harder because of economic upheaval, personal stress, work and lifestyle changes, and the unpredictability of everything. Collaborative planning in an uncertain world |Martha Leibs |February 1, 2021 |MIT Technology Review 

We’re in the midst of a global pandemic and national political upheaval unlike anything we’ve seen in the past 150 years. There Are No Rules for Healthy Eating |Christine Byrne |January 22, 2021 |Outside Online 

By now, schools and students are accustomed to the flux after the pandemic abruptly emptied campuses last March and caused widespread upheaval at the start of the school year in August and September. Spring term delays: New wave of coronavirus uncertainty slams higher education |Nick Anderson |January 20, 2021 |Washington Post 

And by 1918 there had been a tumultuous upheaval of the four dynasties that dominated East and Central Europe. How WWI Produced the Holocaust |J.P. O’Malley |November 21, 2014 |DAILY BEAST 

The country, long viewed as stable, has recently been home to upheaval. An African Dictatorship’s Friend in D.C. |Center for Public Integrity |November 20, 2014 |DAILY BEAST 

At a time when the rest of their lives were in no small upheaval, this result was a reassurance. Parents Sue for 'Wrongful Birth' |Elizabeth Picciuto |August 17, 2014 |DAILY BEAST 

Nothing is particularly clear because Thailand is in the middle of political upheaval and governed by martial law. The Wild, Wild West of Thai Surrogacy |Emily Shire |August 7, 2014 |DAILY BEAST 

That would be a recipe not just for Ukrainian instability but for instability and upheaval throughout the entire region. Putin: The Cold War Comeback Kid |Andrew Nagorski |May 8, 2014 |DAILY BEAST 

He was rejoicing in the upheaval that permitted debts to be paid with a bludgeon and money to be made without toil. The Red Year |Louis Tracy 

Aristide composed his face into an expression of parental interest; but within him there was shivering and sickening upheaval. The Joyous Adventures of Aristide Pujol |William J. Locke 

Life was a long business, not limited by the fiery upheaval which was shaking the foundations of social order. The Light That Lures |Percy Brebner 

For the social upheaval which the Reformation had brought about came in the train of a long period of economic disorder. The Influence and Development of English Gilds |Francis Aiden Hibbert 

When he called him “Dick, old man”, it gave evidence of an internal upheaval without parallel. The Gold Bat |P. G. Wodehouse