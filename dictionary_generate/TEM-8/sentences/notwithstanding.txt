The dividend notwithstanding, frustrated shareholders, marshaled by an investment firm named Engine No. Oil companies’ losses in 2020 were staggering. And that was before the government focused on climate change. |Will Englund |February 4, 2021 |Washington Post 

The fact, though, that NBCUniversal is moving so quickly to get rid of NBCSN — which airs NHL games and NASCAR races and pulls $380 million in revenue before advertising — is surprising, the high cost of sports rights notwithstanding. Future of TV Briefing: Media companies grapple with getting advertisers to buy their platform video inventory |Tim Peterson |January 27, 2021 |Digiday 

Country music, Ray Charles and Charley Pride notwithstanding, is generally unfriendly to people of color, perhaps not by design but definitely out of habit. Inauguration Musical Performances Are Tricky. But Lady Gaga, Jennifer Lopez and Garth Brooks Did Exactly What We Needed Them to Do |Stephanie Zacharek |January 20, 2021 |Time 

Lackluster defense notwithstanding, Ohio State does feature the superior offense, one capable of keeping the Buckeyes in every matchup. What To Watch For In The College Football Playoff Semifinals |Josh Planos |December 31, 2020 |FiveThirtyEight 

Such feats notwithstanding, Carr says that his own virtual event has had a humbling effect. The Max Vert Challenge Is the Craziest Virtual Race Yet |Martin Fritz Huber |October 29, 2020 |Outside Online 

Clickbait title notwithstanding, Bend Over and Take It Like a Prisoner! Huckabee 2016: Bend Over and Take It Like a Prisoner! |Olivia Nuzzi |January 8, 2015 |DAILY BEAST 

Inaccurate label notwithstanding, Plan B is classified by the FDA as a contraceptive. Rand Paul’s Plan B for Pro-Life Critics |Olivia Nuzzi |October 5, 2014 |DAILY BEAST 

One Love notwithstanding, on the night of the gig, Bunny demanded his £10,000 fee in cash before he would take to the stage. Inside London’s Wild Brixton Academy: How Gangsters and Kurt Cobain Made It London’s Top Music Venue |Tom Sykes |September 29, 2014 |DAILY BEAST 

Potentially inflammatory data points notwithstanding, the study presents otherwise unsurprising information. Palestinian Kids’ PTSD Could Last Generations |Russell Saunders |August 18, 2014 |DAILY BEAST 

And Rand Paul notwithstanding, most GOP insiders believe them, too. Dick Cheney’s Awfulness Is Here to Stay |Michael Tomasky |July 15, 2014 |DAILY BEAST 

He leant against the wall of his refuge, notwithstanding this boast, and licked the ice to moisten his parched lips. The Giant of the North |R.M. Ballantyne 

He couldn't know now whether she was serious or merely joking; but notwithstanding it sounded pleasant to his ears. The Homesteader |Oscar Micheaux 

Notwithstanding the intense heat the open-air life of the march was healthy, and, in many respects, agreeable. The Red Year |Louis Tracy 

Notwithstanding the unseemly hour, the people came running out at the outcry and clamor especially those from the nearest houses. The Philippine Islands, 1493-1898, Volume XX, 1621-1624 |Various 

Notwithstanding, they bear with much patience a great deal of abuse from unkind masters. Our Little Korean Cousin |H. Lee M. Pike