It is a graceful book, essentially graceful, with its haunting agreeable melancholy, its pleasing savoury of antiquity. The Works of Robert Louis Stevenson - Swanston Edition Vol. 25 (of 25) |Robert Louis Stevenson 

It was so savoury, and I have reason to believe so wholesome, that I have frequently taken it since. Travels through the South of France and the Interior of Provinces of Provence and Languedoc in the Years 1807 and 1808 |Lt-Col. Pinkney 

It is certainly very savoury, only that according to French cookery, too much is made of the fowl. Travels through the South of France and the Interior of Provinces of Provence and Languedoc in the Years 1807 and 1808 |Lt-Col. Pinkney 

Not a moment is to be lost--every thing is put in requisition--the savoury meat is soon prepared. Female Scripture Biographies, Vol. I |Francis Augustus Cox 

This pudding was so filling that we could hardly struggle through a savoury, "Angels on runners," and cocoa. The Home of the Blizzard |Douglas Mawson