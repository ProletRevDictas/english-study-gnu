The judge said she had been forced to smuggle drugs to cover the cost of surgery for her ailing son. Suits: The New Face of Latin American Crime |Josefina Salomon |August 22, 2021 |Ozy 

Just a few days after Chuu smuggled several Write for Right posters out of her apartment, trucks of police and soldiers searched her building several times and she was questioned by police. Myanmar's Artists Captured the Spirit of the Resistance. Now They're Continuing the Fight from Abroad |Suyin Haynes |June 23, 2021 |Time 

He was held up on shore by a legal matter and only made it to his assignment aboard the Henrietta by smuggling himself in a casket of champagne. The Hard-Drinking Playboys Who Made Transatlantic Sailing History |Allison McNearney |June 6, 2021 |The Daily Beast 

All of this would suggest Bolsonaro’s year-long pandemic blunder is finally catching up to him along with plenty of other scandals, from those involving his family to his environmental minister who was allegedly smuggling illegal timber. Jair Bolsonaro is facing a political reckoning in Brazil. How far will it go? |Jen Kirby |June 4, 2021 |Vox 

She escaped Syria in 2015 as a 17 year old and was smuggled onto a boat headed for Greece. No Change for Syria, Despite Elections |Sean Culligan |May 27, 2021 |Ozy 

Where are the writers who helped smuggle samizdat out from behind the Iron Curtain? Communism's Victims Deserve a Museum |James Kirchick |August 25, 2014 |DAILY BEAST 

It was a high-tech attempt to smuggle in drugs and phones from the skies over a maximum-security facility. What Was This Drone Doing Over a South Carolina Prison? |Melissa Leon |August 1, 2014 |DAILY BEAST 

Egypt has blocked the tunnels Hamas formerly used to smuggle goods and weapons into Gaza—and to get its operatives out again. Israelis and Arabs Shaken by the Aftershock of Teen Murders |Miranda Frum |July 7, 2014 |DAILY BEAST 

He would smuggle the live birds inside his shirt to get them back to his cell, where he had a killing basin. Tales of a Jailhouse Gourmet: How I learned to Cook in Prison |Daniel Genis |June 21, 2014 |DAILY BEAST 

Rep. Steve King raged that this would allow illegals to “smuggle themselves into the military.” Even a Path to Citizenship for Military Volunteers Is Too Much for House Republicans |Ben Jacobs |April 7, 2014 |DAILY BEAST 

Perhaps he would even have to lurk in the woods, awaiting his opportunity to smuggle his liquor to the men. Blazed Trail Stories |Stewart Edward White 

You will therefore do a meritorious work, if you can smuggle this dead body into the house of the damned Jew of a farmer. Solomon Maimon: An Autobiography. |Solomon Maimon 

I adore his broken English, but how is he going to smuggle letters to me, unless maybe Louisa will continue to help? Polly the Pagan |Isabel Anderson 

That Ireland also began in its turn to organize National Volunteers and to smuggle arms. The Outline of History: Being a Plain History of Life and Mankind |Herbert George Wells 

This is what you must do; smuggle me out another way; call another carriage, and take me for a drive and wicked dinner. Lazarre |Mary Hartwell Catherwood