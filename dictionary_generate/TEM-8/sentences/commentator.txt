Smartmatic’s lawsuit may test whether a network can be held financially responsible for things said by political commentators, or even by their guests. Smartmatic files $2.7 billion defamation suit against Fox News over election fraud claims |Jeremy Barr, Elahe Izadi |February 4, 2021 |Washington Post 

The network’s lineup of anchors, hosts, producers and commentators was reshuffled, too. CNN President Jeff Zucker expects to leave the network at the end of this year |Paul Farhi, Jeremy Barr |February 4, 2021 |Washington Post 

Witten first retired after the 2017 season, and he spent 2018 as a color commentator on ESPN’s “Monday Night Football.” In second retirement, Jason Witten to coach at Dallas-area high school |Des Bieler |February 2, 2021 |Washington Post 

As many commentators have noted, if nothing else Musk’s celebrity will give the industry much-needed publicity. Elon Musk’s $100 Million Carbon Capture Push |Edd Gent |February 1, 2021 |Singularity Hub 

He also has worked as a paid part-time political and business commentator on Fox News. Businessman Pete Snyder joins race for Virginia governor |Laura Vozzella |January 26, 2021 |Washington Post 

He appears frequently on television as a political commentator. Will Dirty Pol Vito Fossella Replace Dirty Pol Michael Grimm? |David Freedlander |December 31, 2014 |DAILY BEAST 

This came across in the Showtime Omit the Logic documentary—in which you were a commentator—and it comes across here. How Richard Pryor Beat Bill Cosby and Transformed America |David Yaffe, Scott Saul |December 10, 2014 |DAILY BEAST 

But one commentator—who also happens to have been cast in the film—has his own unique feelings about the movie. Christian Bale: One Man's Moses Is Another Man's Terrorist |Candida Moss, Joel Baden |December 7, 2014 |DAILY BEAST 

Yet Palin—a former local news sports reporter and a current Fox News political commentator—is undeniably telegenic. Stuck in the Lamestream: Sarah Palin TV Barely Registers on the Web |Lloyd Grove |November 7, 2014 |DAILY BEAST 

“I think the timing is incredibly regrettable,” New Zealand political commentator David Farrar tells The Daily Beast. Greenwald, Assange, and Snowden Join Forces with Kim Dotcom in New Zealand Election |Lennox Samuels |September 17, 2014 |DAILY BEAST 

Doubtless the commentator habit is fixed in the nature of man; but it was pre-eminently mediaeval. The Mediaeval Mind (Volume II of II) |Henry Osborn Taylor 

William Lowth died; a celebrated English theological writer and commentator. The Every Day Book of History and Chronology |Joel Munsell 

Some scornful commentator has called this doggerel; but I would that all doggerel were as interesting. The Portsmouth Road and Its Tributaries |Charles G. Harper 

I must now give my reasons, as every preceding commentator has given up the passage as hopeless. Chaucer's Works, Volume 1 (of 7) -- Romaunt of the Rose; Minor Poems |Geoffrey Chaucer 

Now he appears as a commentator of texts, who claims a monopoly in the solution of all questions of faith and ethics. The Life &amp; Letters of Peter Ilich Tchaikovsky |Modeste Tchaikovsky