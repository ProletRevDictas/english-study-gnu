Then he laughed and made a “who knows” face as he nodded toward one of the men discussing how to bring Marxism to power in Iraq. An Iraqi Group Helping Women and Gays Is Receiving Death Threats |Jacob Siegel |July 22, 2014 |DAILY BEAST 

What do we care for Marxism or monarchism, the resurrection of Holy Russia or the Idea of the Common Fate? This 1979 Novel Predicted Putin’s Invasion Of Crimea |Michael Weiss |May 18, 2014 |DAILY BEAST 

The irony is that a little of this so-called Marxism is exactly what could have saved capitalism (and not for the first time). Federal Open Markets Committee Was Clueless to Financial Crisis |Michael Tomasky |February 24, 2014 |DAILY BEAST 

Ortega shows little interest in fascism or capitalism or Marxism, and this troubled me when I first read the book. The Smartest Book About Our Digital Age Was Published in 1929 |Ted Gioia |January 5, 2014 |DAILY BEAST 

What in their history do they find inconsistent with totalitarianism, or at best statism, or at worst Marxism? America’s Cassandra: David Mamet Speaks on the Lies of Obama and War |Lloyd Grove |November 11, 2013 |DAILY BEAST 

Knowledge is the saviour-god and Marxism is his divine gospel of freedom from these capitalistic sufferings. Communism and Christianism |William Montgomery Brown 

It is the same materialistic conception which has triumphed in German Marxism and in the economic interpretation of history. German Problems and Personalities |Charles Sarolea 

In this respect the critics of Marxism form two very distinct groups. Karl Marx |Achille Loria 

Its best-known forms are the historical school in the science of law, and Marxism. Anarchism |Paul Eltzbacher 

This shortcoming of Marxism is cured by Dietzgen, who made the nature of the mind the special object of his investigations. The Positive Outcome of Philosophy |Joseph Dietzgen