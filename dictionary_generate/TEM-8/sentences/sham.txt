When they injected the molecule into the stomachs of normal mice, the rodents were able to remember the location of a platform in an underwater maze and find it three times faster than mice that had received sham injections. The miracle molecule that could treat brain injuries and boost your fading memory |Adam Piore |August 25, 2021 |MIT Technology Review 

Yet it has repeatedly refused to seriously challenge the administration of President Vladimir Putin and has effectively served to legitimize the Kremlin’s sham democracy. Who Are You Calling a Communist? |Charu Kasturi |August 24, 2021 |Ozy 

From that second on, I realized it was all a sham, that he was actually shy, funny, sentimental, a joy to be with. Jane Birkin is back with a new album, but her presence is everlasting |Jeff Weiss |March 19, 2021 |Washington Post 

Artiles allegedly instructed the sham candidate, who did not live in the Miami-area district, to use the address of a property in Palmetto Bay that Alex Rodriguez still owned, but no longer resided at, according to the affidavit. Ex-Florida state senator paid bogus candidate to ‘siphon votes,’ police say, in race GOP narrowly won |Katie Shepherd |March 19, 2021 |Washington Post 

The EPA has turned the entire process into “a sham,” said Lianne Sheppard, a professor of biostatistics and environmental health at the University of Washington. The EPA Refuses to Reduce Pollutants Linked to Coronavirus Deaths |by Lisa Song and Lylla Younes |October 21, 2020 |ProPublica 

Islamist brigades including Suqur al-Sham, a 9,000-strong militia, are openly breaking with Western-favored rebel factions. Spies Warned White House: Don’t Hit Al Qaeda in Syria |Shane Harris, Jamie Dettmer |November 7, 2014 |DAILY BEAST 

Then came the admission of a sham marriage with an immigrant. The Crazy Oregon Governor Race Just Got Crazier |Tim Mak |October 14, 2014 |DAILY BEAST 

Also in 1997, Hayes entered a sham marriage with a Nigerian immigrant, for which she was paid $5,000. The Crazy Oregon Governor Race Just Got Crazier |Tim Mak |October 14, 2014 |DAILY BEAST 

Both Iran and the United States share a common enemy in the Islamic State of Iraq and al-Sham (ISIS). Iran Orders Elite Troops: Lay Off U.S. Forces in Iraq |Eli Lake |October 6, 2014 |DAILY BEAST 

I remember the first time I heard of the Islamic State in Iraq and al-Sham (ISIS). Watching ISIS Come to Power Again |Elliot Ackerman |September 7, 2014 |DAILY BEAST 

Part of that idea was sham bric-à-brac, the rest was carte blanche to Messrs. Spick and Span. The Pit Town Coronet, Volume I (of 3) |Charles James Wills 

The camping-out at Streetly Wood has annually recurred since that date; the first sham fight took place June 20, 1877. Showell's Dictionary of Birmingham |Thomas T. Harman and Walter Showell 

If we nevertheless receive them, it is the effect of His bountiful goodness, and not the result of our sham prayer. Mary, Help of Christians |Various 

It is full of deceit, sham, and pharisaism—an aggravated counterpart of the outside world. Prison Memoirs of an Anarchist |Alexander Berkman 

The sham patient sprang to the door at the end of the passage, opened it softly, and stood listening. The Bag of Diamonds |George Manville Fenn