Pelosi, in turn, said the committee’s work was too important to be derailed by showmanship and histrionics. Why America Needed a 9/11 Style Commission for Jan. 6 |Philip Elliott |January 6, 2022 |Time 

There’s an expectation that the actor who’s allowed to be histrionic and demanding, or the writer who’s allowed to be oversharing and impassioned, will ultimately create better art. Is Jeremy Strong the bad art friend? |Aja Romano |December 17, 2021 |Vox 

If there’s a point to this exercise, it gets lost amid so many histrionic reenactments of scenes we’ve seen replayed on the news and parodied in late-night comedy for more than two decades. Impeachment: American Crime Story Frames the Clinton Scandal as a Case of Women Sabotaging Women. Is That Really So Revolutionary? |Judy Berman |August 31, 2021 |Time 

While King’s narrative romanticizes the relationship between mental illness and creativity, Larraín’s histrionic direction often reduces these elements to camp. Stephen King Adaptation Lisey's Story Is Mawkish, Dull and Too Long by Half |Judy Berman |June 4, 2021 |Time 

We get an amusingly histrionic psychological thriller from Plaza and a clever Ronald Reagan pastiche from Duplass and actor-producer Ethan Sandler. 3 New Stoner-Friendly TV Series to Watch on 4/20 |Judy Berman |April 20, 2021 |Time 

The moral duties and doubts of adulthood are swapped out for the histrionic creeds of adolescence. Two New Films Preach Our Nation’s Corrosive Gridiron Gospel |Steve Almond |September 20, 2014 |DAILY BEAST 

Silver Linings Playbook allowed her to explode, playing a woman unhinged, histrionic, and emotionally volatile. How Jennifer Lawrence Took Over Hollywood. (It’s Not Just Because of Her Charm.) |Kevin Fallon |December 20, 2013 |DAILY BEAST 

Wall Street Journal editorial-page writer Dorothy Rabinowitz recorded a histrionic anti-bike video that went viral. Why Conservatives Should Love Bike Share |Daniel Gross |June 9, 2013 |DAILY BEAST 

Such miscues mired the show in histrionic soapiness, upsetting the delicate balance between domestic drama and social change. ‘Downton Abbey’ Season 3 Review: A Return to Form |Jace Lacob |January 3, 2013 |DAILY BEAST 

In fact, The Newsroom seems to relish putting loud women in their place or to render them helpless and histrionic. HBO’s ‘The Newsroom’: Aaron Sorkin’s Woman Problem |Jace Lacob, Maureen Ryan |July 2, 2012 |DAILY BEAST 

This rare merit even the most fastidious critic must allow: but her histrionic essay is, in another respect, equally remarkable. The Hubble-Shue |Christian Carstairs 

She has inherited the histrionic gift from her mother—from me. Quin |Alice Hegan Rice 

Histrionic art always and everywhere suffers from the ephemeral conditions under which it has to be externalised. The Memoirs of Count Carlo Gozzi; Volume the first |Count Carlo Gozzi 

In the midst of her histrionic triumphs, Mlle. Clairon continued her career of gallantry. Queens of the French Stage |H. Noel Williams 

Here she trained a number of aspirants to histrionic fame, several of whom were destined to make their mark in years to come. Queens of the French Stage |H. Noel Williams