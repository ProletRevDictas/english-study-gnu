Yves Albarello, MP of Seine-et-Marne, said the gunmen told police they were ready to “die as martyrs.” France Kills Charlie Hebdo Murderers |Nico Hines |January 9, 2015 |DAILY BEAST 

The influential al Qaeda propagandist, who was born in New Mexico, died in a U.S. drone strike later that year. France Kills Charlie Hebdo Murderers |Nico Hines |January 9, 2015 |DAILY BEAST 

The al Qaeda-linked gunmen shot back, but only managed to injure one officer before they were taken out. France Kills Charlie Hebdo Murderers |Nico Hines |January 9, 2015 |DAILY BEAST 

According to a Yemeni intelligence source, Saïd met with the notorious U.S. preacher Anwar al Awlaki. France Kills Charlie Hebdo Murderers |Nico Hines |January 9, 2015 |DAILY BEAST 

Two witnesses outside the Charlie Hebdo office building quoted the Kouachi brothers claiming they were members of al Qaeda. France Mourns—and Hunts |Nico Hines, Christopher Dickey |January 8, 2015 |DAILY BEAST 

The Vulgate translates: “Et obviabit illi quasi mater honorificata, et quasi mulier a virginitate suscipiet illum.” Solomon and Solomonic Literature |Moncure Daniel Conway 

De secretis operibus artis et naturae, et de nullitate magiae, p. 533 (Brewer). The Mediaeval Mind (Volume II of II) |Henry Osborn Taylor 

Peu aprs nostre arrive, i'escrivy l'estat auquel nous avons retrouv ceste Eglise et Colonie naissante. The Jesuit Relations and Allied Documents, Vol. II: Acadia, 1612-1614 |Various 

Commenons, comme l'on dict, de chez nous, de la maison et habitation; puis nous sortirons dehors. The Jesuit Relations and Allied Documents, Vol. II: Acadia, 1612-1614 |Various 

Estimez un peu que c'est du reste du symbole et fondemens chrestiens. The Jesuit Relations and Allied Documents, Vol. II: Acadia, 1612-1614 |Various