In response to the PPC announcement, someone on Twitter asked if we’d even need websites anymore eventually. Is Google moving toward being search marketing’s point of singularity: Thursday’s daily brief |Carolyn Lyden |February 11, 2021 |Search Engine Land 

Grief is our natural emotional response to loss, so allow yourself to feel it — and any other emotions that may arise. Politics and conspiracy theories are fracturing relationships. Here’s how to grieve those broken bonds. |Jeff Schrum |February 11, 2021 |Washington Post 

The proper responses to unpleasant remarks by Elizabeth are silence and a change of subject. Miss Manners: Time to cut ties with longtime friend? |Judith Martin, Nicholas Martin, Jacobina Martin |February 11, 2021 |Washington Post 

The response has been amazing to this single day virtual event. Pitch to speak at SMX Create! |Kathy Bushman |February 10, 2021 |Search Engine Land 

These included not just responses to sudden downturns for Covid-19-stricken people, but also natural disasters and helping in related situations when other problems arose. RapidSOS raises $85M for a big data platform aimed at emergency responders |Ingrid Lunden |February 9, 2021 |TechCrunch 

I think the response of the French government so far has been pretty appropriate in that regard. Harry Shearer on The Dangerous Business of Satire |Lloyd Grove |January 8, 2015 |DAILY BEAST 

Their immediate response tells an important truth about a police slowdown that has spread throughout New York City in recent days. Shot Down During the NYPD Slowdown |Michael Daly |January 7, 2015 |DAILY BEAST 

What is known is that Peña Nieto bungled his response to the crisis. Why Mexicans Are Enraged by Obama’s Big Tuesday Meeting |Ruben Navarrette Jr. |January 6, 2015 |DAILY BEAST 

But the enduring response—stop the world, I want to get off—is the same. The Evangelical Apocalypse Is All Your Fault |Jay Michaelson |January 4, 2015 |DAILY BEAST 

With enough changing of the influenza RNA over time, the vaccine no longer provokes the “right” immune response. When You Get the Flu This Winter, You Can Blame Anti-Vaxxers |Kent Sepkowitz |January 1, 2015 |DAILY BEAST 

To make the effort of articulation a vital impulse in response to a mental concept,—this is the object sought. Expressive Voice Culture |Jessie Eldridge Southwick 

There was no response to the knock, and Davy cautiously pushed open the door and went in. Davy and The Goblin |Charles E. Carryl 

The knocker appeared to hear the response, and to assert that it was quite impossible he could wait so long. The Posthumous Papers of the Pickwick Club, v. 2(of 2) |Charles Dickens 

Winifred, indeed, in response to a question, said faintly that she thought she could keep going if she had a drink of milk. The Red Year |Louis Tracy 

To this there was no response, the stranger thinking with bitterness that his trip was anything but one of pleasure. The Cromptons |Mary J. Holmes