She probes and flexes her stylets under the skin to find these blood vessels, like the arms of an octopus reaching into the dark to carry out a dangerous deed. Evolution made mosquitos into stealthy, sensitive vampires |Erica McAlister |October 15, 2020 |Popular-Science 

After the sea pigs, a scattering of octopuses came into view. Whales get a second life as deep-sea buffets |Stephen Ornes |October 15, 2020 |Science News For Students 

What ensues is an incredibly beautiful film about the ecosystem of the kelp forest, the intelligence and amazing abilities of the octopus, and a man’s poignant relationship with a wild creature. Everything Our Editors Loved in September |The Editors |October 8, 2020 |Outside Online 

While exploring a kelp forest, he discovers a female octopus and makes a commitment to go see her every day for a year to learn about her life and to see if she will befriend him. Everything Our Editors Loved in September |The Editors |October 8, 2020 |Outside Online 

Other bacteria from this group make TTX in pufferfish, the blue-ringed octopus and sea snails. Toxic germs on its skin make this newt deadly |Erin Garcia de Jesus |June 23, 2020 |Science News For Students 

Sadly, Paul the Octopus did not outlive his impressive but unpopular World Cup predictions by long. The Amazing Tale of Paul the Psychic Octopus: Germany’s World Cup Soothsayer |Emily Shire |July 12, 2014 |DAILY BEAST 

It's at a time like this that Germans yearn most for Paul the Octopus, the great mollusk soothsayer for Germany. The Amazing Tale of Paul the Psychic Octopus: Germany’s World Cup Soothsayer |Emily Shire |July 12, 2014 |DAILY BEAST 

The Oberhausen aquarium erected a memorial of the psychic octopus with a golden urn containing his ashes. The Amazing Tale of Paul the Psychic Octopus: Germany’s World Cup Soothsayer |Emily Shire |July 12, 2014 |DAILY BEAST 

Octopus is one of those sleazy and boorish Americans whose instincts prove correct. This 1979 Novel Predicted Putin’s Invasion Of Crimea |Michael Weiss |May 18, 2014 |DAILY BEAST 

They are the Tarpon, the Falcon, the Sea Fox, and the Octopus. Read ‘The King in Yellow,’ the ‘True Detective’ Reference That’s the Key to the Show |Robert W. Chambers |February 20, 2014 |DAILY BEAST 

Both Giddiness and the Ice-Maiden seize a man as an octopus seizes all within its reach. Rudy and Babette |Hans Christian Andersen 

But hapless flight: the Boodah is an octopus whose feelers reach far, and they, within her toils, cannot escape her omnipresence. The Lord of the Sea |M. P. Shiel 

From the deeper trawling were obtained a large octopus and several interesting fish. The Home of the Blizzard |Douglas Mawson 

These animals belong to the same division—the Cephalopoda—as the cuttle-fish, the squid, and the octopus. On the Method of Zadig |Thomas Henry Huxley 

A darker, livid hue passed fleetingly over the pallid body of the octopus. Kings in Exile |Sir Charles George Douglas Roberts