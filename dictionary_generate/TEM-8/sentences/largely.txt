Youth athletics largely remained banned under state rules aimed at preventing the spread of the virus. School Sports Became ‘Clubs’ Amid the Pandemic – Now Two Coaches Are Out |Ashly McGlone |September 17, 2020 |Voice of San Diego 

He is holding indoor rallies, inviting people to be largely maskless, and raising the risk that in the states he needs to win, he is playing a role in spreading the coronavirus rather than tamping it down. Trump contradicts CDC director on vaccine; Biden says Americans shouldn’t trust Trump |Colby Itkowitz, Felicia Sonmez, John Wagner |September 16, 2020 |Washington Post 

This was seen as a hugely disproportionate grab for data largely because, in the past, the amount of information needed to carry out this task was far smaller. Podcast: COVID-19 is helping turn Brazil into a surveillance state |Anthony Green |September 16, 2020 |MIT Technology Review 

It also remains possible that the “shy voter” theory remains unfounded and that, just as national polling accurately measured the outcome of the 2016 race, polling is largely capturing the race as it stands. If voters are wary of stating support for Trump in polls, why does he outperform GOP Senate candidates? |Philip Bump |September 16, 2020 |Washington Post 

He is holding indoor rallies, inviting people to be largely maskless, and raising the risk that in these states he needs to win, he is playing a role in spreading the coronavirus rather than tamping it down. Trump keeps dodging the crux of major issues — and that’s showing in his reelection prospects |Amber Phillips |September 16, 2020 |Washington Post 

Eating disorders, on the other hand, are driven largely by biological processes that occur on the inside. How Skinny Is Too Skinny? Israel Bans ‘Underweight’ Models |Carrie Arnold |January 8, 2015 |DAILY BEAST 

Presuming his demographic is largely the same as what it was when he was at Fox, they are not wealthy people. Glenn Beck Is Now Selling Hipster Clothes. Really. |Ana Marie Cox |December 20, 2014 |DAILY BEAST 

Nerney fears that Foerster has otherwise been largely forgotten except by his family and fellow cops. Cuba Protects America’s Most Wanted |Michael Daly |December 18, 2014 |DAILY BEAST 

And largely considered to have been, to those who can still remember, a successful and reasonably popular one. Be the Smarter Bush Brother, Jeb: Don’t Run! |Michael Tomasky |December 17, 2014 |DAILY BEAST 

Over the years, Crawford has been largely silent, speaking out only for an as-told-to obituary to Houston published in Esquire. Inside the Lifetime Whitney Houston Movie’s Lesbian Lover Storyline |Kevin Fallon |December 16, 2014 |DAILY BEAST 

And our views of poverty and social betterment, or what is possible and what is not, are still largely conditioned by it. The Unsolved Riddle of Social Justice |Stephen Leacock 

Its record is largely that of battles and sieges, of the brave adventure of discovery and the vexed slaughter of the nations. The Unsolved Riddle of Social Justice |Stephen Leacock 

It lacks convincingness perhaps from the fact that Thomass theology is so largely philosophy, as Roger Bacon said. The Mediaeval Mind (Volume II of II) |Henry Osborn Taylor 

Having seen no service, he owed his appointment largely to his conceit and good looks. Napoleon's Marshals |R. P. Dunn-Pattison 

At the door-step stood a woman in black, and she smiled largely, with dry chapped lips. Kipling Stories and Poems Every Child Should Know, Book II |Rudyard Kipling