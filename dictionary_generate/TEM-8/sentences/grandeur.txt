You’ve got the unpleasant parts of being outdoors, like rain and mosquitoes and so on, but without the grandeur and privacy of deep wilderness. How to Make Camping Fun |Blair Braverman |June 23, 2021 |Outside Online 

More than most substances, gold has accumulated grandeur over the centuries. Harnessing the Power of Gold |Sean Culligan |June 1, 2021 |Ozy 

It is one critics say aims to re-establish the grandeur of Empire, with the president as its modern-day sultan. The 'Badass Chief of Staff' of Turkey's Opposition Faces Years in Jail After Challenging Erdogan's Power. She's Not Backing Down |Joseph Hincks / Istanbul |February 24, 2021 |Time 

She doesn’t carry herself with aloofness or grandeur but rather like someone who still has faith that simple, hard work is rewarded. Samira Nasr, a fashion first at Harper’s Bazaar: ‘I just want to bring more people with me to the party’ |Robin Givhan |February 19, 2021 |Washington Post 

Earlier, the vacating occupant threw a tacky and sparsely attended going-away party for himself at Joint Base Andrews — an event intended to attach grandeur to his departure for Mar-a-Lago, but all it seemed to telegraph was desperation. America yearns for an era of good feeling. The inaugural ceremony launched one. |Peter Marks |January 20, 2021 |Washington Post 

There is a sense of grandeur in the idea that paying heavily is a means of advancing knowledge. The Daily Beast’s Best Longreads, Dec 1-7, 2014 |William Boot |December 7, 2014 |DAILY BEAST 

The dresses—their classiness and grandeur--spoke for themselves. Fashion Designer Oscar de la Renta, American Great, Dead at 82 |Tim Teeman |October 21, 2014 |DAILY BEAST 

Rizzo himself as usual, was sitting at a table by himself, wolfing his pizza in solitary grandeur. Portrait of the Consummate Con Man |John Lardner |May 17, 2014 |DAILY BEAST 

It also protects the individual against egotism and delusions of grandeur. New Year’s Reading List: Books to Transform Your Sad Life |David Masciotra |January 1, 2014 |DAILY BEAST 

And he just kept doing it with a persistence that is a grandeur. Lou Reed Lives! Why the Man With the Rock ‘n’ Roll Heart Isn’t Dead |Elizabeth Wurtzel |October 28, 2013 |DAILY BEAST 

The minister's eye kept steady to one point; to raise the country he governed, to the utmost pinnacle of earthly grandeur. The Pastor's Fire-side Vol. 3 of 4 |Jane Porter 

Certainly there are no five miles equal in rugged grandeur to those beginning just below and ending above West Point. Glances at Europe |Horace Greeley 

It burst upon them ere long with awful fury and grandeur, the elements warring with incredible vehemence. Hunting the Lions |R.M. Ballantyne 

At once dignified, solemn, and impressive, it combined every element of grandeur. The Posthumous Papers of the Pickwick Club, v. 2(of 2) |Charles Dickens 

Most girls of her age would have been enchanted and bewildered by this display of royal grandeur. Madame Roland, Makers of History |John S. C. Abbott