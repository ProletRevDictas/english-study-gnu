He wears his characteristic white shalwar kameez with a tattered gray waistcoat. Heart of Darkness: Into Afghanistan’s Taliban Valley |Matt Trevithick, Daniel Seckman |November 15, 2014 |DAILY BEAST 

A little gospel here, a little Chesterton there, a little waistcoat here. Mumford and Sons: The Pinterest of Folk Music |Justin Green |October 1, 2012 |DAILY BEAST 

At another point he forgot his waistcoat, and sent another man to look for it. The Vets Who Conquered Everest |Rob Verger |November 3, 2011 |DAILY BEAST 

She took the podium in a black McQueen waistcoat embroidered with gold flowers and knee-high leather boots. Inside Alexander McQueen's Memorial |Isabel Wilkinson |September 20, 2010 |DAILY BEAST 

His dove gray waistcoat matched his jacket and Lillian decided his trousers were a hideous shade of yellowish-brown. America's First Modern Celebrity |Laura Skandera Trombley |March 20, 2010 |DAILY BEAST 

I broke right into his story and seized the lapel of his waistcoat as though he were my dearest friend. The Soldier of the Valley |Nelson Lloyd 

From his waistcoat pocket he took a little silver convex mirror and surveyed himself critically therein. Dope |Sax Rohmer 

Still kneeling, he drew from his waistcoat pocket a powerful lens contained in a washleather bag. Dope |Sax Rohmer 

Rapidly he removed his reefer and his waistcoat, folded them, and placed them neatly beside his overall. Dope |Sax Rohmer 

So I put on my life-saving waistcoat and blew it out; clapped my new gas-mask on my head and entered. Gallipoli Diary, Volume I |Ian Hamilton