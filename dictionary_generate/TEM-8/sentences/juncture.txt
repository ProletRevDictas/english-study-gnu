Thomas’ genius is her ability to craft one man’s history in a way that illuminates the forces that brought us to this critical juncture. Angie Thomas’ New Prequel to The Hate U Give Challenges the Cult of Masculinity |Cleyvis Natera |January 12, 2021 |Time 

Three years later, these projects—with names like Polkadot, Filecoin and Dfinity—are reaching a critical juncture. The blockchain industry faces a moment of truth as high-profile projects go live |Jeff |October 21, 2020 |Fortune 

Another crucial career juncture was Fraser’s decision to accept the role of running strategy for Citi at the beginning of the financial crisis. Incoming Citi CEO Jane Fraser says she’s proof that working part-time isn’t a career killer |Claire Zillman, reporter |October 1, 2020 |Fortune 

At this important juncture, we asked several experts from different fields what their burning question about the coronavirus is. A Million Deaths From Coronavirus: Seven Experts Consider Key Questions |LGBTQ-Editor |September 30, 2020 |No Straight News 

In 2016, polls showed Democratic nominee Hillary Clinton ahead in numerous battleground states at this juncture in the race, but her lead narrowed and ultimately vanished in key states as October wore on. Early surge of Democratic mail voting sparks worry inside GOP |Amy Gardner, Josh Dawsey |September 29, 2020 |Washington Post 

Logistics wins the day, and the Supreme Deity is, at this juncture, nowhere to be seen. Meet Moses the Swashbuckling Israelite |James Romm |December 14, 2014 |DAILY BEAST 

There was no social cachet associated with jazz at that juncture in American history—if anything, the contrary. Jazz (The Music of Coffee and Donuts) Has Respect, But It Needs Love |Ted Gioia |June 15, 2014 |DAILY BEAST 

At a time when U.S.-Iran relations are at a crucial juncture, the claims in The Good Spy will not be welcome. Is Iran’s Missing General, Ali Reza Asgari, Living in the United States? |IranWire |May 30, 2014 |DAILY BEAST 

The correlation between tiredness and activity is not proven—at this juncture, anyway. Can Grandpa Kick Your Ass? |Anna Brand |December 13, 2013 |DAILY BEAST 

And then suddenly fall like a ten-pound weight on the keyboard at just the right juncture? Classic Miles Davis Recordings Reveal New Beauty in Their Classic Mono Format |Malcolm Jones |December 8, 2013 |DAILY BEAST 

Mr. Lowten bethought himself, at this juncture, of looking out of the window. The Posthumous Papers of the Pickwick Club, v. 2(of 2) |Charles Dickens 

At this juncture lord Delaware arrived with three ships, 150 men, and plenty of provisions, and settled the colony. The Every Day Book of History and Chronology |Joel Munsell 

The juncture was in a big, marshy, untillable flat, from which hills rose abruptly. Scattergood Baines |Clarence Budington Kelland 

That his removal at this juncture was extremely convenient was a fact that, on the other hand, she carefully concealed. Skipper Worse |Alexander Lange Kielland 

But the obstacles which should have hindered his assailants hindered Garnache even more at this juncture. St. Martin's Summer |Rafael Sabatini