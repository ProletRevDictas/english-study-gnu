A Walmart-Microsoft acquisition could be a boon for e-commerce companies that want alternatives to the digital advertising duopoly of Facebook and Google. What a Walmart-Microsoft bid for TikTok could mean for e-commerce |Anna Hensel |August 28, 2020 |Digiday 

The pandemic’s forced stay-at-home orders was a huge boon to Netflix, adding 26 million global subscribers in the first half of this year, compared to 12 million during the same period last year. After years of ‘too much TV,’ the pandemic means there’s now barely enough |Aric Jenkins |August 27, 2020 |Fortune 

The coronavirus pandemic has been a boon for the test proctoring industry. Software that monitors students during tests perpetuates inequality and violates their privacy |Amy Nordrum |August 7, 2020 |MIT Technology Review 

Yet another potential boon for targeted drug delivery is nanotechnology, whereby medical nanorobots have now been used to fight incidences of cancer. How AI Will Make Drug Discovery Low-Cost, Ultra-Fast, and Personalized |Peter H. Diamandis, MD |July 23, 2020 |Singularity Hub 

The real boon is that even statements about arithmetic formulas, called metamathematical statements, can themselves be translated into formulas with Gödel numbers of their own. How Gödel’s Proof Works |Natalie Wolchover |July 14, 2020 |Quanta Magazine 

The story of fluoridation reads like a postmodern fable, and the moral is clear: a scientific discovery might seem like a boon. Anti-Fluoriders Are The OG Anti-Vaxxers |Michael Schulson |July 27, 2016 |DAILY BEAST 

Apple is already positioning egg freezing as a boon to women at the company. Don’t Be Fooled by Apple and Facebook, Egg Freezing Is Not a Benefit |Samantha Allen |October 17, 2014 |DAILY BEAST 

Adding vehicles to the grid could be a boon to vehicle and fleet owners in several ways. Adding Vehicles to the Grid |The Daily Beast |October 8, 2014 |DAILY BEAST 

That could be a great boon to the Peshmerga, but not without costs. Obama’s Iraq Plan Has a Killer Flaw—and Airstrikes Alone May Not Save It |Jacob Siegel |August 8, 2014 |DAILY BEAST 

Now they do business year round, which has been a boon in times of economic crisis. Saying Goodbye to the Salvage Saviors of Giglio |Barbie Latza Nadeau |July 21, 2014 |DAILY BEAST 

Renounce the good law of the worshippers of Mazda, and thou shalt gain such a boon as the Murderer gained, the ruler of nations. Solomon and Solomonic Literature |Moncure Daniel Conway 

And now, gentlemen, I have a boon to ask—where there is so much joy, why not make all happy at once? Blackwood's Edinburgh Magazine, Volume 60, No. 372, October 1846 |Various 

The boon was granted, and I remember the wave of delight that swept over us, and how we enjoyed the long summer evenings. Fifty Years of Railway Life in England, Scotland and Ireland |Joseph Tatlow 

He died on March 3rd, 1844, at the age of eighty, having given his subjects the precious boon of twenty-five years of peace. Napoleon's Marshals |R. P. Dunn-Pattison 

Thus many artesian wells have been sunk in the Algerian Sahara which have proved an immense boon to the district. The Wonder Book of Knowledge |Various