League officials and leaders of the NFL Players Association say those still playing must guard against complacency. Chiefs, Bucs enter Super Bowl week with no positive coronavirus tests by players in weeks |Mark Maske |January 31, 2021 |Washington Post 

Although deepfakes haven’t yet become the weapons of mass disinformation that some predicted, there’s no room for complacency. Don’t underestimate the cheapfake |Amy Nordrum |December 22, 2020 |MIT Technology Review 

Constraints challenge teams to think divergently and avoid complacency in the ideation process. Want to innovate while working remotely? Rethink the way you brainstorm |Andrew Nusca |December 22, 2020 |Fortune 

There’s a clear cultural complacency with things as usual, and although disappointing, that’s not particularly surprising in a field where the vast majority just don’t understand the stakes. How our data encodes systematic racism |Amy Nordrum |December 10, 2020 |MIT Technology Review 

As the pandemic wears on, experts worry that complacency and fatigue could further fracture an already uneven response to the disease. As 2020 comes to an end, here’s what we still don’t know about COVID-19 |Science News Staff |December 9, 2020 |Science News 

In one sentence, he asserts: “Panic is worse than complacency.” The Sham, Scaremongering Guide to Ebola |Abby Haglage |November 20, 2014 |DAILY BEAST 

A psychiatrist who attended one such conference blamed television for the complacency. The Myth of the Central Park Five |Edward Conlon |October 19, 2014 |DAILY BEAST 

But judging by our complacency, you would be forgiven for not knowing this. Western Jihadists in Syria Threaten to Bring Their War Back Home |Maajid Nawaz |April 27, 2014 |DAILY BEAST 

They went out of their way to tell me how such programs “breed” complacency, laziness, and—wait for it—dependency. When Did ‘Dependence’ Become a Dirty Word? |Gene Robinson |April 6, 2014 |DAILY BEAST 

This is a film that takes apart your complacency as surely as this alien world destroys Thomas Newton. ‘The Man Who Fell to Earth’ Is a Classic Twice over—as a Movie and a Novel |Malcolm Jones |February 9, 2014 |DAILY BEAST 

But with the immaculate conception of Mary, a being full of grace, an object of God's supreme complacency entered this world. Mary, Help of Christians |Various 

It hardly ruffled the calm stream of his self-complacency, and, for some reasons, he was rather glad that it had happened. Julian Home |Dean Frederic W. Farrar 

It fell, and you were made to look with complacency on objects which not long since you would have regarded with horror. Key-Notes of American Liberty |Various 

Then, in the givers and in their gifts, in the workers and in their work, the Divine heart finds infinite complacency. Separation and Service |James Hudson Taylor 

Maitland regained his old self-complacency in time and was dreadfully mysterious and Maitlandish about the whole affair. The Romance of His Life |Mary Cholmondeley