If you’re sleeping during the daytime, you’re going to sleep less at night. Nap time is the new coffee break. Here’s how to make the most of it. |Galadriel Watson |February 1, 2021 |Washington Post 

A taste of any marks pastry chef Gregory Baumgartner as a brand to follow and helps explain the lines that form outside this daytime offshoot of the dinner-only, Levantine-inspired Albi in Navy Yard. Tom Sietsema’s 8 favorite places to eat right now |Tom Sietsema |January 26, 2021 |Washington Post 

Between now and the time when daytime temperatures again average 70 degrees, I will not wear any pants that are not lined in fleece. The Best Fleece-Lined Pants for the Outdoors |Wes Siler |January 15, 2021 |Outside Online 

For each of the pixels of land area on the global maps, they looked at how the maximum daytime and minimum nighttime temperatures changed over time. Nights are warming faster than days. Here’s what that means for the planet. |Ula Chrobak |October 9, 2020 |Popular-Science 

During the daytime, the enormous marine mammals must gobble up tons of krill daily to prepare for their epic migration to warmer southerly waters. We can protect whales from ship strikes by translating their songs |Kate Baggaley |October 2, 2020 |Popular-Science 

The news should have been handed out during the daytime, McPhearson said, when protests would have been were more peaceful. This Week's Riots Are Part of America's Long History of Racial Rage |Sharon Adarlo |November 29, 2014 |DAILY BEAST 

Meredith did some high-kicking with the Rockettes, and then breezed through a checklist of daytime staples: Fabulous prizes? Will Meredith Vieira Ever Stop Crying? Her Emotional Daytime TV Debut |Lloyd Grove |September 8, 2014 |DAILY BEAST 

The battle-scarred daytime TV vet agrees that Vieira could survive and even thrive, but offers a cautionary note. Can Meredith Vieira Break the Daytime Curse? |Lloyd Grove |September 4, 2014 |DAILY BEAST 

“There are actually three type of shows that succeed in daytime,” says the producer. Can Meredith Vieira Break the Daytime Curse? |Lloyd Grove |September 4, 2014 |DAILY BEAST 

One of their first observations about daytime sleep was that the dreaded mid-afternoon slump is part of human nature. 13 Tips for the Best Nap Ever |DailyBurn |August 12, 2014 |DAILY BEAST 

Miss Boutts replied that they were too busy in the daytime, but were asked once a week to a "bang-up" affair. Ancestors |Gertrude Atherton 

In the daytime he would lie concealed in some thicket, close to a road, his horse always picketed some distance from him. The Courier of the Ozarks |Byron A. Dunn 

No; for at first it only came at night, but after the horn was blown it came in the daytime as well. Second Edition of A Discovery Concerning Ghosts |George Cruikshank 

When it is new, and also when in its waning stages it is visible in the daytime, the spherical form is very apparent. Outlines of the Earth's History |Nathaniel Southgate Shaler 

There is also a night-class for those working in the daytime who desire to extend their theoretical knowledge. The Philippine Islands |John Foreman