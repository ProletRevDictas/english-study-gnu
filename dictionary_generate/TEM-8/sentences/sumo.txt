Employing Smartflex has enabled me not only to transition from a less optimal sumo position into a correct shoulder-width stance, but it’s also allowed me to make real progress on my deadlift for the first time since that crash. After 100 Workouts on Tonal, I’m Never Going to a Gym Again |wsiler |February 12, 2022 |Outside Online 

“I kind of like to think of them as sumo wrestlers,” says Rachel Keefe, a biology doctoral student at the University of Florida. These buff frogs never skip arm day |Sara Kiley Watson |September 25, 2020 |Popular-Science 

Kyo pledged to use all his power and political connections to help make sure Sumo was an Olympic event by 2008. The Yakuza Olympics |Jake Adelstein |February 7, 2014 |DAILY BEAST 

The Sumo Association pledged to end yakuza ties the same year. The Yakuza Olympics |Jake Adelstein |February 7, 2014 |DAILY BEAST 

The droll commercials include a current one for a Japanese bank in which Brad Pitt dines with a Sumo wrestler. Spike Jonze’s Wild World |Caryn James |October 6, 2009 |DAILY BEAST 

Al fin intima al Sumo Pontfice que renuncie al gobierno temporal de los Estados romanos. Novelas Cortas |Pedro Antonio de Alarcn 

Sero clypeum post vulnera sumo—I am too late in taking my shield after being wounded. Dictionary of Quotations from Ancient and Modern, English and Foreign Sources |James Wood