He closed by talking about bullies, saying the best way to deal with them is to punch them in the throat. A college football coach’s season at war with the coronavirus — and his own school |Kent Babb |January 19, 2021 |Washington Post 

However, in those rare cases, people can carry high amounts of the virus in their nose and throat, scientists recently reported, which suggests they may be able to transmit it to others. Already had COVID-19? You still need a vaccine. |Kate Baggaley |January 15, 2021 |Popular-Science 

Broken though it may be, America is ours, and together we can make this place anew, if we are finally honest with ourselves about the ugliness that has the country by the throat again. We Can Make America Anew Only If We're Honest About the Depth of the Ugliness and Hate Today |Eddie S. Glaude Jr. |January 11, 2021 |Time 

In it, a soldier stands on an Australian flag and grins maniacally as he holds a bloodied knife to a boy’s throat. Don’t underestimate the cheapfake |Amy Nordrum |December 22, 2020 |MIT Technology Review 

Ragnow took a hit to the throat area in the first quarter of Sunday’s game. Lions center advised not to talk after throat injury during game against Packers |Glynn A. Hill |December 17, 2020 |Washington Post 

I took out my knife, my Ka-Bar, and knocked his teeth out, but they fell into his throat. The Story Behind Lee Marvin’s Liberty Valance Smile |Robert Ward |January 3, 2015 |DAILY BEAST 

The pressure against my throat seemed completely constricting. Mailer’s Letters Pack a Punch and a Surprising Degree of Sweetness |Ronald K. Fried |December 14, 2014 |DAILY BEAST 

With senility's fingers at his throat, it was clear that no more movies were going to be made. Alfred Hitchcock’s Fade to Black: The Great Director’s Final Days |David Freeman |December 13, 2014 |DAILY BEAST 

He died in July after being grabbed around the throat by a cop and wrestled to ground where the breath flew out of him. The Wildly Peaceful, Human, Almost Boring, Ultimately Great New York City Protests for Eric Garner |Mike Barnicle |December 8, 2014 |DAILY BEAST 

The coup came just months after Simon revealed he had beaten throat cancer. Drawing Room Coup at Brideshead House Forces Out Simon And Becci Howard |Tom Sykes |November 24, 2014 |DAILY BEAST 

A sob rose in her throat, and broke from her lips transformed into a trembling, sharp, glad cry. The Bondboy |George W. (George Washington) Ogden 

Conny stepped smilingly forward, and proceeded to affix the band around the vicar's massive throat. The Pit Town Coronet, Volume I (of 3) |Charles James Wills 

Henceforth he must remember Winifred only when his sword was at the throat of some wretched mutineer appealing for mercy. The Red Year |Louis Tracy 

Black Sheep looked up at Harry's throat and then at a knife on the dinner-table. Kipling Stories and Poems Every Child Should Know, Book II |Rudyard Kipling 

He usually seizes his prey by the flank near the hind leg, or by the throat below the jaw. Hunting the Lions |R.M. Ballantyne