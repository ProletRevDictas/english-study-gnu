The documentary dives into Cardin’s history, from his origins as an haute couture designer in 1950s Paris to his expansion into ready-to-wear clothing for a global market and eventually, industrial design. ‘House of Cardin’ aims to be more than a fashion documentary |radmarya |August 27, 2020 |Fortune 

He doesn’t spend money on clothing, and when movie theaters reopen, he plans to avoid buying overpriced nachos and popcorn “for at least a year.” Could the Recession Revive the Savings Gene in China and India? |Pallabi Munsi |August 16, 2020 |Ozy 

Also, the life cycle of clothing isn’t as long as it once was. Scientists Gene-Hack Cotton Plants to Make Them Every Color of the Rainbow |Jason Dorrier |August 11, 2020 |Singularity Hub 

Zeidan noted to the Blade that Helem at the beginning of the pandemic launched food and clothing drives. Beirut explosion nearly destroys LGBTQ group’s offices |Michael K. Lavers |August 10, 2020 |Washington Blade 

I assume it’s roughly the same face that she made when I confessed my masked trip to a clothing store to buy some summer dresses. Every Decision Is A Risk. Every Risk Is A Decision. |Maggie Koerth (maggie.koerth-baker@fivethirtyeight.com) |July 21, 2020 |FiveThirtyEight 

Today, the city is an Asian hipster outpost, with shopping malls, clothing boutiques, and mixologist-prepared cocktails. Cambodia’s Smoke-and-Mirrors Democracy |David Shaftel |January 9, 2015 |DAILY BEAST 

The healthier appearance and civilian clothing are very peculiar. ISIS’s Futile Quest to Go Legit |Jamie Dettmer |January 5, 2015 |DAILY BEAST 

Families stuff a life-size male doll with memories of the outgoing year and dress him in their clothing. New Year’s Eve, Babylon Style |Candida Moss |December 31, 2014 |DAILY BEAST 

His clothing line that his friend described as “upscale and urban” was a dud. Alleged Cop Killer’s Blood-Soaked Screenplay |M.L. Nestel |December 24, 2014 |DAILY BEAST 

The sport of surfing is a very sexy sport, beautiful people on beautiful beaches in minimal clothing. Anastasia Ashley, Surfer-Cum-Model, Rides The Viral Internet Wave |James Joiner |December 23, 2014 |DAILY BEAST 

The plain furniture was stiffly arranged, and there was no litter of clothing or small feminine belongings. Rosemary in Search of a Father |C. N. Williamson 

I find the weather very cool, however, and one needs warm clothing here. Music-Study in Germany |Amy Fay 

It was a habit with him to disguise himself in ordinary clothing and then to go out and mingle with the common people. Our Little Korean Cousin |H. Lee M. Pike 

She and Henrietta were fast becoming dried, and their outer clothing could soon be put on again. The Campfire Girls of Roselawn |Margaret Penrose 

His clothing was also, in part, that of a parader: a brilliant-hued coat worn over his ordinary faded suit of denim. Dorothy at Skyrie |Evelyn Raymond