She offered hymns and moments of silence, and said she “wanted to bring flowers” to the old mummy. It may be time for museums to return Egyptian mummies to their coffins |Purbita Saha |June 28, 2021 |Popular-Science 

Even at the age of 107, Fletcher can still hear the sound of her mother and siblings singing, the melody of “Go Tell It on the Mountain” and other gospel hymns reverberating through the halls of their home. A Survivor of the Tulsa Race Massacre Says Her Family Is Still Trying to Break Its Curse, 100 Years Later |Paulina Cachero |May 29, 2021 |Time 

He sits cross-legged on the boat and chants hymns, drops some flowers and pours milk on the surface of the Ganges. When a Body Meets a Boatman on the Ganges |Eugene Robinson |February 23, 2021 |Ozy 

Facing the rising sun, he performs some yogic and breathing exercises before finally entering the water chanting hymns in Sanskrit for his ritual dip. When a Body Meets a Boatman on the Ganges |Eugene Robinson |February 23, 2021 |Ozy 

It played the first notes of Amazing Grace, a hymn and a political declaration alike that stands as a testimonial for forgiveness and redemption. The Healing Power of Garth Brooks |Philip Elliott |January 21, 2021 |Time 

The closest she got to a religious hymn was her father's recurring phrase: "That's a great line, write it down." Delia Ephron Talks Her Memoir, ‘Sister, Mother, Husband, Dog’ |Erin Cunningham |September 19, 2013 |DAILY BEAST 

He was so gifted that he could have won any hymn-singing competition, so he never bothered to enter them. Ann Wroe’s ‘Orpheus’: Why the Mythological Muse Haunts Us |Ann Wroe |May 31, 2012 |DAILY BEAST 

There a professional choir had assembled, repeatedly singing the hymn “Hallelujah” a cappella. Silvio Berlusconi Steps Down: Protesters Celebrate in Front of His Home |Barbie Latza Nadeau |November 12, 2011 |DAILY BEAST 

This whimsical story was based on a hymn that she recorded for a CD years ago. Kathie Lee Gifford's Favorite Books |The Daily Beast |June 15, 2011 |DAILY BEAST 

For more on her latest book, Battle Hymn of the Tiger Mother , visit amychua.com. The Tiger Mom's SAT Surprise |Amy Chua |March 20, 2011 |DAILY BEAST 

But at last, as he was on the point of dropping asleep, Madame Torvestad proposed that they should conclude with a hymn. Skipper Worse |Alexander Lange Kielland 

As soon as the hymn was finished, the daughters of the house brought in tea and bread and butter. Skipper Worse |Alexander Lange Kielland 

La Source composed a beautiful hymn, adapted to a sweet and solemn air, which they called their evening service. Madame Roland, Makers of History |John S. C. Abbott 

But when he had finished, Sivert Jespersen, with a cringing smile, said: "I think now we had better sing a hymn." Skipper Worse |Alexander Lange Kielland 

Nobody ventured to touch the dessert, and, after the hymn, the old dyer read a grace after meat. Skipper Worse |Alexander Lange Kielland