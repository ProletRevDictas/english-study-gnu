More recently in topology, I have enjoyed Josh Greene’s changemaker vectors, whose components can sum to any integer less than their total value, as if making exact change with cash. Why Mathematicians Should Stop Naming Things After Each Other - Issue 89: The Dark Side |Laura Ball |September 2, 2020 |Nautilus 

Compared to Walker, they’re no longer variables but fixed integers that make Boston the title contender it is. Give Boston’s Kemba Walker A Double Pick And Watch Him Work |Michael Pina |August 31, 2020 |FiveThirtyEight 

In order to produce any given percentage of huskyness to the nearest integer, you need a contribution “least count” of less than 1% so that you don’t skip any integer. How to Design (or at Least Model) Mixed Dog Breeds |Pradeep Mutalik |July 31, 2020 |Quanta Magazine 

Note, however, that since 128 is more than 100, there are some integer percentages that can be produced in more than one way from 128 purebreds. How to Design (or at Least Model) Mixed Dog Breeds |Pradeep Mutalik |July 31, 2020 |Quanta Magazine 

Gödel numbers are integers, and integers only factor into primes in a single way. How Gödel’s Proof Works |Natalie Wolchover |July 14, 2020 |Quanta Magazine 

A girl bound for college became immediately an integer with which a young man who had not yet mislaid his diploma could reckon. A Hoosier Chronicle |Meredith Nicholson 

But just before the catastrophe one man, integer vitae scelerisque purus, sees the truth. The Longest Journey |E. M. Forster 

In my former book I handled Japan as an integer, avoiding implications. The Pacific Triangle |Sydney Greenbie 

Korea might just as well be an integer of the Japanese Empire. The Pacific Triangle |Sydney Greenbie 

Hence, it is of the utmost importance that China remain an integer. The Pacific Triangle |Sydney Greenbie