She was remarkably nice, and by the time she admitted that she—in an effort to impress us—had watched Spikeball tutorials prior to coming, I knew it was more than a fling. Spikeball Is the Perfect Pandemic Activity |smurguia |August 10, 2021 |Outside Online 

I didn’t have to wonder if he saw me as a fling or if he was ready for a serious girlfriend. Bridal Bliss: Courtney And Torrey Said “I Do” In A Stunning Fourth of July Fête |Victoria Uwumarogie |July 14, 2021 |Essence.com 

In the 1990s, a team at Brookhaven National Laboratory on Long Island built a 50-foot-wide ring to fling muons around and began collecting data. ‘Last Hope’ Experiment Finds Evidence for Unknown Particles |Natalie Wolchover |April 7, 2021 |Quanta Magazine 

She’d had on-trail romances before, and Constantine, she thought, was funny and handsome enough for a rebound fling—some “trail tail,” she kids. What It’s Like to Fall in Love on a Long Hike |Grayson Haver Currin |February 13, 2021 |Outside Online 

Whether he’s rushing into a fling or trying to escape a heartbreak, time never seems to be moving fast enough. Morgan Wallen’s big moment feels about 19 songs too long |Chris Richards |January 14, 2021 |Washington Post 

The protection will last as long as Winston can still fling the ball 50 yards downfield to a streaking wide receiver. Jameis Winston Cleared of Rape Like Every Other College Sports Star |Robert Silverman |December 22, 2014 |DAILY BEAST 

Did you and Christian Slater have a romantic fling on True Romance? Patricia Arquette Uncut: Drunken Mischief with Johnny Depp, ‘True Romance’ Crush, and ‘Boyhood’ |Marlow Stern |July 16, 2014 |DAILY BEAST 

Will she have a terrible relapse and turn to her pal/sometime fling Vause for help? ‘Orange Is the New Black’: Inside the Wild S2 Finale and What’s Next for Season 3 |Kevin Fallon, Marlow Stern |July 12, 2014 |DAILY BEAST 

Remember, the people surveyed have already signed up for a fling. Japan’s Desperate Housewives Opting for Adulterous Online Dating |Angela Erika Kubo, Jake Adelstein |April 2, 2014 |DAILY BEAST 

But this time, we were looking for more than just a couple of great dates or a fling. Best Cities to Find Love and Stay in Love |Brandy Zadrozny, Rachel Bronstein |February 14, 2014 |DAILY BEAST 

When I come home from the lessons I fling myself on the sofa, and feel as if I never wanted to get up again. Music-Study in Germany |Amy Fay 

At sight of this generous enemy, this faithful friend, how could he restrain the grateful impulse to fling himself into his arms! The Pastor's Fire-side Vol. 3 of 4 |Jane Porter 

Honey-Bee was too proper to fling hers up also, so taking off the shoe that wouldn't stay on she threw it joyfully over her head. Honey-Bee |Anatole France 

This, with contemptuous indignation, we fling back into their face, as a scorpion to a vulture. The Condition, Elevation, Emigration, and Destiny of the Colored People of the United States |Martin R. Delany 

Without uttering a word, I pick up the heavy saltcellar, and fling it violently against the French mirror. Prison Memoirs of an Anarchist |Alexander Berkman