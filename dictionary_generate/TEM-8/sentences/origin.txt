I think we can see Liz’s story is sort of an origin point for so many cultural themes today. 'We All Have to Create Our Own Universe.' Producer and Artist Zackary Drucker on Telling Nuanced Trans Stories |Suyin Haynes |February 8, 2021 |Time 

Its 2018 report, “Reclaiming Native Truth,” explored the perceptions that Americans have of Native people, the origins of those perceptions and the impact that the perceptions have on Native people, particularly children. ‘End racism,’ the NFL implored. So what about that Chiefs’ name? |Liz Clarke |February 5, 2021 |Washington Post 

In speaking with Parents for Peace, however, she began to think more about the origins of his views. After Capitol riots, desperate families turn to groups that ‘deprogram’ extremists |Paulina Villegas, Hannah Knowles |February 5, 2021 |Washington Post 

The notion of centering my column on “new” revelations about the origins of the Pentagon Papers seemed to be collapsing. Seeing the Pentagon Papers in a New Light |by Stephen Engelberg |February 3, 2021 |ProPublica 

The scenario involved one storm, with origins in the Pacific Ocean, that would race across the country, die out over Ohio, and then hand off its energy to a secondary storm developing off the Mid-Atlantic coast. How an imperfect snowstorm forecast turned out mostly right |Jason Samenow |February 3, 2021 |Washington Post 

“The origin of Brokpas is lost in antiquity,” a research article from the University of Delhi notes. The Himalayas’ Hidden Aryans |Nina Strochlic |January 3, 2015 |DAILY BEAST 

The mythic origin of the feast was the creation of the world by the god Marduk. New Year’s Eve, Babylon Style |Candida Moss |December 31, 2014 |DAILY BEAST 

Some of the more notorious “green on blue” attacks have their origin in such outraged honor. Afghanistan, We Hardly Knew You |Jonathan Foreman |December 8, 2014 |DAILY BEAST 

Black Alice and Strix have origin stories that more closely resemble the archetypal comic heroes. Gail Simone’s Bisexual Catman and the ‘Secret Six’ |Rich Goldstein |December 6, 2014 |DAILY BEAST 

The virus had to come from somewhere, but no one could figure out its origin. Bats’ Link to Ebola Finally Solved |Carrie Arnold |November 12, 2014 |DAILY BEAST 

Just corporeal enough to attest humanity, yet sufficiently transparent to let the celestial origin shine through. Pearls of Thought |Maturin M. Ballou 

But not only has the name tobacco and the implements employed in its use caused much discussion but also the origin of the plant. Tobacco; Its History, Varieties, Culture, Manufacture and Commerce |E. R. Billings. 

A marked increase indicates some pathologic condition at the site of their origin. A Manual of Clinical Diagnosis |James Campbell Todd 

William King, archbishop of Dublin, died; author of a celebrated treatise on the origin of evil. The Every Day Book of History and Chronology |Joel Munsell 

Carpenter were the leaders, and this is claimed to have been the origin of Mechanics' Institutes. Showell's Dictionary of Birmingham |Thomas T. Harman and Walter Showell