In the past year, the conversation about bird names has taken on a particular sense of urgency. Inside the Movement to Abolish Colonialist Bird Names |Nathalie Alonso |February 12, 2021 |Outside Online 

They’re choosing some combination of sensitive and non-sensitive questions, ostensibly because the payment isn’t enough to overcome some of the problems that they might have with asking particular sensitive questions. Can I Ask You a Ridiculously Personal Question? (Ep. 451) |Stephen J. Dubner |February 11, 2021 |Freakonomics 

As part of that second leaderboard, he is the king of Super Bowls in particular. All The Ways That Tom Brady Is Football’s GOAT |Neil Paine (neil.paine@fivethirtyeight.com) |February 9, 2021 |FiveThirtyEight 

At that time, the strategist said Weaver did nothing to suggest he had an interest in men, let alone an interest in the strategist in particular. Lincoln Project’s avowed ignorance of Weaver texts undercut by leaked communications |Chris Johnson |February 9, 2021 |Washington Blade 

Home Games has developed three products in particular to optimize mobile game creation. Hyper casual game publisher Homa Games raises $15 million |Romain Dillet |February 9, 2021 |TechCrunch 

There is a particular focus in the magazine on attacking the United States, which al Qaeda calls a top target. U.S. Spies See Al Qaeda Fingerprints on Paris Massacre |Shane Harris, Nancy A. Youssef |January 8, 2015 |DAILY BEAST 

For those living in poor communities in particular, interactions with police rarely come with good news and a smile. How to Solve the Policing Crisis |Keli Goff |January 5, 2015 |DAILY BEAST 

He stopped at one point to ask someone directions to a particular housing development. Exclusive: Inside a Cop-Killer’s Final Hours |Michael Daly |December 31, 2014 |DAILY BEAST 

The lack of a cannon is a particular problem, as the F-35 is being counted on to help out infantrymen under fire. New U.S. Stealth Jet Can’t Fire Its Gun Until 2019 |Dave Majumdar |December 31, 2014 |DAILY BEAST 

Americans move around a lot, making it hard to form attachments to any particular place. Will Texas Stay Texan? |David Fontana |December 29, 2014 |DAILY BEAST 

In particular the Governor of Adinskoy offered us a guard of fifty men to the next station, if we apprehended any danger. The Life and Most Surprising Adventures of Robinson Crusoe, of York, Mariner (1801) |Daniel Defoe 

But I doubt if he feels any particular emotion himself, when he is piercing you through with his rendering. Music-Study in Germany |Amy Fay 

No one ever argued with Levison; all understood that this particular phrase was final. The Pit Town Coronet, Volume I (of 3) |Charles James Wills 

One old aunt in particular visited him twice a year, and stayed six months each time. The Book of Anecdotes and Budget of Fun; |Various 

It is the will directing the activity of the intellect into some particular channel and keeping it there. Assimilative Memory |Marcus Dwight Larrowe (AKA Prof. A. Loisette)