The hardware apparently dates back to plans to revitalize Windows for phones, but after that plan fell through, the hardware was upcycled into the most head-scratching Android phone of the year. Surface Duo review—Orphaned Windows hardware makes a poor Android device |Ron Amadeo |October 16, 2020 |Ars Technica 

Target in 2017 launched a $7 billion program to update its stores to resounding success, revitalizing a big-box chain that had lost its way but is now raking in record in-store and online sales. Walmart unveils new store design inspired by Amazon and airports |Phil Wahba |September 30, 2020 |Fortune 

It protects the high limit in coastal neighborhoods and revitalizes this landlocked district. Politics Report: Forged Footnote 15 |Scott Lewis and Andrew Keatts |September 12, 2020 |Voice of San Diego 

“These incentives are highly underutilized and have the potential to unleash a series of opportunities in distressed communities, revitalizing the economy, creating jobs, affordable housing, and healthier neighborhoods,” he said. New South Bay Supervisor’s To-Do List: Ensure an Equitable Recovery |Maya Srikrishnan |August 31, 2020 |Voice of San Diego 

It never works to revitalize those neighborhoods, but it’s a policy that endures. Should America (and FIFA) Pay Reparations? (Ep. 426) |Stephen J. Dubner |July 16, 2020 |Freakonomics 

Inside ISIS-held areas, the Assad regime is quietly working to revitalize long-existing intelligence networks to fight ISIS there. Where Does Obama Stand on Assad? |Hassan Hassan |November 1, 2014 |DAILY BEAST 

Get SocialNot only will a mental break revitalize you, but every social interaction is also an exchange of energy. Short on Zzz’s? 15 Research-Backed Sleep Hacks |DailyBurn |May 9, 2014 |DAILY BEAST 

After the Second World War, government attempts to revitalize the wine trade compromised the quality of the wine produced. Germany’s Wine Revolution Is Just Getting Started |Jordan Salcito |April 26, 2014 |DAILY BEAST 

The Garden at The Standard East Village The Standard has been known to revitalize neighborhoods time and time again. New York City’s Best New Hotspots This Spring |Sara Sayed, Valeriya Safronova |April 2, 2014 |DAILY BEAST 

Here, nine GOP women with the potential to revitalize the right. Nine Women Remaking the Right |Patricia Murphy |September 3, 2013 |DAILY BEAST 

THE primary value of the patriotic play lies in its appeal to the love of country, and its power to revitalize the past. Patriotic Plays and Pageants for Young People |Constance D'Arcy Mackay 

To his notion, the thing most needed to revitalize Prouty was an electric car-line. The Fighting Shepherdess |Caroline Lockhart 

In the U-boat there were potash cartridges to take up the carbon-dioxide, and tanks of pure oxygen to revitalize the air. Inventions of the Great War |A. Russell (Alexander Russell) Bond 

Concurrently, he devoted the aforementioned enthusiasm to heading a program to revitalize the game; with significant results. Squash Tennis |Richard C. Squires 

Exercise will relax the muscles and call for blood which will revitalize and stimulate the weakened conditions. Alcohol: A Dangerous and Unnecessary Medicine, How and Why |Martha M. Allen