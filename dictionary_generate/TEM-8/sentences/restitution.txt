Those losses were multiplied by millions of lives, over hundreds of years, stunting the development of a continent whose governments have since struggled to find the will to ask for restitution. What is owed Africa |Lynsey Chutel |October 13, 2020 |Quartz 

Of the 45,300 felons Sukhatme identified on the voter rolls, 78% might owe fees, fines or restitution, he said. In Florida, the Gutting of a Landmark Law Leaves Few Felons Likely to Vote |by Lawrence Mower and Langston Taylor, Miami Herald and Tampa Bay Times |October 7, 2020 |ProPublica 

They also don’t know how many felons on the voter rolls owe court fees, fines or restitution that would disqualify them from voting under a subsequent state law that limited the amendment’s scope. In Florida, the Gutting of a Landmark Law Leaves Few Felons Likely to Vote |by Lawrence Mower and Langston Taylor, Miami Herald and Tampa Bay Times |October 7, 2020 |ProPublica 

The CFTC said its order will recognize and offset restitution and disgorgement payments made to the Department of Justice and Securities and Exchange Commission. JP Morgan will pay record settlement to resolve ‘spoofing’ case against 15 traders |kdunn6 |September 30, 2020 |Fortune 

A rental car customer from New Mexico, Jeffrey Garvin, then sued the rental car companies in 2020 seeking class action status to obtain restitution and stop the fee. Port Settlements Will Send Refunds to Airport Rental Car Customers |Ashly McGlone |September 22, 2020 |Voice of San Diego 

Serna was sentenced to 121 months in prison for his crimes and ordered to pay $4.6 million in restitution. Ponzi-Scheming Pastor Fleeced His Flock Out of Millions |Brandy Zadrozny |November 11, 2014 |DAILY BEAST 

In 1970, and again in 1980, restitution was back on the agenda, and my mother and my aunt reclaimed paintings by Monet and Léger. My Grandfather's War: Recovering the Art the Nazis Stole |Anne Sinclair |October 5, 2014 |DAILY BEAST 

If they are definitively convicted, they also will share in the responsibility of restitution, whether the house is sold or not. The Perugia House Where Meredith Kercher Was Murdered Is for Sale |Barbie Latza Nadeau |January 3, 2014 |DAILY BEAST 

Sentenced to 180 days in jail; served 30 days with three years probation and $600,000 restitution to Bloom. The Real Bling Ring: Where Are They Now? |Tricia Romano |May 21, 2013 |DAILY BEAST 

In 2010, he was ordered to pay more than $790,000 in restitution, and sentenced to 21 months in federal prison. Anti-Muslim Movie Maker a Meth Cooker |Christine Pelisek |September 13, 2012 |DAILY BEAST 

Consequently, though the donor's intention may be subsequently altered, no obligation to make restitution will arise. Putnam's Handy Law Book for the Layman |Albert Sidney Bolles 

Upon the 1st of April the restitution of the colony to the Spaniards was solemnized. Celebrated Travels and Travellers |Jules Verne 

Even as she had hastened to be revenged on Howard Templeton for her wrongs, she now made haste to offer restitution. A Dreadful Temptation |Mrs. Alex. McVeigh Miller 

And when that act of restitution was accomplished, Xenie fell into strange and dangerous apathy. A Dreadful Temptation |Mrs. Alex. McVeigh Miller 

Prince Henry's favour had brought liberty and restitution very close. Sir Walter Ralegh |William Stebbing