Taking him at his word that he wants to help, it’s worth examining how his donations could have the biggest and most equitable impact. Can Billionaires Really Save Us from Climate Disaster? |Heather Hansman |February 12, 2021 |Outside Online 

A new biweekly podcast called Sex Outside, which launched February 11, examines how outdoor pursuits, sex, gender, and bodies flow into each other. A New Podcast Explores Sex in the Great Outdoors |Heather Hansman |February 12, 2021 |Outside Online 

The first, released last fall, examined the risk of disease transmission aboard aircraft. Airports have taken steps to reduce coronavirus transmission but risks still remain, study says |Lori Aratani |February 12, 2021 |Washington Post 

Let’s examine each of these winter weather possibilities and also look back at how much snow fell from the first wave on Wednesday night. Snow expected tonight, mainly south of D.C., before possible ice on Saturday |Jason Samenow, Wes Junker |February 11, 2021 |Washington Post 

The onus is on the player to carefully examine surroundings to figure out where to go and what to do next. ‘Little Nightmares II’ made me dread every moment. And I loved it. |Elise Favis |February 9, 2021 |Washington Post 

It is also the first study to thoroughly examine emergency department use for post-abortion care. Abortion Complications Are Rare, No Matter What the Right Says |Samantha Allen |December 8, 2014 |DAILY BEAST 

Any institution striving to examine such an iconic figure would find formidable challenges. The Virgin Mary Lookbook |William O’Connor |December 7, 2014 |DAILY BEAST 

It is simply that we have failed to understand and examine the factors that are putting young gay men at risk. The New Face of HIV Is Gay & Young |Adebisi Alimi |December 1, 2014 |DAILY BEAST 

It was a strangely shaped block, due to the area once being underwater, and he took it home with him to examine closer. The Postman Who Built a Palace in France…by Hand |Nina Strochlic |November 20, 2014 |DAILY BEAST 

Gillespie had forensic-imaging experts examine photos taken of the plane and its patched-over window at the Miami airport. How Amelia's Plane Was Found |Michael Daly |October 30, 2014 |DAILY BEAST 

Monsieur le Maire,” said he, “I should like to examine the premises, and beg that you will have the kindness to accompany me. The Joyous Adventures of Aristide Pujol |William J. Locke 

I have not done this before as I had not sufficient leisure to examine them, or do so in the interval allowed by the season. The Philippine Islands, 1493-1898, Volume XX, 1621-1624 |Various 

On nearing the spot, Tom stopped a few moments, and bent down to examine a beautiful flower. Hunting the Lions |R.M. Ballantyne 

The farmer stooped down, and raised the shabby bonnet from the face of the woman to examine her more carefully. The World Before Them |Susanna Moodie 

When cool, replace the acid with water, and examine for hemin crystals with two-thirds and one-sixth objectives. A Manual of Clinical Diagnosis |James Campbell Todd