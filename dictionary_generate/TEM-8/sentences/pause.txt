The Minnesota Wild, on pause for the past week, announced Thursday that it will reopen for team activities Friday. NHL adds game-day rapid testing to coronavirus protocols |Samantha Pell |February 12, 2021 |Washington Post 

The one aspect of this dish that initially gave me pause was the tomatoes, given the time of year. TikTok’s viral baked feta pasta is worth the hype |Aaron Hutcherson |February 11, 2021 |Washington Post 

Yes, I of course refer to the SEO industry and yes, you may take a small pause here to go through the above statement to see if it works. The future of Google and what it means for search |Pete Eckersley |February 5, 2021 |Search Engine Watch 

Though Smith went 5-1 as a starter, the bone bruise that kept him from starting three of the year’s final four games gave everyone pause. Finding the right QB is the first — and most crucial — task for Washington’s new front office |Les Carpenter |February 4, 2021 |Washington Post 

Saint Louis came off a month-plus pause and promptly lost at home to Dayton. NCAA tournament bracketology: Two No. 1 seeds could go to teams outside power conferences |Patrick Stevens |February 2, 2021 |Washington Post 

But pause for a second, and look back at what these generations of regulators and lawmakers have created. Red Tape Is Strangling Good Samaritans |Philip K. Howard |December 27, 2014 |DAILY BEAST 

After a pause she invited me in with a warm smile, as if I were a neighbor. The Life and Hard Times Of The Family A Cuban Defector Left Behind |Brin-Jonathan Butler |December 19, 2014 |DAILY BEAST 

I had to pause for sheep crossing the road, which is a common occurrence when driving through the Highlands of Scotland. A Whisky Connoisseur Remembers That First Sip of The Macallan | |December 10, 2014 |DAILY BEAST 

According to James Madison, there was “a considerable pause.” Why We Can’t Quit Calling Presidents ‘Kings’ |Kevin Bleyer |November 22, 2014 |DAILY BEAST 

We continuously pause to pull them out while Zalwar Khan and his companion smirk at us and chew unbothered. Heart of Darkness: Into Afghanistan’s Taliban Valley |Matt Trevithick, Daniel Seckman |November 15, 2014 |DAILY BEAST 

After a minute's pause, while he stood painfully silent, she resumed in great emotion. The Pastor's Fire-side Vol. 3 of 4 |Jane Porter 

In fact, his appearance was so formidable that Davy did not pause for a second look, but started off at the top of his speed. Davy and The Goblin |Charles E. Carryl 

There was a pause, during which Blanche went through a little mute exhibition of amazement and pleasure. Confidence |Henry James 

For who, while tears are falling, will pause to handle the wreaths, and find whether they are genuine? Solomon and Solomonic Literature |Moncure Daniel Conway 

There was a moment's pause, and Doa Inez returned into the saloon, which was now beginning rapidly to fill. Blackwood's Edinburgh Magazine, Volume 60, No. 372, October 1846 |Various