Not only are the run-ins taking longer to play out, but they’re also materializing earlier. Fighting Didn’t Stop In The NHL Bubble |Josh Planos |August 26, 2020 |FiveThirtyEight 

Certainly, history tells us that plenty of storylines will materialize — or be lost forever — depending on which section of 60 games happens to be the 60 that get played this year. Bad Teams May Be Posing As Good Teams In A 60-Game Baseball Season |Neil Paine (neil.paine@fivethirtyeight.com) |August 7, 2020 |FiveThirtyEight 

Others have suggested that magnetism materialized microseconds later, when protons formed. The Hidden Magnetic Universe Begins to Come Into View |Natalie Wolchover |July 2, 2020 |Quanta Magazine 

The theory doesn’t address, for example, how “live” organisms and “evil” organisms managed to materialize from a primordial smoothie containing both right- and left-handed building blocks. Cosmic Rays May Explain Life’s Bias for Right-Handed DNA |Charlie Wood |June 29, 2020 |Quanta Magazine 

The hang-up partly explains why universal testing plans in skilled nursing facilities were slow to materialize. COVID-19 Lawsuit Against La Jolla Facility Could Signal More Fights to Come |Jared Whitlock |June 23, 2020 |Voice of San Diego 

This Oath Keeper was there for the protest, which had yet to materialize, and had a few friends joining him, he told me. NYC’s Garner Protesters vs. Pro-Cop Protesters |Jacob Siegel |December 20, 2014 |DAILY BEAST 

Backup dancers materialize, sparking a torrent of eye-catching choreography. The Making of Kiesza: From Navy Sharpshooter to Beauty Queen to Pop Diva |Marlow Stern |October 20, 2014 |DAILY BEAST 

It took the beheading videos, and the deaths of two Americans, for that support to materialize. Hey, Congress: Vote on the Damn War |Michael Tomasky |September 30, 2014 |DAILY BEAST 

But the unity deal failed to materialize after the Palestinian Authority declined to pay the salaries of Hamas civil servants. Senators: Take Gaza Away From Hamas |Eli Lake |September 22, 2014 |DAILY BEAST 

Furthermore, by removing them from their post, you create an opportunity for insurgent black magic to materialize a car bomb. Whatever You Do Someone Will Die. A Short Story About Impossible Choices in Iraq |Nathan Bradley Bethea |August 31, 2014 |DAILY BEAST 

And as fast as they could carry a galley of type from the dump, another galley would just materialize there. Nine Men in Time |Noel Miller Loomis 

It now looked as if the drink of milk might materialize, but alas for human expectations! Gold-Seeking on the Dalton Trail |Arthur R. Thompson 

What glorious rows of head-lines they must have seen as a last vision beautiful, never destined to materialize in printer's ink! The Poison Belt |Arthur Conan Doyle 

Some of these foreign elements, like the -ize of materialize or the -able of breakable, are even productive to-day. Language |Edward Sapir 

Yet the hotel did not materialize, and the "view" neither fed nor warmed nor clothed the patient proprietors of the desolate spot. McClure's Magazine, January, 1896, Vol. VI. No. 2 |Various