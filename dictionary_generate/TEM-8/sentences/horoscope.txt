Greene also notes the importance, especially for Gen Zs and younger Millennials, of establishing a digital identity—which personalized app-based horoscopes and personality readings like The Pattern can help shape. High-Tech Astrology Apps Claim to Be More Personalized Than Ever. Gen Z-ers Are Turning Out to Be Believers |Raisa Bruner |July 23, 2021 |Time 

Fox, 35, typically begins her day with tarot cards and reading her horoscope. The Megan Fox renaissance is here. It’s long overdue. |Ilana Kaplan |July 2, 2021 |Washington Post 

She consulted an astrologer and demanded that the president’s staff schedule him around horoscope predictions. On the world stage, Nancy Reagan found the role of a lifetime |Barbara Perry |April 23, 2021 |Washington Post 

These typologies most resemble horoscopes, in the sense that it can’t be very hard to recognize yourself – or your children – at least partially in any of them. Does Birth Order Really Determine Personality? Here's What the Research Says |Lynn Berger |April 13, 2021 |Time 

The space agency started by clarifying that astronomy and astrology are in no way the same field and practice, then dove into a mathematical breakdown of why a 13th sign wouldn’t really change people’s horoscope charts. Your favorite brunch foods are thousands of years old |PopSci Staff |March 3, 2021 |Popular-Science 

Listen: when I was young my father had the astrologers of the king of Seville's court cast my horoscope. God Wills It! |William Stearns Davis 

In other words, he had cast her horoscope, a proceeding common enough in those days, and one which had no treasonable complexion. Witch, Warlock, and Magician |William Henry Davenport Adams 

When his queen, Catherine of France, was about to be confined, he himself cast the horoscope of the expected child. The Life of Joan of Arc, Vol. 1 and 2 (of 2) |Anatole France 

He motioned to them, and they, thinking that the horoscope was to be given, galloped once more to the tent. Chambers's Edinburgh Journal, No. 451 |Various 

It was written in thy horoscope that a Red Bull on a green field—I have not forgotten—should bring thee to honour. Kim |Rudyard Kipling