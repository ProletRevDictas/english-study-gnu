Reached outside his home last week, Gelinas declined to comment on the site, and said that QAnon is “a patriotic movement to save the country.” Citigroup puts employee who ran QAnon website on paid leave |kdunn6 |September 17, 2020 |Fortune 

It would let them circumvent the age issue, while hearkening back to a Constitutionally mandated vision of a patriotic age. Our nation turns its lonely eyes to Kenosha |Ellen McGirt |September 1, 2020 |Fortune 

The Republican Party will remain the voice of the patriotic heroes who keep America safe. Trump stokes fears about Biden presidency in convention speech |Chris Johnson |August 28, 2020 |Washington Blade 

San Diego is on its way to repealing a law designed a century ago to make the citizenry more patriotic by policing speech. Morning Report: SDPD Says It Will Stop Seditious Language Tickets |Voice of San Diego |August 17, 2020 |Voice of San Diego 

So don’t shy away from incorporating patriotic imagery and colors in your marketing efforts. Ecommerce marketing this Independence Day will be tricky: Four must dos |Evelyn Johnson |June 23, 2020 |Search Engine Watch 

But these are all things that every patriotic American should celebrate. Why I’m for the War on Christmas |Asawin Suebsaeng |December 23, 2014 |DAILY BEAST 

It is this kind of abstraction that leads to more mythology, more heroic narratives, more undertones of patriotic martyrdom. War Is About More Than Heroes, Martyrs, and Patriots |Nathan Bradley Bethea |November 12, 2014 |DAILY BEAST 

We sat on the grass, in the hot twilight, watching the fireworks burst in patriotic showers of light over Independence. Those Kansas City Blues: A Family History |Katie Baker |October 24, 2014 |DAILY BEAST 

Across China, there are red banners printed with white fonts that say ai guo—love your country, be patriotic. Chinese Tourists Are Taking Hong Kong Protest Selfies |Brendon Hong |October 23, 2014 |DAILY BEAST 

However, only candidates who are screened by the 1,200-piece panel and deemed “patriotic” will appear on the ballot. Is Hong Kong Tiananmen 2.0? |Brendon Hong |September 29, 2014 |DAILY BEAST 

His action was simply the action of a strong, business-like, and patriotic man, forgetful of finesse. King Robert the Bruce |A. F. Murison 

Rather hard on politicians this, to bracket their patriotic endeavours with pitch-and-toss and alcoholic indulgence! Punch, or the London Charivari, Vol. 109, July 27, 1895 |Various 

We have exquisite types of femininity in Tuscany, said the young man, with patriotic ardor. The Fifth String |John Philip Sousa 

Can any of your correspondents inform me where the virtuous and patriotic William Lord Russell was buried? Notes and Queries, Number 196, July 30, 1853 |Various 

The exuberance of the southern temperament responded quickly to the call for a manifestation of patriotic enthusiasm. Frdric Mistral |Charles Alfred Downer