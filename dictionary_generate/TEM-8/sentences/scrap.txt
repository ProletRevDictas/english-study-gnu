To make matters worse, HSBC sparked anger in Hong Kong earlier this year, alienating some of its most loyal investors, after scrapping its dividend in response to the pandemic. 2 new reports push HSBC stock below its 2009 low |Claire Zillman, reporter |September 21, 2020 |Fortune 

It was at that moment that I scrapped the recipe I had written for a standing mixer, and rewrote it to work by hand. Making homemade ramen noodles is surprisingly challenging and totally worth it |By Catherine Tillman Whalen/Saveur |September 11, 2020 |Popular-Science 

From 2011 to 2019, less than one-third of postseason scraps occurred in the first 15 minutes, while 56 percent happened in the final period. Fighting Didn’t Stop In The NHL Bubble |Josh Planos |August 26, 2020 |FiveThirtyEight 

Of the 11 postseason scraps, eight occurred in the opening period. Fighting Didn’t Stop In The NHL Bubble |Josh Planos |August 26, 2020 |FiveThirtyEight 

Some organizations diverted more resources to their performance marketing campaigns, leaving their brand marketing efforts with the scraps. Replay: Live with Search Engine Land season wrap-up—COVID and marketing disruption |George Nguyen |August 24, 2020 |Search Engine Land