A dental hygienist and mom of little kids, Michelle Schaeffer was looking for at-home exercise options during the pandemic — a stress reliever “healthier than drinking.” Peloton makes toning your glutes feel spiritual. But should Jesus be part of the experience? |Michelle Boorstein |February 5, 2021 |Washington Post 

Principal investigators from Canada and Tanzania worked with partners in Africa, North America, and Europe to describe a large assemblage of stone tools, fossil bones, and chemical proxies from dental and plant materials. Scientist findings in Tanzania show how ancient humans used tools 2 million years ago |Julio Mercader Florin |January 17, 2021 |Quartz 

The group even blamed communists for American cities putting fluoride in their water for dental health. Long before QAnon, Ronald Reagan and the GOP purged John Birch extremists from the party |Erick Trickey |January 15, 2021 |Washington Post 

Companies provide an array of services to employer sponsored health plans, including traditional health insurance, vision and dental products, pharmacy benefits, third-party administration, claims review and more. Lavish Bonus? Luxury Trip? Health Benefits Brokers Will Have to Disclose What They Receive From the Insurance Industry |by Marshall Allen |January 6, 2021 |ProPublica 

Watts is a dental assistant, and her hours at work were slashed. The Pandemic Hasn’t Stopped This School District From Suing Parents Over Unpaid Textbook Fees |by Ellis Simani, ProPublica, and Kim Kilbride, South Bend Tribune |December 12, 2020 |ProPublica 

Have you looked around the American Dental Association website for an explanation of how fluoridation actually works? Anti-Fluoriders Are The OG Anti-Vaxxers |Michael Schulson |July 27, 2016 |DAILY BEAST 

Placed in drinking water, fluoride can serve people who otherwise have poor access to dental care. Anti-Fluoriders Are The OG Anti-Vaxxers |Michael Schulson |July 27, 2016 |DAILY BEAST 

To put it rather uncharitably, the USPHS practiced a major dental experiment on a city full of unconsenting subjects. Anti-Fluoriders Are The OG Anti-Vaxxers |Michael Schulson |July 27, 2016 |DAILY BEAST 

Added to drinking water at concentrations of around one part per million, fluoride ions stick to dental plaque. Anti-Fluoriders Are The OG Anti-Vaxxers |Michael Schulson |July 27, 2016 |DAILY BEAST 

Dental leaders barnstormed the state, and cities began to fluoridate. Anti-Fluoriders Are The OG Anti-Vaxxers |Michael Schulson |July 27, 2016 |DAILY BEAST 

In the milk teeth of man we have another useless and often annoying survival of an ancient state of the dental organs. Man And His Ancestor |Charles Morris 

We have pictures of two score of dental instruments that were used by them. Old-Time Makers of Medicine |James J. Walsh 

Vinegar diluted to about half strength with water makes an excellent dental wash. A Civic Biology |George William Hunter 

Immense quantities of the gas are used in dental operations. Cooley's Cyclopdia of Practical Receipts and Collateral Information in the Arts, Manufactures, Professions, and Trades..., Sixth Edition, Volume I |Arnold Cooley 

Medical and dental inspection of school children will also be of value, and the town doctors will aid in it. The Complete Club Book for Women |Caroline French Benton