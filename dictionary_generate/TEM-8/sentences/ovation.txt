After Carey was shot, the House of Representatives gave the Capitol Police a standing ovation. Miriam Carey was shot at 26 times by law enforcement near the Capitol in 2013. Her sister contrasts her fate to the treatment of the Jan. 6 rioters. |David Montgomery |January 19, 2021 |Washington Post 

Democrats gave Pelosi a standing ovation as the final tally was announced, while the Republican side of the chamber was nearly empty. Nancy Pelosi is narrowly reelected House speaker |Jaclyn Gallucci |January 4, 2021 |Fortune 

Like Nixon, when he declared Bush the winner – asking God to bless his opponent – Gore drew a standing ovation. ‘Grace and humor’: The vice presidents who certified their own election losses |Gillian Brockell |January 2, 2021 |Washington Post 

She not only earned a standing ovation and passing grade, but it laid the groundwork for her academic and musical career. Meet NASA’s Dopest Engineer |Joshua Eferighe |December 2, 2020 |Ozy 

He is happy and at peace with retiring at 28 — even if there was no ovation, no coverage, no ceremony to offer tear-filled gratitude to his teammates, family or fans. Spencer Kieboom built his life around baseball. Then he quietly walked away. |Jesse Dougherty |November 30, 2020 |Washington Post 

The two were greeted with a standing ovation from the audience in the sold-out Empire Theatre. Live from San Antonio: Women in the World Texas! |Women in the World |October 23, 2014 |DAILY BEAST 

When you have a number like “Maybe This Time” that is so powerful you need to have a standing ovation afterwards. Nigel Lythgoe on How to Save Reality TV, ‘On the Town,’ and ‘Brokeback Ballroom’ |Kevin Fallon |October 22, 2014 |DAILY BEAST 

But if someone falls on the floor or voids themselves that is a standing ovation. New York’s Scariest Night Out: The Ghosts, Rats, and Lunatics of ‘Nightmare New York’ |Justin Jones |October 4, 2014 |DAILY BEAST 

Before he could sip, his name was announced and he had to run onto the stage to receive a standing ovation. Why We're Obsessed With George Takei |Jennifer M. Kroot |August 20, 2014 |DAILY BEAST 

Its placing at the apex of British life is itself a little nuts, as the Ovation series shows. The Cult of Royal Porn |Tim Teeman |April 26, 2014 |DAILY BEAST 

The party were greeted with one continued ovation during the journey. Portrait and Biography of Parson Brownlow, The Tennessee Patriot |William Gannaway Brownlow 

When the espada finally performs his courageous feat under such conditions, he obtains such an ovation as his skill deserves. Spanish Life in Town and Country |L. Higgin and Eugne E. Street 

A moment afterward the Kiowa finished his boastful tale, and received a noisy ovation from his people. Three Sioux Scouts |Elmer Russell Gregor 

He insists that this is the only fitting thing to do, that to live after such a reception and ovation would be an anti-climax. My Wonderful Visit |Charlie Chaplin 

Mr. Lincoln met with a splendid ovation from the troops and the colored people as he rode about the city. The Fourth Massachusetts Cavalry in the Closing Scenes of the War for the Maintenance of the Union, from Richmond to Appomatox |William B. Arnold