Though he probably cannot name the political theory he has embraced, his own recklessness, vanity and authoritarian instincts have led him down fascist grooves. Trumpism is American fascism |Michael Gerson |February 1, 2021 |Washington Post 

The fact that calling out a fascist and being a fascist are put on the same platform or the same plane is incredibly upsetting. Seth Rogen says calling his criticism of Ted Cruz a ‘Twitter spat’ distracts from the real political issue |Sonia Rao |January 27, 2021 |Washington Post 

The Mods were, album after album, year after year, grumbling into the void about fascists and hypocrites both. Sleaford Mods gets introspective, but don’t think they’ve gone soft |Zachary Lipez |January 21, 2021 |Washington Post 

Created during comics’ Golden Age, Cap was one of the earliest Marvel heroes — and one of the most explicitly anti-fascist. Can Captain America serve two dramatically different versions of America? |Aja Romano |January 12, 2021 |Vox 

Twitter is simply cracking down on conservatives, abusing its platform, breaching contracts, tortious interfering with businesses, and engaging in fascist suppression of truth and speech. Twitter purged more than 70,000 accounts affiliated with QAnon following Capitol riot |Tony Romm, Elizabeth Dwoskin |January 12, 2021 |Washington Post 

“Scratch a liberal, find a fascist every time,” Woods tweeted in April. How James Woods Became Obama’s Biggest Twitter Troll |Asawin Suebsaeng |December 31, 2014 |DAILY BEAST 

At the time, Valli was in Europe, married to a fascist type who was minor-order royalty. Alfred Hitchcock’s Fade to Black: The Great Director’s Final Days |David Freeman |December 13, 2014 |DAILY BEAST 

In fact the Berlin Wall was called the Anti-Fascist Rampart, as I recall. How The Cold War Endgame Played Out In The Rubble Of The Berlin Wall |William O’Connor |November 9, 2014 |DAILY BEAST 

Earlier this week Putin referred to the UIA as a "pro-fascist organization" and condemned Ukraine for glorifying it. Why Are Swastikas Hot In West Ukraine? |Anna Nemtsova |October 17, 2014 |DAILY BEAST 

We had gone from late-Weimar period to the 1933-torchlight-parade stage of fascist takeover. From ISIS to Ebola, What Has Made Naomi Wolf So Paranoid? |Michael Moynihan |October 11, 2014 |DAILY BEAST 

It had happened in Burgos, in April of 1938, during a review of the 12th Division of the fascist army. The Five Arrows |Allan Chase 

A man who marched with the men who put that fascist bullet through the throat of Uncle Carlos. The Five Arrows |Allan Chase 

We all know what this hypocritical neutrality really is; how it shields the vile aid that fascist Spain is lending to the Axis. The Five Arrows |Allan Chase 

Franco is a fascist, and today fascism must triumph all over the world or be crushed forever. The Five Arrows |Allan Chase 

The man with the papers lifts a heavy fist and he lets fly with a blow that knocks out the fascist's front teeth. The Five Arrows |Allan Chase