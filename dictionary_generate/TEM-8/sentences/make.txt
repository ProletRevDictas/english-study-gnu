Genetics alone does not an eating disorder make, generally speaking, and Bulik points out that environment still plays a role. How Skinny Is Too Skinny? Israel Bans ‘Underweight’ Models |Carrie Arnold |January 8, 2015 |DAILY BEAST 

And yes, our values include tolerance of those who wish to make fun of religion. Ayaan Hirsi Ali: Our Duty Is to Keep Charlie Hebdo Alive |Ayaan Hirsi Ali |January 8, 2015 |DAILY BEAST 

The court ruled she lacked the maturity to make her own medical decisions. Should Teens Have The Right To Die? |Brandy Zadrozny |January 8, 2015 |DAILY BEAST 

And it must make sure that the platform of debate where we can freely exchange ideas is safe and sound. Ayaan Hirsi Ali: Our Duty Is to Keep Charlie Hebdo Alive |Ayaan Hirsi Ali |January 8, 2015 |DAILY BEAST 

Through his company, consumers will be able to cheaply make custom DNA strands, including what Heinz calls “creatures.” Design Your Own Dinosaur: The Era of Custom DNA |Justin Jones |January 8, 2015 |DAILY BEAST 

And to tell the truth, she couldn't help wishing he could see, so he could make the game livelier. The Tale of Grandfather Mole |Arthur Scott Bailey 

She did not need a great cook-book; She knew how much and what it took To make things good and sweet and light. Boys and Girls Bookshelf; a Practical Plan of Character Building, Volume I (of 17) |Various 

Now this setting up of an orderly law-abiding self seems to me to imply that there are impulses which make for order. Children's Ways |James Sully 

Give a sweet savour, and a memorial of fine flour, and make a fat offering, and then give place to the physician. The Bible, Douay-Rheims Version |Various 

Those in whom the impulse is strong and dominant are perhaps those who in later years make the good society actors. Children's Ways |James Sully