An emotionally wrenching marathon of hospital shifts followed. The new Covid-19 case surge in Europe, explained |Julia Belluz |September 17, 2020 |Vox 

These shifts stand to benefit Democrats more than they benefit Republicans. More And More Americans Aren’t Religious. Why Are Democrats Ignoring These Voters? |Daniel Cox |September 17, 2020 |FiveThirtyEight 

Volkswagen AG is making its green-bond debut, about two weeks after Daimler AG, as automakers tap socially responsible investors to help pay for the hugely expensive shift to electric vehicles. Volkswagen is the latest carmaker to tap the red-hot green-bond market to fund its EV ambitions |Bernhard Warner |September 16, 2020 |Fortune 

The five-month quarantine has caused a digital shift across our way of life, leaving people to engage with the Internet, social media and technology even more—and shop. Here’s Why Everyone Can’t Get Enough Telfar |Hope Wright |September 11, 2020 |Essence.com 

Climate models generally show shifts in these broad patterns, though it’s not exactly clear how much the changes might translate to hurricanes themselves. Slow, meandering hurricanes are often more dangerous—and they’re getting more common |Greta Moran |September 9, 2020 |Popular-Science 

Do you think that as we get older our thoughts shift to the more abstract, the music, than the definite, the lyrics? Belle & Sebastian Aren’t So Shy Anymore |James Joiner |January 7, 2015 |DAILY BEAST 

As Sutton shows in his book, the important shift took place gradually, from the end of the Civil War until World War II. The Evangelical Apocalypse Is All Your Fault |Jay Michaelson |January 4, 2015 |DAILY BEAST 

Most other social justice movements are seeking some shift of power and money. The Real Story Behind the Fight for Marriage Equality |E.J. Graff |December 30, 2014 |DAILY BEAST 

Another beautiful Eminor number, with a nice shift up to the major for the chorus. Yes, I Like Christmas Music. Stop Laughing. |Michael Tomasky |December 24, 2014 |DAILY BEAST 

And Asians also showed a shift toward the GOP in the mid-terms. Time to Bring Back the Truman Democrats |Joel Kotkin |December 21, 2014 |DAILY BEAST 

The question was put rather testily and caused the other to shift uncomfortably before making answer. The Homesteader |Oscar Micheaux 

The night shift had clocked in over an hour ago, and there should be no passing through the gate for at least six hours. Hooded Detective, Volume III No. 2, January, 1942 |Various 

So that we were compelled to remain all the next day at the anchorage to shift them. Narrative of a Survey of the Intertropical and Western Coasts of Australia] [Volume 2 of 2] |Phillip Parker King 

Then suddenly he would shift shotgun for rifle and come home with a bearskin in the wagon. Scattergood Baines |Clarence Budington Kelland 

The iris of the human eye dilates and contracts with every shift of illumination, and the Time Observatory had an iris too. The Man from Time |Frank Belknap Long