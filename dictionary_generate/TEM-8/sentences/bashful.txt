It’s fun to get to know these people, their insecurities and their problems and slowly grow closer past their bashful exteriors. ‘Boyfriend Dungeon’ reminded me of dating during the pandemic, swords and all |Shannon Liao |August 31, 2021 |Washington Post 

Organizers tried getting students who might feel bashful online to at least try a game of hangman or charades. Best Buddies program helps high school students maintain connection during pandemic |Dana Hedgpeth |April 23, 2021 |Washington Post 

I remember being very bashful at the time and saying, “Thank you!” ‘She’s All That' 15th Anniversary: Cast and Crew Reminisce About the Making of the ‘90s Classic |Marlow Stern |January 29, 2014 |DAILY BEAST 

Kate meets a young fan, who looks a bit bashful about meeting the Duchess. Will & Kate Attend London Premiere of African Cats | |April 25, 2012 |DAILY BEAST 

Mrs. Nelson, riding in front with the bashful driver, vainly sought to engage him in conversation. The Outdoor Girls in the Saddle |Laura Lee Hope 

It seems ridiculous that a fellow who could hold his head straight up before a storm of cannon shot, should be positively bashful. Overland |John William De Forest 

The French women have none of that bashful modesty which characterises the women of England and America. Travels through the South of France and the Interior of Provinces of Provence and Languedoc in the Years 1807 and 1808 |Lt-Col. Pinkney 

This was so unusual in the bashful Alfred that Miss Caldwell roused and slipped lightly to the ground. Blazed Trail Stories |Stewart Edward White 

But as he seemed stronger and more lively the next day, I concluded he was bashful and only ate when I wasn't looking. Birds and All Nature, Vol. VI, No. 3, October 1899 |Various