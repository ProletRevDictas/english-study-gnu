I even had a homecoming ritual, which was to lie about my return date. Homecomings used to feel special. But that was before we spent all our time at home. |Liz Langley |February 25, 2021 |Washington Post 

I remember driving to Atlanta and I couldn’t even afford a dress for the pageant, so I just wore the same homecoming queen dress that my mom had bought me. Cynthia Bailey on Modeling, Entrepreneurship and the ‘Real Housewife’ Life |Nick Fouriezos |February 4, 2021 |Ozy 

Ousted from politics but still part of the WNBA, Loeffler, at minimum, faces an awkward homecoming. WNBA players helped oust Kelly Loeffler from the Senate. Will she last in the league? |Candace Buckner |January 7, 2021 |Washington Post 

Though the young woman is now too busy with her homecoming date to annoy her older brother, they still make time to signal each other with fries from across the room. The McDonald’s Commercials That Live in Our Minds, Rent Free |MM Carrigan |December 18, 2020 |Eater 

The high school crowned its homecoming king and queen, and sports teams played and scrimmaged before cheering parents, few of whom covered their faces. Two School Districts Had Different Mask Policies. Only One Had a Teacher on a Ventilator. |by Annie Waldman and Heather Vogell |November 23, 2020 |ProPublica 

The Temple University concert represented something of a homecoming for Coltrane. What if Jazz Giant John Coltrane Had Lived? |Ted Gioia |September 14, 2014 |DAILY BEAST 

The two people involved were 20-year-old students who met at an off-campus bar during Homecoming Weekend. Sex, Booze, and Feminism |Cathy Young |February 22, 2014 |DAILY BEAST 

His homecoming is breezily explained at the start of the episode. The Return of New Girl’s Coach…and of Faith in the Still-Great Comedy |Kevin Fallon |November 6, 2013 |DAILY BEAST 

For Lennon, who had not set foot in Britain for nine years, there must have been something of a homecoming feel to the experience. How John Lennon Rediscovered His Music in Bermuda |The Telegraph |November 3, 2013 |DAILY BEAST 

The girl's 21-year-old elder sister told The Daily Beast: "We are going to have a big party tonight to celebrate her homecoming." Blonde Child Reunited With Roma Family After Irish Police Blunder |Tom Sykes |October 23, 2013 |DAILY BEAST 

Such a homecoming is known in all of its keen delight by only the long-absent miner or returning soldier. A Fortune Hunter; Or, The Old Stone Corral |John Dunloe Carteret 

All the sadness surrounding her homecoming could not keep out the sweet feeling of being back that stole through her senses. Fidelity |Susan Glaspell 

It would be hard to exaggerate the stir which the newspapers and the public generally made over the homecoming of Mark Twain. Mark Twain, A Biography, 1835-1910, Complete |Albert Bigelow Paine 

And in Ingvar's house the thralls wrought to prepare a great feast in honour of Jarl Halfden's homecoming. Wulfric the Weapon Thane |Charles W. Whistler 

So we went up to the great hall in silence, sorely cast down; and that was Halfden's homecoming. Wulfric the Weapon Thane |Charles W. Whistler