Port or a fruit-forward cabernet can match a rich, custardy chocolate dessert. The new wine rules: Drink what you like with what you want to eat |Dave McIntyre |February 12, 2021 |Washington Post 

The entrees of fresh seafood and decadent desserts left us, quite simply, marveling. A Maui vacation in three acts |Alex Pulaski |February 12, 2021 |Washington Post 

This soft and sweet dessert is the right ending for this light meal. This Valentine’s Day, we couldn’t help but wonder, which ‘Sex and the City’ character will you most eat like? |Kari Sonde |February 11, 2021 |Washington Post 

Mention this ubiquitous dessert, whose heyday was the ’80s and ’90s but is still going strong, and plenty of people will roll their eyes. If loving a piping hot chocolate lava cake is wrong, I don’t want to be right |Becky Krystal |February 4, 2021 |Washington Post 

A dessert is always welcome, especially this simple, one-layer snacking cake. Our best Super Bowl recipes deliver big flavor for a smaller watch party |Kari Sonde |February 1, 2021 |Washington Post 

The smell of grilled meat mixes with the exotic wafts of cinnamon tea served with a mush of sweet brown dessert. The Photographer Who Gave Up Manhattan for Marrakech |Liza Foreman |January 6, 2015 |DAILY BEAST 

It was popularized as a holiday dessert in 16th-century England and also is known as Christmas pudding or plum pudding. The Most Confusing Christmas Music Lyrics Explained (VIDEO) |Kevin Fallon |December 24, 2014 |DAILY BEAST 

If liquor and dessert are equally essential to you enjoying the holiday, at least choose your libation wisely. 12 Thanksgiving Weight Loss Tips That Actually Work |DailyBurn |November 27, 2014 |DAILY BEAST 

Dessert is a slice of melt-in-your-mouth treacle tart with a dollop of perfectly tart clotted cream. Join The Mile High (Dining) Club |Allison McNearney |September 26, 2014 |DAILY BEAST 

“Oh God, that was so much fun,” Sheehy says, wedging a cookie between two heaping scoops of ice cream—dessert. Gail Sheehy Books Passage to the Past |Lizzie Crocker |September 3, 2014 |DAILY BEAST 

They 'ung 'im in the lamp chains right hover the dinin' table, and then finished the dessert. The Book of Anecdotes and Budget of Fun; |Various 

When I came to serve the dessert Sally was watching me with her eagle eye and her mouth watering. The Campfire Girls of Roselawn |Margaret Penrose 

Between the pastry and the dessert, have salad and cheese placed before each guest. The Ladies' Book of Etiquette, and Manual of Politeness |Florence Hartley 

Coffee follows the dessert, and when this enters, if your guests are gentlemen only, your duty is at an end. The Ladies' Book of Etiquette, and Manual of Politeness |Florence Hartley 

She had submitted to giving up the salmon, but the devil himself should not cheat her out of her dessert. Skipper Worse |Alexander Lange Kielland