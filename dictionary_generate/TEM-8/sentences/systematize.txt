After my colleague Daniel Starza Smith and I began working together, we systematized this information, developed a language for describing it consistently, and started to articulate why we thought our findings were important. Unfolding history |Jane L. Dambrogio |October 27, 2021 |MIT Technology Review 

And, since the late 1800s, its influence has been systematized in American culinary school, as Korsha Wilson reported for Eater. Stop calling food ‘exotic’ |Daniela Galarza |July 8, 2021 |Washington Post 

What Big Hit Entertainment did, however, was systematize these elements in BTS, and market them hard. BTS, the band that changed K-pop, explained |Aja Romano |May 21, 2021 |Vox 

What at first feels artificial to us gradually proves its function as Majella’s effort to systematize the chaos swirling around her. Make room for ‘Big Girl, Small Town’ |Ron Charles |December 2, 2020 |Washington Post 

"We need to systematize what we do for young women," said Carol Lancaster, dean of Georgetown's School of Foreign Service. The New Activism |Tom Watson |November 5, 2009 |DAILY BEAST 

How can we systematize the making of our wardrobes so that sewing shall occupy us only a small part of our time? The Complete Club Book for Women |Caroline French Benton 

I shall thus follow the method of certain historians, and relate the truth rather than systematize it. Mysterious Psychic Forces |Camille Flammarion 

Some persons, endeavor to systematize their pursuits, by apportioning them to particular hours of each day. A Treatise on Domestic Economy |Catherine Esther Beecher 

Science seeks to classify and systematize the objects of the world for the understanding of our brain. The Positive Outcome of Philosophy |Joseph Dietzgen 

The mission of Positivism is, in the language of its founder, "to generalize science and to systematize sociality." The Works of Robert G. Ingersoll, Vol. 11 (of 12) |Robert G. Ingersoll