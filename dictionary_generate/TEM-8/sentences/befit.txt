In recent years senior Taliban members have been feted by government officials for peace talks in Doha and Moscow, driven in chauffeured cars with blackened windows and visited high-end hotels — much that befits a regular diplomat. The U.S. Is Leaving Afghanistan, the Taliban Is Growing in Power, and Education for Girls and Women Is Already at Risk |Amie Ferris-Rotman |July 7, 2021 |Time 

On the other hand — in an ironic twist befitting the pandemic’s impact on Japan — the Tokyo Olympics’ July 23 kick-off may also come too soon. Global green shoots: Where the media and advertising markets are around the world on the road to recovery |Seb Joseph |May 17, 2021 |Digiday 

While Leclerc ranks third in our driver Elo ratings, as befitting one of the quickest and most talented drivers in the field, he ranks last out of 20 drivers in reliability score, and his car ranks seventh out of 10 teams early in the season. In Formula One, Does The Driver Or Car Matter More? |Justin Moore |April 15, 2021 |FiveThirtyEight 

It also befits the fact that Roku would be investing in original programming to reap revenue from advertising alone, which isn’t as lucrative as recurring subscription revenue, especially when combined with ad revenue. Future of TV Briefing: This year’s upfront is set for a streaming showdown |Tim Peterson |March 31, 2021 |Digiday 

This was not the attire befitting a free man, Pace told him. He was locked up at age 15. Almost seven decades later, he’s reentering an unfamiliar world. |Karen Heller |February 19, 2021 |Washington Post 

The love scenes themselves befit all the romantic clichés typical of a story so soap operatic in nature. The 'Reign' Steamy Stairwell Shocker & TV’s 13 Dirtiest Sex Scenes (VIDEO) |Kevin Fallon |October 11, 2012 |DAILY BEAST 

Arrogance and party political calculations with an eye to the next election do not befit the gravity of the moment. Greece on the Brink of Financial Abyss as Syriza Party Weighs Default |John Psaropoulos |May 12, 2012 |DAILY BEAST 

It would ill befit the dignity of a Villiers to be frightened out of her abode by a party of rude soldiers. The Children of the New Forest |Captain Marryat 

I have never craved wealth for its own sake, though I have always known that a costly setting would befit beauty such as mine. A Dreadful Temptation |Mrs. Alex. McVeigh Miller 

The rooms were decorated to befit the day, and great jack-o'-lanterns grinned from mantels or brackets. Marjorie's Busy Days |Carolyn Wells 

And then he would sit in his arm-chair for hours, intending to turn his mind to such solemn thoughts as might befit a dying man. Orley Farm |Anthony Trollope 

They would better befit a debating society than an assembly of statesmen met to consider constitutional questions. A Report of the Debates and Proceedings in the Secret Sessions of the Conference Convention |Lucius Eugene Chittenden