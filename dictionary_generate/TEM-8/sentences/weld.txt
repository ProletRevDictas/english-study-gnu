So, using inference as a way of understanding what a good weld looks like through training, and then through inference very, very quickly identifying that problem. Building the future with software-based 5G networking |MIT Technology Review Insights |December 15, 2021 |MIT Technology Review 

“I can build a custom motorcycle from scratch, can weld, and worked as a lube guy at a GM dealership,” Dustykatt says. Inside the Bizarre World of ‘Bronies,’ Adult Male Fans of ‘My Little Pony’ |Kevin Fallon |May 1, 2014 |DAILY BEAST 

In 1997, President Clinton nominated former Massachusetts governor William Weld, a Republican, as ambassador to Mexico. Senate Democrats Just Took Us a Step Closer to the Imperial Presidency |David Frum |December 1, 2013 |DAILY BEAST 

He happened to be studying groundwater in Weld County when the floods came and decided to change his research goals. Did Floods Cause a Fracking Disaster in Colorado? |Josh Dzieza |September 19, 2013 |DAILY BEAST 

Weld County is one of six in Colorado that will vote on a secession initiative in November. Secession Fever Sweeps Texas, Maryland, Colorado, and California |Caitlin Dickson |September 12, 2013 |DAILY BEAST 

He contacted William Weld, then U.S. Attorney for the District of Massachusetts and later governor of the state. Whitey Bulger’s Defense to Reveal Widespread FBI Complicity |T.J. English |July 30, 2013 |DAILY BEAST 

Weld is a totally distinct word from woad, but most dictionaries confound them. Chaucer's Works, Volume 1 (of 7) -- Romaunt of the Rose; Minor Poems |Geoffrey Chaucer 

I had a big Scotchman in the factory who couldn't learn how to weld. Riders of the Silences |John Frederick 

I felt offended; for my instinct was to weld people together and hold them so welded. Lazarre |Mary Hartwell Catherwood 

And Weld ran up to me, and though I was a good piece of a lad, swung me lightly onto his shoulder. Richard Carvel, Complete |Winston Churchill 

"I am as stout a patriot as you, Weld," I shouted back, and flushed at the cheering that followed. Richard Carvel, Complete |Winston Churchill