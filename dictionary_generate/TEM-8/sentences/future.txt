The destruction caused by a pandemic helps to clear the way for a future resurgence. The math of New York City's recovery |Felix Salmon |September 17, 2020 |Axios 

It seems counterintuitive because education is the best way to protect ourselves from future crises. Malala Yousafzai tells the business community: Education is the best way to guard against future crises |Michal Lev-Ram, writer |September 15, 2020 |Fortune 

The competition is expected to remain neck-and-neck for the foreseeable future. IBM plans a huge leap in superfast quantum computing by 2023 |rhhackettfortune |September 15, 2020 |Fortune 

Last fall, though, as the previous round of fires ravaged California, his phone began to ring, with private-equity investors and bankers all looking for his read on the state’s future. Climate Change Will Force a New American Migration |by Abrahm Lustgarten, photography by Meridith Kohut |September 15, 2020 |ProPublica 

Espina said Moreno would face future persecution if she were to return to her country. Lesbian woman from Cuba granted asylum in U.S. |Yariel Valdés González |September 15, 2020 |Washington Blade 

The program has not made a final selection on which upgrades will actually be included in future versions of the F-35. Pentagon Misfires in Stealth Jet Scandal |Dave Majumdar |January 8, 2015 |DAILY BEAST 

Disordered eating is also linked to higher rates of depression and anxiety, both in the present and in the future. How Skinny Is Too Skinny? Israel Bans ‘Underweight’ Models |Carrie Arnold |January 8, 2015 |DAILY BEAST 

Ass-kicking, bad guy-killing Carter is just a future spinster. Marvel’s ‘Agent Carter’ Stomps on the Patriarchy |Melissa Leon |January 7, 2015 |DAILY BEAST 

Buzzfeed shows us a potentially terrifying glimpse of the future. Use Your Brain—Control a Drone |The Daily Beast Video |January 5, 2015 |DAILY BEAST 

There is, fortunately, not too much telling of the future in Harry Potter. Harry Potter and the Torah of Terror |Candida Moss, Joel Baden |January 4, 2015 |DAILY BEAST 

And as she hesitated between obedience to one and duty toward the other, her life, her love and future was in the balance. The Homesteader |Oscar Micheaux 

The poor must look to the brightness of a future world for the consolation that they were denied in this. The Unsolved Riddle of Social Justice |Stephen Leacock 

In future years the poor-rate (so-called) will include, in addition to these, all other rates levyable by the Corporation. Showell's Dictionary of Birmingham |Thomas T. Harman and Walter Showell 

She embraced Otteline; and gave him her hand to kiss, with repeated expressions of future confidence in the husband of her friend. The Pastor's Fire-side Vol. 3 of 4 |Jane Porter 

In his childlike, impulsive fashion he had not thought of the future when he adopted Jean. The Joyous Adventures of Aristide Pujol |William J. Locke