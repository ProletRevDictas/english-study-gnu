Understanding how the hunger for social contact is produced in the brain might allow a deeper understanding of the role social isolation plays in some illnesses. Why do you feel lonely? Neuroscience is starting to find answers. |Amy Nordrum |September 4, 2020 |MIT Technology Review 

Europe has also seen a new level of social activism among soccer stars, with English national team players Marcus Rashford and Raheem Sterling making tangible gains on issues from childhood hunger to racial equality. NBA players are perfectly positioned to lead a strike |Max Lockie |August 27, 2020 |Quartz 

They help us respond to hunger pangs and then tell us when we’ve eaten enough. Explainer: What is puberty? |Esther Landhuis |August 27, 2020 |Science News For Students 

In addition to facing high rates of homelessness and hunger, food insecurity in the trans community is also a problem. How Black Communities Are Bridging the Food Access Gap |Nadra Nittle |July 8, 2020 |Eater 

If we were able to fix that, so that women were no longer second-class citizens, I think it would impact every other problem that we have — hunger and the environment and war. Does Hollywood Still Have a Princess Problem? (Ep. 394) |Stephen J. Dubner |October 24, 2019 |Freakonomics 

I wish this was the last time I had to worry about hunger and bombs. Has the Kurdish Victory at Sinjar Turned the Tide of ISIS War? |Niqash |December 27, 2014 |DAILY BEAST 

World peace, religious tolerance, and an end to global poverty, hunger, and disease. Santa Fails One More Time |P. J. O’Rourke |December 27, 2014 |DAILY BEAST 

His hunger strike in December 2011 received nation-wide recognition and was one of the sparks that ignited the protest movement. Behind Bars for the Holidays: 11 Political Prisoners We Want to See Free In 2015 |Movements.Org |December 25, 2014 |DAILY BEAST 

While in pre-trial detention, Krivov undertook two hunger strikes. Behind Bars for the Holidays: 11 Political Prisoners We Want to See Free In 2015 |Movements.Org |December 25, 2014 |DAILY BEAST 

Now Jena Malone is 30, and with roles in Inherent Vice, The Hunger Games, and a massive superhero film, all the rage. Jena Malone’s Long, Strange Trip From Homelessness to Hollywood Stardom |Marlow Stern |December 22, 2014 |DAILY BEAST 

It was he who first said, If thine enemy hunger give him food, if he thirst give him drink. Solomon and Solomonic Literature |Moncure Daniel Conway 

Or, if I escaped these dangers for a day or two, what could I expect but a miserable death of cold and hunger? Gulliver's Travels |Jonathan Swift 

Jacob robbed his brother of his birthright by trading on his hunger; Joseph robbed a whole people in the same way. God and my Neighbour |Robert Blatchford 

Hunger had to be satisfied, however, and I had to swallow my pride and my five-pennyworth. Fifty Years of Railway Life in England, Scotland and Ireland |Joseph Tatlow 

To obtain the necessaries of life they endure cold and hunger in an extraordinary manner. The Jesuit Relations and Allied Documents, Vol. II: Acadia, 1612-1614 |Various