According to Grady of Rockefeller Philanthropy Advisors, well-placed donations to smaller organizations can have exponential impacts. Can Billionaires Really Save Us from Climate Disaster? |Heather Hansman |February 12, 2021 |Outside Online 

Before I discard the plastic, I cut it up into tiny pieces so that small animals do not get caught in them and they don't try to eat them. Hints From Heloise: New scam tied to coronavirus pandemic |Heloise Heloise |February 12, 2021 |Washington Post 

Over small plates, Renee talked about her love for food and Willie talked about his love for cooking. Date Lab: One of our setups is still going strong two years later. We caught up with Willie and Renee for an update. |Marin Cogan |February 11, 2021 |Washington Post 

So please think carefully before you demote small-talky situations to a priority below your laundry piles. Carolyn Hax: No time for small talk? That’s building a big wall. |Carolyn Hax |February 11, 2021 |Washington Post 

Castor addressed a small crowd from the stage before Coach Bruce Arians and General Manager Jason Licht offered enthusiastic — and colorful — comments of their own. Tampa Bay Buccaneers celebrate Super Bowl LV victory in boat parade |Cindy Boren, Glynn A. Hill |February 10, 2021 |Washington Post 

Something like fluoride, which is too small for normal filters, yanks away that feeling of agency. Anti-Fluoriders Are The OG Anti-Vaxxers |Michael Schulson |July 27, 2016 |DAILY BEAST 

And yes, someone has already called Spencer a “Small Fry,” har har. Freaking Out About Age Gaps in Gay Relationships Is Homophobic |Samantha Allen |January 9, 2015 |DAILY BEAST 

It was seen by a small delegation of star-struck prelates and dignitaries who later described the film as “moving.” Pope Francis Has the Pleasure of Meeting Angelina Jolie for a Few Seconds |Barbie Latza Nadeau |January 8, 2015 |DAILY BEAST 

The judges who handle arraignments at criminal court in all five boroughs have a small fraction of their usual caseloads. Shot Down During the NYPD Slowdown |Michael Daly |January 7, 2015 |DAILY BEAST 

Inside the guild, men in caps and long gowns sit in twos, weaving together in small rooms. The Photographer Who Gave Up Manhattan for Marrakech |Liza Foreman |January 6, 2015 |DAILY BEAST 

The Duchess had also a tent for their sick men; so that we had a small town of our own here, and every body employed. The Life and Most Surprising Adventures of Robinson Crusoe, of York, Mariner (1801) |Daniel Defoe 

Suddenly, however, he became aware of a small black spot far ahead in the very middle of the unencumbered track. The Joyous Adventures of Aristide Pujol |William J. Locke 

He is perplexed and hindered by the lack of soldiers, but is doing his best with his small forces. The Philippine Islands, 1493-1898, Volume XX, 1621-1624 |Various 

A small book, bound in full purple calf, lay half hidden in a nest of fine tissue paper on the dressing-table. Hilda Lessways |Arnold Bennett 

Before the outlaw can comply with this small request the horn sounds again. Physiology of The Opera |John H. Swaby (AKA "Scrici")