Conch shells fashioned into trumpets were found at the Peruvian temple complex at Chavín de Huántar. The human history found inside a seashell |Katharine Norbury |July 30, 2021 |Washington Post 

Valles, a father of three, had been teaching one of his three grandchildren, 5-year-old Aliq Valles, to play the trumpet. In Texas-Mexico Border Towns, COVID-19 Has Had an Unconscionably High Death Toll |René Kladzyk, Phil Galewitz and Elizabeth Lucas | El Paso Matters and KHN |June 22, 2021 |Time 

I started as a trumpet player, of all instruments, but it wasn’t cool and you could be cool on stage with a guitar and whatever. Tommy Mottola Lives on the Road |Eugene Robinson |June 14, 2021 |Ozy 

Retelling this story isn’t to blow my own trumpet, it’s to make clear that you don’t have to be a marketer by training or commit a huge amount of time and resources to successfully market your startup. Even startups on tight budgets can maximize their marketing impact |Ram Iyer |May 13, 2021 |TechCrunch 

The trumpet, of course, is not one of the customary sounds of springtime in the forest at Wolf Trap, the performing arts national park. At Wolf Trap, this walk in the woods comes with its own soundtrack |Mark Jenkins |April 22, 2021 |Washington Post 

At night jineteras stalk the promenade in search of tourists while a trumpet from a bench serenades the proceedings. The Life and Hard Times Of The Family A Cuban Defector Left Behind |Brin-Jonathan Butler |December 19, 2014 |DAILY BEAST 

A 4th grade Jessie James with a backpack and juice box, who practices trumpet and plays soccer. Woman Finds Mysterious Charges on Her iTunes Bill: A Modern Whodunit! |Nancy Neufeld Callaway |January 31, 2014 |DAILY BEAST 

He had already run through all his personal funds, but luckily the request worked (a trumpet was also traded). ‘Tomorrow Night’ Review: Louis C.K.’s 1998 Indie Movie Is F**ing Weird |Nina Strochlic |January 29, 2014 |DAILY BEAST 

But in more recent wars, where the emphasis has been on “credibility,” not victory, the trumpet has been uncertain, at best. Is Obama Going to War Just to ‘Check the Box’? |Lloyd Green |August 31, 2013 |DAILY BEAST 

My wife and I named our son Gideon after Tony had sounded Gideon's Trumpet. The Essential Anthony Lewis |Anthony Lester, Josh Dzieza |March 25, 2013 |DAILY BEAST 

Roulard had played the trumpet in the regimental band in which Aristide had played the kettle drum. The Joyous Adventures of Aristide Pujol |William J. Locke 

How long shall I see men fleeing away, how long shall I hear the sound of the trumpet? The Bible, Douay-Rheims Version |Various 

And I appointed watchmen over you, saying: Hearken ye to the sound of the trumpet. The Bible, Douay-Rheims Version |Various 

Blow the trumpet, let all be made ready, yet there is none to go to the battle: for my wrath shall be upon all the people thereof. The Bible, Douay-Rheims Version |Various 

Not till Felix comes to her in the chamber above the dining hall—there where that trumpet vine hangs—comes to say good-by to her. The Awakening and Selected Short Stories |Kate Chopin