The lone concern I had then, and remains my concern now, is that instability in this relationship could arise only from mutual misunderstanding as to our respective countries’ intentions and our domestic needs. Trump’s most popular YouTube ad is a stew of manipulated video |Glenn Kessler, Meg Kelly |September 17, 2020 |Washington Post 

“There is lots of misunderstanding about when to use what test,” he said. Spit vs. Swab? Scientists say new studies support use of ‘saliva tests’ for COVID |Lee Clifford |September 5, 2020 |Fortune 

A misunderstanding of a previous Oxford study in April 2020 led many to believe that 60% adoption of digital contact tracing apps is required before they can have any effect. Coronavirus tracing apps can save lives even with low adoption rates |Patrick O'Neill |September 2, 2020 |MIT Technology Review 

The common misunderstanding about first-party data, according to Meron, is that this data comes from identifying elements such as email addresses or usernames from logged in users. ‘Contextual on steroids’: How Insider is tracking and scaling audience behavior using first-party data |Kayleigh Barber |July 29, 2020 |Digiday 

Of course, now we’re seeing how much of a misunderstanding that is. Why It Took So Long For Politicians To Treat The Child Care Crisis As A Crisis |Clare Malone (clare.malone@fivethirtyeight.com) |July 16, 2020 |FiveThirtyEight 

What conflicts do exist between them derive from misunderstanding and accident. The 2014 Novel of the Year |Nathaniel Rich |December 29, 2014 |DAILY BEAST 

Somehow, the brevity of the message creates an inverse potential for misunderstanding. What Would Jane Eyre Sext? |Jennie Yabroff |December 23, 2014 |DAILY BEAST 

The authors categorized responses that indicated a misunderstanding of possible benefit as “germs are germs” beliefs. Without Education, Antibiotic Resistance Will Be Our Greatest Health Crisis |Russell Saunders |December 19, 2014 |DAILY BEAST 

When the story surfaced in the press, Shumlin backtracked and said it all been a misunderstanding. What the Hell Happened in Vermont?! |Stuart Stevens |November 13, 2014 |DAILY BEAST 

Do say ‘Olga, there has been a misunderstanding, for which I wanted to apologise.’ How To Fire Your Maid, By A Russian Oligarch |Tom Sykes |October 1, 2014 |DAILY BEAST 

Mr. Meadow Mouse repeated, as if he wanted to be sure there was no misunderstanding about it. The Tale of Grandfather Mole |Arthur Scott Bailey 

There had been cruel misunderstanding on his part somewhere; that misunderstanding must be burned away. The Wave |Algernon Blackwood 

But this port (to obviate misunderstanding) is not on the Ocean lying eastward, but on that gulf which I have called French bay. The Jesuit Relations and Allied Documents, Vol. II: Acadia, 1612-1614 |Various 

There was some misunderstanding somewhere, he realized, and sheer astonishment had cooled his anger. St. Martin's Summer |Rafael Sabatini 

He was then physician to Mme. de Listomere, whose misunderstanding with Rastignac he learned and afterwards related. Repertory Of The Comedie Humaine, Complete, A -- Z |Anatole Cerfberr and Jules Franois Christophe