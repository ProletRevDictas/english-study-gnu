At the diner, Tom and Greg order omelets and contemplate their futures. In ‘Succession,’ Food Exists Only to Create More Misery |Amy McCarthy |November 22, 2021 |Eater 

They feed on intrigue and the scraps of Logan’s meticulously doled-out attention, not pastries and omelets. In ‘Succession,’ Food Exists Only to Create More Misery |Amy McCarthy |November 22, 2021 |Eater 

Schaefer says the next two to three years of cementing this new regulatory framework are going to be “rough and tumble, breaking some eggs to make this omelet.” Here's What the Crackdown on China's Big Tech Firms Is Really About |Charlie Campbell / Shanghai |July 13, 2021 |Time 

Blonsky preps food for her short trips, chopping veggies and filling a Tupperware with spinach — which doubles as a cushion for her omelet eggs. How to take an overnight trip with your two-wheeled vehicle |Melanie D.G. Kaplan |March 26, 2021 |Washington Post 

While all that is true, it’s also true that you have to break eggs in order to make an omelet. Google’s Lighthouse is now recommending JavaScript library alternates |Detlef Johnson |January 5, 2021 |Search Engine Land 

Failing to make an omelet hardly proves that omelets are illusory if nobody has been willing to break some eggs along the way. Partition Skepticism and the Future of the Peace Process |Avner Inbar, Assaf Sharon |September 25, 2013 |DAILY BEAST 

He Said: I completely disagree, and I see both the ice cream cone scene and the omelet scene entirely differently. ‘The Good Wife’: Has Season 4’s Kalinda Storyline Gone too Far? |Jace Lacob, Maria Elena Fernandez |October 15, 2012 |DAILY BEAST 

That it plays out in such a domestic setting, in a kitchen and he demands that she make him an omelet, is telling as well. ‘The Good Wife’: Has Season 4’s Kalinda Storyline Gone too Far? |Jace Lacob, Maria Elena Fernandez |October 15, 2012 |DAILY BEAST 

Sen. Sherrod Brown knows which side his omelet is hot-sauced on. Planned Parenthood’s Cecile Richards Takes Center Stage in Charlotte |Allison Yarrow |September 5, 2012 |DAILY BEAST 

He was eating an omelet, and had moved the bread off his plate. Mike Daisey: The Man Who Outed Apple’s Abysmal Labor Practices in China |Jacob Bernstein |March 16, 2012 |DAILY BEAST 

And she recommended great care in dripping the coffee and having the omelet done to a proper turn. The Awakening and Selected Short Stories |Kate Chopin 

Sara Lee, enveloped in a large pinafore apron, made the omelet in the kitchen. The Amazing Interlude |Mary Roberts Rinehart 

And Sara Lee beat up the eggs and found, after a bad moment, some salt in a box, and then poured her omelet into the pan. The Amazing Interlude |Mary Roberts Rinehart 

The omelet which Rollo had chosen for his principal dish was excellent too. Rollo on the Rhine |Jacob Abbott 

They need tossing up with as light a hand as an omelet, you see. The Atlantic Monthly, Volume 16, No. 97, November, 1865 |Various