Millions brace for more layoffs, hunger and utility shutoffs as stimulus talks break downOn Thursday, Jenkins drove her old Dodge Caravan to a place that hires day laborers, hoping she could get on a cleaning or roofing crew. The trend that helps explain why the economy is still in trouble — and the recovery is uneven |Heather Long, Andrew Van Dam |October 29, 2020 |Washington Post 

Software is increasingly able to replicate or exceed the performance of human laborers, just as companies are looking to cut costs amid a global economic downturn, which puts existing jobs at risk, Lee said. COVID gave China an edge in A.I. battle against the U.S. |Grady McGregor |October 27, 2020 |Fortune 

He lived in a small village in Haryana and worked as a driver and laborer in the fields. Three Women: Stories Of Indian Trafficked Brides |LGBTQ-Editor |October 5, 2020 |No Straight News 

Employers now tend to only hire laborers who have their own cars, one laborer told VOSD, out of fears of contracting the virus. Morning Report: Big Questions About Seditious Language Law Remain |Voice of San Diego |September 22, 2020 |Voice of San Diego 

Last month, for example, the government banned dining in restaurants as part of its effort to suppress the third wave, effectively forcing manual laborers to eat on the street. Hong Kong’s citywide COVID-19 testing has become a barometer of public trust |eamonbarrett |September 9, 2020 |Fortune 

Shadman started as a laborer, but within a year became an interpreter because he could speak English. Special Forces’ $77M ‘Hustler’ Hits Back |Kevin Maurer |December 8, 2014 |DAILY BEAST 

Soon the missing construction laborer had become a national cause célèbre. Rio’s Security Crisis |Mac Margolis |August 7, 2013 |DAILY BEAST 

On his “Certificate of Employment during the War,” Tudor is listed as a farm laborer. Geoff Dyer's 'The Missing of the Somme' Reconsidered |Louisa Thomas |November 11, 2011 |DAILY BEAST 

Anschutz, a non-drinker, was a 57-year-old laborer at a local moving company. Exclusive Excerpt: MLK's Haunting Final Hours |Hampton Sides |April 24, 2010 |DAILY BEAST 

Sondra Wiener, forced to make pocket money like an out-of-work laborer, endures the pity of her neighbors. Ruth Madoff's Private World |Lucinda Franks |April 6, 2009 |DAILY BEAST 

His parents were peasants and he wrought as a day laborer till he attracted attention. The Every Day Book of History and Chronology |Joel Munsell 

The life of a laborer that is content with what he hath, shall be sweet, and in it thou shalt find a treasure. The Bible, Douay-Rheims Version |Various 

The Washington manual laborer school and the Howard institution can bear testimony to his industry and patriotism. The Every Day Book of History and Chronology |Joel Munsell 

Thus it lightens the toil of the weary laborer plodding along the highway of life. Tobacco; Its History, Varieties, Culture, Manufacture and Commerce |E. R. Billings. 

She went through the usual routine of housework like a laborer who drags after him a ball and chain. The Atlantic Monthly, Volume 17, No. 101, March, 1866 |Various