Freeman’s industry contacts made it clear that 90 days in jail wouldn’t be much of a deterrent. Bee theft is almost a perfect crime—but there’s a new sheriff in town |Andrew Zaleski |February 9, 2021 |Popular-Science 

The pair also are requesting that Congress give the FTC power to issue large civil penalties to first-time offending businesses as a further deterrent to engaging in schemes like Amazon’s, which FTC commissioners called “outrageous.” Amazon will pay nearly $62 million over “outrageous” practice of pocketing driver tips |Jason Del Rey |February 2, 2021 |Vox 

While collecting wastage data is a good business practice, O’Leary said it is most useful as a deterrent against vaccine providers mishandling or discarding doses irresponsibly. How Many Vaccine Shots Go to Waste? Several States Aren’t Counting. |by Ryan Gabrielson, Caroline Chen and Mollie Simon |January 21, 2021 |ProPublica 

The stock market’s record altitude was no deterrent to individual investors last month, who in true 2020 fashion ended the year snapping up shares. Stocks’ worst start in 20 years challenged traders on Monday |McKenna Moore |January 4, 2021 |Fortune 

Criminologists find that other consequences of problematic drug use – such as harm to health, reduced quality of life and strained personal relationships – are more effective deterrents than criminal sanctions. Oregon Just Decriminalized All Drugs – Here’s Why Voters Passed This Groundbreaking Reform |LGBTQ-Editor |December 10, 2020 |No Straight News 

This is supposed to act as a deterrent, but may be an incitement. In Jerusalem Home Demolitions, the Biblical Justice of Revenge |Creede Newton |November 25, 2014 |DAILY BEAST 

The Iranians thought they needed a credible nuclear deterrent, even if it was virtual, to assure the survival of their regime. Iraq Is Not Our War Anymore. Let It Be Iran’s Problem. |Christopher Dickey |July 17, 2014 |DAILY BEAST 

Prosecutors described the verdict and the sentencing as a “deterrent.” Egyptian Court Hands Down Stiff Sentences for Al-Jazeera Journalists |Jesse Rosenfeld |June 23, 2014 |DAILY BEAST 

Still, there may be a deterrent for women that is far less spoken about, at least in polite circles. C’mon, Ladies, Masturbation Isn’t Just for Bad Girls |Emily Shire |June 19, 2014 |DAILY BEAST 

Concealed carry is good for responding to a crime in progress; open carry is a deterrent to it. A Gun Owner Speaks: My Case for Open Carry |CJ Grisham |June 12, 2014 |DAILY BEAST 

As an adjunct of the policy of the deterrent workhouse for the able-bodied, we have to note the coming-in of compulsory detection. English Poor Law Policy |Sidney Webb 

It is clear that a sentence of a year or two, or even more each time that a crime is committed, does not act as a deterrent. Essays In Pastoral Medicine |Austin Malley 

To this it may be answered that punishment for crime is not intended to be retaliatory, but admonitory and deterrent. The Collected Works of Ambrose Bierce |Ambrose Bierce 

It was the infinite pity of His heart that led Him to use a word which might prove the very strongest deterrent. Expositor's Bible: The Gospel of Matthew |John Monro Gibson 

Quay accommodation is no object to such visitors; intricate navigation no deterrent. The Riddle of the Sands |Erskine Childers