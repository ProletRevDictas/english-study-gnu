Today we’re parsing its Q4 2020 data, which shows a return to sequential-quarterly growth at the trading upstart. Robinhood’s Q4 2020 revenue shows a return to growth |Alex Wilhelm |February 1, 2021 |TechCrunch 

Notably, her show was defeated in the 25-to-54 age demographic — but not in total viewers — by upstart network Newsmax on one night in December, though the feat was not repeated. Fox News overhauls daily schedule, moving news anchor Martha MacCallum to make way for opinion expansion |Jeremy Barr |January 11, 2021 |Washington Post 

Meanwhile, upstart sexual wellness and feminine brands have also been turning to Walmart as a new sales channel. How Walmart became an integral part of the DTC playbook |jim cooper |December 29, 2020 |Digiday 

An upstart company, Iowa Beef Packers, introduces a product known as “boxed beef,” transforming the meatpacking industry. How the History of Waterloo, Iowa, Explains How Meatpacking Plants Became Hotbeds of COVID-19 |by Bernice Yeung and Michael Grabell |December 21, 2020 |ProPublica 

This trading surge, along with the popularity of newer app-based platforms like Robinhood, has created a potential opportunity for upstarts. Public, a would-be Robinhood rival, raises $65 million |Jeff |December 15, 2020 |Fortune 

AirAsia, on the other hand, is a relatively new carrier, an upstart in the tradition of Southwest Airlines in the United States. The Presumed Crash of AirAsia Flight QZ8501 Is Nothing Like MH370 |Lennox Samuels |December 29, 2014 |DAILY BEAST 

And if Warren seems like an unlikely upstart now, so did Obama at this time eight years ago. Obama’s 2008 Backers: We’re Ready for Warren |David Freedlander |October 9, 2014 |DAILY BEAST 

If the upstart wins, the Democrats are in play for the first time in a generation. Travis Childers: The Deep South’s Great Liberal Hope |Ben Jacobs |June 5, 2014 |DAILY BEAST 

The crashing and burning of her last presidential campaign to the unknown, young upstart. Inside the World of Hillary Superfans |David Freedlander |April 23, 2014 |DAILY BEAST 

He was the electric young upstart who upended her inevitable path to the White House in Iowa. Ready for Hillary Super PAC Throws In for 2014 Midterms |David Freedlander |March 26, 2014 |DAILY BEAST 

And the man who had done all this—a vulgar upstart out of Paris, reeking of leather and the barrack-room still lived! St. Martin's Summer |Rafael Sabatini 

"I guess that is straight enough for Guitar to believe, instead of that upstart lieutenant," said Harry. The Courier of the Ozarks |Byron A. Dunn 

The hills of Mount Upstart are of primitive form, and were judged to be composed of granite. Narrative of a Survey of the Intertropical and Western Coasts of Australia] [Volume 2 of 2] |Phillip Parker King 

Bill had been watching, but he had not seen the young upstart from the agency go past, and neither had Bill's faithful clerk. Mystery Ranch |Arthur Chapman 

Others were derided by their contemporaries, as we deride the made-to-order coat of arms of some nineteenth century upstart. The Private Life of the Romans |Harold Whetstone Johnston