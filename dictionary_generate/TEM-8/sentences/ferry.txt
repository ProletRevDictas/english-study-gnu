A free, privately owned ferry service at the Port of Cave-In-Rock, continuously operated since the early 1800s, carries roughly 500 vehicles across the river every day. A day’s drive from Chicago, exploring a very different Illinois |Carson Vaughan |February 12, 2021 |Washington Post 

Christened the “Loni Jo,” the ferry chugged back from the Kentucky shore and dropped its gate at the foot of Illinois Route 1. A day’s drive from Chicago, exploring a very different Illinois |Carson Vaughan |February 12, 2021 |Washington Post 

We have seen vaccine go out on helicopters, plans, cars dog sleds, and ferry. How is Alaska leading the nation in vaccinating residents? With boats, ferries, planes and snowmobiles. |Cathy Free |February 4, 2021 |Washington Post 

The adenovirus ferries instructions for making the coronavirus’s spike protein into human cells. One-shot COVID-19 vaccine is effective against severe disease |Tina Hesman Saey |January 29, 2021 |Science News 

Irish customs authorities on Thursday night announced a temporary easing of customs requirements for goods arriving on ferries from Britain, to get freight moving again. Logistics giant DPD pauses U.K. to EU parcel shipments, amid post-Brexit customs chaos |David Meyer |January 8, 2021 |Fortune 

And earlier that day, the 43-year-old had earned the précis, breaking up a skirmish by the Staten Island Ferry. Before Eric Garner, There Was Michael Stewart: The Tragic Story of the Real-Life Radio Raheem |Marlow Stern |December 4, 2014 |DAILY BEAST 

There was no trouble on the ferry as it reached Manhattan and a few of the passengers boarded the subway to the protest uptown. ‘I Can’t Breathe!’ ‘I Can’t Breathe!’ A Moral Indictment of Cop Culture |Michael Daly |December 4, 2014 |DAILY BEAST 

As a precaution against a possible disturbance, the ferry was escorted by a police boat, its blue lights flashing. ‘I Can’t Breathe!’ ‘I Can’t Breathe!’ A Moral Indictment of Cop Culture |Michael Daly |December 4, 2014 |DAILY BEAST 

Marine One will ferry him to JFK Airport, where Air Force One awaits. Behind the Scenes With a ‘Site Agent’: The Secret Service’s Hardest Job |Marc Ambinder |October 2, 2014 |DAILY BEAST 

For a more scenic journey, travel by ferry from Whittier or Valdez. America’s Best Summer Food Festivals |Lonely Planet |July 5, 2014 |DAILY BEAST 

Crossing the river on a ferry, Lawrence and his escort mounted their horses and started for Platte City, but a few miles away. The Courier of the Ozarks |Byron A. Dunn 

Lee's army is sweeping victoriously through Maryland; Harper's Ferry taken with ten thousand prisoners. The Courier of the Ozarks |Byron A. Dunn 

It finally plunged sharply down to a steamboat ferry, over which we crossed the Dart and landed directly in the town. British Highways And Byways From A Motor Car |Thomas D. Murphy 

Leaving the city, we crossed Southampton Water on a steam ferry which was guided by a chain stretched from bank to bank. British Highways And Byways From A Motor Car |Thomas D. Murphy 

The original bridge across the Medway to Strood probably dates from the Roman period, taking the place of a ferry. The Towns of Roman Britain |James Oliver Bevan