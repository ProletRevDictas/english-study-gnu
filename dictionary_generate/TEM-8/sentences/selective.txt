Along with his power surge, and perhaps contributing to it, Tatís has also become more selective about the pitches he swings at. Fernando Tatís Jr. Was Already Mashing. Then He Started Hitting The Ball Harder. |Travis Sawchik |August 21, 2020 |FiveThirtyEight 

Van Eenennaam credits this boost in productivity to conventional selective breeding. Biotechnology Could Change the Cattle Industry. Will It Succeed? |Dyllan Furness |August 16, 2020 |Singularity Hub 

Interestingly, “fusion’s effect on selective censoring occurred regardless of whether the incongruent comments used offensive language.” The Anonymous Culture Cops of the Internet - Facts So Romantic |Jesse Singal |August 12, 2020 |Nautilus 

Since then, SDPD has said it would be selective about when it tapped into the cameras. Police Used Smart Streetlight Footage to Investigate Protesters |Jesse Marx |June 29, 2020 |Voice of San Diego 

That’s due largely to selective breeding and other technologies. The Future of Meat (Ep. 367 Rebroadcast) |Stephen J. Dubner |August 29, 2019 |Freakonomics 

Liberals are outraged over the Steven Scalise scandal—but the left has selective amnesia. Reverend Jeremiah Wright Was Worse Than Scalise |Ron Christie |January 2, 2015 |DAILY BEAST 

Based on our conversation, he decided to do more research and apply to at least one small selective college. Forget the Kids Who Can’t Get In; What About Those Who Don’t Even Apply? |Jonah Edelman |December 9, 2014 |DAILY BEAST 

Justice should not be selective to fit a political narrative when the facts and evidence prove otherwise. It’s Time to Hold Protesters Accountable |Ron Christie |December 4, 2014 |DAILY BEAST 

But a drug like lamotrigine is not selective, and so it also affects the behavior of the rest of the temporal lobe. The Seizure Medication That Turns You Into a Poet |Cat Ferguson |September 12, 2014 |DAILY BEAST 

Whether or not guayusa is a product of selective breeding, the Kichwa have learned to harness its power. Bye Bye Latté, Hello Guayusa: Why The Amazon Holds the Secret to a Cleaner, Healthier Caffeine |Brandon Presser |August 29, 2014 |DAILY BEAST 

But death—the taking of life—was a selective process, intentionally executed, the result a foreseen conclusion. Hooded Detective, Volume III No. 2, January, 1942 |Various 

I'm opposed to dictators, myself; that—and the Selective Service law, of course—was why I was a soldier. Hunter Patrol |Henry Beam Piper and John J. McGuire 

And this selective desire is none other than the universal Law of Attraction. The Hidden Power |Thomas Troward 

All I wish to say here is that the necessity of some selective process is inherent in the conditions of social life. Introduction to the Science of Sociology |Robert E. Park 

Seines are species-selective, due partly to the preference of certain fishes for special habitat niches. Fishes of Chautauqua, Cowley and Elk Counties, Kansas |Artie L. Metcalf