There were yet two mortal hours to get through, and the men of the party sought the cool shades of the vicarage garden. The Pit Town Coronet, Volume I (of 3) |Charles James Wills 

The council gathered in the vicarage room felt itself strengthened. Marriage la mode |Mrs. Humphry Ward 

To be sitting there in an English vicarage plotting violence against a woman disturbed him. Marriage la mode |Mrs. Humphry Ward 

Again the poet appears to have forwarded the following letter to the Stowmarket Vicarage. East Anglia |J. Ewing Ritchie 

He reached the vicarage on a Sunday, when all, except Martha the old servant, were at church. McClure's Magazine, Vol. 1, No. 2, July, 1893 |Various