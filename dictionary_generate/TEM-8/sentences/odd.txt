First off, the middle column had an odd product, which meant every middle digit had to be odd. Can You Cross Like A Boss? |Zach Wissner-Gross |February 12, 2021 |FiveThirtyEight 

Even so, seeing the Wizards take the court without Beal will be an odd sight Friday. Bradley Beal will miss Friday’s game against the Knicks for rest |Ava Wallace |February 11, 2021 |Washington Post 

One of the odder sensations came from the Fruity Pebbles version. ‘Cereal snacks’ are just bigger cereal for people too lazy to add milk |Emily Heil |February 9, 2021 |Washington Post 

Onscreen text at the beginning of The Pink Cloud tells us the film was written in 2017 and shot in 2019, which feels like an odd announcement to make to your audience. 4 new movies that explore — or predict — a year dominated by a pandemic |Alissa Wilkinson |February 5, 2021 |Vox 

The same can even be said for the Winnipeg Jets, who are holding their own in the division odds despite trading uber-talented right wing Patrik Laine to the Columbus Blue Jackets a week and a half ago. Which Of The NHL’s Best Teams So Far Are For Real? |Neil Paine (neil.paine@fivethirtyeight.com) |February 5, 2021 |FiveThirtyEight 

I've seen video of that satirical guide to SXSW in 1998 where you asked a bunch of bands odd questions. Coffee Talk with Fred Armisen: On ‘Portlandia,’ Meeting Obama, and Taylor Swift’s Greatness |Marlow Stern |January 7, 2015 |DAILY BEAST 

As the controversy unfurled late Monday, it created some odd bedfellows. No. 3 Republican Admits Talking to White Supremacist Conference |Tim Mak |December 30, 2014 |DAILY BEAST 

So when he told me, 'You can come to my show, but you can't come to see Phoebe, and you can't come to see Riccardo, that was odd. Kanye West and Kim Kardashian’s Balmain Campaign: High Fashion Meets Low Culture |Amy Zimmerman |December 23, 2014 |DAILY BEAST 

Would he have been careful enough to destroy the odd pieces of jute you've left so messily about? Alfred Hitchcock’s Fade to Black: The Great Director’s Final Days |David Freeman |December 13, 2014 |DAILY BEAST 

Tom Angell, founder of nonprofit Marijuana Majority, says the whole thing is a bit odd. Tribes to U.S. Government: Take Your Weed and Shove It |Abby Haglage |December 13, 2014 |DAILY BEAST 

Do you know, Monsieur, that just as we were coming into Moulins, we remarked your odd-looking cabriolet de poste. Blackwood's Edinburgh Magazine, Volume 60, No. 372, October 1846 |Various 

He heard himself saying lightly, though with apparent lack of interest: 'How curious, Lettice, how very odd! The Wave |Algernon Blackwood 

There is an odd triangular-shaped hill that rises on one side very boldly and abruptly, called the Fox's Head. Music-Study in Germany |Amy Fay 

He sang the words with an odd, emphatic slowness, turning to look at Lettice between the phrases. The Wave |Algernon Blackwood 

Nothing was out of the ordinary except that the Professor developed an odd trick of continually glancing at his right hand. Uncanny Tales |Various