Dredging technology that is used to deepen ports and harvest minerals from the seafloor also generates low-frequency noise that travels far distances. Underwater Noise Pollution Is Disrupting Ocean Life—But We Can Fix It |Aryn Baker |February 5, 2021 |Time 

This knowledge helped deepen my admiration for his writings and mission. In ‘The Whispering Land,’ a British naturalist collects travel tales — and animals with tails — in Argentina |Andrea Sachs |February 4, 2021 |Washington Post 

For a long time we thought about other ways we could kind of deepen our relationships with fans in ways that are both enriching to those fans, but also supporting the business model by ticket sales or fees directly from readers. ‘Urgency around the community’: How Pop-Up Magazine pivoted to (even more) experimental storytelling |Kayleigh Barber |February 2, 2021 |Digiday 

A business that wants to thrive during unprecedented times like these will want to deepen its connection with its customers. COVID and quarantine are driving contactless retail experiences |Trevor Grigoruk |February 1, 2021 |Digiday 

Over the next decade, corporate America must deepen its investment in solutions for the mental health and well-being of employees. Businesses Must Deepen Their Investment in Employees' Mental Health |Margaret Keane |January 29, 2021 |Time 

Over time, old rivalries began to deepen, particularly over the spoils of corruption. ‘The Good Lie’ and the Hard Truths of South Sudan |John Prendergast |October 3, 2014 |DAILY BEAST 

The votes of two other incumbents in close races only deepen the mystery. America United on Syria, for Now |Michael Tomasky |September 19, 2014 |DAILY BEAST 

But partitioning Andhra, far from healing old wounds, is likely only to deepen them. India’s Newest State Telangana Is Bosnia Redux |Kranti Rai |March 22, 2014 |DAILY BEAST 

India was founded to erase divisive identities, not to deepen them. India’s Newest State Telangana Is Bosnia Redux |Kranti Rai |March 22, 2014 |DAILY BEAST 

These sketches evolve into larger, more intense ink prints that deepen and fade with each additional attempt. Jasper Johns: The Secrets of a Master at Work |Justin Jones |March 15, 2014 |DAILY BEAST 

Hence you may steer West 3/4 South through the night, on which course you will very gradually deepen your water. Narrative of a Survey of the Intertropical and Western Coasts of Australia] [Volume 2 of 2] |Phillip Parker King 

The bronze seemed to deepen in the corporal's face, but it was turned steadily towards his officer. Winston of the Prairie |Harold Bindloss 

As if to deepen the effect of the weird stage setting, Nature contrived that all the winds which blew here should blow mournfully. Mystery Ranch |Arthur Chapman 

It was a discovery calculated to deepen the impression already made upon Thomas's mind. The Circular Study |Anna Katharine Green 

Your agreeing to this will only immeasurably deepen, instead of lessening our inexpressible obligation.' Robin Redbreast |Mary Louisa Molesworth