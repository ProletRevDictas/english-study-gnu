Maybe the peace and reconciliation we have been seeking starts with this. Our Nobel Peace Prize win is an honor and a tragedy |jakemeth |October 25, 2020 |Fortune 

You could easily pass, say, a carbon tax through budget reconciliation. The definitive case for ending the filibuster |Ezra Klein |October 1, 2020 |Vox 

In recent decades, senators from both parties have abused the budget reconciliation process to pass legislation they knew would otherwise fall to a filibuster. The definitive case for ending the filibuster |Ezra Klein |October 1, 2020 |Vox 

When it came to repealing and replacing most of Obamacare — which Republicans used budget reconciliation to try and do with only 51 votes — it turned out that Republicans couldn’t even muster the votes in their own party to repeal the law. The definitive case for ending the filibuster |Ezra Klein |October 1, 2020 |Vox 

The budget reconciliation process was created in 1974 as a way to expedite the completion of appropriations bills. The definitive case for ending the filibuster |Ezra Klein |October 1, 2020 |Vox 

They called for peace, reconciliation, and the safe return of Father Gregorio. Mexico’s Priests Are Marked for Murder |Jason McGahan |January 7, 2015 |DAILY BEAST 

In fact, these kinds of advances helped give religion another huge window of opportunity for racial reconciliation in the 1960s. No Gods, No Cops, No Masters |James Poulos |January 1, 2015 |DAILY BEAST 

It was about the hope and longing for redemption and reconciliation that lies somewhere within each of us. During Advent, Lots of Waiting, But Not Enough Hope |Gene Robinson |December 7, 2014 |DAILY BEAST 

The hope is that ceasefires could build to political reconciliation. The Obama Administration Has Assad Amnesia |Jamie Dettmer |December 4, 2014 |DAILY BEAST 

“The confrontation should not be about revenge, but about reconciliation on both parts,” he stressed. Should You Confront Your Old Bully? |Keli Goff |August 4, 2014 |DAILY BEAST 

Neglecting minor discrepancies, one may safely accept Mr Bain's reconciliation of the various accounts. King Robert the Bruce |A. F. Murison 

He was determined not to go through even the form of an apology, but he was equally determined upon a reconciliation. Ancestors |Gertrude Atherton 

The Pope replied that reconciliation with the Church was an indispensable condition precedent. King Robert the Bruce |A. F. Murison 

Secondly, Randolph prayed for safe conducts for Bruce's envoys, presently to be sent to procure reconciliation with the Church. King Robert the Bruce |A. F. Murison 

Billy shook hands, and took a sip out of the case-bottle, by way of clenching the reconciliation. The Floating Light of the Goodwin Sands |R.M. Ballantyne