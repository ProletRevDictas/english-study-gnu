Meanwhile, we and our partners have already evacuated more than 100,000 Afghans and others who feared for their lives under Taliban rule, a truly remarkable logistical achievement under daunting circumstances. What’s happening in Afghanistan is horrible. But how else was U.S. involvement going to end? |Eugene Robinson |August 26, 2021 |Washington Post 

This contributes to the medium’s uniqueness but can be daunting when resources are limited. Digital out-of-home deserves a seat at the adult table |Verizon |August 3, 2021 |Digiday 

That’s an intimidating number for most of us mere mortals, but, says Asmuth, feeling daunted by the distance is integral to growth in the sport. How to Up Your Trail-Running Game |kklein |July 21, 2021 |Outside Online 

For founders, getting their idea from concept to company, or developing a minimum viable product, is daunting enough, but seeking an initial fundraising round brings a complexity that can be especially challenging to manage. 5 fundraising imperatives for robotics startups |Annie Siebert |July 9, 2021 |TechCrunch 

In the book’s final pages, having sifted through a daunting tangle of conflicting accounts and agendas, she arrives at a set of wrenching conclusions about the crime. Haunted by a grisly execution, an investigator decides to dig deeper |Daniel Stashower |March 5, 2021 |Washington Post 

No misfortunes could disturb the serenity of her soul, and no accumulating perils could daunt her courage. Madame Roland, Makers of History |John S. C. Abbott 

He was experiencing a strange new joy of possession, which no possibility of ridicule could daunt. The Butterfly House |Mary E. Wilkins Freeman 

He declared that no opposition, derision, or contempt, should daunt him. The History of Tasmania , Volume II (of 2) |John West 

Even the storm at its height could not daunt such furious riders. Riders of the Silences |John Frederick 

She inspired us with a courage, a power, and a confidence in her and in our cause, which nothing could shake or daunt. A Heroine of France |Evelyn Everett-Green