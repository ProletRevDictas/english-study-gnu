A nurse collapsed on the floor of the bar, a number of strangers begged for a hug, a man confessed to domestic violence, another explained why he hated his kids. The Last Bar Standing? |Eugene Robinson |September 15, 2020 |Ozy 

Mallott gave her an awkward hug and she walked out the door. The Woman Propositioned by Alaska’s Former Lieutenant Governor Tells Her Story for the First Time |by Kyle Hopkins and Michelle Theriault Boots, Anchorage Daily News |September 10, 2020 |ProPublica 

On several occasions, this person initiated a friendly hug when I came to her work place, and I reflexively gave her a peck on top of her head. Alaska’s Attorney General on Unpaid Leave After Sending Hundreds of “Uncomfortable” Texts to a Young Colleague |by Kyle Hopkins, Anchorage Daily News |August 25, 2020 |ProPublica 

Since the outbreak of the coronavirus, the Milwaukee, Wisconsin resident says she’s stayed away from most people, resorting to “air hugs” when she’s seen her 20-year-old niece at a distance. The pandemic problem tech can’t solve: Physical touch |Danielle Abril |August 23, 2020 |Fortune 

The 32-year-old digital strategist said given that she’s not a touchy-feely person, she doesn’t really miss hugs or handshakes. The pandemic problem tech can’t solve: Physical touch |Danielle Abril |August 23, 2020 |Fortune 

With help, he got to his feet, and when she hugged him he lifted his arms slightly as if to return the hug. Alfred Hitchcock’s Fade to Black: The Great Director’s Final Days |David Freeman |December 13, 2014 |DAILY BEAST 

"Kate sought me out and gave me a hug just before she left," said Mrs Smith afterwards to a reporter at the Daily Mail. Tearful Kate Weeps After Meeting Mother Whose Baby Died |Tom Sykes |November 25, 2014 |DAILY BEAST 

And Fred stuck in the card for me that said, “Live from New York…” and gave me a hug. How Aidy Bryant Stealthily Became Your Favorite ‘Saturday Night Live’ Star |Kevin Fallon |October 31, 2014 |DAILY BEAST 

Charlie Crist got thrown out of the party for one hug, after all. John Kasich’s Unforgivable Truth About Obamacare |Michael Tomasky |October 21, 2014 |DAILY BEAST 

He would pull her toward him, hug her, kiss her, and stroke her hair. It Was All a Dream: Drama, Bullshit, and the Rebirth of The Source Magazine |Alex Suskind |October 14, 2014 |DAILY BEAST 

There is a man who says he has been at evening parties out West, where the boys and girls hug so hard that their sides cave in. The Book of Anecdotes and Budget of Fun; |Various 

He was a stubborn idealist, and having found something at last to admire he purposed to hug it. Ancestors |Gertrude Atherton 

"Oh," cried Betty, hugging Amy ecstatically, simply because she happened to be the nearest one to hug. The Outdoor Girls in the Saddle |Laura Lee Hope 

Rushing into the cabin, the girl gave him a hug that caused Jim to nearly drop the coffee pot. The Shepherd of the Hills |Harold Bell Wright 

He quotes me as having then said, that we ought not to hug these lands as a very great treasure. Select Speeches of Daniel Webster |Daniel Webster