The rhythmic, repetitive “ba-ba-ba’s” and “ga-ga-ga’s” of human infants may sound like gibberish, but they are necessary exploratory steps toward learning how to talk. These baby greater sac-winged bats babble to learn their mating songs |Jonathan Lambert |August 19, 2021 |Science News 

Depending on which blocks you take out, you can get multiple words, or complete gibberish. New ‘Universal Switch’ Lets Scientists Fine-Tune Gene Therapy |Shelly Fan |August 3, 2021 |Singularity Hub 

It’s hard to explain my life to him, because all my gripes seem like gibberish. The Pandemic Has Been Tough for Extroverts. Here's How I Managed to Make Friends Anyway |Belinda Luscombe |April 20, 2021 |Time 

Because one of his books was written in an incomprehensible code, some have thought the word gibberish was derived from Jabir. Top 10 science anniversaries to celebrate in 2021 |Tom Siegfried |February 3, 2021 |Science News 

When a clerk at the receiving end set her Enigma to the same starting position and typed the gibberish, the original text appeared. Hackers thrive on the weak link in cybersecurity: People |Gershom Gorenberg |February 1, 2021 |Washington Post 

All Facebook will see is cyphertext—the mathematical gibberish computers generate to thwart spying eyes. Crypto for the Masses: Here’s How You Can Resist the NSA |Quinn Norton |May 12, 2014 |DAILY BEAST 

She said some more gibberish, and then refused the statuette from presenter Roger Moore. The Most ‘WTF’ Oscar Moments Ever: Rob Lowe’s Duet with Snow White, Sacheen Littlefeather, and the Streaker |Kevin Fallon, Marlow Stern |February 27, 2014 |DAILY BEAST 

He ranted and raved in gibberish, always talking about himself, but was also very intelligent. Speed Read: 9 Revelations From Elizabeth Smart’s Memoir, ‘My Story’ |The Daily Beast |October 10, 2013 |DAILY BEAST 

What can be a monologue about class and social-climbing to women can be near gibberish to men. The Language of Margaret Thatcher’s Handbags |Robin Givhan |April 8, 2013 |DAILY BEAST 

As I said, the mainstream media will fall for this gibberish. Michael Tomasky on the Ridiculousness of Paul Ryan and Marco Rubio |Michael Tomasky |December 6, 2012 |DAILY BEAST 

But when he tried to express the cooperative impulse that stirred within him, his noises became gibberish. Before Adam |Jack London 

Now used generally for a spell or conjuring word: mere gibberish. Chambers's Twentieth Century Dictionary (part 1 of 4: A-D) |Various 

At the sound of his approach a woman came running to the door, shrieking for assistance in a Mexican gibberish. Murder in Any Degree |Owen Johnson 

When among strangers, they elude inquiries respecting their peculiar language, calling it Gibberish. The Book of Curiosities |I. Platts 

They're long strings of mathematical gibberish, and they have an almost magic property. Little Brother |Cory Doctorow