A talent who can rap, hit a falsetto, and isn’t afraid to be goofy on wax, Raj’s music and skills show why he is a powerful teacher and a class clown you need to add to your own playlist queue. Soulection’s Joe Kay Presents ‘A Beginner’s Guide To Future Sounds’ |Brande Victorian |February 5, 2021 |Essence.com 

We made them look ridiculous by contrasting our joyous celebration of democracy with their clown show. The Secret History of the Shadow Campaign That Saved the 2020 Election |Molly Ball |February 4, 2021 |Time 

One piece follows a waitress on the hunt for her missing daughter, another captures what happens when a drug dealer meets a lawyer and a clown at a bar. Here Are the 14 New Books You Should Read in February |Annabel Gutterman |February 2, 2021 |Time 

Mazel tov on your engagement, Hannah — your gift of a disembodied clown head on a plank will be in the mail soon. Style Conversational Week 1415: Slaphappy seconds |Pat Myers |December 17, 2020 |Washington Post 

I didn’t find a good hat, but I did happen to find one lot of 100 little disembodied, glummish clown heads — they looked so Loserly! Style Conversational Week 1413: Our new not-so-big top -- the Clowning Achievement trophy |Pat Myers |December 3, 2020 |Washington Post 

So many families come that Vargas has arranged for a clown to entertain the kids. America’s Fastest Growing Death Holiday Is From Mexico |Michael Schulson |November 1, 2014 |DAILY BEAST 

An 18-year-old man dressed as a clown mugged a pedestrian, striking him 30 times in the back and neck with an iron bar. French Freak-Out Over Creepy Clowns |Tracy McNicoll |October 31, 2014 |DAILY BEAST 

There have more recent reports of Wasco Clown inspired sightings from as far away as Fishers, Indiana. Nightmares in Face Paint: Why We’ll Always Be Afraid of Clowns |Oliver Jones |October 18, 2014 |DAILY BEAST 

The subtext of the clown is that life is a joke and can be snatched away at any moment. Nightmares in Face Paint: Why We’ll Always Be Afraid of Clowns |Oliver Jones |October 18, 2014 |DAILY BEAST 

There is something about a clown that stays with people: the bright colors, their tendency to be demonstrative. Nightmares in Face Paint: Why We’ll Always Be Afraid of Clowns |Oliver Jones |October 18, 2014 |DAILY BEAST 

Wheeler and Smith formed a junction, and moved clown upon the abandoned post of Budhawal. The History of England in Three Volumes, Vol.III. |E. Farr and E. H. Nolan 

He must have fallen asleep again, for, when he opened his eyes, there was the clown at the foot of his bed making a face. The Talking Horse |F. Anstey 

It was a singular fact about the clown that the slightest check seemed to take away all his brilliancy. The Talking Horse |F. Anstey 

As he looked back, he could see the clown galloping round the corner and hear his yell of discovery. The Talking Horse |F. Anstey 

Breakfast was over at last, and the clown took Tommy's arm and walked upstairs to the first floor with him. The Talking Horse |F. Anstey