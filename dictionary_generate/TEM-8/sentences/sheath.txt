The included rugged leather sheath securely attaches to a belt. Runner-Up Review: The Hunting Gear That Almost Made Our 2022 Winter Buyer’s Guide |agintzler |October 26, 2021 |Outside Online 

Each season, this whole sheath adds a layer of carbon-capturing tissue from root to crown. The first step in using trees to slow climate change: Protect the trees we have |Susan Milius |July 13, 2021 |Science News 

Inside the branches, the trunk and big roots, an actively growing sheath surrounds the inner ghost plumbing. The first step in using trees to slow climate change: Protect the trees we have |Susan Milius |July 13, 2021 |Science News 

They can essentially shut down for months or even years at a time, and one species can ride out dry spells in a protective mucus sheath. Salamanders have a secret to surviving climate change |Benji Jones |June 17, 2021 |Vox 

It measures in at 7 and ¼ inches in length, and includes a wooden sheath for easy storage and protection. Best ice pick: A versatile winter tool for camping and more |PopSci Commerce Team |February 25, 2021 |Popular-Science 

De la Renta did design some stuff for Kennedy too—notably, one perfect belted sheath in crisp white linen. How Oscar de la Renta Created First Lady Fashion |Raquel Laneri |October 21, 2014 |DAILY BEAST 

Finally, she slithered into a finished dress: the ultimate chic sheath, a collarless long-sleeved navy blue garment. Tilda Swinton and Oliver Saillard Perform the Creation of Fashion in ‘Eternity Dress’ |Sarah Moroz |November 21, 2013 |DAILY BEAST 

And she has been spotted wearing nothing but the most discrete, tailored sheath dresses. Petraeus Affair Stereotypes: The General, The Flirt And The Harlot |Robin Givhan |November 15, 2012 |DAILY BEAST 

The first lady wore a Michael Kors sheath with a matching cropped jacket and traditional pearls. Michelle Obama and Ann Romney: First Ladies of Style |Robin Givhan |October 24, 2012 |DAILY BEAST 

The brightly colored sheath was a form of political détente and self-definition. Michelle Obama’s First-Lady Fashion: Subtle and Savvy |Robin Givhan |September 4, 2012 |DAILY BEAST 

The Datto complaisantly allowed me to draw it from the sheath and pass it round to my friends. The Philippine Islands |John Foreman 

Whipping the long, keen blade from its sheath, Marius bore down upon the rash meddler. St. Martin's Summer |Rafael Sabatini 

Tail truncated, spine on the side of the tail very distinct, imbedded in a sheath. Narrative of a Survey of the Intertropical and Western Coasts of Australia] [Volume 2 of 2] |Phillip Parker King 

He saw a large sheath-knife, and secured that in his own belt; then he took a mouthful of wine, and went to his post. The Chequers |James Runciman 

He was trying to 286 work the knife from its sheath before I could force him backward or break his neck. A Virginia Scout |Hugh Pendexter