That’s a tall order right now for many brick-and-mortar retailers that were already under pressure and watched foot traffic disappear entirely as the country largely shut down in March in response to the pandemic threat. That Whole Foods is an Amazon warehouse; get used to it |Connie Loizos |September 4, 2020 |TechCrunch 

Human bones were found buried beneath the platforms, including a stillborn foetus enclosed in a brick. An Ancient Site with Human Skulls on Display - Issue 89: The Dark Side |Jo Marchant |September 2, 2020 |Nautilus 

Surprisingly, there aren’t many things I miss about a sticks-and-bricks home. 5 Portable Grills for All Your Outdoor Cooking Needs |Amy Marturana Winderl |September 2, 2020 |Outside Online 

HOUSTON — On an afternoon in mid-June, Analleli Solis was walking home from her brother’s house just down the street when she noticed someone she didn’t know retreating from the front door of her modest brick home. The Loan Company That Sued Thousands of Low-Income Latinos During the Pandemic |by Kiah Collier, Ren Larson and Perla Trevizo |August 31, 2020 |ProPublica 

To check if this coherence time was really caused by the natural radiation, they built a giant shield out of lead brick that could block background radiation to see what happened when the qubits were isolated. Could Quantum Computing Progress Be Halted by Background Radiation? |Edd Gent |August 31, 2020 |Singularity Hub 

It was a brick wall that we turned into the on-ramp of a highway. Coffee Talk with Fred Armisen: On ‘Portlandia,’ Meeting Obama, and Taylor Swift’s Greatness |Marlow Stern |January 7, 2015 |DAILY BEAST 

The industry lore is downright jaw-dropping in the details of the cons known as “brick-in-box” returns. The Insane $11 Billion Scam at Retailers’ Return Desks |M.L. Nestel |December 19, 2014 |DAILY BEAST 

Tallinn feels palpably Scandinavian with its polished old-town brick, seaside positioning and glut of cool cafes. Next Stop, Quito: Our Top Cities for 2015 |Brandon Presser |December 19, 2014 |DAILY BEAST 

Once a cadet dropped a brick from a third-story barracks window that barely missed Jackson. Stonewall Jackson, VMI’s Most Embattled Professor |S. C. Gwynne |November 29, 2014 |DAILY BEAST 

Opposite is a red-brick monastery leaning like an ocean liner in the snow. Russia’s Gold Digger Academy |Peter Pomerantsev |November 11, 2014 |DAILY BEAST 

To hear the creature talk about it makes my mouth as a brick kiln and my flesh as that of a goose. The Joyous Adventures of Aristide Pujol |William J. Locke 

Therefore shall Moab howl to Moab, every one shall howl: to them that rejoice upon the brick walls, tell ye their stripes. The Bible, Douay-Rheims Version |Various 

Wherefore my bowels shall sound like a harp for Moab, and my inward parts for the brick wall. The Bible, Douay-Rheims Version |Various 

All the large engines are in this way, and we do not find the door or front plate get hot, as they are lined with brick. Life of Richard Trevithick, Volume II (of 2) |Francis Trevithick 

The boiler was placed underneath the engine, the fire under it, with brick flues. Life of Richard Trevithick, Volume II (of 2) |Francis Trevithick