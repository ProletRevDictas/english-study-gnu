My mother, after two years of college, got married and held secretarial and retail jobs. The ghosts of our motel |Sabaa Tahir |November 29, 2021 |Vox 

In high school, O’Leary did secretarial work at the Pentagon, where she developed an affinity for clerical record books and other bureaucratic items. D.C. is a city of grand monuments and federal buildings. These photos capture the often unseen poetic details. |Kelsey Ables |July 30, 2021 |Washington Post 

A prospective boss told young Cokie Roberts that women like her loved their dead-end secretarial jobs. Four women who broke barriers to become the founding mothers of NPR |Connie Schultz |April 16, 2021 |Washington Post 

In March 1944, shortly before Joye Hummel graduated from the Katharine Gibbs secretarial school in Manhattan, she was invited to meet with one of her instructors, a charismatic psychologist who had been impressed by her essays on a take-home test. Joye Hummel, first woman hired to write Wonder Woman comics, dies at 97 |Harrison Smith |April 8, 2021 |Washington Post 

I had been at secretarial college with those two girls [Valerie and her sister Mary], so I knew them very well. Kate Middleton’s Code-Breaking Granny: Duchess Uncovers Wartime Secrets |Tim Teeman |June 19, 2014 |DAILY BEAST 

After all, she was back in the same tired circle: the icy, gray streets of Detroit, back and forth to work at a secretarial job. The Strange and Mysterious Death of Mrs. Jerry Lee Lewis |Richard Ben Cramer |January 11, 2014 |DAILY BEAST 

The British broadsheets called it “the most coveted secretarial job in the world.” The Beatles’ Secretary, Good Ol’ Freda, Breaks Silence in Exclusive SXSW Interview |Marlow Stern |March 14, 2013 |DAILY BEAST 

When I was in high school, I had a secretarial job at a fuel company in a small town in New Hampshire. Black Women Still Battle Mad Men in Corporate America |Rebecca Carroll |March 30, 2012 |DAILY BEAST 

Draper told Lois that she was not made for a secretarial job. The Mad Men Finishing School |Emma Pearse |August 11, 2009 |DAILY BEAST 

She had done some secretarial work for a charity of which the duchess was patroness. Emily Fox-Seton |Frances Hodgson Burnett 

They'll probably tell you to take the next rocket back and report to the secretarial pool, I'm afraid. Operation: Outer Space |William Fitzgerald Jenkins 

He had discovered her among communist councils in Berlin and naïvely attached her as a part of Dorn's secretarial retinue. Erik Dorn |Ben Hecht 

Miss Conder's secretarial duties apparently left her wide margins of leisure which were always at the disposal of Miss Black. Notwithstanding |Mary Cholmondeley 

Presently I seated myself at the table and recommenced my secretarial duties, while he went forth. The Minister of Evil |William Le Queux