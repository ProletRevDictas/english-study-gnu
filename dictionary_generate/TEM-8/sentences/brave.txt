We have seen enough of this brave new approach among pharma companies, however, to think their collective action over the past three-quarters of a year might actually change the world, or save part of it. ‘The whole world is coming together’: How the race for a COVID vaccine is revolutionizing Big Pharma |cleaf2013 |September 21, 2020 |Fortune 

Finally, some brave producers have started to schedule holiday releases. COVID complicates fall film releases |Brian T. Carney |September 17, 2020 |Washington Blade 

If you’re brave enough, put yourself in Terence Davis’s shoes. Give Boston’s Kemba Walker A Double Pick And Watch Him Work |Michael Pina |August 31, 2020 |FiveThirtyEight 

“You can make sure that when you are talking about de-escalation, you are brave enough to discuss the circumstances where it doesn’t go in a way that feels incredibly neat, where it feels incredibly easy to deal with,” said Cabral. By being too customer-obsessed, DTC startups are failing their retail employees |Anna Hensel |August 21, 2020 |Digiday 

Many of the brave and caring staff in these nursing homes become infected, likely because of the intensity of this higher R0 and their exposure time with residents. Failure To Count COVID-19 Nursing Home Deaths Could Dramatically Skew US Numbers |LGBTQ-Editor |April 27, 2020 |No Straight News 

What I had “on the girls” were some remarkably brave first-person accounts. I Tried to Warn You About Sleazy Billionaire Jeffrey Epstein in 2003 |Vicky Ward |January 7, 2015 |DAILY BEAST 

“He was a brave field commander and an expert in intelligence, and in organizing popular and tribal forces,” said the eulogist. What an Iranian Funeral Tells Us About the Wars in Iraq |IranWire |January 6, 2015 |DAILY BEAST 

But what he did was reasonably brave and freighted with all the symbolism of which he was well aware. Steve Scalise and the Right’s Ridiculous Racial Blame Game |Michael Tomasky |January 2, 2015 |DAILY BEAST 

These brave souls took an icy dip in the ocean to ring in 2015 and raise money for charity. Diving Into 2015 With Polar Bear Plunge Extremists |James Joiner |January 1, 2015 |DAILY BEAST 

Or you may not have many—or any—friends, recasting your social exclusion as brave defiance of social norms. The Refuseniks Hiding From ‘Happy New Year’ |Lizzie Crocker |December 31, 2014 |DAILY BEAST 

Nogués and his brave lads have done their bit indeed for the glory of the Army of France. Gallipoli Diary, Volume I |Ian Hamilton 

Its record is largely that of battles and sieges, of the brave adventure of discovery and the vexed slaughter of the nations. The Unsolved Riddle of Social Justice |Stephen Leacock 

I feel proud and happy to shelter beneath my roof any of our valued and brave allies. Blackwood's Edinburgh Magazine, Volume 60, No. 372, October 1846 |Various 

A few words explained his errand; but the brave Englishman would hardly hear it to the end. The Pastor's Fire-side Vol. 3 of 4 |Jane Porter 

Then the friars call the natives Spaniards and the military officers own us as their sons and they dub us brave soldiers. The Philippine Islands |John Foreman