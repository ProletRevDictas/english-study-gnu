It also requires districts to be contiguous and “as compact … as practicable,” criteria that certain districts in the new map may not satisfy. New York’s Proposed Congressional Map Is Heavily Biased Toward Democrats. Will It Pass? |Nathaniel Rakich (nathaniel.rakich@fivethirtyeight.com) |January 31, 2022 |FiveThirtyEight 

The technology industry—produced and made wealthy by these immense advances in computing—has failed to imagine alternative futures both bold and practicable enough to address humanity’s gravest health and climatic challenges. Where computing might go next |Margaret O’Mara |October 27, 2021 |MIT Technology Review 

His counterfactual musings don't provide any practicable, coherent or implementable alternatives. Israel and Palestine Vs. ‘Blood and Magic’ |Hussein Ibish, Saliba Sarsar |September 17, 2013 |DAILY BEAST 

It makes his prescriptions particularly powerful because they are practicable, if not a little ambitious. Political Independents: The Future of Politics? |John Avlon |September 23, 2012 |DAILY BEAST 

ARIES Power of concentration is at a peak, percolating grains of ideas into practicable projects. What the Stars Hold for Your Week |Starsky + Cox |July 30, 2011 |DAILY BEAST 

But what might have been very practicable for eight hundred and sixty men, was impossible for three hundred and sixty. Blackwood's Edinburgh Magazine, No. CCCXXXIX. January, 1844. Vol. LV. |Various 

My coup-d'œil assured me that it was practicable to give to this feature the character of a projecting under-jaw. Checkmate |Joseph Sheridan Le Fanu 

A study of blood bacteriology is useful, but is hardly practicable for the practitioner. A Manual of Clinical Diagnosis |James Campbell Todd 

It was not practicable to deny a legal-tender value to so much Mexican, and Spanish-Philippine coin in circulation. The Philippine Islands |John Foreman 

But it was not, in fact, found practicable to avoid improving the accommodation, even for the able-bodied. English Poor Law Policy |Sidney Webb