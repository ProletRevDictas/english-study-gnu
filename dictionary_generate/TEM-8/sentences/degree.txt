So this was seen as perhaps the first known use of this degree to enact a large data grab. Podcast: COVID-19 is helping turn Brazil into a surveillance state |Anthony Green |September 16, 2020 |MIT Technology Review 

While it’s unclear how exactly Opendoor is now faring financially, shares of competitors Redfin and Zillow—which have also jumped into the iBuying market, though to a lesser degree—have breached all-time highs. Opendoor will go public via SPAC |Lucinda Shen |September 15, 2020 |Fortune 

Crop yields, though, will drop sharply with every degree of warming. Climate Change Will Force a New American Migration |by Abrahm Lustgarten, photography by Meridith Kohut |September 15, 2020 |ProPublica 

After his two years in the Peace Corps, Hastings went back to school — this time to Stanford for a graduate degree in computer science. What if Your Company Had No Rules? (Bonus Episode) |Maria Konnikova |September 12, 2020 |Freakonomics 

ByteDance is in the mix, along with Microsoft, Walmart and other companies to a lesser degree, like Oracle. China may kill TikTok’s U.S. operations rather than see them sold |Alex Wilhelm |September 11, 2020 |TechCrunch 

“He turned pale, trembled to a great degree, was much agitated, and began to cry,” she told the court. New York’s Most Tragic Ghost Loves Minimalist Swedish Fashion |Nina Strochlic |January 8, 2015 |DAILY BEAST 

Investigators will focus on whether the sudden emergency was so extreme that no degree of pilot skill would have helped. Flight 8501 Poses Question: Are Modern Jets Too Automated to Fly? |Clive Irving |January 4, 2015 |DAILY BEAST 

Dean Todd remained my friend until I graduated in 1988, with my degree in English literature. I Was Gang Raped at a UVA Frat 30 Years Ago, and No One Did Anything |Liz Seccuro |December 16, 2014 |DAILY BEAST 

“I brought it with no small degree of trepidation,” Kucinich recalled in a lengthy phone conversation with the Daily Beast. Repubs Should Take It From Kucinich: Impeachment Isn’t Worth It |Eleanor Clift |December 5, 2014 |DAILY BEAST 

I am not one of those people who believe that anyone with a college degree is by definition smarter than those without one. Are College Educated Police Safer? |Keli Goff |December 1, 2014 |DAILY BEAST 

It is only just to say, that the officers exhibited a degree of courage far beyond any thing we had expected from them. Blackwood's Edinburgh Magazine, No. CCCXXXIX. January, 1844. Vol. LV. |Various 

He asked what time was usually spent in determining between right and wrong, and what degree of expense? Gulliver's Travels |Jonathan Swift 

He was a bookbinder previous to going upon the stage; and acquired a high degree of reputation as an actor. The Every Day Book of History and Chronology |Joel Munsell 

His Indian repute had not preceded him to such degree as to make the way easy for him through the London crowd. Kipling Stories and Poems Every Child Should Know, Book II |Rudyard Kipling 

This is often of great advantage, as the strength of repose is expressed to a great degree in restraint of movement. Expressive Voice Culture |Jessie Eldridge Southwick