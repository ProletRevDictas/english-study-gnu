King’s critique of capitalism and militarism was also tied to his growing interest in radical pacifism and the use of nonviolent direct action. The public has underestimated the radicalism of Martin Luther King Jr.’s early work |Victoria Wolcott |January 17, 2022 |Washington Post 

What most liberals are passionate about is one thing: opposition to U.S. militarism. When Liberals Enable Tyrants |Michael Tomasky |September 6, 2013 |DAILY BEAST 

Was the ‘Great War’ a necessary fight against German militarism, or was it completely avoidable? The Utterly Pointless First World War |Michael F. Bishop |May 22, 2013 |DAILY BEAST 

Prussian militarism was unlovely, to be sure, but the Kaiser was no Führer. The Utterly Pointless First World War |Michael F. Bishop |May 22, 2013 |DAILY BEAST 

But most recent histories depict it as a necessary fight against German militarism. The Utterly Pointless First World War |Michael F. Bishop |May 22, 2013 |DAILY BEAST 

Even more important, we need to pay attention to the emergence of militarism among some military officers. China’s Risky Path, From Revolution to War |Cheng Li |January 20, 2013 |DAILY BEAST 

Militarism did not crush them with its weight of lead and steel and its terrible waste of productive energy. The Scandinavian Element in the United States |Kendric Charles Babcock 

Militarism in the sense of courage, patriotism, discipline, and devotion to duty is a splendid thing. Secret Societies And Subversive Movements |Nesta H. Webster 

Moreover, Germany as the home of militarism offers a wide field for Jewish speculation. Secret Societies And Subversive Movements |Nesta H. Webster 

We went to war against German militarism, and to make the world safe for democracy—meaning thereby capitalist commercialism. The Book of Life: Vol. I Mind and Body; Vol. II Love and Society |Upton Sinclair 

"German militarism," the reply that springs to the lips, is no more a threat to civilisation than French or Russian militarism. The Crime Against Europe |Roger Casement