Some of it is longer-form like Canadian filmmaker Adrijan Assoufi’s six-minute documentary about one of the last matches legendary footballer Diego Maradona played before he retired. ‘Football has lost its soul’: How Copa90 is repositioning itself around the creator economy |Seb Joseph |September 22, 2021 |Digiday 

The social media training a footballer gets, for example, will be focused on not getting the club in trouble. Former Manchester United footballer Louis Saha is helping athletes become media entrepreneurs |Seb Joseph |June 24, 2021 |Digiday 

The NFL teams will all be weighing up the impact of drafting the first openly gay pro-footballer. Which Team Will Make History With Michael Sam Tonight? |Robert Silverman |May 8, 2014 |DAILY BEAST 

But Stoney, who has 116 caps for England, is now the most high-profile active gay footballer in England. England’s Women’s Soccer Captain Casey Stoney Comes Out |The Telegraph |February 12, 2014 |DAILY BEAST 

All-American Surprise, Ruined This Boise State footballer wins the game, and the girl. Public Marriage Proposals Gone Wrong: The 13 Biggest Fails (VIDEO) |Anna Klassen |February 14, 2012 |DAILY BEAST 

The footballer insisted the sex was consensual and was cleared of rape—only to plead guilty to simple assault. Forced to Cheer for Her Attacker |Jessica Bennett |September 15, 2011 |DAILY BEAST 

He wasn't the first professional footballer she had dated," Barboni says, "but it is better that I don't comment on this. Brazil's Soccer Star a Murderer? |Dom Phillips |July 5, 2010 |DAILY BEAST 

Bernard Monck swooped down with the action of a practised footballer and took the furry thing out of Tessa's hold. The Lamp in the Desert |Ethel M. Dell 

He immediately swerved into it like a footballer making a dodging run, then turned away again. Letters from France |C. E. W. Bean 

He felt like a confirmed Association footballer suddenly called upon to play in an International Rugby match. A Man of Means |P. G. Wodehouse and C. H. Bovill 

With the calves of a footballer and the upper limbs of a Sandow, she is a fearful and wonderful example of the female form divine. The Awful Australian |Valerie Desmond 

Good cricketer and footballer, I mean, and all that sort of thing. Mike |P. G. Wodehouse