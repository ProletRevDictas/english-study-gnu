Highly portable, they’re easy to use in gardens, behind shrubs, and anywhere that collects dead leaves. Leaf blowers that help you fight fallen foliage |PopSci Commerce Team |September 25, 2020 |Popular-Science 

Shrubs tend to burn hot, but shrub ecosystems are well-adapted to fire. What the Photos of Wildfires and Smoke Don’t Show You |by Elizabeth Weil and Lisa Larson-Walker |September 21, 2020 |ProPublica 

That heat and dryness turned grasses, shrubs, and trees into easy tinder, ready to ignite at the slightest spark. “Unprecedented”: What’s behind the California, Oregon, and Washington wildfires |Umair Irfan |September 11, 2020 |Vox 

SDG&E said an increased risk from shrubs and bushes “has no relevance to the issue,” meaning it’s trees that matter here. Watchdog Warns: SDG&E’s Tree-Trimming Plan Could Worsen Wildfires |MacKenzie Elmer |August 24, 2020 |Voice of San Diego 

All this suggested that Antarctica was once a forest full of conifers, ferns and flowering shrubs. A rainforest once grew near the South Pole |Carolyn Gramling |May 11, 2020 |Science News For Students 

Luis, whose heart was severed by the blade, had collapsed under a large shrub. Arnold Pardoned My Son’s Attacker |Bruce Henderson |May 11, 2011 |DAILY BEAST 

It was supposed by many on its discovery to grow like the engraving given—in form resembling a tree or shrub rather than an herb. Tobacco; Its History, Varieties, Culture, Manufacture and Commerce |E. R. Billings. 

Almost any other kind of shrub'd have died long ago, neglected as things have been, but you can't kill a currant bush. Dorothy at Skyrie |Evelyn Raymond 

Brutus was getting well, but there would always be a scar on his shoulder, where the sharp-pointed shrub had entered the flesh. The Cromptons |Mary J. Holmes 

He shook his cramped limbs with as little ceremony as if Kano were a shrub, and then turned, with the evident intention of flight. The Dragon Painter |Mary McNeil Fenollosa 

“Guayule” is a resinous rubber secured from a two-foot shrub that grows on the arid plains of Texas and Northern Mexico. The Wonder Book of Knowledge |Various