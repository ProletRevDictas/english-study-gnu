Clad in a blue, striped button-down, a silver watch adorning his left wrist, Huckabee beams on the cover. Huckabee 2016: Bend Over and Take It Like a Prisoner! |Olivia Nuzzi |January 8, 2015 |DAILY BEAST 

One is forced to ask, what on earth was Andrew doing hanging out with scantily clad teenagers? Buckingham Palace Disputes Sex Allegations Against Prince ‘Randy Andy’ |Tom Sykes |January 4, 2015 |DAILY BEAST 

Based on his sock puppet, I expected him to be a burly bearded giant clad in plaid—basically, a Canadian Paul Bunyan. Canada’s Subversive Sock Puppet: Ed the Sock Isn’t Afraid to Say Anything |Soraya Roberts |November 13, 2014 |DAILY BEAST 

They described him as clad in black, his face smeared with mud. Killer Eric Frein Held in Murdered Cop’s Cuffs |Michael Daly |October 31, 2014 |DAILY BEAST 

A photo of the most recent professional tournament showed a fully male, predominantly white, t-shirt and cargo-shorts-clad top 8. Is ‘Magic: The Gathering’ Immune to GamerGate Misogyny? |David Levesley |October 29, 2014 |DAILY BEAST 

When the short-skirted, gossamer clad nymphs made their appearance on the stage they became restless and fidgety. The Book of Anecdotes and Budget of Fun; |Various 

The governor made a strong thrust at him, which almost knocked him down; but showed that he was clad in armor. The Philippine Islands, 1493-1898, Volume XX, 1621-1624 |Various 

The Southern sun shone from a cloudless sky; a light, keen wind blowing from the distant snow-clad Canigou set the blood tingling. The Joyous Adventures of Aristide Pujol |William J. Locke 

A fellow rudely clad—a hybrid between man-at-arms and lackey—lounged on a musket to confront them in the gateway. St. Martin's Summer |Rafael Sabatini 

Her face was mild and pale; but it was the transparent hue of the virgin flower of spring, clad in her veiling leaves. The Pastor's Fire-side Vol. 3 of 4 |Jane Porter