As the low-carb trends declined in the mid-aughts, so too did consumer appetites for blandly seasoned grain cakes. The Rise and Fall of the Rice Cake, America’s One-Time Favorite Health Snack |Brenna Houck |September 17, 2020 |Eater 

In the latest example of bolstering its first-party data offering for advertisers, Vice Media Group is using a new tool from consumer reporting agency Experian and data platform Infosum. ‘Re-architecting the entire process’: How Vice is preparing for life after the third-party cookie |Lara O'Reilly |September 17, 2020 |Digiday 

The silver lining in all this—for the state of Americans’ personal finances, if not for retailers—seems to be a higher savings rate among consumers. What retailers should expect going into a holiday season during a pandemic |Rachel King |September 16, 2020 |Fortune 

Opendoor struggled in the early days of the pandemic, laying off 600 employees, or 35% of its workforce, in April as consumers stayed at home. Opendoor will go public via SPAC |Lucinda Shen |September 15, 2020 |Fortune 

It’s the very purpose of net neutrality that consumers can access everything the internet has to offer. The EU’s top court just closed a major loophole in Europe’s net-neutrality rules |David Meyer |September 15, 2020 |Fortune 

She appeared to be just a happy American consumer out shopping at a big-box store. The Insane $11 Billion Scam at Retailers’ Return Desks |M.L. Nestel |December 19, 2014 |DAILY BEAST 

Zumbiel Packaging A Kentucky-based manufacturer of paperboard packaging for consumer goods. The 26 Next Hobby Lobbys |Abby Haglage |December 17, 2014 |DAILY BEAST 

These insights and discoveries help PepsiCo anticipate, rather than react to, an ever-changing consumer landscape. The Science of Ingredient Innovation | |December 15, 2014 |DAILY BEAST 

What tastes great to an American consumer may not be what folks in China or India would choose to eat or drink. The Science of Ingredient Innovation | |December 15, 2014 |DAILY BEAST 

At some point even the seemingly insatiable American consumer is going to have had his fill. Christmas Is the New Subprime |Doug McIntyre |December 9, 2014 |DAILY BEAST 

But suppose that the consumer, for the things which he himself makes and sells, or for the work which he performs, receives more? The Unsolved Riddle of Social Justice |Stephen Leacock 

For a long time there were no contractors between the European sources of supply and the great consumer, the army. The Supplies for the Confederate Army |Caleb Huse 

The graduated scale was a complete failure, and equally injurious to the purchaser and consumer. The History of England in Three Volumes, Vol.III. |E. Farr and E. H. Nolan 

Sir, a consumer's delegation wishes to speak with you about the new Birthday Quotas. The Great Potlatch Riots |Allen Kim Lang 

Tell that bunch of complainers I'll keep this District's economy healthy if I have to jail every consumer in it. The Great Potlatch Riots |Allen Kim Lang