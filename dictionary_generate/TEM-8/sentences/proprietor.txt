Even small proprietors will need smartphone and tech knowledge that they may not wish to obtain. Creative Currencies — The Anti-Crypto |Sohini Das Gupta |August 11, 2021 |Ozy 

They are primarily sole proprietors or what are called microbusinesses that have just a few employees. SBA will directly handle PPP loan forgiveness so banks don’t have to |Aaron Gregg |July 29, 2021 |Washington Post 

Disputed origins are practically mandatory for regional foods, as are intensely felt loyalties to different proprietors. The Great Pink Sugar Cookie Rivalry of Southwestern Utah |Carlye Wisel |May 26, 2021 |Eater 

Advertising revenue in the newspaper industry peaked in 2005, and the overall decline since then has put tremendous pressure on all newspaper proprietors. Tribune shareholders vote to sell legendary chain of newspapers to a hedge fund |Elahe Izadi, Sarah Ellison |May 21, 2021 |Washington Post 

The nine-digit number is “assigned to employers, sole proprietors, corporations, partnerships, estates, trusts, certain individuals, and other entities for tax filing and reporting purposes.” What Forms Do I Need to File My Taxes and Where Can I Get Them? |by Kristen Doerer for ProPublica and Karim Doumar |April 14, 2021 |ProPublica 

But as an old man, he became proprietor of a palace fit for a king—one he built stone-by-stone with his own two hands. The Postman Who Built a Palace in France…by Hand |Nina Strochlic |November 20, 2014 |DAILY BEAST 

He was the proprietor of a grimy chicken joint in Rochester, New York. The Loser Who Wanted to Be the ISIS Agent Next Door |Michael Daly |September 18, 2014 |DAILY BEAST 

More than any other media proprietor, Rupert Murdoch had an intuitive revelation about the value of news as a commodity. Murdoch on the Rocks: How a Lone Reporter Revealed the Mogul's Tabloid Terror Machine |Clive Irving |August 25, 2014 |DAILY BEAST 

Seyed, the proprietor, tells us that there is always a way to keep the police quiet. Tehran’s Underground Speakeasies |IranWire |June 15, 2014 |DAILY BEAST 

Once I was pregnant, I embraced my own femininity and settled into my role as decision maker and proprietor. Yes, Women Can Make Great Wine |Jordan Salcito |March 22, 2014 |DAILY BEAST 

The landed proprietor still redeemed, day after day, portions of his involved estate. Blackwood's Edinburgh Magazine, No. CCCXXXIX. January, 1844. Vol. LV. |Various 

At the same instant the landed proprietor rose from his chair, and was about to depart likewise. Blackwood's Edinburgh Magazine, No. CCCXXXIX. January, 1844. Vol. LV. |Various 

He was the fourth son of a peasant proprietor of Lectourne, a little town on the slopes of the Pyrenees. Napoleon's Marshals |R. P. Dunn-Pattison 

The stout gentleman in plush walked in, and the landed proprietor pointed to the door. Blackwood's Edinburgh Magazine, No. CCCXXXIX. January, 1844. Vol. LV. |Various 

In some of the mines a few hours' stoppage was a serious matter, as it would cost the proprietor as much as 70l. Life of Richard Trevithick, Volume II (of 2) |Francis Trevithick