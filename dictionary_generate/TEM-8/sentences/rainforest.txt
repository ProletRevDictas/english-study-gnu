Leroi believes it should continue to expand its influence “until it becomes an integral part of explaining diversity wherever we see it in the world, be it in the supermarket or a tropical rainforest.” How Neutral Theory Altered Ideas About Biodiversity |Christie Wilcox |December 8, 2020 |Quanta Magazine 

Young trees have adaptations that could save the AmazonThe famously humid Amazon is likely to face increasing droughts as the planet warms, but the teenagers of the rainforest may be coming to its rescue. 2020 isn’t all bad. Here are 13 science stories to be thankful for. |Rachel Feltman, Sara Chodosh |November 26, 2020 |Popular-Science 

As a chocolate maker, I knew Georgetown to be just west of cacao-rich rainforest. Meet Four Craft Chocolate Makers Decolonizing the Industry |Jinji Fraser |October 22, 2020 |Eater 

Swim or paddle in the Big Island’s Hilo Bay, hike rainforest trails along the northeastern Hamakua Coast, or surf at Honolii Beach Park. Our Favorite Hipcamp in Every State |Alison Van Houten |October 1, 2020 |Outside Online 

Reducing its footprint is the best thing we can do to help the rainforest. The Environmental Headache in Your Shampoo - Issue 90: Something Green |Anastasia Bendebury & Michael Shilo DeLay |September 16, 2020 |Nautilus 

He could be remade into a defender of the environment, a preserver of habitats and champion of rainforest ecology. Can Tarzan of the Apes Survive in a Post-Colonial World? |Ted Gioia |November 23, 2014 |DAILY BEAST 

One of their more memorable ones supported the Rainforest Action Network in 1988 at Madison Square Garden. How the Grateful Dead Invented the Ice Bucket Challenge |Barry Barnes |August 24, 2014 |DAILY BEAST 

This little slice of anarchy is a 24-hour bus ride from Panama City, with plenty of rainforest-y scenery along the way. A Man, a Plan, a Canal: Panama Turns 100 |Bill Schulz |August 17, 2014 |DAILY BEAST 

To make room for these plantations, vast areas of rainforest are felled, which leads to primary and secondary loss of species. Our Taste for Cheap Palm Oil Is Killing Chimpanzees |Carrie Arnold |July 11, 2014 |DAILY BEAST 

The anthropologist Claude Levi-Strauss saw the Amazon rainforest, as he saw most things, as a complex structure. Exploring the Amazon, While We Still Can |Darrell Hartman |May 15, 2014 |DAILY BEAST 

In the dry season the river was clear; it is surrounded by rainforest. Amphibians and Reptiles of the Rainforests of Southern El Peten, Guatemala |William E. Duellman 

Specimens were obtained in rainforest in the immediate vicinity of the settlement. Amphibians and Reptiles of the Rainforests of Southern El Peten, Guatemala |William E. Duellman 

Specimens were obtained in the rainforest and in cleared areas in the immediate vicinity of the town. Amphibians and Reptiles of the Rainforests of Southern El Peten, Guatemala |William E. Duellman 

Obviously, the diversity of ecological niches in the rainforest is sufficient to support a variety of related species. Amphibians and Reptiles of the Rainforests of Southern El Peten, Guatemala |William E. Duellman 

From the examples discussed above, the importance of the three dimensional aspect of the rainforest is apparent. Amphibians and Reptiles of the Rainforests of Southern El Peten, Guatemala |William E. Duellman