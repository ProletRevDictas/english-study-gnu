Other neuroses, including anxiety and depression, are similarly linked to faster aging. Your Personality Could Add Years of Healthy Living |Matt Fuchs |November 8, 2021 |Time 

During thru-hikes, I have sometimes known people for less than an hour before I know about their families and neuroses, sex lives and relationship traumas. Why We Love ‘Hiking with Rappers’ |awise |October 28, 2021 |Outside Online 

From 1952 to 1980, the Diagnostic and Statistical Manual of Mental Disorders, or DSM, divided mental ailments into less-debilitating neuroses and more-debilitating psychoses. Psychology has struggled for a century to make sense of the mind |Bruce Bower |August 11, 2021 |Science News 

It sounds like what you really want, Just Me, is a bike trail with no hikers, to not have to navigate their passive-aggressive shaming nor plumb the acute neuroses that it triggers in you. What Do Mountain Bikers Owe Hikers? |Sundog |June 15, 2021 |Outside Online 

After “an industrious night,” he wrote, “the barriers suddenly lifted, the veils dropped, and everything became transparent — from the details of the neuroses to the determinants of consciousness.” A brain researcher on what Freud got right |Jess Keiser |February 26, 2021 |Washington Post 

Whether it ultimately takes aesthetic discipline or neurosis to get to that point, it's hard to say. Frank Gehry Is Architecture’s Mad Genius |Sarah Moroz |October 27, 2014 |DAILY BEAST 

How do you tell the difference between aesthetic discipline and neurosis? Frank Gehry Is Architecture’s Mad Genius |Sarah Moroz |October 27, 2014 |DAILY BEAST 

Is this neurosis, narcissism, or the farsighted wisdom that allows a fellow to win three hundred games? Will the Real Jim Palmer Please Stand Up |Tom Boswell |September 27, 2014 |DAILY BEAST 

At one level, one could look at this film as a portrayal of the beginnings of full-blown neurosis and mental illness. The Ballerina's Curse |Wendy Whelan |December 4, 2010 |DAILY BEAST 

Marked variation in the amount at successive examinations strongly suggests a neurosis. A Manual of Clinical Diagnosis |James Campbell Todd 

Stekel,41 one of Freud's pupils, in an elaborate monograph, also lays stress on the sexual factor of the anxiety-neurosis. The Sexual Life of the Child |Albert Moll 

The neurosis goes back to some organic defect or other cause of childish humiliation. The Behavior of Crowds |Everett Dean Martin 

Compromise mechanisms will again be formed serving a purpose similar to the neurosis. The Behavior of Crowds |Everett Dean Martin 

Many of the characteristics of the unconscious will then appear and will be similar in some respects to those of neurosis. The Behavior of Crowds |Everett Dean Martin