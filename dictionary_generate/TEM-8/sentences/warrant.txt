Among the sort of policing that fell away, the former officer said, was officers’ routine sweeps of areas where drug users congregated, to check their names for outstanding warrants, which would often net suspects in local burglaries. What Can Mayors Do When the Police Stop Doing Their Jobs? |by Alec MacGillis |September 3, 2020 |ProPublica 

At least a dozen times, the report states, judges issued bench warrants for her arrest for violating the terms of her release requiring her to enter various treatment programs. D.C. study documents ‘Life and Death’ of trans woman Alice Carter |Lou Chibbaro Jr. |September 2, 2020 |Washington Blade 

Housing authority officials didn’t cancel the eviction warrant until a month later. She Was Sued Over Rent She Didn’t Owe. It Took Seven Court Dates to Prove She Was Right. |by Danielle Ohl, Capital Gazette, and Talia Buford and Beena Raghavendran, ProPublica |August 25, 2020 |ProPublica 

Whether new team members or veteran managers, we assume we have neither the capacity nor the warrant to reinvent how our organizations work. How to break free of bureaucracy in the workplace |Michele Zanini |August 17, 2020 |Quartz 

The USA Freedom Reauthorization Act grants federal authorities the ability to collect “tangible things” related to national security investigations without a warrant. Congressional Dems: New Surveillance Bill Strikes Right Balance |Jesse Marx |June 22, 2020 |Voice of San Diego 

Antoine himself had recently been arrested on a six-year-old warrant for a dime bag of weed. Ground Zero of the NYPD Slowdown |Batya Ungar-Sargon |January 1, 2015 |DAILY BEAST 

The risks are too high to warrant supporting public marriage proposals. Public Marriage Proposals Must Die |Tauriq Moosa |December 28, 2014 |DAILY BEAST 

Not enough black films are being made to warrant a piece of the pie. Ava DuVernay on ‘Selma,’ the Racist Sony Emails, and Making Golden Globes History |Marlow Stern |December 15, 2014 |DAILY BEAST 

Because Wright was a no-show in criminal court to face the loud music and pot bust he already had an outstanding warrant. The Navy ‘Hero’ Who Pimped an HIV-Positive Teen |M.L. Nestel |December 11, 2014 |DAILY BEAST 

Olga was on guard as always, and categorically refused to open the door unless the police produced a warrant. How Havel Inspired the Velvet Revolution |Michael Zantovsky |December 6, 2014 |DAILY BEAST 

De Robeck agrees that we don't know enough yet to warrant us in fault-finding or intervention. Gallipoli Diary, Volume I |Ian Hamilton 

He had no rest until the seals were fixed to parchment, and the warrant of his release appeared in public print. Blackwood's Edinburgh Magazine, No. CCCXXXIX. January, 1844. Vol. LV. |Various 

He has no authority to warrant the quality of property sold except custom or authority is expressly given to him. Putnam's Handy Law Book for the Layman |Albert Sidney Bolles 

The context in Chaucer does not seem to warrant the interpretation given by Tyrwhit. Notes and Queries, Number 177, March 19, 1853 |Various 

David had replied, in that short tone of self-sufficiency which conveys so much more than the syllable would seem to warrant. The Garret and the Garden |R.M. Ballantyne