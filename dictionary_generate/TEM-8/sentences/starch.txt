Try the starch to see if there’s any bitterness from the tannins, and if there’s not, discard the last bit of water. 5 Delicious Snacks You Can Forage in the City |Vanessa Hua |February 7, 2021 |Outside Online 

The bleaching process also raises the acidity of the batter, which allows the starch in the flour to absorb more moisture, further enhancing the cake’s tenderness. Four tips for bake-off worthy cakes |By Saveur |January 12, 2021 |Popular-Science 

The starches start cross-linking in a layer on the outside of the potato pieces. How to make crispy, golden potatoes, every time |Becky Krystal |December 18, 2020 |Washington Post 

It’s high in starch, with 24 grams of carbs per quarter-cup serving, which means it can bind doughs together and create a chewy texture. A Curious Eater's Guide to Alternative Flours |Christine Byrne |December 15, 2020 |Outside Online 

Scrape the starch into the bowl with the potatoes and gently mix all the ingredients to combine. This crisp, classic potato latke recipe delivers a satisfying, celebratory crunch |Olga Massov |November 30, 2020 |Washington Post 

Quite a few of these loafs use potato starch and tapioca starch in attempts to produce a lighter, fluffier product. How to Buy Gluten-Free Without Getting Duped |DailyBurn |April 12, 2014 |DAILY BEAST 

Sugar and lots of starch will give you a boost followed by a crash that will end your late night partying. Five Healthy—and Legal—Ways to Stay Awake Longer |Dave Asprey |December 4, 2013 |DAILY BEAST 

Making it with quinoa instead of rice gives you the wonderful taste and comfort of risotto without all that starch. Three Quinoa Recipes for Your Weekend Parties |Jane Coxwell |May 26, 2013 |DAILY BEAST 

Tsai: The ratio of vegetables to meat to starch is much higher. Five World-Famous Chefs Give Their Take on the Asian-Food Craze in America |Marlow Stern |April 29, 2012 |DAILY BEAST 

Starch-granules are recognized by their concentric striations and the fact that they stain blue with iodin solutions. A Manual of Clinical Diagnosis |James Campbell Todd 

Starch-granules sometimes retain their original form, but are ordinarily not to be recognized except by their staining reaction. A Manual of Clinical Diagnosis |James Campbell Todd 

Starch is insoluble in cold water, but by boiling, it dissolves, forming a thick paste. Elements of Agricultural Chemistry |Thomas Anderson 

Mandy Ann had put on her best frock, a white one, stiff with starch, and standing out like a small balloon. The Cromptons |Mary J. Holmes 

Have ready some very clear and weak gum-arabic water, or some thin starch, or rice-water. The Ladies' Book of Etiquette, and Manual of Politeness |Florence Hartley