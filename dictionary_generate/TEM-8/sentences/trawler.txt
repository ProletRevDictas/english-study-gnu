To meet growing global demand for salmon, huge trawlers pillage the fisheries off the coast of West Africa and Peru, robbing subsistence fishers of their livelihood and increasing food insecurity. 3 Reasons to Avoid Farmed Salmon |Douglas Frantz and Catherine Collins |July 21, 2022 |Time 

The other is industrial fisheries, including trawlers and distant water fishing fleets. Africa doesn’t reap the rewards of its $24 billion marine fisheries industry |Edward H. Allison |November 22, 2021 |Quartz 

Local trawlers and lobster boats will find themselves sharing their waterways with huge vessels hefting cranes and massive hydraulic jacks. U.S. Fishermen Are Making Their Last Stand Against Offshore Wind |Alejandro de la Garza |September 30, 2021 |Time 

From the bridge of the Arctic Sunrise, an old ice-breaking fishing trawler turned research vessel now plying the polar waters between Greenland and northern Norway, Laura Meller has an unparalleled view of our planet’s future. 'A Climate Emergency Unfolding Before Our Eyes.' Arctic Sea Ice Has Shrunk to Almost Historic Levels |Aryn Baker |September 21, 2020 |Time 

Late Monday night, the Allegra was joined by a French fishing trawler that answered an SOS call. Costa Allegra Stranded: Another Cruise From Hell |Barbie Latza Nadeau |February 28, 2012 |DAILY BEAST 

McIntyre was 32 years old when he took a job as an engineer on the Valhalla, a fishing trawler moored in Gloucester, Mass. Whitey Bulger's Victim Speaks Out Against FBI |Rikki Klieman |November 18, 2011 |DAILY BEAST 

Is the Prince kept prisoner on a trawler sweeping the North Sea for mines? Punch, or the London Charivari, Vol. 147, November 4, 1914 |Various 

Soon there struggled in the narrow mouth the shadow of a close-reefed trawler of sixty tons or so. Yachting Vol. 2 |Various. 

Besides, to each was attached a yacht, and a trawler which continually plied for it between island and land. The Lord of the Sea |M. P. Shiel 

The Falcon, strong-hearted trawler, was plunging towards the rock when the first line of gay bunting swung clear into the breeze. The Pillar of Light |Louis Tracy 

You can hear what speed he's going, and when you're used to them you can make out what kind of craft he is—trawler or destroyer. The Story of Our Submarines |John Graham Bower