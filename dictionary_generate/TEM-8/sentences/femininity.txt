She argues that regulations that exclude DSD athletes will also hurt female runners who don’t conform to conventional ideals of femininity. The Testosterone Debate Won’t End with Caster Semenya |mmirhashem |July 13, 2021 |Outside Online 

Every act of difference from society’s prescribed model of femininity had to be suppressed. Declared Insane for Speaking Up: The Dark American History of Silencing Women Through Psychiatry |Kate Moore |June 22, 2021 |Time 

In other words, there was nothing about the initial introduction that would have indicated that it would become a game really strongly associated with femininity and with women within a few years. What the Surprising History of Mah-jongg Can Teach Us About America |Cady Lang |May 4, 2021 |Time 

To these women, the military symbolized the ideal versions of masculinity and femininity. Men-only Selective Service registration may end soon, but the fight will remain |Heather Stur |April 15, 2021 |Washington Post 

For women, in particular, there is this idea that femininity is tied to your hair. A New Frontier to Fight This Hair-Loss Disease |Charu Kasturi |March 2, 2021 |Ozy 

One of the ways Fincher and his screenwriter Gillian Flynn accomplished this feat of “femininity” was to give Amy agency. Oral Sex Comes of Age in Hollywood: ‘Gone Girl’ the Latest Film to Showcase Female Pleasure |Marlow Stern |October 4, 2014 |DAILY BEAST 

Bratis, who trained in Athens, creates pieces of “femininity and pure elegance without artifice.” Is This the End of CR Fashion Book?; Armani Names New Protégé |The Fashion Beast Team |August 1, 2014 |DAILY BEAST 

The Big Apple influenced the industry by introducing a more practical look combined with the existing femininity. How World Wars Made Females More Androgynous |Liza Foreman |July 22, 2014 |DAILY BEAST 

To men, long hair on a woman is a turn-on because it signifies fertility and clichéd ideas of femininity. Goodbye Pixie Cut, Hello Lob: The Haircut Taking Over Hollywood |Erin Cunningham |June 16, 2014 |DAILY BEAST 

Once I was pregnant, I embraced my own femininity and settled into my role as decision maker and proprietor. Yes, Women Can Make Great Wine |Jordan Salcito |March 22, 2014 |DAILY BEAST 

Comes of living alone and making a success of it, I suppose, getting ahead of mere femininity and all the pettinesses of life. Ancestors |Gertrude Atherton 

We have exquisite types of femininity in Tuscany, said the young man, with patriotic ardor. The Fifth String |John Philip Sousa 

Mayo stared after her, wrinkling his forehead for a moment, as if he had discovered some new vagary in femininity to puzzle him. Blow The Man Down |Holman Day 

Her amazing fortitude at the time when he had looked for hysterics and collapse gave him new light on the enigma of femininity. Blow The Man Down |Holman Day 

At six o'clock in fluttered Elizabeth, a vision of elegant femininity in her soft furs and plumes and trailing skirts. And So They Were Married |Florence Morse Kingsley