The official talk is of art and nature in harmony, but in truth, both the art and the landscaping are trying to outmuscle nature, rework it, master it. Sculpture parks are a great way to see art during a pandemic. Here’s why some are better than others. |Sebastian Smee |February 11, 2021 |Washington Post 

Ruffin is seeking that harmony of humor in truth-telling amid despair, but it's not always easy. Late-night TV sensation Amber Ruffin and her sister co-wrote a book about racism. And, yes, it’s hilarious. |Hau Chu |February 8, 2021 |Washington Post 

That leaves this decision to the second husbands and wives — with the warning that, if they value family harmony, they will not adopt titles they have not earned. Miss Manners: Stop trying to please abusive parent |Judith Martin, Nicholas Martin, Jacobina Martin |January 27, 2021 |Washington Post 

We’ve seen parts of the world where neighbors have lived with each other in harmony, but there is always the danger that this will break down. Humans Have Gotten Nicer and Better at Making War - Issue 94: Evolving |Steve Paulson |January 6, 2021 |Nautilus 

Our experiments showed that Americans found people who conform to protect others’ feelings or to maintain group harmony to be warmer, more competent, and more authentic. How we can encourage people to wear masks — for others’ sake |Shai Davidai |December 24, 2020 |Vox 

I had to play melody while simultaneously playing harmony with him. The Stacks: John Coltrane’s Mighty Musical Quest |Nat Hentoff |October 18, 2014 |DAILY BEAST 

As a young baby, her mother would sing her lullabies and “get totally freaked out” when she started singing them back in harmony. ‘All About That Bass’ Singer Meghan Trainor On Haters and Her Polarizing (and Unlikely) No. 1 Hit |Marlow Stern |October 7, 2014 |DAILY BEAST 

Low-mass black holes “sing” in harmony with themselves, though with flashes of light instead of sound. The Goldilocks of Black Holes |Matthew R. Francis |August 24, 2014 |DAILY BEAST 

Soon cover versions were recorded by acts like Bone Thugs-N-Harmony, Dope, and Rage Against the Machine. A Brief History of the Phrase 'F*ck the Police' |Rich Goldstein |August 23, 2014 |DAILY BEAST 

He wanted peace and harmony, and in this respect he was just another Roman ruler interested in imperial unity. Plotting Nicea III Could Be Pope Francis's Masterstroke |Candida Moss |June 8, 2014 |DAILY BEAST 

They are so rich in harmony, so weird, so wild, that when you hear them you are like a sea-weed cast upon the bosom of the ocean. Music-Study in Germany |Amy Fay 

Much later, in the case of all but gifted children, do the mysteries of harmony begin to take on definite form and meaning. Children's Ways |James Sully 

There was acute disharmony in the room, where a little time before there had been at least an outward show of harmony. The Wave |Algernon Blackwood 

The little glimpse of domestic harmony which had been offered her, gave her no regret, no longing. The Awakening and Selected Short Stories |Kate Chopin 

In harmony with a fundamental rule of law, a member who has once been acquitted cannot be tried again for the same offense. Putnam's Handy Law Book for the Layman |Albert Sidney Bolles