A few weeks later, though, on April 2, Luckin came clean, fessing up to $310 million in made-up money inflows—a large portion of its reported revenue for 2019. The biggest business scandals of 2020 |Lee Clifford |December 27, 2020 |Fortune 

Alarmingly, this means 50 percent of small businesses would struggle beyond two weeks of disrupted cash inflows. How to Succeed in Small Business |Daniel Malloy |October 23, 2020 |Ozy 

Since Tesla’s credits and its customers’ deficits run in tandem, Tesla trades its CAFE and GHG credits on long-term contracts that produce more consistent, though in the past smaller, revenue than the inflow from lumpy ZEV sales. The next President will hold a lot of sway over Tesla’s biggest profit center |Shawn Tully |October 11, 2020 |Fortune 

Net equity market inflows were 93 billion Rmb for the same period, taking foreign holdings to more than 1 trillion Rmb. Buying Spree Boosts China’s Currency on the World Stage |Daniel Malloy |October 8, 2020 |Ozy 

We kitted up and passed through a special set of double doors designed to reduce the inflow of contaminant-filled air from outside. The Fungal Evangelist Who Would Save the Bees - Issue 90: Something Green |Merlin Sheldrake |September 23, 2020 |Nautilus 

If the pools lose their inflow of circulating cooling water, the water in the pools will evaporate. The Japan Nuke Problem No One's Talking About |Sharon Begley |March 14, 2011 |DAILY BEAST 

Iraq is now beginning to see an inflow of investment capital that is likely to grow. Is Iraq Shifting to Iran? |Gary Sick |November 16, 2009 |DAILY BEAST 

Exercises to strengthen muscles, promote complete expansion, regulate inflow and outflow of air, etc. Voice Production in Singing and Speaking |Wesley Mills 

With that cool, fragrant inflow of air they breathed freely. The Three Partners |Bret Harte 

The cablegrams came first—bundles of them from every corner of the world—then the letters, a steady inflow. Mark Twain, A Biography, 1835-1910, Complete |Albert Bigelow Paine 

The train at this point goes upon a monster ferry that carries it over the swift inflow of the Sacramento river. The Road of a Thousand Wonders |Passenger Dept. Southern Pacific Co 

It depended for its very life on a steady inflow of food from outside sources. Harper's Pictorial Library of the World War, Volume XII |Various