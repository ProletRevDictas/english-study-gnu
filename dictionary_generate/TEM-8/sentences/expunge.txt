Last week, state lawmakers approved a measure that would automatically expunge certain offenses from a juvenile’s record, a move they already had approved for adults. Judges Are Locking Up Children for Noncriminal Offenses Like Repeatedly Disobeying Their Parents and Skipping School |by Jodi S. Cohen and Duaa Eldeib |December 22, 2020 |ProPublica 

Even then, the data might not contain entries for defendants who had their records expunged, which would happen for those who successfully complete pretrial intervention, a program for first-time offenders. What Happens to New Jersey Officers Charged With Official Misconduct? We Gathered the Cases to Find Out. |by Andrew Ford, Asbury Park Press |September 23, 2020 |ProPublica 

The city attorney’s office has also said it wouldn’t fight anyone’s efforts to have the charge expunged from their record. 5 Big Questions About How SDPD Used the Seditious Language Law and What’s Next |Sara Libby |September 21, 2020 |Voice of San Diego 

“Our office would not stand in the way of anyone’s effort to have this charge expunged from their record,” Nemchik wrote. Those Ticketed for Seditious Language Say Their Only Crime Was Talking Back |Kate Nucci |September 9, 2020 |Voice of San Diego 

Even if a person goes free, his or her personal data remains listed among criminal records unless special steps are taken to expunge it. The Bias in the Machine - Issue 89: The Dark Side |Sidney Perkowitz |August 19, 2020 |Nautilus 

King: We must expunge from our society the myths and half-truths that engender such groundless fears as these. Alex Haley’s 1965 Playboy Interview with Rev. Martin Luther King Jr. |Alex Haley |January 19, 2014 |DAILY BEAST 

He would do well to expunge every double-breasted suit from his wardrobe. Herman Cain's Power Suit |Robin Givhan |November 4, 2011 |DAILY BEAST 

If the purge was intended simply to expunge the opposition, then Papen should have been the first to go. A Witness to Hitler's Rise |Zachary Shore |May 27, 2011 |DAILY BEAST 

On February 25 a motion was proposed and carried to expunge the entry of the vote of thanks. Private Letters of Edward Gibbon (1753-1794) Volume 1 (of 2) |Edward Gibbon 

I either soften or expunge many villanous, seditious Whig strokes, which had crept into it. Life and Correspondence of David Hume, Volume II (of 2) |John Hill Burton 

Expunge from the Bible all record of actual revelation and reference thereto, and what remains? The Vitality of Mormonism--Brief Essays |James E. Talmage 

It is to expunge from your statute-book all support of Slavery. Charles Sumner; His Complete Works, Volume XI (of 20) |Charles Sumner 

It is perhaps well that we should expunge the word absolute from our vocabularies. The Mystery of Space |Robert T. Browne