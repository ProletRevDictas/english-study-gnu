Jason and Lizzie Kling, 34, learned to read and speak Spanish with their mom by reading verses of Jesus’s teachings in the Spanish version of the Bible. Blanca Kling, a pillar in the Latino community who helped thousands of crime victims, dies of complications of covid-19 |Luz Lazo |February 5, 2021 |Washington Post 

That he invited everyone—even those of us at home, which was most of us—to sing the final verse was yet another kind of reaching out. Inauguration Musical Performances Are Tricky. But Lady Gaga, Jennifer Lopez and Garth Brooks Did Exactly What We Needed Them to Do |Stephanie Zacharek |January 20, 2021 |Time 

Keep distribution opportunities top of mindIt’s easier than most publishers think for posted articles to disappear into the content-verse, generating zero sales quickly. Five commerce strategies every publisher should consider in 2021 |StackCommerce |January 19, 2021 |Digiday 

A favorite biblical verse from the Book of Colossians commands, “Whatever you do, work at it with all your heart, as working for the Lord, not for human masters.” Who is Intel’s new CEO, Pat Gelsinger |Aaron Pressman |January 13, 2021 |Fortune 

That would incentivize bringing in nurses and doctors who are versed in preventive care, and could keep down resident fees, she said. COVID-19 Exposes Lack of Medical Staff in Assisted Living Facilities |Jared Whitlock |September 28, 2020 |Voice of San Diego 

It needs to be said: bigotry in the name of religion is still bigotry; child abuse wrapped in a Bible verse is still child abuse. Dear Leelah, We Will Fight On For You: A Letter to a Dead Trans Teen |Parker Molloy |January 1, 2015 |DAILY BEAST 

No more allowing people to justify their bigotry by spouting a cherry-picked Bible verse. Dear Leelah, We Will Fight On For You: A Letter to a Dead Trans Teen |Parker Molloy |January 1, 2015 |DAILY BEAST 

I know the verse because Mrs. Bertalan used to have us do it in ninth-grade choir. Yes, I Like Christmas Music. Stop Laughing. |Michael Tomasky |December 24, 2014 |DAILY BEAST 

Shortly thereafter, T.I. lent his first post-incarceration verse to a remix of “Magic.” Future Makes Us Rethink Everything We Thought We Knew About Rap Artists |Luke Hopping |December 15, 2014 |DAILY BEAST 

I have seen the ugliest thoughts expressed, sometimes in verse, while using public restrooms. Blurred Lines at NY Sketchbook Museum |Daniel Genis |November 1, 2014 |DAILY BEAST 

As the next verse is the last you needn't trouble yourself to make any further observations. Davy and The Goblin |Charles E. Carryl 

Did he at all intrench upon your Sovereignty in Verse, because he had now and then written a Comedy that succeeded? A Letter from Mr. Cibber to Mr. Pope |Colley Cibber 

I am of opinion too, that the Indecency of the next Verse, you spill upon me, would admit of an equal Correction. A Letter from Mr. Cibber to Mr. Pope |Colley Cibber 

Only he had carved on the Girl's tombstone the last verse of the Song of the Girl, which stands at the head of this story. Kipling Stories and Poems Every Child Should Know, Book II |Rudyard Kipling 

Carmina de clo possunt de duecere lunam.Pale Phbe, drawn by verse, from heaven descends. A Philosophical Dictionary, Volume 1 (of 10) |Franois-Marie Arouet (AKA Voltaire)