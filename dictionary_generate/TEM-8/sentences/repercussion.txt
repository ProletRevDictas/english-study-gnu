Worse, these women are often deterred from reporting such abuses, due to personal threats, fears of professional repercussions, and the likelihood that those who could help them seek recourse will not believe them. These 10 female journalists deserve justice immediately |Brett Haensel |October 1, 2020 |Fortune 

Disney probably understands the consequences of failing to think about the repercussions of efforts to build its audience in China better than anyone. Netflix’s “Three-Body Problem” has to figure out how to not be the next “Mulan” |Jane Li |September 25, 2020 |Quartz 

In San Diego, tourism-related businesses are taking a tremendous hit, and it is likely there will be ongoing repercussions long after restrictions are lifted. Myths and Shame Shouldn’t Guide Cannabis Regulations |John Bertsch |September 8, 2020 |Voice of San Diego 

The Madden-Julian Oscillation is a pattern of storms that usually forms several times a year in tropical latitudes and can have weather repercussions around the globe. Improved three-week weather forecasts could save lives from disaster |Alexandra Witze |August 27, 2020 |Science News 

The fact that a “no match” from a human investigator can overturn a wrong machine identification should be reassuring, but that came too late to save Williams from false arrest and its repercussions. The Bias in the Machine - Issue 89: The Dark Side |Sidney Perkowitz |August 19, 2020 |Nautilus 

The proposed law would allow men to abuse their wives, children, and sisters without threat of judicial repercussion. Legalized Spousal Abuse Is Coming to Afghanistan |Nina Strochlic |February 13, 2014 |DAILY BEAST 

But we can't live in a world where citizens are allowed to do what he's done without repercussion. Snowden Is a Spy |Michael Tomasky |June 24, 2013 |DAILY BEAST 

Anything done to the Double acts by repercussion upon the physical body. Three More John Silence Stories |Algernon Blackwood 

They're the most curious scars in the world, these scars transferred by repercussion from an injured Double. Three More John Silence Stories |Algernon Blackwood 

When the air around was no longer shaken by constant repercussion, Bobby fell asleep. The Bronze Eagle |Emmuska Orczy, Baroness Orczy 

This is a specimen of the "repercussion" stories, in which the wound inflicted on the wer-animal appears in the human form. Human Animals |Frank Hamel 

We feel the repercussion of his anguish when death was imminent for alleged participation in a nihilistic conspiracy. Ivory Apes and Peacocks |James Huneker