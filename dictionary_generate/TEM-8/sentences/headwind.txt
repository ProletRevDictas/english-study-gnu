McCauley, though, professes not to worry about regulatory headwinds. In a first for the crypto industry, Visa-backed Anchorage gets a federal bank charter |Jeff Roberts |January 13, 2021 |Fortune 

Despite remaining headwinds, the future is looking up for most cannabis businesses, according to these investors. These 5 VCs have high hopes for cannabis in 2021 |Matt Burns |January 12, 2021 |TechCrunch 

And, reflation, in general, is a headwind for growth stocks. As an impeachment vote gains momentum, the markets tick higher |Bernhard Warner |January 12, 2021 |Fortune 

He lowered the plane out of the speedy headwinds, hoping to save some fuel. A CIA spyplane crashed outside Area 51 a half-century ago. This explorer found it. |Sarah Scoles |January 5, 2021 |Popular-Science 

Under no scenario was the effort ever going to be conducted without headwinds. In the COVID vaccine rollout, our expectations don’t match reality |matthewheimer |January 4, 2021 |Fortune 

But we do not live in that world, and that is a headwind pushing against currents of balance, growth, and repair. Why Markets Freak Out |Zachary Karabell |August 12, 2011 |DAILY BEAST 

Webb—or any Virginia Democrat—would be running into quite a headwind in 2012. Why the Dems Could Lose the Senate Next |Samuel P. Jacobs |November 16, 2010 |DAILY BEAST 

Ascending steadily against a continuous headwind, we picked up the second sledge at midday on the 28th. The Home of the Blizzard |Douglas Mawson 

Travelling between the cataracts against a strong headwind was slow work and we longed for the next one to get along faster. Our Caughnawagas in Egypt |Louis Jackson 

The men at the oars now made hard work of it against the headwind and the running sea. Frontier Boys on the Coast |Capt. Wyn Roosevelt 

Next morning we set out, slowly floating with a little headwind, through a fog. The houseboat book |William F. Waugh 

With a cold headwind on the starboard quarter, we hug the lee of the Ohio shore. Afloat on the Ohio |Reuben Gold Thwaites