If nature offers an easy way to transfer organelles between plants, biotechnology researchers can put it to work in creating desirable new crop species. Plant Cells of Different Species Can Swap Organelles |Viviane Callier |January 20, 2021 |Quanta Magazine 

Learning to store and manipulate data using the same language as nature could also open the door to a host of new capabilities in biotechnology. New Research Could Enable Direct Data Transfer From Computers to Living Cells |Edd Gent |January 11, 2021 |Singularity Hub 

The biotechnologies that made Covid-19 mRNA vaccines are here to stay. Fighting Covid-19 Brought These Lasting Breakthroughs to Science and Medicine |Shelly Fan |December 29, 2020 |Singularity Hub 

Co-author Noemi Procopio has been interested in forensic science since she was 14, but initially studied biotechnology because her home country of Italy didn't have forensic science programs. Scientists ID potential biomarkers to peg time of death for submerged corpses |Jennifer Ouellette |December 26, 2020 |Ars Technica 

The decision follows a thumbs-up vote from a panel of experts that convened on December 17 to discuss vaccine data that the biotechnology company had collected from its ongoing clinical trial. How does the newly authorized Moderna COVID-19 vaccine compare to Pfizer’s? |Erin Garcia de Jesus |December 19, 2020 |Science News 

There have been two so far this year, including a biotechnology firm raising about $8 million. America’s Start-Up Silver Lining |Daniel Gross |October 30, 2013 |DAILY BEAST 

She went on to get a Ph.D. in biotechnology from Cambridge in 2001. Rebel With a Cause |Abigail Pesta |October 21, 2011 |DAILY BEAST 

Now they have their gaze fixed on biotechnology and videogames. The G-20's New Balance of Power |Joel Kotkin |June 25, 2010 |DAILY BEAST 

But other scientists counter that basic skills in microbiology and biotechnology can get you a bioweapon. The Coming Bioattack |Stephan Talty |June 5, 2009 |DAILY BEAST 

Much of our job growth will be found in high-skilled fields like health care and biotechnology. Complete State of the Union Addresses from 1790 to 2006 |Various