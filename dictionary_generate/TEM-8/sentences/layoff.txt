“We started to see how women were being disproportionately affected by layoffs, furloughed and being forced out of work to take care of their kids,” she said. Why Brit + Co wants to help women create companies to grow its own consumer revenue |Kayleigh Barber |February 5, 2021 |Digiday 

In addition to the layoffs at Disney, NBCUniversal and WarnerMedia last year, NBCUniversal plans to shut down its TV sports network NBCSN by the end of this year. Future of TV Briefing: Hollywood returns to production as stay-at-home orders, advisories lift |Tim Peterson |February 3, 2021 |Digiday 

Profitability may have been out of reach if it weren’t for the layoffs that these companies underwent. Media Briefing: The media industry’s top trends at the moment |Tim Peterson |January 28, 2021 |Digiday 

Thousands of Chase branches reduced hours in mid-March, with 1,000 closing immediately — some of which have shuttered for good since, as the bank reportedly conducted layoffs. A Consumer Banking Rock Star Makes a Bold Bet |Nick Fouriezos |January 24, 2021 |Ozy 

The ballot measure’s passage has already led to layoffs in the state, and with delivery services adding fees that they previously threatened would only happen if Prop 22 didn’t pass. Who’s Paying for the Great Delivery Wars? |Jaya Saxena |January 21, 2021 |Eater 

The gaming site plans to layoff 18 percent of its workforce and shut several offices. Ex–Zynga Employees Tell All on Reddit |CNBC |June 6, 2013 |DAILY BEAST 

Unemployment claims are trending down, and the number of mass layoff events is declining. Banks Are Thriving Despite Regulations Thanks to Economic Growth |Daniel Gross |May 30, 2013 |DAILY BEAST 

You also knew the libs (including the Super PAC I advised, Priorities USA Action) would zero-in on his record as a layoff artist. Paul Begala on the Five Stages of GOP Grief |Paul Begala |November 7, 2012 |DAILY BEAST 

Neither presidential campaign responded to a request for comment about the layoff announcement. Lockheed Martin’s Layoff Notices: An Empty Threat? |Alex Klein |June 27, 2012 |DAILY BEAST 

Layoff seems to be the most commonly used word despite—or maybe because of—a passivity that cheats the impact of the experience. The Unemployed Finally Speak Out: D.W. Gibson’s ‘Not Working’ |D.W. Gibson |June 19, 2012 |DAILY BEAST 

Come to think of it, Ernie didn't know there was going to be a layoff. All Day Wednesday |Richard Olin 

After this morning, Rogers would post him for the layoff for sure. All Day Wednesday |Richard Olin 

Show them that your layoff hasnt hurt your batting eye, Larry, sang out McRae. Baseball Joe, Home Run King |Lester Chadwick 

Would he come back to the farm if this ten day layoff were extended, or would he catch a train for Chicago? Plowing On Sunday |Sterling North