Most temperate forests, which exist at lower latitudes in both the Northern and Southern hemispheres, lack the carbon-rich peat and soil that fuel zombie fires. Readers discuss the 1921 Tulsa race massacre, zombie fires and more |Science News Staff |August 8, 2021 |Science News 

This suggests some of the peat carbon must instead have gone back into the land as plants grew, according to the researchers. An enormous missing contribution to global warming may have been right under our feet |Chris Mooney |June 4, 2021 |Washington Post 

If humans cut into a lot of peat historically, then another way to balance the carbon budget is to assume they did less deforesting, a process that also results in emissions. An enormous missing contribution to global warming may have been right under our feet |Chris Mooney |June 4, 2021 |Washington Post 

In its normal state, peat slowly pulls carbon out of the atmosphere — unless you disturb it. An enormous missing contribution to global warming may have been right under our feet |Chris Mooney |June 4, 2021 |Washington Post 

Massive losses of tropical peat are even now occurring in countries like Indonesia and Malaysia, for instance, so global losses will be higher. An enormous missing contribution to global warming may have been right under our feet |Chris Mooney |June 4, 2021 |Washington Post 

Two days later, on January 5, 2012, MC Peat Co LLP took out a loan in the amount of £2.73 million, or $4.5 million. Britain’s KGB Sugar Daddy |Michael Weiss |March 7, 2014 |DAILY BEAST 

Charlie Peat denied to me by email that his investment house was lent any money from Roman Abramovich. Britain’s KGB Sugar Daddy |Michael Weiss |March 7, 2014 |DAILY BEAST 

Is the fact that it worked in Norway a good reason to give peat moss a try? 11 Extreme Oil Spill Solutions |The Daily Beast |May 12, 2010 |DAILY BEAST 

He had nearly bitten his swollen tongue in two falling over an unseen peat-cutting, and blood-flecked foam gathered on his lips. Uncanny Tales |Various 

It must be carefully composted with peat, and turned over several times before being used. Elements of Agricultural Chemistry |Thomas Anderson 

It is sometimes mixed with lime or gypsum, and dried with heat, and sometimes with animal charcoal or peat charcoal. Elements of Agricultural Chemistry |Thomas Anderson 

Dry peat of good quality contains about one per cent of nitrogen, and a quantity of ash varying from five to twenty per cent. Elements of Agricultural Chemistry |Thomas Anderson 

A stuffy hole, full of peat-smoke, and with a window that can't open at the best of times. The Daisy Chain |Charlotte Yonge