Just a few seconds on the receiving end of a harangue from such a fellow, whether at a surf break or the crag or the skin track, is enough to ruin an otherwise lovely day. Who Has the Right of Way on the Skin Track? |Sundog |March 26, 2021 |Outside Online 

Watch him unleash a magnificent, expletive-ridden rant—and be grateful for the Internet, where this harangue will live forever. 11 More Epic Anchor Flubs (Video) |The Daily Beast Video |December 24, 2013 |DAILY BEAST 

He used to harangue any of the young men in Homs not participating in protests, recalled Moutlak. A Syrian Rebel’s Firsthand Report on the Fighting and Bombing in Homs |Katie Paul |February 28, 2012 |DAILY BEAST 

The occasion was a gala dinner during which Pinter began to harangue some unfortunate guest for his political views. Antonia Fraser on Her Wild Marriage |Amanda Foreman |November 8, 2010 |DAILY BEAST 

I half-expected him to barnstorm out in riding boots and harangue us, Mussolini-style, underlit from a plinth. Obama Frees His Mojo |Tina Brown |September 10, 2009 |DAILY BEAST 

The last thing an incoming administration needs is to stage a long diplomatic harangue about nothing, inside an echo chamber. How Obama Can Restore American Idealism |Ted Widmer |November 9, 2008 |DAILY BEAST 

Il avoit sa belle robe soubs soy (car c'estoit en est), et se preparoit sa harangue funebre. The Jesuit Relations and Allied Documents, Vol. II: Acadia, 1612-1614 |Various 

La harangue devoit finir en l'adieu et comploration commune de tous. The Jesuit Relations and Allied Documents, Vol. II: Acadia, 1612-1614 |Various 

Voyez l'efficace du sacrement: le lendemain matin, il mande M. de Biancourt et moy, et de nouveau il recommence sa harangue. The Jesuit Relations and Allied Documents, Vol. II: Acadia, 1612-1614 |Various 

There now is Sullivan, in a long harangue, following you in a determined opposition to our petition to the King. The Eve of the Revolution |Carl Becker 

In defiance of all decency, he went to Westminster Hall, demanded a hearing, and pronounced a harangue against standing armies. The History of England from the Accession of James II. |Thomas Babington Macaulay