“I think engaged citizens are good for the republic,” he said. In Florida, the Gutting of a Landmark Law Leaves Few Felons Likely to Vote |by Lawrence Mower and Langston Taylor, Miami Herald and Tampa Bay Times |October 7, 2020 |ProPublica 

Last night’s “presidential debate”—less debate than headache, and far from presidential—set a high-water mark for the division that threatens to drown this pandemic-wracked republic. There is no such thing as ‘apolitical culture’ |rhhackettfortune |September 30, 2020 |Fortune 

In the early years of the republic, the Senate overrepresented the slower-growing South, and many political battles were fought over the admission of new states that could shift the balance between North and South. America needs a democratic revolution |Matthew Yglesias |September 17, 2020 |Vox 

It’s the only way to honor those we have lost, to uphold the ideals this country is supposed to rest on, and to build a stronger, fairer republic for everyone. Levi Strauss CEO: We can’t solve racial inequality if gun violence and voter disenfranchisement persist |jakemeth |September 1, 2020 |Fortune 

At times it appears they are trying to turn the United States into a banana republic or the next Belarus. Be optimistic but work your ass off |Peter Rosenstein |August 20, 2020 |Washington Blade 

An examination of the complicated history of America and its movies in the Republic of Korea. Propaganda, Protest, and Poisonous Vipers: The Cinema War in Korea |Rich Goldstein |December 30, 2014 |DAILY BEAST 

The Second Republic was also considered the another golden age for Korean Cinema. Propaganda, Protest, and Poisonous Vipers: The Cinema War in Korea |Rich Goldstein |December 30, 2014 |DAILY BEAST 

To the Republic of Korea and United States military personnel stationed in the JSA, it is known as Propaganda Village. Propaganda, Protest, and Poisonous Vipers: The Cinema War in Korea |Rich Goldstein |December 30, 2014 |DAILY BEAST 

Not so lucky are the editors and writers at The New Republic. The Novel That Foretold the TNR Meltdown |Nicolaus Mills |December 22, 2014 |DAILY BEAST 

A hard look at campus rape statistics, the collapse of The New Republic and the day John Lennon died. The Daily Beast’s Best Longreads, Dec 8-14, 2014 |William Boot |December 13, 2014 |DAILY BEAST 

Gallinas, the noted slave factory on the west of Africa, purchased by the Liberian republic. The Every Day Book of History and Chronology |Joel Munsell 

Enchanted with the golden period of the Grecian republic, I passed over the storms by which it had been agitated. Madame Roland, Makers of History |John S. C. Abbott 

Recognition of the Philippine Republic as soon as the difficulties with America should be overcome. The Philippine Islands |John Foreman 

Recognition of a Philippine Republic would have been in direct opposition to the spirit of the treaty of peace. The Philippine Islands |John Foreman 

The time had been when the proclamation of a republic would have filled her soul with inexpressible joy. Madame Roland, Makers of History |John S. C. Abbott