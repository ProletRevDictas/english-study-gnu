Making this type of fake persuasive was the task of Ryan Laney, a visual effects artist who worked on the 2020 HBO documentary Welcome to Chechnya. Inside the strange new world of being a deepfake actor |Karen Hao |October 9, 2020 |MIT Technology Review 

Importantly, this was because it had not been able to provide “exceedingly persuasive justification” for making distinctions on the basis of sex. Ruth Bader Ginsburg Forged A New Place For Women In The Law And Society |LGBTQ-Editor |September 23, 2020 |No Straight News 

It is political speech, at times persuasive, at times not, that is made all the more common by weak democratic institutions. Kenosha’s looting is a symptom of a decrepit democracy |Aaron Ross Coleman |September 4, 2020 |Vox 

You can hear how both pulled all the stops to try and be persuasive using everything from homemade cupcakes to bottles of vodka. Power SEO Friendly Markup With HTML5, CSS3, And Javascript |Detlef Johnson |August 20, 2020 |Search Engine Land 

The second is offering additional information, conveying a secondary message that is persuasive. Studying the anatomy of a successful high-conversion landing page |Yasmine Dehimi |June 22, 2020 |Search Engine Watch 

The economic argument for the royals might be persuasive: They bring tourism to Britain. The Cult of Royal Porn |Tim Teeman |April 26, 2014 |DAILY BEAST 

The argument is boilerplate Al Qaeda, but many people in developing countries, Muslims and non-Muslims alike, find it persuasive. Death Squads in Kenya’s Shadow War on Shabaab Sympathizers |Margot Kiser |April 6, 2014 |DAILY BEAST 

As it does, the sinister interpretations of what happened in the cockpit of the Boeing 777 become less persuasive. Malaysia’s Sinister Timeline for Flight 370 Unravels |Clive Irving |March 18, 2014 |DAILY BEAST 

Malaysian leaders have turned the airmen into scapegoats without a single persuasive fact. The Baseless Rush to Blame the Pilots of Flight 370 |Clive Irving |March 16, 2014 |DAILY BEAST 

Some studies even suggest that low mood can improve skill in persuasive argument and sharpen memory. How Depression Could Save Your Life |Nick Romeo |March 4, 2014 |DAILY BEAST 

These committees within the various colonies became very active and persuasive. Hallowed Heritage: The Life of Virginia |Dorothy M. Torpey 

There was a persuasive smile on his lips and in his keen eyes which the monk, almost unconsciously, obeyed. St. Martin's Summer |Rafael Sabatini 

It is then that human speech, losing in a measure its terrestrial nature, becomes persuasive and convincing. Superstition In All Ages (1732) |Jean Meslier 

His persuasive powers of appeal, and his straight, direct way of argument, commended him to his comrades. The Underworld |James C. Welsh 

A better weapon than his waspish tongue was Parpon's voice, for it, before all, was persuasive. When Valmond Came to Pontiac, Complete |Gilbert Parker