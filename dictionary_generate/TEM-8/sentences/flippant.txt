The original films, meanwhile, had a bit more of a flippant edge to them, and they never really tried to be heartwarming in the way that Afterlife is. How Ghostbusters: Afterlife Fits Into the Long-Running Franchise |James Grebey |November 17, 2021 |Time 

They were flippant quotes that effectively handed their opponents an issue to beat into the ground. The two quotes that defined Democrats’ bad Election Day |Aaron Blake |November 5, 2021 |Washington Post 

This has manifested in sometimes flippant, sometimes cruel reactions to the high rates of infection and death among unvaccinated Americans. Where Breitbart’s False Claim That Democrats Want Republicans To Stay Unvaccinated Came From |Kaleigh Rogers |October 1, 2021 |FiveThirtyEight 

To say it’s wrong would be as flippant as the people spraining their thumbs with their flurry of tweets. Why Does Everybody Hate ‘Ted Lasso’ All of a Sudden? |Kevin Fallon |August 27, 2021 |The Daily Beast 

At the risk of sounding unduly flippant, I want to put in a good word for books that contribute to what Samuel Johnson called “the gaiety of nations” and “the public stock of harmless pleasure.” Cheer yourself up with light comedies from another era |Michael Dirda |March 31, 2021 |Washington Post 

Rangel was frequently flippant on the campaign trail - once pulling out an iPad to answer a question during a televised debate. Charlie Rangel Dances On |Gideon Resnick |June 25, 2014 |DAILY BEAST 

When the editor of Outside magazine, Alex Heard, tweeted that I had made an ass of myself, my response was arrogant and flippant. I Was Deluded to Believe Lance Armstrong When He Denied Doping |Buzz Bissinger |January 14, 2013 |DAILY BEAST 

Meanwhile, the real Angela Merkel was predictably less flippant than her phony Twitter doppelgänger. The EU Won What?! Europe Reacts to Nobel |Tracy McNicoll |October 12, 2012 |DAILY BEAST 

Ali Gharib said after the fact that he realized the comments were flippant and irresponsible. Intramural War of Words Raises Question of Who Loves Israel More |Linda Killian |January 6, 2012 |DAILY BEAST 

He seems implacably bespectacled—admonitory even in his flippant asides. Blame the Messenger |Lee Siegel |August 17, 2009 |DAILY BEAST 

Hadria was incorrigibly flippant about the banishment of important local subjects. The Daughters of Danaus |Mona Caird 

Lucian attaches an intelligible meaning to these flippant expletives, and represents Socrates as justifying their use. A Cursory History of Swearing |Julian Sharman 

He had fired up on one occasion when Professor Theobald said something flippant about Mrs. Temperley. The Daughters of Danaus |Mona Caird 

Her mien was quite serious, but her tone was sprightly—even flippant. The Secret Witness |George Gibbs 

That flippant remark broke the tension and the driver climbed gingerly out and viewed the bare hub. Left Tackle Thayer |Ralph Henry Barbour