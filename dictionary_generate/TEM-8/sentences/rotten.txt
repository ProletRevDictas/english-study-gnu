Nonetheless, Win at All Costs often feels less like an exposé than an attempt to fuse previously published reporting into a macro-narrative about how there’s something rotten in the state of Beaverton. A New Book on Nike Pulls No Punches |Martin Fritz Huber |October 2, 2020 |Outside Online 

At Bumpass Hell in California’s Lassen Volcanic National Park, the ground is literally boiling, and the aroma of rotten eggs fills the air. Life on Earth may have begun in hostile hot springs |Jack J. Lee |September 24, 2020 |Science News 

They let our ancestors “sniff out rotten food or potential poisons,” she explains. Stinky success: Scientists identify the chemistry of B.O. |Alison Pearce Stevens |September 15, 2020 |Science News For Students 

THL is not just a single bad apple but part of an expansive industry that’s rotten to its core. Why the Democratic Party must make a clean break with Wall Street |matthewheimer |September 8, 2020 |Fortune 

Reports out of Los Angeles indicate mail delays have led to rotten food and even dead animals. Info About Local Post Office Operations Is Conflicting and Hard to Come By |Ashly McGlone and Kate Nucci |August 27, 2020 |Voice of San Diego 

We are a nation in which a few rotten apples are spoiling different barrels. Obama Is Right on Race. The Media Is Wrong. |Keli Goff |December 29, 2014 |DAILY BEAST 

It has grown from a rotten root—striving to replace human judgment with detailed dictates. Red Tape Is Strangling Good Samaritans |Philip K. Howard |December 27, 2014 |DAILY BEAST 

Which to me, after the initial explosion of the Sex Pistols, always made Rotten kind of boring. The Rancid Ballad of Johnny Rotten: His Memoir Seethes With Anger—And Charm |Legs McNeil |November 20, 2014 |DAILY BEAST 

“I believe we are in the hour of the debacle of the institutions, they cannot be any more rotten,” said Padre Goyo. Mexico’s Holy Warrior Against the Cartels |Jason McGahan |November 18, 2014 |DAILY BEAST 

Yeonmi had been hospitalized at the time for a stomach illness, likely from her diet of rotten potatoes. How ‘Titanic ’Helped This Brave Young Woman Escape North Korea’s Totalitarian State |Lizzie Crocker |October 31, 2014 |DAILY BEAST 

But this alliance is rotten, and cannot endure; the Western men are no partizans of slavery. Blackwood's Edinburgh Magazine, Volume 60, No. 372, October 1846 |Various 

Sounds rotten, but that's their style; and you've been through the mill at home enough to know what it is to be knifed socially. Raw Gold |Bertrand W. Sinclair 

Clodd tells us that one cubic inch of rotten stone contains 41 thousand million vegetable skeletons of diatoms. God and my Neighbour |Robert Blatchford 

It is like the eating of a smothered fire into rotten timber in that it is noiseless and without haste. Kipling Stories and Poems Every Child Should Know, Book II |Rudyard Kipling 

That we should attack one week and the French another week is rotten tactically; but, practically, we have no option. Gallipoli Diary, Volume I |Ian Hamilton