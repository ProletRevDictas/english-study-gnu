Companies like MycoWorks and Modern Meadow both have alternative leather products in the works. Bolt Threads partners with Adidas, owners of Balenciaga and Gucci, and Stella McCartney on mushroom leather |Jonathan Shieber |October 2, 2020 |TechCrunch 

Barton also makes bands in leather and canvas so you can mix up band material if you like. Smartwatch accessories to give your high-tech friends and family |PopSci Commerce Team |October 1, 2020 |Popular-Science 

There’s a cherry red version for $50 more, and a $400 leather case in case you want to make your consumption even more conspicuous. Teenage Engineering’s OB-4 ‘magic radio’ is a weird and beautiful wireless speaker |Devin Coldewey |October 1, 2020 |TechCrunch 

The retro design of these headphones is an immediate attention-grabber—the brown leather and steel combination is an absolute winner. Affordable headphones that make the perfect gift |PopSci Commerce Team |September 28, 2020 |Popular-Science 

This leather-bound journal is the perfect gift for those writers that are longing to explore a different era. These notebooks are excellent gifts |PopSci Commerce Team |September 28, 2020 |Popular-Science 

With every stroke, her leather boot creaked under the weight of her leg. Dungeons and Genital Clamps: Inside a Legendary BDSM Chateau |Ian Frisch |December 20, 2014 |DAILY BEAST 

She tugged on the black rope that wrapped around his thighs and torso, her leather gloves creaking with each adjustment. Dungeons and Genital Clamps: Inside a Legendary BDSM Chateau |Ian Frisch |December 20, 2014 |DAILY BEAST 

Or there he is, matching leather jackets with a baby-faced Bruce Springsteen. ‘All Good Cretins Go to Heaven’: Dee Dee Ramone’s Twisted Punk Paintings |Melissa Leon |December 15, 2014 |DAILY BEAST 

They include signed photographs of Eisenhower and Field Marshal Montgomery, as well as paintings and a red leather despatch box. Churchill’s Secret Treasures for Sale: A British PM’s Life on the Auction Block |Tom Teodorczuk |December 8, 2014 |DAILY BEAST 

With this cool leather moto hat from Vince Camuto, you can look fabulous and fashionable while keeping your locks in line. The Daily Beast’s 2014 Holiday Gift Guide: For the Anthony Bourdain in Your Life |Allison McNearney |November 29, 2014 |DAILY BEAST 

A leather swordbelt, gold-embroidered at the edges, carried a long steel-halted rapier in a leather scabbard chaped with steel. St. Martin's Summer |Rafael Sabatini 

The sleeves of his doublet which protruded from his leather casing were of the same colour and material as his trunks. St. Martin's Summer |Rafael Sabatini 

And the man who had done all this—a vulgar upstart out of Paris, reeking of leather and the barrack-room still lived! St. Martin's Summer |Rafael Sabatini 

His clothes marked him as a man of the city, for we do not wear shooting jackets, and breeches and leather leggings in our valley. The Soldier of the Valley |Nelson Lloyd 

But the jar threw my six-shooter where I couldn't reach it, and the carbine was jammed in the stirrup-leather on the wrong side. Raw Gold |Bertrand W. Sinclair