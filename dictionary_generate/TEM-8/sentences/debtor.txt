The CPUC’s communication division made a verbal agreement with the accounting office in 2018 to not record outstanding citations because the debtors may no longer be in business. Utility Companies Owe Millions to This State Regulatory Agency. The Problem? The Agency Can’t Track What It’s Owed. |by Scott Morris, Bay City News Foundation |February 10, 2021 |ProPublica 

Although neither of those companies received money from the federal loan program, the renewed ability of their debtors to repay is probably helping other debt-collection companies, analysts said. Debt collectors, payday lenders collected over $500 million in federal pandemic relief |Peter Whoriskey, Joel Jacobs, Aaron Gregg |January 15, 2021 |Washington Post 

Many debtors — the primary source of revenue for debt-collection agencies — have at least temporarily been in a better position to pay their debts. Debt collectors, payday lenders collected over $500 million in federal pandemic relief |Peter Whoriskey, Joel Jacobs, Aaron Gregg |January 15, 2021 |Washington Post 

The stories exposed how high-interest lenders and medical debt collectors have taken over American courtrooms, using them to funnel debtors to jail over unpaid bills. Four ProPublica Projects Named Finalists for Loeb Awards |by ProPublica |October 6, 2020 |ProPublica 

A recent study by economists from Dartmouth’s Tuck School of Business and the University of California, San Diego, focused on debtors who, after being sued, agreed to pay in order to avoid garnishment. Debt Collectors Have Made a Fortune This Year. Now They’re Coming for More. |by Paul Kiel and Jeff Ernsthausen |October 5, 2020 |ProPublica 

But that means that the debtor will be on the hook for somewhere around 25% of the forgiven debt. Ask the Blogger |Megan McArdle |December 3, 2012 |DAILY BEAST 

Bartleby ends in debtor's prison, where the lawyer visits him and finds him - dead. David's Bookclub: Bartleby the Scrivener |David Frum |November 26, 2012 |DAILY BEAST 

Given all that, the chances of the IRS coming after the debtor for income tax on the forgiven debt are exactly zero. Debt and Taxes |Megan McArdle |November 14, 2012 |DAILY BEAST 

In Europe the principal divide that has opened is among countries, with debtor nations pitted against creditor nations. Top Economists on How to Fix the Economy | |October 12, 2011 |DAILY BEAST 

They fully understand that American military power cannot survive the United States being a huge debtor nation. Robert Gates' Lonely Crusade |Leslie H. Gelb |May 22, 2010 |DAILY BEAST 

And, 'Whosoever shall swear by the altar, it is nothing; but whosoever shall swear by the gift that is upon it, he is a debtor.' His Last Week |William E. Barton 

By submitting to the rite, every one that received circumcision became a debtor to do the whole law. The Ordinance of Covenanting |John Cunningham 

Likewise a man and a woman who are engaged to be married; and a creditor has an insurable interest in the life of his debtor. Putnam's Handy Law Book for the Layman |Albert Sidney Bolles 

Nor can a debtor compel his creditor to receive one cent and five cent pieces to a greater amount than twenty-five cents. Putnam's Handy Law Book for the Layman |Albert Sidney Bolles 

Does a debtor who turns over a note to his creditor in payment, thereby cancel the debt? Putnam's Handy Law Book for the Layman |Albert Sidney Bolles