The hope was that any small pieces left over would disappear via seagull and other scavengers, Linnman explained. Why the Forest Service has a manual for blowing up horse carcasses |David Roza/Task &amp; Purpose |August 4, 2022 |Popular-Science 

Then, the hypnotherapist will conjure that imagery—focusing, for example, on the salt spray of the ocean, seagulls calling overhead, and sun-kissed skin—to help the person go deeper into the calming visualization. How Hypnosis Works, According to Science |Eleanor Cummins |April 28, 2022 |Time 

The noise of the water was pierced by seagulls and the occasional car as I walked along the dam. The Soviets turned the Volga River into a machine. Then the machine broke. |Olga Dobrovidova |December 15, 2021 |MIT Technology Review 

I still would never put another morsel of seagull anywhere near my mouth again. Tales of a Jailhouse Gourmet: How I learned to Cook in Prison |Daniel Genis |June 21, 2014 |DAILY BEAST 

When he first started, he tried to sell plates of seagull as ‘set-ups’, but no one would touch it. Tales of a Jailhouse Gourmet: How I learned to Cook in Prison |Daniel Genis |June 21, 2014 |DAILY BEAST 

She recalled the line in The Seagull that resonated deeply with her. Carey Mulligan's Naked Turn in 'Shame' |Lorenza Muñoz |November 30, 2011 |DAILY BEAST 

It doesn't matter if tastes like a dead seagull spewing maggots, it will cost more. The Great Cabernet Ripoff |Keith Wallace |July 14, 2009 |DAILY BEAST 

His stage credits include Richard II, Hamlet, King Lear, Hedda Gabler, Crime and Punishment, The Seagull, and Terre Haute. Earthy Beauty |Peter Eyre |March 19, 2009 |DAILY BEAST 

Lower and lower the circling Seagull dropped, then landed gracefully and easily. The Phantom Town Mystery |Carol Norton 

Nearly a dozen shots were fired without a single seagull being hit. Fred Fearnot's New Ranch |Hal Standish 

Ah, a Macphail always feels like a seagull with a broken wing in the South. The Cabinet Minister |Arthur Pinero 

Its situation is quite solitary, and, save for the cry of the seagull, there reigns about it an unbroken silence. Some Private Views |James Payn 

Away went the ships, with their white canvas spread like the wings of a seagull. The Life of a Celebrated Buccaneer |Richard Clynton