There are days as a PR person you’re going out there and, yes, you cherry-pick facts to put the best spin on stuff. Full Transcript: Sean Spicer on ‘The Carlos Watson Show’ |Daniel Malloy |August 26, 2020 |Ozy 

Judges often guess the number of flips or spins based on how long the athlete stayed in the air and what position he or she landed in. A robot referee can really keep its ‘eye’ on the ball |Kathryn Hulick |August 20, 2020 |Science News For Students 

However, we found that15 LHPs generally have had lower spin rates on their pitches, both fastballs and off-speed pitches. What Really Gives Left-Handed Pitchers Their Edge? |Guy Molyneux |August 17, 2020 |FiveThirtyEight 

Pitcher Caleb Smith has elite spin and movement, allowing him to strike out better than a batter per inning for his three-year career. The Dodgers Lead Our National League Predictions, But Don’t Count Out The Nats Or … Reds? |Travis Sawchik |July 22, 2020 |FiveThirtyEight 

Cisco’s social media strategists have however put a new spin on the B2B Snapchat social media experience. How to plan your social media strategy for any business |Sumeet Anand |June 24, 2020 |Search Engine Watch 

The effort to sterilize his image first began when Epstein hired Los Angeles-based spin doctors Sitrick Co. Sleazy Billionaire’s Double Life Featured Beach Parties With Stephen Hawking |M.L. Nestel |January 8, 2015 |DAILY BEAST 

Although he brings a Western spin to things, he seems equally inspired by the local sense of style. The Photographer Who Gave Up Manhattan for Marrakech |Liza Foreman |January 6, 2015 |DAILY BEAST 

Spin control began, Florida-style: the opinion only covers some counties, some people, some times. The Back Alley, Low Blow-Ridden Fight to Stop Gay Marriage in Florida Is Finally Over |Jay Michaelson |January 5, 2015 |DAILY BEAST 

David Greenberg at Rutgers University has a book coming out next year on political spin. Thank Congress, Not LBJ for Great Society |Julian Zelizer, Scott Porch |January 4, 2015 |DAILY BEAST 

Aaron Paul may play a young Han Solo in the first Star Wars spin-off. Juiciest ‘Star Wars: The Force Awakens’ Rumors (and Some Debunked Ones) |Rich Goldstein |January 3, 2015 |DAILY BEAST 

You can probably realize just how headquarters would take the sort of yarn we'd spin if we dashed in and told them the truth. Raw Gold |Bertrand W. Sinclair 

"Let us go down the shore a bit," suggested Jack to Pepper and Andy, and the three joined hands for the spin. The Mystery at Putnam Hall |Arthur M. Winfield 

The women of the interior spin and weave for their household, and they also embroider very beautifully. Journal of a Voyage to Brazil |Maria Graham 

Neither of us had the advantage for the moment, so I went in for a quick wristlock and spin. Hooded Detective, Volume III No. 2, January, 1942 |Various 

It was as if she could spin it out by interposing between the moment and its end a series of insignificant acts. The Creators |May Sinclair