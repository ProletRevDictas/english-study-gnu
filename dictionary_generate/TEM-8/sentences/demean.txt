Being elsewhere meant that they had choices other than a certain death trapped in a system that worked to devalue, demean, and break their spirits. A vacation town promises rest and relaxation. The water knows the truth. |Nneka M. Okona |August 26, 2021 |Vox 

What we are saying … is that there was a campaign — a concerted campaign — to vilify and dismiss and demean, and, frankly, lie about it, the effectiveness of these drugs. How the right’s ivermectin conspiracy theories led to people buying horse dewormer |Aaron Blake |August 24, 2021 |Washington Post 

Women were left out of conversations in the physical office, demeaned, or made to feel like they didn’t belong. For women, remote work is a blessing and a curse |Rani Molla |July 13, 2021 |Vox 

We sort of demean those fundamental principles of the heart and the soul. Is the Future of Farming in the Ocean? (Ep. 467) |Stephen J. Dubner |June 24, 2021 |Freakonomics 

Behavior that demeans or discriminates against people for who they are has no place here. How angry Apple employees’ petition led to a controversial new hire’s departure |Shirin Ghaffary |May 14, 2021 |Vox 

Modern campaigns rarely elevate any subject and have a terrible tendency to demean all who participate. Secret Campaign for Chairman of the Federal Reserve |Stuart Stevens |September 12, 2013 |DAILY BEAST 

Hand it off to a hen-pecked husband or a put-upon assistant and it can demean or belittle. The Language of Margaret Thatcher’s Handbags |Robin Givhan |April 8, 2013 |DAILY BEAST 

The Taliban have said the comments comparing war to a game 'demean' Harry. Prince Harry: "Take a Life To Save A Life", Confirms He Has Killed Insurgents |Tom Sykes |January 22, 2013 |DAILY BEAST 

Canadians are generally mistrustful of rules that subordinate or demean women. The Canadian Supreme Court's Punt on the Niqab |David Frum |December 22, 2012 |DAILY BEAST 

“He is trying to shock whoever finds the body,” said Shepard, who said that Hughes was trying also to demean his victims. LA’s Grisly Woman Killer |Christine Pelisek |October 27, 2011 |DAILY BEAST 

"Mr. Capt don't demean himself to chambermaids, Miss Lucy," retorted the abigail with angry scorn. The Pit Town Coronet, Volume I (of 3) |Charles James Wills 

This comes of your princesses, that turn the world upside down, and demean themselves to hob and nob with these black baldicoots! The Saint's Tragedy |Charles Kingsley 

Why should ladies demean themselves by going amongst dirty beggarly folk? The Haunted Room |A. L. O. E. 

There is no man upon earth who would demean himself by breaking a lance with my master. The White Company |Arthur Conan Doyle 

How well our Champion doth demean himself, As if he had been made for such an action? Beaumont &amp; Fletcher's Works (8 of 10) |Francis Beaumont