After all, it’s not uncommon for wealthy Americans to have lawyers on retainer, and many call their lawyers whenever their hearts desire. The Critical Role of Public Defenders in a Post-Roe v. Wade World |Emily Galvin-Almanza |May 7, 2022 |Time 

Six in 10 clients have shortened deadlines on projects, and the average timeline of a client’s expectation has shrunk from more than a month to 2-4 weeks, while projects now outweigh retainer business by 55% to 45%. Media Buying Briefing: How to retain and empower agency talent when there’s more work than ever |Michael Bürgi |February 14, 2022 |Digiday 

I think that there’s always going to be a place for retainer contracts with agencies for bigger brands. ‘People doing the work have more power’: We Are Rosie founder Stephanie Olson on freelance network’s growth, war on talent |Kristina Monllos |January 6, 2022 |Digiday 

For freelancers on retainer, managing clients’ expectations while also staying within the bounds of the agreed upon job can be a delicate balance. ‘It can easily spin out of control’: Confessions of a freelance creative on the rise of scope creep |Kristina Monllos |June 10, 2021 |Digiday 

Asked by an agent whether he had legal representation, Hopkins said Project Veritas had a lawyer on retainer “in case there’s anything that happens.” Postal Service finds no evidence of mail ballot fraud in Pa. case cited by top Republicans |Jacob Bogage, Shawn Boburg |March 17, 2021 |Washington Post 

Like any high-powered attorney who charges $100,000 for a retainer, Bolt always seems to be one step ahead of the competition. Gone Girl’s Biggest Twist Is the Superb Tyler Perry |Alex Suskind |October 6, 2014 |DAILY BEAST 

One of the perks offered by the challenge is help from a list of professionals the foundation will keep on retainer. Rockefeller Foundation Announces $100 Million Project to Make Cities More Resilient |Miranda Green |May 14, 2013 |DAILY BEAST 

McKesson pays its directors an annual cash retainer of $75,000. He’s One of the Nation’s Highest-Paid CEOs—and You’ve Never Heard of Him |Gary Rivlin |January 2, 2012 |DAILY BEAST 

Is it worth it for these companies to keep stars and their keepers on retainer? Battle of the Oscar Stylists |Merle Ginsberg |February 20, 2009 |DAILY BEAST 

"Very well," said the other, handing him a check for twenty-five dollars as a retainer, and straightway left the office. The Homesteader |Oscar Micheaux 

It was very annoying—more than ever—to the Elder when he was required to put up twenty-five dollars in cash as a retainer. The Homesteader |Oscar Micheaux 

He said with a touch of mock irony: "The sailor shall play his part—the obedient retainer of the house of Devlin." Mrs. Falchion, Complete |Gilbert Parker 

His retainer fees are large; his work is exact; he is a man looked up to by those in the profession following a general practice. Opportunities in Engineering |Charles M. Horton 

It was the custom for each retainer to fit out his men according to his own taste, and at his own expense. The Continental Monthly, Vol. 2 No 4, October, 1862 |Various