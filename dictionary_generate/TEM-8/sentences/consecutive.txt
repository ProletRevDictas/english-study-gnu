His 12 consecutive victories make for the longest active streak in the UFC, and another would push him into a six-way tie for the second-longest streak in the promotion’s history. As Kamaru Usman edges toward UFC greatness, his former teammate wants to stop him |Glynn A. Hill |February 12, 2021 |Washington Post 

The Yellow Jackets missed seven consecutive field goal attempts during an early five-minute stretch before Alvarado made a three-pointer. Virginia pulls away from Georgia Tech, tightens grip on first place in ACC |Gene Wang |February 11, 2021 |Washington Post 

The close loss came after Georgetown earned consecutive victories for the first time this season in its previous two games, including a win over No. Georgetown continues to play well but falls short in the end against Villanova |Kareem Copeland |February 7, 2021 |Washington Post 

Despite appearing in his second straight Super Bowl, Chiefs offensive coordinator Eric Bieniemy was passed over for the third consecutive hiring cycle in which he received multiple interviews. Byron Leftwich is a rising star on a Bucs coaching staff that shows the power of diversity |Adam Kilgore |February 5, 2021 |Washington Post 

Frese was a young hotshot at the University of Minnesota, where she had just been named national coach of the year after taking a Golden Gophers program coming off seven consecutive losing seasons to the NCAA tournament with a 22-8 record. Brenda Frese, closing in on win No. 500, has made Maryland women’s basketball a juggernaut |Kareem Copeland |February 4, 2021 |Washington Post 

He had been left hanging, by handcuffs and not allowed to lower his arms for 22 hours each day for two consecutive days. Inside the CIA’s Sadistic Dungeon |Tim Mak |December 9, 2014 |DAILY BEAST 

We shot that sequence in 12 days over six consecutive weekends. Tom Sizemore’s Revenge: On Tom Cruise’s Scientology Recruitment, Drugs, and Craving a Comeback |Marlow Stern |September 26, 2014 |DAILY BEAST 

Central won thirty-three consecutive games, and Suffridge became a plum for the college recruiters. Football Great Bob Suffridge Wanders Through the End Zone of Life |Paul Hemphill |September 6, 2014 |DAILY BEAST 

Modern Family now holds the record for most consecutive wins in Outstanding Comedy Series, winning five trophies in a row. The Biggest Emmys Snubs and Surprises: 'Modern Family,' McConaughey, and More |Kevin Fallon |August 26, 2014 |DAILY BEAST 

FDR and Harry Truman combined to keep the Presidency in Democratic hands for twenty consecutive years. Political Mythbusting: Third Term’s the Charm |Jeff Greenfield |August 24, 2014 |DAILY BEAST 

For two consecutive seasons he lived in the sunlight of Mademoiselle Duvigne's presence. The Awakening and Selected Short Stories |Kate Chopin 

Altogether, we spent five consecutive days hovering around that collection of law-enforcers, in imminent risk of capture. Raw Gold |Bertrand W. Sinclair 

It ran for a longer consecutive period in Germany than any play by any Englishman—not excepting Shakespeare. Charles Baudelaire, His Life |Thophile Gautier 

Do you realize that I literally never know what it is to have more than three or four consecutive hours of sleep? Love's Pilgrimage |Upton Sinclair 

The books are not written on paper of the same size, nor in consecutive order, although by the same hand. The Geography of Strabo, Volume III (of 3) |Strabo