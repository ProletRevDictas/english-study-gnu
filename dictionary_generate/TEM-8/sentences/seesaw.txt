Express forecastForecast in detailWe’re in for a bit of a weather seesaw over the next few days. D.C.-area forecast: Trending milder before another burst of cold midweek |Brian Jackson |December 27, 2020 |Washington Post 

Washington tourist attractions have engaged in a seesaw battle with the coronavirus this year. The Smithsonian and National of Gallery of Art have closed. Here’s what’s still open. |Fritz Hahn |November 23, 2020 |Washington Post 

As schools seesaw between open and closed, some teachers are left without direction, feeling undervalued and underutilized. Teachers are leaving schools. Will they come to startups next? |Natasha Mascarenhas |October 30, 2020 |TechCrunch 

That seesaw season still gave the Brewers 91 wins in total, but their bad 60-game stretch — with a winning percentage 34 points below what they had in their other games — ultimately cost them a playoff spot. Bad Teams May Be Posing As Good Teams In A 60-Game Baseball Season |Neil Paine (neil.paine@fivethirtyeight.com) |August 7, 2020 |FiveThirtyEight 

When your heartbeat is going, then it’s just loading up the seesaw to one side. How Your Heart Influences What You Perceive and Fear |Jordana Cepelewicz |July 6, 2020 |Quanta Magazine 

After a back and forth seesaw race, both candidates were locked on 49% with 99% of the votes counted. Runoff Required in Mississippi’s Dirtiest Primary |Ben Jacobs |June 4, 2014 |DAILY BEAST 

Absent a complete ban, or total irrationality, judges should simply step off the seesaw. Hands Off Those Gun Laws, Judges |Eric Segall |June 1, 2014 |DAILY BEAST 

If the government infringes a protected right, one side of the seesaw goes in the air and the right is lifted and protected. Hands Off Those Gun Laws, Judges |Eric Segall |June 1, 2014 |DAILY BEAST 

After months of seesaw battles in the Sahara Desert, Libya's rebels are now making their first serious push to Tripoli. The Battle for Tripoli | |August 22, 2011 |DAILY BEAST 

If it waits for the political seesaw by which both parties avoid responsibility, there will be small chance of a navy. Spanish Life in Town and Country |L. Higgin and Eugne E. Street 

I lay silent, watching a bird seesaw on the vine which clambers over my window-ledge in friendly fashion. Mavis of Green Hill |Faith Baldwin 

I whipped the car back, spun it in a seesaw, and took off back towards the first road block. Highways in Hiding |George Oliver Smith 

After prolonged northeast rain a bright day, and with it the setting of sail, a many-handed seesaw at the windlass, and departure. The Atlantic Monthly, Volume 14, No. 86, December, 1864 |Various 

There was a barrel or two, an enormous wooden ball, a collapsible fold-up seesaw and other impedimenta of a trained-animal act. Sundry Accounts |Irvin S. Cobb