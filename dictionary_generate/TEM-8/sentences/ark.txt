“On Animals,” by Susan OrleanWith humor and generosity, the New Yorker writer expounds upon the ark’s worth of birds and mammals she has brought into her life, from apartment-dwelling dogs to disparate livestock. 50 notable works of nonfiction |Washington Post Editors and Reviewers |November 18, 2021 |Washington Post 

After 33 Years and an airplane explosion, their "Raiders of the Lost Ark" remake is almost complete. The Daily Beast’s Best Longreads, Nov 10-16, 2014 |William Boot |November 16, 2014 |DAILY BEAST 

Birlik said the idea was to create a national park on the slopes of Mount Cudi and put the Hollywood ark in the middle of it. Why Turkey Wants Russell Crowe’s Ark |Thomas Seibert |May 11, 2014 |DAILY BEAST 

And, just eyeballing it here, that ark looked closer to four hundred cubits than the biblically prescribed three hundred. ‘Noah’: The Bible vs. the Blockbuster |Joel Baden |March 30, 2014 |DAILY BEAST 

Superstorm Sandy damaged the giant ark and some of the sets you built out in Long Island. ‘Noah’ is a Global Warming Epic About the Battle Between Religion and Science, Says Cinematographer |Marlow Stern |March 27, 2014 |DAILY BEAST 

But the design of the ark lends an element of drama to the film. ‘Noah’ is a Global Warming Epic About the Battle Between Religion and Science, Says Cinematographer |Marlow Stern |March 27, 2014 |DAILY BEAST 

In bewilderment she brought the ark into the room, and read the letter addressed to Janet and herself. The Joyous Adventures of Aristide Pujol |William J. Locke 

Finally the total number of human beings who entered the ark were 4 pairs or eight persons. Assimilative Memory |Marcus Dwight Larrowe (AKA Prof. A. Loisette) 

He went into the nursery, unearthed the now-disused Noah's Ark, and sucked the paint off as many animals as remained. Kipling Stories and Poems Every Child Should Know, Book II |Rudyard Kipling 

And when they came unto the threshing-floor of Chidon, Uzza put forth his hand to hold the Ark; for the oxen stumbled. Tessa Wadsworth's Discipline |Jennie M. Drinkwater 

This alludes to the figure of the cherubims in the sanctuary, which with stretched out wings covered the ark.-Ibid. The Bible, Douay-Rheims Version |Various