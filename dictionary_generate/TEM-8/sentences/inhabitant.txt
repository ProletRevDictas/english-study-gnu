We often forget our connections with our fellow planetary inhabitants, but our lives may depend on them. Why We Should Eat Crickets. And Other Bug Ideas - Facts So Romantic |Mary Ellen Hannibal |October 2, 2020 |Nautilus 

As our planet plows through space, its orbit inevitably crosses the orbits of other inhabitants of the solar system. The World’s Space Agencies Are on a Quest to Deflect a (Harmless) Asteroid |Jason Dorrier |September 27, 2020 |Singularity Hub 

A publicly owned utility is what we need for San Diego’s working-class families and for all living inhabitants of our planet. The Franchise Agreement Ending Offers San Diego a Chance for a Fresh Start |Amanda Moser and Shauna McKenna |September 8, 2020 |Voice of San Diego 

So, the whole idea is much more of being a thriving city, a city that is taking well care of its inhabitants but also taking well care of its environment and taking well care of the rest of the world. Is Economic Growth the Wrong Goal? (Ep. 429) |Stephen J. Dubner |August 13, 2020 |Freakonomics 

If a brain is our Earth, then we, as inhabitants, are individual brain cells. Amazingly Detailed Map Reveals How the Brain Changes With Aging |Shelly Fan |June 16, 2020 |Singularity Hub 

It is empty, the door swung open—perhaps the bird has already flown, or perhaps the cage awaits its next inhabitant. Sor Juana: Mexico’s Most Erotic Poet and Its Most Dangerous Nun |Katie Baker |November 8, 2014 |DAILY BEAST 

Lei Huazhen is a handsome woman and unusually tall for an inhabitant of Sichuan. An Oral History of Mao’s Greatest Crime |Zhou Xun |November 24, 2013 |DAILY BEAST 

Bartleby might well be the street's only constant inhabitant. David's Bookclub: Bartleby the Scrivener |David Frum |November 26, 2012 |DAILY BEAST 

And he said: Until the cities be wasted without inhabitant, and the houses without man, and the land shall be left desolate. The Bible, Douay-Rheims Version |Various 

Ice of such thickness on Lake Luna at this early date, however, surprised even that apocryphal person, the oldest inhabitant. The Girls of Central High on the Stage |Gertrude W. Morrison 

No one to-day questions that man was an inhabitant of Europe during the Glacial Age. Man And His Ancestor |Charles Morris 

No creature of the Greek imagination would have been a suitable inhabitant for it except Prometheus alone. Overland |John William De Forest 

I like to think that this rare wayward and terrible creature of emotion was once an inhabitant of these walls. A Wanderer in Paris |E. V. Lucas