Theresa Causa, a nurse practitioner in San Antonio, tried a new dating app called S’More, which reveals matches’ photos only after a pair has exchanged several messages. What’s sexy in a pandemic? Caution. |Lisa Bonos |February 12, 2021 |Washington Post 

In a brief message on Instagram, the City Girls thanked Rae for the opportunity to enter the world of television. A New Comedy From Issa Rae Is Coming To HBO Max |Brande Victorian |February 11, 2021 |Essence.com 

With no pregame anthem, maybe there’s no way for the NBA to spread that message as powerfully as players did last summer in Orlando. The pregame national anthem — in all its roiling contradictions — still has something to offer |Barry Svrluga |February 11, 2021 |Washington Post 

Now you need to signal the Dear Leader that you have received his message and are standing down, but standing by. Gene Weingarten: I come bearing good news, QAnon. Meet WAnon, the new Mr. Right. |Gene Weingarten |February 11, 2021 |Washington Post 

Kazuko Fukuda, one of the women who started the petition, said she had wanted a way to get the message across to politicians in their “boys’ club” who had closed ranks around Mori and cling to old attitudes. Japan Olympics chief who said women talk too much will resign over remarks, reports say |Simon Denyer, Julia Mio Inuma |February 11, 2021 |Washington Post 

When it comes to educating our children, Congress should heed that message, not ignore it. The ‘No Child’ Rewrite Threatens Your Kids’ Future |Jonah Edelman |January 3, 2015 |DAILY BEAST 

Leelah Alcorn's message was sent, and heard, and things started changing. Cover-Ups and Concern Trolls: Actually, It's About Ethics in Suicide Journalism |Arthur Chu |January 3, 2015 |DAILY BEAST 

Does the sending of the message “justify” the tragedy that caused it? Cover-Ups and Concern Trolls: Actually, It's About Ethics in Suicide Journalism |Arthur Chu |January 3, 2015 |DAILY BEAST 

But the act of killing herself done, the message was sent, and heard, and things started changing. Cover-Ups and Concern Trolls: Actually, It's About Ethics in Suicide Journalism |Arthur Chu |January 3, 2015 |DAILY BEAST 

The audio message featured the words, “the real battle in Lebanon is yet to begin.” A Sunni-Shia Love Story Imperiled by al Qaeda |Ruth Michaelson |December 26, 2014 |DAILY BEAST 

His head fell back limp on MacRae's arm, and the rest of the message went with the game old Dutchman across the big divide. Raw Gold |Bertrand W. Sinclair 

We were interrupted at this moment by a message from General Houston, to whom we immediately hastened. Blackwood's Edinburgh Magazine, No. CCCXXXIX. January, 1844. Vol. LV. |Various 

"It will go through, if I live," calmly replied Harry, as he carefully concealed the message in the lining of his coat. The Courier of the Ozarks |Byron A. Dunn 

In the telegraph office a young signaler was sending a thrilling message to Umballa, Lahore and the north. The Red Year |Louis Tracy 

Aristide again sought the message of the stars; but the sky was clouded over, and soon a fine rain began to fall. The Joyous Adventures of Aristide Pujol |William J. Locke