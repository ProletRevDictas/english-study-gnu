Sure, you would be offering them some comforting words to soften the impact created by the error. Guide to using interactive 404s to boost your SEO |Amanda Jerelyn |September 24, 2020 |Search Engine Watch 

Warming temperatures also soften the ice itself, so that it slides more quickly toward the sea. Global warming may lead to practically irreversible Antarctic melting |Carolyn Gramling |September 23, 2020 |Science News 

The puff portion refers to softened crude, thin as heated maple syrup, rising up through production wells. Oil Companies Are Profiting From Illegal Spills. And California Lets Them. |by Janet Wilson, The Desert Sun, and Lylla Younes, ProPublica |September 18, 2020 |ProPublica 

Many of those organizations pursued ad opportunities on social networks like Twitter to soften the loss of match-day revenue. ‘An unprecedented period of Darwinian experimentation’: As sports return, Twitter eyes ad boost |Seb Joseph |September 18, 2020 |Digiday 

As a result, some schools have worked to soften the blow by skipping scheduled tuition hikes or even cutting the costs. College tuition is seeing its steepest drop since 1978 |Lee Clifford |September 12, 2020 |Fortune 

Cover with plastic wrap and allow the dates to soften, about 15 minutes. Make ‘The Chew’s’ Carla Hall’s Sticky Toffee Pudding |Carla Hall |December 28, 2014 |DAILY BEAST 

But now, facing a tough re-election, Walker is trying to soften his rhetoric. Women Voters Haunted by the GOP's Tricks |Sally Kohn |October 31, 2014 |DAILY BEAST 

Russia did not only rely on Ketchum to soften its image in the West. Confessions of a Putin Spin Doctor |Eli Lake |March 11, 2014 |DAILY BEAST 

Indeed, after the end of a relationships, there are many reasons to turn to sex to soften the blow. People Who Have Had Rebound Sex Tell Us Why It Is Awesome |Emily Shire |January 31, 2014 |DAILY BEAST 

There are hints that a behind-the-scenes strategy to soften the blame put on the Portuguese is already underway. Investigation Into Madeleine McCann Disappearance Reopened in Portugal |Barbie Latza Nadeau |October 24, 2013 |DAILY BEAST 

A trace of light had begun to soften the sky over the dome, but had not yet seeped down to ground level. Fee of the Frontier |Horace Brown Fyfe 

The old woman meanwhile tried to soften the obdurate wall with melted butter and new milk—but in vain. The Book of Anecdotes and Budget of Fun; |Various 

He had tried to soften his reply, but not being politic or tactful had succeeded only in expressing himself more brusquely. Dorothy at Skyrie |Evelyn Raymond 

The rapidity of their decomposition is such, that when spread on the land they are seen to soften and disappear in a short time. Elements of Agricultural Chemistry |Thomas Anderson 

Madame Malmaison knew it also, but the hard look on her greedy face did not soften. The Weight of the Crown |Fred M. White