Clark confirmed Ingalls’ daughter was the only returning cheerleader cut this season, but said freshmen trying out for the first time were also cut. School Sports Became ‘Clubs’ Amid the Pandemic – Now Two Coaches Are Out |Ashly McGlone |September 17, 2020 |Voice of San Diego 

Planned for nearly 20 years—a pact between my mother and her college roommate to each name their first daughter Kate. Dawn of the Heliocene - Issue 90: Something Green |Summer Praetorius |September 16, 2020 |Nautilus 

One parent of two teenagers involved in the effort, Robert Jason Noonan, said his 16- and 17-year-old daughters were being paid by Turning Point to push “conservative points of view and values” on social media. Pro-Trump youth group enlists teens in secretive campaign likened to a ‘troll farm,’ prompting rebuke by Facebook and Twitter |Isaac Stanley-Becker |September 15, 2020 |Washington Post 

So what Tyler did was get out ahead of being shunted to secretaries and daughters by confused investors, the way Jenn Hyman had. ‘How I Built This’ host Guy Raz on insights from some of the world’s most famous entrepreneurs |Rachel King |September 15, 2020 |Fortune 

By the time Pure went public in 1995, my wife and I had been married for four years and we had one young daughter. What if Your Company Had No Rules? (Bonus Episode) |Maria Konnikova |September 12, 2020 |Freakonomics 

Then came Bess Myerson, a daughter of Russian-Jewish immigrants who was raised in the Sholem Aleichem Houses in the Bronx. Why Was Bess Myerson the First and Last Jewish Miss America? |Emily Shire |January 7, 2015 |DAILY BEAST 

Like most Jewish mothers, Myerson thought her daughter could do better. Why Was Bess Myerson the First and Last Jewish Miss America? |Emily Shire |January 7, 2015 |DAILY BEAST 

This is about no longer accepting that—as so many others have stated—a family would rather have a dead son than a living daughter. Dear Leelah, We Will Fight On For You: A Letter to a Dead Trans Teen |Parker Molloy |January 1, 2015 |DAILY BEAST 

I noticed a picture of her daughter, who was my classmate, and out of curiosity visited her page. 50 Shades of Iran: The Mullahs’ Kinky Fantasies about Sex in the West |IranWire, Shima Sharabi |January 1, 2015 |DAILY BEAST 

Her adopted daughter tried to suffocate a younger biological sibling. Judge: Rehoming Kids Is Trafficking |Tina Traster |December 30, 2014 |DAILY BEAST 

"The Smoker," and "Mother and Daughter," a triptych, are two of her principal pictures. Women in the fine arts, from the Seventh Century B.C. to the Twentieth Century A.D. |Clara Erskine Clement 

The Rev. Alonzo Barnard, seventy-one years of age, accompanied by his daughter, was present. Among the Sioux |R. J. Creswell 

He reached forward and took her hands, and if Mrs. Vivian had come in she would have seen him kneeling at her daughter's feet. Confidence |Henry James 

Every word that now fell from the agitated Empress was balm to the affrighted nerves of her daughter. The Pastor's Fire-side Vol. 3 of 4 |Jane Porter 

She looked from the picture to her daughter, with a frightful glare, in their before mild aspect. The Pastor's Fire-side Vol. 3 of 4 |Jane Porter