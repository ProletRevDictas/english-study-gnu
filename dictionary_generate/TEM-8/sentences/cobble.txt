For Melodi, and many of her friends who are in creative industries, the pandemic has been a time of cobbling together ways to get by. Remote Work Is All Gen Z Knows. But Are They Satisfied? |Raisa Bruner |August 16, 2021 |Time 

In 2019, the Heat cobbled together a four-team deal to bring Jimmy Butler to Biscayne Boulevard via sign-and-trade. Which NBA Teams Are Making The Same Free Agency Moves, And Which Ones Are Mixing It Up? |Jared Dubin |August 4, 2021 |FiveThirtyEight 

Then, like many chefs, he laid off most of his staff — many had worked with him for years to establish the restaurant’s reputation as destination-worthy — and cobbled together a takeout program of dishes like burgers and pot stickers. Does It Get Better For the Indie Fine Dining Restaurant? |Sara Sheridan |July 22, 2021 |Eater 

When the pandemic hit, those of us who were able to work from home had to quickly cobble together work stations. What Happens to Your Body After a Year Inside? |mmirhashem |July 14, 2021 |Outside Online 

Germany is a team in transition, and scraped through the tournament’s group stage after losing to France and cobbling together a tie with Hungary. England vs Germany Offers End to 25 Years of Hurt for One Man, and a Nation |Dan Stewart |June 28, 2021 |Time 

Just cobble it together, plonk it on your head, and smile a lot. Flower Crowns Are Phony and Must Die |Sara Lieberman |September 5, 2014 |DAILY BEAST 

A couple years ago, I was walking one weekday morning down Court Street in Cobble Hill, Brooklyn and came upon Robin Williams. When I Met Robin Williams in Afghanistan |Matthew Kaminski |August 20, 2014 |DAILY BEAST 

If only the Dems or the Reps could cobble one or two together out back in the shed. Hey, Boomers—Millennials Hate Your Partisan Crap |Nick Gillespie |July 10, 2014 |DAILY BEAST 

The Lees were the 1st Black Family to move into the predominantly Italian-American Brooklyn Neighborhood of Cobble Hill. Spike Lee Blasts The New York Times’ Story on Brooklyn Gentrification in Fiery Op-Ed |Marlow Stern |March 31, 2014 |DAILY BEAST 

He could call new elections, but he will more likely call on Renzi to cobble together a government where Letta failed. Florentine Mayor Matteo Renzi to Lead Italy |Barbie Latza Nadeau |February 14, 2014 |DAILY BEAST 

The pavement is of rough cobble-stones, and swarms of dogs and children crowded the way everywhere. British Highways And Byways From A Motor Car |Thomas D. Murphy 

The streets are lined with cobble stones and bowlders and low, white houses, mostly one-story high. Ways of War and Peace |Delia Austrian 

There were no harsh commands that he uttered, no rattling of wheels over cobble stones, no exhibition of a despotic will. The Broken Sword |Dennison Worthington 

Many of its streets are paved with cobble-stones, and some of its buildings are, if not handsome, at least substantial. The Head Hunters of Northern Luzon From Ifugao to Kalinga |Cornelis De Witt Willcox 

There is a short squat cobble, flat-bottomed and of intolerable weight, down near the waters, and its owner makes for it. Somehow Good |William de Morgan