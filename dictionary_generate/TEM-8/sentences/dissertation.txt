Thirteen years ago, while working on her PHD dissertation in Madagascar’s Masoala Peninsula, Borgerson encountered a problem. They're Healthy. They're Sustainable. So Why Don't Humans Eat More Bugs? |Aryn Baker |February 26, 2021 |Time 

At Harvard, he received a PhD in government and wrote his dissertation under Henry Kissinger, who became a lifelong friend. Guido Goldman, who forged closer ties between Germany and U.S., dies at 83 |Harrison Smith |December 11, 2020 |Washington Post 

I planned to go back to physics after a couple of years and then return to wrap up my dissertation. Five Scientists on the Heroes Who Changed Their Lives - Issue 93: Forerunners |Alan Lightman, Hope Jahren, Robert Sapolsky, |December 2, 2020 |Nautilus 

My buba’s lived experience helped shape me into the girl who wrote her college dissertation on the gender pay gap, arguing for equal parental leave for dads and moms, almost 20 years before any major employer implemented any such thing. Ruth Bader Ginsburg’s passing feels like millions of women have lost their bubbie |jakemeth |September 22, 2020 |Fortune 

My PhD dissertation was a highly theoretical model representing computer systems that were framed as a mathematical model, and if they were interconnected in such a way that these interconnected computers would communicate like cells in the body. Chancellor Harold L. Martin, on His Plan to Safely Open the Nation's Largest HBCU During COVID-19 |Eben Shapiro |August 23, 2020 |Time 

A terrific cultural studies dissertation awaits on how the fortunes of the Cheneys provide a mirror on a changing America. The Cheneys’ Permanent War |Heather Hurlburt |June 18, 2014 |DAILY BEAST 

Today, he visits online forums and bombards them with dissertation-length comments. Inside the Misunderstood Mind of Jeopardy! Champ Arthur Chu, Who Is Not Ruining the Show |Sujay Kumar |February 24, 2014 |DAILY BEAST 

In her dissertation, McFate had asked whether ‘good anthropology’ might lead to ‘better killing.’ Send in the Marines—and the Anthropologists too? |John Kael Weston |August 23, 2013 |DAILY BEAST 

Heritage has distanced itself from Richwine and his dissertation. Immigrants’ IQ Lower, Wrote Coathor of Heritage Foundation Report |Jamelle Bouie |May 9, 2013 |DAILY BEAST 

No single dissertation will alter the status quo on its own. Powerless In Gaza |Matt Sienkiewicz |May 1, 2013 |DAILY BEAST 

I've never had time to write home about it, for I felt that it required a dissertation in itself to do it justice. Music-Study in Germany |Amy Fay 

Dr. Pitcairn, published at Leyden his dissertation on the circulation of the blood through the veins. The Every Day Book of History and Chronology |Joel Munsell 

Start not, reader, I am not going to trouble you with a poetical dissertation; no, no! Lavengro |George Borrow 

Dissertation sur les Assassins, Académie des Inscriptions, tom. The History of the Knights Templars, the Temple Church, and the Temple |Charles G. Addison 

This dissertation, which is illustrated by several plates, will repay for the time spent in reading it. Notes and Queries, Number 69, February 22, 1851 |Various