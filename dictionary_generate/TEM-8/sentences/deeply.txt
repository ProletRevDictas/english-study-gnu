She reasons that racism is “baked so deeply into the system that it’s invisible” to the actors within that system. A Georgetown professor trades her classroom for a police beat |Ronald S. Sullivan Jr. |February 12, 2021 |Washington Post 

The catch, of course, is that she doesn’t actually get in — and while on a class trip across the country, she falls deeply in love with New York University instead. It was time for the ‘To All the Boys’ franchise to end |Sonia Rao |February 12, 2021 |Washington Post 

Hard to imagine in this day and age, especially since my politics and work life are so deeply intertwined. Meet D.C.’s Most Eligible LGBTQ Singles |Staff reports |February 11, 2021 |Washington Blade 

And, while it’s a very nice and deeply interesting study, it’s also got a long list of limitations. Can I Ask You a Ridiculously Personal Question? (Ep. 451) |Stephen J. Dubner |February 11, 2021 |Freakonomics 

Excited and not sure what to expect, Mellaart cut deeply into the eastern mound in 1961, roughly 200 meters south of where archaeologist Ruth Tringham later uncovered the skeleton of a woman whom she nicknamed “Dido.” What archaeologists got wrong about female statues, goddesses, and fertility |Annalee Newitz |February 10, 2021 |Popular-Science 

Human evolution has left men as deeply wired for emotional connections to children as women are. How Good Dads Can Change the World |Gary Barker, PhD, Michael Kaufman |January 6, 2015 |DAILY BEAST 

Marriott, with its deep history in the Mormon faith, portrays itself as a deeply ethical institution. How ‘Ethical’ Hotel Chain Marriott Gouges Guests in the Name of Wi-Fi Security |Kyle Chayka |December 31, 2014 |DAILY BEAST 

But I am deeply concerned with the lack of progress in my case and feel that I must take some action. An American Marine in Iran’s Prisons Goes on Hunger Strike |IranWire |December 18, 2014 |DAILY BEAST 

Koenig proceeds to deliver her deeply conflicted, sorta-kinda support for Adnan. Adnan Killed Her! No, Jay Did It! Serial’s Uncertain, True-to-Reality End |Emily Shire |December 18, 2014 |DAILY BEAST 

The first meeting featured multiple speakers deeply rooted in a partisan agenda. The Left’s Answer to ALEC |Ben Jacobs |December 15, 2014 |DAILY BEAST 

At another time her affections were deeply engaged by a young gentleman who visited a lady on a neighboring plantation. The Awakening and Selected Short Stories |Kate Chopin 

He was already deeply indebted to his wife; not one of his three partners had proved to be such as he expected and required. Blackwood's Edinburgh Magazine, No. CCCXXXIX. January, 1844. Vol. LV. |Various 

She had been silent while he was so deeply engrossed in thought, and upon hearing her voice he started abruptly. The Homesteader |Oscar Micheaux 

"Then I think it was your duty to have first consulted me, Lady Maude," he said, feeling deeply mortified. Elster's Folly |Mrs. Henry Wood 

His best friend would not have recognized him on that deeply interesting occasion. Blackwood's Edinburgh Magazine, No. CCCXXXIX. January, 1844. Vol. LV. |Various