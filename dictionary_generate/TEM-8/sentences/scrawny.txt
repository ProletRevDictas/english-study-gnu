Plants potted in lunar dirt were far scrawnier than those grown in volcanic material from Earth. The first plants ever grown in moon dirt have sprouted |Maria Temming |June 16, 2022 |Science News For Students 

The cat was so small and scrawny that Mike’s mother decided he needed a big name. The name game: For these pets, inspiration came in many forms |John Kelly |November 23, 2020 |Washington Post 

I asked a scrawny 15-year-old boy who works at a candy packaging facility about how he sees his future. The Stolen Childhood of Teenage Factory Workers |by Melissa Sanchez |November 20, 2020 |ProPublica 

Toward the end of the cycle, the birds are scrawnier, their fat stores depleted over the months of cold, so they tend to start huddling at warmer temperatures. Math of the Penguins |Susan D'Agostino |August 17, 2020 |Quanta Magazine 

If scrawny little Tutankhamun can do it, a badass like Khufu could probably cause them to spontaneously combust. Why Do These Russians Hate History So Much? |Justin Green |March 28, 2013 |DAILY BEAST 

He was still achieving in school and sports, though less brilliantly than before, and was somewhat small and scrawny. Speed Read: 11 Juiciest Bits From Philip Norman’s Biography of Mick Jagger |The Daily Beast |October 1, 2012 |DAILY BEAST 

He grew up a scrawny kid with nagging allergy problems in a suburb of Stockholm. Dolph Lundgren’s Wild Ride: From Fulbright Scholar to ‘The Expendables 2’ |Marlow Stern |August 17, 2012 |DAILY BEAST 

A few are recovering from eating disorders; their cheeks are hollow and their scrawny arms droop like slack rubber bands. 'Fat Studies' Go to College |Eve Binder |November 3, 2010 |DAILY BEAST 

She bore Marneffe a child, a stunted, scrawny urchin named Stanislas. Repertory Of The Comedie Humaine, Complete, A -- Z |Anatole Cerfberr and Jules Franois Christophe 

Bluish dawnlight seemed to tint their scrawny bare arms and legs a deeper, ghastly blue. Restricted Tool |Malcolm B. Morehart 

Their sales of scrawny cattle jist about paid the taxes en bought their salt en terbacker. David Lannarck, Midget |George S. Harney 

A stub of a root and two scrawny plum branches would at any time arouse my imagination like the circus posters' appeal to a boy. Dwarf Fruit Trees |F. A. Waugh 

The squatter lifted it up with infinite tenderness, binding the rags more closely about the scrawny body. Tess of the Storm Country |Grace Miller White