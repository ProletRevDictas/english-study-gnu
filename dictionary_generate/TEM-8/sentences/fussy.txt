Like some of the other prizes here, this legacy from when Azerbaijan was under Soviet rule is fussy going for the baker. This new Azerbaijani bakery offers stories as good as its pastries |Tom Sietsema |February 26, 2021 |Washington Post 

Toddlers want to touch and push and pull, but they can get easily frustrated if something is too technical or fussy. Toys and science gifts for kids of all ages |PopSci Commerce Team |February 18, 2021 |Popular-Science 

The best science gifts for kids over $50 will be educational, well made, not overly complicated and fussy, and allow for many hours of play and use. Toys and science gifts for kids of all ages |PopSci Commerce Team |February 18, 2021 |Popular-Science 

One day as we were going out for breakfast, she was extremely irritable and fussy that morning. How To Find Freedom From Grief |Jasmine Grant |January 7, 2021 |Essence.com 

Of its options, she says Philodendron bipennifolium and Rhaphidophora tetrasperma aren’t “super fussy.” Online plant sellers are having a moment. Here’s where to shop. |Lindsey Roberts |November 11, 2020 |Washington Post 

In the era of Tea Party stunts and dramatic fan-based delays, the debate was moderately fussy. What Al Franken’s Normcore Senate Race Can Teach Other Democrats |Ana Marie Cox |October 27, 2014 |DAILY BEAST 

It was nearing naptime and so the three hurried to grab groceries, worrying that the baby would get fussy after too long. Westgate's Chilling Security Video Reveals Shopping Mall Bloodbath |Nina Strochlic |September 15, 2014 |DAILY BEAST 

“Those kids are starting to be of legal drinking age,” McMahan says of once-fussy eaters who he hopes remain picky drinkers, too. People for the Ethical Treatment of Vodka |Debra A. Klein |July 23, 2014 |DAILY BEAST 

The fussy legal language complicates what is sometimes a life-or-death situation. When Pro-Life Means Death |Kitty Holland |November 16, 2012 |DAILY BEAST 

Last month a similarly fussy—if more PG—correction went viral. Best New York Times Corrections Ever: 'The Shining,' Twilight Sparkle & More |Josh Dzieza |February 3, 2012 |DAILY BEAST 

Farewell, too, energetic and laborious dancer, my partner's middle-aged fussy cousin! Blackwood's Edinburgh Magazine, Volume 67, Number 414, April, 1850 |Various 

"I don't know," he muttered in the face of a fussy little woman, who jumped aside to let him pass. Sinister Street, vol. 1 |Compton Mackenzie 

We could get along with her all right; sometimes she is splendid, even if she is so fussy. Tabitha at Ivy Hall |Ruth Alberta Brown 

By calling fussy little strikes often enough I could have kept the profits close to the zero mark. The Iron Puddler |James J. Davis 

I wish my wife was not so fussy, though that is a kind of thing, Lady Eustace, that one has to expect from young wives. The Prime Minister |Anthony Trollope