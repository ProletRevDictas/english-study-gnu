When we came in he looked indisposed and nervous, and there happened to be a good many artists there. Music-Study in Germany |Amy Fay 

After finishing her dinner she went to her room, having instructed the boy to tell any other callers that she was indisposed. The Awakening and Selected Short Stories |Kate Chopin 

The doctor who had attended his wife during her confinement was indisposed, and was represented only by an affidavit. The Homesteader |Oscar Micheaux 

A heretic in medicine being indisposed, his physician happened to call. The Book of Anecdotes and Budget of Fun; |Various 

The animals become indisposed, and the secretion of milk is much lessened. An Inquiry into the Causes and Effects of the Variolae Vaccinae |Edward Jenner