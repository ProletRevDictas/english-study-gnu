Traffic and street noise drown out yells, screeches, or the entire Moana soundtrack when my kids sing it at the top of their lungs. Pandemic Aside, Outdoor Dining Has Been a Godsend for Parents With Young Children |Kristen Hawley |November 22, 2021 |Eater 

What had been a walled garden accessible only to government-funded researchers became an extraordinary new platform for communication and business, as the screech of dial-up modems connected millions of home computers to the World Wide Web. Where computing might go next |Margaret O’Mara |October 27, 2021 |MIT Technology Review 

The shackles around my ankles create an arrhythmic concert of metallic screeches on the way to the plane. Locked up in the Land of Liberty: Part I |Yariel Valdés González |July 7, 2021 |Washington Blade 

“I guess some part of me will always be Screech,” young Diamond says at the end of the film. How Bad Was 'The Unauthorized Saved By the Bell Story'? |Kevin Fallon |September 2, 2014 |DAILY BEAST 

But if you say Screech or the guy from Clueless, who are people going to know? 'Saved by the Bell' Star Dustin Diamond Doesn't Want to Be a Jerk Anymore |Kevin Fallon |August 11, 2014 |DAILY BEAST 

Yeah, and especially me, because the Screech character was so hard to shake. 'Saved by the Bell' Star Dustin Diamond Doesn't Want to Be a Jerk Anymore |Kevin Fallon |August 11, 2014 |DAILY BEAST 

Well, he came across like Screech from Saved By the Bell, only without the necessary follicles to sustain a Jewfro. Iowa GOP Senate Debate Shows Divides |Ben Jacobs |April 25, 2014 |DAILY BEAST 

The grief of losing Frankie to this trip so overwhelmed me that I let out one long screech. ‘Tracing the Blue Light’: Read Chapter 1 of Eileen Cronin’s ‘Mermaid’ |Eileen Cronin |April 8, 2014 |DAILY BEAST 

That screech was so blamed genuwine I almost fergot to stick out my laig and trip Boston as he come by me. Alec Lloyd, Cowpuncher |Eleanor Gates 

I thought how at college I used to hear from my chamber the screech of trolley cars rounding a curve and biting my nerves. The Idyl of Twin Fires |Walter Prichard Eaton 

The distant screech of the grinder was muffled and not unpleasant. The Worshippers |Damon Francis Knight 

From a tree near the forest Tess could hear the screech of a night-owl die away in smothered laughter. Tess of the Storm Country |Grace Miller White 

Dangerfield and Farrelly were gone—and a rending screech from behind the buildings told only too well where. The Onslaught from Rigel |Fletcher Pratt