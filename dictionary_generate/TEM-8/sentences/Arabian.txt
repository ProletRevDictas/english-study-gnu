So in an episode that played out like a political thriller, in 1811, Muhammed Ali invited many senior Mamluk members of the realm to his palace in Cairo – to apparently toast the start of a war with the Wahhabis in the Arabian peninsula. Mamluks: The Incredible Islamic Slave-Warriors of Egypt |Dattatreya Mandal |June 23, 2022 |Realm of History 

Axes quickly spread through Africa, then were carried by modern humans into the Arabian peninsula, Australia, and ultimately Europe. How a Handful of Prehistoric Geniuses Launched Humanity’s Technological Revolution |Nicholas R. Longrich |January 7, 2022 |Singularity Hub 

Wecker brings together mythical figures from different traditions—the Jewish golem and the Arabian jinni—in an entertaining and surprising tale set in early-20th-century New York. Everything Our Editors Loved in November |smurguia |December 8, 2021 |Outside Online 

You have to know what kind of seam works for the Arabian vinyl. How Waterbeds, Teen Love and an Unhinged Bradley Cooper Led To Licorice Pizza |Matthew Jacobs |November 23, 2021 |Time 

Traditional Arabian techniques like mashrabiya have been incorporated into iconic modern buildings in Abu Dhabi, a city in one of the most heat-threatened countries in the world. Ancient architecture might be key to creating climate-resilient buildings |Angely Mercado |October 16, 2021 |Popular-Science 

As the Arabian Peninsula flourished, the area became the center of cultural development. When Saudi Arabia Ruled the World |Emily Wilson |October 31, 2014 |DAILY BEAST 

A Saudi Arabian television ad for Viagra shows a man struggling to push a straw through the lid of his beverage. Laughter Will Be the Legacy of Viagra |Samantha Allen |October 2, 2014 |DAILY BEAST 

The Saudi Arabian government paid for her tuition in addition to a $1,800 stipend for personal expenses. Saudi Beauty Says She Robbed Banks for Her Mafia Lover |Caitlin Dickson |May 10, 2014 |DAILY BEAST 

So was Nasir al Wuhayshi, the Yemeni firebrand who runs al Qaeda in the Arabian Peninsula. How Al Qaeda Escaped Afghanistan and Lived to Fight Another Day |Yaniv Barzilai |March 16, 2014 |DAILY BEAST 

Dusty paths lined with flickering orange lanterns led us to our final camp, a slice of dramatic Arabian luxury. On Foot in the High Atlas Mountains of Morocco |Joanna Eede |January 22, 2014 |DAILY BEAST 

The apocryphal gospels contain many, and some are preserved by Persian and Arabian poets. Solomon and Solomonic Literature |Moncure Daniel Conway 

To the girls, that jolting ride was like an adventure straight from the Arabian Nights. The Outdoor Girls in the Saddle |Laura Lee Hope 

A short distance from the station lay a true Arabian sand desert, but which was fortunately not of very great extent. A Woman's Journey Round the World |Ida Pfeiffer 

Thus the Phoenicians and the Amorites belong to the first stage of the second great Arabian migration. Encyclopaedia Britannica, 11th Edition, Volume 5, Slice 2 |Various 

I was warned secretly by a strange Arabian woman, who required of me solemn oath not to reveal her. God Wills It! |William Stearns Davis