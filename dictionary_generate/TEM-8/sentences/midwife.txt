You can define sex as being based on your body and whether the doctor or midwife said you were a boy or girl when you were born. How to use science to talk to kids about gender |Purbita Saha |July 22, 2021 |Popular-Science 

Watterdal holds weekly WhatsApp calls with Taliban officials, who, he says, have understood that in order for their communities to have midwives, women must be educated through twelfth grade. The U.S. Is Leaving Afghanistan, the Taliban Is Growing in Power, and Education for Girls and Women Is Already at Risk |Amie Ferris-Rotman |July 7, 2021 |Time 

Most others had their babies delivered by midwives and their fevers and wounds treated by family members or local healers. How medicine sought to control women’s bodies while ignoring their symptoms |Susan Okie |July 2, 2021 |Washington Post 

What was remarkable, considering that this was a patriarchy, is that the most valued witnesses were the women who supervised the birth, the midwife and nurses. With Lilibet and Archie, Harry and Meghan Aim to Break the Cycle of Painful Royal Parenting |Clive Irving |June 6, 2021 |The Daily Beast 

Rhea had already given birth five times, and each time, Gaia attended her daughter as midwife. Gaia, the Scientist - Issue 99: Universality |Hope Jahren |April 7, 2021 |Nautilus 

The parents had chosen to give birth at home, with a certified professional midwife attending. Are Water Births Toxic to Babies? |Brandy Zadrozny |December 12, 2014 |DAILY BEAST 

At the time of her arrival in 2011, many of the facilities in Liberia lacked even a single midwife, let alone trained OB/GYNs. The Only Thing More Terrifying Than Ebola Is Being Pregnant With Ebola |Kent Sepkowitz, Abby Haglage |October 2, 2014 |DAILY BEAST 

It means care with a mother-focused doctor or midwife, sometimes in a place other than a hospital. Natural Childbirth Is Not a Cult |Brandy Zadrozny |June 27, 2014 |DAILY BEAST 

Later on they came and said something else, but a midwife later told me the same [not to have more children]. The Mom Forced to Have a C-Section |Emily Shire |June 5, 2014 |DAILY BEAST 

Instead, he wound up being the midwife for the Soviet Union's demise. Ex- CIA Chief: Why We Keep Getting Putin Wrong |Eli Lake, Noah Shachtman, Christopher Dickey |March 2, 2014 |DAILY BEAST 

In this case the midwife was afraid to go alone with her summoner, and begged that her husband might accompany her. The Science of Fairy Tales |Edwin Sidney Hartland 

Conversely, when the midwife is rewarded with that which seems valuable it turns out worthless. The Science of Fairy Tales |Edwin Sidney Hartland 

The quondam midwife, with tears in her eyes, looked at her, and blessed the moment she had done a generous act. The Science of Fairy Tales |Edwin Sidney Hartland 

During this time,—from 1760 to 1775,—a Mrs. Peck was also known in the same town as an excellent midwife. The College, the Market, and the Court |Caroline H. Dall 

The midwife, without the ointment, is deceived like Thor by Utgard-Loki: nothing is as it appears to her. The Science of Fairy Tales |Edwin Sidney Hartland