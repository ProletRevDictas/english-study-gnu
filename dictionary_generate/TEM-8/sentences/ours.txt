In this world of ours, some melodies are just more beautiful than others. Yes, I Like Christmas Music. Stop Laughing. |Michael Tomasky |December 24, 2014 |DAILY BEAST 

And all kinds of friends of ours have raised money for Mary Landrieu to support her as a candidate. Hillary Praises Fracking, Stays Silent on Keystone |David Freedlander |December 2, 2014 |DAILY BEAST 

And in a culture as paranoid as ours, we freak out about them all the time. Valerie Jarrett, Obama Consigliere—and Democracy Killer |James Poulos |November 12, 2014 |DAILY BEAST 

“Font, logo, edge finish, surface finish … everything is different from ours,” said Sung Hwang, the general manager. Patients Screwed in Spine Surgery ‘Scam’ |The Center for Investigative Reporting |November 3, 2014 |DAILY BEAST 

In a society as race-crazy as ours, this sort of news is equal parts shocking and unsurprising. Ex-NFL Linebacker: We Talk Around Race, Not About It |Carl Banks |October 23, 2014 |DAILY BEAST 

That, too, is a process which in this changing new world of ours can never be completed. The Salvaging Of Civilisation |H. G. (Herbert George) Wells 

Well, we must try our luck with a regulation sabre; they can't well refuse it; ours is the stronger and bigger man. The Pit Town Coronet, Volume I (of 3) |Charles James Wills 

I began to fear I should have that unlucky expedition of ours on my conscience for the rest of my days. Blackwood's Edinburgh Magazine, Volume 60, No. 372, October 1846 |Various 

Their patroles of horse, and ours, frequently go over the same ground. Notes and Queries, Number 177, March 19, 1853 |Various 

And what manner of fool may this one be, whose fortunes were so desperate that he could throw them in with ours? St. Martin's Summer |Rafael Sabatini