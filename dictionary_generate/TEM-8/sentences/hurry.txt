Higher wattage isn’t nearly as significant when it comes to slow cooking, but if you’re looking to achieve perfectly prepared meals in a hurry, it’s important to go for a toaster oven that’s powered sufficiently for your needs. Best toaster oven: Save counter space and time with our toaster oven picks |Julian Cubilllos |February 5, 2021 |Popular-Science 

In no hurry to leave, it could generate snow showers and flurries into Tuesday. Updates: An icy morning with scattered snow showers developing into the afternoon |Jason Samenow |February 1, 2021 |Washington Post 

Ohio State can also throw up points in a hurry, with the No. Justin Fields outduels Trevor Lawrence as Ohio State upsets Clemson in playoff semifinal |Emily Giambalvo, Kareem Copeland |January 2, 2021 |Washington Post 

I hope down the line Apple tweaks it to add a mode that captures both a raw and a jpeg version like many DSLRs do, so it’s easy to upload a shot to an Instagram story in a hurry, but also leaves you with a high-quality raw file to edit later. How and when to shoot with Apple’s hidden new photo format |Stan Horaczek |December 16, 2020 |Popular-Science 

In a home setting, it will be useful for tasks like truly wireless AR and VR, which require lots of data delivered in a hurry. What the heck is Wi-Fi 6E? |Stan Horaczek |December 9, 2020 |Popular-Science 

I could complain about how, two out of eight episodes in, Agent Carter is in no hurry to introduce its real villain. Marvel’s ‘Agent Carter’ Stomps on the Patriarchy |Melissa Leon |January 7, 2015 |DAILY BEAST 

Lorenzo Music delivers his lines as the title character in no particular hurry and with plenty of bite. Garfield Television: The Cat Who Saved Primetime Cartoons |Rich Goldstein |November 5, 2014 |DAILY BEAST 

And so some long-standing policies have changed dramatically and in a hurry. Exclusive: ISIS’s Enemies Ask Pentagon for Drones |Eli Lake |August 13, 2014 |DAILY BEAST 

They also want the administration to hurry up and decide how it plans to go after the group, both in Iraq and in Syria. After Underestimating ISIS, Obama Scrambles for Plan to Defeat Them |Josh Rogin |August 9, 2014 |DAILY BEAST 

Johnson indicated the school was in no hurry to unveil a new nickname or logo. The Native Americans Who Voted for ‘The Fighting Sioux’ |Evan Weiner |June 26, 2014 |DAILY BEAST 

With him one is at high pressure all the time, and I have gained a good many more ideas from him than I can work up in a hurry. Music-Study in Germany |Amy Fay 

The little crowd and the boats on the beach were right under them and no one paid any attention or seemed to be in a hurry. Gallipoli Diary, Volume I |Ian Hamilton 

That was because he was hungry, you see, but pigs nearly always eat fast, as though they were continually in a hurry. Squinty the Comical Pig |Richard Barnum 

Murat was in no hurry to commence his reign, and his subjects showed no great anxiety to see their new ruler. Napoleon's Marshals |R. P. Dunn-Pattison 

Enough for him that it was one of the hated race, to be killed in a violent hurry or fled from in tremendous haste! Hunting the Lions |R.M. Ballantyne