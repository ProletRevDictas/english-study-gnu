Scientists have been trying to figure out how ancient people developed their tools and built their cities and monuments. Let’s learn about ancient technology |Bethany Brookshire |September 15, 2020 |Science News For Students 

Like so many of our museums and monuments, it’s always some place you’ve been meaning to go. We owe it to places like the Tabard Inn |Brock Thompson |September 11, 2020 |Washington Blade 

Hearing sounds of some kind circulating inside the ancient monument “must have been one of the fundamental experiences of Stonehenge.” Stonehenge enhanced sounds like voices or music for people inside the monument |Bruce Bower |August 31, 2020 |Science News 

Ginzel said that traditional monuments, such as the Columbus statues, can be problematic because they represent power from a specific point of view. A Closer Look at the Public Art at Chicago Police Stations |by Logan Jaffe |August 21, 2020 |ProPublica 

People living back then often created monuments to frame views of natural features, she says. Underground mega-monument found near Stonehenge |Avery Elizabeth Hurt |August 11, 2020 |Science News For Students 

The man whom Time dubbed a “Black Leonardo,” became the first African-American to have a national monument dedicated to him. Growth Stocks |The Daily Beast |October 17, 2014 |DAILY BEAST 

But, in my mind—and many of the townspeople—the monument was far from the main attraction. New York’s Century-Old Time Capsule Is a Dud |Justin Jones |October 8, 2014 |DAILY BEAST 

On one occasion, a drone operator flew a drone over a crowd at Mount Rushmore, then out over the monument itself. Soon We’ll Be Watching Whales By Drone |Abigail Golden |August 25, 2014 |DAILY BEAST 

At its center was a monument, perhaps just over six feet high. Fighting Back With Faith: Inside the Yezidis’ Iraqi Temple |Michael Luongo |August 21, 2014 |DAILY BEAST 

The entire city can seem like a singular monument to his decades in office. Can America’s Favorite Ex-Con Mayor Win Again? |David Freedlander |June 22, 2014 |DAILY BEAST 

After his death crowds flocked to his grave to touch his holy monument, till the authorities caused the church yard to be shut. The Every Day Book of History and Chronology |Joel Munsell 

In a statuesque attitude, she sat, like Marius on the ruins of Carthage, or Patience on a monument smiling at grief. The Pit Town Coronet, Volume I (of 3) |Charles James Wills 

His grand work, the Animal Kingdom, forms an imperishable monument of his genius. The Every Day Book of History and Chronology |Joel Munsell 

It is undoubtedly Cavaill-Coll's finest work, and a lasting monument to his genius. The Recent Revolution in Organ Building |George Laing Miller 

The column was suggested in 1862 as a suitable monument to the memory of the late Prince Albert. Life of Richard Trevithick, Volume II (of 2) |Francis Trevithick