His father, a drama professor, and his mother, a playwright, would put on performances in a grove of oak trees behind the house every summer, in which young Francis performed. NIH Director Francis Collins Is Fighting This Coronavirus While Preparing for the Next One |Belinda Luscombe |February 4, 2021 |Time 

North America contains the largest number of oak species, with a surprising diversity of 160 species in Mexico. 13 edible plants you can still find in the winter |By Tim MacWelch/Outdoor Life |December 1, 2020 |Popular-Science 

The day after our trip Davi joins Edward Cook to search again for the old oak trees. We didn’t track hurricanes hundreds of years ago—but trees did |Greta Moran |October 16, 2020 |Popular-Science 

I’m out with a chain saw cutting down the oak tree that I should have trimmed back two years ago. What the Photos of Wildfires and Smoke Don’t Show You |by Elizabeth Weil and Lisa Larson-Walker |September 21, 2020 |ProPublica 

That means large coast live oak, rare flowering bushes and other plants could be ripped out when necessary. Oil Companies Are Profiting From Illegal Spills. And California Lets Them. |by Janet Wilson, The Desert Sun, and Lylla Younes, ProPublica |September 18, 2020 |ProPublica 

“The influence of the oak maturation casks on the final character of The Macallan is vital,” says MacPherson. How Much Do Whisky Casks Really Affect Taste? | |December 10, 2014 |DAILY BEAST 

Spanish oak, which has an open grain and high levels of tannin, gives you dried fruit, spice, and even chocolate flavors. How Much Do Whisky Casks Really Affect Taste? | |December 10, 2014 |DAILY BEAST 

They embraced as they ran down the list of Red Oak residents at the fair. Joni Ernst's Big Pivot: From Pig Castrator to Iowa Nice |Ben Jacobs |August 11, 2014 |DAILY BEAST 

All of the whisky used in both types of scotch must be matured in Scotland and aged for a minimum of three years in oak casks. Don't Be a Single-Malt Scotch Snob |Kayleigh Kulp |August 9, 2014 |DAILY BEAST 

Like bourbon, rye must be aged in charred oak barrels for at least two years. Why Rye Is The Nation's Spirit, And Why No One Can Get It |Kayleigh Kulp |July 12, 2014 |DAILY BEAST 

Isabel sat on the bench under an ancient oak for half an hour or more, but took no note of the time. Ancestors |Gertrude Atherton 

An oak gallery runs round two sides of the hall and descends in broad and gentle stairs down the right side of it. First Plays |A. A. Milne 

My smithy lies yonder, beyond that turn of the road and behind the biggest oak tree in the country. Dorothy at Skyrie |Evelyn Raymond 

Dorothy lighted a candle, and led the way up the wide oak staircase at the bottom of the hall to the chambers above. The World Before Them |Susanna Moodie 

She led the way into a tiny parlour, very clean, very simple with its furniture of old oak and brass, and bade him sit. The Joyous Adventures of Aristide Pujol |William J. Locke