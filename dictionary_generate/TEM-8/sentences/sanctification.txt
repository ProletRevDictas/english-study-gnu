"Sanctification," said the pale boy near the door, in a prompt but mechanical manner. Skipper Worse |Alexander Lange Kielland 

Seek to become established by the grace of sanctification; then you will be better able to meet temptation and persecution. The value of a praying mother |Isabel C. Byrum 

And it is an important means of sanctification, and of perseverance in grace. The Ordinance of Covenanting |John Cunningham 

And seven days after I was saved I was convicted for sanctification or a clean heart. Prisons and Prayer: Or a Labor of Love |Elizabeth Ryder Wheaton 

He of God is made unto us wisdom, and righteousness, and sanctification and redemption. Commentary on the Epistle to the Galatians |Martin Luther