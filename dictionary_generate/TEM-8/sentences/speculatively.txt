Like many Bitcoin derivatives, the value of the speculative tokens is volatile. Are Chinese spies trying to hack this anticensorship startup? Its execs believe so |rhhackettfortune |November 5, 2020 |Fortune 

He’s speculative the initial ban on his pages were due to mass reporting by anti-LGBTQ groups and individuals because of his work with San Juan Mayor Carmen Yulín Cruz, who lost in August’s gubernatorial primary. Puerto Rico LGBTQ activist blocked from Facebook accounts for two months |Kaela Roeder |October 23, 2020 |Washington Blade 

The tokens in question represented equity in future blockchain projects but, in practice, were treated mostly as speculative investments. The blockchain industry faces a moment of truth as high-profile projects go live |Jeff |October 21, 2020 |Fortune 

McConnell, having become interested in Hydén’s work, scrambled to test for a speculative molecule that he called “memory RNA” by grafting portions of trained planaria onto the bodies of untrained planaria. Memories Can Be Injected and Survive Amputation and Metamorphosis - Facts So Romantic |Marco Altamirano |October 20, 2020 |Nautilus 

A WarnerMedia spokesperson told Ars that "we are not discussing or confirming any speculative numbers" regarding how many jobs will be cut. AT&T plans thousands of layoffs at HBO, Warner Bros., rest of WarnerMedia |Jon Brodkin |October 9, 2020 |Ars Technica 

"They're pretty thin," Cash observed speculatively, as though he was measuring them mentally for some particular need. Cabin Fever |B. M. Bower 

He was regarding, speculatively, the back of young Ovid Nixon, the assistant cashier. Scattergood Baines |Clarence Budington Kelland 

Often she had gazed speculatively at the fine home they had built upon Stewart Island, wondering why the two were so aloof. The Missing Formula |Mildred A. Wirt, AKA Ann Wirt 

He felt speculatively certain that Horace Endicott sat before him, and he knew Sonia to be a guilty woman. The Art of Disappearing |John Talbot Smith 

It was perhaps inevitable that she should have thought speculatively of her wedding gown; what girl would not? And So They Were Married |Florence Morse Kingsley