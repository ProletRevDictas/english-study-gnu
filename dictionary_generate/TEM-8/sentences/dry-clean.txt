With Charlie Hebdo, “you really have a clean case here,” Shearer said. Harry Shearer on The Dangerous Business of Satire |Lloyd Grove |January 8, 2015 |DAILY BEAST 

This is a guy who has his son-in-law clean his eyeglasses, for crying out loud. Will Chris Christie Regret His Cowboy Hug? |Matt Lewis |January 5, 2015 |DAILY BEAST 

Millions of dollars in renovation later the building is gorgeous—Clean, well-kept, organized. His First Day Out Of Jail After 40 Years: Adjusting To Life Outside |Justin Rohrlich |January 3, 2015 |DAILY BEAST 

My understanding was that according to most Christian beliefs, being trans or gay was a sin, cut and dry. Dear Leelah, We Will Fight On For You: A Letter to a Dead Trans Teen |Parker Molloy |January 1, 2015 |DAILY BEAST 

“Clean as a whistle,” says a senior investigator involved in the case. Exclusive: Inside a Cop-Killer’s Final Hours |Michael Daly |December 31, 2014 |DAILY BEAST 

The tears came so fast to Mrs. Pontellier's eyes that the damp sleeve of her peignoir no longer served to dry them. The Awakening and Selected Short Stories |Kate Chopin 

But Polavieja started his campaign with the immense advantage of having the whole of the dry season before him. The Philippine Islands |John Foreman 

Their method of curing the leaves was to air-dry them and then packing them until wanted for use. Tobacco; Its History, Varieties, Culture, Manufacture and Commerce |E. R. Billings. 

He shall give his mind to finish the glazing, and his watching to make clean the furnace. The Bible, Douay-Rheims Version |Various 

The smoke from her kitchen fire rose white as she put in dry sumac to give it a start. The Bondboy |George W. (George Washington) Ogden