A tornado hit Welch and Rawlings’ studio earlier this year and almost destroyed their archives, inspiring them to release the music as soon as they could. Everything Our Editors Loved in August |The Editors |September 10, 2020 |Outside Online 

So I back-burnered tornadoes for decades and nearly forgot about them. My Week Shadowing a Tornado Hunter in Oklahoma |Linda Logan |September 10, 2020 |Outside Online 

I decided to book the Mayhem 1 tour with Extreme Chase Tours, one of some 20 stormchasing outfits in the country, which promises a 90 percent chance of seeing a tornado over the course of six days. My Week Shadowing a Tornado Hunter in Oklahoma |Linda Logan |September 10, 2020 |Outside Online 

Imagine a straight length of hose, representing the length of a straight vortex like a tornado. An Unexpected Twist Lights Up the Secrets of Turbulence |David H. Freedman |September 3, 2020 |Quanta Magazine 

Each day of additional warning gives emergency managers that much more time to prepare for incoming heat waves, cold snaps, tornadoes or other wild weather. Improved three-week weather forecasts could save lives from disaster |Alexandra Witze |August 27, 2020 |Science News 

At the time, sirens were not yet standard in tornado country. Heed the Warnings: Why We’re on the Brink of Mass Extinction |Sean B. Carroll |November 30, 2014 |DAILY BEAST 

About 9:30 p.m. on Palm Sunday in 1965, a tornado struck Toledo, Ohio. Heed the Warnings: Why We’re on the Brink of Mass Extinction |Sean B. Carroll |November 30, 2014 |DAILY BEAST 

The classic film that opens with a tornado sweeping through a Kansas farm made its debut 75 years ago in 1939. Fact-Checking the Sunday Shows: November 2 |PunditFact.com |November 2, 2014 |DAILY BEAST 

Fallin has received high marks for her leadership after a tornado devastated the town of Moore. The Democrats’ Great Plains Firewall: Can Joe Dorman Take the Oklahoma Statehouse? |David Freedlander |October 3, 2014 |DAILY BEAST 

And the town of Moore was no longer known just for the tornado that devastated it a year ago. The Muslim Convert Behind America’s First Workplace Beheading |Michael Daly |September 27, 2014 |DAILY BEAST 

A fearsome thunderstorm or howling tornado of dust might reveal her fickleness of mood at any moment. The Red Year |Louis Tracy 

A tremendous tornado passed over the city of Natchez, very destructive to life and property. The Every Day Book of History and Chronology |Joel Munsell 

A destructive tornado swept over a portion of Lapeer county, Michigan. The Every Day Book of History and Chronology |Joel Munsell 

She was timid during any thunder shower and this was worse than a shower which threatened—a tornado seemed imminent. Dorothy at Skyrie |Evelyn Raymond 

He fell upon Mrs. Buttershaw, a slatternly and sour-visaged woman, and hurled at her a tornado of questions. The Joyous Adventures of Aristide Pujol |William J. Locke