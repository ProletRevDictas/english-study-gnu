Others will reflect the shaky scrawls of a woman who struggled to hold a pen steady. The incredible story of how 1,700 handwritten cards came from across the world for a group of D.C. hospital workers |Theresa Vargas |January 27, 2021 |Washington Post 

Beijing might say yes, pointing to the shaky loyalty of the population. The Only Path Ahead for Hong Kong Is Reform |Brian Wong |January 13, 2021 |Time 

The lawyer who spoke to him that day remembered his voice sounding shaky, his words coming too fast to understand. To stay or to go? |Hannah Dreier |December 26, 2020 |Washington Post 

It’s an attempt to balance a lot of what the government views as shaky forces. “He put QR-coded wristbands on each of the chickens” |Katie McLean |December 18, 2020 |MIT Technology Review 

Egeland had just visited Um Rakuba, a refugee camp in the eastern part of Sudan, and — over a somewhat shaky connection — he shared what he’d seen there and his concerns about the potential for a deepening catastrophe in the region. Ethiopia’s unfolding humanitarian crisis, explained by a top aid official |Jen Kirby |December 3, 2020 |Vox 

Never mind that some of the atmospherics are shaky, like the white Christmas, for example. How Dickens and Scrooge Saved Christmas |Clive Irving |December 22, 2014 |DAILY BEAST 

And even The Lancet and Nature publish their share of shaky studies. How to Tell When a Scientific Study Is Total B.S. |Russell Saunders |August 22, 2014 |DAILY BEAST 

Its addictive “sidebar of shame” catalogues every celebrity roll of fat, fashion faux pas, and shaky early-morning nightclub exit. Hollywood vs. The Daily Mail: George Clooney and Angelina Jolie Take On The UK's Leanest, Meanest Gossip Machine |Lizzie Crocker, Lloyd Grove |July 12, 2014 |DAILY BEAST 

Law enforcement officials started with the proposition that Cope was guilty because of his shaky “confessions.” The Supreme Court Must Right the Wrong Done to Billy Wayne Cope |Andrew Cohen |June 19, 2014 |DAILY BEAST 

After his shaky hosting—sorry gays—Hugh Jackman is indubitably straight. Hugh Jackman's Tony Jumping Fail, Plus the Winners and Standout Moments of Broadway's Biggest Night |Tim Teeman |June 9, 2014 |DAILY BEAST 

I'm rather shaky on my pins yet and the chair it must be, if I'm to put myself in connection with that lounge. Dorothy at Skyrie |Evelyn Raymond 

It came at last, not like anything Peter had ever heard, and was more like a howl than a cry, for "Shaky; me wants Shaky." The Cromptons |Mary J. Holmes 

But he held his tongue, and tried to make up to the little girl her loss of Shaky, for whom she cried for days. The Cromptons |Mary J. Holmes 

He was terribly white and shaky, and he seemed to have some difficulty in getting out his words. The Weight of the Crown |Fred M. White 

They grew more shaky and more illegible towards the end, but they were sufficient to make the truth absolutely clear. The Doctor of Pimlico |William Le Queux