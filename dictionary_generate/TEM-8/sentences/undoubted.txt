Of course, Beijing and Taipei have many reasons to avoid war, given the lives that hang in the balance and undoubted economic devastation. The Retreat from Afghanistan Has Made America's Allies Nervous. None More So Than Taiwan |Charlie Campbell / Shanghai |September 3, 2021 |Time 

That said, let me ease your undoubted panic a little: you already have more in retirement assets than most people. Ask the Blogger: Worried Savers Edition |Megan McArdle |March 19, 2013 |DAILY BEAST 

Because she wants the world to know—to paraphrase her undoubted nemesis Gloria Steinem—that “this is what 55 looks like.” The One Good Thing Michele Bachmann Did: Proudly Blurt Out Her Age |Judith Newman |January 8, 2012 |DAILY BEAST 

Despite the undoubted impact of this novel, this disturbing vision bends the story in an unlikely direction. Philip Roth's Extreme Novel |Morris Dickstein |October 2, 2010 |DAILY BEAST 

But the undoubted apogee of the festival was the young Pakistani platoon. The Greatest Literary Show on Earth |Amulya Gopalakrishnan |January 27, 2009 |DAILY BEAST 

Undoubted specimens of the great masters are now very rarely to be had, unless at a very high price. Violins and Violin Makers |Joseph Pearce 

An undoubted violin of any period of this great master's make, is well worthy the attention of the virtuoso. Violins and Violin Makers |Joseph Pearce 

The first and most prominent thing which strikes an observer, is, the undoubted general revival of trade and commerce. Blackwood's Edinburgh Magazine, No. CCCXXXIX. January, 1844. Vol. LV. |Various 

Fight for your undoubted rights, but gladly give up anything which may conduce to the pleasure of others, or benefit them. Digby Heathcote |W.H.G. Kingston 

Here, therefore, is an undoubted link between the Marian and Elizabethan Churches. Ireland Under the Tudors, Vol. II (of 3) |Richard Bagwell