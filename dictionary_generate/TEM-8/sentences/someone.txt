And yes, someone has already called Spencer a “Small Fry,” har har. Freaking Out About Age Gaps in Gay Relationships Is Homophobic |Samantha Allen |January 9, 2015 |DAILY BEAST 

For someone with anorexia, self-starvation makes them feel better. How Skinny Is Too Skinny? Israel Bans ‘Underweight’ Models |Carrie Arnold |January 8, 2015 |DAILY BEAST 

“Someone is determined to keep Bill Cosby off TV,” she continued. Phylicia Rashad and the Cult of Cosby Truthers |Stereo Williams |January 8, 2015 |DAILY BEAST 

Binge eating and purging does the same for someone with bulimia. How Skinny Is Too Skinny? Israel Bans ‘Underweight’ Models |Carrie Arnold |January 8, 2015 |DAILY BEAST 

But if you have a hearing and you prove that someone is mature enough, well then that state interest evaporates. Should Teens Have The Right To Die? |Brandy Zadrozny |January 8, 2015 |DAILY BEAST 

Ollie saw someone standing before it, bending slightly forward in the pose of expectation. The Bondboy |George W. (George Washington) Ogden 

His untidy hair was rumpled, as if someone had been hanging onto it while in the process of giving him the shiner. Fee of the Frontier |Horace Brown Fyfe 

In 1851 she visited Birmingham and was a welcome guest until "someone blundered" and charged her with being an impostor. Showell's Dictionary of Birmingham |Thomas T. Harman and Walter Showell 

Still, it will be healthier if we push out of this planetary system before someone else pushes in. Fee of the Frontier |Horace Brown Fyfe 

He worked with a will, delighted that someone as enthusiastic and even younger than himself was now in charge. Fifty Years of Railway Life in England, Scotland and Ireland |Joseph Tatlow