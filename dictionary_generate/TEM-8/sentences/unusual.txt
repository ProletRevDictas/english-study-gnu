There has also been some data analysis of voter registration stats showing Republicans leaving the party in unusual numbers. How Has The Nation Changed Since The Insurrection At The Capitol? |Micah Cohen (micah.cohen@fivethirtyeight.com) |February 11, 2021 |FiveThirtyEight 

Perched on each of these unusual, mellow forms is a transparent sculpture of a small bird, made from 3-D printed resin and cut into solids and hollows that serve as perches or baths for actual birds. Sculpture parks are a great way to see art during a pandemic. Here’s why some are better than others. |Sebastian Smee |February 11, 2021 |Washington Post 

You took sort of an unusual path to becoming the president of digital. Media Briefing: Facebook pivots away from politics –publishers say ‘we’re just along for the ride’ |Tim Peterson |February 11, 2021 |Digiday 

It may be difficult to definitively prove the genome is gone, he says, especially if the chloroplast is “unusual in its structure or abundance” and therefore difficult to identify. A reeking, parasitic plant lost its body and much of its genetic blueprint |Jake Buehler |February 10, 2021 |Science News 

Bellows told Chastain’s attorney and Descano’s office to provide legal briefs on whether he could dismiss the case under the unusual circumstances. Judge faults Fairfax County prosecutors for failing to notify victim of trial |Justin Jouvenal |February 5, 2021 |Washington Post 

Each individual race involves an unusual collaboration between researchers, manufacturers, and public-health entities. The Race for the Ebola Vaccine |Abby Haglage |January 7, 2015 |DAILY BEAST 

So, in an unusual order (PDF) issued on New Years Day, District Judge Robert Hinkle clarified the issue. The Back Alley, Low Blow-Ridden Fight to Stop Gay Marriage in Florida Is Finally Over |Jay Michaelson |January 5, 2015 |DAILY BEAST 

The weather on the route of AirAsia Flight 8501 was not unusual for the region and the season. Did Bad Weather Bring Down AirAsia 8501? |Clive Irving |December 29, 2014 |DAILY BEAST 

It is an unusual kind of patriotism in the name of national security. Why Did We Panic After 9/11 and Ignore All We Knew About Responding to Security Threats? |Deborah Pearlstein |December 18, 2014 |DAILY BEAST 

One morning at about eleven, he announces his intention as though it's truly an unusual thought: “Let's have a little drink.” Alfred Hitchcock’s Fade to Black: The Great Director’s Final Days |David Freeman |December 13, 2014 |DAILY BEAST 

So it came to pass that another change came into his life, hence another epoch in the unusual life was his. The Homesteader |Oscar Micheaux 

It is a notable fact that under the wholly unusual circumstances prevailing, the recovery was so prompt and effective. Readings in Money and Banking |Chester Arthur Phillips 

Certainly captain Merveilles and his people showed unusual piety. The Jesuit Relations and Allied Documents, Vol. II: Acadia, 1612-1614 |Various 

She did not appear conscious that she had done anything unusual in commanding his presence. The Awakening and Selected Short Stories |Kate Chopin 

The weapons, the most unusual weapons, we are reluctantly compelled to accept under protest. The Pit Town Coronet, Volume I (of 3) |Charles James Wills