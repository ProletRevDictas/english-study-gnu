He told lawmakers that the company "took a page from Big Tobacco's playbook, working to make our offering addictive at the outset" and arguing that his former employer has been hugely detrimental to society. Former Facebook manager: “We took a page from Big Tobacco’s playbook” |Kate Cox |September 24, 2020 |Ars Technica 

Today Purdue is the target of thousands of lawsuits, charged with having downplayed the addictive nature of OxyContin. The Opioid Tragedy, Part 2: “It’s Not a Death Sentence” (Ep. 403) |Stephen J. Dubner |January 23, 2020 |Freakonomics 

While none of these new drugs proved to be non-addictive, they did become very useful in the medical management of acute pain. The Opioid Tragedy, Part 1: “We’ve Addicted an Entire Generation” (Ep. 402) |Stephen J. Dubner |January 16, 2020 |Freakonomics 

We’re just providing them with information that this is an opioid, it’s addictive, it’s dangerous, you should bring it back. The Opioid Tragedy, Part 1: “We’ve Addicted an Entire Generation” (Ep. 402) |Stephen J. Dubner |January 16, 2020 |Freakonomics 

Oxycontin was a time-released version of oxycodone that Purdue aggressively marketed to the medical community, saying it “might” be less addictive than other opioids. The Opioid Tragedy, Part 1: “We’ve Addicted an Entire Generation” (Ep. 402) |Stephen J. Dubner |January 16, 2020 |Freakonomics 

It remains a Schedule I narcotic to this day, considered as dangerous and addictive by the federal government as heroin and MDMA. Pot-Smoking Grannies, Jimmy Fallon Covers U2, and More Viral Videos |The Daily Beast Video |November 23, 2014 |DAILY BEAST 

The Flash is the latest addictive series from Berlanti, who serves as showrunner with Kreisberg. The TV Superhero Guru Behind ‘The Flash’ |Jason Lynch |October 6, 2014 |DAILY BEAST 

The celebrity scandals became addictive to both the reporters and the readers. Murdoch on the Rocks: How a Lone Reporter Revealed the Mogul's Tabloid Terror Machine |Clive Irving |August 25, 2014 |DAILY BEAST 

The show is as precisely plotted as a soap opera and beautifully lit; entirely, fatally addictive. ‘Made in Chelsea’ Has a New York Moment |Tim Teeman |August 19, 2014 |DAILY BEAST 

The best part, says Ansel, is the “utterly addictive” whipped honey brown butter served in a dish alongside. The Cronut Gives Way to the Penis Pretzel |Tim Teeman |August 7, 2014 |DAILY BEAST 

Free and fun -- addictive social games with lots of cool people playing them from all over the world. Little Brother |Cory Doctorow 

The drugs students are taking today are more potent, more dangerous, and more addictive than ever. What Works: Schools Without Drugs |United States Department of Education 

Crack or freebase rock is extremely addictive, and its effects are felt within 10 seconds. What Works: Schools Without Drugs |United States Department of Education 

He should have reasoned that out long ago; he should have realized it was impossible to have immunity to an addictive drug. Love Story |Irving E. Cox, Jr.