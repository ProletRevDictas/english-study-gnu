On the 4th of July she had conquered for herself the headship of that powerful brotherhood. The Atlantic Monthly, Volume 18, No. 109, November, 1866 |Various 

Of all objects of human ambition, that headship was best worth struggling for. History of the Intellectual Development of Europe, Volume I (of 2) |John William Draper 

In connection with the headship of St Johns College there existed a rather curious custom. Rowlandson's Oxford |A. Hamilton Gibbs 

"Where the McGregor sits," he would affirm, "there is the head of the table," and so he cares nothing about the nominal headship. A Midsummer Drive Through The Pyrenees |Edwin Asa Dix 

You must cease in any case to retain the headship of the house, even for the few days of the term that remain. Tom, Dick and Harry |Talbot Baines Reed