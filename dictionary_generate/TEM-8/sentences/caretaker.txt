Italy has had a surprising number of caretaker governments over the years. Bitcoin, stocks and crude take off as the markets brace for a wave of stimulus checks |Bernhard Warner |February 8, 2021 |Fortune 

With his brother leaving for school at Boston University the following month, Gross became his mom’s main caretaker. A sick mother, a loving son, a signed Ovechkin jersey — and how the Caps tried to help |Kyle Melnick |February 1, 2021 |Washington Post 

Other teens have become the primary caretakers for younger siblings or cousins. A steady stream of Latino students was arriving on college campuses. Then the pandemic hit. |Danielle Douglas-Gabriel, Hannah Natanson, John D. Harden |January 31, 2021 |Washington Post 

As vaccinations have been rolled out to older Americans, digitally savvy ones, or caretakers and family members with the time and resources, have quickly moved to snag appointments on apps and websites. A big hurdle for older Americans trying to get vaccinated: Using the internet |Rebecca Heilweil |January 27, 2021 |Vox 

The union has also demanded additional medical and caretaker accommodations for telework, and the district is prepared to offer as many accommodations as possible while ensuring we can safely instruct our students. Chicago Public Schools suspends in-person learning as possible teachers strike looms |Dawn Reiss, Kim Bellware |January 27, 2021 |Washington Post 

A cheerful convict was found dead by his devoted caretaker one morning. A Million Ways to Die in Prison |Daniel Genis |December 8, 2014 |DAILY BEAST 

Police say she fled to Florida and became a caretaker of an elderly man. The Mystery Woman Who Tried to Outdo Dillinger |Michael Daly |September 29, 2014 |DAILY BEAST 

The film, however, makes Asher a pilot and Fiona a caretaker to the newborns—seemingly innocuous decisions that become meaningful. Why 'The Giver' Movie Will Disappoint the Book's Fans |Kevin Fallon |August 15, 2014 |DAILY BEAST 

Crown became caretaker of the property when his uncle, who lived on the property, passed away in February. Idaho Woman Who Gave Birth on Highway: ‘I Had to Pull My Pants Down to Get the Baby Out’ |Dale Eisinger |July 10, 2014 |DAILY BEAST 

It is not clear what the whereabouts are of now ex-caretaker Prime Minister Niwatthamrong Boonsongpaisan. Thailand’s 19th Nervous Breakdown |Lennox Samuels |May 22, 2014 |DAILY BEAST 

The tenant acts as caretaker and apparently takes pride in keeping the place in order. British Highways And Byways From A Motor Car |Thomas D. Murphy 

When a caretaker enters the house of one of the good neighbours, is she accompanied by her annoyances? Punch, or the London Charivari, Vol. 109, July 27, 1895 |Various 

Does the caretaker lead a happy life in the house of one of the good neighbours? Punch, or the London Charivari, Vol. 109, July 27, 1895 |Various 

The caretaker told me they call it the 'Cradle of Liberty,' here; and I don't wonder. The Sunbridge Girls at Six Star Ranch |Eleanor H. (Eleanor Hodgman) Porter 

“Seems more cheerful like,” observed the caretaker, as the steady tick-tack began to sound through the quiet room. The Daughters of Danaus |Mona Caird