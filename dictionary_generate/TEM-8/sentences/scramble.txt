That may diminish the scramble for shots, though it also means health officials and leaders will have to work harder to make sure they’re delivering shots to those who need them. Don’t hate the vaccine tourists, hate the vaccine game |Jen Kirby |February 12, 2021 |Vox 

Unless you put it out the night before, it’s a bit of a scramble in the morning. When you walk a dog, your mind is as likely to wander as the dog is |John Kelly |February 10, 2021 |Washington Post 

In the aftermath of the late-night scramble to get vaccinated, I felt a strange mix of relief and guilt. I jumped the queue to get an expiring vaccine. Did I do the right thing? |Niall Firth |February 1, 2021 |MIT Technology Review 

Now it’s a little bit of a scramble to get the pipeline back in order. Future of TV Briefing: Media companies grapple with getting advertisers to buy their platform video inventory |Tim Peterson |January 27, 2021 |Digiday 

You can also see it in how he anticipates his receivers’ improvisational scrambles. What Makes Patrick Mahomes So Great |Josh Hermsmeyer |January 14, 2021 |FiveThirtyEight 

When Carter lost reelection in 1980, Rubenstein had to scramble. Patriotic Philanthropy: Not an Oxymoron |Eleanor Clift |November 27, 2014 |DAILY BEAST 

In the meantime, the scramble is on and, in Republican presidential politics, anything can happen. The Social Conservative Royal Rumble Is Brewing in Iowa |Ben Jacobs |October 17, 2014 |DAILY BEAST 

Colleges churn out graduates and confer advanced degrees, but the scramble for jobs continues. How Young People Are Destroying Liberty |James Poulos |October 11, 2014 |DAILY BEAST 

Perhaps worst of all, this scramble for spoils raises the value of gains even as it lowers the bar for action. Is Democracy Doomed Abroad? |James Poulos |August 31, 2014 |DAILY BEAST 

The camera sweeps over to a room filled with seemingly holographic monitors as two technicians scramble. Gamers Want to Game: Video Games Aren't Blockbuster Movies |Alec Kubas-Meyer |August 28, 2014 |DAILY BEAST 

Please remember that under socialism the scramble for wealth is limited; no man can own capital, but only consumption goods. The Unsolved Riddle of Social Justice |Stephen Leacock 

In all Trevithick's moves there was a scramble for money, in which he invariably came worst off. Life of Richard Trevithick, Volume II (of 2) |Francis Trevithick 

Bascomb could scramble and make headway up the scarred bank, but there was no chance for the motor-cycle to follow. Motor Matt's "Century" Run |Stanley R. Matthews 

What will it be like a few days hence, when I shall scramble through the passes and over glaciers by myself! The Life &amp; Letters of Peter Ilich Tchaikovsky |Modeste Tchaikovsky 

Half of the people had taken their seats when he began; there was a hasty scramble, and a decorous, half-checked smile. Hilda |Sarah Jeanette Duncan