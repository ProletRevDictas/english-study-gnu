The kids are wearing bottoms with animal print, chains and charms, pleats, textures, neon colors, and shades of brown and beige. Pants are fun now |Melinda Fakuade |March 5, 2021 |Vox 

You look at a structured coat from the front, and then from the back you see this beautiful pleat. Fashion’s Most Powerful Women: Victoria Beckham & Diane von Furstenberg Show at New York Fashion Week |Erin Cunningham |February 10, 2014 |DAILY BEAST 

Some have one pleat on each leg rather than double pleats, for example. Back Away From the Pleats! |Justin Green |October 11, 2012 |DAILY BEAST 

Captain Raff had done the bandaging: he stood back from the last neat pleat. Where the Pavement Ends |John Russell 

If a soft-looking or puffy, “fat”-looking bow is desired, pleat the ribbon singly before making the loops. Make Your Own Hats |Gene Allen Martin 

If the pleats are to be one-half inch deep, the box pleat will be one inch across. Make Your Own Hats |Gene Allen Martin 

Be very accurate, being careful to keep the box pleat the exact width desired. Make Your Own Hats |Gene Allen Martin 

The pleating may be caught together at top and bottom of box pleat, and it is then known as rose pleating. Make Your Own Hats |Gene Allen Martin