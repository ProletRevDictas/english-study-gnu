When my sister looks at a Mason jar now—whether in someone’s hand filled with tea or bursting with flowers on a friend’s coffee table—it reminds her of her son Mason. How to find joy during tough times |By Angela Gorrell/The Conversation |October 9, 2020 |Popular-Science 

Between 2004 and 2007, another former client also alleged Mason repeatedly exposed himself to her in parking lots, at her job and at a convenience store. Maine Hires Lawyers With Criminal Records to Defend Its Poorest Residents |by Samantha Hogan, The Maine Monitor, with data analysis by Agnel Philip |October 6, 2020 |ProPublica 

They failed, and as a result, Mason’s political career “was kind of curtailed,” Hyland says. Thank This Forgotten Founding Father for Your ‘Pursuit of Happiness’ |Nick Fouriezos |September 27, 2020 |Ozy 

In May 1776, Mason — by then not just a tobacco farmer but also a pioneer in the Virginia wine industry — was asked to help draft the Virginia Declaration of Rights. Thank This Forgotten Founding Father for Your ‘Pursuit of Happiness’ |Nick Fouriezos |September 27, 2020 |Ozy 

One day while we were outside playing, my mother called Mason’s mom and told me to come home immediately. How Outdoor Companies Can Back Up Their DEI Pledges |Kai Lightner |September 23, 2020 |Outside Online 

Adam Thierer is a senior research fellow with the Technology Policy Program at the Mercatus Center at George Mason University. Why Do ‘Progressives’ Want to Ban Uber and AirBnB? |Adam Thierer, Christopher Koopman |December 30, 2014 |DAILY BEAST 

Like Cohen, many of these casualties were white Democrats from below the Mason-Dixon Line. Southern Dems Won’t Rise Again |Ben Jacobs |December 5, 2014 |DAILY BEAST 

It was recorded as a little online extra while Mason and his crew waited to film a stand-up for the broadcast news program. UK Reporter’s Anti-Banker Rant Goes Viral |Nico Hines |November 13, 2014 |DAILY BEAST 

A network insider insisted: “No expletives were uttered by Mr Mason in the recording of his rant.” UK Reporter’s Anti-Banker Rant Goes Viral |Nico Hines |November 13, 2014 |DAILY BEAST 

The injuries, she says, only happened when Hawking and Mason were alone. The Other Side of Stephen Hawking: Strippers, Aliens, and Disturbing Abuse Claims |Marlow Stern |November 6, 2014 |DAILY BEAST 

A stone mason was employed to engrave the following epitaph on a tradesman's wife: "A virtuous woman is a crown to her husband." The Book of Anecdotes and Budget of Fun; |Various 

Mr. Mason was fond of children, and stooping down he kissed the child, who drew back and hid behind Jake. The Cromptons |Mary J. Holmes 

Something in her eyes attracted and fascinated, and at the same time troubled Mr. Mason, he scarcely knew why. The Cromptons |Mary J. Holmes 

As Mr. Mason finished the services and sat down, he was startled with an outburst of "Shall we meet beyond the river." The Cromptons |Mary J. Holmes 

Her voice was singularly sweet and full, and Mr. Mason said to himself, "She'll be a singer some day, if she is not crazy first." The Cromptons |Mary J. Holmes