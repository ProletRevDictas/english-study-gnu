Since 2017, Moscow has managed a permanent military base in Tartus and an air base in Khmeimim — both on Syria’s Mediterranean coast. Butterfly Effect: The Next U.S.-Russia Conflict Theater Could be the Mediterranean |Charu Kasturi |September 17, 2020 |Ozy 

Some are in rental units without subsidies and the largest group, 43 percent, are in permanent housing. Morning Report: Punished for Pissing Off Police |Voice of San Diego |September 10, 2020 |Voice of San Diego 

As Amazon’s first “permanent online-only” Whole Foods in Brooklyn underscores, they may wind up in what seem like the unlikeliest of places, too. That Whole Foods is an Amazon warehouse; get used to it |Connie Loizos |September 4, 2020 |TechCrunch 

If Nintendo does that clock feature right, I can see these things earning a permanent spot on a lot of people’s desks. Nintendo is remaking the first portable gaming system it ever built |Greg Kumparak |September 3, 2020 |TechCrunch 

Turns out the head injury had left some permanent aftereffects. The Accidental Attempted Murder |Eugene Robinson |September 2, 2020 |Ozy 

The offices were firebombed in 2011; no one was hurt but a permanent police car was subsequently stationed outside. France Mourns—and Hunts |Nico Hines, Christopher Dickey |January 8, 2015 |DAILY BEAST 

Plus, while sometimes IPs can be “permanent”, at other times IPs last just a few seconds. No, North Korea Didn’t Hack Sony |Marc Rogers |December 24, 2014 |DAILY BEAST 

In the past, Fidel Castro and his brother Raul were considered permanent enemies of Washington. Cuba Is A Kleptocracy, Not Communist |Romina Ruiz-Goiriena |December 19, 2014 |DAILY BEAST 

At least one parent would have to be a U.S. citizen or permanent resident. The Progressive Case Against Birthright Citizenship |Keli Goff |December 15, 2014 |DAILY BEAST 

They seemed like a permanent part of the mindscape, the way mountains or rivers are part of the physical world. Alfred Hitchcock’s Fade to Black: The Great Director’s Final Days |David Freeman |December 13, 2014 |DAILY BEAST 

Let them open their minds to us, let them put upon permanent record the significance of all their intrigues and manœuvres. The Salvaging Of Civilisation |H. G. (Herbert George) Wells 

Will this position be permanent or will its duration be limited practically to the period of the war? Readings in Money and Banking |Chester Arthur Phillips 

There is a sharp curve in the permanent way outside the station, so that a train is on you all of a sudden. Uncanny Tales |Various 

Loss, where she was concerned, involved a permanent and irremediable bereavement—no substitute was conceivable. The Wave |Algernon Blackwood 

Our troops have done all that flesh and blood can do against semi-permanent works, and they are not able to carry them. Gallipoli Diary, Volume I |Ian Hamilton