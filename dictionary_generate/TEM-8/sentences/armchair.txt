We have all had to learn to be armchair epidemiologists and now we have these new realities. Many who have received the coronavirus vaccine wonder: What can I safely do? |Laurie McGinley, Lenny Bernstein |February 1, 2021 |Washington Post 

As armies of armchair investors download brokerage apps, stock prices have looked increasingly detached from economic reality. The dark side of the democratization of trading |John Detrixhe |January 29, 2021 |Quartz 

We’ve selected the best stylish fabric armchairs for your living room from classic comfort, to chic swivel - perfect for the everyday and sophisticated enough for your special gatherings. The best fabric armchairs for your living room |PopSci Commerce Team |January 6, 2021 |Popular-Science 

The trails range in length from 54 miles to 5,000 and are perfect for socially distant adventures—whether you decide to hike, bike, or armchair-dream about them. 36 Awesome Gifts for Adventure Travelers |Mary Turner and Erin Riley |December 11, 2020 |Outside Online 

In fact, the armchair-travel nature of the postcards has led to the project’s most successful year yet. Not getting any travel postcards? A project in Mali delivers delight — and a lifeline. |Bailey Berg |December 3, 2020 |Washington Post 

“I love Christmas,” he begins in the film, smiling in an armchair. Kirk Cameron Saves Christmas from Abominable Killjoys (Other Christians) |Brandy Zadrozny |November 14, 2014 |DAILY BEAST 

He slept in an upright position in a custom armchair, so the reasons for his lying down to sleep are open to speculation. The True Story of ‘The Elephant Man’ |Russell Saunders |November 3, 2014 |DAILY BEAST 

Armchair sleuths have been competing for years to determine the identity of one of the most notorious serial killers. Jack the Ripper Is Still at Large |Christopher Moraff |September 29, 2014 |DAILY BEAST 

As a result, Akerman said CrowdMed may appeal to the more neurotic of us who wish to be “armchair physicians.” Strangers Diagnose Your Illness and Get Cash in Return |Kevin Zawacki |August 15, 2014 |DAILY BEAST 

Raoul Walsh saw him effortlessly carrying an armchair above his head and was captivated. A New Biography Shows That ‘John Wayne’ Was His Own Best Creation |Christopher Bray |April 6, 2014 |DAILY BEAST 

A big dictionary placed in an armchair, raised little Henrietta to the proper height at the Norwood dinner table. The Campfire Girls of Roselawn |Margaret Penrose 

Hans Fennefos entered, saluted Sarah, and at the same time inquired for whom the armchair was placed by her side. Skipper Worse |Alexander Lange Kielland 

He had been sitting at his ease in an armchair, over the back of which he had tossed the baldric from which his sword depended. St. Martin's Summer |Rafael Sabatini 

Over this pit is an armchair, to which the deceased bonze is fastened in full costume. A Woman's Journey Round the World |Ida Pfeiffer 

Scattergood went back to his hardware store and sat down in his reinforced armchair on the piazza. Scattergood Baines |Clarence Budington Kelland