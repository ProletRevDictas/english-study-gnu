The idea was to give families a bit of extra money as well as hire navigators who would help families understand the value of relocating to “high opportunity” neighborhoods, away from concentrated poverty and blight. Joe Biden’s surprisingly visionary housing plan, explained |Matthew Yglesias |July 9, 2020 |Vox 

If the algorithm chose one of the original faces, the value was recorded. How and Why Computers Roll Loaded Dice |Stephen Ornes |July 8, 2020 |Quanta Magazine 

Rather than mission statements, management directives, or corporate values printed on placards, organizational behavior is driven by peer pressure and behavioral norms that spread, like pathogens, through organizations. The coronavirus crisis is increasing the risk of bank fraud. Here’s how banks can play defense |matthewheimer |July 7, 2020 |Fortune 

The most reliable way to stop baselines from shifting is to encode the public’s values and aspirations into law and practice, through politics. The scariest thing about global warming (and Covid-19) |David Roberts |July 7, 2020 |Vox 

This powerful imagery surely ignites an emotional response in anyone who values life and is capable of empathy. Racism Is a National Security Problem |Daniel Malloy |June 16, 2020 |Ozy 

But there's a ton of value for me in my background and my history, and losing it would be a shame. My Week on Jewish Tinder |Emily Shire |January 5, 2015 |DAILY BEAST 

There is reference after reference to the “black community,” “black worth ethic,” and adherence to the “black value system.” Reverend Jeremiah Wright Was Worse Than Scalise |Ron Christie |January 2, 2015 |DAILY BEAST 

As Randy notes, “Maybe there is a value in shining a light on this and asking the questions.” Your Husband Is Definitely Gay: TLC’s Painful Portrait of Mormonism |Samantha Allen |January 1, 2015 |DAILY BEAST 

Canned drinks like Mercy contain up 5,000 percent of the daily value of certain vitamins. History's Craziest Hangover Cures |Justin Jones |December 30, 2014 |DAILY BEAST 

For me and some of my students, 2014 was the year of rediscovering old resources whose value is not exhausted yet. American Democracy Under Threat for 250 Years |Jedediah Purdy |December 28, 2014 |DAILY BEAST 

Other things being equal, the volume of voice used measures the value that the mind puts upon the thought. Expressive Voice Culture |Jessie Eldridge Southwick 

Of course the expression of this value is modified and characterized by the nature of the thing spoken of. Expressive Voice Culture |Jessie Eldridge Southwick 

Was a pupil of Caspar Netscher of Heidelberg, whose little pictures are of fabulous value. Women in the fine arts, from the Seventh Century B.C. to the Twentieth Century A.D. |Clara Erskine Clement 

He must trust to his human merits, and not miracles, for his Sonship is of no value in this conflict. Solomon and Solomonic Literature |Moncure Daniel Conway 

The living (value £250) is in the gift of trustees, and is now held by the Rev. M. Parker, Vicar. Showell's Dictionary of Birmingham |Thomas T. Harman and Walter Showell