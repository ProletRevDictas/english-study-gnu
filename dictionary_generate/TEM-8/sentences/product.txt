The company is currently shooting for a product launch in May of next year. Crover’s robot swims in grain silos to monitor environmental conditions |Brian Heater |September 17, 2020 |TechCrunch 

The financing will be used to grow InsideSherpa’s staff, with more engineering, product and sales roles. Forage, formerly InsideSherpa, raises $9.3 million Series A for virtual work experiences |Natasha Mascarenhas |September 17, 2020 |TechCrunch 

The product is multifunctional and has a ton of uses as a stand-alone product. Tower 28 Announces Winner Of The Clean Beauty Summer School Program |Hope Wright |September 17, 2020 |Essence.com 

Making the most of that work — translating a successful clinical product into real-world progress — will require some patience. The risks of moving too fast on a coronavirus vaccine |Sam Baker |September 17, 2020 |Axios 

She notes that Google does pollution monitoring and that IKEA has started buying up crop stubble, which is typically burned and a major source of pollution in India, to turn into products. Why fighting climate change is key to America’s health |Erika Fry |September 16, 2020 |Fortune 

Together, the teams are working 24 hours a day for a product that promises much higher risk than it does profit. The Race for the Ebola Vaccine |Abby Haglage |January 7, 2015 |DAILY BEAST 

The billionaire philanthropist tastes the product of a machine that processes human sewage into drinking water and electricity. Bill Gates Drinks Sewer Water |Jack Holmes, The Daily Beast Video |January 7, 2015 |DAILY BEAST 

Bitcoin began 2013 with a roaring price of $770 per unit, and businesses right and left were converting to the ethereal product. You Were Wrong About Miley & Bitcoin: 2014’s Failed Predictions |Nina Strochlic |December 31, 2014 |DAILY BEAST 

And, with Coca-Cola announcing the launch of a new milk product, the beverage could be back in our hands before we know it. History's Craziest Hangover Cures |Justin Jones |December 30, 2014 |DAILY BEAST 

The resulting product included four single-cask variants along with finished pictures of McKidd enjoying a glass of The Macallan. The Restaurant, Flask, And Photography Worthy of The Macallan Whisky | |December 16, 2014 |DAILY BEAST 

It is the principal waste-product of metabolism, and constitutes about one-half of all the solids excreted—about 30 gm. A Manual of Clinical Diagnosis |James Campbell Todd 

The product is then multiplied by the number of cubic centimeters voided in twenty-four hours and divided by 1000. A Manual of Clinical Diagnosis |James Campbell Todd 

I should judge that a peck of corn is about the average product of a day's work through all this region. Glances at Europe |Horace Greeley 

I suspect, from the evident care taken of it, that its product is considerably relied on for food. Glances at Europe |Horace Greeley 

Virginia leaf still continues to flourish, and to-day it is the great agricultural product of the State. Tobacco; Its History, Varieties, Culture, Manufacture and Commerce |E. R. Billings.