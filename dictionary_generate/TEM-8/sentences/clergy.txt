In this position, she’s been providing support to clergy who are on leave, become disabled or in spiritual crisis. Meet Rev. Bos, first out lesbian Evangelical Lutheran bishop |Kathi Wolfe |August 11, 2021 |Washington Blade 

There are anti-crime programs that might work but they need buy-in from the entire community including activists and the clergy who must work in concert with our political leadership. Opinion | Blame Mayor Bowser for violence epidemic? |Peter Rosenstein |July 28, 2021 |Washington Blade 

It was not unusual, Chapnin says, for people with an intelligence background to advise or even join the clergy. Exclusive: How an Accused Russian Agent Worked With Rudy Giuliani in a Plot Against the 2020 Election |Simon Shuster/Kyiv |May 28, 2021 |TIme 

Rather than make the clergy rethink its commitment to the fascist cause, the carnage just seemed to confirm their view that civilization itself was under threat. My Father Fled Fascism in Spain—and Taught Me How Lies Can Destroy a Democracy |Sebastian Junger |May 19, 2021 |Time 

Advocates, clergy and elected leaders celebrated the city holiday while pleading for full voting rights. Full House scheduled to vote Thursday on D.C. statehood |Meagan Flynn |April 16, 2021 |Washington Post 

As for the federal authorities, they have made themselves available but the clergy have not requested special protection. Mexico’s Priests Are Marked for Murder |Jason McGahan |January 7, 2015 |DAILY BEAST 

The Coalition is comprised of labor unions, anti-war activists, clergy, and so-called black empowerment groups. As Michael Brown Grand Jury Winds Down, Is Ferguson on the Brink of War? |Ron Christie |November 16, 2014 |DAILY BEAST 

African American clergy are getting in on the action as well. Can Ferguson Swing the Election? |Joshua DuBois |October 26, 2014 |DAILY BEAST 

Akin to the clergy receiving “The Call” from God himself, Minaj has been touched by a booty angel. Nicki Minaj’s Ass-tastic ‘Anaconda’ Video and the Curse of the Butt Career |Kevin Fallon |August 21, 2014 |DAILY BEAST 

There are supportive African (and African-American) clergy calling for coexistence rather than violence. The Uganda Ruling is Good For Everyone But Gays |Jay Michaelson |August 1, 2014 |DAILY BEAST 

He gives a list of the sponsors of the baptized Indians, who included many of the French nobility and clergy. The Jesuit Relations and Allied Documents, Vol. II: Acadia, 1612-1614 |Various 

Among the clergy therein he finds no offenses, save that a few have gambled in public; these are promptly disciplined. The Philippine Islands, 1493-1898, Volume XX, 1621-1624 |Various 

Visitation of the ecclesiastical cabildo, clergy of Manila, and province of Pampanga. The Philippine Islands, 1493-1898, Volume XX, 1621-1624 |Various 

The country clergy are without doubt the most over-rated persons in the country—I mean, of course, from a fiscal point of view. Punch, or the London Charivari, Volume 158, April 28, 1920 |Various 

This body consisted of the nobility, the higher clergy, and representatives, chosen by the people from all parts of France. Madame Roland, Makers of History |John S. C. Abbott