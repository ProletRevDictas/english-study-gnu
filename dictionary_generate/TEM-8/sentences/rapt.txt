Nothing had me more rapt than three of the Roy siblings spiraling while in a dusty parking lot. The Daily Beast’s 21 Best TV Shows of 2021: From ‘Squid Game’ and ‘Succession’ to ‘Sex Education’ |Kevin Fallon |December 20, 2021 |The Daily Beast 

KAYEKhan welcomed a rapt crowd of viewers — from consumer advocates and policy wonks to antitrust lawyers, tech analysts and live-tweeting reporters — in July to a rare FTC meeting that anyone in the public could watch virtually. Kill Your Algorithm: Listen to episode two of the podcast featuring tales from a more fearsome FTC |Kate Kaye |October 28, 2021 |Digiday 

Kai is rapt, prodding me to take more photos and reporting on the animal’s every move as I make dinner. A father-son backpacking trip in the Grand Canyon is an introduction to adventure |John Briley |April 9, 2021 |Washington Post 

Ali walked up and hugged Gil, and the pair discussed music and racism and current events before a rapt audience. ‘The Prince of Chocolate City’: When Gil Scott-Heron Became A Music Icon |Marcus Baram |November 15, 2014 |DAILY BEAST 

Golden Age-of-TV evangelists prate on about which glorified soap operas are most deserving of our rapt attention. America’s Meddlers Are Our Worst Enemies |Stefan Beck |October 3, 2014 |DAILY BEAST 

The crowd in Powell's was rapt—when it wasn't doubled over in laughter. Reza Aslan’s Sweet Revenge |Winston Ross |July 31, 2013 |DAILY BEAST 

The inside-the-Beltway press, dumbfounded by the strange partisan bedfellowship, paid rapt attention. Inside the Movement to Legalize Hemp |Jonathan Miller |May 14, 2013 |DAILY BEAST 

Iran's nuclear plan is progressing," he told a rapt audience, but added, "Iran is being careful not to cross any red lines. What The Head Of Israel's Military Intelligence Thinks Of Iran |Ali Gharib |March 14, 2013 |DAILY BEAST 

He walked on, and thought of the rapt liberty of the soul in the sweet serenities of beautiful solitude. The Pastor's Fire-side Vol. 3 of 4 |Jane Porter 

Lucy clapped her hands with delight, her more staid cousin was rapt in pleased astonishment. The Pit Town Coronet, Volume I (of 3) |Charles James Wills 

The exercises of the day soon commenced, and the old lady became wholly rapt in her devotional feelings. The Book of Anecdotes and Budget of Fun; |Various 

He stood watching the carriage rapt in meditation, and his face wore a puzzled air. The Pit Town Coronet, Volume II (of 3) |Charles James Wills 

Here Vincèn narrates a foot-race in which he took part at Nimes, and Mirèio listens in rapt attention. Frdric Mistral |Charles Alfred Downer