When Nancy Pelosi was speaker, the gold medal was authorized for golfer Arnold Palmer and conferred by Boehner. Obama’s Civil Rights Snub? |Eleanor Clift |June 24, 2014 |DAILY BEAST 

The House recently passed for the second time authorization to honor golfer Jack Nicklaus; the Senate has yet to act. Obama’s Civil Rights Snub? |Eleanor Clift |June 24, 2014 |DAILY BEAST 

One of my regular playing companions, a near-scratch golfer himself, once told me that three-quarters of duffers never break 100. Forget Bipartisan Golf, Obama, and Play Against Me |Michael Tomasky |May 26, 2014 |DAILY BEAST 

When a golfer is out there on the course, any new bet he makes is probably made with his own money, without the help of a backer. Portrait of the Consummate Con Man |John Lardner |May 17, 2014 |DAILY BEAST 

Billy Duffy once backed Titanic in a bet against a powerful amateur golfer, noted for his long drives. Portrait of the Consummate Con Man |John Lardner |May 17, 2014 |DAILY BEAST 

For a moment he wriggled his toes, just as a golfer waggles his driver preparatory to the stroke. Scattergood Baines |Clarence Budington Kelland 

He was slowly going to pieces, forgetting the invincible stoicism that is the pride of the true golfer. Murder in Any Degree |Owen Johnson 

Now he was only a golfer of one generation; there was nothing in his inheritance to steady him in such a crisis. Murder in Any Degree |Owen Johnson 

Each will go on with his interrupted job as Austin has snatched up his hose-pipe or the golfer continued his game. The Poison Belt |Arthur Conan Doyle 

For each guest a toy figure of a hunter, football player, golfer, prize fighter or any desired athlete could be used. Suppers |Paul Pierce