That some are inherently sinful and some are inherently saintly. Tucker Carlson is very mad at ‘evil’ critical race theory, whatever it is |Philip Bump |November 4, 2021 |Washington Post 

They have awoken to a country run by thugs who have banned women from public life, condemned the arts as sinful, and plan to return the nation's soccer stadiums to sights of mass public executions. ‘Last Chance to Leave’: The Fight to Get One Woman Out of Afghanistan |Diana Falzone |August 26, 2021 |The Daily Beast 

We call these undergarments “libido killers” in Cuba because they annihilate any “sinful” desire. Locked up in the Land of Liberty: Part II |Michael K. Lavers |July 14, 2021 |Washington Blade 

So will central themes such as the enduring sting of antisemitism and the push-pull between the sacred and the sinful. With ‘Antiquities,’ Cynthia Ozick is as vibrant on the page as ever |Diane Cole |April 16, 2021 |Washington Post 

We expect this judgement to be respected and that for once the oil corporations will accept the truth and bring their sinful flaring activities to a halt. Lawyers Are Working to Put 'Ecocide' on a Par with War Crimes. Could an International Law Hold Major Polluters to Account? |Mélissa Godin |February 19, 2021 |Time 

Alcohol and sugar, even in moderate amounts, are not only sinful but poisonous. How Taryn Toomey’s ‘The Class’ Became New York’s Latest Fitness Craze |Lizzie Crocker |January 9, 2015 |DAILY BEAST 

And now, as a part of the Fourth Judicial Circuit, North Carolina is about to have the sinful practice foisted on it. Who Are the Judicial Activists Now? |Michael Tomasky |October 7, 2014 |DAILY BEAST 

It is a festival of joy for being created this way after centuries of being told that we are sinful, loathsome, and disgusting. June, The Month When Pride Isn’t a Sin |Gene Robinson |June 22, 2014 |DAILY BEAST 

“They say that drinking is haram,” or sinful under Islamic law. With New Turkish Liquor Ban, Raki Goes Underground |Thomas Seibert |June 13, 2014 |DAILY BEAST 

The group takes its name from a phrase meaning “Western education is sinful.” Up to Speed: What’s Happening to Nigeria’s Kidnapped Girls? |Nina Strochlic |May 7, 2014 |DAILY BEAST 

He felt himself the meanest, vilest thing a-crawl upon this sinful earth, and she—dear God! St. Martin's Summer |Rafael Sabatini 

Aristide lived on bread and cheese, and foresaw the time when cheese would be a sinful luxury. The Joyous Adventures of Aristide Pujol |William J. Locke 

The temple of God shall not protect a sinful people, without a sincere conversion. The Bible, Douay-Rheims Version |Various 

He should not consider himself to be called upon to prohibit only some practices clearly evinced to be sinful. The Ordinance of Covenanting |John Cunningham 

A vow may sometimes be sinful, notwithstanding the use of the utmost care to make it in consistency with the calls of duty. The Ordinance of Covenanting |John Cunningham