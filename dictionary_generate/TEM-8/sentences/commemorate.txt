In 1964, the plaque commemorating their stay was placed on the wall by Lincoln’s bust, recalling the early instance when troops were quartered there. Troops lodged in the Capitol in 1861. It was a wreck when they left. |Meryl Kornfield, Felicia Sonmez |January 14, 2021 |Washington Post 

The sideline consultation between Pederson and Foles for that famed play-call was commemorated with a statue outside Lincoln Financial Field. Doug Pederson ousted as coach of Philadelphia Eagles, less than three years after Super Bowl victory |Mark Maske |January 12, 2021 |Washington Post 

The city strung a 6-foot-high, 21-foot-long banner across three columns of City Hall commemorating the division title, and fans have driven from miles away just to snap a picture in front of it. ‘Bills Mafia’ waited a generation for a team like this. It’s had to embrace it from afar. |Adam Kilgore |January 7, 2021 |Washington Post 

This time last year we were commemorating the end of a decade and looking ahead to the next one. These Were Our Favorite Tech Stories From Around the Web in 2020 |Singularity Hub Staff |January 2, 2021 |Singularity Hub 

National Park Week first started in 1991 to commemorate the 75th anniversary of the NPS. Every Day that the National Parks Are Free in 2021 |Emily Pennington |December 30, 2020 |Outside Online 

At one point, Turness suggested that Gregory have a live band close out the show to commemorate the death of Nelson Mandela. David Gregory's 'Meet the Press' Eviction Exposed in Washingtonian Takedown |Lloyd Grove |December 23, 2014 |DAILY BEAST 

So, to commemorate her 75th birthday, the two got matching butterfly tattoos on their wrists. Masters of Alt Sex: SuicideGirls Hits Puberty and Wants to Invade Your TV Set |Marlow Stern |December 9, 2014 |DAILY BEAST 

Instead, the Giglese will commemorate the shipwreck with a prayer service on Jan. 13 every year, and nothing more. Saying Goodbye to the Salvage Saviors of Giglio |Barbie Latza Nadeau |July 21, 2014 |DAILY BEAST 

Nearly 10,000 people, most of them freedmen, gathered in the freshly landscaped burial ground to commemorate the dead. The Real Memorial Day: Oliver Wendell Holmes's Salute To A Momentous American Anniversary |Malcolm Jones |May 26, 2014 |DAILY BEAST 

Now, the date is used to commemorate those who have died in all conflicts. William and Kate Bid Farewell To Australasia |Tom Sykes |April 24, 2014 |DAILY BEAST 

It is not likely that the inhabitants of Ivrea, who thus commemorate her heroic deed, will ever forget their Mugnaia. The Pit Town Coronet, Volume I (of 3) |Charles James Wills 

It is 421 proposed to commemorate the incident by the erection of a suitable pile. The Works of Robert Louis Stevenson - Swanston Edition Vol. 25 (of 25) |Robert Louis Stevenson 

I remember the leading Dissenters came to Bungay with a piece of plate, to present to Mr. Childs, to commemorate his heroism. East Anglia |J. Ewing Ritchie 

First, though, I'd like to present you a decoration to commemorate your part in this skirmish, Wes. The Great Potlatch Riots |Allen Kim Lang 

With what grander monument could one commemorate his little span on earth? Walnut Growing in Oregon |Various