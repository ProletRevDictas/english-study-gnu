She then joined her sister at her small dispensary, where she provided poor women medications, health advice and referrals to charities. Determined to practice medicine, two sisters defied conventions |Janet Golden |February 5, 2021 |Washington Post 

So she drove to Hollywood and pulled into a parking space outside a dispensary. Sarah Silverman just wants to make things right |Geoff Edgers |February 4, 2021 |Washington Post 

Projections show Lemon Grove taking in about $350,000 annually from the sale of medicinal cannabis from every operating dispensary, with one business currently open and another slated to open in early 2021. Lemon Grove Once Again Facing Bitter Choices Over Its Future |Bella Ross |January 8, 2021 |Voice of San Diego 

Stockton University, SUNY Morrisville and Colorado State University at Pueblo offer undergraduate minors to prepare students for careers in dispensary operations, manufacturing and law. Medical cannabis master’s program at University of Maryland sees growth since launch |Tracy Mitchell Griggs |December 18, 2020 |Washington Post 

Founded in 2014, the family-owned business operates dispensaries rooted in a patient-centric approach. Curio Wellness launches $30M fund to help women and minorities own a cannabis dispensary |Matt Burns |November 30, 2020 |TechCrunch 

This year, the dispensary is launching its sale on Thanksgiving, carrying over into Black Friday. Colorado Weed Dispensaries Celebrate ‘Green Friday’ |Abby Haglage |November 28, 2014 |DAILY BEAST 

Thus far, the most talked-about Green Friday hotspot is Denver dispensary the Grass Station. Colorado Weed Dispensaries Celebrate ‘Green Friday’ |Abby Haglage |November 28, 2014 |DAILY BEAST 

Few applicants had a location nailed down to accommodate their indoor cultivation and dispensary needs. Weed Cops Blaze New Trail |Valerie Vande Panne |March 4, 2014 |DAILY BEAST 

But the dispensary business is built on fictions that make it almost inherently non-law-abiding. Tax Cuts for Pot Law-Breakers? |David Frum |September 13, 2013 |DAILY BEAST 

One day it did accompany her to the knowledge dispensary, Which to every rule and precedent was recklessly contrary. The Book of Humorous Verse |Various 

Aided by the dean and chapter he established the first dispensary for the poor at Carlisle. Bell's Cathedrals: The Cathedral Church of Carlisle |C. King Eley 

Why, my father kept on his dispensary in the days when the practice was at its best. A Houseful of Girls |Sarah Tytler 

From that moment the travelling dispensary was locked up, and the doctor was the only one to profit by its contents. More Tales by Polish Authors |Various 

The dispensary doctor dines off a table which once graced the parlour of a parish priest. Hyacinth |George A. Birmingham