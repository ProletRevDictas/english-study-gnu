Canada’s oilsands are especially dirtyFossil fuels are hydrocarbons, which is just the chemical term for a molecule made up of hydrogen and carbon. Keystone XL was supposed to be a green pipeline. What does that even mean? |Juliet Grable |February 5, 2021 |Popular-Science 

The process produces hydrocarbon gases too, which could be used to generate heat to run the reaction at a recycling plant, Scott says. Chemists are reimagining recycling to keep plastics out of landfills |Maria Temming |January 27, 2021 |Science News 

The resulting hydrocarbon molecules can serve as a jet fuel. A new catalyst turns greenhouse gas into jet fuel |Maria Temming |January 27, 2021 |Science News For Students 

The fossil fuels they burn to create all these emissions are hydrocarbons, which means they are made up of a combination of carbon and hydrogen. Scientists Just Created a Catalyst That Turns CO2 Into Jet Fuel |Edd Gent |January 3, 2021 |Singularity Hub 

There are many good alternatives now to burning fossilized hydrocarbons. Why Birds Can Fly Over Mount Everest - Issue 94: Evolving |Walter Murch |December 30, 2020 |Nautilus 

The allegation concerns hydrocarbon releases from the wastewater system at the unloading facility. Oil Tankers Leaking into Seattle’s Water |Bill Conroy |October 13, 2014 |DAILY BEAST 

Its offshore territory is part of the hydrocarbon-rich Levant Basin. Israel Looks to Eurasia to Achieve Top Energy and National Security Goals |Allison Good |October 11, 2013 |DAILY BEAST 

The hydrocarbon bonanza in the United States gives Obama a significant opportunity. A Foreign-Policy Cheat Sheet for Obama |Martin Indyk |January 17, 2013 |DAILY BEAST 

That horizontal segment allows the well to have far greater exposure to the hydrocarbon reservoir. Both Candidates Push Myth of Energy Independence |Robert Bryce |November 1, 2012 |DAILY BEAST 

In Denmark, the poster child for wind power, neither carbon dioxide emissions nor hydrocarbon consumption have been reduced. A Centrist Gets Fighting Mad |Mark McKinnon |October 13, 2010 |DAILY BEAST 

Chemically, the camphors may be divided into two main groups, according to the nature of the corresponding hydrocarbon or terpene. Encyclopaedia Britannica, 11th Edition, Volume 5, Slice 2 |Various 

One of the chemical compounds here indicated is a hydrocarbon similar to that found in comets. A Text-Book of Astronomy |George C. Comstock 

Hence it is a hydrocarbon of the terpene series, having the general formula CnH2n-4. Scientific American Supplement, No. 460, October 25, 1884 |Various 

The undissolved hydrocarbon is similarly purified by fractional distillation, and furnishes the solid crystalline naphthalene. Coal |Raphael Meldola 

The greater portion of the hydrocarbon is contained in the carbolic oil, and is separated and purified in the manner described. Coal |Raphael Meldola