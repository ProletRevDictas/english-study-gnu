The Chicago-based restaurant chain’s entry into the Soviet Union, nearly two years before the fall of the communist state in December 1991, was anything but straightforward and reportedly followed 14 years of tortuous negotiations. As Starbucks Exits Russia, Another Symbol of American Capitalism Fades |Eloise Barry |May 24, 2022 |Time 

Failure, as seen in a previously tragic case, could lead to a tortuous death. After First Pig-to-Human Heart Transplant, Scientists Aim to Make It Routine |Shelly Fan |January 25, 2022 |Singularity Hub 

Further study of their chemistry revealed a tortuous journey that included several phases of cooling, crystalizing, heating, and remelting. The Ghost of Ancient Earth’s Magma Oceans Found in Greenland Rocks |Jason Dorrier |March 14, 2021 |Singularity Hub 

Wicker’s tortuous justification reflects the difficulty Republicans are having in finding a message to counter the popularity of the legislation. The Democrats’ coronavirus relief package is popular. Republicans hope to dent it. |Marianna Sotomayor, Mike DeBonis |March 12, 2021 |Washington Post 

All of the various things I do I love doing, and occasionally I find them tortuous. Bob Balaban: How I Write |Noah Charney |February 5, 2014 |DAILY BEAST 

In 2011, after tortuous negotiations, a deal was struck for $1.2 trillion in spending cuts over 10 years. Markets Approve of the Way U.S. Political System Handles Debt Problems |Robert Shapiro |January 3, 2013 |DAILY BEAST 

So why take the tortuous and offensive back-door route to appreciating Wharton? Why Jonathan Franzen Can’t Appreciate Edith Wharton |Marina Budhos |March 1, 2012 |DAILY BEAST 

The Quetta Shura official says the negotiations with the French were tortuous. How the Taliban’s Hostages Were Freed |Sami Yousafzai, Ron Moreau |July 1, 2011 |DAILY BEAST 

Now, negotiations will be much slower and more tortuous, but likely will begin before 2010. Hillary's Tricky Iran Game |Leslie H. Gelb |June 13, 2009 |DAILY BEAST 

Long, tortuous lines of light showed immense numbers of large fish darting about as if in consternation. Gospel Philosophy |J. H. Ward 

It is in threading these tortuous windings that many a fearless venturer has lost foot-hold and been utterly cast away. A Cursory History of Swearing |Julian Sharman 

A magnificent view is had from Sparrow Hill; the ascent is made by a steep and tortuous road. Ways of War and Peace |Delia Austrian 

The river gradually became narrower and more tortuous as we approached its head waters. Famous Adventures And Prison Escapes of the Civil War |Various 

This defile was extremely tortuous, and was never without water even in the low tides. Toilers of the Sea |Victor Hugo