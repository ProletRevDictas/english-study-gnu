What we have now is a system which gives naming rights to the discoverers in an implicit way, where your contributions will bear your names by default, unless you decide to agitate for something else. Why Mathematicians Should Stop Naming Things After Each Other - Issue 89: The Dark Side |Laura Ball |September 2, 2020 |Nautilus 

Bazzo also said the district should focus on implicit bias training for staff, doing more restorative justice programs and bringing more teachers of color into classrooms. School Leaders Can’t Suspend the Discipline Discussion Any Longer |Will Huntsberry |July 16, 2020 |Voice of San Diego 

It also cited implicit and explicit bias and cultural competency. Agencies Are Updating Policies to Comply With New Use-of-Force Standards |Jesse Marx |June 22, 2020 |Voice of San Diego 

Many times, that bias is implicit — meaning the people who are biased don’t know they are. Let’s learn about bias |Bethany Brookshire |June 3, 2020 |Science News For Students 

You can find out about some of yours by taking an implicit bias test from Harvard University. Let’s learn about bias |Bethany Brookshire |June 3, 2020 |Science News For Students 

As Randy wrote, “I guess this speaks to the church not really having a place for gay people so getting married is still implicit.” Your Husband Is Definitely Gay: TLC’s Painful Portrait of Mormonism |Samantha Allen |January 1, 2015 |DAILY BEAST 

Think about it: Dodd-Frank was explicitly passed to drive a stake through the heart of the implicit concept of “too big to fail.” How Naive is Elizabeth Warren? |Nick Gillespie |December 18, 2014 |DAILY BEAST 

The question implicit in this effort, “If you were starting a museum, what would you put in your collection?” The Best Coffee Table Books of 2014 |Robert Birnbaum |December 13, 2014 |DAILY BEAST 

By the implicit laws of capitalism, I'm pretty sure this makes Bieber the new commander in chief. Justin Bieber Isn’t Even 21, Yet Makes More Money Than Meryl Streep |Amy Zimmerman |November 25, 2014 |DAILY BEAST 

It is part and parcel of the implicit politics of Snap Judgment, which folds the margins of American society into its center. NPR’s Smooth-Talking Millennial Whisperer |Batya Ungar-Sargon |October 7, 2014 |DAILY BEAST 

We are told their ideas of government consist in believing that implicit obedience is due both to king and priests. Journal of a Voyage to Brazil |Maria Graham 

Let us, then, rejoice that we possess such a powerful advocate in heaven, and let us place implicit trust in her. Mary, Help of Christians |Various 

In his family his only use is to be a pattern of timid silence and of implicit submission. The 'Characters' of Jean de La Bruyre |Jean de La Bruyre 

Implicit and immediate obedience he demandedno questioning of his higher authority. Those Dale Girls |Frank Weston Carruth 

Implicit faith has been the source of the greatest outrages which have been committed upon the earth. Superstition In All Ages (1732) |Jean Meslier