If you multiply two polynomials, some terms might cancel out, but the term with the highest exponent will always survive the cancellation process. Mathematician Disproves 80-Year-Old Algebra Conjecture |Erica Klarreich |April 12, 2021 |Quanta Magazine 

For computer scientists and mathematicians, opinions about “exponent two” boil down to a sense of how the world should be. Matrix Multiplication Inches Closer to Mythic Goal |Kevin Hartnett |March 23, 2021 |Quanta Magazine 

If exponent two is achievable, then it’s possible to carry out matrix multiplication as fast as physically possible. Matrix Multiplication Inches Closer to Mythic Goal |Kevin Hartnett |March 23, 2021 |Quanta Magazine 

Since then mathematicians and computer scientists have jockeyed to lower the exponent further. New Algorithm Breaks Speed Limit for Solving Linear Equations |Kevin Hartnett |March 8, 2021 |Quanta Magazine 

She established an institute that conducted classes, wrote a book on the practice, and became one of its chief exponents in the United States. Marie Mongan, champion of hypnobirthing, dies at 86 |Olesia Plokhii |February 11, 2021 |Washington Post 

Brilliant as an exponent of the virtues in Spenser, Dante, Chaucer, Lewis could not write his own poetry. The Odd Story of C.S. Lewis, an Extremely Odd Man |A.N. Wilson |March 10, 2013 |DAILY BEAST 

The biggest laugh was claimed by the writer, actor and gay-rights exponent, Stephen Fry. The Hitchens Memorial Service |David Frum |April 21, 2012 |DAILY BEAST 

Not as an exponent of entertainment, but as part of the group having a pleasant, homey evening. My Wonderful Visit |Charlie Chaplin 

It is this also which renders the dictionary meaning of a word, by universal remark so imperfect an exponent of its real meaning. A System of Logic: Ratiocinative and Inductive |John Stuart Mill 

Nothing came of the plan, which is a pity, as each was a supreme exponent of his point of view. The Letters of Ambrose Bierce |Ambrose Bierce 

Sherman was the chief Union exponent of the tactical gift that makes marches count as much as fighting. The Civil War Through the Camera |Henry W. (Henry William) Elson 

All this prepared the way for the advent of Margaret Fuller, and brought about the condition of which she was the exponent. The College, the Market, and the Court |Caroline H. Dall