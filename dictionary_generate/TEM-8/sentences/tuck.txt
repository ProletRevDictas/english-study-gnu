On the downhills, ski mountaineers don’t get into as deep a tuck as alpine skiers, both because the terrain is a lot more uneven and unpredictable and, let’s be honest, because their legs are still shot from the uphill. The Complex Physiology of Ski Mountaineering |mmirhashem |November 13, 2021 |Outside Online 

First flipSecond flipBiles said she chose to flip in a piked position, rather than a tuck, because it is easier for her to grab her legs. Yurchenko and beyond: The origins of Simone Biles’s revolutionary vault |Emily Giambalvo |June 4, 2021 |Washington Post 

Even in the 1980s, Yurchenko attempted double tucks when she vaulted over stacked mats into the pit. Yurchenko and beyond: The origins of Simone Biles’s revolutionary vault |Emily Giambalvo |June 4, 2021 |Washington Post 

First flip Second flip Biles said she chose to flip in a piked position, rather than a tuck, because it is easier for her to grab her legs. Yurchenko and beyond: The origins of Simone Biles’s revolutionary vault |Emily Giambalvo |June 4, 2021 |Washington Post 

Pre-pandemic, Cassileth says business was good as patients had more disposable income, and were excited to do popular procedures like liposuction, breast implants, and tummy tucks. The Zoom effect: Why plastic surgery and cosmetic procedures might be more popular because of the pandemic |Rachel King |October 3, 2020 |Fortune 

They saw the light years ago and now many do a healthy nip-and-tuck business, especially on noses. The New World of Anti-Aging Dentistry |Kent Sepkowitz |June 4, 2014 |DAILY BEAST 

In 1966 a man called Dick Tuck stood as a Democrat in the California Senate elections. Will Jargon Be the Death of the English Language? |The Telegraph |March 30, 2014 |DAILY BEAST 

But did she really deserve all the nip-and-tuck hate-tweeting? Should We Give Kim Novak a Break on the Oscar Plastic Surgery Hate-Tweeting? |Lizzie Crocker |March 6, 2014 |DAILY BEAST 

She will then, at a time of her choosing, tuck it, say, into my carry-on bag just before I leave on a research trip. How I Write: Erik Larson Revisits ‘Isaac’s Storm’ |Noah Charney |October 31, 2012 |DAILY BEAST 

Romney is like the sheriff of Nottingham: all castle, no conviction; which makes Newt Gingrich the earthy Friar Tuck. Romney and Gingrich Set the GOP on a Path Toward Self-Destruction |John Batchelor |January 30, 2012 |DAILY BEAST 

And, old ink pot, tuck a horse blanket under my chin, and rub me down with brickbats while I feed! The Book of Anecdotes and Budget of Fun; |Various 

I pick up the newspaper that had fallen under the seat, spread it over his legs, and tuck the ends underneath. Prison Memoirs of an Anarchist |Alexander Berkman 

It is to guard against this that so many of his compatriots tuck their napkins in at their necks. Punch, or the London Charivari, Vol. 147, November 4, 1914 |Various 

Ive seen a lot of booze-fighters, and helped tuck some of them underground, but I never saw any rum hound just like this guy. The Woman Gives |Owen Johnson 

I don't think you've grown much, Francie—and oh, by-the-bye, I believe there's a tuck that could be let down.' Robin Redbreast |Mary Louisa Molesworth