After nine months of isolation and lockdowns and more than 300,000 deaths, it is difficult to find superlatives to fit this moment. Does It Matter Which COVID-19 Vaccine You Get? |Maggie Koerth (maggie.koerth-baker@fivethirtyeight.com) |December 18, 2020 |FiveThirtyEight 

Hill, superlative quarterback Patrick Mahomes and the Kansas City Chiefs scorched the field where the next Super Bowl will be played, holding off a vintage Tom Brady-led comeback for a 27-24 victory over the Tampa Bay Buccaneers. Tom Brady rallies the Bucs, but Patrick Mahomes, Tyreek Hill show why Chiefs are NFL’s best |Eric Adelson |November 30, 2020 |Washington Post 

When he discovered tentacled snakes are born knowing how to strike at prey rather than learning through failure, Catania recalls that he couldn’t “find enough superlatives to sum up these results.” ‘Great Adaptations’ unravels mysteries of amazing animal abilities |Erin Garcia de Jesus |October 12, 2020 |Science News 

Some have gone on to become superlative research mathematicians. At the Math Olympiad, Computers Prepare to Go for the Gold - Facts So Romantic |Kevin Hartnett |September 22, 2020 |Nautilus 

Despite my aversion to superlatives, I consider communication to be one of the most difficult, important, and ubiquitous activities. Book recommendations from Fortune’s 40 under 40 in tech |Rachel King |September 4, 2020 |Fortune 

In fact, the recent outpouring of travel superlative lists is already targeting the next best thing. Next Stop, Quito: Our Top Cities for 2015 |Brandon Presser |December 19, 2014 |DAILY BEAST 

You will have to forgive him for labeling more than one destination in such a superlative fashion. The Nile: Where Ancient and Modern Meet |William O’Connor |June 21, 2014 |DAILY BEAST 

The interview illustrates that, among all her other superlative qualities, is a remarkable level of self awareness. 11 Angelina Jolie Quotes From Her 'Maleficent' Q&A Proving She's Perfect |Marina Watts |March 14, 2014 |DAILY BEAST 

Together, we decide that the drink should be called a Cucumber Superlative. The Absinthe-Minded Porteños of Buenos Aires |Jeff Campagna |March 10, 2014 |DAILY BEAST 

Floyd Landis was a superlative natural talent, even more so than Armstrong ever was. Dope on Wheels: Speed Read of 'Wheelmen' About Lance Armstrong |Thomas Flynn |October 16, 2013 |DAILY BEAST 

I am told that it was just when I was on the point of leaving that I received your superlative epistle about the cricket eleven. The Works of Robert Louis Stevenson - Swanston Edition Vol. 25 (of 25) |Robert Louis Stevenson 

It must, however, be ascribed chiefly to the moment of its presentation rather than to any superlative merit in the drama. Spanish Life in Town and Country |L. Higgin and Eugne E. Street 

All are quarried out upon a superlative scale of magnitude, and every one of them is a marvel. Birds and All Nature, Vol. VI, No. 3, October 1899 |Various 

It seemed the superlative compliment, though he valued feminine brains and ability. Historic Fredericksburg |John T. Goolrick 

There should be a superlative form of perfect for a day like this! Patchwork |Anna Balmer Myers