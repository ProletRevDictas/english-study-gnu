“If it weren’t for a pandemic and potential economic calamity, we could really get excited,” Barton, Zillow’s chief executive, said on the company’s last earnings call. Behind real estate’s surprise 2020 boom and what comes next |Rachel Schallom |October 20, 2020 |Fortune 

Many are ginning up new business models and adopting new technologies that will, they hope, make their businesses more resilient to future calamities—assuming they survive this one. Momofuku CEO: Restaurants ‘need to diversify’ after COVID |rhhackettfortune |October 13, 2020 |Fortune 

A possible figure of some fun for nonbelievers pre-2020, Rawles’ stature and standing now, post every single calamity that’s struck this year, is damned near mythical, and it’s harder to get ahold of him than your average celebrity. A Prepper’s Secrets to Surviving What’s Left of 2020 |Eugene Robinson |September 21, 2020 |Ozy 

Tesla’s stock decline also coincides with major reversal in the Dow Jones and Nasdaq indices, which had been on a tear this year despite the economic calamity caused by the coronavirus pandemic. Tesla’s stock has plunged 34% in one week |Danielle Abril |September 8, 2020 |Fortune 

Likewise Nick Goulette, executive director of the Watershed Research and Training Center, has seen too little movement for too long to believe anything but utter calamity can get us back on track. They Know How to Prevent Megafires. Why Won’t Anybody Listen? |by Elizabeth Weil |August 28, 2020 |ProPublica 

Calamity,” Roth writes elsewhere, “when it comes, comes in a rush. American Dreams: How Bush Shaped Our Reading of Roth’s ‘The Plot Against America’ |Nathaniel Rich |November 23, 2014 |DAILY BEAST 

Corruption, suspicion, and a lack of doctors all add up to a growing calamity in Freetown. In Sierra Leone, the Plague Is Closing in Around Us |Ned Eustace |October 13, 2014 |DAILY BEAST 

Opposingly, White believed it would take cathartic calamity to trigger meaningful change. They Saw Our Sports Problem Coming |T.D. Thornton |September 14, 2014 |DAILY BEAST 

These facts cast a new nightmarish tint to an already overwhelming public health calamity. Ebola Might Be Sexually Transmitted |Kent Sepkowitz |September 4, 2014 |DAILY BEAST 

This weekend these war memories now flow into another historical calamity, the outbreak of World War I, 100 years ago. Life Under Air Strikes: Children Under Fire Will Never Forget — or Forgive |Clive Irving |August 3, 2014 |DAILY BEAST 

She waited for no further formalities, but shaken with the sure foreboding of calamity, turned and fled the room. Ancestors |Gertrude Atherton 

Ethel's understanding perceived, but her mind refused to grasp the extent of the calamity. The Daisy Chain |Charlotte Yonge 

The destruction of Moab is near to come: the calamity thereof shall come on exceeding swiftly. The Bible, Douay-Rheims Version |Various 

I became keenly aware of the dreadful psychic calamity it involved. Three More John Silence Stories |Algernon Blackwood 

Owing to their strange appearance, comets were to the ancients omens of calamity. Outlines of the Earth's History |Nathaniel Southgate Shaler