Napa cabbage is also sometimes called celery cabbage, perhaps an indication of a similar flavor profile, and bok choy reminds some people of spinach. Cabbage is always there for you. Here’s how to give it the respect it deserves. |Aaron Hutcherson |February 19, 2021 |Washington Post 

Tournedos Rossini — blushing beef tenderloin paired with foie gras — rests on sauteed spinach alongside buttery potato puree. Fresh reasons to return to 1789, one of D.C.’s oldest restaurants |Tom Sietsema |February 19, 2021 |Washington Post 

Swiss chard, though not actually from Switzerland but from the Mediterranean, is a beautiful vegetable with bright stems and deep-green leaves, quite as easily used as spinach. Swiss chard’s versatility as a leafy green shines in these 4 recipes |Kari Sonde |February 18, 2021 |Washington Post 

Over the past weekend, the FDA added baby spinach to the list, another common culprit, for potential Salmonella contamination. Poopy salad greens still plague public health |Beth Mole |November 30, 2020 |Ars Technica 

Each type of seaweed can be prepared differently, from drying and eating as a snack to preparing like spinach, making into tea, seasoning meat, and making sauces. 14 wild edibles you can pull right out of the ocean |By Bob McNally/Field & Stream |October 19, 2020 |Popular-Science 

How is life worth living without spinach, tender asparagus, or purple-sprouting broccoli? Brits Are Very Fussy Eaters |Emma Woolf |August 5, 2014 |DAILY BEAST 

It was my job to cook the vegetables, one of which was creamed spinach. A Real-Life ‘Downton Abbey’ Affair |Margaret Powell |January 13, 2013 |DAILY BEAST 

I should also think about having a protein shake with kale and spinach for breakfast every morning. Can Daniel Amen Read Your Mind? |Eliza Shapiro |December 14, 2012 |DAILY BEAST 

I want more spinach and less sugar in this big meal we give viewers. Ann Curry's Rocky Ride |Howard Kurtz |June 20, 2012 |DAILY BEAST 

But regular broccoli will do; also rough greens—spinach, kale, dandelion greens, Swiss chard. Overrated/Underrated: Food, Glorious and Otherwise |Michael Tomasky |June 1, 2012 |DAILY BEAST 

Here Justin entered with a steaming bowl of stewed moose meat and prairie spinach. Menotah |Ernest G. Henham 

Vegetables, however, of any kind are very scarce, though in the summer a species of spinach can be got in some places. Mount Everest the Reconnaissance, 1921 |Charles Kenneth Howard-Bury 

Make a mound of spinach pure in the centre of the dish, and place the pigeons around, standing up against the pure. Dressed Game and Poultry la Mode |Harriet A. de Salis 

Even years afterward Peter could never look at spinach without blinking. The Boy Grew Older |Heywood Broun 

Lake tried the edible herbs and found them to be something like spinach in taste. Space Prison |Tom Godwin