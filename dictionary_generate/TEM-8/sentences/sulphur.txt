Here are the best hikes to sulphur pools in the United States. The 25 Best Hikes to Hot Springs in the U.S. |eriley |January 31, 2022 |Outside Online 

Let blue flames rise from the living sulphur and the sheep bleat loud as she feels the touch of the smoking sulphur. The Religion of Ancient Rome |Cyril Bailey 

Thou who didst teach weak men and those who suffer To mix saltpetre and sulphur,Have pity on my long misery! Charles Baudelaire, His Life |Thophile Gautier 

Ain't got used to brimstone yet, but I'd trade mosquitoes for sulphur smoke and give some boot. Cabin Fever |B. M. Bower 

Found enormous ledge of black quartz, looks like sulphur stem during volcanic era but may be iron. Cabin Fever |B. M. Bower 

The fumes of the sulphur have the effect of destroying the color, or whitening the straw. The Ladies' Book of Etiquette, and Manual of Politeness |Florence Hartley