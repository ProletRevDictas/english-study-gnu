And yet, although we know it to be a mere delusion, we all idealise and idolise our childhood. Eric, or Little by Little |Frederic W. Farrar 

All my intelligence and being have turned to spirit, to idolise you. Juliette Drouet's Love-Letters to Victor Hugo |Louis Guimbaud 

The children idolise him, and so indeed does the whole neighbourhood. The Posthumous Papers of the Pickwick Club, v. 2(of 2) |Charles Dickens 

Fred had been married ten months, and appeared to idolise his wife. Original Penny Readings |George Manville Fenn 

Before it grew too hot, they took me out to see the barracks and a ramshackle old fieldpiece which they seemed to idolise. Oriental Encounters |Marmaduke Pickthall