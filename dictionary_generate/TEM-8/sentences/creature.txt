The dolphinlike creature was nearly 5 meters long, about the length of a canoe. This ichthyosaur died after devouring a creature nearly as long as itself |Maria Temming |August 20, 2020 |Science News 

We may now be in a world where in-person events are a rarity, but that hasn’t curbed our desire for gatherings as we’re naturally social creatures. An SEO’s guide to event schema markup |Paul Morris |August 14, 2020 |Search Engine Watch 

It keeps genes in the pool that might not be of use today, but might save a creature’s descendants from plagues, pestilence, and parasites. Sex Is Driven by the Impetus to Change - Issue 88: Love & Sex |Jill Neimark |August 12, 2020 |Nautilus 

During the Blob from 2015–2016, some creatures may have traveled more than 2,000 kilometers. Species may swim thousands of kilometers to escape ocean heat waves |Carolyn Gramling |August 10, 2020 |Science News 

We are all creatures of habit, and shopping is largely habit-driven. There are only a few moments in life when buying habits change, and a pandemic is one of them |Marc Bain |August 8, 2020 |Quartz 

Their logic: the sea-creature would come alive and drink up any remaining alcohol. History's Craziest Hangover Cures |Justin Jones |December 30, 2014 |DAILY BEAST 

Exactly when the transition to modern domestic creature took place, for a bird that is wild to this day, is controversial. The History of the Chicken: How This Humble Bird Saved Humanity |William O’Connor |December 27, 2014 |DAILY BEAST 

And the Gävle Goat, apparently a sensitive creature, took the destruction hard. Sweden’s Burning Christmas Goat |Nina Strochlic |December 25, 2014 |DAILY BEAST 

Indeed, Dr. Shaheed has noted that Rouhani has only “limited authority” to change the system of which he is a creature. Iran’s Horrific Human-Rights Record |Sen. Mark Kirk, Sen. Marco Rubio |November 7, 2014 |DAILY BEAST 

Pillay used the 747 to deliver creature comforts, particularly for business travelers, that were previously unheard of. The Sexy Dream of the 747 |Clive Irving |October 26, 2014 |DAILY BEAST 

He was the strangest-looking creature Davy had ever seen, not even excepting the Goblin. Davy and The Goblin |Charles E. Carryl 

Some of the alarm returned, however, when the creature attempted to climb up by his own ladder. The Giant of the North |R.M. Ballantyne 

While Benjy sat contemplating this creature, and wondering what was to be the end of it all, a bright idea occurred to him. The Giant of the North |R.M. Ballantyne 

That poor, pretty creature, starving, in her charming pink dress and hat of roses. Rosemary in Search of a Father |C. N. Williamson 

To hear the creature talk about it makes my mouth as a brick kiln and my flesh as that of a goose. The Joyous Adventures of Aristide Pujol |William J. Locke