At the moment, more hyper-realistic deepfakes are far more technically challenging and computationally expensive to create. Memers are making deepfakes, and things are getting weird |Karen Hao |August 28, 2020 |MIT Technology Review 

It’s been a long time since the search engine market was a realistic place to compete. What the commoditization of search engine technology with GPT-3 means for Google and SEO |Manick Bhan |August 21, 2020 |Search Engine Watch 

The idea of some sort of economic independence is beyond any realistic hope. Why You Should Care About Belarus |Tracy Moran |August 17, 2020 |Ozy 

However, in a team sport like baseball, regular practice against left-handed opponents is not a realistic option for most young players. What Really Gives Left-Handed Pitchers Their Edge? |Guy Molyneux |August 17, 2020 |FiveThirtyEight 

In today’s NBA, there is a clear consensus around which players are contending for the league’s highest individual honor, making it harder to justify a vote for a candidate when he doesn’t have a realistic chance of winning. MVP Voting Has Never Been More Boring |Owen Phillips |August 14, 2020 |FiveThirtyEight 

Maybe Mary is being more realistic about a second marriage—but is it too much to ask for a little fire? What Downton’s Fashion Really Means |Katie Baker |January 2, 2015 |DAILY BEAST 

Just a month from that date, he now no longer believes that to be realistic, and will no longer estimate a timeline for the trial. Prosecutors Have No Idea When 9/11 Mastermind’s Trial Will Start |Tim Mak |December 17, 2014 |DAILY BEAST 

“I just wanted to make it realistic and not glamorize life,” she said. A First Lady of Punk Rock Talks |Justin Jones |December 9, 2014 |DAILY BEAST 

Tiger Lily and her tribe, however, were outfitted in semi-realistic outfits (read: nearly naked). ‘Peter Pan Live!’ Review: No Amount of Clapping Brings It to Life |Kevin Fallon |December 5, 2014 |DAILY BEAST 

John came to see The Realistic Joneses and we went out to dinner after and talked casually about the show, but that was it. Michael C. Hall on Going Drag for ‘Hedwig and the Angry Inch’ and Exorcising ‘Dexter’ |Marlow Stern |December 4, 2014 |DAILY BEAST 

Kielland's method is realistic, and a number of his works are written with a fairly distinct "purpose." Skipper Worse |Alexander Lange Kielland 

One of the twins, deciding to play "savages," had pounced upon the ink bottle as a means of making the play more realistic! The Outdoor Girls in the Saddle |Laura Lee Hope 

As much as possible he banished from poetry a too realistic imitation of eloquence, passion, and a too exact truth. Charles Baudelaire, His Life |Thophile Gautier 

The kind of criticism that is appropriate for realistic literature is here quite out of place. Frdric Mistral |Charles Alfred Downer 

Tootles had progressed along the arduous road to masterpieces to the extent that he felt a need of realistic detail. The Woman Gives |Owen Johnson