That, of course, makes for an expensive endeavor, but “the tile becomes an interesting decorative layer in the room,” Ridder says. Captivated by bold tile on social media? Here’s what to consider before committing to the look. |Elizabeth Mayhew |October 29, 2020 |Washington Post 

A week later, I’d constructed a ten-by-six-foot L-shaped structure, bracketed by three posts crowned with old wood corbels—decorative supports—that I found in the junk pile. I Missed Bars. So I Built One in My Own Backyard. |Nick Heil |October 16, 2020 |Outside Online 

Plants add a beautiful, elegant, and relaxing look to your home or office, and a festive decorative look to parties and gatherings. The best faux plants to bring a little green to your home or office |PopSci Commerce Team |October 5, 2020 |Popular-Science 

These large, eye-catching, and decorative pieces will accentuate your space whilst educating and entertaining you and your guests. Space-related coffee table books that are out of this world |PopSci Commerce Team |October 1, 2020 |Popular-Science 

The smaller, squatter size and decorative style make this a great option for small offices, or work-from-home spaces. Great filing cabinets for your home office |PopSci Commerce Team |September 17, 2020 |Popular-Science 

Decorative yes, but a daily handbag that will sweep through the closets of women worldwide? Handbags: The More You Pay, The Smaller They Shrink |Elizabeth Landers |December 29, 2014 |DAILY BEAST 

The work at Art Basel is often interesting, often dull, and disproportionately decorative in nature. Sneer and Clothing in Miami: Inside The $3 Billion Woodstock of Contemporary Art |Jay Michaelson |December 6, 2014 |DAILY BEAST 

Decorative value is largely incidental to artistic merit as defined by critics. Sneer and Clothing in Miami: Inside The $3 Billion Woodstock of Contemporary Art |Jay Michaelson |December 6, 2014 |DAILY BEAST 

Having been taught Modernism, a school of thought that scoffs at the decorative, materials became his primary means of expression. Frank Gehry Is Architecture’s Mad Genius |Sarah Moroz |October 27, 2014 |DAILY BEAST 

Undergarments did enjoy a brief moment of exposure in the 18th century, with the ruling class indulging in decorative corsets. What Lies Beneath: How Lingerie Got Sexy |Raquel Laneri |June 5, 2014 |DAILY BEAST 

The painter breakfasting at the next table is hard at work on a decorative panel for a ceiling. The Real Latin Quarter |F. Berkeley Smith 

All decorative painting, carving, and inlaying is done by them; in short, they excel in all ingenious mechanical arts. Journal of a Voyage to Brazil |Maria Graham 

Over the wall was spread a coating of fine marble stucco for decorative purposes, which gave it a finish of dazzling white. The Private Life of the Romans |Harold Whetstone Johnston 

There is considerable decorative skill displayed in the edifice; but the work looks opaque and needs brightening up. Our Churches and Chapels |Atticus 

Silver, even, is less common in heavy decorative metal work than is solid gold. The Devil-Tree of El Dorado |Frank Aubrey