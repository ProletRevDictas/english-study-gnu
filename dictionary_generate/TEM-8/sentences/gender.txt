I think there’s a lot of talk about trying to figure out a way to make restaurants more equitable, meaning equitable among gender, among races, more equitable among workers that are there. Tom Colicchio Hopes (and Fears) COVID-19 Will Change the Restaurant Industry |Pallabi Munsi |September 16, 2020 |Ozy 

After the story was published she said in a note to staff and investors her issue had nothing to do with gender. The losses continue to pile up for hedge fund king Ray Dalio |Bernhard Warner |September 15, 2020 |Fortune 

Studies have also shown it helps if the person doing the intervention shares characteristics, such as gender or race, with the people for whom the messaging is targeted. Why Coming Up With Effective Interventions To Address COVID-19 Is So Hard |Neil Lewis Jr. (nlewisjr@cornell.edu) |September 14, 2020 |FiveThirtyEight 

Combining Glemaud’s joyful, gender-neutral, body positive approach with the clean knitwear designs he’s become known for, the designer created three knit bands. Victor Glemaud Launches Knit Bands with Fitbit |Nandi Howard |September 11, 2020 |Essence.com 

Those gender disparities largely persisted even when the researchers zoomed in on households where men and women both held jobs that could be completed at home. How COVID-19 worsened gender inequality in the U.S. workforce |Sujata Gupta |September 9, 2020 |Science News 

There was a lot of positive feedback from people interested in non-gender binary people. Grindr’s Trans Dating Problem |David Levesley |January 9, 2015 |DAILY BEAST 

Gender roles exceed the biological circumstances of childbirth and they are, perhaps, much less likely to change. Men Will Someday Have Kids Without Women |Samantha Allen |January 3, 2015 |DAILY BEAST 

There have been changes in our society on issues of sexual and gender justice. Do LGBTs Owe Christians an Olive Branch? Try The Other Way Around |Jay Michaelson |December 14, 2014 |DAILY BEAST 

The unfortunate reality is that race, gender, and economic status do matter when justice is meted out. The Post-Brown and Garner Question: Who ‘Deserves’ to Die? |Goldie Taylor |December 9, 2014 |DAILY BEAST 

That they will leverage their voices and their power to make real change to improve gender diversity. Tech’s Male ‘Feminists’ Aren’t Helping |Cate Huston, Karen Catlin |December 8, 2014 |DAILY BEAST 

They are sometimes represented as being of both sexes, all having the power to change their gender. Ancient Faiths And Modern |Thomas Inman 

Though we may not have followed the Greek rule, we to the present day always look upon a ship as of the feminine gender. How Britannia Came to Rule the Waves |W.H.G. Kingston 

The sacred scriptures, in Hebrews, bestow on him the masculine gender, and so do the authors of the Greek version. Fishes, Flowers, and Fire as Elements and Deities in the Phallic Faiths and Worship of the Ancient Religions of Greece, Babylon, |Anonymous 

But the gender must be changed, when it becomes necessary to speak of separate numbers. Summary Narrative of an Exploratory Expedition to the Sources of the Mississippi River, in 1820 |Henry Rowe Schoolcraft 

It is only in the conjugations that the principle of gender becomes lost in that of vitality. Summary Narrative of an Exploratory Expedition to the Sources of the Mississippi River, in 1820 |Henry Rowe Schoolcraft