Where a fan 10 years ago had a coin-flip chance of attending a fracas, the modern-day one has less than a 17 percent chance of watching athletes drop the mitts. Fighting Didn’t Stop In The NHL Bubble |Josh Planos |August 26, 2020 |FiveThirtyEight 

Ash, who often interacts and streams with fans on social media, has seen the number of esports athletes increase both in Pakistan and globally. An Unlikely Esports Star Emerges From Pakistan |Daniel Malloy |August 21, 2020 |Ozy 

Neck gaiters, buffs—whatever you call them, the jersey-type loops of fabric that can be worn around the neck and over the face and nose—have been a favorite of athletes during the Covid-19 pandemic. Don’t give up on your buff just yet |Katherine Ellen Foley |August 12, 2020 |Quartz 

Effectively, the ruling meant that clubs and teams in EU countries could sign players from nations that have free trade agreements with the bloc, without treating them as overseas athletes. Guess Who’s Cheering for Brexit? South African Cricket |Charu Kasturi |August 12, 2020 |Ozy 

At the university level, students have more power as paying customers — and athletes in particular as revenue generators. Is School Out Forever? |Daniel Malloy |August 9, 2020 |Ozy 

Nowhere to be found is the anguish, the drama, the pain of an athlete on that level who considering walking away. The Story of the World’s Greatest Cricket Player |William O’Connor |December 24, 2014 |DAILY BEAST 

In 87 Bounces, a lone athlete shoots an airball that goes on a journey through some of the most memorable films in cinema history. Basketball Bounces Through Classic Movies |Alex Chancey, The Daily Beast Video |October 15, 2014 |DAILY BEAST 

But I guess you have to regard him as a great athlete who never grew up. Football Great Bob Suffridge Wanders Through the End Zone of Life |Paul Hemphill |September 6, 2014 |DAILY BEAST 

But I think that the more important question is will I see a benefit as an athlete? Does Fasted Cardio Really Burn More Fat? |DailyBurn |August 22, 2014 |DAILY BEAST 

What if I were a kid who looked up to an athlete, and that athlete made me want to do better in my own life, and then he left? LeBron James Returns to Cleveland: How 'The Decision 2.0' Happened |Robert Silverman |July 11, 2014 |DAILY BEAST 

Sometimes this cup was won by a middle-aged man, sometimes by a girl, and sometimes by a trained athlete. The Box-Car Children |Gertrude Chandler Warner 

More than ordinarily tall, she was shaped like a Juno, and moved with all the grace and freedom of an athlete. The Everlasting Arms |Joseph Hocking 

Suddenly the man, with the strength and ease of an athlete, sprang lightly out on to the roof. The Rival Campers |Ruel Perley Smith 

His blue flannel suit hung loose on his shoulders and chest, his athlete's limbs. Marriage la mode |Mrs. Humphry Ward 

Mr. Norcross stands six feet two in his socks, and I've heard that he was the best all-around athlete in his college bunch. The Wreckers |Francis Lynde