Musk has already said the project allowed a monkey to control a computer device with its mind, and as the New York Times reported in 2019, Neuralink has demonstrated a system with 1,500 electrodes connected to a lab rat. Elon Musk is one step closer to connecting a computer to your brain |Rebecca Heilweil |August 28, 2020 |Vox 

On the whole, monkeys in the groups that faced one or two stressors clung less to their mothers and more readily explored their new surroundings — showing less anxiety — than both the no-stress and the two high-stress groups. Puberty can repair the brain’s stress responses after hardship early in life |Esther Landhuis |August 28, 2020 |Science News 

So the team studied squirrel monkeys that had not yet reached puberty. A bit of stress may help young people build resilience |Esther Landhuis |August 27, 2020 |Science News For Students 

Studies in rodents and monkeys had shown that adversity early in life throws the HPA axis off-kilter. Puberty may reboot the brain and behaviors |Esther Landhuis |August 27, 2020 |Science News For Students 

Remdesivir, however, was more potent in human lung cells, while GS-441524 was more potent in the monkey cells. How two coronavirus drugs for cats might help humans fight COVID-19 |Erin Garcia de Jesus |August 11, 2020 |Science News 

This has occurred with bean bag chairs, children's sweaters, and the Coco The Monkey Teething Toy. 9-Year Old With an Uzi? America Is Tougher on Toys Than Guns |Cliff Schecter |August 28, 2014 |DAILY BEAST 

Using skewers/tooth picks, attach monkey bread, Cinnabons, and churros to battleship. Epic Meal Empire’s Meat Monstrosities: From the Bacon Spider to the Cinnabattleship |Harley Morenstein |July 26, 2014 |DAILY BEAST 

In July 1925, the town hosted the Scopes Monkey Trial, a landmark case in the history of creationism. The Scopes Monkey Trial 2.0: It’s Not About the Stupid Science-Deniers |Michael Schulson |July 21, 2014 |DAILY BEAST 

The monkey seemed to be sticking his tongue out at me in defiance. How I (Digitally) Killed My Twitter Impostor |JoBeth McDaniel |July 21, 2014 |DAILY BEAST 

I had no way of knowing—or even asking—if they had mistaken this monkey-faced avatar for me. How I (Digitally) Killed My Twitter Impostor |JoBeth McDaniel |July 21, 2014 |DAILY BEAST 

But the greatest danger I ever underwent in that kingdom was from a monkey, who belonged to one of the clerks of the kitchen. Gulliver's Travels |Jonathan Swift 

So when Yung Pak's father made him a present of a monkey—a real monkey—alive—he just danced with glee. Our Little Korean Cousin |H. Lee M. Pike 

After a little while Yung Pak got used to these "monkey shines," and he knew that his pet would not stay away long after mealtime. Our Little Korean Cousin |H. Lee M. Pike 

And here M. Barbiche suddenly threw himself into the attitude of an enraged and aggressive monkey. The Pit Town Coronet, Volume I (of 3) |Charles James Wills 

Every night when he goes to bed, the monkey curls himself up by his side and lies there till morning. Alila, Our Little Philippine Cousin |Mary Hazelton Wade