As you jump, switch your leg position in the air and land with soft knees in the opposite split-squat stance. How to Train Power for the Lower Body |Hayden Carpenter |January 27, 2021 |Outside Online 

Grasp the door frame with one arm extended in front of you, and perform a squat, keeping resistance through your arm as you stand back up. Indoor exercises to prepare you for hiking the great outdoors |Pam Moore |January 27, 2021 |Washington Post 

OK, legs a little tired, gonna squat down here, maybe just take a knee. Optimal Methods for Never Recovering After Your Workout |Brendan Leonard |January 24, 2021 |Outside Online 

To do this, they measure joint angles while the subjects do an unloaded half-squat. There’s a New Way to Choose the Right Running Shoe |Alex Hutchinson |January 5, 2021 |Outside Online 

This might look like a minute of burpees followed by two minutes of recovery or 20 seconds of jump squats followed by a minute of recovery. Some trainers tout HIIT as the best way to burn belly fat. Here’s what science says. |Pam Moore |December 15, 2020 |Washington Post 

The fitness coaches at a globo-gym like Gold's would notice that a user was struggling with their squat or treadmill run. Amiigo Tracker Pairs Data With Deadlifts |Gregory Ferenstein |August 4, 2014 |DAILY BEAST 

The barracks is a squat building surrounded by sandbags on a side street near the city center. Ukraine’s Pro-Putin Rebels Prepare for a Last Stand |David Patrikarakos |July 10, 2014 |DAILY BEAST 

There may well be a chill here, but it has diddly-squat to do with the one Senator Alexander fears. The Republican Street Fight Over Transparency in Government |Lawrence Lessig |March 26, 2014 |DAILY BEAST 

Deep Squat—For most squats you want to get your thighs parallel with the ground but for a deep squat you go ALL the way down. Squats: The Absolutely Incredible Secret to Staying in Shape |Ari Meisel |January 2, 2014 |DAILY BEAST 

The only difference is that you jump straight up and then land in a squat position. Squats: The Absolutely Incredible Secret to Staying in Shape |Ari Meisel |January 2, 2014 |DAILY BEAST 

Neither of us spoke again, and at length the squat log buildings of Pend d' Oreille loomed ahead of us in the night. Raw Gold |Bertrand W. Sinclair 

Madame Antoine seated her fat body, broad and squat, upon a bench beside the door. The Awakening and Selected Short Stories |Kate Chopin 

Kerry crossed the room, laid his oilskin and cane upon a chair, and from the shelf where it reposed took a squat volume. Dope |Sax Rohmer 

These people simply squat down wherever they can find a natural catchment for water. Campaign Pictures of the War in South Africa (1899-1900) |A. G. Hales 

I could not feel comfortable in the seats and lounges, as they were very low, requiring an oriental squat at which I am not adept. Valley of the Croen |Lee Tarbell