Southwest and the Transport Workers local representing ramp, provisioning and freight employees are within about $10 million of reaching the $70 million in reductions sought by the carrier, said Charles Cerf, president of the unit. Southwest Airlines warns nearly 13% of workforce at risk of layoffs after punishing year for travel |kdunn6 |December 4, 2020 |Fortune 

Some stations lack ramps and platforms wide enough to allow passage of people using wheelchairs. Amtrak agrees to fix stations and pay $2.25 million to settle ADA discrimination claims |Luz Lazo |December 2, 2020 |Washington Post 

These layers take on a triangular wedge shape, as shown in the figure below, with warm air riding up and over a ramp of subfreezing air trapped against the surface. Ice storms: Inside wintertime’s dreaded, frozen mess |Jeffrey Halverson |December 2, 2020 |Washington Post 

She designed a ramp that she could coat with different materials. Student scientists work to help all of us survive a warmer world |Bethany Brookshire |October 21, 2020 |Science News For Students 

Not every kid is looking to get air off ramps or leave skid marks all over their elementary school parking lot. Kids’ bikes that make wonderful gifts |PopSci Commerce Team |October 1, 2020 |Popular-Science 

It was a brick wall that we turned into the on-ramp of a highway. Coffee Talk with Fred Armisen: On ‘Portlandia,’ Meeting Obama, and Taylor Swift’s Greatness |Marlow Stern |January 7, 2015 |DAILY BEAST 

I think Thrones has been better than your average show with the equality, but they could definitely ramp it up! Natalie Dormer Talks ‘Hunger Games,’ Feminism, and Why ‘Game of Thrones’ Needs More Dick |Marlow Stern |November 21, 2014 |DAILY BEAST 

Jimbo and I walked up its ramp and into the hull, which looked like the gutted inside of a school bus. I Shot Bin Laden |Elliot Ackerman |November 16, 2014 |DAILY BEAST 

Jimbo and I headed towards the ramp when the crew chief grabbed us. I Shot Bin Laden |Elliot Ackerman |November 16, 2014 |DAILY BEAST 

“The Zone” is of course the ramp where Jews arriving by train are selected for slavery or gas. How Hitch & Amis Discovered Evil In My House |Peter Foges |September 28, 2014 |DAILY BEAST 

Delancy turned the sedan through the door of the big garage, rolled across the wide parking floor to the cement ramp at the rear. Hooded Detective, Volume III No. 2, January, 1942 |Various 

He crossed the room to the concrete ramp that twisted up to the second story. Hooded Detective, Volume III No. 2, January, 1942 |Various 

The painted men had hauled up their ramp, the hatch in the globe closed with a definite snap. Star Born |Andre Norton 

Once again Dalgard read his mind and waved the mermen back, sending them through the door to the ramp and the lower engine room. Star Born |Andre Norton 

The car slid up the ramp, and the man outside pushed it in after them and closed the doors. Security |Poul William Anderson