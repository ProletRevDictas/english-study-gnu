What we do know, as the report noted, is “the drop-off will be higher for more populated locations like big cities, with smaller reductions for outdoor and less-populated destinations.” COVID redraws 2020 LGBTQ travel roadmap |Scott Stiffler |July 9, 2020 |Washington Blade 

It was a broad-based drop that saw the big tech rally fade and travel and retail stocks tank on reopening worries. Move over Nasdaq. This exchange has been killing it over the past month |Bernhard Warner |July 8, 2020 |Fortune 

A big thing we are leading the charge on is the … language that’s been used in gaming that has been tolerated. ‘It is important for us to take a leadership role’: How esports giant FaZe Clan is working to root out bad behavior in the gaming community |Lara O'Reilly |July 8, 2020 |Digiday 

In essence, clean rooms have enabled the big tech companies to become channel-specific agencies for their advertisers. Why data clean rooms are a start, but not enough |LiveRamp |July 8, 2020 |Digiday 

Now that the first wave of big announcements is winding down, raceAhead will be turning our attention to the nuts and bolts of the work that must happen in the longer term. The NFL’s apparent new wokeness might be performative, but it still matters |Ellen McGirt |July 7, 2020 |Fortune 

In that photo, Merabet has a big smile that spreads across his whole face and lights up his eyes. The Muslim Cop Killed by Terrorists |Michael Daly |January 9, 2015 |DAILY BEAST 

The Big Five banks dubbed too big to fail, are 35 percent bigger than they were when the meltdown was triggered. Sen. Warren’s Main Street Crusade to Pressure Clinton |Eleanor Clift |January 8, 2015 |DAILY BEAST 

Their three-day scientific outing was paid for by Epstein and was big success. Sleazy Billionaire’s Double Life Featured Beach Parties With Stephen Hawking |M.L. Nestel |January 8, 2015 |DAILY BEAST 

I really wanted Trenchmouth to succeed and at the time wished we were as big as Green Day. Coffee Talk with Fred Armisen: On ‘Portlandia,’ Meeting Obama, and Taylor Swift’s Greatness |Marlow Stern |January 7, 2015 |DAILY BEAST 

The big slug happened to hit the suspect in the street, passing through his arm and then striking Police Officer Andrew Dossi. Shot Down During the NYPD Slowdown |Michael Daly |January 7, 2015 |DAILY BEAST 

The big room at King's Warren Parsonage was already fairly well filled. The Pit Town Coronet, Volume I (of 3) |Charles James Wills 

Sol laughed out of his whiskers, with a big, loose-rolling sound, and sat on the porch without waiting to be asked. The Bondboy |George W. (George Washington) Ogden 

There were at least a dozen ladies seated round the big table at the Parsonage. The Pit Town Coronet, Volume I (of 3) |Charles James Wills 

I pictured him as slim and young looking, smooth-faced, with golden curly hair, and big brown eyes. The Boarded-Up House |Augusta Huiell Seaman 

Big Reginald took their lives at pool, and pocketed their half-crowns in an easy genial way, which almost made losing a pleasure. The Pit Town Coronet, Volume I (of 3) |Charles James Wills