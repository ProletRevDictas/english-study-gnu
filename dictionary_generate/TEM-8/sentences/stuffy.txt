O’Hara moved poetry from the stuffy realm of the Academy into bars, the movies – the streets. ‘Love and Other Poems’ will satisfy your need for romance |Kathi Wolfe |May 27, 2021 |Washington Blade 

Filtered air may be a bit stuffy, but would have lower infection risks than unfiltered air. Cleaning indoor air may prevent COVID-19’s spread. But it’s harder than it looks |Tina Hesman Saey |May 18, 2021 |Science News 

Every part of this year’s ceremony felt more intimate and less stuffy than just about any awards show I can remember. The Pandemic Oscars Were Surprisingly Decent. But Will the Academy Learn Anything From Its Break With Tradition? |Judy Berman |April 26, 2021 |Time 

When your nose is stuffy, odorants – the lightweight odor active molecules found in the air – are physically unable to reach the smell receptors at the top of your nasal cavity. Daily DIY Sniff Checks Could Catch Many Cases Of COVID-19 |LGBTQ-Editor |December 17, 2020 |No Straight News 

In either case, try to pick places that have limited capacity, and avoid establishments that feel hot and stuffy or don’t have many windows—they probably don’t have good ventilation, Miller says. Is There Any Safe Way to Socialize Inside This Winter? |Jamie Ducharme |October 27, 2020 |Time 

With a pop of color and fun print, this cotton pair is not at all stuffy. The Daily Beast’s 2014 Holiday Gift Guide: For the Don Draper in Your Life |Allison McNearney |November 29, 2014 |DAILY BEAST 

It was dark and somewhat stuffy, and it was “home” to a troupe of six. Spirit Tripping With Colombian Shamans |Chris Allbritton |August 24, 2014 |DAILY BEAST 

The ceremony is always too stuffy, unless it is way too silly. Breaking: The Oscars Might Not Suck This Year |Kevin Fallon |February 27, 2014 |DAILY BEAST 

Yesterday, the media were supposed to be focused on the stuffy royal luncheon at Buckingham Palace. Pippa Steals The Royal Limelight Again as Wedding Rumors Trump Stuffy Royal Lunch |Tom Sykes |December 19, 2013 |DAILY BEAST 

The style is stuffy, the syntax is antique, and the conceit is never really convincing. Robin Sloan’s Book Bag: Five Science Fiction Books That Matter |Robin Sloan |September 24, 2013 |DAILY BEAST 

It would be like opening the windows upon a stuffy, overcrowded and unventilated room of disputing people. The Salvaging Of Civilisation |H. G. (Herbert George) Wells 

"No, thanks," the stranger said, taking his bag and shutting himself into his stuffy little stateroom. The Cromptons |Mary J. Holmes 

"The things," Mr. Devenish, is my rather stuffy way of referring to all the delightful poems that you are going to eat to-night. First Plays |A. A. Milne 

A stuffy hole, full of peat-smoke, and with a window that can't open at the best of times. The Daisy Chain |Charlotte Yonge 

The crowd swarmed into the court-room, stuffy and hot enough already, and the air vibrated with expectancy. The Rival Campers |Ruel Perley Smith