Over the next few hours, paperwork was faxed back and forth between the hospitals, and the insurance company was contacted for approval. Dying on the Waitlist |by David Armstrong and Marshall Allen |February 18, 2021 |ProPublica 

She called again, and after hours on hold, a representative told her to fax a copy of the check. The IRS Cashed Her Check. Then the Late Notices Started Coming. |by Lydia DePillis |February 16, 2021 |ProPublica 

You can use these gadgets to not only print but scan, copy, and fax your documents. Best all-in-one printer: Upgrade your home office with these multitasking machines |Carsen Joenk |February 8, 2021 |Popular-Science 

Yow went to bed at the men’s Final Four in Atlanta not knowing Frese’s decision, but she got up in the middle of the night with the faxed agreement slipped under her door. Brenda Frese, closing in on win No. 500, has made Maryland women’s basketball a juggernaut |Kareem Copeland |February 4, 2021 |Washington Post 

Researchers had to sort through texts, faxes, case reports in foreign languages and whatever else they could get their hands on, all the while worrying about where that data was coming from and how accurate it was. The Hard Lessons of Modeling the Coronavirus Pandemic |Jordana Cepelewicz |January 28, 2021 |Quanta Magazine 

According to a police source, that fax came in at 2:46 p.m.—literally a after before the fatal bullets flew. Alleged Cop Killer Ismaaiyl Brinsley Had a Death Wish |M.L. Nestel |December 22, 2014 |DAILY BEAST 

But by the time a critical wanted poster sent via fax arrived, more than two hours elapsed. Alleged Cop Killer Ismaaiyl Brinsley Had a Death Wish |M.L. Nestel |December 22, 2014 |DAILY BEAST 

But this is a regular office with fax machines and telephones and computers. Inside the Smuggling Networks Flooding Europe with Refugees |Barbie Latza Nadeau |December 15, 2014 |DAILY BEAST 

In fact, new technologies—copiers, fax machines, and recently the iPad—were significant sources of inspiration for him. The Many Lives of Artist David Hockney |William O’Connor |November 23, 2014 |DAILY BEAST 

Beginning in 1988, he also dove into making art using a fax machine. The Many Lives of Artist David Hockney |William O’Connor |November 23, 2014 |DAILY BEAST 

His troops, and about nine hundred of his friends, went on board the British vessels in the harbor, and sailed off to Hali-fax. The Story of the Thirteen Colonies |H. A. (Hlne Adeline) Guerber 

He says, when peeple cums to know all the true fax of the case, they'll willingly pay dubble price for tea-total Waiters. Punch, or the London Charivari, Vol. 103, October 29, 1892 |Various 

Fax is the general expression for any sort of torch; tda is a natural pine torch; funale, an artificial wax-torch. Dderlein's Hand-book of Latin Synonymes |Ludwig Dderlein 

Fax mentis honest gloria—Glory is the torch of an honourable mind. Dictionary of Quotations from Ancient and Modern, English and Foreign Sources |James Wood 

Fax mentis incendium glori—The flame of glory is the torch of the mind. Dictionary of Quotations from Ancient and Modern, English and Foreign Sources |James Wood