Through a hatred of formal schooling, my parents and I failed for many years to understand that what I really needed to succeed was being allowed to find my passion. Traditional school isn’t always the way to go, and I wish my parents had seen that earlier |Kenneth R. Rosen |February 5, 2021 |Washington Post 

Political parties and politicians can no longer seek to leverage their resentment and hatreds for their own political gain. We Can Make America Anew Only If We're Honest About the Depth of the Ugliness and Hate Today |Eddie S. Glaude Jr. |January 11, 2021 |Time 

Each organized around a loose cluster of themes—money and its New York handmaiden real estate, Lebowitz’s love of smoking and hatred of wellness culture—the episodes resemble chapters of her books. In Pretend It’s a City, Martin Scorsese Shares the Pleasure of Fran Lebowitz’s Company |Judy Berman |January 8, 2021 |Time 

LGBTQ supportive prosecutors have said the clarification was needed because it is often difficult to prove that hatred is the only motive behind a violent crime. D.C. budget lacks funds to implement part of LGBTQ bill |Lou Chibbaro Jr. |January 5, 2021 |Washington Blade 

Instead, the one thing that unites the right and drives the GOP is hatred of liberals. Hatred of liberals is all that’s left of conservatism |Paul Waldman |December 11, 2020 |Washington Post 

Hatred for international media is intense but not, it seems, universal. Pro-Russian Protestors in Ukraine Dream of Soviet Glory Days |David Patrikarakos |April 8, 2014 |DAILY BEAST 

“Hatred of health-care reform is what gives all segments [of the party] purpose,” the survey found. By the Numbers: Inside the GOP |Eleanor Clift |July 26, 2013 |DAILY BEAST 

A Convenient Hatred: The History of Antisemitism An unparalleled education on discrimination against Jews. This Week’s Hot Reads |Lizzie Crocker, Malcolm Jones |December 5, 2011 |DAILY BEAST 

A Convenient Hatred tells the long history of anti-Semitism, from ancient Egypt to the 21st-century Vatican. This Week’s Hot Reads |Lizzie Crocker, Malcolm Jones |December 5, 2011 |DAILY BEAST 

Hatred for a religion is not something you acquire quickly, not even over nine years. Islamophobia's Slow Boil |Scott L. Malcomson |September 10, 2010 |DAILY BEAST 

Hatred burned in his eyes at the memory, like some fire that had been banked but had never died. Space Prison |Tom Godwin 

These pictures are very rare, Time and Hatred have hidden them but too well. A German Pompadour |Marie Hay 

Hatred of Russia and religious fanaticism inspired the Turks with something of the old love of battle and lust of conquest. A History of the Nineteenth Century, Year by Year |Edwin Emerson 

Hatred means nothing, in temptation or response, to a heart overflowing with love. Spirit and Music |H. Ernest Hunt 

Hatred is a vice of narrow souls; they feed it with all their meanness, and make it a pretext for sordid tyranny. Parisians in the Country |Honore de Balzac