Still, many PE instructors said they’re eager to return to the gym and sports fields. Kids are shooting hoops with rolled up socks, but pandemic physical education is not canceled |Kelly Field |February 12, 2021 |Washington Post 

Whether you're eager for a half-hour session or a weekend-long binge, interested in a platformer or a twin-stick shooter, or playing on PC, Switch, Xbox, or PlayStation, our selection should have something worth your time. The games of love: our favorite couch co-op games to play with a partner |Jeff Dunn |February 12, 2021 |Ars Technica 

Yet the rise of these agencies suggests that brands are finally eager to break into platforms like Twitch and into virtual, in-game universes in games like Fortnite. Why new agencies are trying to capitalize on the online gaming boom |jim cooper |February 12, 2021 |Digiday 

He’s fine with it becoming something that can bring big profits to the companies eager to mine these data. New technology can get inside your head. Are you ready? |Laura Sanders |February 11, 2021 |Science News For Students 

As a child, Wells read the newspaper to her father and his friends who were eager to vote and take advantage of opportunities as full citizens. My great-grandmother Ida B. Wells left a legacy of activism in education. We need that now. |Michelle Duster |February 11, 2021 |Washington Post 

This is the Mexico that the U.S. Chamber of Commerce, and most major U.S. corporations, are eager to call amigo. Why Mexicans Are Enraged by Obama’s Big Tuesday Meeting |Ruben Navarrette Jr. |January 6, 2015 |DAILY BEAST 

A woman in a smart uniform scribbles out tickets for a growing line of tourists eager to take a trip on the old-fashioned train. Riding Thailand’s WWII Death Railway |Liza Foreman |December 21, 2014 |DAILY BEAST 

In August 1984, I arrived at the University of Virginia in Charlottesville, eager to jump into college life. I Was Gang Raped at a UVA Frat 30 Years Ago, and No One Did Anything |Liz Seccuro |December 16, 2014 |DAILY BEAST 

In the days leading up to Thanksgiving, most of us are eager to return home for the holiday. How to Make It Through Thanksgiving Alive |Lizzie Crocker |November 26, 2014 |DAILY BEAST 

Why the government is so vehemently eager to delist the grizzly remains a troublesome question. What It Takes to Kill a Grizzly Bear |Doug Peacock |November 23, 2014 |DAILY BEAST 

The rebellion spread to their district, and many of the natives on and about the estate were eager to join in the movement. The Philippine Islands |John Foreman 

Sam sat opposite him in perfect silence, waiting, with eager curiosity, for the termination of the scene. The Posthumous Papers of the Pickwick Club, v. 2(of 2) |Charles Dickens 

The morning came and with it Shaffer, and with him five hundred and fifty men, eager for the combat. The Courier of the Ozarks |Byron A. Dunn 

Taken for guerrillas, every Southern sympathizer was eager to give them all the information possible. The Courier of the Ozarks |Byron A. Dunn 

Among others, an Abb thrice lifted his fork to his mouth, and thrice laid it down, with an eager stare of surprise. The Book of Anecdotes and Budget of Fun; |Various