By the time she reaches her desperate endgame, however, he has taken us beyond the tropes of his faux-horror conceit and brought us squarely into the climax of a caper film. Lesbian love story becomes thriller in ‘Two of Us’ |John Paul King |February 4, 2021 |Washington Blade 

While this conceit might feel forced in the hands of a lesser writer, Mendelsohn pulls it off surprisingly well. Everything Our Editors Loved in January |The Editors |February 4, 2021 |Outside Online 

Even if you believe that to be so, Cassie’s doggedness is an exhausting and not particularly deep conceit for a film. Promising Young Woman Starts with a Cathartic Blast. Then It Gets Bogged Down With Cynicism |Stephanie Zacharek |January 15, 2021 |Time 

If we accept the conceit of Gray’s book and just look at how cats live, then maybe we can learn a thing or two. Everything is awful. Here’s a Q&A with a philosopher about why cats rule. |Sean Illing |January 15, 2021 |Vox 

All of that could have been alleviated, perhaps, by a cast capable of finding truth within the conceits and translating them to the screen in a form that at least resonates with real human emotion. Queer horror film chokes on surrealist pretensions in ‘The One You Feed’ |John Paul King |January 15, 2021 |Washington Blade 

Wolf concurs that the conceit of the show seems to have everyone but the sex worker in mind. To Catch a Sex Worker: A&E’s Awful, Exploitative Ambush Show |Samantha Allen |December 19, 2014 |DAILY BEAST 

So the heaviness was not so much a literary conceit but something I wanted to talk about. Meghan Daum On Tackling The Unspeakable Parts Of Life |David Yaffe |December 6, 2014 |DAILY BEAST 

Because that conceit was straight/gay vs straight/straight, I could do a lot of overtly straight humor and it would be acceptable. Oscars Host Neil Patrick Harris on His Best and Worst Emcee Moments (VIDEO) |Neil Patrick Harris |October 15, 2014 |DAILY BEAST 

But Sex Box, with its ridiculous guinea pigs screwing conceit, will only augment our cultural hang-ups about sex. 'Sex Box,' a Reality TV Show Where Couples Have Sex in a Box and Discuss It, Is Coming to America |Marlow Stern |August 21, 2014 |DAILY BEAST 

And this is how we arrive at the lovely conceit of Scarlett Johansson as an alien seductress. Scarlett Johansson is an Alien Seductress in ‘Under the Skin’ |Jimmy So |April 3, 2014 |DAILY BEAST 

Having seen no service, he owed his appointment largely to his conceit and good looks. Napoleon's Marshals |R. P. Dunn-Pattison 

I am out of conceit with England just now; and would far rather have gone to the Antipodes. Elster's Folly |Mrs. Henry Wood 

It would argue too much literary conceit on my part were I anxious to restore it to the light of day. Marguerite |Anatole France 

It came to Lowell in a flash that Bill's arrogance sprang from something deeper than mere conceit or drunkenness. Mystery Ranch |Arthur Chapman 

He is made to talk like a man of the greatest vanity and conceit, and to throw contempt and scorn on everybody else. Beacon Lights of History, Volume I |John Lord