After this he went on to become one of the world’s greatest warriors and the founder of an empire whose progressiveness, reach, and might would astound poets, writers, travelers, historians, and archaeologists for centuries to come. Africa’s post-Covid economic recovery will be an uphill battle |Ciku Kimeria |August 2, 2021 |Quartz 

The fact that they’re not applying that retroactively to people who’ve already been sentenced astounds me. “Progressive prosecutors” are working within the system to change it. How is that going? |Jamil Smith |July 30, 2021 |Vox 

Imaginative and inspired, Spirited Away immerses the viewer in a fantastical world that at once astounds and alarms. How Spirited Away Changed Animation Forever |Kat Moon |July 20, 2021 |Time 

Our planet was very different 100,000 years ago, and if we could survey that time, we would be astounded by the human diversity across its surface. The Human Family Tree, It Turns Out, Is Complicated - Issue 102: Hidden Truths |Razib Khan |June 30, 2021 |Nautilus 

While the Switch doesn’t push polygons like its competitors, it still manages to use innovative design choices to present graphic art that can astound, immerse, and enthrall. Best Nintendo Switch games for graphics: The titles that justify jumping to the big screen |Billy Cadden |June 20, 2021 |Popular-Science 

But it continues to astound me that there are troves of archives that have not been looked at. What Lincoln Could Teach Fox News |Scott Porch |November 6, 2014 |DAILY BEAST 

The depth of rage, animus and violence that was directed at him—“Spittle flying, the N word flying”—continues to astound him. NPR’s Smooth-Talking Millennial Whisperer |Batya Ungar-Sargon |October 7, 2014 |DAILY BEAST 

In the first years of his frolicking life he loved also to astound Rome, and succeeded a number of times. Let us follow Him |Henryk Sienkiewicz 

Yet she was a rare enough exception to astound my abstracted mind. The Pacific Triangle |Sydney Greenbie 

You rather astound me with respect to value of grounds of generalisation in the morphology of plants. More Letters of Charles Darwin Volume II |Charles Darwin 

In it there appeared volunteer troops to astound seasoned veterans by their dash and discipline. Generals of the British Army |Francis Dodd 

Master Mather is to bring his feathers to show the Governor, and to astound the Governor's skeptical wife. Dulcibel |Henry Peterson