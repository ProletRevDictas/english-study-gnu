Then before leaving the Atwood Building, Potts caught up with her boss in the lobby of the Atwood. The Woman Propositioned by Alaska’s Former Lieutenant Governor Tells Her Story for the First Time |by Kyle Hopkins and Michelle Theriault Boots, Anchorage Daily News |September 10, 2020 |ProPublica 

The main floor is split up between the lobby, the restaurant, and there’s even an intimate little nook for whatever the mood calls for. Pre-pandemic, Kuala Lumpur’s hospitality scene was heating up with new design-forward boutique hotels |Rachel King |September 5, 2020 |Fortune 

When you want to get together for a planned or unplanned meeting, you can pull someone from the lobby and create another room. Teemyco creates virtual offices so you can grab a room and talk with colleagues |Romain Dillet |September 4, 2020 |TechCrunch 

If we don’tspeak, strike, protest, lobby, marchwrite, boycott, weigh inJackie RobinsonDay will be just anotherswing and a big miss. It’s time to make Jackie Robinson proud |Ellen McGirt |August 29, 2020 |Fortune 

A lot of people you never would have imagined are now saying that maybe the anti-vaccination lobby has a point. Liberal, Educated … and Anti-Vaxxer: Pandemic Births New Vaccine Doubters |Charu Kasturi |August 25, 2020 |Ozy 

There was an air of excitement and anticipation in the lobby as showtime approached. I Was Honeydicked Into Spending Christmas with ‘The Interview’ |Allison McNearney |December 26, 2014 |DAILY BEAST 

While the Hobby Lobby decision may have lost its cultural appeal, it still carries weight in the federal court system. The 26 Next Hobby Lobbys |Abby Haglage |December 17, 2014 |DAILY BEAST 

Yet that is precisely what President Obama and more specifically the immigration lobby is asking Americans to do. The Progressive Case Against Birthright Citizenship |Keli Goff |December 15, 2014 |DAILY BEAST 

They are for corporations like Hobby Lobby, and vast hospital networks, and, yes, adoption agencies. Do LGBTs Owe Christians an Olive Branch? Try The Other Way Around |Jay Michaelson |December 14, 2014 |DAILY BEAST 

Change the location from a hotel lobby to an airport check-in desk and this crazy scenario becomes all too familiar. Flying Coach Is the New Hell: How Airlines Engineer You Out of Room |Clive Irving |November 25, 2014 |DAILY BEAST 

Spencer Perceval, prime minister of Great Britain, shot in the lobby of the house of commons. The Every Day Book of History and Chronology |Joel Munsell 

His answer was, that the direct method would be by forwarding a petition in the way proposed when at the lobby. Life of Richard Trevithick, Volume II (of 2) |Francis Trevithick 

Behind him, a lobby lounger moved over to the elevator boy, jerking his chin in Wilson Lamb's direction as he asked a question. Hooded Detective, Volume III No. 2, January, 1942 |Various 

Halfway across the lobby, a tall swarthy man with one of those deadpan faces rose to greet him. Hooded Detective, Volume III No. 2, January, 1942 |Various 

Lamb went back into the main lobby and ensconced himself behind a morning paper. Hooded Detective, Volume III No. 2, January, 1942 |Various