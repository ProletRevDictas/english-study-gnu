She was the toast of the theater town for a while, earning a spot on a “30 under 30” list, but that was a minute ago, and now she feels stuck. Two of the year’s best movies are now on Netflix |Alissa Wilkinson |October 9, 2020 |Vox 

The Los Angeles Rams were the toast of football under second-year coach Sean McVay in 2018, finishing 13-3 and going all the way to the Super Bowl before losing a defensive struggle to Bill Belichick’s New England Patriots. The L.A. Rams Are Running Their Way Back Into Contention |Neil Paine (neil.paine@fivethirtyeight.com) |October 8, 2020 |FiveThirtyEight 

That last button allows you to extend the toast time without going overboard. These pieces of kitchen gear make excellent gifts |PopSci Commerce Team |October 6, 2020 |Popular-Science 

I can’t count how many mornings I’ve mindlessly folded a piece of toast around a strip of bacon, or piled big, fat slices of fresh summer tomatoes on a hunk of good bread. BLTs Should Be a Breakfast Food |Lesley Suter |October 6, 2020 |Eater 

In 2016, a new credit card joined avocado toast as a cultural touchstone for affluent millennials. Chase’s Sapphire card created a millennial ‘cult.’ Can it last through COVID? |Jeff |August 24, 2020 |Fortune 

For Paul, the thrill of breakfast with the Reverend, may be giving way to the taste of burnt toast. GOP Won’t Forgive Rand for Cop Critique |Lloyd Green |December 23, 2014 |DAILY BEAST 

Just three years ago, Chris Hughes and Sean Eldridge were the toast of the liberal establishment. The Rise and Fall of Chris Hughes and Sean Eldridge, America’s Worst Gay Power Couple |James Kirchick |December 9, 2014 |DAILY BEAST 

Much like the Taj Mahal, Revel opened in classically gaudy Atlantic City style in April 2012—with a sunrise Champagne toast. I Watched a Casino Kill Itself: The Awful Last Nights of Atlantic City’s Taj Mahal |Olivia Nuzzi |December 8, 2014 |DAILY BEAST 

His cannabis-infused menus range from truffle tuna casserole and coconut chicken to French toast and omelets. Meet the Julia Child of Weed |Justin Jones |November 13, 2014 |DAILY BEAST 

Over a glass (or more) of port, we toast to the Queen…and Dame Judi Dench. Join The Mile High (Dining) Club |Allison McNearney |September 26, 2014 |DAILY BEAST 

He silenced her with a gesture, and, leaving a piece of toast half-eaten on his plate, he got up and went into his study. Uncanny Tales |Various 

Could we men of to-day have done it justice and sat it and the toast list out, I wonder. Fifty Years of Railway Life in England, Scotland and Ireland |Joseph Tatlow 

The menu was long, elaborate and imposing; equalled only by the toast list, which contained no less than sixteen separate toasts. Fifty Years of Railway Life in England, Scotland and Ireland |Joseph Tatlow 

But being observed, one evening, to omit it, a gentleman reminded him that he had forgotten to toast his favorite lady. The Book of Anecdotes and Budget of Fun; |Various 

Coals!what would he do with coals?Toast his cheese with em, and then come back for more. Oliver Twist, Vol. II (of 3) |Charles Dickens