Failure to enforce laws that exist in the United States leads to chaos and chaos leads to inhumanity. Texas Gov. Abbott sends miles of cars along border to deter migrants |Adela Suliman |September 22, 2021 |Washington Post 

Her lyrical account presents the obscene inhumanity of slavery while celebrating the humanity of its victims. A humble cloth sack tells a story of enslavement and separation |Marjoleine Kars |July 9, 2021 |Washington Post 

I think that any form of activism that is against inhumanity is a good movement. 'America’s Notorious for Saying One Thing and Doing Something Else.' Albert Woodfox Talks Solitary Confinement, Social Distancing and Racial Justice |Josiah Bates |February 19, 2021 |Time 

As the conversations about racial injustice grew more intense and the examples of inhumanity became more pronounced and horrifying, he knew he couldn’t remain a passive observer. Deshaun Watson is taking a stand against disingenuous NFL owners. It could change the league. |Jerry Brewer |February 1, 2021 |Washington Post 

Another 10 slaves threw themselves overboard in a display of defiance at the inhumanity. The Life and Hard Times Of The Family A Cuban Defector Left Behind |Brin-Jonathan Butler |December 19, 2014 |DAILY BEAST 

Yet here we are, dispensing another dollop of inhumanity to some of the most troubled and despised people in America. The GOP’s Hidden Ban on Prison Abortions |Harold Pollack |December 13, 2014 |DAILY BEAST 

His giddy glee turns sickening when you consider the coldhearted inhumanity that necessarily lies beneath. ISIS Has a Bigger Coalition Than We Do |Michael Daly |October 15, 2014 |DAILY BEAST 

Some Egyptians find it difficult to believe such inhumanity could take place alongside such awe-inspiring protests. Raped in Tahrir: The Frightening Reality Women Face at Egypt Protests |Manar Ammar |July 3, 2013 |DAILY BEAST 

She opted to stay, believing her exit would be a victory for the authors of the inhumanity she dedicated her life to exposing. Edward Snowden Should End His Cold War Tour, Come Home to America |Jelani Cobb |June 25, 2013 |DAILY BEAST 

Nature, ever buoyant and imperative, does her best to remedy the ills created by "Man's inhumanity to Man." Glances at Europe |Horace Greeley 

The grim old ruin has many dark traditions of the times when "man's inhumanity to man" was the rule rather than the exception. British Highways And Byways From A Motor Car |Thomas D. Murphy 

The Star is shadowed by our thoughtless inhumanity to those who serve us and our forgetfulness of the needy. Fifty Contemporary One-Act Plays |Various 

Mai profited by his high station to show his cruelty and inhumanity. Celebrated Travels and Travellers |Jules Verne 

If Tiberius must exhibit his colossal inhumanity, could he have anywhere in all the world chosen a better spot? Humanly Speaking |Samuel McChord Crothers