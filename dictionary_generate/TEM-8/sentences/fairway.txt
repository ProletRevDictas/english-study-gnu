He kneels down and whacks it out over the ridge to the fairway. At a breezy PGA, normalcy beckoned — in the galleries, if not the top of the leader board |Chuck Culpepper |May 21, 2021 |Washington Post 

It closed for 10 months in 2020, and Miller added as much as six feet of elevation to some fairways and greens, digging new ponds to hold the water. Golf thrives on the ocean’s edge. What happens when the oceans rise? |Dave Sheinin |May 20, 2021 |Washington Post 

DeSantis had a few questions about how well Florida had managed the pandemic placed on a tee in front of him, and he landed each one squarely on the fairway. DeSantis enacts voting restrictions touted by Fox News with only Fox News in the room |Philip Bump |May 6, 2021 |Washington Post 

Matsuyama’s tee shot at the 13th flew well to the right of the fairway but hit a pine tree, and with a large smacking sound it bounced back out to an easily playable position. With confidence and calm, Hideki Matsuyama ends Japan’s long wait for a Masters title |Chuck Culpepper, Scott Allen, Des Bieler |April 11, 2021 |Washington Post 

The 31-year-old Northern Ireland native began 4 over through his first 11 holes, and while he failed to find the fairway on five of his first eight drives, he did manage to hit his dad in the leg with an errant shot on the par-4 seventh. Justin Rose, with a back nine out of a dream, races to a four-shot lead at the Masters |Chuck Culpepper, Scott Allen, Des Bieler |April 9, 2021 |Washington Post 

In Manhattan, where I grew up, people seemed to eat a lot of takeout, or prepared foods from places like Zabars and Fairway. Friday Forum: How often do you cook? |Megan McArdle |January 11, 2013 |DAILY BEAST 

Both initially were unpopular with neighbors, and Fairway admits on its website that opening a store there was “just plain nuts.” Red Hook Still Crying Out for Relief in Sandy’s Wake |Allison Yarrow |November 6, 2012 |DAILY BEAST 

She was lifted bodily by Mr. Fairway's arm, which had been flung round her waist before she had become aware of his intention. Return of the Native |Thomas Hardy 

She indicated the faint light at the bottom of the valley which Fairway had pointed out; and the two women descended the tumulus. Return of the Native |Thomas Hardy 

"There was Kingsbere church likewise," Fairway recommenced, as one opening a new vein of the same mine of interest. Return of the Native |Thomas Hardy 

Afterwards the door opened, and Fairway appeared on the threshold, accompanied by Christian and another. Return of the Native |Thomas Hardy 

"A man who is doing well elsewhere wouldn't bide here two or three weeks for nothing," said Fairway. Return of the Native |Thomas Hardy