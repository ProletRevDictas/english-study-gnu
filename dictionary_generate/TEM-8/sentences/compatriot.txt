Then he defended himself publicly against his former compatriots, who had criticized him as a “rogue” and a selfish coward. TheDonald’s owner speaks out on why he finally pulled plug on hate-filled site |Craig Timberg, Drew Harwell |February 5, 2021 |Washington Post 

He also admires Atlético Madrid’s Diego Simeone, another compatriot, for finding tactical balance. D.C. United’s Hernán Losada must learn a new league and a new city. He’s up for the challenge. |Steven Goff |January 19, 2021 |Washington Post 

Not only were a great number of the participants using their smartphones to document themselves and their compatriots as they launched the attack, but many of them in turn shared the footage on Parler. Inside the Capitol Riot: What the Parler Videos Reveal |by Alec MacGillis |January 17, 2021 |ProPublica 

Both mice and humans share a connectedness with their compatriots in emotional situations, he says, and research points to a shared evolutionary basis for empathy. Mice may ‘catch’ each other’s pain — and pain relief |Carolyn Wilke |January 12, 2021 |Science News 

Police believe that once inside, other rioters opened another door or a few doors to let still more of their compatriots in. Capitol breach prompts urgent questions about security failures |Carol D. Leonnig, Aaron Davis, Dan Lamothe, David Fahrenthold |January 7, 2021 |Washington Post 

We all know about Tolstoy, Chekhov, and Dostoevsky, but what about their compatriot, Nikolai Leskov? The Forgotten Russian: The Genius of Nikolai Leskov |Benjamin Lytal |April 10, 2013 |DAILY BEAST 

Lagarde succeeded embattled compatriot Dominique Strauss-Kahn for the IMF job in 2011. Sarkozy’s Old Lady Troubles |Tracy McNicoll |March 22, 2013 |DAILY BEAST 

And his secular Tel Avivi compatriot Ayelet Shaked destroys all stereotypes of an Israeli “radical.” Why I Like Naftali Bennett |Shaul Magid |January 17, 2013 |DAILY BEAST 

Huffman said his friend Bunny had no idea—one more close compatriot allegedly kept in the dark. Edwards Staffers Keep Spilling Secrets in Day 9 of Testimony |Diane Dimond |May 4, 2012 |DAILY BEAST 

Strange to say, with all his love for what belonged to and came from Poland, he kept compatriot musicians at a distance. Frederick Chopin as a Man and Musician |Frederick Niecks 

"Neighbor," in Gospel language, means a compatriot, a person belonging to the same nationality. My Religion |Leo Tolstoy 

His compatriot, Le Fèbvre, had begun to give a few lessons as a dancing-master. My Lady Ludlow |Elizabeth Gaskell 

"Your cheerful compatriot is right," said Ethan, shaken suddenly out of his rôle as Nature's apologist. The Open Question |Elizabeth Robins 

A compatriot of his, Edmund Falconer, like himself an actor as well as an author, had opened the way for him. The English Stage |Augustin Filon