The best kind of cast iron scrubber also happens to look like it could be part of a medieval suit of armor. These pieces of kitchen gear make excellent gifts |PopSci Commerce Team |October 6, 2020 |Popular-Science 

For now, the study offers a few small hints about medieval life and suggests that ancient toilets have more to tell us. Archaeologists delved into medieval cesspits to study old gut microbiomes |Kiona N. Smith |October 6, 2020 |Ars Technica 

Eventually, that data will help researchers plumb the depths of medieval microbiomes to understand how the microscopic populations of our intestines have evolved over the centuries. Archaeologists delved into medieval cesspits to study old gut microbiomes |Kiona N. Smith |October 6, 2020 |Ars Technica 

To the artists who embraced it, including Leonardo, Rembrandt, and Vermeer, this fresh approach also established a sharp break from the hard, flat style of medieval art. These Images Expose the Dark Side of the Solar System - Issue 89: The Dark Side |Corey S. Powell |August 26, 2020 |Nautilus 

Called folium, this watercolor had been used to paint images on the pages of medieval manuscripts. Ancient recipes helped scientists resurrect a long-lost blue hue |Carolyn Wilke |May 26, 2020 |Science News For Students 

Even in the medieval era this disparity made Christians uncomfortable. Oops! Jesus’ Last Steps Are in the Wrong Place |Candida Moss |January 6, 2015 |DAILY BEAST 

The folk memory of medieval community life had been wiped out by the industrial revolution. How Dickens and Scrooge Saved Christmas |Clive Irving |December 22, 2014 |DAILY BEAST 

If a Queen did cheat, her crimes fade into insignificance compared to the extensive philandering engaged in by medieval monarchs. The Sex Life of King Richard III's Randy Great Great Great Grandfather |Tom Sykes |December 4, 2014 |DAILY BEAST 

It is difficult to overstate how destructive the practice of dismembering ancient and medieval books is. Dismembering History: The Shady Online Trade in Ancient Texts |Candida Moss |November 23, 2014 |DAILY BEAST 

From reports it must once have been a lovely old city with stone houses and a medieval quarter. Blood in the Sand: When James Jones Wrote a Grunt’s View of D-Day |James Jones |November 15, 2014 |DAILY BEAST 

In classical and medieval times bridges were constructed of timber or masonry, and later of brick or concrete. Encyclopaedia Britannica, 11th Edition, Volume 4, Part 3 |Various 

The medieval importance of these markets and fairs for the sale of wool and wine and later of cloth has gone. Encyclopaedia Britannica, 11th Edition, Volume 4, Part 3 |Various 

The Celtic language reappeared; the Celtic art emerged from its shelters in the west to develop in new and medieval fashions. Encyclopaedia Britannica, 11th Edition, Volume 4, Part 3 |Various 

Towers of Babel, medieval mythologies, and extensive smearings of that kind, he could find leisure! History Of Friedrich II. of Prussia, Vol. III. (of XXI.) |Thomas Carlyle 

Besides, what he has to say about dentistry occurs in typical medieval form. Old-Time Makers of Medicine |James J. Walsh