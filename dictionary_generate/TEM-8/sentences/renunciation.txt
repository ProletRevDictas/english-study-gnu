Eight years ago, I researched the renunciation and repudiation of the Eagle rank, but I never followed through. The Eagle Scouts and Boy Scouts of America’s Antigay Policy |Naka Nathaniel |June 17, 2012 |DAILY BEAST 

Now deeply bitter, he sees this renunciation as “his last opportunity to be a man of integrity.” Philip Roth's Extreme Novel |Morris Dickstein |October 2, 2010 |DAILY BEAST 

Nor did the future appear any more in a rainbow glory, since he realised that it would bring renunciation as well as joy. The Life &amp; Letters of Peter Ilich Tchaikovsky |Modeste Tchaikovsky 

That renunciation was therefore a nullity; and no swearing, no signing, no sealing, could turn that nullity into a reality. The History of England from the Accession of James II. |Thomas Babington Macaulay 

That happiness now requires my renunciation in behalf of my very dear eldest son, Charles Louis, prince of the Asturias. The History of England in Three Volumes, Vol.III. |E. Farr and E. H. Nolan 

It is somewhat self-regarding certainly, in spite of the incessant renunciation and sacrifice. The Daughters of Danaus |Mona Caird 

Nothing requires more moral courage than the renunciation of a popular and generally received religious belief. Beacon Lights of History, Volume II |John Lord