While USTR’s profile heightened, Lighthizer largely avoided the limelight, knowing that upstaging his boss could hasten his exit. Robert Lighthizer Blew Up 60 Years of Trade Policy. Nobody Knows What Happens Next. |by Lydia DePillis |October 13, 2020 |ProPublica 

“These results suggest cultivating awe enhances positive emotions that foster social connection and diminishes negative emotions that hasten decline,” the researchers concluded in their paper. Regular doses of awe can do wonders for emotional health |Kat Eschner |September 22, 2020 |Popular-Science 

New seafloor maps reveal the first clear view of a system of channels that may be helping to hasten the demise of West Antarctica’s vulnerable Thwaites Glacier. New maps show how warm water may reach Thwaites Glacier’s icy underbelly |Carolyn Gramling |September 9, 2020 |Science News 

Biochemical changes from loneliness can accelerate the spread of cancer, hasten heart disease and Alzheimer’s, or simply drain the most vital among us of the will to go on. Why do you feel lonely? Neuroscience is starting to find answers. |Amy Nordrum |September 4, 2020 |MIT Technology Review 

If that makes male leopards change their migratory patterns permanently, it could hasten the animal’s decline even faster. Leopards Are Indeed Changing Their Spots |Eromo Egbejule |August 14, 2020 |Ozy 

“This has got to be the oldest way in the human race to hasten death,” he said. The Nurse Coaching People Through Death by Starvation |Nick Tabor |November 17, 2014 |DAILY BEAST 

As I hasten to reassure these exasperated moms and dads, I had to be in the office anyway. The Doctor’s Note Must Die! |Russell Saunders |September 16, 2014 |DAILY BEAST 

And his election would not hasten the Republican apocalypse. Thad Cochran Wins One for Sanity Over Tea Partier Chris McDaniel |Michael Tomasky |June 25, 2014 |DAILY BEAST 

But fluctuations of mere feet during its flood season could sustain the rise of empires, or hasten their fall. The Nile: Where Ancient and Modern Meet |William O’Connor |June 21, 2014 |DAILY BEAST 

And in the case of South Africa, the divestment effort helped hasten the demise of an evil regime. Why Stanford Should Keep Its Coal Stocks |Daniel Gross |May 15, 2014 |DAILY BEAST 

By the second process, it is made to the advantage of the issuer of the notes to hasten their withdrawal himself. Readings in Money and Banking |Chester Arthur Phillips 

Who he could not make out, except that it was a Kirton: and it prayed him to hasten down immediately. Elster's Folly |Mrs. Henry Wood 

Hasten the time, and remember the end, that they may declare thy wonderful works. The Bible, Douay-Rheims Version |Various 

He had perhaps placed in her hand the weapon that should hasten his own defeat, stretch him bleeding on the sand. The Wave |Algernon Blackwood 

Let them hasten and take up a lamentation for us: let our eyes shed tears, and our eyelids run down with waters. The Bible, Douay-Rheims Version |Various