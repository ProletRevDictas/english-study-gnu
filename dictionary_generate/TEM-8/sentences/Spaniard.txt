The Spaniard has played a bogey-free round with three birdies and an eagle, and is one stroke ahead of Jordan Spieth, who is 1 over through five holes. With confidence and calm, Hideki Matsuyama ends Japan’s long wait for a Masters title |Chuck Culpepper, Scott Allen, Des Bieler |April 11, 2021 |Washington Post 

The 55-year-old Spaniard dedicated his achievement to his friend and former Ryder Cup partner Seve Ballesteros, who died in 2011 and would’ve turned 64 on Friday. Hideki Matsuyama’s sublime day at the Masters gives him a four-shot lead and a shot at history |Chuck Culpepper, Scott Allen |April 11, 2021 |Washington Post 

The Spaniard’s baby boy may have cost him some sleep and practice rounds, but that was arguably nothing compared to what McIlroy did to his father at the seventh hole. Justin Rose, with a back nine out of a dream, races to a four-shot lead at the Masters |Chuck Culpepper, Scott Allen, Des Bieler |April 9, 2021 |Washington Post 

The buzziest moment from this Masters week was without question Spaniard Jon Rahm performing this stunt — and holing out. Masters golfers won’t see the blooms, but they will really miss hearing the roars |Barry Svrluga |November 12, 2020 |Washington Post 

Jon Rahm is a 26-year-old Spaniard who has already been ranked No. At the Masters, Tiger Woods walks in two worlds: Yarn-spinning legend and defending champ |Barry Svrluga |November 11, 2020 |Washington Post 

A Spaniard by birth, Victor Serna left home shy of his 14th birthday and entered the monastery to become a Marist brother. Obama’s One Hand Clap With Castro |Doug McIntyre |December 24, 2014 |DAILY BEAST 

The Daily Pic: In 1936, the Spaniard captured elegance without settling for it. Picasso Eyes Good Taste |Blake Gopnik |September 10, 2013 |DAILY BEAST 

The anonymous blog Tennis Has a Steroid Problem has a laundry list of “evidence” against the 27-year-old Spaniard. Tennis Has a Doping Problem |Sujay Kumar |July 8, 2013 |DAILY BEAST 

Sock, who was ultimately felled by 27-year-old Spaniard Nicolas Almagro, was the youngest man on the list. U.S. Open: Why Serena Williams Has Still Got Game |Nicholas McCarvel |September 2, 2012 |DAILY BEAST 

He sent the Spaniard on his way then visited a series of shops in Lisbon and had the visa reproduced down to the special stamps. The Spy Who Tricked Hitler: The Story of Double Agent Juan Pujol and D-Day |Stephan Talty |July 11, 2012 |DAILY BEAST 

Soon after they parted, with a sarcastic laugh from the Spaniard, and Ma'amselle mingled with the crowd. The Pastor's Fire-side Vol. 3 of 4 |Jane Porter 

He passed to and fro in the city without the least insult being offered him by any Spaniard. The Philippine Islands |John Foreman 

In such a case, he may go to the house of the noble Spaniard who was his uncle's guest at Lindisfarne. The Pastor's Fire-side Vol. 3 of 4 |Jane Porter 

The tall, slender Spaniard, swarthy and of classic feature, looks about him with suppressed disdain. Prison Memoirs of an Anarchist |Alexander Berkman 

The Spaniard's boat was lashed so that no mortal could get her clear, and the little craft was used as a sort of lumber-closet. The Chequers |James Runciman