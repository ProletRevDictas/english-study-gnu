A successful writer finds herself trapped as a 19th-century slave. ‘Antebellum’ tackles the past head on in an effort to ‘move forward’ |radmarya |September 17, 2020 |Fortune 

Andreas Weber is a Berlin-based philosopher, biologist, and writer. What the Meadow Teaches Us - Issue 90: Something Green |Andreas Weber |September 16, 2020 |Nautilus 

Ali Faagba is a copywriter, content marketer, and a tech freelance writer. Inbound marketing for brand awareness: Four up-to-date ways to do it |Ali Faagba |September 11, 2020 |Search Engine Watch 

As a freelance writer, she specializes in marketing and graphic design. Top six ways to optimize crawl budget for SEO |April Brown |September 9, 2020 |Search Engine Watch 

Bill Malcolm is the only nationwide syndicated LGBTQ value travel writer. Escape to Indy for a weekend getaway |Bill Malcolm |September 4, 2020 |Washington Blade 

In his view, a writer has only one duty: to be present in his books. Houellebecq’s Incendiary Novel Imagines France With a Muslim President |Pierre Assouline |January 9, 2015 |DAILY BEAST 

Spencer, 27,  is variously described as a writer and a stand-up comic. Meet Stephen Fry’s Future Husband (Who Is Less Than Half His Age) |Tom Sykes |January 6, 2015 |DAILY BEAST 

Decades ago, the writer-director wrote an episode of the animated comedy that never was. Here’s the Lost Judd Apatow ‘Simpsons’ Episode, Penned by Judd Apatow |Asawin Suebsaeng |January 6, 2015 |DAILY BEAST 

The Perfect Storm writer talks combat brotherhood and the threat posed by growing wealth inequality. Sebastian Junger on War, Loss, and a Divided America |The Daily Beast Video |January 1, 2015 |DAILY BEAST 

His father, a writer, and his mother, who worked in insurance, were flawless. The Story of the World’s Greatest Cricket Player |William O’Connor |December 24, 2014 |DAILY BEAST 

William Woodville died; a distinguished English physician and medical writer. The Every Day Book of History and Chronology |Joel Munsell 

With this letter is another by the same writer, dated July 30, 1622—a postscript to a duplicate of the preceding letter. The Philippine Islands, 1493-1898, Volume XX, 1621-1624 |Various 

Christopher Bennet died; a distinguished London physician, and writer on medical subjects. The Every Day Book of History and Chronology |Joel Munsell 

I do not think it would have been possible to stand between the public and a writer in this way in the years before 1914. The Salvaging Of Civilisation |H. G. (Herbert George) Wells 

Richard Brathwaite, an English poet and miscellaneous writer, died. The Every Day Book of History and Chronology |Joel Munsell