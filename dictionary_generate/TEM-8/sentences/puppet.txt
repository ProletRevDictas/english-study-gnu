She says she no longer thinks of God as a personified being pulling the puppet strings. Julien Baker questioned her faith. Music helped her embrace the uncertainty. |Sonia Rao |February 26, 2021 |Washington Post 

The satirical twist is that all the footage shown as real is, of course, deepfaked, while all the footage labeled fake is either real or played by puppets. The creators of South Park have a new weekly deepfake satire show |Karen Hao |October 28, 2020 |MIT Technology Review 

Computer-programmed light pulses can then target these newly light-sensitive neurons in a particular region of the brain and control their activity like puppets on a string. Scientists Found a New Way to Control the Brain With Light—No Surgery Required |Shelly Fan |October 13, 2020 |Singularity Hub 

He’d look at the wrong target, knowing it was wrong, but still stare at it like a puppet. How Scientists Influenced Monkeys’ Decisions Using Ultrasound in Their Brains |Shelly Fan |May 27, 2020 |Singularity Hub 

Here’s what the actor Geena Davis — by the way, she prefers “actor” to “actress” — here’s what she told us about the beloved educational series with a cast of puppet characters. Does Hollywood Still Have a Princess Problem? (Ep. 394) |Stephen J. Dubner |October 24, 2019 |Freakonomics 

Based on his sock puppet, I expected him to be a burly bearded giant clad in plaid—basically, a Canadian Paul Bunyan. Canada’s Subversive Sock Puppet: Ed the Sock Isn’t Afraid to Say Anything |Soraya Roberts |November 13, 2014 |DAILY BEAST 

In 2006, Israel's Rechov Sumsum added Mahboub, an Arab-Israeli puppet who spoke both languages on the series. ‘Sesame Street’ Is Middle-Aged and Awesome |Emily Shire |November 10, 2014 |DAILY BEAST 

It was a hand, it was a puppet, it was half-CGI, but mostly puppetry. All Eyes on Anjelica Huston: The Legendary Actress on Love, Abuse, and Jack Nicholson |Alex Suskind |November 10, 2014 |DAILY BEAST 

In another study, children saw a puppet show where a mouse was eaten by an alligator. Why Are Millennials Unfriending Organized Religion? |Vlad Chituc |November 9, 2014 |DAILY BEAST 

So, in short, everyone knows Leung is a mere puppet with zero power and will read out whatever the communists dictate to him. Beijing/Hong Kong: A Tale of Two Cities as Demonstrations Continue |Ben Leung |October 1, 2014 |DAILY BEAST 

The puppet had been torn from Mazaroff's hands; those compromising papers had vanished from Countess Saens's drawer. The Weight of the Crown |Fred M. White 

I have heard of puppet rulers before—woman whom I am delighted to learn has a human heart after all. Valley of the Croen |Lee Tarbell 

A number of persons were assembled in the tap-room of the inn, where a man was exhibiting a puppet-show. Rudy and Babette |Hans Christian Andersen 

If you would call me puppet to-morrow, we might strike a balance and find—what should we find? When Valmond Came to Pontiac, Complete |Gilbert Parker 

Falser than the Bank of Fancy, frailer than a shilling glove, Puppet to a father's anger, minion to a nabob's love! The Book of Humorous Verse |Various