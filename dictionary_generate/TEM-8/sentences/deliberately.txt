Prosecutors said they were unable to determine whether Kay threw the gun deliberately or reflexively on being shot. Federal prosecutors will not press charges against D.C. officer who fatally shot 18-year-old Deon Kay |Keith L. Alexander, Peter Hermann |November 20, 2020 |Washington Post 

You can use this phenomenon to your advantage by deliberately creating FOMO and driving sales. Harness the power of integrated CRO and social media |Ricky Wang |November 17, 2020 |Search Engine Watch 

They also revealed that Intuit was deliberately hiding the free edition from Google search by adding code on its site telling Google and other search engines not to list TurboTax Free File in search results. Two ProPublica Projects Win Loeb Awards |by ProPublica |November 13, 2020 |ProPublica 

If we want to live in a future full of sounds that make us feel good and with fewer of the ones that make our teeth clench, we’ve got to start designing our soundscapes more deliberately. Delivery drones could drive us batty—unless they take some sonic cues from owls |PopSci Staff |November 10, 2020 |Popular-Science 

“And we did that all very deliberately because of that kind of lack of supply,” Gambuzza said. Golf bet big on legalized sports gambling. It’s paying off. |Rick Maese |November 9, 2020 |Washington Post 

He alleges that a third boy, aged 10 or 11, was deliberately hit by a car and killed by a member of the pedophile network in 1979. Victim: I Watched British MPs Rape and Murder Young Boys |Nico Hines |December 18, 2014 |DAILY BEAST 

Patrons eat their cannoli slowly and deliberately, dabbing the powdered sugar on their plates with licked fingers. De Robertis, a New York Great, Bids Farewell |Lizzie Crocker |December 4, 2014 |DAILY BEAST 

Nonetheless, Turing killed himself on June 7, 1954, in a deliberately prepared way, by eating a cyanide-laced apple. The Castration of Alan Turing, Britain’s Code-Breaking WWII Hero |Clive Irving |November 29, 2014 |DAILY BEAST 

He refused to be drawn on whether the authors might be guilty of a deliberately deception. Is ‘The Lost Gospel’ Book a Fraud? |Nico Hines |November 12, 2014 |DAILY BEAST 

Second, penalties need to be increased for lying or deliberately withholding relevant requested information from Congress. George W. Bush’s Puzzling WMD Coverup |Rick Santorum, Pete Hoekstra |October 27, 2014 |DAILY BEAST 

But that she could calmly tell him about it, that she could deliberately describe this effect upon her of another man—! The Wave |Algernon Blackwood 

He had come down after the wagon load, which had to be pitched on again rather more deliberately. The Book of Anecdotes and Budget of Fun; |Various 

Now, after a weary march and a protracted fight in the burning sun, some of the men deliberately lay down to die. The Red Year |Louis Tracy 

It was a crisis engendered deliberately by men of evil purpose, public enemies well known and often named. The Eve of the Revolution |Carl Becker 

But she could easily believe that Belle had deliberately entangled Darry in this thing. The Campfire Girls of Roselawn |Margaret Penrose