I would venture that, if I were to compare the two, the series that tackles that more elegantly is The Voice, and not the one who strips contestants of their entire identity, renders them inhuman, and turns them into cartoons. ‘Alter Ego’ Is the Most Dystopian Reality Competition Yet |Kevin Fallon |September 23, 2021 |The Daily Beast 

I asked the jail’s warden during a later interrogation — about which you will later know why — about this inhuman behavior and he told me that they did it so that we could sleep at night. Locked up in the Land of Liberty: Part II |Michael K. Lavers |July 14, 2021 |Washington Blade 

“It’s inhuman that people are making a business out of it,” she says. How Indians are crowdsourcing aid as covid surges |Varsha Bansal |April 28, 2021 |MIT Technology Review 

They will surely be instructed that their political opponents are really ruthless, inhuman enemies, bent on canceling and silencing them by any means necessary. There’s nothing conservative about CPAC |Michael Gerson |February 25, 2021 |Washington Post 

She was asked to embody an inhuman level of perfection in order to be accepted as 'good enough' by the doubters but showed them all what it means to be truly outstanding. Kate Mulgrew returning as Capt. Janeway in Star Trek: Prodigy |Kate Cox |October 8, 2020 |Ars Technica 

In a close-to-human face, every inhuman trait becomes magnified. Kevin Spacey Stars as a Frank Underwood-like Warmonger in ‘Call of Duty: Advanced Warfare’ |Alec Kubas-Meyer |November 8, 2014 |DAILY BEAST 

Last but not least, the Inhumans are a genetically advanced “inhuman race” that was formed via experimentation by the alien Kree. Inside Marvel’s Phase 3: How ‘The Avengers’ Cross Paths with Black Panther and the New Superheroes |Marlow Stern |October 30, 2014 |DAILY BEAST 

They, too, bear some responsibility for the scandal represented by these T-shirts mass-produced under inhuman conditions. The Fashion Victims of Bangladesh |Bernard-Henri Lévy |May 8, 2014 |DAILY BEAST 

His inhuman biceps and sultry hip pumping will ease any pain felt after hearing about his exit from the figure skating scene. Movie Title Break-Up, Star Wars on the Slopes, and More Viral Videos |Ariana Dickey |February 15, 2014 |DAILY BEAST 

Liberalism, the evolving attempt to fully express the human, was about to face an inhuman threat. Anne Frank’s Amsterdam |Russell Shorto |October 12, 2013 |DAILY BEAST 

By all the sounded consonants we have—“Inhuman Civil War;” the latter shorter, more significant, and more easily remembered. Assimilative Memory |Marcus Dwight Larrowe (AKA Prof. A. Loisette) 

For some twelve centuries the Holy Church carried out this inhuman policy. God and my Neighbour |Robert Blatchford 

There was a long stretch of wood country, where the wretch's most inhuman deeds had been located. Motor Matt's "Century" Run |Stanley R. Matthews 

Suddenly, some common impulse born of the moment and the scene—of its inhuman ghostliness and grandeur—drew them to each other. Marriage la mode |Mrs. Humphry Ward 

It is evidently religion; it is a zeal which renders inhuman, and which serves to cover the greatest infamy. Superstition In All Ages (1732) |Jean Meslier