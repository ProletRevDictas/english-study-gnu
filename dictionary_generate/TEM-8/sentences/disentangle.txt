Researchers will have to disentangle the various ways in which the asteroid has changed if they hope to use it as a window into the early solar system. Local asteroid Bennu used to be filled with tiny rivers |Charlie Wood |October 8, 2020 |Popular-Science 

Colonialism itself was broad and complex, and its modern-day outcomes are not easily disentangled. What the UK owes in reparations |Hasit Shah |October 6, 2020 |Quartz 

Scientists are still trying to disentangle how much of the harm from hepatitis C is caused by the immune response against the virus rather than the virus itself. Scientists Win Nobel Prize for Discovering the Hepatitis C Virus |Jordana Cepelewicz |October 5, 2020 |Quanta Magazine 

By acquiring SDG&E’s infrastructure and instituting full public control of our energy service, San Diego can more easily and efficiently disentangle from fossil fuels and provide energy at a significantly lower monetary and environmental cost. The Franchise Agreement Ending Offers San Diego a Chance for a Fresh Start |Amanda Moser and Shauna McKenna |September 8, 2020 |Voice of San Diego 

Ecologists need to recognize individuals when disentangling the complex symbioses and relationships that define a community. What Is an Individual? Biology Seeks Clues in Information Theory. |Jordana Cepelewicz |July 16, 2020 |Quanta Magazine 

So it's hard to disentangle any possible negative effects from the effects of divorce and other family instability. Can Gay Marriage Solve Our Adoption Problem? |Megan McArdle |March 29, 2013 |DAILY BEAST 

All of these factors are related to cognitive enhancement, but they're difficult to disentangle. Busting the Adderall Myth |Casey Schwartz |December 21, 2010 |DAILY BEAST 

But how then do we disentangle from this place in a responsible way? The Hotspots No Speech Can Fix |Leslie H. Gelb |September 23, 2009 |DAILY BEAST 

In order to carry out my wife's orders, I had to disentangle Susan from Liosha's embrace and pack her off rueful to the nursery. Jaffery |William J. Locke 

You will probably see Williams before I can disentangle myself from the affairs with which I am now surrounded. The Life and Letters of Mary Wollstonecraft Shelley, Volume I (of 2) |Florence A. Thomas Marshall 

Miranda and the Queen curiously examined the quaint instrument, and helped to disentangle and divest it of its broken strings. Baron Bruno |Louisa Morgan 

He cast his mind back over the interview, but failed to disentangle anything definite. Joan of the Sword Hand |S(amuel) R(utherford) Crockett 

There is a complex totality, as yet difficult to disentangle, of psychic and physical forces. Mysterious Psychic Forces |Camille Flammarion