It comes with a curtain you can attach to any side of the dome to create a full wall of protection, and it sets up quickly and packs down to the size of a small camping tent. Car Camping Is Way Better with These 5 Pieces of Gear |Jakob Schiller |September 26, 2020 |Outside Online 

When it rains, the dome doubles as a useful cooking or hangout structure. Car Camping Is Way Better with These 5 Pieces of Gear |Jakob Schiller |September 26, 2020 |Outside Online 

The company says the dome was inspired by the Pantheon in Rome. Apple’s ‘most ambitious’ new store is a departure from its signature design |claychandler |September 8, 2020 |Fortune 

Using these photos as a reference, we constructed a to-scale version of the dome. The Minecraft Institvte of Technology |Tate Ryan-Mosley |August 19, 2020 |MIT Technology Review 

The decision to close up the domes and leave the mountaintops behind might seem odd — after all, we put telescopes out in the middle of nowhere and study things that are billions of light-years away. Social Distancing From the Stars |Emily Levesque |August 11, 2020 |Quanta Magazine 

The dome could be rebuilt by 2021 if work stays on schedule, according to workers at the site. Madonna, Carla Bruni & Obama Abandoned Pledges To Rebuild L'Aquila After The Quake |Barbie Latza Nadeau |November 18, 2014 |DAILY BEAST 

From deep within, looking up at the tropical sky is like staring through the dome of some kind of earthen cathedral. What Made Mexico’s Most Mysterious Beach? |Brandon Presser |October 14, 2014 |DAILY BEAST 

Nevertheless, should Iron Dome funding come up for a vote this week, it is expected to receive overwhelming support. Even Left-Wing Politicians Can’t Quit Israel |Tim Mak |July 30, 2014 |DAILY BEAST 

Without an Iron Dome defense system, air raid sirens or even bomb shelters, people resign themselves to their fate. A Child’s Funeral in Gaza |Jesse Rosenfeld |July 18, 2014 |DAILY BEAST 

But very little of the ISIS ethos has to do with hitting the Freedom Tower or the Capitol Dome. Iraq Is Not Our War Anymore. Let It Be Iran’s Problem. |Christopher Dickey |July 17, 2014 |DAILY BEAST 

If it had not been for the high, dome-shaped roof, the air would have grown heavy and impure. Alila, Our Little Philippine Cousin |Mary Hazelton Wade 

In front of him, dome upon dome of wooded mountain stood against the sky. Three More John Silence Stories |Algernon Blackwood 

The buildings grew higher toward the center of the dome, but I stopped while they were still two stories. Fee of the Frontier |Horace Brown Fyfe 

The inside of Jorgensen's always surprised new visitors to Asaph Dome. Fee of the Frontier |Horace Brown Fyfe 

A trace of light had begun to soften the sky over the dome, but had not yet seeped down to ground level. Fee of the Frontier |Horace Brown Fyfe