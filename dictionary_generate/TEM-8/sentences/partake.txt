It halted a disastrous law enforcement experiment, which, among other things, unleashed the Ku Klux Klan to perpetuate state-sanctioned violence against people of color who had the temerity to partake in a simple drink. Congress should legalize cannabis now—for the economy and for social justice |matthewheimer |December 3, 2020 |Fortune 

Even as she approaches a breakdown and partakes in suicidal ideation, the writing maintains a glassy, poetic remove. Emma Glass’s ‘Rest and Be Thankful’ powerfully describes what it means to be a health-care worker |Pete Tosiello |December 2, 2020 |Washington Post 

That said, women who are less fit and don’t partake in regular exercise may not respond so effectively. How The Contraceptive Pill Could Help Female Athletes Stay Cool |LGBTQ-Editor |November 21, 2020 |No Straight News 

Virtual high school as a product isn’t built for adoption en masse, but instead works best for students who can afford to partake in self-directed and independent learning. With $2.7M in fresh funding, Sora hopes to bring virtual high school to the mainstream |Natasha Mascarenhas |October 2, 2020 |TechCrunch 

When she placed her first cowrie piece on her head, she did so knowing she had left Côte d’Ivoire for the first time to partake in Ghana’s Chale Wote Street Art Festival, a festival of great social significance. Lafalaise Dion Takes ESSENCE On A Personal Journey |Nandi Howard |September 4, 2020 |Essence.com 

This world is a horrid cancer that no decent soul should ever partake from. Is This The Most Hated Man in Books?: Twitter vs. Edward Champion |Brandy Zadrozny |September 26, 2014 |DAILY BEAST 

I'm pro the freedom to smoke and the freedom to grow and pass and partake. Weed Gave My Family Everything—Then Took It Away |Abby Haglage |April 9, 2014 |DAILY BEAST 

The Liebeck jury intuited that the only way to punish this logic of bigness was to partake of it. Supersize Me, Your Honor: Liebeck v. McDonald’s and Our Era of Ambition |James Poulos |October 21, 2013 |DAILY BEAST 

The point of fasting is not to die, just to suffer a little and those who cannot partake donate to feed the truly hungry. First Friday of Ramadan For Palestinians |Maysoon Zayid |July 12, 2013 |DAILY BEAST 

That all young citizens of Israel will partake both in Torah study and in military and civilian service. Who’s Afraid Of Ruth Calderon? |Sigal Samuel |February 14, 2013 |DAILY BEAST 

Victor was proud of his achievement, and went about recommending it and urging every one to partake of it to excess. The Awakening and Selected Short Stories |Kate Chopin 

The cliffs of Red Point partake of a reddish tinge and appear to be disposed nearly in horizontal strata. Narrative of a Survey of the Intertropical and Western Coasts of Australia] [Volume 2 of 2] |Phillip Parker King 

Having once taken your seat at table, you have nothing to do with the dinner but to partake of it. The Ladies' Book of Etiquette, and Manual of Politeness |Florence Hartley 

If offered any dish of which you do not wish to partake, decline it, but do not assign any reason. The Ladies' Book of Etiquette, and Manual of Politeness |Florence Hartley 

If there is a delicacy upon the table, partake of it sparingly, and never help yourself to it a second time. The Ladies' Book of Etiquette, and Manual of Politeness |Florence Hartley