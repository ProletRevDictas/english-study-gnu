The name of the restaurant demands she offer duck and peaches, and the combination of crisp-skinned fowl and juicy fruit is simple and satisfying. 2021 Fall Dining Guide |Tom Sietsema |October 6, 2021 |Washington Post 

Mid-Atlantic dishes, from fish to fowl, play large on the menu. Seven new restaurants to try this fall |Evan Caplan |September 16, 2021 |Washington Blade 

Healthy birds housed within view of fellow fowl infected with a common pathogen mounted an immune response, despite not being infected themselves, researchers report online June 9 in Biology Letters. The mere sight of illness may kick-start a canary’s immune system |Jonathan Lambert |June 8, 2021 |Science News 

She was born to be a helpmeet, to supply what the beasts of the field and the fowls of the air could not, in sorrow and pain. Gaia, the Scientist - Issue 99: Universality |Hope Jahren |April 7, 2021 |Nautilus 

You’d know what time of year it is simply by the chef’s choice of meat and fowl. A reopened Marcel’s reminds me that fine dining, especially now, is about more than food |Tom Sietsema |November 6, 2020 |Washington Post 

It is a multimillion-dollar business in which roughly 15 million fowl die a year. The History of the Chicken: How This Humble Bird Saved Humanity |William O’Connor |December 27, 2014 |DAILY BEAST 

It all began, the consensus seems to be, with the red jungle fowl. The History of the Chicken: How This Humble Bird Saved Humanity |William O’Connor |December 27, 2014 |DAILY BEAST 

Like all fowl, turkeys tend to go quiet when held upside down. Confessions of a Turkey Killer |Tom Sykes |November 26, 2014 |DAILY BEAST 

They had planned dinners together every night and ate guinea fowl, duck and other “interesting” dishes. Jeff Goldblum Says Justin Bieber Should Play Him in ‘Jurassic Park’ Reboot |Melissa Leon |March 8, 2014 |DAILY BEAST 

But it was her light dinner—typically a broth with vegetables and either chicken or guinea fowl—that Wheeler saw as key. The Marie Antoinette Diet |Erin Cunningham |February 1, 2014 |DAILY BEAST 

He stirred the smoldering ashes till the broiled fowl began to sizzle afresh. The Awakening and Selected Short Stories |Kate Chopin 

Water-fowl that had not moved at the first alarm now sprang in myriads from reeds and sedges, and darkened the very air. Hunting the Lions |R.M. Ballantyne 

Chloride of Lime … bad smell … bad egg … white of egg … fowl … grain … flour … flour and water … milk fluid … milk. Assimilative Memory |Marcus Dwight Larrowe (AKA Prof. A. Loisette) 

The Chinese esteem it as a great delicacy and mix it with fowl and vegetables. Tobacco; Its History, Varieties, Culture, Manufacture and Commerce |E. R. Billings. 

The priests shall not eat of any thing that is dead of itself or caught by a beast, whether it be fowl or cattle. The Bible, Douay-Rheims Version |Various