It was completely quiet except for the whir of my cassette and the huff of my own breathing, my bike rolling over the slickrock and striated sandstone. Why We Should Be Turning Former Mines into Trails |Heather Hansman |May 29, 2021 |Outside Online 

Meanwhile, Godzilla leaves Florida in a huff and makes his way to Hong Kong, on the way encountering King Kong, who is being transported to a Hollow Earth portal by boat. Godzilla vs. Kong Pairs Two Formidable Monster Foes—Too Bad About the People |Stephanie Zacharek |March 31, 2021 |Time 

Walking out in a huff would be rude, which is why you will be discovering unavoidable conflicts, for which you will apologize on your way out the door. Miss Manners: Get out of Dodge before the abusive mother-in-law visits |Judith Martin, Nicholas Martin, Jacobina Martin |February 25, 2021 |Washington Post 

With that, he took a huff off a morning joint and moved into the throng of jovial patrons. A Report From the Misunderstood Gathering of the Juggalos |Steve Miller |July 28, 2014 |DAILY BEAST 

So we salute you, Mr. Fielder, even as we continue to huff and puff at the gym in pursuit of those rippling ridges. Prince Fielder’s Demi Moore Moment: World Loses It Over Athlete Without Six-Pack |Tim Teeman |July 10, 2014 |DAILY BEAST 

In response, Smith quit the party in a huff that July, trashing it as insufficiently principled on his way out the door. Sen. Bob Smith: The Thing That Wouldn’t Leave |Michelle Cottle |December 4, 2013 |DAILY BEAST 

In elementary school, children will disagree and fight, then storm away in a huff and simply ignore each other. Was It Irresponsible of Israel to Apologize to Turkey? |Brent E. Sasley |May 21, 2013 |DAILY BEAST 

Her grandmother, Elisabeth Huff, saw her last week, at her birthday party. What Amanda Knox Is Up to Now |Winston Ross, Barbie Latza Nadeau |March 25, 2013 |DAILY BEAST 

Their capital was limited and Mr. Young gave Mr. Huff his note for seven thousand dollars for a half interest in the business. Lyman's History of old Walla Walla County, Vol. 2 (of 2) |William Denison Lyman 

Oh, no, he went off in a terrible huff because the girls laid their plans before him and told him what they were going to do. The Beauty |Mrs. Wilson Woodrow 

And the Colonel was so devoted to her, he might go off in a huff as poor Job Manning had done, and stand it out to the bitter end. Hubert's Wife |Minnie Mary Lee 

He's gone off in a huff about something; never mind, luncheon comes up all the same. The Rector |Mrs. (Margaret) Oliphant 

Patrick may perhaps fume and get into a huff about it, but who cares for Patrick? Blackwood's Edinburgh Magazine, Volume 69, No. 423, January 1851 |Various