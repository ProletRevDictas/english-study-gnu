You can summit crests like 10,064-foot Mount San Antonio and 8,985-foot Telegraph Peak right out the gates of Mount Baldy Resort in the San Gabriel Mountains. 15 Ways to Play in the Snow This Year |Megan Michelson |November 26, 2020 |Outside Online 

Pernigotti suffered only a ruptured Achilles tendon and, with his partner’s help, made it to the crest, where a helicopter came to his rescue. How Climate Change Is Making the Alps More Dangerous |Agostino Petroni |October 23, 2020 |Outside Online 

The distance between each crest, or tip, of a wave is its wavelength. Spotted: Milky Way’s giant gas bubbles in visible light |Emily Conover |July 16, 2020 |Science News For Students 

The 2020 Dodgers may represent a rare opportunity, the crest of a franchise’s talent-rich era. The Dodgers’ Legacy May Depend On This Short Season |Robert O'Connell |July 13, 2020 |FiveThirtyEight 

Wavelength is the distance from one point on a wave to an identical point on the next, such as from crest to crest or from trough to trough. Explainer: Understanding waves and wavelengths |Jennifer Look |March 5, 2020 |Science News For Students 

To break her self-destructive cycle and heal, she decides to hike 1,100 miles of the Pacific Crest Trail solo. Exclusive: The Making of Reese Witherspoon’s Golden Globe-Nominated ‘Wild’ |Marlow Stern |December 12, 2014 |DAILY BEAST 

Plus, Procter & Gamble has already removed triclosan from its Crest toothpaste. Antibacterial Soap’s Deadly Secret |Kent Sepkowitz |May 21, 2014 |DAILY BEAST 

The Tea Party wave, which would crest a few months later, still seemed far out to shore. Michael Grimm’s Unhappily Ever After |David Freedlander |April 29, 2014 |DAILY BEAST 

Internal Revenue has its own crest or coat of arms or something. Up to a Point: I Do My Own Taxes With No Help, Except From a Couple of Bloody Marys |P. J. O’Rourke |April 15, 2014 |DAILY BEAST 

Procter & Gamble introducing Crest toothpaste line with nontraditional flavors—chocolate, vanilla, lime. Up to a Point: P.J. O’Rourke on Valentine’s Day and Oral Hygiene |P. J. O’Rourke |February 14, 2014 |DAILY BEAST 

But at the instant I caught a sight of my counterfeit presentment in a shop window, and veiled my haughty crest. God and my Neighbour |Robert Blatchford 

The crest-fallen astronomer plodded on his weary way, another example of a fool and his money soon parted. The Book of Anecdotes and Budget of Fun; |Various 

He swam with her upon the summit of the breaking Wave, lifted upon its crest, swept onward irresistibly. The Wave |Algernon Blackwood 

Still a-shiver at dawn, I saddled up and loped for the crest of the nearest divide to get the benefit of the first sun-rays. Raw Gold |Bertrand W. Sinclair 

I rubbed in the spot indicated, and out came the crest and initials exactly as Jack had described them. Uncanny Tales |Various