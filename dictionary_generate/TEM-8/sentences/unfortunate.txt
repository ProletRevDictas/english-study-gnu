Hansen says that vaccines have “an unfortunate history of not being safe,” but that the need for long-term safety studies needed to be balanced against the risks of the pandemic. Trump wants a COVID-19 vaccine by Election Day. But will one be ready? |Jeremy Kahn |September 4, 2020 |Fortune 

I thought about the unfortunate end of the “K” while reading in The Wall Street Journal that AT&T, once merely a phone company, wants to sells its advertising technology business, the unfortunately named Xandr. Why AT&T is leaving the lucrative ad-tech business |Adam Lashinsky |September 3, 2020 |Fortune 

While it’s obviously unfortunate when artists achieve greater success after death than when they were alive, that success is still something to be celebrated, Howard says. We Hear Dead People: Our Favorite Posthumous Hip-Hop Albums. Ever |Joshua Eferighe |August 31, 2020 |Ozy 

Wherever you land on the gender spectrum, rocking a dress can be a freeing experience, and it’s unfortunate that the stigma deters people from enjoying it. In Praise of the Adventure Dress |Alison Van Houten |August 22, 2020 |Outside Online 

They’ve been hung up on — all kinds of things, which is really unfortunate because they’re working very, very hard and helping extra hours. For Election Administrators, Death Threats Have Become Part of the Job |by Jessica Huseman |August 21, 2020 |ProPublica 

And when two bros start quoting the show to her, the unfortunate line, "Say 'old woman's pussy!'" ‘The Comeback’ Finale: Give Lisa Kudrow All of the Awards |Kevin Fallon |December 29, 2014 |DAILY BEAST 

The unfortunate reality is that race, gender, and economic status do matter when justice is meted out. The Post-Brown and Garner Question: Who ‘Deserves’ to Die? |Goldie Taylor |December 9, 2014 |DAILY BEAST 

It makes it seem all the more unfortunate, that having finally achieved such understanding, most of those personnel are leaving. Afghanistan, We Hardly Knew You |Jonathan Foreman |December 8, 2014 |DAILY BEAST 

How ironic and unfortunate that the critics tend to focus on one “bad” class or the other. Walmart Lifts Black Friday’s Curse |James Poulos |November 26, 2014 |DAILY BEAST 

There is no doubt that some unfortunate reporter, tasked with working the weekend shift, would have looked into them. The IRS Email Double Standard |Matt Lewis |November 25, 2014 |DAILY BEAST 

The moment was an awkward one, and Cynthia wished madly that she had not been prompted to ask that unfortunate question. The Boarded-Up House |Augusta Huiell Seaman 

She and her younger sister, Janet, had quarreled a good deal through force of unfortunate habit. The Awakening and Selected Short Stories |Kate Chopin 

Dressed in full uniform, amid cries of "Long live our King Joachim," the unfortunate man landed with twenty-six followers. Napoleon's Marshals |R. P. Dunn-Pattison 

It was very unfortunate that the whole establishment stood in unaffected awe of the redoubted Mr Bellamy. Blackwood's Edinburgh Magazine, No. CCCXXXIX. January, 1844. Vol. LV. |Various 

This selection was unfortunate; good strategist and organiser, he was not the man the Emperor required. Napoleon's Marshals |R. P. Dunn-Pattison