Matteo Salvini, leader of the populist right-wing Lega party and a notorious promoter of anti-immigration laws, called him “the champion of the left-wing snobs.” How an Italian Mayor Who Turned His Town into a Haven for Migrants Wound Up Facing 13 Years in Jail |Francesca Berardi |October 7, 2021 |Time 

Despite the fact that I can be a snob about audio quality, I genuinely love the experience of listening to music with smart glasses. Ray-Ban Stories Smart Sunglasses Review: All-Seeing Eyes |Mike Epstein |October 5, 2021 |Popular-Science 

Via the Harlem Cultural Festival, which even a music snob like me didn’t even know about. "This Film Was My Chance to Correct History": Questlove on Summer of Soul and the Oscars |Andrew R. Chow |June 28, 2021 |Time 

I wanted to bridge the gaps between the serious music snob, the audiophile, and the casual listener, a task on which Kay was eager to advise. Soulection’s Joe Kay Presents ‘A Beginner’s Guide To Future Sounds’ |Brande Victorian |February 5, 2021 |Essence.com 

So we try not to be honey snobs, but we do want people to understand there’s a difference, and what we consume does matter. How Zach & Zoe Sweet Bee Farm Harvests Honey for NYC Restaurants |Terri Ciccone |November 28, 2020 |Eater 

You write a lot about how you were a jerk or a snob when it came to comedy or film. Patton Oswalt on Fighting Conservatives With Satire |William O’Connor |January 6, 2015 |DAILY BEAST 

Read another way, she is a horrible mother, an uptight snob, and a bit of a shrew. Colm Toibin Describes The Creation Of His Quiet Masterpiece ‘Nora Webster’ |Jennie Yabroff |November 3, 2014 |DAILY BEAST 

Surely, an unreconstructed snob could misconstrue much more. This Week’s Hot Reads: July 8, 2013 |Mythili Rao |July 8, 2013 |DAILY BEAST 

What a snob ... Oh, I understand why he wants you to go to college. The Ultimate 2012 Quiz |Michael Tomasky |December 27, 2012 |DAILY BEAST 

He was educated, like Fleming, at Eton, but unlike his creator, he was no snob. Literary Bond Superior to Movie Version |Allen Barra |November 11, 2012 |DAILY BEAST 

Perhaps, like father, I am a snob at heart and liked the sensation of a sort of artistic alliance with the British aristocracy. Ancestors |Gertrude Atherton 

"I and Disraeli put up at the same tavern last night," said a dandified snob, the other day. The Book of Anecdotes and Budget of Fun; |Various 

The three children call him the "Party Bird" for he is always so dressed up, but their father says he is "a bit of a snob." Seven O'Clock Stories |Robert Gordon Anderson 

Or perhaps a species of snob who cannot see the difference between his own foolishness and the foolishness of others. The Whale and the Grasshopper |Seumas O'Brien 

She knew him quite well for an ill-bred little snob at heart. The Highgrader |William MacLeod Raine