Movies currently in theatrical release will also be available on-demand, including The Hunt, The Invisible Man, and Emma. How the coronavirus outbreak is roiling the film and entertainment industries |Alissa Wilkinson |September 11, 2020 |Vox 

After the coronavirus pandemic forced Disney to delay and eventually cancel a global theatrical release of its live-action Mulan earlier this year, the movie debuted on the company’s streaming platform on Friday. Mulan’s Disney+ release is reigniting a boycott movement |Naomi Xu Elegant |September 4, 2020 |Fortune 

In addition to the upcoming broadcast, the studio has also created a number of documentaries and scripted products, some with scheduled theatrical releases. ‘A blueprint for what is going to happen’: Time’s Time 100 franchise on track to double revenue in 2020 |Kayleigh Barber |September 3, 2020 |Digiday 

Hollywood studios subsequently delayed the theatrical releases of their blockbuster movies that were scheduled to come out this summer. The summer movie event of 2020 was released in 2014 |Adam Epstein |August 3, 2020 |Quartz 

The 90-day theatrical exclusivity window dates back to the 1980s, when VHS tapes became popular around the world. Universal’s deal with AMC means seismic shifts in the movie industry |Adam Epstein |July 29, 2020 |Quartz 

In reality,” Francis said, “theatrical severity and sterile pessimism are often symptoms of fear and insecurity. Pope Francis Denounces the Vatican Elite’s 'Spiritual Alzheimer’s' |Barbie Latza Nadeau |December 23, 2014 |DAILY BEAST 

He speaks about the challenges of doing a live theatrical production without being able to feed off a live audience. The Cast of ‘Peter Pan Live!’ Knows You Hatewatched ‘The Sound of Music’ |Kevin Fallon |December 2, 2014 |DAILY BEAST 

This is one of the reasons why young people enjoy theatrical activities. Joseph Campbell on the Roots of Halloween |Joseph Campbell |October 31, 2014 |DAILY BEAST 

He approaches the composition of a painting rather as a theatrical director might set the scene of a play. The Stacks: Edward Hopper’s X-Ray Vision |Hilton Kramer |October 25, 2014 |DAILY BEAST 

No doubt Smith hopes to paint Glock as a monster once again, and the theatrical complaint pulls no punches. Glock Family Goes Down, Guns Blazing |Brandy Zadrozny |October 11, 2014 |DAILY BEAST 

The same is true when children play at being Indians or what not: they are not "acting" in the theatrical sense of the word. Children's Ways |James Sully 

It was theatrical: he stood upon the stage, an audience watching him with intent expectancy, wondering upon his decision. The Wave |Algernon Blackwood 

I dont believe your mother knew you had that theatrical man to drive with you, said Laura, bluntly. The Girls of Central High on the Stage |Gertrude W. Morrison 

It is with them as with theatrical pieces, there may be one good out of a thousand. A Philosophical Dictionary, Volume 1 (of 10) |Franois-Marie Arouet (AKA Voltaire) 

We are the heaven-sent leaders of all "New" enterprises, whether literary, theatrical, or artistic. Punch, or the London Charivari, Vol. 108, April 6, 1895 |Various