To their surprise, Blum said, the composition of mercury isotopes in the deep-sea fish strongly resembled that of fish inhabiting the upper 1,600 feet or so of the ocean. Researchers found signs of human pollution in animals living six miles beneath the sea |Kate Baggaley |November 19, 2020 |Popular-Science 

The games reflect the two worlds the boys inhabit, one by day and the other at night. Inside the Lives of Immigrant Teens Working Dangerous Night Shifts in Suburban Factories |by Melissa Sanchez |November 19, 2020 |ProPublica 

We know, for instance, of several occasions when fish and frogs have “rained” onto inhabited areas. What really happened during the ‘Kentucky meat shower’? |PopSci Staff |November 11, 2020 |Popular-Science 

The adventure-filled island world of “Bugsnax” that they inhabit is a surprising, comic and enjoyable one, but the game of “Bugsnax” bugged me just enough to sap some of that enjoyment. ‘Bugsnax’ is charming and funny, but sapped by vexing gameplay |Harold Goldberg |November 9, 2020 |Washington Post 

Now research shows they also damage the forest soils they inhabit. Jumping ‘snake worms’ are invading U.S. forests |Megan Sever |November 9, 2020 |Science News For Students 

Only two soil types in the world are inhabitable for this root louse: one is sand, and the other is slate. Germany’s Wine Revolution Is Just Getting Started |Jordan Salcito |April 26, 2014 |DAILY BEAST 

But to make the country inhabitable, and to build towns, it must have cost immense labor. A Philosophical Dictionary, Volume 1 (of 10) |Franois-Marie Arouet (AKA Voltaire) 

Soon the fertile lowlands ended and they passed beyond the limit of the inhabitable region. Kai Lung's Golden Hours |Ernest Bramah 

The distinguished lady considered that no cities were inhabitable except the capitals that have a court. Woman Triumphant |Vicente Blasco Ibaez 

Bellerophon made this mountain inhabitable, and was therefore said to have killed the Chimæra. The Student's Mythology |Catherine Ann White 

Water-soaked, ill-smelling, but inhabitable, the old house again possessed a light and a hearth. A Daughter of the Middle Border |Hamlin Garland