For a district and school desperate for enrollment and anxious to counter the narrative that ambitious students should seek out schools to the north or charters, Zora Williams was an absolute gift. When a Calculus Class Abruptly Became Ceramics at Lincoln High |Scott Lewis |September 16, 2020 |Voice of San Diego 

There’s an “absolute beginner” program built-in, which might come in handy for those of us who have been sitting completely still for the past six months or so. Apple just announced a new iPad, iPad Air, and Apple Watch Series 6 |Stan Horazek |September 15, 2020 |Popular-Science 

What haunted Milwaukee, among other things, was starting point guard Eric Bledsoe’s inability to shoot — an absolute killer in a series when a defense is banking on just that. The Bucks Played It Safe And Made The Wrong Kind Of History |Chris Herring (chris.herring@fivethirtyeight.com) |September 9, 2020 |FiveThirtyEight 

Amazon “has seen a 50% decrease in unintended wakes over the last year,” he said, without revealing any absolute numbers. Amazon’s A.I. voice project gets help from Facebook, Dolby, and Garmin |jonathanvanian2015 |September 9, 2020 |Fortune 

In absolute terms, it was worse than that, says Goldman Sachs. Why last week’s great tech sell-off should make investors wary |Bernhard Warner |September 8, 2020 |Fortune 

They are to face oppression with humble persistence and absolute conviction. Mexico’s Priests Are Marked for Murder |Jason McGahan |January 7, 2015 |DAILY BEAST 

House rules require an absolute majority of members voting to choose a speaker. Kamikaze Congress Prepares to Strike Boehner |Ben Jacobs |January 6, 2015 |DAILY BEAST 

Absent a body, no one can say with absolute certainty whether Castro is dead, even if all signs point in that direction. An Informant, a Missing American, and Juarez’s House of Death: Inside the 12-Year Cold Case of David Castro |Bill Conroy |January 6, 2015 |DAILY BEAST 

And this song is just absolute genius and totally universal. Yes, I Like Christmas Music. Stop Laughing. |Michael Tomasky |December 24, 2014 |DAILY BEAST 

You have to risk it, and be in danger of looking like an absolute fool. The Brit Who Stormed Broadway |Tim Teeman |December 7, 2014 |DAILY BEAST 

Solely over one man therein thou hast quite absolute control. Pearls of Thought |Maturin M. Ballou 

Marriage is like Mayonnaise sauce, either a great success or an absolute and entire failure. The Pit Town Coronet, Volume I (of 3) |Charles James Wills 

He was greeted by hoots and jeers, but with absolute imperturbability he reorganised his forces and checked the enemy. Napoleon's Marshals |R. P. Dunn-Pattison 

It is evident that an absolute increase of any variety may be accompanied by a relative decrease. A Manual of Clinical Diagnosis |James Campbell Todd 

An increase in actual number is an absolute increase; an increase in percentage only, a relative increase. A Manual of Clinical Diagnosis |James Campbell Todd