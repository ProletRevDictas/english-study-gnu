From the hallway, without entering his bedroom, she inspected Ferdinand and told the family he was getting ready to transition, Connie said. Sent Home to Die |by Annie Waldman and Joshua Kaplan |September 2, 2020 |ProPublica 

Gen Z is more likely to be inhabiting smaller apartments or living with their parents, making the office a more welcome change than it might be for someone working out of their spare bedroom. Gen Z Really Wants to Get Back to the Office |Fiona Zublin |September 1, 2020 |Ozy 

Entire office buildings sit empty as work has transitioned from cubicles and shared desks to kitchen tables and spare bedrooms. Deep Dive: How companies and their employees are facing the future of work |Digiday |September 1, 2020 |Digiday 

Alfred Kinsey catalogued the enormous variety of sexual practices unspooling inside suburban bedrooms. Gender Is What You Make of It - Issue 88: Love & Sex |Charles King |August 5, 2020 |Nautilus 

Madhav says his bedroom was messy, and his parents were always after him to clean it up. Honey, I Grew the Economy (Ep. 399) |Stephen J. Dubner |December 5, 2019 |Freakonomics 

In the 21st century women are earning their equality every step of the way… including the bedroom. Career-Minded Women Turn to Male Escorts For No-Strings Fun and (Maybe) Sex |Aurora Snow |January 3, 2015 |DAILY BEAST 

Barry showed me his room—a one bedroom with a killer view of Riverbank State Park and the Hudson. His First Day Out Of Jail After 40 Years: Adjusting To Life Outside |Justin Rohrlich |January 3, 2015 |DAILY BEAST 

That means visiting Fargo each week from the comforts of your bedroom without having to run any cables from the living room. Four TV Shows We Can’t Wait to Return In 2015 |DISH |December 22, 2014 |DAILY BEAST 

The first potential scientific secret to improving your bedroom experience: surround yourself with men. Was 2014 the Year Science Discovered The Female Orgasm? |Samantha Allen |December 6, 2014 |DAILY BEAST 

Ultimately Seevakumaran only pointed a gun at his roommate, who locked himself in a bedroom and called 911. School Shooters Love This Pickup Artist Website |Brandy Zadrozny |December 5, 2014 |DAILY BEAST 

For three days Black Sheep was shut in his own bedroom—to prepare his heart. Kipling Stories and Poems Every Child Should Know, Book II |Rudyard Kipling 

Mrs. Haughstone mentioned it in due course, however, having watched the tête-à-tête from her bedroom window, unobserved. The Wave |Algernon Blackwood 

He, too, sought his bedroom, a cool apartment with a balcony outside the French window. The Joyous Adventures of Aristide Pujol |William J. Locke 

As a matter of fact, he burst his way past my scandalized valet into my bedroom and woke me up. The Joyous Adventures of Aristide Pujol |William J. Locke 

The tired children clambered into the "bedroom," Jess coming last with the wounded dog. The Box-Car Children |Gertrude Chandler Warner