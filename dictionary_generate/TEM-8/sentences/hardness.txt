I see the hardness, but I also see that little boy inside that rough person. A former cop created a program to help Baltimore kids. Now, she’s hoping to give them more: a permanent safe haven. |Theresa Vargas |August 25, 2021 |Washington Post 

Using nanoparticles of a mineral similar to santabarbaraite, the scientists also 3-D printed strong, light materials with a range of hardness and stiffness. The teeth of ‘wandering meatloaf’ contain a rare mineral found only in rocks |Charles Q. Choi |May 31, 2021 |Science News 

Participants compared the softness or hardness of different blocks. Capturing the sense of touch could upgrade prosthetics and our digital lives |Kathiann Kowalski |April 22, 2021 |Science News 

The result is “art that is a self-willed test of hardness,” which some will consume “not to feel more but to feel less.” The Controversy Around Amazon's Them Underscores the Trouble With Realistic Violence in Genre TV |Judy Berman |April 13, 2021 |Time 

In our outdoor experiments, DyRET used a machine learning model, seeded with knowledge about the best leg configuration for a given combination of terrain hardness and roughness taken from the controlled tests. This Shape-Shifting Robot Can Rearrange Its Body to Walk in New Environments |David Howard |March 25, 2021 |Singularity Hub 

Where some hear hardness in hip-hop, Tupac heard transformation, evolution. Broadway Was Made for Tupac |Henry Louis Gates, Jr., Marcyliena Morgan |July 7, 2014 |DAILY BEAST 

His district, just south of Washington DC was among the hardness hit by the trembler. FEMA’s Budget Disaster |Daniel Stone, Laura Colarusso |August 28, 2011 |DAILY BEAST 

When she felt his hardness,” however, “the feelings evaporated. Natalie Portman's Dad's Bizarre Novel |Marlow Stern |April 26, 2011 |DAILY BEAST 

I portray Mecca as it really was, which means in all its hardness and brutality. 7 Books for the Spiritually Starved |Spencer Bailey |September 18, 2010 |DAILY BEAST 

It is of an exceedingly hard, densely compact nature; from its hardness difficult to work, but susceptible of a very high polish. Asbestos |Robert H. Jones 

All things are come upon thee, because of the multitude of thy sorceries, and for the great hardness of thy enchanters. The Bible, Douay-Rheims Version |Various 

We have learned so much lately about self-denial, and crossing one's own inclinations, and enduring hardness. The Daisy Chain |Charlotte Yonge 

A man from whom everything is torn at one blow; a man of not very strong character, not accustomed to endure hardness. Marriage la mode |Mrs. Humphry Ward 

But those who loved him best saw the stony hardness of his face, beyond anything that came after the great stroke at St. Julien. God Wills It! |William Stearns Davis