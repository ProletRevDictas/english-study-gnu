He urged, that it would flatter the peculiarities of her character, and might conciliate her good offices for his liberty. The Pastor's Fire-side Vol. 3 of 4 |Jane Porter 

She begged her not to read too late, and went out, promising to conciliate the offended Mrs. Hofer. Ancestors |Gertrude Atherton 

William omitted nothing that a brother could have done to soothe and conciliate a brother. The History of England from the Accession of James II. |Thomas Babington Macaulay 

He spoke as if he would conciliate the ghastly shades that moved restlessly up and down, when suddenlySanders, dont be a fool! The Fifth String |John Philip Sousa 

The King had probably hoped that, by calling them to his councils, he should conciliate the opposition. The History of England from the Accession of James II. |Thomas Babington Macaulay