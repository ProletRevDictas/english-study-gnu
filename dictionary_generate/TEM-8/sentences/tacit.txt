Citing the coronavirus pandemic, the IOC said it’s making this early step “given the uncertainty the world is facing right now,” but the move is also a tacit acknowledgment that its traditional bidding system is not as effective as it once was. Olympic officials targeting return to Australia for 2032 Games |Rick Maese |February 24, 2021 |Washington Post 

It’s a tacit admission that USPS is not currently America’s preferred delivery option, and an acknowledgement the organization needs to change. Delivery is more important than ever—so why is the US post office struggling? |Marc Bain |February 24, 2021 |Quartz 

A conviction is unlikely to come at this point, meaning the United States Senate is going to give its tacit approval for what happened just steps from the Senate floor. This Video of Jan. 6’s Insurrection Should Be Mandatory |Philip Elliott |February 10, 2021 |Time 

A statement signed by former cross-country world champion Paula Radcliffe, along with 22 other elite women athletes, pushed back against the assertion that having shorter races for women was a tacit insult to their ability. Should Men and Women Race the Same Distance? |Martin Fritz Huber |January 27, 2021 |Outside Online 

Part of this stemmed from the tacit encouragement of people who knew better. The lie that lingers: 3 in 10 Americans falsely believe the election was riddled with fraud |Philip Bump |January 19, 2021 |Washington Post 

Meese, with the tacit acquiescence of other top officials, had laid out a version of events all were expected to uphold. How the Reagan White House Bungled Its Response to Iran-Contra Revelations |Malcolm Byrne |November 3, 2014 |DAILY BEAST 

At the same time, this focus on pragmatism is a tacit acknowledgment from the president. The Flaw in My Brother’s Keeper |Jamelle Bouie |February 28, 2014 |DAILY BEAST 

In briefings with some reporters U.S. officials indicated tacit Libyan approval had been provided. Anas al-Liby’s Health Care During Terror Trial Could Gouge Taxpayers |Jamie Dettmer |October 22, 2013 |DAILY BEAST 

The five- page document, which has the tacit support of Senate GOP leaders, represents a remarkable shift for the party. A Clever Move by Senate Republicans |Michael Tomasky |February 27, 2013 |DAILY BEAST 

There is, in the cancellation, a tacit admission of culpability where there is none. ‘Luck’ Runs Out: If Horses Die While Cameras Roll, You Must Quit |Max Watman |March 16, 2012 |DAILY BEAST 

Without any known cause of offence, a tacit acknowledgement of mutual dislike was shewn by Louis and de Patinos. The Pastor's Fire-side Vol. 3 of 4 |Jane Porter 

The visitors, seeing how distressed the General was, by tacit consent avoided the subject, but everyone felt the dampening effect. Uncanny Tales |Various 

Mr. Pontellier had been a rather courteous husband so long as he met a certain tacit submissiveness in his wife. The Awakening and Selected Short Stories |Kate Chopin 

His attitude became one of good-humored subservience and tacit adoration. The Awakening and Selected Short Stories |Kate Chopin 

When a man talks about "spiritual discernment," he makes a tacit assertion which ought not to be allowed to pass unchallenged. God and my Neighbour |Robert Blatchford