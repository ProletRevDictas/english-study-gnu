After reading the poem “The Ballad of Birmingham” by Dudley Randall, Curtis was inspired and changed the setting of the story. How a simple story about a road trip became a kids’ classic |Haben Kelati |November 30, 2020 |Washington Post 

It found about 1 in 4 high school seniors never read stories or novels and about half never read poems on their own. ‘Nation’s report card’ shows declines for lowest-performing students |Laura Meckler |October 28, 2020 |Washington Post 

It can spew poems, short stories, and songs with little prompting, and has been demonstrated to fool people into thinking its outputs were written by a human. These weird, unsettling photos show that AI is getting smarter |Karen Hao |September 25, 2020 |MIT Technology Review 

The classic Mulan is unthreatening because she returns home to her old life as a good daughter at the end of the poem, but 1998 Mulan is so revolutionary that she single-handedly changes the entire empire’s opinion of women. The history of Mulan, from a 6th-century ballad to the live-action Disney movie |Constance Grady |September 4, 2020 |Vox 

While a student, Coel wrote Chewing Gum Dreams, a one-woman play whose title she borrowed from one of her own poems. How Michaela Coel Tells Awkward Truths |Eromo Egbejule |August 30, 2020 |Ozy 

Distraught, she wrote her poem on the subway on the way to the event. Defying Stereotypes, Young Muslim Writers Find Community Onstage |Julianne Chiaet |October 12, 2014 |DAILY BEAST 

Uneeb Khan asked in his poem about his quest for answers following the death of his non-Muslim friend. Defying Stereotypes, Young Muslim Writers Find Community Onstage |Julianne Chiaet |October 12, 2014 |DAILY BEAST 

Each word, from aa to za, gets its own two-page spread with a poem and a pic. Well, La Ti Da: Stephin Merritt’s Winning Little Words of Scrabble |David Bukszpan |October 11, 2014 |DAILY BEAST 

How much of your family was able to see you read your poem at the inauguration? Richard Blanco’s Gay Latino Poet Survival Kit |William O’Connor |October 8, 2014 |DAILY BEAST 

You include a story about a poem, The Love Song of J. Alfred Prufrock, really affecting you in your formative years. Richard Blanco’s Gay Latino Poet Survival Kit |William O’Connor |October 8, 2014 |DAILY BEAST 

In his condemned cell he composed a beautiful poem of 14 verses (“My last Thought”), which was found by his wife and published. The Philippine Islands |John Foreman 

Sebastian Brandt died; counsellor of Strassburg, a lawyer, and author of a curious poem. The Every Day Book of History and Chronology |Joel Munsell 

And in his heart the name of the poem repeated itself with significant insistence: Unwelcome! The Wave |Algernon Blackwood 

They were still talking of the poem and the music, exchanging intimate thoughts in the language he could not understand. The Wave |Algernon Blackwood 

He was for many years judge of a court in Connecticut, and is known as the author of the popular poem, McFingal. The Every Day Book of History and Chronology |Joel Munsell