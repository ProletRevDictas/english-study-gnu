Covid-19 has triggered economists to rethink their profession all the way from the philosophical down to the practical. Econ 3.0? What economists can contribute to (and learn from) the pandemic |Claire Beatty |September 28, 2020 |MIT Technology Review 

This is the philosophical quest for the ground of moral truth. The Universe Knows Right from Wrong - Issue 89: The Dark Side |Philip Goff |September 9, 2020 |Nautilus 

I do a lot of reading on different spiritual and philosophical traditions. Queery: Kerry Hallett |Staff reports |September 2, 2020 |Washington Blade 

I was waiting for the words to come out of my mouth, elegantly framing something philosophical and profound. ‘The Dream Architects’: Inside the making of gaming’s biggest franchises |Rachel King |September 1, 2020 |Fortune 

The panelists fielded a range of questions, from the philosophical to the tactical. Watch: An expert panel offers advice on how to build antiracist companies |Heather Landy |August 14, 2020 |Quartz 

Dear Thief is worthy of the abused critical adjectives philosophical, atmospheric, and masterful. A Novel Nearly Impossible to Review |Nicholas Mancusi |December 28, 2014 |DAILY BEAST 

But what about the philosophical beliefs of thousands of incarcerated pregnant women (PDF) across the United States? The GOP’s Hidden Ban on Prison Abortions |Harold Pollack |December 13, 2014 |DAILY BEAST 

There was Radim Palouš, the sonorous philosophical godfather of Kampademia. How Havel Inspired the Velvet Revolution |Michael Zantovsky |December 6, 2014 |DAILY BEAST 

It is a brief text with philosophical leanings that revolves around the notions of normality and abnormality in human nature. Living Black & Gay in the ’50s |Alain Mabanckou |December 3, 2014 |DAILY BEAST 

It is not hard to see how this debate calls into question deeply personal religious and philosophical beliefs. What It’s Like to Wake Up Dead |Dr. Anand Veeravagu, MD, Tej Azad |November 21, 2014 |DAILY BEAST 

Martyrdoms do not occur for ethical generalizations, much less for philosophical affirmations. Solomon and Solomonic Literature |Moncure Daniel Conway 

It struck me that he was uncommonly philosophical about it, so I merely grunted and went on with my dinner. Raw Gold |Bertrand W. Sinclair 

It relates to ancient philosophical ideas concerning the spiritual and the material worlds. Solomon and Solomonic Literature |Moncure Daniel Conway 

This rather is, I should deem, the more perilous, and a plainer and better object for philosophical attack. Notes and Queries, Number 177, March 19, 1853 |Various 

He was one of the lords of session in Scotland, and a philosophical writer of considerable learning, but of peculiar notions. The Every Day Book of History and Chronology |Joel Munsell