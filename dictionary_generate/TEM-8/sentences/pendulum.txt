Over the decades, the pendulum has swung back and forth between more decentralized and more coordinated models of the economy, each time expecting a different result. Why do attempts at reforming capitalism fail? |jakemeth |October 10, 2020 |Fortune 

Elections have swung back and forth in an almost predictable pendulum fashion since 1992 — unified control of one party, divided government, unified control of the other party, and so forth, over and over. How Hatred Came To Dominate American Politics |Lee Drutman (drutman@newamerica.org) |October 5, 2020 |FiveThirtyEight 

In the past four months, talk of reform and proposed structural changes to combat racism at agencies has started to shift the pendulum — but more action is required. ‘It’s all been plan, plan, plan mode:’ Agencies have big ideas for greater diversity, but more action is needed |Seb Joseph |September 15, 2020 |Digiday 

Snails with coiled or elongated shells tended to swim straight up, and to sink straight down whenever they stopped flapping, their shells hanging like pendulums beneath their wings. Sea butterflies’ shells determine how the snails swim |Maria Temming |September 8, 2020 |Science News 

As the pendulum swings away from data-heavy, third-party based audience targeting, publishers are using contextual data tools in smarter ways and gaining more control over their contextual ad revenues. ‘Supercharging contextual’: Publishers eye potential for contextual ad revenue growth |Lucinda Southern |September 3, 2020 |Digiday 

But gerrymandering has cold cocked the pendulum weight, stopped it dead. Up to a Point: In Defense of Lobbyists |P. J. O’Rourke |October 25, 2014 |DAILY BEAST 

The pendulum swing between moods and tone, however, became a staple of the shoot. How Bill Hader and Kristen Wiig Pulled Off Their Most Dramatic Roles Yet |Kevin Fallon |September 12, 2014 |DAILY BEAST 

I think the pendulum has swung back on that because of books like The Blood Telegram. The Real Legacy of Richard Nixon |Scott Porch |August 5, 2014 |DAILY BEAST 

The pendulum has swung too far in the other direction from physician paternalism towards willful ignorance by patients. Why Smart People Are Dumb Patients |Jean Kim |July 14, 2014 |DAILY BEAST 

Shaked spoke in these generalities initially—referring to two sets of people, two polar opposites on a pendulum. Knesset Member Walks Back On Facebook Post Calling Palestinian Kids ‘Little Snakes’ |Gideon Resnick |July 11, 2014 |DAILY BEAST 

By what word is the relation between “pendulum” and “a smile and tear” described? Assimilative Memory |Marcus Dwight Larrowe (AKA Prof. A. Loisette) 

The sword clattered from his hand and rolled, with a pendulum-like movement, to the feet of Garnache. St. Martin's Summer |Rafael Sabatini 

To swing a pendulum, picked out from a number of them at random, without touching it is a very puzzling trick. The Boy Mechanic, Book 2 |Various 

Another pendulum may be pointed out and he will start that one apparently by looking at it, while the other one stops. The Boy Mechanic, Book 2 |Various 

With a little practice anyone can become a skilled medium in pendulum swinging. The Boy Mechanic, Book 2 |Various