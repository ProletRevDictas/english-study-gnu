The poll found 54 percent in Wisconsin disapprove of the way Trump has handled the pandemic, while 44 percent approve. Trump keeps dodging the crux of major issues — and that’s showing in his reelection prospects |Amber Phillips |September 16, 2020 |Washington Post 

On the pandemic, 54 percent disapprove of the way he has handled things, while 44 percent approve. Post-ABC Wisconsin poll shows Biden holding narrow edge over Trump |Dan Balz, Emily Guskin |September 16, 2020 |Washington Post 

A 58 percent majority of Minnesota registered voters say the nation’s economy is “not so good” or “poor,” though voters split about evenly on Trump’s handling of the issue, with 47 percent approving and 49 percent disapproving. Post-ABC poll and others suggest Minnesota has shifted since 2016, but by how much? |Scott Clement, Dan Balz |September 16, 2020 |Washington Post 

Interestingly, when asked about a proposed sale, 50% of the public say they disapprove of a forced TikTok sale. ‘Deal of a decade’: How buying TikTok could transform Microsoft |Lance Lambert |August 25, 2020 |Fortune 

About 58 percent now disapprove of his handling of the pandemic while just 38 percent approve, according to FiveThirtyEight’s coronavirus polling tracker. Americans Increasingly Dislike How Republican Governors Are Handling The Coronavirus Outbreak |Geoffrey Skelley (geoffrey.skelley@abc.com) |July 17, 2020 |FiveThirtyEight 

I disapprove of what you say, but I will defend to the death your right to say it. The Muslim Cop Killed by Terrorists |Michael Daly |January 9, 2015 |DAILY BEAST 

Majorities disapprove of the job President Obama is doing, as well as the job being done be Republicans and Democrats in Congress. New Harvard Study Finds the Door to Millennial Vote Is Open to GOP |Kristen Soltis Anderson |October 30, 2014 |DAILY BEAST 

Some believe you need to be taught to disapprove of her morals and ethics. My Commencement Speech to Rutgers’ Geniuses: Go Forth and Fail |P. J. O’Rourke |May 18, 2014 |DAILY BEAST 

So why do we still love her but disapprove of all things GOOP? Why Do We Love Gisele Bundchen but Hate Gwyneth Paltrow? |Erin Cunningham |May 15, 2014 |DAILY BEAST 

Her uncles disapprove of her work, but the real challenge lies in persuading parents to allow their daughters to participate. Syria’s Female Refugee Soccer Stars |Peter Schwartzstein |March 31, 2014 |DAILY BEAST 

The acts done under incompetent rulers, by those who disapprove of their claims, come from neither. The Ordinance of Covenanting |John Cunningham 

With the supporters of such constitutions unamended, some who disapprove of them, have in some respects to co-operate. The Ordinance of Covenanting |John Cunningham 

There is no academy to check abuse, no large, cultivated public to disapprove of the new forms. Frdric Mistral |Charles Alfred Downer 

On the whole, Bud did not greatly disapprove of that; he was too actively resentful of his own mother-in-law. Cabin Fever |B. M. Bower 

They neither approve nor disapprove, commend or condemn, till they have consulted his looks and his countenance. The 'Characters' of Jean de La Bruyre |Jean de La Bruyre