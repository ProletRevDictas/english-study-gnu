When he was 12, Philip moved to a novel boarding school in Germany, Schloss Salem, set up by his sister’s father-in-law and an eccentric but brilliant educationist named Kurt Hahn. Prince Philip, royal consort to Queen Elizabeth II, dies at 99 |Adrian Higgins |April 9, 2021 |Washington Post 

Here I am obliged to mention something which as an educationist I can by no means pass lightly by. Autobiography of Friedrich Froebel |Friedrich Froebel 

The enterprising lady-educationist was a Mrs. Murray, who had been a mistress in the Female Asylum. The Story of Madras |Glyn Barlow 

It was a stroke of genius to invent a great educationist called Hégésippe Simon. The Book of This and That |Robert Lynd 

Such must be the conduct of the Educationist, if he expects to succeed in an equal degree. A Practical Enquiry into the Philosophy of Education |James Gall 

Precisely similar should be the plan of operation pursued by the Educationist. A Practical Enquiry into the Philosophy of Education |James Gall