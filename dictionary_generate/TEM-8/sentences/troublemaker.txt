Months later, he purged the state party of troublemakers ahead of what would be his final re-election bid in 2016. No, Trumpism Isn’t Over. Look at What Just Happened in Arizona |Philip Elliott |January 25, 2021 |Time 

The union reminded flight attendants that the best solution is to keep potential troublemakers off planes. FAA warns of jail time, fines as airports and airlines prep for unruly passengers ahead of the inauguration |Ian Duncan, Lori Aratani |January 11, 2021 |Washington Post 

If other leaders are tone-policing you, and you’re too loud, you’re like a troublemaker—we all know that’s what happens to people like me—then if someone defends you, they’re obviously going to also be a problem for the other leaders. “I started crying”: Inside Timnit Gebru’s last days at Google—and what happens next |Karen Hao |December 16, 2020 |MIT Technology Review 

Last year, pro-democracy protesters in Hong Kong used the occasion to don Halloween-themed masks at a time the city’s government had banned face coverings to make it easier to identify troublemakers. Be Very Afraid: A Virtual House of Horrors |Tracy Moran |October 25, 2020 |Ozy 

I am happy to report that our listenership includes plenty of troublemakers as well as smoothers-over. How to Make Meetings Less Terrible (Ep. 389) |Stephen J. Dubner |September 19, 2019 |Freakonomics 

Trailblazing comedian and troublemaker Joan Rivers died on Thursday at the age of 81. What Joan Rivers Said She Would Do If She Were Dictator of America |Asawin Suebsaeng |September 5, 2014 |DAILY BEAST 

The Nasty Gal queen went from troublemaker to CEO of a million-dollar company in just a few years. The ‘Nasty Gal’ Invasion: Sophia Amoruso Wants to Create an Army of #GIRLBOSSes |Alison Baitz |May 8, 2014 |DAILY BEAST 

“Broken” is the first song the Canadian troublemaker unleashed. Erin Andrews Joins ‘DWTS’; Marion Cotillard is Lady Macbeth | |February 24, 2014 |DAILY BEAST 

In later years, Mandela would translate it with mingled pride and self-deprecating humor as “troublemaker.” Mandela: The Miracle Maker |Sam Seibert |December 5, 2013 |DAILY BEAST 

Ted Cruz is known as a right-wing troublemaker, but he is tame compared to his dad. The Six Craziest Quotes From Ted Cruz’s Father, Rafael Cruz |Brandy Zadrozny |November 7, 2013 |DAILY BEAST 

We don't know whether you're working for him or not, but you're a troublemaker. A World is Born |Leigh Douglass Brackett 

Brownie was a troublemaker, Brownie talked too much, Brownie philosophized in a world that ridiculed philosophy. Derelict |Alan Edward Nourse 

Yet the bloody shirt lingered long as a troublemaker, and was invoked by both parties. Marse Henry (Vol. 2) |Henry Watterson 

Well, I can hardly be called a troublemaker, and you had a pretty peaceful time with me. Creditors; Pariah |August Strindberg 

In such cases, Investigations had stepped in and the Martian or Earthling troublemaker had been sent to the rare-earth mines. Blind Spot |Bascom Jones