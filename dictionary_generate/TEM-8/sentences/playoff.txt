During a Patroons playoff game, Richardson had screamed profanities and a gay slur at hecklers. Mark Cuban Warns That Basketball Players Could Get the Sterling Treatment Next |Evan Weiner |June 3, 2014 |DAILY BEAST 

They made the playoffs just four times and managed to eke out only one playoff series win. Donald Sterling’s Last Laugh: Force Him to Sell the Clippers and He Could Pay No Taxes |Nick Lum |May 5, 2014 |DAILY BEAST 

Certainly looked like Patriots had been sampling local wares during Denver playoff game against Broncos. Up to a Point: P.J. O’Rourke on SOTU, the Super Bowl and Nukes |P. J. O’Rourke |January 31, 2014 |DAILY BEAST 

Problem is, the New England Patriots and Tom Brady—the winningest quarterback in playoff history—are riding into town. Peyton Manning and Tom Brady Don’t Control Their Own Legacies |Evin Demirel |January 18, 2014 |DAILY BEAST 

This should be remembered when Brady and Manning square off for what could be their final playoff showdown. Peyton Manning and Tom Brady Don’t Control Their Own Legacies |Evin Demirel |January 18, 2014 |DAILY BEAST