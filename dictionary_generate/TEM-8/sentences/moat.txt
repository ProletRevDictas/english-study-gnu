According to local media, Hebei’s leaders have committed to defending the capital, pledging the province will act as a “political moat” around Beijing, protecting it from the viral spread. ‘Political moat’: China locks down 11 million to insulate Beijing from new COVID outbreak |Eamon Barrett |January 11, 2021 |Fortune 

For Hawaii, the Pacific Ocean has helped serve as the world’s biggest moat. Hawaii Is Riding Out the COVID-19 Storm. But Geographic Isolation Isn't the Blessing it May Seem |Alejandro de la Garza |November 25, 2020 |Time 

So if the prince’s castle has a moat, Cinderella might be able to make a grand entrance from a pumpkin after all. Here’s how giant pumpkins get so big |Bethany Brookshire |October 28, 2020 |Science News For Students 

With the rollout of the new campaign format, Snapchat is widening its moat against TikTok and other challengers for short-form video budgets. Snapchat is pitching high-frequency, high-reach ‘Platform Burst’ ad campaigns |Seb Joseph |September 28, 2020 |Digiday 

Broadcast and cable are highly geographic but the franchise value becomes higher because of the regulatory moat. With Goat Capital, Justin Kan and Robin Chan want to keep founding alongside the right teams |Eric Eldon |September 17, 2020 |TechCrunch 

But over the years, cloistered in their mountain keep, complete with moat, Bender and Patton became ever more reclusive. Gems, Guns and Death in a Jungle Mansion |Zach Dyer |May 25, 2014 |DAILY BEAST 

The brain is a castle and this is its moat, as experts have described it. Scientists Find Bacteria Where It Isn’t Supposed to Be: The Brain |Amanda Schaffer |March 17, 2013 |DAILY BEAST 

Thinking to escape and summon assistance from the cantonment, Douglas mounted the wall and leaped into the moat. The Red Year |Louis Tracy 

The house itself was built nearly two hundred years earlier and was later surrounded by a moat as a further means of defense. British Highways And Byways From A Motor Car |Thomas D. Murphy 

But the Scots tower proved useless, for its wheels stuck in the mud of the moat, and it could not be got up to the wall. King Robert the Bruce |A. F. Murison 

He'll immediately throw down his bunch of flowers and dive despairingly into the moat. First Plays |A. A. Milne 

An ornamental lake indicates where once was the moat, but the outlines of the walls are shown only by grass-covered ridges. British Highways And Byways From A Motor Car |Thomas D. Murphy