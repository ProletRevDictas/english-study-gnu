When Hurricane Gustav struck the Gulf Coast in 2008, leaving more than 100,000 Louisiana customers without power for over a week, Entergy’s grid was in shambles. Entergy Resisted Upgrading New Orleans’ Power Grid. When Ida Hit, Residents Paid the Price. |by Max Blau and Annie Waldman, ProPublica, and Tegan Wendland, WWNO/NPR; Photography by Kathleen Flynn, special to ProPublica |September 22, 2021 |ProPublica 

At least 21 people are dead, hundreds of homes are in shambles and the wreckage of people’s lives is strewn across the landscape. Tennessee floods show a pressing climate danger across America: ‘Walls of water’ |Sarah Kaplan |August 24, 2021 |Washington Post 

My supposition is that North Korea does not want to be 90% dependent upon China for its economy, which is currently in a shambles under those circumstances, and is seeking a different kind of independence. 'This Is a Window of Opportunity.' Ret. General Vincent K. Brooks on Why Things Might Be Moving Again With North Korea |Charlie Campbell / Shanghai |June 24, 2021 |Time 

With his emotional life in shambles, Pauli took up drinking and smoking heavily. The Synchronicity of Wolfgang Pauli and Carl Jung - Issue 93: Forerunners |Paul Halpern |November 18, 2020 |Nautilus 

The resulting chaos has left nearly 200,000 Americans dead and the economy in shambles. Vote for Biden (duh) |Kevin Naff |September 10, 2020 |Washington Blade 

The deck of the Frenchman was truly a shamble; not a spot appeared free from some dead or wounded occupant. Hurricane Hurry |W.H.G. Kingston 

The men were past revolt now, they could only shamble dizzily about. Beggars on Horseback |F. Tennyson Jesse 

They could not walk, they could only shamble; they could not laugh, they could only leer. The Profits of Religion, Fifth Edition |Upton Sinclair 

His knees still knocked together in a loathsome paralysis, but he made effort to shamble forward. The Air Pirate |Cyril Arthur Edward Ranger Gull 

It is called Shamble Oak because a butcher once used its hollow trunk to conceal stolen sheep. Zigzag Journeys in Europe |Hezekiah Butterworth