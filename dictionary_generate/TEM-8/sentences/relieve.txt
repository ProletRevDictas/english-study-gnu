They seem to understand that what matters most in the end is whether the patient is relieved of his suffering. How colonialism and capitalism helped place a stigma on mental illness |Balaji Ravichandran |February 12, 2021 |Washington Post 

Djamo relieves that stress and even allows customers to use their cards with zero fees in a wide range of services. YC-backed Djamo is building a financial super app for consumers in Francophone Africa |Tage Kene-Okafor |February 5, 2021 |TechCrunch 

I was relieved to be one step safer to the people around me in the community, all the while acknowledging that my social privilege, access to technology, and vehicle had given me a major advantage. I jumped the queue to get an expiring vaccine. Did I do the right thing? |Niall Firth |February 1, 2021 |MIT Technology Review 

Across the region, children giggled under snowflakes while their parents said they were relieved to see some magic back in their lives. D.C. area’s biggest snowstorm in two years brings slick roads, but much-needed respite |Katherine Shaver, Emily Davies, Lauren Lumpkin |February 1, 2021 |Washington Post 

The LuxFit is also helpful for relieving back pain that crops up in the course of daily life. This Is the Foam Roller My Family Fights Over |Abigail Wise |January 27, 2021 |Outside Online 

“Seth kept the team together, but his constant need to relieve himself kept the team down,” says the announcer. James Franco and Seth Rogen Get ‘Naked and Afraid’… And It’s Hilarious |Marlow Stern |December 8, 2014 |DAILY BEAST 

Women have become more professional and independent, so a lot of them are looking at role revisal to relieve similar tensions. Whip It: Secrets of a Dominatrix |Justin Jones |November 25, 2014 |DAILY BEAST 

Adam Kawalek went from Los Angeles to Gaza to try to relieve the suffering of the sick and wounded there. A Gay Jewish Zionist American Doctor in Gaza and What He Saw |Itay Hod |September 22, 2014 |DAILY BEAST 

However, medications and therapies have been developed to relieve symptoms significantly. The Burden Robin Williams Carried: Diagnosed With Parkinson’s and Depression |Dr. Anand Veeravagu, MD, Tej Azad |August 15, 2014 |DAILY BEAST 

Each of these developments would be good news for Tesla and would relieve the company of billions of dollars in annual costs. Tesla’s Radical Patent Move is a Plot to Take Over the Road |Daniel Gross |June 15, 2014 |DAILY BEAST 

We should have to admit that the new law does little or nothing to relieve such a situation. Readings in Money and Banking |Chester Arthur Phillips 

Learn to do well: seek judgment, relieve the oppressed, judge for the fatherless, defend the widow. The Bible, Douay-Rheims Version |Various 

Ciudad Rodrigo and Almeida fell without the English commander making any apparent effort to relieve them. Napoleon's Marshals |R. P. Dunn-Pattison 

Dorothy had tried her best to relieve Letty of half her burthen, and in return had been made a bone of contention between them. The World Before Them |Susanna Moodie 

Consequently the House gave a friendly reception to a Bill intended to relieve them of some of their pecuniary burdens. Punch, or the London Charivari, Volume 158, April 28, 1920 |Various