We see detoxing as a path to transcendence, a symbol of modern urban virtue and self-transformation through abstinence. How Taryn Toomey’s ‘The Class’ Became New York’s Latest Fitness Craze |Lizzie Crocker |January 9, 2015 |DAILY BEAST 

Stephanie Giorgio, a classical musician, credits The Class for helping her cope with anxiety, focus, fear, and self-doubt. How Taryn Toomey’s ‘The Class’ Became New York’s Latest Fitness Craze |Lizzie Crocker |January 9, 2015 |DAILY BEAST 

For someone with anorexia, self-starvation makes them feel better. How Skinny Is Too Skinny? Israel Bans ‘Underweight’ Models |Carrie Arnold |January 8, 2015 |DAILY BEAST 

But in the case of black women, another study found no lack of interest. The Unbearable Whiteness of Congress |Dean Obeidallah |January 8, 2015 |DAILY BEAST 

But if you have a hearing and you prove that someone is mature enough, well then that state interest evaporates. Should Teens Have The Right To Die? |Brandy Zadrozny |January 8, 2015 |DAILY BEAST 

In the parish churches, many of which are of great interest, the predominant styles are Decorated and Perpendicular. Encyclopaedia Britannica, 11th Edition, Volume 3, Slice 4 |Various 

And with some expressions of mutual good-will and interest, master and man separated. The Posthumous Papers of the Pickwick Club, v. 2(of 2) |Charles Dickens 

A desultory conversation on politics, in which neither took the slightest interest, was a safe neutral ground. The Pit Town Coronet, Volume I (of 3) |Charles James Wills 

Let the thought of self pass in, and the beauty of great action is gone, like the bloom from a soiled flower. Pearls of Thought |Maturin M. Ballou 

His also was the intellectual point of view, and the intellectual interest in knowledge and its deductions. The Mediaeval Mind (Volume II of II) |Henry Osborn Taylor