At the time, they were a tease, revealing little detail about their “incredibly exciting” method that matched experimental results in accuracy. Protein Folding AI Is Making a ‘Once in a Generation’ Advance in Biology |Shelly Fan |July 20, 2021 |Singularity Hub 

It is this giant tease, as though someone built Carnegie Hall but forgot to put doors on it. Gene Weingarten: New York, from the bizarre to the truly bizarre |Gene Weingarten |June 3, 2021 |Washington Post 

That’s the tantalizing tease Malcolm Gladwell drops in his Revisionist History podcast episode on memory … and how it failed the NBC News anchor in the worst possible way. Do You Remember? |Sean Culligan |April 13, 2021 |Ozy 

Greenhouse tomatoes are ever-present — they’re a tease, often watery and dull. How to make a wonderful BLT with out-of-season tomatoes |Daniela Galarza |February 25, 2021 |Washington Post 

Flood said Emrick will have a limited role with NBC and will provide the voice-over for the opening tease on Wednesday night’s telecast. NBC Sports has parted ways with Mike Milbury, months after sexist on-air comment |Matt Bonesteel |January 11, 2021 |Washington Post 

He proceeds to tease me, asking if our interview is “secretly a date?” My Bizarre Night With James Deen, Libertarian Porn Star |Emily Shire |November 12, 2014 |DAILY BEAST 

His youngest son, Orange Scott, was a rough-and-tumble trickster and a terrible tease. Those Kansas City Blues: A Family History |Katie Baker |October 24, 2014 |DAILY BEAST 

The sexual acts are mysterious, unpredictable, and passionate; they tease your senses. What Porn Stars Find Sexy on TV: From ‘Game of Thrones’ to ‘Deadliest Catch’ |Aurora Snow |September 20, 2014 |DAILY BEAST 

We were about to go to sleep, but I decided to tease him about his weird habit of having the pillow a certain way on the bed. I Was Pregnant When He Hit Me. Here's #WhyIStayed. |Anonymous |September 10, 2014 |DAILY BEAST 

Journalism assumes an immutable truth, that a few more calls, a bit more reporting will tease it out of reluctant informants. We Interrupt This Broadcast: How a TV Producer Learned to Write Fiction |George Lerner |September 9, 2014 |DAILY BEAST 

Osmond Orgreave entered the room, quizzical, and at once began to tease Clayhanger about the infrequency of his visits. Hilda Lessways |Arnold Bennett 

The moment we arrive at the house the others begin to tease us and leave us together. The Life &amp; Letters of Peter Ilich Tchaikovsky |Modeste Tchaikovsky 

The two engineers, being idle, had drunk liquor and were trying to tease the animals nearby. Kari the Elephant |Dhan Gopal Mukerji 

She told us children never to tease him, or worry him, but that we needn't be afraid of him, either. The Sunbridge Girls at Six Star Ranch |Eleanor H. (Eleanor Hodgman) Porter 

Dont tease me, she said, so quietly that an embarrassing silence fell between them. The Woman Gives |Owen Johnson