Those who were laid off cried, slammed doors, and shouted in frustration. What if Your Company Had No Rules? (Bonus Episode) |Maria Konnikova |September 12, 2020 |Freakonomics 

We put together this user guide to help you navigate Disrupt 2020 with maximum efficiency and minimal frustration. A user’s guide to Disrupt 2020 |Emma Comeau |September 11, 2020 |TechCrunch 

Nathan Johnson, a representative from TruConnect and a panelist on the town hall, expressed his ongoing frustrations with the California Public Utilities Commission. Sacramento Report: Bipartisan Support for a Special Session |Sara Libby |September 11, 2020 |Voice of San Diego 

“And that was the start of my frustration,” said Cañestro, a professor of genetics, microbiology and statistics at the University of Barcelona and a group leader at its Institute for Research on Biodiversity. By Losing Genes, Life Often Evolved More Complexity |Viviane Callier |September 1, 2020 |Quanta Magazine 

The employees’ frustrations are not only aimed at the leaders atop their companies but also those overseeing individual departments. ‘Feels very much lip service’: Media employees agitate over companies’ inaction following diversity and inclusion pledges |Tim Peterson |August 31, 2020 |Digiday 

That man was Xavier Cortada, a gay man who wrote of his frustration that he and his partner of eight years were unable to marry. Jeb Bush’s Unseen Anti-Gay Marriage Emails |Jackie Kucinich |January 9, 2015 |DAILY BEAST 

After some animated debate at the conference, Lelaie declared, with some frustration, “If you push on the stick, you will fly.” Flight 8501 Poses Question: Are Modern Jets Too Automated to Fly? |Clive Irving |January 4, 2015 |DAILY BEAST 

My survival no longer offers the time, but to see others expressing frustration they can barely put into words is helpful. The Unbearable Whiteness of Protesting |Rawiya Kameir, Judnick Mayard |December 10, 2014 |DAILY BEAST 

He also posted the results of the interactions that usually ended in frustration, but on rare successes, began with “DATE!” School Shooters Love This Pickup Artist Website |Brandy Zadrozny |December 5, 2014 |DAILY BEAST 

Cook walked more slowly than most, stopping to engage with passersby who expressed their own frustration and support. ‘They Let Him Off?’ Scenes from NYC in Disbelief |Jacob Siegel |December 4, 2014 |DAILY BEAST 

Smoke, whiskey, good music—they washed his mind clean of worry and frustration; he drifted off in a mist of unformed dreams. Security |Poul William Anderson 

"He died of frustration—or sorrow—over having killed the wrong man," Taylor said grimly. The Whispering Spheres |Russell Robert Winterbotham 

They became silent under Emil's gaze of acute pathos—human life aware of its present frustration. The Wrong Twin |Harry Leon Wilson 

This frustration of Lincoln's ambition had a marked effect on his political views. Abraham Lincoln, Volume 2 (of 2) |William H. Herndon 

John Marshall Glenarm had explicitly provided against any such frustration of his plans. The House of a Thousand Candles |Meredith Nicholson