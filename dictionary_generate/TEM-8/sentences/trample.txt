"Trample on my feelings as much as you like," and as he arranged Sylvia's cushions he gave a second sharp glance at her face. The Opened Shutters |Clara Louise Burnham 

Trample not on any; there may be some work of grace there, that thou knowest not of. Aids to Reflection |Samuel Taylor Coleridge 

Trample me with the blessed weight of the adorable feet which crushed the serpent! Very Woman |Remy de Gourmont 

Trample, too, upon that parliament in their turn, and scornfully expel them as soon as they gave him ground of dissatisfaction? The History of England in Three Volumes, Vol.I., Part E. |David Hume 

Trample out Protestantism; or drive it into remote nooks, where under sad conditions it might protract an unnoticed existence. History of Friedrich II. of Prussia, Vol. IX. (of XXI.) |Thomas Carlyle