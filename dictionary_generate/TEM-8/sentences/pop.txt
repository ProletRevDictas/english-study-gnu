Just tap and hold on the keyboard when it pops up in any app, then slide your finger across to the keys you want to press without lifting it. Get around your phone more quickly than you already are |David Nield |September 16, 2020 |Popular-Science 

It started out as a way of finding stuff on the various websites that were popping up, but ended up shaping the very medium it was indexing. Are search engines dead in China? |Ada Luo |September 14, 2020 |Search Engine Watch 

In the original complaint, Murphy made the eye-popping charge that Nielsen had perjured herself in front of Congress. What to make of the DHS whistleblower’s shocking complaint |Alex Ward |September 11, 2020 |Vox 

It’s a revolving door of regular events, from karaoke and DJ nights to pop-up dinners. Pre-pandemic, Kuala Lumpur’s hospitality scene was heating up with new design-forward boutique hotels |Rachel King |September 5, 2020 |Fortune 

Instances of the Sator square have popped up all over Europe. The ancient palindrome that explains Christopher Nolan’s Tenet |Alissa Wilkinson |September 4, 2020 |Vox 

Sophisticated, nuanced, melodious pop music, that sweeps you away. Belle & Sebastian Aren’t So Shy Anymore |James Joiner |January 7, 2015 |DAILY BEAST 

The airline industry objects that sometimes these deployable recorders can pop out without cause, spreading needless alarm. Red Tape and Black Boxes: Why We Keep ‘Losing’ Airliners in 2014 |Clive Irving |December 29, 2014 |DAILY BEAST 

Within a few summer weeks, “Hot N—” had become an inescapable pop-culture phenomenon and Bobby landed a major record deal. Bobby Shmurda and Rap’s Ultimate Hoop Dream |Rawiya Kameir |December 23, 2014 |DAILY BEAST 

Growing up in that suburbia and air of pop culture, these images stayed with me like a weird dream. Tim Burton Talks ‘Big Eyes,’ His Taste For the Macabre, and the ‘Beetlejuice’ Sequel |Marlow Stern |December 17, 2014 |DAILY BEAST 

Nicki treats the obsession with her pop ambitions as an irrelevant, surface-level irritation. Nicki Minaj Bares Her Own Vulnerability on ‘The Pinkprint’ |Rawiya Kameir |December 16, 2014 |DAILY BEAST 

Now, when I am called upon to produce a laugh from Timothy, I no longer make faces or "pop." Punch, or the London Charivari, Volume 158, April 28, 1920 |Various 

Pop that shawl away in my castor, Dodger, so that I may know where to find it when I cut; thats the time of day! Oliver Twist, Vol. II (of 3) |Charles Dickens 

There came a "pop" like an exploding fire-cracker, and a bullet whistled past Matt's ear. Motor Matt's "Century" Run |Stanley R. Matthews 

Here's my notion: To make a pop'lar measure of it; somethin' that'll appeal to the folks. Scattergood Baines |Clarence Budington Kelland 

To Mildred he was one of that numerous army of brevet relations known as gran-pop, pop, or uncle. The Fifth String |John Philip Sousa