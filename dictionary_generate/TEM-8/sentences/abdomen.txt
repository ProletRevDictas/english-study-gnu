Others saw a hole in a toad’s abdomen with their teeth, shove their heads in and gorge on organs and tissues — while the amphibian is still alive. This snake rips a hole in living toads’ stomachs to feast on their organs |Erin Garcia de Jesus |October 2, 2020 |Science News 

Hayward was drive-stunned twice in the abdomen and once in the thigh. The Startling Reach and Disparate Impact of Cleveland Clinic’s Private Police Force |by David Armstrong |September 28, 2020 |ProPublica 

The other was in stable condition with an injury to the abdomen. A woman killed. An officer shot. And no one legally responsible. |David Fahrenthold |September 24, 2020 |Washington Post 

Then there are the aptly-named bombardier beetles, which can discharge noxious and boiling hot chemicals from their abdomens when under attack. This scientist thought he’d found the source of all sexual energy |PopSci Staff |September 17, 2020 |Popular-Science 

At the end of that chain, Hox genes turn on to specify the insect’s head, thorax and abdomen. How Two Became One: Origins of a Mysterious Symbiosis Found |Viviane Callier |September 9, 2020 |Quanta Magazine 

But even I have my share of patients with chronic pain of some kind, be it in the abdomen or head or back. DEA's Painkiller Crackdown Too Little, Too Late? |Russell Saunders |August 27, 2014 |DAILY BEAST 

The tight fit also restricts your abdomen, reducing your food intake during the day. Waist Training: Can You Cinch Your Waist Thin? |DailyBurn |July 18, 2014 |DAILY BEAST 

The sick sister was healed after relics from John XXIII were placed on the fistula on her abdomen. Popes, Saints, Miracles, Weird Relics and Odd Omens Converge on Rome |Barbie Latza Nadeau |April 26, 2014 |DAILY BEAST 

Two bullets lodged in his abdomen and he was taken to the hospital for surgery. The Man Syria’s Jihadists Want Dead |Patrick Hilsman |January 30, 2014 |DAILY BEAST 

A WWII re-enactor is brought into the hospital when a homemade bazooka backfires and leaves an unexploded grenade in his abdomen. 11 Wacky, Moving, Memorable ‘Grey’s Anatomy’ Moments (VIDEO) |Chancellor Agard |October 10, 2013 |DAILY BEAST 

The body is very convex:, having the thorax as wide as the abdomen, subquadrate, with very convex sides. Narrative of a Survey of the Intertropical and Western Coasts of Australia] [Volume 2 of 2] |Phillip Parker King 

The head and thorax are of the colour of the wings, their sides and the conical abdomen being rather lighter. Narrative of a Survey of the Intertropical and Western Coasts of Australia] [Volume 2 of 2] |Phillip Parker King 

The Semangs of Malacca are jet-black in color, with thick lips, flat nose, and protruding abdomen. Man And His Ancestor |Charles Morris 

Some slight injury in the abdomen, as from a blow or a kick, may precipitate an attack in predisposed individuals. Essays In Pastoral Medicine |Austin Malley 

I found him extremely unwell, with what I conceived to be a dropsy, for his abdomen was very much swollen. The Expeditions of Zebulon Montgomery Pike, Volume II (of 3) |Elliott Coues