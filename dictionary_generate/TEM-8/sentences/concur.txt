Most experts concur that even with the highly transmissible delta variant of the coronavirus circulating in the country, schools should remain open. 6 answers to parents’ COVID-19 questions as kids return to school |Sujata Gupta |August 11, 2021 |Science News 

“I concur with the same observation where we’re seeing an increase of competition on Android with more and more budgets being allocated,” said Jean-Sebastien Laverge, svp of growth at mobile game publisher Tilting Point. As ATT hits critical mass, media spending see-saw from iOS to Android continues |Seb Joseph |June 24, 2021 |Digiday 

The most likely explanation for the Court’s restraint comes from Justice Barrett’s concurring opinion in Fulton. An epic Supreme Court showdown over religion and LGBTQ rights ends in a whimper |Ian Millhiser |June 17, 2021 |Vox 

Justices Barrett and Kavanaugh concurred in the majority opinion. LGBTQ Rights Dodge a Supreme Court Bullet, But More Shots Are Coming |Jay Michaelson |June 17, 2021 |The Daily Beast 

Perficient concurred that searchers on desktop were more likely to click-through. As SEO changes, so do the tools that serve practitioners |Pamela Parker |June 15, 2021 |Search Engine Land 

Had he been competently represented, the jury might well have failed to concur on a death sentence. How the U.S. Justice System Screws Prisoners with Disabilities |Elizabeth Picciuto |December 16, 2014 |DAILY BEAST 

I disagree with Spencer on pretty much everything imaginable, but I concur on this. American Racist Richard Spencer Gets to Play the Martyr in Hungary |James Kirchick |October 7, 2014 |DAILY BEAST 

You write “There is nothing incongruous about educated, ambitious women wanting to be wives and mothers,” and I completely concur. Dear Princeton Mom, Stop Telling Me To Husband-Hunt |Emily Shire |February 14, 2014 |DAILY BEAST 

Reminded of that revving motor down in Dixie, I have to concur. A History of American Fun |Stefan Beck |February 9, 2014 |DAILY BEAST 

All military experts concur that Syria has significant air defense systems, which Libya did not. What To Do In Syria |Hussein Ibish |June 28, 2013 |DAILY BEAST 

All the best authorities concur in the uncertain properties of the salts of gold. Notes and Queries, Number 194, July 16, 1853 |Various 

His companion, who appeared to concur in this, glanced with evident regret at the six dollars which still lay beside him. The Gold Trail |Harold Bindloss 

In England, both government and people concur in this improvement. Travels through the South of France and the Interior of Provinces of Provence and Languedoc in the Years 1807 and 1808 |Lt-Col. Pinkney 

Though the English historians all concur in her praise, they seem to know very little of her. Travels through the South of France and the Interior of Provinces of Provence and Languedoc in the Years 1807 and 1808 |Lt-Col. Pinkney 

The question is, what is the probability that a statement, in which they both concur, will be true. A System of Logic: Ratiocinative and Inductive |John Stuart Mill