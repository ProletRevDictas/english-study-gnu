In the hospital it is the brain or heart surgeon, and in the sawmill it is the sawyer who with squinting eyes makes the first major cut that turns a log into boards. ‘Llama therapy’ in the wilds of Yellowstone National Park |Mary Winston Nicklin |October 8, 2021 |Washington Post 

Trees, after all, take years to grow, and in some parts of the US, there’s limited sawmill capacity to turn timber into lumber. You can buy stuff online, but getting it is another story |Terry Nguyen |September 2, 2021 |Vox 

When it reached a certain size, the tree was logged and transported to a sawmill to be made into lumber. No Trees Harmed: MIT Aims to One Day Grow Your Kitchen Table in a Lab |Jason Dorrier |January 24, 2021 |Singularity Hub 

Andrew Miller, Stimson’s CEO, said his company wanted to share details about its forestry and sawmill operations with the community. Timber Tax Cuts Cost Oregon Towns Billions. Then Polluted Water Drove Up the Price. |by Tony Schick, Oregon Public Broadcasting, and Rob Davis, The Oregonian/OregonLive |January 1, 2021 |ProPublica 

We come to a clearing, the site of a small abandoned sawmill. Pete Dexter’s Indelible Portrait of Author Norman Maclean |Pete Dexter |March 23, 2014 |DAILY BEAST 

Thus, one who is employed as a workman in a sawmill on such days as it was in operation for four months was not a casual employee. Putnam's Handy Law Book for the Layman |Albert Sidney Bolles 

They saw the log into the tie—they work from the piles of timber which they have about the sawmill, to store up the supply. The White Desert |Courtney Ryley Cooper 

They drove down to the sawmill, delivered their requisition, and had their wagon loaded with newly-sawn plank. Si Klegg, Book 2 (of 6) |John McElroy 

A sawmill was the largest figure of the town, and the railway station was the center. Wayside Courtships |Hamlin Garland 

He's a child's plaything—about as much use as the little wooly dog that lives down by the sawmill. The Tale of Pony Twinkleheels |Arthur Scott Bailey