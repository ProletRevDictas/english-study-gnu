Anyway, basically the gist of it was that they would not be picking that meal up and they wanted to donate it. How One Instagram Post Inspired Diners From Around the World to Donate Restaurant Meals |Amanda Kludt |January 27, 2021 |Eater 

“The gist is we have more resources and it’s easier to get these resources to the fire quickly because of technology,” he said. Lessons From San Diego’s Approach to Wildfires |Diana Leonard |December 21, 2020 |Voice of San Diego 

Even for those who “have got the gist of that,” like VW, “whether they’re able to act on it or not within the required time frame is more challenging.” Is the boom in electric vehicle stocks almost over? |Verne Kopytoff |December 19, 2020 |Fortune 

It takes a bit of deciphering to understand, but once you’ve got the hang of it, you can get the gist of a specific location’s weather in just a few minutes. How to Read a Surface Weather Map |Dennis Mersereau |October 15, 2020 |Outside Online 

Somehow, ART had learned “the gist” to the secret of biofuel manufacturing. How Machine Learning Made Hops-Free Hoppy Beer (and Other SynBio Wonders) Possible |Shelly Fan |October 6, 2020 |Singularity Hub 

Despite a spiraling rabbit hole of media quibbling about what Huckabee supposedly said, the basic gist of his remarks were clear. The GOP’s Latest Ploy: Flatter Married Women |Amanda Marcotte |January 31, 2014 |DAILY BEAST 

Mrs. Kennedy, interpreting the gist of the exchange, signaled to White that Camelot must be kept in the text. How Jackie Kennedy Invented the Camelot Legend After JFK’s Death |James Piereson |November 12, 2013 |DAILY BEAST 

One niece of a victim wrote her a letter, the gist of which was, “Lois Robison, shut up,” she said. Wisconsin Spa Shooting Brings Back Painful Memories for the Moms of Mass Killers |Winston Ross |October 25, 2012 |DAILY BEAST 

But, in short, the gist of this argument is: Afghanistan's a loser. Don’t Abandon Afghanistan! | |June 9, 2011 |DAILY BEAST 

I suggest the 90-minute experience tour and a night watching of Citizen Kane to get the full gist of William Randolph Hearst. Gal With a Suitcase |Jolie Hunt |February 20, 2010 |DAILY BEAST 

Dont forget to tell the examiner that Toly and Modi are prepared for the upper division, so runs the gist of his letters. The Life &amp; Letters of Peter Ilich Tchaikovsky |Modeste Tchaikovsky 

As the gist of the tort consists of the injury done to one's reputation, the defamatory statement must have been published. Putnam's Handy Law Book for the Layman |Albert Sidney Bolles 

The gist of the Minority Report so far, at any rate, as the non-able-bodied are concerned may be put even more shortly. English Poor Law Policy |Sidney Webb 

The boss pushed that part of it aside abruptly, as he always does when he has got hold of the gist of a thing. The Wreckers |Francis Lynde 

I can tell you the gist of them both in a few well-chosen phrases! Jane Journeys On |Ruth Comfort Mitchell