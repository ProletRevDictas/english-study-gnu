The ability to measure productivity and how it’s changing in a warming world could not only inform scientists about ocean health, but could also impact the operation of fisheries and people whose livelihoods depend on the sea. These free-floating robots can monitor the health of our oceans |Charlotte Hu |August 18, 2021 |Popular-Science 

This process is called upwelling and results in the richest fisheries in the world. Twilight of the Nautilus - Issue 104: Harmony |Peter Ward |August 11, 2021 |Nautilus 

The waters off Bridlington, UK, and nearby along the Holderness coast make up the largest lobster fishery in Europe. What happened after a wind farm was built near the lobster capital of Europe |Clarisa Diaz |July 1, 2021 |Quartz 

So that was the moment from a community-based fishery or nationally-based fishery, to a global-industrial fishery. Is the Future of Farming in the Ocean? (Ep. 467) |Stephen J. Dubner |June 24, 2021 |Freakonomics 

As far as harm on actual fisheries, the Madison is an interesting river. COVID drove more people to go fishing, but at what cost? |Purbita Saha |June 23, 2021 |Popular-Science 

The credits relieved the sailors and owners of tariffs, essentially tax payments they had to make on supplies for the fishery. A Founding Father Profit Sharing Fix for Inequality |Joseph Blasi |July 12, 2014 |DAILY BEAST 

Close behind is California Set Gillnet Fishery, where 65 percent of animals caught are thrown away. New Report Reveals U.S. Fisheries Killing Thousands of Protected and Endangered Species |Abby Haglage |March 23, 2014 |DAILY BEAST 

This rule put management of the pollock fishery in the context of the ecosystem. The Big Idea: Saving the World’s Most Important Fish |Kevin M. Bailey |August 9, 2013 |DAILY BEAST 

Even modest fishery harvests influence the health of ocean ecosystems. The Big Idea: Saving the World’s Most Important Fish |Kevin M. Bailey |August 9, 2013 |DAILY BEAST 

What does the story of Alaska pollock tell us about maintaining a more sustainable fishery? The Big Idea: Saving the World’s Most Important Fish |Kevin M. Bailey |August 9, 2013 |DAILY BEAST 

A ship will sail for the South Sea fishery in about five weeks, and will engage to take the whole of the engines. Life of Richard Trevithick, Volume II (of 2) |Francis Trevithick 

There are an extensive mackerel and herring fishery, and motor engineering works. Encyclopaedia Britannica, 11th Edition, Volume 4, Part 3 |Various 

The Colchester oyster beds are mainly in this part of the Colne, and the oyster fishery is the chief industry. Encyclopaedia Britannica, 11th Edition, Volume 4, Part 3 |Various 

The fur-seal fishery is an important industry, though apparently a declining one. Encyclopaedia Britannica, 11th Edition, Volume 4, Part 3 |Various 

Pass by the other parts, and look at the manner in which the people of New England have of late carried on the whale fishery. English: Composition and Literature |W. F. (William Franklin) Webster