Still, despite all she does and all we’ve heard, despite how wholly she inhabits the music — be it a Bach partita or a searing contemporary concerto like Peter Eötvös’s “DoReMi” — there remains what can only be described as a Midori mystique. Midori’s career started with a fleeting moment. It’s evolved into a lasting legacy. |Michael Andor Brodeur |May 13, 2021 |Washington Post 

Instead, he lives a cozy life, listening to piano concertos and drinking alone. Politics and the pandemic have changed how we imagine cities |Joanne McNeil |April 28, 2021 |MIT Technology Review 

He toggled between electric and acoustic bands and delved into the classical world, composing his own concerto for classical orchestra. Remembering Chick Corea, An Endlessly Inquisitive Jazz Pioneer |Andrew R. Chow |February 12, 2021 |Time 

He even performed piano concertos by Mozart and other classical composers. Chick Corea, versatile pianist who made jazz eclectic and electric, dies at 79 |Matt Schudel |February 11, 2021 |Washington Post 

Recognizing a scent is a precise and intricate process in which chemistry, biology, and physics must play together in a synchronized concerto—whether you’re relishing the aroma of a rose or pinching your nose at a pile of dog poop. The Doctor Will Sniff You Now - Issue 95: Escape |Lina Zeldovich |February 3, 2021 |Nautilus 

Back to the concerto, or a little light Plato, or some such. ‘The Real Housewives of New York City’ Loses a Leg in Sixth-Season Finale |Tim Teeman |July 23, 2014 |DAILY BEAST 

His first great piano concerto is widely considered to be the No. 9, Jeunehomme, written at age 21. The Myth of Innate Genius |David Shenk |May 13, 2011 |DAILY BEAST 

His speeches have the elegance and control of a Haydn concerto. Big Dog and the Whippet |Tina Brown |November 3, 2008 |DAILY BEAST 

Going back, Liszt indulged in a little graceful badinage apropos of the concerto. Music-Study in Germany |Amy Fay 

Frulein Fichtner was the young lady who was going to play his concerto in A major at the concert that evening. Music-Study in Germany |Amy Fay 

The concerto made a generally dazzling and difficult impression upon me, but did not "take hold" of me particularly. Music-Study in Germany |Amy Fay 

Frulein Fichtner had already departed, but the first violinist played Mendelssohn's famous concerto for violin. Music-Study in Germany |Amy Fay 

Then, child, you've fallen on your head, if you don't know that at least you must have a second copy of the concerto! Music-Study in Germany |Amy Fay