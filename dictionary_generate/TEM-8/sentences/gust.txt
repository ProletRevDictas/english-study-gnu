Winds are out of the north around 10 mph, with gusts near 20 mph. PM Update: Light snow mainly passes south of us tonight; cloudy and cold Friday |Ian Livingston |February 12, 2021 |Washington Post 

Winds are from the northwest at 10 to 15 mph, with some gusts to 25 mph. D.C.-area forecast: Cold today before another possible winter storm starting Wednesday night |Jason Samenow |February 8, 2021 |Washington Post 

Breezes are moderate from the north with an occasional gust to 20 mph. D.C.-area forecast: Sunny today and showers early Friday before a chilly weekend |David Streit |February 4, 2021 |Washington Post 

Winds will decrease to about 5 mph overnight, with gusts to around 10 mph. PM Update: Cold tonight under clearer skies, then it turns milder Thursday |Ian Livingston |February 3, 2021 |Washington Post 

Blustery winds from the northwest at 15 to 25 mph with gusts to 35 mph will make the highs in the mid- to upper 30s feel like the 20s. Updates: Snow showers focus inside the Beltway |Matt Rogers, Jason Samenow |February 2, 2021 |Washington Post 

A gust of smoke dances around her naked frame as she bathes for one final time in the prayer leaves. Hallucinating Away a Heroin Addiction |Abby Haglage |May 4, 2014 |DAILY BEAST 

The accessory was easily lifted by a gust of wind and would regularly get entangled in the wheel spokes of carriages. Corsets, Muslin Disease, and More of the Deadly Fashion Trends |The Fashion Beast Team |April 1, 2014 |DAILY BEAST 

Despite the gust of excitement most scientists are keeping their emotions in check. Why Can't We Find an AIDS Vaccine? |Kent Sepkowitz |September 15, 2013 |DAILY BEAST 

And one strong gust of wind could blow the whole edifice of human habitation away. Following the Great Iditarod |Justin Green |April 26, 2013 |DAILY BEAST 

An added gust turned the rainbow to multi-colored wisps of rare beauty. A Tale of Two Trees |Michael Daly |November 5, 2011 |DAILY BEAST 

The next morning a gust of wind carried him, and him only, out of the boat into the waves, and he was never seen again. The Jesuit Relations and Allied Documents, Vol. II: Acadia, 1612-1614 |Various 

It was a windy night and a sudden gust blew his tall hat into the river, and after it unfortunately dropped the meerschaum. Fifty Years of Railway Life in England, Scotland and Ireland |Joseph Tatlow 

It blew out of the canyon-mouth like a gust from a chimney, rolling over and over in billowy masses. Raw Gold |Bertrand W. Sinclair 

The violent gust passed on its way, the flying cloud of sand subsided, settling down on everything. The Wave |Algernon Blackwood 

Somebody was lighting a candle, which was at once extinguished when the door was open, and a gust of wind and rain swept in. The Cromptons |Mary J. Holmes