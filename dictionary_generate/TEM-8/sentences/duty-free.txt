In his view, a writer has only one duty: to be present in his books. Houellebecq’s Incendiary Novel Imagines France With a Muslim President |Pierre Assouline |January 9, 2015 |DAILY BEAST 

In other words, the free thinker defending freedom of thought. Houellebecq’s Incendiary Novel Imagines France With a Muslim President |Pierre Assouline |January 9, 2015 |DAILY BEAST 

Tend to your own garden, to quote the great sage of free speech, Voltaire, and invite people to follow your example. How the PC Police Threaten Free Speech |Nick Gillespie |January 9, 2015 |DAILY BEAST 

The simple, awful truth is that free speech has never been particularly popular in America. How the PC Police Threaten Free Speech |Nick Gillespie |January 9, 2015 |DAILY BEAST 

Cambodia, with its seemingly free press, is also a haven for foreign journalists. Cambodia’s Smoke-and-Mirrors Democracy |David Shaftel |January 9, 2015 |DAILY BEAST 

What need to look to right or left when you are swallowing up free mile after mile of dizzying road? The Joyous Adventures of Aristide Pujol |William J. Locke 

It seemed to free her of a responsibility which she had blindly assumed and for which Fate had not fitted her. The Awakening and Selected Short Stories |Kate Chopin 

If we can free this State of Yankees, we will accomplish more than your armies down south have. The Courier of the Ozarks |Byron A. Dunn 

And as she hesitated between obedience to one and duty toward the other, her life, her love and future was in the balance. The Homesteader |Oscar Micheaux 

The voice of the orator peculiarly should be free from studied effects, and responsive to motive. Expressive Voice Culture |Jessie Eldridge Southwick