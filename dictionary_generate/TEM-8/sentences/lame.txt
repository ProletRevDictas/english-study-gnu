Just lame, fake-seeming "Thank you soooo much" nonsense, and an Amazon gift card. Carolyn Hax: Walk away from a fat-shamer. It’s the best kind of exercise. |Carolyn Hax |February 18, 2021 |Washington Post 

In recent weeks, the lame-duck president has issued a rapid flurry of pardons to allies, friends and fellow Republicans, including full pardons to some who pleaded guilty to lying to federal law enforcement during the Russia investigation. Rioters wanted to ‘capture and assassinate’ lawmakers, prosecutors say. A note left by the ‘QAnon Shaman’ is evidence. |Teo Armus |January 15, 2021 |Washington Post 

The analysis does not consider reelections or transitions between leaders within the same party, because the risk of anti-democratic actions or abuses of lame-duck power in those scenarios is relatively low. Mob violence at the Capitol underscores risks of lengthy presidential transitions |Christopher Ingraham |January 7, 2021 |Washington Post 

While not a huge shift, this is the opposite of what typically happens in the lame-duck period. Why A Split Verdict In Georgia Isn’t That Crazy |Nate Silver (nrsilver@fivethirtyeight.com) |December 30, 2020 |FiveThirtyEight 

Facebook apps make lame promises like this in exchange for access to the scads of personal information that Facebook already collects about you. Start 2021 by fixing your online privacy |Stan Horaczek |December 29, 2020 |Popular-Science 

One of the most persistent myths in American politics is the media-fueled concept of the lame duck. The Liberation of the Lame Duck: Obama Goes Full Bulworth |John Avlon |December 19, 2014 |DAILY BEAST 

Almost every two-term president gets a pasting in the midterms, but Obama now faces lame-duck issues on an historic scale. Obama: Lamest Duck Ever? |Jonathan Alter |November 6, 2014 |DAILY BEAST 

In the short term, any hopes for an active lame-duck session will rest on the results of the election, King said. Steve King: GOP Congress Should Keep Repealing Obamacare |Ben Jacobs |October 29, 2014 |DAILY BEAST 

What he had created was a quick Web tool, but QuickWeb sounded lame, as if conjured up by a com­mittee at Microsoft. You Can Look It Up: The Wikipedia Story |Walter Isaacson |October 19, 2014 |DAILY BEAST 

Also, he gave a lame excuse: ‘I couldn't find a pic that expresses both sides.’ Knicks’ Amar’e Stoudemire Posts Pro-Palestine Photo, Allegedly Cyberbullies Israeli-Born MTV VJ |Robert Silverman |July 14, 2014 |DAILY BEAST 

My head feels quite lame from so much practicing, the consequence, I suppose, of so much listening. Music-Study in Germany |Amy Fay 

Next morning the hero of Wagram, lame from the effect of a kick from his horse, was summoned before the Emperor. Napoleon's Marshals |R. P. Dunn-Pattison 

His donkey had gone lame, he abandoned it to the boys behind, he climbed in to drive with Lettice. The Wave |Algernon Blackwood 

As she turned the corner, a lame child in a calico dress and torn hood staggered past her bent with the weight of a heavy basket. Tessa Wadsworth's Discipline |Jennie M. Drinkwater 

Some one must have noticed that they were more apt to be lame after sitting on the cold ground while they were warm. The Later Cave-Men |Katharine Elizabeth Dopp