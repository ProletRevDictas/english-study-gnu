Under the settlement, families are entitled to up to 25 sessions, though providers can lay out different plans for families depending on their need. Border Report: The Lingering Trauma of Family Separation |Maya Srikrishnan |August 31, 2020 |Voice of San Diego 

San Diegans who meet certain requirements and can show with a doctor’s written prescription that they suffer from a disability are entitled to reduced bus and trolley rides. Morning Report: MTS Rejects Many Who Applied for Disabled Fare Reductions |Voice of San Diego |August 31, 2020 |Voice of San Diego 

Current and prospective Trademark owners are entitled to take legal action against so-called “domain squatters” in federal court, or by filing for arbitration with an international body that oversees web names. Biden campaign trolls Trump by snatching ‘Keep America Great’ website |Jeff |August 28, 2020 |Fortune 

The state last year passed a series of laws entitling every single-family lot in the state to build two secondary homes on the property, essentially allowing three homes on each single-family lot. Single-Family Zoning’s Century of Supremacy in San Diego |Andrew Keatts |August 26, 2020 |Voice of San Diego 

Of these 842 loans, 792 were for less than $150,000, which should have entitled the recipient to more confidentiality under SBA’s release policies. Out of hundreds of names exposed in the PPP data, 98% used Bank of America |David Yanofsky |July 13, 2020 |Quartz 

He even went so far as to entitle one blog post (since sadly deleted) “How To Succeed at Failure.” Can Richard Branson Bounce Back From His Space Disaster? |Tom Sykes |November 3, 2014 |DAILY BEAST 

In the Vanity Fair piece, Lawrence defended her celebrity status and said that that did not entitle people to her body. ‘The Fappening’ Perpetuators Have a J.Law Come-to-Jesus Moment and ‘Cower With Shame’ |Marlow Stern |October 8, 2014 |DAILY BEAST 

Teenagers can pay taxes; that alone should entitle them to a voice in the political process. Paying Taxes and Going to Jail Like Adults; Teens Deserve the Right to Vote, Too |Jillian Keenan |October 6, 2014 |DAILY BEAST 

Different contracts may entitle Chesapeake to charge varying amounts. How the Kings of Fracking Double-Crossed Their Way to Riches |ProPublica |March 13, 2014 |DAILY BEAST 

Hint: It does not entitle him to the right to do damage to the Republican brand. Let Ron Paul Speak at the Tampa Convention? |David Frum |January 13, 2012 |DAILY BEAST 

These are the violins which by common consent most entitle this artiste to rank with the great masters. Violins and Violin Makers |Joseph Pearce 

As a rule, the consideration of a contract must totally fail to entitle a person to recover back the money he has paid. Putnam's Handy Law Book for the Layman |Albert Sidney Bolles 

A disciple asked him what qualities a man must possess to entitle him to be called a scholar. Beacon Lights of History, Volume I |John Lord 

Our tickets, which we bought of the concierge at the Hotel B., entitle us to a drive or a railway journey. Italian Days and Ways |Anne Hollingsworth Wharton 

The wise and high-minded counsels he bestowed on me entitle him to an honoured place in my memory and my grateful affection. Autobiographical Reminiscences with Family Letters and Notes on Music |Charles Gounod