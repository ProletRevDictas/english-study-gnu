Some last-minute software updates alleviated a number of issues and slowdowns, but it remained a pretty buggy experience overall. Microsoft's Double-Screened Surface Duo Smartphone Is Exciting, But I'm Waiting for Next Year's Model |Patrick Lucas Austin |September 10, 2020 |Time 

Its buggy software, dearth of double-screen app support and underwhelming camera will only frustrate people trying to do anything besides write emails and take notes. Microsoft's Double-Screened Surface Duo Smartphone Is Exciting, But I'm Waiting for Next Year's Model |Patrick Lucas Austin |September 10, 2020 |Time 

People living in southern Africa around 200,000 years ago not only slept on grass bedding but occasionally burned it, apparently to keep from going buggy. The oldest known grass beds from 200,000 years ago included insect repellents |Bruce Bower |August 13, 2020 |Science News 

Time for Googlebot to re-crawl the page and then a combination of Google’s cache and a buggy new Search Console to be able to interpret those changes. JavaScript rendering and the problems for SEO in 2020 |Anthony Lavall |May 6, 2020 |Search Engine Watch 

The Galaxy S20 Ultra, aside from its buggy camera, questionable zoom functionality, and eye-watering price point, is a decent smartphone. Review: The Samsung Galaxy S20 Ultra Can't Deliver On All the Hype |Patrick Lucas Austin |March 4, 2020 |Time 

The cars had plush green upholstery and stained-glass windows and were faster and cheaper than a horse-and-buggy. Those Kansas City Blues: A Family History |Katie Baker |October 24, 2014 |DAILY BEAST 

Ford began tinkering in his garage in Detroit in the 1890s, trains and the horse and buggy was the dominant mode of transport. From the Model T to the Model S |The Daily Beast |September 24, 2014 |DAILY BEAST 

But the programs were buggy and often prone to false positives, alerting a network administrator too often to routine behavior. Catching the Next WikiLeaker |Eli Lake |October 20, 2011 |DAILY BEAST 

Some people believe it is only a matter of time until all bookstores go the way of the horse and buggy. Ode to the Bookstore |John Avlon |October 13, 2011 |DAILY BEAST 

As illustrated in this publication, we have already landed on it and driven across it in a buggy. Man on the Moon |The Daily Beast |July 19, 2009 |DAILY BEAST 

Accordingly, she had the boys to hitch a team to a buggy and took him driving over the great estate. The Homesteader |Oscar Micheaux 

He had transferred himself to the buggy with a grumble of disgust, and begged her to come for him early in the morning. Ancestors |Gertrude Atherton 

He drives a white mule, and has managed to put a top of sail cloth on an old ramshackle buggy, which he calls a 'shay.' The Cromptons |Mary J. Holmes 

Gwynne rang for his guest's buggy, thanked him for his advice; then ordered his horse and rode about the ranch half the night. Ancestors |Gertrude Atherton 

And she carefully gathered up her papers and went to the rescue of the weary Miss Boutts, while Gwynne ordered the buggy. Ancestors |Gertrude Atherton