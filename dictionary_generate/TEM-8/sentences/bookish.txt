When I couldn’t think of anything to say, I wrote notes furiously and perfected what I hoped was a bookish and curious presence in the hopes that people would assume my head was full of insightful observations rather than abject fear. In a virtual world, power dynamics in meetings are changing for the better |Laura Simpson |October 11, 2021 |Quartz 

Keith said she was “really cute” and looked “bookish” in her glasses. Date Lab: ‘The most interesting part of the date was when we talked about rating it’ |Vijai Nathan |April 1, 2021 |Washington Post 

A popular athlete and a bookish social pariah start a secret relationship while in high school, then float in and out of each other’s lives as they journey into adulthood. Washington Post paperback bestsellers | |January 26, 2021 |Washington Post 

Born in 1903 in the tiny dusty colonial outpost of Pietersburg, in northern South Africa, Plomer was bookish and reclusive. The Mild-Mannered Poet Who Championed James Bond |Fiona Zublin |November 30, 2020 |Ozy 

Here at last was a home for the nerdy, the bookish, the hypercompetent others. In the dumbest and darkest of times, Alex Trebek maintained a safe space for intelligence |Hank Stuever |November 8, 2020 |Washington Post 

Aloof and bookish, Pius XI (Achille Ratti) spent years as a Vatican librarian before becoming a diplomat and cardinal. How the Catholic Church Got in Bed with Mussolini |Jason Berry |February 5, 2014 |DAILY BEAST 

Margot, three years older than Anne, was quiet and bookish but still a part of things. Anne Frank’s Amsterdam |Russell Shorto |October 12, 2013 |DAILY BEAST 

Poet Jackie Kay said of the nominees, “It is a sad day when even the Booker is afraid to be bookish.” The 12 Biggest Booker Prize Controversies |Thomas Flynn |July 23, 2013 |DAILY BEAST 

Especially when you look at the other side of the bracket and see bookish James Thurber. Celebrity March Madness 2012: How to Pick NCAA Tournament Winners |Michael Solomon |March 20, 2012 |DAILY BEAST 

Great new novels on hippie California, a bookish adventure, and the gritty Midwest. 3 Must-Read Novels |Taylor Antrim, Anne Trubek, Nicholas Mancusi |July 21, 2011 |DAILY BEAST 

Technical words and bookish terms are not words of national use. English: Composition and Literature |W. F. (William Franklin) Webster 

It all came back in cash to the working man; and yet it was my own pals who had rebuked me for being too bookish. The Iron Puddler |James J. Davis 

I envy you the acquaintance of a genuine non-bookish man like Captain Speke. George Eliot's Life, Vol. II (of 3) |George Eliot 

It was only geography that morning, any way: and the practical thing was worth any quantity of bookish theoric. The Golden Age |Kenneth Grahame 

He seemingly was a bookish young man who would probably enjoy hunting a Greek verb to its lair. Ruth Fielding In the Red Cross |Alice B. Emerson