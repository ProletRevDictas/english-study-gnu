You might know him as the Messiah, the son of God, a prophet of God, or an enlightened and selfless radical who defied the oppressive powers of his time to champion the rights and dignity of the poor and marginalized. We Mock ‘Jesus Guns Babies’ Platform at Our Own Peril |Wajahat Ali |February 28, 2022 |The Daily Beast 

Jesus begs God to let him fulfill his role as the Messiah, and suddenly finds himself back on the cross, where he dies for the sins of mankind. The Green Knight is glorious and a little baffling. Let’s untangle it. |Alissa Wilkinson |July 30, 2021 |Vox 

Singing along with Handel’s “Messiah” at the Kennedy Center after waiting in the long line for free tickets. Today in D.C.: Headlines to start your Thursday in D.C., Maryland and Virginia |Dana Hedgpeth, Teddy Amenabar |December 10, 2020 |Washington Post 

He was just another wannabe messiah who ended up on the wrong side of the authorities. So-Called ‘Biblical Scholar’ Says Jesus A Made-Up Myth |Candida Moss, Joel Baden |October 5, 2014 |DAILY BEAST 

You either have to be a masochist or have an acute Messiah complex. Obama Really Seems to Be Looking Forward to the End of His Presidency |Dean Obeidallah |December 2, 2013 |DAILY BEAST 

And that is the key to grasping how we Americans can and cannot take him seriously as a would-be political messiah. Russell Brand: Not Quite a Messiah |James Poulos |October 28, 2013 |DAILY BEAST 

Fayyad was by no means despised by the masses, but nor was he seen as the messiah-like figure held up by the West. The End Of 'Fayyadism' In Palestine |Khaled Elgindy |April 22, 2013 |DAILY BEAST 

He was a nontragic, happy-ending, American Christ: half-messiah, half Santa Claus. Blood for Sale? Reagan’s Pagan Cult |Lee Siegel |May 26, 2012 |DAILY BEAST 

After their restoration, they will acknowledge Messiah at God's right hand as in all things their sovereign Lord. The Ordinance of Covenanting |John Cunningham 

On the eighth day after his birth, this immaculate Child was circumcised, both because he was a Jew, and the predicted Messiah. Female Scripture Biographies, Vol. II |Francis Augustus Cox 

The chief of them went out to get a glimpse of the famous preacher, whom so many hailed as the long-expected Messiah. The Hearth-Stone |Samuel Osgood 

Perhaps it may be said, that few nations had even heard of the promised Messiah, and still fewer desired his coming. Jesus, The Messiah; or, the Old Testament Prophecies Fulfilled in the New Testament Scriptures |(A Lady) Anonymous 

These predictions of the Prince Messiah are peculiarly striking. Jesus, The Messiah; or, the Old Testament Prophecies Fulfilled in the New Testament Scriptures |(A Lady) Anonymous