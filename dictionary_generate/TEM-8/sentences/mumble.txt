Casablancas speaks in a drowsy mumble and occasionally needs prodding, but once you do, becomes surprisingly engaged. Julian Casablancas Enters the Void: On the Strokes’ Friction, Why He Left NYC, and Starting Over |Marlow Stern |October 9, 2014 |DAILY BEAST 

Everywhere we go, inspiration hits us and we just kind of mumble things into our iPhones. Haim: We’re a Band. Not a ‘Girl Band.’ |Melissa Leon |May 21, 2014 |DAILY BEAST 

He defines Dynamic Inaction with one pithy aphorism: “When in doubt, mumble; when in trouble, delegate; when in charge, ponder.” When In Doubt, Mumble—Dynamic Inaction May Be Our Best Hope |Joe McLean |April 6, 2013 |DAILY BEAST 

Fatah's leaders mumble in English that they recognize Israel while delivering incendiary rhetoric in Arabic. Why Chuck Hagel is Wrong on Israel |Justin Green |February 1, 2013 |DAILY BEAST 

"The mumble could be passive aggressive—the person wants to have someone work very hard at hearing them," Batson says. Mumbling Wins Oscars! |Zachary Pincus-Roth |March 3, 2010 |DAILY BEAST 

Pausing at the threshold before opening the door, the sonorous mumble sounding through the deal panels misled me. The Soldier of the Valley |Nelson Lloyd 

The blows of the ax, off in the chaparral, were louder in their ears now, and they could hear a mumble of voices. Motor Matt's "Century" Run |Stanley R. Matthews 

Bristow made no comment on this, and Mattie, turning slowly away from him, began to mumble something. The Winning Clue |James Hay, Jr. 

Inside45 the parlor could be heard the mumble of men's voices. Wayside Courtships |Hamlin Garland 

She waited until he answered, in an indistinct mumble, that he did not know. Colonial Born |G. Firth Scott