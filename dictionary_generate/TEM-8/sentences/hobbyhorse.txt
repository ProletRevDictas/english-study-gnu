Spanning 40 feet, it’s a riff on a hobbyhorse, but with a horned goat’s head. The Hudson Valley’s outdoor art parks make for an alluring pandemic destination |Nevin Martell |April 23, 2021 |Washington Post 

You can get “money, Nobels… celebrity for any cause or hobbyhorse….” No One’s Going to Challenge Hillary Clinton |Robert Shrum |May 10, 2014 |DAILY BEAST 

Obama came out for medical malpractice reform, a GOP hobbyhorse roughly forever (although the devil is in the details). A Grownup Speech to Please Folks at Home, Not Pundits |Howard Kurtz |January 25, 2011 |DAILY BEAST 

That you must carry everyone with you, swelling the ranks, is a hard-ridden Wasp hobbyhorse. The Last of the Wasps |Tad Friend |September 27, 2009 |DAILY BEAST 

He laid hold of it and109 drew it out and set down on the faint rug of light a small wooden hobbyhorse. Christmas |Zona Gale 

It was Malcolm's hobbyhorse, dappled gray, the tail and the mane missing and the paint worn off—and tenderly licked off—his nose. Christmas |Zona Gale 

While he went within doors he had left the hobbyhorse in the snow, close to the wall; and he came back there to wait. Christmas |Zona Gale 

There he saw a spring hobbyhorse, as large as a Shetland pony, all saddled and bridled, too—lacking nothing but a rider. The Book of Stories for the Storyteller |Fanny E. Coe 

They were smaller than the tiniest hobbyhorse that has ever been seen, as small almost as the toy horses in a "Noah's ark." Appletons' Popular Science Monthly, March 1899 |Various