Imagine his surprise when the person on the other end of his Zoom date was a bona fide introvert, 23-year-old technical government contractor Amanda Davis. Date Lab: Opposites didn’t attract — but they didn’t repel, either |Rich Juzwiak |February 4, 2021 |Washington Post 

This was a bona fide country music legend whose fans hail largely from the reddest of Red States on the map. The Healing Power of Garth Brooks |Philip Elliott |January 21, 2021 |Time 

Grand Lake, the town on the western side of the park, has a bona fide nordic center with 22 miles of groomed trails. Your Guide to Visiting National Parks in Winter |The Editors |January 4, 2021 |Outside Online 

It augurs the arrival, in the midst of a fraught time for theater and other performing arts, of a bona fide new musical. Judy and Mickey would have loved the TikTok ‘Ratatouille’ — 2021’s answer to ‘Let’s put on a show!’ |Peter Marks |January 2, 2021 |Washington Post 

The 6-foot-6 shooting guard wasn’t even a lottery pick, much less a bona fide star, out of Michigan in 2016. 25 Rising Stars to Track in 2021 |Daniel Malloy |December 20, 2020 |Ozy 

Another sent back a flat-screen television with a bona fide tombstone within. The Insane $11 Billion Scam at Retailers’ Return Desks |M.L. Nestel |December 19, 2014 |DAILY BEAST 

The Facebook co-founder and his politically ambitious husband embodied all the attributes of a bona fide “gay power couple.” The Rise and Fall of Chris Hughes and Sean Eldridge, America’s Worst Gay Power Couple |James Kirchick |December 9, 2014 |DAILY BEAST 

A conservative coloring book publisher is out with a new title imagining the tea party heartthrob as a bona fide superhero. Ted Cruz saves America in This Right-Wing Coloring Book |Asawin Suebsaeng |November 13, 2014 |DAILY BEAST 

Stephen Hawking is not only a bona fide genius, but also one of the most resilient men on the planet. The Other Side of Stephen Hawking: Strippers, Aliens, and Disturbing Abuse Claims |Marlow Stern |November 6, 2014 |DAILY BEAST 

By the time the CFDA awards rolled round in early 1994, Moss was a bona fide star. Sex, Drugs, and Kate Moss: Secrets of a Wild Supermodel |Tom Sykes |October 9, 2014 |DAILY BEAST 

Nor can other creditors through filing objections to a claim prevent a bona fide claimant from voting. Putnam's Handy Law Book for the Layman |Albert Sidney Bolles 

The fact that you are here tells me that the wireless you got on the ship was not only bona fide but important. The Everlasting Arms |Joseph Hocking 

Such relief was to be granted with due consideration and the bona fide intention of recovering. English Poor Law Policy |Sidney Webb 

It was an instantaneous apparition of absolute bodily substance, which carried its own warrant of complete bona fides. Devil-Worship in France |Arthur Edward Waite 

If we leave Philippeville in the evening, we find ourselves next morning in the handsome roadstead of Bona. Lippincott's Magazine of Popular Literature and Science, Volume 11, No. 24, March, 1873 |Various