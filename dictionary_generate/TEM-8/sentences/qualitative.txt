The point is to understand what is the trend, what is the qualitative take-home message you get from this. The Hard Lessons of Modeling the Coronavirus Pandemic |Jordana Cepelewicz |January 28, 2021 |Quanta Magazine 

By injecting more qualitative and quantitative data into your evaluation, you increase the chances that you uncover not only new but inclusive insights that consider the perspectives of a more diverse group, not just the “general market consumer.” Take your campaigns to deeper levels by disrupting the peace |Max Braun |December 24, 2020 |Search Engine Watch 

Our qualitative associations with food and drink have also changed. What Did the Past Smell Like? - Issue 93: Forerunners |Ann-Sophie Barwich |December 9, 2020 |Nautilus 

The industry is moving towards attention as an additional qualitative currency for ad buying and selling. Why ad buyers (and sellers) need to pay more attention to viewer attention |TVision |December 2, 2020 |Digiday 

If nothing else, qualitative keyword research will allow you to understand the breadth of your audience’s interests and concerns better. The untapped value of qualitative keyword data |Jack Telford |November 2, 2020 |Search Engine Watch 

For us, there was a big kind of qualitative leap on this record. Chromeo’s Dave 1 on ‘White Women’ and Bringing Back the Funk |Melissa Leon |May 12, 2014 |DAILY BEAST 

Then there is more qualitative research, such as contemporary fiction. American Household Gadget Exceptionalism |Megan McArdle |February 7, 2013 |DAILY BEAST 

The approach she takes in her book is what is known in psychological research as a “qualitative” study. Generation Rx? Review of ‘Dosed: The Medication Generation Grows Up’ |Casey Schwartz |April 15, 2012 |DAILY BEAST 

The Pentagon and CIA work constantly with their Israeli counterparts to ensure Israel's qualitative edge. The Israeli Anti-Attack-Iran Brigade |Bruce Riedel |November 7, 2011 |DAILY BEAST 

Preserving that qualitative edge enjoys broad bipartisan support in the United States. Iran Blusters but Israel Has the Edge |Bruce Riedel |September 28, 2011 |DAILY BEAST 

The action is an exceedingly sensitive qualitative test for gold. The Elements of Qualitative Chemical Analysis, vol. 1, parts 1 and 2. |Julius Stieglitz 

Qualitative tests demonstrated the presence of a chlorid, a nitrate, a mercuric salt, free acid and glycerin. The Propaganda for Reform in Proprietary Medicines, Vol. 1 of 2 |Various 

Qualitative tests demonstrated the presence of sodium, a carbonate, caffein and acetanilid, the latter in considerable quantities. The Propaganda for Reform in Proprietary Medicines, Vol. 1 of 2 |Various 

Qualitative tests demonstrated the presence of antipyrin, free boric acid and sodium borate. The Propaganda for Reform in Proprietary Medicines, Vol. 1 of 2 |Various 

This objection does not hold good if either merely qualitative evidence, or a fairly approximate quantation, is required. Poisons: Their Effects and Detection |Alexander Wynter Blyth