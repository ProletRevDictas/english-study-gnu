God has to continually prompt and prod him, and puts his brother Aaron at his side to do most of the real leadership. Meet Moses the Swashbuckling Israelite |James Romm |December 14, 2014 |DAILY BEAST 

These tests prod and poke the children, creating lots of anxiety and taking away from the joy of learning. Hunger Games Comes to New York State’s Public Schools |Zephyr Teachout |November 26, 2014 |DAILY BEAST 

But to prod the War on Drugs ever further into history is to make the death of children like Michael Brown ever less likely. The True Stereotypes Behind Michael Brown's Death |John McWhorter |August 13, 2014 |DAILY BEAST 

Those were executive action, designed to call attention to an issue, prod Congress, or achieve results. How Obama Can Use Executive Actions to Improve Our Democracy |Michael Waldman |April 18, 2014 |DAILY BEAST 

And, of course, a rebuke to our current politics and especially our president, who seems unable to prod a bee to buzz. ‘Breaking Bad’ in the White House: Bryan Cranston as LBJ in 'All the Way' |David Freedlander |March 7, 2014 |DAILY BEAST 

No sooner had his feet hit the floor, however, than he felt the cold, stern prod of the barrel of an automatic. Hooded Detective, Volume III No. 2, January, 1942 |Various 

She was badly needed to prod the Mexican women in their labors of making beds and sweeping rooms that were occupied twice daily. David Lannarck, Midget |George S. Harney 

For the time being, I'd like to loaf on you for a week or so and watch the wheels go around without my having to prod them. The Wreckers |Francis Lynde 

It is wonderful how a mite of laudation will prod us to be more worthy. The Eugenic Marriage, Vol. 3 (of 4) |W. Grant Hague 

We felt the prod of gregarious instinct, the drawing together as though for united action, the impulse toward cooperation. Before Adam |Jack London