Once the shoreline of a great inland sea, the sandstone here is roughly 320 million years old and reaches four miles below the surface. A day’s drive from Chicago, exploring a very different Illinois |Carson Vaughan |February 12, 2021 |Washington Post 

That probably means we’ll have to rethink the system, rip infrastructure out of the ground and move it inland where it’s safe from waves. Environment Report: The High Cost of Getting Rid of Water |MacKenzie Elmer |February 8, 2021 |Voice of San Diego 

The situation is worse in southwestern and inland northwestern states. Why South Africa stopped using the AstraZeneca COVID-19 vaccine |Rahul Rao |February 8, 2021 |Popular-Science 

The Abenaki people, who have migrated between this coast and inland Maine for thousands of years, share their continuing story at the Abbe Museum, which has a location both inside and outside the park. The Ultimate Acadia National Park Travel Guide |Virginia M. Wright |February 8, 2021 |Outside Online 

Conservationists are also worried about Pakistan’s inland waterways project, under which the Indus is to be developed for goods transportation by giant vessels. The Unlikely Dolphin Rescue Hot Spot: Pakistan |Charu Kasturi |February 2, 2021 |Ozy 

To “link up the beachheads and peg out claims well inland” was necessarily the first aim of Overlord. Blood in the Sand: When James Jones Wrote a Grunt’s View of D-Day |James Jones |November 15, 2014 |DAILY BEAST 

Another ship, the Eduard Bohlen, currently rests half-buried inland after crashing in 1909. Namibia’s Spooky Skeleton Coast |Nina Strochlic |March 5, 2014 |DAILY BEAST 

They scrambled onto boats to Lebanon, Jordan or Gaza, or raced inland. Jaffa: A Tale Of Two Lands |Lauren Gelfond Feldinger |February 16, 2014 |DAILY BEAST 

Follow that borderline inland for a couple hundred kilometers and what do you hit? Half of This Bar Is in Slovenia, the Other Half Is in Croatia |Jeff Campagna |January 6, 2014 |DAILY BEAST 

Now dense settlements crowd coastlines, and inland areas are no longer empty. Mark Hertsgaard Analyzes the Psychology of Climate-Change Activism |Mark Hertsgaard |July 14, 2013 |DAILY BEAST 

Thus four thousand Indians at most roam through, rather than occupy, these vast stretches of inland territory and sea-shore. The Jesuit Relations and Allied Documents, Vol. II: Acadia, 1612-1614 |Various 

This was bordered by salt marshes only, covered occasionally at spring tides by the sea, some of which extended pretty far inland. Birds of Guernsey (1879) |Cecil Smith 

The tidal rivers mentioned previously are actually estuaries of Chesapeake Bay and they flow periodically inland. Hallowed Heritage: The Life of Virginia |Dorothy M. Torpey 

Dead ground; defiladed from inland batteries; deep water right close to the shore! Gallipoli Diary, Volume I |Ian Hamilton 

Deedes also met me and the whole band of us made our way inland to my battle dugout. Gallipoli Diary, Volume I |Ian Hamilton