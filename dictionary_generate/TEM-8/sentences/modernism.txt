Alfred Stieglitz began exhibiting photographs in New York in the early 1900s as part of his project of introducing modernism to America. The art of the photograph; the photograph as art |Michael Roth |April 16, 2021 |Washington Post 

Secular modernism has tried to get the fruits of the Jesus-message without the roots. What is the Point of Celebrating Easter During a Pandemic? |N.T. Wright |April 2, 2021 |Time 

It’s that I think it might produce a new kind of literature, like the way modernism transformed the novel. Kazuo Ishiguro on How His New Novel Klara and the Sun Is a Celebration of Humanity |Dan Stewart |March 2, 2021 |Time 

Having been taught Modernism, a school of thought that scoffs at the decorative, materials became his primary means of expression. Frank Gehry Is Architecture’s Mad Genius |Sarah Moroz |October 27, 2014 |DAILY BEAST 

European Jews gravitated toward modernism as a way to get away from history. How Jews Created American Modernism |Andrew Romano |August 1, 2014 |DAILY BEAST 

What every fan of modernism may not know is that all of these designers were Jewish. How Jews Created American Modernism |Andrew Romano |August 1, 2014 |DAILY BEAST 

In the early years—the 1920s and 1930s—modernism was seen as “out there.” How Jews Created American Modernism |Andrew Romano |August 1, 2014 |DAILY BEAST 

In America, modernism was stripped of its socialist leanings. How Jews Created American Modernism |Andrew Romano |August 1, 2014 |DAILY BEAST 

Immanence—Agnosticism is the negative side of Modernism; immanence constitutes its positive constituent. The War Upon Religion |Rev. Francis A. Cunningham 

That its measures were effective is evident from the history of Modernism in the last three years. The War Upon Religion |Rev. Francis A. Cunningham 

They are the quaint quintessence of conservatism, and will occupy youthful minds menaced by modernism. Bizarre |Lawton Mackall 

Too angry to deny the convenient charge of "modernism," he sought the street. The Higher Court |Mary Stewart Daggett 

He bowed his head, revolving in his mind the definite charge of "modernism." The Higher Court |Mary Stewart Daggett