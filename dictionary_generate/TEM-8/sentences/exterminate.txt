Many of these scientists are fighting the very genetic diseases threatening to exterminate numbats. How the Newly Decoded Numbat Genome Could Help Bring the Tasmanian Tiger Back From Extinction |Parwinder Kaur |February 20, 2022 |Singularity Hub 

His big winning idea was to exterminate poverty for 3 million elderly people in Korea by publishing an interactive research paper to influence Korean pension reform. This OZY Genius Tackled Korean Pensions |Pallabi Munsi |March 15, 2021 |Ozy 

Fingers crossed that one of those over-the-air updates exterminates this bug without introducing something worse. Ford’s electric Mustang Mach-E is an important leap into the future |Dan Carney |February 12, 2021 |Popular-Science 

I should have voted for at least three presidential candidates at this point in my life, but that possibility where I come from was exterminated like a deadly pandemic. My vote on behalf of those who are excluded |Yariel Valdés González |October 27, 2020 |Washington Blade 

Stamets’ other mushroom nostrums include dissolving petroleum waste, exterminating unwanted insects, and creating vaccines that can eradicate viruses. How Psilocybin Can Save the Environment - Issue 90: Something Green |Mark MacNamara |September 30, 2020 |Nautilus 

Next was World War II: machine enabled Hitler, who in turn attempted to exterminate an entire people. How ‘Her’ Gets the Future Right |Andrew Romano |December 21, 2013 |DAILY BEAST 

Riegner warned the Allies that Berlin had a plan to exterminate all Jews in countries occupied or controlled by Germany. The Savior of Sobibor |Richard Rashke |October 14, 2013 |DAILY BEAST 

I think your efforts also may rival that of Germany's Adolf Hitler in his attempt to exterminate an entire race of people. Pol Pot and Me (and Guns) |Michael Tomasky |May 8, 2013 |DAILY BEAST 

“We expect the regime to try to exterminate us all,” he said. Syrians Flee to Lebanon as Regime Forces Finish ‘Cleaning’ Operation |Katie Paul |March 8, 2012 |DAILY BEAST 

Despite having the nickname “the exterminator,” DeLay did not try to exterminate wasteful spending when in power. He's Back! |John Avlon |March 8, 2009 |DAILY BEAST 

There was not a moment to lose, for one well-directed shot might exterminate half of us. Blackwood's Edinburgh Magazine, No. CCCXXXIX. January, 1844. Vol. LV. |Various 

From the very first of the war their work was to help exterminate the guerrilla bands which infested the State. The Courier of the Ozarks |Byron A. Dunn 

The regular troops, the constabulary, and other armed forces combined were unable to exterminate brigandage. The Philippine Islands |John Foreman 

You like to divide yourselves into nations, to trick yourselves out in national costumes, and to exterminate each other to music! Urania |Camille Flammarion 

Shepherds have entered into a conspiracy to exterminate the wolves. The Blot on the Kaiser's 'Scutcheon |Newell Dwight Hillis