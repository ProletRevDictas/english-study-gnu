The Midland were anxious to buy and the Ennis were willing to sell, but Parliament alone could legalise the bargain. Fifty Years of Railway Life in England, Scotland and Ireland |Joseph Tatlow 

I will give you all, but I have not the power to legalise your position.' A German Pompadour |Marie Hay 

And it would be but another step in the same direction to legalise brigandage. Chambers's Journal of Popular Literature, Science, and Art |Various 

Now only Commissaires of Districts and Missionaries can legalise marriages and the official named was neither. A Journal of a Tour in the Congo Free State |Marcus Dorman 

Don't you want to get married; to—er—legalise this extraordinary situation in which we are placed? Second Plays |A. A. Milne