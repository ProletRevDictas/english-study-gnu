As she pulled the rack out in jerky inch-by-inch increments, she gasped so loud the room fell silent. Puerto Rican pernil is a standout Thanksgiving roast. Just guard that crispy skin. |Monti Carlo |November 18, 2021 |Washington Post 

He snacked on typical thru-hiker fare—jerky, individually-wrapped pastries, chips—and did push-ups along the way, hoping to keep some upper body strength. Could Thru-Hiking Be Bad For Your Health? A New Study Makes a Troubling Find. |syadron |July 14, 2021 |Outside Online 

As a person who loses stuff and moves in a jerky fashion perfect for flinging out earbuds, the idea of truly wireless Bluetooth earbuds still worries me a little. Jaybird Run wireless earbud review: solid sound for your sweat and swole sessions |empire |June 24, 2021 |Popular-Science 

When the orchestra lit into a fast and jerky rhythm, Birkin attempted to drag him onto the dance floor. Jane Birkin is back with a new album, but her presence is everlasting |Jeff Weiss |March 19, 2021 |Washington Post 

He may be right, but the two companies could be more ambitious than just vegan Doritos and make snacks that have been largely neglected by plant-based meat producers, like beef jerky or pork rinds. Beyond Meat and Pepsi are teaming up to make plant-based snacks and drinks |Dylan Matthews |January 27, 2021 |Vox 

They became so brown and shriveled that they looked like walking beef jerky with New York accents. Powerful Congressman Writes About ‘Fleshy Breasts’ |Asawin Suebsaeng |January 7, 2015 |DAILY BEAST 

You, too, will be zipping along to the angular guitars and zigzagging, herky-jerky vocals. The 10 Best Albums of 2014: Taylor Swift, Sia, Run the Jewels, and More |Marlow Stern |December 28, 2014 |DAILY BEAST 

He's so white he's almost mock-white, and so are his jerky, long-necked, mechanical-man movements. The Stacks: Pauline Kael's Talking Heads Obsession |Pauline Kael |November 22, 2014 |DAILY BEAST 

I sent him some caribou jerky from Alaska to help keep up his strength on the Senate floor. Nostalgia Act: The Great Sarah Palin Revival Tour of 2013 |Nick Gillespie |June 18, 2013 |DAILY BEAST 

It was superficial and jumpy and herky-jerky, bopping all over the place. Debate III: Obama Wins, But Does It Do Him Any Good? |Michael Tomasky |October 23, 2012 |DAILY BEAST 

Each sentence came as if torn piecemeal from his unwilling tongue; short, jerky phrases, conceived in pain and delivered in agony. Raw Gold |Bertrand W. Sinclair 

Nervous and jerky he walked to the center of the stage, and raised his hand begging silence. The Fifth String |John Philip Sousa 

With jerky motions the black monster drew down, the water rippling and gurgling along the sides. Menotah |Ernest G. Henham 

He knocked the ashes from his cigar and threw out his arms with one of his odd, jerky gestures. A Hoosier Chronicle |Meredith Nicholson 

He spoke in very fair language, short, jerky sentences, but well-chosen words. The Hills and the Vale |Richard Jefferies