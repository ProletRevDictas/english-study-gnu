She and other detractors also argue the bill’s affirmative defense provision would place extra burden on defendants because it could require them to produce documentation in their defense which can lead to costly litigation. Cheat sheet: U.S. lawmakers propose Section 230 reforms to regulate online paid speech |Kate Kaye |February 10, 2021 |Digiday 

He also helped establish one of the federal government’s first affirmative action programs, the so-called Philadelphia Plan, which set goals for minority employment at federally subsidized construction sites. George P. Shultz, counsel and Cabinet member for two Republican presidents, dies at 100 |Michael Abramowitz, David Hoffman |February 8, 2021 |Washington Post 

Another recent celebrity disclosure seems to offer a disappointingly affirmative answer to that question. Rupert Everett reminds us homophobia persists in Hollywood |John Paul King |October 10, 2020 |Washington Blade 

A Labor Department spokesperson told Axios that the agency "appreciates Microsoft's assurance on its website that it is not engaging in racial preferences or quotas in seeking to reach its affirmative action and outreach goals." The numbers behind tech's big diversity gaps |Ina Fried |October 9, 2020 |Axios 

Allowing Los Angeles to pass an affirmative action policy would allow it to more quickly diversify its workforce, she argues. Morning Report: SANDAG Head Wades Into Supes Race |Voice of San Diego |August 14, 2020 |Voice of San Diego 

His was one of six votes against the day, which received 90 votes in the affirmative. Steve Scalise and the Right’s Ridiculous Racial Blame Game |Michael Tomasky |January 2, 2015 |DAILY BEAST 

In another unusual move, the grand jury considered not only the basic elements of the crime, but also affirmative defenses. We Need More Ferguson-style Grand Juries |Kaimipono Wenger |November 30, 2014 |DAILY BEAST 

A sizable number of Asian Americans feel that affirmative action, in college admissions or elsewhere, has hurt them personally. Why Harvard's Asians Are Invisible |Tim Mak |November 28, 2014 |DAILY BEAST 

And how could an accused person prove affirmative consent, especially of the nonverbal variety? Harvard Liberals Hate New Campus Sex Laws |Cathy Young |October 19, 2014 |DAILY BEAST 

And Derbyshire “always assume[s] that any black person in a well-paid position is an Affirmative Action hire.” Fringe Factor: Keep Harvey Milk Off Our Mail! |Caitlin Dickson |June 1, 2014 |DAILY BEAST 

She asks if the piece shown to her by the saleswoman is all silk, who makes an affirmative reply. Putnam's Handy Law Book for the Layman |Albert Sidney Bolles 

The attendant stooped over the bed to ascertain, and nodded in the affirmative. Oliver Twist, Vol. II (of 3) |Charles Dickens 

We are ourselves satisfied, and undertake to demonstrate to our readers, that this question must be answered in the affirmative. Blackwood's Edinburgh Magazine, No. CCCXXXIX. January, 1844. Vol. LV. |Various 

Mr. Boar, who had watched his approach, rose at the removal of the hat, and replied in the affirmative. Life of Richard Trevithick, Volume II (of 2) |Francis Trevithick 

Fagin nodded in the affirmative, and pointing in the direction of Saffron Hill, inquired whether any one was up yonder to-night. Oliver Twist, Vol. II (of 3) |Charles Dickens