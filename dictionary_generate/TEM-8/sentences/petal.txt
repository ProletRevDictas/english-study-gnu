That’s not to say that the process is fast—it can take a month for the petals to fully dry out—but your work load up front is minimal. How to preserve flowers in 4 easy ways |empire |August 20, 2021 |Popular-Science 

Marigolds’ fluffy, tightly-packed petals set them apart from other garden favorites, but you can be even more certain you’ve found the right plant by looking for its distinctive fern-like leaves. 7 edible flowers and how to use them |John Kennedy |July 20, 2021 |Popular-Science 

As the first wave of the pandemic raged in India, Modi called for festivals of light and asked the air force to dispatch helicopters to shower hospitals with flower petals. 'Medical Populism' Has Defined the Philippines' Response to COVID-19. That's Why the Country Is Still Suffering |Aie Balagtas See / Manila |June 28, 2021 |Time 

The artichokes are done when the large outer petals can be easily pulled off. How to prep, cook and enjoy stuffed artichokes |Ann Maloney |March 11, 2021 |Washington Post 

Then, using scissors, trim away any remaining thorny tips from the petals, working your way around the vegetable. How to prep, cook and enjoy stuffed artichokes |Ann Maloney |March 11, 2021 |Washington Post 

The shopper bags continued the petal effect, made perhaps from extra material from a wedding dress covered in rosettes. Marni's Sophisticated Spring/ Summer 2014 Collection |Liza Foreman |September 22, 2013 |DAILY BEAST 

And to keep the sun out of her eyes, the Burberry girl will be wearing white framed, petal-shaped dark glasses next spring. Burberry Prorsum Spring/ Summer 2014 |Tom Sykes |September 16, 2013 |DAILY BEAST 

He only brushed a flower so lightly with his fingertips that he did not disturb as much as a petal. The Sept. 11 Survivor Tree in Bloom |Michael Daly |March 24, 2012 |DAILY BEAST 

The satirical Petal Paper found an audience—but more and more of them were out of state. Don't Buy Haley Barbour's Myth |Harold Evans |December 20, 2010 |DAILY BEAST 

He lived in the small town of Petal, Mississippi, across the river from Hattiesburg. Don't Buy Haley Barbour's Myth |Harold Evans |December 20, 2010 |DAILY BEAST 

Its shape is of a lotos bud, and the long fissures that plough a mountain side are now but delicate gold veining on a petal. The Dragon Painter |Mary McNeil Fenollosa 

"Then I am quite sure you will remember," she laughed, and fell to picking a rose apart, petal by petal. The Colonel of the Red Huzzars |John Reed Scott 

She took another bungling stitch in the petal of a white floss daisy. The Butterfly House |Mary E. Wilkins Freeman 

Ariadne was the white and slender lily, slowly unfolding petal after petal in obedience to the law of its own inner growth. Unveiling a Parallel |Alice Ilgenfritz Jones and Ella Marchant 

That other night, after he had gone home, he had found a solitary pink petal clinging to his scarf-pin. The White Shield |Myrtle Reed