At 14 hives, the researchers used a balloon tied to a stick to chase off hornets, says Otis. Honeybees fend off deadly hornets by decorating hives with poop |Asher Jones |January 19, 2021 |Science News For Students 

Let’s start with the stick vacuumVacuum cleaners these days come in all shapes and sizes and there are various aspects you might want to think about when working out which one you want. Best vacuum cleaner: How to tidy up fast |Charlotte Marcus |January 14, 2021 |Popular-Science 

If he had 10 plates spinning on sticks before, now he’s got 20. Americans across the political spectrum fear what the Capitol attack portends |Annie Gowen, Jenna Johnson, Holly Bailey |January 12, 2021 |Washington Post 

If crosspieces are in the right place, your toes should be covering the first stick you tied, and your heels should be covering the second stick you tied, for each shoe. How to build snowshoes on the fly—and 4 other tips for surviving deep snow |By Tim MacWelch/Outdoor Life |January 5, 2021 |Popular-Science 

This guy’s charging at them, with a knife in one hand, a stick in the other, screaming at them, in a confined space. “Nobody” Hurt, “Just a Perp,” Say Officers After NYPD Shot and Killed Man in His Own Home |by Eric Umansky |January 4, 2021 |ProPublica 

Added to drinking water at concentrations of around one part per million, fluoride ions stick to dental plaque. Anti-Fluoriders Are The OG Anti-Vaxxers |Michael Schulson |July 27, 2016 |DAILY BEAST 

He wore white gloves, a dignified long black coat, and matching pants and vest, and he carried a dark walking stick. The Black Man Who Replaced Jefferson Davis in the Senate |Philip Dray |January 7, 2015 |DAILY BEAST 

After some animated debate at the conference, Lelaie declared, with some frustration, “If you push on the stick, you will fly.” Flight 8501 Poses Question: Are Modern Jets Too Automated to Fly? |Clive Irving |January 4, 2015 |DAILY BEAST 

The birds poop all over the forest, and thanks to the viscin, the mistletoe seeds in said poop stick to branches. Mistletoe is the Vampire of Plants |Helen Thompson |December 21, 2014 |DAILY BEAST 

And for Larry Flynt, this might be a monumental opportunity to stick it to the dictator the best way he knows how. Kim Jong Un, Avert Your Eyes: Sony’s ‘The Interview’ Gets the Porn Parody Treatment |Aurora Snow |December 20, 2014 |DAILY BEAST 

You see, they always butter their chairs so that they won't stick fast when they sit down. Davy and The Goblin |Charles E. Carryl 

Whoever succeeded in getting the ring on his stick won the game, and carried the prize home as a sign of victory. Our Little Korean Cousin |H. Lee M. Pike 

By using his walking stick he discovered that they formed a trail to a point in the wall. The Joyous Adventures of Aristide Pujol |William J. Locke 

I am not informed further; but inasmuch as you are living on the place, my advice is that you stick right there, and hold it. The Homesteader |Oscar Micheaux 

The last time I tried it, I caught the end of my stick between two rocks and it broke. The Soldier of the Valley |Nelson Lloyd