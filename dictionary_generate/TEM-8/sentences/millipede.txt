He says that their mention of millipedes will no longer require the line that technically, their name is a misnomer. Scientists discover the first true millipede |Jonathan Lambert |January 5, 2022 |Science News For Students 

The leafy bits of bait nabbed a group of eight curiously long, threadlike millipedes from the soil. Scientists discover the first true millipede |Jonathan Lambert |January 5, 2022 |Science News For Students 

Yet no millipede with more than 750 legs has ever been found, until now. A 1,306-legged millipede is the first to live up to its name |Jonathan Lambert |December 21, 2021 |Science News 

Researchers nabbed the specimen and seven other curiously long, threadlike millipedes by dropping cups baited with leaf litter into drill holes used for mineral prospecting that were up to 60 meters deep. A 1,306-legged millipede is the first to live up to its name |Jonathan Lambert |December 21, 2021 |Science News 

At long last, scientists have discovered a millipede that lives up to its name and actually has 1,000 legs. This eyeless millipede shattered the record for most legs |Kate Baggaley |December 17, 2021 |Popular-Science 

If there is pathos in this, there is bathos in his apostrophe to the millipede, beginning "Poor sowbug!" Medical Essays |Oliver Wendell Holmes, Sr. 

The monstrous millipede stood immobile, trapped for the moment by the gratification of all its desires. The Forgotten Planet |Murray Leinster 

The creature was a monstrous millipede, forty feet in length, with features of purest, unadulterated horror. The Forgotten Planet |Murray Leinster 

And this could have been safety for them—save for the giant millipede no more than half a mile below. The Forgotten Planet |Murray Leinster 

The Captain and Murray fell in at the tail of the quivering millipede. The Trimmed Lamp |O. Henry