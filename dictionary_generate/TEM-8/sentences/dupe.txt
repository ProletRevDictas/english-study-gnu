Wohl and Burkman also duped The Washington Post last year, when the pair staged a fake FBI raid that The Post briefly reported on, then removed. FCC proposes record $5 million fine against Jacob Wohl, Jack Burkman for election robocalls |Rachel Lerman |August 24, 2021 |Washington Post 

It is insulting to suggest that they are mere dupes or tools. Supreme Court gives another big green light to GOP voting restrictions |Aaron Blake |July 1, 2021 |Washington Post 

“I understand, sir,” said Young, an MBA graduate who has said he felt “duped” by the Oath Keepers and whose sister has also been charged after signing up with the group. Second alleged Oath Keepers member pleads guilty in Jan. 6 Capitol riot, will cooperate as prosecutors seek momentum |Spencer Hsu |June 23, 2021 |Washington Post 

However, we are less inclined to accept being duped into voting a certain way by big tech social media forces. To What Extent Are We Ruled by Unconscious Forces? |Magda Osman |May 31, 2021 |Singularity Hub 

The males co-opted by the fungus also flick their wings like females to dupe other males into attempting sex. A fungus could turn some cicadas into sex-crazed ‘salt shakers of death’ |Marisa Iati |May 19, 2021 |Washington Post 

It was a beautified camp the Nazis used to dupe international visitors and officials. How Do You Write About the Holocaust? |Ilana Bet-El |May 5, 2013 |DAILY BEAST 

Anyone who disagreed with their thinking, including fellow Republicans, was a traitor, or a liar, or a dupe. Republicans Allowed Karl Rove to Mislead Them Again |Matt Latimer |November 17, 2012 |DAILY BEAST 

Steven decided to dupe his doctor when he returned from his elite boarding school exhausted by the intense competition there. Faking ADHD Gets You Into Harvard |Heidi Mitchell |January 25, 2012 |DAILY BEAST 

But they apparently rejected the idea that Rana remained a dupe once the carnage in India had happened. Chicago Trial's Explosive Revelations |ProPublica |June 10, 2011 |DAILY BEAST 

Parker would dupe customers into buying polyester sweaters he claimed were 100 percent cashmere, then gloat about how easy it was. Partying All the Way to Jail |Peter Davis |December 21, 2009 |DAILY BEAST 

But Mr. Howard, dupe or rogue, was extremely busy in publishing to the world the particulars of this extraordinary case. The Portsmouth Road and Its Tributaries |Charles G. Harper 

He knows when a sentiment is simple and when it is complex, when the heart is a dupe of the mind and when of the senses. Repertory Of The Comedie Humaine, Complete, A -- Z |Anatole Cerfberr and Jules Franois Christophe 

Had my spirit really been transported to the planet Mars, or had I been the dupe of a purely imaginary illusion? Urania |Camille Flammarion 

He imagined himself the dupe of one of those mirages which he had more than once beheld when in his dreamy moods. Toilers of the Sea |Victor Hugo 

Whoever does not know this and is a Socialist, that man is merely one of the herd or he is a dupe. The New Society |Walther Rathenau