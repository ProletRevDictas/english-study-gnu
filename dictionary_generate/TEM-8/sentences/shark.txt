The researchers compared the amount of carbon-14 in the growth bands of whale shark vertebrae with the known carbon-14 levels in surface seawater in different years. Traces from nuclear-weapons tests offer clues to whale sharks’ ages |Maria Temming |May 20, 2020 |Science News For Students 

It’s been surprisingly hard to figure out how old a whale shark is. Traces from nuclear-weapons tests offer clues to whale sharks’ ages |Maria Temming |May 20, 2020 |Science News For Students 

His group also used the total number of growth bands in sharks’ dated vertebrae to figure out their the animals’ ages. Traces from nuclear-weapons tests offer clues to whale sharks’ ages |Maria Temming |May 20, 2020 |Science News For Students 

If young whale sharks spend much of their time deep underwater, they may not take in the same amount of carbon-14 that is measured at the ocean’s surface. Traces from nuclear-weapons tests offer clues to whale sharks’ ages |Maria Temming |May 20, 2020 |Science News For Students 

Not knowing that made it hard to gauge just how fast these sharks were growing or how long they lived. Traces from nuclear-weapons tests offer clues to whale sharks’ ages |Maria Temming |May 20, 2020 |Science News For Students 

Porter was convicted and shortly after sentenced to death by a judge who compared him to a shark in a feeding frenzy. Wrongly Imprisoned for 15 Years Thanks to an Innocence Project |Jacob Siegel |November 13, 2014 |DAILY BEAST 

Her downfall came about, because for a second she forgot that to swim in the shark pool, you have to always act like a shark. ‘Housewife Tycoon’ Took On ‘Mad Men’ NYC Real Estate Market and Won |Vicky Ward |October 26, 2014 |DAILY BEAST 

For humans, Shark Week is just a once-a-year sweeps event for the Discovery Channel. Shark Deathmatch, Crazy ‘Simpsons’ Couch Gag, and More Viral Videos |Alex Chancey |October 4, 2014 |DAILY BEAST 

However, for sharks—and Tracy Jordan—every week is Shark Week. Shark Deathmatch, Crazy ‘Simpsons’ Couch Gag, and More Viral Videos |Alex Chancey |October 4, 2014 |DAILY BEAST 

Worse, she obsesses over this with all of the friends and then tries to incorporate shark imagery into their sex life. 15 Times ‘Friends’ Was Really, Really Weird |Kevin Fallon |September 18, 2014 |DAILY BEAST 

A shark was also taken, eleven feet long; and many curious specimens of crustacea and medusa were obtained by the towing-net. Narrative of a Survey of the Intertropical and Western Coasts of Australia] [Volume 2 of 2] |Phillip Parker King 

This engaging rascal is found helping a young cricket player out of the toils of a money shark. The Idyl of Twin Fires |Walter Prichard Eaton 

Since their desperate dive into the sea, and the adventure with the shark, the two darkeys and the orphan had become fast friends. Jack Harkaway's Boy Tinker Among The Turks |Bracebridge Hemyng 

“If we do,” I said, and I pointed with a thrill of horror to the fin of the shark as its wicked eye glanced up at us. In the Wilds of Florida |W.H.G. Kingston 

What would have been his fate, however, had the monster of a shark we saw been near at hand at the moment he fell overboard! In the Wilds of Florida |W.H.G. Kingston