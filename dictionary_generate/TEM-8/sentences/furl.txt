Once, when it was rather gusty weather, all hands were wanted, and the skipper ordered him to furl a sail. Eric, or Little by Little |Frederic W. Farrar 

He instantly gave orders to furl the awning, and to be ready to make sail as soon as the breeze should reach us. Peter the Whaler |W.H.G. Kingston 

He leaped upon the house and helped Dolph and Otie furl the mainsail that lay sprawled in the lazy-jaeks. Blow The Man Down |Holman Day 

It was my duty to furl the fore-royal; and while standing by to loose it again, I had a fine view of the scene. Is Shakespeare Dead? |Mark Twain 

Mr Nott now took the helm, while the crew went aloft to furl the lighter canvas and to take a reef in the topsails. True Blue |W.H.G. Kingston