Sure enough, the average change in diastolic blood pressure was a clinically significant reduction of five mmHg, which is good news. Why Altitude Training Helps Some but Not Others |Alex Hutchinson |September 11, 2020 |Outside Online 

Driven by environmental concerns, Kim-Parker saw an opportunity to create a space that would aid in the reduction of pollution in the fashion landscape. The CEO striving to make vintage, secondhand clothing as popular as fast fashion |Rachel King |September 6, 2020 |Fortune 

Those reductions would mean much longer wait times and more crowded buses and subways. New York City transit needs a $12 billion bailout—or the entire U.S. economic recovery may suffer |dzanemorris |September 3, 2020 |Fortune 

The potential for future reductions in the cost of electricity from silicon solar, for example, is limited. How a New Solar and Lighting Technology Could Propel a Renewable Energy Transformation |Sam Stranks |September 3, 2020 |Singularity Hub 

The housing authority agreed to lower Brown’s rent to $318 a month but said the reduction would not take effect until March. She Was Sued Over Rent She Didn’t Owe. It Took Seven Court Dates to Prove She Was Right. |by Danielle Ohl, Capital Gazette, and Talia Buford and Beena Raghavendran, ProPublica |August 25, 2020 |ProPublica 

Does that mean a reduction in policing would be a good thing? Ground Zero of the NYPD Slowdown |Batya Ungar-Sargon |January 1, 2015 |DAILY BEAST 

Like many I spoke to, Williams seemed to desire a reorientation of policing, rather than just a reduction. Ground Zero of the NYPD Slowdown |Batya Ungar-Sargon |January 1, 2015 |DAILY BEAST 

Proper use could lead to weight loss and reduction in gastric reflux. Nothing Says I Love You Like Data |The Daily Beast |December 8, 2014 |DAILY BEAST 

The reduction in the unemployment levels is largely due to part time jobs and more people simply giving up looking for jobs. Voters Remind D.C. That the Economy Still Sucks |Stuart Stevens |November 6, 2014 |DAILY BEAST 

Reagan learned this in the midst of negotiating historic arms-reduction treaties with the Soviets at the height of the Cold War. It’s Time to Nail the Iran Nuke Deal |Rep. Rush Holt, Kate Gould |October 15, 2014 |DAILY BEAST 

It is clear, therefore, that the reserve reduction contemplated by the act will not be realized in practice. Readings in Money and Banking |Chester Arthur Phillips 

If a company has no debts, a reduction in its capital made in an open manner in accordance with law, is legal. Putnam's Handy Law Book for the Layman |Albert Sidney Bolles 

On the 28th of March however it was found necessary to make a considerable reduction in the allowance. Narrative of a Survey of the Intertropical and Western Coasts of Australia] [Volume 2 of 2] |Phillip Parker King 

That high-pressure engines owed their advantages mainly to a reduction of the relative importance of this latent heat. Life of Richard Trevithick, Volume II (of 2) |Francis Trevithick 

This agreement caused a great reduction in the number of imports from Great Britain to these colonies. Hallowed Heritage: The Life of Virginia |Dorothy M. Torpey