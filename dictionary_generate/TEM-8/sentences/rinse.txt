If your washer allows you to select rinse temperature options, pick warm. How to Clean Your Cycling Gear |agintzler |July 17, 2021 |Outside Online 

I use that water to water my garden, as well as any other water that is clean, such as rinse water that I use when washing my dishes. Hints From Heloise: Bride gets an awkward phone call |Heloise Heloise |May 21, 2021 |Washington Post 

Give the bottles and cans a quick rinse with water before tossing them in the bin. Go ‘green’ with your wine: Choose cans or boxes, drink local and consider farming practices |Dave McIntyre |April 23, 2021 |Washington Post 

Though most quinoa now comes pre-rinsed, which removes saponins that can make quinoa taste slightly bitter, you can give it an extra rinse to ensure it turns out tasty. 5 hearty grains and how to use them in bowls, salads and even cake |Kari Sonde |February 25, 2021 |Washington Post 

Cleaning farmed mussels generally means a simple rinse and a sort to make sure they’re all alive. Mussels are simple to prepare, and add tasty drama to the table |Martha Holmberg |October 29, 2020 |Washington Post 

So I did all I could do: rinse, spit and climb back into bed. Can a Jew Get Down With Hot Jesus from 'Son of God?' |Sara Lieberman |March 9, 2014 |DAILY BEAST 

Chop the wings and bones into 1-inch (2.5cm) pieces, rinse with cold water, pat dry, and reserve, chilled, for the jus. Daniel Boulud Reveals His 4 Favorite Recipes From His New Cookbook |Daniel Boulud |October 15, 2013 |DAILY BEAST 

For the Glazed Radishes Trim the radishes, leaving a bit of the stem, and rinse. Daniel Boulud Reveals His 4 Favorite Recipes From His New Cookbook |Daniel Boulud |October 15, 2013 |DAILY BEAST 

Rinse and repeat until the only people in the market are incredibly expensive to cover. Are Young, Single Adults Expecting Obamacare to Cost So Much? |Megan McArdle |June 4, 2013 |DAILY BEAST 

We found that people are less likely to become ill if they at least rinse their produce. Be Afraid of Your Food: An Epidemiologist’s Sensible Advice |Amanda Kludt |March 16, 2013 |DAILY BEAST 

Rinse it in two cold waters, with a drop or two of liquid blue in the last. The Ladies' Book of Etiquette, and Manual of Politeness |Florence Hartley 

Next rinse the veil through two cold waters, tinging the last with indigo. The Ladies' Book of Etiquette, and Manual of Politeness |Florence Hartley 

Rinse out a second time, and brush, and dry near a fire, or in a warm room. The Ladies' Book of Etiquette, and Manual of Politeness |Florence Hartley 

Rinse it through a clean, lukewarm water; wring it lengthways, and stretch it well. The Ladies' Book of Etiquette, and Manual of Politeness |Florence Hartley 

Then transfer it wet to a lukewarm suds, wash and rinse it well, and dry and iron it. The Ladies' Book of Etiquette, and Manual of Politeness |Florence Hartley