In a second complaint filed last December, Nuñez said that her credit score had been damaged after Oportun charged off her account, meaning it didn’t expect to be repaid, despite the fact that she had resumed payments. The Loan Company That Sued Thousands of Low-Income Latinos During the Pandemic |by Kiah Collier, Ren Larson and Perla Trevizo |August 31, 2020 |ProPublica 

In a filing announcing its initial public offering, it said that the vast majority of dollars lent had been repaid. The Loan Company That Sued Thousands of Low-Income Latinos During the Pandemic |by Kiah Collier, Ren Larson and Perla Trevizo |August 31, 2020 |ProPublica 

Businesses will scramble to make up for thousands of dollars in lost income, to repay debts to vendors, to pay back months of rent. Is the Government Just Going to Watch the Restaurant Industry Die? |Elazar Sontag |August 28, 2020 |Eater 

Having produced all living things, it deserves our gratitude, while its power and subtle complexity repay scientific scrutiny. Just Because It’s Natural Doesn’t Mean It’s Good - Issue 89: The Dark Side |David P. Barash |August 19, 2020 |Nautilus 

This left them with insufficient funds to repay the buyers, so they’re offering a 120% future credit, which many people are upset about. What to do if you lost money on an event canceled because of COVID-19 |Rachel King |August 19, 2020 |Fortune 

She vowed to repay the money—no official word, however, on whether she ever did that. Fergie Dives Into Prince Andrew’s Sex Scandal |Tom Sykes |January 5, 2015 |DAILY BEAST 

There is no way I could ever repay the debt I owe him for his standing by me through the challenges of the last decade. A Bishop’s Decision to Divorce |Gene Robinson |May 4, 2014 |DAILY BEAST 

Chesapeake pledged to pay Access enough in fees to repay the $5 billion plus a 15 percent return on its pipelines. How the Kings of Fracking Double-Crossed Their Way to Riches |ProPublica |March 13, 2014 |DAILY BEAST 

The electric sports car maker is using a debt offering to repay a $465 million government loan – nine years before it is due. Tesla Motors will repay government loan nine years early |Daniel Gross |May 15, 2013 |DAILY BEAST 

But whatever the amount, over a couple of years it will more than repay the hour that you spent setting all this up. Happy Tax Day. Now Stop Making Interest-Free Loans to the U.S. Government. |Megan McArdle |April 14, 2013 |DAILY BEAST 

Ill should I repay the family who fostered my son, were I to surrender their darling into the hands of his enemies. The Pastor's Fire-side Vol. 3 of 4 |Jane Porter 

In former transactions with his wife, he had pledged his word of honour to repay her. Blackwood's Edinburgh Magazine, No. CCCXXXIX. January, 1844. Vol. LV. |Various 

As unto revenge, as it were to repay wrath to his adversaries, and a reward to his enemies: he will repay the like to the islands. The Bible, Douay-Rheims Version |Various 

For how otherwise but by diminishing wages can they repay themselves for lost time, for trouble, and for expense? Blackwood's Edinburgh Magazine, No. CCCXXXIX. January, 1844. Vol. LV. |Various 

To erect steam-engines, they never could believe would repay the expense. Life of Richard Trevithick, Volume II (of 2) |Francis Trevithick