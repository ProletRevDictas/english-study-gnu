It’s just one of a series of new taxation strategies that a host of countries are embracing to adapt to a world transformed by the pandemic, technology and globalization in ways unimaginable even a decade ago. Can These Creative Taxes Catch Up With a Changing World? |Nick Fouriezos |January 14, 2021 |Ozy 

It’s a classic case of “taxation without representation,” critics say. Arguments for D.C. statehood grow stronger following Capitol attack and plans for inauguration violence |Aric Jenkins |January 12, 2021 |Fortune 

Policy advocates argue that traditional property taxation discourages people from using land productively by raising their costs if they want to make improvements. These Cities Tried to Tackle Disinvestment. Here Are Lessons From What Happened. |by Haru Coryne and Tony Briscoe |December 30, 2020 |ProPublica 

The community’s impoverished farmers found their goods seized in taxation and used to feed the army. These Pioneers Tried to Get the Confederate Flag Out of Mississippi 156 Years Ago |Fiona Zublin |December 3, 2020 |Ozy 

Economists have long understood that taxation can have a distortionary effect on our economy, discouraging things we want more of, such as commerce. A Sixth Big Move for SANDAG’s New Transportation Vision |Joe Bettles |November 30, 2020 |Voice of San Diego 

In America, low turnout is the new black eye for a country founded on “no taxation without representation.” Founding Fathers Loved Drunk Voters |Kevin Bleyer |November 1, 2014 |DAILY BEAST 

Taxes are an obvious benchmark, since right now, employed teenagers are literally subjected to taxation without representation. Paying Taxes and Going to Jail Like Adults; Teens Deserve the Right to Vote, Too |Jillian Keenan |October 6, 2014 |DAILY BEAST 

The marginal rate of taxation applies much more heavily on the secondary wage. Transcript: Thomas Friedman Interviews Hillary Clinton and Christine Lagarde | |April 5, 2014 |DAILY BEAST 

They support progressive taxation, they support many or even most categories of government spending, and so on. A Most Revealing Week for Republicans |Michael Tomasky |April 4, 2014 |DAILY BEAST 

“Taxation Without Representation” is on many license plates. There’ are Gray Shadows in Tuesday’s DC Primary |Eleanor Clift |March 31, 2014 |DAILY BEAST 

Regular taxation, monopolies, mortgages, and loans barely sufficed to provide for the budget. Napoleon's Marshals |R. P. Dunn-Pattison 

And it cannot do this unless it continues to use the terrific engine of taxation already fashioned in the war. The Unsolved Riddle of Social Justice |Stephen Leacock 

Clearly, it was no mere question of taxation but the larger question of legislative independence that now confronted Americans. The Eve of the Revolution |Carl Becker 

The court said the game would not work, that for the purposes of taxation the concern must be regarded as an individual. Putnam's Handy Law Book for the Layman |Albert Sidney Bolles 

This provision also had the effect of preventing the imposition of taxation upon the community by means of railway rates. Fifty Years of Railway Life in England, Scotland and Ireland |Joseph Tatlow