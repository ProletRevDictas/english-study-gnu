Working remotely was supposed to make us more engaged and productive, liberating our creativity from the confines of the office. Innovation just isn’t happening over Zoom |matthewheimer |December 17, 2020 |Fortune 

Rather, such new technologies effectively “liberate” quantum mechanics from the confines of atoms and molecules and bring it to the macroscopic scales of everyday life. Contemplating the End of Physics |Robbert Dijkgraaf |November 24, 2020 |Quanta Magazine 

Instead, we should think of them as liberating, a way to keep us away from the virus. As CDC Warns Against Thanksgiving Travel, Here Are A Dozen More Things You Can Do To Help Stop COVID-19 |LGBTQ-Editor |November 21, 2020 |No Straight News 

The following age will be modulated through ubuntu, liberating the last of humankind to exist in a hierarchal civilization. Fiction: Quiet earth philosophy |Katie McLean |October 21, 2020 |MIT Technology Review 

Or, I realized after some further thought, they might be planning to shop around for new partners as soon as they were liberated from the existing contract, exploring all the available options regarding studio, technology, publisher, and cash. ‘The Dream Architects’: Inside the making of gaming’s biggest franchises |Rachel King |September 1, 2020 |Fortune 

And we will liberate Chechnya and the entire Caucasus, God willing. ISIS Is Putin’s Problem, Too, and This Chechen Is One Reason Why. |Anna Nemtsova |September 29, 2014 |DAILY BEAST 

I would enslave them even more and liberate men so they would start behaving like men again. What Joan Rivers Said She Would Do If She Were Dictator of America |Asawin Suebsaeng |September 5, 2014 |DAILY BEAST 

We fought in Mosul 10 days,” Jasim said, “then they sent us to Bayji to help the army liberate the refinery. Bikers of Baghdad: Sunnis, Shias, Skulls, ‘Harleys,’ and Iraqi Flags |Jacob Siegel |July 20, 2014 |DAILY BEAST 

During the conflict to liberate Kuwait, Egypt contributed the fourth-largest contingent of troops to the international coalition. Let's Get Real: Washington Can't Walk Away From Cairo |Frank G. Wisner |May 26, 2014 |DAILY BEAST 

They participated in the struggle to liberate India in language borrowed from or revitalized by their rulers. India’s Newest State Telangana Is Bosnia Redux |Kranti Rai |March 22, 2014 |DAILY BEAST 

He promised to liberate all prisoners of war who might fall into insurgent hands, on surrender of their arms and ammunition. The Philippine Islands |John Foreman 

At the first meeting the Filipinos agreed to liberate all except the friars, because these might raise trouble. The Philippine Islands |John Foreman 

Fearless, strong, and proud, he will conquer all obstacles; he will break his chains and liberate mankind. Prison Memoirs of an Anarchist |Alexander Berkman 

But to act, I have come, madame, to liberate from this shambles the gentle lamb you hold here prisoned. St. Martin's Summer |Rafael Sabatini 

They profess to liberate the soul from the evils of mortal life,--to arrive at eternal beatitudes. Beacon Lights of History, Volume I |John Lord