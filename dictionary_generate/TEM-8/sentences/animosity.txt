One of the most toxic is racial animosity — resentment and anger that take shape as the belief that people of another race aren’t like you, can’t be trusted and don’t deserve what you deserve. Our Radicalized Republic |Maggie Koerth (maggie.koerth-baker@fivethirtyeight.com) |January 25, 2021 |FiveThirtyEight 

Whitsell wrote that Democrats only pursued the impeachment because of their animosity to the president and the party’s “long program” to damage any critics. Morning Report: Gloria’s Big Plans and Priorities |Voice of San Diego |January 15, 2021 |Voice of San Diego 

A criminal investigation found ongoing internal animosity at eBay toward the bloggers, who sometimes had been critical of eBay in their coverage. The biggest business scandals of 2020 |Lee Clifford |December 27, 2020 |Fortune 

Rivalry, animosity, and ego have long been hallmarks of the bird world. How eBird Changed Birding Forever |Jessie Williamson |December 4, 2020 |Outside Online 

After weeks of violent play and increasing animosity comes one of the sports world’s great shows of sportsmanship and respect. Danny Meyer wants to bring everyone to the table after a fractious election. Twitter says: Too soon. |Tim Carman |November 9, 2020 |Washington Post 

But the animosity between the community and law enforcement is nothing new. Ferguson Shows a Nation at War With Itself |Roland S. Martin |August 16, 2014 |DAILY BEAST 

Is it weird to see all the current animosity between the U.S. and Russia now? 'Archer Creator Adam Reed on 'Vice,' Season 6's 'Unreboot,' and New Characters |Marlow Stern |August 5, 2014 |DAILY BEAST 

The result created quite a bit of “animosity and bad blood.” How Eric Cantor Sabotaged Himself |Ben Jacobs, Tim Mak |June 11, 2014 |DAILY BEAST 

It is this mindless atrocity, driven by both avarice and animosity, that is at play in the film. Holocaust Horrors Haunt the Films ‘Ida’ And ‘The German Doctor’ |Jack Schwartz |May 12, 2014 |DAILY BEAST 

But animosity started in the1920s, with Jewish-Arab clashes. Jaffa: A Tale Of Two Lands |Lauren Gelfond Feldinger |February 16, 2014 |DAILY BEAST 

Hilda impetuously turned her head; their glances met for an instant, in suspicion, challenge, animosity. Hilda Lessways |Arnold Bennett 

The animosity to the Dutch mingled itself both with the animosity to standing armies and with the animosity to Crown grants. The History of England from the Accession of James II. |Thomas Babington Macaulay 

Animosity is resolute even in its caprices; it has few facilities for disguise and but little capacity for assumption. A Cursory History of Swearing |Julian Sharman 

There was therefore not the same animosity in their struggle as there might have been had the religious question entered it. Catherine de' Medici |Honore de Balzac 

Frederick's animosity reached its highest pitch at that time, and we now know the full extent of the malady. The Seven Cardinal Sins: Envy and Indolence |Eugne Sue