I don’t think that they were ever in that predicament to that degree. The Tampa Bay Lightning Are Skating It Back By Sliding Just Under The Salary Cap |Julian McKenzie |February 2, 2021 |FiveThirtyEight 

One user in East Moline, Illinois, who described the predicament on a DSLReports forum in early January, said they paid for the 6TB plan "to make sure we wouldn't go over the cap" and had never used more than 4TB. Cable ISP warns “excessive” uploaders, says network can’t handle heavy usage |Jon Brodkin |January 29, 2021 |Ars Technica 

Part of the reason for this predicament is that we too often look at online threats through a partisan lens. Want to Stop the Next Crisis? Teaching Cyber Citizenship Must Become a National Priority |Michael McConnell |January 21, 2021 |Time 

Specialists in clinical trials said the case of Novavax highlights the predicament in trying to develop a roster of multiple vaccines simultaneously to battle a global pandemic. Elderly begin to drop out of Novavax vaccine trial to get Pfizer and Moderna shots |Christopher Rowland |January 19, 2021 |Washington Post 

I think there are millions of others who are in the same predicament. Don’t file a paper 2020 federal tax return if you don’t have to, IRS watchdog warns |Michelle Singletary |January 15, 2021 |Washington Post 

Their solidified friendship is one of the most touching details of the premiere, but it also puts Branson in a tricky predicament. What Downton’s Fashion Really Means |Katie Baker |January 2, 2015 |DAILY BEAST 

Mark Reay knows his predicament is very different from theirs. This Fashion World Darling Is Homeless |Erica Wagner |December 2, 2014 |DAILY BEAST 

Earlier this year, 78-year-old Walter Williams found himself in a similar predicament. What It’s Like to Wake Up Dead |Dr. Anand Veeravagu, MD, Tej Azad |November 21, 2014 |DAILY BEAST 

His predicament eventually become something of a cause célèbre, attracting even the attention of the Princess of Wales. The True Story of ‘The Elephant Man’ |Russell Saunders |November 3, 2014 |DAILY BEAST 

In a way, the roots of this predicament reach back hundreds of years. How Young People Are Destroying Liberty |James Poulos |October 11, 2014 |DAILY BEAST 

“This is a distressing predicament for these young people,” thought Mr. Pickwick, as he dressed himself next morning. The Posthumous Papers of the Pickwick Club, v. 2(of 2) |Charles Dickens 

Finally, his predicament became so awkward that an expression of distress crept into his face. The Homesteader |Oscar Micheaux 

Of course, the smoke-signals, passed along by Dangerfield's chain of guards, were responsible for Matt's predicament. Motor Matt's "Century" Run |Stanley R. Matthews 

Jogging on over the sand, I sat silent, cudgelling my brains for a solution of the disastrous predicament I had gotten into. In Search of the Unknown |Robert W. Chambers 

I went to the Junior, told him my predicament, and he kindly offered to wait for his debt, though the note was overdue. Tom Fairfield's Schooldays |Allen Chapman