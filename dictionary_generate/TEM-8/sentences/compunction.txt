The stars of the Westminster Kennel Club Dog Show, however, have no such compunctions. How to think like a winner (at the Westminster Dog Show) |Sarah Todd |June 14, 2021 |Quartz 

Plus, part of the reason he’s shot so well from deep is that defenses have no compunction about leaving him open. Can Utah’s Wings Hold Their Own Against The Best Of The West? |Jared Dubin |April 21, 2021 |FiveThirtyEight 

Ron and Russell, raised in Los Angeles, tell us that their parents loved bringing them to the movies, and had no compunction about walking in right in the middle. The Sparks Brothers, a Sundance Delight, Tells the Grand Story of This Enduringly Enigmatic Art-Pop Duo |Stephanie Zacharek |February 3, 2021 |Time 

You feel no compunction in removing them from the face of the Earth. Humans Have Gotten Nicer and Better at Making War - Issue 94: Evolving |Steve Paulson |January 6, 2021 |Nautilus 

Salazar, as Win at All Costs reminds us, had no compunction about gaming therapeutic-use-exemption rules to get his runners on medication for the specific purpose of performance enhancement. A New Book on Nike Pulls No Punches |Martin Fritz Huber |October 2, 2020 |Outside Online 

He had been audited when he was out of office, and now he had no compunction about using his power as president. IRS Audits, Benghazi, Sebelius: Obama’s Second Term Is Scandal Heaven |Eleanor Clift |May 14, 2013 |DAILY BEAST 

This is a man who has traveled to Iran and China with no compunction. Hawking's Bad Boycott Timing |Gil Troy |May 13, 2013 |DAILY BEAST 

But we do know that voters who believe Mitt deplores them will have little compunction in not voting for him. Mitt Disowns Rand, Embraces the 100% |Justin Green |October 5, 2012 |DAILY BEAST 

Penn State students, however, have shown no compunction about buying up as many season tickets as possible. Penn State's Economic Fallout: Will the Sandusky Scandal Sink a Whole City? |Matthew Zeitlin |July 17, 2012 |DAILY BEAST 

When I get them alone, I have no compunction about blowing them to bits. Daniel Klaidman on the Mind of a Drone Strike Operator |Daniel Klaidman |June 8, 2012 |DAILY BEAST 

Why should he have compunction—why think about it, when the hour of repayment was so near at hand? Blackwood's Edinburgh Magazine, No. CCCXXXIX. January, 1844. Vol. LV. |Various 

And, after one swift glance at the first letter, Aristide had no compunction in reading. The Joyous Adventures of Aristide Pujol |William J. Locke 

And, therewith, a great tenderness and compunction in this man's heart, and a steady determination to put things right. Marriage la mode |Mrs. Humphry Ward 

Yet this conclusion of the intellect did not prevent the pain of pity and compunction, nor an inconsequent sense of guilt. The Daughters of Danaus |Mona Caird 

I felt in those moments that for every hair of her head I could have killed a man and felt no compunction afterwards. In Accordance with the Evidence |Oliver Onions