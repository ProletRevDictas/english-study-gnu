More than likely he was celebrating from the VIP section, in the second floor rafters overlooking the dance floor. In Texas, Cruz, Perry Crow Over GOP Rout |Tim Mak |November 5, 2014 |DAILY BEAST 

His service as a guard had earned him the right to be the VIP greeter that day. Behind the Scenes With a ‘Site Agent’: The Secret Service’s Hardest Job |Marc Ambinder |October 2, 2014 |DAILY BEAST 

Some websites, like “MedsCorp VIP,” offered membership benefits. Is FedEx America’s No. 1 Drug Dealer? |Abby Haglage |July 29, 2014 |DAILY BEAST 

He began his evening on the fourth floor of the Taino Towers - near 123rd and Lexington Ave - in what was dubbed the VIP room. Charlie Rangel Dances On |Gideon Resnick |June 25, 2014 |DAILY BEAST 

As far as cash-grabs go, the VIP pass is only the tip of the iceberg. Coachella, Oasis For Douchebags and Trust Fund Babies, Should Be Avoided At All Costs |Marlow Stern |April 12, 2014 |DAILY BEAST 

The VIP's thought that the native population should be aware of it. To Choke an Ocean |Jesse F. (Jesse Franklin) Bone 

Goil then excused himself coldly and left for the VIP quarters. Jack of No Trades |Charles Cottrell 

Now that I've hanswered all you vant to know, you von't vip me any more, vill you? The Gold Hunter's Adventures |William H. Thomes 

These exercises executed with vim, vigor, and vip—deep breathing between each set—will take ten to fifteen minutes. Diet and Health |Lulu Hunt Peters 

Chermany cannot be vip, but Ameriga shall down mit her knees go, und Chermany shall says vords dot Ameriga does not like to hear. Dave Darrin and the German Submarines |H. Irving Hancock