A set of ethics — be courteous to other chasers — is also widely embraced within the community. Sky Fall: Meet the Storm Chasers |Sohini Das Gupta |September 19, 2021 |Ozy 

It’s courteous to go fast enough so the next session can head out as soon as possible, but still take it a little easy so your car can cool off. How to read race car flags like a pro |Peter Nelson/Car Bibles |September 13, 2021 |Popular-Science 

Not only is this simply a good business practice, but it’s also just inclusive and courteous. Now is the best time to stitch your search marketing loopholes before 2022 |Kris Jones |August 9, 2021 |Search Engine Watch 

The Captain decreed that correct courteous behavior for Downhill Skier, during the two seconds required to avert collision, is to shout “TRACK!” Who Has the Right of Way on the Skin Track? |Sundog |March 26, 2021 |Outside Online 

He’s handsome, courteous, clever, witty, great at doing impressions of people and has fabulous taste in art, food, and wine. Highsmith at 100: Literary legacy marred by racism |Kathi Wolfe |February 10, 2021 |Washington Blade 

He was courteous, explained the legitimate reason we were briefly pulled over, and then let us continue on our way. Obama Is Right on Race. The Media Is Wrong. |Keli Goff |December 29, 2014 |DAILY BEAST 

Greetings were courteous and warm, requests were focused, translations were careful. Pyongyang Primer: Kenneth Bae Comes Home |Kevin Bleyer |November 15, 2014 |DAILY BEAST 

Deen is not only punctual, but also professionally courteous to his former co-stars. My Bizarre Night With James Deen, Libertarian Porn Star |Emily Shire |November 12, 2014 |DAILY BEAST 

“He was gentle and courteous even though his love of the U.S. had faded over time,” said Bogucki. A Navy Lawyer Cries Foul on Gitmo’s Kafkaesque Legal System |Eleanor Clift |September 26, 2014 |DAILY BEAST 

In the subway train in Japan, when people are not necessarily in a good mood, they will nevertheless be courteous. Vilified Bitcoin Tycoon After Losing $500 Million: My Life Is at Risk |Nathalie-Kyoko Stucky |September 17, 2014 |DAILY BEAST 

Mr. Pontellier had been a rather courteous husband so long as he met a certain tacit submissiveness in his wife. The Awakening and Selected Short Stories |Kate Chopin 

It was from the biggest face that the voice had come, and Dorothy responded with a courteous "Good-morning!" Dorothy at Skyrie |Evelyn Raymond 

Easily accessible to all, courteous and reasonable ever, he was in many respects a model railway manager. Fifty Years of Railway Life in England, Scotland and Ireland |Joseph Tatlow 

I was enraptured—the communication was made in the most courteous manner to the marquis. Blackwood's Edinburgh Magazine, No. CCCXXXIX. January, 1844. Vol. LV. |Various 

Singularly courteous and obliging on all occasions, I, personally, have been much indebted to him for help and advice. Fifty Years of Railway Life in England, Scotland and Ireland |Joseph Tatlow