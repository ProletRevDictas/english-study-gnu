Immanuel Christian School doesn’t just have policies excluding LGBTQ people, but declares in its employment application “homosexual acts and lifestyles are clearly perversion and reprehensible in the sight of God.” Anti-LGBTQ school where Karen Pence teaches obtained $725K in PPP money |Chris Johnson |December 4, 2020 |Washington Blade 

The students implicated in the affair were called in one by one and accused of participating in homosexual activities before being expelled. Honoring victims of the Harvard Secret Court of 1920 |Diego Garcia Blum |November 5, 2020 |Washington Blade 

The confinement somehow helped me to stay away from society that did not tire of making me feel bad about my obvious homosexual condition in much of my youth. A non-binary Cuban artist is born again in Spain |Yariel Valdés González |October 16, 2020 |Washington Blade 

I am talking about the 80s, when things were seen from a very different point of view and to be homosexual in that time was the worst. Cubans share their coming out stories |Tremenda Nota |October 13, 2020 |Washington Blade 

In 1969, homosexual acts were illegal in every state except Illinois. How the New York Media Covered The Stonewall Riots |LGBTQ-Editor |June 23, 2020 |No Straight News 

Homosexual acts come naturally to them, heterosexual acts do not. The Vatican's Same-Sex Synod: The Bishops Hear About Reality. Do They Listen? |Barbie Latza Nadeau |October 12, 2014 |DAILY BEAST 

Okay, but still, “Homosexual persons are called to chastity.” Catholic University’s Harvey Milk Ban Reflects A Church In Transition |Jay Michaelson |October 3, 2014 |DAILY BEAST 

Homosexual conduct between consenting adults is legal in Turkey, but far from accepted. Turkey’s Violent Homophobia |John Beck |July 1, 2013 |DAILY BEAST 

In March 2009, Lively arrived in Uganda to headline the “Seminar on Exposing the Homosexual Agenda.” Ugandan Gay-Rights Group Sues U.S. Pastor |Nina Strochlic |January 27, 2013 |DAILY BEAST 

Stanley Kauffman in the Times complained of “Homosexual Drama and Its Disguises.” How Gossip Became History: Eminent Outlaws by Christopher Bram |Brad Gooch |January 29, 2012 |DAILY BEAST 

Homosexual intimacies between girls are far more often platonic than similar intimacies between boys. The Sexual Life of the Child |Albert Moll 

Homosexual Eros has a different finality than heterosexual Eros. The Civilization of Illiteracy |Mihai Nadin 

Homosexual choice of object is originally more natural to narcism than the heterosexual. A General Introduction to Psychoanalysis |Sigmund Freud 

Homosexual love has played a much greater part in the world's history than is generally believed. The Sexual Question |August Forel 

Homosexual practices everywhere flourish and abound in prisons. Studies in the Psychology of Sex, Volume 2 (of 6) |Havelock Ellis