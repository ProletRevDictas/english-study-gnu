The sun ducked under the hill, and I cooked dinner in the spacious kitchen with the twilight peeking through the open skylight. Tested: Airstream’s New Remote-Work Trailer |mlarsen |August 5, 2021 |Outside Online 

This work established honeybees as models for animal behavior, and showed that the bees use polarized skylight to orient themselves. The Math of Living Things - Issue 102: Hidden Truths |Sidney Perkowitz |June 23, 2021 |Nautilus 

Construction on the building’s skylights continues, but it is expected to open for visitors in June, Feldman said. The National Gallery is back. And it’s done being subtle about its brand and mission. |Peggy McGlone |May 13, 2021 |Washington Post 

A skylight and two windows allow natural light in for added ventilation. The best outdoor tool sheds for all of your storage needs |PopSci Commerce Team |October 14, 2020 |Popular-Science 

In life, José Saramago refused to see his once-rejected book Skylight published. The Lost Novel of Nobel-Winner José Saramago |Charles Shafaieh |January 5, 2015 |DAILY BEAST 

He was not originally so uninhibited, however, as can now be seen in his “lost” novel, Skylight. The Lost Novel of Nobel-Winner José Saramago |Charles Shafaieh |January 5, 2015 |DAILY BEAST 

Every morning an old jackdaw perched on a chimney outside our skylight, and entertained us with his chatter. Fifty Years of Railway Life in England, Scotland and Ireland |Joseph Tatlow 

Beyond the skylight rose the bright brass funnel of the cabin chimney, and the winch, by means of which the lantern was hoisted. The Floating Light of the Goodwin Sands |R.M. Ballantyne 

The studio was lit by two windows on the street and a great, slanting skylight overhead. The Woman Gives |Owen Johnson 

The skylight was open in deference to the first warmth of the spring, as March went out like a lamb. The Woman Gives |Owen Johnson 

And then came a glimpse of thought, of imagination, like the sight of a soaring eagle through a staircase skylight. The New Machiavelli |Herbert George Wells