This song is specifically for the underdogs whose bark is as big as their bite and want to prove they’re number one on the block. Busta Rhymes’ Teaches the Art of Bragging in This Week’s The Playlist |cmurray |December 4, 2020 |Essence.com 

Pine needles and conesThis common needle-bearing tree can provide tea and an edible inner bark. 13 edible plants you can still find in the winter |By Tim MacWelch/Outdoor Life |December 1, 2020 |Popular-Science 

Low-severity embers can ramp up a tree’s response such that it’s better equipped to fight back bark beetles, native parasites than can devastate entire stands of conifers when the trees are already weakened by forces like drought. How we can burn our way to a better future |Ula Chrobak |October 2, 2020 |Popular-Science 

Prior to a jumping worm invasion, the soft layer of decomposing leaves, bark and sticks covering the forest floor might be more than a dozen centimeters thick. Invasive jumping worms damage U.S. soil and threaten forests |Megan Sever |September 29, 2020 |Science News 

Cotton balls, drier lint, curls of birch bark, and even greasy snack chips can turn the small flame of a match into the roaring flame of a campfire. The 10-step guide to survival in an emergency |By Tim MacWelch/Outdoor Life |September 29, 2020 |Popular-Science 

A neighborhood dog had begun to bark, and they were worried about the police coming. The Stacks: The Judas Priest Teen Suicide Trial |Ivan Solotaroff |June 28, 2014 |DAILY BEAST 

The wire is long gone, but a rusted snag remains entombed in the bark. How the Kings of Fracking Double-Crossed Their Way to Riches |ProPublica |March 13, 2014 |DAILY BEAST 

Rock reportedly coined the phrase, “Cows moo, dogs bark, Labour puts up taxes.” British Prime Minister’s Child Porn Adviser Arrested Over Child Porn |Tim Teeman |March 4, 2014 |DAILY BEAST 

“Impossible,” began the other, but was silenced by a sort of bark from Mr. Wilde. Read ‘The King in Yellow,’ the ‘True Detective’ Reference That’s the Key to the Show |Robert W. Chambers |February 20, 2014 |DAILY BEAST 

It all boils down to scratching your name in the bark of a tree. Mel Brooks Is Always Funny and Often Wise in This 1975 Playboy Interview |Alex Belth |February 16, 2014 |DAILY BEAST 

A primitive savage makes a bow and arrow in a day: it takes him a fortnight to make a bark canoe. The Unsolved Riddle of Social Justice |Stephen Leacock 

As I came near the house, the dogs began to bark, just as I discovered my horse tied to a tree. Ramona |Helen Hunt Jackson 

Suddenly the spaniel leapt up with that feverish, spider-like activity of the toy species and began to bark. Dope |Sax Rohmer 

The dog gave a short bark, and looked to the front, as if to say, "Look out—trouble ahead." The Courier of the Ozarks |Byron A. Dunn 

So with a fearful growl, and a bark that might have frightened a lion, Bravo made a leap and a spring after poor little Downy. The Nursery, July 1873, Vol. XIV. No. 1 |Various