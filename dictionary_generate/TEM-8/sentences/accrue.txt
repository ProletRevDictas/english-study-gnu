Any additional time off would come out of his accrued sick and vacation time, he was told. Workers are getting fired and penalized for reporting COVID safety violations |lbelanger225 |October 23, 2020 |Fortune 

It sought, unsuccessfully, to reduce nurses’ accrued vacation time and to cut pension benefits for all employees who didn’t work full time. Investors Extracted $400 Million From a Hospital Chain That Sometimes Couldn’t Pay for Medical Supplies or Gas for Ambulances |by Peter Elkind with Doris Burke |September 30, 2020 |ProPublica 

Over at Palantir, which we have covered extensively the past few weeks, the company is even more of an outlier, with large-contract government sales that accrue over many years. Asana up 39% and Palantir still holding as both direct listings hit the public markets |Danny Crichton |September 30, 2020 |TechCrunch 

The order also does not prevent landlords from charging fees or accruing interest, if those are included under the renter’s lease. Everything to know about Trump’s halt on evictions |Aric Jenkins |September 3, 2020 |Fortune 

Subscription services, however, retained almost three-quarters of the extra viewing they had accrued over lockdown. ‘We’ll get briefs we couldn’t access before’: Inside Channel 4’s push for programmatic advertisers |Seb Joseph |August 11, 2020 |Digiday 

The fines accrue thousands of dollars in interest every week. He Bullies Kids and Calls It News |Brandy Zadrozny |June 26, 2014 |DAILY BEAST 

You have to accrue power, use it in ethical ways, and hope that voters reward you for doing this. Bill de Blasio Mayoral Win Signals Working Families Party Ascendancy |David Freedlander |November 5, 2013 |DAILY BEAST 

Makes your kids want to do their chores, by allowing them to purchase prizes with the points they accrue. The 15 Hottest New Apps at Dublin’s Web Summit |Tom Sykes |October 31, 2013 |DAILY BEAST 

Makes your kids want to do their chores by allowing them to purchase prizes with the points they accrue. The 15 Hottest New Apps at Dublin’s Web Summit |Tom Sykes |October 31, 2013 |DAILY BEAST 

And Blizzard takes a 15% cut of the real-money transactions; the commissions that used to flow to eBay now accrue to them. Diablo 3 Director Regrets Building an In-Game Market |Megan McArdle |March 29, 2013 |DAILY BEAST 

From this time, by the help of these machines, immense and incalculable riches will accrue to the nation. Life of Richard Trevithick, Volume II (of 2) |Francis Trevithick 

Other cases may occur, in which great advantage would accrue, if the principle were once admitted. Decline of Science in England |Charles Babbage 

No possible benefit could accrue to Sylvia from a disclosure of his suspicion that he had borne the letter to her grandfather. A Hoosier Chronicle |Meredith Nicholson 

His excellency also referred to the advantages which would accrue from the establishment of an agricultural society. History of Prince Edward Island |Duncan Campbell 

The only gain that would accrue from his confession would be, he considered, a subjective gain to himself. A Life Sentence |Adeline Sergeant