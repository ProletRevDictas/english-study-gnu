The skull comes from an extinct animal, Crocodylus checchiai. American crocs seem to descend from kin that crossed the Atlantic |Carolyn Wilke |August 25, 2020 |Science News For Students 

Next, the scientists combined eggshell data with what’s known about the family trees of extinct and living egg-laying animals. Early dinosaurs may have laid soft-shelled eggs |Jack J. Lee |August 3, 2020 |Science News For Students 

New analyses of a roughly 7-million-year old skull from the extinct Crocodylus checchiai suggest that crocodiles journeyed from Africa to the Americas millions of years ago, researchers report July 23 in Scientific Reports. An ancient skull hints crocodiles swam from Africa to the Americas |Carolyn Wilke |July 23, 2020 |Science News 

Combining this and other eggshell data with the evolutionary relationships of extinct and living egg-laying animals, the researchers calculated the most likely scenario for dinosaur egg evolution. Fossil discoveries suggest the earliest dinosaurs laid soft-shelled eggs |Jack J. Lee |June 24, 2020 |Science News 

It is not even guaranteed to produce more species, since evolution can occur in a single lineage and this can go extinct at any time. Evolution: Why It Seems to Have a Direction and What to Expect Next |Matthew Wills |June 10, 2020 |Singularity Hub 

In origins, as in Washington politics, moderates are slowly going extinct. What’s Driving America’s Evolution Divide? |Karl W. Giberson |June 22, 2014 |DAILY BEAST 

So while the divas are fewer in number in 2014, they are far from extinct. Mariah Carey Is the Last of the Traditional Divas |Phoebe Robinson |May 29, 2014 |DAILY BEAST 

His decision reflected a quality—largely extinct in show business now—called integrity. Stephen Colbert’s Groveling ‘Late Show’ Debut |Tom Shales |April 23, 2014 |DAILY BEAST 

Yabuki also notes that as tigers go extinct throughout the world, China sees raising them as a good business opportunity. Why Do Chinese Oligarchs Secretly Love Illegal Tiger Meat? |Jake Adelstein |March 31, 2014 |DAILY BEAST 

Wild tigers are rare in China, with some varieties now believed to be extinct. Why Do Chinese Oligarchs Secretly Love Illegal Tiger Meat? |Jake Adelstein |March 31, 2014 |DAILY BEAST 

What course was taken to supply that assembly when any noble family became extinct? Gulliver's Travels |Jonathan Swift 

Her hope persisted until half-past nine: it then began to fade, and, at ten o'clock, was extinct. Hilda Lessways |Arnold Bennett 

The senior branch of the family being thus extinct the whole of the entailed estate had devolved on me. Uncanny Tales |Various 

The forests there are wonderful, and it is there, if anywhere, that the almost extinct Indian lion is still to be found. Uncanny Tales |Various 

The great auk is but a memory; the bittern booms more rarely in our eastern marshes; and now they tell me Brigadiers are extinct. Punch, or the London Charivari, Volume 158, April 28, 1920 |Various