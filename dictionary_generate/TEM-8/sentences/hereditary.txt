Furthermore, by the 14th century BC, the pharaohs created a separate military caste which was basically hereditary in its nature. The Ancient Egyptian Soldiers of the New Kingdom |Dattatreya Mandal |July 6, 2022 |Realm of History 

The term gene was introduced later, in 1909, by the Danish biologist Wilhelm Johannsen to refer to the unit of hereditary material. How we got from Gregor Mendel’s pea plants to modern genetics |Elizabeth Quill |February 7, 2022 |Science News 

They respected the ritual specialists as guardians of ancient texts, the Vedas, and the priests gradually formed a hereditary class, the brahmins. What Ancient Laws Can Teach Us About Holding Autocrats to Account Today |Fernanda Pirie |December 23, 2021 |Time 

The PIEZ01 and PIEZ02 genes that Patapoutian discovered have also been implicated in a number of hereditary diseases involving proprioception. Nobel Prize awarded to researchers who parsed how we feel temperature and touch |Claire Maldarelli |October 4, 2021 |Popular-Science 

Much of this unwritten Constitution had deep roots in hereditary political powers of kings and noblemen. The Story Behind the Declaration of Independence's Most Memorable Line |Akhil Reed Amar |May 7, 2021 |Time 

That surely was the sentiment of more than a few of the hereditary distillers in bourbon country. Hillbilly Heaven: The History of Small-Batch Bourbon |Dane Huckelbridge |March 29, 2014 |DAILY BEAST 

This son has begun thrumming the strings of hereditary determinism, and is finding them holding taut. Michael Hainey and Aleksandar Hemon’s Chicago Dreams |Chris Wallace |March 3, 2013 |DAILY BEAST 

Some naive types might say that this indicates an enormous hereditary factor as an explanation of successful coin-flipping. Just How Wise is the Sage of Omaha? |Megan McArdle |December 6, 2012 |DAILY BEAST 

After 33 years of working in a factory, Nowlin had developed a hereditary eye disease called Retinitis pigmentosa. Powerball Winners on Being Filthy Rich |Caitlin Dickson |November 27, 2012 |DAILY BEAST 

The public ceremony unveiling the wife of the newly anointed hereditary dictator of North Korea was anything but traditional. North Korea IDs Mystery Woman as Kim Jong-Un’s Wife—But Who Is She, Really? |Nate Thayer |July 26, 2012 |DAILY BEAST 

Hereditary legislation in the twentieth century and the most civilized country in the world! Ancestors |Gertrude Atherton 

Membership in the Virginia Council was considered a position of the greatest prestige and was almost an hereditary position. Hallowed Heritage: The Life of Virginia |Dorothy M. Torpey 

Being the hereditary Datto, the inhabitants of the valley generally sympathized with him, at least passively. The Philippine Islands |John Foreman 

This the chapel owes to the residence of the royal family, whose passion and talent for music are hereditary. Journal of a Voyage to Brazil |Maria Graham 

No family history of epilepsy, insanity, nervous or other hereditary disorders in 59 per cent. A Statistical Inquiry Into the Nature and Treatment of Epilepsy |Alexander Hughes Bennett