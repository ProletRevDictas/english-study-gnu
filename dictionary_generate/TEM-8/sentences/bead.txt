Each blanket is made from 100% cotton and contains non-toxic glass beads. Best weighted blanket: Sleep like a baby with our comfy bedding picks |PopSci Commerce Team |February 12, 2021 |Popular-Science 

Pieces in the Wixarikas Collection are especially time consuming, requiring up to a week of work on just the colorful, detailed bead patterns. Someone Please Buy Me This Gorgeous Cocktail Set |Nick Mancall-Bitel |December 30, 2020 |Eater 

Now researchers have demonstrated the bizarre effect for the first time in the laboratory by cooling glass beads as a proxy for the more complex freezing process of water. From Elvis worms to the Milky Way’s edge, these science stories sparked joy in 2020 |Erika Engelhaupt |December 17, 2020 |Science News 

They start their lives as ribbons of linear components, called amino acids, like beads on a string. DeepMind’s AlphaFold Is Close to Solving One of Biology’s Greatest Challenges |Shelly Fan |December 15, 2020 |Singularity Hub 

In his rustic office, a cow tail with bead embroidery and a fez hanging above it like a crown are displayed on one wall. East Africa’s Healers Embrace Modern Medicine to Treat Depression |Charu Kasturi |December 8, 2020 |Ozy 

The wall directly opposite the entrance was covered in colorful glass and bead mosaics. A Little Too Off the Beaten Path in Burma |Katya Cengel |June 2, 2014 |DAILY BEAST 

You can turn it over 17 times in your head and not really get a clear bead on what all that craziness means. The American Prophet of Delusion: Robert Stone in Conversation |David Samuels |November 15, 2013 |DAILY BEAST 

The bead embroidery on the back of her coat said “Revolution.” Ilya Yashin & Ksenia Sobchak, the Russian Opposition’s Romeo & Juliet |Anna Nemtsova |December 17, 2012 |DAILY BEAST 

It also takes years of training to be able to sew, embroider, bead, and otherwise embellish these clothes. Chanel, Armani, and Givenchy Present Their Haute-Couture Collections in Paris |Robin Givhan |July 4, 2012 |DAILY BEAST 

In contrast, Hell on Wheels often has too much happening to get a bead on where the story is going. ‘Hell on Wheels’ Has Found Its Way |Allen Barra |January 6, 2012 |DAILY BEAST 

He seemed to have abandoned himself to a reverie, and to be seeing pleasing visions in the amber bead. The Awakening and Selected Short Stories |Kate Chopin 

When the doctor arrived in the country, Butifer drew a bead on him, in a corner of the forest. Repertory Of The Comedie Humaine, Complete, A -- Z |Anatole Cerfberr and Jules Franois Christophe 

They are very timid, and will not look a stranger in the face, their bead-like eyes constantly shifting. Man And His Ancestor |Charles Morris 

Before he could draw a bead, the rabbit vanished behind a distant scrub oak. Restricted Tool |Malcolm B. Morehart 

This projects some inches from the side of the boat, and the effect of this small bead in throwing off seas is most remarkable. Yachting Vol. 2 |Various.