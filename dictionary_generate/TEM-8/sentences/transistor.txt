Swan took over a troubled giant, as Intel was struggling to perfect new technology to squeeze more transistors onto each processing chip. Intel gets the leader it needs |Aaron Pressman |January 13, 2021 |Fortune 

There are a lot of options to go through, but the effort to find the best computer speakers will be worth it when you’re upgrading from sound that’s transistor-radio quality to audio that can fill your entire home. Best computer speakers: Make music, video chats, and more a whole lot clearer |Jeremy Helligar |January 12, 2021 |Popular-Science 

Putting more transistors on a chip may increase the computing power but also draws more power and complicates the design in other ways. Former Intel and Apple design star Jim Keller joins A.I. chip startup |Aaron Pressman |January 6, 2021 |Fortune 

Digital computers harness the power of hundreds of millions if not billions of transistors that are each fundamentally a simple on-off switch. The Year in Biology |John Rennie |December 23, 2020 |Quanta Magazine 

Memristor-based neuromorphic chips, for example, mimic the brain by putting processing and memory into individual transistor-like components. The Trillion-Transistor Chip That Just Left a Supercomputer in the Dust |Jason Dorrier |November 22, 2020 |Singularity Hub 

People had been carrying around music for several years – transistor radios, the Walkman, etc. From Edison to Jobs |The Daily Beast |September 25, 2014 |DAILY BEAST 

Electronically, that machine is the equivalent of an early transistor radio. Your iPod (Most Likely) Won’t Bring Down the Plane |Clive Irving |October 31, 2013 |DAILY BEAST 

At the Namegabe home, the transistor radio, charged with batteries, was the property of the men. Congo's Anti-Rape Crusader |Delphine Minoui |June 28, 2010 |DAILY BEAST 

The electrical properties of this odd specimen are unusual and interesting and could lead to a new type of transistor. The Atomic Fingerprint |Bernard Keisch 

There was a miniature pocket radio—a transistor radio—on top of the short wave cabinet. Operation Terror |William Fitzgerald Jenkins 

The PDP-3 circuitry is the static type using saturating transistor flip-flops and, for the most part, transistor switch elements. Preliminary Specifications: Programmed Data Processor Model Three (PDP-3) |Digital Equipment Corporation 

You mean them tiny transistor things that feelie actors have stuck in their heads? The Premiere |Richard Sabia 

It was a tiny transistor, an integral part of modern electronic apparatus. The Scarlet Lake Mystery |Harold Leland Goodwin