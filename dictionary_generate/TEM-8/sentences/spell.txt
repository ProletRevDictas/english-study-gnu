He said everything — community health data, facility preparedness, the staffing situation — spells a return to in-person instruction. Alexandria City Public Schools sets date for reopening; Arlington refuses to follow suit |Hannah Natanson |February 5, 2021 |Washington Post 

Thus, many educators reasonably teach a small set of high-frequency, irregularly spelled words as special cases. Is there really a ‘science of reading’ that tells us exactly how to teach kids to read? |Valerie Strauss |January 26, 2021 |Washington Post 

Married for a spell to Ellen Barkin, he’s made a few appearances in the tabloids, too. Gabriel Byrne’s ‘Walking with Ghosts’ is a revelation in unexpected ways |Keith Donohue |January 12, 2021 |Washington Post 

The rise in demand for flexible office space could also spell a future for repurposed storefronts. Welcome to the ground floor: street-level offices for a post-pandemic world |Jessica Davies |January 11, 2021 |Digiday 

I have no doubt Icahn knows all the numbers, but he appears to rely on instincts that tell him, I’ve seen this scenario before, and it always spells trouble. Investing legends Carl Icahn and Jeremy Grantham see a stock market bubble |Shawn Tully |January 8, 2021 |Fortune 

“Then I learned he can't spell and is a manager at a CPK,” she said. My Week on Jewish Tinder |Emily Shire |January 5, 2015 |DAILY BEAST 

After my crying spell stopped, I gritted my teeth, tucked my crutch under my right arm, and turned to my husband. You’re Never ‘Cured’ of an Eating Disorder |Carrie Arnold |December 20, 2014 |DAILY BEAST 

In order to break the spell and bear children, they must collect four items from the mysterious woods. Anna Kendrick on Feminism, #GamerGate, and the Celebrity Hacking Attack |Marlow Stern |November 25, 2014 |DAILY BEAST 

A personal favorite is “C Is For Cookie” for guiding me through a 1994 playground debate over how to spell the word. ‘Sesame Street’ Is Middle-Aged and Awesome |Emily Shire |November 10, 2014 |DAILY BEAST 

Unless Spotify can figure out how to better pay artists and develop exclusive deals, this could easily spell the end for them. Taylor Swift Dumps Spotify, Igniting Turf War Between Spotify and Apple |Dale Eisinger |November 4, 2014 |DAILY BEAST 

Finally he shook himself free from the dreamy spell of the place, and turned his face southward again. Ramona |Helen Hunt Jackson 

"I just happened to be passin' and thought I'd drop in for a spell," he said, with a profound bow to Mary, who arose to greet him. The Soldier of the Valley |Nelson Lloyd 

Her quiet eyes, held by his during the spell that had bound them speechless, did not flinch at the breaking of it. Uncanny Tales |Various 

Aristide prayed that some Thaïs might come along, cast her spell upon him, and induce him to wink. The Joyous Adventures of Aristide Pujol |William J. Locke 

After the first short spell of shelling our men fixed bayonets and lifted them high above the parapet. Gallipoli Diary, Volume I |Ian Hamilton