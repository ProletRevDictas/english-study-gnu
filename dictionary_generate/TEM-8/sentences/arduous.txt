A harsh lockdown, one of the strictest in the world, created a humanitarian crisis of its own, pushing millions of migrant workers to take arduous journeys back home on foot. Has the Covid-19 pandemic effectively ended in India? |Manavi Kapur |February 18, 2021 |Quartz 

When changes concern the matter of how we mark off our identities, though, any alteration would be extremely arduous and require more than education. Why a Universal Society Is Unattainable - Issue 95: Escape |Mark W. Moffett |January 14, 2021 |Nautilus 

Smith played in his first game since his devastating injury, completing an arduous comeback and withstanding a brutal sack from Los Angeles Rams defensive tackle Aaron Donald. Washington football timeline: From Ron Rivera hiring to playoff exit |Sam Fortier |January 12, 2021 |Washington Post 

First, we didn’t try to stand up an entirely new educational program on our own, which almost certainly would be a long, arduous process that may not meet the differing needs of academic institutions around the world. There’s no better time than now to build a better pipeline for women in tech |Andrew Nusca |January 11, 2021 |Fortune 

After seventeen surgeries, a life-threatening infection, a lost season and months of arduous rehab, Smith returned to the field in October. A timeline of Alex Smith’s remarkable comeback — from life-threatening injury to the playoffs |Scott Allen |January 6, 2021 |Washington Post 

It's slow and arduous and takes great concentration under the best of circumstances. Alfred Hitchcock’s Fade to Black: The Great Director’s Final Days |David Freeman |December 13, 2014 |DAILY BEAST 

Bailey, who is also dating the director, said working on a Leigh production was incredibly arduous. Mike Leigh Is the Master Filmmaker Who Hates Hollywood |Nico Hines |October 14, 2014 |DAILY BEAST 

Even more striking are the courteous and collegial manners displayed, even during the arduous filibuster in the Senate. How a Dream Became a Law: Passing the Civil Rights Act of 1964 |Wendy Smith |March 31, 2014 |DAILY BEAST 

Once the budget has been creatively handled, a director still faces the arduous task of casting. The Art of Smutty Spoofs: Porn Parodies Aren’t a Joke Anymore |Aurora Snow |March 15, 2014 |DAILY BEAST 

The road to the Olympics is already long and arduous enough. India’s Olympic Mess: Why You Won’t See the Nation’s Flag in Sochi |Kevin Fixler |February 6, 2014 |DAILY BEAST 

Indeed, 'we have laid upon him various arduous tasks touching the state of the country, and especially its tranquillity.' King Robert the Bruce |A. F. Murison 

But Samuel Adams, who thought "nothing should be despaired of," took upon himself the performance of this arduous task. The Eve of the Revolution |Carl Becker 

Sir Edward Bruce, after an arduous struggle, had taken a firm grip of Galloway by the end of 1308. King Robert the Bruce |A. F. Murison 

Nature seems still to wish to keep the young and blushing girl apart from that connection which entails grave and arduous duties. The Ladies' Book of Etiquette, and Manual of Politeness |Florence Hartley 

Though Weston was more or less accustomed to the work, he found the first few hours sufficiently arduous. The Gold Trail |Harold Bindloss