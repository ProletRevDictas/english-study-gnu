It’s time for the courageous few to step forward and tell your stories so we can speak truth to power and bring about justice for all. ‘Real change will happen when there’s an eight-figure problem for agencies’: Confessions of a creative strategist on taking action on diversity issue |Seb Joseph |October 13, 2020 |Digiday 

As we shared on Friday, we’ll close out Tuesday’s programming with a new conversation about courageous leadership with Meghan, The Duchess of Sussex. Inside the Fortune MPW Next Gen Summit |ehinchliffe |October 12, 2020 |Fortune 

“There is great hope in America’s future if we’re courageous enough to confront the past,” he adds. ‘Antebellum’ tackles the past head on in an effort to ‘move forward’ |radmarya |September 17, 2020 |Fortune 

Since our nation’s founding, so many courageous Americans have risked so much on buses, bridges, and battlefields so that we might enjoy our democratic freedoms, and so that we might continue the work of making our union more perfect. Why CEOs must take action on democracy and election integrity—and how they can do it |matthewheimer |August 27, 2020 |Fortune 

“At a time when people across the nation are calling for a fairer, more just society, we must empower and equip students and educators to have these courageous conversations in the classroom,” Thurmond said. As School Resumes, Students Bring Racial Justice Push to the Classroom |Kayla Jimenez |August 18, 2020 |Voice of San Diego 

This courageous act earned him a late-night knock on the door with orders for Serna to vamos from Cuba. Obama’s One Hand Clap With Castro |Doug McIntyre |December 24, 2014 |DAILY BEAST 

I still wonder what my wife thinks about her big courageous man coming home shaken, seeing my tears for the first time. The Day I Used Eric Garner’s Voice |Joshua DuBois |December 5, 2014 |DAILY BEAST 

That was a much harder and more courageous decision than is recognized by those who oppose this opera. Rudy Giuliani: Why I Protested ‘The Death of Klinghoffer’ |Rudy Giuliani |October 20, 2014 |DAILY BEAST 

On Nov. 1, when Brittany ends her short but productive life, I will be thanking God for her remarkable and courageous example. On Her Own Terms: Why Brittany Maynard Has Chosen to Die |Gene Robinson |October 12, 2014 |DAILY BEAST 

The men who killed these courageous workers committed a heinous crime. The Fear That Killed Eight Ebola Workers |Abby Haglage |September 20, 2014 |DAILY BEAST 

She chatted freely, discoursed on almost every topic, and during it all he saw what a wonderfully courageous woman she was. The Homesteader |Oscar Micheaux 

None knew better than Victoria the value and rarity of a free and courageous soul. Ancestors |Gertrude Atherton 

The Virginia Assembly rewarded Pharaoh for his courageous act by giving him complete freedom. Hallowed Heritage: The Life of Virginia |Dorothy M. Torpey 

It is a true saying that, if there were no dames nor damsels in the world, men would be less courageous. Child Life In Town And Country |Anatole France 

He was a man of eminently fair character, upright, courageous and independent. The History of England from the Accession of James II. |Thomas Babington Macaulay