With no dialogue, the narrative is minimalist and focused on survival, though there are fascinating connections between the monstrous world and the characters within it. ‘Little Nightmares II’ made me dread every moment. And I loved it. |Elise Favis |February 9, 2021 |Washington Post 

Two recent offerings transport readers into fascinating settings — and the first, in particular, features appealing characters, who are the true draw of any well-told tale. Two historical mystery novels plunge readers into the past while keeping them guessing |Clare McHugh |February 8, 2021 |Washington Post 

That will be a fascinating area of inquiry at next week’s hearings. Bitcoin, stocks and crude take off as the markets brace for a wave of stimulus checks |Bernhard Warner |February 8, 2021 |Fortune 

He’s so fascinating that Steve Levitt brought him back for a second conversation. Yul Kwon (Part 2): “Hey, Do You Have Any Bright Ideas?” (People I (Mostly) Admire, Ep. 13) |Steven D. Levitt |February 6, 2021 |Freakonomics 

To me, that’s where you get to people’s relationships playing out in a really fascinating way. How Gideon the Ninth author Tamsyn Muir queers the space opera |Constance Grady |February 5, 2021 |Vox 

One example would be how fascinating it must be to be Martin Scorsese and have an Oscar at home for The Departed. Coffee Talk with Ethan Hawke: On ‘Boyhood,’ Jennifer Lawrence, and Bill Clinton’s Urinal Exchange |Marlow Stern |December 27, 2014 |DAILY BEAST 

Lawler is more interested in the more fascinating story of how the chicken spread. The History of the Chicken: How This Humble Bird Saved Humanity |William O’Connor |December 27, 2014 |DAILY BEAST 

But the history of the church, which emerged in the late '60s, is far more complicated—and fascinating. The Daily Beast’s Best Longreads, Dec 15-21, 2014 |William Boot |December 21, 2014 |DAILY BEAST 

Something fascinating is going on in the gravitational dance of galaxies, from watching the slow twirls of the dancers. The Black Hole Tango |Matthew R. Francis |November 24, 2014 |DAILY BEAST 

He was immediately sold on the quality of the screenplays and the fascinating story. Charles Dance on Tywin Lannister’s S5 Return, A ‘Game of Thrones’ Movie,’ and Sexy Peter Dinklage |Marlow Stern |November 18, 2014 |DAILY BEAST 

Still, gambling seemed to be made particularly fascinating here, and he wanted to be fascinated, wanted it badly. Rosemary in Search of a Father |C. N. Williamson 

Jean clung to his English nurse, who played the fascinating game of pretending to eat his hand. The Joyous Adventures of Aristide Pujol |William J. Locke 

This other by what seems a congenial activity, fascinating as a game of chess, acquires uncounted millions. The Unsolved Riddle of Social Justice |Stephen Leacock 

There are any number of these latter in Hamburg, and you have no idea how fascinating many of them are—so handsome and so bright. Music-Study in Germany |Amy Fay 

The fond parent made the sprightly and fascinating child his daily companion. Madame Roland, Makers of History |John S. C. Abbott