The state quadrupled eligibility for the vaccine last month and began dispersing its limited number of doses among a broader base of providers. Maryland Democrats call for ‘course correction’ amid regionwide frustration over vaccine rollout |Erin Cox, Rebecca Tan, Antonio Olivo |February 3, 2021 |Washington Post 

Outside the court, hundreds of Navalny’s supporters crowded the sidewalks, two days after riot police used batons and stun guns to violently disperse protests in dozens of cities, arresting a record 5,000 people, including dozens of journalists. Russian opposition leader Navalny ordered jailed, calls on supporters to keep pressure on Putin |Isabelle Khurshudyan, Robyn Dixon |February 2, 2021 |Washington Post 

The state dispersed $266 million to districts to help pay for some expenses and issued public health guidance to help schools plan. In Connecticut, Miguel Cardona led a full-court press for schools to reopen |Laura Meckler, Nick Anderson |February 2, 2021 |Washington Post 

Then maybe you can disperse the photographs to different family members. A thrift shop photo album is full of mystery photos of a District family |John Kelly |January 27, 2021 |Washington Post 

Those chemicals caused the plastic molecules that were dispersed in the liquids to bunch together into solid clumps that could be fished out. Chemists are reimagining recycling to keep plastics out of landfills |Maria Temming |January 27, 2021 |Science News 

At about 11 p.m. State Police started flying a helicopter over the scene, ordering the crowds to disperse. Frat Culture Clashes With Riot Police at Keene, N.H., Pumpkin Festival |Melanie Plenda |October 19, 2014 |DAILY BEAST 

Two officers, their badge numbers covered by black tape, watch as guests disperse from a house party they have recently disrupted. A Brief History of the Phrase 'F*ck the Police' |Rich Goldstein |August 23, 2014 |DAILY BEAST 

This is a problem, since no traffic police can identify any of the trucks if they start to disperse once they enter Ukraine. Putin’s “Humanitarian” Convoy Nears Ukraine, APCs Cross in Secret |Anna Nemtsova |August 14, 2014 |DAILY BEAST 

When a big cache of weapons is inbound, rival outfits often gang together to disperse the load among their safe houses. On the Contraband Trail With Libya’s Gun Smugglers |Peter Schwartzstein |June 16, 2014 |DAILY BEAST 

“Instead of car tires, concrete blocks are placed there now, and nobody intends to disperse,” the minister said. Reality Check in Ukraine |Jamie Dettmer |April 27, 2014 |DAILY BEAST 

When the last scarlo is burned out a funeral march is played and all disperse to their homes. The Pit Town Coronet, Volume I (of 3) |Charles James Wills 

So the clouded day broke sullenly, with gusts of warm rain and red gleams of a sun striving to disperse the mists. The Red Year |Louis Tracy 

During the night they disperse, and take up their abode on surrounding farms as peaceful tillers of the soil. Campaign Pictures of the War in South Africa (1899-1900) |A. G. Hales 

Plants are the accumulators of the power which animals distribute and disperse. English: Composition and Literature |W. F. (William Franklin) Webster 

When others gather, do thou disperse; when others disperse, gather. Hebraic Literature; Translations from the Talmud, Midrashim and Kabbala |Various