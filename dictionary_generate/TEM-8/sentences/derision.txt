This is wrong, and so “solutionism” has been a term of derision. Why I’m a proud solutionist |Jason Crawford |July 13, 2021 |MIT Technology Review 

Anyone who tries to be a hard seltzer snob will richly deserve the derision they receive. Sorry, Snobs. Hard Seltzer and Canned Cocktails Are Blowing Up |Lew Bryson |May 7, 2021 |The Daily Beast 

Words of derision have come from random people on social media or the street, but also from the nation’s highest office. Democrats link Atlanta massacre to anti-Asian rhetoric during pandemic |Cleve Wootson, Marianna Sotomayor |March 17, 2021 |Washington Post 

Some of these staffers gave me looks of derision, but slowly complied. Rep. Watson Coleman: I’m 75. I had cancer. I got covid-19 because my GOP colleagues dismiss facts. |Bonnie Watson Coleman |January 12, 2021 |Washington Post 

So when Republicans start talking about deficits, it should be greeted with derision, contempt and outrage — not over their hypocrisy, but because of what they’re trying to do to the country. How Democrats should wage war on coming GOP obstructionism |Paul Waldman |November 30, 2020 |Washington Post 

Foss occasionally supplied pulpits in Baltimore and its suburbs, to the derision of the Herald agnostics. The Stacks: H.L. Mencken on the 1904 Baltimore Fire |H.L. Mencken |October 4, 2014 |DAILY BEAST 

And the omission or derision of dads in the parent (aka “mommy”) blogosphere is a perennial pet peeve. Move Over, Ladies: Dove Does Dads |Andy Hinds |June 17, 2014 |DAILY BEAST 

This version is still being greeted with derision and genuine concern by various parents who oppose mandatory vaccination. Colorado’s Anti-Anti-Vaxxer Bill Gets Watered Down |Kent Sepkowitz |April 23, 2014 |DAILY BEAST 

When he gave interviews to the press, he was often quoted talking about his native country with derision. Why Are All of Johnny Depp’s Movies Bombing at the Box Office? |Tricia Romano |April 21, 2014 |DAILY BEAST 

Most objects of Internet derision only remain interesting for a day or two, tops. ‘American Blogger’ Is So Bad, You’ll Wish It Was a Spoof |Andy Hinds |April 16, 2014 |DAILY BEAST 

The whole crowd shouted in derision, and Jones, in anger, fired every shot in his revolver before they could stop him. The Courier of the Ozarks |Byron A. Dunn 

Count Romanoff shrugged his shoulders, and a smile of derision and contempt passed over his features. The Everlasting Arms |Joseph Hocking 

At those words of his the men interrogating him laughed in derision, declaring it to be a very elegant excuse. The Doctor of Pimlico |William Le Queux 

Hence this Christian emblem became the object of scoffing and derision by the persecuting heathen. The Catacombs of Rome |William Henry Withrow 

I recalled patches of the bright dreams filling my poor noodle when I was riding to meet her, and I smiled in derision at myself. A Virginia Scout |Hugh Pendexter