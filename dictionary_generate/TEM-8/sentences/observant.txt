Her brother Shlomo, 24, is preparing for law school, dipping a toe into dating and struggling over how observant he ultimately wants to be. Netflix’s My Unorthodox Life Is More Bravo Docusoap Than Real-Life Unorthodox |Judy Berman |July 14, 2021 |Time 

Changes are happening everywhere, and so we need them to be observant and not be distracted. With a focus on equity, Everett Lott takes helm of agency overseeing how people get around D.C. |Luz Lazo |July 1, 2021 |Washington Post 

Even a moderately observant UFO monitoring the United States would have noticed, in late March of 2020, that something was seriously amiss. The Great American Reopening, in 5 Charts |Chris Wilson |June 9, 2021 |Time 

Frazes considers herself culturally Jewish but not particularly observant. On Clubhouse, a Global Jewish Community Takes Root |Raisa Bruner |May 6, 2021 |Time 

They realized that for the second year in a row, the pandemic would disrupt the usual gathering of families and friends for the seder, the ritual dinner in which observants re-tell the story of the Jewish exodus from Egypt. A voice game boom is giving kids a break from screen time |Tanya Basu |March 27, 2021 |MIT Technology Review 

The Turkish history of imposed Jacobin Secularism ended up creating virtual segregation against observant Muslims. Turkey’s Struggle for Checks and Balances |Husain Haqqani |January 3, 2014 |DAILY BEAST 

Really quippy, relevant, observant, and, daresay, relatable dialogue. ‘Girls’ Season 3 Trailer Debuts. Is It the Most Relatable Yet? |Kevin Fallon |November 22, 2013 |DAILY BEAST 

Kamala's character will struggle to exist in her observant Muslim family. Meet the Muslim Ms. Marvel: Kamala Khan’s Fight Against Stereotypes |Rula Jebreal |November 8, 2013 |DAILY BEAST 

I am a proud Israeli and a religiously observant Jew and a functional pacifist. What's Wrong With the Discourse About Throwing Rocks? |Moriel Rothman |August 9, 2013 |DAILY BEAST 

Even in the most ridiculous moments, an observant, compassionate intelligence shines. Book Bag: Rosecrans Baldwin’s Favorite Celebrity Memoirs |Rosecrans Baldwin |June 25, 2013 |DAILY BEAST 

This man does not appear at all put out by Mr. Arden's observant presence, nor even conscious of it. Checkmate |Joseph Sheridan Le Fanu 

Her youthful vanity had its way in a mind too speculative, intelligent, observant, merely to be shocked. Ancestors |Gertrude Atherton 

Mr. Pontellier, who was observant about such things, noticed it, as he served the soup and handed it to the boy in waiting. The Awakening and Selected Short Stories |Kate Chopin 

Had she, so observant, so discerning in her fastidious taste—had she failed to notice the small detail too? The Wave |Algernon Blackwood 

Whereupon the observant Seton saw a quick change take place in the girl's expression. Dope |Sax Rohmer