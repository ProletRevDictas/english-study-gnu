Left-wing populists such as Hugo Chávez in Venezuela centralized many aspects of the national economy, and other reforms ran aground because of labor opposition, corruption and other deep-rooted institutional problems. John Williamson, economist who devised ‘Washington Consensus’ model of reform, dies at 83 |Matt Schudel |April 15, 2021 |Washington Post 

This mammoth symbol of the hubris of capitalism, ran aground by a windy day — like a modern-day Titanic, if all the passengers were giant metal boxes, no one died, and everyone was on Twitter. The fallout from the Suez Canal ship is coming to a store near you |Hilary George-Parkin |April 2, 2021 |Vox 

As a ship’s captain, I almost went aground in the Great Bitter Lake, as the Suez is called, after a couple of bad navigational decisions on my part, but, fortunately, my navigator saved my career with some good advice. The Blocked Suez Canal Isn't the Only Waterway the World Should Be Worried About |James Stavridis |March 29, 2021 |Time 

The giant vessel is afloat again after running aground in the Suez Canal last week, but it’s unclear how quickly the waterway will be completely unclogged. Ever Given budges, Chauvin trial opens, space wine |Amanda Shendruk |March 29, 2021 |Quartz 

Typically, Suez-based pilots guide the ship through the narrow passage, and the management company has said that two pilots were on board when the boat ran aground. Piracy fears mount as ships take long way around Africa to avoid blocked Suez Canal |Sudarsan Raghavan, Antonia Noori Farzan |March 26, 2021 |Washington Post 

I have covered the Costa Concordia since it ran aground in 2012, and I have covered many migrant shipwrecks in Italy, too. The Racism of Disaster Coverage |Barbie Latza Nadeau |July 25, 2014 |DAILY BEAST 

In May 1596, his expedition ran aground on the northern edge of Nova Zembla, and his ship was destroyed by moving glaciers. Pale Fire and the Cold War: Redefining Vladimir Nabokov’s Masterpiece |Michael Weiss |October 13, 2013 |DAILY BEAST 

Those who like to point out consistent themes also run aground. The Wonderful ‘Hemingway & Gellhorn:’ Nicole Kidman, Clive Owen, and the HBO Movie |Allen Barra |May 28, 2012 |DAILY BEAST 

Hillary Clinton's campaign ran aground on the shallow shoals of "electability," and now Romney's yacht has done the same. Romney’s Dixie Debacle in Mississippi and Alabama Primaries |Paul Begala |March 14, 2012 |DAILY BEAST 

Just weeks after the Costa Concordia ran aground, a liner with the same company stalled out in the pirate-infested Indian Ocean. Costa Allegra Stranded: Another Cruise From Hell |Barbie Latza Nadeau |February 28, 2012 |DAILY BEAST 

The big sloop, hard aground and full of iron ballast, was not a thing to be moved easily. The Rival Campers |Ruel Perley Smith 

Some of the wherrymen will say that they could not put their craft aground if they would while sailing sideways along the mud. Yachting Vol. 2 |Various. 

She also got aground on a mud bank near the Jersey shore and at noon blew up. Elsie's Vacation and After Events |Martha Finley 

That may be because some one ran aground sometime on the sand-bar off the end, and thought it deceitful. The Belted Seas |Arthur Colton 

On June 9, while engaged in a chase, the Gaspee ran aground, and on the night of the 10th was boarded by eight boat-loads of men. The Political History of England - Vol. X. |William Hunt