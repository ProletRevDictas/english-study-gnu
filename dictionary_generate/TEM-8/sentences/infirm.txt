Conservatorships are supposed to be reserved for seriously infirm or elderly people with dementia. The Investigative Reporting Behind America’s Obsession With Britney Spears’ Conservatorship |by Robin Fields |July 13, 2021 |ProPublica 

Petrie prides himself on being the youngest and least infirm among them, but they all share the predicament of having little purpose left in life and no place else to go. With ‘Antiquities,’ Cynthia Ozick is as vibrant on the page as ever |Diane Cole |April 16, 2021 |Washington Post 

Such sores can develop when an infirm person remains in the same position for too long in a bed or wheelchair. The Nursing Home Didn’t Send Her to the Hospital, and She Died |by Sean Campbell |January 8, 2021 |ProPublica 

Coronavirus cases and deaths in Virginia, Maryland and the DistrictThe early assumption that the virus would devastate only the elderly and the infirm has unraveled over eight months. More than 10,000 people in D.C., Maryland and Virginia have died of covid-19 |Rebecca Tan |December 10, 2020 |Washington Post 

We have great-grandmothers in their nineties, we have parents and in-laws who are in their seventies and medically a little more infirm—it just didn’t make sense to bring lots of people together in the sort of way that we would usually do. How to plan a COVID-safe Thanksgiving, week-by-week |Kate Baggaley |October 30, 2020 |Popular-Science 

The next evening, Romero was saying mass in the chapel at the hospice where he lived in a tiny room near the infirm and the dying. Why Pope Francis Wants to Declare Murdered Archbishop Romero a Saint |Christopher Dickey |August 24, 2014 |DAILY BEAST 

Was the infirm old soldier, perhaps, taking Obama to task for the scandals in the U.S. Department of Veterans Affairs? What the D-Day Veteran Told Obama at the 70th Anniversary Commemoration |Christopher Dickey |June 6, 2014 |DAILY BEAST 

Are we unfairly neglecting the up-and-coming in favor of the old and infirm? Do Blues Musicians Need to be Really, Really Old? |Ted Gioia |September 22, 2013 |DAILY BEAST 

This created a good incentive for the other justices to lobby the infirm one to step down. Justice Stevens Should Quit Now |Adam Winkler |April 5, 2010 |DAILY BEAST 

I get sick when I hear of the charities obliterated and the old and infirm investors who are left with nothing. I Made the List! |Burt Ross |February 5, 2009 |DAILY BEAST 

I do not intend to vex or grieve you by any conduct of mine; nor do I mean to leave you, now you are both infirm and old. The World Before Them |Susanna Moodie 

In addition there were several buildings devoted to the care of the aged, the infirm, and the sick. Our Little Korean Cousin |H. Lee M. Pike 

Governmental care of the unemployed, the infant and the infirm, sounds like a chapter in socialism. The Unsolved Riddle of Social Justice |Stephen Leacock 

When that did not avail, its use was limited to feasts, banquets and sacrifices, and to guests and infirm old age. The Art of Drinking |Georg Gottfried Gervinus 

It is probable that his infirm health and his isolated position were his protection. The History of England from the Accession of James II. |Thomas Babington Macaulay