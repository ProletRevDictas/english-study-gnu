The only way such changes would occur is if Congress passes a law to forgive the collection of payroll taxes deferred under the order—a move supported by both the President and the signatories of Tuesday’s letter. Business groups don’t expect to implement Trump’s ‘unworkable’ payroll order |Jeff |August 18, 2020 |Fortune 

An HHS spokesperson in response to a Washington Blade inquiry on enforcement deferred the Justice Department, which didn’t respond to a request to comment. Judge blocks Trump admin from enforcing anti-trans health care rule |Chris Johnson |August 17, 2020 |Washington Blade 

If the number of deferred applicants who queue up to join college in 2021 is significantly larger than usual, that cuts into the number of seats available for new high school graduates. Is School Out Forever? |Daniel Malloy |August 9, 2020 |Ozy 

That dreamy Mediterranean breeze will have to remain a dream deferred. The (Deferred) Class of 2020 |Sandya Kola |August 9, 2020 |Ozy 

When sports initially went off air in March, most advertisers deferred the money they had planned to spend on TV sporting events until major sports returned. Shut out of Fire TV and Roku, Peacock is the latest example of the arrival power moves to streaming |Tim Peterson |July 15, 2020 |Digiday 

Or will we simply see more senseless bloodshed and another generation of Palestinians defer their dreams of a homeland? In the Middle East, the Two-State Solution Is Dead |Dean Obeidallah |January 2, 2015 |DAILY BEAST 

Owens also accused her of making far more than she claims to need in order to defer the costs of her $58,000 tuition. Duke's Freshman Porn Starlet Isn't Ashamed—and She Shouldn't Be |Emily Shire |February 24, 2014 |DAILY BEAST 

So did Rush simply defer to the Pontiff when he was speaking about caring for the poor and helping those in need. How to Be Loved Like Rush Limbaugh, the 9th Most Admired Person in the US |Dean Obeidallah |January 15, 2014 |DAILY BEAST 

The president respects him and has been known to defer to him. Afghan Elections: The Warlords Are Back |Ron Moreau, Sami Yousafzai |October 16, 2013 |DAILY BEAST 

Messina asked to defer the conversation until after the midterm elections of 2010. No Drama Obama’s Dramatic 2012 Reelection Campaign |Richard Wolffe |September 12, 2013 |DAILY BEAST 

When thou vowest a vow unto God, defer not to pay it; for he hath no pleasure in fools: pay that which thou hast vowed. The Ordinance of Covenanting |John Cunningham 

Lady Engleton received the impression that Mrs. Temperley was too sure of her own judgment to defer even to the wisest. The Daughters of Danaus |Mona Caird 

The war, which then existed between the Persians and the Turks, could not make him defer the execution of his pious enterprise. Superstition In All Ages (1732) |Jean Meslier 

The answer to my invitation was that much as she would love to see me we should have to defer our meeting to some other time. Ways of War and Peace |Delia Austrian 

But as my marriage will lead me into far different scenes, I shall, if you please, defer them till some other evening. The Adopted Daughter |Elizabeth Sandham