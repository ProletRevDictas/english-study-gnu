By mixing cultural foodways with modern sensibilities, we can feel good about having a sustainable holiday feast. Your guide to cooking a sustainable holiday meal |By Allie Wist/Saveur |December 17, 2020 |Popular-Science 

I was just, my sensibilities are just offended by its very existence. Podcast: Facial recognition is quietly being used to control access to housing and social services |Tate Ryan-Mosley |December 2, 2020 |MIT Technology Review 

Vallely said the original philanthropists, such as 18th-century prison reformer John Howard, functioned under the “cult of sensibility.” After growing up poor, a Premier League star corners U.K. into feeding its hungry |Amos Barshad |November 30, 2020 |Washington Post 

Salt, Fat, Acid, Heat does just that—it teaches you how to cook, the science of what makes food taste good, and how to gain your own flavor sensibilities and confidence in the kitchen. Gifts for people who are learning how to cook |Sara Kiley Watson |November 24, 2020 |Popular-Science 

It helps me have a sensibility,This is how I feel my ancestry speaks, through food and especially through cacao. Meet Four Craft Chocolate Makers Decolonizing the Industry |Jinji Fraser |October 22, 2020 |Eater 

He stayed up all night, looking at the streets he had biked around as a kid with a whole new sensibility. DJ Spooky Wants You To Question Everything You Know About Music, Technology, and Philosophy |Oliver Jones |December 27, 2014 |DAILY BEAST 

Lyricist E. Y. “Yip” Harburg was as provocative as Hammerstein, though with a much less earnest, more whimsical sensibility. When Broadway Musicals Were Dark And Subversive |Laurence Maslon |December 16, 2014 |DAILY BEAST 

Hitchcock's sensibility was being shaped by the German Expressionist masters. Alfred Hitchcock’s Fade to Black: The Great Director’s Final Days |David Freeman |December 13, 2014 |DAILY BEAST 

I do feel there is a gay sensibility in everything I do, including the Twilight movies. Can Condon's Freak Show Win Broadway? |Tim Teeman |November 18, 2014 |DAILY BEAST 

As he debuts on Broadway, he talks Beyoncé, Kristen Stewart, Benedict Cumberbatch, and the ‘gay sensibility’ in all he does. Can Condon's Freak Show Win Broadway? |Tim Teeman |November 18, 2014 |DAILY BEAST 

It represents an engaging personality, in which vivacity and sensibility are distinctly indicated. Women in the fine arts, from the Seventh Century B.C. to the Twentieth Century A.D. |Clara Erskine Clement 

No one with even an ordinary share of sensibility can witness a ceremony involving such consequences without the deepest emotion. Madame Roland, Makers of History |John S. C. Abbott 

She was delighted with these indications of gratitude and sensibility on the part of the unenlightened and lowly peasantry. Madame Roland, Makers of History |John S. C. Abbott 

What an agitation, and at the same time what an unhealthy stimulus to his over-sensibility! The Life &amp; Letters of Peter Ilich Tchaikovsky |Modeste Tchaikovsky 

In this change of attitude his artistic sensibility unquestionably played a part. The Life &amp; Letters of Peter Ilich Tchaikovsky |Modeste Tchaikovsky