It took nearly 300 years for Peruvians to achieve independence, and longer for indigenous Andean peoples’ population and traditional husbandry practices to resume. Llamas Are Having A Moment In The US, But They’ve Been Icons In South America For Millennia |LGBTQ-Editor |December 19, 2020 |No Straight News 

Maybe no one will be the “husband” (as in, animal husbandry) and no one the chattel. Were Christians Right About Gay Marriage All Along? |Jay Michaelson |May 27, 2014 |DAILY BEAST 

What does that have to do with good animal husbandry and biodiversity? Whose Elephant Is It? |Winston Ross |December 9, 2012 |DAILY BEAST 

They are experienced with the grim side of husbandry, but they are not inured to it. ‘Luck’ Runs Out: If Horses Die While Cameras Roll, You Must Quit |Max Watman |March 16, 2012 |DAILY BEAST 

Maybe some of my kind have taken on similar 'husbandry' projects. The Extinction Parade: An Original Zombie Story by Max Brooks |Max Brooks |January 14, 2011 |DAILY BEAST 

I'd only just thought of the Borneo-New Guinea option and so those details seemed even more trivial than human husbandry. The Extinction Parade: An Original Zombie Story by Max Brooks |Max Brooks |January 14, 2011 |DAILY BEAST 

When these covenants were made, Lysias went to the king, and the Jews gave themselves to husbandry. The Bible, Douay-Rheims Version |Various 

He thought also that the duty on windows in farm-houses, and on horses used in husbandry, should be taken off entirely. The History of England in Three Volumes, Vol.III. |E. Farr and E. H. Nolan 

John Milton was a noble poet, but he was not a safe guide in matters pertaining to animal husbandry. The Red Cow and Her Friends |Peter McArthur 

They were unfit for the mere routine of husbandry, and unprovided with funds for working their farms. Landholding In England |Joseph Fisher 

I am hard at work, studying spade husbandry, inspectors' reports of industrial schools, &c. I am glad you are all so happy. Life of John Coleridge Patteson |Charlotte M. Yonge