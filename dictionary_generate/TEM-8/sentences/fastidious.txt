She vacuums my carpet, and I asked her why she was so fastidious about vacuuming every nook and cranny in the house. Hints From Heloise: Longtime high school teacher sees troubling trend |Heloise Heloise |October 18, 2021 |Washington Post 

As she builds a suitable existence, marrying a fastidious Oxford don and having babies, Fanny’s yearning for her cousin’s outsize presence gnaws at what is supposed to be domestic bliss. Amazon’s Divine Period Romance The Pursuit of Love Gives Classic Social Satire a Modern Twist |Judy Berman |July 30, 2021 |Time 

All three are fastidious in not appearing to be party stooges. Why Kyrsten Sinema Could Control the Future of the Filibuster |Philip Elliott |June 4, 2021 |Time 

The novel is fast-paced, but González is fastidious in tying together every character and almost-missable detail by the end. 3 Beloved Nature Books You Probably Haven’t Read |Erin Berger |March 25, 2021 |Outside Online 

Similarly, as Chattman points out, you should be fastidious about making sure your bowls are completely clear of oil or other grease, as those fats can cause the same issue. How to separate eggs without the stress or mess |Becky Krystal |February 8, 2021 |Washington Post 

Very rarely, though, that fastidious and precise pulse deteriorates into a disorganized scramble. Heart Attack 101: What May Have Killed James Gandolfini |Kent Sepkowitz |June 20, 2013 |DAILY BEAST 

Jeffries is apparently a frequent flyer as well as a fastidious and exacting one. ‘Abercrombie’ Lawsuit: Why CEOs Love Their Jets |Daniel Gross |October 19, 2012 |DAILY BEAST 

But I can attest first hand: one lapse aside, Fareed is just such a fastidious writer. A False Charge Against Fareed Zakaria (UPDATED) |David Frum |August 14, 2012 |DAILY BEAST 

So that fastidious snuff-takers may dismiss this bugbear at once and forever. Tobacco; Its History, Varieties, Culture, Manufacture and Commerce |E. R. Billings. 

It is one of the finest burning tobaccos in the world, and does not fail to suit the taste of the most fastidious of smokers. Tobacco; Its History, Varieties, Culture, Manufacture and Commerce |E. R. Billings. 

Had she, so observant, so discerning in her fastidious taste—had she failed to notice the small detail too? The Wave |Algernon Blackwood 

There are a great many obscene minds, yet more railing and satirical, but very few fastidious ones. The 'Characters' of Jean de La Bruyre |Jean de La Bruyre 

He was slow and fastidious in composition, and the poem suffered from over-elaboration. Encyclopaedia Britannica, 11th Edition, Volume 5, Slice 2 |Various