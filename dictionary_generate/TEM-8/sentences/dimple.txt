The unique dimple pattern on the ball helps keep the ball on a straighter path through the air, which is good if you have a hook, or more likely, a slice. The best golf balls for beginners to help beginners learn the game |Stan Horaczek |September 24, 2021 |Popular-Science 

The dimple pattern has fewer dimples, which cuts down on wind drag as the ball cuts through the air for maximum flight time. The best golf balls for beginners to help beginners learn the game |Stan Horaczek |September 24, 2021 |Popular-Science 

There are no dimples or edges in the rock to put her toes on. Emily Harrington Made History on El Cap. She’s Still Ascending. |jversteegh |July 26, 2021 |Outside Online 

I want to take it away from her to refill the dimples, but I also don’t want to break her heart. The Best Dog Toys, According to a Musher |cobrien |July 16, 2021 |Outside Online 

Air flowing over those dimples can have a pronounced effect on the trajectory of a ball as it flies through the air. Nerf’s newest blaster shoots spinning balls for dramatic curves |Stan Horaczek |January 27, 2021 |Popular-Science 

If there was a rough cellulite dimple, we might take it out. Joanna Coles: Why Cosmopolitan Does Sexy and Serious So Well |Lloyd Grove |August 22, 2014 |DAILY BEAST 

Devastated, Walt sidles up to the bar and orders one last Dimple Pinch, neat. ‘Granite State,’ the Penultimate Episode of ‘Breaking Bad,’ Is Walter White’s Final Act |Andrew Romano |September 23, 2013 |DAILY BEAST 

When the salt starts to pop (from the water trapped in the salt crystal), slap down your burger, dimple side up. The Perfect Burger (Sans Bun) |Deborah Krasner |November 1, 2010 |DAILY BEAST 

The woman who stepped off the elevator smiled, showing a lovely dimple, and Anson beamed on her. The Salesman |Waldo T. Boyd 

But yet I could have sworn I saw a dimple in her cheek through the mask, and a smile of mockery on her lips. The Way of a Man |Emerson Hough 

My fair vis-a-vis looked me now full in the face and smiled, so that a dimple in her right cheek was plainly visible. The Way of a Man |Emerson Hough 

Just then Miss Dimple appeared at the door with an uncertain smile. Prudy Keeping House |Sophie May 

"I like to sit and think in the dark," she explained, and her one dimple broke in a rich, brown-faced animal smile. Tramping on Life |Harry Kemp