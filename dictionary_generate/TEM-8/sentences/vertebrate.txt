Indigenous lands in Canada, Brazil and Australia had similar, or slightly higher, levels of vertebrate diversity than non-Indigenous protected areas in the same countries. Can people protect as much space as nature needs? |Jonathan Lambert |January 21, 2021 |Science News For Students 

“I don´t know of papers that have reported on traps built by spiders specifically for capturing vertebrates.” These spiders may sew leaves into fake shelters to lure frogs to their doom |Jake Buehler |January 4, 2021 |Science News 

Converting these largely natural habitats, collectively about the size of India, would squeeze more than 17,000 vertebrate species from some of their lands, researchers report December 21 in Nature Sustainability. Clearing land to feed a growing human population will threaten thousands of species |Jonathan Lambert |December 21, 2020 |Science News 

The researchers analyzed 14,000 vertebrate populations across the world and found that previously accepted worldwide declines in wildlife could be attributed to a few outlier populations. Wildlife conservation works, and vertebrates have the numbers to prove it |Rachael Zisk |December 1, 2020 |Popular-Science 

Additionally, the group of biologists from the Natural History Museum in London found that frogs have the biggest eyes of any vertebrate animal in relation to their body size. What hundreds of pickled frog carcasses can tell us about their enormous eyes |María Paula Rubiano A. |October 1, 2020 |Popular-Science 

Then in the Cambrian era, around 570 million years ago, recognizably complex animal life evolved, including vertebrate ancestors. Why Did It Take So Long For Complex Life To Evolve On Earth? Blame Oxygen. |Matthew R. Francis |November 2, 2014 |DAILY BEAST 

Some 28 percent of the vertebrate species red-listed by the International Union for Conservation of Nature live there. Is Rafael Correa the Next Hugo Chavez? |Mac Margolis |August 22, 2013 |DAILY BEAST 

It's so typical of everything we've seen of this semi-vertebrate. Romney and Ryan and the Medicare Lies |Michael Tomasky |August 14, 2012 |DAILY BEAST 

Two other structures common to most of the vertebrate animals exist in man, though they render him little or no service. Man And His Ancestor |Charles Morris 

It is a plantigrade circumflex vertebrate bacterium that hasnt any wings and is uncertain. A Horse's Tale |Mark Twain 

Plantigrade circumflex vertebrate bacterium that hasnt any wings and is uncertain. A Horse's Tale |Mark Twain 

The vertebrate animals deserve more of our attention than other forms of life because man himself is a vertebrate. A Civic Biology |George William Hunter 

The illustration here given shows the effect of nicotine upon a fish, one of the vertebrate animals. A Civic Biology |George William Hunter