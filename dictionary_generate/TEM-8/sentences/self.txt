Both of the tests she took were of the nasal swab variety, although the one she took on campus was self-administered. Even the most cautious schools are seeing outbreaks |Sy Mukherjee |September 17, 2020 |Fortune 

A vicious cycle of rejection by others played out in which undercontrolled youngsters never had opportunities to learn social skills and self-control. ‘The Origins of You’ explores how kids develop into their adult selves |Bruce Bower |September 16, 2020 |Science News 

Even though what she’s doing is sometimes very self-serving, and sometimes it’s downright menacing, she still has her reasons. ‘Ratched’ brings back iconic cinematic villain |Brian T. Carney |September 16, 2020 |Washington Blade 

Those crystals provide a nucleation point where the sugar structure — the candy — can self-assemble. Build ice towers with bottled water and ice |Bethany Brookshire |September 16, 2020 |Science News For Students 

Florida, concerned that it had taken on too much risk, has since scaled back its self-insurance plan. Climate Change Will Force a New American Migration |by Abrahm Lustgarten, photography by Meridith Kohut |September 15, 2020 |ProPublica 

We see detoxing as a path to transcendence, a symbol of modern urban virtue and self-transformation through abstinence. How Taryn Toomey’s ‘The Class’ Became New York’s Latest Fitness Craze |Lizzie Crocker |January 9, 2015 |DAILY BEAST 

Stephanie Giorgio, a classical musician, credits The Class for helping her cope with anxiety, focus, fear, and self-doubt. How Taryn Toomey’s ‘The Class’ Became New York’s Latest Fitness Craze |Lizzie Crocker |January 9, 2015 |DAILY BEAST 

For someone with anorexia, self-starvation makes them feel better. How Skinny Is Too Skinny? Israel Bans ‘Underweight’ Models |Carrie Arnold |January 8, 2015 |DAILY BEAST 

I went into the audition as Fericito, the Venezuelan percussionist, and then I did a self-defense expert. Coffee Talk with Fred Armisen: On ‘Portlandia,’ Meeting Obama, and Taylor Swift’s Greatness |Marlow Stern |January 7, 2015 |DAILY BEAST 

Those who come to the Dinner Party are self-selecting; they do want to talk about it. Everyone at This Dinner Party Has Lost Someone |Samantha Levine |January 6, 2015 |DAILY BEAST 

Let the thought of self pass in, and the beauty of great action is gone, like the bloom from a soiled flower. Pearls of Thought |Maturin M. Ballou 

Now this setting up of an orderly law-abiding self seems to me to imply that there are impulses which make for order. Children's Ways |James Sully 

At present, Louis was too self-absorbed by the struggles within him, to look deep into what was passing around him. The Pastor's Fire-side Vol. 3 of 4 |Jane Porter 

But to wave this discourse of Heathens, how many self-contradicting principles are there held among Christians? The Life and Most Surprising Adventures of Robinson Crusoe, of York, Mariner (1801) |Daniel Defoe 

Jean grinned and dribbled self-consciously, and showed his two little teeth to the proudest father in the world. The Joyous Adventures of Aristide Pujol |William J. Locke