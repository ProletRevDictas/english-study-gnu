Conventional wisdom holds that cleaning up a company’s environmental record is more expensive than paying fines, settling with plaintiffs, or dodging the liability altogether. Companies are turning their environmental liabilities into cash |Michael J. Coren |January 28, 2021 |Quartz 

Shalini Dogra, one of the attorneys for the plaintiffs, declined to say exactly what ingredients the lab tests revealed. Subway’s tuna is not tuna, but a ‘mixture of various concoctions,’ a lawsuit alleges |Tim Carman |January 27, 2021 |Washington Post 

Another plaintiff, who works as a massage therapist, said his account was suspended for 42 days in early 2020 after he posted messages about the coronavirus pandemic. California plaintiffs sue Chinese tech giant Tencent, alleging WeChat app is censoring and surveilling them |Jeanne Whalen |January 20, 2021 |Washington Post 

The health care industry was among those that welcomed the changes, and in 2011, the legislature strengthened the state’s medical malpractice laws, making claims harder to file and capping what plaintiffs could collect. The Nursing Home Didn’t Send Her to the Hospital, and She Died |by Sean Campbell |January 8, 2021 |ProPublica 

In this case, a Justice Department lawyer, writing on Pence’s behalf, wrote that the interests of Gohmert and the other plaintiffs were not sufficiently opposed to Pence’s own — since they were seeking to expand his power — to justify a suit. Pence seeks rejection of lawsuit that aimed to expand his power to overturn the election |Rosalind Helderman, John Wagner |January 1, 2021 |Washington Post 

Officer Eddie Boyd then started beating plaintiff in the head and body by punching him over and over. From Ferguson Cop Embroiled in a Brutality Suit to City Councilwoman |Michael Daly |August 20, 2014 |DAILY BEAST 

But the brain tumor plaintiff does not have to say what he would have done. Parents Sue for 'Wrongful Birth' |Elizabeth Picciuto |August 17, 2014 |DAILY BEAST 

Other plaintiff companies include Stone River Management Co. and Dunstone Co. 9. After Hobby Lobby, These 82 Corporations Could Drop Birth Control Coverage |Abby Haglage |June 30, 2014 |DAILY BEAST 

United States District Judge Nanette Laughrey ruled for the plaintiff, awarding her $450,0000. Why the Crisis in VA Hospitals Shames Our Country on Memorial Day |Jake Adelstein |May 26, 2014 |DAILY BEAST 

“While there have been well-documented rumors and cases filed, he was sued and the plaintiff lost the lawsuit,” he said. The NBA’s War With Donald Sterling Is Just Getting Started |Robert Silverman |April 29, 2014 |DAILY BEAST 

The Judge inquired if that was the sole object of the plaintiff, or was it not rather baiting with a sprat to catch a herring? The Book of Anecdotes and Budget of Fun; |Various 

The issue in an action for defamation is not the character of the plaintiff, but the wrongfulness of the particular statement. Putnam's Handy Law Book for the Layman |Albert Sidney Bolles 

It is enough if the defendant induces an ill opinion to be held of the plaintiff, or to make him contemptible or ridiculous. Putnam's Handy Law Book for the Layman |Albert Sidney Bolles 

If the defendant attempt evasion or flight, the plaintiff shall take him by force. The Two Great Republics: Rome and the United States |James Hamilton Lewis 

Then: "Your Honor, the plaintiff desires to withdraw all charges at this time." Meeting of the Board |Alan Edward Nourse