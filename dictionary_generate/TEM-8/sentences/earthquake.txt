Continuous buildup of carbon dioxide underground, he says, could drive earthquakes, which fracture Earth’s crust and allow more CO2 to creep upward, which in turn generates more quakes. Carbon dioxide from Earth’s mantle may trigger some Italian earthquakes |Maria Temming |August 26, 2020 |Science News 

When they do, they can raise up mountains, cause earthquakes — and open up volcanoes. Let’s learn about volcanoes |Bethany Brookshire |August 5, 2020 |Science News For Students 

Phenomena like earthquakes and ocean tides continually knock Earth’s rotation off-kilter, requiring constant correction of GPS satellite signals. A giant underground motion sensor in Germany tracks Earth’s wobbles |Maria Temming |July 17, 2020 |Science News 

While this is not the first time Ghana’s capital has experienced earth tremor, the frequency and intensity of recent tremors have left many residents worried about an imminent major earthquake. One of Africa’s fastest-growing cities is not prepared for the earthquake it knows is coming |Kwasi Gyamfi Asiedu |July 3, 2020 |Quartz 

Christchurch, New Zealand, transitioned to telework after earthquakes rocked the country in 2010 and 2012. Environment Report: The Latest Power Struggles for SDG&E and Sempra |MacKenzie Elmer |June 29, 2020 |Voice of San Diego 

But on Thursday Boxer triggered a Golden State political earthquake, announcing that she would not seek a fifth term in 2016. The Golden State Preps for the ‘Red Wedding’ of Senate Races |David Freedlander |January 9, 2015 |DAILY BEAST 

In 1997, an earthquake in Assisi caused the collapse of the main cathedral and killed ten people. Florence Preps ‘David’ for the Big One |Barbie Latza Nadeau |December 25, 2014 |DAILY BEAST 

In 2010 Cuba provided the largest contingent of medical staff during the aftermath of the huge earthquake that shook Haiti. The Dark Side of Cuba’s Ebola Economy |James Bloodworth |December 9, 2014 |DAILY BEAST 

The earthquake sparked a surge in religious belief in Japan. Takashi Murakami’s Art From Disaster |Justin Jones |November 28, 2014 |DAILY BEAST 

It is a glimpse at life exactly as it was at 3:32 am on April 6, 2009 when the earthquake stopped time. Madonna, Carla Bruni & Obama Abandoned Pledges To Rebuild L'Aquila After The Quake |Barbie Latza Nadeau |November 18, 2014 |DAILY BEAST 

An earthquake completely destroyed Schiraz in Persia; 12,000 lives were lost. The Every Day Book of History and Chronology |Joel Munsell 

She is always in fear of an earthquake, and feels safer to have a light burning in readiness all night long. Alila, Our Little Philippine Cousin |Mary Hazelton Wade 

Matthew, in his account of the fact of the Resurrection, says that there was an earthquake when the angel rolled away the stone. God and my Neighbour |Robert Blatchford 

A terrible earthquake happened at Pekin, in China, throwing down houses and burying more than 1,000 inhabitants in the ruins. The Every Day Book of History and Chronology |Joel Munsell 

The town of Praia, in the island of Terceira, completely destroyed by an earthquake. The Every Day Book of History and Chronology |Joel Munsell