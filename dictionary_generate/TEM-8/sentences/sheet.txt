The analysts said that most reinsurance companies would likely be able to rebuild their balance sheets through higher premiums next year. Insurers are getting nervous as Hurricane Laura is set to make landfall as a powerful Category 4 storm |Jeremy Kahn |August 26, 2020 |Fortune 

That was supported by the discovery in 2010 of the original, mislogged observation sheet bearing five separate errors. Death Valley hits 130° F, the hottest recorded temperature on Earth since 1931 |Carolyn Gramling |August 17, 2020 |Science News 

Many of the geological features thought to have been carved out by flowing rivers and waterways replenished by frequent rainfall, the research suggests, may have actually resulted from massive glaciers and ice sheets that melted over time. Mars may not have been the warm, wet planet we thought it was |Neel Patel |August 7, 2020 |MIT Technology Review 

Our plan is backed by a master developer, Toll Brothers, with decades of experience and an impressive balance sheet that answers any viability questions our competition raises. Public Benefits Define Midway Proposal |Frank Wolden, Aruna Doddapaneni and Colin Parent |July 21, 2020 |Voice of San Diego 

It’s like origami, but “instead of folding the flat sheet, you cut … along the specific pattern,” explains Sahab Babaee. Shape-shifting cuts give shoes a better grip |Carolyn Wilke |July 14, 2020 |Science News For Students 

Divide batter into prepared ramekins, place ramekins on a baking sheet, and bake about 20 minutes. Make ‘The Chew’s’ Carla Hall’s Sticky Toffee Pudding |Carla Hall |December 28, 2014 |DAILY BEAST 

Pat the chicken dry with paper towels, place on a sheet pan, brush with olive oil, and sprinkle with salt and pepper. Make These Barefoot Contessa Chicken Pot Pies |Ina Garten |November 29, 2014 |DAILY BEAST 

With a 1¾-inch ice cream scoop (or two spoons), scoop round balls of dough onto the prepared sheet pans. Make These Barefoot Contessa Salty Oatmeal Chocolate Chunk Cookies |Ina Garten |November 28, 2014 |DAILY BEAST 

Place the package, folded side up, on a sheet pan lined with parchment paper. The Barefoot Contessa’s Tasty Trip to Paris |Ina Garten |November 27, 2014 |DAILY BEAST 

Place one sheet of phyllo on the board, brush it with butter, and sprinkle it with ¾ teaspoon of bread crumbs. The Barefoot Contessa’s Tasty Trip to Paris |Ina Garten |November 27, 2014 |DAILY BEAST 

If you use it wisely, it may be Ulysses' hauberk; if you reject it, the shirt of Nessus were a cooler winding-sheet! The Pastor's Fire-side Vol. 3 of 4 |Jane Porter 

The Professor took his pen and wrote a large X upon the sheet of paper in front of him. Uncanny Tales |Various 

Instead of a cloth, on each table was a sheet of fine glazed paper which had the appearance of oiled silk. Our Little Korean Cousin |H. Lee M. Pike 

Demons—was the horrible word that flashed through his brain like a sheet of fire. Three More John Silence Stories |Algernon Blackwood 

A very shallow sheet of water flowed down over a broad but nowise precipitous ledge of rock into the valley beneath. A Woman's Journey Round the World |Ida Pfeiffer