For example, diamonds that formed in the crust and upper mantle had inclusions enriched in oxygen-18 — suggesting that the gemstones crystallized out of magma formed from subducted oceanic crust. Earth’s rarest diamonds form from primordial carbon in the mantle |Carolyn Gramling |September 14, 2020 |Science News 

That approach has driven up stock prices in the past four months and enriched drug executives betting with somebody else’s money. Oxford’s COVID vaccine deal with AstraZeneca raises concerns about access and pricing |lbelanger225 |August 24, 2020 |Fortune 

Still today, the mission of the center is to enrich the lives of the LGBTQ community through support, education, advocacy, and celebration. Cleveland – The Rock and Roll Capital of the World |LGBTQ-Editor |August 21, 2020 |No Straight News 

The Partnership was created to serve as a collaborative forum for our industry to ensure addressability standards that preserve privacy, provide a consistent and effective framework for advertisers, and enrich the consumer experience. Ad industry groups launch effort to push for addressable media standards |Ginny Marvin |August 4, 2020 |Search Engine Land 

What was problematic was the misuse of them, was when the private sector was able to manipulate and use these types of tools so as to enrich themselves and expose others to risk that they otherwise might not have needed to take on. Should America (and FIFA) Pay Reparations? (Ep. 426) |Stephen J. Dubner |July 16, 2020 |Freakonomics 

The Great Society is a place where every child can find knowledge to enrich his mind and to enlarge his talents. Thank Congress, Not LBJ for Great Society |Julian Zelizer, Scott Porch |January 4, 2015 |DAILY BEAST 

Collecting is a basic human drive, an incredibly various one and one which does much to enrich the lives of all. Pryor Dodge's Two-Wheeled Obsession Is Now a Museum of Bike History |Anthony Haden-Guest |September 15, 2014 |DAILY BEAST 

The primary goal of a city should not be to enrich already wealthy landlords and construction companies. Welcome to the Billion-Man Slum |Joel Kotkin |August 25, 2014 |DAILY BEAST 

Paul suggested that this reversal by Cheney was the result of a desire to enrich his former employers at Haliburton. 9/11 Truthers Can Be Politicians, Too |Ben Jacobs |April 21, 2014 |DAILY BEAST 

Engaging with these bodily and ritual practices serves to enrich the human spirit within. The Meaning of Vaisakhi, the Biggest Sikh Celebration |Simran Jeet Singh |April 13, 2014 |DAILY BEAST 

What the ear hears is the fundamental pitch only; the overtones harmonize with the primary or fundamental tone, and enrich it. Expressive Voice Culture |Jessie Eldridge Southwick 

Its walls and some other buildings still stand and abundant Roman remains enrich the local museum. Encyclopaedia Britannica, 11th Edition, Volume 5, Slice 2 |Various 

The rulers of a republic do not care though they should ruin the state, provided they enrich themselves. Great Men and Famous Women. Vol. 3 of 8 |Various 

The people in England had seemed to think all along that the colonies in America ought to do all they could to enrich England. The Story of the Thirteen Colonies |H. A. (Hlne Adeline) Guerber 

The house of Lorraine also continued to enrich the gallery, which did not escape Napoleon's generals. Florence and Northern Tuscany with Genoa |Edward Hutton