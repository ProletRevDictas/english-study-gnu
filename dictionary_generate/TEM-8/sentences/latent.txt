By the time England slumped into its latest lockdown, I would come to think of my project not merely as a surrogate for my latent curiosity but as an inspiration to endure. In London, looking backward to move forward |Henry Wismayer |March 5, 2021 |Washington Post 

Nordica set the benchmark for accessible or latent power in 2019 with the Enforcer 104 Free. Long-Term Review: The Best Skis, Period |Heather Schultz and Marc Peruzzi |March 4, 2021 |Outside Online 

So when I said earlier that cognition is not intrinsically conscious, that’s just saying that memories are, for the most part, latent. Consciousness Is Just a Feeling - Issue 98: Mind |Steve Paulson |March 3, 2021 |Nautilus 

Washington can be a lonely place at the top, and these climbers are coming up together, resting on their excellence and latent potential. Introducing the People Who Will Shape Washington for the Next Generation |Philip Elliott |February 17, 2021 |Time 

I would rather feel they are bringing to the surface latent prejudices that are already there. Martin Luther King Jr.’s challenge to liberal allies — and why it resonates today |Jeanne Theoharis |February 8, 2021 |Washington Post 

But a recent trend is capitalizing on our latent desires to actually become the famous people we love and love to hate. Kevin Spacey and the Most Inexplicable Celebrity Video Game Cameos |Amy Zimmerman |May 3, 2014 |DAILY BEAST 

So it carbonates all of these, I'd say latent desires, to have more meaning in his life. 'About a Boy' Star David Walton Is No Hugh Grant, in the Best Way |Kevin Fallon |February 20, 2014 |DAILY BEAST 

Many pent-up resentments and latent frustrations are boiling inside the Negro, and he must release them. Alex Haley’s 1965 Playboy Interview with Rev. Martin Luther King Jr. |Alex Haley |January 19, 2014 |DAILY BEAST 

It was more science faction, a monument to human potential—including the latent potential for self-destruction. At the Hanford Nuclear Reservation, a Steady Drip of Toxic Trouble |Eric Nusbaum |February 24, 2013 |DAILY BEAST 

But the "Zoabiz" was a small reminder that latent chauvinism is alive and well. The Latent Chauvinism Of Lapid's "Zoabiz" Apology |Rawia Aburabia |February 14, 2013 |DAILY BEAST 

Now listen to me, said the dying woman, aloud, as if making a great effort to revive one latent spark of energy. Oliver Twist, Vol. II (of 3) |Charles Dickens 

That high-pressure engines owed their advantages mainly to a reduction of the relative importance of this latent heat. Life of Richard Trevithick, Volume II (of 2) |Francis Trevithick 

In descending it is recondensed, and by the process by which its atoms are brought together its latent heat is made sensible. Outlines of the Earth's History |Nathaniel Southgate Shaler 

Again she felt that everlasting calm, arguing such latent forces, was the thing she hated most in him. Mistress Wilding |Rafael Sabatini 

But it possessed a latent purchasing power such as probably no other Government in history ever had. The Supplies for the Confederate Army |Caleb Huse