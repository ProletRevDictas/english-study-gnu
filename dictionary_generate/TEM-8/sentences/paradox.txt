The only paradox is that till 35 years ago, this view of the indigenous plant and its psychotropic by-products was not viewed as a crime. How did marijuana become illegal in India? |Manavi Kapur |September 11, 2020 |Quartz 

So there’s another paradox there, which is that language maintains as well as creates. Talking Is Throwing Fictional Worlds at One Another - Issue 89: The Dark Side |Kevin Berger |September 9, 2020 |Nautilus 

The modern resolution to the paradox contains some subtleties, but it does indeed mostly come down to the fact that we do not live in an endless and unchanging universe. The Universe Has Made Almost All the Stars It Will Ever Make - Issue 89: The Dark Side |Caleb Scharf |August 19, 2020 |Nautilus 

You might think you could just posit some extra axiom, use it to prove G, and resolve the paradox. How Gödel’s Proof Works |Natalie Wolchover |July 14, 2020 |Quanta Magazine 

My mother taught me that the enemy of my enemy is my friend, and this paradox has been useful while dealing with both high school and workplace politics. “Paradox” vs. “Oxymoron”: How To Tell The (Seemingly Similar) Difference |Lauren Levy |July 7, 2020 |Everything After Z 

But I feel like films are uniquely suited towards addressing paradox, recursiveness, and worlds-within-worlds. Christopher Nolan Uncut: On ‘Interstellar,’ Ben Affleck’s Batman, and the Future of Mankind |Marlow Stern |November 10, 2014 |DAILY BEAST 

To appreciate the Palmer paradox, it's important to understand that Palmer's childhood and young adulthood were dichotomous. Will the Real Jim Palmer Please Stand Up |Tom Boswell |September 27, 2014 |DAILY BEAST 

“Maybe we need a new category other than theism, atheism or agnosticism that takes paradox and unknowing into account,” he writes. Frank Schaeffer, the Atheist Who Believes in God |Nick Tabor |August 3, 2014 |DAILY BEAST 

But Washington was a prisoner to its paradox of an Iraq policy. Why the White House Ignored All Those Warnings About ISIS |Eli Lake |July 6, 2014 |DAILY BEAST 

As a result of this paradox, the Iraq policy process ground to a halt at the very moment that ISIS was on the rise. Why the White House Ignored All Those Warnings About ISIS |Eli Lake |July 6, 2014 |DAILY BEAST 

It offers, to those who see it aright, the most perplexing industrial paradox ever presented in the history of mankind. The Unsolved Riddle of Social Justice |Stephen Leacock 

But in reality this paradox of value is the most fundamental proposition in economic science. The Unsolved Riddle of Social Justice |Stephen Leacock 

It was the spiritual way, whose method and secret lie in that subtle paradox: Yield to conquer. The Wave |Algernon Blackwood 

But it was a strange paradox, that precisely the depth of his love for her made him willing to think of losing her. Love's Pilgrimage |Upton Sinclair 

But this very paradox leads to the real principle of generalization concerning the properties of numbers. A System of Logic: Ratiocinative and Inductive |John Stuart Mill