He tells her about his divorce from his ex-wife, his two adult sons. The mind-boggling end of Susan Choi’s Trust Exercise, explained |Constance Grady |November 20, 2020 |Vox 

Their computers spent nearly 9,000 hours running equations, and their model, published in August as a preprint, shows that if there is only a low supply of vaccine at first, older adults should be prioritized if the goal is to reduce deaths. Who Should Get a Covid-19 Vaccine First? |Jill Neimark |November 20, 2020 |Singularity Hub 

One 16-year-old told police it was “obvious” she and a friend who met Erasquin were not adults. Matthew Erasquin, Arlington lawyer, accused of paying underage girls for sex |Rachel Weiner |November 20, 2020 |Washington Post 

The only thing successful about Thursday’s news conference, Kimmel argued, was the fact that it did not take place in the parking lot next to an adult shop. ‘Our voting system is fine. ... It’s Rudy that’s broken’: Late-night hosts mock Giuliani’s ‘insane’ news conference |Andrea Salcedo |November 20, 2020 |Washington Post 

In that way it’s like learning a language—children can assimilate the game’s complex rules and action much more intuitively and quickly than an adult. Scientists Analyzed 24,000 Chess Matches to Understand Cognition - Facts So Romantic |Brian Gallagher |November 20, 2020 |Nautilus 

It is hardly a rejection of adulthood, rather a momentary escape from routine. Don’t Diss the Beauty of Brunch: Defending Our Favorite Meal |Tim Teeman |October 15, 2014 |DAILY BEAST 

To appreciate the Palmer paradox, it's important to understand that Palmer's childhood and young adulthood were dichotomous. Will the Real Jim Palmer Please Stand Up |Tom Boswell |September 27, 2014 |DAILY BEAST 

The moral duties and doubts of adulthood are swapped out for the histrionic creeds of adolescence. Two New Films Preach Our Nation’s Corrosive Gridiron Gospel |Steve Almond |September 20, 2014 |DAILY BEAST 

The play, set in 1982, depicts the struggles of three privileged slackers to come to terms with impending adulthood. Michael Cera Brings ‘This Is Our Youth’ to Broadway After 18 Years |Tom Teodorczuk |September 12, 2014 |DAILY BEAST 

As an adulthood now living out his passion, his dedication sometimes borders on obsession. Fighting Ebola With Nothing but Hope |Abby Haglage |August 27, 2014 |DAILY BEAST 

No one of these characters alone was accepted as proof of adulthood but only the three in combination. Speciation in the Kangaroo Rat, Dipodomys ordii |Henry W. Setzer 

Reproductive-wise, there is no question as to adulthood; each of the four females was pregnant. Mammals Obtained by Dr. Curt von Wedel from the Barrier Beach of Tamaulipas, Mexico |E. Raymond Hall 

The idea of education as preparation and of adulthood as a fixed limit of growth are two sides of the same obnoxious untruth. Reconstruction in Philosophy |John Dewey 

The Youth, advanced to adulthood, proceeded to steal things of still greater value. Aesop's Fables |Aesop 

Dombey and Son is full of appeals for the tender sympathy of adulthood for childhood. Dickens As an Educator |James L. (James Laughlin) Hughes