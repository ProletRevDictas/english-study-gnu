Aki was dead right — the way I’d handled the situation was terrible. What if Your Company Had No Rules? (Bonus Episode) |Maria Konnikova |September 12, 2020 |Freakonomics 

Maybe even, if the threat seems terrible enough and the situation pulls you along, drawing the worst from you, you might find yourself a perpetrator. Believing in Monsters: David Livingstone Smith on the Subhuman - Facts So Romantic |Eric Schwitzgebel |September 11, 2020 |Nautilus 

Seems like it would have been easier to write how terrible a style choice oversized T-shirts are. When Did Gavin McInnes Lose His Mind? |Eugene Robinson |September 6, 2020 |Ozy 

He was a business owner also, and it really did work, all the jobs I had that I was terrible at — well, no, it wasn’t that I’m terrible at, that I was angry at because I wasn’t getting the management I needed. Tan France Goes Deep on Racism and When He Almost Quit ‘Queer Eye’ |Eugene Robinson |September 3, 2020 |Ozy 

The workers at this facility had the courage to stand up against this terrible treatment. Temp Workers Fight Back Against Alleged Sexual Harassment and Say They Face Retaliation for Doing So |by Melissa Sanchez |August 28, 2020 |ProPublica 

He returned home to learn that his 9-year-old son had been awakened in the night by a terrible dream. Choking Back Tears, Thousands of Cops Honor Fallen Officer Ramos |Michael Daly |December 28, 2014 |DAILY BEAST 

The birds are debeaked, suffer ulcers, and terrible feet conditions. The History of the Chicken: How This Humble Bird Saved Humanity |William O’Connor |December 27, 2014 |DAILY BEAST 

Amelia says some truly terrible things to Sam, supposedly inhabited by the Babadook but really consumed in grief. Grief: The Real Monster in The Babadook |Tim Teeman |December 19, 2014 |DAILY BEAST 

Most frustratingly for the school chancellor, this made it all but impossible to fire terrible teachers. Your Local School Doesn’t Have to Suck |Michael S. Roth |December 17, 2014 |DAILY BEAST 

I knew because I rifled through his mail that terrible October morning. I Was Gang Raped at a UVA Frat 30 Years Ago, and No One Did Anything |Liz Seccuro |December 16, 2014 |DAILY BEAST 

Judge or sheriff, it was all one to them, each being equally terrible in their eyes. The Bondboy |George W. (George Washington) Ogden 

Terror drives you on; fate coerces you; you can't help yourself, and my delight is to make the plunge terrible. Checkmate |Joseph Sheridan Le Fanu 

The king was struck with horror at the description I had given him of those terrible engines, and the proposal I had made. Gulliver's Travels |Jonathan Swift 

The history of that terrible hour is brightened by many such instances of native fealty. The Red Year |Louis Tracy 

He stood listening to what I was saying, and I recall that when I turned slightly and saw his face, it was terrible! The Homesteader |Oscar Micheaux