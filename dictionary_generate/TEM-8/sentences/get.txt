In this cockamamie get-rich scheme, would they all issue an apology if he cut a check? Phylicia Rashad and the Cult of Cosby Truthers |Stereo Williams |January 8, 2015 |DAILY BEAST 

The kids are out of school, Mom is out of get-up-and-go, Dad is out of work. Congress’ Gift That Keeps on Giving |P. J. O’Rourke |December 20, 2014 |DAILY BEAST 

But while Yariv and Nakarin have had plenty of their friends over for dinner, this is no regular get-together. The Airbnb of Home-Cooked Meals |Itay Hod |November 3, 2014 |DAILY BEAST 

So it's really a take-what-you-can-get kind of situation with him. ‘Scandal’ Review: Olivia Pope Has Lost Her Damn Mind |Kevin Fallon |September 26, 2014 |DAILY BEAST 

And so I knew from the get-go that they would have to add action, which they have. A Trailblazer in YA Dystopian Fiction: An Interview With 'The Giver' Author Lois Lowry |Marianne Hayes |August 12, 2014 |DAILY BEAST 

The man called Shiv was driving Delancy's get-away car at a conservative pace so as not to excite suspicion. Hooded Detective, Volume III No. 2, January, 1942 |Various 

Ten minutes later, Delancy drove the get-away car out of the service station. Hooded Detective, Volume III No. 2, January, 1942 |Various 

It was in this room that Delancy's get-away car had changed paint jobs, and in about ten minutes. Hooded Detective, Volume III No. 2, January, 1942 |Various 

But the exquisite was used to it; he would only have felt badly if they had ignored his new get-up. The Girls of Central High on the Stage |Gertrude W. Morrison 

There was some sort of a phonograph device under the cowl of that get-away car, and this was hooked up to the radio switch. Hooded Detective, Volume III No. 2, January, 1942 |Various