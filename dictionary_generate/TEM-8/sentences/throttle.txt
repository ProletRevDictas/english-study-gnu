It operates with a twist throttle, so Wilder is learning real motorcycle control skills, as well as balance, as he rips around trails on this thing at nine miles per hour, the top speed. A Bad Uncle's Guide to Dangerous Gifts for Kids |Wes Siler |December 11, 2020 |Outside Online 

Because you don’t need to shift, accelerating is an easy, linear experience—twist that throttle and zoom forward. I rode an electric motorcycle for the first time. Here’s what I learned. |Rob Verger |December 10, 2020 |Popular-Science 

Although sometimes when it’s difficult, the tendency for me is not to want to walk away from it, I just want to go full throttle forward and just plunge into it. Dan Rather: Propaganda and injustice are a ‘very potent, toxic mix’ |Joe Heim |November 23, 2020 |Washington Post 

Ergonomic controls are mounted right on the tube for ease-of-use, and a cruise-control feature allows you to maintain constant air-power for extended periods without riding the throttle. Leaf blowers that help you fight fallen foliage |PopSci Commerce Team |September 25, 2020 |Popular-Science 

The motor assists when the rider is pedaling or when the rider uses a throttle to accelerate without pedaling. Do You Want to Buy an E-Cargo Bike? Read This First. |Joe Lindsey |August 30, 2020 |Outside Online 

When you hit a throttle on a sprint car, the car sets sideways. Homicide or Accident in Tony Stewart’s NASCAR Scandal? |Robert Silverman |August 11, 2014 |DAILY BEAST 

Even Biden seems hesitant to go full throttle against Hillary. Does America Really Want to Coronate Hillary? |Myra Adams |March 14, 2014 |DAILY BEAST 

So while the Republican race is in full throttle, the Democrats are left in standby mode. Democratic Presidential Contenders Stay in the Shadows |David Catanese |May 26, 2013 |DAILY BEAST 

When adultescents return home, mothers tend to reinvest full throttle in their dormant parenting role. Boomerang Moms: When Mommy Returns to Deal With ‘Adultescents’ |Sally Koslow |June 15, 2012 |DAILY BEAST 

Riders get traction control, electronic suspension adjustment, and a ride-by-wire throttle. Ducati’s Panigale and History’s Most Innovative Motorcycles |Chris Hunter |November 13, 2011 |DAILY BEAST 

Bending lower over the handlebars, he opened the throttle with a twist of his left hand. Motor Matt's "Century" Run |Stanley R. Matthews 

A further control of the high speed jet is provided by the fuel metering valve operated by the carbureter throttle. Marvel Carbureter and Heat Control |Anonymous 

Advance spark lever about half way and throttle lever about one-quarter way and depress starter pedal. Marvel Carbureter and Heat Control |Anonymous 

If the engine idles too fast with throttle closed, the latter may be adjusted by means of the throttle lever adjusting screw. Marvel Carbureter and Heat Control |Anonymous 

Karl cried, trying blindly to reach him, to grasp his throat to throttle him. The Devil |Joseph O'Brien