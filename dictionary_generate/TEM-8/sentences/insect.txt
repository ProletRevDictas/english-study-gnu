The findings could illustrate for the first time that, when targeted with pesticides, changes to the physical traits of “pest” species can occur in bigger animals, not just insects and rodents. Culling dingoes with poison may be making them bigger |Jake Buehler |August 19, 2020 |Science News 

After a few weeks, that larval insect wraps itself in a jade green covering. Pesticides contaminate most food of western U.S. monarchs |Rebecca E. Hirsch |August 17, 2020 |Science News For Students 

Prior to this new find, the oldest plant bedding — mainly consisting of sedge leaves, ash and aromatic plants likely used to keep insects away — dated to around 77,000 years ago at South Africa’s Sibudu rock-shelter. The oldest known grass beds from 200,000 years ago included insect repellents |Bruce Bower |August 13, 2020 |Science News 

Chemical communication among insects isn’t always monosyllabic, with a single compound changing a behavior wholesale. A single molecule may entice normally solitary locusts to form massive swarms |Jonathan Lambert |August 12, 2020 |Science News 

Miyako and Yang say their bubble solution was biocompatible, but Potts worries that dousing flowers in human-made substances could dissuade insects from visiting those trees. Bubble-blowing drones may one day aid artificial pollination |Maria Temming |June 22, 2020 |Science News 

Bats are crucial to the ecosystem, performing extremely valuable jobs like pollination and insect control. Bats’ Link to Ebola Finally Solved |Carrie Arnold |November 12, 2014 |DAILY BEAST 

Their nightly flights bring with them the powers to pollinate plants and control insect populations. Bats’ Link to Ebola Finally Solved |Carrie Arnold |November 12, 2014 |DAILY BEAST 

When you crush an insect, you have all these long worms uncoiling from the belly. Vampires without Glitter or Girl Problems: Inside Guillermo del Toro’s ‘The Strain’ |Andrew Romano |July 14, 2014 |DAILY BEAST 

One of these heroes is an insect-loving contemporary of Charles Darwin, the other a crocodile-wrestling Steve Irwin acolyte. Exploring the Amazon, While We Still Can |Darrell Hartman |May 15, 2014 |DAILY BEAST 

On the heels of a U.N. report urging more insect consumption, Nina Strochlic rounds up the yummiest. Cicadas, Grasshoppers, Locusts, Ants Among the Tastiest Insects |Nina Strochlic |May 14, 2013 |DAILY BEAST 

To the animal whose special finger enables him to catch the insect? God and my Neighbour |Robert Blatchford 

In my house there has never been sufficient food for a solid meal, and I have not land enough even for an insect to rest upon. Our Little Korean Cousin |H. Lee M. Pike 

The heat of the water destroys the insect and instantly removes the pain of the bite. The Book of Anecdotes and Budget of Fun; |Various 

This insect is much less in size than the former, and is more convex. Narrative of a Survey of the Intertropical and Western Coasts of Australia] [Volume 2 of 2] |Phillip Parker King 

There is a wretched figure of this insect given in the fourth volume of Cuvier's Regne Animal. Narrative of a Survey of the Intertropical and Western Coasts of Australia] [Volume 2 of 2] |Phillip Parker King