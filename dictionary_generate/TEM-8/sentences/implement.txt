You’ll never ever use these implements for anything else, but they take up so little room that it’s worth it to buy a few if you’re a grapefruit lover. Nine kitchen gifts that do one thing really well |Sara Chodosh |November 26, 2020 |Popular-Science 

Middle Stone Age tools were smaller and more carefully crafted implements. How environmental changes may have helped make ancient humans more adaptable |Bruce Bower |October 21, 2020 |Science News 

Although it’s still unclear where in Africa — as well as when and by whom — Middle Stone Age tools were invented, early humans would have found such implements invaluable for adapting to environmental disruptions, Stringer says. How environmental changes may have helped make ancient humans more adaptable |Bruce Bower |October 21, 2020 |Science News 

I’d lost the implement I’d needed to claw myself out of my cage, and I hadn’t had what he’d needed from me when he’d needed it. Fiction: Quiet earth philosophy |Katie McLean |October 21, 2020 |MIT Technology Review 

Existence felt like a cage, and gaming was an implement that clawed us out. Fiction: Quiet earth philosophy |Katie McLean |October 21, 2020 |MIT Technology Review 

St. Laurent, however, pointed out why this step might be especially difficult to implement. How the U.S. Justice System Screws Prisoners with Disabilities |Elizabeth Picciuto |December 16, 2014 |DAILY BEAST 

For its part, the Pentagon said that it is no surprise that it is taking some time to implement the mission. U.S. Hasn’t Even Started Training Rebel Army to Fight ISIS |Tim Mak |November 25, 2014 |DAILY BEAST 

Adding a checkbox to a driver's license and another form would make this easy to implement. A Navy Vet’s Case for Gun Control |Shawn VanDiver |November 23, 2014 |DAILY BEAST 

Republicans have a rare opportunity to implement policies that are truly compassionate and transcend toxic identity politics. How a GOP Senate Can Help the Poor |Veronique de Rugy |November 23, 2014 |DAILY BEAST 

The White House just launched a major initiative to implement a more modern, sensible drug policy. Why Isn’t Prison Justice on the Ballot This Tuesday? |Inimai Chettiar, Abigail Finkelman |November 1, 2014 |DAILY BEAST 

He stood up, crowbar in hand, and inserted the chisel blade of the implement between the edge of the door and the doorcase. Dope |Sax Rohmer 

I looked among the dead, to see if I could find any iron implement with which to excavate the wall, or to break the chains. Rule of the Monk |Giuseppe Garibaldi 

Soon the captain drove his implement through the hay, and against something that gave back a resistance like that of soft pine. Uncle Sam's Boys as Lieutenants |H. Irving Hancock 

As the implement fell upon the stones of the courtyard, Thyra's quick ear noticed the peculiar sound. Jack Harkaway's Boy Tinker Among The Turks |Bracebridge Hemyng 

In his hand the measuring-rod was a far mightier implement than the pen. Ten Books on Architecture |Vitruvius