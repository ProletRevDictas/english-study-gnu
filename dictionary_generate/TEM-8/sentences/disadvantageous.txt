Kyiv was forced to accept a deeply disadvantageous ten-year gas transit deal with Moscow, among other concessions. Russia Is Still Winning the Energy War |Suriya Jayanti |April 29, 2022 |Time 

So a plausible working hypothesis is that centenarians carry rare, beneficial genetic variations rather than a lack of disadvantageous ones. Why It’s Still a Scientific Mystery How Some Live Past 100—and How to Crack It |Richard Faragher |December 5, 2021 |Singularity Hub 

That party is now intertwined with the extent to which America is willing to tamp down on a deadly virus is, to put it mildly, disadvantageous. The inescapable overlap of pandemic and politics |Philip Bump |September 10, 2021 |Washington Post 

They thought it would involve the president personally in a way that would be politically disadvantageous. How Robert F. Kennedy Shaped His Brother's Response to Civil Rights |Patricia Sullivan |August 11, 2021 |Time 

Kimura posited that most of the variation between organisms is neither advantageous nor disadvantageous. How Neutral Theory Altered Ideas About Biodiversity |Christie Wilcox |December 8, 2020 |Quanta Magazine 

As Justice Kennedy noted, “this creates a disadvantageous position for some employees.” Contraception Looks Like a Loser at the Supreme Court |Jay Michaelson |March 25, 2014 |DAILY BEAST 

The suit should not be so full of possible tenaces as to make it disadvantageous to open it. Encyclopaedia Britannica, 11th Edition, Volume 4, Part 3 |Various 

Castalia lacked the Ancram gift of embellishing disadvantageous circumstances. A Charming Fellow, Volume II (of 3) |Frances Eleanor Trollope 

Constructional requirements determined as the only available position for this rudder a rather disadvantageous one. Langley Memoir on Mechanical Flight, Parts I and II |S. P. (Samuel Pierpont) Langley and Charles M. (Charles Matthews) Manly 

Most of his English counsellors dissuaded him from accepting conditions so disadvantageous and dishonorable. The History of England in Three Volumes, Vol.I., Part E. |David Hume 

Egerton advised him that the demise was disadvantageous, but that it might be hard to terminate it without Browne's concurrence. Sir Walter Ralegh |William Stebbing