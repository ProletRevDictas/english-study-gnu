The reason given was an incident in January when his daughter was found on campus with a vape pen. School Sports Became ‘Clubs’ Amid the Pandemic – Now Two Coaches Are Out |Ashly McGlone |September 17, 2020 |Voice of San Diego 

The publication confirmed that Dorris had told both a friend in the city and her mother about the incident — both of whom corroborated her account. Former model Amy Dorris says President Donald Trump sexually assaulted her |Li Zhou |September 17, 2020 |Vox 

For any incident, she says, “it’s really about trying to see the positive sides, and it’s about how you deal with it.” How a Swiss Ski Resort Was Ravaged by Typhoid and Survived |Daniel Malloy |September 9, 2020 |Ozy 

It’s incredible the amount of media intensity that happens when there’s a viral incident. What Can Mayors Do When the Police Stop Doing Their Jobs? |by Alec MacGillis |September 3, 2020 |ProPublica 

Robinhood is facing class action lawsuits over the outages in March from customers who allege the incident caused them to lose money. Robinhood app stumbles amid surge in Apple, Tesla trading volume |Jeff |September 1, 2020 |Fortune 

Which is impossible unless people talk publicly rather than letting each crime be its own isolated incident. Cover-Ups and Concern Trolls: Actually, It's About Ethics in Suicide Journalism |Arthur Chu |January 3, 2015 |DAILY BEAST 

The “crying” incident is thought to have hurt Muskie in the primary--which he won handily, but with under 50 percent of the vote. The World’s Toughest Political Quiz |Jeff Greenfield |December 31, 2014 |DAILY BEAST 

“I guess it was their first incident where they lose a plane,” said Dobersberger, the travel agent. The Presumed Crash of AirAsia Flight QZ8501 Is Nothing Like MH370 |Lennox Samuels |December 29, 2014 |DAILY BEAST 

They finished out the tour without incident, while newspapers across the country picked up the story. How Martin Luther King Jr. Influenced Sam Cooke’s ‘A Change Is Gonna Come’ |Peter Guralnick |December 28, 2014 |DAILY BEAST 

The incident sparked his belief in Santa, but he would have to wait nearly two decades before dressing up as Jolly St. Nick. Kerry Bentivolio: The Congressman Who Believes in Santa Claus |Ben Jacobs |December 24, 2014 |DAILY BEAST 

Nevertheless the evening and the night passed away without incident. Blackwood's Edinburgh Magazine, No. CCCXXXIX. January, 1844. Vol. LV. |Various 

A little incident which his mother remembers is not without a pretty allegoric significance. Kipling Stories and Poems Every Child Should Know, Book II |Rudyard Kipling 

It is not, however, the incident in itself that is now referred to, but only the formality ascribed to it in the narrative. Solomon and Solomonic Literature |Moncure Daniel Conway 

The incident did not demand more than a few seconds for its transaction and Winifred hardly noticed it, so unstrung was she. The Red Year |Louis Tracy 

A curious incident: during the night a Fleet-sweeper tied up alongside, full of wounded, chiefly Australians. Gallipoli Diary, Volume I |Ian Hamilton