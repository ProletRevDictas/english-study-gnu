Quesada has legalized same-sex marriage, taken on leftist autocrats like Venezuela’s Nicholás Maduro and driven an ambitious green agenda, including a plan to reduce to zero all carbon emissions — and not just net pollution — by 2050. Who Are You Calling a Communist? |Charu Kasturi |August 24, 2021 |Ozy 

These cryptosocialists view digital money as a valuable ally in everything from achieving equitable housing distribution to defeating autocrats. Creative Currencies — The Anti-Crypto |Sohini Das Gupta |August 11, 2021 |Ozy 

He had worked for many years in the Middle East and Africa, dealing with warlords and autocrats who could become new clients for Ukrainian weapons and aircraft. Exclusive: Documents Reveal Erik Prince's $10 Billion Plan to Make Weapons and Create a Private Army in Ukraine |Simon Shuster/Kyiv |July 7, 2021 |Time 

It’s a sucker punch that has worked for teenagers and autocrats alike for millennia. Moscow's Long History of Turning the Tables on Washington When It Comes to Human Rights |Philip Elliott |June 17, 2021 |Time 

In his plays, you always have the autocrats, who want to control others, and then you have the purer people, usually younger characters, like Hamlet, who propose love, who pursue a humanitarian vision against them. ‘Hamlet/Horatio’ queers the Bard |John Paul King |May 13, 2021 |Washington Blade 

These reforms were mostly designed to reassure Sunnis and Kurds that Maliki would not become an autocrat. How Iran and America Can Beat ISIS Together |Ben Van Heuvelen |June 21, 2014 |DAILY BEAST 

Beholden to a base that, like a capricious autocrat, will turn against them at the slightest provocation. Ted Cruz is Still Not Here to Make Friends |Jamelle Bouie |February 26, 2014 |DAILY BEAST 

Nothing humbles an autocrat quite like the need to grub for votes. Memo: The Aaron Sorkin Model of Political Discourse Doesn't Actually Work |Megan McArdle |April 23, 2013 |DAILY BEAST 

Tunisia was the first in a string of Middle Eastern countries to go through autocrat-toppling protests. Tunisian Leader Speaks on Democracy to Council on Foreign Relations |Luke Darby |September 29, 2012 |DAILY BEAST 

But the issue should not be where a wounded autocrat convalesces. Hold Yemen Officials Who Sanctioned Civilian Attacks Accountable |Letta Tayler |January 28, 2012 |DAILY BEAST 

It was the case with the agricultural communities of the southern United States, whose Mico was at once high priest and autocrat. Man And His Ancestor |Charles Morris 

Humble as Lecamus seemed to the outer world, he was despotic in his own home; there he was an autocrat. Catherine de' Medici |Honore de Balzac 

She has always had the germ of the ruler and autocrat in her soul. Fair to Look Upon |Mary Belle Freeley 

The Irish Minister for Agriculture by no means rules as an autocrat. Home Rule |Harold Spender 

It was curious how he was growing to be a kind of autocrat in the village; and how unconscious he was of it. My Lady Ludlow |Elizabeth Gaskell