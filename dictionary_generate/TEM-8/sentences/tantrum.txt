No wonder I recently had a claustrophobic tantrum and walked out of my house so I could sit alone in my car in the dark for 30 minutes. Why working moms deserve a tantrum (and how to survive the remainder of the pandemic) |Christine Koh |February 1, 2021 |Washington Post 

Those early philosophers sought logical explanations for things like thunderstorms, rather than attributing them to Zeus throwing temper tantrums in the form of thunderbolts. ‘Fundamentals’ shows how reality is built from a few basic ingredients |Tom Siegfried |January 26, 2021 |Science News 

Clemson Coach Dabo Swinney, 51, went childlike tantrum on being unable to go outside and play in the yard at Florida State, even when such possibilities and responsibilities always hovered over the misshapen season. Nick Saban, the ultimate control freak, learned how to handle disruption this season |Chuck Culpepper |January 1, 2021 |Washington Post 

As a parent coach, I am confident that your daughter will learn to read and write and do arithmetic, but the crying, tantrums and fighting are more worrisome. My third-grader gets distracted during online classes. How can I help? |Meghan Leahy |December 2, 2020 |Washington Post 

Her angry tantrums have always been an issue, but they've gotten worse since we've been staying at home. My third-grader gets distracted during online classes. How can I help? |Meghan Leahy |December 2, 2020 |Washington Post 

Whether the country or the courts buy into his imperial tantrum remains to be seen. With Immigration Move, Obama and the Welfare Party Strike Again |Lloyd Green |November 24, 2014 |DAILY BEAST 

Just a few months ago my 3-year-old son, mid-tantrum, broke my grasp to run from me in a crowded subway station. Why Adrian Peterson Changed My Mind on Spanking |Brandy Zadrozny |September 17, 2014 |DAILY BEAST 

Any argument I try to hold comes tumbling out in the form of a whiny temper tantrum. Growing Up with Bart Simpson |Alex Suskind |August 31, 2014 |DAILY BEAST 

An 8-year-old girl in Missouri was held in a police car for two hours after throwing a temper tantrum in March. Do Cops With Guns Mean Safer Schools? |Eliza Shapiro |April 2, 2013 |DAILY BEAST 

This year, however, the tantrum has continued, and the words have become increasingly dire. ‘The State of War’—Kim Jong-un’s Bombastic, and Ominous, Bluster |Gordon G. Chang |March 30, 2013 |DAILY BEAST 

You probably saved my life, for you can't tell what a half-wit will do, when in a tantrum and armed with a knife. David Lannarck, Midget |George S. Harney 

Her voice soared shrilly to match the heights of her tantrum. From Place to Place |Irvin S. Cobb 

He's not had a tantrum or a whining fit since you made friends. The Secret Garden |Frances Hodgson Burnett 

"I may be obliged to have a tantrum," said Colin regretfully. The Secret Garden |Frances Hodgson Burnett 

Of course Dr. Craven had been sent for the morning after Colin had had his tantrum. The Secret Garden |Frances Hodgson Burnett