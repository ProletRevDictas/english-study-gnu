The idea of being able to summon a custom ride anytime, anywhere using a powerful but tiny handheld computer was nothing short of inconceivable back then. Uber Wants to Go All-Electric by 2030. It Won’t Be Easy |Vanessa Bates Ramirez |September 10, 2020 |Singularity Hub 

They could summon other locusts to morph into a gregarious swarm. A single chemical may draw lonely locusts into a hungry swarm |Jonathan Lambert |September 7, 2020 |Science News For Students 

In Chu Renhu’s 17th-century Historical Romance of the Sui and Tang Dynasties, Mulan must choose between loyalty to the emperor and her commitment to chastity after the emperor summons her to be his consort. The history of Mulan, from a 6th-century ballad to the live-action Disney movie |Constance Grady |September 4, 2020 |Vox 

For example, people would be able to instantly play their favorite song by thinking about it or summon a self-driving car. Elon Musk shows off Neuralink brain implant technology in a living pig |jonathanvanian2015 |August 29, 2020 |Fortune 

Determined to summon a resiliency I wasn’t feeling, I gamely joined Zoom reunions and went to Zoom cocktail parties. That chatbot I’ve loved to hate |Tate Ryan-Mosley |August 19, 2020 |MIT Technology Review 

Johnson could not summon a single word when he saw that Wilson had drawn his gun. 90 Seconds of Fury in Ferguson Are the Key to Making Peace in America |Michael Daly |November 26, 2014 |DAILY BEAST 

If boards thought for even a second about the long-term interests of their companies, they would summon their lawyers and sign. The United States Needs Corporate 'Loyalty Oaths' |Jonathan Alter |August 4, 2014 |DAILY BEAST 

Ullom had failed to summon help and had instead sought to remedy the situation by injecting her twice with cocaine. The Black Widow of Silicon Valley |Michael Daly |July 14, 2014 |DAILY BEAST 

Now you can summon them instantly on your screen, and track their new life post-you… Is he looking older, more haggard? Psychologists View Both Divorce and Marriage as Major Life Stresses |Emma Woolf |May 12, 2014 |DAILY BEAST 

For the unitiated, Uber is a hugely popular app that allows people to summon a private car with a few taps of their fingers. Uber’s Biggest Problem Isn’t Surge Pricing. What If It’s Sexual Harassment by Drivers? |Olivia Nuzzi |March 28, 2014 |DAILY BEAST 

Thinking to escape and summon assistance from the cantonment, Douglas mounted the wall and leaped into the moat. The Red Year |Louis Tracy 

But in August, 1805, the approaching war with Austria caused the Emperor to summon his most brilliant cavalry leader to his side. Napoleon's Marshals |R. P. Dunn-Pattison 

Out sprang Rabecque, to be immediately sent by his master to summon mademoiselle. St. Martin's Summer |Rafael Sabatini 

You baito where you are,” he commanded, bidding a comrade summon an officer, “or somebody who can talk the lingo. The Red Year |Louis Tracy 

With a half-smile he rose, and going to the door he bade his page who was idling in the anteroom go summon the captain. St. Martin's Summer |Rafael Sabatini