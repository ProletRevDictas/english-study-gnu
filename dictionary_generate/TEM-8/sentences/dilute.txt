Specifically, the commentary discusses the theory of variolation, an pre-vaccine form of inoculation that uses a dilute form of the smallpox virus to give people a mild infection that would lead to lifelong immunity to the virus. Wearing a mask could protect you from COVID-19 in more ways than you think |Kat Eschner |September 10, 2020 |Popular-Science 

Tesla also announced it had sold $5 billion of new shares, diluting the value of stock held by existing investors. Tesla’s stock has plunged 34% in one week |Danielle Abril |September 8, 2020 |Fortune 

Unfortunately, Lo says, in many schools, these systems don’t mix the air well enough to dilute any virus. Here’s how COVID-19 is changing classes this year |Bethany Brookshire |September 8, 2020 |Science News For Students 

Mixing dilutes the indoor air, spreading out any virus it may hold. Here’s how COVID-19 is changing classes this year |Bethany Brookshire |September 8, 2020 |Science News For Students 

Tesla shares are now officially in correction territory, in a week that saw the electric-vehicle maker announce a share sale that could dilute its value and a top holder cut its stake nearly in half. Over the past three days, Tesla’s stock has dropped 18% |Verne Kopytoff |September 3, 2020 |Fortune 

You would have to be drinking LITERS of water to dilute your stomach acid in any meaningful way. Quora Q: Does Drinking Water During Meals Help or Hinder the Digestive System? |Quora Contributor |January 30, 2014 |DAILY BEAST 

The other obvious way to help your body deal with excessive toxins from that large meal is to dilute the toxins by drinking more. How to Recover from Christmas |Dave Asprey |December 25, 2013 |DAILY BEAST 

To dilute the bitterness, the less-than-brave steep them in green tea. The Wildest Hangover Cures From Around the World |Nina Strochlic |November 29, 2013 |DAILY BEAST 

The minute you undermine the insurance, or dilute it, a bank run might ensue. Cyprus on Fire? Blame the German Bullies. |Daniel Gross |March 19, 2013 |DAILY BEAST 

I also refuse to dilute the power of the accusation through inaccurate overuse. Hagel: Not An Anti-Semite, Just A Slob |Gil Troy |December 19, 2012 |DAILY BEAST 

The metal is then removed, and washed successively with very dilute sodium hydroxid solution, alcohol, and ether. A Manual of Clinical Diagnosis |James Campbell Todd 

The remainder of the residue may be dissolved in a little dilute sulphuric acid. A Manual of Clinical Diagnosis |James Campbell Todd 

It dissolves in dilute potash, and on the addition of acetic acid is deposited in a pure state. Elements of Agricultural Chemistry |Thomas Anderson 

The ether solution was washed with water and dilute sodium carbonate solution, and the ether was evaporated. Some Constituents of the Poison Ivy Plant: (Rhus Toxicodendron) |William Anderson Syme 

When the small residue was completely dry, it was a yellow solid soluble in dilute alcohol and acid to litmus. Some Constituents of the Poison Ivy Plant: (Rhus Toxicodendron) |William Anderson Syme