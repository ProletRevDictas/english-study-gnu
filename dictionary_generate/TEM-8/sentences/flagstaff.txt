Floods have hit New York and Flagstaff, Arizona, in recent weeks. Cities are scrambling to prevent flooding |Casey Crownhart |July 20, 2021 |MIT Technology Review 

She stood during the four-hour bus ride to Flagstaff for the team’s first cross-country meet of the 2017 season. Running’s Cultural Reckoning Is Long Overdue |Christine Yu |May 27, 2021 |Outside Online 

George and the Flagstaff crew are there, as are at least five other parties, and my second emotion, after deep relief, is of overcrowding. A father-son backpacking trip in the Grand Canyon is an introduction to adventure |John Briley |April 9, 2021 |Washington Post 

Chad Trujillo is an astronomer at Northern Arizona University in Flagstaff. Signs of a hidden Planet Nine in our solar system may be an illusion |Lisa Grossman |March 31, 2021 |Science News For Students 

Until my home of Flagstaff, Arizona, gets more snow, I’m taking notes on these online classes to prep myself for an avy course come spring. A Beginner’s Guide to Splitboarding |Amelia Arvesen |December 26, 2020 |Outside Online 

I could see a thunderhead boiling up above Flagstaff, Arizona, 350 miles to the west. Sky Wars: Richard Branson’s Rival in the Great Space Race |Tom Sykes |October 7, 2014 |DAILY BEAST 

In 2010, in what it says was the first such effort by a U.S. utility, APS placed panels on 125 homes in Flagstaff—for no charge. Panel Discussion |The Daily Beast |September 8, 2014 |DAILY BEAST 

The Flagstaff fire, though a trifling 300 acres, threatened Boulder and activated a top-level fire team. Colorado Blazes Remind Us That National Policy on Fire Needs a Fix |Stephen J. Pyne |June 29, 2012 |DAILY BEAST 

At the Flagstaff Tower the 74th and the remainder of the 38th suddenly told their officers that they would obey them no longer. The Red Year |Louis Tracy 

She was officially placed in commission with Old Glory flying proudly at her flagstaff on the 5th day of May, 1919. A Brief History of the U. S. S. Imperator, one of the two Largest Ships in the U. S. Navy. |Anonymous 

There seemed to be a blockhouse on shore, and a kind of earthwork, near which was a flagstaff, but no flag was exhibited. The British Expedition to the Crimea |William Howard Russell 

We could see, ascending the great flagstaff at the end of its halyard, the broad folds of the flag. The Way of a Man |Emerson Hough 

The Federal flag had been struck some time before, and the flagstaff now stood gaunt and undecorated. The Relief of Mafeking |Filson Young