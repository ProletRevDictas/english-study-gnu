He made but one fierce and rapid irruption into the neighborhood of the "red sea," and returned sick and shuddering therefrom. Harper's New Monthly Magazine, No. VII, December 1850, Vol. II |Various 

Came a great irruption of new social, religious, and political ideas into the general European mind. The Outline of History: Being a Plain History of Life and Mankind |Herbert George Wells 

By the retreat of Torstensohn, the Emperor was relieved from all fears of an irruption on the side of Bohemia. The Thirty Years War, Complete |Friedrich Schiller 

The irruption of Bennie and Zephyr threatened disaster even to this forlorn hope. Blue Goose |Frank Lewis Nason 

Tamerlane made his irruption through this plain when he took and destroyed so many cities. Early Travels in Palestine |Arculf et al.