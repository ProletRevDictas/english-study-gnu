They are also odor-resistant and get increasingly softer over time. Best weighted blanket: Sleep like a baby with our comfy bedding picks |PopSci Commerce Team |February 12, 2021 |Popular-Science 

The artist can’t turn lead into gold, but making the hard gray substance appear soft and lacy is almost as impressive. In the galleries: Rejuvenating the obsolete into unconventional art |Mark Jenkins |February 12, 2021 |Washington Post 

The center will still be quite soft and slightly dipped, but the sides will be set. If loving a piping hot chocolate lava cake is wrong, I don’t want to be right |Becky Krystal |February 4, 2021 |Washington Post 

Tough leather palms make them perfect for cold-weather tasks at home or digging your car out of a snowbank, while a thin construction with touchscreen functionality, articulated fingers, and a breathable soft-shell back mean it can work as a liner. My Favorite Winter Gloves for Various Activities |Jakob Schiller |February 3, 2021 |Outside Online 

Giving up alcohol and coffee was easier than sticking to my soft diet, in fact, because I kept trying to force every meal into a “healthy” form. Six Months of Soft Food Set Me Free |Meghan McCarron |February 2, 2021 |Eater 

There were stomachs, taut and flat, but also undulating bellies, soft and bloated from the breakfast buffet. Powerful Congressman Writes About ‘Fleshy Breasts’ |Asawin Suebsaeng |January 7, 2015 |DAILY BEAST 

Francis is well into his seventies, looks it, has a mild demeanor and soft speaking style; but his rhetoric is electrifying. How Pope Francis Became the World’s BFF |Jason Berry |December 21, 2014 |DAILY BEAST 

My surgeon told me my bones were so soft he could barely install the screws. You’re Never ‘Cured’ of an Eating Disorder |Carrie Arnold |December 20, 2014 |DAILY BEAST 

Of course, with such a soft-handed approach comes criticism from the Danish right. What the U.S. Can Learn from Europe About Dealing with Terrorists |Scott Beauchamp |December 15, 2014 |DAILY BEAST 

And Christopher Walken warbling and doing a little soft-shoe? ‘Peter Pan Live!’ Review: No Amount of Clapping Brings It to Life |Kevin Fallon |December 5, 2014 |DAILY BEAST 

And once more, she found herself desiring to be like Janet--not only in appearance, but in soft manner and tone. Hilda Lessways |Arnold Bennett 

A few small rocks of some soft stone may be added, and in between these the Ferns are planted. How to Know the Ferns |S. Leonard Bastin 

His face flushed with annoyance, and taking off his soft hat he began to beat it impatiently against his leg as he walked. The Awakening and Selected Short Stories |Kate Chopin 

The delicious soft rains set in early, promising a good grain year. Ramona |Helen Hunt Jackson 

Not a zephyr ruffled the leaf of a rose, and a soft breathing fragrance bathed his reposing senses. The Pastor's Fire-side Vol. 3 of 4 |Jane Porter