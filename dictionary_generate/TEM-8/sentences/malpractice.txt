Speaking to state legislators on March 26, the head of the medical society, Chip Baggett, said his members didn’t yet have the resources they needed and were anxious about the potential for malpractice lawsuits. The Nursing Home Didn’t Send Her to the Hospital, and She Died |by Sean Campbell |January 8, 2021 |ProPublica 

A government investigation revealed several malpractices and Apple has since pressed pause on business with the Taiwanese company. Apple’s first iPhone supplier in India stretched its workers too far—and they snapped |Ananya Bhattacharya |December 21, 2020 |Quartz 

Suddenly advertisements pop up for medical malpractice lawyers, but you haven’t told anyone about the surgery and you certainly didn’t post about it on social media. The downfall of adtech means the trust economy is here |Walter Thompson |November 23, 2020 |TechCrunch 

So, there will be claims associated with medical malpractice because people will believe patients haven’t been treated appropriately for the pandemic, for instance. Many Businesses Thought They Were Insured for a Pandemic. They Weren’t. (Ep. 437) |Stephen J. Dubner |October 29, 2020 |Freakonomics 

To play it poorly, or not at all, is to commit institutional malpractice. Why I’m giving up my board seat to make room for someone from an underrepresented community |jakemeth |September 21, 2020 |Fortune 

If there were a pill with such poor efficacy, it might be considered malpractice to prescribe it. ‘The Biggest Loser’ Could Be TV’s Most Important Show Ever |Daniela Drake |September 26, 2014 |DAILY BEAST 

But the malpractice system is not robust in China, and patients feel powerless. Will US Health Care Follow in China’s Bloody Footsteps? |Daniela Drake |September 21, 2014 |DAILY BEAST 

And an overzealous medical professional, hoping to safeguard against malpractice, can also be a cause of unneeded procedure. Are Routine Scans Causing Cancer? |Dale Eisinger |September 17, 2014 |DAILY BEAST 

Parents who bring wrongful birth suits seem to face a burden faced by no other plaintiffs in medical malpractice cases. Parents Sue for 'Wrongful Birth' |Elizabeth Picciuto |August 17, 2014 |DAILY BEAST 

The burden on plaintiffs in wrongful birth cases “is unique, and is inconsistent with other types of malpractice cases,” she said. Parents Sue for 'Wrongful Birth' |Elizabeth Picciuto |August 17, 2014 |DAILY BEAST 

But it is certainly true that the State has the right to prevent malpractice—a right none of us would wish renounced. The Arena |Various 

Driven from San Francisco for malpractice, he turned up in Denver, where he again aroused the authorities to action. John Marsh's Millions |Charles Klein 

Upon inquiry I found the lawyer was but just disbarred for some malpractice, and the discovery added excessively to my disquiet. The Works of Robert Louis Stevenson - Swanston Edition Vol. 13 (of 25) |Robert Louis Stevenson 

But for this very reason no doctor dare accuse another of malpractice. The Doctor's Dilemma: Preface on Doctors |George Bernard Shaw 

Only monsters smoke at meals, but a monster assured me that Gorgonzola best survives this malpractice. The Complete Book of Cheese |Robert Carlton Brown