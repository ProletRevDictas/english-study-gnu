France and Germany’s legal codes permit a form of universal jurisdiction, which allows their national courts to prosecute individuals accused of heinous offenses committed in any county. Syrian Medics Lay Out the Devastating Scale of Attacks on Healthcare Facilities Over 10 Years of War |Joseph Hincks |March 3, 2021 |Time 

Those Republicans who defended him and his heinous acts will find history holds them to account. Impeachment trial worth holding for history |Peter Rosenstein |February 18, 2021 |Washington Blade 

Doctors, the FBI and local law enforcement are all called in to not only try to solve a medical mystery, but also a heinous crime. What to watch on Thursday: “Locked Down” on HBO Max |Nina Zafar |January 14, 2021 |Washington Post 

This is a heinous and violent assault on the heart of our democracy. Woman dies after shooting in U.S. Capitol; D.C. National Guard activated after mob breaches building |Washington Post Staff |January 7, 2021 |Washington Post 

Environmental criminals, authoritarian regimes, tax evaders and financial criminals, drug traffickers, wildlife poachers, and gun runners—all those responsible for the most heinous crimes have turned to anonymous shell companies. Congress just passed the most important anti-corruption reform in decades, but hardly anyone knows about it |jakemeth |December 26, 2020 |Fortune 

“Today, we are before a heinous crime the likes of which are unprecedented in our safe country,” he said. Abu Dhabi Treats U.S. Teacher’s Murder as Terrorist Attack |Chris Allbritton |December 4, 2014 |DAILY BEAST 

How was he and the brothers Bridgman found guilty, without any physical evidence tying them to this heinous crime? For Ricky Jackson, a Just Verdict—But 39 Years Too Late |Cliff Schecter |November 26, 2014 |DAILY BEAST 

The Israeli response to the heinous crimes committed by Palestinians often seems overkill. Intifada 3.0: Growing Unrest and a Plot to Kill an Israeli Minister |Creede Newton |November 21, 2014 |DAILY BEAST 

Purists sometimes seem to think that disregarding rules about prepositions is as heinous as torturing children. Go Ahead, End With a Preposition: Grammar Rules We All Can Live With |Nick Romeo |November 3, 2014 |DAILY BEAST 

Every year—maybe every month—America is disgraced with an especially heinous lawsuit. How One Lawsuit Shows What’s Wrong With America |James Poulos |October 20, 2014 |DAILY BEAST 

He was thrashed at school before the Jews and the hubshi, for the heinous crime of bringing home false reports of progress. Kipling Stories and Poems Every Child Should Know, Book II |Rudyard Kipling 

It was not a heinous sin, nor would it affect his moral character. A Final Reckoning |G. A. Henty 

Next she was heard discussing and excusing the most heinous crimes of which human nature can be guilty. Choice Readings for the Home Circle |Anonymous 

Still more heinous was the verdict based upon evidence which, if enough in quantity, was manifestly worthless in quality. Sir Walter Ralegh |William Stebbing 

All three were asked if their offences were not heinous, and if they had not been justly tried and lawfully condemned. Sir Walter Ralegh |William Stebbing