Meanwhile, Mikhail had had several fitful starts before he finally rebuilt his career as an entrepreneur. What It’s Like for Ukrainians Forced to Flee Russian Invasions Twice in 8 Years |Sasha Vasilyuk |March 31, 2022 |Time 

After months of fitful supply, India’s Covid-19 vaccination program has finally reached a happy momentum. India is making a tough choice between global and domestic responsibility for Covid-19 vaccines |Manavi Kapur |September 13, 2021 |Quartz 

Every day we spent 16 hours on small dugout canoes and then got eight hours of fitful sleep on the deck of a Melanesian sea craft. Twilight of the Nautilus - Issue 104: Harmony |Peter Ward |August 11, 2021 |Nautilus 

There have been fitful mutterings about Catalonia, the region of Spain where Barcelona is, seceding from Spain. Homage to Catalonia |Megan McArdle |December 19, 2012 |DAILY BEAST 

So squandering away decades of a sterling credit rating over a “contentious and fitful process” will seem petty. Downgraded For Bad Behavior |John Solomon |August 6, 2011 |DAILY BEAST 

He made some fitful attempts to calm down a bit, filling modest, collage-like monochromatic surfaces with angular shapes. Death of a Modernist Master |Simon Schama |July 6, 2011 |DAILY BEAST 

The stress, compounded by fitful rest and sleepless nights, ages you quickly. Trust Me, Dominique, Don't Run |Mansfield Frazier |May 18, 2011 |DAILY BEAST 

Hossein breathed heavily as he fell in and out of a fitful sleep. When Everything Changed |Gayle Tzemach Lemmon |March 11, 2011 |DAILY BEAST 

It was still raining—a gray day with fitful showers that never entirely ceased but only varied in intensity. British Highways And Byways From A Motor Car |Thomas D. Murphy 

Down below there are only fitful puffs now and then, telling of something else in store. The Floating Light of the Goodwin Sands |R.M. Ballantyne 

In the fitful firelights gleam they could see three shadowy figures crossing the creek. The Adventure Girls at K Bar O |Clair Blank 

In the soft wind the myrtle rustled faintly, and on the roses at our feet the dew-drops glinted in fitful splendor. A Fortune Hunter; Or, The Old Stone Corral |John Dunloe Carteret 

I hope he slept better than I did, for my own night was a series of fitful, restless tossings. The Diary of Philip Westerly |Paul Compton