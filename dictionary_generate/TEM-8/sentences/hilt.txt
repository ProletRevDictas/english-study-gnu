The film merely reaffirms that he’s an ambitious entrepreneur who saw a financial opportunity and exploited it to the hilt, and then doubled-down when calls for his head rang out. New Documentary Confirms Martin Shkreli Is Just an Evil Little Prick |Nick Schager |September 27, 2021 |The Daily Beast 

The grave also included jewelry traditionally associated with women and two swords, including one with a bronze hilt, typically attributed to men. A 1,000-year-old grave may have held a powerful nonbinary person |Bruce Bower |August 16, 2021 |Science News 

In a panic, I grabbed the hilt of my sword, ready to cut down my foe. I Went Camping as My Dungeons & Dragons Character |smurguia |July 15, 2021 |Outside Online 

When I heard the snap of branches coming from the darkness surrounding our camp, my hand tightened around the hilt of my sword. I Went Camping as My Dungeons & Dragons Character |smurguia |July 15, 2021 |Outside Online 

All sleek and tightly constructed, all packed to the hilt with quality character actors, all the type of films you’d happily watch for half an hour before realizing you’d already rented them before. The surprisingly durable second act of Liam Neeson |Scott Tobias |June 25, 2021 |Washington Post 

The premise of the sketch was that sex was too spontaneous to be regulated, and the quiz show played that idea to the hilt. How Antioch College Got Rape Right 20 Years Ago |Nicolaus Mills |December 10, 2014 |DAILY BEAST 

He played affirmative action to the hilt and took full advantage of it. Herman Cain Used the Affirmative Action He Decries to Advance. |Wayne Barrett, Andy Ross |December 2, 2011 |DAILY BEAST 

"He had enthusiasm to the hilt," said his niece, Kathleen Kennedy Townsend. Sargent Shriver's America |Adam Clymer |January 18, 2011 |DAILY BEAST 

Since then, Becky Hilt says she avoids eye contact with Thomas whenever he walks his dog. Teen Hostage Horror |Lynn Waddell |September 24, 2010 |DAILY BEAST 

His open brow lowered, and his fingers instinctively began playing with the hilt of his sabre. Blackwood's Edinburgh Magazine, No. CCCXXXIX. January, 1844. Vol. LV. |Various 

George, seeing no living soul, struck the hilt of his sword sharply against the door of the castle. Honey-Bee |Anatole France 

Mary stepped beside him, took the hilt in both her little hands, and made shift to raise the great sword. God Wills It! |William Stearns Davis 

In the traditions of Denmark, the oath upon the sword-hilt was preserved in a spirit of deep solemnity. A Cursory History of Swearing |Julian Sharman 

But ten thousand saw Musa's hand clap to hilt, and Iftikhar's lance half fall to rest. God Wills It! |William Stearns Davis