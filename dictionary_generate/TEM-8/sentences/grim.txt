The Council is also set to hear from city staff Tuesday on the grim five-year outlook for funding all the necessary improvements to the city’s roads, streets, drains, pipes and all the rest. San Diego’s Infrastructure Deficit Is Really a Stormwater Deficit |Andrew Keatts |February 8, 2021 |Voice of San Diego 

It’s because of surfing that I still know how to smile when things are otherwise grim. The Spark After the Darkness |Bonnie Tsui |February 6, 2021 |Outside Online 

Regulators increasingly viewed Amazon as a threat to competition, and the company’s own workers at times told grim tales about their mistreatment, as they sought to carry out Bezos’s mission to create a consumer-first “everything store.” Jeff Bezos stepping down as Amazon CEO, transitioning to executive chair role |Jay Greene, Tony Romm |February 4, 2021 |Washington Post 

He warned it will be a months-long and difficult process to distribute vaccines, and characterized himself as a “straight shooter” for offering that grim timeline. Hogan urges resilience in State of State speech, warns of grim vaccination timeline |Erin Cox, Ovetta Wiggins |February 4, 2021 |Washington Post 

If one can summon any optimism nearly a year into a grim and persistent pandemic, this is the moment to do it. Welcome to the “New” Issue of The Highlight |Vox Staff |February 1, 2021 |Vox 

These were conversations that took a fairly grim twist pretty quickly. I Tried to Warn You About Sleazy Billionaire Jeffrey Epstein in 2003 |Vicky Ward |January 7, 2015 |DAILY BEAST 

The grim instability of shelter life is hardly a recipe for success under the best of circumstances. His First Day Out Of Jail After 40 Years: Adjusting To Life Outside |Justin Rohrlich |January 3, 2015 |DAILY BEAST 

But if the goal is to maintain any hope—grim as it is— for serious negotiations leading to a two state solution. Why We Should Delay The Israel-Palestinian Peace Process |Aaron David Miller |December 19, 2014 |DAILY BEAST 

Alan Gross was in a cheery mood, having survived a grim five-year stint in a Cuban prison. Castro's Hipster Apologists Want to Keep Cuba ‘Authentically’ Poor |Michael Moynihan |December 18, 2014 |DAILY BEAST 

The worst may be over for many of the refugees, but their first look at life in Europe is pretty grim. Inside the Smuggling Networks Flooding Europe with Refugees |Barbie Latza Nadeau |December 15, 2014 |DAILY BEAST 

But Ulm was only the commencement of the campaign, and even after Austerlitz Napoleon pursued the enemy with grim resolution. Napoleon's Marshals |R. P. Dunn-Pattison 

War turns them from making the glittering superfluities of peace to making its grim engines of destruction. The Unsolved Riddle of Social Justice |Stephen Leacock 

As he read, a look of surprise came over his face, and then his countenance grew stern and grim. The Courier of the Ozarks |Byron A. Dunn 

Taking his stand at the end of the desk, he made MacRae reiterate in detail the grim happenings of that night. Raw Gold |Bertrand W. Sinclair 

By every art known to the wily Porter did he try to mislead his pursuers; but they hung on to his trail like grim death. The Courier of the Ozarks |Byron A. Dunn