The team first found MRSA in hedgehogs by coincidence years ago when biologist Sophie Rasmussen, who was part of the new work and is now at the University of Oxford, approached Larsen’s team about sampling a freezer full of dead hedgehogs. Drug-resistant bacteria evolved on hedgehogs long before the use of antibiotics |Carolyn Wilke |January 7, 2022 |Science News 

Sea bass cooked low and slow with lavender and lime and delivered on steel-cut oats and hedgehog mushrooms is one of those combinations that make you go “huh” when you read about it and “whoa!” 2021 Fall Dining Guide |Tom Sietsema |October 6, 2021 |Washington Post 

The call came in so fast, we had just been having a tea party with your hedgehog. A Mother’s Day letter to my daughter, after a year that changed our family forever |Alexandra Moe |May 7, 2021 |Washington Post 

Researchers tested hundreds of animals in and around the market for the coronavirus — including animals for sale such as rabbits, hedgehogs, salamanders and birds — but none tested positive. 4 takeaways from the WHO’s report on the origins of the coronavirus |Erin Garcia de Jesús |April 1, 2021 |Science News 

Kate Ryder, founder and CEO, Maven ClinicThe best book that has helped me in my career involves foxes and hedgehogs. Book recommendations from Fortune’s 40 under 40 in health |Rachel King |September 9, 2020 |Fortune 

Hedgehog Birthday From the folks who brought you ‘Tiny Hamsters Eating Tiny Burritos’ comes another squee-worthy video. Hedgehog Birthday, ‘Frozen Is the New Black,’ and More Viral Videos |Alex Chancey |July 12, 2014 |DAILY BEAST 

As you might have already guessed, a tiny hedgehog has a tiny birthday party. Hedgehog Birthday, ‘Frozen Is the New Black,’ and More Viral Videos |Alex Chancey |July 12, 2014 |DAILY BEAST 

DuBois was a man of many ideas; Washington, of just one: the proverbial contrast of hedgehog and fox. David's Book Club: The Souls of Black Folk |David Frum |May 5, 2013 |DAILY BEAST 

Once again, the “hedgehog,” as Jeremy is known in the XXX industry, gave hope to round, hairy men everywhere. Harry Potter Hip-Hop |Jaimie Etkin |July 13, 2011 |DAILY BEAST 

“I used to remember drawing Sonic the Hedgehog when I was super young,” says Moss. Olly Moss: The Savior of Movie Posters |Marlow Stern |May 9, 2011 |DAILY BEAST 

Randolph, whose men were on foot, instantly threw them into a schiltron, 'like a hedgehog.' King Robert the Bruce |A. F. Murison 

A hedgehog crept stealthily along the ground, and at a sudden sound curled himself up like a wee brown bear. Penelope's Experiences in Scotland |Kate Douglas Wiggin 

But even with the sharp eyes, and the sharp, stickery-ickery quills of the hedgehog, Uncle Wiggily couldn't find his fortune. Uncle Wiggily's Travels |Howard R. Garis 

A hedgehog is a rough and prickly fellow—better his prickles than the reptile's poisonous slime. The Secret Glory |Arthur Machen 

He is very timid, and when any one approaches him, coils himself up in a ball, like a hedgehog. The Desert World |Arthur Mangin