Once again, it will take a Democratic president and Congress to fix the economic mess created by the outgoing Republican administration. Vote for Biden (duh) |Kevin Naff |September 10, 2020 |Washington Blade 

Now, with programmatic ad rates rebounding, outgoing CEO Jim Egan is optimistic about the prospect of ad-funded news. ‘We’re getting more used to the uncertainty’: BBC Global News chief on ad-funded news |Lucinda Southern |September 10, 2020 |Digiday 

It is made up of three layers to filter incoming and outgoing particles. Apple has developed special face masks for employees |Verne Kopytoff |September 9, 2020 |Fortune 

It's a much faster car to build than the outgoing model, with a stiffer chassis and sharper panel creases. Acura builds a stiffer chassis and sharper creases into the new TLX |Jonathan M. Gitlin |September 4, 2020 |Ars Technica 

He said that letter carriers returning with outgoing mail at the end of their routes usually handed them straight to mail sorters on overtime, he said. Info About Local Post Office Operations Is Conflicting and Hard to Come By |Ashly McGlone and Kate Nucci |August 27, 2020 |Voice of San Diego 

Families stuff a life-size male doll with memories of the outgoing year and dress him in their clothing. New Year’s Eve, Babylon Style |Candida Moss |December 31, 2014 |DAILY BEAST 

Sabrine was the outgoing, sociable type, and had many friends, while Ziad was shy and a little more introverted. A Sunni-Shia Love Story Imperiled by al Qaeda |Ruth Michaelson |December 26, 2014 |DAILY BEAST 

Meet the outgoing Michigan Republican congressman who switched his vote and kept the government funded Thursday. Quirky Reindeer Farmer Keeps Government Open for Christmas |Ben Jacobs |December 11, 2014 |DAILY BEAST 

But the President could easily reposition it as a friendly “pro-gun rights” gesture by outgoing Attorney General Eric Holder. The GOP Senate: A New Utopia Dawns |P. J. O’Rourke |November 8, 2014 |DAILY BEAST 

In the midst of these efforts, she was raising identical twin sons, Julian—“the reserved one” and Joaquin—“the outgoing one.” Mother Knows Best |Sara Lieberman |October 14, 2014 |DAILY BEAST 

On the fancied outgoing, the observer would pass the interval between the sun and the earth in about eight minutes. Outlines of the Earth's History |Nathaniel Southgate Shaler 

As it was, for some reason they could not ascertain, the outgoing train was over an hour late in starting. The Outdoor Girls in the Saddle |Laura Lee Hope 

There is certainly a marked distinction between the oaths of the outgoing and incoming creeds. A Cursory History of Swearing |Julian Sharman 

And something within me, something passionate surrendered myself to it, and was borne away upon it as by an outgoing tide. The Romance of His Life |Mary Cholmondeley 

She herself had been most carefully trained concerning manners of incoming and outgoing. The Butterfly House |Mary E. Wilkins Freeman