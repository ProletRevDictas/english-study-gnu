There would be less total health care, less child care, less energy available to households, and less value added in the university sector. Does Anyone Really Know What Socialism Is? (Ep. 408 Rebroadcast) |Stephen J. Dubner |September 17, 2020 |Freakonomics 

The report says a record 765 hospitals and other health care facilities participated in the annual Healthcare Equality Index survey at a time when they faced unprecedented challenges in caring for patients in the midst of the coronavirus pandemic. HRC examines hospital policies, impact of COVID on LGBTQ people |Lou Chibbaro Jr. |September 16, 2020 |Washington Blade 

Those who say health care is their single top issue back Biden 77 percent to 19 percent. Post-ABC Wisconsin poll shows Biden holding narrow edge over Trump |Dan Balz, Emily Guskin |September 16, 2020 |Washington Post 

Regulators and health care leaders, backed up by political leaders, must take the lead in building public confidence in the integrity of the entire vaccine development and distribution process. 6 questions that must be answered in the race for a vaccine |jakemeth |September 15, 2020 |Fortune 

Feigenholtz said she wants the agency to establish benchmarks on moving the children out of the hospitals and increasing specialized foster care, then report back to legislators. Still No Answers to Lawmakers’ Questions About Children Stuck in Psychiatric Hospitals |by Duaa Eldeib |September 15, 2020 |ProPublica 

Placed in drinking water, fluoride can serve people who otherwise have poor access to dental care. Anti-Fluoriders Are The OG Anti-Vaxxers |Michael Schulson |July 27, 2016 |DAILY BEAST 

He has wild swings between trying not to care about Lana and the baby, and being completely obsessed by it. ‘Archer’ Creator Adam Reed Spills Season 6 Secrets, From Surreal Plotlines to Life Post-ISIS |Marlow Stern |January 8, 2015 |DAILY BEAST 

Who among Scalise's constituents could possibly care if he supported naming a post office for a black judge who died in 1988? The Price of Steve Scalise’s Silence |Jason Berry |January 7, 2015 |DAILY BEAST 

Haringey Council told The Daily Beast that the children had not been taken permanently into state care. Britain May Spy on Preschoolers Searching for Potential Jihadis |Nico Hines |January 7, 2015 |DAILY BEAST 

In these regions, men are now doing between 30 and 45 percent of the care work. How Good Dads Can Change the World |Gary Barker, PhD, Michael Kaufman |January 6, 2015 |DAILY BEAST 

I do not care very much how you censor or select the reading and talking and thinking of the schoolboy or schoolgirl. The Salvaging Of Civilisation |H. G. (Herbert George) Wells 

And this summer it seemed to her that she never would be able to take proper care of her nestful of children. The Tale of Grandfather Mole |Arthur Scott Bailey 

Polavieja, as everybody knew, was the chosen executive of the friars, whose only care was to secure their own position. The Philippine Islands |John Foreman 

On this account, great care should be taken to provide well-drained positions. How to Know the Ferns |S. Leonard Bastin 

You never cared—you were too proud to care; and when I spoke to you about my fault, you did n't even know what I meant. Confidence |Henry James