“The charter does not give the city attorney the power to invalidate a City Council action by withholding her signature for that reason or any reason other than legal sufficiency,” Elliott spokeswoman Leslie Wolf Branscomb wrote in an email. Gloria, City Attorney Disagree on Best Path for 101 Ash Suits |Lisa Halverstadt |June 21, 2022 |Voice of San Diego 

But such an approach works against the traditional pride in self-sufficiency espoused by many in the American middle class. Time to Bring Back the Truman Democrats |Joel Kotkin |December 21, 2014 |DAILY BEAST 

Domestically, the former governor offers an avowedly nationalistic agenda, focused on American self-sufficiency. Iowa Frontrunner Mike Huckabee Talks to The Daily Beast |Lloyd Green |September 22, 2014 |DAILY BEAST 

Academic studies have found that mental health parenting evaluations often take this self-sufficiency view of parenting. One Breakdown Can Mean Losing Your Kid Forever |ProPublica |May 30, 2014 |DAILY BEAST 

Food in Calabria was pride, self-sufficiency and community all mixed together in one mouthful. A Young Chef Travels to Calabria, Italy, and Learns the Old Ways of Cooking |Curtis Stone |November 28, 2013 |DAILY BEAST 

I feel conservative in terms of limited government, individual responsibility, self-sufficiency — that sort of thing. Ted Olson, the Conservative Republican Who Argued for Marriage Equality at the Supreme Court |Justin Green |March 26, 2013 |DAILY BEAST 

David had replied, in that short tone of self-sufficiency which conveys so much more than the syllable would seem to warrant. The Garret and the Garden |R.M. Ballantyne 

Some are merely well off, others immensely wealthy—with a sufficiency invested elsewhere. Ancestors |Gertrude Atherton 

Who can understand its nature, its operations, the sufficiency which is not sufficient, and the efficacy which is ineffectual. A Philosophical Dictionary, Volume 1 (of 10) |Franois-Marie Arouet (AKA Voltaire) 

It is to enable this class of readers to test the quality and sufficiency of their belief that this book has been prepared. Man And His Ancestor |Charles Morris 

The unfortunate peasant frequently has scarcely a sufficiency left to keep life in himself and his family. A Woman's Journey Round the World |Ida Pfeiffer