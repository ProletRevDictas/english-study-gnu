I most often use this technique for filets of fish, but it’s also great for developing crispy bits on a tray of roasted cabbage or any number of other vegetables. Don’t Fear the Broiler |Elazar Sontag |February 11, 2021 |Eater 

Journalist Anne Applebaum, formerly of The Washington Post, drew this similarity with her piece in the Atlantic last week that compared the vaccine rollout and scramble for appointments in Maryland to a Soviet line for cabbage. It’s Prague on the Potomac, as we wearily wait for a shot at the vaccine |Petula Dvorak |February 8, 2021 |Washington Post 

It extends the life of vegetables by days, or, in the case of cabbage, weeks. From the Cut: 33 Valentine’s Day Gifts for the Foodie in Your Life |The Cut Staff |February 8, 2021 |Eater 

You add chopped carrot and cabbage to the rice and stock, and then cook it all up in the rice maker’s porridge setting. 20 Days of Turkey |Meghan McCarron |December 23, 2020 |Eater 

Potatoes, peas, cabbage and garlic all contribute to the hearty, healthy flavor, with some pasta just for the heck of it. This Weekend: Escape This Messy World With a Memoir |Joshua Eferighe |November 6, 2020 |Ozy 

There was also the grapefruit diet, the cabbage soup diet, and the cookie diet. Why Your New Year’s Diet Will Fail |Carrie Arnold |December 30, 2014 |DAILY BEAST 

The cooking odors of cabbage and meatloaf and carrots drifted through doorways. Football Great Bob Suffridge Wanders Through the End Zone of Life |Paul Hemphill |September 6, 2014 |DAILY BEAST 

The stewed cabbage is insanely tender, vegetable-sweet, and more luxurious than cabbage has a right to be. The Heart and Soul (Food) of Orlando |Jane & Michael Stern |June 8, 2014 |DAILY BEAST 

Is she back in the orphanage where it smells like ammonia and cooked cabbage? When An Adopted Child Won’t Attach |Tina Traster |May 2, 2014 |DAILY BEAST 

But equally super are Brussels sprouts, broccoli and cabbage. The Dangers of Superfoods |Lizzie Crocker |April 9, 2014 |DAILY BEAST 

What of the infinite goodness of God in teaching the grub of the ichneumon-fly to eat up the cabbage caterpillar alive? God and my Neighbour |Robert Blatchford 

Beans and bacon, cabbage and brown hard dumplings, formed the bill of fare, which the men washed down with plenty of table beer. The World Before Them |Susanna Moodie 

They never do anything but eat cabbage and cause gardeners to use bad language. The Whale and the Grasshopper |Seumas O'Brien 

And they both think of the cabbage soup steaming in the pot that hangs from the hook right under the great chimney. Child Life In Town And Country |Anatole France 

When she reached the parlour where the cabbage soup was smoking on the table, Catherine shivered again. Child Life In Town And Country |Anatole France