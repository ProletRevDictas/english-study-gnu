Even our hospitals are being taken over and hollowed out by Wall Street. Why the Democratic Party must make a clean break with Wall Street |matthewheimer |September 8, 2020 |Fortune 

For some, the announcement back in March that he would pick a woman started to ring hollow the longer the search dragged on. Biden’s Pledge To Choose A Woman For VP Was Popular — And So Is Harris |Geoffrey Skelley (geoffrey.skelley@abc.com) |August 14, 2020 |FiveThirtyEight 

These hornets nest in hollow trees and cozy nooks within walls. What you need to know about ‘murder hornets’ |Susan Milius |July 20, 2020 |Science News For Students 

He liked to play in a hollow tree where insect-eating bats used to live. How to find the next pandemic virus before it finds us |Lindsey Konkel |April 30, 2020 |Science News For Students 

The explanation for these landmines mainly related to the use of hollow cast iron balls that were presumably filled with gunpowder. 14 Exceptional Weapon Systems from History That Were Ahead of their Time |Dattatreya Mandal |March 26, 2020 |Realm of History 

The young man weaves through clusters of bamboo and cuts a diagonal slash into a tree, positioning a hollow log at the end. The Congo's Forgotten Colonial Getaway |Nina Strochlic |December 18, 2014 |DAILY BEAST 

But Byrne himself is the parodist, and he commands the stage by his hollow-eyed, frosty verve. The Stacks: Pauline Kael's Talking Heads Obsession |Pauline Kael |November 22, 2014 |DAILY BEAST 

He pressed a hollow shell casing into my palm and leaned towards my ear, “I PICKED IT UP FROM THE BEDROOM!” I Shot Bin Laden |Elliot Ackerman |November 16, 2014 |DAILY BEAST 

It will always ring hollow when people try to turn it into a product—no matter how much Bill Clinton gushes over it. How One Lawsuit Shows What’s Wrong With America |James Poulos |October 20, 2014 |DAILY BEAST 

Last year Fox gifted the world with the “Legend of Sleepy Hollow” retelling we never knew we needed. Naked Ben Franklin Christens the Campy Return of ‘Sleepy Hollow’ |Amy Zimmerman |September 23, 2014 |DAILY BEAST 

The next instruments discovered in use among the Indians were straight, hollow reeds and forked canes. Tobacco; Its History, Varieties, Culture, Manufacture and Commerce |E. R. Billings. 

It lit up every ridge and hollow for two or three seconds, and showed me four riders tearing up the slope at a high run. Raw Gold |Bertrand W. Sinclair 

The formula would be: “The pump invented—Drain a well ,” or Water raised in a hollow. Assimilative Memory |Marcus Dwight Larrowe (AKA Prof. A. Loisette) 

But, just as they were piling some more leaves in the hollow stump, they heard many voices of men shouting in the woods. Squinty the Comical Pig |Richard Barnum 

That hollow is a very likely place for one of them to run along, therefore the best shot among you had better go up there. Hunting the Lions |R.M. Ballantyne