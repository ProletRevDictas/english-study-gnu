The specimen dates back 50 million years to the Eocene epoch, meaning this particular taxonomic group may be twice as old as scientists previously assumed. Rare 50 million-year-old fossilized bug flashes its penis for posterity |Jennifer Ouellette |February 4, 2021 |Ars Technica 

The key to fine pruning is to end up with a specimen that is groomed and structurally sound but still looks natural. It’s tree-pruning season. Here’s how to do it without resorting to ‘crape murder.’ |Adrian Higgins |January 27, 2021 |Washington Post 

His 2016 NBA Finals heroics made him a champion, and his signature shoes have the best design concepts of the three, but he is neither a comparable scorer nor physical specimen. The Nets got better with the James Harden trade. How much better is up to Kyrie Irving. |Jerry Brewer |January 14, 2021 |Washington Post 

The oldest specimens studied by the La Jolla team were obtained about nine months ago, Weiskopf said. Post-infection coronavirus immunity usually robust after 8 months, study shows |Joel Achenbach |January 7, 2021 |Washington Post 

Although these policies apply to USGS scientists, they’re a good model of specimen stewardship for the entire scientific community. How fossil preservation and public health are intertwined |By Colella & McLean/The Conversation |December 18, 2020 |Popular-Science 

I just love the fact that they believe me to be enough of a physical specimen to be a part of those organizations! Jon Stewart Talks ‘Rosewater’ and the ‘Chickensh-t’ Democrats’ Midterm Massacre |Marlow Stern |November 9, 2014 |DAILY BEAST 

He loved planting unusual trees, shrubs, and flowers throughout the grounds, and carefully directed where each specimen would go. When Gary Wright Met George Harrison: Dream Weaver, John and Yoko, and More |Gary Wright |September 29, 2014 |DAILY BEAST 

That gave participants an opportunity to try taxidermy without skinning the specimen, which some people find hard to stomach. Edible Taxidermy: It’s a Good Thing |Lizzie Crocker |August 5, 2014 |DAILY BEAST 

Under Mongolian law, any fossil specimen found in the Gobi Desert must stay in the possession of a Mongolian institution. Stopping the Million-Dollar Fossil Thieves: Illegal Trade Meets World of Insatiable Research |Scott Bixby |June 11, 2014 |DAILY BEAST 

Eventually, several people squat over a newspaper in the hopes of matching a fresh specimen with the one in question. Why ‘It’s Always Sunny’ Is Funny: An Examination of Scenes, Stripped of Context |Caitlin Dickson |November 10, 2013 |DAILY BEAST 

As a rule, however, even in the case of extreme varieties, a careful examination of the specimen will enable it to be identified. How to Know the Ferns |S. Leonard Bastin 

A moderately enlarged testis (probably regressing) was noted in the specimen from Pisté, on July 21. Summer Birds From the Yucatan Peninsula |Erwin E. Klaas 

Miss Thangue sat forward with the frank curiosity of the Englishwoman when inspecting a foreign specimen. Ancestors |Gertrude Atherton 

"A beautiful specimen; a man of great force," I unburdened myself when we got outside. Raw Gold |Bertrand W. Sinclair 

There was one honest dog in that company, but the two-legged specimen was a little "too sweet to be wholesome." The Book of Anecdotes and Budget of Fun; |Various