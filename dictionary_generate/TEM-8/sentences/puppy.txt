An 8-week-old puppy is roughly equal to a 9-month-old baby, but a year-old dog is closer to a 31-year-old human. Here’s the summer science you might have missed |Janet Raloff |September 1, 2020 |Science News For Students 

Going a little deeper, if your dog is stronger than you, or if you’re training a puppy, consider a front-clip or no-pull harness. The right way to walk your dog |John Kennedy |August 26, 2020 |Popular-Science 

A one-year-old dog is adult enough to have puppies of its own. To figure out your dog’s ‘real’ age, you’ll need a calculator |Bethany Brookshire |August 12, 2020 |Science News For Students 

Agree on a subject, like puppies or your favorite book characters and everyone can begin. Virtually Celebrate in Style |Tracy Moran |August 5, 2020 |Ozy 

From the beginning, as he recorded the introduced wolves’ pairings, puppies and the rise and fall of packs, he wished Yellowstone visitors and other passionate wolf fans also could follow the story lines. How Yellowstone wolves got their own Ancestry.com page |Susan Milius |July 21, 2020 |Science News 

Yeah, the “Giant man-puppy” that is Gronkowski won't hold a sexual candle to the blue-eyed dreamboat. ‘A Gronking to Remember’ Speed Read: 8 Naughtiest Bits |Emily Shire |January 7, 2015 |DAILY BEAST 

Who went to a school where she was allowed to take her puppy to gym class, and her best friend played the didgeridoo. Speed Read: Lena Dunham’s Most Shocking Confessions From ‘Not That Kind of Girl’ |Kevin Fallon |September 26, 2014 |DAILY BEAST 

The plums came off the trees hot from the sun, and I had a cocker spaniel puppy that followed me everywhere I went. The Stacks: A Dog Dies, a Boy Grows Up |Pete Dexter |June 21, 2014 |DAILY BEAST 

I carried the puppy up the hill, stumbling under the weight. The Stacks: A Dog Dies, a Boy Grows Up |Pete Dexter |June 21, 2014 |DAILY BEAST 

A neighbor woman came out from behind her screen door and told me to leave the puppy out in the street. The Stacks: A Dog Dies, a Boy Grows Up |Pete Dexter |June 21, 2014 |DAILY BEAST 

Mr. Norwood was at home, and Jessie flew at him a good deal like an eager Newfoundland puppy. The Campfire Girls of Roselawn |Margaret Penrose 

I was flattering myself that the puppy was choosing my company to the hunt, for I always value the approval of a dog. The Soldier of the Valley |Nelson Lloyd 

One of them, a mere puppy, with a beard of English cut and a gold chain, had been at Rio—and twice! Skipper Worse |Alexander Lange Kielland 

If I live through this breach of habit, I shall be a white-livered puppy indeed. The Works of Robert Louis Stevenson - Swanston Edition Vol. 25 (of 25) |Robert Louis Stevenson 

When Tiney first saw Leo, he was only a puppy, and I suppose was frightened at the sight of so large a dog. Minnie's Pet Dog |Madeline Leslie