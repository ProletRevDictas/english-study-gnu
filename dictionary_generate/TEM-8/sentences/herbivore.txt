In fact, she reasoned, it was likely that early humans used tools for both purposes—the strict dichotomy between herbivores and carnivores in our past was never clean-cut. Tarzan Wasn’t for Her - Issue 100: Outsiders |Erika Lorraine Milam |June 2, 2021 |Nautilus 

The gene helps plants neutralize and safely store certain toxic molecules they use to deter herbivores. A plant gene may have helped whiteflies become a major pest |Jonathan Lambert |March 25, 2021 |Science News 

Anteosaurus probably had leaner limbs than related herbivores so it seems that this animal could have been capable of running bursts, he says. An ancient hippo-sized reptile may have been surprisingly agile |Carolyn Wilke |March 15, 2021 |Science News 

With vast deserts stretching north and south of the equator, Kent says, there would have been few plants available for the herbivores to survive the journey north for much of that time period. Climate change helped some dinosaurs migrate to Greenland |Anushree Dave |February 24, 2021 |Science News 

With no tasty reward to offer, and no ants to protect the cotton from hungry herbivores, these plants suffered the most damage compared with native plants that didn’t have the transgene. Modified genes can distort wild cotton’s interactions with insects |Emiliano Rodríguez Mega |February 16, 2021 |Science News 

Are young Americans ready to move off the competitive playing field and onto the herbivore pastureland? Are Millennials Turning Their Backs on the American Dream? |Joel Kotkin |November 10, 2013 |DAILY BEAST 

A garbage can is an herbivore grazing on stalks of ringworm. The Land of Look Behind |Paul Cameron Brown 

In this way the great enemies of the individual herbivore are not the carnivores, but the other herbivores. Charles Darwin |Grant Allen 

There are a few species of rodent-like animals—they're scavengers—and a herbivore we called the woods goat. Space Prison |Tom Godwin