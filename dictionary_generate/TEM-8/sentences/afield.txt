I’ve always wanted to ski Japan’s north island and also in Scandinavia, but following the pandemic, I plan to stay slightly closer to home and venture further afield as I adjust to whatever the new normal is. Why and how to plan next season’s epic ski trip |Rachel Walker |January 21, 2021 |Washington Post 

If they have lost a job or income due to the pandemic, they might be moving to Arizona or Mexico or even further afield. The Learning Curve: Enrollment Drops Hit Schools Rich and Poor |Will Huntsberry |December 4, 2020 |Voice of San Diego 

Delivery in its neighborhood — San Francisco’s Sunset district — costs $5, while those farther afield in the city pay $10. Ghost Kitchens Are the Wave of the Future. But Is That a Good Thing? |Kristen Hawley |November 9, 2020 |Eater 

Candidate sandwichesWhen chefs think of Delaware, they think of Capriotti’s — a sandwich shop that began in Wilmington and now has locations as far afield as Texas and California. Where to find food and drink specials for Election Day 2020 |Fritz Hahn |October 30, 2020 |Washington Post 

For Clorox executives, that means focusing on new areas, becoming experts on their own, and not having investment banks push ill-advised deals in categories too far afield. How Clorox’s new CEO plans to turn disinfectant wipes into future wins |Phil Wahba |October 21, 2020 |Fortune 

Rising labor costs are another force pushing restaurant owners further afield, Pemoulie says. High Rents Are Killing the Restaurant Capital |Will Doig |October 28, 2014 |DAILY BEAST 

At some point, he would like to take the project even further afield. What Does Your Shrink’s Decor Say About You? |Oliver Jones |September 22, 2014 |DAILY BEAST 

And this is pretty far afield, but keep an eye on Aleppo in Syria. Why the Border Bigots Are Beating Obama |Michael Tomasky |July 9, 2014 |DAILY BEAST 

Some of them came from wealthier families who could afford to flee a little further afield than the countries bordering Syria. Desperate to Go to War, Syrians in Egypt Find an Ally to Help |Alastair Beach, Abdulhamid Mallas |May 3, 2013 |DAILY BEAST 

Afield said no one knows why Julie Schenecker killed her children, except Julie Schenecker. Did Julie Schenecker Kill Her Kids? |Amy Green |February 8, 2011 |DAILY BEAST 

They come in wonderful hunting regalia and in all the wonderful splendor of the Britisher when he is afield. In Africa |John T. McCutcheon 

The British officer afield is a very different creature from the gilded ornament of an English mess. The Relief of Mafeking |Filson Young 

But he did know that they met now and then, that Mills seemed to have some curious knowledge of when Bland was far afield. The Hidden Places |Bertrand W. Sinclair 

These were days that forbade Michael to walk afield, and that with haunting, autumnal birdsong held him in a trance. Sinister Street, vol. 1 |Compton Mackenzie 

Afield, he was able to pick up propaganda broadcasts from Ceres. The Planet Strappers |Raymond Zinke Gallun