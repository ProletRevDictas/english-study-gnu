Just as humans have industrialized farming with large, AI-powered tractors and sprawling monocultures, we’ve also figured out how to harvest massive quantities of fish with large nets, trawls, and dredges. Climate scientists should pay more attention to fish poop. Really. |Benji Jones |October 8, 2021 |Vox 

Every accessible square meter of the North Sea was being hit approximately twice per year by a trawl. The Human Error Darwin Inspired - Issue 90: Something Green |Aaron Hirsh |September 30, 2020 |Nautilus 

Why, I offered to thrash him and his two boys only three weeks ago, for hanging around after dark where I had a trawl set. The Rival Campers Afloat |Ruel Perley Smith 

Indeed, the pelicans enclosed the fish with their united wings in a regular line as close and compact as a trawl or drag-net. In the Wilds of Florida |W.H.G. Kingston 

There is some ship-building, some brewing, with oyster and trawl fishing; the fishery engages nearly seven hundred persons. The Cornwall Coast |Arthur L. Salmon 

Five soundings were taken, and, on July 9, the trawl was put over in three hundred and forty-five fathoms. The Home of the Blizzard |Douglas Mawson 

We had been driving before a light westerly wind, when the trawl caught on the bottom and stopped the vessel. The Home of the Blizzard |Douglas Mawson