The work was often done in miserable weather, with stench and mud, and the soldiers lived in primitive housing, Budreau wrote. After World War I, U.S. families were asked if they wanted their dead brought home. Forty thousand said yes. |Michael Ruane |May 30, 2021 |Washington Post 

He has cleared paths through rooms with horrible stenches and picked through mold, dead rodents and worse. How downsizing expert Matt Paxton helps Americans sort through their stuff |Jura Koncius |January 21, 2021 |Washington Post 

The stench might mask the scent that hornets use to mark hives for attack. Honeybees fend off deadly hornets by decorating hives with poop |Asher Jones |January 19, 2021 |Science News For Students 

Yanna Casey, 25 of Atlanta, said the stench is particularly bad when she is around cleaning supplies. When coffee smells like gasoline: Covid isn’t just stealing senses — it may be warping them |Allyson Chiu |November 5, 2020 |Washington Post 

Instead, there’s a stench that’s increasingly hard to ignore. Butterfly Effect: The Unscientific Vaccine |Charu Kasturi |August 13, 2020 |Ozy 

The stench of corruption is settling over world soccer like a poisonous fog, and players are paying the price. Is Soccer Great Lionel Messi Corrupt? |Jack Holmes |December 8, 2014 |DAILY BEAST 

In the darkness none of the others could tell where the stench came from. I Shot Bin Laden |Elliot Ackerman |November 16, 2014 |DAILY BEAST 

Hot orange flames leap into the sky bringing with them the sickening, inescapable stench of death. Rage Against the Ebola Crematorium |Abby Haglage |November 11, 2014 |DAILY BEAST 

They were being carried out and the stench of their rotting flesh and bloated guts made it hard to examine them closely. Did Israel Execute Jihadists in Gaza? |Jesse Rosenfeld |September 7, 2014 |DAILY BEAST 

The small family home is still intact but the stench of rotting flesh that comes from inside is overpowering. Who Is Behind Gaza's Mass Execution? |Jesse Rosenfeld |August 1, 2014 |DAILY BEAST 

We elephants never fear anyone or hate anyone and that is why we exude no stench, but a tiger has to live by killing. Kari the Elephant |Dhan Gopal Mukerji 

On our way home, I verified the law of the jungle, for Kari had really developed a slight stench. Kari the Elephant |Dhan Gopal Mukerji 

Most of the party were now really ill from the foul stench in which they had lived so long. Famous Adventures And Prison Escapes of the Civil War |Various 

And as Raf pushed down another aisle, paralleling his course, he was conscious of a sickly sweet, stomach-churning stench. Star Born |Andre Norton 

Just as the stench of the snake-devil's lair had betrayed its site, here disaster and death had an odor of its own. Star Born |Andre Norton