HRC has named Chase Brexton a “Leader in LGBTQ Healthcare Equality” every year since it launched its Healthcare Equality Index, but the organization said the recognition is even more notable with the additional challenges faced this year. Chase Brexton recognized in HRC Healthcare Equality Index |Philip Van Slooten |September 11, 2020 |Washington Blade 

Given these claims, it is notable that Nikola repeatedly described the video as showing the truck “in motion,” which would be technically true even if the truck were not moving under its own power. New report claims widespread deception by Nikola Motor and founder Trevor Milton |dzanemorris |September 10, 2020 |Fortune 

A notable project comes from both NASA and Lockheed Martin, which is working on an aircraft called the X-59 QueSST. Air Force transport jets for VIPs could have a supersonic future |Rob Verger |September 10, 2020 |Popular-Science 

The most notable is a meta-analysis of seven existing studies on the subject, sponsored by the World Health Organization. Corticosteroids can help COVID-19 patients with severe respiratory distress |Kat Eschner |September 3, 2020 |Popular-Science 

Right now, that dominance is most notable whenever Leonard posts up, which is at a higher volume and efficiency than we’ve ever seen from him in the playoffs. If Kawhi Turns His Back To The Basket, Watch Out |Michael Pina |September 3, 2020 |FiveThirtyEight 

“We would love to finish what we started some years ago,” Branson told journalists at a news conference with notable hesitancy. You Were Wrong About Miley & Bitcoin: 2014’s Failed Predictions |Nina Strochlic |December 31, 2014 |DAILY BEAST 

Here are 15 notable books published in 2014 about the decade of the moment. The Best Books About the Volatile ’60s |Scott Porch |December 19, 2014 |DAILY BEAST 

Ed Sheeran, one of the performers on the night, was a notable exception. I Got Kicked Out Of The Victoria’s Secret Fashion Show |Nico Hines |December 3, 2014 |DAILY BEAST 

But the display was notable for the eruptions of smoke from the engines, most likely Russian RD-93s. How China Will Track—and Kill—America’s Newest Stealth Jets |Bill Sweetman |December 2, 2014 |DAILY BEAST 

Jackson had several notable confrontations with cadets who were unhappy with him or who felt he had been unjust. Stonewall Jackson, VMI’s Most Embattled Professor |S. C. Gwynne |November 29, 2014 |DAILY BEAST 

But even the great Mr. Abrahams, on one notable occasion at least, had been deceived. The Pit Town Coronet, Volume I (of 3) |Charles James Wills 

It is a notable fact that under the wholly unusual circumstances prevailing, the recovery was so prompt and effective. Readings in Money and Banking |Chester Arthur Phillips 

A notable increase is limited almost exclusively to myelogenous leukemia, where they are sometimes very numerous. A Manual of Clinical Diagnosis |James Campbell Todd 

Sensations were not so common in San Bernardino that they could afford to slight so notable an occasion as this. Ramona |Helen Hunt Jackson 

Stained films show notable irregularities in size, shape, and staining properties only in advanced cases. A Manual of Clinical Diagnosis |James Campbell Todd