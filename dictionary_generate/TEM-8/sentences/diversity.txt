At a time when other companies are only just initiating racial equality programs, McDonald’s has long understood and embraced the importance of diversity. Why McDonald’s sets the standard for equitable business models |jakemeth |September 16, 2020 |Fortune 

Our Most Powerful Women community, now two decades old, has helped increase diversity in the top ranks of business leaders. Announcing Fortune Connect, our new membership community |Alan Murray |September 15, 2020 |Fortune 

That’s why he has been prioritizing this work and internal diversity and inclusion efforts at Lowe’s since he began his tenure there in 2018. Lowe’s teams up with Daymond John to diversify its suppliers with a virtual pitch competition |McKenna Moore |September 15, 2020 |Fortune 

While agencies have learned lessons from previous diversity drives, they still have a lot to learn. ‘It’s all been plan, plan, plan mode:’ Agencies have big ideas for greater diversity, but more action is needed |Seb Joseph |September 15, 2020 |Digiday 

I asked Lorson if she plans to consider diversity of racial backgrounds in the hiring process. Oceanside Is Rethinking Its Police Chief Hiring Process Following Community Concerns |Kayla Jimenez |September 14, 2020 |Voice of San Diego 

That would truly be a milestone to celebrate—until you see what that record “diversity” actually means. The Unbearable Whiteness of Congress |Dean Obeidallah |January 8, 2015 |DAILY BEAST 

Parker tells of a new Texas struggling to deal with diversity. Will Texas Stay Texan? |David Fontana |December 29, 2014 |DAILY BEAST 

We know there needs to be diversity in storytellers telling their own stories. Ava DuVernay on ‘Selma,’ the Racist Sony Emails, and Making Golden Globes History |Marlow Stern |December 15, 2014 |DAILY BEAST 

The speakers emphasized the diversity of the crowd and seemed to almost play defense over any perceived media attacks. Sharpton Recalls Civil Rights Struggle in DC March Against Police Violence |Ben Jacobs |December 13, 2014 |DAILY BEAST 

That they will leverage their voices and their power to make real change to improve gender diversity. Tech’s Male ‘Feminists’ Aren’t Helping |Cate Huston, Karen Catlin |December 8, 2014 |DAILY BEAST 

But when these are made and renewed with due care, there is, according to circumstances, a great diversity in their character. The Ordinance of Covenanting |John Cunningham 

In Mexico the tobacco plantations exhibit a diversity of scenery not met with in other portions of America. Tobacco; Its History, Varieties, Culture, Manufacture and Commerce |E. R. Billings. 

The symptoms may comprise not only a diversity of physical ailments, but intellectual disturbances of the most terrible import. A Statistical Inquiry Into the Nature and Treatment of Epilepsy |Alexander Hughes Bennett 

A variety of geographical resources and of human resources results in diversity in the economic life of the state. Hallowed Heritage: The Life of Virginia |Dorothy M. Torpey 

Less intelligible is the existing diversity of policy of the Central Authority in 1907 with regard to able-bodied women. English Poor Law Policy |Sidney Webb