It’s a notion that’s launched more than a few books and pseudo-docs, all hinging on the fact that most of the planet’s nether waters are unexplored—and therefore rife with primo dens for enigmatic beasts. Could an ancient megashark still lurk in the deep seas? |By Riley Black |October 15, 2020 |Popular-Science 

Yet behind the we’re-just-here-to-help-folks-realize-their-dreams facade, I’d say the Silicon Valley company always has been a den of buttoned-up MBAs trying to optimize its e-commerce engine. eBay’s pathetic, power-hungry pranksters |Adam Lashinsky |September 28, 2020 |Fortune 

Chestnut says that finding and measuring these dens would have provided insight as to how the mothers choose their nesting locations, and helped them protect areas of their park that could serve as dens in the future. Mount Rainier’s first wolverine mama in a century is a sign of the species’ comeback |Hannah Seo |August 28, 2020 |Popular-Science 

Female wolverines use snowfall in the winter to build their natal dens, where they birth and take care of their young. Mount Rainier’s first wolverine mama in a century is a sign of the species’ comeback |Hannah Seo |August 28, 2020 |Popular-Science 

This is especially true of the 1002 coastal plain portion, which has the largest number of polar bear dens in Alaska. The Trump administration opened the Arctic National Wildlife Refuge to oil companies—but none may bite |By Scott L. Montgomery/The Conversation |August 26, 2020 |Popular-Science 

In a chapter titled “Fox Den,” he braves brambles in an attempt to follow his neighborhood urban fox. Discovering Underground Labyrinths, Remote Cities, and More of the World’s Lost Places |Nina Strochlic |July 8, 2014 |DAILY BEAST 

It is unknown whether John Doe is David Neuman, who was once an executive at DEN. New Hollywood Sex Scandal: Bryan Singer’s Accuser Files Another Suit |Tim Teeman |June 10, 2014 |DAILY BEAST 

The occasion was his Pitch At The Palace event, a Dragon's den-style forum for entrepreneurs to seek investment. Prince Andrew Tweets First True Royal Selfie |Tom Sykes |April 2, 2014 |DAILY BEAST 

He introduces me to the head bartender, Peter Van Den Bossche. The Secret Speakeasies of Buenos Aires |Jeff Campagna |February 25, 2014 |DAILY BEAST 

Heinz wrote the book in his makeshift den, a bantam-size office that he had constructed off the TV room in a corner of his garage. The Night Vince Lombardi Lay Awake Brooding Over a 49-0 Win |W.C. Heinz |January 25, 2014 |DAILY BEAST 

Over the mantel in our parlor we have a picture of the lion's den, and it is one of the choicest of our family treasures. The Soldier of the Valley |Nelson Lloyd 

"'Bared the lion in hith den—the Doog-dug-lath——'" Abraham stopped and took a long breath. The Soldier of the Valley |Nelson Lloyd 

Then he returned to the den and found his people recovering somewhat from their surprise. The Garret and the Garden |R.M. Ballantyne 

Mr. Ralph found me in his den, I was arranging one of his tables, and he said that he wanted to talk to me. Tessa Wadsworth's Discipline |Jennie M. Drinkwater 

Often the doctor came to Hill Street and sat for long periods with the general in that small, cosy room which was his den. The Doctor of Pimlico |William Le Queux