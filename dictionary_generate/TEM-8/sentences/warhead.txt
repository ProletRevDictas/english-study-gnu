The United States includes both Air Launched Cruise Missiles, designed to be released from bombers, and ship- or land-launched cruise missiles like the Tomahawk, in its war planning, with both types of missile capable of carrying a nuclear warhead. How North Korea’s cruise missiles could surprise its enemies |Kelsey D. Atherton |September 15, 2021 |Popular-Science 

It changes how those warheads could be delivered, but it does not increase the amount of warheads the country can produce, which is already quite limited. How North Korea’s cruise missiles could surprise its enemies |Kelsey D. Atherton |September 15, 2021 |Popular-Science 

South Korea claims that North Korean experts have worked at Natanz, one of Iran’s premier nuclear hubs, and the IAEA has reports that the North transferred to Iran mathematical formulas and codes for nuclear warhead design. First Comes Iran, Then Comes North Korea |Tracy Moran |April 8, 2021 |Ozy 

With this technology in place, warhead developers can rapidly and confidently design munitions adapted for our next generation of warfighters. The Army’s new tool for analyzing bomb shrapnel could lead to better body armor |Kelsey D. Atherton |March 3, 2021 |Popular-Science 

This process is often cost prohibitive due to the time and labor required to collect and analyze vast amounts of data after a destructive warhead event. The Army’s new tool for analyzing bomb shrapnel could lead to better body armor |Kelsey D. Atherton |March 3, 2021 |Popular-Science 

The weapon carries a 200-kiloton nuclear warhead; by comparison, the bomb that destroyed Nagasaki was a mere 21 kilotons. Are Russian Bombers Flying Nuclear Drills Near Europe—Or Just Testing NATO’s Defenses? |Dave Majumdar |October 30, 2014 |DAILY BEAST 

It was a simple weapon, cheap to build, and carried a half-ton explosive warhead. Life Under Air Strikes: Children Under Fire Will Never Forget — or Forgive |Clive Irving |August 3, 2014 |DAILY BEAST 

As the missile closes on its target the warhead explodes before the missile strikes the airplane. MH17 Is the World’s First Open-Source Air Crash Investigation |Clive Irving |July 22, 2014 |DAILY BEAST 

In this way the missile does not need pinpoint accuracy: widely spread supersonic shrapnel from the warhead is deadly. MH17 Is the World’s First Open-Source Air Crash Investigation |Clive Irving |July 22, 2014 |DAILY BEAST 

Optionally, it can live up to its description by crashing into targets and detonating a five-pound warhead. Smoke Rings, Mystery Backpacks and Gun-Toting Robots: The Weird Wartech of the Korean Conflict |Kyle Mizokami |April 3, 2014 |DAILY BEAST 

He twisted the dial in the warhead and, while Burl held the aim, shoved in the rocket shell. The Secret of the Ninth Planet |Donald Allen Wollheim 

But they would have to hurry if they were to get all their supplies and gear moved from Bandit before the warhead struck. First on the Moon |Jeff Sutton 

While he weighed the problem, one part of his mind told him a warhead was scorching down from the sides. First on the Moon |Jeff Sutton 

Thus, a warhead loaded with the material has an indefinite shelf-life. U.S. Patent 4,293,314: Gelled Fuel-Air Explosive |Bertram O. Stull 

Any indication down there of any power planning to start a war, and we'll send our own atomic warhead rockets down. By Earthlight |Bryce Walton