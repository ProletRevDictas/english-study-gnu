There are still many unanswered questions about how the space-surviving microbes could physically survive the transfer from one celestial body to another. Clumps of bacteria could spread life between planets |Paola Rosa-Aquino |August 27, 2020 |Popular-Science 

Phobos might even be a kind of celestial Phoenix, born from the remains of an earlier moon that got ripped apart into rings that then reassembled. These Images Expose the Dark Side of the Solar System - Issue 89: The Dark Side |Corey S. Powell |August 26, 2020 |Nautilus 

A star is a celestial object whose surface is so hot that it emits light. Explainer: Stars and their families |Ken Croswell |August 18, 2020 |Science News For Students 

Such crisp telescope images could help astronomers study a range of celestial objects, from solar system bodies to distant galaxies. An Antarctic ice dome may offer the world’s clearest views of the night sky |Maria Temming |July 29, 2020 |Science News 

Typically, corrections use telescope observations, which offer a set of celestial coordinates to determine Earth’s orientation in space. A giant underground motion sensor in Germany tracks Earth’s wobbles |Maria Temming |July 17, 2020 |Science News 

One has to believe that Brazil was at the receiving end of some great celestial wrath. Germany Humiliates World Cup Host Brazil 7-1 in Semifinal Slaughter |Tunku Varadarajan |July 8, 2014 |DAILY BEAST 

As a music fan, it was very exciting to finally have access to something close to a celestial jukebox—all music, instantly. 15 Years After Napster: How the Music Service Changed the Industry |Alex Suskind |June 6, 2014 |DAILY BEAST 

We can only thank some celestial power that he did not seek refuge in the United States. Cut the Baloney on Ukraine |Leslie H. Gelb |March 9, 2014 |DAILY BEAST 

Geniuses joined the realm of intermediate beings, alternately exalted and tormented by celestial visions. What is a Genius? |Nick Romeo |November 9, 2013 |DAILY BEAST 

Bathed in celestial light, and with her husband, Jor-El (Russell Crowe), looking on, she gives birth to their son, Kal-El. ‘Man of Steel,’ New Superman Movie Starring Henry Cavill, Falls Flat |Marlow Stern |June 11, 2013 |DAILY BEAST 

Just corporeal enough to attest humanity, yet sufficiently transparent to let the celestial origin shine through. Pearls of Thought |Maturin M. Ballou 

At first men imagined the celestial bodies to be, as they seemed, small objects not very far away. Outlines of the Earth's History |Nathaniel Southgate Shaler 

Atmospheric envelopes appear to be common features about the celestial spheres. Outlines of the Earth's History |Nathaniel Southgate Shaler 

A dark moon has joined this celestial grouping, and is now swinging in an orbit about the earth. Astounding Stories, May, 1931 |Various 

Perhaps I was but startled yesterday to find a celestial loveliness where I expected to encounter pallid inanity. The Circular Study |Anna Katharine Green