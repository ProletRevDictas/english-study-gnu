We did take the time to just drive around town each day, breaking free of cabin fever and trying to keep our sanity. Fun haulers |Joe Phillips |December 19, 2020 |Washington Blade 

With real life scares like tanned-ghouls and Boogaloos Bois threatening the sanctity of your sanity — this bop is still great whenever you’re looking to escape the stressors that threaten your sunny days. Trick Or Treat: An All Hallows’ Eve Playlist For You Guys And Ghouls |cmurray |October 30, 2020 |Essence.com 

Less cautionary tales than desperate pleas for sanity, “Subsequent Moviefilm” and “American Selfie” join a slew of narrative features and documentaries designed to move the needle during election season. Movies are rushing to impact the election. Don’t ask whether they’ll work. Ask whether they’ll last. |Ann Hornaday |October 30, 2020 |Washington Post 

I witness their reckoning with mortality, with the fragility of health and sanity. Thank You for the 7 PM Clapping, But Camaraderie Is Needed More Than Ever - Facts So Romantic |Ayala Danzig |October 5, 2020 |Nautilus 

The onset of this pandemic has put a strain on the sanity of many people forced to isolate themselves from friends and family. People Are Discovering the Joy of Actually Talking on a Phone - Facts So Romantic |Brian Gallagher |September 23, 2020 |Nautilus 

You still have to take care of your sanity, your piece of mind. Goodbye To A Natural Hair Guru: Miss Jessie's Cofounder Titi Branch Dead At 45 |Danielle Belton |December 16, 2014 |DAILY BEAST 

His sanity is slowly unraveling, like the claustrophobic narrator of The Tell-Tale Heart. ‘Interstellar’ Is Wildly Ambitious, Very Flawed, and Absolutely Worth Seeing |Marlow Stern |November 7, 2014 |DAILY BEAST 

But in the long-term, Democrats must do something to pull the debate back toward the center; toward basic rational sanity. Obama’s New Emissions Rules Will Yank the Climate Change Debate Back Into Reality |Sally Kohn |June 2, 2014 |DAILY BEAST 

What is in dispute is the sanity of the judge who sentenced Boardman's husband to only eight years of house arrest. House Arrest for Marital Rape, Are you Kidding?! |Sally Kohn |May 21, 2014 |DAILY BEAST 

For my own sanity—as morally discomfiting as it is—I'm planning to play along. Why We Should Pretend the ‘Game of Thrones’ Rape Scene Never Happened |Andrew Romano |May 4, 2014 |DAILY BEAST 

It was an acid test of his sanity and he knew as he worked that his reasoning faculties at least had suffered no impairment. The Man from Time |Frank Belknap Long 

It was in her hour of sanity and insight that she had said virginity was the law, the indispensable condition. The Creators |May Sinclair 

How destructive to common sense and sanity and everything that kept life running on reasonable lines. The Daughters of Danaus |Mona Caird 

She sang the praises of Athenian literature and art and life; there was sanity and clarity, there was balance and serenity! Love's Pilgrimage |Upton Sinclair 

During these three months there were moments when he felt himself perilously close to the borders of his sanity. The Woman Gives |Owen Johnson