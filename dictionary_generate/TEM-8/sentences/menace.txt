The country seems obsessed with imposing, bluff-fronted SUVs and trucks these days, and the Highlander conforms to this trend, albeit with less implicit menace than others have achieved. The Toyota Highlander hybrid is a big three-row with a buzzy engine |Jonathan M. Gitlin |March 5, 2021 |Ars Technica 

The energy swelled, ideas improved, and the menace returned. Rose Lavelle lifts U.S. women’s national team past Canada at SheBelieves Cup |Steven Goff |February 19, 2021 |Washington Post 

Now the counterterrorism apparatus has to shift its aim to a new menace, one that is more opaque and diffuse than Islamist networks, experts said. Global Right-Wing Extremism Networks Are Growing. The U.S. Is Just Now Catching Up. |by Sebastian Rotella |January 22, 2021 |ProPublica 

If they knock off the Tampa Bay Buccaneers next weekend, it’ll be because Sean Payton understood his team had shifted away from a points machine under Brees to a defensive menace. What to know from NFL playoffs first round: Lamar Jackson wins and Tom Brady finds a way |Adam Kilgore |January 11, 2021 |Washington Post 

Wildfires in the West, hurricanes around the world, and an increasingly volatile climate were already a menace as the overheated Earth continued to run a fever. 2020: Watch The Year in Review |Jeffrey Kluger |December 11, 2020 |Time 

Hitchcock leans toward me in a conspiratorial, almost lascivious, way and says, “Let's pile on the menace.” Alfred Hitchcock’s Fade to Black: The Great Director’s Final Days |David Freeman |December 13, 2014 |DAILY BEAST 

The menace and abuse was constant; it reads as a household under siege. Alan Cumming: The Truth About My Father |Tim Teeman |October 14, 2014 |DAILY BEAST 

But as of Sept. 10, our intelligence agencies knew of no specific menace to the homeland. Why ISIS Keeps Running Circles Around Us, Just Like Al Qaeda Did Before 9/11 |Christopher Dickey |September 11, 2014 |DAILY BEAST 

Unbridled nationalism is a menace; it leads to trade wars and, all too often, real wars. The United States Needs Corporate 'Loyalty Oaths' |Jonathan Alter |August 4, 2014 |DAILY BEAST 

“I am convinced that the only way to fight this menace is by attacking it on many fronts,” he said in a letter to Congress. Chris Christie to the Drug War: I Wish I Knew How to Quit You |Olivia Nuzzi, Abby Haglage |June 18, 2014 |DAILY BEAST 

The menace of a thunder-cloud approached as in his childhood's dream; disaster lurked behind the quiet outer show. The Wave |Algernon Blackwood 

It was clear to every thinking man, American or European, that the control of such a formidable body was a menace to peace. The Philippine Islands |John Foreman 

When vicious animals are kept for any purpose and are a menace to human beings they are a nuisance. Putnam's Handy Law Book for the Layman |Albert Sidney Bolles 

I dare not envy many a man:Who runs his life-race well; Whose brave, undaunted peasant bloodDeath's menace cannot quell. Charles Baudelaire, His Life |Thophile Gautier 

The worst cases we have are girls, and it is quite clear some of them are an absolute menace. Report of the Special Committee on Moral Delinquency in Children and Adolescents |Oswald Chettle Mazengarb et al.