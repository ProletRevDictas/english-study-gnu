In his City of God, Augustine was quite clear that in the hereafter humans would no longer need bathroom breaks. Sorry, Internet: Pope Francis Didn't Open Paradise to Pets |Candida Moss |December 14, 2014 |DAILY BEAST 

It combined the rambling, torturous pace of J. Edgar, the oddball clairvoyance of Hereafter, and … that line from Dirty Harry. Clint Eastwood’s RNC Speech: Ben Affleck and Other Hollywood Insiders React |Marlow Stern |September 1, 2012 |DAILY BEAST 

“I think he has a nice philosophical plan for the fiscal hereafter, two or three decades down the road,” Stockman sniffed. Reagan Budget Guru: Shut It Down! |Lloyd Grove |April 7, 2011 |DAILY BEAST 

But where there is no existing relation between the words or ideas, it is a case for Synthesis, to be taught hereafter. Assimilative Memory |Marcus Dwight Larrowe (AKA Prof. A. Loisette) 

In such cases, Synthesis, which is taught hereafter, develops an indirect relation. Assimilative Memory |Marcus Dwight Larrowe (AKA Prof. A. Loisette) 

Nor shall we do more hereafter if you do my pleasure now and give this Monsieur de Garnache the answer that I bid you. St. Martin's Summer |Rafael Sabatini 

Synthesis will be sometimes hereafter resorted to to connect in our minds an event to its date. Assimilative Memory |Marcus Dwight Larrowe (AKA Prof. A. Loisette) 

Subject to the exceptions hereafter named, all dates and numbers should be exactly expressed in the date or number words. Assimilative Memory |Marcus Dwight Larrowe (AKA Prof. A. Loisette)