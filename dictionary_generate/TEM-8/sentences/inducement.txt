In response, for almost 70 years Pakistan tried either to influence or to weaken Afghanistan through a combination of economic pressure and inducements with support for rebellions within Afghanistan. What Pakistan Stands to Gain From the Taliban Takeover of Afghanistan |Anatol Lieven |August 19, 2021 |Time 

Yet some local governments are adding further inducements in the hopes of accelerating full immunization. Philadelphia is offering extreme vaccine incentives with help from Wharton economists |Annalisa Merelli |June 9, 2021 |Quartz 

Access to power and business favors add to the inducements to stay on message. It Isn't Just Modi. India’s Compliant Media Must Also Take Responsibility for the COVID-19 Crisis |Debasish Roy Chowdhury |May 3, 2021 |Time 

No State, in the current economic situation, can turn down this “financial inducement.” The new suit attacking the stimulus law, explained |Ian Millhiser |March 19, 2021 |Vox 

Shah is a master at making opposition legislators switch sides through inducements and intimidation, and thereby flipping elected state governments. Save the Sarcasm for Other Democracies. America Is Fine |Debasish Roy Chowdhury |January 11, 2021 |Time 

Though fraudulent inducement does not ordinarily augur well, it worked. The Drunken Downfall of Evangelical America's Favorite Painter |Zac Bissonnette |June 8, 2014 |DAILY BEAST 

But the same inducement in song at the end of a Woody Allen show has the potential to come off as less-than sincere. Woody Allen’s ‘Bullets Over Broadway’ Musical and the Moral Responsibility of an Artist |Brian Spitulnik |April 10, 2014 |DAILY BEAST 

East replied, "I'm sorry, that's not sufficient inducement." Don't Buy Haley Barbour's Myth |Harold Evans |December 20, 2010 |DAILY BEAST 

The New York Times called the film “an effective inducement to rage.” Revenge of the Electric Car |Matthew Dakotah |October 23, 2009 |DAILY BEAST 

So the billion-dollar question is: How do they accomplish this feat of inducement? Questions for Obama's Car Czar |Edward Jay Epstein |April 21, 2009 |DAILY BEAST 

It is to be feared that the attractions of the house-dinner were not the sole inducement to many of those sitting there. The Pit Town Coronet, Volume I (of 3) |Charles James Wills 

"Quite an inducement for me to know her, I'm sure," observed Miss Jane, dryly. The Sunbridge Girls at Six Star Ranch |Eleanor H. (Eleanor Hodgman) Porter 

Its wealth and stability were also an additional inducement to the kings in granting to the towns their firma burgi. The Influence and Development of English Gilds |Francis Aiden Hibbert 

They were offered every inducement to desert,—heavy bribes, and promotion in a new service,—but they refused them all. The Flag Replaced on Sumter |William A. Spicer 

An invitation from Prince Radziwill was the inducement that led him to quit the paternal roof so soon after his return to it. Frederick Chopin as a Man and Musician |Frederick Niecks