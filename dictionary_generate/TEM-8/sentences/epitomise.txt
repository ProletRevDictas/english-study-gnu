You cannot epitomise the knowledge that it would take years to acquire into a few volumes that may be read in as many weeks. Friends in Council |Arthur Helps 

The brief summary will serve to broadly epitomise the subject, and will prove the ceaseless variety of interest which it involves. Old and New London |Walter Thornbury 

I shall, however, in a few pages briefly epitomise what passed. The Reign of Henry the Eighth, Volume 1 (of 3) |James Anthony Froude 

It always seemed to her to characterise and to epitomise him, that grotesque expression. This Freedom |A. S. M. Hutchinson 

The actual achievements of Manet epitomise the secondary in art. Modern Painting, Its Tendency and Meaning |Willard Huntington Wright