We soon found it too heavy to carry that way, so skinned it and hung up the foreparts and took the skin and hindquarters. Fifty Years a Hunter and Trapper |Eldred Nathaniel Woodcock 

He stooped to examine the dead wolf, catching it by the tail and hoisting its hindquarters off the ground. The Time Traders |Andre Norton 

At this great oath the horse stood up on his fore-feet, and sat like a dog on his hindquarters. The Yellow Rose |Mr Jkai 

He strained on the traces, exposing to them only his hindquarters, running well ahead, and keeping his throat safe. Murder Point |Coningsby Dawson 

But to-day he caught the edge of it, and he knew why Gray Wolf's ears flattened, and her hindquarters drooped. Kazan |James Oliver Curwood