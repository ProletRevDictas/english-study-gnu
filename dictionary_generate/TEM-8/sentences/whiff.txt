Off it, Snyder continues to recirculate his bad air and give the organization an unsavory whiff. Today in D.C.: Headlines to start your Wednesday in D.C., Maryland and Virginia |Dana Hedgpeth, Teddy Amenabar |December 10, 2020 |Washington Post 

Suddenly, it hits you—a texture of reality carried by a strong whiff of … horse manure. What Did the Past Smell Like? - Issue 93: Forerunners |Ann-Sophie Barwich |December 9, 2020 |Nautilus 

Some Great Big Story employees caught that whiff of trouble later in 2019 as WarnerMedia began to move employees from Time Warner Center to its new home at Hudson Yards. ‘Two very, very different companies’: Why CNN’s Great Big Story failed to survive |Tim Peterson |November 2, 2020 |Digiday 

We got a whiff of investor enthusiasm last week as the S&P 500 and Nasdaq both rallied to their highest weekly gains in three months. Why earnings season could be the next big lift for stocks |Bernhard Warner |October 12, 2020 |Fortune 

I appreciate the simple things I took for granted – each breath, whiff of fresh air, the sun and trees. The Anatomy Of A Breast Cancer Survivor: ‘Early Detection Saved My Life’ |Charli Penn |October 6, 2020 |Essence.com 

There were stories of distant strife, in Bosnia, Rwanda, and Northern Ireland, and those stories had the whiff of a different era. The Best Columns of 2014 |John Avlon, Errol Louis |December 31, 2014 |DAILY BEAST 

Griswold was undeniably an incursion on democratic powers with a definite whiff of activism. The Right Wing Screams for the Wambulance Over Gay Marriage Ruling |Walter Olson |October 13, 2014 |DAILY BEAST 

Nobody should waste their time penning letters that any pediatrician with a whiff of insight will ignore. Kids Don’t Know How Overweight They Really Are |Russell Saunders |July 29, 2014 |DAILY BEAST 

I stepped off the plane, caught that first groggy whiff of jet fuel and my body instantly registered where I was. How I’ll End the War: My First Week Back in Afghanistan |Nick Willard |May 1, 2014 |DAILY BEAST 

But alongside all the true-fandom, a whiff of regret lingers. Why The Tea Party Won’t Go Away And More Wisdom From Matt Kibbe |Michael Signer |April 23, 2014 |DAILY BEAST 

Then I caught a whiff of burning wood and in ten minutes I was reconnoitering a tiny glade. A Virginia Scout |Hugh Pendexter 

The next instant he caught a whiff of smoke and saw it rising in a dense cloud through the trees. Gold-Seeking on the Dalton Trail |Arthur R. Thompson 

A whiff of foreign smell and a sound of foreign speech reached the passengers at about the same moment. The Daughters of Danaus |Mona Caird 

At last the fire of the corsair ceased, and a whiff of air carried away the smoke. Stories of Our Naval Heroes |Various 

Then when he caught a faint whiff of the game, he would stop short, and look around, and wag his tail. Blazed Trail Stories |Stewart Edward White