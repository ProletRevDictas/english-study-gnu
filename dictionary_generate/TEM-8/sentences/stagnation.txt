This concentration of innovation in a few dominant companies has led to inequality and, for many, wage stagnation. Capitalism is in crisis. To save it, we need to rethink economic growth. |Katie McLean |October 14, 2020 |MIT Technology Review 

This period, between 1964 and 1982, is called the “era of stagnation” in the graph below. Analyze This: Perfumes from everyday products collect in distant ice |Carolyn Wilke |September 30, 2020 |Science News For Students 

There’s no wasted time and little of the stagnation that usually happens when one player surveys the floor. The Aces Don’t Need Threes To Win |Mike Prada |September 18, 2020 |FiveThirtyEight 

Much of this discontent is related to economic issues — some of them specific, like wage stagnation and the spike in health-care and college costs. Does Anyone Really Know What Socialism Is? (Ep. 408 Rebroadcast) |Stephen J. Dubner |September 17, 2020 |Freakonomics 

The pattern of no owls, poor sleep, second-guessing, and general stagnation continued on for more than a week. The quest to snare—and save—the world’s largest owl |Jonathan Slaght |August 28, 2020 |Popular-Science 

Republicans must seize this opportunity to pass legislation to improve the economic stagnation. Voters Remind D.C. That the Economy Still Sucks |Stuart Stevens |November 6, 2014 |DAILY BEAST 

The Taliban, power cuts, corruption, economic stagnation, Osama bin Laden, all of it. Why So Many Pakistanis Hate Their Nobel Peace Prize Winner |Chris Allbritton |October 10, 2014 |DAILY BEAST 

Others face career uncertainty and stagnation as promotion rates continue to drop for both enlisted and officers. Downsizing the War: Layoffs and Yard Sales in Afghanistan |Nick Willard |August 1, 2014 |DAILY BEAST 

India expects Modi to deliver the country from economic stagnation. Modi Crushes Gandhi in India’s Election Landslide |Tunku Varadarajan |May 16, 2014 |DAILY BEAST 

So this is Obama, hope and change, stock market rallies for the top five percent, and wage stagnation for the rest. Income Inequality is a Recipe for Stagnation |Lloyd Green |January 21, 2014 |DAILY BEAST 

This is a state of affairs tending to produce stagnation and vigorously to check advance. Man And His Ancestor |Charles Morris 

He was a savage still, and at the close of the struggle he settled down into a second stage of stagnation. Man And His Ancestor |Charles Morris 

Once reached, it tended to continue indefinitely, stagnation following the era of growth. Man And His Ancestor |Charles Morris 

While talking to this officer, a lieutenant, he contrived to interest him with an account of the stagnation of trade. Catherine de' Medici |Honore de Balzac 

This remarkable contrast between the progress of the north-east and the stagnation of the rest of the country is no new thing. Is Ulster Right? |Anonymous