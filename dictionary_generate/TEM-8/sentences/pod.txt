Weganool fabric made with calotropis pod fibers, which are viewable on the left. How a Wasteland Shrub Is Becoming the Next Big Thing in Fashion |Daniel Malloy |August 28, 2020 |Ozy 

They usually travel in pods, too, so it’s especially odd this beluga was spotted alone. Environment Report: Why Your Water Bill Might Spike |MacKenzie Elmer |July 27, 2020 |Voice of San Diego 

These pods will still be made up of public school students, who are getting some level of online instruction from public school teachers. The Learning Curve: The Dystopian Future of ‘Learning Pods’ |Will Huntsberry |July 16, 2020 |Voice of San Diego 

I asked him if the rise of the learning pod will make existing disparities worse. The Learning Curve: The Dystopian Future of ‘Learning Pods’ |Will Huntsberry |July 16, 2020 |Voice of San Diego 

Sitting at the bow of a boat, they sniff the air blown toward them from a pod of orcas. Conservation is going to the dogs |Alison Pearce Stevens |April 2, 2020 |Science News For Students 

The Navy and Marine Corps versions of the F-35 have differing configurations and rely on an external gun pod. New U.S. Stealth Jet Can’t Fire Its Gun Until 2019 |Dave Majumdar |December 31, 2014 |DAILY BEAST 

But at Cheshire, prisoners rarely, if ever, leave their pod. ‘Progressive Jail’ Is a 21st-Century Hell, Inmates Complain |Sarah Shourd |September 29, 2014 |DAILY BEAST 

The faction leaders each take a colony pod and set out to form their own society on the new planet. Nerdiness from Noah: Alpha Centauri |Noah Kristula-Green |March 29, 2013 |DAILY BEAST 

When he first tries the pods for himself, he fails to realize that a fly has made its way into the other pod. Zac Efron, Tom Cruise & More Actors in Their Tighty Whities (VIDEO) |Mike Munoz |October 4, 2012 |DAILY BEAST 

Top adviser Eric Fehrnstrom told MSNBC that Santorum and Gingrich are “two peas in a pod—longtime Washington legislators.” Romney Losing His Mojo After Caucus, Primary Losses to Santorum |Howard Kurtz |February 9, 2012 |DAILY BEAST 

It bears a pod similar to that of the locust, to which it is related, containing eight to twelve beans. Early Western Travels 1748-1846, Volume XVI |Various 

Instantly it split, showing the gummed red seeds clinging to the inner walls of the sensitive pod. The Escape of Mr. Trimm |Irvin S. Cobb 

The bark is light in colour; and the capsule pod contains a large quantity of down, of a brown tint, and exquisite silky softness. The Desert World |Arthur Mangin 

It is very long--nearly twice as long as this page and looks much more like a stem than a seed-pod. On the Seashore |R. Cadwallader Smith 

She and her kitten were as much alike as two peas in a pod—jet-black, and with beautiful yellow-green eyes. Mary and I |Stephen Return Riggs