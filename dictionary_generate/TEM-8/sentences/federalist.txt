Though Abiy’s government will weather the storm, there is more controversy to come as he moves forward with plans to amend Ethiopia’s constitution to change the country’s ethno-federalist structure. Ethiopia Is at a Crossroads. Can the Nation Survive in Its Current Form? |Ian Bremmer |June 25, 2021 |Time 

America’s federalist structure also makes collective action, handed down from the federal level, extremely difficult. America still needs to learn from its biggest pandemic failure |German Lopez |June 4, 2021 |Vox 

And as one of the initial organizers of the Federalist Society, he is a skilled political tactician. The Outside Game of Justice Scalia, a Loner With Clout |David Fontana |June 16, 2014 |DAILY BEAST 

In the Federalist Papers, James Madison wrote, “If men were angels, no government would be necessary.” Obamacare Is as American as the Founding Fathers |Sally Kohn |April 1, 2014 |DAILY BEAST 

As Madison put it in Federalist 52, the House was to be “dependent on the People alone.” The Court Case That Pivots on What ‘Corrupt’ Really Means |Lawrence Lessig |September 26, 2013 |DAILY BEAST 

Not the rich,” as he wrote in Federalist 57, “more than the poor. The Court Case That Pivots on What ‘Corrupt’ Really Means |Lawrence Lessig |September 26, 2013 |DAILY BEAST 

The first sentence of The Federalist Society states that this is a book “about the power of ideas.” A Small Right-Wing Conspiracy: The Federalist Society |David Fontana |June 11, 2013 |DAILY BEAST 

But Mr. Parnell was thinking only of Ireland, and he was not a Federalist. Home Rule |Harold Spender 

But the propertied classes in the interior belonged chiefly to the Federalist party. Argentina |W. A. Hirst 

To-day Jeffersons directions are observed, and the Federalist remains the text-book. Nullification, Secession Webster's Argument and the Kentucky and Virginia Resolutions |Caleb William Loring 

He says the Republican and the Federalist parties were divided by a bottomless gulf in their theories of constitutional powers. Nullification, Secession Webster's Argument and the Kentucky and Virginia Resolutions |Caleb William Loring 

The Federalist clergy joined in denouncing Jefferson on the ground that he was an atheist. Thomas Jefferson |Edward S. Ellis et. al.