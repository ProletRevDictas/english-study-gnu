A new effort to ban Native mascots was quashed by the state school board this year. Hundreds Of Schools Are Still Using Native Americans As Team Mascots |Hope Allchin |October 12, 2020 |FiveThirtyEight 

Scoring those long-term contracts in a competitive global market seemed unlikely, even before the pandemic quashed demand for North America’s highly priced gas. It’s His Land. Now a Canadian Company Gets to Take It. |by Lee van der Voo for ProPublica |October 1, 2020 |ProPublica 

Within Silicon Valley itself, unease over how big tech may quash smaller tech businesses has also grown. Majority of tech employees and potential founders say big tech needs more regulation |Lucinda Shen |September 4, 2020 |Fortune 

Researchers want to know how elite controllers quash the virus for long periods of time. In a first, a person’s immune system fought HIV — and won |Tina Hesman Saey |August 26, 2020 |Science News 

Its draconian lockdown measures had quashed the virus at home so effectively that doctors couldn’t find patients to fully test their vaccine on. Every country wants a covid-19 vaccine. Who will get it first? |Katie McLean |August 13, 2020 |MIT Technology Review 

Putin had to put on a show with lots of lights and dancing, but quash the gay factor. Sorry Putin, the Sochi Opening Ceremony Was Totally Gay |Tim Teeman |February 7, 2014 |DAILY BEAST 

Fergie did nothing to quash rumors of remarriage when asked about the controversial subject this weekend, reports Tom Sykes. Fergie Remarriage Rumors Heat Up as She Says, ‘Andrew Will Always Be My Prince!’ |Tom Sykes |September 30, 2013 |DAILY BEAST 

Egyptian forces have also launched a series of coordinated operations in Sinai in an attempt to quash rising insurgency. A Bloody Assassination Attempt in Egypt |Sophia Jones |September 5, 2013 |DAILY BEAST 

But despite rumors of a Game of Thrones theme, Parker was quick to quash the speculation. Sean Parker Weds Alexandra Lenas, ‘Game of Thrones’ Theme a Mystery |Nina Strochlic |June 2, 2013 |DAILY BEAST 

It all seems perfectly choreographed to quash the assumption that she is no more than a little flirt. Petraeus Affair Stereotypes: The General, The Flirt And The Harlot |Robin Givhan |November 15, 2012 |DAILY BEAST 

The low hiss of steam, the faint roar of the fires on the grates, the quish-quash of the pumps, were music to his ears. Red Dynamite |Roy J. Snell 

This was an opportunity to lead her into an admission which might immediately quash all of the grounds of the complaint. Lightnin' |Frank Bacon 

Hence it was necessary for the State and Church to quash their indictment before God could do the same. Little Journeys to the Homes of the Great, Vol. 13 |Elbert Hubbard 

After all it was precipitated as a fine film, and you can quash it and even slice it up without any trouble. Tillie |Roger Phillips Graham 

I recognized the legal rights of her husband, but no ruffling Daniel should quash the undeniable rights of Yours Truly. Desert Dust |Edwin L. Sabin