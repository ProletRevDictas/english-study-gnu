To paint how grimy this metropolis is, “Cyberpunk” relies on the suffering of minorities, particularly women, to embellish the seedy underworld of Night City. ‘Cyberpunk 2077’ is a thrill ride through an ugly, unexamined world |Elise Favis |December 11, 2020 |Washington Post 

In more tryouts, I gave into chefly urges to embellish the seasonings, adding, then subtracting, onion powder, garlic powder, fresh garlic and Worcestershire sauce. Give in to pandemic cravings, starting with a homemade cheesesteak sandwich |David Hagedorn |December 10, 2020 |Washington Post 

Email security marketing tactics don’t have to be embellished or overly exaggerated to get noticed and be effective. The email security market is littered with false claims. How to fix it |jakemeth |November 27, 2020 |Fortune 

Linda Yoshida—arguably Instagram’s most prominent calligrapher, with 19,000 followers—has been sending postcards to representatives since 2017, embellishing them with gorgeous calligraphy and documenting them on her Instagram account. Letter-writing staved off lockdown loneliness. Now it’s getting out the vote. |Tanya Basu |September 18, 2020 |MIT Technology Review 

Special care was taken to alternate the curves of these shaped tusks in the concurrent rows, while the crown was embellished with a plume or a knob. Know Your Ancient Greek Helmets: From Attic to Phrygian |Dattatreya Mandal |May 19, 2020 |Realm of History 

If the actual facts are so repugnant to you, then why embellish them? The Woody Allen Allegations: Not So Fast |Robert B. Weide |January 27, 2014 |DAILY BEAST 

Why did you feel the need to embellish and lie about your involvement in the UOF? Rogue L.A. Cop’s Facebook Manifesto: ‘You Will Now Live the Life of Prey’ |The Daily Beast |February 8, 2013 |DAILY BEAST 

And like all bad liars, immediately gave into the urge to embellish. Friday Forum: What's the Biggest Lie You've Ever Told? |Megan McArdle |January 18, 2013 |DAILY BEAST 

It also takes years of training to be able to sew, embroider, bead, and otherwise embellish these clothes. Chanel, Armani, and Givenchy Present Their Haute-Couture Collections in Paris |Robin Givhan |July 4, 2012 |DAILY BEAST 

He loved to embellish and we were determined not to let him slip one past us. Hollywood's Loosest Cannon |Kim Masters |April 15, 2009 |DAILY BEAST 

The first is to combine the higher excellences and embellish them to the greatest advantage. Seven Discourses on Art |Joshua Reynolds 

The artist, not being able to embellish nature, has sought at least to develop its means, to increase its effect and power. The Aesthetical Essays |Friedrich Schiller 

"Nothing can embellish a beautiful face more than a narrow band drawn over the brow," says Richter. Return of the Native |Thomas Hardy 

Fruits of the loom in rarest silk and linen, embellish the chambers and luxury sits enthroned. Historic Papers on the Causes of the Civil War |Mrs. Eugenia Dunlap Potts 

The refined and noble style of the Greeks was neglected, and there was an attempt to embellish the beautiful more and more. The New Gresham Encyclopedia. Vol. 1 Part 2 |Various