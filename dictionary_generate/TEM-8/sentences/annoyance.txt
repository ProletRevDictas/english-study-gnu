Nina is shut out of the secret life she and Madeleine have shared, and as she hovers about anxiously, Madeleine’s children view her first as an annoyance and eventually as a threat. In France's Official Oscar Entry Two of Us, Two Women’s Love Story Plays Like a Thriller |Stephanie Zacharek |February 5, 2021 |Time 

Affixed to the sleigh, the dashboard provided “more effectual protection from annoyance by the throwing up of snow.” Covid-19 dashboards are vital, yet flawed, sources of public information |Jacqueline Wernimont |January 26, 2021 |Washington Post 

I am angry that several of our nation’s leaders were unwilling to deal with the small annoyance of a mask for a few hours. Rep. Watson Coleman: I’m 75. I had cancer. I got covid-19 because my GOP colleagues dismiss facts. |Bonnie Watson Coleman |January 12, 2021 |Washington Post 

Those habits, combined with their demand for personalized experiences, means millennials aren’t afraid to hide their annoyance at outbound marketing tactics when online. What can 129 million calls tell us about inbound marketing? A lot, it turns out |Sponsored Content: CallRail |December 21, 2020 |Search Engine Land 

He showed no signs of annoyance at the question, instead pivoting to a Drake song. Big East coaches pick Georgetown to finish last. Patrick Ewing says that’s about right. |Kareem Copeland |October 28, 2020 |Washington Post 

He was at times both an ally and annoyance to President Obama. How House Dems Lost Their Last Southern White Guy |James Richardson |November 9, 2014 |DAILY BEAST 

But the unknown potential health risks seem like a mild annoyance, if that, to Deen. Dinner With James Deen During Porn’s Latest HIV Scare |Emily Shire |October 17, 2014 |DAILY BEAST 

She then expressed her annoyance that the leader of the Oklahoma mosque where Nolan had worshipped refused to appear on her show. Megyn Kelly’s Really Scary Muslim |Dean Obeidallah |October 5, 2014 |DAILY BEAST 

In the winter, they can shield drivers from the annoyance of having to wipe snow and ice off their windshields. Paved Paradise |The Daily Beast |September 24, 2014 |DAILY BEAST 

Again, this is an annoyance and a reflection of a double standard against women. #YesAllWomen Has Jumped the Shark |Emily Shire |May 28, 2014 |DAILY BEAST 

The manifest annoyance of her household was thus easily accounted for, but he marveled at the strength of her bodyguard. The Red Year |Louis Tracy 

His face flushed with annoyance, and taking off his soft hat he began to beat it impatiently against his leg as he walked. The Awakening and Selected Short Stories |Kate Chopin 

But all these inconveniences are comparatively trifling; the greatest amount of annoyance begins towards the end of the voyage. A Woman's Journey Round the World |Ida Pfeiffer 

For twenty years he had shown no sign of joy or sorrow or anger, scarcely even of pleasure or annoyance. The Joyous Adventures of Aristide Pujol |William J. Locke 

You may be overheard, and give pain or cause annoyance by your untimely conversation. The Ladies' Book of Etiquette, and Manual of Politeness |Florence Hartley