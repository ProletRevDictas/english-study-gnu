Some of the carbon eventually returns to the surface, via erupting volcanoes or as diamonds, while some gets sequestered away in the deep crust or upper mantle. Earth’s rarest diamonds form from primordial carbon in the mantle |Carolyn Gramling |September 14, 2020 |Science News 

It can cut through thick, thin, and deep-dish crusts with ease while being gentle on your wrist. Pizza cutters that will get you the slice of your dreams |PopSci Commerce Team |September 2, 2020 |Popular-Science 

That is what lies beyond the outer crust of Kate Raworth’s doughnut. Is Economic Growth the Wrong Goal? (Ep. 429) |Stephen J. Dubner |August 13, 2020 |Freakonomics 

The study highlights how seismologists are increasingly acknowledging the importance of fluids in the crust, Shelly says. Machine learning helped demystify a California earthquake swarm |Carolyn Gramling |June 18, 2020 |Science News 

In the first scenario, the flare of energy remains anchored to the crust of the star via magnetic field lines. A Surprise Discovery Points to the Source of Fast Radio Bursts |Shannon Hall |June 11, 2020 |Quanta Magazine 

Cover crust with parchment paper and pour in baking beans or weights. Make ‘The Chew’s’ Carla Hall’s Pumpkin Pecan Pie |Carla Hall |December 26, 2014 |DAILY BEAST 

Kanye refuses to stomach any rejection, no matter how upper crust. Kanye West and Kim Kardashian’s Balmain Campaign: High Fashion Meets Low Culture |Amy Zimmerman |December 23, 2014 |DAILY BEAST 

It has French ingredients like leeks and tarragon, and I use puff pastry to make the crust easy! Make These Barefoot Contessa Chicken Pot Pies |Ina Garten |November 29, 2014 |DAILY BEAST 

Note: The egg wash both affixes the pastry to the dish and makes a lovely browned crust. Make These Barefoot Contessa Chicken Pot Pies |Ina Garten |November 29, 2014 |DAILY BEAST 

This chicken features a thin, abundant crust with so many facets and angles you want to call it rococo. Charlottesville Is Swimming in Finger Lickin’ Gas Station Fried Chicken |Jane & Michael Stern |May 26, 2014 |DAILY BEAST 

A very little crust thrown to the very hungry is always accepted with gratitude. The Pit Town Coronet, Volume I (of 3) |Charles James Wills 

To demonstrate them, a crust or a hair from the affected area is softened with a few drops of 20 per cent. A Manual of Clinical Diagnosis |James Campbell Todd 

A crust of bread and clear air are far preferable to luxuries enveloped in clouds of smoke and heaps of filth. Life of Richard Trevithick, Volume II (of 2) |Francis Trevithick 

These layers form what is termed the crust of the earth, and are altogether several miles in thickness. Gospel Philosophy |J. H. Ward 

She holds her knife in her right hand, and in the other a crust of bread with her toothsome morsel on it. Child Life In Town And Country |Anatole France