For a test of dry-land mirrors, Franklin and her colleagues turned to a showy group of about 40 species of scarab beetles, some with a natural gleam. Mirror beetles’ shiny bodies may not act as camouflage after all |Susan Milius |March 9, 2022 |Science News 

Baugh had brought nothing but his remarkable good will, and it was in full gleam for all to see when the president entered. The President and the Tow Truck Driver |Michael Daly |September 25, 2014 |DAILY BEAST 

Polish them until they gleam with malice, wicked glee, and non-registry gifts. The First-World Anarchist’s Guide to Weddings |Kelly Williams Brown |May 31, 2014 |DAILY BEAST 

A brown tacky gleam of years of paint accumulation covers the three-story apartment. The Weird Underground World of Urban Animal Husbandry |Dale Eisinger |May 19, 2014 |DAILY BEAST 

He asked me what I put on my nail polish to give all the colors a warm gleam. “I hear Gore’s voice and I want so much to be with him” |Anaïs Nin |October 26, 2013 |DAILY BEAST 

Evans, 31, whose eyes gleam behind a mess of blonde hair, was a formerly committed Christian whose faith had lapsed. Sunday Assembly Is the Hot New Atheist Church |Nico Hines |September 21, 2013 |DAILY BEAST 

It was the darkest hour of twilight, when there was just enough of gleam from the lurid sky, to shew the outline of objects. The Pastor's Fire-side Vol. 3 of 4 |Jane Porter 

He was sure that I was really there, and the gleam of white teeth showed a broadening dog-smile. The Soldier of the Valley |Nelson Lloyd 

Mr. Levi enters almost beside him; how white his big eyeballs gleam, as he steps in under the same cold light! Checkmate |Joseph Sheridan Le Fanu 

She smiled back at him, a pale, timid smile, like a gleam of sunshine from a wintry sky. St. Martin's Summer |Rafael Sabatini 

Indirect lighting gave a pretty gleam to the metal gadgets on the tables. Fee of the Frontier |Horace Brown Fyfe