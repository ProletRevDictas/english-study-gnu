The question sounded ludicrous as soon as it left our mouths. We Looked for Some of the Hottest Places in California. We Found Climate Injustice in a Nutshell. |by Elizabeth Weil |August 24, 2021 |ProPublica 

Fink said it’s ludicrous to keep young gymnasts from participating in other activities, such as skiing — one that many coaches discourage or even forbid their athletes from doing lest they get injured on the slopes. Time For The End Of The Teen Gymnast |Dvora Meyers |July 27, 2021 |FiveThirtyEight 

Another sequence, where the film claims that Kennedy’s greatness is evident in the number of roads and statues and airports named after him around the world, with stills of each one succeeding each other in a flurry, is perfectly ludicrous. Oliver Stone’s Crackpot JFK Conspiracy Movie Is a Hilarious, Head-Spinning Mess |Caspar Salmon |July 12, 2021 |The Daily Beast 

With each successive film, Fast & Furious becomes a little more ludicrous and a little more irresistible — and the world has embraced that idea. The rise and rise and rise of the Fast & Furious franchise |Emily VanDerWerff |June 25, 2021 |Vox 

These were absolutely ludicrous things that I told myself, mantras that I realized were ludicrous even while repeating them. Herd immunity is love |Monica Hesse |June 17, 2021 |Washington Post 

It's cheesy and ludicrous and, therefore, delightful; it's the reading equivalent of hate-watching. ‘A Gronking to Remember’ Speed Read: 8 Naughtiest Bits |Emily Shire |January 7, 2015 |DAILY BEAST 

Professor Penelope Leach told The Daily Beast it was ludicrous to monitor young children in that way. Britain May Spy on Preschoolers Searching for Potential Jihadis |Nico Hines |January 7, 2015 |DAILY BEAST 

When things reached the ludicrous crescendo of the finale—when neither story made sense—my heart sank. What On Earth Is ‘The Affair’ About? Season One’s Baffling Finale |Tim Teeman |December 22, 2014 |DAILY BEAST 

There are those who accuse their games of not really being video games at all, which is ludicrous. ‘Game of Thrones’ Interactive FanFiction: Whoops, My Friend Was Speared in the Throat |Alec Kubas-Meyer |December 13, 2014 |DAILY BEAST 

The fact that an entire interview was being conducted solely about his looks was ludicrous, yet this is the sort of thing that E! The Outrage Over Beyonce’s Bettie Page Bangs: Why the Media Must Stop Objectifying Women |Phoebe Robinson |October 15, 2014 |DAILY BEAST 

And the girl, scarce believing her good fortune, departed with a speed that bordered on the ludicrous. St. Martin's Summer |Rafael Sabatini 

Even the simplest services are performed with an almost ludicrous waste of energy. The Unsolved Riddle of Social Justice |Stephen Leacock 

I met with a ludicrous instance of the dissipation of even latter days, a few months after my marriage. The Book of Anecdotes and Budget of Fun; |Various 

In fact, to hear Skipper Worse utter the word Romarino was one of the most ludicrous things imaginable. Skipper Worse |Alexander Lange Kielland 

Mollock's discharge by the magistrate put the Chief in a very ludicrous position. Prison Memoirs of an Anarchist |Alexander Berkman