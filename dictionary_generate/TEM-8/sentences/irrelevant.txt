Prosecutors say it is irrelevant whether Caldwell personally breached the building. Former FBI official, a Navy veteran, is ‘key figure’ in Jan. 6 riot, prosecutors allege |Rachel Weiner, Spencer Hsu |February 11, 2021 |Washington Post 

That you really wanted a table saw is irrelevant to the exchange. Miss Manners: Sorry, receptionists aren’t going to remember your name |Judith Martin, Nicholas Martin, Jacobina Martin |February 5, 2021 |Washington Post 

Brands generally are realizing that they have to get behind these larger social issues or they will quickly become irrelevant. TikTok pays Group Nine Media to create science videos as Gen Z segment grows |Sara Guaglione |February 3, 2021 |Digiday 

While keyword placement is essential, avoid titles that are just filled with irrelevant keywords or variations of your target keyword. Five hacks to enhance your organic CTR and rankings in SERPs |Karl Tablante |January 27, 2021 |Search Engine Watch 

When asked, Faulconer would sort of dismiss the point as irrelevant, because it is irrelevant. Politics Report: DeMaio v. Faulconer |Scott Lewis |January 23, 2021 |Voice of San Diego 

The quandary of whether to freeze eggs or not could become irrelevant overnight. Men Will Someday Have Kids Without Women |Samantha Allen |January 3, 2015 |DAILY BEAST 

That is decidedly not to say that politics and economics are irrelevant. No Gods, No Cops, No Masters |James Poulos |January 1, 2015 |DAILY BEAST 

The added charge for access to hotel Wi-Fi is not only exploitative but increasingly irrelevant. How ‘Ethical’ Hotel Chain Marriott Gouges Guests in the Name of Wi-Fi Security |Kyle Chayka |December 31, 2014 |DAILY BEAST 

Nicki treats the obsession with her pop ambitions as an irrelevant, surface-level irritation. Nicki Minaj Bares Her Own Vulnerability on ‘The Pinkprint’ |Rawiya Kameir |December 16, 2014 |DAILY BEAST 

Today many in the economics and urban planning professions consider such factors close to irrelevant. The Rustbelt Roars Back From the Dead |Joel Kotkin, Richey Piiparinen |December 7, 2014 |DAILY BEAST 

Either they are unavoidable if your living questions are fully discussed, or they are irrelevant and they do not matter. The Salvaging Of Civilisation |H. G. (Herbert George) Wells 

All the dull months he had spent with Cash and the burros dwarfed into a pointless, irrelevant incident of his life. Cabin Fever |B. M. Bower 

Without wasting time in irrelevant chat, the seconds walked apart for consultation. Alone |Marion Harland 

Much competent evidence (to borrow from the lawyers) we must reject as irrelevant or immaterial to our main issue. A Hoosier Chronicle |Meredith Nicholson 

The mention by Hogarth of Ridley and Latimer they considered irrelevant; their fathers' heroic mood was a detail: not an entail. The Lord of the Sea |M. P. Shiel