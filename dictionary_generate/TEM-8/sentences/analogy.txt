It’s Dyson, however, that provides the most direct analogy for what the executive hoping to do at Ember. Ember names former Dyson head as consumer CEO, as the startup looks beyond the smart mug |Brian Heater |February 12, 2021 |TechCrunch 

When they took CT scans of fossilized Tiktaalik skulls, however, “the crocodilian analogy just fell apart,” he says. This ancient fish-crocodile mashup snared its prey using a key adaptation |Kate Baggaley |February 3, 2021 |Popular-Science 

The idea of using NLP to examine viruses started with an analogy. A Language AI Is Accurately Predicting Covid-19 ‘Escape’ Mutations |Shelly Fan |January 19, 2021 |Singularity Hub 

These frequencies relate to something called the Rydberg constant—the tension of the guitar string in the analogy—which appears to be one of the potentially more significant sources of uncertainty proton size-wise. A Breakthrough in Measuring the Building Blocks of Nature - Facts So Romantic |Subodh Patil |January 8, 2021 |Nautilus 

The analogy, in my view of brand keyword advertising is handing out the coupons inside the restaurant. Does Advertising Actually Work? (Part 2: Digital) (Ep. 441) |Stephen J. Dubner |November 26, 2020 |Freakonomics 

Dr. Butler, the theologian and author of "The Analogy," was born in the town and this house is still to be seen. British Highways And Byways From A Motor Car |Thomas D. Murphy 

Analogy, it must be confessed, is against Reaumur's opinion; since other kinds of silkworms make their escape by means of a fluid. An Introduction to Entomology: Vol. III (of 4) |William Kirby 

Analogy of physical processes (cutting and burning) appealed to by Sokrates — does not sustain his inference against Protagoras. Plato and the Other Companions of Sokrates, 3rd ed. Volume III (of 4) |George Grote 

Analogy between Cognition and Pleasure: in each, there are gradations of truth and purity. Plato and the Other Companions of Sokrates, 3rd ed. Volume III (of 4) |George Grote 

Analogy with Anthoceros confirmed him in his views on the reproduction of ferns. Makers of British Botany; a collection of biographies by living botanists |Various