Durable and stiffer than diamond, osmium makes for excellent pen nibs and record player needles. Transparent aluminum, and 5 more remarkable metals |Shi En Kim |August 2, 2022 |Popular-Science 

It mimics the sensation of a graphic nib dragging across paper with eerie accuracy. The most innovative gadgets of 2021 |Stan Horaczek |December 1, 2021 |Popular-Science 

Instead go for something else textural, such as cocoa nibs, toasted oats, nuts or even crushed pretzels. These no-bake peppermint chocolate bars bring holiday cheer to your cookie plate |Edd Kimber |December 2, 2020 |Washington Post 

When the cocoa beans are roasted, their shells crack to expose the nib, which is then ground into a thick paste. Four Chocolate Questions Answered |Mary Goodbody |September 29, 2009 |DAILY BEAST 

In the stormy days of his youth the old man had been a "Black Nib." Auld Licht Idylls |J. M. Barrie 

It might not be safe to kill him; but it would be safe enough to kill Nib. That Lass O' Lowrie's |Frances Hodgson Burnett 

They wrangled together for a few minutes, and then Nib was handed over. That Lass O' Lowrie's |Frances Hodgson Burnett 

Jud drew Nib closer, and turned, if possible, a trifle paler. That Lass O' Lowrie's |Frances Hodgson Burnett 

"Stopped to nib a quill after lunch," grumbled the director of the Arnold Academie, as he gave Robert a pump-handle squeeze. The Incendiary |W. A. (William Augustine) Leahy