“The institution of marraige [sic] is under attack in our society and it needs to be strengthened,” Bush wrote. Jeb Bush’s Unseen Anti-Gay Marriage Emails |Jackie Kucinich |January 9, 2015 |DAILY BEAST 

It is grandstanding for a right rarely protected unless under immediate attack. Politicians Only Love Journalists When They're Dead |Luke O’Neil |January 8, 2015 |DAILY BEAST 

“Price for adults to $4250; From 10 years to 14 years to $2125; Under 10 years free,” the listing says. Ghost Ships of the Mediterranean |Barbie Latza Nadeau |January 6, 2015 |DAILY BEAST 

“Under Suleimani several military branches have taken shape [in Iraq] which are run by Iran and the Iranian military,” he said. What an Iranian Funeral Tells Us About the Wars in Iraq |IranWire |January 6, 2015 |DAILY BEAST 

And that means they also fall under the umbrella of programs most likely to get the axe when state and federal budgets are tight. How to Solve the Policing Crisis |Keli Goff |January 5, 2015 |DAILY BEAST 

Under the one-sixth they appear as slender, highly refractive fibers with double contour and, often, curled or split ends. A Manual of Clinical Diagnosis |James Campbell Todd 

The Pontellier and Ratignolle compartments adjoined one another under the same roof. The Awakening and Selected Short Stories |Kate Chopin 

The Majesty on high has a colony and a people on earth, which otherwise is under the supremacy of the Evil One. Solomon and Solomonic Literature |Moncure Daniel Conway 

Poor Squinty ran and tried to hide under the straw, for he knew the boy was talking about him. Squinty the Comical Pig |Richard Barnum 

For several months he remained under a political cloud, charged with incompetency to quell the Philippine Rebellion. The Philippine Islands |John Foreman