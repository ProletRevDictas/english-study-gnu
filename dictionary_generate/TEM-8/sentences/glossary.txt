Bustle parent company BDG’s Inclusivity Guide, created by the company’s Inclusion Council, goes over how to promote diversity “while not being tokenizing,” and also includes a glossary of terms to avoid, according to a spokesperson. Media Briefing: How publishers with teen audiences are making their Instagram presences more inclusive |Tim Peterson |October 14, 2021 |Digiday 

The page includes a pandemic glossary, a vaccine explainer and a guide to identifying misinformation. How science museums reinvented themselves to survive the pandemic |Emily Anthes |June 4, 2021 |Science News 

If you’re new to the chat and wondering about acronyms, here’s a glossary of frequently-used chat terms. Carolyn Hax Live (March 12 | 12 p.m. ET) |Carolyn Hax |March 12, 2021 |Washington Post 

A glossary of what all those strange phrases in classic Christmas songs really mean. The Most Confusing Christmas Music Lyrics Explained (VIDEO) |Kevin Fallon |December 24, 2014 |DAILY BEAST 

The nine-page glossary in the back of the book is helpful, but only up to a point. ‘The Bone Season’ Could Be the Next ‘Hunger Games,’ But Is It Any Good? |Leila Sales |August 21, 2013 |DAILY BEAST 

The downside is that you may read in a perpetual state of glossary-checking and Goodreads-searching. ‘The Bone Season’ Could Be the Next ‘Hunger Games,’ But Is It Any Good? |Leila Sales |August 21, 2013 |DAILY BEAST 

The essential glossary here: “Molly” is a slang word for MDMA, a drug often used for date rape; “that” is the date raping. 11 Ways Rappers Are Just Like Right-Wing Radio Hosts |Kevin Fallon |March 29, 2013 |DAILY BEAST 

Plus, read an account of an escape from Scientology's Sea Org and a Scientology glossary. In Tom Cruise Divorce, Scientology Loses Fashion Icons |Rebecca Dana |July 6, 2012 |DAILY BEAST 

He prepared a glossary of provincial and archological words, intended for a supplement to Johnson's Dictionary. The Every Day Book of History and Chronology |Joel Munsell 

It has a complete glossary of terms, and is illustrated with two hundred original drawings. Practical Mechanics for Boys |J. S. Zerbe 

Except for yogh, h-stroke and paired , unusual letters appear only in the editorial material (introduction, notes and glossary). King Horn, Floriz and Blauncheflur, The Assumption of Our Lady |Various 

The preceding examples give most of the more important weak verbs; others can be found in the Glossary. Chaucer's Works, Volume 6 (of 7) -- Introduction, Glossary, and Indexes |Geoffrey Chaucer 

Also, of the pronoun me; as in d m'endyte, G 32; see M' in the Glossary, p. 157. Chaucer's Works, Volume 6 (of 7) -- Introduction, Glossary, and Indexes |Geoffrey Chaucer