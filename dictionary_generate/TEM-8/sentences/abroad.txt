Since her arrival in Dubai, Kriel has welcomed members of the city’s well-heeled Jewish community and travelers from abroad, including the chief rabbi of Poland, to her home for holiday and Sabbath dinners. The Newest Fusion Cuisine: Kosherati |Fiona Zublin |September 16, 2020 |Ozy 

By redoubling its investments in clean-energy innovation at home and recommitting to its promises abroad, the United States can speed the development of technologies critical for deep decarbonization. To confront the climate crisis, the US should launch a National Energy Innovation Mission |Amy Nordrum |September 15, 2020 |MIT Technology Review 

According to its official website, the fund was set up to collect donations from India and abroad to “undertake and support relief or assistance of any kind relating to a public health emergency or any other kind of emergency.” Lack of transparency over Modi’s Covid-19 relief fund hurts Indian democracy |Vidya Venkat |September 14, 2020 |Quartz 

In 2018, Berkshire invested in two payments companies abroad, plowing $600 million altogether into Brazil’s StoneCo and India’s Paytm in deals that were largely attributed to Todd Combs, one of Berkshire’s portfolio managers. Warren Buffett’s Berkshire Hathaway buys up Snowflake ahead of its IPO |Lucinda Shen |September 9, 2020 |Fortune 

After several delays from its planned July release date, the movie opened abroad on August 26 and did reasonably well, grossing about $53 million in its opening weekend, higher than analysts’ projected $40 million. This is the most important movie weekend of the year |Alissa Wilkinson |September 4, 2020 |Vox 

I had been studying abroad in London, and came back to finish the semester at Tufts. Everyone at This Dinner Party Has Lost Someone |Samantha Levine |January 6, 2015 |DAILY BEAST 

A single father, he had been living abroad and returned when his mother was diagnosed with cancer. Everyone at This Dinner Party Has Lost Someone |Samantha Levine |January 6, 2015 |DAILY BEAST 

Groups like the Crips and MS-13 have spread from coast to coast, and even abroad. The Daily Beast’s Best Longreads, Dec 29-Jan 4, 2014 |William Boot |January 4, 2015 |DAILY BEAST 

Those who served abroad were treated with suspicion that they had been infected by European diplomacy. U.S. Embassies Have Always Been for Sale |William O’Connor |January 2, 2015 |DAILY BEAST 

Youssef said the jailings are not only driving the community underground but pushing many to move abroad. Sisi Is Persecuting, Prosecuting, and Publicly Shaming Egypt’s Gays |Bel Trew |December 30, 2014 |DAILY BEAST 

Like every other Spanish general in supreme command abroad, Polavieja had his enemies in Spain. The Philippine Islands |John Foreman 

While the fortress was undermining at home, they were not idle, who were preparing to storm it from abroad. The Pastor's Fire-side Vol. 3 of 4 |Jane Porter 

And thou didst multiply riddles in parables: thy name went abroad to the islands far off, and thou wast beloved in thy peace. The Bible, Douay-Rheims Version |Various 

Germany invests money abroad, but she seems to borrow as much, and more, in the discount markets of London and Paris. Readings in Money and Banking |Chester Arthur Phillips 

In the town the European mode of living is entirely prevalent—more so than in any other place abroad that I have seen. A Woman's Journey Round the World |Ida Pfeiffer