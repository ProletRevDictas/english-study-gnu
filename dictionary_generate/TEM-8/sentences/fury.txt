The American people are perfectly capable of judging the policies that affect their lives and conveying the fury they would feel toward politicians who would threaten them. The definitive case for ending the filibuster |Ezra Klein |October 1, 2020 |Vox 

It is easy to imagine a Republican Party that tips deeper into ethnonationalist grievance and social traditionalism in the coming years and builds a fuller agenda through which to express its furies. The definitive case for ending the filibuster |Ezra Klein |October 1, 2020 |Vox 

It poses a unique opportunity for bad actors to inject misinformation into the situation and for fury and frustration to build. Most Americans understand that we probably won’t see a winner on election night |Philip Bump |September 23, 2020 |Washington Post 

As he has throughout the course of his political career, he doubled down and struck back at his opponents with equal if not greater fury. Sexual misconduct allegations are playing out just like they did in 2016—even after the #MeToo movement |reymashayekhi |September 22, 2020 |Fortune 

It’s impossible to attribute the fury of any one storm to climate change, but scientists have observed a statistically significant link between warmer waters and hurricane intensity. What’s behind August 2020’s extreme weather? Climate change and bad luck |Carolyn Gramling |August 27, 2020 |Science News 

And black fury toward cops today is fueled by historic economic disparities and by the economic disaster of the past decade plus. No Gods, No Cops, No Masters |James Poulos |January 1, 2015 |DAILY BEAST 

From righteous fury to faux indignation, everything we got mad about in 2014—and how outrage has taken over our lives. The Daily Beast’s Best Longreads, Dec 15-21, 2014 |William Boot |December 21, 2014 |DAILY BEAST 

The song is about rage and fury and passion, and I had a lot of pain that I wanted to release. Shut Up, Lady Gaga’s Rape Isn’t About You |Amy Zimmerman |December 4, 2014 |DAILY BEAST 

Head mistress Jean Harris is the ultimate proof of “Hell Hath No Fury like a Woman Scorned.” Headmasters Behaving Badly |Emily Shire |November 29, 2014 |DAILY BEAST 

Photos: Fury at the Ferguson Decision The fight for a fair justice system has gone far beyond Ferguson. Mike Brown Dies, a Generation Comes Alive |Roland S. Martin |November 25, 2014 |DAILY BEAST 

The attack was commenced by the allies under Blucher upon the French centre, with a fury irresistible. The Every Day Book of History and Chronology |Joel Munsell 

Meanwhile the cabal against the ruined Ripperda raged with redoubled fury in the Spanish cabinet. The Pastor's Fire-side Vol. 3 of 4 |Jane Porter 

It burst upon them ere long with awful fury and grandeur, the elements warring with incredible vehemence. Hunting the Lions |R.M. Ballantyne 

That struck the people in wrath with an incurable wound, that brought nations under in fury, that persecuted in a cruel manner. The Bible, Douay-Rheims Version |Various 

Because in far distant times he saved the life of a Chinaman from the fury of a crocodile. Alila, Our Little Philippine Cousin |Mary Hazelton Wade