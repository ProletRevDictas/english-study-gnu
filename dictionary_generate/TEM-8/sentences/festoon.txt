These form one of the many island groups that hang like a fringe or festoon on the skirt of the continent of Asia. Alila, Our Little Philippine Cousin |Mary Hazelton Wade 

Between the posts hung a festoon of signalling flags, long pointed strips of bunting with red balls or blue on them. Hyacinth |George A. Birmingham 

For decoration use autumnal grasses, wheat, oats and corn, and festoon strings of them wherever possible. Suppers |Paul Pierce 

Bank the fire-place and corners with boughs of autumn leaves, and festoon them in garlands wherever there is a vacant place. Suppers |Paul Pierce 

"But I am not a fortune-teller," he said, letting his head drop into a festoon of towel, and towelling away at his two ears. Great Expectations |Charles Dickens