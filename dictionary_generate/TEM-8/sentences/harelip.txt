Harwood might treat her, as she said, like a step-child with a harelip, but occasionally she made him sit up. A Hoosier Chronicle |Meredith Nicholson 

Common illustrations of such deforming developmental accidents are harelip, cleft palate, and club foot. The Mother and Her Child |William S. Sadler 

Jean was even more illfavored, having a scar across his mouth which gave him an artificial harelip. Greener Than You Think |Ward Moore 

There is just the possibility that a contrivance like our harelip pin with a figure of eight thread may be indicated. Surgical Instruments in Greek and Roman Times |John Stewart Milne 

He was a man with a harelip and a pure heart, and everybody said he was as true as steel. Some Rambling Notes of an Idle Excursion |Mark Twain (Samuel Clemens)