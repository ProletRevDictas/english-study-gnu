When the ad shifts to Hunter Biden, it makes another tricky maneuver. Trump’s most popular YouTube ad is a stew of manipulated video |Glenn Kessler, Meg Kelly |September 17, 2020 |Washington Post 

By design, comparing the performance of RSAs to ETAs is tricky because they are two different animals. RSAs: Are they living up to the promise? It depends |Ginny Marvin |September 9, 2020 |Search Engine Land 

Below are a range of options to tackle the trickiest of scalp problems. Scalp scrubs that banish scaly patches and build-up |PopSci Commerce Team |September 4, 2020 |Popular-Science 

Even so, some say that navigating the talk can be tricky especially when doing so with a client. ‘Safe and healthy’: As in person meetings resume, business execs are having the coronavirus ‘talk’ |Kristina Monllos |September 4, 2020 |Digiday 

We opened that up nationwide for free to the entire country because we knew navigating a relationship and dating in this environment was tricky. Match’s CEO explains how dating has changed during the COVID pandemic |Danielle Abril |September 3, 2020 |Fortune 

The digital dating sphere can prove tricky, and bruising, for the trans user. Grindr’s Trans Dating Problem |David Levesley |January 9, 2015 |DAILY BEAST 

Their solidified friendship is one of the most touching details of the premiere, but it also puts Branson in a tricky predicament. What Downton’s Fashion Really Means |Katie Baker |January 2, 2015 |DAILY BEAST 

The power delivered by the rocket motor was uneven and tricky to control. Virgin Galactic’s Flight Path to Disaster: A Clash of High Risk and Hyperbole |Clive Irving |November 1, 2014 |DAILY BEAST 

It is a tricky and perilous path, but there are no realistic alternatives. There’s Only One Way to Beat ISIS: Work With Assad and Iran |Leslie H. Gelb |October 18, 2014 |DAILY BEAST 

What makes Islam so tricky that it trips up even the usually more discerning among us? ISIS and BS |Amal Ghandour |October 15, 2014 |DAILY BEAST 

The English have too much pride to be tricky or shabby, even in the essentially corrupting relation of buyer and seller. Glances at Europe |Horace Greeley 

A man's mind is a tricky thing—or, speaking more exactly, a man's emotions are tricky things. Cabin Fever |B. M. Bower 

He was concerned with the villainous intrigues of Cerizet, his copy-clerk, and with Theodose de la Peyrade, the tricky lawyer. Repertory Of The Comedie Humaine, Complete, A -- Z |Anatole Cerfberr and Jules Franois Christophe 

The thing, as he had said, was tricky; it came and went; and the fear of losing it was the most overpowering of all fears. The Creators |May Sinclair 

You may have married a healthy animal, but animals are tricky and uncertain. The Eugenic Marriage, Vol. 3 (of 4) |W. Grant Hague