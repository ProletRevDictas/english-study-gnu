There are many problems plaguing California at the moment that demand urgent attention. Sacramento Report: Bipartisan Support for a Special Session |Sara Libby |September 11, 2020 |Voice of San Diego 

Nothing is that urgent, and if it is, your team will call or text you. 17 extremely useful productivity tips from this year’s 40 Under 40 |Maria Aspan |September 6, 2020 |Fortune 

The more obvious something seems, the more urgent the need to question it. The business advice Socrates would give if he wrote a management book today |jakemeth |August 25, 2020 |Fortune 

Sustainability isn’t just an urgent ethical issue — it sells. Sky News, Hearst UK and RT are Digiday Media Awards Europe winners |Digiday Awards |August 21, 2020 |Digiday 

“Agencies recognize that there’s an urgent need to envision themselves as a group that values diversity but they’re not there yet,” said Nandi Welch, co-founder and head of business strategy for the brand consultancy Rupture. ‘Urgent need’: How agencies are deploying diversity and inclusion execs, forming new councils to create more equitable companies |Kristina Monllos |August 21, 2020 |Digiday 

The need for increased community policing is more urgent than ever before. How to Solve the Policing Crisis |Keli Goff |January 5, 2015 |DAILY BEAST 

“The recent earthquakes make this project urgent,” Franceschini told reporters. Florence Preps ‘David’ for the Big One |Barbie Latza Nadeau |December 25, 2014 |DAILY BEAST 

Then, depending on how urgent I think it is to get it, sometimes I have to go back home and drop it off. #Setinthestreet: Your Street Corner Is Their Art Project |James Joiner |December 24, 2014 |DAILY BEAST 

Is there anyone who thinks the urgent problem we need to solve in Washington, D.C. is how to allow more spending on campaigns? Just What We Needed: More Campaign Spending |Mark McKinnon |December 8, 2014 |DAILY BEAST 

We need to strike a balance between creating false alarms and letting any urgent medical matters fall through the cracks. Did This Flu Vaccine Kill 13? |Barbie Latza Nadeau |December 2, 2014 |DAILY BEAST 

They are very urgent questions; our sons and daughters will have to begin to deal with them from the moment they leave college. The Salvaging Of Civilisation |H. G. (Herbert George) Wells 

Nor is there anyone of that order who talks of going back to those kingdoms without the most urgent reason making it necessary. The Philippine Islands, 1493-1898, Volume XX, 1621-1624 |Various 

I humbly petition your Majesty to be pleased to order that he be despatched here, so that this so urgent need may be supplied. The Philippine Islands, 1493-1898, Volume XX, 1621-1624 |Various 

Never again would she ignore an urgent telegram, though she did not believe that this telegram had any real importance. Hilda Lessways |Arnold Bennett 

In the first place, there is an immediate and urgent demand for at least Half a Million comfortable rain-proof dwellings. Glances at Europe |Horace Greeley