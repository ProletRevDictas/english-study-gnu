There are lots of things, including changing the kind of inner dialog, that can mitigate anxiety. A Q&A with Scott Stossel, Author of ‘My Age of Anxiety: Fear, Hope, Dread, and the Search for Peace of Mind’ |Jesse Singal |February 20, 2014 |DAILY BEAST 

I remember being amazed at how scary The Sixth Sense was when I went to do ADR [automated dialog replacement]. Whatever Happened to ‘The O.C.’ Star Mischa Barton? |Ramin Setoodeh |March 26, 2013 |DAILY BEAST 

In End of Watch, their dialog is spiced with a seemingly endless stream of “bros” and “dudes.” Jake Gyllenhaal & Michael Peña on Their ‘End of Watch’ Bromance |Chris Lee |September 19, 2012 |DAILY BEAST 

We're fully aware of that work and we have an ongoing dialog. Al Sharpton’s Conflicting Roles in the Trayvon Martin Case |Howard Kurtz |March 26, 2012 |DAILY BEAST 

The daughters take part in the dialog and begin to abuse each other, being jealous of Edmund. Tolstoy on Shakespeare |Leo Tolstoy 

Narrative sentences followed by quoted dialog sometimes end with commas rather than with periods. The Wasted Generation |Owen Johnson 

With minor variations in the dialog, and with longer and more frequent silences, it almost followed the Wednesday night script. The Fourth R |George Oliver Smith 

However, that is a matter of no consequence, as we are both familiar with the dialog—, or rather the service. Punchinello, Vol. 2., No. 32, November 5, 1870 |Various 

Your speech is not a monolog, but a dialog, in which you are the speaker, and the auditor a silent tho questioning listener. Model Speeches for Practise |Grenville Kleiser