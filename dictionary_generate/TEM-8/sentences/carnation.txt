Xenophyophores growing on the sediment can resemble carnations, roses, or lattices, and like corals in shallow water, their bodies create a unique habitat in the deep sea. The Largest Cells on Earth - Issue 99: Universality |Rebecca R. Helm |May 5, 2021 |Nautilus 

He purchased dozens of carnations in a variety of bright colors and paid for them with money he earned from dog-walking. After the shooting, a boy gave flowers to workers at King Soopers stores near the attack |Sydney Page |March 25, 2021 |Washington Post 

Most of the Mothers of Srebrenica have no living relatives to honor and love them on this day, or to bring them the traditional Bosnian Mother’s Day gift of a bunch of red carnations. Angelina Jolie Talks to Director Jasmila Zbanic About Quo Vadis, Aida? and the Role of Art in Healing Trauma |Angelina Jolie |March 8, 2021 |Time 

Justin, a 4-year-old West Highland terrier from Long Island, was having his face hair-sprayed into the shape of a carnation. Backstage at the 2013 Westminster Dog Show, Won by Banana Joe |Isabel Wilkinson |February 13, 2013 |DAILY BEAST 

He was carrying himself with less than his usual stoop, he wore a red carnation in his buttonhole. Mr. Grex of Monte Carlo |E. Phillips Oppenheim 

Cannot you see the lovely Adele fastening the carnation to the lapel so that papa may be gay upon the street? Sixes and Sevens |O. Henry 

In Major Ellison's buttonhole there was a carnation and a rosebud backed by a geranium leaf. Sixes and Sevens |O. Henry 

While I think of it, I'll draw in a little mite of this red into my carnation pink. The Village Watch-Tower |(AKA Kate Douglas Riggs) Kate Douglas Wiggin 

The lips too are figured out; but where's the carnation dew, the pouting ripeness that tempts the taste in the original? The Beaux-Stratagem |George Farquhar