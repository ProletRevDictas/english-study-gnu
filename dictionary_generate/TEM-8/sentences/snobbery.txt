There’s no justification for snobbery of pure over applied work. Lessons for a Young Scientist - Issue 112: Inspiration |Martin Rees |January 12, 2022 |Nautilus 

It creates a type of detachment, coldness, and snobbery that is disenfranchising. It’s Time to Reconsider What Skiers Think Is Cool |cobrien |October 31, 2021 |Outside Online 

I feel like a trainspotter, noting the distinct edges and grooves that make the shared enthusiasms of Gen Z infinitely more interesting than whatever media and culture fueled my own aesthetic snobbery. The great American cool |Safy-Hallan Farah |July 14, 2021 |Vox 

The Spotify roasting AI that’s been making the rounds this week is a fun exercise in music snobbery. Music made 2020 better, but we failed to make 2020 better for musicians |Brian Heater |December 25, 2020 |TechCrunch 

His absolute snobbery is bizarrely refreshing, while the Just Like You's around him try to seem as “normal” as possible. Sting and Hillary Are Just Like You: How the Very Rich Play at Being Very Ordinary |Tim Teeman |June 24, 2014 |DAILY BEAST 

Wellie-snobbery is terrible but growing vice among the British upper classes. Pippa Instagrammed in Front of Pile of Dead Pheasants at Shoot |Tom Sykes |October 8, 2013 |DAILY BEAST 

My theory was that a personal and geographic food bias was at work, a kind of coastal snobbery. Wall Street Gets Punked Again With Sprouts Farmers Market IPO |Daniel Gross |August 7, 2013 |DAILY BEAST 

Both went to Oxford University and chafed at the snobbery of English elites. The Margaret Thatcher, Rupert Murdoch Connection |Peter Jukes |April 9, 2013 |DAILY BEAST 

This is because cruising stories allow the media to indulge in two of their favourite traits: envy and snobbery. In Defense of Cruises: Ignore the Carnival Sewage Disaster |Andrew Roberts |February 15, 2013 |DAILY BEAST 

That the Business College should no longer require me I could understand—for snobbery plays a terrible part in business. In Accordance with the Evidence |Oliver Onions 

"Ah kin lick any three Neboes wid mah toes an' teeth," he would boast in religious snobbery. Mountain |Clement Wood 

An assembly of two thousand snobs will never stint its applause to an author who chastises snobbery. The English Stage |Augustin Filon 

This gentleman—for he was no less than that—was a man well-read, and his tribute was not inspired by mere snobbery. Mark Twain, A Biography, 1835-1910, Complete |Albert Bigelow Paine 

The snobbery of the "social column" would really be amusing were it not so painfully apparent. The Sign of Silence |William Le Queux