The previous terms hadn’t included language on the formation of that committee. Oracle, Bytedance, and U.S. Treasury tentatively agree on terms for TikTok bid |radmarya |September 17, 2020 |Fortune 

According to a 2016 survey obtained by the committee, 39% of Boeing’s Authorized Representatives, senior engineers who conducted reviews for FAA, at times perceived “undue pressure” on them from management. Boeing crashes were the “horrific culmination” of multiple mistakes, House report says |kdunn6 |September 16, 2020 |Fortune 

Ring refused to give the House committee a copy of the ethics memorandum in the McDonald’s case, but he agreed to let committee staff review it at NLRB headquarters. “Cover Up”: House Democrats Subpoena Documents That NLRB Refused to Share in Ethics Investigation |by Ian MacDougall |September 15, 2020 |ProPublica 

By Monday, reactions to the Oracle-TikTok deal—which is subject to final review from a powerful interagency committee called CFIUS—began pouring in. Twitter users sound off about Oracle’s TikTok deal |Jeff |September 14, 2020 |Fortune 

Halpern was the chair in 2016 of a committee tasked with overseeing the SANDAG transportation program that was the subject of the scandal that eventually remade the agency. Politics Report: Forged Footnote 15 |Scott Lewis and Andrew Keatts |September 12, 2020 |Voice of San Diego 

Unlike former Florida Gov. Jeb Bush and Sen. Rand Paul of Kentucky, Huckabee is not immediately forming an exploratory committee. Can Huckabee Convert the GOP’s Moneymen? |Lloyd Green |January 4, 2015 |DAILY BEAST 

Portraits of Lincoln and Eisenhower were removed from the offices of the Republican National Committee. Ed Brooke: The Senate's Civil Rights Pioneer and Prophet of a Post-Racial America |John Avlon |January 4, 2015 |DAILY BEAST 

Tom Rust, a spokesman for the House Ethics Committee, declined to comment to The Daily Beast. The Felon Who Wouldn’t Leave Congress |Ben Jacobs, David Freedlander |December 23, 2014 |DAILY BEAST 

Still, his conviction will restart a House Ethics Committee investigation into his actions. The Felon Who Wouldn’t Leave Congress |Ben Jacobs, David Freedlander |December 23, 2014 |DAILY BEAST 

“I think that all Slovaks should feel offended,” said MP Tomas Galbavy, who served on the parliamentary culture committee. When Countries Lose Their Shit Over American Movies |Asawin Suebsaeng |December 17, 2014 |DAILY BEAST 

In the case of the Midland—they appointed a committee of inquiry, and the directors assented to the appointment. Fifty Years of Railway Life in England, Scotland and Ireland |Joseph Tatlow 

Mr. Wainwright acted as secretary and I kept the minute book and papers relating to the business of the committee. Fifty Years of Railway Life in England, Scotland and Ireland |Joseph Tatlow 

This Committee shall be divided into three sections, viz.:—Of diplomacy; of the navy; and of the army. The Philippine Islands |John Foreman 

New proposals regarding telephone charges are expected as soon as the Select Committee has reported. Punch, or the London Charivari, Volume 158, April 28, 1920 |Various 

I hope the committee will consider J. Price's report as from a disappointed man. Life of Richard Trevithick, Volume II (of 2) |Francis Trevithick