Drones also snapped photos of 105 whales to help the researchers estimate gulp size. Baleen whales eat — and poop — a lot more than we thought |Jonathan Lambert |December 1, 2021 |Science News For Students 

They used drones and sonar to measure the whale’s mouths, and the size of krill swarms, which gave an estimate of how much a whale could scoop up in a gulp. Biologists vastly underestimated how much whales eat and poop |Philip Kiefer |November 3, 2021 |Popular-Science 

Some scientists thought that krill — the tiny crustaceans that many whales eat in gargantuan gulps — would explode in number as a result, mostly free from the feeding pressure of the largest animals that have ever lived. Baleen whales eat (and poop) a lot more than we realized |Jonathan Lambert |November 3, 2021 |Science News 

Aerial drones snapped photos of 105 whales, which the researchers used to estimate gulp size. Baleen whales eat (and poop) a lot more than we realized |Jonathan Lambert |November 3, 2021 |Science News 

Baikal seals may be using these teeth to efficiently sieve their plankton prize from the lake, expelling excess water with every gulp, the researchers say. Using comb-shaped teeth, Baikal seals feed on tiny crustaceans like whales do |Jake Buehler |December 11, 2020 |Science News 

Even so distant, I can taste the grief, / Bitter and sharp with stalks, he made you gulp…Where bridal London bows the other way. Viva Hate: Inside the World of Morrissey |Michael Weiss |December 23, 2013 |DAILY BEAST 

But even allowing for the fact that the state elected Mama Big Gulp as its governor, the Alaska number has to be a solid majority. The NRA Can Still Lose |Michael Tomasky |March 28, 2013 |DAILY BEAST 

The sugar in a $1.39 Big Gulp soda at 7-Eleven accounts for only a few cents of its cost. How Public Health Experts Turned Corporations into Public Enemy #1 |Megan McArdle |March 25, 2013 |DAILY BEAST 

While delivering her speech at CPAC, Sarah Palin drank from a big gulp. Sarah Palin's Big Gulp of a Speech |Ben Jacobs |March 16, 2013 |DAILY BEAST 

There was even a speech from Sarah Palin who made a joke about her "rack" and sipped a big gulp on stage. What Do We Know About 2016 After the CPAC Straw Poll? |Ben Jacobs |March 16, 2013 |DAILY BEAST 

He devoured it whole with a kind of visual gulp—a flash; the entire meaning first, then lines, then separate words. The Wave |Algernon Blackwood 

"Five minutes to twelve, baby," said the old man, and his voice had a gulp in it that broke June down. The Trail of the Lonesome Pine |John Fox, Jr. 

He made the speech with a gulp, as though it were distasteful to him. Mary Gray |Katharine Tynan 

He got the question out with a separate gulp for each separate word. From Place to Place |Irvin S. Cobb 

"They are all round us in the scrub; you never know where they are," Eustace said with a gulp. Queensland Cousins |Eleanor Luisa Haverfield