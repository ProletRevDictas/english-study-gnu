We are driving down the New Jersey Turnpike on a raw Sunday morning in March. When An Adopted Child Won’t Attach |Tina Traster |May 2, 2014 |DAILY BEAST 

He was heading toward home on the Pennsylvania Turnpike when he found himself pulling over. Central Park’s Carriages Saved This Horse |Michael Daly |April 24, 2014 |DAILY BEAST 

Jersey Turnpike (v.)—to perform a dance move in which one jams his/her rear end against a man's crotch and then bends over. The Real Jersey Dictionary, Vol. 3 |Jaimie Etkin |March 24, 2011 |DAILY BEAST 

In the "good old coaching days" the turnpike tolls paid on a coach running daily from here to London amounted to £1,428 per year. Showell's Dictionary of Birmingham |Thomas T. Harman and Walter Showell 

The place was a field, the first beyond the turnpike gate, and within a mile of the city. Blackwood's Edinburgh Magazine, No. CCCXXXIX. January, 1844. Vol. LV. |Various 

A country girl, riding by a turnpike-road without paying toll, the gate-keeper hailed her and demanded his fee. The Book of Anecdotes and Budget of Fun; |Various 

Sixty, nay fifty, years ago, there were six toll-houses and turnpike bars between London and Portsmouth. The Portsmouth Road and Its Tributaries |Charles G. Harper 

Chicot stopped at a turnpike, and asked the man if he had seen three travelers pass on mules. Chicot the Jester |Alexandre Dumas, Pere