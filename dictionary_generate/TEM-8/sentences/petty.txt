Look, of course it makes celebrities look weak and petty to get all het up about Chris Discourse now — Now! How Chris Pratt became the internet’s least favorite Chris |Constance Grady |October 22, 2020 |Vox 

For another, many of the alleged crimes, like petty theft, are not that serious—while others aren’t specified at all. Live facial recognition is tracking kids suspected of being criminals |Karen Hao |October 9, 2020 |MIT Technology Review 

What she is watching is mostly petty trivialities, and the power of the novel comes from Batuman’s ability to render those trivialities worthwhile. Why art matters at the end of the world |Constance Grady |September 25, 2020 |Vox 

Imagine a president using that awesome power again for good rather than for exacting petty revenge on real and imagined enemies. Vote for Biden (duh) |Kevin Naff |September 10, 2020 |Washington Blade 

Another employee reported Carson searched a school site looking for employees mishandling petty cash. Accusations Flew, Then National School District Official Got Paid to Resign |Ashly McGlone |July 20, 2020 |Voice of San Diego 

Petty, shade, and thirst are my favorite human “virtues” and the trifecta of any good series of “stories.” ‘Empire’ Review: Hip-Hop Musical Chairs with an Insane Soap Opera Twist |Judnick Mayard |January 8, 2015 |DAILY BEAST 

In no way, he said, did Brown deserve to die for what began as petty theft. A Black Cop’s Tough Words for Mike Brown |Mary M. Chapman |December 3, 2014 |DAILY BEAST 

No more cynically cutting opposition candidates out of a district for petty political purposes. Hate Hyper-Partisanship? Support Redistricting Reform Now |John Avlon |November 3, 2014 |DAILY BEAST 

Of how incredibly petty the offense can be and how insanely disproportionate the retaliation can be. Of Gamers, Gates, and Disco Demolition: The Roots of Reactionary Rage |Arthur Chu |October 16, 2014 |DAILY BEAST 

But when the end result is tens of millions raised, do the shades of vanity or petty grudges truly matter? #IceBucketChallenge Wisdom From 'Jackass' Steve-O |Kevin Zawacki |August 21, 2014 |DAILY BEAST 

Aunty Rosa had credited him in the past with petty cunning and stratagem that had never entered into his head. Kipling Stories and Poems Every Child Should Know, Book II |Rudyard Kipling 

On the 25th of November following the magistrates began to sit daily at Petty Sessions. Showell's Dictionary of Birmingham |Thomas T. Harman and Walter Showell 

He became the low-born, petty tradesman, using the language of the hands of his jam factory. The Joyous Adventures of Aristide Pujol |William J. Locke 

He would give them money to return to their lines and for petty expenses en route. The Philippine Islands |John Foreman 

Of the numerous petty divinities which watched over the child's early years we have already given some account. The Religion of Ancient Rome |Cyril Bailey