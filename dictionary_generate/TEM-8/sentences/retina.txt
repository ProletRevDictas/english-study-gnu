The lowest layer makes predictions about actual sensory input — say, the photons falling on the retina. Artificial Neural Nets Finally Yield Clues to How Brains Learn |Anil Ananthaswamy |February 18, 2021 |Quanta Magazine 

Computer vision and bioengineered retinas tag-teamed to bolster artificial vision. Scientists Made a Biohybrid Nose Using Cells From Mosquitoes |Shelly Fan |January 26, 2021 |Singularity Hub 

The best VR displays are somewhere between super-screen-door and retina resolution. New Display Packs 10,000 PPI and Could Paint Stunning VR Worlds |Jason Dorrier |November 1, 2020 |Singularity Hub 

That means for VR to hit retina resolution, we’ll need displays with way more, way smaller pixels. New Display Packs 10,000 PPI and Could Paint Stunning VR Worlds |Jason Dorrier |November 1, 2020 |Singularity Hub 

The tissue there, known as the retina, contains millions of light-sensitive cells. Explainer: How our eyes make sense of light |Tina Hesman Saey |July 16, 2020 |Science News For Students 

The updated iPad Mini, which will start at $399 ($599 for cell), gets a retina display as well as better performance. Apple Launches New iPads in Effort to Boost Sales |William O’Connor |October 22, 2013 |DAILY BEAST 

The reality of the Retina MacBook is a study in compromises. 5 Reasons I Hate My New MacBook Pro: A Geek’s Critique |Jason Stewart |June 15, 2012 |DAILY BEAST 

It is a building block of protein and found in large amounts in the brain, retina, heart, and blood platelets. Was Demi Moore Really Addicted to Red Bull? |Anneli Rufus |February 1, 2012 |DAILY BEAST 

What is possibly lovable about the cornea—or the iris or the retina for that matter? Found: Rand Paul's Secret Papers |Kent Sepkowitz |June 21, 2010 |DAILY BEAST 

This apparition fixed itself upon her mental retina like a marvellous dream. Urania |Camille Flammarion 

It is not the retina which is affected by a positive reality, it is the optic thalami of the brain which are excited. Urania |Camille Flammarion 

A diminution or total loss of sight, arising from paralysis of the retina or optic nerve. Cooley's Cyclopdia of Practical Receipts and Collateral Information in the Arts, Manufactures, Professions, and Trades..., Sixth Edition, Volume I |Arnold Cooley 

I suppose it to be a memory of looking at the sun—a quick glance at the sun leaves something such an impression on the retina. The Hills and the Vale |Richard Jefferies 

We can, however, move the images on the retina by the aid of prisms without movement of the object. Schweigger on Squint |C. Schweigger