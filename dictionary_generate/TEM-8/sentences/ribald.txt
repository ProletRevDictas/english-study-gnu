He is the master of ribald repartee; he could be cutting without being catty. 'The Village Voice' Was Crazy to Fire Him: 5 Reasons Why Michael Musto Matters |Tricia Romano |May 17, 2013 |DAILY BEAST 

“Bear and the Maiden Fair, The”: A traditional, if exceedingly ribald, song that is quite popular throughout Westeros. ‘Game of Thrones’ Season 3 for Dummies |Jace Lacob |March 27, 2013 |DAILY BEAST 

The writer Daphne Merkin, who has covered ribald territory herself, proved just how game she is when contacted for this article. Hollywood's Most Private Accessory |Claire Howorth |September 20, 2010 |DAILY BEAST 

At ABC, their ribald cop show, The Job, was jerked around the schedule before finally being canceled in 2002 after two seasons. Denis Leary to the Rescue |Joel Keller |April 7, 2009 |DAILY BEAST 

Nerto becomes a nun, but Don Rodrigue, with a band of ribald followers, succeeds in carrying her off with all the other nuns. Frdric Mistral |Charles Alfred Downer 

At the center of the sanctuary stood Perrette the Ribald, her hair disheveled like a Bacchante's. The Pilgrim's Shell or Fergan the Quarryman |Eugne Sue 

And at the trail's end the unkempt, ribald crew swarmed their dark and dirty camp as a band of pirates a galleon. Blazed Trail Stories |Stewart Edward White 

Fromentin was singing,—a ribald marching song, an unprintable thing, salacious and vilifying the Boches. The Wasted Generation |Owen Johnson 

He replied with a ribald tirade, and she warned that she would count ten-that if he remained a second longer she would fire. Mark Twain, A Biography, 1835-1910, Complete |Albert Bigelow Paine