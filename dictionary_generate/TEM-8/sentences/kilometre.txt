Finally we stopped about a kilometre from a little village, which must be nameless. Over the Front in an Aeroplane and Scenes Inside the French and Flemish Trenches |Ralph Pulitzer 

For a kilometre, it was a struggle, side by side, but an unequal struggle in which the issue was certain. The Extraordinary Adventures of Arsene Lupin, Gentleman-Burglar |Maurice Leblanc 

Theodorics tomb is in La Rotonda, a kilometre or more from Ravenna in the midst of a vineyard. Italian Highways and Byways from a Motor Car |Francis Miltoun 

There are sixty-six kilometres of roads to the square kilometre (kilometre carré). The Automobilist Abroad |M. F. (Milburg Francisco) Mansfield 

They average, taken together, eighty-three kilometres to the kilometre carré. The Automobilist Abroad |M. F. (Milburg Francisco) Mansfield