Forsberg conveys the story’s sadness with the sorrowful ballad “Heart of Stone.” ‘Six’ an empowering musical remix of English history |Patrick Folliard |August 4, 2022 |Washington Blade 

Sanford subsequently engaged in sorrowful interviews with the Associated Press and other outlets, in an effort to be forgiven by South Carolinians. America’s strange obsession with marital affairs |Isaac Amend |January 29, 2022 |Washington Blade 

Still, she said there have been many nights where she has lain awake, thinking of Floyd, sorrowful that she did not do more to save his life. My violent assault, the bystanders who did nothing, and what it means to bear witness |Kim Le |April 2, 2021 |Vox 

It is also one of the few places in Georgia where the sorrowful beauty of the call to prayer still can be heard. The Secret Life of an ISIS Warlord |Will Cathcart, Vazha Tavberidze, Nino Burchuladze |October 27, 2014 |DAILY BEAST 

He was consumed by a sorrowful bitterness and cynicism that could only be responded to with large purses. Bloody Brilliant |Stanley Crouch |April 24, 2009 |DAILY BEAST 

She felt very sorrowful as she smoothed the homely garments, and placed them in a small leathern trunk. The World Before Them |Susanna Moodie 

Ney and Marmont did not accompany the other Commissioners with their sorrowful terms; like rats they left the sinking ship. Napoleon's Marshals |R. P. Dunn-Pattison 

For the affliction of the daughter of my people I am afflicted, and made sorrowful, astonishment hath taken hold on me. The Bible, Douay-Rheims Version |Various 

The post-boy returned, sorrowful, to Petersfield, where he procured another horse and rode back to Liphook. The Portsmouth Road and Its Tributaries |Charles G. Harper 

For she must always find something to be glad of before she could be sorrowful about any thing. Tessa Wadsworth's Discipline |Jennie M. Drinkwater