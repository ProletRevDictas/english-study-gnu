Despite his siblings’ blank looks and incredulous scoffs, he continues to spew his fantasy for the future—an unholy blend of hollow corporate jargon masquerading as self-actualization and shallow appeals to a sibling bond that does not exist. ‘Succession’ Season 3, Episode 2 Recap: Kendall Goes Full Misogynist—and Completely Shits the Bed |Laura Bradley |October 25, 2021 |The Daily Beast 

Men’s Olympic sprints traffic heavily in machismo and self-assurance, so an incredulous victor is a rarity on the track. The Fastest Men In The World Are Still Chasing Usain Bolt |Josh Planos |August 3, 2021 |FiveThirtyEight 

Sounding incredulous, she told her TikTok followers that she was “blown away at the constitutional rights they’re signing away.” Inside One Combat Vet's Journey From Defending His Country to Storming the Capitol |W.J. Hennigan/Washington |July 9, 2021 |Time 

A live reporter heard every play call, every incredulous objection to a ref, every exuberant spurt of trash talk. Action, boredom, protest and celebration inside the NBA bubble |Aram Goudsouzian |June 4, 2021 |Washington Post 

The Velvet Underground regaled an incredulous New Jersey high school with their classic song “Heroin.” Peacock's All-Girl Muslim Punk Band Comedy We Are Lady Parts Is a Rockin' Good Time |Judy Berman |June 2, 2021 |Time 

I tried this twice and both drivers gave me an incredulous look before driving off. The Life and Hard Times Of The Family A Cuban Defector Left Behind |Brin-Jonathan Butler |December 19, 2014 |DAILY BEAST 

At the time, critics were incredulous that the show would work. People Prefer ‘The Bachelor’ to ‘The Bachelorette.’ Why? It’s Science. |Brandy Zadrozny |July 1, 2014 |DAILY BEAST 

Discussing the popular television program All in the Family, Nixon was incredulous at a positive portrayal of gay characters. Gay Men and the Presidents Who Loved Them |James Kirchick |June 24, 2014 |DAILY BEAST 

On Wednesday, Republican Senator John McCain was incredulous about this decision. U.S. Waited Months to Send Ukraine Spare Tires |Eli Lake |May 16, 2014 |DAILY BEAST 

For the judge who is incredulous, Martin again offers two simple words: “Excuuuuuse me!” Change the Constitution in Six Easy Steps? It Won’t Be That Simple, Justice Stevens |Richard L. Hasen |April 20, 2014 |DAILY BEAST 

As they walked along, he listened with trembling, half-incredulous hope to Jos's interpretation of Aunt Ri's voluble narrative. Ramona |Helen Hunt Jackson 

Of the astonishment of her parents and Dorothy's wild, almost incredulous delight, there is no need to tell. Dorothy at Skyrie |Evelyn Raymond 

On hearing this most unexpected sound, Mr. Bumble looked first incredulous, and afterwards, amazed. Oliver Twist, Vol. II (of 3) |Charles Dickens 

We told him we should probably want it at 7:30, and he looked at us in an incredulous manner. British Highways And Byways From A Motor Car |Thomas D. Murphy 

I had expected to find quantities of charred black paper, with possibly some fragments of binding, and was quite incredulous. The Sinn Fein rebellion As I Saw It. |Mrs. Hamilton Norway