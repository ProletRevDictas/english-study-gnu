So without further ado, here’s a smattering of dogs — and birds, and squirrels — filling up while the filling’s good. These dogs, birds, and squirrels are stuffing their faces with Brood X cicadas |Benji Jones |May 21, 2021 |Vox 

Without further ado, here’s what Housewives beginners need to know about each of the different Real Housewives series, presented chronologically first for seasons still on the air, and then for those that left us too soon. The Ultimate Beginner's Guide to Watching The Real Housewives |Cady Lang |April 9, 2021 |Time 

Without further ado, here’s how to get the most out of your broiler. Don’t Fear the Broiler |Elazar Sontag |February 11, 2021 |Eater 

So without further ado, here are the 20 most-commented-upon stories from Ars in 2020, alongside the highest-rated and most-insightful comments, as voted on by our readers. 2020’s 20 most-commented stories |Eric Bangeman |December 25, 2020 |Ars Technica 

Without further ado, here is the result for the United States overall. Can Google searches predict where coronavirus cases will soon emerge? |Philip Bump |October 29, 2020 |Washington Post 

Without further ado, here are the craziest—and most sensible—theories of how True Detective will come to a close. Craziest Theories of How ‘True Detective’ Will End: Killer Marty, the Five Horsemen, and More |Marlow Stern |March 5, 2014 |DAILY BEAST 

So, without further ado, here are the craziest moments in The Wolf of Wall Street. The 21 Craziest Moments in ‘The Wolf of Wall Street’: Cocaine-Fueled Orgies and More |Marlow Stern |December 17, 2013 |DAILY BEAST 

Without further ado, the most relatable lines from the Girls season three trailer. ‘Girls’ Season 3 Trailer Debuts. Is It the Most Relatable Yet? |Kevin Fallon |November 22, 2013 |DAILY BEAST 

So without further ado, here are eight of the best long takes ever made. ‘Gravity’ and Film’s Eight Best Long Takes (VIDEO) |Alec Kubas-Meyer |October 4, 2013 |DAILY BEAST 

Pictured above is the sculpture titled Hopeful Had Much Ado from Pilgrim's Progress. The Future of Print! |William O’Connor |June 4, 2013 |DAILY BEAST 

Then he clapped his fiddle under his chin and without more ado struck up "Bobbing Joan." The Pit Town Coronet, Volume I (of 3) |Charles James Wills 

And, without more ado, he caught up a chair and held it before him in readiness to receive the other's onslaught. St. Martin's Summer |Rafael Sabatini 

Mademoiselle Marthe eats elegantly, without any ado or any noise, just like a grown-up lady. Child Life In Town And Country |Anatole France 

If he is not so compassionate, he will lay his finger on the wound without more ado. The Life &amp; Letters of Peter Ilich Tchaikovsky |Modeste Tchaikovsky 

Without more ado they secured a broken shovel and two case-knives and began operations. Famous Adventures And Prison Escapes of the Civil War |Various