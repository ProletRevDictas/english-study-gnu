Late in the regular season, against three lousy defenses, he padded stats and started connecting on deep balls. What to know from NFL playoffs first round: Lamar Jackson wins and Tom Brady finds a way |Adam Kilgore |January 11, 2021 |Washington Post 

YouTube is lousy with Christmas and winter soundscapes, which I turn to every year, because California Christmas will always be just a touch disappointing to a Midwestern girl raised on drifting snow. One Good Thing: The gentle, lovely beauty of the YouTube Christmas coffee shop |Emily VanDerWerff |December 25, 2020 |Vox 

For all these reasons, it would be misguided to treat lousy lying as a “cognitive failure”, as it clearly serves several social functions. ‘I Won The Election’ – How Powerful People Use Lousy Lies To Twist Reality |LGBTQ-Editor |November 25, 2020 |No Straight News 

Challenger banks—some call them digital banks or neobanks—took off several years ago by exploiting customer frustration with the abusive fees and lousy mobile experience offered by incumbents. Digital banks: Feature, product or business? |Jeff |October 21, 2020 |Fortune 

One obvious factor shaking their faith is that growth has been lousy for decades. Capitalism is in crisis. To save it, we need to rethink economic growth. |Katie McLean |October 14, 2020 |MIT Technology Review 

Their intentions may be good, but their execution and insight are lousy. Grief: The Real Monster in The Babadook |Tim Teeman |December 19, 2014 |DAILY BEAST 

Thailand has had a lousy track record with functioning democracy for many decades. ‘The Hunger Games’ Stars Silent on Thai Protesters |Asawin Suebsaeng |November 21, 2014 |DAILY BEAST 

Peter Shumlin has been doing a lousy job and voters turned against him, even though he outspent his opponent five to one. What the Hell Happened in Vermont?! |Stuart Stevens |November 13, 2014 |DAILY BEAST 

Not surprisingly, some around Barry Goldwater thought it was a lousy idea. Remembering Reagan’s Defining Speech |Stuart Stevens |October 27, 2014 |DAILY BEAST 

The Getaway Car is lousy with these throwaway lines and asides. Donald E. Westlake, The Man With The Getaway Face |Malcolm Jones |October 25, 2014 |DAILY BEAST 

Frey's a private dick—a lousy one, reckless and careless, but still he's a dick and your story didn't go. Hooded Detective, Volume III No. 2, January, 1942 |Various 

Got more loyalty to a lousy machine than to the whole human race. The Stutterer |R.R. Merliss 

Come one, come all—all you moth-eaten, lousy stiffs from Stiffville. American Sketches |Charles Whibley 

Several kinds of beetles are subject to lice, but particularly that kind called by way of eminence the lousy beetle. The Book of Curiosities |I. Platts 

And such a lousy, brawling lot of convicts I had never clapped eyes upon. Richard Carvel, Complete |Winston Churchill