Fortune recently spoke with Tedder to learn more about his new business venture, the lessons learned, the hurdles overcome, and plans for the next year. OneRepublic’s Ryan Tedder on launching a hemp-infused sparkling water brand |Rachel King |August 24, 2020 |Fortune 

Public-health officials will also have to overcome reluctance by some Americans to get a shot. U.S. will conduct an unofficial dry run of a COVID-19 vaccine campaign this fall |Claire Zillman, reporter |August 19, 2020 |Fortune 

Joe knows what it’s like to struggle, which is why he gives his personal phone number to kids overcoming a stutter of their own. ‘He is clearly in over his head’: Read Michelle Obama’s full speech denouncing Donald Trump |kdunn6 |August 18, 2020 |Fortune 

Now, scientists have pinpointed a compound emitted by congregating locusts that might explain how individuals of one widespread species overcome their innate aversion to socializing. A single molecule may entice normally solitary locusts to form massive swarms |Jonathan Lambert |August 12, 2020 |Science News 

In addition to that tension, the Jazz will also have to overcome the absence of Bojan Bogdanović, who is out for the restart after having surgery on his wrist. Who’s Who In The NBA Bubble: The Potential Party-Crashers |Jared Dubin |July 23, 2020 |FiveThirtyEight 

Who knew that “we shall overcome” meant “we, the few, shall book covers every decade or so, maybe, sometimes, if we are in style.” One Vogue Cover Doesn’t Solve Fashion’s Big Race Problem |Danielle Belton |January 2, 2015 |DAILY BEAST 

She fails to appreciate the congressional and constitutional obstacles Johnson had to overcome to win passage of the bill. Dr. King Goes to Hollywood: The Flawed History of ‘Selma’ |Gary May |January 2, 2015 |DAILY BEAST 

She was separated from her colleagues after they were overcome by smoke and heat and ordered to withdraw. The Mystery Death Of A Female Firefighter |Christopher Moraff |December 13, 2014 |DAILY BEAST 

To overcome these impediments, at least two tour operators bring visitors into the region. Egypt Ain’t The Only Pyramid Show In Town |Nina Strochlic |December 11, 2014 |DAILY BEAST 

What kind of advice would you give to young women to overcome that glass ceiling? In All-Girls Schools, a Formula for Success |Bobby Blanchard |October 21, 2014 |DAILY BEAST 

His departure in autumn had been so gradual, that it was difficult to say when night began to overcome the day. The Giant of the North |R.M. Ballantyne 

In brief, by the close of the year, the phenomenal conditions growing directly out of the European war had been met and overcome. Readings in Money and Banking |Chester Arthur Phillips 

De Castellor was now in his seat; and when Castanos came off; the Duke was stunned into stupor, overcome by the illimitable ruin. The Pastor's Fire-side Vol. 3 of 4 |Jane Porter 

A very brief exercise of Mr. Sikess art sufficed to overcome the fastening of the lattice, and it soon stood wide open also. Oliver Twist, Vol. II (of 3) |Charles Dickens 

He gazed at us all in wonderment, and, overcome by mingled shame and exhaustion, I sank into a chair and popped no more. Punch, or the London Charivari, Volume 158, April 28, 1920 |Various