On average, we use about 100 calories per mile when running, depending upon run pace and body mass. ‘Oh, we’re still in this.’ The pandemic wall is here. |Maura Judkis |February 9, 2021 |Washington Post 

This sweet treat represents life-saving calories at one of the roughest times of the year for survival, but it’s also great for everyday culinary uses. Make your own maple syrup without harming the trees |By Tim MacWelch/Outdoor Life |February 7, 2021 |Popular-Science 

So people need to get some of their calories from fat and carbohydrates. Harsh Ice Age winters may have helped turn wolves into dogs |Bruce Bower |February 3, 2021 |Science News For Students 

That means that, per calorie, the plant-based nuggets actually have less protein and more saturated fat than regular chicken tenders. Is Fast Food Healthier When It's Plant-Based? |Christine Byrne |February 3, 2021 |Outside Online 

Rachel is a 24-year-old obsessed with counting her calories. Here Are the 14 New Books You Should Read in February |Annabel Gutterman |February 2, 2021 |Time 

Related: Low-Calorie Foods That Will Actually Fill You Up 3. 12 Thanksgiving Weight Loss Tips That Actually Work |DailyBurn |November 27, 2014 |DAILY BEAST 

So turning to calorie-free, “diet-friendly” alternatives makes sense, right? Are Artificial Sweeteners Wrecking Your Diet? |DailyBurn |September 30, 2014 |DAILY BEAST 

Found in tons of foods, these zero-calorie sugar substitutes have taken the U.S. market by storm. Are Artificial Sweeteners Wrecking Your Diet? |DailyBurn |September 30, 2014 |DAILY BEAST 

And evidence continues to mount against zero-calorie additives. Are Artificial Sweeteners Wrecking Your Diet? |DailyBurn |September 30, 2014 |DAILY BEAST 

After missing that mark with the empty-calorie fluff of Salem, WGN is nailing it with Manhattan. WGN’s ‘Manhattan’ Is Summer’s Best New Show. But Will Anyone Watch? |Kevin Fallon |July 27, 2014 |DAILY BEAST 

A Calorie is the amount of heat required to raise the temperature of one kilogram of water from zero to one degree Centigrade. A Civic Biology |George William Hunter 

A calorie is the heat unit used in the estimation of the fuel value of various foods. The Mother and Her Child |William S. Sadler 

The calorie nevertheless is not a food substance, it is the unit by which energy-giving heat is measured. The Library of Work and Play: Housekeeping |Elizabeth Hale Gilman 

A Calorie is the amount of heat required to raise one kilogram of water 1° Centigrade or one pound of water 4° Fahrenheit. Better Meals for Less Money |Mary Green 

A person in bed for twenty-four hours will require about 0.5 Calorie per pound per hour to prevent use of body material for fuel. Foods and Household Management |Helen Kinne