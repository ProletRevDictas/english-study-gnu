In a video, Carolina Mayorga superimposes her pink-tinted performance over Armando López Bircann’s setting. In the galleries: Artists pair up to create a third identity, or when 1+1 equals one. |Mark Jenkins |November 19, 2021 |Washington Post 

The reflection is later superimposed on the video the spider viewed, revealing exactly what the spider’s principal eyes were focused on. Jumping spiders’ remarkable senses capture a world beyond our perception |Betsy Mason |October 25, 2021 |Science News 

Online platforms have their own visual style — a filter that superimposes dog ears onto a person’s face wouldn’t be out of place — that doesn’t always translate to film to television. Influencers are failing to break out in TV and movies. Can Charli D’Amelio and Addison Rae beat the curse? |Travis Andrews |September 2, 2021 |Washington Post 

It is these resources which can be superimposed on the civilian hospitals to take on the overload. Indian Generals Blast Government for Withholding Life-Saving Army Aid From Its Own People |Aakash Hassan |May 14, 2021 |The Daily Beast 

Rather than restoring networks or adding new connections, Lifestyle usually superimposed new networks onto the old neighborhood. How Does New York City Keep Reinventing Itself? (Bonus) |Kurt Andersen |March 21, 2021 |Freakonomics 

Structure in a novel is something you discover, not something you superimpose. Rick Moody: Why I Write |Rick Moody |February 1, 2013 |DAILY BEAST 

Just snap a picture with your iPhone, and this app will superimpose horns, tail, and Van Dyke on the beloved caucusing turncoat! Why Congress Needs an iPhone |Chris Regan |November 7, 2008 |DAILY BEAST 

If we superimpose or combine these two squares, we get the arrangement of Diagram 3, which is one solution. Amusements in Mathematics |Henry Ernest Dudeney 

For the latter it is only necessary to cut the long rectangle in half and superimpose the two halves. Montessori Elementary Materials |Maria Montessori 

The next step is to superimpose the little cards on the first chart of the tens series, having the resultant numbers read aloud. Montessori Elementary Materials |Maria Montessori 

What new impressions would superimpose themselves upon the memories of the past—the memory of Hellayne? Under the Witches' Moon |Nathan Gallizier 

Plotinus very properly said that the proper thing to do was to superimpose the idea upon the actual. West African studies |Mary Henrietta Kingsley