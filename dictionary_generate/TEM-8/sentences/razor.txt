Precision trimmers will have additional blades that can be added to the razor head. The best beard trimmer: Shape your facial hair with ease |Carsen Joenk |January 19, 2021 |Popular-Science 

This razor gives the closest, smoothest shave I’ve ever experienced, and I love that the automatic shipments give me one less thing to worry about, especially during a year when my brain is maxed out. The 12 best beauty and wellness products under $70 I added to my routine this year |Rachel Schallom |December 25, 2020 |Fortune 

Waterproof electric razors are particularly great if you’re not ready to give up wet shaves. Best electric shaver: Get a smooth shave with our picks |Jeremy Helligar |December 18, 2020 |Popular-Science 

Around every corner was a well for a child to fall into or an apple with a razor blade in it. The McDonald’s Commercials That Live in Our Minds, Rent Free |MM Carrigan |December 18, 2020 |Eater 

It’s not the first time Young has had to send out updates encouraging customers to be patient — it’s taken longer than usual this year for the company to get shipments of its razors, which are manufactured in Germany. DTC brands are preparing for nightmare holiday shipping delays and out of stocks |Anna Hensel |December 4, 2020 |Digiday 

Their jagged edges and razor sharp teeth make you stand a little further back then normal. Takashi Murakami’s Art From Disaster |Justin Jones |November 28, 2014 |DAILY BEAST 

Many of the so-called “razor-close” races in key states were never truly competitive. How the Lame Democrats Blew It |Goldie Taylor |November 5, 2014 |DAILY BEAST 

But as soon as she pressed the razor blade against her neck, the guard lowered the gun. How ‘Titanic ’Helped This Brave Young Woman Escape North Korea’s Totalitarian State |Lizzie Crocker |October 31, 2014 |DAILY BEAST 

Factors like these are especially hard on restaurants, which tend to operate on razor-thin profit margins. High Rents Are Killing the Restaurant Capital |Will Doig |October 28, 2014 |DAILY BEAST 

We stood on the rooftop of a derelict farmhouse meters away from a Turkish tank and a razor wire fence marking the end of Turkey. Impotent U.S. Airstrikes, Passive Turks and an ISIS Triumph |Jamie Dettmer |October 3, 2014 |DAILY BEAST 

The cigar store had a counter display of a bargain buy of razor blades combined with some unknown brand of shaving cream. Hooded Detective, Volume III No. 2, January, 1942 |Various 

Taking the precaution to remove his razor, Lillyston locked the door upon him, and determined at once to get medical advice. Julian Home |Dean Frederic W. Farrar 

There are attacks that may be cured by the razor-strop or a bucket of cold water, but these are exceptional. Essays In Pastoral Medicine |Austin Malley 

The little fellow watched him closely during the process, and noticed where the man put his razor and brush. Minnie's Pet Monkey |Madeline Leslie 

"Be sure and don't cut my head off," murmured the orphan, as he watched the razor flashing to and fro along the strop. Jack Harkaway's Boy Tinker Among The Turks |Bracebridge Hemyng