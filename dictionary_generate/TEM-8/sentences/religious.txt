If you’re a religious leader, you can give a sermon about climate and run a collection drive to support one of the groups above. Want to fight climate change effectively? Here’s where to donate your money. |Sigal Samuel |September 17, 2020 |Vox 

Right now, voters with no religious affiliation look like they might back Biden in record numbers. More And More Americans Aren’t Religious. Why Are Democrats Ignoring These Voters? |Daniel Cox |September 17, 2020 |FiveThirtyEight 

During that meeting, McCoy called for another meeting with religious leaders, and agreed to talk more about the particular needs in their communities. Oceanside Is Rethinking Its Police Chief Hiring Process Following Community Concerns |Kayla Jimenez |September 14, 2020 |Voice of San Diego 

There are few things in India that enjoy religious and cultural sanctions as much as the use of cannabis does. How did marijuana become illegal in India? |Manavi Kapur |September 11, 2020 |Quartz 

Now the fringe religious group Israelites of the New Universal Pact, mostly unknown outside Peru, holds 15 seats in a fractured national legislature after 2019’s elections, making it the country’s third largest party. American Fringes: How the Media Makes it Worse |Nick Fouriezos |September 7, 2020 |Ozy 

In 2009, a Pakistani Christian woman got into a religious argument with some Muslim women with whom she was harvesting berries. In Defense of Blasphemy |Michael Tomasky |January 9, 2015 |DAILY BEAST 

The gentleman was listed as Orthodox and kosher, which is way too religious for my friend whose JSwipe account I was test-driving. My Week on Jewish Tinder |Emily Shire |January 5, 2015 |DAILY BEAST 

It is the kind of compassion espoused by every world religion and every revered religious leader. In 2015, Let’s Try for More Compassion |Gene Robinson |January 4, 2015 |DAILY BEAST 

But the last national figure to wield ancient personal authority in an explicitly religious way was Robert F. Kennedy. No Gods, No Cops, No Masters |James Poulos |January 1, 2015 |DAILY BEAST 

In my search for answers about who I was, I pored over religious texts in search of enlightenment. Dear Leelah, We Will Fight On For You: A Letter to a Dead Trans Teen |Parker Molloy |January 1, 2015 |DAILY BEAST 

On the thirteenth of the same month they bound to the stake, in order to burn alive, a man who had two religious in his house. The Philippine Islands, 1493-1898, Volume XX, 1621-1624 |Various 

We shall recover again some or all of the steadfastness and dignity of the old religious life. The Salvaging Of Civilisation |H. G. (Herbert George) Wells 

But you will find most colleges and most college societies bar religious instruction and discussion. The Salvaging Of Civilisation |H. G. (Herbert George) Wells 

Hence arise factions, dissensions, and loss to their religious interests and work; and these intruders seek to rule the others. The Philippine Islands, 1493-1898, Volume XX, 1621-1624 |Various 

So much for the attitude of the various schools of religious thought towards the Bible. God and my Neighbour |Robert Blatchford