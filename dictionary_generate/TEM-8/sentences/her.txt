Vladimir Putin endorses plan to tweet about Barneys models from bathroom of his/her choice. Up to a Point: PJ O’Rourke on Sochi and Senate Slackers |P. J. O’Rourke |February 7, 2014 |DAILY BEAST 

The social media was quick to pick up the word and redefine it as “someone who fights for his/her individual rights.” Smiling Under a Cloud of Tear Gas: Elif Shafak on Istanbul’s Streets |Elif Shafak |June 11, 2013 |DAILY BEAST 

Or of dinners where the wealthiest friend picks up the check while everyone else looks at his/her sneakers. Income Inequality Within Families is Emerging as a Major Issue |Janna Malamud Smith |January 19, 2013 |DAILY BEAST 

Winner agrees to the use of his/her name and likeness in publicity without any additional compensation (except where prohibited). The Orbitz Business Travel Survey Sweepstakes | |December 14, 2012 |DAILY BEAST 

Every president and governor needs time for his/her policies to take effect. 30 Is the New 47, So Say the Mittsters |Michael Tomasky |June 4, 2012 |DAILY BEAST 

Ay, murmured the sick woman, relapsing into her former drowsy state, what about her?what aboutI know! Oliver Twist, Vol. II (of 3) |Charles Dickens 

Wants to wear the boots just once, she says, to lay the ghost of this what's-her-name—Maria Modena. The Medici Boots |Pearl Norton Swet 

Her virtues are merely milk-and-morality-her intelligence is pure spiritual whey. The Fiend's Delight |Dod Grile 

After the fall of the Ramessidian kings, the priestly Dynasty of Her-hor does not appear to have made use of them very largely. Scarabs |Isaac Myer 

The morning is more modest than thy praises, What a thing does he make her?Arc. Beaumont &amp; Fletcher's Works (8 of 10) |Francis Beaumont