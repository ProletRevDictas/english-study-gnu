This genius to superintend and be the head, while others contribute the hands, is not the most common of human endowments. Madame Roland, Makers of History |John S. C. Abbott 

Madame Roland frequently retired to the library, to write letters to her friends, or to superintend the lessons of Eudora. Madame Roland, Makers of History |John S. C. Abbott 

He had sent his most trusted foreman to his own beautiful home, to superintend matters there. The Box-Car Children |Gertrude Chandler Warner 

The invalid sat on the shank of a mushroom anchor, and smoked his pipe while he affected to superintend the work. The Floating Light of the Goodwin Sands |R.M. Ballantyne 

They have been doing all of the hanging on one side, and I wish to superintend it on the other. Portrait and Biography of Parson Brownlow, The Tennessee Patriot |William Gannaway Brownlow