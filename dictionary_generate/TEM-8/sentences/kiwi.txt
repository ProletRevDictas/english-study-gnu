You’ll see strawberry kiwi, you see strawberry goji berry, strawberry acai berry, because, “Well, I like strawberry, so I’m willing to try whatever the other new thing is, as long as it’s still with strawberries.” The Downside of Disgust (Ep. 448) |Stephen J. Dubner |January 21, 2021 |Freakonomics 

On Monday, the Kiwi singer announced a new collaboration with MAC Cosmetics. Lorde Announces MAC Collaboration; ‘Vamp’ Magazine Puts Women on Top |The Fashion Beast Team |March 11, 2014 |DAILY BEAST 

When Dmitri visited, he would act as a megaphone, relaying to his mother what her ears could not catch of my Kiwi accent. Remembering Dmitri Nabokov, the Novelist’s Son and Literary Executor |Brian Boyd |May 10, 2012 |DAILY BEAST 

First Kiwi, the 17-year-old son, departs to land a job working in the World of Darkness theme park. Great Weekend Reads |The Daily Beast |February 12, 2011 |DAILY BEAST 

The radical element here is kiwi-, a verb stem indicating the general notion of “indefinite movement round about, here and there.” Language |Edward Sapir 

I can't think it's on account of their looks; for there's the kiwi, the hornbill, and sakes alive—the puffins! The Adventures of a Grain of Dust |Hallam Hawksworth 

Another of the long-nosed earth workers, as curious in his make-up as the flamingoes, is the kiwi of New Zealand. The Adventures of a Grain of Dust |Hallam Hawksworth 

Katipo killed a kiwi in the course of our morning's hunt, and this bird is now being skinned, cut up, and roasted on sticks. Brighter Britain! (Volume 1 of 2) |William Delisle Hay 

Allied to these are the four species of Kiwi or apteryx, still existing there. More Science From an Easy Chair |Sir E. Ray (Edwin Ray) Lankester