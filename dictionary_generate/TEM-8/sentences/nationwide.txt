We’re very used to helping people navigate difficult times, but usually those difficult times are one-to-one, not nationwide or worldwide. How Hallmark is handling pandemic Valentine’s Day |Luke Winkie |February 12, 2021 |Vox 

The most important thing we can do for the kids, besides push for every possible way to speed up the nationwide vaccination effort, is to keep in mind that whatever they end up doing in these crazy times, they are not losing a year of learning. The mid-pandemic return to school is totally weird for kids. And possibly lonely, too. |Petula Dvorak |February 11, 2021 |Washington Post 

The company’s CEO, Garry Ridge, says that the nationwide popularity of “isolation renovation” projects are driving the increase. 20 ingenious uses for WD-40 |By Bill Heavey/Field & Stream |February 11, 2021 |Popular-Science 

The New York City bill in particular could serve as a model for cities and states nationwide. Auditors are testing hiring algorithms for bias, but there’s no easy fix |Amy Nordrum |February 11, 2021 |MIT Technology Review 

Jurisdictions nationwide are struggling with similar issues. Confusion and chaos: Inside the vaccine rollout in D.C., Maryland and Virginia |Julie Zauzmer, Gregory S. Schneider, Erin Cox |February 9, 2021 |Washington Post 

Egypt has a comparatively low number of HIV cases compared to the rest of Africa, with just 11,000 infected people nationwide. Sisi Is Persecuting, Prosecuting, and Publicly Shaming Egypt’s Gays |Bel Trew |December 30, 2014 |DAILY BEAST 

This would outfit less than one tenth of police officers nationwide but gets the idea circulating. Dear GOP: Fix the Damn Justice System! |Jonathan Alter |December 7, 2014 |DAILY BEAST 

Trailing only the economy, education and classroom issues dominated thinking among voters nationwide. Why Voters Love Common Core |Harold Ford Jr. |November 28, 2014 |DAILY BEAST 

White working class distrust of the Democratic Party has gone nationwide. With Immigration Move, Obama and the Welfare Party Strike Again |Lloyd Green |November 24, 2014 |DAILY BEAST 

Now there are only around 5-6,000 members nationwide, according to the SPLC. The Klan’s Call to Violence in Ferguson Blows the Lid Off Its Hypocritical Rebrand |Caitlin Dickson |November 14, 2014 |DAILY BEAST 

General strikes, sympathetic strikes, nationwide boycotts and nation-wide political movements became the order of the day. A History of Trade Unionism in the United States |Selig Perlman 

A bigger game it turned out than any of the players knew, bigger in its immediate sweep and in its nationwide issues. Corporal Cameron |Ralph Connor 

Were you writing boys' stories for a nationwide magazine of high circulation and accredited quality? The Fourth R |George Oliver Smith 

Do you know how many cases within the 7,337 noted here, which I understand is nationwide, were from Texas? Warren Commission (4 of 26): Hearings Vol. IV (of 15) |The President's Commission on the Assassination of President Kennedy 

Then towards the bottom of that page you have given gross figures during the same 2-year period of the nationwide activity. Warren Commission (4 of 26): Hearings Vol. IV (of 15) |The President's Commission on the Assassination of President Kennedy