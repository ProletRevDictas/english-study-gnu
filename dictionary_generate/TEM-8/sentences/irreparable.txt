The choice isn’t so coercive as to constitute irreparable constitutional harm. Can Your Employer Require That You Get Vaccinated? It Depends Where You Live |I. Glenn Cohen |August 2, 2021 |Time 

Because the merits are so uncertain, Wisk has also not adequately shown irreparable injury based on misappropriation. Judge denies Wisk Aero’s request for preliminary injunction against Archer Aviation |Aria Alamalhodaei |July 23, 2021 |TechCrunch 

Last month, an Indiana state court judge, citing possible “irreparable harm” to the unemployed, ruled Indiana must continue participation in the federal unemployment benefits program. The U.S. Spends Less Than Nearly Every Country on Unemployment. That's Why People Can't Get Jobs. |Sarah Damaske |July 7, 2021 |Time 

Delaying the predevelopment work would do irreparable harm to the state. Vote on Maryland toll lanes will move forward Tuesday |Justin George |June 4, 2021 |Washington Post 

Doing so would be a violation of my oath, do irreparable harm to our great democracy and set a dangerous precedent for future elections. Pair of Georgia runoff races are razor close with U.S. Senate control at stake |Felicia Sonmez, Colby Itkowitz, John Wagner, Paulina Firozi, Amy B Wang |January 6, 2021 |Washington Post 

The building had to be rebuilt in 1963 after extensive damage from the Second World War was finally deemed irreparable. Inside The World’s 10 Oldest Restaurants |Justin Jones |December 20, 2014 |DAILY BEAST 

Amelia must do this every day in loving Samuel—who often appears as a reminder of the irreparable past and death of Oskar. ‘The Babadook’ Is the Best (and Most Sincere) Horror Movie of the Year |Samuel Fragoso |November 30, 2014 |DAILY BEAST 

One thing is for certain, the events of the past few weeks have done irreparable damage to the image of Nigeria. Boko Loco: A View From Nigeria |Lola Ogunnaike |May 9, 2014 |DAILY BEAST 

It reached the point where it was irreparable, and the best thing to do for the entire family was to separate. Mel Brooks Is Always Funny and Often Wise in This 1975 Playboy Interview |Alex Belth |February 16, 2014 |DAILY BEAST 

Moreover, some children's hearing losses are discovered too late; the language delay has already done irreparable damage. This Is What It Is Like To Be Deaf From Birth |Quora Contributor |December 23, 2013 |DAILY BEAST 

Indeed the mere absence of such warnings for one stormy night would certainly result in loss irreparable to life and property. The Floating Light of the Goodwin Sands |R.M. Ballantyne 

Captain Frank Johnson, died in Philadelphia in 1844, universally respected, and regretted as an irreparable loss to society. The Condition, Elevation, Emigration, and Destiny of the Colored People of the United States |Martin R. Delany 

The moment passed by for ever; Eric had listened without objection to foul words, and the irreparable harm was done. Eric, or Little by Little |Frederic W. Farrar 

If he fell, the monument would find itself bereft of all its elegance, split as by some long and irreparable crack. The Nabob |Alphonse Daudet 

I tried to maintain to myself that this hidden love made no difference to the now irreparable breach between husband and wife. The New Machiavelli |Herbert George Wells