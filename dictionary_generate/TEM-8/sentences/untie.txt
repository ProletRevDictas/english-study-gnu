Until members of Jerusalem’s fractious faiths—including Christians—accept and respect one another’s existing prayer spaces, there is little chance for untying the world’s ultimate geopolitical knot. Christian Archaeologists Wanted to Excavate the Biblical Past. They Ended Up Sparking Today's Strife in Jerusalem |Andrew Lawler |November 3, 2021 |Time 

Together, we must be untied by the goal to prepare our young people to wrestle with the complexities of civic life in ways that are thoughtful, informed by evidence, grounded in democratic values and respectful of different points of view. What civics education should really look like |Valerie Strauss |May 14, 2021 |Washington Post 

A regular eight-turn clinch knot would not hold at all, slipping out and coming untied at about 10 pounds of force. The strongest fishing knots you can tie |By John Merwin/Field & Stream |March 2, 2021 |Popular-Science 

In anticipation of the deal closing, CEO Frank Gibeau told me that this represents Zynga’s first move into the world of hyper-casual games — games where, as their titles suggest, players perform simple tasks like throwing knives and untying knots. Zynga completes its acquisition of hyper-casual game maker Rollic |Anthony Ha |October 2, 2020 |TechCrunch 

“I bent over to untie my shoelaces, and I felt an agent pouring cold water on me,” said Adonys, who has filed an asylum request. Immigrants Held in Border Deep Freezers |Rachael Bale, The Center for Investigative Reporting |November 19, 2013 |DAILY BEAST 

Then he proceeded to untie the stops in the mainsail, and was thus engaged when a voice hailed him from the shore. The Rival Campers |Ruel Perley Smith 

I got up and tried to untie her, but I was so excited my hands shook so I couldn't hardly do anything with them. Adventures of Huckleberry Finn, Complete |Mark Twain (Samuel Clemens) 

Pulasi ang sanggutan arun mamúnga, Untie the coconut bud so it will bear fruit. A Dictionary of Cebuano Visayan |John U. Wolff 

So far, therefore, as relates to simple existence, the Inductive Logic has no knots to untie. A System of Logic: Ratiocinative and Inductive |John Stuart Mill 

Would you jest as soon, ma'am, if it ain't troubling you too much, jest nat'rally sort of untie Billy? Blazed Trail Stories |Stewart Edward White