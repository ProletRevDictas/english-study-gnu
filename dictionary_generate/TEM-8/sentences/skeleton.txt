Once the system had these data, it could track that person’s skeleton. A robot referee can really keep its ‘eye’ on the ball |Kathryn Hulick |August 20, 2020 |Science News For Students 

When his team examined the fossilized embryos, they noticed egg-shaped halos around the skeletons. Early dinosaurs may have laid soft-shelled eggs |Jack J. Lee |August 3, 2020 |Science News For Students 

It’s all based on coral — small marine animals that build themselves stony skeletons, often on top of older corals. Let’s learn about coral reefs |Bethany Brookshire |July 22, 2020 |Science News For Students 

That light — including its blue wavelengths — can enter and bounce around inside the corals’ skeleton. Going bright may help corals recover from bleaching |Carolyn Wilke |June 25, 2020 |Science News For Students 

As they crawl over the reef, the starfish liquefy polyps with digestive enzymes, sponging up the nutrients and leaving behind a coral skeleton. Fish poop exposes what eats the destructive crown-of-thorns starfish |Jake Buehler |June 8, 2020 |Science News 

That meant the talent that DJ Brinsely hired that night performed for a skeleton audience. Alleged Cop Killer’s Blood-Soaked Screenplay |M.L. Nestel |December 24, 2014 |DAILY BEAST 

The identity of the skeleton remains the big question, and answers may not be forthcoming anytime soon. Is This Alexander the Great’s Tomb—or His Wife’s? |James Romm |December 12, 2014 |DAILY BEAST 

Enriqueta Romero put a skeleton on the sidewalk, and helped give us Santa Muerte. America’s Fastest Growing Death Holiday Is From Mexico |Michael Schulson |November 1, 2014 |DAILY BEAST 

Day of the Dead parades follow a cross-cultural flow, embellishing Halloween stylizations of the dancing skeleton. New Orleans’ Carnivalesque Day of the Dead |Jason Berry |November 1, 2014 |DAILY BEAST 

Posada used the skeleton as a way of talking about politics, commenting on life. New Orleans’ Carnivalesque Day of the Dead |Jason Berry |November 1, 2014 |DAILY BEAST 

The sun was printing over the floor the shadow skeleton of the juniper-tree by the westerly window. The Soldier of the Valley |Nelson Lloyd 

The mutilated skeleton of a girl was found, which had apparently been in that place for considerably over a hundred years. The Portsmouth Road and Its Tributaries |Charles G. Harper 

A well preserved natural skeleton of this animal was brought home and deposited in the British Museum. Narrative of a Survey of the Intertropical and Western Coasts of Australia] [Volume 2 of 2] |Phillip Parker King 

The veriest tyro in natural history would see that at the first glance of the massive skeleton. Notes and Queries, Number 196, July 30, 1853 |Various 

The chestnuts are stripped bare already and lift their black skeleton arms in the air. Child Life In Town And Country |Anatole France