A 'profound revolution'The scope and velocity of the society-wide rectification has some worried China may be at the beginning of the kind of cultural and ideological upheaval that has brought the country to a standstill before. Xi Jinping’s crackdown on everything is remaking Chinese society |Lily Kuo |September 9, 2021 |Washington Post 

U.S.-China relations could certainly stand a little Confucian “rectification.” Find a China Reset Button |Orville Schell |June 7, 2013 |DAILY BEAST 

The error has been acknowledged, and its rectification is in the works. Israel To Return Farmland To Palestinian Owners |Emily L. Hauser |February 20, 2013 |DAILY BEAST 

The great Chinese philosopher Confucius put forward a timeless and important doctrine: The rectification of names. Yousef Munayyer Responds to His Critics |Yousef Munayyer |March 30, 2012 |DAILY BEAST 

And what is wanting in such is principally the rectification of their views: their endeavours are harmonious and consistent. The Ordinance of Covenanting |John Cunningham 

Hence the large expenses attending rectification, which produce fine alcohols necessarily at an elevated price. Scientific American Supplement No. 299 |Various 

With the cost of a single distillation we have, at once, distillation and rectification, or a single expense for two results. Scientific American Supplement No. 299 |Various 

The history of the drainage of the fens and the rectification of its river courses is a long and complicated one. The Rivers of Great Britain: Rivers of the East Coast |Various 

During stage L an apparent rectification of the cranial flexure commences, and is completed by stage Q. The Works of Francis Maitland Balfour, Volume 1 |Francis Maitland Balfour