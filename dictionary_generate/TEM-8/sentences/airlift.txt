When he emerged from his medically induced coma in Berlin, having been airlifted there from Omsk, he immediately accused Putin of being behind the attack. 'His Fight Is in Russia.' Why Navalny Flew Home Straight Into Putin's Clutches |Madeline Roache |January 18, 2021 |Time 

It jumped the river and raced through the timber to a reservoir known as Mammoth Pool, where a last-minute airlift saved 200 campers from fiery death. What the complex math of fire modeling tells us about the future of California’s forests |Amy Nordrum |January 18, 2021 |MIT Technology Review 

Lufthansa will airlift fresh produce to Britain on Wednesday amid fears of shortages over the festive season, even as France prepares to reopen the main truck-ferry route with its neighbor. Lufthansa will airlift food to the U.K. amid border closure and fears of food shortages due to new COVID strain |Rachel King |December 22, 2020 |Fortune 

Later that month, she was airlifted to a hospital in Jacksonville, Florida. Two School Districts Had Different Mask Policies. Only One Had a Teacher on a Ventilator. |by Annie Waldman and Heather Vogell |November 23, 2020 |ProPublica 

In one of the hijackings, an injured crew member had to be airlifted by helicopter to a hospital on land. Piracy is thriving off the coast of West Africa despite COVID nearly crushing global marine traffic |kdunn6 |November 20, 2020 |Fortune 

“Air refueling and airlift assets were the literal pinch I am describing here,” the official said. First U.S. Stealth Jet Attack on Syria Cost More Than Indian Mission to Mars |Dave Majumdar |September 24, 2014 |DAILY BEAST 

So after a year of record killings, an ambitious rescue effort is underway to airlift the great beasts to safety. South Africa’s Great Rhino Airlift |Nina Strochlic |August 17, 2014 |DAILY BEAST 

Throughout its history, Israel has helped airlift vulnerable Jewish populations in countries like Yemen and Ethiopia to safety. Europe’s Jews Punished for Israel’s War |Eli Lake |July 25, 2014 |DAILY BEAST 

That was when “instinct kicked in,” said Chief Master Sergeant James Ross, a friend and fellow airman from the 152nd Airlift Wing. Nevada Teacher Mike Landsberry Survived War. He Was Killed in Class. |Jacob Siegel |October 23, 2013 |DAILY BEAST 

"We don't have any trains or direct buses," said U.S. Marshal Pickering as he explained the airlift procedure. Madoff Secretary Annette Bongiorno Jailed Over Ponzi Millions |Allan Dodds Frank |December 21, 2010 |DAILY BEAST 

The need to respond on a moment's notice adds to the value of airlift and prepositioned ships. Shock and Awe |Harlan K. Ullman 

An airlift of supplies dropped by parachute was being organized. Prologue to an Analogue |Leigh Richmond 

How long do you think it'd take, with the equipment you have, to airlift all of Jonkvank's loyal troops into the city? Ullr Uprising |Henry Beam Piper 

It looked as helpless as isolated Berlin did before the first airlift proved what men and planes could do in the way of transport. Space Tug |Murray Leinster 

The Soviet effort to capture Berlin by blockade was thwarted by the courageous Allied airlift. State of the Union Addresses of Harry S. Truman |Harry S. Truman