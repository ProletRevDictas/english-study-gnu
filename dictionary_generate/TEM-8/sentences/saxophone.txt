There’s the sound of explosions, barking dogs and a storm of violins, saxophone and percussion. Akram Khan delivers a powerful indictment of war in ‘Xenos’ at the Kennedy Center |Sarah Kaufman |November 19, 2021 |Washington Post 

Ellis joined the horn section on alto saxophone before switching to the larger, deeper-toned tenor saxophone, his preferred instrument. Pee Wee Ellis, who helped put the funk in James Brown’s music, dies at 80 |Matt Schudel |September 26, 2021 |Washington Post 

Survivor Hal Singer became a saxophone virtuoso and a world-renowned musician. A Survivor of the Tulsa Race Massacre Says Her Family Is Still Trying to Break Its Curse, 100 Years Later |Paulina Cachero |May 29, 2021 |Time 

Starr was a music teacher and songwriter who played flute, bass, saxophone and an electronic wind synthesizer. Washington Teachers’ Union president was speeding and driving intoxicated at time of fatal crash, police say |Perry Stein, Dana Hedgpeth |May 28, 2021 |Washington Post 

Then Monk said to both of them, 'You play saxophone, right?' The Stacks: John Coltrane’s Mighty Musical Quest |Nat Hentoff |October 18, 2014 |DAILY BEAST 

There was never any one criterion for how every trombone or tenor saxophone or singer should sound. The Stacks: John Coltrane’s Mighty Musical Quest |Nat Hentoff |October 18, 2014 |DAILY BEAST 

Paul Winter played the saxophone and Melissa Leo, the actress, read texts written by Petit himself. Philippe Petit’s Moment of Concern Walking the WTC Tightrope |Anthony Haden-Guest |August 8, 2014 |DAILY BEAST 

Charles played saxophone, and Cyril, the youngest, played congas and sang like no tomorrow. The Cradle of Jazz, Blues and Gospel Endlessly Rocking |Jason Berry |April 25, 2014 |DAILY BEAST 

But the best was that they had to call and tell me to come in and redo the butt saxophone. ‘Saturday Night Live’ Star Bobby Moynihan Is the ‘Chozen’ One |Kevin Fallon |January 13, 2014 |DAILY BEAST 

But Mr. Hingman had long before this subsided into his chair and was emitting sounds like those from a saxophone. By Advice of Counsel |Arthur Train 

And we're going to get one of those things—a saxophone or whatever you call it—to take our latitude and longitude with! Pee-Wee Harris Adrift |Percy Keese Fitzhugh 

The Policeman took the saxophone, and the German the slide trombone. The Brownies and Prince Florimel |Palmer Cox 

When all poor Ernest seemed to want these days was to play the saxophone. A Mixture of Genius |Arnold Castle 

It was tougher for me than learning to play a saxophone is for a boy of ten. Stamped Caution |Raymond Zinke Gallun