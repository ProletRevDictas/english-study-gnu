The beauty of the thinly sliced ham is that you can finely chop it much more quickly than you can whole ham, so I recommend starting with that. Calling all dill lovers: This old-school ham salad sandwich is for you |Ann Maloney |January 26, 2021 |Washington Post 

The finely-tuned Netflix drama about the co-writer of Citizen Kane is about the power of movies. David Fincher’s new movie Mank is a broadside against Hollywood self-congratulation |Alissa Wilkinson |December 4, 2020 |Vox 

I like them finely chopped as an aromatic seasoning ingredient, in both salads and cooked dishes. 13 edible plants you can still find in the winter |By Tim MacWelch/Outdoor Life |December 1, 2020 |Popular-Science 

Combining finely-tuned haptic feedback moments with 3-D audio makes the PS5 feel like a truly next-generation experience. PlayStation 5 review: PS5 is a sensory game-changer |Elise Favis |November 6, 2020 |Washington Post 

By July 3, 2019, the hospital had begun dropping lawsuits it’d filed against defendants and the finely tuned debt collection machine ground to a halt. What Happens After a Debt Collection Machine Grinds to a Halt |by Wendi C. Thomas, MLK50: Justice Through Journalism |October 2, 2020 |ProPublica 

It was as finely balanced and perfectly toned as Rivers' funeral. Melissa Rivers: Life After Joan—A Funny, Moving Celebration on a Special 'Fashion Police' |Tim Teeman |September 20, 2014 |DAILY BEAST 

Most New Yorkers have developed a finely-tuned sixth sense of when subway doors should close. Leaky Ceilings, Catcalls, and Uncaged Pythons: 4 Hours on NYC’s Worst Subway |Kevin Zawacki |August 8, 2014 |DAILY BEAST 

The debate about the future, the very existence of the royal family, has always been finely balanced. Prince George’s First Year Bodes Well for the Survival of the Royals |Tom Sykes |July 22, 2014 |DAILY BEAST 

Qualms about violence versus sex aside, the book is a finely spun tale. A Fantasy Titan Invades the YA Kingdom |William O’Connor |July 18, 2014 |DAILY BEAST 

It requires a finely honed sense of timing and a griddle that has been seasoned just right. The Real Cheeseburger Paradise |Jane & Michael Stern |June 22, 2014 |DAILY BEAST 

Beneath its far-flung branches a syce was sitting in front of a finely-proportioned and unusually big Arab horse. The Red Year |Louis Tracy 

The finely granular variety is the least significant, and is found when the epithelium is only moderately affected. A Manual of Clinical Diagnosis |James Campbell Todd 

The baby's white robe, finely wrought in open-work, was also done, and freshly washed and ironed. Ramona |Helen Hunt Jackson 

The sediment contains a few hyaline and finely granular casts and an occasional red blood-cell. A Manual of Clinical Diagnosis |James Campbell Todd 

His finely-marked eyebrows went up in surprise at the irrelevance of my question, but he smiled. Uncanny Tales |Various