Needless to say, when the North Face sent me a pair of its newest trail shoes, I assumed I’d take them out for one obligatory run and then relegate them to the bottom shelf. The Gear Our Editors Loved in February |The Editors |March 4, 2021 |Outside Online 

This became especially problematic when Sanders received the obligatory Gatorade shower as the clock neared zero. Deion Sanders’s FCS debut at Jackson State is postponed because of covid concerns |Matt Bonesteel |February 26, 2021 |Washington Post 

The “CRISPR babies” episode is now an obligatory chapter in any telling of the gene-editing story. Decoding the CRISPR-baby stories |Katie McLean |February 24, 2021 |MIT Technology Review 

Doria and Bolsonaro have also publicly bickered about everything from social distancing to the use of face masks and whether vaccines should be obligatory or not throughout the tumultuous year. Brazil’s Jair Bolsonaro undercuts the Chinese COVID vaccine his country is betting on |Claire Zillman, reporter |October 22, 2020 |Fortune 

Corporate managers, like most of us, take obligatory duties seriously. 50 years later, Milton Friedman’s shareholder doctrine is dead |jakemeth |September 13, 2020 |Fortune 

There was also the obligatory shopping spree at the F.A.O. Schwarz toy store across from Central Park. Exclusive: Michael Jackson Hit With New Sex Abuse Claim |Diane Dimond |May 12, 2014 |DAILY BEAST 

Unspoken consensus made driving up and down certain streets obligatory and parking in certain places required. P.J. O’Rourke on Grabbing the Keys to Happiness |P. J. O’Rourke |January 24, 2014 |DAILY BEAST 

Yes, they showed up, wore their “Team Mitch” shirts, and joined in the obligatory anti-Obama chants. McConnell's Fancy Farm Monster Comes Back to Haunt Him |Jonathan Miller |August 4, 2013 |DAILY BEAST 

I actually quit prefacing my Ralph Nader screeds with the obligatory he-gave-us-the-seatbelt boilerplate years ago. Ralph Nader Has Truly Lost It |Michael Tomasky |July 29, 2013 |DAILY BEAST 

Lean In, by Sheryl Sandberg (Knopf) Obligatory ‘Power’ SubtitleWomen, Work, and the Will to Lead. Five Girl-Power Books Exactly Like Sheryl Sandberg’s ‘Lean In’ |Sean Macaulay |March 29, 2013 |DAILY BEAST 

It was therefore obligatory that purchasers should know his work, that in fact his sign manual should be always present. Antonio Stradivari |Horace William Petherick 

Two duties at least are, therefore, obligatory on him then;—to seek a disposition willingly to vow, and then to make the vow. The Ordinance of Covenanting |John Cunningham 

Both are obligatory at the same instant of time, and both might possibly be performed in one moment. The Ordinance of Covenanting |John Cunningham 

The principles on which the vow is made, are immutable; and while the Church is on earth, it will continue to be obligatory. The Ordinance of Covenanting |John Cunningham 

Commands enjoining Covenanting must be obligatory on men, in an individual, or in a social capacity, or in both. The Ordinance of Covenanting |John Cunningham