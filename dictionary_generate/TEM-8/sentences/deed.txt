Because it takes these murderous thieves who did terrible things — like locking women and children in a burning church — and makes them a symbol of freedom and adventure, erasing their wicked deeds from historical memory. The Buccaneers embody Tampa’s love of pirates. Is that a problem? |Jamie Goodall |February 5, 2021 |Washington Post 

Beyond the money, though, Currey is grateful that his good deed propelled someone else to do good, too. A homeless man found a wallet. After he returned it, a 12-year-old raised hundreds of dollars for him. |Sydney Page |January 6, 2021 |Washington Post 

Their explanation of why someone like Danson would obscure his good deed “is based on the intuition that making a positive signal harder to spot can serve as a signal in itself,” they write. Larry David and the Game Theory of Anonymous Donations - Facts So Romantic |Brian Gallagher |December 16, 2020 |Nautilus 

Water rights were bought and sold through private contracts and government deeds, and public agencies doled out most of the coveted commodity. California has a new market to hedge against record droughts: water futures |Michael J. Coren |December 8, 2020 |Quartz 

Miss Manners lives in hope that people will learn to care enough about their reputations to curb their offensive words and deeds. Miss Manners: Shunning, shaming and ‘cancel culture’ |Judith Martin, Nicholas Martin, Jacobina Martin |November 23, 2020 |Washington Post 

Over the course of the year, Klaus would repeatedly, through word and deed, demonstrate his sympathies with Putin. Vaclav Klaus, Libertarian Hero, Has His Wings Clipped by Cato Institute |James Kirchick |December 22, 2014 |DAILY BEAST 

Perhaps more telling, state media called the attack on the studio “a righteous deed.” Kim Jong Un’s Kid Gloves Are Now Off |Gordon G. Chang |December 17, 2014 |DAILY BEAST 

Then stab her to death and bring me back her lungs and liver as proof of your deed. In New Brothers Grimm 'Snow White', The Prince Doesn't Save Her |The Brothers Grimm |November 30, 2014 |DAILY BEAST 

But, alas, Philadelphia received the honor and President Gerald Ford did the deed. New York’s Century-Old Time Capsule Is a Dud |Justin Jones |October 8, 2014 |DAILY BEAST 

The endgame for Hamas, avowedly in both word and deed, is Jewish genocide. Numbers Don’t Tell the Mideast Story |Thane Rosenbaum |July 10, 2014 |DAILY BEAST 

Is it true that whenever we are about to do an ill or unjust deed a shadow of the fruits it will bring comes over us as a warning? Elster's Folly |Mrs. Henry Wood 

She pressed her hands tighter upon her bosom; her eyes sparkled with an odd approval of that brisk deed. St. Martin's Summer |Rafael Sabatini 

It is not likely that the inhabitants of Ivrea, who thus commemorate her heroic deed, will ever forget their Mugnaia. The Pit Town Coronet, Volume I (of 3) |Charles James Wills 

Poor wretches—they were afraid to refuse, yet their gorge rose at the deed, and they fired at the ceiling! The Red Year |Louis Tracy 

We soon found opportunity for another deed of charity not dissimilar to this, though its result was more auspicious. The Jesuit Relations and Allied Documents, Vol. II: Acadia, 1612-1614 |Various