For people to feel good about sending their kids to school each day, the buildings should be conveniently located, appealing, comfortable to spend several hours in, and of course safe. The World’s First 3D Printed School Will Be Built in Madagascar |Vanessa Bates Ramirez |February 26, 2021 |Singularity Hub 

Turban shares that kids who feel controlled or shamed are less likely to be forthcoming about potentially risky online behaviors. Digital self-harm: What to do when kids cyberbully themselves |Juli Fraga |February 26, 2021 |Washington Post 

It would let the team compare how kids who attended quality-rate preschools did on their reading and math tests through their senior year of high school, compared to those who didn’t. Morning Report: Did Mexico Solve the Border Sewage Problem? |Voice of San Diego |February 26, 2021 |Voice of San Diego 

Hearing a kid rant or shout in a store reflects poorly on the parents, not the child. Hints From Heloise: Falling for a scam can be as simple as saying ‘yes’ |Heloise Heloise |February 26, 2021 |Washington Post 

Proving the shots are safe and effective for children is a crucial first step to vaccinating this population and protecting kids’ health. COVID-19 vaccines may be ready for teens this summer |Aimee Cunningham |February 25, 2021 |Science News 

The kid from next door drops by and Marvin talks to him about the stunts in his latest film, Death Hunt. The Story Behind Lee Marvin’s Liberty Valance Smile |Robert Ward |January 3, 2015 |DAILY BEAST 

He stayed up all night, looking at the streets he had biked around as a kid with a whole new sensibility. DJ Spooky Wants You To Question Everything You Know About Music, Technology, and Philosophy |Oliver Jones |December 27, 2014 |DAILY BEAST 

After years at the head of a parochial school classroom, he could no longer distinguish one blond Irish Catholic kid from another. Obama’s One Hand Clap With Castro |Doug McIntyre |December 24, 2014 |DAILY BEAST 

“I was watching ‘Daniel The Tiger’ with my kid and I heard two shots like ‘boom-boom,’” he said. Anger at The Cop Killer - And The Police |M.L. Nestel |December 21, 2014 |DAILY BEAST 

“I walk my kid to school, passed that cop car everyday,” he said. Anger at The Cop Killer - And The Police |M.L. Nestel |December 21, 2014 |DAILY BEAST 

"I'll look in the bar," I volunteered, remembering the kid had left with more of a roll than Meadows had now. Fee of the Frontier |Horace Brown Fyfe 

Zoomed over the German lines in the war, stoking an airplane, although at that time he was only a kid. The Campfire Girls of Roselawn |Margaret Penrose 

And dragged Joe into it, a good kid who had made only one really bad mistake in his life—the mistake of asking her to marry him. The Man from Time |Frank Belknap Long 

Again it was empty except for the operator, a tow-headed kid with a Racing Form tucked in a side pocket. Hooded Detective, Volume III No. 2, January, 1942 |Various 

Your gloves must be of kid, white, or some very light tint to suit your dress. The Ladies' Book of Etiquette, and Manual of Politeness |Florence Hartley