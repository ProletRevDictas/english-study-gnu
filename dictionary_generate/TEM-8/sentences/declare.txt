They were not being hyperbolic in declaring medical racism a public health crisis. Medical racism has shaped U.S. policies for centuries |Deirdre Owens |February 12, 2021 |Washington Post 

Plus, by essentially declaring that you quit, you’ve made all the friends you’re going to make in this life, you’re denying yourself whatever joys serendipity has to offer. Carolyn Hax: No time for small talk? That’s building a big wall. |Carolyn Hax |February 11, 2021 |Washington Post 

Then the city declared it was moving ahead to reopen schools without the consent of the teachers union. Lawsuits, lockouts and strike threats: Fights to reopen classrooms reach a head in several school districts |Moriah Balingit |February 8, 2021 |Washington Post 

A year after smallpox was declared eradicated, bad lab safety procedures led to another outbreak in Birmingham in the UK. Smallpox used to kill millions of people every year. Here’s how humans beat it. |Kelsey Piper |February 5, 2021 |Vox 

Further exacerbating the situation, the county sent residents a recent email declaring that just because the state allows people in lower tiers to get vaccinated, “that doesn’t mean that every county is able to do that.” Getting a coronavirus vaccine appointment fills me with despair. Every search ends in frustration, and I’m at a low point of a long, dark year. |Marlene Cimons |February 5, 2021 |Washington Post 

Being the first to declare feelings is incredibly difficult. Random Hook-Ups or Dry Spells: Why Millennials Flunk College Dating |Ellie Schaack |January 1, 2015 |DAILY BEAST 

And it led him in his teenage years to declare his ambition to become a cop. In The Shadow of Murdered Cops |Michael Daly |December 26, 2014 |DAILY BEAST 

That attack prompted the government to declare a three-month state of emergency in parts of North Sinai. ISIS Wannabes Claim They Killed an American in Egypt |Jamie Dettmer |December 1, 2014 |DAILY BEAST 

If J-Law suddenly decided to declare solidarity with Thai anti-junta activists? ‘The Hunger Games’ Stars Silent on Thai Protesters |Asawin Suebsaeng |November 21, 2014 |DAILY BEAST 

Beijing also demanded that Abe declare he will no longer pay visits to the war-linked Yasukuni Shrine in Tokyo. Beijing’s ‘Star Trek’ APEC Summit |Jake Adelstein |November 11, 2014 |DAILY BEAST 

Nations shall declare his wisdom, and the church shall shew forth his praise. The Bible, Douay-Rheims Version |Various 

Hasten the time, and remember the end, that they may declare thy wonderful works. The Bible, Douay-Rheims Version |Various 

Insurrectionary movements at Rome in consequence of the pope's refusal to declare war against Austria. The Every Day Book of History and Chronology |Joel Munsell 

They used to declare that every unbaptised baby would go to Hell and burn for ever in fire and brimstone. God and my Neighbour |Robert Blatchford 

"I declare, wife, that was an awful accident over to the mills," said Mr. Slocum. The Book of Anecdotes and Budget of Fun; |Various