With an RDP, the propeller is inside a duct, and the duct itself contains the motor that powers the prop blades. Electric propulsion makes this French submarine concept extra sneaky |Christina Mackenzie |November 30, 2020 |Popular-Science 

They broke conversational conventions, they had new and stricter rules, they incorporated movement or props, there was both choreography and craft in their construction. Better conversations: The 7 essential elements of meaningful communication |matthewheimer |November 24, 2020 |Fortune 

“Mattresses were brought out for the stage, bedsheets were used for the curtains, and with the aluminum foil in cigarette packets my parents used to make crowns for us” as props, he told India’s Telegraph newspaper. Soumitra Chatterjee, celebrated Indian actor and ‘one-man stock company’ for Satyajit Ray, dies at 85 |Harrison Smith |November 23, 2020 |Washington Post 

The 2012 agreement between the Obama and Romney campaigns included rules against using props or visual aids and barred follow-up questions from the audience during the town hall. The first presidential debate was chaotic. Here’s why improving the next one will be tough. |Kim Bellware |October 1, 2020 |Washington Post 

The propeller sits in back for the same reason—a prop in the front would be unfriendly to smooth flow. This weird-looking plane could someday be a fast, clean option for air travel |Rob Verger |September 28, 2020 |Popular-Science 

Without the proper equipment to repair and operate the Mohajer-4 it may be more of a photo prop than a piece of weaponry. ISIS: We Nabbed an Iranian Drone |Jacob Siegel |November 17, 2014 |DAILY BEAST 

But this year, New Yorkers have a shot at making real changes through a ballot initiative sonorously known as Prop 1. Hate Hyper-Partisanship? Support Redistricting Reform Now |John Avlon |November 3, 2014 |DAILY BEAST 

This is good—no one needs rape on TV used as a prop, or as background noise, or for shock value. The Walking Dead’s ‘Slabtown’: The Real Source of Terror Isn’t Walkers, It’s Rape |Melissa Leon |November 3, 2014 |DAILY BEAST 

They see themselves being set up as a sacrifice for a U.S. policy meant to prop up Iraq. Why Does the Free Syrian Army Hate Us? |Jamie Dettmer |October 3, 2014 |DAILY BEAST 

After we tour the river we fly on a single-prop plane back to Fort Mac. Our Trip to The Climate War's Ground Zero |Darren Aronofsky |September 19, 2014 |DAILY BEAST 

He realized for the first time what a prop and resource the deep maturity and scornful strength of his mother had been. Ancestors |Gertrude Atherton 

Then the thought of what had awakened her made her prop herself up on an elbow and gaze around. The Adventure Girls at K Bar O |Clair Blank 

Just now, Grandfather's keyster is the Rock of Gibraltar, the financial prop that is sustaining the whole structure. David Lannarck, Midget |George S. Harney 

If, by accident, the underground roots die off, the plant relies entirely on these air and prop roots for support and food. Philippine Mats |Hugo H. Miller 

The strong prop roots are generally of the same diameter throughout, though sometimes they thicken at the ends. Philippine Mats |Hugo H. Miller