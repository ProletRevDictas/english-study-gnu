A crimson orifice was seen just back of the foreleg, which showed where the tiny messenger of death had entered. Two Boys in Wyoming |Edward S. Ellis 

Recalling the words of Hank Hazletine, Fred aimed at a point just back of the foreleg, as it reached forward. Two Boys in Wyoming |Edward S. Ellis 

On the plain two mounted Tibetans were pursuing a wild ass, which was wounded in the near foreleg and had four dogs at his heels. Trans-Himalaya, Vol. 2 (of 2) |Sven Hedin 

Bill started a bunch of five deer and succeeded in getting a shot and breaking a foreleg of a large doe. Fifty Years a Hunter and Trapper |Eldred Nathaniel Woodcock 

Her tail was a sort of stump, in size and in look very  much like a spare foreleg, stuck in anywhere to be near. Spare Hours |John Brown