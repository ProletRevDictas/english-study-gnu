The country’s statistics office specifically gave shout-outs to the computer chips sector in reporting its advance estimates of economic data last week, crediting “strong demand for parts of electronics products” in propelling the growth of exports. Semiconductors helped make Taiwan Asia’s top-performing economy in 2020 |Mary Hui |February 3, 2021 |Quartz 

After initially proposing to expand the five-month post-count processing period, the government condensed it to about half that amount of time, causing alarm among statistics experts who warned it could result in an inaccurate count. Lawmaker assails Commerce Dept. over failure to respond to questions about 2020 Census anomalies |Tara Bahrampour |December 3, 2020 |Washington Post 

The national statistics agency said earlier this week that the impact of the Covid crisis will dent life expectancy in Sweden, after over a century of almost uninterrupted growth. Suddenly, Swedes are ‘very worried’ that the ‘herd immunity’ strategy is no match for COVID |kdunn6 |November 26, 2020 |Fortune 

When it comes to search engine market share in China, only traditional search engines like Baidu and Sogou will be classified in the landscape by all the statistics providers. Are search engines dead in China? |Ada Luo |September 14, 2020 |Search Engine Watch 

Only 10 percent of high school students take a statistics class — and even most statistics courses are primarily theoretical rather than requiring students to get their hands dirty with data. America’s Math Curriculum Doesn’t Add Up (Ep. 391) |Steven D. Levitt |October 3, 2019 |Freakonomics 

So however detailed the statistics of the battlefield are, they cannot achieve the goal. Pentagon Doesn’t Know How Many People It’s Killed in the ISIS War |Nancy A. Youssef |January 7, 2015 |DAILY BEAST 

A hard look at campus rape statistics, the collapse of The New Republic and the day John Lennon died. The Daily Beast’s Best Longreads, Dec 8-14, 2014 |William Boot |December 13, 2014 |DAILY BEAST 

What is much more important than these numbers is an internal dynamic for which there are no statistics. Beer Countries vs. Wine Countries |Clive Irving |December 7, 2014 |DAILY BEAST 

In fact, what this map really showed was the fallacy of aggregates – and how statistics can mask real cultural shifts. Beer Countries vs. Wine Countries |Clive Irving |December 7, 2014 |DAILY BEAST 

In the face of those statistics, these two non-indictments are glaring. First Mike Brown, Then Eric Garner: Prosecutors Can’t Be Trusted to Try Cops |Sally Kohn |December 3, 2014 |DAILY BEAST 

Gottfried Achenwall, an eminent German lecturer on statistics, history and the laws of nature, died at Gttingen. The Every Day Book of History and Chronology |Joel Munsell 

He published several volumes on political economy, and was much interested in statistics. The Every Day Book of History and Chronology |Joel Munsell 

Well pleased was Mr. Wainwright to see that statistics took my fancy. Fifty Years of Railway Life in England, Scotland and Ireland |Joseph Tatlow 

These statistics were of much practical use when considering questions of economy and other matters from day to day. Fifty Years of Railway Life in England, Scotland and Ireland |Joseph Tatlow 

I was soon surprised to find that I too had a taste for statistics and acquired some skill in their compilation. Fifty Years of Railway Life in England, Scotland and Ireland |Joseph Tatlow