The system would be better poised to help the region recover once the pandemic ends, he said. Metro board expresses wariness over increased debt but gives tentative approval |Justin George |February 11, 2021 |Washington Post 

It was the rare offseason in which Embiid wasn’t recovering from surgery or an injury, and the group took advantage. Joel Embiid Changed His Offseason Conditioning. Now He’s Playing Like An MVP. |Yaron Weitzman |February 9, 2021 |FiveThirtyEight 

I found I was more tired at the end of the day but made an effort to consistently get to bed earlier so that I could recover and run the next day. Want to Improve Your Running? Focus on Recovery. |Outside Editors |February 5, 2021 |Outside Online 

There is not enough money to help seriously ill children recover. 'The Whole System Needs Changing.' The Russia Protests Are About More Than Just Alexei Navalny |Madeline Roache |February 5, 2021 |Time 

“They all recovered, but not everyone is that lucky,” text in the video reads, affixed over footage of maskless people celebrating. Super spreader Sunday? Experts worry Super Bowl could trigger coronavirus explosion |Brittany Shammas, Fenit Nirappil, Mark Maske |February 5, 2021 |Washington Post 

We need to recover and grow the idea that the proper answer to bad speech is more and better speech. How the PC Police Threaten Free Speech |Nick Gillespie |January 9, 2015 |DAILY BEAST 

Women are more likely to recover sooner from birth and less likely to experience post-partum depression. How Good Dads Can Change the World |Gary Barker, PhD, Michael Kaufman |January 6, 2015 |DAILY BEAST 

This slows the rate of all the above processes and increases the chances that someone can recover quickly enough to wake up. What It’s Like to Wake Up Dead |Dr. Anand Veeravagu, MD, Tej Azad |November 21, 2014 |DAILY BEAST 

It took decades for comics to recover and emerge as an adult art form. The Insane Swedish Plan to Rate Games for Sexism |Nick Gillespie |November 20, 2014 |DAILY BEAST 

She literally had to lie down in between sessions in order to recover her strength. Jeopardy! Champion Julia Collins’s Brain Feels Like Mush |Sujay Kumar |November 20, 2014 |DAILY BEAST 

We shall recover again some or all of the steadfastness and dignity of the old religious life. The Salvaging Of Civilisation |H. G. (Herbert George) Wells 

The purchasers found that this claim was not well founded, and sought to recover their money. Women in the fine arts, from the Seventh Century B.C. to the Twentieth Century A.D. |Clara Erskine Clement 

Having paused a few seconds to recover breath, he brushed his hat with his elbow, and declared himself ready. The Posthumous Papers of the Pickwick Club, v. 2(of 2) |Charles Dickens 

The law only gave you the right to proceed against him to recover money damages for the legal injury. Putnam's Handy Law Book for the Layman |Albert Sidney Bolles 

Again, if the agent deposited the money in his own name the true owner could proceed against the bank to recover it. Putnam's Handy Law Book for the Layman |Albert Sidney Bolles