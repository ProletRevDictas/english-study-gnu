In a robotic patter, a guard read a proposal to change Navalny’s status at the prison. The Man Putin Fears |Simon Shuster |January 19, 2022 |Time 

While there are plenty of sounds to choose from, our favorites included the gentle patter of rainfall, which masked a bed partner’s snores. Our Favorite Gear For a Better Night’s Sleep |syadron |September 1, 2021 |Outside Online 

In the patter around, and woven into, his brisk piano reading of “Tenth Avenue Freeze-Out” he recalled his late friend and longtime bandmate Clarence Clemons. The Reopening of Springsteen on Broadway Brought Broadway Out of Hibernation—and One Packed Theater Into a Brighter Future |Stephanie Zacharek |June 28, 2021 |Time 

Demi had been keeping up a soft patter of chat, but just now she wasn’t sure what to say. How a rural Virginia town came together for an unforgettable pandemic prom |Hannah Natanson |May 7, 2021 |Washington Post 

“Without knowing it, during the kite chases I was learning how to get around — by sensing the currents of air and by listening to the patter of feet on a roof, to the scrapes of shoes along a wall,” he wrote. Ved Mehta, whose monumental autobiography explored life in India, dies at 86 |Harrison Smith |January 12, 2021 |Washington Post 

The scenes are succinct, by and large; the patter of the characters rolls right along, whether you catch their drift or not. Novelist D. Foy Dubs His Debut ‘Gutter Opera’ And Who Are We To Argue? |J.T. Price |May 12, 2014 |DAILY BEAST 

But feverish speculation and the constant patter of Vaudevillian innuendo came to overshadow more serious business. Francois Hollande Announces Breakup with First Lady Valerie Trierweiler |Tracy McNicoll |January 26, 2014 |DAILY BEAST 

Her father, Frederick Dalziel, was British and with a bearing and patter that suggested far more wealth than he had. Understanding Diana Vreeland, ‘Empress of Fashion’ |Robin Givhan |November 28, 2012 |DAILY BEAST 

You pretty much can't get a better absurdist parody of politicians' vapid sure-is-nice-to-be-here patter than that. Mitt's Sense of Humor, Ctd. |Justin Green |October 19, 2012 |DAILY BEAST 

My heart kept up its pitter-patter as I continued reading down the thread. Daddy, How Come You’re Always Broke? Benjamin Anastas’s ‘Too Good to Be True’ |Benjamin Anastas |October 15, 2012 |DAILY BEAST 

The short steps patter on the bridge connecting the upper rotunda with the cell-house, and pass along the gallery. Prison Memoirs of an Anarchist |Alexander Berkman 

When the carriage-door was shut and the driver was mounting his box, the same old patter attracted my attention. The Nursery, December 1881, Vol. XXX |Various 

Weston looked up sharply as a patter of approaching footsteps rose out of the shadows behind him. The Gold Trail |Harold Bindloss 

At that moment there was the sound of a scream, then the patter of running feet in the court below. The Sunbridge Girls at Six Star Ranch |Eleanor H. (Eleanor Hodgman) Porter 

Let others who have more sin on their souls, and are more frighted by priests' patter, go if they list. God Wills It! |William Stearns Davis