Like all great political art, it captured its era without being captive to it. Movies are rushing to impact the election. Don’t ask whether they’ll work. Ask whether they’ll last. |Ann Hornaday |October 30, 2020 |Washington Post 

It is also home to three small packs of captive-born wolves, a handful of injured raptors, and five American river otters. These Bears Have a Job, and It's Destroying Coolers |Emma Walker |October 9, 2020 |Outside Online 

For some it’s minted millions with captive audiences realizing that they really, really hate that couch and it’s finally time to replace it. Sophie Hill on the changing face of retail and surviving 2020 |Margaret Trainor |September 17, 2020 |TechCrunch 

The workers feel the tooting vibration and move to keep the other queens captive. Quacks and toots help young honeybee queens avoid deadly duels |Sharon Oosthoek |September 14, 2020 |Science News For Students 

This metric tends to keep B2B brands captive on this professional platform for its lead generation opportunities. How to plan your social media strategy for any business |Sumeet Anand |June 24, 2020 |Search Engine Watch 

Sabrine reports that the latest demands by ISIS militants are three prisoners for every captive soldier. A Sunni-Shia Love Story Imperiled by al Qaeda |Ruth Michaelson |December 26, 2014 |DAILY BEAST 

It was once the most glamorous hotel in town, but in 1964, hundreds of European hostages were held captive in its rooms. The Congo's Forgotten Colonial Getaway |Nina Strochlic |December 18, 2014 |DAILY BEAST 

Question 9: If the female captive was impregnated by her owner, can he then sell her? ISIS Jihadis Get ‘Slavery for Dummies’ |Jamie Dettmer |December 9, 2014 |DAILY BEAST 

Their night takes an unexpected twist when they break into a home and discover a young girl is being held captive inside. Nitehawk Shorts Festival: ‘Brute,’ a Twisted Take on Playing in the Dark |Julia Grinberg |November 28, 2014 |DAILY BEAST 

Earlier this year, a mutual friend told me that Peter was being held captive in Syria. ISIS Thugs Behead Peter Kassig |Nick Schwellenbach |October 4, 2014 |DAILY BEAST 

The only thing that at all tended to shake this conviction, was the extraordinary poltroonery of our new captive. Blackwood's Edinburgh Magazine, No. CCCXXXIX. January, 1844. Vol. LV. |Various 

The wee round wonders with their mystery of velvety colour are well fitted to take captive the young eye. Children's Ways |James Sully 

American vessels made occasional trips outside the Bay, and brought in captive sailing-vessels. The Philippine Islands |John Foreman 

The cities of the south are shut up, and there is none to open them: all Juda is carried away captive with an entire captivity. The Bible, Douay-Rheims Version |Various 

And he jerked his thumb towards the tower where mademoiselle was a captive, and where at night "Battista" was locked in with her. St. Martin's Summer |Rafael Sabatini