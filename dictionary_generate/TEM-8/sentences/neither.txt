But the enemy of the new emirs is neither the Jew nor the Christian, it is the godless militant defending secularism. Houellebecq’s Incendiary Novel Imagines France With a Muslim President |Pierre Assouline |January 9, 2015 |DAILY BEAST 

Despite the strong language, however, the neither the JPO nor Lockheed could dispute a single fact in either Daily Beast report. Pentagon Misfires in Stealth Jet Scandal |Dave Majumdar |January 8, 2015 |DAILY BEAST 

Neither the Republican nor the Democratic party have done anything to consistently target Asian- American voters. Asian-Americans Are The New Florida |Tim Mak |January 8, 2015 |DAILY BEAST 

This reporter knocked at the Wilkins home on Tuesday morning but received neither an answer nor the business end of a shotgun. The 7-Year-Old Plane Crash Survivor’s Brutal Journey Through the Woods |James Higdon |January 7, 2015 |DAILY BEAST 

Hmm, who are these people standing in front of the machines at the gym, neither occupying them nor not occupying them? How to Survive the New Year ‘Gympocalypse’ |Tim Teeman |January 6, 2015 |DAILY BEAST 

A desultory conversation on politics, in which neither took the slightest interest, was a safe neutral ground. The Pit Town Coronet, Volume I (of 3) |Charles James Wills 

On the morning after Ramona's disappearance, words had been spoken by each which neither would ever forget. Ramona |Helen Hunt Jackson 

This was no strange sight to the boy by that time, but it was awkward in the circumstances, for he had neither gun nor spear. The Giant of the North |R.M. Ballantyne 

Neither privately owned nor government stock is entitled to voting power. Readings in Money and Banking |Chester Arthur Phillips 

But she did not succeed in finding a suitable studio, neither an instructor who pleased her, and she returned to Amsterdam. Women in the fine arts, from the Seventh Century B.C. to the Twentieth Century A.D. |Clara Erskine Clement