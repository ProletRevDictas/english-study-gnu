For a corporate client who hosted 1,500-person holiday fetes before the pandemic, Tinsel Experiential Design in New York instead organized three separate events with two different concepts. Office holiday parties are back and smaller than ever |Rani Molla |November 22, 2021 |Vox 

She spent the day wandering around in shock until she could pop corks with her colleagues in a virtual fete. These scientists spent decades pushing NASA to go back to Venus. Now they’re on a hot streak. |Corinne Iozzio |June 29, 2021 |Popular-Science 

The real hackers—whoever they may prove to be—had pulled off a feat: they ruined a Hollywood fete. Sony Hack: A Dictator Move? |Kevin Bleyer |December 14, 2014 |DAILY BEAST 

The glamorous fete was followed by the premiere of the 4-hour miniseries Olive Kitteridge, which will premiere on HBO Nov. 2. Frances McDormand on 'Olive Kitteridge,' Dropping LSD, and Her Beef With FX's 'Fargo' |Marlow Stern |September 3, 2014 |DAILY BEAST 

The Fete Worse Than Death was an organizational disaster and a massive success. Joshua Compston Was Once the Wunderkind of the British Art World…and Now He’s Been Practically Forgotten |Anthony Haden-Guest |January 17, 2014 |DAILY BEAST 

Thanks to the presence of blackface and KKK costumes, her offensive fete went viral and earner her instant infamy. Why It’s Time to End Blackface, Finally |Soraya Roberts |October 31, 2013 |DAILY BEAST 

Paris Hilton tried her hand at DJing in L.A. at a fete celebrating the release of her new single with Lil Wayne, "Good Time." Paris Hilton's Trippy Los Angeles Release Party For Her Single With Lil Wayne |Jean Trinh |October 9, 2013 |DAILY BEAST 

At midsummer was to be a fete in the Saboba village, and the San Bernardino priest would come there. Ramona |Helen Hunt Jackson 

Two or three times during the winter he gave a fete as a matter of social pride in return for the civilities he received. Juana |Honore de Balzac 

Both were present at this fete given at the New Palace on the night of the 15th of July in their character of reporters. Michael Strogoff |Jules Verne 

Everybody wore rich apparel and was talking of the coming fete. The Land of the Changing Sun |William N. Harben 

Next week, sometime, I am to have my promised lawn fete to introduce 53 the countryside to the new member of our household. Mavis of Green Hill |Faith Baldwin