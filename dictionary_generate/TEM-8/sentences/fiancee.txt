Bird, 40, told her fiancée she was very tired and very happy. Sue Bird And Diana Taurasi Set a New Olympic Basketball Bar |Sean Gregory/Saitama, Japan |August 8, 2021 |Time 

As an engaged woman on the cusp of 30, my fiancée Lexi wants little more than anything to be a mom—a plant mom, that is. Personal Rise Garden review: A simple solution for bad plant parents |Tony Ware |July 22, 2021 |Popular-Science 

I lost the closest thing I had to a friend, I eventually lost my fiancée, I lost the ability to connect with others. The Few, the Proud, the Suicidal |Eugene Robinson |May 31, 2021 |Ozy 

If I’m being completely honest, it was a long conversation that kind of went over multiple days with my fiancée and I talking about the pros and cons. 2021 Could Be the Biggest Wedding Year Ever. But Are Guests Ready to Gather? |Eliana Dockterman |May 29, 2021 |Time 

He was fresh off of a breakup with his fashion designer fiancée, whom he dumped through a text message. Katie Holmes’ Pandemic Romance May Be Over. How About Yours? |Alaina Demopoulos |April 23, 2021 |The Daily Beast 

This engagement has been such a very public affair, so far, that I think I'd like to see my fiancee alone for a moment. April Hopes |William Dean Howells 

You have nothing to bother you—no family, no wife, no fiancee? The American |Henry James 

Moreover, he showed not the least sign that he had any idea such information might be startlingly obnoxious to his fiancee. The Call of the Canyon |Zane Grey 

As soon as he was in the presence of his fiancee he saw that she was again in the throes of some violent agitation. The Grain Of Dust |David Graham Phillips 

If there was never fiancee stronger-minded and more reserved than she, never was there mother more tender. Mauprat |George Sand