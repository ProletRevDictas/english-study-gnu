In 2017, tax dollars diverted into deductible voucher “donations” exceeded a billion dollars, providing “donors” with a dollar-for-dollar tax credits. Betsy DeVos is gone — but ‘DeVosism’ sure isn’t. Look at what Florida, New Hampshire and other states are doing. |Valerie Strauss |February 5, 2021 |Washington Post 

Using Carrum, there are no co-pays, deductibles or co-insurance. Connecting employer healthcare plans to surgical centers of excellence nets Carrum Health $40 million |Jonathan Shieber |January 7, 2021 |TechCrunch 

For example, some insured patients who opt not to use their benefits and instead take the company’s discount might be annoyed to find out that their purchase didn’t count toward their health insurance plan’s deductible. What Amazon’s big pharmacy news means for US health care |Dylan Scott |November 23, 2020 |Vox 

She said she wants to hold off on accepting the money until her paperwork is filed so the donations can be tax-deductible. Robin Kemp lost her news job in Clayton County, Ga. — but she kept reporting the news. It paid off on election week. |Reis Thebault |November 10, 2020 |Washington Post 

Because that plan has a $6,000 deductible, however, Barber said she would look for something better during open enrollment, in consultation with the same navigator. Many workers who lost their jobs due to COVID need help finding health coverage |lbelanger225 |November 10, 2020 |Fortune 

Bronze plans start at $191 a month, fully one-third more than the catastrophic plan, with a $6,000 deductible. With More Competition and Choice, Obamacare Might Not Be So Horrible |Nick Gillespie |May 12, 2014 |DAILY BEAST 

The change, by the way, removes deductible caps from certain plans small businesses can offer their employees. The House GOP’s Down-Low, Backhanded Endorsement of Obamacare |Michael Tomasky |April 7, 2014 |DAILY BEAST 

The application of a deductible is often tied to whether the National Weather Service identifies the event as a hurricane. MH370: How Do Insurers Put a Price on Life? |Daniel Gross |March 26, 2014 |DAILY BEAST 

Nonetheless, I still had to cough up $1,300 a month with a $5,000 deductible for the two of us. Obamacare May Be Dangerous to Your Health |Richard Woodward |November 30, 2013 |DAILY BEAST 

Pre-Obamacare, a Carefirst plan with a $2,700 deductible would have cost me $111 a month, according to the ehealthinsurance site. The Obamacare Death Spiral |David Frum |November 4, 2013 |DAILY BEAST