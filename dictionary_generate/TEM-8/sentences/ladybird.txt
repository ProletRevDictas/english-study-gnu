But then the biggest danger we faced was tripping over the occasional turtle that clambered out of Ladybird Lake. Ranger Rick and the Coyote |Carol Flake Chapman |September 10, 2011 |DAILY BEAST 

Most important of these is the natural enemy of the orange-tree scale, the ladybug, or ladybird beetle. A Civic Biology |George William Hunter 

A ladybird beetle, which has also been imported, is the most effective agent in keeping this pest in check. A Civic Biology |George William Hunter 

Perhaps this is heightened by the contrast between the pretty, trim form of the ladybird and the ugliness of the assassin bug. Book of Monsters |David Fairchild and Marian Hubbard (Bell) Fairchild 

A Prussian ladybird rhyme also mentions the boat that sailed across heaven. Comparative Studies in Nursery Rhymes |Lina Eckenstein 

It may linger still in the "beeship" of our rhymes, and in the "Khnchen" of the corresponding German ladybird rhyme. Comparative Studies in Nursery Rhymes |Lina Eckenstein