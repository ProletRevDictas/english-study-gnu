Number one, the wife of a venture capitalist is a multimillionaire. ‘How I Built This’ host Guy Raz on insights from some of the world’s most famous entrepreneurs |Rachel King |September 15, 2020 |Fortune 

Like any cash-poor landowner, Lister decides that she needs a rich wife and sets her sights on Ann Walker, a local heiress who’s been dazzled by Lister’s charisma for years. FROM THE VAULTS: The opposite of binge-watching |Brian T. Carney |September 4, 2020 |Washington Blade 

I don’t think it’s something that most people are voluntarily doing or are interested in doing, but it took us a long time to track down the wives that we did, and we’re fairly confident that when the show drops, more will come out. “People want to believe”: How Love Fraud builds an absorbing docuseries around a romantic con man |Alissa Wilkinson |September 4, 2020 |Vox 

The morning after we were evacuated, I left my wife, Jenny, and our two daughters, who are 15 and 17, in Santa Cruz and went back to my house in Bonny Doon with my friend Josh. How We Saved Our Neighborhood from a Wildfire |Dain Zaffke |September 2, 2020 |Outside Online 

My wife and I understood and challenged our hidden biases when our children introduced us to friends who were different from them. We won’t have a true economic recovery until we tackle the racial wealth gap |matthewheimer |September 1, 2020 |Fortune 

In straight relationships with an age gap, words like ‘gold-digger’ and ‘trophy wife’ get thrown around. Freaking Out About Age Gaps in Gay Relationships Is Homophobic |Samantha Allen |January 9, 2015 |DAILY BEAST 

His wife passed away and they had kids, and he wanted to focus on being a dad so he just stopped to raise his kids. Coffee Talk with Fred Armisen: On ‘Portlandia,’ Meeting Obama, and Taylor Swift’s Greatness |Marlow Stern |January 7, 2015 |DAILY BEAST 

The band was still on its way back as De Blasio and his wife departed. Funeral Protest Is Too Much for NYPD Union Boss |Michael Daly |January 5, 2015 |DAILY BEAST 

Except for maybe his brainiac wife… but she could do better anyway. All Your Internet Boyfriends Are Taken: Gosling, Cumberbatch, and now Joseph Gordon-Levitt |Melissa Leon |January 3, 2015 |DAILY BEAST 

So when my wife and I moved to Laurel Canyon I spent my first year working night and day on the show. The Story Behind Lee Marvin’s Liberty Valance Smile |Robert Ward |January 3, 2015 |DAILY BEAST 

But Mrs. Dodd, the present vicar's wife, retained the precious prerogative of choosing the book to be read at the monthly Dorcas. The Pit Town Coronet, Volume I (of 3) |Charles James Wills 

And that was that if he and his wife were to ever live together again and be happy, the family were to be kept out of it. The Homesteader |Oscar Micheaux 

His wife stood smiling and waving, the boys shouting, as he disappeared in the old rockaway down the sandy road. The Awakening and Selected Short Stories |Kate Chopin 

To Harrison and his wife there was no distinction between the executive and judicial branches of the law. The Bondboy |George W. (George Washington) Ogden 

The Authorised Version has: “And as a mother shall she meet him, and receive him as a wife married of a virgin.” Solomon and Solomonic Literature |Moncure Daniel Conway