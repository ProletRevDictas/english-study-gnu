I’m a mountain person who, for some reason, enjoys climbing thousands of vertical feet by myself in the dark, so elk and mule deer were the choice for me. The Beginner's Guide to Hunting |Ian Fohrman |October 30, 2020 |Outside Online 

Earlier this year, she got a chance to find out when she joined a bow hunt for mule deer with two rising stars of huntstagram, the social media sphere dedicated to all things hunting. A First-Time Hunter Gets a Lesson from #WomenWhoHunt |Outside Editors |October 7, 2020 |Outside Online 

While relatively small burns intended to regenerate habitat—say, 100 acres to improve mule deer range—are exempt from rigorous analysis, larger projects can take years to be approved, says Van de Water. How we can burn our way to a better future |Ula Chrobak |October 2, 2020 |Popular-Science 

That you don’t have to be somebody’s emotional or spiritual mule to be worthy of love. Get Gabrielle Union’s Best Career Advice |Joshua Eferighe |September 16, 2020 |Ozy 

The company was working on military contracts, and BigDog was supposed to be a sort of pack mule for soldiers. The Robot Revolution Was Televised: Our All-Time Favorite Boston Dynamics Robot Videos |Jason Dorrier |July 19, 2020 |Singularity Hub 

He weighed only 185 pounds, but he had killer instincts and rabbit quickness and the stamina of a mule. Football Great Bob Suffridge Wanders Through the End Zone of Life |Paul Hemphill |September 6, 2014 |DAILY BEAST 

He took the left one and, with a pile driver of a mule kick, almost ripped it off its hinges. The Night the SEALS Captured the Butcher of Fallujah |Patrick Robinson |November 11, 2013 |DAILY BEAST 

Those players called Robinson “Mule” because he worked them hard as pack animals. Eddie Robinson, College Football’s Winningest Coach |Samuel G. Freedman |August 23, 2013 |DAILY BEAST 

In some places we want cows but not bison, or mule deer but not coyotes, or cars but not elk. Why Do We Save Some Species and Let Others Get Devastated? |Melissa Holbrook Pierson |May 21, 2013 |DAILY BEAST 

Steadily, our gunman pushed forward, his mule high-stepping through brush. An Obsessive’s Search for a Lost Jungle City |Christopher S. Stewart |December 30, 2012 |DAILY BEAST 

At this moment the tinkling of a mule's bells, mingled with the song of the muleteer, came on the air. Blackwood's Edinburgh Magazine, Volume 60, No. 372, October 1846 |Various 

Massed on the plateau above the mule-path, the whole population of the village stood to watch them down the steep descent. Rosemary in Search of a Father |C. N. Williamson 

On crossing it for the first time, I perceived lying about me half putrid cats and dogs—and even a mule in the same state. A Woman's Journey Round the World |Ida Pfeiffer 

He drives a white mule, and has managed to put a top of sail cloth on an old ramshackle buggy, which he calls a 'shay.' The Cromptons |Mary J. Holmes 

I took my own saddle ashore: and being mounted on a fine mule, we all began our journey towards the hill. Journal of a Voyage to Brazil |Maria Graham