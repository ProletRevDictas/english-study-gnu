In the Jurassic Park and Jurassic World movies, live dinosaurs enthrall human visitors. Cool Jobs: Bringing paleontology to the people |Beth Geiger |June 9, 2022 |Science News For Students 

The Viktor Wynd Museum of Curiosities, Fine Art & Natural History is here to enthrall you with the odd, the unusual and the otherworldly. Boo! Spooky Cocktails Inspired by the Occult |Wayne Curtis |October 29, 2021 |The Daily Beast 

Love it or hate it, technology enthralls us with the promise of change. The dangerous appeal of technology-driven futures |Sheila Jasanoff |June 30, 2021 |MIT Technology Review 

Choose carefully and this soft, plummy varietal can enthrall like a fine Cabernet Sauvignon. Five Ways to Drink Bravely |Mark Oldman |October 25, 2010 |DAILY BEAST 

But what's certain is that Flight of the Conchords will continue to enthrall its fan base for years to come. HBO's Troubadours Take Flight |Jace Lacob |September 18, 2009 |DAILY BEAST 

Can we not interest him in our books, enthrall him in the Chronicles of Kisington, so that he will cease to make war? Kisington Town |Abbie Farwell Brown 

Kingsley's "Water Babies" and other choice writings appear in full; these enthrall the imagination and cultivate the taste. The World and Its People: Book VII |Anna B. Badlam 

The surrounding mountains were in full panoply of their blazing October foliage, a scene to enthrall the dullest vision. Wanted: A Husband |Samuel Hopkins Adams 

The beach of Fales would enthrall but sterilize me—I mean the social muse would disjoint the classic nose of the other. The Letters of Henry James (volume I) |Henry James 

A man that so passionately avows his repudiation of the world must have felt its attraction, its power to tempt and enthrall. Hymns and Hymnwriters of Denmark |Jens Christian Aaberg