These chips were initially designed to handle the high data volumes that need to be crunched for animation—making them the standard chip for use in computer gaming and computer-aided design. Nvidia’s purchase of Arm creates an A.I. computing juggernaut |Jeremy Kahn |September 14, 2020 |Fortune 

For some, it kept the economy in a state of suspended animation, allowing families without income and businesses without profits to continue to pay rent and stay afloat. The Big Corporate Rescue and the America That’s Too Small to Save |by Lydia DePillis, Justin Elliott and Paul Kiel |September 12, 2020 |ProPublica 

Create your new blogs in the video format with a voiceover and some animation. How businesses can use YouTube to tackle the COVID-19 business crisis |Catherrine Garcia |September 7, 2020 |Search Engine Watch 

To bypass that protection, the researchers used code that was tailored to a built-in animation component called spinner-grow. A single text is all it took to unleash code-execution worm in Cisco Jabber |Dan Goodin |September 4, 2020 |Ars Technica 

Networks and streamers alike may get more creative with their programming, relying more on animation and user-generated content—like web series on YouTube. After years of ‘too much TV,’ the pandemic means there’s now barely enough |Aric Jenkins |August 27, 2020 |Fortune 

Stop-motion animation artist PES has unveiled a new short this week. ‘Sexual’ Barbershop Quartet, a Panda Family Reunion, and More Viral Videos |The Daily Beast Video |December 14, 2014 |DAILY BEAST 

As for Ready for Hillary, it now survives in a sort of suspended animation. Team Clinton Prepares for the Other Side of If |David Freedlander |November 22, 2014 |DAILY BEAST 

Jeffrey Katzenberg, the CEO of DreamWorks Animation, is basically the “new George Soros.” Meet the Hollywood Power Couple Who Bet Big on the Midterms—and Lost |Asawin Suebsaeng |November 6, 2014 |DAILY BEAST 

Shooting entirely on film, Davis personally oversaw the animation of the specters at his production studios in Indiana. Garfield Television: The Cat Who Saved Primetime Cartoons |Rich Goldstein |November 5, 2014 |DAILY BEAST 

Garfield the cat occupies an understated and often overlooked position critical to the history of televised animation. Garfield Television: The Cat Who Saved Primetime Cartoons |Rich Goldstein |November 5, 2014 |DAILY BEAST 

The quadroon was following them with little quick steps, having assumed a fictitious animation and alacrity for the occasion. The Awakening and Selected Short Stories |Kate Chopin 

He spoke with an animation and earnestness that gave an exaggerated importance to every syllable he uttered. The Awakening and Selected Short Stories |Kate Chopin 

The duke was in the highest animation, and he talked to every one round him, as we marched along, with more than condescension. Blackwood's Edinburgh Magazine, No. CCCXXXIX. January, 1844. Vol. LV. |Various 

The orators of the opposition declaimed against him with great animation and asperity. The History of England from the Accession of James II. |Thomas Babington Macaulay 

As the hour of death drew near, her courage and animation seemed to increase. Madame Roland, Makers of History |John S. C. Abbott