They are able to move through obscure currencies, but eventually they end in the same spot, which is moving it back to Bitcoin and through the over-the-counter market. North Korean hackers steal billions in cryptocurrency. How do they turn it into real cash? |Patrick O'Neill |September 10, 2020 |MIT Technology Review 

Another approach, called “chain hopping,” moves the money through different cryptocurrencies and blockchains to get it away from Bitcoin—where every transaction is posted to a public ledger—and into other, more private currencies. North Korean hackers steal billions in cryptocurrency. How do they turn it into real cash? |Patrick O'Neill |September 10, 2020 |MIT Technology Review 

Projects like Libra and, especially, the digital yuan also pose significant privacy risks, as the networks on which the currencies travel can also track who is spending money and where. Mastercard launches digital currency kit for central banks |Jeff |September 9, 2020 |Fortune 

The Mastercard initiative comes at a time of growing interest in digital currency among central banks. Mastercard launches digital currency kit for central banks |Jeff |September 9, 2020 |Fortune 

The digital currency itself is just a small part of this ideal world. Smart stimulus: Cash as code |Claire Beatty |September 9, 2020 |MIT Technology Review 

Then the gift card is shopped online in a gray market to collect cold currency. The Insane $11 Billion Scam at Retailers’ Return Desks |M.L. Nestel |December 19, 2014 |DAILY BEAST 

Russia depends on oil exports for almost 70 percent of its foreign-currency earnings and almost 50 percent of its annual budget. How Crimea Crashed the Russian Economy |Anna Nemtsova |December 17, 2014 |DAILY BEAST 

At currency auctions, it traded at around 64.45 rubles to the dollar and 78.8 to the euro. How Crimea Crashed the Russian Economy |Anna Nemtsova |December 17, 2014 |DAILY BEAST 

Currency problems are procyclical, which is to say that they create their own momentum. Putin Can’t Bully or Bomb a Recession |Daniel Gross |December 16, 2014 |DAILY BEAST 

The Arabs offered the Nazis a haven, as well as a market for all their nefarious dealings in arms and black market currency. Hitler’s Henchmen in Arabia |Guy Walters |December 7, 2014 |DAILY BEAST 

It stands at one extreme of our currency, with a dollar of gold set aside behind each dollar of paper. Readings in Money and Banking |Chester Arthur Phillips 

Between these two extremes the Federal Reserve note, a new form of currency, has been introduced. Readings in Money and Banking |Chester Arthur Phillips 

But the sheer quantity of the inflated currency and false money forces prices higher still. The Unsolved Riddle of Social Justice |Stephen Leacock 

That is, a demand for more currency in the hands of the public could have been supplied by the bank, but was not. Readings in Money and Banking |Chester Arthur Phillips 

But in one respect the currency notes helped to maintain the country's gold standard. Readings in Money and Banking |Chester Arthur Phillips