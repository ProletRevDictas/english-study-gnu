Its long-recorded history, which dates back to a ninth-century Icelandic saga and its mapping by Portuguese sailors in the 1500s, is littered with accounts of deadly shipwrecks on the island’s treacherous shoals. Sable Island’s famous wild horses are at the heart of a conservation controversy |Moira Donovan / Hakai Magazine |August 1, 2022 |Popular-Science 

Expect lots of shoals and waves, with long eddies between rapids. The Ultimate New River Gorge National Park Travel Guide |awise |July 19, 2021 |Outside Online 

Alter finds little evidence of open bigotry in his past, but Carter usually managed to navigate the perilous shoals of racial politics in the Deep South by portage — avoiding them as best he could. The case for Jimmy Carter as a ‘consequential’ president |Russell L. Riley |December 4, 2020 |Washington Post 

Off the entrance is a high rocky islet, the Nobby, within which the channel is shoal and dangerous to pass. Narrative of a Survey of the Intertropical and Western Coasts of Australia] [Volume 2 of 2] |Phillip Parker King 

The continuation of the shoal between the islands and Point Lookout was not clearly ascertained. Narrative of a Survey of the Intertropical and Western Coasts of Australia] [Volume 2 of 2] |Phillip Parker King 

At a short quarter of a mile from the point is a rocky shoal of small size, between which and the shore there is deep water. Narrative of a Survey of the Intertropical and Western Coasts of Australia] [Volume 2 of 2] |Phillip Parker King 

Some shoal marks on the water were observed opposite these islands, but their existence was not ascertained. Narrative of a Survey of the Intertropical and Western Coasts of Australia] [Volume 2 of 2] |Phillip Parker King 

At the bottom of Knocker's Bay is a shoal mangrove opening, of no importance. Narrative of a Survey of the Intertropical and Western Coasts of Australia] [Volume 2 of 2] |Phillip Parker King