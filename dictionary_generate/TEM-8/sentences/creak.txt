The creaks at night and the shifting shadows along the bedroom wall intimate to a young person’s mind that the external world is uncontrollable. ‘Little Nightmares II’: A hypnotic, dark fairy tale |Christopher Byrd |February 9, 2021 |Washington Post 

An iceberg splits with the same yawning creak as a tree beginning its fall to earth. Has Nature Gotten Louder During the Pandemic? |Greg Noone |October 15, 2020 |Outside Online 

You could hear the buildings creak and groan as they shook in their foundations. My Earthquake Experience in Tokyo |Andrew Pateras |March 11, 2011 |DAILY BEAST 

Garnache took the proffered chair, and sank down with creak and jingle to warm himself at the fire. St. Martin's Summer |Rafael Sabatini 

And this time Mrs. Armine noticed that the basket chair did not creak beneath his movement. Bella Donna |Robert Hichens 

A gentle breath from heaven makes the basket decline a little and the ropes creak against the hardwood clinch blocks. The Real Latin Quarter |F. Berkeley Smith 

Joe agreed; the anchor was lost, and the men prepared for the first creak that would show that the tide was coming. The Chequers |James Runciman 

But her ears magnified a thousandfold each crackling of a log and each creak of the floor sent expectant shivers along her spine. The Adventure Girls at K Bar O |Clair Blank