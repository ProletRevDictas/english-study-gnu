Obenshain, who was one of several Republicans who raised procedural concerns about the resolution. Virginia senator who called U.S. Capitol rioters ‘patriots’ is censured |Gregory S. Schneider |January 27, 2021 |Washington Post 

This is a more procedural power jump when compared to the GPU progress, but the new 11th-gen chips should provide some tangible performance upgrades. The most exciting new laptop tech coming in 2021 |Stan Horaczek |January 20, 2021 |Popular-Science 

Republicans, who are in the minority, have said they will insist that the session stick to a technical limit of 30 days, which is usually extended by more than two weeks with a procedural vote. Gov. Northam unveils Virginia budget plan with big spending on coronavirus as economy improves |Gregory S. Schneider |December 16, 2020 |Washington Post 

The City Council unanimously approved the measure Wednesday, but must vote on it a second time, per procedural rules. Encinitas Wants to Exempt Itself From New State Density Law |Kara Grant |December 11, 2020 |Voice of San Diego 

He is signaling openness to changing the House rules to curtail a minority procedural tool that Republicans have frequently used to divide the majority. ‘We’re in the foxhole together’: House Democrats reckon with a diminished majority |Mike DeBonis |November 23, 2020 |Washington Post 

The House Republican leadership could barely win a procedural vote earlier Thursday afternoon. Bachmann and Pelosi vs. Boehner and Obama Over Spending Bill |Ben Jacobs |December 11, 2014 |DAILY BEAST 

(Not one Democrat supported it on the procedural vote earlier Thursday afternoon). Nancy Pelosi Plays Hardball On Cromnibus |Ben Jacobs |December 11, 2014 |DAILY BEAST 

Republicans supported the procedural vote to let the “Democracy for All” amendment proceed to floor debate. The New War on Big Money in Politics |Eleanor Clift |September 10, 2014 |DAILY BEAST 

What you remember him from: Elliott played the lead in the military court procedural JAG for 10 seasons on CBS. The Most Random Old TV Stars to Appear on ‘Mad Men’ |Kevin Fallon |May 29, 2014 |DAILY BEAST 

He has put his name in history: There was absolute absence of procedural justice in these trials. The Hanging Judge of Minya, Egypt, Sentences Hundreds to Death |Bel Trew |April 28, 2014 |DAILY BEAST 

Second, there was the mass of rules, in form largely procedural, which was contained in the edicts. An Introduction to the Philosophy of Law |Roscoe Pound 

What Federal procedural requirements apply to suspension or expulsion? What Works: Schools Without Drugs |United States Department of Education 

First: Revising our customs regulations to remove procedural obstacles to profitable trade. Complete State of the Union Addresses from 1790 to 2006 |Various 

It might give procedural privileges: trial by combat is excluded, and trial by compurgation is secured and regulated. Our Legal Heritage, 5th Ed. |S. A. Reilly 

The essential nature of "consideration" in contract is evolving from the procedural requirements for the action of assumpsit. Our Legal Heritage, 5th Ed. |S. A. Reilly