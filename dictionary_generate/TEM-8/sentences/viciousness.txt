This can lead borrowers to take out another high-interest loan to pay off the last one, leading them into a vicious cycle that only profits the person who owns their debt. Apple is tougher on predatory lenders than the US government |Tim Fernholz |October 6, 2020 |Quartz 

They also viewed the vicious conversations in private groups as more deserving of protection than other types of content. As QAnon grew, Facebook and Twitter missed years of warning signs about the conspiracy theory’s violent nature |Craig Timberg, Elizabeth Dwoskin |October 1, 2020 |Washington Post 

These patterns are all part of a vicious cycle that has been feeding on itself for decades. Why There Are So Few Moderate Republicans Left |Lee Drutman (drutman@newamerica.org) |August 24, 2020 |FiveThirtyEight 

Worse yet, perhaps, are the cases of vicious genetic self-promotion at the expense of others—not content with enhancing the numerator, but actively diminishing the denominator. Just Because It’s Natural Doesn’t Mean It’s Good - Issue 89: The Dark Side |David P. Barash |August 19, 2020 |Nautilus 

Our Jesse Marx reported Friday that the man who spread vicious robocalls about it now says he was working for the family of another GOP candidate. Politics Report: What Comes Next for Sports Arena |Scott Lewis and Andrew Keatts |July 11, 2020 |Voice of San Diego 

“Vicious pecking, avian hysteria, mysterious deaths, and even cannibalism” are the results, he writes. The History of the Chicken: How This Humble Bird Saved Humanity |William O’Connor |December 27, 2014 |DAILY BEAST 

The late-November hacking of Sony, perhaps the most vicious episode of its kind, comes at the end of the period of mourning. Kim Jong Un’s Kid Gloves Are Now Off |Gordon G. Chang |December 17, 2014 |DAILY BEAST 

But most of all, Ramone lingered on Vicious, whom he painted and drew over and over again. ‘All Good Cretins Go to Heaven’: Dee Dee Ramone’s Twisted Punk Paintings |Melissa Leon |December 15, 2014 |DAILY BEAST 

Sid Vicious is stomping all over Steve Jones, about to smash in his guitar (again). ‘All Good Cretins Go to Heaven’: Dee Dee Ramone’s Twisted Punk Paintings |Melissa Leon |December 15, 2014 |DAILY BEAST 

In one painting, framed as a split-panel comic between the two, Ramone simply asks Vicious, “Did you kill her?” ‘All Good Cretins Go to Heaven’: Dee Dee Ramone’s Twisted Punk Paintings |Melissa Leon |December 15, 2014 |DAILY BEAST 

There was a vicious aching in his nerves, his muscles were flaccid and unstrung; a numbness was in his brain as well. The Wave |Algernon Blackwood 

There was a sudden forward movement on the part of the men; but if Garnache was vicious, he was calm. St. Martin's Summer |Rafael Sabatini 

And I answer that when a man does wrong he does it because he knows no better, or because he is naturally vicious. God and my Neighbour |Robert Blatchford 

With a vicious snarl, the dog lifted his great body into the air and plunged toward the Comet. Motor Matt's "Century" Run |Stanley R. Matthews 

He began to be afraid lest he might be overwhelmed in this slough of a petty, useless, and vicious existence. The Life &amp; Letters of Peter Ilich Tchaikovsky |Modeste Tchaikovsky