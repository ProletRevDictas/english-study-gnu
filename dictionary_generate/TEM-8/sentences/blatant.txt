Perhaps the most blatant, she said, was its sole use of voter registration lists as a source of finding potential jurors. Federal Court’s Jury Selection Plan Under Fire |Maya Srikrishnan |January 22, 2021 |Voice of San Diego 

Carrasco’s inclusion in the deal was a blatant salary dump, owing to the $27 million he is owed over the next two seasons. With deep pockets, grand ambition, new Mets owner swings blockbuster deal for Francisco Lindor |Dave Sheinin |January 7, 2021 |Washington Post 

Separately, there’s a blatant disconnect between those designing the PPP process and the businesses trying to benefit from it. Why the PPP still falls short for small businesses |jakemeth |December 31, 2020 |Fortune 

The show’s writers force us to confront how we think we would act if we woke up covered in someone else’s blood in a luxury Bangkok hotel, and then prod us with Cassie doing the blatant opposite. The absurd, sadistic joy of The Flight Attendant |Alex Abad-Santos |December 18, 2020 |Vox 

Critics fume that the shorts’ actions represent a blatant conflict of interest. Little Big Shorts: How tiny ‘activist’ firms became sheriffs in the stock market’s Wild West |Bernhard Warner |December 3, 2020 |Fortune 

A blatant case of interrogators asking leading questions is that of David Vasquez. How the U.S. Justice System Screws Prisoners with Disabilities |Elizabeth Picciuto |December 16, 2014 |DAILY BEAST 

Besides the blatant silliness of it all, it does raise some questions—and not about sex. The UK’s War on Porn: ‘Proof That Men Making These Rules Do Not See Women as Equals’ |Aurora Snow |December 6, 2014 |DAILY BEAST 

The second intervention was much more blatant and actually occurred in the middle of an election campaign. The Inside Story of U.S. Meddling in Israel’s Elections |Aaron David Miller |December 4, 2014 |DAILY BEAST 

According to Haselberger, the archdiocese ignored not only blatant secular crimes, but obvious canonical crimes as well. How Sicko Priests Got Away With It |Barbie Latza Nadeau |November 16, 2014 |DAILY BEAST 

True, it is grounded in the realities of a fight against a sort of blatant segregation that no longer exists. Martin Luther King’s Nobel Speech Is an Often Ignored Masterpiece |Malcolm Jones |October 16, 2014 |DAILY BEAST 

She realised now from what a blatant scoundrel she had been saved; but she still bitterly resented our intervention. Jaffery |William J. Locke 

Neither our blatant friend Sabatier, nor our courteous acquaintance of last night, shall catch me sleeping. The Light That Lures |Percy Brebner 

There is not a patent medicine on the market for which any more blatant, extravagant and ridiculous claims are made. The Propaganda for Reform in Proprietary Medicines, Vol. 1 of 2 |Various 

Three months' time was all that these blatant boasters allowed for the utter destruction of the Huguenots in France. History of the Rise of the Huguenots |Henry Baird 

At other times the great bull would merely have been enraged at this blatant clamor and taken it as a challenge. Kings in Exile |Sir Charles George Douglas Roberts