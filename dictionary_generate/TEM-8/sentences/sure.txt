I’m 98 percent sure I saw him at my local Seattle post office over the holidays. Can Billionaires Really Save Us from Climate Disaster? |Heather Hansman |February 12, 2021 |Outside Online 

As an SEO professional, we’re sure that there’s one word you’ve heard and used often, and that is, “keywords”. A comprehensive guide on using Google Trends for keyword research |Aayush Gupta |February 12, 2021 |Search Engine Watch 

It’s his life and his job on the line, and I want to make sure that I’m protecting his values and everything that he’s doing. What’s sexy in a pandemic? Caution. |Lisa Bonos |February 12, 2021 |Washington Post 

Also be sure to let friends who haunt garage and estate sales know that you’re on the lookout. Replacing pieces of flatware or china can be a challenge. Here’s how to track them down. |Laura Daily |February 11, 2021 |Washington Post 

When she made a half-joking comment about how she was starting to talk to herself, Willie made sure to check up on her more often and visit when she was getting lonely. Date Lab: One of our setups is still going strong two years later. We caught up with Willie and Renee for an update. |Marin Cogan |February 11, 2021 |Washington Post 

“They sure took the Sony thing seriously,” Attkisson said dryly. Ex-CBS Reporter Sharyl Attkisson’s Battle Royale With the Feds |Lloyd Grove |January 9, 2015 |DAILY BEAST 

And it must make sure that the platform of debate where we can freely exchange ideas is safe and sound. Ayaan Hirsi Ali: Our Duty Is to Keep Charlie Hebdo Alive |Ayaan Hirsi Ali |January 8, 2015 |DAILY BEAST 

Our duty is to make sure that they realize that the Prophet is not avenged. Ayaan Hirsi Ali: Our Duty Is to Keep Charlie Hebdo Alive |Ayaan Hirsi Ali |January 8, 2015 |DAILY BEAST 

I like the idea of Jon Hamm… There have been discussions—though I'm not sure how serious they've been. ‘Archer’ Creator Adam Reed Spills Season 6 Secrets, From Surreal Plotlines to Life Post-ISIS |Marlow Stern |January 8, 2015 |DAILY BEAST 

“He has to really stay on the down low, he has to make sure that he blends in,” Ney told the Beast. Abramoff’s Advice for Virginia’s New Jailhouse Guv |Tim Mak, Jackie Kucinich |January 7, 2015 |DAILY BEAST 

When a man converses with himself, he is sure that he does not converse with an enemy. The Life and Most Surprising Adventures of Robinson Crusoe, of York, Mariner (1801) |Daniel Defoe 

I am therefore quite sure I shall be content to await his father's consent, should it not come these many years. The Pastor's Fire-side Vol. 3 of 4 |Jane Porter 

And sure enough when Sunday came, and the pencil was restored to him, he promptly showed nurse his picture. Children's Ways |James Sully 

He had seen the act committed, he felt sure but had made no effort whatever to stop the thief. The Homesteader |Oscar Micheaux 

Gold and silver make the feet stand sure: but wise counsel is above them both. The Bible, Douay-Rheims Version |Various