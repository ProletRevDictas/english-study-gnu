Using a serrated or chef’s knife, cut the stem off the artichoke flush with the base so that the vegetable can stand upright. How to prep, cook and enjoy stuffed artichokes |Ann Maloney |March 11, 2021 |Washington Post 

Using a serrated knife, slice off about 1 to 1½ inches of the petals to create a flat top. How to prep, cook and enjoy stuffed artichokes |Ann Maloney |March 11, 2021 |Washington Post 

A small serrated spoon makes quick work of separating out the individual segments as you eat. Nine kitchen gifts that do one thing really well |Sara Chodosh |November 26, 2020 |Popular-Science 

Using a serrated knife, cut it evenly into about 12 to 15 buns. Ease into Winter with Backcountry-Approved Comfort Food |Christina Bernstein |November 12, 2020 |Outside Online 

It feels like an innocuous moment, it should be an innocuous moment, but such moments are like serrated precipices. Why 7 Times 8 Tripped Up the UK Chancellor |Tim Teeman |July 4, 2014 |DAILY BEAST 

The Superstar was the 1969 Adidas sneaker which featured the rubber toe cap and the iconic three serrated stripes along the side. How Sneaker Culture Conquered the World |William O’Connor |March 16, 2014 |DAILY BEAST 

Still, wagging a tail is a far sight better than cutting one off with a rusty serrated knife. What History Will Say About Obamacare and the Government Shutdown |Michael Tomasky |October 2, 2013 |DAILY BEAST 

You can get a decent serrated bread knife from a restaurant-supply store for less than $20. The Perfect Use for Deadly Weapons |Chad Ward |July 21, 2009 |DAILY BEAST 

Serrated knives are the disposable ballpoint pen of the knife world. The Perfect Use for Deadly Weapons |Chad Ward |July 21, 2009 |DAILY BEAST 

Our insect bears a remarkable similarity to a Surinam Buprestis, with serrated elytra. Narrative of a Survey of the Intertropical and Western Coasts of Australia] [Volume 2 of 2] |Phillip Parker King 

To the left a black serrated crest was hanging in the sky incredibly. Mount Everest the Reconnaissance, 1921 |Charles Kenneth Howard-Bury 

The shield of the Marquis bore, in reference to his title, a serrated and rocky mountain. The Ontario Readers: The High School Reader, 1886 |Ministry of Education 

In the distance inland the view was bounded by a serrated line of mountain peaks. Recollections of Thirty-nine Years in the Army |Charles Alexander Gordon 

Between each two balls a luminous serrated body extended, and at the last a blaze issued, which terminated in a point. The Book of Curiosities |I. Platts