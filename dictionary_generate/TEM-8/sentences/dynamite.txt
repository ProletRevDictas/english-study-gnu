Often taking the heavy-handed approach of detonating dynamite to free specimens, he amassed a collection of fossils representing both the young and the old. Fossils and ancient DNA paint a vibrant picture of human origins |Erin Wayman |September 15, 2021 |Science News 

For journalists suspicious about China’s handling of the virus, the thread—and those that followed—were dynamite. They called it a conspiracy theory. But Alina Chan tweeted life into the idea that the virus came from a lab. |Antonio Regalado |June 25, 2021 |MIT Technology Review 

Still, between the dynamite premise and the excellent cast, the show could easily be salvaged. AMC’s Kevin Can F**K Himself Squanders a Thrillingly Subversive Premise |Judy Berman |June 18, 2021 |Time 

The Taglieris, who work in IT and finance, are used to hearing dynamite detonations at least once a month at the plant, the couple said. A huge explosion cracked house foundations in New Hampshire. An ‘extreme’ gender-reveal party was to blame. |Andrea Salcedo |April 23, 2021 |Washington Post 

This was way louder and way more powerful than a dynamite blast. A huge explosion cracked house foundations in New Hampshire. An ‘extreme’ gender-reveal party was to blame. |Andrea Salcedo |April 23, 2021 |Washington Post 

The world that Black Dynamite lives in is not the most PC place to be in. ‘Black Dynamite’ Presents Police Brutality: The Musical |Stereo Williams |January 9, 2015 |DAILY BEAST 

Music is a huge part of the tone of Black Dynamite overall—going back to the original 2009 movie on which the series is based. ‘Black Dynamite’ Presents Police Brutality: The Musical |Stereo Williams |January 9, 2015 |DAILY BEAST 

One strip, Foolish Grandpa and Sour Henry, shows Grandpa being hit on the head by a sandbag and blown up by dynamite. The Magazine That Made—and Unmade—Politicians |Anthony Haden-Guest |November 2, 2014 |DAILY BEAST 

Jamiroquai's "Canned Heat," which was also featured in Napoleon Dynamite's epic dance scene. Viral Video of the Day: 100 Days of Dance |The Daily Beast Video |May 5, 2014 |DAILY BEAST 

But the poem set off tiny sticks of dynamite behind my eyes. Dealing With Dad the Dealer |Tony Doukopil |April 9, 2014 |DAILY BEAST 

Dynamite, by the good fortune of invention, came to the revolutionary at the very moment when it was most wanted. The Unsolved Riddle of Social Justice |Stephen Leacock 

He had thought he was amusing himself with a toy cannon, and he had fired a charge of dynamite. The Joyous Adventures of Aristide Pujol |William J. Locke 

Perhaps he helped to dynamite the barges and drive those Hessians out of town. Prison Memoirs of an Anarchist |Alexander Berkman 

The latter crouched there, frozen, hanging onto his hat as if it were a hunk of dynamite. Hooded Detective, Volume III No. 2, January, 1942 |Various 

At the bow of this launch was a long spar, and at the end of this spar was a torpedo holding a hundred pounds of dynamite. Stories of Our Naval Heroes |Various