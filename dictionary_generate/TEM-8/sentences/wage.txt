Minimum-wage debates for years have been tempered by warnings that companies would lay off many people to pay higher wages to some. CBO report finds $15 minimum wage would cost jobs but lower poverty levels |Eli Rosenberg |February 8, 2021 |Washington Post 

He might have, for example, made it de rigueur for large companies to help employees in entry-level jobs work their way into a solid middle-class life, with strong wages and fair benefits. Weekend edition—Jeff Bezos’s unfinished business, pandemic-style Super Bowl, emoji capitalism |Susan Howson |February 6, 2021 |Quartz 

He might have, for example, made Amazon the kind of place that helps employees in entry-level jobs work their way into a solid middle-class life, with strong wages and fair benefits. The unfinished business Jeff Bezos leaves behind at Amazon |Lila MacLellan |February 3, 2021 |Quartz 

So higher wages mean higher spending, which means faster economic growth. What happens after the GameStop bubble bursts? |Peter Kafka |February 2, 2021 |Vox 

Another league executive said the partnership will have a focus on wage equality for female athletes and that 50 percent of revenue going to the players was a big motivator. Women’s Hockey Is Seeing A Sponsorship Boom |Marisa Ingemi |January 26, 2021 |FiveThirtyEight 

He was treated like an immigrant, working for minimum wage, missing his family and having to move on from his musical career. Cuban Hip-Hop Was Born in Alamar |Daniel Levin |December 26, 2014 |DAILY BEAST 

Of course, declining or stagnant wage growth started well before this president took office. Time to Bring Back the Truman Democrats |Joel Kotkin |December 21, 2014 |DAILY BEAST 

Even public-service lawyering jobs, while underpaid for the field, still pay better than low-wage warehouse labor. How Amazon Became Santa’s Sweatshop |Sally Kohn |December 11, 2014 |DAILY BEAST 

The Supreme Court just handed a big holiday present to low-wage workers across America in the form of a giant f*ck you. How Amazon Became Santa’s Sweatshop |Sally Kohn |December 11, 2014 |DAILY BEAST 

Who will want to enter this profession for a poverty wage and little or no paid time off? Care Providers Fight for $15 and a Union |Jasmin Almodovar, Shirley Thompson |December 5, 2014 |DAILY BEAST 

Cincinnatus will not back to his plow, or, at the best, stands sullenly between his plow-handles arguing for a higher wage. The Unsolved Riddle of Social Justice |Stephen Leacock 

If the high wage is paid and the short hours are granted, then the price of the thing made, so it seems, rises higher still. The Unsolved Riddle of Social Justice |Stephen Leacock 

The single employer rightly knows that there is a wage higher than he can pay and hours shorter than he can grant. The Unsolved Riddle of Social Justice |Stephen Leacock 

They wage war as a tribe on account of wrongs done to a private individual. The Jesuit Relations and Allied Documents, Vol. II: Acadia, 1612-1614 |Various 

The minimum wage law ought to form, in one fashion or another, a part of the code of every community. The Unsolved Riddle of Social Justice |Stephen Leacock