Many trail runs, at least in the United States, stay on wide, rolling footpaths that have relatively smooth surfaces, perhaps with an occasional small rock, log, or root to navigate over or around. The Best New Running Shoe for Every Type of Trail |jbeverly |January 13, 2022 |Outside Online 

We waited for a park ranger to vacate the turnout, then crossed the road and scrambled over a granite ledge, where we found an unmarked but well-trodden footpath that wound up the hillside. The Ghost Trail Hunters of Mount Desert Island |lwhelan |July 12, 2021 |Outside Online 

Imagine running on a cement footpath, and then suddenly through dry sand. This Shape-Shifting Robot Can Rearrange Its Body to Walk in New Environments |David Howard |March 25, 2021 |Singularity Hub 

A single winding footpath leads to the grim old gateway, and we rang the bell many times before the custodian admitted us. British Highways And Byways From A Motor Car |Thomas D. Murphy 

He chose the footpath alongside the low wall of the garden, but stopped at the angle where the wild mallow grew. Toilers of the Sea |Victor Hugo 

As we went by the footpath and beside the gate-posts and the unfinished lodge, we kept looking back at the house of my fathers. The Works of Robert Louis Stevenson - Swanston Edition Vol. 10 (of 25) |Robert Louis Stevenson 

This time I turned up the small footpath, of which I have already spoken. Lavengro |George Borrow 

They both turned into the footpath which led to the farm and stile. Stories in Light and Shadow |Bret Harte