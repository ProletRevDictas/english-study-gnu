Bed Bath & Beyond is also refreshing its store brands under Mark Tritton, the private brand guru who came to the company last autumn from Target. Bed Bath & Beyond stock jumps 30% on first sales gain in years |Phil Wahba |October 1, 2020 |Fortune 

Along the way, she'll be joined by a revolving cast of experts, including Scotland Yard detectives, medical examiners, weapons gurus and more. Famous strange demises get a second look in The Curious Life and Death of… |Jennifer Ouellette |October 1, 2020 |Ars Technica 

Catering to the needs of the top tier has caused a steady flow of commissions and fees to rain upon the world’s top private bankers and wealth management gurus. From bailout debacle to global dominance: Inside the turnaround at UBS |Bernhard Warner |September 21, 2020 |Fortune 

There are many aspiring gurus shilling advice, but there’s no single prescription as our jobs and living situations are vastly different. How to design a happy home office |Anne Quito |September 20, 2020 |Quartz 

There are a lot of gurus who are coming at it from a media buying perspective pedaling secret methods that can make you absurd returns and things like that. ‘The dollar amount isn’t worth the mental toll’: Confessions of a media buyer on the pressure to keep performance up amid the pandemic |Kristina Monllos |August 25, 2020 |Digiday 

The guru Rampal is in custody after a deadly battle at his ruined ashram. Is India’s Fallen ‘God-Man’ So Different From a Megachurch Pastor? |Jay Michaelson |November 21, 2014 |DAILY BEAST 

And yes, that authoritarian, patriarchal guru-power structure, invariably, always leads to abuses. Is India’s Fallen ‘God-Man’ So Different From a Megachurch Pastor? |Jay Michaelson |November 21, 2014 |DAILY BEAST 

And while guru literally means “teacher,” in Hindu and Buddhist contexts, it often means much more. Is India’s Fallen ‘God-Man’ So Different From a Megachurch Pastor? |Jay Michaelson |November 21, 2014 |DAILY BEAST 

Point one: It pits a best-selling management guru against a trendy yogurt company. How One Lawsuit Shows What’s Wrong With America |James Poulos |October 20, 2014 |DAILY BEAST 

Because, in what an ethics guru might call a “very real sense,” how is not a brand, it is reality. How One Lawsuit Shows What’s Wrong With America |James Poulos |October 20, 2014 |DAILY BEAST 

With Guru to lead them and point out passages through the swamp, they made speedy time in the boat. The Lost Warship |Robert Moore Williams 

As he hurried out of the ravine he saw Guru hastily helping his mate carry the child to a higher, safer cave. The Lost Warship |Robert Moore Williams 

Craig felt the weirdness of the scene as he and Guru started down the side of the mountain. The Lost Warship |Robert Moore Williams 

After disposing his force and ordering them to get as much rest as possible, Craig and Guru started down to the city of the Ogrum. The Lost Warship |Robert Moore Williams 

"Big rock cave," Guru answered, pointing toward the large stone temple that stood in the center of the city. The Lost Warship |Robert Moore Williams