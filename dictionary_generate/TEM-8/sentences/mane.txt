With tons of options on the market, choosing the right one for your mane can be tricky. The best hot air brushes for any head of hair |Jackson Cooper |September 10, 2021 |Popular-Science 

Yes, the 2003 and 2021 governor recall elections have plenty of similarities, all the way down to Gray Davis and Gavin Newsom both having luxurious manes. Politics Report: What Did the Housing Commission Chief Know and When? |Andrew Keatts |August 14, 2021 |Voice of San Diego 

It has two heat settings, a cooling option, and is capable of handling significant manes. How to choose the best hair dryer for sleek salon-style locks |Florie Korani |July 12, 2021 |Popular-Science 

Literally feathering on different parts of its body, mainly on its back, like a mane perhaps, wherever it was, an “extravagance” in evolutionary terms. T. Rex Was a Slacker - Issue 102: Hidden Truths |Mark MacNamara |July 7, 2021 |Nautilus 

The only woman onstage, she sat at the piano and began to play, her face hidden by a fierce mane that danced as her hands flitted across the keys. The Piano Is Her Weapon of Change in Saudi Arabia |Charu Kasturi |April 30, 2021 |Ozy 

The 25-year-old is tiny, with the paleness of her skin augmented by her fiery mane. The Making of Kiesza: From Navy Sharpshooter to Beauty Queen to Pop Diva |Marlow Stern |October 20, 2014 |DAILY BEAST 

Unlike Cosby, who had only a fringe of gray hair left, he still sported a silver mane. Why Comedians Still Think Bill Cosby Is a Genius |Mark Whitaker |October 5, 2014 |DAILY BEAST 

Pence, with his thick mane of hair and thick build, looks great on a flier. Mike Pence, Dark Horse in Training for 2016 |David Freedlander |June 27, 2014 |DAILY BEAST 

His long black mane, a hipster staple run wild, has grown into legend. Learning To Fear Dodger Pitcher Brian Wilson’s Beard |Sujay Kumar |October 16, 2013 |DAILY BEAST 

First lady Jackie Kennedy would often add a perfectly coiffed swirl of human hair to her own mane for updos and special occasions. Chris Rock to Blame for Obsession With Black Hair and Beyoncé |Allison Samuels |August 12, 2013 |DAILY BEAST 

Those two, Harry and Mary, are exactly alike, except for Harry's curly mane of lion-coloured wig. The Daisy Chain |Charlotte Yonge 

The poor beast has no ears left and his mane is all notched like an old broken comb; but Roger loves him. Child Life In Town And Country |Anatole France 

The civet has nothing in common with the hyna but the glandular pouch, under the tail, and the mane along the neck and back-bone. Buffon's Natural History. Volume VII (of 10) |Georges Louis Leclerc de Buffon 

His white mane and tail were washed and brushed and his red coat fairly shone from the attention given. David Lannarck, Midget |George S. Harney 

Richard rode with his head bent over Rollo's black mane, letting the horse thunder at will at the heels of Marchegai. God Wills It! |William Stearns Davis