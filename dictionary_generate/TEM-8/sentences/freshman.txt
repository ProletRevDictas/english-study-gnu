There’s really no reason to call a freshman senator who had just been sworn in weeks earlier and has no real legislative heft if your intent is to talk about the riot. The important thing about the Mike Lee impeachment trial dust-up |Aaron Blake |February 11, 2021 |Washington Post 

Let’s say a child of mine goes off to college, they’re a freshman, and I want to know how they’re doing, like really how they’re doing — emotionally, academically, socially. Can I Ask You a Ridiculously Personal Question? (Ep. 451) |Stephen J. Dubner |February 11, 2021 |Freakonomics 

A freshman representative with little apparent appetite for policy or coalition-building, Greene wasn’t likely to wield much legislative power in the House. House punishes Republican lawmaker who promoted violent conspiracy theories |Taylor Hatmaker |February 5, 2021 |TechCrunch 

Mendelsohn is a classics professor at Bard College in New York, and when the book opens, his 81-year-old dad, Jay, has just decided to audit his freshman course on The Odyssey. Everything Our Editors Loved in January |The Editors |February 4, 2021 |Outside Online 

In the statement on Greene, first reported by the Hill, McConnell did not mention the freshman lawmaker by name but listed a series of actions that describe her pattern of inflammatory behavior. As House GOP faces decision on its future, McConnell defends Cheney, rebukes Greene in rare set of statements |Felicia Sonmez, Mike DeBonis |February 2, 2021 |Washington Post 

But the show is inoffensively good, which is high praise in a pretty terrible year for freshman TV series. It’s Time to Stop Hating Katherine Heigl |Kevin Fallon |November 17, 2014 |DAILY BEAST 

“There was a lot of stuff that I had never really thought about before,” freshman Thomas Long said. Kirk Cameron Saves Christmas from Abominable Killjoys (Other Christians) |Brandy Zadrozny |November 14, 2014 |DAILY BEAST 

More than 40 percent of incoming freshman said that a campus visit and social activities affected their choice. The Daily Beast College Rankings Methodology |Brandy Zadrozny |November 5, 2014 |DAILY BEAST 

Despite performing in a respectable amount of sketches for a SNL freshman throughout the season, her contract was not renewed. The Casual Genius of Jenny Slate: ‘Marcel the Shell,’ ‘Obvious Child,’ and the Ghost of ‘SNL’ |Kevin Fallon |October 17, 2014 |DAILY BEAST 

“Ted Cruz, Team Player” is a twist few saw coming from the freshman who has made too many enemies to count in Washington. Ted Cruz Quits Screwing With the GOP |Patricia Murphy |October 16, 2014 |DAILY BEAST 

The folks that know it all are the squabs, chuckled Bobby, referring to the freshman class. The Girls of Central High on the Stage |Gertrude W. Morrison 

When I was a freshman, there entered a woman over fifty, with perfectly white hair. Ruth Fielding At College |Alice B. Emerson 

It was, perhaps, the sympathy in her tone that urged the instructor to confide her trouble to a strange girl—a freshman, at that! Ruth Fielding At College |Alice B. Emerson 

The other Briarwood girls were the only members of the freshman class Ruth was likely to be intimate with for some days. Ruth Fielding At College |Alice B. Emerson 

But for a freshman to show sufficient athletic training to make any of the first teams, would almost seem impossible. Ruth Fielding At College |Alice B. Emerson