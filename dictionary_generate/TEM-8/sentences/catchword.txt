The idea that jazz has become a catchword for pomposity is painful for those of us who care deeply about this music. What’s With This Uncool Surge in Jazz Bashing? |Ted Gioia |November 2, 2014 |DAILY BEAST 

Whenever we take up a new idea as a crowd, we at once turn it into a catchword and a fad. The Behavior of Crowds |Everett Dean Martin 

It used to be a catchword of naval correspondents that "submarine cannot fight submarine." The Story of Our Submarines |John Graham Bower 

“Unclean,” he muttered, recalling a catchword of the world he gazed upon. Day and Night Stories |Algernon Blackwood 

Despots obtain their mastery over the crowd by the sword: demagogues by the catchword. The Book of This and That |Robert Lynd 

A man who has done that has seen England--not the name or the map or the rhetorical catchword, but the thing. First and Last |H. Belloc