It won’t solve the problem, but it will sure as hell lower the temperature. American Fringes: How the Media Makes it Worse |Nick Fouriezos |September 7, 2020 |Ozy 

Given the state of misinformation globally, he has a hell of a repair job ahead. He’s Fighting QAnon With Sunlight |Nick Fouriezos |September 6, 2020 |Ozy 

In the second movie, which came out in 1991, Bill and Ted end up on a “bogus journey” that takes them, for whatever reason, to the afterlife — both hell and heaven. The essential kindness of Bill and Ted |Alissa Wilkinson |August 28, 2020 |Vox 

Venus is also very similar to Earth in size and composition, yet somehow it has turned into a hell planet. These Images Expose the Dark Side of the Solar System - Issue 89: The Dark Side |Corey S. Powell |August 26, 2020 |Nautilus 

Doing good and doing well cannot be mutually exclusive for us to have a chance in hell at creating the world that we want to live in. Uncharted Power’s Jessica O. Matthews has a plan to revive America’s crumbling infrastructure |Brooke Henderson |August 23, 2020 |Fortune 

Hell, he says Koenig never referred to it as Serial or even as a podcast. The Deal With Serial’s Jay? He’s Pissed Off, Mucks Up Our Timeline |Emily Shire |December 31, 2014 |DAILY BEAST 

The revelation that, at age 42, Ben Affleck has one hell of an ass. Year of the Butt: How the Booty Changed the World in 2014 |Kevin Fallon |December 30, 2014 |DAILY BEAST 

How the hell does somebody show up at a David Duke organized event in 2002 and claim ignorance? No. 3 Republican Admits Talking to White Supremacist Conference |Tim Mak |December 30, 2014 |DAILY BEAST 

Hell, one of the Dixie Chicks even offered to Uber her balls over to the company. Sony: Hollywood’s Most Subversive Studio Under Attack |Marlow Stern |December 23, 2014 |DAILY BEAST 

And, the Chilbosan would make a hell of a comedy movie; “Fawlty Towers” meets the “Great Dictator.” Inside the ‘Surprisingly Great’ North Korean Hacker Hotel |Michael Daly |December 20, 2014 |DAILY BEAST 

They used to declare that every unbaptised baby would go to Hell and burn for ever in fire and brimstone. God and my Neighbour |Robert Blatchford 

The city hell hounds sprang to meet them and the slaughter of inoffensive Europeans began in Darya Gunj. The Red Year |Louis Tracy 

A gentleman does not call his opponents vipers and consign them to hell, but Jahveh is not under any such obligations. Solomon and Solomonic Literature |Moncure Daniel Conway 

Hell below was in an uproar to meet thee at thy coming, it stirred up the giants for thee. The Bible, Douay-Rheims Version |Various 

To escape from these horrible theories, the Christians (some of them) have thrown over the doctrines of Hell and the Devil. God and my Neighbour |Robert Blatchford