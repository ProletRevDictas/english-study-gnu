Multiple attorneys have since come forward alleging that their clients had been subjected to hysterectomies and other gynecological procedures. A woman in ICE detention says her fallopian tube was removed without her consent |Nicole Narea |September 17, 2020 |Vox 

In August 2019, the environmental groups filed suit against Bluestone, alleging excess discharges of selenium, which can be toxic to fish and other aquatic life. This Billionaire Governor’s Coal Company Might Get a Big Break From His Own Regulators |by Ken Ward Jr. |September 17, 2020 |ProPublica 

In the lawsuit against the city, Taylor’s family alleged that the 26-year-old was not given any medical attention and was left to die in the apartment. The policing reforms in the Breonna Taylor settlement, explained |Fabiola Cineas |September 17, 2020 |Vox 

The push to avoid simulator training led to multiple poor decisions by Boeing, the committee alleged. Boeing crashes were the “horrific culmination” of multiple mistakes, House report says |kdunn6 |September 16, 2020 |Fortune 

They seek to cast doubt on the integrity of the electoral process, asserting that Democrats are using mail balloting to steal the election — “thwarting the will of the American people,” they alleged. Pro-Trump youth group enlists teens in secretive campaign likened to a ‘troll farm,’ prompting rebuke by Facebook and Twitter |Isaac Stanley-Becker |September 15, 2020 |Washington Post 

They also allege their children are not in fact siblings, despite having been told they were. Judge: Rehoming Kids Is Trafficking |Tina Traster |December 30, 2014 |DAILY BEAST 

Was it, as some former employees allege, that the bosses bit off more than they could chew? The Godfather of Right-Wing Radio |Caitlin Dickson |November 23, 2014 |DAILY BEAST 

Police allege the man was in a close relationship with Kakehi and they are investigating the details of his passing. Beware of Japan’s “Black Widows” |Jake Adelstein |November 20, 2014 |DAILY BEAST 

The papers allege that Hayden escorted J.W. down a number of hallways. Rape, Lies & Videotape in Ferguson |Michael Daly |November 18, 2014 |DAILY BEAST 

Army officials also allege that he worked for ethnic rebels as a “communications captain.” Hope and Change? Burma Kills a Journalist Before Obama Arrives |Joshua Carroll |November 11, 2014 |DAILY BEAST 

The English authorities expressly allege a deliberate purpose on Bruce's part to rid himself of his rival. King Robert the Bruce |A. F. Murison 

It was neither age nor sickness, as the chroniclers allege, that prevented King Robert from leading the Weardale foray. King Robert the Bruce |A. F. Murison 

If I am a forger or a friend of forgers, as you allege me to be, then I am unworthy to have served in the uniform of France. The Doctor of Pimlico |William Le Queux 

The emir would bring 500 witnesses to-morrow to establish any crime he was pleased to allege. Fox's Book of Martyrs |John Foxe 

The mere assertion of what he meant to allege must at least delay this hateful marriage. Tristram of Blent |Anthony Hope