Yes, it’s practically a cliché these days, as meditation gets lobbed at us as the cure for everything from overeating to the pain of tough workouts. We’re a Year into COVID. We Still Drink Too Much. |AC Shilton |February 18, 2021 |Outside Online 

I have acted out my favorite athlete cliché, the one in which a player emerges from the offseason claiming to be in the best shape of his life. Tallying up a year of loss: A lot of pounds, too many loved ones, countless connections |Jerry Brewer |December 27, 2020 |Washington Post 

For dancers like Krishnan — who has lived in North America for the past 30 years — it’s still frustrating when he’s “boxed into annoying” clichés that ask him to act out his heritage. No Dirty Dancing: India’s Classical Dancers Break Caste Taboos |Charu Kasturi |December 4, 2020 |Ozy 

One that also would fit right in, as the gear-copy cliché goes, at the bar afterward. The Gear Our Editors Loved in October |The Editors |November 5, 2020 |Outside Online 

At a time when we’ve already exhausted all the clichés about the trials of 2020, Kipchoge losing a marathon seems like further proof that the time is out of joint. Eliud Kipchoge’s Streak Comes to an End in London |Martin Fritz Huber |October 5, 2020 |Outside Online 

Normally, the idea that a work is “timeless” is both praise and empty cliche. A Martian's Eye View of Modern Art |Blake Gopnik |January 27, 2014 |DAILY BEAST 

Or maybe both cultures got off on a glorious combination of cliche and novelty, even if they disagreed on which was which. Chop Suey with Canard à l'Orange |Blake Gopnik |December 30, 2013 |DAILY BEAST 

I'm not sure that artist-chosen shows are of that much curatorial or art-historical use, and anyway they've become a cliche. An Ad Made for Poster-ity |Blake Gopnik |July 9, 2013 |DAILY BEAST 

This is a very different situation that cannot be wished away by an increasingly cliche-ridden "moral defense of capitalism." We Need a Visionary Like Margaret Thatcher for our 21st Century Challenges |David Frum |April 8, 2013 |DAILY BEAST 

I realize how cliche and seemingly insufficient that sounds. I Was Adam Lanza, Part 2 |David Frum |December 22, 2012 |DAILY BEAST 

Von Schlichten described the situation succinctly, in an ancient and unprintable military cliche. Uller Uprising |Henry Beam Piper, John D. Clark and John F. Carr 

The beautiful casts of the French medals known to all electrotypers as Cliche moulds are in the alloy No. 3. Cooley's Cyclopdia of Practical Receipts and Collateral Information in the Arts, Manufactures, Professions, and Trades..., Sixth Edition, Volume I |Arnold Cooley 

As the cliche goes, "With enough eyeballs, all bugs are shallow." Little Brother |Cory Doctorow 

First, Engraved plate or Cliche representing the ornament with which the edge is to be decorated. The Progress of the Marbling Art |Josef Halfer 

Apart from the comfortable cliche in which she was seen enfolded, Sanchia pleased the eye. Rest Harrow |Maurice Hewlett