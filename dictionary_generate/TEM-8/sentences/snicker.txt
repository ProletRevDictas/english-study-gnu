There were plenty of snickers and jokes about who would be the No. Have to pee? Check out new John Waters Restrooms |Staff reports |October 28, 2021 |Washington Blade 

The attitude of the local colleagues at first puzzled us, and then made us snicker in a superior way. The Stacks: H.L. Mencken on the 1904 Baltimore Fire |H.L. Mencken |October 4, 2014 |DAILY BEAST 

Brace yourself, friends, for the new hate-and-snicker-fest on the right about the Obamacare numbers. Obamacare Crosses the Finish Line |Michael Tomasky |March 31, 2014 |DAILY BEAST 

Each time I tried to begin a story, each time I said the word “gay” or “lesbian” or “transgender,” the gendarmes began to snicker. Processing the Murder of Eric Ohena Lembembe |Neela Ghoshal |July 17, 2013 |DAILY BEAST 

He offered little evidence to that, and his testimony was reported to have made some of the jurors snicker. Casey’s Dysfunctional Lawyer |Diane Dimond |June 27, 2011 |DAILY BEAST 

“It sounds horrible,” Hef says on the phone from the Mansion in Los Angeles, punctuating his sarcasm with a snicker. Getting a Rise Out of Hef |Lloyd Grove |January 4, 2011 |DAILY BEAST 

Enslee began to snicker again, taking some support in his shame from another man's disgrace. What Will People Say? |Rupert Hughes 

Jest tell her there's more Smithses wanted an' she'll leave the Greenses 'thout a snicker.' The Orpheus C. Kerr Papers. Series 1 |Robert H. Newell 

A gratified snore from Dee and Miss Cox with a little snicker went to her room. At Boarding School with the Tucker Twins |Nell Speed 

The snicker grew to a laugh—a laugh with a thread of grim menace in it, and a tinge of mounting man-hysteria. Back Home |Irvin S. Cobb 

You see, I'm goin' to croak 'fore long—oh, you don't need to snicker; 't's a fact. Tramping with Tramps |Josiah Flynt