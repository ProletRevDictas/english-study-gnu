Sometimes you swallow pills you don’t like to get things done. TheDonald’s owner speaks out on why he finally pulled plug on hate-filled site |Craig Timberg, Drew Harwell |February 5, 2021 |Washington Post 

Body temperature can be monitored through smart pills but also through wearables put on the skin. As biometrics boom, who owns athletes’ data? It depends on the sport. |Nick Busca |February 2, 2021 |Washington Post 

He gave her 10 pills each day, in addition to a few liquid medications. A sick mother, a loving son, a signed Ovechkin jersey — and how the Caps tried to help |Kyle Melnick |February 1, 2021 |Washington Post 

They take the pills for 15 days and log symptoms on a web-based platform. The antidepressant fluvoxamine could keep mild COVID-19 from worsening |Esther Landhuis |February 1, 2021 |Science News 

Maybe 15 or 20 percent of lung cancers in the United States are targeted by these pills that are quite effective and not very toxic. How to Fix the Incentives in Cancer Research (Ep. 449) |Stephen J. Dubner |January 28, 2021 |Freakonomics 

For Randy, a 50-year-old ex-Mormon gay man, this cure was a particularly bitter pill to swallow. Your Husband Is Definitely Gay: TLC’s Painful Portrait of Mormonism |Samantha Allen |January 1, 2015 |DAILY BEAST 

“He gave me a blue pill, which he said was an antihistamine,” said Chelan. Two New Bill Cosby Accusers Come Forward: ‘We Challenge Mr. Cosby to End This Nightmare’ |Marlow Stern |December 3, 2014 |DAILY BEAST 

Medication can now be taken in a single pill rather than a complex cocktail of tablets. The New Face of HIV Is Gay & Young |Adebisi Alimi |December 1, 2014 |DAILY BEAST 

A plastic surgeon gave her a supposedly lethal pill that also failed. The Nurse Coaching People Through Death by Starvation |Nick Tabor |November 17, 2014 |DAILY BEAST 

For the Times, which had won four Pulitzer Prizes in 2013, the Snowden slip-up was a bitter pill to swallow. Is The Guardian Holding Back The New York Times’ Snowden Stories? |Lloyd Grove |October 14, 2014 |DAILY BEAST 

However cleverly the pill was gilded, the Marshal knew that it was the Emperor's distrust which had lost him the command. Napoleon's Marshals |R. P. Dunn-Pattison 

Some people swallow the universe like a pill; they travel on through the world, like smiling images pushed from behind. The Pocket R.L.S. |Robert Louis Stevenson 

Thence to my office, and after several letters writ, home to supper and to bed, and took a pill. Diary of Samuel Pepys, Complete |Samuel Pepys 

That was a good initial effort, running down the opium pill mail-order enterprise. Average Jones |Samuel Hopkins Adams 

But on setting down the cup his eye caught sight of the pill-box. Stories in Light and Shadow |Bret Harte