“It was ludicrous,” she said of the piece, which ran in June. Former Pence aide says she will vote for Biden because of Trump’s ‘flat out disregard for human life’ during pandemic |Josh Dawsey |September 17, 2020 |Washington Post 

Likewise, I may update this piece as more information becomes available. Want to fight climate change effectively? Here’s where to donate your money. |Sigal Samuel |September 17, 2020 |Vox 

You start with a theory of small pieces, say the atoms in a billiard ball. How Mathematical ‘Hocus-Pocus’ Saved Particle Physics |Charlie Wood |September 17, 2020 |Quanta Magazine 

What to remember when casting your ballotThe most important piece of advice you’ll need when voting, early or not, is to follow directions carefully. Why you should vote as early as possible (and how to do it) |John Kennedy |September 17, 2020 |Popular-Science 

So some of the people I spoke to while reporting this piece explained that there are some inconsistencies between the decree that brought in this database and the data sharing and the new data law, the LGPD. Podcast: COVID-19 is helping turn Brazil into a surveillance state |Anthony Green |September 16, 2020 |MIT Technology Review 

The well, ghost or no ghost, is certainly a piece of history with a bold presence. New York’s Most Tragic Ghost Loves Minimalist Swedish Fashion |Nina Strochlic |January 8, 2015 |DAILY BEAST 

My doctor insisted that once I filed this piece I lie down on my bed and not get out. I Tried to Warn You About Sleazy Billionaire Jeffrey Epstein in 2003 |Vicky Ward |January 7, 2015 |DAILY BEAST 

Last March they gave Airbus a huge piece of new business, ordering 169 A320s and 65 of the slightly larger A321. Annoying Airport Delays Might Prevent You From Becoming the Next AirAsia 8501 |Clive Irving |January 6, 2015 |DAILY BEAST 

The attempt to “breed back” the Auroch of Teutonic legend was of a piece with the Nazi obsession with racial purity and eugenics. ‘Nazi Cows’ Tried to Kill British Farmer |Tom Sykes |January 6, 2015 |DAILY BEAST 

However, an article designed to act as a tie-in to the piece has been published as planned in the BBC magazine Radio Times. Pulled Documentary Says William Felt ‘Used’ by Charles’ Push for Camilla |Tom Sykes |December 30, 2014 |DAILY BEAST 

I assure you, no matter how beautifully we play any piece, the minute Liszt plays it, you would scarcely recognize it! Music-Study in Germany |Amy Fay 

I was busy loading the piece when an exclamation of surprise from one of the men made me look up. Blackwood's Edinburgh Magazine, No. CCCXXXIX. January, 1844. Vol. LV. |Various 

There were two battalions, together about a thousand men; and they brought a field-piece with them. Blackwood's Edinburgh Magazine, No. CCCXXXIX. January, 1844. Vol. LV. |Various 

She got up and stood in front of the fire, having her hand on the chimney-piece and looking down at the blaze. Confidence |Henry James 

With a groan, wrung from the very depths of his heart, he tossed the man a gold-piece; another to the woman. Ramona |Helen Hunt Jackson