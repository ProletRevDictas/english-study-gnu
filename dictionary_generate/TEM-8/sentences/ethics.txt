Will Huntsberry reports that as part of her probation she’ll also be required to take a medical ethics course. Morning Report: Hotel Workers Want Their Jobs Back |Voice of San Diego |September 8, 2020 |Voice of San Diego 

It is yet another potential violation of the city’s ethics rules. Barrios Was Paid by Union While Working for Council President |Andrew Keatts and Jesse Marx |September 2, 2020 |Voice of San Diego 

Barrios stressed in the new filing that “no conflicts arose” during his period of overlapping employment, but he could have run afoul of the city’s ethics ordinance anyway. Barrios Was Paid by Union While Working for Council President |Andrew Keatts and Jesse Marx |September 2, 2020 |Voice of San Diego 

This is, of course, nothing new, but it has highlighted the need for international standards on ethics and reporting mechanisms that are flexible but responsive. Why South Asia’s COVID-19 Numbers Are So Low (For Now) |Puja Changoiwala |June 23, 2020 |Quanta Magazine 

Anya’s been thinking a lot about Disney princesses lately because of a writing project she had in school, for ethics class. Does Hollywood Still Have a Princess Problem? (Ep. 394) |Stephen J. Dubner |October 24, 2019 |Freakonomics 

Fridays there is ethics and law of war training and instruction. Pentagon Insider on New Plan to Fight ISIS: ‘Of Course It’s Not Enough’ |Nancy A. Youssef |January 6, 2015 |DAILY BEAST 

Tom Rust, a spokesman for the House Ethics Committee, declined to comment to The Daily Beast. The Felon Who Wouldn’t Leave Congress |Ben Jacobs, David Freedlander |December 23, 2014 |DAILY BEAST 

Still, his conviction will restart a House Ethics Committee investigation into his actions. The Felon Who Wouldn’t Leave Congress |Ben Jacobs, David Freedlander |December 23, 2014 |DAILY BEAST 

Whether or not Hippocrates ever actually said “First, do no harm,” the axiom is central to medical ethics. Why So Many Surgeons Are Psychos |Russell Saunders |December 17, 2014 |DAILY BEAST 

Arthur Caplan is the director of medical ethics for NYU Langone Medical Center. U.K. Courts Grant Mother Right to End Her 12-Year-Old Disabled Daughter’s Life |Elizabeth Picciuto |November 4, 2014 |DAILY BEAST 

She was just as honestintentionallyas she could be, but the ethics of business dealing were not quite straight in her mind. The Girls of Central High on the Stage |Gertrude W. Morrison 

The religion of Rome may not have advanced the theology or the ethics of the world, but it made and held together a nation. The Religion of Ancient Rome |Cyril Bailey 

Your religion does not make it—its ethics are too weak, its theories too unsound, its transcendentalism is too thin. God and my Neighbour |Robert Blatchford 

Impatiently I smother the accusing whisper of my conscience, "By the right of revolutionary ethics." Prison Memoirs of an Anarchist |Alexander Berkman 

Ethics, in short, may be regarded as composed of unlike halves, which unite centrally to form a whole. Man And His Ancestor |Charles Morris