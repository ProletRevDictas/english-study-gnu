The second shortened the amount of time laid-off employees have to decide whether they want to come back. Morning Report: A Questionable Stat That’s Guiding Reopening |Voice of San Diego |September 9, 2020 |Voice of San Diego 

Advertisers followed through on their plans to negotiate options to cancel a higher amount of their committed ad dollars and to shorten the deadlines for when cancelation requests need to be submitted before a quarter begins. ‘There wasn’t a huge shift’: TV upfront market did not undergo expected overhaul this year |Tim Peterson |September 9, 2020 |Digiday 

The tools Cleveland uses are meant in part to shorten the throwing motion. Cleveland’s League-Leading Rotation Relies On Homegrown Talent … From A Single Draft |Travis Sawchik |August 25, 2020 |FiveThirtyEight 

Response times shortened vastly as Mayo Clinic and these partners focused on an immediate shared goal. COVID-19 has spurred rapid transformation in health care. Let’s make sure it stays that way |jakemeth |August 20, 2020 |Fortune 

The 20th amendment was ratified to shorten the period between the presidential election in November and the inauguration, which had been in March. Trump Can’t Postpone The Election, But He Can Delegitimize The Results. |Sarah Frostenson (sarah.frostenson@abc.com) |July 30, 2020 |FiveThirtyEight 

“In some cases, it has taken 15 days to report the deaths and circumstances, which I would like to shorten to 48 hours,” she said. Did This Flu Vaccine Kill 13? |Barbie Latza Nadeau |December 2, 2014 |DAILY BEAST 

We have to shorten the distance between inevitable and inconceivable. Square Deal, New Deal, and Now, From Hillary Clinton, a “Fair Shot” |Eleanor Clift |September 19, 2014 |DAILY BEAST 

Just like nobody is going to tell David O. Russell to shorten American Hustle (138 minutes). Our Pop Culture Wish List for 2014 |Kevin Fallon |December 30, 2013 |DAILY BEAST 

A new study says living in the South will shorten your life. Study Proves Southern States Have Lowest Life Expectancy |Eliza Shapiro |July 20, 2013 |DAILY BEAST 

We have to overturn Citizens United and shorten the campaign season. David Stockman on ‘The Great Deformation’ and Our Economic Doom |Daniel Gross |April 1, 2013 |DAILY BEAST 

The law still branded as conspiracy any united attempt of workingmen to raise wages or to shorten the hours of work. The Unsolved Riddle of Social Justice |Stephen Leacock 

If your neighbor seems disposed to shorten the time by conversing, do not be too hasty in checking him. The Ladies' Book of Etiquette, and Manual of Politeness |Florence Hartley 

In spite, however, of the strong wind, neither the smugglers nor those on board the cruiser appeared inclined to shorten sail. Digby Heathcote |W.H.G. Kingston 

On the 15th of May Mr. Tennyson submitted a motion for leave to introduce a bill to shorten the duration of parliaments. The History of England in Three Volumes, Vol.III. |E. Farr and E. H. Nolan 

The committee decided to run up signal to shorten course and conclude at the first round. Yachting Vol. 2 |Various.