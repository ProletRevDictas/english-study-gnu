Other bacteria can access this carbon by “eating” their hydrogen-powered neighbors or the carbon-rich ooze they produce. How Antarctic Bacteria Live on Air and Use Hydrogen as Fuel to Make Water |Pok Man Leung |November 19, 2021 |Singularity Hub 

Houston’s DJ Screw changed the trajectory of rap by famously slowing records down, loosening the tempo until it melted into a beautiful ooze — so it makes sense that a fast remix does something like the opposite. Florida’s ‘fast’ rap remixes are speeding up another endless summer |Chris Richards |July 22, 2021 |Washington Post 

Curiously, even the dark meat does not ooze rivers of juice when you bite it. Charlottesville Is Swimming in Finger Lickin’ Gas Station Fried Chicken |Jane & Michael Stern |May 26, 2014 |DAILY BEAST 

No, this is about which conservative leaders ooze a stereotypical, gut-level manliness. Squishes, Step Aside: Ted Cruz and Chris Christie’s Old-School Manliness |Michelle Cottle |May 13, 2013 |DAILY BEAST 

There they vanish, their fine tones never to be tried more, and ooze through the red-hot ruin, "Hush-sh-sht!" History Of Friedrich II. of Prussia, Vol. VII. (of XXI.) |Thomas Carlyle 

And now, for fear my courage will ooze out, I must tell you quickly. The Rose of Old St. Louis |Mary Dillon 

He lifted a pseudopod from primordial ooze, and the pseudopod was him. The Status Civilization |Robert Sheckley 

"They thought they could prick us like that, and let the life ooze out," said the doctor. Pharaoh's Broker |Ellsworth Douglass 

Depend on it, the story will ooze out, you are so well known, and so much visited now. Harper's New Monthly Magazine, No. VII, December 1850, Vol. II |Various