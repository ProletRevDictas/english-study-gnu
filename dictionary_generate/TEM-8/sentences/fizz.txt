Both players were important at the start, injecting fizz into an urgent cause. D.C. United makes an early goal stand up, collects a crucial win over the Red Bulls |Steven Goff |October 28, 2021 |Washington Post 

For open bottles of bubbly, go for a sparkling wine-specific topper, as they can help the fizz go on for up to two days instead of 24 hours. How to keep wine fresh after opening it |Sandra Gutierrez |August 24, 2021 |Popular-Science 

Hike to Bumpass Hell from Cold Boiling Lake—where bubbles rise like soda fizz—along a five-mile trail. 15 National Park Trails to Add to Your Bucket List |elessard |August 6, 2021 |Outside Online 

There is also pétillant-naturel, a rebirth of an old-style sparkling wine that simply completes fermentation in the bottle to capture some carbon dioxide and create a mild fizz. Chillable reds and low-alcohol pours: Winemakers try to bottle success with younger consumers |Dave McIntyre |May 7, 2021 |Washington Post 

We also have two more kosher wines for Passover — a rich, spicy red from Israel and a refreshing fizz from South Africa. Usher in spring with this delightfully crisp sauvignon blanc that costs just $14 |Dave McIntyre |March 19, 2021 |Washington Post 

The bubbles still came out, but some of the fizz was getting lost. Font of Invention | |September 18, 2014 |DAILY BEAST 

But the fizz in Brazil and Turkey has yet to go flat, and the excitement and turmoil may well continue to spread across the globe. You Say You Want a Revolution? |Christopher Dickey |June 23, 2013 |DAILY BEAST 

Summer is here, people are thinking about vacation, and the fizz has fizzled. You Say You Want a Revolution? |Christopher Dickey |June 23, 2013 |DAILY BEAST 

The Tea Party in the United States still operates within the system, but its base is full of fizz and is a political force. You Say You Want a Revolution? |Christopher Dickey |June 23, 2013 |DAILY BEAST 

“This is a take on a Ramos Gin Fizz but done as a ‘bomb,’” Maloney explains. Lights, Camera, Cocktails |Brody Brown |August 13, 2011 |DAILY BEAST 

I shall give 'em real turtle from Birch's, and as for fizz, they shall swim in it if they like. The Pit Town Coronet, Volume II (of 3) |Charles James Wills 

There's nothing like fizz, it makes 'em all so friendly; and as for music, I've secured Toot and Kinney. The Pit Town Coronet, Volume II (of 3) |Charles James Wills 

The fizz water must have something of the sort of thing that old Ponce de Leon sought. My Wonderful Visit |Charlie Chaplin 

So your Seltzer water doesn't pour fast enough to fizz outside the bottle, and your heart is sad. Somehow Good |William de Morgan 

Silver Fizz rushed in, hoping to stop him, Morris and Hank closely following his lead. The Empty House And Other Ghost Stories |Algernon Blackwood