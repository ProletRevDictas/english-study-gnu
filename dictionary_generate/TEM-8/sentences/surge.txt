For all its benefits, the much-vaunted surge in digital advertising has come at a cost. ‘Chronically understaffed’: Confessions of an agency exec on the cost of online advertising’s Covid growth |Seb Joseph |February 11, 2021 |Digiday 

There was a “big surge of reader interest with the pandemic and the election,” Pasick said. The New York Times aims to convert newsletter readers into paid subscribers as The Morning newsletter tops 1 billion opens |Sara Guaglione |February 10, 2021 |Digiday 

Several users Recode spoke to said that in recent weeks, the platform has seen a surge in people desperate for more information on where and how to get vaccinated, and neighbors trading information about how to find an available dose. How the Covid-19 pandemic broke Nextdoor |Rebecca Heilweil |February 9, 2021 |Vox 

There might only be six to 12 weeks before the new variant triggers another surge. A virus locomotive is heading straight at us |Editorial Board |February 8, 2021 |Washington Post 

This pattern suggests there is more to the story than retail investors buying shares and holding them through the stock surge, said Shapiro, the Georgetown policy fellow. How the rich got richer: Reddit trading frenzy benefited Wall Street elite |Douglas MacMillan, Yeganeh Torbati |February 8, 2021 |Washington Post 

The U.S. launched campaigns in the restive Iraqi city of Fallujah and a surge campaign in Baghdad. Pentagon Doesn’t Know How Many People It’s Killed in the ISIS War |Nancy A. Youssef |January 7, 2015 |DAILY BEAST 

“People are generally diplomatic,” says Steinbrick of regulars dealing with the surge of new faces. How to Survive the New Year ‘Gympocalypse’ |Tim Teeman |January 6, 2015 |DAILY BEAST 

The quality of the music is a major factor in this recent surge. Down With the King: Christianity Isn’t Hiding in Rap’s Closet |Stereo Williams |December 28, 2014 |DAILY BEAST 

“When I first met her I felt this tremendous surge of power,” he explained. Dungeons and Genital Clamps: Inside a Legendary BDSM Chateau |Ian Frisch |December 20, 2014 |DAILY BEAST 

Uber responded to the PR nightmare by reversing the surge, refunding those affected, and doling out free rides. In Defense of Uber’s Awful Sydney Surge Pricing |Olivia Nuzzi |December 16, 2014 |DAILY BEAST 

It was a question whether the mutineers would not surge over it in triumph within the hour. The Red Year |Louis Tracy 

A fearsome struggle would surge around that tower where the British flag was flying. The Red Year |Louis Tracy 

But meanwhile the keeper has shouted for a fresh set of bears, who surge wildly into the room. Punch, or the London Charivari, Volume 158, April 28, 1920 |Various 

Joe saw her slim against the light, and his thoughts were like the sea surge, wild, unruly. The Man from Time |Frank Belknap Long 

At one hundred feet in depth, the passage of the surge would be strong enough to urge considerable pebbles before it. Outlines of the Earth's History |Nathaniel Southgate Shaler