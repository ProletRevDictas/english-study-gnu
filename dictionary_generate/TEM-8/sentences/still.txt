Domestically, the prime minister maintains the dubious line that he is the only man who can keep the still-fragile peace. Cambodia’s Smoke-and-Mirrors Democracy |David Shaftel |January 9, 2015 |DAILY BEAST 

Genetics alone does not an eating disorder make, generally speaking, and Bulik points out that environment still plays a role. How Skinny Is Too Skinny? Israel Bans ‘Underweight’ Models |Carrie Arnold |January 8, 2015 |DAILY BEAST 

Many young people are still shedding the ignorance of our parents. ‘Empire’ Review: Hip-Hop Musical Chairs with an Insane Soap Opera Twist |Judnick Mayard |January 8, 2015 |DAILY BEAST 

Drugeon survived an airstrike last year and is believed to be still at large, officials have said. U.S. Spies See Al Qaeda Fingerprints on Paris Massacre |Shane Harris, Nancy A. Youssef |January 8, 2015 |DAILY BEAST 

That fantasy, however, is still heavily regimented by all sorts of norms. ‘Empire’ Review: Hip-Hop Musical Chairs with an Insane Soap Opera Twist |Judnick Mayard |January 8, 2015 |DAILY BEAST 

Joe looked at her with a smile, his face still solemn and serious for all its youth and the fires of new-lit hope behind his eyes. The Bondboy |George W. (George Washington) Ogden 

The aged woman made no reply; her eyes still studied Ramona's face, and she still held her hand. Ramona |Helen Hunt Jackson 

As there are still many varieties of the plant grown in America, so there doubtless was when cultivated by the Indians. Tobacco; Its History, Varieties, Culture, Manufacture and Commerce |E. R. Billings. 

"Better so," was the Senora's sole reply; and she fell again into still deeper, more perplexed thought about the hidden treasure. Ramona |Helen Hunt Jackson 

Few people, I think, realize that, and fewer still realize the reasonable consequences of that. The Salvaging Of Civilisation |H. G. (Herbert George) Wells