The blue-chip recruits that routinely came to the school will no longer matriculate. Joe Paterno Was a Dictator: Penn State Deserved Its Punishment |Buzz Bissinger |July 24, 2012 |DAILY BEAST 

He said that I had better come up to matriculate next term, but should not have another examination. Life of John Coleridge Patteson |Charlotte M. Yonge 

I had presently to arrange a holiday and go to London to matriculate, and so it was I came upon my aunt and uncle again. Tono Bungay |H. G. Wells 

Baden was the first German State that allowed women to matriculate at its universities. Home Life in Germany |Mrs. Alfred Sidgwick 

He received me with profound courtesy and feigned respect, but was staggered at my request to matriculate. The Woman-Hater |Charles Reade 

We were admitted to matriculate and study medicine, under certain conditions, to which I beg your attention. The Woman-Hater |Charles Reade