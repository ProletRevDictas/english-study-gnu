Early orbital reconnaissance of Cerberus Fossae suggested that some sort of fluid washed over the area not too long ago in geologic terms. Rumbles on Mars Raise Hopes of Underground Magma Flows |Robin George Andrews |February 1, 2021 |Quanta Magazine 

The Cessna was the perfect vehicle for a casual reconnaissance mission. How One Man Survived a Plane Crash and 5 Days in the Snowy Canadian Wilderness—and Went On to Help Shape the Modern Ski Industry |Cassidy Randall |December 27, 2020 |Time 

While the Air Force had tried to couch the recent demonstration as being about reconnaissance, in the training exercise that reconnaissance helped select targets for a missile strike. Air Force A.I. test raises concerns over killer robots |Jeremy Kahn |December 21, 2020 |Fortune 

Its modular framework excels at gaining powerful administrator privileges, spreading rapidly from computer to computer in networks and performing reconnaissance that identifies infected computers belonging to high-value targets. One of the Internet’s most aggressive threats could take UEFI malware mainstream |Dan Goodin |December 3, 2020 |Ars Technica 

The company wanted to demonstrate the drone’s ability to conduct maritime intelligence, surveillance and reconnaissance missions. Drone Test Flight Moved Out of San Diego Following Safety Concerns |Jesse Marx |October 26, 2020 |Voice of San Diego 

In a Lynx, however, Harry could take part in reconnaissance missions and transport passengers. Can Harry Bury the Party Prince? |Tom Sykes |December 2, 2014 |DAILY BEAST 

The New York Times reported that Iran has deployed reconnaissance drones and military equipment to the Iraqi army. Inside Iran’s Fling With The U.S. |IranWire |July 4, 2014 |DAILY BEAST 

This was a tethered reconnaissance balloon, as first used 220 years ago in the French Revolutionary War. Why Old-School Airships Now Rule Our Warzones |Bill Sweetman |June 30, 2014 |DAILY BEAST 

On August 18, an IED killed Private First Class Morris Walker and Staff Sergeant Clayton Bowen during a reconnaissance mission. We Lost Soldiers in the Hunt for Bergdahl, a Guy Who Walked Off in the Dead of Night |Nathan Bradley Bethea |June 2, 2014 |DAILY BEAST 

ASW assets and crews have been diverted to reconnaissance missions in overland and littoral wars. Tomorrow’s Stealthy Subs Could Sink America’s Navy |Bill Sweetman |May 12, 2014 |DAILY BEAST 

At the date of Rose's first reconnaissance to this cellar, these cells were vacant and unguarded. Famous Adventures And Prison Escapes of the Civil War |Various 

Finally, to conclude this preliminary reconnaissance, the attitude of Socialism to religion is wholly unjustifiable. The Inhumanity of Socialism |Edward F. Adams 

It appears somewhat strange that no reconnaissance was made of the Russian position by the generals. The British Expedition to the Crimea |William Howard Russell 

During the diplomatic reconnaissance led by Caulaincourt, the statesmen of these countries had been busy at Fontainebleau. The Life of Napoleon Bonaparte |William Milligan Sloane 

During the various expeditions of our reconnaissance I came to employ two distinct methods of working the legs with the lungs. Mount Everest the Reconnaissance, 1921 |Charles Kenneth Howard-Bury