The executive then sends out an abort signal to the brain’s memory center, the hippocampus. The Brain Has a Built-in System to Keep Unwanted Memories Out, Study Finds |Shelly Fan |May 3, 2022 |Singularity Hub 

If the NFT thinks the TAGSAM arm is coming down on something dangerous flagged by the hazard map, it will automatically execute an abort burn that moves the spacecraft up and away from the surface. A NASA spacecraft is about to scoop up some asteroid rubble |Neel Patel |October 14, 2020 |MIT Technology Review 

Because the abort was triggered just before the rocket's main RS-68 engines had begun to ignite, the delay before the next launch attempt may be less than a week. Delta IV Heavy scrubs again, ULA chief vows to change readiness operations |Eric Berger |October 1, 2020 |Ars Technica 

Ellen makes her choice, and she throws herself down the stairs to abort the baby. Why Deny the Obvious: Hollywood’s Backward Stance on Abortion |Teo Bugbee |June 10, 2014 |DAILY BEAST 

They abort their young children, they put their young men in jail, because they never learned how to pick cotton. What Cliven Bundy’s Famous Backers Said, Before and After |Olivia Nuzzi |April 25, 2014 |DAILY BEAST 

Devastated, she climbed with feral intensity, hoping the baby might spontaneously abort. Breaking Mount Everest’s Glass Ceiling |Amanda Padoan, Peter Zuckerman |March 30, 2014 |DAILY BEAST 

Well, Sarah Palin is travelling across the country alerting people that “angry atheists” want to “abort Christ from Christmas.” It’s Conservatives Who Really Want Christ Out of Christmas |Dean Obeidallah |December 17, 2013 |DAILY BEAST 

All of a sudden, Mission Control (voiced by Ed Harris, paying homage to Apollo 13) orders the team to abort immediately. Alfonso Cuarón On His Spellbinding Sci-Fi Film ‘Gravity,’ Starring Sandra Bullock and George Clooney |Marlow Stern |September 1, 2013 |DAILY BEAST 

I've tried my best to abort this big bug, but I can't find anything amiss. Tight Squeeze |Dean Charles Ing 

After we took off and headed for Europe across the Channel there would usually be someone who would abort the mission. The Biography of a Rabbit |Roy Benson 

Some appearances make me think that they abort by becoming confluent with the main petiole. More Letters of Charles Darwin Volume II |Charles Darwin 

Continuous application of equal parts of alcohol and water night and day may abort it. The Eugenic Marriage, Volume IV. (of IV.) |Grant Hague 

The bruised leaves are used locally in painful affections of the joints and to abort syphilitic buboes and abscesses of all kinds. The Medicinal Plants of the Philippines |T. H. Pardo de Tavera