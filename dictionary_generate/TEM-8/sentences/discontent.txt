The United States, like many countries around the world, seems to have entered a period of broad and deep discontent. Does Anyone Really Know What Socialism Is? (Ep. 408 Rebroadcast) |Stephen J. Dubner |September 17, 2020 |Freakonomics 

Foreign actors, from Russia to China, can take advantage of social unrest, the digital age and human psychology to stir discontent — and potentially affect elections. The American Fringes Go Mainstream |Nick Fouriezos |September 6, 2020 |Ozy 

The US has been brought to this point by a long-term legitimacy crisis of the American elite, accompanied by rising levels of mass discontent and coercive state responses. George Floyd Protests Show How The US Has Retreated From Its Position As A World Leader |LGBTQ-Editor |June 9, 2020 |No Straight News 

Much of this discontent is related to economic issues — some of them specific, like wage stagnation and the spike in healthcare and college costs. Does Anyone Really Know What Socialism Is? (Ep. 408) |Stephen J. Dubner |March 5, 2020 |Freakonomics 

Now that he’s facing re-election, his Democratic opponents are trying to direct the discontent in their direction, with some of them calling to fundamentally remake the American economy. Does Anyone Really Know What Socialism Is? (Ep. 408) |Stephen J. Dubner |March 5, 2020 |Freakonomics 

Almost from the beginning, there have been rumblings of discontent about Pope Francis. Is the Pope Catholic? Critics Rally Around Benedict As Talk of Schism Looms |Candida Moss, Joel Baden |November 2, 2014 |DAILY BEAST 

Discontent with life led to complete recklessness on the battlefield. The Woman Stuck in a Navy SEAL's Body |Nina Strochlic |September 4, 2014 |DAILY BEAST 

But in the Palestinian camps, there has been public discontent, too. Inside the Kerry-Israel Meltdown |Josh Rogin, Eli Lake |July 29, 2014 |DAILY BEAST 

His new documentary, A Cinema of Discontent, is about film censorship in Iran. Jafar Panahi: Filmmaking Ban Is My Iranian Prison |Jamsheed Akrami |July 8, 2014 |DAILY BEAST 

Hamas exploits the resulting Arab discontent to fan the flames of violence and war. Israelis and Arabs Shaken by the Aftershock of Teen Murders |Miranda Frum |July 7, 2014 |DAILY BEAST 

The seed of discontent was again germinating under the duplicity of the Spanish lay and clerical authorities. The Philippine Islands |John Foreman 

The new forces controlled by mankind have been powerless as yet to remove want and destitution, hard work and social discontent. The Unsolved Riddle of Social Justice |Stephen Leacock 

Discontent was so widespread that the new general at once ordered all troops, save some three thousand, to leave the capital. Napoleon's Marshals |R. P. Dunn-Pattison 

The vision itself is an outcome of that divine discontent which raises man above his environment. The Unsolved Riddle of Social Justice |Stephen Leacock 

Lannes secretly informed Bonaparte of the plans of those who led the discontent, and, in the words of Murat, "sold the cocoanut." Napoleon's Marshals |R. P. Dunn-Pattison