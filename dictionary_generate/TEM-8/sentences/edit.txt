Murphy replied that he wouldn’t allow any edits to the assessment that altered the intelligence. What to make of the DHS whistleblower’s shocking complaint |Alex Ward |September 11, 2020 |Vox 

Only a small proportion of edits from Wikipedia are potential vandalism, and we’ve improved our systems to now detect 99 percent of those cases. Google now uses BERT to match stories with fact checks |Barry Schwartz |September 10, 2020 |Search Engine Land 

If you need to make any edits, just open up your 1Password account on the web or on your phone. How to get started using a password manager |David Nield |September 8, 2020 |Popular-Science 

Those edits are in adult cells and can’t be carried into future generations. Strict new guidelines lay out a path to heritable human gene editing |Tina Hesman Saey |September 3, 2020 |Science News 

For example, the launch of NowThis Kids stemmed from the whitespace that the edit team saw for a “co-viewing” platform for parents and children that helped to answer some of the more complicated questions kids have around current events. ‘Not a simple adjacency strategy’: How Group Nine is selling advertisers on bigger and longer editorial deals |Kayleigh Barber |August 21, 2020 |Digiday 

Being there teaches you to think quickly, edit yourself, and not get too precious about your own work. Coffee Talk with Fred Armisen: On ‘Portlandia,’ Meeting Obama, and Taylor Swift’s Greatness |Marlow Stern |January 7, 2015 |DAILY BEAST 

We just saw an edit of one called, “Doug Becomes A Feminist,” and I just really enjoyed watching it. Coffee Talk with Fred Armisen: On ‘Portlandia,’ Meeting Obama, and Taylor Swift’s Greatness |Marlow Stern |January 7, 2015 |DAILY BEAST 

But after a while, the edit wars ended, and the article no longer had Einstein going to Albania. You Can Look It Up: The Wikipedia Story |Walter Isaacson |October 19, 2014 |DAILY BEAST 

WardsWiki (as it became known) allowed anyone to edit and contribute, without even needing a password. You Can Look It Up: The Wikipedia Story |Walter Isaacson |October 19, 2014 |DAILY BEAST 

The result was a new content management application that allowed users to edit and contribute to a Web page. You Can Look It Up: The Wikipedia Story |Walter Isaacson |October 19, 2014 |DAILY BEAST 

He showed how the state might print and bind and distribute, while men in "free associations" might edit and publish. Love's Pilgrimage |Upton Sinclair 

Only I wonder why you edit his book if it's like that, you know. Tristram of Blent |Anthony Hope 

He would edit a paper, comprehensive in its scope, and liberal in its views. Mr. Opp |Alice Hegan Rice 

There are persons, indeed, who would like to edit such songs and stories especially for the use of children. The Sexual Life of the Child |Albert Moll 

I had hoped to meet him some day, to draw out his confidences, perchance to edit his memoirs. Punch, or the London Charivari, Vol. 147, September 2nd, 1914 |Various