Holding a Guinness World Record as the planet’s most prolific professional blogger, Murph has been working from home for nearly his entire professional career. How the COO of Zoom is handling the responsibility of powering work from home |Brett Haensel |October 1, 2020 |Fortune 

That means the county can cut other kinds of gases, like methane from landfills, but it’s all measured against carbon dioxide, the most prolific greenhouse gas. Environment Report: County Celebrates Then Sinks Its Climate Plan |MacKenzie Elmer |September 28, 2020 |Voice of San Diego 

Studies pointed out the bias toward temperate zones in previous work, and indicated that in the tropics, females of many species are prolific singers. Few people knew female birds had unique songs—until women started studying them |By Omland, Rose & Odom/The Conversation |September 28, 2020 |Popular-Science 

I would be hard pressed to think of any author more prolific than Baggott when it comes to quantum mechanics. Your Guide to the Many Meanings of Quantum Mechanics - Facts So Romantic |Sabine Hossenfelder |September 3, 2020 |Nautilus 

East Ventures, one of the most prolific and influential investment firms in Indonesia, Southeast Asia’s largest market, will be represented by Melisa Irene, the firm’s first female partner. Presenting TechCrunch Disrupt’s Asia sessions |Catherine Shu |August 28, 2020 |TechCrunch 

As a prolific and early entry in the cannon of television drama, The Twilight Zone never fully disappeared from the airwaves. How a War-Weary Vet Created ‘The Twilight Zone’ |Rich Goldstein |November 13, 2014 |DAILY BEAST 

Glackens was a prolific cartoonist in Philadelphia and his comics are one of the most surprising elements in the Puck book. The Magazine That Made—and Unmade—Politicians |Anthony Haden-Guest |November 2, 2014 |DAILY BEAST 

Since Westlake was as prolific as he was versatile, this all took a while. Donald E. Westlake, The Man With The Getaway Face |Malcolm Jones |October 25, 2014 |DAILY BEAST 

All of this comes across in her writing about cultural politics—and she is staggeringly prolific. Join Caitlin Moran’s Riotous Feminist Revolution |Lizzie Crocker |September 29, 2014 |DAILY BEAST 

Now, here is a sweet taste of the South from one of our most prolific and talented writers. Let Us Now Praise Famous Rednecks and Their Unjustly Unsung Kin |Allison Glock |August 23, 2014 |DAILY BEAST 

The soil of Cuba is prolific, and the variety of tropical plants and fruits grown upon the island is quite remarkable. Tobacco; Its History, Varieties, Culture, Manufacture and Commerce |E. R. Billings. 

The free book system that prevails in some schools is a prolific source of infection. Essays In Pastoral Medicine |Austin Malley 

Such are the gifts the gods have endowed us withal: such was the facility of this prolific writer! The Works of Robert Louis Stevenson - Swanston Edition Vol. 25 (of 25) |Robert Louis Stevenson 

The previous works of this prolific author have proved by their popularity that they meet a genuine demand. The Atlantic Monthly, Volume 17, No. 101, March, 1866 |Various 

These animals are very prolific; the young ones follow the dam, and do not separate from her till they are full grown. Buffon's Natural History. Volume VII (of 10) |Georges Louis Leclerc de Buffon