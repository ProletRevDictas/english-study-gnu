Justin, as I would soon learn, is a large and taciturn man, but he was eager to talk, and urged me to come to New Mexico to learn more about what Kooper and his friends had been through. The Lost Year: What the Pandemic Cost Teenagers |by Alec MacGillis, photography by Celeste Sloman |March 8, 2021 |ProPublica 

Her father, a lumber company owner, was taciturn and remote, she recalled in her self-titled memoir. Cloris Leachman, Oscar-winning actress who played Frau Blücher (neighhh!) in ‘Young Frankenstein,’ dies at 94 |Adam Bernstein |January 27, 2021 |Washington Post 

His is a particularly taciturn manifestation of masculinity. Nigeria’s Larger-Than-Life Nobel Laureate Chronicles a Fascinating Life |Chimamanda Adichie |August 9, 2014 |DAILY BEAST 

No one would confuse him the taciturn, forgetful and vengeful Senate Majority Leader. Nevada Guv Faces Fans and Foes in Reelection |Lloyd Green |March 18, 2014 |DAILY BEAST 

Tall and taciturn, he exuded the easy authority of a young man used to money and the deference that came with it. Doug Kenney: The Odd Comic Genius Behind ‘Animal House’ and National Lampoon |Robert Sam Anson |March 1, 2014 |DAILY BEAST 

The exuberant, indefatigable Democrat from Oregon and the dour, taciturn Republican from New Hampshire made an odd couple. The Senate’s New Taxman Won’t Be Controlled By His Own Party |Linda Killian |February 18, 2014 |DAILY BEAST 

The usually taciturn Zakhilwal spoke spiritedly and aggressively for the first hour of his impeachment. Afghanistan’s Cycle of Corruption |Mujib Mashal |May 16, 2013 |DAILY BEAST 

While the latter was remarkably eloquent, the former was taciturn to the last degree. The Life &amp; Letters of Peter Ilich Tchaikovsky |Modeste Tchaikovsky 

She spoke no word of English and seemed rather taciturn; the only anxiety she manifested was as to the amount of her remuneration. The Pit Town Coronet, Volume II (of 3) |Charles James Wills 

Pale, lean, taciturn and somewhat deaf, he bore much resemblance to the Knight of the Rueful Countenance. Repertory Of The Comedie Humaine, Complete, A -- Z |Anatole Cerfberr and Jules Franois Christophe 

Josephine appeared as usual at breakfast: talkative to her father, and taciturn to her female companion. Alone |Marion Harland 

They were silent and taciturn, and acted as though a short conversation indicated a "financial touch." Ways of War and Peace |Delia Austrian