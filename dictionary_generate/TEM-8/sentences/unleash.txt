He isn’t surprised that, once unleashed, Vandersloot’s play was historic. WNBA Star Courtney Vandersloot Has Assisted Her Way Into The Record Books |Howard Megdal |September 2, 2020 |FiveThirtyEight 

In the early-morning hours of Sunday, August 16, a thunderstorm unleashed more than 12,000 dry lightning strikes across central and Northern California. How We Saved Our Neighborhood from a Wildfire |Dain Zaffke |September 2, 2020 |Outside Online 

Wall Street clawed back the last of the historic, frenzied losses unleashed by the new coronavirus, as the S&P 500 closed at an all-time high Tuesday. S&P 500 hits a new record, erasing last of pandemic losses |Verne Kopytoff |August 18, 2020 |Fortune 

It was about unleashing human beings’ potential from the roles that society had fashioned, seeing each person as a parcel of possibilities that might get expressed in many creative ways. Gender Is What You Make of It - Issue 88: Love & Sex |Charles King |August 5, 2020 |Nautilus 

This “double-arc instability” leads to more magnetic reconnection, and the m-shaped loop expands, unleashing energy. The physics of solar flares could help scientists predict imminent outbursts |Emily Conover |July 30, 2020 |Science News 

But it is too early to tell if the changes he helped unleash will prove sustainable, or if they will broadly serve our citizenry. Your Local School Doesn’t Have to Suck |Michael S. Roth |December 17, 2014 |DAILY BEAST 

He said he watched waste haulers back up to the pit and unleash torrents of watery muck. Two Texas Regulators Tried to Enforce the Rules. They Were Fired. |David Hasemyer, InsideClimate News |December 9, 2014 |DAILY BEAST 

Keep in mind that this is just the first round of legislation the newly empowered Republicans are planning to unleash. GOP States’ Hitlist: Abortion, Unions & Hillary |Nancy Kaffer |November 18, 2014 |DAILY BEAST 

And yet, Biden and Obama now seek to again unleash Klain on America. Where There’s Trouble, You’ll Usually Find Joe Biden |Lloyd Green |October 21, 2014 |DAILY BEAST 

Many are certain that China will unleash something sinister in the territory this week if the protests continue. Beijing/Hong Kong: A Tale of Two Cities as Demonstrations Continue |Ben Leung |October 1, 2014 |DAILY BEAST 

Unleash your dogs of war and make these hounds of convict stripe pay penalty for the great injury done. The Modern Ku Klux Klan |Henry Peck Fry 

It is easy to unleash such a tempest of fire, but once started it is beyond all human control. The Frontier Boys in the Grand Canyon |Wyn Roosevelt 

After mature consideration we had decided to unleash the Birdsburg contingent on the old boy ten at a time. My Man Jeeves |P. G. Wodehouse 

Once unleash the sea-dogs and it was extremely difficult to bring them again under restraint. The Buccaneers in the West Indies in the XVII Century |Clarence Henry Haring 

The General awaited the moment when the cannonade should cease, as suddenly as it had begun, and he should unleash his troops. Canada in Flanders, Volume I (of 3) |Lord Max Aitken Beaverbrook