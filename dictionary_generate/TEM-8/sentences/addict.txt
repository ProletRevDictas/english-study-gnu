“Ninth House,” by Leigh BardugoIn Bardugo’s engrossing fantasy thriller, a recovering addict lands a full ride to Yale in exchange for keeping an eye on the supernatural — and dangerous — activities of the school’s secret societies. October paperback releases: 14 books to read now |Stephanie Merry |October 30, 2020 |Washington Post 

Perpetual connectivity has led to a plethora of early social media apps that are designed to addict and distract us. Stop doomscrolling on social media and read a book |matthewheimer |October 25, 2020 |Fortune 

I remember having this strategy of muttering loudly to myself in hopes that people would think that I was either a heroin addict or otherwise insane and leave me alone. Is New York City Over? (Ep. 434) |Stephen J. Dubner |October 8, 2020 |Freakonomics 

You never completely eliminate something you learned that young, but you can learn to recognize it in yourself and take action to avoid it in the future, not unlike a recovering addict in a 12-step program. Coronado Isn’t Immune From National Issues Like Racism |Scott Douthwright |August 31, 2020 |Voice of San Diego 

“The judge was in a T-shirt in his basement and he grew a beard during the pandemic,” she says, laughing, and adds that one addict made her required court appearance while lying in bed and smoking a cigarette. How the Pandemic Is a Boon to Recovering Addicts |Fiona Zublin |August 24, 2020 |Ozy 

He cast her as Hope, an ex-addict with an impressive pair of fake chompers—the result of years of drug abuse. Jena Malone’s Long, Strange Trip From Homelessness to Hollywood Stardom |Marlow Stern |December 22, 2014 |DAILY BEAST 

They were getting more imaginative,” a pawn shop owner thinks of his addict customers in “Back of Beyond. This Week’s Hot Reads: December 22, 2014 |Mythili Rao |December 22, 2014 |DAILY BEAST 

He was an on-and-off drug addict and sometimes a criminal; he was a collector of switchblades and vintage Disney T-shirts. ‘All Good Cretins Go to Heaven’: Dee Dee Ramone’s Twisted Punk Paintings |Melissa Leon |December 15, 2014 |DAILY BEAST 

Because they agree that Ty Burrell looks like Jon Hamm if Jon Hamm were a crack addict? The Latest "Celebrities Read Mean Tweets" |Jack Holmes, The Daily Beast Video |November 21, 2014 |DAILY BEAST 

If you are a real poet, you are hooked more deeply than any narcotics addict could possibly be on heroin. Stephen King, “Falling,” and My Father’s Poetry |Christopher Dickey |September 14, 2014 |DAILY BEAST 

As soon as he mentioned the fact that Ansaldo's assistant Marina was a morphine addict, Santiago interrupted him. The Five Arrows |Allan Chase 

The streets were almost deserted, except for some prowler or desperation-driven drug addict. Police Your Planet |Lester del Rey 

Nor is he allowed to prescribe narcotics for an addict without decreasing the dosage. The Opium Monopoly |Ellen Newbold La Motte 

The Cure was a brief hell, but it was fair payment for having had his fun, and if the addict had any guts he would face it. The Man Who Staked the Stars |Charles Dye 

And it is a very risky thing for a woman to marry an addict with the idea of reforming him. Woman |William J. Robinson