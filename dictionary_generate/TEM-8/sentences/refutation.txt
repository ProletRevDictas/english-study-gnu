Being open to refutation is one of the most widely appreciated principles of science. How to Make Sense of Contradictory Science Papers - Issue 100: Outsiders |Haixin Dang & Liam Kofi Bright |June 2, 2021 |Nautilus 

The philosopher Karl Popper once said that science needs bold conjectures and attempted refutations. How to Make Sense of Contradictory Science Papers - Issue 100: Outsiders |Haixin Dang & Liam Kofi Bright |June 2, 2021 |Nautilus 

Douthat more or less treats the Oregon study as a de facto refutation of that entire, separate area of research. No, Really, It's Possible That Health Insurance May Not Make Us Healthier |Megan McArdle |May 7, 2013 |DAILY BEAST 

Which is not exactly a refutation or a denial so much as a talking point aimed at giving political partisans something to say. Jack Lew and the Obama Administration’s Finance-Friendly Status Quo |Lloyd Green |February 19, 2013 |DAILY BEAST 

Charlus' physical bravery is Proust's refutation of an ancient antigay stereotype. David's Bookclub: Sodom and Gomorrah |David Frum |September 29, 2012 |DAILY BEAST 

The geologist David R. Montgomery set out to write a “straightforward refutation of creationism.” How Noah’s Flood Spurred Science: David R. Montgomery’s ‘The Rocks Don’t Lie’ |David Sessions |August 28, 2012 |DAILY BEAST 

The metaphor that America is like a garden is not a gimmick, but powerful refutation of neoclassical economics. A New Theory of Democracy |David Frum |February 8, 2012 |DAILY BEAST 

But this pleasantry, excellent as pleasantry, hardly deserves serious refutation. The History of England from the Accession of James II. |Thomas Babington Macaulay 

And it may be asserted, without fear of refutation, that no federative government could exist without a similar provision. Key-Notes of American Liberty |Various 

Neither was the campaign of 1813 or 1814 any refutation of this. The Posthumous Works of Thomas De Quincey, Vol. II (2 vols) |Thomas De Quincey 

The refutation of this error was reserved for the surgeon, Bass. Celebrated Travels and Travellers |Jules Verne 

"Refutation and conclusion by the affirmative," said the chairman. Ramsey Milholland |Booth Tarkington