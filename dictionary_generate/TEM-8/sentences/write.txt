“It was ludicrous,” Troye now says, but she helped write it. A devastating picture of Trump’s coronavirus response — from a firsthand witness |Aaron Blake |September 17, 2020 |Washington Post 

He has been writing during unusual times, even before the pandemic spread earlier this year. The first volume of Barack Obama’s long-awaited memoir finally has a release date |Rachel King |September 17, 2020 |Fortune 

I’ve written before about the remarkable acceleration of digital transformation that’s taken place during the pandemic. Why Accenture thinks the ‘Henry Ford moment of the digital era’ is coming |Alan Murray |September 17, 2020 |Fortune 

It is fully collapsible, lightweight and offers a bold graphic “LAUNDRY” written on the side of the bin. Attractive laundry hampers that make your dirty clothes look a little better |PopSci Commerce Team |September 16, 2020 |Popular-Science 

We have written time and time again about the role good goaltending plays in a successful Stanley Cup run. Teams Don’t Win The Stanley Cup With A Goal Deficit. Can The Dallas Stars Change That? |Terrence Doyle |September 16, 2020 |FiveThirtyEight 

At some point during his busy schedule, Israel found the time to write a book, titled The Global War on Morris. Powerful Congressman Writes About ‘Fleshy Breasts’ |Asawin Suebsaeng |January 7, 2015 |DAILY BEAST 

My publisher had asked, “If you wanted to write another book, what would you want to write about?” Patton Oswalt on Fighting Conservatives With Satire |William O’Connor |January 6, 2015 |DAILY BEAST 

You write a lot about how you were a jerk or a snob when it came to comedy or film. Patton Oswalt on Fighting Conservatives With Satire |William O’Connor |January 6, 2015 |DAILY BEAST 

What made you want to write a memoir now about your “addiction” to film? Patton Oswalt on Fighting Conservatives With Satire |William O’Connor |January 6, 2015 |DAILY BEAST 

And “what kind of person,” Steinberg asks, “dares to write a sequel to the Bible?” Was ‘The Book of Mormon’ a Great American Novel? |Stefan Beck |January 4, 2015 |DAILY BEAST 

Now first we shall want our pupil to understand, speak, read and write the mother tongue well. The Salvaging Of Civilisation |H. G. (Herbert George) Wells 

I've never had time to write home about it, for I felt that it required a dissertation in itself to do it justice. Music-Study in Germany |Amy Fay 

The other is the new theory: that the Bible is the work of many men whom God had inspired to speak or write the truth. God and my Neighbour |Robert Blatchford 

Whatever you do, don't write a word to that Carr friend of yours; he's as sharp as a two-edged sword. Elster's Folly |Mrs. Henry Wood 

He must write down the first two words, “Ice” and “Slippery,” the latter word under the former. Assimilative Memory |Marcus Dwight Larrowe (AKA Prof. A. Loisette)