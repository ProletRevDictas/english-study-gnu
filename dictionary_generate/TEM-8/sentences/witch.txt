You would think I’d be sick of reading about catastrophes after a god-awful year, but I found Gabbert’s collection of essays about our darkest moments, including September 11, Chernobyl, witch hunts, and, yes, pandemics, to be strangely comforting. Everything Our Editors Loved in December |The Editors |January 8, 2021 |Outside Online 

He also asks about quitting, witch trials, and whether we need a Manhattan Project for climate change. “We Get All Our Great Stuff from Europe — Including Witch Hunting.” (Ep. 446) |Steven D. Levitt |January 7, 2021 |Freakonomics 

He still loves Halloween, and this Saturday his house’s decorations will be themed around the “Hocus Pocus” witches. Put on a frightful face: This former CNN makeup artist got his start in horror |John Kelly |October 28, 2020 |Washington Post 

TikTok, with its 49 million daily users in the US, is impossibly diverse, a home for comedians and singers and witches and police officers and protesters. How a TikTok house destroyed itself |Rebecca Jennings |October 1, 2020 |Vox 

Mulan runs away from home to take her father’s place in the army and fight a powerful witch. Women like Mulan didn’t need to go to war in disguise |Bethany Brookshire |September 4, 2020 |Science News For Students 

He once experimented with dressing as “Hilda the Wicked Witch” as a way to expand his business to Halloween. Kerry Bentivolio: The Congressman Who Believes in Santa Claus |Ben Jacobs |December 24, 2014 |DAILY BEAST 

Breitbart forced her to correct a small part of her story, but witch hunts like these will leave every victim cowering. The Right's Rape Trolls vs. Lena Dunham |Emily Shire |December 10, 2014 |DAILY BEAST 

In her mind, the entire ordeal was a witch-hunt led by the local authorities. Whip It: Secrets of a Dominatrix |Justin Jones |November 25, 2014 |DAILY BEAST 

She faces a jury of famous villains and a judge from the Salem witch trials. Election Day Is Scarier Than Halloween |P. J. O’Rourke |November 1, 2014 |DAILY BEAST 

Likewise, pressure must be placed on Egypt to abandon its witch hunt of the Muslim Brotherhood. To Beat ISIS, the Arab World Must Promote Political and Religious Reforms |Rula Jebreal |September 15, 2014 |DAILY BEAST 

Belle, my mother, and I rode home about midnight in a fine display of lightning and witch-fires. The Works of Robert Louis Stevenson - Swanston Edition Vol. 25 (of 25) |Robert Louis Stevenson 

But the Witch was more furious than ever, and as soon as I raised my axe to chop, it twisted around and cut off one of my arms. The Tin Woodman of Oz |L. Frank Baum 

The Witch did not wish me to marry the girl, so she enchanted my sword, which began hacking me to pieces. The Tin Woodman of Oz |L. Frank Baum 

It was while she was away on this errand that Dorothy's house fell on the Wicked Witch, and she turned to dust and blew away. The Tin Woodman of Oz |L. Frank Baum 

Halgernon was a barrystir—that is, he lived in Pump Cort, Temple: a wulgar naybrood, witch praps my readers don't no. Memoirs of Mr. Charles J. Yellowplush |William Makepeace Thackeray