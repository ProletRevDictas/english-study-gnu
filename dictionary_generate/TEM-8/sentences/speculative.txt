This is speculative, but it’s quite possible that Apple could become the number two mobile search engine. Is Apple getting real about search and about to take on Google? |Greg Sterling |August 27, 2020 |Search Engine Land 

“These are speculative for sure, with a capital S,” says William Gabella. Could ripples in spacetime point to wormholes? |Emily Conover |August 24, 2020 |Science News For Students 

He openly advocates that catching the coronavirus is a good thing, because healthy people will build up antibodies, a claim that is entirely speculative at this point. Supervisor by Day, But a COVID-19 Skeptic on the Airwaves |Katy Stegall |August 20, 2020 |Voice of San Diego 

The most recent numbers are speculative and from 2016, but they suggest that at the time, at least half of Americans had photos in a face recognition system. There is a crisis of face recognition and policing in the US |Tate Ryan-Mosley |August 14, 2020 |MIT Technology Review 

All to be taken for what they are—informed but utterly speculative proposals. How Life Could Continue to Evolve - Issue 88: Love & Sex |Caleb Scharf |August 12, 2020 |Nautilus 

Dardagan and his peers are the first to admit that local media reports often are speculative in the extreme. ISIS Fighters Are Killing Faster than Statisticians Can Count |Peter Schwartzstein |December 5, 2014 |DAILY BEAST 

Architecture has a long history of this type of speculative design. Why Architects Dream Big—and Crazy |Nate Berg |August 23, 2014 |DAILY BEAST 

The speculative architecture of today can play that same role, and it often does. Why Architects Dream Big—and Crazy |Nate Berg |August 23, 2014 |DAILY BEAST 

But it might break down in regimes we have trouble testing, and some speculative theories have proposed something like that. The Equivalence Principle and Testing Einstein With Spaceships and Atoms |Matthew R. Francis |June 4, 2014 |DAILY BEAST 

Luskin makes the best argument he can, of course, but it is piecemeal and speculative. The Crazy Way Creationists Try To Explain Human Tails Without Evolution |Karl W. Giberson |June 1, 2014 |DAILY BEAST 

Her youthful vanity had its way in a mind too speculative, intelligent, observant, merely to be shocked. Ancestors |Gertrude Atherton 

It was not a languid, speculative, preference of one theory of government to another, but a fierce and dominant passion. The History of England from the Accession of James II. |Thomas Babington Macaulay 

It may be something; it may be nothing; but my speculative instinct has been aroused by a strange peculiarity in his playing. The Fifth String |John Philip Sousa 

Any lurking danger of too great speculative restlessness disappeared. The Daughters of Danaus |Mona Caird 

But from the day it was completed its securities have figured in the market only for their speculative values. The Wreckers |Francis Lynde