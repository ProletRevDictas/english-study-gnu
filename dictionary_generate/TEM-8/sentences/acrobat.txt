With the lights of the Coco Bongo disco illuminating the night sky, tourists dance in open-air clubs while acrobats twirl above them, raining glitter down on a packed and maskless crowd. Tourists Are Returning to Cancún. But Workers' Fears About COVID-19 Never Went Away |Oscar Lopez / Cancún, Mexico |January 11, 2021 |Time 

His skills as an acrobat were his ticket out of his impoverished circumstances. A dishy, definitive look at Cary Grant |Kathi Wolfe |November 5, 2020 |Washington Blade 

Acrobat death In 2004, Dessi Espana, a performer with Ringling Bros. and Barnum Bailey, fell 35 feet during a performance. Thrills and Too Many Spills: The Dangers of the Circus |Marina Watts |May 5, 2014 |DAILY BEAST 

The acrobat was twirling during her performance, suspended by a chiffon scarf. Thrills and Too Many Spills: The Dangers of the Circus |Marina Watts |May 5, 2014 |DAILY BEAST 

Everybody in her class was either a potential Van Gogh or an acrobat. Mel Brooks Is Always Funny and Often Wise in This 1975 Playboy Interview |Alex Belth |February 16, 2014 |DAILY BEAST 

Leaning against the bar, the Chinese acrobat looks weary, though he says none of the tricks are difficult for him anymore. A Mad Feast Is the Next 'Sleep No More' |Nina Strochlic |February 3, 2014 |DAILY BEAST 

There is simply no way to compete against a 66-year-old actress dangling daringly off the arm of a muscular acrobat. Who’ll Win a 2013 Tony Award—and Who Deserves To |Janice Kaplan |June 6, 2013 |DAILY BEAST 

Of no more than medium height but with shoulders like an acrobat, he had slim, straight legs and the feet of a dancing master. Dope |Sax Rohmer 

Shoeblossom leaped back with a readiness highly creditable in one who was not a professional acrobat. The Gold Bat |P. G. Wodehouse 

Starcus proved that the rest of his limbs were uninjured by coming as nimbly as an acrobat to an upright posture. The Young Ranchers |Edward S. Ellis 

Balancing on his hands like an acrobat, he crawled over the edge, down to the main deck, and began to explore forward. Where the Pavement Ends |John Russell 

It is strange to see a quiet-looking shell suddenly take to hopping and jumping like an acrobat. On the Seashore |R. Cadwallader Smith