They may argue that the House managers have failed to prove that what he did meets the terms of inciting an insurrection. All eyes on Republican senators after strong presentation by House managers |Dan Balz |February 12, 2021 |Washington Post 

I don’t think he was inciting violence by that, but I do think that it led to a dangerous situation. When Madison Cawthorn Felt ‘Invisible’ |Eugene Robinson |February 9, 2021 |Ozy 

Parler was effectively knocked offline in January when Amazon, Apple and Google stopped providing it technical services for violating their policies against online posts that incite violence. Dominion Voting tells Facebook, Parler and other social media sites to preserve posts for lawsuits |Rachel Lerman |February 5, 2021 |Washington Post 

She argued there needs to be accountability for Republicans in Congress who made false claims of widespread fraud in the 2020 election and who have not condemned the events or apologized for their alleged role in inciting the attempted insurrection. Ocasio-Cortez reveals she’s a sexual assault survivor, recounts ‘trauma’ of Capitol riot: ‘I thought everything was over’ |Jaclyn Peiser |February 2, 2021 |Washington Post 

The former president was impeached for a second time in the House of Representatives, most recently for “engaging in high Crimes and Misdemeanors by inciting violence against the Government of the United States.” Ghosts of our unsettled past |Robin Givhan |January 26, 2021 |Washington Post 

For example, the MTA prohibits ads that “incite or provoke violence.” To Fight Pam Geller, Join Our Comedy Jihad at the MTA |Dean Obeidallah |September 23, 2014 |DAILY BEAST 

Though Rabinowitz muses that something like this would not only be impractical but also incite severe backlash. Porn Fights For Your Right to Surf: Pornhub, YouPorn, and Redtube Lead Charge For Net Neutrality |Aurora Snow |September 13, 2014 |DAILY BEAST 

Few people touch that nerve and incite so much passionate conversation than Dunham and Girls. Lena Dunham on 'SNL' Review: Very Funny, Very Dunham-y |Kevin Fallon |March 9, 2014 |DAILY BEAST 

Robertson, they said, would “never incite or encourage hate.” A&E Ducks for Cover by Forgiving Phil Robertson |Michael Musto |December 30, 2013 |DAILY BEAST 

Russell Brand is trying to use comedy to incite political revolution. Russell Brand: Not Quite a Messiah |James Poulos |October 28, 2013 |DAILY BEAST 

This is the more annoying, as there are circumstances that particularly incite our curiosity. Frederick Chopin as a Man and Musician |Frederick Niecks 

And to incite the efforts of honest but unfortunate men, bankrupt laws equally useful to creditor and debtor were established. A short history of Rhode Island |George Washington Greene 

It seems impossible to believe that these are the men whom Irish patriots incite to mutiny. In the Ranks of the C.I.V. |Erskine Childers 

Thy countrymen are wild, fierce, and warlike: why not incite their martial passions in defence of thy doctrines? Beacon Lights of History, Volume V |John Lord 

Not one true believer whom the flesh does not again and again incite to impatience, anger, pride. Commentary on the Epistle to the Galatians |Martin Luther