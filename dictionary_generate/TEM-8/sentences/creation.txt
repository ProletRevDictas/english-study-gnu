It was the “first creation of life in a test tube,” according to the newspaper headlines, and a possible bioterror threat, too. Synthetic biologists have created a slow-growing version of the coronavirus to give as a vaccine |David Rotman |September 16, 2020 |MIT Technology Review 

I think it will be better for privacy, but what that means for content creation or algorithms or our clout, I don’t know. What’s Oracle? TikTok users react to proposed Oracle deal |Danielle Abril |September 15, 2020 |Fortune 

Under the new guidelines, these reader apps will also offer account creation within iOS, as long as it’s for the free tier of the product. Apple revises App Store rules to permit game streaming apps, clarify in-app purchases and more |Sarah Perez |September 11, 2020 |TechCrunch 

During that time, he oversaw the creation and initial development of the agency's large Space Launch System rocket. Charlie Bolden says the quiet part out loud: SLS rocket will go away |Eric Berger |September 11, 2020 |Ars Technica 

Then, we went to the vineyard to begin the proper creation of our wine. Luxury hotels are wooing guests with exclusive wines and spirits |Rachel King |September 5, 2020 |Fortune 

The mythic origin of the feast was the creation of the world by the god Marduk. New Year’s Eve, Babylon Style |Candida Moss |December 31, 2014 |DAILY BEAST 

The presiding light behind this creation was a woman named Natalia Murray. The Bookstore That Bewitched Mick Jagger, John Lennon, and Greta Garbo |Felice Picano |December 16, 2014 |DAILY BEAST 

The movie works because we are engulfed in the music and believe that we have been in the presence of its creation. Why Can’t Movies Capture Genius? |Clive Irving |December 14, 2014 |DAILY BEAST 

And suddenly you were in the throes of both creation and destruction. Is Bigger Better for St. Vincent? |David Yaffe |December 4, 2014 |DAILY BEAST 

Roughly half of black Protestants and white Evangelicals believe that creation is purely for human benefit. Extreme Weather? Blame the End Times |Jay Michaelson |November 28, 2014 |DAILY BEAST 

In the Parsî religion that fall of man, by a lie, was recovered from by the creation of a new man. Solomon and Solomonic Literature |Moncure Daniel Conway 

It may be added that they start with the most dignified part of this crown of creation, viz., the human head. Children's Ways |James Sully 

On land and in sea the animal creation chase and maim, and slay and devour each other. God and my Neighbour |Robert Blatchford 

She is still inspiring the unfinished work of creation, and her delight is with the children of men. Solomon and Solomonic Literature |Moncure Daniel Conway 

To address Fleurette, impalpable creation of fairyland, as “old girl” was particularly distasteful. The Joyous Adventures of Aristide Pujol |William J. Locke