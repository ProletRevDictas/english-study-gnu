This is a comment that usually earns my quiet and undying enmity. The ghosts of our motel |Sabaa Tahir |November 29, 2021 |Vox 

And, while there are paths out of this predicament, this amplified disagreement is emblematic of the dangers the debt ceiling presents to our political system, especially when partisan enmity is so high. The Debt Ceiling Is Dangerous. Here’s Why It Probably Isn’t Going Anywhere. |Geoffrey Skelley (geoffrey.skelley@abc.com) |October 7, 2021 |FiveThirtyEight 

This earned her the enmity of progressives who funneled more than $5 million to the campaign of her 2020 Democratic opponent, Tedra Cobb. Elise Stefanik could be just what the GOP needs |Henry Olsen |May 6, 2021 |Washington Post 

Quite a burn there, considering the much-talked-about enmity between the governor and the mayor. Cuomo sidelines major media players in remote press conferences |Erik Wemple |April 23, 2021 |Washington Post 

It’s long been an oasis of peace despite the civil wars in Syria and Iraq just across its borders, the bitter enmity between two of its neighbors, Israel and Syria, and the masses of refugees it hosts. Butterfly Effect: The Hidden Story Behind Jordan’s Attempted Palace ‘Coup’ |Charu Kasturi |April 6, 2021 |Ozy 

Without a common enemy, the Naqshabandi and ISIS might resume their natural enmity. Saddam’s Former Deputy, the Red Skull of Baghdad, Still at Large in Iraq and Allied With ISIS |Jacob Siegel, Christopher Dickey |July 21, 2014 |DAILY BEAST 

And they bristle too at the notion that they had some kind of personal enmity toward the president. These Clinton Haters Can’t Quit the Crazy |David Freedlander |May 22, 2014 |DAILY BEAST 

Believe what your post-hard-work-day brain will allow, but the enmity went from entrenched to fleeting in a blink of an eye. Betrayal, Blowjobs, and Bitchery: the 'Real Housewives' Get Really Desperate |Tim Teeman |March 12, 2014 |DAILY BEAST 

We were involved in a terribly acrimonious breakup, with great enmity between us and a custody battle slowly gathering energy. Woody Allen Fires Back: Dylan Farrow Was Brainwashed By Her Mother, Mia Farrow |Tim Teeman |February 8, 2014 |DAILY BEAST 

Yet the government in which Lapid serves appears tied to a notion of eternal enmity. Israeli Politicians Forbidden to Attend Rosh Hashanah Event With Abbas |Emily L. Hauser |August 30, 2013 |DAILY BEAST 

Rather blow out your own brains than treat with enmity those who are your liberators. The Philippine Islands |John Foreman 

It is the crystallizer of character, the acid test of friendship, the final seal set upon enmity. Cabin Fever |B. M. Bower 

In short, they are human parasites on the larger natives, who suffer from their extortions, yet fear to provoke their enmity. Man And His Ancestor |Charles Morris 

But this same nature, when pinched and starved, becomes a perfect storehouse of enmity and ill-feeling. A Cursory History of Swearing |Julian Sharman 

Oh, horrible thought, yet too natural to the unhappy prisoner, everywhere in fear of enmity and fraud! My Ten Years' Imprisonment |Silvio Pellico