We have already reported on failures in testing, the effectiveness of popular hand sanitizers and hospitals retaliating against medical providers for bringing their own masks to work. Are You Participating in a Vaccine Trial? Are You Running One? We’d Like to Hear About It. |by Caroline Chen, Ryan Gabrielson and Isaac Arnsdorf |September 17, 2020 |ProPublica 

Starting in 2012, workers at McDonald’s franchises claimed they were fired or otherwise retaliated against for their involvement in protests seeking union representation and higher wages as part of the “Fight for $15” campaign. “Cover Up”: House Democrats Subpoena Documents That NLRB Refused to Share in Ethics Investigation |by Ian MacDougall |September 15, 2020 |ProPublica 

Yet apart from propaganda-laced vitriol against the United States in its state media, China hasn’t meaningfully retaliated so far. Butterfly Effect: Can Xi Avoid Trump’s Trap? |Charu Kasturi |August 27, 2020 |Ozy 

Boston went on to play a critical role in American History, as the American Revolution erupted in Boston, after the British retaliated against the Americans for the Boston Tea Party, and the patriots fought back. Boston: A Historic City and a Trailblazer for Equality |LGBTQ-Editor |August 2, 2020 |No Straight News 

Carson later added Brady to his complaint and alleged his boss and fellow administrators created a hostile work environment for him, retaliated against him for filing the complaint and discriminated against him. Accusations Flew, Then National School District Official Got Paid to Resign |Ashly McGlone |July 20, 2020 |Voice of San Diego 

Pyongyang has given the Obama administration no choice but to retaliate now by imposing sanctions or even an embargo. U.S. Should Make North Korea Pay for Sony Hack |Gordon G. Chang |December 18, 2014 |DAILY BEAST 

Hamas is firing missiles and begging Israel to retaliate so they could use the images as propaganda. Gaza, You're No Good For My Marriage |Josh Robin |August 9, 2014 |DAILY BEAST 

Nuclear weapons could prove the only way for it to retaliate in-kind, and nobody wants that. Death at Five Times the Speed of Sound |Kyle Mizokami |June 23, 2014 |DAILY BEAST 

A frightened country could be prompted to quickly retaliate with nuclear weapons. Death at Five Times the Speed of Sound |Kyle Mizokami |June 23, 2014 |DAILY BEAST 

At least some members of Congress are looking to retaliate in kind. Congress Targets Russia’s ‘Satan’ Missile |Eli Lake |May 19, 2014 |DAILY BEAST 

These associations showed the English manufacturers that Ireland could retaliate upon them. The Political History of England - Vol. X. |William Hunt 

Please give my love to all your people, and forgive my intolerably long letters—or retaliate in kind. The Letters of Ambrose Bierce |Ambrose Bierce 

She dropped her bridle-reins, springing back a quick step, turning her eyes about for some weapon by which she might retaliate. The Flockmaster of Poison Creek |George W. Ogden 

The fleet of Sator tried to retaliate, but the Nansalians were prepared for them. Islands of Space |John W Campbell 

The representation in the House would surely suffer by his action, because in this way only could the offended people retaliate. The Art of Disappearing |John Talbot Smith