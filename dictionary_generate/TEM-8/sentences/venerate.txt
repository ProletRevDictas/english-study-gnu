The coach, a guest, venerates the stranger, who’s presumably a member, saying how he’d like to teach Howard golfers as much about the game that the older man has already forgotten. Howard University, with a coach’s moxie and a superstar’s money, takes on golf |Candace Buckner |April 8, 2021 |Washington Post 

Newly discovered remnants of a nearby stone circle seem to confirm the theory that a stone shrine was dismantled and somehow dragged to the Stonehenge site — there to be venerated anew. Hiking along Virginia’s Aquia Creek and finding history a stone’s throw from home |Walter Nicklin |April 2, 2021 |Washington Post 

Philadelphia is home to some of the most venerated medical institutions in the country. Philadelphia let ‘college kids’ distribute vaccines. The result was a ‘disaster,’ volunteers say. |Antonia Noori Farzan |January 27, 2021 |Washington Post 

The reason the “who deserves the credit for Citizen Kane” controversy still has legs is that the film is still venerated as one of the greatest films of all time, and plenty of writers have tackled the question of its authorship. The tangled history behind David Fincher’s Mank — and its link to Citizen Kane — explained |Alissa Wilkinson |December 4, 2020 |Vox 

He is much more interesting as a political curmudgeon than as a venerated figure absorbing flowery testimonials. Four presidents who put Virginia’s stamp on early America |Andrew Burstein |October 30, 2020 |Washington Post 

Confucius rarely openly rebuked any one, especially a prince, whom it was his duty to venerate for his office. Beacon Lights of History, Volume I |John Lord 

He has a perfect right to venerate Mr. Tindall, and if he is a little fashionable, what of that? Our Churches and Chapels |Atticus 

Those who do not venerate their poets, and have respect to the early history of their country, are a dull, besotted people. The Mysteries of All Nations |James Grant 

The one who is first in music, who succeeds the best on the violin or piano, is like a king to them; they love, they venerate him. Cuore (Heart) |Edmondo De Amicis 

This is a world worth abiding in while one man can thus venerate and love another. George Eliot's Life, Vol. I (of 3) |George Eliot