Just a 15-minute drive west is Distillery 98, where you can sip sustainably-produced vodka made from 100-percent Florida Panhandle corn that gets filtered through a unique oyster shell apparatus to smooth the liquor out. The 25 Best Fall Trips in the World |jversteegh |August 9, 2021 |Outside Online 

For centuries, going back to Ivan the Terrible, the tsarist government maintained an incredibly lucrative monopoly on the vodka trade. Tsar Nicolas II Thought Vodka Was Hurting Russians—But Banning It Helped Destroy His Empire |Mark Lawrence Schrad |July 20, 2021 |Time 

The group sat down to a multicourse dinner served with wine and vodka. Inside the FBI, Russia, and Ukraine’s failed cybercrime investigation |Patrick Howell O'Neill |July 8, 2021 |MIT Technology Review 

For a lighter option from the spirits world, Stillhouse has a great selection of bourbon, whiskey, and vodka in stainless steel rectangular cans. The cooking gear you need—and don’t need—for a camping trip |Purbita Saha |June 22, 2021 |Popular-Science 

The pair was handcuffed and the key was at the bottom of a vodka bottle they had to drain before being released. A freshman was ‘hazed to death,’ his family says. 15 former fraternity members now face charges. |Reis Thebault |June 4, 2021 |Washington Post 

And Ollie says, ‘Oh, I see, well, let me have two double vodka martinis.’ The Story Behind Lee Marvin’s Liberty Valance Smile |Robert Ward |January 3, 2015 |DAILY BEAST 

The substitute nurse says to him in a stage whisper, “You know, the doctor says no vodka.” Alfred Hitchcock’s Fade to Black: The Great Director’s Final Days |David Freeman |December 13, 2014 |DAILY BEAST 

This fancy spice pack pairs with four different spirits—vodka, tequila, aquavit, and gin—to ensure the perfect morning pick-me-up. The Daily Beast’s 2014 Holiday Gift Guide: For the Don Draper in Your Life |Allison McNearney |November 29, 2014 |DAILY BEAST 

More so than any American activist that was dumping out bottles of Latvian vodka thinking it was Russian in the summer of 2013. ‘To Russia With Love’: Can Johnny Weir Save Russia’s Gays? |Kevin Fallon |October 29, 2014 |DAILY BEAST 

Served tapas style, an ABSOLUT vodka cocktail accompanied each plate. Wash ‘Pulp Fiction’ Down With a Tasty Beverage |Rich Goldstein |October 17, 2014 |DAILY BEAST 

If the troops in Trans-Caucasia were not much libelled, many of them came from their barracks, in exchange for vodka! The Cradle of Mankind |W.A. Wigram 

Josef laughed, and with a shaky hand poured himself out another glass of vodka. Eastern Nights - and Flights |Alan Bott 

Wine and vodka, as aids to forgetfulness of the fear that hovered over every feast, were well worth their sixty roubles a bottle. Eastern Nights - and Flights |Alan Bott 

When the inspector saw that I was without help, and the sweat was running off my forehead, he called out: 'Vodka! More Tales by Polish Authors |Various 

So they sat awhile and talked and then the Wolf took another deep swallow of the vodka. Mighty Mikko |Parker Fillmore