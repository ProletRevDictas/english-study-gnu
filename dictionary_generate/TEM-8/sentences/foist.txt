Alarmed, Graft launched a campaign against the book and another about a boy who likes to wear dresses, suggesting that their presence in the library foisted inappropriate themes on unsuspecting children. A Push to Remove LGBTQ Books in One County Could Signal Rising Partisanship on School Boards |by Jeremy Schwartz, ProPublica and The Texas Tribune |February 7, 2022 |ProPublica 

It’s easier to sell the crap you make in your spare time, and you’re more likely to need the money than you might have been a few decades ago, when you could have just foisted it all on your friends. The complicated reality of doing what you love |Marian Bull |August 25, 2021 |Vox 

They often saw those benefits as bribes to make up for the grueling expectations foisted on them. ‘My mental abilities are impaired by work’: Disparity between bosses and staffers on mental wellness intensify |Seb Joseph |June 1, 2021 |Digiday 

“It’s going to require some important messaging to help people understand we’re not foisting the bad vaccine on you—there isn’t a bad vaccine that we would allow to stay in use,” Fernandez Lynch says. The J&J vaccine is back. Next comes trust. |Mia Sato |April 29, 2021 |MIT Technology Review 

Glenn Beck did it before, although the abstract noun he chose to foist on the nation was "honor." Sanity Is Overrated |Tunku Varadarajan |October 29, 2010 |DAILY BEAST 

Shame on John McCain—and every other Republican who says the Senate health deal would foist single-payer on the country. The GOP's New Health-Care Hoax |Matt Miller |December 12, 2009 |DAILY BEAST 

Galley-foist may be the name of some dress of the period, so-called for its resemblance to the gaily bedecked Mayors-barge. The Fatal Dowry |Philip Massinger 

This is hardly a changeling story, as no attempt was made to foist a false child on the parent. The Science of Fairy Tales |Edwin Sidney Hartland 

Even Latin, living Latin had not the network of rules they foist upon unfortunate school-children. Instigations |Ezra Pound 

Me mudder wasn't built to stand de wear and tear, an' about de time I was foist chased off to school, she went out o' biz. To Him That Hath |Leroy Scott 

The part had been thrust on me one day, when Edward proposed to foist the House of Lords on our small republic. The Golden Age |Kenneth Grahame