The schedule adjustment gives Maryland the chance to win three games next week. In schedule shuffle, Maryland will host Nebraska on back-to-back days next week |Emily Giambalvo |February 12, 2021 |Washington Post 

Some families have grown used to a pandemic schedule — Mom and Dad aren’t going into the office — so they decide that everyone should just stay home. The mid-pandemic return to school is totally weird for kids. And possibly lonely, too. |Petula Dvorak |February 11, 2021 |Washington Post 

Contact tracing has begun, the league said, and Thursday’s game between the Ducks and Golden Knights remains on schedule. The Caps are dealing with an unexpected break. They hope to use it to recover and reset. |Samantha Pell |February 10, 2021 |Washington Post 

A few others on the list — namely Montana, Terry Bradshaw and Kurt Warner — have had better per-game or per-play Super Bowl performances than Brady after we adjust for schedule and era. All The Ways That Tom Brady Is Football’s GOAT |Neil Paine (neil.paine@fivethirtyeight.com) |February 9, 2021 |FiveThirtyEight 

After a while, I made a point of having him tell me his travel schedule so I could be sure to see him in person. Tom Konchalski made basketball better. The sport won’t be the same without him. |John Feinstein |February 8, 2021 |Washington Post 

Therefore, it is not possible for any F-35 schedule to include a video data link  or infrared pointer at this point. Pentagon Misfires in Stealth Jet Scandal |Dave Majumdar |January 8, 2015 |DAILY BEAST 

At some point during his busy schedule, Israel found the time to write a book, titled The Global War on Morris. Powerful Congressman Writes About ‘Fleshy Breasts’ |Asawin Suebsaeng |January 7, 2015 |DAILY BEAST 

Doubling down on Schedule I is, at best, a deranged way to push Americans away from “medical,” and toward recreational, use. Obama’s Pot Policy Is Refer Madness |James Poulos |January 5, 2015 |DAILY BEAST 

If 2014 was any indication, the coming TV schedule is sure to be filled with plenty of water-cooler shows. Four TV Shows We Can’t Wait to Return In 2015 |DISH |December 22, 2014 |DAILY BEAST 

One minute the script, the next a story about Ivor Novello's tailor or the Tahiti steamer schedule in the Thirties. Alfred Hitchcock’s Fade to Black: The Great Director’s Final Days |David Freeman |December 13, 2014 |DAILY BEAST 

But we were already behind schedule and the afternoon found us on the road to Ayr. British Highways And Byways From A Motor Car |Thomas D. Murphy 

How much time he had lost he did not know, but that down-grade had put his schedule many minutes to the good. Motor Matt's "Century" Run |Stanley R. Matthews 

The theory of the new scheme was that it might permit of a lower Customs tariff schedule. The Philippine Islands |John Foreman 

Great alterations were said to have been made in schedule A: fifty-one boroughs out of fifty-six remained as before. The History of England in Three Volumes, Vol.III. |E. Farr and E. H. Nolan 

The consideration of the schedule ought to be postponed till that information had been obtained. The History of England in Three Volumes, Vol.III. |E. Farr and E. H. Nolan