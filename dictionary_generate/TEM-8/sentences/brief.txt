As they moved down the street, Harris waved, held her hand to her heart, and bumped elbows and had brief conversations with several onlookers. Election live updates: Trump returns to Wisconsin; Biden to face live audience at town hall |Colby Itkowitz, Felicia Sonmez, John Wagner |September 17, 2020 |Washington Post 

Amazon, meanwhile, set their preorders live late Wednesday around midnight ET, only for consoles to sell out within a similarly brief timeframe. F5 for PS5: All your PlayStation 5 preorder links in one place |Jeff Dunn |September 17, 2020 |Ars Technica 

For over a decade, astronomers have puzzled over the origins of fast radio bursts, brief blasts of radio waves that come mostly from distant galaxies. Neutrinos could reveal how fast radio bursts are launched |Lisa Grossman |September 16, 2020 |Science News 

After brief introductions, he told me to follow him, but he went the wrong way. How to hunt for star-nosed moles (and their holes) |Kenneth Catania |September 15, 2020 |Popular-Science 

I’m most in love with the liner, which feels like a comfy pair of boxer-briefs even when they get wet. The Gear That Lets Me Enjoy the Last Days of Summer |Graham Averill |September 15, 2020 |Outside Online 

In his brief appearance today, Scalise never mentioned Duke. The Price of Steve Scalise’s Silence |Jason Berry |January 7, 2015 |DAILY BEAST 

Before we get to all that, permit me a brief reflection on this matter of Steve Scalise. Steve Scalise and the Right’s Ridiculous Racial Blame Game |Michael Tomasky |January 2, 2015 |DAILY BEAST 

Hawking, of course, came to global fame with his book A Brief History of Time. Why Can’t Movies Capture Genius? |Clive Irving |December 14, 2014 |DAILY BEAST 

Even when financial facilitators are arrested, incarceration is brief. U.S. Ally Qatar Shelters Jihadi Moneymen |Jamie Dettmer |December 10, 2014 |DAILY BEAST 

Even the brief time spent chewing exposes foods to enzymes that begin to break it down. ‘Rectal Feeding’ Has Nothing to Do with Nutrition, Everything to Do with Torture |Russell Saunders |December 10, 2014 |DAILY BEAST 

Vicars' wives had come and gone, but all had submitted, some after a brief struggle, to old Mrs. Wurzel's sway. The Pit Town Coronet, Volume I (of 3) |Charles James Wills 

As Perker said this, he looked towards the door, with an evident desire to render the leave-taking as brief as possible. The Posthumous Papers of the Pickwick Club, v. 2(of 2) |Charles Dickens 

And I finished all with a brief historical account of affairs and events in England for about a hundred years past. Gulliver's Travels |Jonathan Swift 

The events which succeeded this fortunate capture are too well known to require more than a very brief recapitulation. Blackwood's Edinburgh Magazine, No. CCCXXXIX. January, 1844. Vol. LV. |Various 

In brief, by the close of the year, the phenomenal conditions growing directly out of the European war had been met and overcome. Readings in Money and Banking |Chester Arthur Phillips