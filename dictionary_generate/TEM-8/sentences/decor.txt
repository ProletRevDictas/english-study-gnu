This side piece with similar décor will offer a morning-to-evening coffee program, with a smaller menu based on Philotimo’s. 8 new D.C. restaurants to try this spring |Evan Caplan |March 11, 2022 |Washington Blade 

They can manufacture pieces in blue, red, green, and even in custom colors to match other items in your décor. Leather and lace in your home decor |Valerie Blake |January 15, 2022 |Washington Blade 

BESTINNKITSThis comes in more than 30 designs, so you don’t need to worry about it not matching the rest of your home décor. The best candle warmers for the perfect melt |Pam Collins |November 24, 2021 |Popular-Science 

Sleek and modern, the TaoTronics Home Air Purifier can fit with any décor for any room that you place it in. Get rid of 99.7 percent of indoor pollutants with this air purifier |Quinn Gawronski |July 8, 2021 |Popular-Science 

To achieve this, designers can incorporate dampening materials such as rubber or mossy plants into structures and décor to reduce echo. Designing spaces with marginalized people in mind makes them better for everyone |Eleanor Cummins |March 2, 2021 |Popular-Science 

Now, we're in a very small, very hot room with no decor—there is only an ominous-looking vault door. The Secret Speakeasies of Buenos Aires |Jeff Campagna |February 25, 2014 |DAILY BEAST 

It makes clear that, from the beginning, decor went hand-in-hand with serious modernist art. Tinted Love |Blake Gopnik |October 4, 2013 |DAILY BEAST 

The decor and vibe are cool and contemporary, but still warm and inviting. Fresh Picks |Gina DePalma |August 24, 2010 |DAILY BEAST 

I am so happy to read the above letters especially from MVL and Decor. Dad Just Moved In and He's Driving Me Insane |The Daily Beast |October 25, 2008 |DAILY BEAST 

It went along with the ersatz rustic decor of the rest of the Sky Hi Club. Vigorish |Gordon Randall Garrett 

Clearly Vitruvius's rule of decor had not restrained even the publicists of Palladianism. The Art of Architecture |Anonymous 

Hugo has much to say of the pulchritudo and the decor of the creature-world. The Mediaeval Mind (Volume II of II) |Henry Osborn Taylor 

She had taken expensive rooms in a good location, and furnished them with the assistance of a decor store. K |Mary Roberts Rinehart 

This was the decor of convention that Madame Sans Gene rendered classic. Royal Palaces and Parks of France |Milburg Francisco Mansfield