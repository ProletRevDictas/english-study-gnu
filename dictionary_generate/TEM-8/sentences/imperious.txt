Here are the many results of Week 124, complete with an introduction in which I clearly was trying to establish a snotty, imperious tone. Style Conversational Week 1463, with our guest Kyle Stonversational |Pat Myers |November 18, 2021 |Washington Post 

As Charles, Roe Hartrampf is all imperious whining in mouse-that-roared uniforms and Savile Row suits. In time for Thanksgiving, Broadway serves up a turkey: ‘Diana’ the musical |Peter Marks |November 18, 2021 |Washington Post 

If it’s a couple on a date, he would be imperious and try to intimidate the young man into spending a lot. How to Get Anyone to Do Anything (Ep. 463) |Stephen J. Dubner |May 27, 2021 |Freakonomics 

Facebook’s ad ban and its imperious claim to objectivity are symptoms of the company’s misguided understanding of what America needs from social media. Did Facebook ‘cancel Abe Lincoln’? The truth is complicated — and alarming. |Ron Charles |March 4, 2021 |Washington Post 

Blind almost from birth, Davis was nonetheless an imposing human being—in photographs and on film, he appears imperious, kinglike. Blues Musicians in Unmarked Graves Are Finally Getting Some Respect |Malcolm Jones |January 12, 2014 |DAILY BEAST 

In the following issue, The Group was the subject of a snide, imperious review by Norman Mailer. American Dreams, 1963: ‘The Group’ by Mary McCarthy |Nathaniel Rich |July 25, 2013 |DAILY BEAST 

Imperious despot, insolent in strife, Lover of ruin, enemy of life! Egypt's Revolutionary Poetry |Josh Dzieza |February 3, 2011 |DAILY BEAST 

And while Chasen could be imperious, “road rage was not her thing.” New Clue in Chasen Murder |A. L. Bardach |December 6, 2010 |DAILY BEAST 

A trademark outfit is vital to sending out the right signals of imperious power as real-life dictators understand all too well. Villains as Fashion Heroes |Sean Macaulay |July 8, 2010 |DAILY BEAST 

Edna was a trifle embarrassed at being thus signaled out for the imperious little woman's favor. The Awakening and Selected Short Stories |Kate Chopin 

He was a refuge from herself; in his imperious demands her memory slept, her depths were stagnant. Ancestors |Gertrude Atherton 

He paused, and to illustrate the imperious humor of the Scot, he waved his fingers and a red wrister at me. The Soldier of the Valley |Nelson Lloyd 

She might have settled down for life on Russian Hill, so completely did she make the new environment fit her imperious person. Ancestors |Gertrude Atherton 

Marriage, and later the birth of his son had softened Armand Aubigny's imperious and exacting nature greatly. The Awakening and Selected Short Stories |Kate Chopin