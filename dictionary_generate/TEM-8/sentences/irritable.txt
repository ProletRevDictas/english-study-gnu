Chronic stress increases the risk of cardiovascular disease, irritable bowel disease, obesity, depression, asthma, arthritis, autoimmune diseases, cardiovascular disease, cancer, diabetes, neurological disorders and obesity. They Don’t Come As Pills, But Try These 6 Under-Prescribed Lifestyle Medicines For A Better, Longer Life |LGBTQ-Editor |December 23, 2021 |No Straight News 

Normally even-keeled, he was irritable and short-tempered, including with her son — and Huddleston’s soon-to-be stepson — Allan Osborn. In the shadow of the towers: Five lives and a world transformed |Joby Warrick, Souad Mekhennet |September 2, 2021 |Washington Post 

As the day approached, my daughter grew more and more irritable. Kids are feeling anxiety about a ‘return to normal’ |Melinda Wenner Moyer |June 24, 2021 |Washington Post 

Take, for example, irritable bowel syndrome (IBS), a condition that affects some 15 percent of Americans. Clinton Doc: This Is How We’ll Fix Health Care |Daniela Drake |June 12, 2014 |DAILY BEAST 

But new research shows it is indeed real, and may be the cause of asthma, irritable bowel syndrome, arthritis, and more. New Research Shows Poorly Understood “Leaky Gut Syndrome” Is Real, May Be the Cause of Several Diseases |Daniela Drake |March 27, 2014 |DAILY BEAST 

FODMAP foods can worsen irritable bowel symptoms for many of the 15 percent of Americans that suffer from the condition. Pizza Might Be Your Enemy |Daniela Drake |March 9, 2014 |DAILY BEAST 

She was irritable and ornery almost all of the time, and she seemed to be trying to distance herself from the others. Inside Tania Head’s Terrible 9/11 Lie: ‘The Woman Who Wasn’t There’ |Robin Gaby Fisher, Angelo J. Guglielmo, Jr. |April 6, 2012 |DAILY BEAST 

And he came across as irritable and snarky when asked to provide a succinct answer on how to address health care. Newt Gingrich Will Be Everyone's Target on the Next Debate |Mark McKinnon |December 10, 2011 |DAILY BEAST 

He became irritable, distressed, and anxious—struggled hard to get the needful sum together, struggled and strove; but failed. Blackwood's Edinburgh Magazine, No. CCCXXXIX. January, 1844. Vol. LV. |Various 

As, during the whole pepper-harvest, they feed wholly on this stimulant, they become exceedingly irritable. The Book of Anecdotes and Budget of Fun; |Various 

There was also a moral reaction, and the boy became capricious, irritable, and unlike his former self. The Life &amp; Letters of Peter Ilich Tchaikovsky |Modeste Tchaikovsky 

There are few greater annoyances of life than an irritable woman, rendered doubly morose by the infirmities of years. Madame Roland, Makers of History |John S. C. Abbott 

The Colonel, who was suffering from an attack of rheumatic gout, was more irritable than usual. The Cromptons |Mary J. Holmes