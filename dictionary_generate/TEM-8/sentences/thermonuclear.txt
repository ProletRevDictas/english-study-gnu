On January 27, 1996, France conducted Xouthos, its 210th and final live nuclear test, detonating a thermonuclear warhead beneath the Fangataufa lagoon in the southern Pacific Ocean. Inside France’s super-cooled, laser-powered nuclear test lab |Kelsey D. Atherton |August 1, 2022 |Popular-Science 

The sun roils with heat as thermonuclear reactions in its center produce high amounts of energy. How worried should we be about solar flares and space weather? |Eva Botkin-Kowacki |July 31, 2022 |Popular-Science 

Climate change threatens human lives and livelihoods at a scale perhaps unmatched by any other issue short of global thermonuclear war, and governments haven’t come anywhere close to adequately addressing it despite decades of efforts. The Manchin-Schumer Deal May Have Saved the Climate Fight |Justin Worland |July 29, 2022 |Time 

The United States has allocated over $400 million for research in controlled thermonuclear reactors to date and the USSR more than twice that amount. From the archives: How we covered fusion power |The Editors |February 23, 2022 |MIT Technology Review 

A fusion reactor, or a fusion power plant or thermonuclear reactor, is a device that scientists can use to create electrical power from the energy released in a nuclear fusion reaction. Energy from nuclear fusion just got a little bit more feasible |Angely Mercado |January 11, 2022 |Popular-Science 

If that strikes us as tiresome and tedious, we might as well just hang it up and trigger some global thermonuclear war. In Defense of the Selfie, Oxford English Dictionary’s Word of the Year |James Poulos |November 20, 2013 |DAILY BEAST 

There need to be perfect conditions for the thermonuclear reaction to take place. David Baldacci Has a New Hero |Janice Kaplan |November 2, 2011 |DAILY BEAST 

Any deal is good in that it averts thermonuclear economic collapse. The Debt Deal Will Be Painful |Michael Tomasky |July 15, 2011 |DAILY BEAST 

The daily brief said nothing about widespread plutonium dispersal or about the lost thermonuclear bomb. America's Secret Nuclear Test Revealed in Area 51 |Annie Jacobsen |May 13, 2011 |DAILY BEAST 

A second group worked to locate the lost thermonuclear bomb, called a “broken arrow” in Defense Department terms. America's Secret Nuclear Test Revealed in Area 51 |Annie Jacobsen |May 13, 2011 |DAILY BEAST 

Larch put off another entertainment of small stuff, with a fifty megaton thermonuclear, viewscreen-piloted, among them. Space Viking |Henry Beam Piper 

In that year, too, tests were made at Eniwetok preliminary to the detonation of the first thermonuclear device. Atoms, Nature, and Man |Neal O. Hines 

Project Sedan, an underground thermonuclear detonation in 1962, established conditions for one such study. Atoms, Nature, and Man |Neal O. Hines 

Thermonuclear weapons, complemented over time by strong conventional forces, threatened societal damage to Russia. Shock and Awe |Harlan K. Ullman 

It also included immersion in technology and systems from thermonuclear weapons to advanced weapons software. Shock and Awe |Harlan K. Ullman