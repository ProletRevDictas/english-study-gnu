While conceding that the story was “much bigger than Eileen Gu,” Fox News host Will Cain told Tucker Carlson Tonight that it was “ungrateful” of Gu to “betray” and “turn her back” on America. Why Winter Olympic Athletes Like Eileen Gu Are Getting Caught Up in U.S.-China Tensions |Amy Gunia / Hong Kong |February 11, 2022 |Time 

I don’t mean to sound ungrateful, but I am tired of coming home with bags of trinket-y stuff that lasts for a few days before it becomes plastic trash. The pandemic ended big gift exchanges. Here’s what we’re doing instead. |Abha Bhattarai |December 20, 2021 |Washington Post 

So it was very hard for me to accept that I was struggling because I didn’t want to seem ungrateful. How Olympians Are Fighting to Put Athletes' Mental Health First |Alice Park |July 22, 2021 |Time 

Republicans are furious that corporations appear so ungrateful. Republicans are learning that there’s more to capitalism than tax cuts |Catherine Rampell |April 8, 2021 |Washington Post 

I wanted to take it back, but he pouted so much about how ungrateful I was that I just let it go. Carolyn Hax: She’s not charmed by boyfriend’s unwanted gift |Carolyn Hax |April 1, 2021 |Washington Post 

“This may sound ungrateful but if you want to protect yourself, you may want to stay away,” she continues. Anime King Hayao Miyazaki’s Cursed Dreams |Melissa Leon |December 2, 2014 |DAILY BEAST 

I know one shouldn't be ungrateful, but what is it about people giving your kids ridiculous presents when you are on holiday? Prince George Given Giant Toy Wombat |Tom Sykes |April 16, 2014 |DAILY BEAST 

There will be tears, ungrateful betrayals, but at some point—if you keep your faith—the problem child will grow up. Juvenilia: A Bibliography |Eric Puchner |February 15, 2010 |DAILY BEAST 

Small children are ungrateful; to do one a favor is, from a business point of view, about as shrewd as making a subprime mortgage. Are the Glory Days of Fatherhood Over? |Michael Lewis |June 20, 2009 |DAILY BEAST 

No, no,” added Mr. Pickwick more cheerfully, “it would be selfish and ungrateful. The Posthumous Papers of the Pickwick Club, v. 2(of 2) |Charles Dickens 

"This is the most ungrateful outfit of greasers I ever met up with," cried Clip, hurrying toward the second man. Motor Matt's "Century" Run |Stanley R. Matthews 

I am grateful—voyons—if anybody ever says Aristide Pujol is ungrateful, he is a liar. The Joyous Adventures of Aristide Pujol |William J. Locke 

Somehow or other the maternal rôle, which had been so suddenly thrown upon Georgina, had become not ungrateful to her. The Pit Town Coronet, Volume II (of 3) |Charles James Wills 

It is a sweet revenge to a man who loves passionately to make an ungrateful mistress appear still more so, by his very actions. The 'Characters' of Jean de La Bruyre |Jean de La Bruyre