Nearby, hoarfrost shimmered on hemlock branches, a bear cub peered out from a hole in the snow, and a sudden, terrifying silence echoed in the ears of the man’s ski partners. It’s No Fun to Wake a Sleeping Bear |jversteegh |August 11, 2021 |Outside Online 

Defined by thick hardwood and hemlock forests speckled across its 8,633 acres, the area feels a world away from Pittsburgh, despite being only 130 miles away. 50 U.S. Wilderness Areas, Ranked |eriley |July 26, 2021 |Outside Online 

You’ll sleep under towering hemlocks and pines along the shores of the scenic ten-mile lake, perfect for spying local wildlife including moose. The Best Campground in Every State |thodgson |July 12, 2021 |Outside Online 

We walked up the creek and made camp in a little stand of sycamore and hemlock that was nestled into the curve of a ridge. Sebastian Junger on Walking America’s Railroads |Sebastian Junger |May 19, 2021 |Outside Online 

On a hazy August afternoon, a troupe of excited twentysomethings fanned out into the muggy shade of oaks and hemlocks in the woods of central Pennsylvania. Meet the Renaissance Man Obsessed with Mushrooms |Doug Bierend |March 10, 2021 |Outside Online 

Upstairs, in the living room, splintered logs of hemlock cackled and spat from inside the wood stove. Dungeons and Genital Clamps: Inside a Legendary BDSM Chateau |Ian Frisch |December 20, 2014 |DAILY BEAST 

In the interim, Herring took a short break from the band to dabble in his side-project—rapping under the moniker Hemlock Ernst. Future Islands Frontman Samuel T. Herring on Their 11-Year Journey to Letterman and Viral Stardom |Marlow Stern |April 3, 2014 |DAILY BEAST 

He figures on starting in early March, likely in Hemlock Alley, off Polk Street, near a present needle exchange. Clean Crack Pipes for All! San Francisco’s Final Social Justice Frontier |Michael Daly |January 31, 2014 |DAILY BEAST 

But Hemlock Grove does not feel like a step in the right direction. ‘Hemlock Grove’: Netflix’s Latest Original Show Is Scary Bad |Jace Lacob |April 19, 2013 |DAILY BEAST 

Horror aficionados may lap this up, but for me, Hemlock Grove is about as appealing as curdled milk. ‘Hemlock Grove’: Netflix’s Latest Original Show Is Scary Bad |Jace Lacob |April 19, 2013 |DAILY BEAST 

A hemlock had fallen athwart it, and they sat down where they could look out upon a majestic panorama of towering rock and snow. The Gold Trail |Harold Bindloss 

Moss was placed between the logs to keep out the wind, and a thick roof was made from hemlock boughs. Stories the Iroquois Tell Their Children |Mabel Powers 

The clear, colorless distillate had an alkaline reaction, and an odor resembling henbane or hemlock. Some Constituents of the Poison Ivy Plant: (Rhus Toxicodendron) |William Anderson Syme 

Pine or hemlock boughs are a good substitute for tar, and afford a most healthful change in the winter-food of sheep. Domestic Animals |Richard L. Allen 

Now condemned to death, Socrates was put into prison, where some days after, he died by drinking the poison hemlock. Great Men and Famous Women. Vol. 3 of 8 |Various