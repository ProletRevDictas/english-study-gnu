Finally, some biologists argue for preserving vast tracts of wilderness not yet altered by human activity. Can people protect as much space as nature needs? |Jonathan Lambert |January 21, 2021 |Science News For Students 

As soon as it gets turned on by one virus, any other virus that comes along and tries to grow in the respiratory tract can’t. Coronavirus shutdowns have quashed nearly all other common viruses. But scientists say a rebound is coming. |Dan Hurley |January 12, 2021 |Washington Post 

It’s just that despite years of research into the use of vitamin D in respiratory tract infections, there still hasn’t really been a clear, slam-dunk answer that there’s benefit. Vitamin D sales are up. But experts still don’t know whether it can prevent or treat covid. |Allyson Chiu |January 11, 2021 |Washington Post 

The Nature Conservancy partnered with the American Forest Foundation to create a new offset protocol designed to allow owners of small tracts of wooded land to earn credits for taking steps to suck up and store more carbon. How Amazon’s offsets could exaggerate its progress toward “net zero” emissions |James Temple |November 2, 2020 |MIT Technology Review 

In other words, “Knives Out” sought to work as entertainment, not as a social tract or policy prescription. Movies are rushing to impact the election. Don’t ask whether they’ll work. Ask whether they’ll last. |Ann Hornaday |October 30, 2020 |Washington Post 

The gastrointestinal (GI) tract performs different digestive functions are various different locations. ‘Rectal Feeding’ Has Nothing to Do with Nutrition, Everything to Do with Torture |Russell Saunders |December 10, 2014 |DAILY BEAST 

On a 2,813-acre tract roughly 30 miles west, Washington found a Calvinist sect called the Seceders squatting on his land. Washington’s Wheeler-Dealer Patriotism |William O’Connor |October 31, 2014 |DAILY BEAST 

A procedure to reopen his urinary tract could have been done under local anesthesia. Ayatollah Khamenei’s Cancer Scare |IranWire |September 20, 2014 |DAILY BEAST 

Please note that I made a contribution after reading the tract, i.e., I too am a hug-a-whale sort of guy. Blame Climate Change for Your Terrible Seasonal Allergies |Kent Sepkowitz |May 14, 2014 |DAILY BEAST 

For Coming Soon, Gordon's initial plan was to make and then display her wreath paintings in a low-budget California tract house. Kim Gordon: Going Solo After Sonic Youth, and Why She Identifies With ‘Girls’ |Andrew Romano |April 10, 2014 |DAILY BEAST 

It is often present in the respiratory tract under normal conditions. A Manual of Clinical Diagnosis |James Campbell Todd 

Their usual source is the deeper layers of the urinary tract, especially of the bladder. A Manual of Clinical Diagnosis |James Campbell Todd 

In suppurations of the urinary tract pus-producing organisms may be found. A Manual of Clinical Diagnosis |James Campbell Todd 

Barclay, in his tract on "The Vertues of Tobacco," recommends its use as a medicine. Tobacco; Its History, Varieties, Culture, Manufacture and Commerce |E. R. Billings. 

Then I ditch from the lake, and I am the proud owner of a large tract of valuable irrigated land. Ancestors |Gertrude Atherton