The risks of opening are uncertain, but the benefits are clear. I’m an epidemiologist and a dad. Here’s why I think schools should reopen. |Benjamin P. Linas |July 9, 2020 |Vox 

One benefit of this setup, called a “stepped wedge trial,” is that it doesn’t relegate one block of individuals into a control group that goes without training for the duration of the study. There’s little evidence showing which police reforms work |Sujata Gupta |July 9, 2020 |Science News 

Unnecessary state occupational licenses—often costly, time-consuming, and offering little benefit to consumers—should be eliminated. 4 ways to close America’s huge racial ‘opportunity gap’ |matthewheimer |July 9, 2020 |Fortune 

There is one additional Flex 5G feature that sets it apart from competitors, and is both a big benefit and a big downside. Review: The Lenovo Flex 5G, the world’s first 5G laptop |Aaron Pressman |July 8, 2020 |Fortune 

Praising the benefits of physical exercise helps little when jogging in a nearby neighborhood could cost your life. These 5 numbers tell you everything you need to know about racial disparities in health care |matthewheimer |July 8, 2020 |Fortune 

Two-thirds of those who likely to benefit from the new policy are Mexican. Why Mexicans Are Enraged by Obama’s Big Tuesday Meeting |Ruben Navarrette Jr. |January 6, 2015 |DAILY BEAST 

Not for the benefit of the harasser, of course, but for your own safety. Cover-Ups and Concern Trolls: Actually, It's About Ethics in Suicide Journalism |Arthur Chu |January 3, 2015 |DAILY BEAST 

When the audience laughed he added that, “They think freedom would benefit them but they were cheated.” 50 Shades of Iran: The Mullahs’ Kinky Fantasies about Sex in the West |IranWire, Shima Sharabi |January 1, 2015 |DAILY BEAST 

And in either case, “the significant benefit from allowing Wi-Fi hotspots outweighs these concerns.” How ‘Ethical’ Hotel Chain Marriott Gouges Guests in the Name of Wi-Fi Security |Kyle Chayka |December 31, 2014 |DAILY BEAST 

“Hence, there might be a net benefit, at least to some females, of breeding within the natal group,” the researchers speculate. Mongooses, Meerkats, and Ants, Oh My! Why Some Animals Keep Mating All in the Family |Helen Thompson |December 29, 2014 |DAILY BEAST 

As he walked back to his hotel, his head was full of plans for the girl's transient pleasure and lasting benefit. Rosemary in Search of a Father |C. N. Williamson 

Mr. Spurrell came down to see a horse, and we shall be very glad to have the benefit of his opinion by-and-by. Punch, or the London Charivari, Volume 107, November 3, 1894 |Various 

These oral inanities only served to make Lyn give me the benefit of a look of amused wonder. Raw Gold |Bertrand W. Sinclair 

Once he permitted himself a digression, that he might point a moral for the benefit of his servant. St. Martin's Summer |Rafael Sabatini 

This lesson in figures is given for the benefit of those who have not yet mastered Numeric Thinking. Assimilative Memory |Marcus Dwight Larrowe (AKA Prof. A. Loisette)