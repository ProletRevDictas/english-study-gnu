That is roughly in line with a Post average of recent polls from the state that put Biden’s advantage at seven points. Election live updates: Trump returns to Wisconsin; Biden to face live audience at town hall |Colby Itkowitz, Felicia Sonmez, John Wagner |September 17, 2020 |Washington Post 

FiveThirtyEight’s average of state polls at the end of the 2016 race gave Clinton a five-point lead in Wisconsin and an eight-point lead in Minnesota. The key shifts in Minnesota and Wisconsin that have improved Biden’s chances of victory |Philip Bump |September 16, 2020 |Washington Post 

Since the lockout of 2004-05, five cup winners have averaged fewer goals per game. Teams Don’t Win The Stanley Cup With A Goal Deficit. Can The Dallas Stars Change That? |Terrence Doyle |September 16, 2020 |FiveThirtyEight 

The Wisconsin poll is consistent with other recent polls in the state, with The Post’s average showing Biden’s margin at seven points, narrower than in midsummer but not much different from what it was immediately after the GOP convention. Post-ABC Wisconsin poll shows Biden holding narrow edge over Trump |Dan Balz, Emily Guskin |September 16, 2020 |Washington Post 

That is a somewhat larger margin than a Washington Post average of recent polls in the state, which shows Biden’s lead to be seven percentage points. Trump, in town hall, says he wouldn’t have done anything differently on pandemic |Colby Itkowitz, Josh Dawsey, Felicia Sonmez, John Wagner |September 16, 2020 |Washington Post 

Well over a thousand holes in, I average less than four strokes per hole. Lost For Thousands of Strokes: 'Desert Golfing' Is 'Angry Birds' as Modern Art |Alec Kubas-Meyer |January 2, 2015 |DAILY BEAST 

On average, the vaccine has an efficacy of about 60 percent. When You Get the Flu This Winter, You Can Blame Anti-Vaxxers |Kent Sepkowitz |January 1, 2015 |DAILY BEAST 

Average age ranges from 45 to 65, with her youngest client at 18 and the oldest in her 80s. Inside A Finishing School for Transwomen |Sharon Adarlo |December 27, 2014 |DAILY BEAST 

Ramos was 38—nearly two decades older than the average recruit. In The Shadow of Murdered Cops |Michael Daly |December 26, 2014 |DAILY BEAST 

All because Murthy believes that gun violence, which kills an average of 86 Americans every day, is a public health issue. The NRA’s Twisted List for Santa |John Feinblatt |December 23, 2014 |DAILY BEAST 

I do not think the average number of passengers on a corresponding route in our country could be so few as twenty. Glances at Europe |Horace Greeley 

Though the average speaker is generally limited by one type of voice, which he varies somewhat, it is not often disguised. Expressive Voice Culture |Jessie Eldridge Southwick 

I should judge that a peck of corn is about the average product of a day's work through all this region. Glances at Europe |Horace Greeley 

The average citizen of three generations ago was probably not aware that he was an extreme individualist. The Unsolved Riddle of Social Justice |Stephen Leacock 

He was a pretty bright sort, that same Goodell, quick-witted, nimble of tongue above the average Englishman. Raw Gold |Bertrand W. Sinclair