Rover engineers say they also hope to hear the wheels crunch as the rover drives. Watch real video of Perseverance’s Mars landing |Lisa Grossman |February 22, 2021 |Science News 

“We found the parking lot and hit it,” Chen said, avoiding dangerous terrain that was all but certain to doom the rover. NASA rover Perseverance lands on Mars in mission to search for past life |Joel Achenbach, Sarah Kaplan, Ben Guarino |February 18, 2021 |Washington Post 

Onboard systems tracked the surface for hazards during descent and steered the rover away from any threats. This is the first image taken by NASA’s Perseverance Mars rover. Now the hunt for life begins. |Neel Patel |February 18, 2021 |MIT Technology Review 

Mars is far enough away from Earth that any radio signal we send the rover takes seven minutes to reach Mars. NASA’s Perseverance rover landed safely on Mars. Now it will look for signs of ancient life. |Brian Resnick |February 18, 2021 |Vox 

The voyage culminated in the skycrane gently lowering the rover to the surface before rocketing off to land at a safe distance. Perseverance rover has landed safely on Mars |John Timmer |February 18, 2021 |Ars Technica 

However, several probes—most recently the Curiosity rover—have measured methane in the Martian atmosphere. Methane on Mars: Life or Just Gas? |Matthew R. Francis |December 17, 2014 |DAILY BEAST 

Jeep steadily gave up a market it had created to rivals, particularly Toyota and Range Rover. Nationalism on Four Wheels |Clive Irving |October 18, 2014 |DAILY BEAST 

But the runaway best example of the game is another cluster of British luxury vehicles—Range Rover, Land Rover and Jaguar. Nationalism on Four Wheels |Clive Irving |October 18, 2014 |DAILY BEAST 

The prototype Land Rover was designed by a Jeep owner and built on a Jeep chassis. Nationalism on Four Wheels |Clive Irving |October 18, 2014 |DAILY BEAST 

Knee deep in mud, sweat mixing with rain, they forced the Land Rover through the jungle. The Original Ebola Hunter |Abby Haglage |September 14, 2014 |DAILY BEAST 

The great big dog is named Rover, the middle-sized one Brownie, and the little yellow curly one Wienerwurst. Seven O'Clock Stories |Robert Gordon Anderson 

But once when Hepzebiah fell in the pond after her doll, Rover swam in and caught her dress in his mouth and brought her to shore. Seven O'Clock Stories |Robert Gordon Anderson 

Under the big oak by the brook sat the three happy children with Rover, Brownie, and little yellow Wienerwurst. Seven O'Clock Stories |Robert Gordon Anderson 

Rover and Brownie and Wienerwurst lay curled up in their kennels, with their eyes tight shut. Seven O'Clock Stories |Robert Gordon Anderson 

A strange word for the old rover; but we all have a taste for home and the home-like, disguise it how we may. Tales and Fantasies |Robert Louis Stevenson