He said that Deep Longevity is in discussions with several insurance companies, which he declined to name, about future partnerships. What’s your biological age? A new app promises to reveal it—and help you slow the aging process |Jeremy Kahn |September 17, 2020 |Fortune 

Leading discussions about the global rules to regulate digital privacy and surveillance is a somewhat unusual role for a developing country to play. Podcast: COVID-19 is helping turn Brazil into a surveillance state |Anthony Green |September 16, 2020 |MIT Technology Review 

Following a long discussion and a lot of consideration the membership voted to cancel MAL 2021. D.C. Mid-Atlantic Leather Weekend 2021 cancelled due to COVID |Lou Chibbaro Jr. |September 15, 2020 |Washington Blade 

You could reasonably argue the time has already come to have the discussion on Harden. Everything Should Be On The Table For The Houston Rockets. Even James Harden’s Future. |Chris Herring (chris.herring@fivethirtyeight.com) |September 14, 2020 |FiveThirtyEight 

We host digital AI ethics meetups, which are open discussions that anyone with an internet connection or phone can join. AI ethics groups are repeating one of society’s classic mistakes |Amy Nordrum |September 14, 2020 |MIT Technology Review 

Other FBI officials joined the discussion via conference call, he said. FBI Won’t Stop Blaming North Korea for Sony Hack -- Despite New Evidence |Shane Harris |December 30, 2014 |DAILY BEAST 

The Vatican then hosted a final, secret discussion between the two delegations this fall in Rome. The Pope's Diplomatic Miracle: Ending the U.S.-Cuba Cold War |Barbie Latza Nadeau |December 17, 2014 |DAILY BEAST 

In the 1960 campaign between Richard Nixon and John F. Kennedy, there was virtually no discussion of crime. The GOP and Police Unions: A Love Story |Eleanor Clift |December 12, 2014 |DAILY BEAST 

Men may opt out of the discussion rather than risk saying something wrong. Tech’s Male ‘Feminists’ Aren’t Helping |Cate Huston, Karen Catlin |December 8, 2014 |DAILY BEAST 

Although in at least one instance, there was discussion of saving black-owned shops. Raging Protesters Set Ferguson on Fire |Justin Glawe |November 25, 2014 |DAILY BEAST 

But you will find most colleges and most college societies bar religious instruction and discussion. The Salvaging Of Civilisation |H. G. (Herbert George) Wells 

But not only has the name tobacco and the implements employed in its use caused much discussion but also the origin of the plant. Tobacco; Its History, Varieties, Culture, Manufacture and Commerce |E. R. Billings. 

So far as their thought is still alive these men will come into the discussion of living questions now. The Salvaging Of Civilisation |H. G. (Herbert George) Wells 

For example, there is a vast discussion afoot upon the questions that centre upon Property, its rights and its limitations. The Salvaging Of Civilisation |H. G. (Herbert George) Wells 

Free discussion—never a very free thing in Russia—has now on any general scale become quite impossible. The Salvaging Of Civilisation |H. G. (Herbert George) Wells