Larger tires provide more ground clearance, but also can make the machine more susceptible to tipping and can require a clutch kit to work properly. Six ways to make your ATV even more rugged |By Tyler Freel/Outdoor Life |September 17, 2020 |Popular-Science 

The “necessity” argument may be clutching at straws for companies that, as many do, use their customers’ data for other purposes than the core services they provide. Time is running out for Big Tech’s monetization of Europeans’ personal data |David Meyer |September 10, 2020 |Fortune 

As briefly mentioned before, different displacements are also paired with clutch systems that accommodate the skill level of the intended rider. Your kid wants a dirt bike. Here’s what to buy them. |By Serena Bleeker/Dirt Rider |September 4, 2020 |Popular-Science 

For his part, Anthony — who has redeemed himself in Portland after an abrupt exit from Houston, including by hitting a number of clutch triples late since the restart31 — said his bad-word boarding is mostly a way of hyping himself up. Carmelo Anthony’s On-Court Cussing Has TV Networks Playing Defense |Chris Herring (chris.herring@fivethirtyeight.com) |August 18, 2020 |FiveThirtyEight 

The researchers analyzed a clutch of dinosaur eggs found in Mongolia. Early dinosaurs may have laid soft-shelled eggs |Jack J. Lee |August 3, 2020 |Science News For Students 

Who knew that a competition where you clutch the hand of another man and lock eyes across a table could be this damn gay. High-End Pervs Film Benedict Cumberbatch and Reese Witherspoon Sucking Face |Amy Zimmerman |December 11, 2014 |DAILY BEAST 

Available at La Boîte SHOLDIT Clutch Wrap Purse, $70 We can all agree the dorky passport holders and money bags have got to go. The Daily Beast’s 2014 Holiday Gift Guide: For the Anthony Bourdain in Your Life |Allison McNearney |November 29, 2014 |DAILY BEAST 

At Oscar after-parties, movie stars clutch In-N-Out burgers in one hand and gilded trophies in the other. My Big, Buttery Lobster Roll Rumble: We Came, We Clawed, We Conquered |Scott Bixby |June 7, 2014 |DAILY BEAST 

The Kentucky freshman sunk his third game-winning three-pointer in a row, launching fresh claims about his ‘clutch gene.’ Was Aaron Harrison’s Game-Winning Three-Pointer ‘Clutch’? |Robert Silverman |April 7, 2014 |DAILY BEAST 

Take those two years away, and his lifetime clutch rating is essentially zero. Was Aaron Harrison’s Game-Winning Three-Pointer ‘Clutch’? |Robert Silverman |April 7, 2014 |DAILY BEAST 

While he stood, apparently quiescent, in the clutch of his adversary, he still held his hand on his sword. The Pastor's Fire-side Vol. 3 of 4 |Jane Porter 

It is probable he wished to provide written proof of a plea that he was an unwilling agent in the clutch of a mutinous army. The Red Year |Louis Tracy 

Black Hood let the clutch slap in and the roadster bounded back onto the tarvia drive. Hooded Detective, Volume III No. 2, January, 1942 |Various 

A quick impulse that was somewhat spasmodic impelled her fingers to close in a sort of clutch upon his hand. The Awakening and Selected Short Stories |Kate Chopin 

And the nightmare clutch laid hold upon his heart with giant pincers. The Wave |Algernon Blackwood