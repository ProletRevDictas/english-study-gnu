This, after all, is where a beachfront estate originally built for the Ford family was recently listed for $145 million. That splashy Chainsmokers concert in the Hamptons raised all of $152,000 for charity |radmarya |August 25, 2020 |Fortune 

In 2014, James Seifert, the company’s real estate chief, testified to the California Public Utilities Commission that part of the reason the employees were leaving the building was because it needed significant capital improvements. How the City Came to Lease a Lemon |Lisa Halverstadt and Jesse Marx |August 10, 2020 |Voice of San Diego 

Vacchi was not directly involved in the real estate debacle. City Official Managing Coronavirus Response Abruptly Left Last Month |Lisa Halverstadt |August 5, 2020 |Voice of San Diego 

City insiders hoped Thompson, who had spent more than two decades in the commercial real estate business, could address some of the department’s long-running challenges. City’s Real Estate Assets Director Resigns Amid Scrutiny Over Ash Street Deal |Lisa Halverstadt and Jesse Marx |August 4, 2020 |Voice of San Diego 

It’s another recent example of real estate deals by the city gone awry. VOSD Podcast: 101 Ash St. Is a Fiery Mayor’s Race Issue |Nate John |July 31, 2020 |Voice of San Diego 

Al Qaeda has never managed to carve out a large chunk of real estate to call its own—in Afghanistan it was a guest of the Taliban. ISIS’s Futile Quest to Go Legit |Jamie Dettmer |January 5, 2015 |DAILY BEAST 

Last week, property owners were beaten by security guards as they confronted a real-estate developer who defrauded them. China’s Internet Is Freer Than You Think |Brendon Hong |December 27, 2014 |DAILY BEAST 

Plus, the Spey, one of the most famous salmon rivers in the world, bordered the south side of the estate. Ester Elchies, The Estate Built By Whiskey | |December 10, 2014 |DAILY BEAST 

Which is why in 1961, the distillery finally decided to purchase the estate and its adjoining home. Ester Elchies, The Estate Built By Whiskey | |December 10, 2014 |DAILY BEAST 

So the trip to The Macallan estate was sort of a pilgrimage. A Whisky Connoisseur Remembers That First Sip of The Macallan | |December 10, 2014 |DAILY BEAST 

But if what I told him were true, he was still at a loss how a kingdom could run out of its estate like a private person. Gulliver's Travels |Jonathan Swift 

He is dead; but his three sons have the estate yet, and I think they would keep their father's promise to the Indians. Ramona |Helen Hunt Jackson 

The rebellion spread to their district, and many of the natives on and about the estate were eager to join in the movement. The Philippine Islands |John Foreman 

Aunt Ri, at her best estate, had never possessed a room which had the expression of this poor little mud hut of Ramona's. Ramona |Helen Hunt Jackson 

The senior branch of the family being thus extinct the whole of the entailed estate had devolved on me. Uncanny Tales |Various