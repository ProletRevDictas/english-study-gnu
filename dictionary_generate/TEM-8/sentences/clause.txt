So far, TV networks have pushed back against agreeing to these enigmatic clauses. ‘Kind of squishy’: Advertisers lobby to add pandemic clauses to TV upfront deals |Tim Peterson |September 1, 2020 |Digiday 

The contract clause had an end date that was slowly approaching, so the question of when Avatar 2 was expected to release was critical. ‘The Dream Architects’: Inside the making of gaming’s biggest franchises |Rachel King |September 1, 2020 |Fortune 

The document in both cases includes the same clauses placing the burden on the city if things don’t go as planned. The Deal Before the 101 Ash St. Debacle Helps Explain How We Got Here |Lisa Halverstadt and Jesse Marx |August 24, 2020 |Voice of San Diego 

Allied Universal’s contract also includes a clause stating that its video footage is not considered a public record, meaning members of the public need a court order to view footage recorded by more than two-thirds of the agency’s security force. MTS Purged Body Camera Footage Before Man’s Attorney Could Access it |Lisa Halverstadt |July 21, 2020 |Voice of San Diego 

Her practice already had a digital platform compliant with health-related privacy laws and patient release forms that included a clause about telehealth. Teletherapy is finally here to stay |Alexandra Ossola |July 5, 2020 |Quartz 

They would not, for example, supersede federal law regarding the Establishment Clause in the First Amendment. A Field General in the War on Christmas |David Freedlander |December 24, 2014 |DAILY BEAST 

Sanford hits back at Sullivan, who “has certainly not lived up to this clause.” Mark Sanford’s Epic Facebook Overshare |Olivia Nuzzi |September 12, 2014 |DAILY BEAST 

Weird as the theory is, invoking “quantum physics” is not an escape clause from obeying physical laws. Dear NASA: Fuel-Free Rocket Thruster Is Literally Too Good to Be True |Matthew R. Francis |August 4, 2014 |DAILY BEAST 

There may even be a clause in her contract by which she has to agree to certain content restrictions. Sarah Palin Is Perfect for ‘The View’ |Dean Obeidallah |July 15, 2014 |DAILY BEAST 

But an aspect that is particularly troubling is that such a clause exists at all. Sex Workers Deserve Health Care, Too |Tauriq Moosa |May 20, 2014 |DAILY BEAST 

He added that lessening clause, remembering, quite simply, how much more brilliant he was than Nigel. Bella Donna |Robert Hichens 

The Act contained another practical clause, designed to block the construction of lines from political considerations. Fifty Years of Railway Life in England, Scotland and Ireland |Joseph Tatlow 

An insurance policy may be assigned, though it usually contains a clause that the consent of the insurer is needful. Putnam's Handy Law Book for the Layman |Albert Sidney Bolles 

This clause was adopted, and James Madison for the first time attracted state-wide attention to his thinking and philosophy. Hallowed Heritage: The Life of Virginia |Dorothy M. Torpey 

His lordship next proceeded to the ten-pound qualification clause. The History of England in Three Volumes, Vol.III. |E. Farr and E. H. Nolan