It’s a perfect weeklong route for mountain lovers looking to escape the hordes visiting nearby parks and take in a bit of locals-only Washington. The Best Scenic Drive in Every State |eriley |August 26, 2021 |Outside Online 

Merck employed hordes of chemists to produce large quantities of chemical compounds for use in new drugs. Building a better chemical factory—out of microbes |Leigh Buchanan |August 24, 2021 |MIT Technology Review 

Its population explodes from about 5,000 to 250,000 when the vacation hordes descend. Help Wanted: An airbrush artist to save the summer |Todd Frankel |May 28, 2021 |Washington Post 

Every title sticks to the original’s formula of some kind of item management, exploration and puzzle solving — and, crucially, fighting off zombie hordes. Every Resident Evil game, ranked |Gene Park |May 7, 2021 |Washington Post 

It was always there, a shuffling horde of familiar faces dissolved into vague, generic sketches. Fiction: Unpaired |Tim Maughan |April 28, 2021 |MIT Technology Review 

But along with the cartoon funk is an all-too-real story of police brutality embodied by a horde of evil Pigs. ‘Black Dynamite’ Presents Police Brutality: The Musical |Stereo Williams |January 9, 2015 |DAILY BEAST 

Here is a title that, in its prologue, tasks players with fighting a horde of angels on top of a moving jet. Bayonetta Is Nintendo’s Graphic, Ass-Kicking Barbie |Alec Kubas-Meyer |October 24, 2014 |DAILY BEAST 

Perhaps the threat of legal action has also played a role in curbing the horde of dyspeptic deviants. ‘The Fappening’ Is Dead: From A-List Hacking Victims to D-Listers Accused of Leaking Nudes For PR |Marlow Stern |October 18, 2014 |DAILY BEAST 

Mrs. Clooney has been followed around Athens during a three-day visit by a horde of paparazzi that number into the hundreds. Can Amal Clooney Save Greece’s Antiquities? |Nico Hines |October 15, 2014 |DAILY BEAST 

At about 10 p.m., a horde of Hungarian police officers raided the bar, demanding that everybody show their identification. American Racist Richard Spencer Gets to Play the Martyr in Hungary |James Kirchick |October 7, 2014 |DAILY BEAST 

In China the patriarch of a nomad horde became emperor of a nation retaining ancestor worship as its chief religious system. Man And His Ancestor |Charles Morris 

The failure of this horde did not in the least check the proceedings of Sharp or Lauderdale or their like-minded colleagues. Hunted and Harried |R.M. Ballantyne 

In 1810 a threatened attack from a marauding horde of Kafirs was averted in answer to prayer. Robert Moffat |David J. Deane 

It was this inspiration that changed a strong German horde into a people that loved culture, art and education. Ways of War and Peace |Delia Austrian 

He led the Auvergners to the left of the battle, where the Seljouk horde seemed thinnest. God Wills It! |William Stearns Davis