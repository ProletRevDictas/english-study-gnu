In response, the state’s electric grid manager, a private non-profit known as CAISO, said the system was overtaxed and it ordered utilities to cut power to customers, as you may have noticed, for the first time since the 2001 energy crisis. Environment Report: Real Estate Sellers Aren’t Required to Disclose Sea Level Rise Risk |MacKenzie Elmer |August 24, 2020 |Voice of San Diego 

According to Blanchette’s model, the birds arrange themselves as if they were each standing on their own hexagon in a grid. Math of the Penguins |Susan D'Agostino |August 17, 2020 |Quanta Magazine 

These microgrids continue to provide power to smaller communities if their larger grid is shut off. Sacramento Report: Uber vs. California |Voice of San Diego |August 14, 2020 |Voice of San Diego 

It also had to be within a few hours’ drive of JPL, and not totally off the grid — the rover team slept in hotels, ate dinner in restaurants and had reliable Wi-Fi to send data to the Earth team every night. To rehearse Perseverance’s mission, scientists pretended to be a Mars rover |Lisa Grossman |July 29, 2020 |Science News 

Siegele said the consultant based the recommended upfront cost on “bad math” and undershot SDG&E’s true profit from building the grid on public land. Morning Report: How We Got Here With Smart Streetlights |Voice of San Diego |July 10, 2020 |Voice of San Diego 

The sound of birds, quail, even doe, make a wild grid of noise. The Story Behind Lee Marvin’s Liberty Valance Smile |Robert Ward |January 3, 2015 |DAILY BEAST 

It was around noon that Brinsley chucked the phone behind a radiator at the basketball stadium and went off the grid. Alleged Cop Killer Ismaaiyl Brinsley Had a Death Wish |M.L. Nestel |December 22, 2014 |DAILY BEAST 

Skiing would appear to be the ultimate off-the-grid activity. Solar Powered Ski Lift |The Daily Beast |November 24, 2014 |DAILY BEAST 

Several experiments in the emerging vehicle-to-grid sector are underway. Adding Vehicles to the Grid |The Daily Beast |October 8, 2014 |DAILY BEAST 

So it is not surprising that a larger-scale vehicle-to-grid project is underway in Southern California. Adding Vehicles to the Grid |The Daily Beast |October 8, 2014 |DAILY BEAST 

He huddled as close to the grid as he could get, alert to the slightest movement below as the prisoner faced his captors. Star Born |Andre Norton 

On impulse he stopped to measure it, sure he could squeeze through here, if he could work loose the grid. Star Born |Andre Norton 

The return from the grid is by means of the small pipe leading to the top of the large water tank. Langley Memoir on Mechanical Flight, Parts I and II |S. P. (Samuel Pierpont) Langley and Charles M. (Charles Matthews) Manly 

You could tell, because the steel was rusting in the salt air, and the grid shone through the green paint in red-orange. Little Brother |Cory Doctorow 

Then there is the man that pops sugary beans over a charcoal fire, and makes a delicious noise with his shaking grid-like box. A Journal from Japan |Marie Carmichael Stopes