As the machines become more controllable and less error-prone, they become more useful. IBM plans a huge leap in superfast quantum computing by 2023 |rhhackettfortune |September 15, 2020 |Fortune 

Alcántara doesn’t fit with Liverpool’s recent transfer strategy — he’s 29 and injury prone — but he would certainly give the Reds a different look going forward. Will Liverpool Run Away With The Premier League Again, Or Can Manchester City Take The Title Back? |Terrence Doyle |September 10, 2020 |FiveThirtyEight 

There is no doubt that many people would be prone to anthropomorphize even a simple chatbot built with GPT-3. Welcome to the Next Level of Bullshit - Issue 89: The Dark Side |Raphaël Millière |September 9, 2020 |Nautilus 

Here’s an example of the tool in action on a million-dollar property in flood-prone Beverly Hills, Calif. This online tool can tell you if your home is likely to flood this hurricane season |Andrew Nusca |August 26, 2020 |Fortune 

While in the future we may be able to build quantum computers that are less prone to disturbances, in the meantime we will need to find ways to mitigate the errors they cause. New Algorithm Paves the Way Towards Error-Free Quantum Computing |Edd Gent |August 14, 2020 |Singularity Hub 

He was funny and self-effacing, though prone to fits of anger. ‘All Good Cretins Go to Heaven’: Dee Dee Ramone’s Twisted Punk Paintings |Melissa Leon |December 15, 2014 |DAILY BEAST 

That gave a huge advantage to the sunny Republican prone to hugging supporters. Tea Party Firebrand Wins Big in Iowa |Ben Jacobs |November 5, 2014 |DAILY BEAST 

Did her handlers fear what for the gaffe-prone Republican might say? Did Joni Ernst’s Des Moines Register Diss Just Destroy Her ‘Iowa-Nice’? |Ben Jacobs |October 25, 2014 |DAILY BEAST 

Some kids are prone to letting their minds wander and daydreaming. Daydreaming Is Not a Disorder |Russell Saunders |October 4, 2014 |DAILY BEAST 

For people prone to believe doctors like me are part of some malign conspiracy, nothing I say will make a difference. Vaccines Are Poison, Cellphones Cause Cancer, and Other Medical Conspiracies |Russell Saunders |July 18, 2014 |DAILY BEAST 

Last night I saw Jean Baptiste lying prone upon the floor, and knew that she had beaten him down to it, and he had not resisted. The Homesteader |Oscar Micheaux 

He was flung down heavily, and pinned prone in a corner by one of those bullies who knelt on his spine. St. Martin's Summer |Rafael Sabatini 

They are extremely prone to change, and in presence of animal matters readily ferment, and are converted into salts of ammonia. Elements of Agricultural Chemistry |Thomas Anderson 

And here I was, prone and helpless, being powwowed not for one ailment, but for all the diseases known in Happy Valley. The Soldier of the Valley |Nelson Lloyd 

Man is innately more prone to good than to evil; and the path of his destiny is upward. God and my Neighbour |Robert Blatchford