An extension of the conference business, Murray said that Connect will link conferences to the digital reader revenue business that the company recently began to cultivate last year with the launch of its three-tiered paywall. Fortune Connect is bringing its conference business to a larger audience, with a higher price tag |Kayleigh Barber |September 15, 2020 |Digiday 

Curtis said senior leaders need to cultivate an atmosphere of mutual care in the workplace, rather than treating team members as disposable units of productivity. Deep Dive: How companies and their employees are facing the future of work |Digiday |September 1, 2020 |Digiday 

I was lucky enough for Kevin Lee, who helped cultivate the search industry in so many ways, to come out to my house and do an interview in a socially-distant manner. Video: Search veteran Kevin Lee on why digital PR is key for SEO |Barry Schwartz |August 31, 2020 |Search Engine Land 

Such a policy required Japan to have a very close relationship with the United States, which Abe pursued and cultivated throughout his time in power. Abe Shinzo, Japan’s longest-serving prime minister, steps down over health concerns |Alex Ward |August 28, 2020 |Vox 

Since Google pulled out of China in 2010 over the government’s censorship of search results, China has cultivated a walled garden of applications primarily for use by its citizens. Covid-19 and the geopolitics of American decline |Katie McLean |August 19, 2020 |MIT Technology Review 

The strong ties he would cultivate with America were first instilled by his American mother. The Real-Life ‘Downton’ Millionairesses Who Changed Britain |Tim Teeman |December 31, 2014 |DAILY BEAST 

Former President Nicolas Sarkozy used it to cultivate right-wing anti-immigrant voters. Abu Dhabi Stabbing: Why Law Enforcement Hates The Niqab & Burqa |Christopher Dickey |December 3, 2014 |DAILY BEAST 

And Facebook, under COO and Lean In author Sheryl Sandberg, has attempted to cultivate a reputation for being friendly to parents. Don’t Be Fooled by Apple and Facebook, Egg Freezing Is Not a Benefit |Samantha Allen |October 17, 2014 |DAILY BEAST 

With Bruce Wayne out of the picture, Dick Grayson is free to cultivate that hitherto underdeveloped aspect of his abilities. The CIA Spook Turned Comic Book Scribe: Robin Grabs a Gun in ‘Grayson’ |Rich Goldstein |June 24, 2014 |DAILY BEAST 

Perhaps the general did not cultivate his fame as “The Marble Man,” but he earned it. How I Learned to Hate Robert E. Lee |Christopher Dickey |June 22, 2014 |DAILY BEAST 

He wishes to cultivate it still, and offers to renew the lease for any number of years, and pay the rent punctually. Glances at Europe |Horace Greeley 

The easiest way to cultivate the geographic sense is by practising the art of making sketch maps. Outlines of the Earth's History |Nathaniel Southgate Shaler 

The Chinese cultivate the plant like the Japanese, and give it as much care and attention as they do the tea plant. Tobacco; Its History, Varieties, Culture, Manufacture and Commerce |E. R. Billings. 

The Chinese planter often raises large fields of the plants, and employs many hands to tend and cultivate them. Tobacco; Its History, Varieties, Culture, Manufacture and Commerce |E. R. Billings. 

Nothing is more like the most ardent friendship than those acquaintances which we cultivate for the sake of our love. The 'Characters' of Jean de La Bruyre |Jean de La Bruyre