Rieger also hinted that she would like to learn how to play the cello or even the tuba. Wendy Rieger to retire from NBC4 |Michael K. Lavers |December 15, 2021 |Washington Blade 

Still, a tuba, two trombones, a clarinet and a lot of percussion was enough to bring melodies to life. What science tells us about reducing coronavirus spread from wind instruments |Betsy Ladyzhets |August 6, 2021 |Science News 

She played the girl who played the tuba in the school band, and it was about Seth Rogen’s first girlfriend. How ‘Freaks and Geeks’ went from misfit dramedy to cult classic, as told by its cast and creators: ‘People just like it so much that it thrusts itself from the grave’ |Sonia Rao |January 27, 2021 |Washington Post 

In Taipei, Taiwan, a Bach flash mob consisting of cellists and tuba players took over a train. Can Bach Make It on NYC’s Subways? |Tom Teodorczuk |March 22, 2014 |DAILY BEAST 

There were a couple of black children on one of the floats, and a plump black tuba player marched with the high school band. ‘The Land of the Permanent Wave’ Is Bud Shrake’s Classic Take on ‘60s Texas |Edwin Shrake |February 2, 2014 |DAILY BEAST 

He played a bit of flute (and for a brief stint, tuba) in the high school band with just a rudimentary sense of the instruments. The Football Player Turned Opera Singer |Eve Conant |February 19, 2011 |DAILY BEAST 

Perhaps you remember that when Alila was christened there was a good supply of tuba at the feast. Alila, Our Little Philippine Cousin |Mary Hazelton Wade 

Tapping the trees for tuba is dangerous work, but Alila, you know, loves danger. Alila, Our Little Philippine Cousin |Mary Hazelton Wade 

There is another cocoanut grove on the farm, beside the one where Alila gets the tuba. Alila, Our Little Philippine Cousin |Mary Hazelton Wade 

The remaining two-thirds of the Pedal organ and three Tuba stops occupy the northeast quarter gallery in the dome. The Recent Revolution in Organ Building |George Laing Miller 

The process can also be followed with rice vinegar (see bleaching agents) substituted for the tuba vinegar. Philippine Mats |Hugo H. Miller