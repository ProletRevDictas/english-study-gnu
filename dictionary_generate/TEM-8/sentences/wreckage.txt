The pandemic has also left some travelers agentless, combing the industry’s wreckage in search of a new travel professional. How to find a travel adviser amid the pandemic |Christopher Elliott |January 27, 2021 |Washington Post 

They want to just move along — smiling politely in the face of the wreckage. Ghosts of our unsettled past |Robin Givhan |January 26, 2021 |Washington Post 

We ask that you do not air photos of the wreckage, helicopter in the air or accident scene. The Kobe Bryant crash one year later: Lakers’ plans, the probe, Vanessa Bryant’s wishes |Cindy Boren |January 26, 2021 |Washington Post 

As college football tries to emerge from the wreckage of 2020, this game could set a hopeful tone. Trevor Lawrence and Justin Fields will rekindle their reluctant rivalry in CFP semifinal |Jerry Brewer |January 1, 2021 |Washington Post 

In a nationwide referendum on Sunday, Swiss voters will decide whether companies headquartered there should be held legally liable for whatever environment wreckage and human rights abuses occur as a result of their operations, no matter where. In a historic vote, the Swiss will determine if multinationals should be held liable for global abuses |Vivienne Walt |November 27, 2020 |Fortune 

The wreckage lies no more than around 100 feet down in the Java Sea. Flight 8501 Poses Question: Are Modern Jets Too Automated to Fly? |Clive Irving |January 4, 2015 |DAILY BEAST 

Search teams find dozens of people and jet debris floating in the Java Sea, as the airline confirms the wreckage is from QZ8501. Wreckage, Bodies of AirAsia Crash Found |Lennox Samuels |December 30, 2014 |DAILY BEAST 

The wreckage of the Airbus A320 has been located in relatively shallow water. Who Will Get AsiaAir 8501’s Black Boxes? |Clive Irving |December 30, 2014 |DAILY BEAST 

As the sun set on Monday and the search was called off for the day, there had been no positive update on the possible wreckage. The Presumed Crash of AirAsia Flight QZ8501 Is Nothing Like MH370 |Lennox Samuels |December 29, 2014 |DAILY BEAST 

Pakistani troops even fought off militants attempting to reach the wreckage of drones that had crashed. Obama’s Deadly Informants: The Drone Spotters of Pakistan |Umar Farooq, Syed Fakhar Kakakhel |November 12, 2014 |DAILY BEAST 

Charred beams and blackened walls showed stark and gaunt in the glow of a smoldering mass of wreckage. The Red Year |Louis Tracy 

Then the company had become bankrupt and only a miserable ninety pounds a year had been saved from the wreckage. The Joyous Adventures of Aristide Pujol |William J. Locke 

There were no odds and ends, even, of wreckage which I could salvage for one more week of the old life. A Virginia Scout |Hugh Pendexter 

The position in which the wreckage was said to have been seen on the Monday morning was verified by sights taken on that morning. Loss of the Steamship 'Titanic' |British Government 

"Not when John Fly am carryin' dem," put in the colored waiter, who stood looking at the wreckage with a sober face. The Rover Boys on the Farm |Arthur M. Winfield (AKA Edward Stratemeyer)