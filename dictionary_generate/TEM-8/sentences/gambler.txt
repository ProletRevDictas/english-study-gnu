For parents willing to take a gamble on a gift not turning up in time for the festive season, the NextMaker Box is slated to deliver monthly hardware projects and coding courses designed to keep young minds engaged. Gift Guide: 22 STEM toy gift ideas for every little builder |Natasha Lomas |November 23, 2020 |TechCrunch 

The Cardinals’ path here started with a visionary gamble at the franchise’s nadir, a bet that they could peek around the corner at where the NFL was headed even as they reached the bottom of it. Kyler Murray runs out of miracles as Seahawks hang on to take over first place in NFC West |Mark Maske |November 20, 2020 |Washington Post 

Perhaps one of the greatest design gambles Ridder has taken was installing lavender hexagonal ceramic tiles from Mosaic House in the entryway of the house that she and Pennoyer designed for themselves in New York’s Dutchess County. Captivated by bold tile on social media? Here’s what to consider before committing to the look. |Elizabeth Mayhew |October 29, 2020 |Washington Post 

In dark times like these, skiers are accustomed to envisioning the bright side, because by its very nature, skiing is a gamble on an unknown future. Building Stoke for a Winter Unlike Any Other |Heather Hansman |October 26, 2020 |Outside Online 

The offers are a gamble that if the mobile-service providers cover the cost of the phones over a two-year-plus payment schedule, subscribers will be inclined to sign on for higher-priced unlimited plans. Here’s how to get a free Apple 5G iPhone |Verne Kopytoff |October 13, 2020 |Fortune 

As a producer on The Gambler, he read a bunch of women for the female lead, and settled on Larson. Brie Larson’s Hollywood Transformation |Marlow Stern |December 29, 2014 |DAILY BEAST 

My grandfather lived fast and large—he liked his liquor and his tobacco, and he was also an ace gambler. Those Kansas City Blues: A Family History |Katie Baker |October 24, 2014 |DAILY BEAST 

His father, Ronald, was a professional sports gambler who died in 2005. Oh Yes, He’s The Great Connector: Jason Hirschhorn’s Expertly Curated World |Lloyd Grove |October 17, 2014 |DAILY BEAST 

The most ridiculous character in Pay Any Price may be Dennis Montgomery, who is described as an inveterate gambler and swindler. Speed Read: James Risen Indicts The War On Terror’s Costly Follies |William O’Connor |October 14, 2014 |DAILY BEAST 

He was smart and tough in the way of the hard worker, the long-distance runner, the gambler who wins on stamina. The Stacks: How Leonard Chess Helped Make Muddy Waters |Alex Belth |August 2, 2014 |DAILY BEAST 

But you are a gambler and so am I. I will play you for those documents against twenty-five thousand francs. The Joyous Adventures of Aristide Pujol |William J. Locke 

A good gambler never cares whose money he spends or how much he loses. The Whale and the Grasshopper |Seumas O'Brien 

Always a gambler, Long had tumbled into the legitimate million-dollar business accidentally. Hooded Detective, Volume III No. 2, January, 1942 |Various 

That style of gambler is no longer seen in society of a certain topographical height. Juana |Honore de Balzac 

Diard was, therefore, not a mere commonplace gambler who is seen to be a blackguard, and ends by begging. Juana |Honore de Balzac