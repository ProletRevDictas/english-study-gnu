Doctors at NYU Langone saw a 41% year-over-year increase in women fertilizing their eggs. Data Show More Women Are Freezing Their Eggs During the Pandemic, Defying Doctors’ Expectations |Eliana Dockterman |January 13, 2021 |Time 

With this approach, conventional farming practices such as watering and fertilizing crops are performed at the right place and time, and with the appropriate intensity. How technology rewrites your diet |Amy Nordrum |December 18, 2020 |MIT Technology Review 

The eggs are moved to the lab, where they are fertilized and the CRISPR molecules are introduced. Gene editing has made pigs immune to a deadly epidemic |Katie McLean |December 11, 2020 |MIT Technology Review 

My hypothesis is that fertilized plants will be bigger than those that are not fertilized. How to grow your own science experiment |Bethany Brookshire |December 9, 2020 |Science News For Students 

The fertilized waters prompt a bloom of phytoplankton that attracts krill—which, despite the crabeaters’ name, is a favorite meal of the seals. These Photos Remind Us Why Conservation Matters - Issue 92: Frontiers |Kevin Berger |November 11, 2020 |Nautilus 

The animals proved to be a great attraction as well as a handy way to fertilize the grass and keep it short. Central Park’s Carriages Saved This Horse |Michael Daly |April 24, 2014 |DAILY BEAST 

Some is sold as a liming agent, and some is disposed of in landfills (though it used to be sent to Colorado to fertilize crops). Toilet Made for Densely Populated Settlements Turns Waste Into Dollars |Mike Miesen |April 5, 2014 |DAILY BEAST 

Their byproduct is used to organically fertilize the food, while the plants naturally clean the fish tanks. Vertical Indoor Farms Are Growing in the U.S. |Miranda Green |May 7, 2013 |DAILY BEAST 

I fertilize them and I shower them, but they stubbornly refuse to do well. Amateur Gardencraft |Eben E. Rexford 

German laws, German language, German civilization are to find no ground for replenishing, no soil to fertilize and make rich. The Crime Against Europe |Roger Casement 

This is one of the consequences of the Nubians depending upon the overflow of the Nile to fertilize their soil. Our Caughnawagas in Egypt |Louis Jackson 

This is the most effectual of inventions to fertilize the rich man's fields by the sweat of the poor man's brow. Complete State of the Union Addresses from 1790 to 2006 |Various 

The fountains of sympathy, of gratitude, of love, were opened; might not these waters prove sufficient to fertilize a life? Harper's New Monthly Magazine, No. IX.--February, 1851.--Vol. II. |Various