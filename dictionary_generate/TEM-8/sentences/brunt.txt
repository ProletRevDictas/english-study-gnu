There’s pain across the board, but women are definitely bearing the brunt. Nearly 80% of the 346,000 workers who vanished from the U.S. labor force in January are women |Maria Aspan |February 5, 2021 |Fortune 

We are counting on this vaccine to get us out of the pandemic, and we’re going to be left with populations where they will continue to be exposed and bearing the brunt of this if we don’t get the vaccines to those high-risk areas. Many States Don't Know Who's Getting COVID-19 Vaccines. That's a Huge Problem for Equity |Chris Wilson |January 28, 2021 |Time 

That pain has been most concentrated in service sectors — restaurants, bars, hotels — that have borne the brunt of the pandemic. Federal Reserve Chair Powell says ‘nothing more important’ to economy than vaccinating Americans |Erica Werner, Rachel Siegel |January 27, 2021 |Washington Post 

Women and girls have borne the brunt of the pandemic, but they have also led the fight against it. Melinda Gates: Why Women's Voices Must Be at the Center of Rebuilding After COVID-19 |Melinda Gates |January 15, 2021 |Time 

Many of those communities are in Oregon counties already bearing the brunt of timber tax cuts, which cost the state nearly $3 billion in revenue that would have been largely used to fund schools and local governments. Timber Tax Cuts Cost Oregon Towns Billions. Then Polluted Water Drove Up the Price. |by Tony Schick, Oregon Public Broadcasting, and Rob Davis, The Oregonian/OregonLive |January 1, 2021 |ProPublica 

As the Harvard Crimson noted, Byrne “had been bearing the brunt of the Harvard attack” all afternoon. When West Point Football Turned Fatal |Nicolaus Mills |October 30, 2014 |DAILY BEAST 

Though vampire legends exist the world over, Romania and Bulgaria have born the brunt of the attention. Bulgaria’s Vampire Graveyards |Nina Strochlic |October 15, 2014 |DAILY BEAST 

As the epidemic rages on, the children will continue to bear a huge brunt of the blow. Liberia’s Ebola Orphans |Abby Haglage |October 14, 2014 |DAILY BEAST 

Consequently, the ones who suffer the brunt of your bluster are not Muslims in other nations that you may want to influence. After Maher-Affleck, We Need an Honest—and Calm—Dialogue on Islam |Dean Obeidallah |October 10, 2014 |DAILY BEAST 

Now, in a slightly meta moment, Brunt and Sky News are being harassed by the online community who blame them for her death. Outed Madeleine McCann Troll Kills Herself. But Millions Live On Online. |Barbie Latza Nadeau |October 6, 2014 |DAILY BEAST 

I hope you are able to bear the brunt of the battle, for my vocabulary will scarcely carry me through ten words. Blackwood's Edinburgh Magazine, Volume 60, No. 372, October 1846 |Various 

The men of the other brigade were scarcely,—if any,—better prepared, and upon them fell the brunt of the first assault. Stone's River |Wilson J. Vance 

We writhe in pain and bear the brunt of an arrogant tyranny from whatever force that created and controls us. Tyranny of God |Joseph Lewis 

It is the old men, the women, the children, the babies and the physically imperfect who must bear the brunt of dreadfulness. Private Peat |Harold R. Peat 

They have to bear the brunt of the war, which to them is a fight of endurance and eternal, everlasting waiting—waiting—waiting. Private Peat |Harold R. Peat