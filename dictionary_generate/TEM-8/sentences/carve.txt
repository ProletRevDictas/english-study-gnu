Operating successfully in a competitive market, the website operators discovered tremendous growth opportunities for the business if a larger market share could be carved out. SEO horror stories: Here’s what not to do |Kaspar Szymanski |August 24, 2020 |Search Engine Land 

India, Brazil, South Africa, Nigeria, and Indonesia are all deciding whether to try to carve out their own digital agendas or to act more as fence-sitters, playing China, the US, and the EU against one another. Covid-19 and the geopolitics of American decline |Katie McLean |August 19, 2020 |MIT Technology Review 

Rivers can only carve out valleys if the water is running downhill. Mars may not have been the warm, wet planet we thought it was |Neel Patel |August 7, 2020 |MIT Technology Review 

The Tchambuli tended to put men in the role of artists who spent their days carving wooden masks and dancing, while the women fished and prepared food. Gender Is What You Make of It - Issue 88: Love & Sex |Charles King |August 5, 2020 |Nautilus 

Such attempts to use information flows, in theory or practice, to carve nature at its joints are “the beginning of sketching out ideas and concepts that could be potentially foundational for new areas of biology,” Hoyal Cuthill said. What Is an Individual? Biology Seeks Clues in Information Theory. |Jordana Cepelewicz |July 16, 2020 |Quanta Magazine 

Al Qaeda has never managed to carve out a large chunk of real estate to call its own—in Afghanistan it was a guest of the Taliban. ISIS’s Futile Quest to Go Legit |Jamie Dettmer |January 5, 2015 |DAILY BEAST 

Her new paradigm leads her to carve up shibboleths and heroes alike. Naomi Klein’s ‘This Changes Everything’ Will Change Nothing |Michael Signer |November 17, 2014 |DAILY BEAST 

I sort of got lucky in that I was able to carve a niche for myself. Juliette Lewis on Hollywood, Why the MSM Hates Scientology, and Masturbating to George Clooney |Marlow Stern |September 19, 2014 |DAILY BEAST 

In Brazil, there was a microcosmic slice of the kind of public role he is attempting to carve. Prince Harry Should Be King: The Royal Family’s Ace Card |Tim Teeman |June 27, 2014 |DAILY BEAST 

Could it just be that prison itself conditions a sort of pavlovian reaction to carve chess pieces? Reading Prison Novels In Prison |Daniel Genis |May 24, 2014 |DAILY BEAST 

You could carve him to pieces without hearing a cheep, if he decided to keep his mouth shut. Raw Gold |Bertrand W. Sinclair 

A pair of carvers, laid with my cover, tell me that I shall have to carve the ham which is here eaten with the chicken. Friend Mac Donald |Max O'Rell 

Houdon then returned to France and proceeded to carve a Carrara marble statue of his subject. Hallowed Heritage: The Life of Virginia |Dorothy M. Torpey 

The painter cannot put sounds upon a canvas, nor can the sculptor carve from marble an odor or a taste. English: Composition and Literature |W. F. (William Franklin) Webster 

He was yet young, vigorous and ambitious, and with the help of heaven he would carve out his own fortune. Adrift on the Pacific |Edward S. Ellis