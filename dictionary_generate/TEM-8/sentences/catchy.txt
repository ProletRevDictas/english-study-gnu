It’s ideal to use catchy phrases such as “holiday season” or “holiday sale” as they will increase your open rates. SMB’s guide to marketing: Stand out and boost sales during the holidays |Steffen Schebesta |December 24, 2020 |Search Engine Watch 

In between a catchy refrain of “Diwali with Mi,” the two trade zingers about who’s the better rapper. How Chinese phonemaker Xiaomi conquered India—and outperformed Apple |eamonbarrett |December 3, 2020 |Fortune 

Her brash style feels new but is catchy enough to make it on pop radio. Crystal Ball 2021: Predictions for the economy, politics, technology, and more |lbelanger225 |December 1, 2020 |Fortune 

Like many of her fellow TikTok stars, she built her following of more than 5 million by lip-syncing to catchy hip-hop songs and joking around with friends. How a TikTok house destroyed itself |Rebecca Jennings |October 1, 2020 |Vox 

It’s a hard concept to reduce to a catchy tagline, but it’s an engaging gender-bending boundary-busting movie. FROM THE VAULTS: The teen edition |Brian T. Carney |August 8, 2020 |Washington Blade 

But damn, the music is catchy—a neo-soul aural assault of horns, electro swirls, yelps, funky basslines, and harmonized vocals. The 14 Best Songs of 2014: Bobby Shmurda, Future Islands, Drake, and More |Marlow Stern |December 31, 2014 |DAILY BEAST 

But not all of us can put our feelings into a catchy and soulful song. The Daily Beast’s 2014 Holiday Gift Guide: For the Taylor Swift in Your Life |Allison McNearney |November 29, 2014 |DAILY BEAST 

What an alternately messed up, irresistibly catchy, reprehensible, utterly charming holiday classic. The Most WTF Covers of ‘Baby, It’s Cold Outside,’ Everyone’s Favorite Date-Rape Holiday Classic |Kevin Fallon |November 19, 2014 |DAILY BEAST 

It effortlessly combined a catchy tune, an emotional arc and a surprisingly easy way to remember multiples of three. Schoolhouse Rock: A Trojan Horse of Knowledge and Power |Jason Lynch |September 6, 2014 |DAILY BEAST 

Hear those rap interludes, ultra-catchy choruses, and dance breaks? Black K-Pop Fans Come Out of the Closet |Sterling Wong |August 31, 2014 |DAILY BEAST 

The meeting opened with the singing of a popular hymn which carried a refrain catchy enough but running to doggerel. The Major |Ralph Connor 

You dont get a very large repertoire here, but what they do give you is sort of catchy. The Wayfarers |Mary Stewart Cutting 

The infidel review is crisp in style, its arguments catchy, and the brilliancy of its diction captivates. The Young Priest's Keepsake |Michael Phelan 

The tunes were very catchy and bright, and everybody seemed constantly to be humming them, in season or out of season. The Leader of the Lower School |Angela Brazil 

The flapper drew a long and rather catchy breath, then she adjusted a strand of hair misplaced by his violence. Bunker Bean |Harry Leon Wilson