Throughout his 21 years in power, Putin has seen Ukraine as a fraternal nation, tied to Russia by bonds of faith, family, politics, and a millennium of common history. The Untold Story of the Ukraine Crisis |Simon Shuster/Kyiv |February 2, 2022 |Time 

Boys’ lodges and fraternal bodies such as the Boy Scouts began to crop up, aimed at preventing “feminization” of young boys. Empires and ‘Effeminate Men.’ After Britain and America, It’s China’s Turn to Worry about Masculinity |Debasish Roy Chowdhury |September 10, 2021 |Time 

They have structures of fraternal bonding, they have the organizational social elements that are very similar to the armed forces—and they’re built that way deliberately. Inside One Combat Vet's Journey From Defending His Country to Storming the Capitol |W.J. Hennigan/Washington |July 9, 2021 |Time 

Banquet halls, concert venues and social and fraternal clubs will remain limited to 50 percent maximum capacity. Maryland lifting capacity restrictions on restaurants, entertainment venues |Ovetta Wiggins, Rebecca Tan, Rachel Chason |May 12, 2021 |Washington Post 

The Craft, as members call it, was based on a fraternal bond created by ritual and symbol. What the Freemasons Taught the World About the Power of Secrecy |John Dickie |August 13, 2020 |Time 

In hindsight, however, he feels that the suspension has “had a positive impact on the fraternal community.” Fraternities in a Post-UVA World |Samantha Allen |December 12, 2014 |DAILY BEAST 

How many people get called by the Fraternal Order of the Police and say no? The GOP and Police Unions: A Love Story |Eleanor Clift |December 12, 2014 |DAILY BEAST 

The nonvoters include 16 experts, eight fraternal delegates and 38 auditors of whom 12 are married couples. The Pope vs. the Church on Family Values? |Barbie Latza Nadeau |October 6, 2014 |DAILY BEAST 

This was more than just the fraternal bond between firefighters and police officers; this was family. The Resilient City: New York After 9/11 |John Avlon |September 11, 2014 |DAILY BEAST 

He was also the local president of the Union League, a fraternal organization closely aligned with the Republican Party. Enough With This 'Lost Cause' Nonsense |Justin Green |February 19, 2013 |DAILY BEAST 

Suddenly both cheeks were painted black by a too fraternal hand, and then a man tried to kiss her. Ancestors |Gertrude Atherton 

His words had been more fraternal than lover-like; but she had lost their exact sense in the caressing warmth of his voice. Summer |Edith Wharton 

Dear Mother, by thy heart pierced through, obtain for me the virtue of fraternal charity and the gift of understanding. Mary, Help of Christians |Various 

His manner of greeting the family and friends was so expressive of fraternal sympathy that one felt it a privilege to witness it. The Leaven in a Great City |Lillian William Betts 

This model of fraternal affection rode off with the mule lest it should get stolen, and left his brother in the snow till morning! The Cradle of Mankind |W.A. Wigram