In electric stoves, the broiler is a coil of metal that gets hot as hell but never lets out flames. Don’t Fear the Broiler |Elazar Sontag |February 11, 2021 |Eater 

She did give us some instructions, like how to use the wood stove, but she didn’t charge us. How to Apologize to Your Friend |Blair Braverman |February 8, 2021 |Outside Online 

While nothing can beat the satisfaction of consuming them at an actual diner, these pancakes are almost as good coming off your own stove. The Best Pancake Recipes, According to Eater Editors |Eater Staff |February 1, 2021 |Eater 

Given their varying cooking skills and some units not including full stoves, the players frequent Roaming Rooster, Chipotle and Negril, a Jamaican restaurant. The Howard Bison’s hopes for a dream basketball season have turned into a nightmare |Tramel Raggs |January 27, 2021 |Washington Post 

In the annual grant application for extra state funding, Varahn secured a bigger clothing allowance — $200 per resident — and a double-oven stove for the communal kitchen. “We Don’t Even Know Who Is Dead or Alive”: Trapped Inside an Assisted Living Facility During the Pandemic |by Ava Kofman |November 30, 2020 |ProPublica 

Upstairs, in the living room, splintered logs of hemlock cackled and spat from inside the wood stove. Dungeons and Genital Clamps: Inside a Legendary BDSM Chateau |Ian Frisch |December 20, 2014 |DAILY BEAST 

Putin, with his comments Friday, just moved it to the front of the stove. Putin Threatens Nuclear War Over Ukraine |Gordon G. Chang |August 31, 2014 |DAILY BEAST 

I was stuck between the sink and the stove, which I thought was fantastic! Julianna Margulies's Favorite 'The Good Wife' Scenes |Julianna Margulies |August 11, 2014 |DAILY BEAST 

There is also a mysterious Moomin ancestor who lives permanently in the stove. Tove Jansson, Queen of the Moomins |John Garth |August 9, 2014 |DAILY BEAST 

He is, however, obedient about not turning on the stove burner without permission. The Ridiculousness of Father's Day |P. J. O’Rourke |June 15, 2014 |DAILY BEAST 

Elmer Spiker, mine host of the inn, was huddled close to the stove, and was reading by the light of a lamp. The Soldier of the Valley |Nelson Lloyd 

Henry Holmes was standing with his back to the stove, one hand wagging up and down at the solemn line of figures on the bench. The Soldier of the Valley |Nelson Lloyd 

Improved stove pipings are now being manufactured in the States which in appearance exactly resemble cast-iron. Asbestos |Robert H. Jones 

Suddenly he took them in his arms and walked quickly over to the stove, his eye roving in search of a match-box. Ancestors |Gertrude Atherton 

Elmer Spiker folded the county paper and came around to our side of the stove. The Soldier of the Valley |Nelson Lloyd