Evident today was a new signet ring that Carole has recently taken to wearing on the little finger of her right hand. Carole's Mum Scales Social Heights With Royal Box |Tom Sykes |July 3, 2012 |DAILY BEAST 

He wears a real pearl stud and a good signet ring; also a gold wrist watch, face broken and hands stopped at seven-fifteen. Dope |Sax Rohmer 

In the end, therefore, it was he who took the signet ring and the letter the prince had written and brought back the gold. A Prince of Cornwall |Charles W. Whistler 

But for the dark circles about the eyes and mouth, the unmistakeable signet of Death, he might have seemed in perfect health. Alone |Marion Harland 

And that figure upon the shields is the sacred sign that was engraved upon his signet-ring. The Devil-Tree of El Dorado |Frank Aubrey 

Of all the large Algerian cities, Constantina is that which has best preserved its primitive signet. Lippincott's Magazine of Popular Literature and Science, Volume 11, No. 24, March, 1873 |Various