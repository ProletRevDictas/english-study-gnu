Some reports even tie psychosis to the withdrawal episodes experienced while weaning off of phenibut. The Newest Dangerous High: Phenibut |Eugene Robinson |January 20, 2021 |Ozy 

In the meantime, San Diego is spending billions to diversify its local water supply in an effort to wean itself off the Colorado River. Morning Report: Who Owns the Tijuana River Water? |Voice of San Diego |January 11, 2021 |Voice of San Diego 

It’s good that oil demand is waning as the world should be in a rush to wean itself off of fossil fuels. There’s growing consensus that oil demand won’t make a comeback |eamonbarrett |September 17, 2020 |Fortune 

A government push in 1970 to wean India off costly imports and manufacture cheaper medicines for its own citizens led to legal reforms that kickstarted growth of India’s generics industry. More than manufacturing: India’s homegrown COVID vaccines could transform its pharma industry |Naomi Xu Elegant |September 6, 2020 |Fortune 

Hyena moms nurse their cubs for around 14 months and help them get enough food even after they’re weaned. Female hyenas kill off cubs in their own clans |Carolyn Wilke |August 25, 2020 |Science News 

“Since MGP whiskey is [more than] 80 percent of my revenues, it might be silly to wean myself off of that,” Perkins says. Your ‘Craft’ Rye Whiskey Is Probably From a Factory Distillery in Indiana |Eric Felten |July 28, 2014 |DAILY BEAST 

The court postponed execution of the sentence, to give her time to recover from childbirth and to wean the new baby. In Sudan a Pregnant Woman May Be Hanged for Marrying a Christian |Nina Shea |May 17, 2014 |DAILY BEAST 

Direct payments came into being in 1996, originally as an effort to wean farmers off of direct government subsides altogether. Why Don't We Eliminate Farm Subsidies? |Justin Green |April 12, 2013 |DAILY BEAST 

But it was Carter who first crusaded for the U.S. to wean itself off of its dependence on oil. Carter in Oscarland: The Rehabilitation of the 39th President |Douglas Brinkley |February 24, 2013 |DAILY BEAST 

“I was trying to wean him off,” Murray said to the detectives. The Fight Over Jackson's Health |Diane Dimond |October 12, 2011 |DAILY BEAST 

He would not, however, wean the calf till the winter time, when she was shut up in the yard and fed on hay. The Children of the New Forest |Captain Marryat 

We were always the best of friends, and I even ventured gradually to wean them from cannibalism. The Adventures of Louis de Rougemont |Louis de Rougemont 

"Madame de la Fayette and I are using every effort to wean him from so dangerous an attachment," she writes to her daughter. Queens of the French Stage |H. Noel Williams 

Then you must put your hand to the plough with a will; and the first thing to do is to wean him away from Saul Harrington. The Mynns' Mystery |George Manville Fenn 

Suffering is not always punitive; it is sometimes disciplinary, designed to wean the good man from his sin. Encyclopaedia Britannica, 11th Edition, Volume 15, Slice 4 |Various