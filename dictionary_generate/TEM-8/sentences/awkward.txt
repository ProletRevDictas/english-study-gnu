It’s the first time I’ve read a book where every sentence is cowritten, and it didn’t feel awkward at all. The best things I bought in September to make working and living at home easier |Rachel Schallom |September 27, 2020 |Fortune 

The painful consequence of that will be a clunky, suboptimal transposition of the physical to the virtual, right down—one fears—to an awkward videoconference cocktail hour. Can the corporate ‘offsite’ survive COVID? |matthewheimer |September 24, 2020 |Fortune 

You can also stretch one app across both screens, but it’s unsightly and awkward to have your content sliced down the middle with a big blank bar. Review of the Microsoft Surface Duo folding phone: Very pretty but just how useful is it? |Aaron Pressman |September 10, 2020 |Fortune 

The latest service disruptions come at an awkward time for Robinhood. Robinhood app stumbles amid surge in Apple, Tesla trading volume |Jeff |September 1, 2020 |Fortune 

Except that it’ll be rooted in awkward, funny and compelling reality. How Michaela Coel Tells Awkward Truths |Eromo Egbejule |August 30, 2020 |Ozy 

The squabble was also immortalized in this incredibly awkward family portrait. Solange Smacks Jay Z, Legolas Slaps Bieber, and the Biggest Celebrity Feuds of the Year |Amy Zimmerman |December 24, 2014 |DAILY BEAST 

The rapper will.i.am was one such panelist, forced upon Gregory for an excruciatingly awkward roundtable segment. David Gregory's 'Meet the Press' Eviction Exposed in Washingtonian Takedown |Lloyd Grove |December 23, 2014 |DAILY BEAST 

Some immediately treated the young rapper as a punchline, turning his awkward posture in the photo into a meme. Bobby Shmurda and Rap’s Ultimate Hoop Dream |Rawiya Kameir |December 23, 2014 |DAILY BEAST 

He sketched it quickly, his hand trembling, giving the drawing an awkward, palsied look. Alfred Hitchcock’s Fade to Black: The Great Director’s Final Days |David Freeman |December 13, 2014 |DAILY BEAST 

By two-thirty on the first afternoon at his house, I long for a cup of coffee but feel awkward about asking. Alfred Hitchcock’s Fade to Black: The Great Director’s Final Days |David Freeman |December 13, 2014 |DAILY BEAST 

The moment was an awkward one, and Cynthia wished madly that she had not been prompted to ask that unfortunate question. The Boarded-Up House |Augusta Huiell Seaman 

This was no strange sight to the boy by that time, but it was awkward in the circumstances, for he had neither gun nor spear. The Giant of the North |R.M. Ballantyne 

Finally, his predicament became so awkward that an expression of distress crept into his face. The Homesteader |Oscar Micheaux 

Meyer Isaacson said nothing; and, after a silence that was awkward, Nigel changed the conversation, and not long after went away. Bella Donna |Robert Hichens 

Perhaps that made it awkward for him, as he was not accustomed to having his wife in such close proximity with him daily. The Homesteader |Oscar Micheaux