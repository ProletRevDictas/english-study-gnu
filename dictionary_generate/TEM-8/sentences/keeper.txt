The leader of the party is not necessarily the keeper and enforcer of its entire message, but it’s a big choice. Even Deeper in the Wilderness, San Diego Republicans Face Major Choice |Scott Lewis |November 24, 2020 |Voice of San Diego 

Cam Newton had a five-yard touchdown run on a quarterback keeper and completed 13 of 14 first-half passes for 120 yards. The Patriots’ season isn’t fixed, but at least they avoided losing to the Jets |Mark Maske |November 10, 2020 |Washington Post 

Today, one of the keepers of the ephemerides is the Jet Propulsion Laboratory, in California. We Never Know Exactly Where We’re Going in Outer Space - Issue 92: Frontiers |Caleb Scharf |November 4, 2020 |Nautilus 

In the novel, Schweblin weaves together the stories of several keepers and dwellers around the globe, exploring factors that lead them to connect with a person in a way that feels both intimate and invasive. Everything Our Editors Loved in August |The Editors |September 10, 2020 |Outside Online 

In fact, the goalkeepers have really been the stars of the tournament, with keepers like Kailen Sheridan, Jane Campbell and Britt Eckerstrom keeping pace with the USWNT’s starter, Alyssa Naeher. At Long Last, Baseball’s (Almost) Back |Sarah Shachat |July 21, 2020 |FiveThirtyEight 

Now, the goalkeeper is out with a memoir about his life until that point: The Keeper: A Life of Saving Goals and Achieving Them. Tim Howard’s Wall of Intensity |William O’Connor |December 22, 2014 |DAILY BEAST 

This Oath Keeper was there for the protest, which had yet to materialize, and had a few friends joining him, he told me. NYC’s Garner Protesters vs. Pro-Cop Protesters |Jacob Siegel |December 20, 2014 |DAILY BEAST 

Rudder seems content to play the record keeper and let the philosophers sort out the sigificance. Heartache by the Numbers and OkCupid’s Founder Has Got Yours |Will Doig |October 6, 2014 |DAILY BEAST 

“The state has been trying to lay its hands on them for years,” one keeper of a 700 book-strong library told the paper. The Lost Libraries of the Sahara‬ |Nina Strochlic |September 11, 2014 |DAILY BEAST 

In a lot of ways, that's what My Brother's Keeper is all about, and why it's so important. Billie Holiday, Barack Obama, and the Pain of Black Women |Joshua DuBois |June 22, 2014 |DAILY BEAST 

Before he could finish the sentence the Hole-keeper said snappishly, "Well, drop out again—quick!" Davy and The Goblin |Charles E. Carryl 

About her neck was hung a covered basket and a door-key; and Davy at once concluded that she was Sindbad's house-keeper. Davy and The Goblin |Charles E. Carryl 

Presently the Hole-keeper stopped short and said, faintly, "It strikes me the sun is very hot here." Davy and The Goblin |Charles E. Carryl 

"They're great pink birds, without any feathers on 'em," replied the Hole-keeper, solemnly. Davy and The Goblin |Charles E. Carryl 

In the wall were eight gates, and at each one a keeper was stationed at all hours of the day and night. Our Little Korean Cousin |H. Lee M. Pike