Beyond the fear of being attacked, there are other reasons that former advisers have not spoken out publicly. Former Pence aide says she will vote for Biden because of Trump’s ‘flat out disregard for human life’ during pandemic |Josh Dawsey |September 17, 2020 |Washington Post 

Directing duties are split among Arena’s formidable out artistic director Molly Smith, deputy artistic director Seema Sueko, and senior artistic adviser Anita Maynard-Losh, along with local directors Paige Hernandez and director Psalmayene 24. D.C. theater scene adapts with films, concerts, and more |Patrick Folliard |September 17, 2020 |Washington Blade 

Quartz spoke to Casey Dreier, the senior space policy adviser at the Planetary Foundation, about what’s on the horizon for US space policy. What Joe Biden could mean for US space policy |Tim Fernholz |September 17, 2020 |Quartz 

Biden has assembled a team of advisers on the issue, some of whom worked in the Obama-Biden administration. Trump contradicts CDC director on vaccine; Biden says Americans shouldn’t trust Trump |Colby Itkowitz, Felicia Sonmez, John Wagner |September 16, 2020 |Washington Post 

One of the president’s chief political concerns, advisers say, is convincing the public that his performance on coronavirus is better than they believed. Trump, in town hall, says he wouldn’t have done anything differently on pandemic |Colby Itkowitz, Josh Dawsey, Felicia Sonmez, John Wagner |September 16, 2020 |Washington Post 

The advisor would cite reasonable-sounding sources like haltabuse.org and the FBI. Cover-Ups and Concern Trolls: Actually, It's About Ethics in Suicide Journalism |Arthur Chu |January 3, 2015 |DAILY BEAST 

Jeb next found himself as an advisor to Barclays, which had picked through the carcass of what was left of Lehman. Bush, Christie, Romney: Who’ll Be the GOP Class Warrior? |Lloyd Green |December 15, 2014 |DAILY BEAST 

In Vietnam, Lewis was advisor to a Vietnamese infantry unit, whose nickname for him was “Captain of Many Kilos.” A West Point MVP Who Never Played a Down |Nicolaus Mills |December 13, 2014 |DAILY BEAST 

Malcolm Tucker, a foul-mouthed political advisor, was the role that turned Capaldi into a household name in Britain. Doctor Who: It’s Time For a Black, Asian, or Woman Doctor |Nico Hines |December 11, 2014 |DAILY BEAST 

He reiterated the statements “I am not a politician” and “I am not a political advisor” so often that it seemed like a verbal tic. Obamacare Architect: I Wanted to Sound Smart |Ben Jacobs |December 9, 2014 |DAILY BEAST 

He must constantly stand as the able and ready advisor of the librarian, and for the honor and defence of the library. The Library and Society |Various 

The advisor is concerned with making the best use of what is there—putting it at work under the most favorable conditions. Democracy and Education |John Dewey 

To be a safe medical advisor requires that the mind be free from the petty cares of life. How to Collect a Doctor Bill |Frank P. Davis 

The reply could hardly have been better if they'd penned it themselves for the signature of the faculty advisor. The Fourth R |George Oliver Smith 

He will re-appear on the scene, the advisor of General Wolfe, as to the best landing place round Quebec. The Seats Of The Mighty, Complete |Gilbert Parker