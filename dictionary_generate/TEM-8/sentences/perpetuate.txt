I should add that by perpetuating lies that have led to the deaths of hundreds of thousands and by promoting lies that have undermined democracy, Fox is actively complicit in this. The MAGA Movement’s a Bigger Threat to America Than the Taliban |David Rothkopf |August 26, 2021 |The Daily Beast 

With the Better Common Names Project, the ESA now prohibits names perpetuating negative stereotypes, and welcomes public input about which names to change next. Racism lurks in names given to plants and animals. That’s starting to change |Jaime Chambers |August 25, 2021 |Science News 

By focusing on Google above all else, we perpetuate a cycle that overlooks the value that smaller competitors might be offering and keeps the search behemoth at the top. The shifting sands of SERPs; Tuesday’s daily brief |George Nguyen |August 24, 2021 |Search Engine Land 

By focusing on Google above all else, we perpetuate a cycle that disenfranchises smaller competitors and keeps the search behemoth at the top. The case for advertising on search engines other than Google |John Smith |August 23, 2021 |Search Engine Land 

Such treatment brings with it pain and perpetuates intolerance. Former PFLAG president Paulette Goodman dies at 88 |Lou Chibbaro Jr. |August 16, 2021 |Washington Blade 

And while some sex workers turn to advocacy groups, even rape support organizations sometimes perpetuate stigma. Sex Workers Don't Deserve to be Raped |Jillian Keenan |September 27, 2014 |DAILY BEAST 

In Paris, a new generation of entrepreneurs are  launching initiatives to perpetuate the Yiddish way of life. Paris's Nouveau Yiddish Culture |Laurent David Samama |September 3, 2014 |DAILY BEAST 

Unused funds, sitting idle, do nothing to perpetuate the cycle of support that America relies on. Ex-Politicians Keeping $100 Million in Private Slush Funds |Dave Levinthal, Center for Public Integrity |May 22, 2014 |DAILY BEAST 

But religion also compels us to fight the unjust, prejudiced systems that cause and perpetuate that misfortune. Believers Must Fight for Gay Teens |Gene Robinson |May 18, 2014 |DAILY BEAST 

But it serves no one to perpetuate the idea that parenting is supposed to be an agonizing and thankless slog. Ad's Message to Moms: If You Don’t Think Parenting Sucks, You’re Doing it Wrong |Andy Hinds |April 18, 2014 |DAILY BEAST 

It is a very laudable spirit on the part of a dying man to wish to—ah—perpetuate these old English names. First Plays |A. A. Milne 

I think this is a wicked, wicked war, waged to perpetuate slavery and to destroy the Union. The Courier of the Ozarks |Byron A. Dunn 

By perpetrating an act of injustice, which would perpetuate agitation. The History of England in Three Volumes, Vol.III. |E. Farr and E. H. Nolan 

What he wanted was to gain time, and perpetuate the war, even though waging an unequal contest. The Border Rifles |Gustave Aimard 

That the animals selected for breed, should unite in themselves all the good qualities we wish to perpetuate in the offspring. Domestic Animals |Richard L. Allen