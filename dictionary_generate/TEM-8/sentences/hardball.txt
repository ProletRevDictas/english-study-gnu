What determines how many appointments a president gets is a combination of pure luck and partisan hardball. I Spent 7 Months Studying Supreme Court Reform. We Need to Pack the Court Now |Kermit Roosevelt III |December 10, 2021 |Time 

They used hardball tactics to push through a map for Oregon that eliminates one highly competitive seat and adds two Democratic-leaning seats. Redistricting Has Maintained The Status Quo So Far. That’s Good For Republicans. |Nathaniel Rakich (nathaniel.rakich@fivethirtyeight.com) |December 2, 2021 |FiveThirtyEight 

Last week on Hardball, Kathleen Parker said he is definitely running. The Numbers Don’t Lie: Jeb Bush Can’t Beat Hillary Clinton |Michael Tomasky |May 5, 2014 |DAILY BEAST 

Scott Simkus bemusingly explores it all in Outsider Baseball: The Weird World of Hardball on the Fringe, 1876—1950. Home Runs, Frozen Ropes, And Some Wild Cards In Best Baseball Books |Robert Birnbaum |April 11, 2014 |DAILY BEAST 

In any case, the healthy sampling below confirms the narrative-friendly qualities of hardball. Home Runs, Frozen Ropes, And Some Wild Cards In Best Baseball Books |Robert Birnbaum |April 11, 2014 |DAILY BEAST 

Will the Democrats engage in an aggressive, hardball type strategy? Dems Need to Channel ‘House of Cards’’ Frank Underwood |Dean Obeidallah |March 19, 2014 |DAILY BEAST 

Transfixingly candid and gamely stepping up to the plate for a round of hardball, Lohan seemed to be answering yes. Oprah’s Lindsay Lohan Interview Settles It: Lohan’s Comeback Is Real |Kevin Fallon |August 19, 2013 |DAILY BEAST