For marketers, this means an opportunity to sell to a group of prospects who aren’t just passive readers or random social media users but active listeners who are willing to end the day having learned and encountered something new. Inbound marketing for brand awareness: Four up-to-date ways to do it |Ali Faagba |September 11, 2020 |Search Engine Watch 

More passive screen time was linked to worse outcomes in health and school achievement, the researchers found, compared with the other categories. Healthy screen time is one challenge of distance learning |Kathryn Hulick |September 11, 2020 |Science News For Students 

If the situation turns another way, you might find yourself passive in the face of great evil, unsure what to make of it. Believing in Monsters: David Livingstone Smith on the Subhuman - Facts So Romantic |Eric Schwitzgebel |September 11, 2020 |Nautilus 

As others have pointed out, for example, the term “officer-involved shooting” is a passive phrasing that deemphasizes police officers’ use of deadly force, obscuring their role in state violence. What’s missing from corporate statements on racial injustice? The real cause of racism. |Amy Nordrum |September 5, 2020 |MIT Technology Review 

It’s not surprising that his therapist says Charlie engages in “passive suicidal behavior” or that Charlie’s love life sucks. ‘A Star is Bored’ a delicious work of fiction |Kathi Wolfe |August 13, 2020 |Washington Blade 

If we want to prevent others from your fate, we need to stop being so passive on these issues. Dear Leelah, We Will Fight On For You: A Letter to a Dead Trans Teen |Parker Molloy |January 1, 2015 |DAILY BEAST 

In a rather passive aggressive letter, the House Minority Leader wrote. Nancy Pelosi Plays Hardball On Cromnibus |Ben Jacobs |December 11, 2014 |DAILY BEAST 

These “smart benches” can do more than simply serve as passive producers of electricity. Parks and Regeneration |The Daily Beast |November 3, 2014 |DAILY BEAST 

It turned Web surfers into passive consumers of published content. You Can Look It Up: The Wikipedia Story |Walter Isaacson |October 19, 2014 |DAILY BEAST 

More and more, Obama seems like a passive observer of events who dismisses criticism as superficial. Does Obama Remember He's President? |Stuart Stevens |September 8, 2014 |DAILY BEAST 

Passive hyperemia occurs most commonly in diseases of the heart and liver and in pregnancy. A Manual of Clinical Diagnosis |James Campbell Todd 

In conversation their minds are apt to remain in a recipient passive state. Assimilative Memory |Marcus Dwight Larrowe (AKA Prof. A. Loisette) 

A purely passive defence is not possible for us; it implies losing ground by degrees—and we have not a yard to lose. Gallipoli Diary, Volume I |Ian Hamilton 

Whatever his secret care might have been, it was now passive; he was a general favourite, and courted in society. Elster's Folly |Mrs. Henry Wood 

And we shall not go there, to be idle—passive spectators to an invasion of South American rights. The Condition, Elevation, Emigration, and Destiny of the Colored People of the United States |Martin R. Delany