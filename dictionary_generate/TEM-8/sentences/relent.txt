Cruz’s office declined for many hours to confirm the trip, eventually relenting when the senator decided to come home Thursday afternoon. Sean Hannity’s attempt to cover for Ted Cruz goes poorly -- thanks to Ted Cruz |Aaron Blake |February 19, 2021 |Washington Post 

Google only relented on its own threat to pull its search engine out of Australia after signing deals with the country’s major publishers that would allow it to sidestep the most stringent parts of the law. Australia wants Facebook and Google to pay for news on their sites. Other countries think it’s a good idea too |Gerrit De Vynck |February 19, 2021 |Washington Post 

A dry weekend offers relief, but cold temperatures do not relent. Updated forecast: Steady sleet wanes, but spotty mixed precipitation possible into early Friday |David Streit, Jason Samenow |February 18, 2021 |Washington Post 

The council passed a measure that encouraged the song’s playing before sporting events, and Ehlers relented. Mavericks’ Mark Cuban isn’t the first to try to stop playing the national anthem |Matt Bonesteel |February 10, 2021 |Washington Post 

Express forecastForecast in detailAn unsettled, wintry weather regime took hold in late January and shows no sign of relenting this week. D.C.-area forecast: Cold today before another possible winter storm starting Wednesday night |Jason Samenow |February 8, 2021 |Washington Post 

She begged their father to contact him, refusing to relent until he sent an inquiring telegram. Knocking on Heaven's Door: True Stories of Unexplained, Uncanny Experiences at the Hour of Death |Patricia Pearson |August 11, 2014 |DAILY BEAST 

Only then does Trudy relent, saying that he should get an apartment in Manhattan. ‘Mad Men’ Returns: A Recap of Season Five |Jace Lacob |April 5, 2013 |DAILY BEAST 

Then, in 1937, in a 5–4 decision, the Justices began to relent. Is the Supreme Court’s Health-Care Ruling a Turning Point in Constitutional Law? |Randy Barnett |June 28, 2012 |DAILY BEAST 

By early Wednesday morning, Abedin had returned, but Weiner sent no signals to Washington that he was ready to relent. How the Dems Toppled Weiner |Patricia Murphy |June 17, 2011 |DAILY BEAST 

And the senate, pressured by businesses, is in no mood to relent. Why Arizona is Retreating on its Immigration Law |Terry Greene Sterling |March 19, 2011 |DAILY BEAST 

Pultusk and Eylau bore witness to his bravery and address on the battlefield, and Napoleon began to relent. Napoleon's Marshals |R. P. Dunn-Pattison 

Anyway she has refused—and will, I fancy, never relent—to allow any extreme idea of food shortage to disturb her routine. Punch, or the London Charivari, Vol. 152, May 16, 1917. |Various 

Fate had been fickle and cold so long; but now, when her smile was worse than a frown, she could easily relent. A Fortune Hunter; Or, The Old Stone Corral |John Dunloe Carteret 

But the King would neither give him release, nor relent towards the Americans. The Loyalists of America and Their Times, Vol. 1 of 2 |Egerton Ryerson 

The idea struck me that if I were to attack Monsieur Roberge on his weak point, he might relent. Autobiographical Reminiscences with Family Letters and Notes on Music |Charles Gounod