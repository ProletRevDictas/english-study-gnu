Troye held a key role on the coronavirus task force but also carried out an array of other duties for the vice president, advising him on mass shootings, immigration, hurricanes and some foreign affairs issues, she said. Former Pence aide says she will vote for Biden because of Trump’s ‘flat out disregard for human life’ during pandemic |Josh Dawsey |September 17, 2020 |Washington Post 

New York's boom years, at the beginning of two successive centuries, coincided with its peak levels of immigration. America's cities are facing an immigration deficit |Felix Salmon |September 17, 2020 |Axios 

She was nevertheless ordered deported in an immigration court ruling that she is now appealing before the 11th Circuit. A woman in ICE detention says her fallopian tube was removed without her consent |Nicole Narea |September 17, 2020 |Vox 

So long as immigration remains suppressed, New York will suffer. The math of New York City's recovery |Felix Salmon |September 17, 2020 |Axios 

The president told a voter he would offer an immigration plan soon. Trump, in town hall, says he wouldn’t have done anything differently on pandemic |Colby Itkowitz, Josh Dawsey, Felicia Sonmez, John Wagner |September 16, 2020 |Washington Post 

And so the same creeping rot of the rule of law that the administration has inflicted on immigration now bedevils our drug laws. Obama’s Pot Policy Is Refer Madness |James Poulos |January 5, 2015 |DAILY BEAST 

But failing that, he advised pro-immigration reform Republican candidates such as former Gov. Jeb Bush to just skip the state. Can This Republican Bring the GOP Back to Its Senses on Immigration? |Tim Mak |December 29, 2014 |DAILY BEAST 

Hispanics, notes a recent Pew survey economic issues easily trump immigration. Time to Bring Back the Truman Democrats |Joel Kotkin |December 21, 2014 |DAILY BEAST 

As a man who helped people become fugitives to save their skins, his focus on immigration is rooted in a personal life experience. How Pope Francis Became the World’s BFF |Jason Berry |December 21, 2014 |DAILY BEAST 

Rubio has his own troubles with immigration, but people close to him said he still may have a path even with a Bush candidacy. How A Jeb Bush Candidacy Would Hurt Chris Christie And Shake Up The 2016 GOP Field |David Freedlander |December 16, 2014 |DAILY BEAST 

The newer nations boasted proudly of their immigration tables. The Unsolved Riddle of Social Justice |Stephen Leacock 

The number of communicants, at the latest date, was thirty, and the field of labor was rapidly enlarging by immigration. The Condition, Elevation, Emigration, and Destiny of the Colored People of the United States |Martin R. Delany 

This large immigration made it easy for French spies and revolutionary agents to carry on their work undetected. The Political History of England - Vol. X. |William Hunt 

What anthropologist accepts the theory of Aryan overland immigration from somewhere in Asia? Archaic England |Harold Bayley 

The year 1803 was remarkable in the history of the island for a large immigration of highlanders from Scotland. History of Prince Edward Island |Duncan Campbell