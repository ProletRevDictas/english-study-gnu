He got his wish for a silly role Saturday night, returning to the desk for the 109th anniversary of the Titanic’s demise to play the iceberg that sank the British luxury liner. Bowen Yang steals SNL playing the iceberg that sank the Titanic |Sonia Rao |April 11, 2021 |Washington Post 

I don’t feel like a lot of the men, especially men in power, stand up in their position and hold on to the women who are about to fall over the Titanic and make sure the ship doesn’t sink. 21 Years After Her Debut, Trina Is Still ‘Da Baddest’ |Brande Victorian |March 19, 2021 |Essence.com 

From abroad, movies including Titanic, Frozen and Harry Potter and the Philosopher’s Stone have topped the box office charts. Everything to Know About Demon Slayer: The Manga, TV Series and Record-Breaking Film |Kat Moon |February 24, 2021 |Time 

There is likely to be a titanic legal battle one day between local governments and the fossil-fuel energy companies that officials blame for the climate-change damage they say their cities have suffered. Supreme Court hears dispute with big implications for climate-change litigation |Robert Barnes |January 19, 2021 |Washington Post 

The minute-long video features a band playing, “Nearer My God To Thee,” believed to be the final song played by the band on the Titanic. Music made 2020 better, but we failed to make 2020 better for musicians |Brian Heater |December 25, 2020 |TechCrunch 

“A turning point in my life was when I watched the movie Titanic,” Yeonmi told the audience at the Oslo Freedom Forum. How ‘Titanic ’Helped This Brave Young Woman Escape North Korea’s Totalitarian State |Lizzie Crocker |October 31, 2014 |DAILY BEAST 

You know, James Cameron: the director of The Terminator, True Lies, Titanic, and Avatar, among other obscure movies. James Cameron Dives into the Ocean's Abyss |Andrew Romano |July 21, 2014 |DAILY BEAST 

And while I may have put a bunch of stunt guys in peril on Titanic, it was my ass in the sphere on the dive. James Cameron Dives into the Ocean's Abyss |Andrew Romano |July 21, 2014 |DAILY BEAST 

The American people largely thought him a crazy man in 1964, and of course he lost to Johnson by titanic proportions. The Roots of the GOP’s Race Problem |Michael Tomasky |May 22, 2014 |DAILY BEAST 

Titanic once bet $10,000 that Nick (the Greek) Dandolos, another high operator, would not sink a 25-foot putt. Portrait of the Consummate Con Man |John Lardner |May 17, 2014 |DAILY BEAST 

Then came the end: the Titanic, with a low long slanting dive went down and with her Thomas Andrews. Fifty Years of Railway Life in England, Scotland and Ireland |Joseph Tatlow 

She was here, and the place was about to be blasted by some titanic explosive of the Croen science creation! Valley of the Croen |Lee Tarbell 

At every turn in the river the panorama changed, and they beheld new marvels of this Titanic architecture. Overland |John William De Forest 

So far as can be ascertained, it was never heard of by anyone on board the Titanic outside the Marconi room. Loss of the Steamship 'Titanic' |British Government 

This message was from the steamship Californian to the steamship Antillian, but was picked up by the Titanic. Loss of the Steamship 'Titanic' |British Government