With Democrats defending a fragile majority, Republicans are bullish about their chances of taking back the House in 2022. How Rep. Marjorie Taylor Greene Became a Leadership Test for Kevin McCarthy |Lissandra Villa |February 4, 2021 |Time 

Monday's price action showed there is still a contingent of bullish traders willing to bargain hunt after stocks fall. Stock indexes bounce back as traders return to buy the dip |Dion Rabouin |February 2, 2021 |Axios 

The company is also betting that corporate travel trends will fundamentally change even in the most bullish scenarios. How will the pandemic reshape corporate travel? |Lucinda Shen |January 21, 2021 |Fortune 

Dealmakers, though, are the most bullish they have been in seven years around the M&A landscape. Forget A.I., cybersecurity is the new buzzword |Lucinda Shen |January 15, 2021 |Fortune 

Ironically, the DoJ’s lawsuit blocking the deal perhaps laid out the most bullish case for Plaid’s future in plain words. Why investors are excited to see the Plaid and Visa merger die |Lucinda Shen |January 13, 2021 |Fortune 

Tesla Motors, for example, which now enjoys a $30 billion market capitalization thanks to bullish investors. Capitalism Is Saving the Climate, You Hippies |Daniel Gross |September 24, 2014 |DAILY BEAST 

Investors are bullish in Aereo, too; the company has raised almost $100 million in funding. What the Aereo Decision Means for You |Kyle Chayka |June 25, 2014 |DAILY BEAST 

But on the big screen, this bullish Brit transformed into the quintessential Cockney accented tough guy with a big heart. Remembering Bob Hoskins, the Burly British Star of ‘Who Framed Roger Rabbit,’ Who Died at 71 |Lorenza Muñoz |April 30, 2014 |DAILY BEAST 

There were plenty of other reasons to be bullish about Sean Saves the World. The Failure of ‘Sean Saves the World’ Is Epically Disappointing |Kevin Fallon |January 29, 2014 |DAILY BEAST 

She was less bullish on the idea that the industry is changing to be more welcoming to movies starring women. 2013 Was the Year of Women at the Box Office |Kevin Fallon |December 10, 2013 |DAILY BEAST 

You love toothless satires; let me inform you, a toothless satire is as improper as a toothed sleekstone, and as bullish.' An Introduction to the Prose and Poetical Works of John Milton |Hiram Corson 

A toothless satire is as improper as a toothed sleck stone, and as bullish. Notes and Queries, Number 74, March 29, 1851 |Various 

"And curb securities naturally feel the influence of the bullish sentiment," Fiedler continued. Potash &amp; Perlmutter |Montague Glass 

His face was intelligent, dark, pleasing, and not at all John-Bullish. Passages From the English Notebooks, Volume 1 |Nathaniel Hawthorne 

In politics and religion this curious and very John Bullish unreason is still more apparent. Essays in English Literature, 1780-1860 |George Saintsbury