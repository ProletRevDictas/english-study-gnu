Sip Wines, an online platform launched in October largely as a response to the pandemic, tags wineries as sustainable or organic, socially responsible, led by women, family-owned and first-generation small business. Online wine sales continue to grow, but can they — or should they — replace local shops? |Dave McIntyre |February 26, 2021 |Washington Post 

In a relentless pandemic, nursing-home workers are worn down and stressed outOn the surface, October was another quiet month. A small town in denial comes face to face with the virus |Will Englund |February 26, 2021 |Washington Post 

An October survey by Seattle-based Wilkening Consulting and AAM found that nearly 30 percent of museums remain closed since lockdown last March. Could GoFundMe campaigns save our cultural collections? Las Vegas’s Pinball Hall of Fame is banking on it. |Kate Silver |February 26, 2021 |Washington Post 

In October, BarFly announced it was being purchased by two investment firms. The wave of covid bankruptcies has begun |Jonathan O'Connell, Anu Narayanswamy |February 26, 2021 |Washington Post 

PG&E’s filing is in response to a breach of contract lawsuit filed against the utility in October by Bay Area Concrete. Lawsuit Reveals New Allegations Against PG&E Contractor Accused of Fraud |by Scott Morris, Bay City News Foundation |February 26, 2021 |ProPublica 

Grindr introduced the feature themselves in October the same year and called it ‘tribes.’ Grindr’s Trans Dating Problem |David Levesley |January 9, 2015 |DAILY BEAST 

In October, he traveled to Denver with Fry to support his work with LGBT rights organization The Matthew Sheppard Foundation. Meet Stephen Fry’s Future Husband (Who Is Less Than Half His Age) |Tom Sykes |January 6, 2015 |DAILY BEAST 

According to the AP, as of October, there were only four people still alive who be affected by this legislation. Nazis, Sunscreen, and Sea Gull Eggs: Congress in 2014 Was Hella Productive |Ben Jacobs |December 29, 2014 |DAILY BEAST 

What got leaked to the St. Louis Post-Dispatch on October 22? Michael Tomasky’s Year-End Quiz: Test Your 2014 News Knowledge |Michael Tomasky |December 26, 2014 |DAILY BEAST 

Krivov was arrested in October 2012, on the dubious charges of participation in “mass riots.” Behind Bars for the Holidays: 11 Political Prisoners We Want to See Free In 2015 |Movements.Org |December 25, 2014 |DAILY BEAST 

Napoleon himself arrived at Wrzburg on October 2nd, and found his army concentrated, but deficient of supplies. Napoleon's Marshals |R. P. Dunn-Pattison 

September died away in the brown arms of October, and at last a letter came from Nigel. Bella Donna |Robert Hichens 

He was at once arrested, and on October 13th tried by court martial, condemned to death, and executed a few hours later. Napoleon's Marshals |R. P. Dunn-Pattison 

The day was perfect; as clear and bright, as mellow and crisp, as rich in colour, as only an October day in England can be. Fifty Years of Railway Life in England, Scotland and Ireland |Joseph Tatlow 

The great pressure-engine I expect will be at work before the middle of October. Life of Richard Trevithick, Volume II (of 2) |Francis Trevithick