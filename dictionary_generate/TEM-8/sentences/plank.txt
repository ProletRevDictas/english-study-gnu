This house featured wide plank flooring and a massive chimney. This might be Washington’s oldest house, but it came from New England |John Kelly |November 28, 2020 |Washington Post 

If this variation is too easy, do the move from a plank position. Strength Training for Lower-Back Pain |Lauren Bedosky |November 18, 2020 |Outside Online 

Luckily for Signet, e-commerce was already a big plank in Drosos’s three-year “Path to Brilliance” turnaround plan, now in its third year. The company behind Kay and Zales wants to give jewelry shopping a pandemic-era makeover |Phil Wahba |November 12, 2020 |Fortune 

To take the challenge even further, try it in a full plank position. Jessie Diggins's Killer 8-Minute Core Workout |Hayden Carpenter |October 30, 2020 |Outside Online 

Go as far as you can while maintaining a strong plank position—no lifting, sagging, or rotating the hips—and keep your core engaged throughout the movement. Jessie Diggins's Killer 8-Minute Core Workout |Hayden Carpenter |October 30, 2020 |Outside Online 

Lacking any sense of irony, Eldridge made campaign-finance reform a signature plank. The Rise and Fall of Chris Hughes and Sean Eldridge, America’s Worst Gay Power Couple |James Kirchick |December 9, 2014 |DAILY BEAST 

We all jumped from our seats and stood rigid as plank boards. I Shot Bin Laden |Elliot Ackerman |November 16, 2014 |DAILY BEAST 

It has a lovely rustic feel with plank wooden floors and uncovered fireplaces. The Hell of the Hamptons: Why the Exclusive Hotspot Is a Mind-Numbing Drag |Robert Gold |August 18, 2014 |DAILY BEAST 

And John McCain actually had a decent-ish health-care platform plank in 2008. The GOP’s 20-Year War on Health Care |Michael Tomasky |July 25, 2014 |DAILY BEAST 

First Duggan defied him, then Rusev smashed Duggan's plank over his knee. Putin Vs. Obama—In Spandex: Wrestling’s New Cold War |Tim Teeman |May 14, 2014 |DAILY BEAST 

The body was resting upon a plank supported by four stakes and covered with skins. The Jesuit Relations and Allied Documents, Vol. II: Acadia, 1612-1614 |Various 

I like sus-sus-pen-sheen bridges that fly from bank to bank, with one big step, like a gang-plank. Kipling Stories and Poems Every Child Should Know, Book II |Rudyard Kipling 

He did not tell the name of his friend, who, as if loath to cross the plank, held back for a few more words. The Cromptons |Mary J. Holmes 

Use in packing a plank or board, placing it against the front of the tier and bring the ends of the hands up against it. Tobacco; Its History, Varieties, Culture, Manufacture and Commerce |E. R. Billings. 

The plank fell to its horizontal position, bringing her head under the fatal ax. Madame Roland, Makers of History |John S. C. Abbott