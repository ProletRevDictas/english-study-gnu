We see detoxing as a path to transcendence, a symbol of modern urban virtue and self-transformation through abstinence. How Taryn Toomey’s ‘The Class’ Became New York’s Latest Fitness Craze |Lizzie Crocker |January 9, 2015 |DAILY BEAST 

If anything the work the two cops and the maintenance guy were doing deserves more respect and probably helped a lot more people. Trolls and Martyrdom: Je Ne Suis Pas Charlie |Arthur Chu |January 9, 2015 |DAILY BEAST 

Stephanie Giorgio, a classical musician, credits The Class for helping her cope with anxiety, focus, fear, and self-doubt. How Taryn Toomey’s ‘The Class’ Became New York’s Latest Fitness Craze |Lizzie Crocker |January 9, 2015 |DAILY BEAST 

For someone with anorexia, self-starvation makes them feel better. How Skinny Is Too Skinny? Israel Bans ‘Underweight’ Models |Carrie Arnold |January 8, 2015 |DAILY BEAST 

I went into the audition as Fericito, the Venezuelan percussionist, and then I did a self-defense expert. Coffee Talk with Fred Armisen: On ‘Portlandia,’ Meeting Obama, and Taylor Swift’s Greatness |Marlow Stern |January 7, 2015 |DAILY BEAST 

Let the thought of self pass in, and the beauty of great action is gone, like the bloom from a soiled flower. Pearls of Thought |Maturin M. Ballou 

Now this setting up of an orderly law-abiding self seems to me to imply that there are impulses which make for order. Children's Ways |James Sully 

At present, Louis was too self-absorbed by the struggles within him, to look deep into what was passing around him. The Pastor's Fire-side Vol. 3 of 4 |Jane Porter 

But to wave this discourse of Heathens, how many self-contradicting principles are there held among Christians? The Life and Most Surprising Adventures of Robinson Crusoe, of York, Mariner (1801) |Daniel Defoe 

It is, however, true, that in this respect the German hexametrist has a considerable advantage over the English. Blackwood's Edinburgh Magazine, Volume 60, No. 372, October 1846 |Various