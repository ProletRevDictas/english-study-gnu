This is the termination of the present participle and verbal adjective derived from verbs in -a. Frdric Mistral |Charles Alfred Downer 

Bessoun (in French, besson) means a twin, and the participle expresses the idea, clasped together like twins. Frdric Mistral |Charles Alfred Downer 

"Nay, I wanna thinking-g o' that," Ned replied, strongly doubling the "g" that terminated the present participle. Mushroom Town |Oliver Onions 

In this participle the termination -ing seems almost equivalent to that of the past participle: comp. Milton's Comus |John Milton 

Trod (or trodden), past participle of tread: ‘to tread a measure’ is a common expression, meaning ‘to dance.’ Milton's Comus |John Milton