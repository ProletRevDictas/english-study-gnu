It marked the first time the organization agreed to change a bird’s name because it was racially offensive. Inside the Movement to Abolish Colonialist Bird Names |Nathalie Alonso |February 12, 2021 |Outside Online 

He risks upsetting the fans, not to mention his offensive line. The NFL’s top QBs are waking up to their power, following Tom Brady and LeBron James |Jerry Brewer |February 11, 2021 |Washington Post 

Okay, Einav Hart is a good sport, but our goal today isn’t to ask questions that are outright offensive. Can I Ask You a Ridiculously Personal Question? (Ep. 451) |Stephen J. Dubner |February 11, 2021 |Freakonomics 

Heinicke, who was set to become a restricted free agent in March, said he wanted to stay with Washington because a two-year deal gives him security and he feels comfortable with Coach Ron Rivera and offensive coordinator Scott Turner. Quarterback Taylor Heinicke signs two-year contract to return to Washington Football Team |Sam Fortier |February 10, 2021 |Washington Post 

Taking advanced metrics like those into account, Turner’s defensive contributions stand out far more than his traditional offensive numbers do. Myles Turner’s Game Has Evolved. The Box Score Doesn’t Know It Yet. |Chris Herring (chris.herring@fivethirtyeight.com) |February 10, 2021 |FiveThirtyEight 

There is no such thing as speech so hateful or offensive it somehow “justifies” or “legitimizes” the use of violence. Trolls and Martyrdom: Je Ne Suis Pas Charlie |Arthur Chu |January 9, 2015 |DAILY BEAST 

It is not only clerics and Islamic ideologues who use offensive words and images to describe the sexual life of Westerners. 50 Shades of Iran: The Mullahs’ Kinky Fantasies about Sex in the West |IranWire, Shima Sharabi |January 1, 2015 |DAILY BEAST 

After all, the Russians were about to mount a winter offensive of their own. Hitler’s Hail Mary |James A. Warren |December 20, 2014 |DAILY BEAST 

Seeking to be celebrated for simply hiring a woman is tokenizing and offensive. Wonder Woman Takes a Big Step Back |Hugh Ryan |December 16, 2014 |DAILY BEAST 

The story was so appalling, the attack so brutish and morally offensive, that it provoked an immediate, furious response. Why It Was Right to Question Rolling Stone’s U-VA Rape Story |Michael Moynihan |December 5, 2014 |DAILY BEAST 

It being offensive to the French, they took none of it with them on their return. Tobacco; Its History, Varieties, Culture, Manufacture and Commerce |E. R. Billings. 

They determined that an offensive war should be carried on against them, and voted to raise 90 men! The Every Day Book of History and Chronology |Joel Munsell 

But he is as insolent as you could wish, and has a superb confidence in himself that his enemies call by the most offensive names. Ancestors |Gertrude Atherton 

The climax was reached when a most offensive policeman in a dictatorial manner ordered me to 'Move on.' The Pit Town Coronet, Volume I (of 3) |Charles James Wills 

Her directness had made all possible 'buts' seem ridiculous and futile, and had made the expression of curiosity seem offensive. Hilda Lessways |Arnold Bennett