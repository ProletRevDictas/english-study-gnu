Such dark matter, a vast abyss, is evidence of a viral world we must come to understand better. The Vast Viral World: What We Know (and Don’t Know) - Issue 99: Universality |Lauren E. Oakes |April 7, 2021 |Nautilus 

Doolittle, for example, is just one of many veteran relievers floating through this offseason abyss. The Nationals’ bullpen doesn’t need a makeover. But it could use another arm or two. |Jesse Dougherty |January 14, 2021 |Washington Post 

Out of the abyss of the primary came Darrell Issa, who served for nine terms in Congress in the neighboring 49th Congressional District. The Ultimate Guide to the Local Election |Voice of San Diego |October 19, 2020 |Voice of San Diego 

In this age of advanced gene technology, the true abyss of renunciation from which we speak “I” is only now becoming obvious to us. What the Meadow Teaches Us - Issue 90: Something Green |Andreas Weber |September 16, 2020 |Nautilus 

Big names use their longevity, time-tested popularity, and a fat purse to create a monopoly and call the market shot — and having earned proprietary eponyms, they push low-budget upcoming competitors into the abyss. Inbound marketing for brand awareness: Four up-to-date ways to do it |Ali Faagba |September 11, 2020 |Search Engine Watch 

Hollywood, too, became enraptured by the exotic abyss of Stanleyville. ‘Argo’ in the Congo: The Ghosts of the Stanleyville Hostage Crisis |Nina Strochlic |November 23, 2014 |DAILY BEAST 

But, Ali warns all that can change quickly if Anbar continues to crumble, “right now, we are looking into the Abyss.” Iraqis Swear: Baghdad Airport is Safe From ISIS |Susannah George |October 13, 2014 |DAILY BEAST 

And my beloved Zimbabwe has sunk from a promising beacon into an abyss of greed and dictatorship. How I Got Addicted to Africa (and Wrote a Thriller About It) |Todd Moss |September 9, 2014 |DAILY BEAST 

But when patients open their jaws, he totters on the abyss and gets no steadying hand from God. Joshua Ferris’s New Novel Chronicles an Existential Dentist in Despair |Tom LeClair |May 6, 2014 |DAILY BEAST 

And when I made the Abyss, which had a giant wave scene in it, those stopped. James Cameron on How to Find Flight MH370, Climate Change, Leonardo DiCaprio, and More |Marlow Stern |April 12, 2014 |DAILY BEAST 

He sees no longer the brink of the abyss beside which the path of progress picks its painful way. The Unsolved Riddle of Social Justice |Stephen Leacock 

In the abyss of her heart, too deep at first for recognition, the girl loved him, and had loved him from the very beginning. Three More John Silence Stories |Algernon Blackwood 

The path leading to it is over a small ledge of rock, skirted on each side by a yawning abyss. A Woman's Journey Round the World |Ida Pfeiffer 

The poor, tearful desire lays a pale hand on reason's lips and gazes wistfully into the mysterious abyss of the Great Silence. God and my Neighbour |Robert Blatchford 

She points to her pinions stretched over the abyss of primeval fire, her eyes blinded by its awful glare, and remains silent. Gospel Philosophy |J. H. Ward