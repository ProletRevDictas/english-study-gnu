He was due to begin treatment Monday, the day after he died. A rare disease, a covid diagnosis, a painful decision: The death of basketball coach Lew Hill |Dave Sheinin |February 11, 2021 |Washington Post 

Schottenheimer, who died Monday at the age of 77 after being diagnosed with Alzheimer’s in 2014, was something of surprise hire to replace Norv Turner before the 2001 season. Marty Schottenheimer’s legacy can be measured in teams’ regret over letting him go |Leonard Shapiro |February 9, 2021 |Washington Post 

The Lakers marked the anniversary of the crash in a low-key way last month, and Bryant’s widow, Vanessa, asked people to focus on the victims’ lives rather than the way they died. Helicopter pilot flying Kobe Bryant didn’t follow his training when flying into disorienting clouds, federal investigators say |Ian Duncan |February 9, 2021 |Washington Post 

Though it can be frustrating to die frequently in these scenarios, with each death you hone your strategy to get the timing right, making it satisfying in the long run. ‘Little Nightmares II’ made me dread every moment. And I loved it. |Elise Favis |February 9, 2021 |Washington Post 

The patient had used up all her time planning her brother’s funeral after he died of covid-19, the disease caused by the virus. Lacking a Lifeline: How a federal effort to help low-income Americans pay their phone bills failed amid the pandemic |Tony Romm |February 9, 2021 |Washington Post 

Yves Albarello, MP of Seine-et-Marne, said the gunmen told police they were ready to “die as martyrs.” France Kills Charlie Hebdo Murderers |Nico Hines |January 9, 2015 |DAILY BEAST 

Asserting our right to free speech is the only to ensure that 12 people did not die in vain. Ayaan Hirsi Ali: Our Duty Is to Keep Charlie Hebdo Alive |Ayaan Hirsi Ali |January 8, 2015 |DAILY BEAST 

Though this too is debatable given that 25,000 to 40,000 people a year die of influenza—the vast majority of them unvaccinated. When You Get the Flu This Winter, You Can Blame Anti-Vaxxers |Kent Sepkowitz |January 1, 2015 |DAILY BEAST 

They made it home, after which he did die, she nursing him to the end. The Real-Life ‘Downton’ Millionairesses Who Changed Britain |Tim Teeman |December 31, 2014 |DAILY BEAST 

It is a multimillion-dollar business in which roughly 15 million fowl die a year. The History of the Chicken: How This Humble Bird Saved Humanity |William O’Connor |December 27, 2014 |DAILY BEAST 

"A camp-fire would hardly flash and die out like that, Sarge," he answered thoughtfully. Raw Gold |Bertrand W. Sinclair 

But men, through neglecting the rules of health, pass quickly to old age, and die before reaching that term. The Mediaeval Mind (Volume II of II) |Henry Osborn Taylor 

I cannot believe that God would think it necessary to come on earth as a man, and die on the Cross. God and my Neighbour |Robert Blatchford 

With three or four more wounds, and the words with which he aided her to die, he finished with her. The Philippine Islands, 1493-1898, Volume XX, 1621-1624 |Various 

His symptoms became so serious that often we expected nothing less than that he would die on our hands. The Jesuit Relations and Allied Documents, Vol. II: Acadia, 1612-1614 |Various