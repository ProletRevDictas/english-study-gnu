On our recent vacation trip to Portugal, I spotted a woman in a fetching dress I thought my lovely wife might like. Quirky coincidences enliven vacation travel |John Kelly |August 3, 2022 |Washington Post 

In this world, once-proud physicians are over-prescribing and over-ordering, grinning and pretending, stepping and fetching. You Can't Yelp Your Doctor |Daniela Drake |May 21, 2014 |DAILY BEAST 

He changed his name to Ronnie Rocket, becomes a bona fide rock star, and attracts a fetching tap-dancer, Electra-Cute. Doomed Passion Projects of Hollywood: The Lost Classics of Stanley Kubrick, Alfred Hitchcock, and More |Marlow Stern |March 28, 2014 |DAILY BEAST 

He was surrounded by friends and family, and women—one was fetching him a piece of cake. The Stacks: The True Greatness of Muhammad Ali |Peter Richmond |February 23, 2014 |DAILY BEAST 

Fetching, gracious, ladylike, she has devoted her adult life to taking care of Mitt and the boys. When Good Wives Attack: Ann Romney’s Tricky Defense of Mitt |Michelle Cottle |September 22, 2012 |DAILY BEAST 

Fetching French actress Audrey Tautou is back in the romantic drama “Delicacy.” Audrey Tautou on ‘Amélie,’ Her New Film ‘Delicacy,’ & More |Marlow Stern |March 18, 2012 |DAILY BEAST 

Anyway there was a lot of embroidery on it, full of little holes, which somehow contrived to be extraordinarily fetching. Rosemary in Search of a Father |C. N. Williamson 

The fancy costumes and the funny masks the girls and boys wore certainly were “fetching.” The Campfire Girls of Roselawn |Margaret Penrose 

Ever see anything more fetching than those great Irish eyes in a regular little Dago mug? Ancestors |Gertrude Atherton 

Fetching pen and paper, the blacksmith made a rapid computation of what would be due Oliver at any time within the next month. Dorothy at Skyrie |Evelyn Raymond 

And there need never be any difficulty about sending you all three to school and fetching you, even when it is not holiday time. Robin Redbreast |Mary Louisa Molesworth