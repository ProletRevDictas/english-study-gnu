Her move also broke with the “own the libs” ethos of today’s GOP. What Does Impeachment And The Division Over Marjorie Taylor Greene And Liz Cheney Tell Us About The GOP? |Sarah Frostenson (sarah.frostenson@abc.com) |February 3, 2021 |FiveThirtyEight 

It has fueled the “eat the rich” ethos behind the GameStop frenzy. ‘Anonymous’ fintech startup Millions raises $3 million, gives away cash on Twitter |Sarah Perez |February 2, 2021 |TechCrunch 

This spirited, trial-and-error ethos courses through New York University’s theatrical production wing, which has been holding live performances since last October. A careful return to live theater is coming down to costume design |Anne Quito |February 2, 2021 |Quartz 

The company did just hire a CFO, which makes this move appear in concert with its general ethos, so more to come there we presume. How trading apps are responding to the GameStop fustercluck |Alex Wilhelm |January 27, 2021 |TechCrunch 

On the mainland, the pandemic has inspired projects guided by the doughnut’s ethos. Amsterdam Is Embracing a Radical New Economic Theory to Help Save the Environment. Could It Also Replace Capitalism? |Ciara Nugent |January 22, 2021 |Time 

When Cocker took on board the black American ethos, he turned it into something completely different. The Greatest Rock Voice of All Time Belonged to Joe Cocker |Ted Gioia |December 23, 2014 |DAILY BEAST 

The best, or at least most successful, are bridging the gap between punk-rock DIY ethos and social-media savvy. On Tour With The Head and the Heart, Indie Rock’s Next Big Thing |James Joiner |December 17, 2014 |DAILY BEAST 

But very little of the ISIS ethos has to do with hitting the Freedom Tower or the Capitol Dome. Iraq Is Not Our War Anymore. Let It Be Iran’s Problem. |Christopher Dickey |July 17, 2014 |DAILY BEAST 

The emphasis on freshness and simplicity laid forth by the governmental guidelines is in line with his cooking ethos. Meet the Chef Fighting to Ensure That Brazilians Will Never Be as Fat as Americans |Brandon Presser |June 25, 2014 |DAILY BEAST 

More importantly, the evangelistic ethos is supposed to infuse everyday life. Did the Southern Baptist ‘Conservative Resurgence’ Fail? |Molly Worthen |June 1, 2014 |DAILY BEAST 

For in the view of those who distinguish harmonia from tonos it is the harmonia upon which the ethos of music depends. The Modes of Ancient Greek Music |David Binning Monro 

These moveable notes, then, give an ethos to the music because they determine the genus of the scale. The Modes of Ancient Greek Music |David Binning Monro 

"Ethics" were things which pertained to the ethos and therefore the things which were the37 standard of right. Folkways |William Graham Sumner 

By virtue of the latter element the mores are traits in the specific character (ethos) of a society or a period. Folkways |William Graham Sumner 

In the war with Russia, in 1904, this people showed what a group is capable of when it has a strong ethos. Folkways |William Graham Sumner