The hydrovac trucks generate potentially toxic slurry waste. The Mystery House: How a Suspicious Multimillion Dollar Real Estate Deal Is Connected to California’s Deadliest Fire |by Scott Morris, Bay City News Foundation |August 26, 2020 |ProPublica 

I also think that media buying has a very aggressive, toxic culture tied to it. ‘The dollar amount isn’t worth the mental toll’: Confessions of a media buyer on the pressure to keep performance up amid the pandemic |Kristina Monllos |August 25, 2020 |Digiday 

They require lots of energy and produce large volumes of toxic gas too, creating more pollution and leaving a large carbon footprint. We’re Using Microbes to Clean Up Toxic Electronic Waste. Here’s How |Sebastien Farnaud |August 20, 2020 |Singularity Hub 

A working sense of taste also helps protect us from eating something spoiled or toxic. Newly discovered cells in mice can sense four of the five tastes |Carolyn Wilke |August 13, 2020 |Science News 

Other MOFs can filter toxic chemicals from the air and then store them or help break them down. Here’s one way to harvest water right out of the air |Sid Perkins |April 24, 2020 |Science News For Students 

But it is not actually as toxic as it seems (although, admittedly, it does seem pretty toxic). New York’s Conservative Fracking Ban |Jay Michaelson |December 20, 2014 |DAILY BEAST 

Infernal, it can cause fires and explosions; toxic, it can debilitate, poison, and kill. The Fiery Underground Oil Pit Eating L.A. |Geoff Manaugh |December 6, 2014 |DAILY BEAST 

But as her audience broadens, you have to wonder, is the big-time toxic to her art? Is Bigger Better for St. Vincent? |David Yaffe |December 4, 2014 |DAILY BEAST 

Republicans have a rare opportunity to implement policies that are truly compassionate and transcend toxic identity politics. How a GOP Senate Can Help the Poor |Veronique de Rugy |November 23, 2014 |DAILY BEAST 

An autopsy found highly toxic cyanide levels in the blood of the not-so-dearly departed. Beware of Japan’s “Black Widows” |Jake Adelstein |November 20, 2014 |DAILY BEAST 

As he finds rag weed more toxic than the English timothy, his actual first dose is one-half of this theoretical dose. The Treatment of Hay Fever |George Frederick Laidlaw 

His experiments show that amorphous phosphorus was not toxic to animals. The Propaganda for Reform in Proprietary Medicines, Vol. 1 of 2 |Various 

As before, I see it turn brown and die on the spot, still half inside the toxic corpse. More Hunting Wasps |J. Henri Fabre 

His constitution has never thrown off the malady resulting from this toxic (poisonous) agent. A Mortal Antipathy |Oliver Wendell Holmes, Sr. 

The drowsy feeling which we experience in a crowded, ill-ventilated room is due entirely to the influence of these toxic gases. Nervous Breakdowns and How to Avoid Them |Charles David Musgrove