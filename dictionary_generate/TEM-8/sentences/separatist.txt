Matrix is a rich, beautifully written novel about ambition and desire, and also witchy separatist medieval nuns. The Vox Book Club is spending October reading and talking to Lauren Groff |Constance Grady |September 30, 2021 |Vox 

By terrorists, China principally means the Islamic State group, which has a worrying presence in Afghanistan, and Uyghur separatists. Butterfly Effect: Is China Ready to Step Up in Afghanistan? |Charu Kasturi |April 23, 2021 |Ozy 

At the 1993 awards, the first time they were broadcast live in China, actor Richard Gere expressed support for the Dalai Lama, the Tibetan spiritual leader China considers a separatist. China’s Oscars boycott mixes politics with push to curb Hollywood dominance |Lyric Li, Steven Zeitchik |April 22, 2021 |Washington Post 

While county leaders where the city would be are pushing back against this kind of “separatist government control,” they remain interested in the possibilities the futuristic city represents. Inside the Newest Crypto Craze: The NFT |Joshua Eferighe |March 4, 2021 |Ozy 

Next came news that Erdogan’s government had arrested more than 700 people, including members of a pro-Kurdish political party on suspicion of ties to separatists. How Erdogan's Increasingly Erratic Rule in Turkey Presents a Risk to the World |Ian Bremmer |March 4, 2021 |Time 

We have Matthew Rhys from The Americans as a Welsh separatist. ‘Archer’ Creator Adam Reed Spills Season 6 Secrets, From Surreal Plotlines to Life Post-ISIS |Marlow Stern |January 8, 2015 |DAILY BEAST 

Kurnosova does not welcome such separatist movements in Russia. Russians Plot Exiled Government in Kiev |Anna Nemtsova |December 16, 2014 |DAILY BEAST 

A ceasefire was in effect, but the Ukrainian army was still fighting with pro-Russian separatist forces in the east. Ukraine’s Home Front Grows War Weary |Anna Nemtsova |October 23, 2014 |DAILY BEAST 

In this capacity, Spitz provided information on the German Red Cross and the Bavarian Separatist Movement in southern Germany. On the Trail of Nazi Counterfeiters |Dr. Kevin C. Ruffner |September 20, 2014 |DAILY BEAST 

More importantly, he has frustrated other separatists and those who would foment separatist movements, like China and Russia. Scotland Says Resounding ‘No’ to Independence |Nico Hines |September 19, 2014 |DAILY BEAST 

The separatist theories of the Homeric poems are not more secure than the Solar Myth, "like a wave shall they pass and be passed." Homer and His Age |Andrew Lang 

Tone then was not merely a Republican and a Separatist but a Democrat prepared for a democratic and revolutionary policy. The Evolution of Sinn Fein |Robert Mitchell Henry 

My legs developed separatist tendencies, and started on independent orbits. Punch, or the London Charivari, Vol. 102, June 11, 1892 |Various 

Robinson was the pastor of the Separatist congregation at Leyden. A Source Book in American History to 1787 |Various 

Separatist tendencies were stronger than those of coalescence. The Colonization of North America |Herbert Eugene Bolton