Given that the coronavirus is a novel virus, we need to ensure that our at-risk wild animal and sea life populations do not experience massive die-offs or suffer extinctions because we failed to act to protect them. Everything we know—and don’t know—about human-to-animal COVID transmission |jakemeth |September 4, 2020 |Fortune 

But, there’s still the chance that humans played their part in knocking the animals into extinction, as a sort of one-two punch. Climate change probably contributed to the woolly rhino’s rapid demise |Sara Kiley Watson |August 25, 2020 |Popular-Science 

Based on the new study, he says, “there is no evidence so far of human hunting being a deciding factor in woolly rhino extinction.” Climate change, not hunters, may have killed off woolly rhinos |Bruce Bower |August 13, 2020 |Science News 

How or whether volcanic activity in India around the same time as the impact exacerbated the climate change and mass extinction remains controversial. How Earth’s Climate Changes Naturally (and Why Things Are Different Now) |Howard Lee |July 21, 2020 |Quanta Magazine 

It came after the mass extinction that finished off the dinosaurs. Saber-toothed anchovy relatives were once fearsome hunters |Carolyn Wilke |June 11, 2020 |Science News For Students 

Pat Robertson wants to talk about the extinction of the gays. No Gods, No Cops, No Masters |James Poulos |January 1, 2015 |DAILY BEAST 

Your existence contributes to over-population, climate change, and species extinction. Keep Christmas Commercialized! |P. J. O’Rourke |December 6, 2014 |DAILY BEAST 

How might we resurrect a tradition threatened with extinction? Can Baseball’s All-Star Game Be Saved? |Peter C. Bjarkman |July 15, 2014 |DAILY BEAST 

And thanks to oil palm plantations springing up in Africa, chimpanzees are in danger of extinction. Our Taste for Cheap Palm Oil Is Killing Chimpanzees |Carrie Arnold |July 11, 2014 |DAILY BEAST 

Over this image we hear: “Freedom is never more than one generation from extinction.” Rick Santorum’s Hobby Lobby Horror Movie |Dean Obeidallah |July 2, 2014 |DAILY BEAST 

Poor brutes, they deserved a better fate than the cruel method of extinction which Turkish rule administered. Fifty Years of Railway Life in England, Scotland and Ireland |Joseph Tatlow 

With the extinction of the lights on the other boat came at last deeper night to her aid. The Amazing Interlude |Mary Roberts Rinehart 

I am bored to death, to extinction; my thoughts are the colour of that water which flows over yonder, brackish and heavy. The Nabob |Alphonse Daudet 

You must remember that the next war between France and Germany must mean extinction for one. Ways of War and Peace |Delia Austrian 

Further difficulties now arose which led finally to the extinction of the company. Encyclopaedia Britannica, 11th Edition, Volume 4, Part 3 |Various