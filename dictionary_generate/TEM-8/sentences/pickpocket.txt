This strap makes it harder for pickpockets to slash the strap in their attempt to steal it. Best RFID wallets to help you guard your digital identity |Ashley Hubbard |December 16, 2021 |Popular-Science 

The menacing thugs, petty criminals, and pickpockets sized up the intruder in their midst. New York’s First Crime Boss: Captain Isaiah Rynders |Mark Lawrence Schrad |September 10, 2021 |The Daily Beast 

Be mindful when you’re gawking at your surroundings — pickpockets abound! Five sensational vacation destinations from the virtual worlds of video games |Shelly Tan, Elise Favis, Gene Park, Armand Emamdjomeh |February 25, 2021 |Washington Post 

Stolen features a pickpocket with a cleavage to kill for who gets caught and has to return a wallet. Sexier Than ‘50 Shades of Grey’: Naughtiest Bits From 12 Other Erotica Bestsellers | |July 12, 2012 |DAILY BEAST 

The Thief tells the story of an anonymous pickpocket who steals as much for the thrill of the hunt as he does for the money. 3 Secret Novels: Coral Glynn, The Thief, and Bloodland |Claiborne Smith, Hugh Ryan, Jane Ciabattari |March 16, 2012 |DAILY BEAST 

Why not have kept him here among the rest, and made a sneaking, snivelling pickpocket of him at once? Oliver Twist, Vol. II (of 3) |Charles Dickens 

The pickpocket finds it easy to unsnap a handbag and remove some of the contents, especially in crowded places. The Boy Mechanic, Book 2 |Various 

He was out of it in an instant with the agility of a pickpocket, was across the room and at Hade's throat like a dog. Gallegher and Other Stories |Richard Harding Davis 

"Crowd chasing a pickpocket, I imagine," said Trencher indifferently. From Place to Place |Irvin S. Cobb 

"Say, boss, they've caught the pickpocket—if that's what he was," he cried out excitedly. From Place to Place |Irvin S. Cobb