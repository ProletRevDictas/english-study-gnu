She noted where and when conifer forests began to return, where they didn’t, and where opportunistic invasive species like cheatgrass took over the landscape. The pandemic slashed the West Coast’s emissions. Wildfires already reversed it. |James Temple |July 27, 2021 |MIT Technology Review 

With her gentle snuffling, the pig helps the man locate truffles as the sun streams through the cathedral of conifers around them. In Pig, Nicolas Cage Plays a Grouchy, Meditative Hermit—and Gives His Best Performance in Years |Stephanie Zacharek |July 15, 2021 |Time 

Even in systems where fire suppression is responsible for fuel buildup, such as mixed conifer stands in the Rockies and Sierra Nevada, fuel treatments can’t always affect the course of a fire. Underpaid firefighters, overstretched budgets: The U.S. isn’t prepared for fires fueled by climate change |Sarah Kaplan |July 1, 2021 |Washington Post 

The regional average for mixed conifer forests in the inland region is much lower than the average for similar forests in the coastal region. The Climate Solution Actually Adding Millions of Tons of CO2 Into the Atmosphere |by Lisa Song, ProPublica, and James Temple, MIT Technology Review |April 29, 2021 |ProPublica 

After the fireball, the ferns and conifers largely vanished. Dinosaur-killing asteroid radically changed Earth’s tropical forests |Carolyn Gramling |April 26, 2021 |Science News For Students 

Cheryl Brown, former CEO of the Conifer Council in Texas/Arkansas also saw the storm coming. Why Are Girl Scout Camps Being Closed? |Alessandra Rafferty |January 12, 2014 |DAILY BEAST 

Thus they encamped near the conifer, and called the place Toha-a-muk-is after the spruce they were afraid to touch. Indian Legends of Vancouver Island |Alfred Carmichael 

It suffices to name the families of the Conifer and the Amentace, which compose the greater portion of the Flora of our forests. The Desert World |Arthur Mangin 

The family of Conifer exhibit themselves in Australia, like every other group of plants, under strange and novel forms. The Desert World |Arthur Mangin 

The Australian species are comprised in a small number of families, notably in those of the Conifer and Myrtace. The Desert World |Arthur Mangin 

Its cones are the largest produced by any conifer, occasionally reaching the length of nearly two feet. Your National Parks |Enos A. Mills