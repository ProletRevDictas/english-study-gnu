Cotton and leather boot laces are simply no match for paracord’s breaking strength, high knot strength, or durability, then or now. This essential survival tool can save your life 10 different ways |By Tim MacWelch/Outdoor Life |September 15, 2020 |Popular-Science 

If such a fluid existed, then no matter what changes a vortex or group of linked vortices in the fluid went through, the number of links and knots would add up to the same number. An Unexpected Twist Lights Up the Secrets of Turbulence |David H. Freedman |September 3, 2020 |Quanta Magazine 

Vortex helicity has long been defined as the total number of links and knots in a vortex or in a connected group of vortices. An Unexpected Twist Lights Up the Secrets of Turbulence |David H. Freedman |September 3, 2020 |Quanta Magazine 

In the season 7 finale, George and Susan were finally tying the knot and George’s wallet was taking a beating. Science of ‘Seinfeld’ |LGBTQ-Editor |June 15, 2020 |No Straight News 

The rules predict only the relative strength of each knot — that is, whether one knot is stronger than another. Color-changing fibers help unravel a knotty problem |Emily Conover |January 31, 2020 |Science News For Students 

There is something irrevocable-feeling about couples tying the knot on the steps of the county courthouse. Gay Marriage Chaos Begins |Jay Michaelson |November 11, 2014 |DAILY BEAST 

Every day before leaving home, Sara stands before the mirror and tightens the knot on her scarf. Acid Attacks on Women Spread Terror in Iran |IranWire |October 18, 2014 |DAILY BEAST 

The most famous people in the world tied the knot secretly over the weekend. Angelina Jolie and Brad Pitt Got Married and We’re Worried About Jennifer Aniston |Kevin Fallon, Tim Teeman |August 28, 2014 |DAILY BEAST 

The moment he was finally able to loop a knot by himself was a milestone, his first step to becoming a man. Miami’s Chris Bosh Goes High Fashion |Justin Jones |August 13, 2014 |DAILY BEAST 

Star-studded guests arrived in fancy cars, and music and cheers rose above the castle walls as Kimye tied the knot. Eavesdropping On Kim and Kanye’s Florentine “Wedding of the Century” |Barbie Latza Nadeau |May 24, 2014 |DAILY BEAST 

At the head they insert a bamboo knot, with its point well sharpened into two edges. The Philippine Islands, 1493-1898, Volume XX, 1621-1624 |Various 

This is called the "Investiture of the Top-knot," and is always attended by solemn ceremonies. Our Little Korean Cousin |H. Lee M. Pike 

In accordance with that statement, he had decided that on the next day his son should be formally "invested" with the top-knot. Our Little Korean Cousin |H. Lee M. Pike 

He went down the road, collected his little knot of listeners, and began the Song of the Girl. Kipling Stories and Poems Every Child Should Know, Book II |Rudyard Kipling 

I asked sharply, and MacRae flung the same query over one shoulder as he fumbled at the tight-drawn latigo-knot. Raw Gold |Bertrand W. Sinclair