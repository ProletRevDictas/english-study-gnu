Instead, many of these hardy organisms rely on rocks and water to survive, scientists reported on December 21 in the journal Proceedings of the National Academy of Sciences. Glacier-dwelling bacteria thrive on chemical energy derived from rocks and water |Kate Baggaley |December 30, 2020 |Popular-Science 

He says that as a sign that maybe even these hardy invaders have their limits. Jumping ‘snake worms’ are invading U.S. forests |Megan Sever |November 9, 2020 |Science News For Students 

A salt solution keeps the nerves functioning as they would if the nerves were in a live fish, Hardy says. A fish’s fins may be as sensitive to touch as fingertips |Carolyn Wilke |November 3, 2020 |Science News 

Long-term monitoring of the restored seagrass beds reveals a remarkably hardy ecosystem that is trapping carbon and nitrogen that would otherwise contribute to global warming and pollution, the team reports October 7 in Science Advances. How planting 70 million eelgrass seeds led to an ecosystem’s rapid recovery |Joseph Polidoro |October 14, 2020 |Science News 

Perhaps that’s a hopeful sign that even these hardy worms have their limits, but in the meantime, the onslaught of worms continues its march — with help from the humans who spread them. Invasive jumping worms damage U.S. soil and threaten forests |Megan Sever |September 29, 2020 |Science News 

It satirizes and parodies the romanticised, pessimistic accounts of rural life by writers like Thomas Hardy and Mary Webb. These Female Contemporaries Weren’t Afraid of Virginia Woolf |Louisa Treger |November 20, 2014 |DAILY BEAST 

Two other victims besides Jones and Hardy have been identified. Indiana Serial Killer’s Confession Was Just the Start |Michael Daly |October 21, 2014 |DAILY BEAST 

The family then fell on hard times, and Hardy moved to live with relatives in the Chicago area. Indiana Serial Killer’s Confession Was Just the Start |Michael Daly |October 21, 2014 |DAILY BEAST 

Vann is said to have told cops that he had begun to fight with Hardy in the course of sex. Indiana Serial Killer’s Confession Was Just the Start |Michael Daly |October 21, 2014 |DAILY BEAST 

Hardy had started life in Illinois and then had moved to Colorado with her family. Indiana Serial Killer’s Confession Was Just the Start |Michael Daly |October 21, 2014 |DAILY BEAST 

Hardy and Hicks flung the huge marrow bones with which they happened to be engaged at the time. Hunting the Lions |R.M. Ballantyne 

He knew it was a handkerchief, and smiled inwardly as he wondered what Tom Hardy would say if he could see him now. The Cromptons |Mary J. Holmes 

Even the hardy Mohammedan was haggard and spent, and his oblique eyes glowed like the red embers of a dying fire. The Red Year |Louis Tracy 

It was more than three years since Tom Hardy's letter had thrown him into a chill, and everything as yet was quiet. The Cromptons |Mary J. Holmes 

She, Anne herself, was as strong as a horse and had never been ill in her life, but others were not quite so hardy. The Joyous Adventures of Aristide Pujol |William J. Locke