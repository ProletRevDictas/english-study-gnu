Non-challenge replay reviews per game were actually down during the pre-hiatus regular season compared to the past two regular seasons, and that continued through the seeding games. Don’t Blame The Refs For All Of These Replay Reviews |Jared Dubin |September 17, 2020 |FiveThirtyEight 

They continue to hold events and run advertisements focused on this theme. Biden questions whether a vaccine approved by Trump would be safe |Sean Sullivan |September 16, 2020 |Washington Post 

More likely, the Stars will get punished by a team like Tampa Bay if they continue to concede at their current rate. Teams Don’t Win The Stanley Cup With A Goal Deficit. Can The Dallas Stars Change That? |Terrence Doyle |September 16, 2020 |FiveThirtyEight 

Puerto Rican voters have tended to favor Democrats, and Biden is wagering that he can continue that trend. Biden visits Florida as Democrats worry about his standing in the state |Sean Sullivan |September 15, 2020 |Washington Post 

The Greens' presidential nominee, Howie Hawkins, has continued to campaign despite the ballot challenges. The Trailer: The First State goes last |David Weigel |September 15, 2020 |Washington Post 

The debate over who really pulled off the Sony hack, then, could continue indefinitely. Was Sony Hit With a Second Hack? |Shane Harris |January 8, 2015 |DAILY BEAST 

Although the blood-spattered offices will be off-limits, staff have vowed to continue producing the magazine. France Mourns—and Hunts |Nico Hines, Christopher Dickey |January 8, 2015 |DAILY BEAST 

They tried to continue their getaway but had to quickly abandon their vehicle on the Rue de Meaux in the 19th. Police Hunt for Paris Massacre Suspects |Tracy McNicoll, Christopher Dickey |January 7, 2015 |DAILY BEAST 

“Our members continue to face a number of challenges,” she said. The Republican War on Kale |Patricia Murphy |January 7, 2015 |DAILY BEAST 

The people who are involved in the violence, they figure out ways to remain here at all costs and continue causing trouble. Mexico’s Priests Are Marked for Murder |Jason McGahan |January 7, 2015 |DAILY BEAST 

Felipe was so full of impatience to continue his search, that he hardly listened to the Father's words. Ramona |Helen Hunt Jackson 

If he continue, he shall leave a name above a thousand: and if he rest, it shall be to his advantage. The Bible, Douay-Rheims Version |Various 

It was never the intention of the Federal Reserve Act that member banks should continue the maintenance of these reserve accounts. Readings in Money and Banking |Chester Arthur Phillips 

Take care of a good name: for this shall continue with thee, more than a thousand treasures precious and great. The Bible, Douay-Rheims Version |Various 

A good life hath its number of days: but a good name shall continue for ever. The Bible, Douay-Rheims Version |Various