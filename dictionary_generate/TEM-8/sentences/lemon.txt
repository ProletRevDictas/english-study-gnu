One of the easiest and most delicious ways to cook your trout is by seasoning it inside and out with olive oil, salt, and lemon pepper. How to Clean, Cook, and Eat Trout in the Backcountry |Ryan Wichelns |October 15, 2020 |Outside Online 

Some produce requires added acid in the form of lemon juice or vinegar to make this process safe. How to can your favorite foods without dying |Rachel Feltman |October 13, 2020 |Popular-Science 

This bundle comes with 2 cylinders that each make approximately 60 liters or 250 8oz glasses of seltzer, plus two BPA-free 1-liter bottles, and lemon and orange calorie-free drops to add flavor. These pieces of kitchen gear make excellent gifts |PopSci Commerce Team |October 6, 2020 |Popular-Science 

The citric acid naturally present in lemon juice, another classic fondue ingredient, accomplishes a similar feat. The scientific way to make perfectly creamy fondue |By Pat Polowsky/Saveur |October 2, 2020 |Popular-Science 

Rogers describes Apeel’s approach as essentially learning from lemons and oranges and their robust peels, and teaching those lessons to the likes of cucumbers. Exclusive: Startup Apeel is launching ‘plastic-free’ cucumbers at Walmart to cut back on waste |Beth Kowitt |September 21, 2020 |Fortune 

We brought in Don Lemon, the year that he wrote his book, and I told that story to the audience that was there. How A Company’s Support of Gay Employees Helped One of Them To Come Out | |December 24, 2014 |DAILY BEAST 

Twist a large piece of orange zest and a large piece of lemon zest over the drink and drop into the glass. The Rise and Fall…and Rise Again of the Old-Fashioned |Allison McNearney |June 14, 2014 |DAILY BEAST 

We were at the Bitter Lemon now, Furry, Versie, Charley and I, waiting for the crowd to arrive. Stanley Booth on the Life and Hard Times of Blues Genius Furry Lewis |Stanley Booth |June 7, 2014 |DAILY BEAST 

“They all throwed me down,” Furry said, then laughed and told Versie he was going out to play at the Bitter Lemon. Stanley Booth on the Life and Hard Times of Blues Genius Furry Lewis |Stanley Booth |June 7, 2014 |DAILY BEAST 

Think all-tequila margaritas, carne asada tacos spritzed with lemon, key lime pies that are mounds of crust and nothing more. Limepocalypse! Inside the Great Lime Shortage of 2014 |Kara Cutruzzula |April 30, 2014 |DAILY BEAST 

Some one had gathered orange and lemon branches, and with these fashioned graceful festoons between. The Awakening and Selected Short Stories |Kate Chopin 

The acid is extracted from the juice of the citron, the lime, and the lemon, fruits grown in Sicily and the West Indies. Showell's Dictionary of Birmingham |Thomas T. Harman and Walter Showell 

Never afterward would she write a word upon lemon-colored paper. Tessa Wadsworth's Discipline |Jennie M. Drinkwater 

She selected a sheet of lemon-colored note paper, and wrote a message, hurriedly, in pencil. Tessa Wadsworth's Discipline |Jennie M. Drinkwater 

When he turns the brush in the cup it makes something like whipped cream, or the top of mother's lemon pies. Seven O'Clock Stories |Robert Gordon Anderson