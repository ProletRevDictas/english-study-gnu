What’s different this time is an uncompromising focus on elections themselves. Heeding Steve Bannon’s Call, Election Deniers Organize to Seize Control of the GOP — and Reshape America’s Elections |by Isaac Arnsdorf, Doug Bock Clark, Alexandra Berzon and Anjeanette Damon |September 2, 2021 |ProPublica 

Anjana Vasan carries the lighter side of the show, while Sarah Kameela Impey, playing the uncompromising frontwoman, ably shoulders its weightier story lines. The 10 best TV shows from the first half of 2021 |Inkoo Kang |July 1, 2021 |Washington Post 

The author is uncompromising in her descriptions of the micro-aggressions Nella experiences at the office. Three New Books Find Drama in the Scandals and Controversies of the Publishing World |Annabel Gutterman |May 12, 2021 |Time 

“Southwest anticipates minimal disruption to our operation, and we appreciate the understanding of our Customers and Employees as Safety is always the uncompromising priority at Southwest Airlines,” the airline said in a statement. Airlines ground some 737 Max jets after Boeing discloses electrical problem |Ian Duncan |April 9, 2021 |Washington Post 

Bypass the formations most frequently seen on postcards and check out the Secret 7, a handful of lesser-known yet uncompromising trails that offer you all the views with fewer crowds. Arizona's Best Trails for Every Kind of Hiker |Outside Editors |April 1, 2021 |Outside Online 

Francis fired the uncompromising commander of his Swiss Guard. Is The Pope Unprotected Now That He’s Fired the Head of the Swiss Guards? |Barbie Latza Nadeau |December 5, 2014 |DAILY BEAST 

Once, that uncompromising stand would have kept him in office no matter how corporatist or out of touch he became. The Rhinohawks Come Roaring Back |James Poulos |September 7, 2014 |DAILY BEAST 

Rigg, of course, had her fair share of uncompromising critiques. Diana Rigg: GOT's Queen of Thorns Speaks |Nico Hines |August 22, 2014 |DAILY BEAST 

These bohemian joints were so uncompromising that they reminded Moss “you needed chutzpah to live in New York,” he says. The End of New York: How One Blog Tracks the Disappearance of a Vibrant City |Tim Teeman |August 6, 2014 |DAILY BEAST 

Both sides pack an impressive physical punch, and for much of the game the tackles and challenges were uncompromising. Brazil and Colombia Bring the Ugly Game |Tunku Varadarajan |July 4, 2014 |DAILY BEAST 

Then he suddenly went in for politics and announced himself an uncompromising Liberal. Ancestors |Gertrude Atherton 

Thine is the spirit of universal liberty and love—of uncompromising hostility to every form of injustice and wrong. Solomon and Solomonic Literature |Moncure Daniel Conway 

I know his reputation for uncompromising candor, and love him for his bold, frank ways. Prison Memoirs of an Anarchist |Alexander Berkman 

He defied all detractors in the most uncompromising way: they were liars and slanderers, and he only wished he knew their names. Ireland Under the Tudors, Vol. II (of 3) |Richard Bagwell 

Others urge the necessity of restraint and uncompromising obedience, on the part of children, to the commands of their parents. Charles Duran |The Author of The Waldos