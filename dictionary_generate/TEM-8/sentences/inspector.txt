Covid policies affecting USMS prisoners held by the Bureau of Prisons were not included in the report because the inspector general is examining those facilities separately. U.S. Marshals Service’s lax covid-19 oversight of some inmates reflects a larger problem |Joe Davidson |February 12, 2021 |Washington Post 

In the early 2000s, the department’s inspector general found several private lenders, including Navient’s former sister company Sallie Mae, overcharged the federal government by tens of millions of dollars. Education Dept. orders Navient to refund $22.3 million in decade-old student loan scandal |Danielle Douglas-Gabriel |February 2, 2021 |Washington Post 

Brown pointed out that inspectors fill out a field interview form during the inspection that is left at the facility, and that copies are signed by both the inspectors and facility employees. Air Quality Regulators in “Cancer Alley” Have Fallen Dangerously Behind |by Mark Schleifstein, The Times-Picayune and The Advocate |January 29, 2021 |ProPublica 

“Because there were no specific details outlining what the M&A costs supported, we cannot determine whether the expenditures supported the BARDA mission,” the inspector general concluded. Millions earmarked for public health emergencies were used to pay for unrelated projects, inspector general says |Dan Diamond, Lisa Rein |January 27, 2021 |Washington Post 

The team began working in the area and was surprised by an oncoming train, inspectors said. Metrorail system still has work to do on safety, board says |Justin George |January 27, 2021 |Washington Post 

Few of us are as clever as my Inspector Morse-loving friend. The Refuseniks Hiding From ‘Happy New Year’ |Lizzie Crocker |December 31, 2014 |DAILY BEAST 

The driver then got on the highway and started going "well above the speed limit," with the taxi inspector still in tow. The Ten Worst Uber Horror Stories |Olivia Nuzzi |November 19, 2014 |DAILY BEAST 

One case in particular became the focus of Stuart Bowen, Special Inspector General for Iraq Reconstruction. Speed Read: James Risen Indicts The War On Terror’s Costly Follies |William O’Connor |October 14, 2014 |DAILY BEAST 

Reports of scheduling fraud and wait time cover-ups kept coming after the 2012 inspector general report. Exclusive: Texas VA Run Like a ‘Crime Syndicate,’ Whistleblower Says |Jacob Siegel |May 28, 2014 |DAILY BEAST 

And the bipolar junkie will stop at nothing to be promoted to detective inspector in a bid to win back his wife and children. James McAvoy on ‘Filth,’ His Wild Bachelor Party, and BB Gun Fights with Jennifer Lawrence |Marlow Stern |May 21, 2014 |DAILY BEAST 

Proceeding to a room further along the corridor, Chief Inspector Kerry opened the door and looked in. Dope |Sax Rohmer 

"Wait," said Chief Inspector Kerry, and went swinging in, carrying his overall and having the malacca cane tucked under his arm. Dope |Sax Rohmer 

He looked up wearily and met the fierce gaze of the chief inspector with a glance almost apologetic. Dope |Sax Rohmer 

He turned his cool regard upon Chief Inspector Kerry, twirling the cord of his monocle about one finger. Dope |Sax Rohmer 

Mrs. Kerry, enveloped in a woollen dressing-gown, which obviously belonged to the Inspector, came into the room. Dope |Sax Rohmer