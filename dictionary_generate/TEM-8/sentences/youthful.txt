It’s a rare display of precocity even in a field that celebrates youthful genius. Undergraduate Math Student Pushes Frontier of Graph Theory |Kevin Hartnett |November 30, 2020 |Quanta Magazine 

Known for its exciting and youthful vibe, Royal Oak offers plenty to see and do, with many boutiques, salons, and retail shops, as well as several movie theaters, live theater options, a comedy club, and more. Detroit : What to Know if You’re Moving to the Motor City |LGBTQ-Editor |November 24, 2020 |No Straight News 

As the years passed, the two women traced over those youthful visions with the brushstrokes of real life. Working moms are not okay |Amy Joyce, Ellen McCarthy |October 30, 2020 |Washington Post 

Garrett Bradley’s documentary follows Fox Rich, who has spent 21 years doggedly petitioning for the release of her husband Rob, from prison, where he’s been sentenced to spend 60 years following a youthful crime in which they were both involved. 11 compelling documentaries to watch for this fall |Alissa Wilkinson |October 9, 2020 |Vox 

In a 2019 podcast interview, Hayungs said he “went to county jail” 17 times when he was young, the result of what he called “youthful indiscretions.” The COVID-19 Charmer: How a Self-Described Felon Convinced Elected Officials to Try to Help Him Profit From the Pandemic |by Vianna Davila, Jeremy Schwartz and Lexi Churchill |September 25, 2020 |ProPublica 

Julio Cardenas, 25 and an MC from the group RCA, was older but with a youthful smile that hid the harsher sides of Cuban life. Cuban Hip-Hop Was Born in Alamar |Daniel Levin |December 26, 2014 |DAILY BEAST 

John Paul was youthful in his sixties with a radiant charisma. How Pope Francis Became the World’s BFF |Jason Berry |December 21, 2014 |DAILY BEAST 

I agree with you, but the youthful energy in the libertarian movement foresees a tipping point. This Republican Loved Taxes & Modern Art |Scott Porch |November 19, 2014 |DAILY BEAST 

Imagine if you were doomed forever to live inside a youthful mistake. Here’s a Reform Even the Koch Brothers and George Soros Can Agree On |Tina Brown |November 10, 2014 |DAILY BEAST 

This was before Cassidy entered politics, and the Republican congressman now calls that donation a “youthful indiscretion.” Mary Landrieu-Bill Cassidy Louisiana Senate Race Heads to a Runoff |Tim Mak |November 5, 2014 |DAILY BEAST 

For this reason the story of Yung Pak's youthful days may be the more interesting to his Western cousins. Our Little Korean Cousin |H. Lee M. Pike 

Her youthful vanity had its way in a mind too speculative, intelligent, observant, merely to be shocked. Ancestors |Gertrude Atherton 

Much older than I he had long since passed through these youthful phases. Fifty Years of Railway Life in England, Scotland and Ireland |Joseph Tatlow 

The handsome person and gallant bearing of the youthful knight excited general sympathy and regret. King Robert the Bruce |A. F. Murison 

Her real bitterness taught me what a purely youthful symptom mine had been, and she was rather a clever girl, often entertaining. Ancestors |Gertrude Atherton