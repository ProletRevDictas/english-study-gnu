With covid-19 cases and positivity rates ticking up in the region and across the state, it is critical for all Marylanders who need a test to be diligent about getting a test. Frostburg State University cancels in-person classes, again, as virus surges through western Maryland |Lauren Lumpkin |November 11, 2020 |Washington Post 

This week, the coach seemed happy that Haskins would get to see Smith — a quarterback long regarded as one of the league’s most diligent workers — preparing for games. Alex Smith is the NFL’s best story, but Washington’s next few weeks are about Dwayne Haskins |Les Carpenter |November 11, 2020 |Washington Post 

The men had been training together under the diligent eye of Alberto Salazar, a former Nike athlete and American record holder in the 10,000 meters. Inside a secret running program at Nike and a win-at-all-costs corporate culture |Rachel King |October 6, 2020 |Fortune 

Nonetheless, even the most diligent amateur risks taking a beating if they trade for any length of time. ‘Going to Vegas:’ Newbie options traders face a reckoning as the tech stock rally fades |Jeff |September 14, 2020 |Fortune 

Despite their treatment of others’ cubs, female hyenas can be “very attentive and diligent mothers,” Strauss says. Female hyenas kill off cubs in their own clans |Carolyn Wilke |August 25, 2020 |Science News 

Why not let the whole thing quietly fade away for want of a diligent investigation? The Castration of Alan Turing, Britain’s Code-Breaking WWII Hero |Clive Irving |November 29, 2014 |DAILY BEAST 

But as open as they are, they are also businessmen who know their brand and are their best, most diligent marketers. How the Property Brothers Became Your Mom’s Favorite TV Stars |Kevin Fallon |November 25, 2014 |DAILY BEAST 

Ferris credits her books Louisiana Cookery (1954) and New Orleans Cuisine (1969) as exemplars of diligent reporting and research. The Tragic History of Southern Food |Jason Berry |November 12, 2014 |DAILY BEAST 

And yes, Pineda had honored diligent teachers and top students. Mexico’s First Lady of Murder Is on the Lam |Michael Daly |October 29, 2014 |DAILY BEAST 

“You just have to be diligent in figuring out what they really look like,” El said. Mining Instagram for Models |Justin Jones |August 15, 2014 |DAILY BEAST 

But he was studious, diligent, and anxious to avoid repremands and to fulfil the expectations of his parents. Bastien Lepage |Fr. Crastre 

He was sent from one place to another, in quest of his friend, and made diligent use of his long legs, but without success. The Garret and the Garden |R.M. Ballantyne 

The three plodded on, taking a diligent constitutional walk, exchanging very few words, and those chiefly between the girls. The Daisy Chain |Charlotte Yonge 

While we considered whether we shall negociate, I fear the French have been more diligent. Private Letters of Edward Gibbon (1753-1794) Volume 1 (of 2) |Edward Gibbon 

Half an hour later, after a diligent consultation of certain books, he slipped back and beckoned OLeary into the hall. The Woman Gives |Owen Johnson