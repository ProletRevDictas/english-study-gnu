Lyons and her colleagues argue, though, that most of the soot from these fires would have remained lower in the atmosphere and been removed by precipitation. An asteroid didn’t kill the dinosaurs by itself. Earth helped. |Kate Baggaley |September 30, 2020 |Popular-Science 

The impact vaporized rock, ignited wildfires, and created a cloud of soot and dust that darkened and cooled the entire planet. An asteroid didn’t kill the dinosaurs by itself. Earth helped. |Kate Baggaley |September 30, 2020 |Popular-Science 

There’s plenty of evidence that air pollution — a broad category that includes soot, smog, and other pollutants from sources such as traffic, industry and fires — can harm health. What we know and don’t know about wildfire smoke’s health risks |Maria Temming |September 18, 2020 |Science News 

Previously, she was a staff writer at the Los Angeles Times, where she wrote about everything from desert wind power battles to the sale of national forest lands and poor neighborhoods grappling with deadly soot. Oil Companies Are Profiting From Illegal Spills. And California Lets Them. |by Janet Wilson, The Desert Sun, and Lylla Younes, ProPublica |September 18, 2020 |ProPublica 

He thinks that developing fuels that release less soot — as well as more efficient ways to burn that fuel — seem like better tactics. How to curb the climate heating by contrails |Stephen Ornes |April 27, 2020 |Science News For Students 

Indeed, a common racial slur in Dutch is, precisely, roetmop, which means soot mop. Dutch Try to Save Santa’s Slave |Nadette De Visser |December 2, 2014 |DAILY BEAST 

There is also soot staining the tiles, suggesting the bodies were burned or there had been a small blast. Who Is Behind Gaza's Mass Execution? |Jesse Rosenfeld |August 1, 2014 |DAILY BEAST 

Air pollution gets worse during drought; in California the problem is soot, and in Texas it was ozone. America’s Axis of Drought |Kate Galbraith |March 4, 2014 |DAILY BEAST 

Some computer models, he said, indicate that about half of the global warming in the Arctic is driven by methane and soot. The End of the Arctic? Ocean Could be Ice Free by 2015 |Mark Hertsgaard |December 13, 2013 |DAILY BEAST 

Ladder 118 looks small on the Brooklyn Bridge; in the foreground both towers billow soot. With the Fireman of Brooklyn’s Company 224 as They Observe the Fallen |Maurice Emerson Decaul |September 12, 2013 |DAILY BEAST 

As it passed out of the chimney, the soot left those long streaks of black which we see now on the woodpecker's back. Stories the Iroquois Tell Their Children |Mabel Powers 

A rubber blanket was procured, and the soot from the chimney carefully swept into it. Famous Adventures And Prison Escapes of the Civil War |Various 

Then he crouched trembling in the fireplace, his pretty green hair all blackened with soot and covered with ashes. The Tin Woodman of Oz |L. Frank Baum 

Fearing to be met by some of the guests of the Duke of Aquitaine, the serf had smeared soot mixed with grease over his face. The Pilgrim's Shell or Fergan the Quarryman |Eugne Sue 

Upon it they wrote with pens made of split reeds and with a thick ink made of soot (lampblack) mixed with resinous gums. The Private Life of the Romans |Harold Whetstone Johnston