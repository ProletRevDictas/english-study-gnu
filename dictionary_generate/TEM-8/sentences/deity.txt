Dissections of her politics aside, Didion will forever be a certain type of person’s idea of a deity—the literary, the cool. 'My Wine Bills Have Gone Down.' How Joan Didion Is Weathering the Pandemic |Lucy Feldman |January 22, 2021 |Time 

Martin didn’t hear from any deities, but plenty of other trippers have. What happens when psychedelics make you see God |Sarah Scoles |November 9, 2020 |Popular-Science 

Maintaining enough of a belief in the mechanics of the universe to entrust your life to them — something we all do all the time — seems like solid footing, not on the same level as trusting in an unseen deity or other spirits. The ancient palindrome that explains Christopher Nolan’s Tenet |Alissa Wilkinson |September 4, 2020 |Vox 

These actions tended to follow the same pattern across participants, such as holding an oil lamp or incense stick and moving it slowly clockwise before the statue of a deity. Why do we miss the rituals put on hold by the COVID-19 pandemic? |Sujata Gupta |August 14, 2020 |Science News 

To that end, Raijin is the deity of thunder and lightning who unleashes his tempests with wielding of his hammer and beating of drums. 12 Major Japanese Gods and Goddesses You Should Know About |Dattatreya Mandal |May 6, 2020 |Realm of History 

Beyoncé has, for close to a decade now, been a deity in entertainment: untouchable, successful, divine. Bow Down, Bitches: How Beyoncé Turned an Elevator Brawl Into a Perfect Year |Kevin Fallon |December 31, 2014 |DAILY BEAST 

For his tireless assault on evolutionary biology and downsizing the deity to fit within science, I give Meyer second place. 2014: Revenge of the Creationists |Karl W. Giberson |December 27, 2014 |DAILY BEAST 

Logistics wins the day, and the Supreme Deity is, at this juncture, nowhere to be seen. Meet Moses the Swashbuckling Israelite |James Romm |December 14, 2014 |DAILY BEAST 

In the background, outside, is Papa Legba, who is a Loa, the word for a Voodoo deity. ‘Gods of Suburbia’: Dina Goldstein’s Arresting Photo Series on Religion vs. Consumerism |Dina Goldstein |November 8, 2014 |DAILY BEAST 

Meanwhile our movie theaters have become temples for superheroes—in the absence of God, Hollywood has offered us a new deity. Welcome to Snowpiercer’s Apocalypse |Teo Bugbee |June 29, 2014 |DAILY BEAST 

In short, he is the creature of this 'Joss'—this home-made deity—to which he bows down and worships. Tobacco; Its History, Varieties, Culture, Manufacture and Commerce |E. R. Billings. 

This is their great mistake, and arises from a misconception of the character and ways of Deity. The Condition, Elevation, Emigration, and Destiny of the Colored People of the United States |Martin R. Delany 

It was the case in Peru, where the Inca was the direct representative on earth of the solar deity. Man And His Ancestor |Charles Morris 

In fact, the highest of these claims, that of the existence of a deity, must lie forever beyond its reach. Man And His Ancestor |Charles Morris 

Origen, Ambrose, and Augustine unite in prohibiting the representation of the Deity by any material object. The Catacombs of Rome |William Henry Withrow