We’d drill down about a meter, scan the borehole for organic matter, and then drill down another meter, looking for life as we went. He Found ‘Islands of Fertility’ Beneath Antarctica’s Ice |Steve Nadis |July 20, 2020 |Quanta Magazine 

Normally, the top meter or so of the permafrost, called the “active layer,” actually thaws in summer. 4 ways to put the 100-degree Arctic heat record in context |Carolyn Gramling |July 1, 2020 |Science News 

They also used data from smart meters in homes and other measures of energy use. CO2 emissions have nosedived as COVID-19 keeps people home |Carolyn Gramling |May 27, 2020 |Science News For Students 

Billions of cubic meters of concrete are produced every year. This ‘living’ concrete slurps up a greenhouse gas |Carolyn Wilke |May 6, 2020 |Science News For Students 

On the scale of a billionth of a meter, it’s called a nanotube. Here’s how butterfly wings keep cool in the sun |Erin Garcia de Jesus |March 6, 2020 |Science News For Students 

The concentration of PM2.5, the smallest particulate matter, is at 153 micrograms per cubic meter. Beijing’s ‘Star Trek’ APEC Summit |Jake Adelstein |November 11, 2014 |DAILY BEAST 

You can only see from above about a meter below the surface. James Cameron Dives into the Ocean's Abyss |Andrew Romano |July 21, 2014 |DAILY BEAST 

The government of Colombia decided to loan the 28,000 square meter fixer-upper to a fraternity of hermetic Benedictine monks. Pablo Escobar’s Private Prison Is Now Run by Monks for Senior Citizens |Jeff Campagna |June 7, 2014 |DAILY BEAST 

On one recent night, with only a brief break between, she had two five-hour private sessions—the meter running by the minute. Meet LittleRedBunny, the Queen of Cam Girls |Richard Abowitz |March 2, 2014 |DAILY BEAST 

The calorie meter—accurate or not—helps too, because it feels like an accomplishment to see that number go up. Can I Lose Weight Playing Video Games? |Alec Kubas-Meyer |January 14, 2014 |DAILY BEAST 

Down the block, a taxi that had been parked with meter ticking across from Engel's apartment-hotel drew away slowly. Hooded Detective, Volume III No. 2, January, 1942 |Various 

Plants are set out one meter apart in each direction, as they spread considerably. Philippine Mats |Hugo H. Miller 

I think the change began with the failure of the supply of gas from the penny-in-the-slot meter. In Accordance with the Evidence |Oliver Onions 

Indeed, he states explicitly that most forms of poetry do use all of the media mentioned: rhythm, tune, and meter. Rhetoric and Poetry in the Renaissance |Donald Lemen Clark 

I have been glad ever since to live where there is nothing more to do than turn the gas off at the meter when one goes to bed. Thirty Years in Australia |Ada Cambridge