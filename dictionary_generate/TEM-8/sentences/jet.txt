The jet engines are above the wings, located at the back of each wing opposite a small vertical fin. The weird and wonderful Flying-V made a successful first flight |Jonathan M. Gitlin |September 11, 2020 |Ars Technica 

Last year, Popular Science took an in-depth look at Boom and other companies that want to bring supersonic flight back for commercial or business-jet passengers—an idea that comes with serious economic, technical, and environmental challenges. Air Force transport jets for VIPs could have a supersonic future |Rob Verger |September 10, 2020 |Popular-Science 

When the chinquapin trees burn, they sound like jet engines taking off. How We Saved Our Neighborhood from a Wildfire |Dain Zaffke |September 2, 2020 |Outside Online 

It uses air to propel the jets of water, which ends up using less liquid than some of its competitors—a benefit when the water reservoir is small. Water flossers that get between your teeth |PopSci Commerce Team |August 27, 2020 |Popular-Science 

The jet stream strengthens over the Pacific Ocean, setting up long-range patterns that are ultimately conducive to tornadoes east of the Rocky Mountains. Improved three-week weather forecasts could save lives from disaster |Alexandra Witze |August 27, 2020 |Science News 

Nor does the jet have the ability to capture high-definition video, utilize an infra-red pointer. Pentagon Misfires in Stealth Jet Scandal |Dave Majumdar |January 8, 2015 |DAILY BEAST 

The jet engine instantly brought two advances over propellers: it doubled the speed and it was far more reliable. Flight 8501 Poses Question: Are Modern Jets Too Automated to Fly? |Clive Irving |January 4, 2015 |DAILY BEAST 

But even when the jet will be able to shoot its gun, the F-35 barely carries enough ammunition to make the weapon useful. New U.S. Stealth Jet Can’t Fire Its Gun Until 2019 |Dave Majumdar |December 31, 2014 |DAILY BEAST 

Search teams find dozens of people and jet debris floating in the Java Sea, as the airline confirms the wreckage is from QZ8501. Wreckage, Bodies of AirAsia Crash Found |Lennox Samuels |December 30, 2014 |DAILY BEAST 

Searchers reported seeing a large shadow on the seabed, suggesting the crashed jet has been located. Wreckage, Bodies of AirAsia Crash Found |Lennox Samuels |December 30, 2014 |DAILY BEAST 

Again, the big howitzers led the infernal orchestra pitting the face of no man's land with jet black blotches. Gallipoli Diary, Volume I |Ian Hamilton 

Jet black ringlets—then in vogue—clustered round an exceedingly fair face, on which there dwelt the hue of robust health. The Floating Light of the Goodwin Sands |R.M. Ballantyne 

Violet embroidered two beautiful eyes in black and white, and a jet black nose-tip. The Box-Car Children |Gertrude Chandler Warner 

It is there very slow and graceful; the feet are thrown out in a single long step, which Turveydrop, I presume, would call a jet. The Ladies' Book of Etiquette, and Manual of Politeness |Florence Hartley 

The complexion was not jet black, like the typical negro, but of a dull brown hue, the hair being somewhat similar in color. Man And His Ancestor |Charles Morris