Doing so has often come at a high cost with regards to career prospects or getting blacklisted on websites like Canary Mission that keep detailed and misleading dossiers of supporters of the Palestinian cause. How Online Activism and the Racial Reckoning in the U.S. Have Helped Drive a Groundswell of Support for Palestinians |Sanya Mansoor |May 21, 2021 |Time 

She addresses many issues in “Know Before You Go,” a dossier shared on the members-only site. The latest hotel amenity doesn’t involve massages or cookies: It’s a free coronavirus test. |Andrea Sachs |March 12, 2021 |Washington Post 

The head of Voice of America’s parent agency hired a law firm at a rate of about $500 an hour and spent $2 million in taxpayer funds to compile personnel dossiers on managers he had targeted for removal, according to a complaint filed Tuesday. Voice of America overseer spent $2 million investigating employees, complaint alleges |Paul Farhi |January 19, 2021 |Washington Post 

The Jets delivered the dossier to the paper over the summer. A divisive Jets reporter, accused of bullying, loses his place on the beat |Ben Strauss |December 11, 2020 |Washington Post 

The Clinton campaign’s funding of Steele’s research only emerged months later, long after the dossier was published. Pence’s allegations about Hillary Clinton: A guide for the perplexed |Glenn Kessler |October 9, 2020 |Washington Post 

And yet, a dossier of allegations involving human rights could not help any cardinal at a moment like that. How Pope Francis Became the World’s BFF |Jason Berry |December 21, 2014 |DAILY BEAST 

Last year, the Home Office said the dossier had mysteriously disappeared from the archives. Victim: I Watched British MPs Rape and Murder Young Boys |Nico Hines |December 18, 2014 |DAILY BEAST 

Recently, when whistleblowers finally surfaced, the Home Office officials could find no trace of the dossier. The Castration of Alan Turing, Britain’s Code-Breaking WWII Hero |Clive Irving |November 29, 2014 |DAILY BEAST 

He copies and pastes the listing into an ever-expanding dossier of suspected loot. Egyptian Tomb-Robbing Market Explodes on eBay |Bel Trew |May 31, 2014 |DAILY BEAST 

He was released from the case in 2007 for “showing bias against the McCanns” according to the court dossier. The Hunt for Madeleine McCann’s Grave |Barbie Latza Nadeau |May 12, 2014 |DAILY BEAST 

"You have had access to my dossier—I feel sure you have, monsieur," Paul said, addressing Pierrepont. The Doctor of Pimlico |William Le Queux 

The dossier is not complete, but, such as it is, it furnishes a riddle in which the supernatural appears to play a part. A Thin Ghost and Others |M. R. (Montague Rhodes) James 

The former opinion was, no doubt, justified by the evidence which the lost dossier contained. Queens of the French Stage |H. Noel Williams 

Meanwhile, my secretary will give you a complete dossier on my planned Official Bulletin. With a Vengeance |J. B. Woodley 

He placed the dossier back in a drawer and, lighting a cigar, paced up and down the room puffing furiously. The Grell Mystery |Frank Froest