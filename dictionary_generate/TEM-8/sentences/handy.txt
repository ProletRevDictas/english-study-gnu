The latest information on travel requirements, coronavirus test results and someday perhaps, proof of vaccinations — all available in one handy place. Digital health passports promise to simplify travel, but come with a lack of standards |Lori Aratani |February 26, 2021 |Washington Post 

This handy combo tool often comes with an extended, easy-grip handle that allows you to clear the windshield of compact cars and oversize SUVS equally well. Best ice scraper: Hassle-free ways to get rid of snow and ice |PopSci Commerce Team |February 26, 2021 |Popular-Science 

Our handy guide will give you five points to consider when buying a case for your iPhone 12, so whether you’re focused on functionality, protection from the harshest conditions, or pure style, we’ll help you find the right case. Best iPhone cases for protection and style |PopSci Commerce Team |February 26, 2021 |Popular-Science 

They also come in handy if you aren’t quite ready to commit to a particular space or to putting in the time to select the perfect piece of furniture. Best home office desk: Standing desks, office tables, and more |PopSci Commerce Team |February 25, 2021 |Popular-Science 

If—or, as Padnos assures us, when—Islamist terror makes its spectacular return, Blindfold will be a handy reference. Islamist Terrorism Is Not Done With Us, Warns Former al Qaeda Hostage Theo Padnos |Karl Vick |February 25, 2021 |Time 

There was a handy distraction in the Che t-shirt the tourist was wearing while celebrating the death. The Life and Hard Times Of The Family A Cuban Defector Left Behind |Brin-Jonathan Butler |December 19, 2014 |DAILY BEAST 

Before Cuba was shunned and sanctioned, it was a handy place for the randy. Will Hyman Roth Return to Havana With Normalized Relations? |John L. Smith |December 18, 2014 |DAILY BEAST 

You should look at our summary of benefits,” she continued, directing me to a handy online chart of “coverage examples. Despite ObamaCare, US Health System Still a Complete Mess |Molly Worthen |October 11, 2014 |DAILY BEAST 

Again, that pre-communion questionnaire sin litmus test might prove handy. The Great Divide Facing Pope Francis That Only Catholics Understand |Barbie Latza Nadeau |September 21, 2014 |DAILY BEAST 

She also was handy, hammering nails into walls and using a heat gun to chisel glue off the floor. The Wonderful Weirdness of Christine McConnell, Queen of Creepy Cookies |Tim Teeman |July 9, 2014 |DAILY BEAST 

She was so handy with a needle, and allus ready to cut out calico dingusses that the peon gals could sew up. Alec Lloyd, Cowpuncher |Eleanor Gates 

Half frantic, I dashed forward, snatching as I did so a rapier from the wall, the only weapon handy. Uncanny Tales |Various 

Anything thats handy, miss; dont put yourself out of the way on our accounts. Oliver Twist, Vol. II (of 3) |Charles Dickens 

He had got so that he felt a little lonely when he didn't have Louis the Goon right handy. Hooded Detective, Volume III No. 2, January, 1942 |Various 

As a preliminary to his courting trip, Bill took a drink from a bottle that he kept handy in his corner. Mystery Ranch |Arthur Chapman