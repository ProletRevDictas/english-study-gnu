Depictions in media trivialising desirous or sexually active older women, or women who seek sex outside of loving and steady relationships as abnormal, contribute to negative stereotypes and to judgemental attitudes about older sexuality. How To Please A Woman shifts the way we depict the sexuality of older women |LGBTQ-Editor |May 25, 2022 |No Straight News 

As consumers become more desirous of sustainability, responsible shopping, green travel and plant-based food alternatives “will likely contribute to a surge in companies in this space,” said Sofia Dolfe of Index Ventures. 8 investors discuss Stockholm’s maturing startup ecosystem |Mike Butcher |February 19, 2021 |TechCrunch 

That’s disturbing in various ways because it means that you are almost desirous of infection within those groups. Herd immunity alone won’t stop COVID-19. Here’s why. |Kate Baggaley |September 2, 2020 |Popular-Science 

She is equally desirous of Levine, as animalistic and eager to consume him while sticky with sanguine fluid. Sex, Blood and Maroon 5: Pop Culture’s Wounds Run Deep |Lizzie Crocker |October 3, 2014 |DAILY BEAST 

Sam is neither desirous of the attention nor does he want his story to be different from his fellow draftees. Which Team Will Make History With Michael Sam Tonight? |Robert Silverman |May 8, 2014 |DAILY BEAST 

More painful is that a wide swath of the haredi population is ready and desirous for a more complete life. Voting For Yair Lapid, Israel’s Maimonides |Rabbi Daniel Landes |February 4, 2013 |DAILY BEAST 

Her prior experience as a Wall Street whistle-blower had not left her desirous of more tumultuous press attention. Noreen Harrington: The Mets’ Madoff Whistle-Blower |Michael Daly |March 19, 2012 |DAILY BEAST 

This makes you, at turns, desirous of luxury and monkish asceticism, as you strike a balance between the two. The Stars Predict Your Week |Starsky + Cox |October 9, 2011 |DAILY BEAST 

It is ill-bred, and looks as if you were unaccustomed to such pleasures, and so desirous to prolong each one. The Ladies' Book of Etiquette, and Manual of Politeness |Florence Hartley 

None of them are desirous of continuing after death the part which they have, perhaps, so frequently commenced in this life. A Woman's Journey Round the World |Ida Pfeiffer 

Flora had been long desirous to effect a regular call at Abbotstoke, and it was just now that she succeeded. The Daisy Chain |Charlotte Yonge 

Every wise and honest man is desirous to soothe the angry passions of our neighbours. The History of England from the Accession of James II. |Thomas Babington Macaulay 

They mean to visit Bath again this spring, and I am very desirous that you should be better acquainted with her. Private Letters of Edward Gibbon (1753-1794) Volume 1 (of 2) |Edward Gibbon