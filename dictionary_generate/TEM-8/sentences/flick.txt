She plans to hold movie nights, where they’ll gather in the living room to catch a flick. ‘We are struggling’: Unemployed Americans face a bleak Christmas |Lee Clifford |December 24, 2020 |Fortune 

His latest film, Bad Hair, is a horror flick about a bloodsucking weave that has political relevance you may not expect. 25 Rising Stars to Track in 2021 |Daniel Malloy |December 20, 2020 |Ozy 

It allowed content creators to shoot their footage with two distinct looks, which viewers could seamlessly switch with the flick of a wrist. The 100 greatest innovations of 2020 |Popular Science Staff |December 2, 2020 |Popular-Science 

If you’re looking to eventually control your entire house with a flick of the wrist, you’ll be looking for completely different functionality. Best smartwatch: Five things to consider |Eric Alt |November 28, 2020 |Popular-Science 

The ongoing earthly presence of the enormous shark persists in our collective imagination thanks to rumors, legends, and summer B flicks. Could an ancient megashark still lurk in the deep seas? |By Riley Black |October 15, 2020 |Popular-Science 

Craig is signed on for just one more Bond flick after Spectre. Exclusive: Sony Emails Reveal Studio Head Wants Idris Elba For the Next James Bond |William Boot |December 19, 2014 |DAILY BEAST 

And who better to do that with than the actress who is playing the object of said (alleged) lesbian affection in the flick? Inside the Lifetime Whitney Houston Movie’s Lesbian Lover Storyline |Kevin Fallon |December 16, 2014 |DAILY BEAST 

But the little-known stories behind the making of the film are almost as fascinating as the flick itself. The Secrets of ‘Pulp Fiction’: 20 Things You Didn’t Know About the Movie on Its 20th Anniversary |Marlow Stern |October 19, 2014 |DAILY BEAST 

You and Nic Cage are basically having a contest in that flick of who can go further. John Travolta Doesn’t Regret a Thing |Marlow Stern |September 12, 2014 |DAILY BEAST 

Franco optioned the tome back in March 2011, and will star and direct the flick. James Franco Shot His New Movie at the Venice Film Festival and I Was in It |Marlow Stern |September 5, 2014 |DAILY BEAST 

Tell you what I did over in Chattanooga—in red-hot midsummer, too, said Flick, in a burst of confidence. The Woman Gives |Owen Johnson 

Im too sober, said Flick, with a discouraged shake of his head, as though to convey the idea that the day had been too short. The Woman Gives |Owen Johnson 

Im beginning to feel like the Fourth of July, said Flick, who gave in completely with this last display of magnificence. The Woman Gives |Owen Johnson 

No peace on earth, no good-will to men, said Flick, seeing the idea and almost moved to tears. The Woman Gives |Owen Johnson 

Flick Wilder now began to return, talking violently and flopping about in the last stages of a nightmare. The Woman Gives |Owen Johnson