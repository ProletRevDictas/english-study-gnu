If presidents are often thought of as figureheads for a political party, video game presidents are stand-ins for many different functions. The best presidents in video games |Gene Park |January 20, 2021 |Washington Post 

Fox News host Tucker Carlson, whose fears of change include a dystopian Starbucks-drinking future, is the chief figurehead of the Say Anything style. Fork in the Road: Where Will the GOP Turn? |Nick Fouriezos |January 10, 2021 |Ozy 

He quickly realized he was more of a figurehead, he said, than someone who had significant control over the chain. Profit and pain: How California’s largest nursing home chain amassed millions as scrutiny mounted |Debbie Cenziper, Joel Jacobs, Alice Crites, Will Englund |December 31, 2020 |Washington Post 

Not only is the highly esteemed Berners-Lee the figurehead for Inrupt, but the company also has cybersecurity guru Bruce Schneier as its chief of security architecture. Web inventor Tim Berners-Lee’s Solid data-privacy project enters the real world |David Meyer |November 9, 2020 |Fortune 

It’s not even so much policy-based, I just think politics has become so much of a machine — and I like having the feeling that one person can ultimately make the decision, that he is not just the figurehead and the machine is running everything. Undecided, With Just a Week to Go |Nick Fouriezos |October 27, 2020 |Ozy 

How will we know if Kim, when he eventually surfaces, is ruler or figurehead? Kim Jong Un: Erased? |Gordon G. Chang |October 10, 2014 |DAILY BEAST 

Waters herself is a gay figurehead, but again she shrinks from any compliments. Sarah Waters: Queen of the Tortured Lesbian Romance |Tim Teeman |September 30, 2014 |DAILY BEAST 

Less than 12 hours later, they lost their inspirational figurehead. Aaahm Ooot! SNP Leader Salmond Quits After Failed Vote |Nico Hines |September 19, 2014 |DAILY BEAST 

He may be a figurehead, but the president who is elected today is worth keeping an eye on for signs of where Pakistan is going. Mamnoon Hussain: The New Man in Pakistan |Husain Haqqani |July 30, 2013 |DAILY BEAST 

Indeed, Medvedev was never really more than a figurehead president, despite the theoretically huge powers attached to the job. Return of President Putin |Owen Matthews |September 24, 2011 |DAILY BEAST 

The statues here, and the lions before the Prince of Orange's palace, would disgrace almost the figurehead of a ship. Little Travels and Roadside Sketches |William Makepeace Thackeray 

Without power or privileges, she was a mere figurehead—a good mother looking after her family. Women of Modern France |Hugo P. Thieme 

The Speaking Oak then bade him cut off one of its great limbs, and carve from it a figurehead for his ship. Stories of Old Greece and Rome |Emilie Kip Baker 

Would people really prefer a figurehead and a symbol of undisputed authority? With a Vengeance |J. B. Woodley 

Walking steadily, but with a face set as the figurehead on one of his own ships, the captain went to answer the knock. Cy Whittaker's Place |Joseph C. Lincoln