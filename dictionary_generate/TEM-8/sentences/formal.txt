Broadly, the war is between the formal linguists and the sociolinguists. Talking Is Throwing Fictional Worlds at One Another - Issue 89: The Dark Side |Kevin Berger |September 9, 2020 |Nautilus 

Taking place after the 400-person formal dinner, it’s geared specifically toward younger people and costs less than one-third of the spring benefit’s entry-level ticket. How nonprofits are catering to millennials and rethinking the charity gala for younger generations |Rachel King |September 7, 2020 |Fortune 

No surprise, there’s been a trend away from formal event–oriented wear, but other than that there hasn’t been much change. The CEO striving to make vintage, secondhand clothing as popular as fast fashion |Rachel King |September 6, 2020 |Fortune 

Those interactions are less formal than what you get with video-conferencing services. Teemyco creates virtual offices so you can grab a room and talk with colleagues |Romain Dillet |September 4, 2020 |TechCrunch 

It’s not as formal as perhaps a medical leave of absence where a third party is approving a leave request. How managers can recognize burnout remotely |Kristine Gill |August 28, 2020 |Fortune 

As a result, training squadrons—called Formal Training Units (FTU)—are being staffed with less than half the people they need. Exclusive: U.S. Drone Fleet at ‘Breaking Point,’ Air Force Says |Dave Majumdar |January 5, 2015 |DAILY BEAST 

He hoped also to be a chaplain through his local church, and he was nearing the end of his formal training. In The Shadow of Murdered Cops |Michael Daly |December 26, 2014 |DAILY BEAST 

“The psychology of BDSM is lacking in other formal training regiments and interactions,” added Stella. Dungeons and Genital Clamps: Inside a Legendary BDSM Chateau |Ian Frisch |December 20, 2014 |DAILY BEAST 

I remember that he's a food-and-wine maven and rather formal. Alfred Hitchcock’s Fade to Black: The Great Director’s Final Days |David Freeman |December 13, 2014 |DAILY BEAST 

“To my knowledge, there was no formal consultation done with the tribes on this policy,” says Eid. Tribes to U.S. Government: Take Your Weed and Shove It |Abby Haglage |December 13, 2014 |DAILY BEAST 

Twice a year the formal invitation was sent out by the old nobleman to his only son, and to his two nephews. The Pit Town Coronet, Volume I (of 3) |Charles James Wills 

Various matters mentioned by the governor receive perfunctory and formal answers. The Philippine Islands, 1493-1898, Volume XX, 1621-1624 |Various 

Tressan advanced to meet him, a smile of cordial welcome on his lips, and they bowed to each other in formal greeting. St. Martin's Summer |Rafael Sabatini 

She sat in a distant corner of the formal room discreetly lit by a shaded lamp. The Joyous Adventures of Aristide Pujol |William J. Locke 

There were two hard formal-looking couches, with straight backs and spider legs. The Pit Town Coronet, Volume I (of 3) |Charles James Wills