Then to close out the season, Washington returns home for meetings with Rivera’s old team, the Panthers, and the Seattle Seahawks, before heading to Philadelphia with possibly a playoff berth on the line. Ron Rivera tries to keep Washington focused amid increasing coronavirus disruptions |Nicki Jhabvala |November 30, 2020 |Washington Post 

After everyone cooled down, the 6-3 teams, in contention for a wild-card berth, turned their attention to football. AFC South thrives and the Steelers stay unbeaten in NFL Week 11 |Cindy Boren, Mark Maske, Des Bieler |November 23, 2020 |Washington Post 

From here, Ohio State needs to sweep Illinois, Michigan State and Michigan — none of whom will escape this weekend with a winning record — and then defeat the Big Ten West champ to effectively sew up a playoff berth. College football winners and losers for Week 12: Northwestern on track for Big Ten title game |Patrick Stevens |November 22, 2020 |Washington Post 

Like at airports, berths are used by multiple ships, and a late ship can cause larger delays in the system. Whale ‘roadkill’ is on the rise off California. A new detection system could help. |Erik Olsen |September 29, 2020 |Popular-Science 

It ended, as Butler was leading Miami to a berth in the NBA Finals, with a first-round sweep and the dismissal of head coach Brett Brown. Can The Sixers Find A Way To Win It All With Embiid And Simmons? |James L. Jackson |September 28, 2020 |FiveThirtyEight 

Applying the Fourth Amendment to street stops, the Court has long preferred bright, clear rules that give wide berth to police. Can Government Call the Shots on Cellphone Privacy? |Aziz Huq |April 30, 2014 |DAILY BEAST 

Only then would a racer likely get a chance at a World Cup berth, from the very back of the start list. Skiing Prodigy Mikaela Shiffrin Looks Ahead to Sochi |Jake Bright |December 1, 2013 |DAILY BEAST 

Nobody wants to come anywhere near you, and the more expensive the other car, the wider berth it allows. How to Be a Racing Pirate King |David Frum |May 21, 2013 |DAILY BEAST 

Still, I retain hope that—given its midseason berth—NBC can tweak this enough to improve on an underwhelming first showing. TV Preview: Snap Judgments of 2012-13’s New Shows |Jace Lacob, Maria Elena Fernandez |June 12, 2012 |DAILY BEAST 

They arranged for a berth for Liebling on LCI(L)-88, one of the first large landing crafts scheduled to hit Omaha. The Story of the American Journalists Who Landed on D-Day |Timothy M. Gay |June 6, 2012 |DAILY BEAST 

He showed his wisdom in giving the Pandemonium card-room a very wide berth for the rest of his days. The Pit Town Coronet, Volume I (of 3) |Charles James Wills 

Let the young philosopher avoid such practice, and give a wide berth to those who follow them. Tobacco; Its History, Varieties, Culture, Manufacture and Commerce |E. R. Billings. 

This was not a bad idea, although the stranger shuddered as he thought of his ill-smelling stateroom and short berth. The Cromptons |Mary J. Holmes 

This seemed reasonable, and the people settled upon it, and gave him a wide berth as one who wished to be let alone. The Cromptons |Mary J. Holmes 

He kept his official berth, and continued to go into society, frequenting dances and theatres. The Life &amp; Letters of Peter Ilich Tchaikovsky |Modeste Tchaikovsky