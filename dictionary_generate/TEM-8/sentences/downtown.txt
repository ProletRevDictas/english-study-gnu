Washington subscribers include Yours Truly in the West End and Sofitel downtown. Now that’s room service: What it’s like to check into a hotel just for dinner |Tom Sietsema |February 12, 2021 |Washington Post 

It’s worth the extended detour into the city of Boise, Idaho, where skiing at Bogus Basin is less than an hour from downtown. 4 Awesome Winter Road Trips to National Parks |Megan Michelson |February 11, 2021 |Outside Online 

That means temperatures only fall modestly overnight, from the mid-20s in our colder spots to around 30 downtown. D.C.-area forecast: Cold today before another possible winter storm starting Wednesday night |Jason Samenow |February 8, 2021 |Washington Post 

Quite cold overnight, with temperatures in the teens outside the Beltway and low 20s downtown. PM Update: Icy spots develop overnight with frigid temperatures |Greg Porter |February 7, 2021 |Washington Post 

San Diego is known for its iconic scenes — we’ve got beaches, Balboa Park and the downtown skyline just to name a few. Morning Report: Growing the County’s Carbon-Cutting Efforts |Voice of San Diego |February 3, 2021 |Voice of San Diego 

Soon thereafter, Bentivolio was asked to play Santa in downtown Milford and became a professional. Kerry Bentivolio: The Congressman Who Believes in Santa Claus |Ben Jacobs |December 24, 2014 |DAILY BEAST 

He speaks while sipping a soda in the restaurant of the Residence Victoria in downtown Kisangani. The Congo's Forgotten Colonial Getaway |Nina Strochlic |December 18, 2014 |DAILY BEAST 

It was headquartered in Stanleyville, in a tall corner building that still stands in the decrepit, yet lively, downtown. The Congo's Forgotten Colonial Getaway |Nina Strochlic |December 18, 2014 |DAILY BEAST 

Will and Kate will start with a visit to the 9/11 Memorial downtown, and end with a monster fundraiser at the Met. When Your Royalty Met Ours: Kate Meets Bey Courtside |Tom Sykes |December 9, 2014 |DAILY BEAST 

Just downtown shone the Freedom Tower, which has risen where the World Trade Center came down in the 9/11 attacks. Eric Garner Was Just a Number to Them |Michael Daly |December 5, 2014 |DAILY BEAST 

The first time she had leave to go downtown she made an excuse to go into a book store and purchase a copy. The Courier of the Ozarks |Byron A. Dunn 

He rode downtown with Louis and they went over to that same East side hotel and Louis went upstairs. Hooded Detective, Volume III No. 2, January, 1942 |Various 

Bill Chadwick, another classmate of mine, came up from downtown, and met us at the church door. The Idyl of Twin Fires |Walter Prichard Eaton 

She was quite sure that he had not expected to spend the afternoon downtown, and she wondered what was troubling him. A Hoosier Chronicle |Meredith Nicholson 

The problem of providing social opportunity faced the downtown churches. The Leaven in a Great City |Lillian William Betts