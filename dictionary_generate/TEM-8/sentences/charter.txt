She left a charter school downtown and enrolled in Lincoln because she wanted to be part of the Lincoln community. When a Calculus Class Abruptly Became Ceramics at Lincoln High |Scott Lewis |September 16, 2020 |Voice of San Diego 

What looked at first like reasons to attend a charter school, to me felt like reasons why we are losing kids at my public school. Our Public Schools Have a Customer Service Problem |Thomas Courtney |September 10, 2020 |Voice of San Diego 

Generally speaking, within online schools – most of these are charter schools that are also publicly funded by the state – attendance is based on the amount of work students complete. The Learning Curve: San Diego Unified Is Terrified of Kids Opting Out |Will Huntsberry |September 10, 2020 |Voice of San Diego 

Trains had been chartered, and officials decided not to cancel. How a Swiss Ski Resort Was Ravaged by Typhoid and Survived |Daniel Malloy |September 9, 2020 |Ozy 

Meanwhile, some charter schools like e3 Civic High are moving quickly to incorporate curriculum reflecting underrepresented communities in existing history and English classes. As School Resumes, Students Bring Racial Justice Push to the Classroom |Kayla Jimenez |August 18, 2020 |Voice of San Diego 

A second document was titled: “Gambia Reborn: A Charter for Transition from Dictatorship to Democracy and Development.” The Shadowy U.S. Veteran Who Tried to Overthrow a Country |Jacob Siegel |January 6, 2015 |DAILY BEAST 

In neighborhoods such as Harlem, 33 percent of students attend charter schools, a majority of them black or Latino. How Public Sector Unions Divide the Democrats |Daniel DiSalvo |December 29, 2014 |DAILY BEAST 

JetBlue has been flying charter jets to Cuba for three years, and others are sure to follow. Up To Speed: The Cuba Embargo |Nina Strochlic |December 18, 2014 |DAILY BEAST 

Klein paints a rosy picture of the charter schools, while admitting that not all outperformed traditional public schools. Your Local School Doesn’t Have to Suck |Michael S. Roth |December 17, 2014 |DAILY BEAST 

Charter schools, rejecting the tenet of promotion through seniority, promised to do better. Your Local School Doesn’t Have to Suck |Michael S. Roth |December 17, 2014 |DAILY BEAST 

First permanent settlement began in 1669; original charter included North Carolina and Georgia. The Every Day Book of History and Chronology |Joel Munsell 

Benefit societies may be purely voluntary associations or incorporated either by statute or charter. Putnam's Handy Law Book for the Layman |Albert Sidney Bolles 

They thus establish a law for themselves somewhat like a charter of a corporation. Putnam's Handy Law Book for the Layman |Albert Sidney Bolles 

Gainful corporations have no such power unless it has been granted by their charter or by statute. Putnam's Handy Law Book for the Layman |Albert Sidney Bolles 

A company cannot purchase its own shares unless by charter or statute such action is clearly authorized. Putnam's Handy Law Book for the Layman |Albert Sidney Bolles