The hotel’s booked up, with only the $7,800 presidential suite available. What floor? |Sophia Nguyen |February 12, 2021 |Washington Post 

During the fourth quarter of 2020 when it seemed all eyes were on the presidential election, 40% of the top stories were not political. Why local broadcaster Tegna is making a big bet on its fact-checking vertical Verify |Sara Guaglione |February 12, 2021 |Digiday 

Lachlan Murdoch, the CEO of Fox Corporation, wants the world to know that Fox News won’t change, even as the network struggles through a ratings slump in the wake of the 2020 US presidential election. Fox News claims it won’t move further right, but the evidence says otherwise |Adam Epstein |February 9, 2021 |Quartz 

Of these 15 states, 114 also broke down the results of the 2016 presidential election by voting method. What Absentee Voting Looked Like In All 50 States |Nathaniel Rakich (nathaniel.rakich@fivethirtyeight.com) |February 9, 2021 |FiveThirtyEight 

The crew also looks at Republican proposals to change election laws in Georgia and other states in the wake of their 2020 presidential election loss. What To Expect From The Senate Impeachment Trial |Galen Druke |February 9, 2021 |FiveThirtyEight 

Like many Americans—but few Republican presidential candidates—the former Florida governor has evolved on the issue. Jeb Bush’s Unseen Anti-Gay Marriage Emails |Jackie Kucinich |January 9, 2015 |DAILY BEAST 

Bush busy engaging constituents on both sides of the same-sex marriage debate ahead of the 2004 presidential election. Jeb Bush’s Unseen Anti-Gay Marriage Emails |Jackie Kucinich |January 9, 2015 |DAILY BEAST 

Just 47 percent of Asian-Americans voted in the 2012 presidential election. Asian-Americans Are The New Florida |Tim Mak |January 8, 2015 |DAILY BEAST 

Should lightning strike and Hillary Clinton forgoes a presidential run, Democrats have a nominee in waiting. Sen. Warren’s Main Street Crusade to Pressure Clinton |Eleanor Clift |January 8, 2015 |DAILY BEAST 

There are four photos there of representative presidential candidates. Today’s GOP: Still Cool With Racist Pandering? |Michael Tomasky |January 7, 2015 |DAILY BEAST 

Here is a distinguished statesman with presidential possibilities; I shall proceed to fall in love with him.' The Awakening and Selected Short Stories |Kate Chopin 

Therefore, he came to the Presidency well prepared to assume presidential duties. Hallowed Heritage: The Life of Virginia |Dorothy M. Torpey 

Tyler approved the annexation of Texas to the Union near the end of his Presidential administration. Hallowed Heritage: The Life of Virginia |Dorothy M. Torpey 

Virginia made national headlines in the Presidential election of 1928. Hallowed Heritage: The Life of Virginia |Dorothy M. Torpey 

He was a prominent candidate for the Republican presidential nomination in 1876. Encyclopaedia Britannica, 11th Edition, Volume 4, Part 3 |Various