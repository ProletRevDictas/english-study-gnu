The Finns have a long tradition of universal conscription and keeping their reservist training very up-to-date. Finland and Sweden Joining NATO Is the Latest Example of Putin Miscalculating the Ukraine War |James Stavridis |May 18, 2022 |Time 

Dozens of military veterans are among the hundreds charged in the riots, and several others are reservists or National Guard members. Rep. Pat Fallon claimed SPLC listed 2 veterans organizations as ‘hate groups.’ His source? A satirical news blog. |Teo Armus |March 25, 2021 |Washington Post 

According to the friend, Brinsley rang his ex-girlfriend, an Air Force reservist named Shaneka Thompson, to no avail. Alleged Cop Killer’s Blood-Soaked Screenplay |M.L. Nestel |December 24, 2014 |DAILY BEAST 

The Marine reservist then went after his ex-wife, Nicole Hill Stone. Hunt for Iraq Vet After Killing Spree |M.L. Nestel |December 16, 2014 |DAILY BEAST 

Recruits acquire Kenyan Police Reservist status once they have completed the course, which allows them to carry arms. Borana Joins the Fight to Save Kenya’s Rhinos…and Wants You to Help Too |Joanna Eede |February 18, 2014 |DAILY BEAST 

Twelve people were killed at the Navy yard by ex–Navy reservist Aaron Alexis. Why Don’t We Have a Database on Gun Deaths? |Brandy Zadrozny |September 18, 2013 |DAILY BEAST 

Then–lieutenant colonel Rick Welch, now a full-bird colonel, was a reservist and district attorney from Morgan County, Ohio. What If the Iraq War Never Happened? |Michael Ware |March 20, 2013 |DAILY BEAST 

Sitting in the window, one could trace the Reservist's progress from his entrance at the gate to his disappearance into quarters. The Relief of Mafeking |Filson Young 

For the sake of the peace of Europe we had, up till then, deliberately refrained from calling up a single reservist. The Story of the Great War, Volume I (of 8) |Various 

A Reservist is a dug-out, a recruit a rookie, and a veteran an old sweat. Anecdotes of the Great War |Carleton Britton Case 

On the outbreak of war he was recalled to the Colours as a reservist, and took part in many famous engagements. The Irish on the Somme |Michael MacDonagh 

He was evidently a reservist, a feeble little man of about forty, with three days' growth on his chin. Adventures of a Despatch Rider |W. H. L. Watson