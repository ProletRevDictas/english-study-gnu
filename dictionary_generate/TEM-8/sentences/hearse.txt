The lead-off car for this week’s march will be a hearse, carrying what organizers say will be a symbolic representation of Manchin’s backbone. Joe Manchin Faces Protests from the Left at Home in West Virginia |Philip Elliott |August 25, 2021 |Time 

From those grounds, we watched as the hearse and motorcade made its way down the dusty driveway that August afternoon and the world bid farewell to truly an American original. Cindy McCain Opens Up — On Her Terms |Philip Elliott |April 27, 2021 |Time 

Police on motorcycles escorted the hearse to Arlington National Ceremony as hundreds of officers stood in lines. House to vote on removing GOP’s Marjorie Taylor Greene from her committees |Felicia Sonmez, John Wagner, Colby Itkowitz |February 4, 2021 |Washington Post 

The hearse then made its way to Annapolis, where Miller served in the General Assembly for 50 years. Maryland Senate President Miller lies in state: ‘Lion of the Senate’ |Ovetta Wiggins |January 22, 2021 |Washington Post 

Scores of officers lined up outside the Capitol and saluted as a hearse carrying Sicknick’s remains passed by. Capitol Police Chief Sund has stepped down; embattled agency reports another officer death |Allison Klein, Rebecca Tan |January 11, 2021 |Washington Post