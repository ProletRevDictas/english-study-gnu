I suppose that’s a testament to how good this movie is — so good that you can overlook Elgort’s aggressively flaccid performance. Why the new West Side Story works — and one thing that doesn’t |Constance Grady |December 10, 2021 |Vox 

Snoring usually occurs when the flaccid muscles on one’s throat block the airways, preventing the air from flowing through a person’s nose and throat as they breathe. Save nearly $60 on this sleep aid that guides your muscles to stop habitual snoring |Stack Commerce |November 17, 2021 |Popular-Science 

It was not long before several food substitutes appeared to overflow the flaccid trays and the same notes with little hearts and phrases were once again there. Locked up in the Land of Liberty: Part II |Michael K. Lavers |July 14, 2021 |Washington Blade 

She’d pin me to the bed and lick my mouth until, exhausted from defending themselves, my mouth muscles became flaccid and I spoke comically, thorta like thif, and the whole family laughed. Gene Weingarten: Confessions of a compassionate hit man |Gene Weingarten |February 25, 2021 |Washington Post 

In our less sexist age, Barack Obama has nevertheless found his Syria policy called “flaccid” and “impotent.” Obama and Syria: Fighting the Wimp Factor |Gil Troy |September 18, 2013 |DAILY BEAST 

Waiting for a taxi, he breathed in the spicy, flaccid atmosphere of the city and felt the strangeness of things around him. Olivia Manning, Married to the War |Lauren Elkin |June 7, 2013 |DAILY BEAST 

There was a vicious aching in his nerves, his muscles were flaccid and unstrung; a numbness was in his brain as well. The Wave |Algernon Blackwood 

The great hope after all lies in the knotless, rather flaccid character of the people. The Works of Robert Louis Stevenson - Swanston Edition Vol. 25 (of 25) |Robert Louis Stevenson 

His tail is not prehensile but flaccid, and half as long again as his head and body. Buffon's Natural History. Volume IX (of 10) |Georges Louis Leclerc de Buffon 

The two men went to the conservatory and gazed in upon a ruin of limp leaves and flaccid petals, killed by the powerful gases. Average Jones |Samuel Hopkins Adams 

Fie, fie upon the flaccid, castrated century, that has no other use than to chew over again the deeds of the past. The History of Modern Painting, Volume 1 (of 4) |Richard Muther