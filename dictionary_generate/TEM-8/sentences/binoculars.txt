They were found camped in the woods, armed with binoculars, flying a small helicopter. What Was This Drone Doing Over a South Carolina Prison? |Melissa Leon |August 1, 2014 |DAILY BEAST 

The search for the remains of Flight 370 is not simply a case of binoculars out of the window, although they play a part. Was MH370 Carrying Killer Cargo? |Clive Irving |March 21, 2014 |DAILY BEAST 

We were on the outer periphery of the operation, too distant to see the scene clearly without binoculars. The Devil’s Drug: The True Story of Meth in New Mexico |Nick Romeo |August 24, 2013 |DAILY BEAST 

Without a bribed official, a halcyon, or eagle, will watch the entry point with binoculars for patterns and opportunities. The Devil’s Drug: The True Story of Meth in New Mexico |Nick Romeo |August 24, 2013 |DAILY BEAST 

Detective Brian Sallee lifted a pair of binoculars and scanned a parking lot a quarter mile down the road. The Devil’s Drug: The True Story of Meth in New Mexico |Nick Romeo |August 24, 2013 |DAILY BEAST 

Somewhat awed by my calm fury, he hastened back to camp and returned with the binoculars. In Search of the Unknown |Robert W. Chambers 

At last he lowered the binoculars and handed them to Wade, who sat next to him. Islands of Space |John W Campbell 

Morey had been scanning the horizon with a pair of powerful binoculars. Islands of Space |John W Campbell 

At length he lowered the binoculars and turned toward his companions. Boy Scouts in the North Sea |G. Harvey Ralphson 

In front of each was a stand which held something like a set of binoculars and what looked like a pair of ear muffs. The Jewels of Aptor |Samuel R. Delany