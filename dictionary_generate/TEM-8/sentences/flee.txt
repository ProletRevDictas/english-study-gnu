Thousands of Rum families fled Turkey in the aftermath, and the Rum populace that remained — and the city itself — were never the same again. ‘A Recipe for Daphne’ is a delicious debut |Vanessa Larson |February 5, 2021 |Washington Post 

Others watched on TV and frantically tried to reach our bosses and colleagues as they fled for their lives. Ocasio-Cortez, other Democrats recount on House floor what they experienced during Capitol siege |Amy B Wang, Colby Itkowitz |February 5, 2021 |Washington Post 

On Saturday, Green opened fire at Baltimore police officers at a grocery store in northern Baltimore before fleeing, Harrison said. U.S. marshal shot, hospitalized in Baltimore; suspect fatally shot |Justin Wm. Moyer, Emily Davies |February 4, 2021 |Washington Post 

Others watched on TV and frantically tried to reach bosses and colleagues as they fled for their lives. House to vote on removing GOP’s Marjorie Taylor Greene from her committees |Felicia Sonmez, John Wagner, Colby Itkowitz |February 4, 2021 |Washington Post 

In reality, the frantic effort is clearly meant to compensate for voters fleeing the party. Shrinking the GOP, one state at a time |Jennifer Rubin |February 2, 2021 |Washington Post 

Many more illegal migrants face labor trafficking in Europe as they flee the conflict regions of North Africa and the Middle East. ISIS, Boko Haram, and the Growing Role of Human Trafficking in 21st Century Terrorism |Louise I. Shelley |December 26, 2014 |DAILY BEAST 

The risk to his life was great enough that he had to flee Munich when Hitler attempted to seize power in November 1923. The Catholic Philosopher Who Took on Hitler |John Henry Crosby |December 26, 2014 |DAILY BEAST 

As a cafe in Sydney, Australia came under siege by a hostage-taking gunman on Monday, those nearby attempted to flee the area. In Defense of Uber’s Awful Sydney Surge Pricing |Olivia Nuzzi |December 16, 2014 |DAILY BEAST 

But, in Jamaica, Maurice Tomlinson was forced to flee his country after his marriage to his Canadian husband made front-page news. A Quorum For Change: The Fight For Global LGBT Equality |Justin Jones |December 11, 2014 |DAILY BEAST 

Within two years, fighting was so bad, she was forced to flee. Death Metal Angola: Heavy Metal in War-Torn Africa |Nina Strochlic |November 21, 2014 |DAILY BEAST 

The remaining guerrilla had no stomach to continue the fight, and wheeled his horse to flee. The Courier of the Ozarks |Byron A. Dunn 

As they look him in his face and his shadowy wings cover them, nature recoils and would flee from him. The Courier of the Ozarks |Byron A. Dunn 

Where sabre, lance, and bayonet, right soon would turn and flee! Blackwood's Edinburgh Magazine, No. CCCXXXIX. January, 1844. Vol. LV. |Various 

And the shepherds shall have no way to flee, nor the leaders of the flock to save themselves. The Bible, Douay-Rheims Version |Various 

Finally, they became so frightened at this unusual type of fighting that they broke ranks and tried to flee. Hallowed Heritage: The Life of Virginia |Dorothy M. Torpey