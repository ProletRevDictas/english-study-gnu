In particular, it had very dense leg bones, a feature of some aquatic creatures like manatees that need the bones for ballast to stay submerged. Spinosaurus’ dense bones fuel debate over whether some dinosaurs could swim |Carolyn Gramling |March 23, 2022 |Science News 

Other options include dredging around the ship and offloading ballast water, fuel, or cargo. How That Massive Container Ship Stuck in the Suez Canal Is Already Costing the World Billions of Dollars |Joseph Hincks |March 25, 2021 |Time 

While not exactly the Ugly American, Sinatra provided plenty of his own homegrown ballast. Frank Sinatra and the Birth of the Jet Set |William Stadiem |August 2, 2014 |DAILY BEAST 

Especially the Southern ones, who by and large run the party, or at least provide its cultural ballast. What That RNC Aide’s Equal Pay Blunder on MSNBC Says About Her Party |Michael Tomasky |April 9, 2014 |DAILY BEAST 

“Gold lost its structural ballast when it lost its formal relationship to money,” he concludes. All that Glitters Is Not Gold: Inside the New Bubble |Wendy Smith |December 5, 2013 |DAILY BEAST 

Straighten up and fly right: the capsule is rolling and jettisoning its remaining ballast masses for parachute deploy. Curiosity’s Mars Landing Narrated Moment by Moment by Flight Director Keith Comeaux |Keith Comeaux |August 7, 2012 |DAILY BEAST 

Spin down, turn to entry attitude and jettison ballast mass in one minute. Curiosity’s Mars Landing Narrated Moment by Moment by Flight Director Keith Comeaux |Keith Comeaux |August 7, 2012 |DAILY BEAST 

Sand and gravel are also used for "fill," for engine sands, railroad ballast and glass. Hallowed Heritage: The Life of Virginia |Dorothy M. Torpey 

Old and new measurements, tonnage, time allowances and movable ballast, are all a sealed book to me. The Pit Town Coronet, Volume II (of 3) |Charles James Wills 

The big sloop, hard aground and full of iron ballast, was not a thing to be moved easily. The Rival Campers |Ruel Perley Smith 

Our new craft worked and sailed well, after a little addition of ballast. Famous Adventures And Prison Escapes of the Civil War |Various 

I ordered the ballast to be thrown overboard, and determined, as our only chance, to attempt to force her over the reef. Famous Adventures And Prison Escapes of the Civil War |Various