In the autumn, we might substitute in zucca, or sweet pumpkin. Global markets climb as investors focus on bank earnings and Yellen testimony |Bernhard Warner |January 19, 2021 |Fortune 

Last week, you and 60 of your closest friends decided to play a socially distanced game of hot pumpkin. Can You Make An Unfair Coin Fair? |Zach Wissner-Gross |November 6, 2020 |FiveThirtyEight 

The special is bookended by two sequences involving Linus and Lucy going to the pumpkin patch. Watching It’s the Great Pumpkin, Charlie Brown with a 5-year-old |Emily VanDerWerff |October 30, 2020 |Vox 

She then declares “2” and passes the pumpkin one space to her left. Beware The Hot Pumpkin |Zach Wissner-Gross |October 30, 2020 |FiveThirtyEight 

Instead, they’re planning to stay in and whip up homemade pumpkin ravioli. Welcome to ‘Thanksgiving-ish,’ with fondue nights, soup buffets and takeout turkey |Emily Heil |October 30, 2020 |Washington Post 

Finally, Deborah Racicot of Narcissa prepares mouth-watering (and non-basic) pumpkin crepes with warm sage cinnamon en glaze. Thanksgiving Favorites, With a Twist |Sara Sayed, The Daily Beast Video |November 26, 2014 |DAILY BEAST 

On Pumpkin Fest weekend, they did things a little differently. FinnaRage Wants You to Rage at Its Parties. So What if It Ends Up a Riot? |Melanie Plenda |October 27, 2014 |DAILY BEAST 

Riots broke out both after last year's pumpkin festival and after the Red Sox World Series win last year. Frat Culture Clashes With Riot Police at Keene, N.H., Pumpkin Festival |Melanie Plenda |October 19, 2014 |DAILY BEAST 

He joked, as he split open pumpkin seeds with his teeth between puffs on a smoke. I Heard About the Latest Crazed Shooter While I Watched the World Cup with Guys He Almost Killed |Daniel Genis |July 1, 2014 |DAILY BEAST 

The pumpkin, when he produced it, was the size of an orange—but still a pumpkin. Portrait of the Consummate Con Man |John Lardner |May 17, 2014 |DAILY BEAST 

It wasn't long before she surprised the object of her search in the act of eating a fat grub beside a pumpkin. The Tale of Grandfather Mole |Arthur Scott Bailey 

And he decided, after thinking deeply for some time, that there could not possibly be a bee inside the pumpkin. The Tale of Buster Bumblebee |Arthur Scott Bailey 

That there fellow was the sweetest cuttin' man I ever did cut in all my life—he was jest like a ripe pumpkin.' The Way of a Man |Emerson Hough 

“Why, but the good little pumpkin was eaten up, too,” said the boy. Christmas Every Day and Other Stories |W. D. Howells 

The question was, How can a pumpkin vine climb a fence, anyway? Christmas Every Day and Other Stories |W. D. Howells