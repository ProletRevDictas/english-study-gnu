His voice is quiet, melodic, and often tinged with an undercurrent of mirth. Colm Toibin Describes The Creation Of His Quiet Masterpiece ‘Nora Webster’ |Jennie Yabroff |November 3, 2014 |DAILY BEAST 

The mirth is misleading, as are the soft features of the baby-faced Surkov. Is This the Mastermind Behind Russia’s Crimea Grab? |Anna Nemtsova, Eli Lake |March 19, 2014 |DAILY BEAST 

That's better than Germany, France and even Brazil, where mirth is nearly a religion. Venezuela Unveils Orwellian Ministry of Supreme Social Happiness |Mac Margolis |October 31, 2013 |DAILY BEAST 

The unlikely saga of the spies known as The Illegals has caused much mirth, but no diplomatic crisis, with Russia. Russia Is Laughing at Us |Julia Ioffe |June 29, 2010 |DAILY BEAST 

The mirth came from imagining how he would twist the Tet Offensive, which was under way, into “progress.” McNamara's Lethal Illusions |Kevin Buckley |July 6, 2009 |DAILY BEAST 

Then—(with difficulty restraining another outburst of mirth)—how about "27 for oysters and Chablis" after the visit? Punch, or the London Charivari, Volume 107, November 3, 1894 |Various 

The mirth of timbrels hath ceased, the noise of them that rejoice is ended, the melody of the harp is silent. The Bible, Douay-Rheims Version |Various 

There shall be a crying for wine in the streets: all mirth is forsaken: the joy of the earth is gone away. The Bible, Douay-Rheims Version |Various 

Mrs. Chester promptly obeyed, surprised by the mingled mirth and vexation expressed by her husband's face. Dorothy at Skyrie |Evelyn Raymond 

The blood hummed through Garnache's head as he tightened his lips and watched this gentleman indulge his inexplicable mirth. St. Martin's Summer |Rafael Sabatini