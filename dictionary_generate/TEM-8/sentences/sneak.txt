Quarterback Jared Goff threw an interception but had a first-half rushing touchdown on a quarterback sneak and threw a second-half touchdown pass to wide receiver Cooper Kupp. Cam Newton’s struggles continue as Patriots’ postseason hopes plummet with loss to Rams |Mark Maske |December 11, 2020 |Washington Post 

The Rams converted twice on fourth and one, once on a quarterback sneak by Goff and again when the Patriots jumped offside. Cam Newton’s struggles continue as Patriots’ postseason hopes plummet with loss to Rams |Mark Maske |December 11, 2020 |Washington Post 

Scheduled to make her official debut on the small screen in early 2021, fans have already gotten a sneak peek into what we can expect when season two of TheCW series airs. The $10 Product Behind Batwoman’s Perfect Curls |cmurray |November 23, 2020 |Essence.com 

Now, Adobe has released a sneak peak of its upcoming Sky Replacement tool, which uses AI to analyze a scene and automatically swap out underwhelming areas of sky. Photoshop will soon use AI to add dramatic skies to your boring photos |Stan Horaczek |September 23, 2020 |Popular-Science 

Will Smith shared a sneak peek of The Fresh Prince of Bel-Air cast reunion Thursday that brought the fictional Banks family back to its California home after 30 years. ‘Full Fresh Prince Of Bel-Air’ Cast Reunites To Celebrate The Show’s 30th Anniversary |Hope Wright |September 11, 2020 |Essence.com 

Rick suggests a Woodbury-esque sneak attack on the hospital and lays out a meticulous strategy relying heavily on timing and luck. The Walking Dead’s ‘Crossed’: The Stage Is Now Set for a Bloody, Deadly Midseason Finale |Melissa Leon |November 24, 2014 |DAILY BEAST 

“It was a magical feeling, leaving daylight to sneak into a theater,” he says wistfully. Can Condon's Freak Show Win Broadway? |Tim Teeman |November 18, 2014 |DAILY BEAST 

While attempting to pull a bin of canned food toward him, Bob is momentarily pulled underwater by a walker sneak-attack. The Walking Dead’s Most Gruesome Scene Yet: ‘A Man’s Gotta Eat’ |Melissa Leon |October 20, 2014 |DAILY BEAST 

Check out a sneak peek of one of the most anticipated films of the year. Exclusive: Watch a Clip From ‘Birdman,’ Featuring an Award-Worthy Turn by Michael Keaton |Marlow Stern |October 1, 2014 |DAILY BEAST 

The easily concealable and muted weapon would allow him to sneak up on his victims and get away afterward to kill again. The Loser Who Wanted to Be the ISIS Agent Next Door |Michael Daly |September 18, 2014 |DAILY BEAST 

That young cove right opposite to you is one of the best-known sneak-thieves in the city. The Double Four |E. Phillips Oppenheim 

I was so angry when I saw another week sneak round and another bill appear, that I left it unopened on my bureau for a week. Ways of War and Peace |Delia Austrian 

So soon as this shelling stops I must sneak off to try and put our cemeteries straight. Letters of Lt.-Col. George Brenton Laurie |George Brenton Laurie 

He even ventured to reproach his friend: 'I shan't sneak of you, of course, he said, 'but you know you did it!' The Talking Horse |F. Anstey 

We would have to sneak in order to keep the younger children from begging to be taken along. The Red Cow and Her Friends |Peter McArthur