The first viable, non-toxic chemically derived sugar substitute was discovered in the late 18th century by a German chemist. Has a startup finally found one of food science’s holy grails with its healthy sugar substitute? |Jonathan Shieber |February 26, 2021 |TechCrunch 

It was 19th century German astronomer Samuel Heinrich Schwabe who realized that such solar activity ebbs and flows during 11-year cycles. Solar storms can wreak havoc. We need better space weather forecasts |Ramin Skibba |February 26, 2021 |Science News 

The German group then plans to react hydrogen with carbon dioxide captured from the cement plant to produce both methanol, a chemical feedstock, and synthetic jet fuel. Cheap renewables could make green hydrogen a practical replacement for fossil fuels |Katie McLean |February 24, 2021 |MIT Technology Review 

The German auto giant also agreed to set up a joint venture with the company to mass-produce the batteries and says they’ll be in its electric cars and trucks on the road by 2025. Novel lithium-metal batteries will drive the switch to electric cars |Katie McLean |February 24, 2021 |MIT Technology Review 

The German team had perhaps the toughest experimental recipe. Scientists Communicated With People While They Were Lucid Dreaming |Shelly Fan |February 22, 2021 |Singularity Hub 

An additional 12,000 took to the streets in other German towns. Europe’s Islam Haters Say We Told You So |Barbie Latza Nadeau |January 8, 2015 |DAILY BEAST 

But few of us would recognize the name of Dietrich von Hildebrand, a German philosopher-turned-outspoken Nazi antagonist. The Catholic Philosopher Who Took on Hitler |John Henry Crosby |December 26, 2014 |DAILY BEAST 

In the video, the bus is getting searched by a cop with a German shepherd. Alleged Cop Killer Ismaaiyl Brinsley Had a Death Wish |M.L. Nestel |December 22, 2014 |DAILY BEAST 

Many more German divisions would enter the fray over the next few days. Hitler’s Hail Mary |James A. Warren |December 20, 2014 |DAILY BEAST 

In the event, the enemy did plenty—far more than SHAEF, or for that matter the German high command, imagined possible. Hitler’s Hail Mary |James A. Warren |December 20, 2014 |DAILY BEAST 

He was a bookseller, but better known as a translator of the German contributor to the Gentleman's Magazine, &c. The Every Day Book of History and Chronology |Joel Munsell 

It is, however, true, that in this respect the German hexametrist has a considerable advantage over the English. Blackwood's Edinburgh Magazine, Volume 60, No. 372, October 1846 |Various 

To talk German was beyond the sweep of my dizziest ambition, but an Italian runner or porter instantly presented himself. Glances at Europe |Horace Greeley 

Gottfried Achenwall, an eminent German lecturer on statistics, history and the laws of nature, died at Gttingen. The Every Day Book of History and Chronology |Joel Munsell 

The governess is Swiss and for one week she talks nothing but French and for another nothing but German. The Salvaging Of Civilisation |H. G. (Herbert George) Wells