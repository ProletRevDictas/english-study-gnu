Herewith for your enjoyment and frustration is my list, by category and in no particular order. My Favorite Books of 2013: Jill Lepore, Adelle Waldman, James Salter & More |Lucas Wittmann |December 15, 2013 |DAILY BEAST 

Herewith the 100 funniest televised moments in the history of the British House of Commons. 100 Funniest Moments from the British House of Commons |David Frum |October 2, 2012 |DAILY BEAST 

I ask you herewith, please help us, please look our case very serious. Mother of Los Angeles’ Alleged Arsonist Had a Wild Life |Christine Pelisek |January 14, 2012 |DAILY BEAST 

Herewith, a 12 Days of Christmas reading list you might actually enjoy. Twelve Unusual Christmas Reads |Stefan Beck |December 25, 2011 |DAILY BEAST 

Herewith, as a public service, the new Herman Cain Board Game. The Cain Sexual-Harassment Game |Michael Tomasky |November 2, 2011 |DAILY BEAST 

You will also receive herewith a small belt for the Panis and a large one for the Tetaus or Camanches. The Expeditions of Zebulon Montgomery Pike, Volume II (of 3) |Elliott Coues 

The little memorandum illustrated herewith is very handy to carry in the coat or vest pocket for taking notes, etc. The Boy Mechanic, Book 2 |Various 

Among the monkish loot at St. Albans was an ancient cameo herewith reproduced. Archaic England |Harold Bayley 

Said Hallblithe raising the cup: “Herewith I wish thee youth!” The Story of the Glittering Plain |William Morris 

Herewith is given a metrical translation of an ancient Pawnee ritualistic hymn. Prairie Smoke (Second Edition, Revised) |Melvin Randolph Gilmore