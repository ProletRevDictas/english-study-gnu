Erosion may have partly worn away what was originally a circular earthwork, the researchers speculate. Drones find signs of a Native American ‘Great Settlement’ beneath a Kansas pasture |Bruce Bower |September 10, 2020 |Science News 

However, some of those people also speculate Apple may have plans to build a bigger ads business of its own. ‘It’s the first time they’re listening’: Apple is striking a more conciliatory tone with the ad industry |Lara O'Reilly |September 8, 2020 |Digiday 

In the face of sustained hardship, which has the potential to encourage dangerously high levels of cortisol, a weak stress response — that is, producing less cortisol — could be “nature’s way of preserving the brain and body,” Gunnar speculates. Puberty can repair the brain’s stress responses after hardship early in life |Esther Landhuis |August 28, 2020 |Science News 

Scientists have to speculate, which could go one of two or three ways, or maybe some way they haven’t seen yet. How Pseudoscientists Get Away With It - Facts So Romantic |Stuart Firestein |August 28, 2020 |Nautilus 

So a weak stress response could be “nature’s way of preserving the brain and body,” Gunnar now speculates. Puberty may reboot the brain and behaviors |Esther Landhuis |August 27, 2020 |Science News For Students 

“Hence, there might be a net benefit, at least to some females, of breeding within the natal group,” the researchers speculate. Mongooses, Meerkats, and Ants, Oh My! Why Some Animals Keep Mating All in the Family |Helen Thompson |December 29, 2014 |DAILY BEAST 

We can only speculate as to the intentions behind these ambiguous words. Is Pope Francis Backpedaling on Gays? |Jay Michaelson |November 19, 2014 |DAILY BEAST 

Forgács did not care to speculate, repeatedly and vehemently claiming that he is not a political artist. In Hands of Hungarian Artist, Jewish Home Movies of the ’30s a Warning of Coming Holocaust |Daniel Genis |October 25, 2014 |DAILY BEAST 

I hesitate to speculate on exactly where the problem is, though after spending some time with the paper I have my suspicions. Black Holes Exist. So Does Bad Science |Matthew R. Francis |September 28, 2014 |DAILY BEAST 

Some doctors speculate they are generated in the spinal cord. Real Life Lazarus: When Patients Rise From the Dead |Sandeep Jauhar |August 21, 2014 |DAILY BEAST 

There were more subtle changes in him which it was too warm and dusty to speculate upon at the moment. Ancestors |Gertrude Atherton 

Perhaps it was in the bottle of brandy that the peril lay; perhaps—but why speculate further! The Staircase At The Hearts Delight |Anna Katharine Green (Mrs. Charles Rohlfs) 

It was not a folly, in a rude age, to speculate on the first or fundamental principle of things. Beacon Lights of History, Volume I |John Lord 

But there is no need to speculate on what might be, when we have positive and certain knowledge of what has been. A System of Logic: Ratiocinative and Inductive |John Stuart Mill 

But he snarled up at me like a startled animal, and I was obliged to go to bed and toss about and speculate. In Search of the Unknown |Robert W. Chambers