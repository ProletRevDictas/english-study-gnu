Koslow had faced criticism earlier in the summer for serving moldy jam at her restaurant, allegedly taking credit for other people’s recipes, and hiding an illegal kitchen space from the health department. The Mess That Is the 2020 James Beard Awards, Explained |Elazar Sontag |September 11, 2020 |Eater 

Many of these worker bees weren’t looking for electronic jams or Gregorian chants. People who really miss the office are listening to its sounds at home |Tanya Basu |September 10, 2020 |MIT Technology Review 

In early April, during that brief phase when lockdown felt more like an unexplored alien planet than the inescapable traffic jam it soon became, I called Japan’s biggest karaoke operators to see what they made of it all. Can Japan’s Karaoke Bars Survive the Pandemic? |Fiona Zublin |September 7, 2020 |Ozy 

That weekend and Monday saw 10-hour traffic jams to cross the border, the Union-Tribune reports. Border Report: The Lingering Trauma of Family Separation |Maya Srikrishnan |August 31, 2020 |Voice of San Diego 

However, if that is your jam and a wall with a hole in it somewhere awaits you, have at it. Sex Is Dead: Discuss |Eugene Robinson |August 10, 2020 |Ozy 

Most Cacophony events were one-off affairs, just enough to jam the culture a bit before moving on. Before the Bros, SantaCon Was as an Anti-Corporate Protest |David Freedlander |December 12, 2014 |DAILY BEAST 

The rears of planes are becoming hell with smaller, harder seats to jam as many passengers in as possible. Flying Coach Is the New Hell: How Airlines Engineer You Out of Room |Clive Irving |November 25, 2014 |DAILY BEAST 

Like Jolly, most of the women raced other motorized vehicles before making it into Monster Jam. The Moms of Monster Jam Drive Trucks, Buck Macho Culture |Eliza Krigman |November 22, 2014 |DAILY BEAST 

In 2015, Monster Jam will have a fleet of eight female drivers. The Moms of Monster Jam Drive Trucks, Buck Macho Culture |Eliza Krigman |November 22, 2014 |DAILY BEAST 

When my British husband insisted that what he truly wanted for his birthday was to see a Monster Jam Truck show, I cringed inside. The Moms of Monster Jam Drive Trucks, Buck Macho Culture |Eliza Krigman |November 22, 2014 |DAILY BEAST 

He became the low-born, petty tradesman, using the language of the hands of his jam factory. The Joyous Adventures of Aristide Pujol |William J. Locke 

The system would be perfect for the mellowing of port or madeira, but when it is applied to plum and apple jam or, when 18 pr. Gallipoli Diary, Volume I |Ian Hamilton 

I dont think much of this jam pie, complained Chet, holding up a wedge that he had taken from his sisters basket. The Girls of Central High on the Stage |Gertrude W. Morrison 

A river a hundred feet in width was crossed by a convenient jam of logs and trees. Gold-Seeking on the Dalton Trail |Arthur R. Thompson 

If anybody else offered him a bigger piece, or more jam, he would very quickly leave me. Digby Heathcote |W.H.G. Kingston