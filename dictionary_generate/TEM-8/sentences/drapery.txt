From color palette to navigation items, visitors see blinds, shutters, and drapery in all imagery and content throughout the site. Your website doesn’t need to cater to everybody — and that’s OK |Sponsored Content: Exults |March 11, 2021 |Search Engine Land 

Her yellow curtains replaced the neoclassical pale blue draperies designed by Mark Hampton for George and Barbara Bush. Presidents come and go, but these curtains are forever |Jura Koncius |February 1, 2021 |Washington Post 

Tjarks, who owns a drapery company called Gotcha Covered, is a conservative Republican. A rural S.D. community ignored the virus for months. Then people started dying. |Annie Gowen |December 9, 2020 |Washington Post 

Bending, with a breaking heart, I touched the marble drapery with my lips, then crept back into the silent house. Read ‘The King in Yellow,’ the ‘True Detective’ Reference That’s the Key to the Show |Robert W. Chambers |February 20, 2014 |DAILY BEAST 

Thompson uses elaborate drapery and ordinary objects to create mystical settings in isolated environments. Photographer Kyle Thompson Elevates the ‘Selfie’ to Self-Portraiture |Justin Jones |December 2, 2013 |DAILY BEAST 

The drapery and drawing of the figures in the earlier examples are also exceptionally good. The Catacombs of Rome |William Henry Withrow 

Ronald looked at the mass of chiffon and the quivering fall of drapery before him and smiled. The Weight of the Crown |Fred M. White 

Look more for the vibration of light and air on the flesh and drapery colors than for these colors in themselves. The Painter in Oil |Daniel Burleigh Parkhurst 

This drawing is now covered with its drapery, which is drawn from the life in charcoal, or a frottée of some sort. The Painter in Oil |Daniel Burleigh Parkhurst 

He was the first who painted woman with brilliant drapery and variegated head-dresses. Beacon Lights of History, Volume I |John Lord