He went on to lead Mumbai in scoring in a season that he began at age 15, making the international team at 16. The Story of the World’s Greatest Cricket Player |William O’Connor |December 24, 2014 |DAILY BEAST 

According to Merritt, at the time of the killings they were thrilled about scoring a big deal in Saudi Arabia. Family's Best Friend Charged With Murdering Them All |Nina Strochlic |November 7, 2014 |DAILY BEAST 

As governors, they have to be more interested in solving problems than scoring political points. The Secret GOP Swing State Election Romp |John Avlon |October 28, 2014 |DAILY BEAST 

We were scoring it like the Olympics: presentation, technique. Nigel Lythgoe on How to Save Reality TV, ‘On the Town,’ and ‘Brokeback Ballroom’ |Kevin Fallon |October 22, 2014 |DAILY BEAST 

As with any negotiation, what matters in the end is getting the desired outcome, not just scoring points along the way. It’s Time to Nail the Iran Nuke Deal |Rep. Rush Holt, Kate Gould |October 15, 2014 |DAILY BEAST 

It is usual for the scoring block to have two vertical columns divided halfway by a horizontal line. Encyclopaedia Britannica, 11th Edition, Volume 4, Part 3 |Various 

"Funny how I'm doing all the scoring," said Celia meditatively. Punch, or the London Charivari, Vol. 146, February 11, 1914 |Various 

He does not relate these characters to scoring or cracking quality. Northern Nut Growers Association Thirty-Fourth Annual Report 1943 |Various 

In 1876 tennis scoring was adopted, and it was the general opinion that lawn-tennis had come to stay. The Sportswoman's Library, v. 2 |Various 

In scoring the papers, allow one credit for each blank correctly filled. The Science of Human Nature |William Henry Pyle