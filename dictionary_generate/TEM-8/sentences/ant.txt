The most cooperative organisms like ants, bees, termites—or, if you want to compare mammals to mammals, the beautiful naked mole rats—aren’t renowned for their brilliance. The Dark Side of Smart - Facts So Romantic |Diana Fleischman |September 15, 2020 |Nautilus 

In exchange, the carpenter ants provide a protective cellular environment for the Blochmannia and transmit them to their offspring, ensuring the bacteria’s survival. How Two Became One: Origins of a Mysterious Symbiosis Found |Viviane Callier |September 9, 2020 |Quanta Magazine 

So the ants evolved to leave the ancestral germline as a “decoy” to attract the bacteria, Rajakumar said. How Two Became One: Origins of a Mysterious Symbiosis Found |Viviane Callier |September 9, 2020 |Quanta Magazine 

Yet while the cows’ bacteria merely inhabit the animals’ stomach, the bacteria in the ants live inside their gut cells as endosymbionts. How Two Became One: Origins of a Mysterious Symbiosis Found |Viviane Callier |September 9, 2020 |Quanta Magazine 

Bullet ant stings, he finds, are roughly 10 times more painful. What you need to know about ‘murder hornets’ |Susan Milius |July 20, 2020 |Science News For Students 

An Uber driver went on an anti-gay, ant-American rant before physically assaulting his passenger. The Ten Worst Uber Horror Stories |Olivia Nuzzi |November 19, 2014 |DAILY BEAST 

I was reducing everything to ant scale, the U.S. included—an ant White House, an ant CIA, an ant Congress, an ant Pentagon. Whit Stillman on the 20th Anniversary of ‘Barcelona’, His New Amazon Series, and the Myth of the Ugly Expat |Michael Weiss |August 10, 2014 |DAILY BEAST 

Strangely, he did this by diluting the sting of the ant scene. Whit Stillman on the 20th Anniversary of ‘Barcelona’, His New Amazon Series, and the Myth of the Ugly Expat |Michael Weiss |August 10, 2014 |DAILY BEAST 

In the U.S. view a small group—or cadre—of fierce red ants have taken power and are opposing the black-ant majority. Whit Stillman on the 20th Anniversary of ‘Barcelona’, His New Amazon Series, and the Myth of the Ugly Expat |Michael Weiss |August 10, 2014 |DAILY BEAST 

RAMON: (Seething with contempt) Secret ant landing strips, illegally established on foreign soil. Whit Stillman on the 20th Anniversary of ‘Barcelona’, His New Amazon Series, and the Myth of the Ugly Expat |Michael Weiss |August 10, 2014 |DAILY BEAST 

Her feet crush creeping things: there is a busy ant or blazoned beetle, with its back broken, writhing in the dust, unseen. God and my Neighbour |Robert Blatchford 

Pervenimvs huc (sicut ant numeratum est) vigesim secund Maij. The Jesuit Relations and Allied Documents, Vol. II: Acadia, 1612-1614 |Various 

Because we are crowded here and there in the ant-hills of our cities, we dream that the world is full. The Unsolved Riddle of Social Justice |Stephen Leacock 

But Burguy explains that romant is a false form, due to confusion with words rightly ending in -ant. Chaucer's Works, Volume 1 (of 7) -- Romaunt of the Rose; Minor Poems |Geoffrey Chaucer 

The ant individual preserves its powers of observation and thought and may initiate new processes. Man And His Ancestor |Charles Morris