Many tons of pink salt, which has a virginal, untouched aura, are mined in the Himalayas and exported. One man’s crusade to end a global scourge with better salt |Katie McLean |December 18, 2020 |MIT Technology Review 

McCarthy wore a virginal white wedding dress for the occasion. Jenny McCarthy: I Am Not Anti-Vaccine |Lloyd Grove |October 24, 2014 |DAILY BEAST 

The impression is left that this project would despoil a virginal natural landscape. Why Environmentalists Actually Oppose Keystone XL |David Frum |January 12, 2012 |DAILY BEAST 

They need to be converted from 1990s-Lolita to virginal-Miss Havisham in a matter of minutes. New York Fashion Week: Backstage at Marchesa |Isabel Wilkinson, Kevin Tachman |February 17, 2011 |DAILY BEAST 

And on the page, we finally get to meet the virginal Carrie, literally pre- Sex. The Making of Carrie Bradshaw |Rebecca Dana |May 24, 2010 |DAILY BEAST 

Her face, too, was inexpressibly virginal in its expression of innocence and of melancholy suffering. Camille (La Dame aux Camilias) |Alexandre Dumas, fils 

He saw a mere slender slip of a body, a virginal body, straight-clad; the body and the face of a white child. The Creators |May Sinclair 

But as soon as she had made this great advance, virginal instinct suggested a proportionate retreat. The Atlantic Monthly, Volume 17, No. 101, March, 1866 |Various 

The troubled virginal sweetness of the girl went to his soul. The Highgrader |William MacLeod Raine 

Who that has had this window opened for him into the virginal chamber of awakening woman-life can look through it without tears? The Open Question |Elizabeth Robins