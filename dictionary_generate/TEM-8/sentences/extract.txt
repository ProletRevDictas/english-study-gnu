Mycologist Paul Stamets, for one, has shown that a mycelium extract can decompose petroleum waste and sprout oyster mushrooms in its place. Imitation Is the Sincerest Form of Environmentalism - Issue 90: Something Green |Anastasia Bendebury & Michael Shilo DeLay |October 7, 2020 |Nautilus 

He and his team had shown that extracts of certain fungi could be used to reduce bee mortality dramatically. The Fungal Evangelist Who Would Save the Bees - Issue 90: Something Green |Merlin Sheldrake |September 23, 2020 |Nautilus 

He had been producing these extracts for human consumption for several years—it is largely these products that have made Fungi Perfecti into a multimillion-dollar business. The Fungal Evangelist Who Would Save the Bees - Issue 90: Something Green |Merlin Sheldrake |September 23, 2020 |Nautilus 

The jar dribbled sugar water laced with fungal extracts into the dish, and bees crawled through a chute to get to it. The Fungal Evangelist Who Would Save the Bees - Issue 90: Something Green |Merlin Sheldrake |September 23, 2020 |Nautilus 

The health benefits to me are real—anti-anxiety, anti-inflammation, et cetera—but it is up to each person to determine how hemp extract and CBD can benefit them. OneRepublic’s Ryan Tedder on launching a hemp-infused sparkling water brand |Rachel King |August 24, 2020 |Fortune 

Humanitarian organizations had already pulled out, and French troops rushed in to extract the 15 foreigners left in the city. ‘Argo’ in the Congo: The Ghosts of the Stanleyville Hostage Crisis |Nina Strochlic |November 23, 2014 |DAILY BEAST 

The scientists were able to extract sufficient DNA from the roots, and they did indeed find the virus fossils. Ebola's Roots Are 50 Times Older Than Mankind. And That Could Be the Key to Stopping It. |Michael Daly |October 20, 2014 |DAILY BEAST 

The procedure they undergo to extract eggs is intense and invasive and there are no sexual kicks involved. Today’s Sperm Donor Isn’t a Broke 20-Something |Stephanie Fairyington |September 20, 2014 |DAILY BEAST 

Sophia pays the $20,000 or more necessary to extract and freeze a large number of her eggs. ‘Designer’ Babies Are Only for the Rich |Jean Twenge |July 7, 2014 |DAILY BEAST 

So the advantages of being able to extract and store the most energy out of the minimum of calories far outweighed any risks. How Famines Make Future Generations Fat |Carrie Arnold |May 11, 2014 |DAILY BEAST 

It may be applied directly to a suspected fluid, or, better, to the ethereal extract. A Manual of Clinical Diagnosis |James Campbell Todd 

The following extract from the "Australasian" entitled, "Tobacco Smoking" refers to many literary smokers. Tobacco; Its History, Varieties, Culture, Manufacture and Commerce |E. R. Billings. 

As to the concluding line of the extract, I must leave it to some better Irish scholar than I can boast myself. Notes and Queries, Number 196, July 30, 1853 |Various 

This Extract will make a convenient statistic reference for matters concerning Liberia. The Condition, Elevation, Emigration, and Destiny of the Colored People of the United States |Martin R. Delany 

See the whole extract from Boccaccio, given and translated in the Introduction; see p. 68, above. Chaucer's Works, Volume 1 (of 7) -- Romaunt of the Rose; Minor Poems |Geoffrey Chaucer