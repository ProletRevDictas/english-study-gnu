Today, even as soda consumption falls, the rivalry rages on, with both companies adding juices, teas, and waters to their portfolios. America’s Hidden Duopoly (Ep. 356 Rebroadcast) |Stephen J. Dubner |September 3, 2020 |Freakonomics 

Meanwhile, the connected TV platform rivalry between Amazon and Roku shows no signs of slowing, especially with Google poised to make a bigger play for a piece of the market with a new Android TV device in the works. The streaming wars have escalated over turf grabs |Tim Peterson |August 26, 2020 |Digiday 

The debate touches every area of policy, from trade rivalries to unemployment benefits, and everyone has an interest in the outcome. After $20 trillion in pandemic relief spending, there’s still no sign of inflation. What happened? |Bernhard Warner |August 25, 2020 |Fortune 

First, rising tensions between the US and China threaten to initiate a new arms race and a return of great-power rivalry. Covid-19 and the geopolitics of American decline |Katie McLean |August 19, 2020 |MIT Technology Review 

The contrast between the serious rivalry among the female lovers and the silly homoerotic rituals of the male courtiers is fascinating. FROM THE VAULTS: Journeys to the LGBTQ past |Brian T. Carney |August 13, 2020 |Washington Blade 

Making this scenario more complicated is the rivalry between al Qaeda and ISIS. A Gift to the Jihadis: The Unseen Airport Security Threat |Clive Irving |December 27, 2014 |DAILY BEAST 

What seemed to be an old political rivalry took on ethnic overtones, with the very survival of entire communities now at stake. ‘The Good Lie’ and the Hard Truths of South Sudan |John Prendergast |October 3, 2014 |DAILY BEAST 

Their sibling rivalry drove a significant number of episodes. Orange Is the New Weeds: The Adventures of Jenji Kohan Across the 8th Dimension |Rich Goldstein, Emily Shire |August 18, 2014 |DAILY BEAST 

Because Cuba has only one 16-team circuit, league rivalry is no issue. Can Baseball’s All-Star Game Be Saved? |Peter C. Bjarkman |July 15, 2014 |DAILY BEAST 

No real rivalry, envy, betrayal, or even affection, at least not yet. Generic and Superficial ‘Tyrant’ Amerisplains the Middle East |Andrew Romano |June 25, 2014 |DAILY BEAST 

Those were the days when between the Scottish railway companies the keenest rivalry and the bitterest competition existed. Fifty Years of Railway Life in England, Scotland and Ireland |Joseph Tatlow 

He had, moreover, the adroitness to extirpate that rivalry which alone destroys all united effort. The Philippine Islands |John Foreman 

That greatly excited my rivalry, and I succeeded in finding some reasons for eulogy that she had forgotten. Charles Baudelaire, His Life |Thophile Gautier 

The Royal Yacht Squadron's fine class of schooners and vessels of large tonnage, however, created and revived rivalry. Yachting Vol. 2 |Various. 

The latter learned that in athletics especially the rivalry between the two lower and the two upper classes was intense. Ruth Fielding At College |Alice B. Emerson