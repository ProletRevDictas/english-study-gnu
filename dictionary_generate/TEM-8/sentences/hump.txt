The problem is getting over the disgust hump, because people don’t realize they’ll cease to be disgusted once they get used to something. The Downside of Disgust (Ep. 448) |Stephen J. Dubner |January 21, 2021 |Freakonomics 

They were about how animals got their famous features, like the camel’s hump and the leopard’s spots. Why Birds Can Fly Over Mount Everest - Issue 94: Evolving |Walter Murch |December 30, 2020 |Nautilus 

The vehicles will have longer wheelbases with shorter overhangs, which means the steering wheel and pedals can come forward, and there’s no hump down the center of the cabin to accommodate the drivetrain. Hyundai plans to build dozens of vehicles on its new modular EV platform |Stan Horaczek |December 3, 2020 |Popular-Science 

If Democrats can get lower-propensity voters in the Rio Grande Valley and the big metro areas to turn out, I’m convinced we’d get over the hump. The Trailer: The seven political states of Texas |David Weigel |October 4, 2020 |Washington Post 

It’s no longer Wednesday, but you should still check out our OZY hump day playlist. This Weekend: A Sangria Slushie Summer |Fiona Zublin |August 7, 2020 |Ozy 

Diplomatic dispatches at the time, written by men who had no reason to lie to their own rulers, reported no hump or withered arm. Unraveling King Richard III’s Secrets | |February 13, 2013 |DAILY BEAST 

Also 2004: John Kerry kept it close but never got over the hump. Barack Obama’s Cockiness Problem in His Contest With Mitt Romney |Michael Tomasky |April 17, 2012 |DAILY BEAST 

Despite 20 years of cycling between expectation and disappointment, I hump along with that aim in mind. Author to Bestsellers: Drop Dead |Valerie Frankel |September 19, 2011 |DAILY BEAST 

In the former Bill Hurt longed to hump you and in the latter a large dog did just that in your role as an animal trainer. Kathleen Turner's New Broadway High |Kevin Sessums |April 17, 2011 |DAILY BEAST 

Of course, not even a new Contract With America may be enough to help Republicans over the hump in 2010. The GOP Replays 1994 |Samuel P. Jacobs |October 12, 2009 |DAILY BEAST 

She wore an old poke bonnet and carried a crooked stick, and there seemed to be a hump upon her back. The Campfire Girls of Roselawn |Margaret Penrose 

The hump-backed little figure with poke-bonnet and cane was chased out upon the broken landing. The Campfire Girls of Roselawn |Margaret Penrose 

The cloak and the hump under it were likewise torn off and went sailing away on the current. The Campfire Girls of Roselawn |Margaret Penrose 

Now, said the hump-backed man, who had watched him keenly, what do you mean by coming into my house in this violent way? Oliver Twist, Vol. II (of 3) |Charles Dickens 

Hump, stay and take this wolfeOut of my brest, that thou hast lodgd there, orFor euer lose mee. The Fatal Dowry |Philip Massinger