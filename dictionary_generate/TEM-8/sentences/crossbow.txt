Men who live in multimillion-dollar houses and type on computers all day but spend their weekends running around the woods with crossbows! You Season 3 Is the Best Installment Yet—and a Brilliant Send-Up of Suburbia |Judy Berman |October 17, 2021 |Time 

Previously, scientists deduced these weapons act much like crossbows. Mantis shrimp start practicing their punches at just 9 days old |Charles Choi |April 29, 2021 |Science News 

When police interviewed Jeremy Farmer, he told them he’d last seen his father in early November 2019, just before he supposedly left on the hunting trip with his gear, including a crossbow and rifles, packed as if he was ready to leave. An Indiana man never returned from a hunting trip. Strange texts and emojis led police to his son. |Katie Shepherd |February 12, 2021 |Washington Post 

So, unlike the regular crossbow which was usually supported by the shoulder, the balance for Chu-Ko-Nu was kept by only using the hands. 14 Exceptional Weapon Systems from History That Were Ahead of their Time |Dattatreya Mandal |March 26, 2020 |Realm of History 

Tyrion, now on the lam for patricide by crossbow, is destined for an unknown foreign port like a diminutive Edward Snowden. Valar Morghulis: Game of Thrones’ Women Are Going to Rule the World |Scott Bixby |June 17, 2014 |DAILY BEAST 

Things take a dark turn when Joffrey forces Ros to beat Daisy with a whip, while he aims his crossbow at the pair. Game of Thrones’ 8 Most WTF Scenes: Twincest Rape, Shadow Baby, George W. Bush’s Head, and More |Marlow Stern |April 26, 2014 |DAILY BEAST 

All these things make Daryl more than just the cool guy with a crossbow. A Perfect 'Walking Dead' Episode |Melissa Leon |March 3, 2014 |DAILY BEAST 

And Daryl and his crossbow are as lethal as ever—though for a minute there it looked like a falling helicopter might do him in. ‘The Walking Dead’: Season 4 Premiere Reminds Us Why We Love This Show |Melissa Leon |October 14, 2013 |DAILY BEAST 

“Cross-sector,” “compounding factor,” and inspiring statistics fly like bolts from some particularly cheery crossbow. Is It Over? A 2012 Clinton Global Initiative Postmortem |Matthew DeLuca |September 26, 2012 |DAILY BEAST 

Herbert laid down his great axe, set his crossbow, laid a quarrel and levelled into the dark. God Wills It! |William Stearns Davis 

He grasps with his right hand a gun, or crossbow, and looks angrily towards the King, who seems somewhat confused and alarmed. The Fortunes of Nigel |Sir Walter Scott 

He leaped upward, as high as possible, his crossbow in his hand. Space Prison |Tom Godwin 

The Quarel or bolt used for the crossbow is shorter and thicker than that used for the longbow. Armour &amp; Weapons |Charles John Ffoulkes 

Betwixt the third couple of towers were the butts for arquebus, crossbow, and arbalist. The Best of the World's Classics, Restricted to Prose, Vol. VII (of X)--Continental Europe I |Various