I’ve put roofs on, made fences, laid sod, swept the floors—just about everything you can think of. Volunteer service, RV style |Mark Wolverton |June 29, 2022 |MIT Technology Review 

You can rent a machine called a sod cutter or dig it up with a shovel. It’s time to rip up your lawn and replace it with something you won’t need to mow |John Kennedy |June 27, 2021 |Popular-Science 

Similarly, fresh sod also needs soil preparation and may not make it through the summer, but it will look good for a few weeks, at least. Time to dig in |Adrian Higgins |March 17, 2021 |Washington Post 

The fishiest places we found were along the sod banks to the east of the inlet, where the current had cut nice underhangs along the banks. This motorized kayak can drive itself |By Nate Matthews/Outdoor Life |August 28, 2020 |Popular-Science 

The poor sod is undertaking a 24 hour drive to meet up with the teams. Prince Harry Goes from Sunny 70s To Freezing -40s |Tom Sykes |November 22, 2013 |DAILY BEAST 

“For some reason I thought, Oh, sod it, I will sign up,” he says. My Fancy Twitter Followers |Tom Sykes |November 2, 2011 |DAILY BEAST 

They painted his house, maintained his yard, replaced the sod, installed artificial turf, and planted and moved shrubbery. Hoover’s Secret Files |Ronald Kessler |August 2, 2011 |DAILY BEAST 

The dooryard was covered with a heavy sod and the ancient flower beds had run wild with weeds. The Campfire Girls of Roselawn |Margaret Penrose 

She covered her face with her hands and sank down on the wet sod, while the rain beat upon her unmercifully. The Campfire Girls of Roselawn |Margaret Penrose 

And as he did so, he regarded the spot where the sod house had once stood and wherein he had spent many happy days. The Homesteader |Oscar Micheaux 

We slipped on the wet sod, we fell together sprawling on the rocks. The Works of Robert Louis Stevenson, Volume XXI |Robert Louis Stevenson 

His front foot dug in the sod, his eyes were red, and between his grumbles his breath came in puffs and snorts of anger. The Way of a Man |Emerson Hough