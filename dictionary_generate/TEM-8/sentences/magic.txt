The Nobel prize-winning psychologist Dan Kahneman has said it’s the “most significant of the cognitive biases” and the first thing he’d change about humans if he had a magic wand. Use this tool to overcome the biggest decision-making mistake |David Yanofsky |September 6, 2020 |Quartz 

I think he might be wondering if something that some of us call “God” or “magic” is tied up with all of those still undiscovered mysteries. The ancient palindrome that explains Christopher Nolan’s Tenet |Alissa Wilkinson |September 4, 2020 |Vox 

To get a little bit of that magic combination of determination and foresight, we asked them to tell us the best career advice they ever got. The advice that helped this year’s 40 under 40 find their own path |kdunn6 |September 3, 2020 |Fortune 

It’s really about the experience and the learning and the people—the magic of the trail. Inside an FKT Attempt on the Appalachian Trail |Martin Fritz Huber |September 3, 2020 |Outside Online 

While she has turned in strong performances in those efforts, it’s in her own shows that Coel makes magic happen. How Michaela Coel Tells Awkward Truths |Eromo Egbejule |August 30, 2020 |Ozy 

In the end, the line between magic and religion may be something of an artificial one. Harry Potter and the Torah of Terror |Candida Moss, Joel Baden |January 4, 2015 |DAILY BEAST 

“One of the producers on a story we were doing on the Orlando Magic told me about this young guy he really liked,” Jaffe said. Remembering ESPN’s Sly, Cocky, and Cool Anchor Stuart Scott |Stereo Williams |January 4, 2015 |DAILY BEAST 

The future Mr. Vergara—and star of ‘True Blood’ and ‘Magic Mike’ shares some life advice in an exclusive video. Who Is Joe Manganiello? Sofia Vergara’s Fiancé on the Value of Hard Work |The Daily Beast Video |December 29, 2014 |DAILY BEAST 

Despite all the gun talk in “Hot N—,” everyone wanted a piece of him and his magic. Bobby Shmurda and Rap’s Ultimate Hoop Dream |Rawiya Kameir |December 23, 2014 |DAILY BEAST 

Both Prados have enough magic that, after you visit them, the whole world feels like their gift shop for a few hours. The Life and Hard Times Of The Family A Cuban Defector Left Behind |Brin-Jonathan Butler |December 19, 2014 |DAILY BEAST 

Here began indeed, in the drab surroundings of the workshop, in the silent mystery of the laboratory, the magic of the new age. The Unsolved Riddle of Social Justice |Stephen Leacock 

A tall phantom in livery appeared, as if by magic, and signed to me to ascend the grand staircase. Music-Study in Germany |Amy Fay 

However, on reaching Spain, the magic of the Emperor's personality soon restored the vigour and prestige of the French arms. Napoleon's Marshals |R. P. Dunn-Pattison 

All this I admit to be the fever of the mind—a waking dream—an illusion to which mesmerism or magic is but a frivolity. Blackwood's Edinburgh Magazine, No. CCCXXXIX. January, 1844. Vol. LV. |Various 

On every side rose little islands, covered with small trees or underwood, lending a most magic appearance to the river. A Woman's Journey Round the World |Ida Pfeiffer