Marketers need to be respectful of these pioneers and create messaging that speaks to their resourceful spirit. ‘Bullish on the space’: Why Anheuser Busch InBev is testing NFTs in its marketing |Seb Joseph |June 16, 2021 |Digiday 

He was an immensely resourceful young man who left a deep impression in the hearts and minds of all who were fortunate to have met, known, and loved him. 'Don't Give Up Hope.' A Mother On The Pain of Losing Her Son To Opioids and Learning To Find Her Voice |Paul Moakley |May 7, 2021 |Time 

With better education and a widening of employment options, women were becoming more independent and resourceful. What the Rise and Fall of the Cinderella Fairy Tale Means for Real Women Today |Carol Dyhouse |April 19, 2021 |Time 

When the shots aren’t falling, the Pelicans need to be resourceful in getting open looks. Lonzo Ball Has Taken A Leap This Season. Will It Be Enough To Keep Him In New Orleans? |James L. Jackson |April 1, 2021 |FiveThirtyEight 

For years he was a resourceful link and activist-connector for American and emerging worldwide LGBT rights movements. Activist, writer John Stephen Hunt dies at 85 |Staff reports |March 30, 2021 |Washington Blade 

Joey was strong, resourceful, plucky, goofy, and vulnerable—and, of course, you hoped she would pick Pacey over Dawson. How Can Katie Holmes Escape Tom Cruise—and ‘Dawson’s Creek’? |Tim Teeman |October 30, 2014 |DAILY BEAST 

Yet, I also witnessed the resourceful way quidditch players responded to criticism. Why I Named My Quidditch Film Mudbloods |Farzad Sangari |October 14, 2014 |DAILY BEAST 

But colleagues insist he is smart, resourceful, talented, and terrifically hard-working. Privilege and Power: The Rise and Rise of Ben Sherwood |Lloyd Grove |March 26, 2014 |DAILY BEAST 

Milchberg was careful and resourceful, acquiring a work permit for the Ostbahn railway yard, where he unloaded coal trucks. The Week in Death: Irving Milchberg, the Teenage Gunrunner of the Warsaw Ghetto |The Telegraph |March 1, 2014 |DAILY BEAST 

Children are not idealized: they are resourceful but prickly, cunning but confused. Hayao Miyazaki’s ‘The Wind Rises’: An Anime Icon Bows Out |Andrew Romano |November 15, 2013 |DAILY BEAST 

It was a difficult undertaking, but Kerr thought a bold, resourceful man could carry it out with profit. The Girl From Keller's |Harold Bindloss 

You're young and strong, reasonably intelligent, and extremely resourceful. The Status Civilization |Robert Sheckley 

A peep into a resourceful mind would be more to the purpose. By the Christmas Fire |Samuel McChord Crothers 

At last the resourceful Vertumnus hit upon the plan that brought him well-earned success. Stories of Old Greece and Rome |Emilie Kip Baker 

I looked with admiration upon this resourceful Celestial, and then felt mildly irritated at the completeness of the whole ménage. A Woman's Journey through the Philippines |Florence Kimball Russel