Not to speak of their inmost feelings does not, on the other hand, prevent them at times from being most confidential. The Real Latin Quarter |F. Berkeley Smith 

After it is on the bottle, take some of the best sweet oil and with a clean sponge wet the lace thoroughly to the inmost folds. The Ladies' Book of Etiquette, and Manual of Politeness |Florence Hartley 

It seemed that this great searcher of human hearts must be able to read at a glance the inmost secrets of my own. The Life &amp; Letters of Peter Ilich Tchaikovsky |Modeste Tchaikovsky 

The inmost experience of life becomes incomparably more complete for each of them. Urania |Camille Flammarion 

The place of honour was not on a dais at the inmost end of the hall, like the high table in college halls. Homer and His Age |Andrew Lang