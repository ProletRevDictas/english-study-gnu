Considering Beyonce was just named the most powerful celebrity in the world, Watters may want to revise his definition. New York City's Professional Bridesmaid; Fox News Creates 'Beyonce Voters' Demographic |The Fashion Beast Team |July 2, 2014 |DAILY BEAST 

Obeidi explained that Gordon truly did revise the script in many—not all—places flagged by MPAC. For Muslims, Howard Gordon’s ‘Tyrant’ Is a Step in the Right Direction |Dean Obeidallah |June 24, 2014 |DAILY BEAST 

Pakistan needs to revisit, revise and improve its foreign relations to ask for support if needed. Fighting The Talibanization Of Pakistan |Dr. Mona Kazim Shah |June 14, 2014 |DAILY BEAST 

To revise, as they do, the landay tradition, once the sole purview of man, is to risk death. Beauty and Subversion in the Secret Poems of Afghan Women |Daniel Bosch |April 6, 2014 |DAILY BEAST 

The Senate voted on Thursday to revise its rules governing filibusters. Senate Democrats Didn’t Go Far Enough to Kill the Filibuster |Dean Obeidallah |November 22, 2013 |DAILY BEAST 

A convention of delegates to revise the constitution of New York met at Albany. The Every Day Book of History and Chronology |Joel Munsell 

But my limited experience of the Mahajan of Champaran has made me revise the accepted opinion about his 'blighting influence.' Third class in Indian railways |Mahatma Gandhi 

He had great faith in the opinions of his little helper, and was always ready to revise his own judgment if hers contradicted it. In the Onyx Lobby |Carolyn Wells 

The results will surprise them, and they will quickly be forced to revise their methods of treating illness. The Book of Life: Vol. I Mind and Body; Vol. II Love and Society |Upton Sinclair 

The London publishers of the Yankee were keenly anxious to revise the text for their English readers. Mark Twain, A Biography, 1835-1910, Complete |Albert Bigelow Paine