Some of the most damaging storms to hit North America bear these names. Soggy coastal soils? Here’s why ecologists love them |Alison Pearce Stevens |September 17, 2020 |Science News For Students 

Five days after those lightning storms set California on fire, the flames reached his home in the Santa Cruz Mountains and burned it to ashes. Suppressing fires has failed. Here’s what California needs to do instead. |James Temple |September 17, 2020 |MIT Technology Review 

Hurricane season stretches into November, which means we have at least another month and of storms. A nearly unprecedented cluster of tropical storms are brewing in the Atlantic |Sara Chodosh |September 15, 2020 |Popular-Science 

Although both of these storms should turn north well short of the continental United States, there is some concern about Paulette reaching Bermuda as a Category 1 hurricane by early next week. It’s the peak of the Atlantic hurricane season, and the tropics are bonkers |Eric Berger |September 10, 2020 |Ars Technica 

Meanwhile, most teachers, students, and parents are essentially waiting for the storm to pass. Why Can’t Schools Get What the N.F.L. Has? (Ep. 431) |Stephen J. Dubner |September 10, 2020 |Freakonomics 

The Perfect Storm writer talks combat brotherhood and the threat posed by growing wealth inequality. Sebastian Junger on War, Loss, and a Divided America |The Daily Beast Video |January 1, 2015 |DAILY BEAST 

The fear that Pascal might weather the storm has Du Vernay, Oprah Winfrey, and other Hollywood elites pulling their punches. The Disaster Story That Hollywood Had Coming |Doug McIntyre |December 17, 2014 |DAILY BEAST 

Random House is also covering the legal fees of an innocent man called Barry who was caught up in the storm. The Right's Rape Trolls vs. Lena Dunham |Emily Shire |December 10, 2014 |DAILY BEAST 

But so many years later, I still get a tense feeling in my stomach when I see a strong storm approaching. Heed the Warnings: Why We’re on the Brink of Mass Extinction |Sean B. Carroll |November 30, 2014 |DAILY BEAST 

Rather than storm the hospital, Tyreese says, the group should take a couple of cops hostage then set up a trade with Dawn. The Walking Dead’s ‘Crossed’: The Stage Is Now Set for a Bloody, Deadly Midseason Finale |Melissa Leon |November 24, 2014 |DAILY BEAST 

A wise man hateth not the commandments and justices, and he shall not be dashed in pieces as a ship in a storm. The Bible, Douay-Rheims Version |Various 

This treacherous sort of calm, we thought, might forbode a storm, and we did not allow it to lull us into security. Blackwood's Edinburgh Magazine, No. CCCXXXIX. January, 1844. Vol. LV. |Various 

It was depressing to think of going to bed in such circumstances with the yelling of an Arctic storm for a lullaby. The Giant of the North |R.M. Ballantyne 

The storm, however, was over; the moon and stars were shining in a clear sky, and the aurora was dancing merrily. The Giant of the North |R.M. Ballantyne 

While the fortress was undermining at home, they were not idle, who were preparing to storm it from abroad. The Pastor's Fire-side Vol. 3 of 4 |Jane Porter