Hold the weight plumb under your shoulder and next to your hip on your standing-leg side. How to Fix Fallen Arches |aweinberg |September 22, 2021 |Outside Online 

The show’s science fiction background and action-packed battles keep each episode engaging, even during the slower-paced moments when it plumbs the depth of each character’s mental state. What to Know About the New 'Neon Genesis Evangelion’ Film, According to Series Creator Hideaki Anno |Patrick Lucas Austin |August 20, 2021 |Time 

The city had likewise been disappointed by its own forensic experts, the outside digital-audio specialists who neither police nor prosecutors had sought out to plumb the Gaines tape. Minneapolis Police Were Cleared in the 2013 Killing of Terrance Franklin. A Video Complicates the Story—and Now the Case May Be Reopened |Josiah Bates |June 25, 2021 |Time 

It’s an at-times challenging work that plumbs her maternal relationship to its deepest depths. The Best Books of 2021 So Far |Lucas Wittmann |May 28, 2021 |Time 

Miller says he sympathizes with what I’m saying about the power of art coming from the connection with a human artist, plumbing their emotions and consciousness. I Am Not a Machine. Yes You Are. - Issue 98: Mind |Kevin Berger |March 31, 2021 |Nautilus 

He has no redeeming or (even complicating) qualities—no depths to plumb, no angles to survey, no gray areas to explore. Game of Thrones’ ‘The Lion and the Rose’: Joffrey’s Demented, Shocking Royal Wedding |Andrew Romano |April 14, 2014 |DAILY BEAST 

Nearly 65 years after the fact, it's amazing how much of what we think we know about Britain's "finest" hour is just plumb wrong. Rethink Everything You Think You Know About World War II |David Frum |February 10, 2013 |DAILY BEAST 

In the “just plumb crazy” class, I put the business of his chaining his mug to the radiator to prevent its being stolen. Alan Turing’s Brother: He Should Be Alive Today |John Ferrier Turing |June 23, 2012 |DAILY BEAST 

She believes her illness has bestowed on her a single-mindedness that causes her to plumb the same waters again and again. Josephine King Connects Madness and Art in London |Casey Schwartz |May 1, 2012 |DAILY BEAST 

Her companions stuck to the side of the road, but Suu Kyi walked into the middle, plumb in the line of fire. ‘The Lady and the Peacock’: Peter Popham’s Biography Reveals the Real Aung San Suu Kyi |Peter Popham |March 29, 2012 |DAILY BEAST 

Then, grandpaw, he turns round to the baby again, plumb took up with them four new nippers. Alec Lloyd, Cowpuncher |Eleanor Gates 

He put his hand to his belt, screwed up his mug, and said he felt plumb et up inside. Alec Lloyd, Cowpuncher |Eleanor Gates 

I just sit there, knocked plumb silly, almost, and looked at a big rose in the carpet. Alec Lloyd, Cowpuncher |Eleanor Gates 

An' I'm here t' declare that it's plumb foolish t' mix things with that layout till we can see t' shoot tolerable straight. Raw Gold |Bertrand W. Sinclair 

On either side of this isolated bar of sandstone a plumb-line might have been dropped straight to the level of the river. Overland |John William De Forest