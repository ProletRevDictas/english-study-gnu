Mason is long gone from Groupon, but he remains a bit of a wag. Descript raises $30M to build the next generation of video and audio editing tools |Ingrid Lunden |January 12, 2021 |TechCrunch 

One wag joked that Liberty was the only university where football players and nerds got the same amount of sex. Alleged U.Va. Abductor Accused of Rape at Christian College |Michael Daly |September 28, 2014 |DAILY BEAST 

“The first cover-up of the de Blasio administration,” one wag joked. What Really Happened to De Blasio’s FDR Bible |Michael Daly |January 3, 2014 |DAILY BEAST 

One wag tweeted: “I saved millions of lives … by getting people to not vote for your father.” Josh Romney’s Awkward Car-Crash ‘Selfie’ |Lloyd Grove |November 30, 2013 |DAILY BEAST 

Rather, this tiny tail of the car industry is starting to wag the dog. Tesla’s Rise Forces Other Automakers to Up Their Electric Car Game |Daniel Gross |September 25, 2013 |DAILY BEAST 

And ultimately, Kumar said, “efforts at message discipline tend not to work” and tongues, at long last, begin to wag. At the Obama White House: Transparency Transhmarency |Lloyd Grove |August 23, 2013 |DAILY BEAST 

"I bought them boots to wear only when I go into genteel society," said one of the codfish tribe, to a wag, the other day. The Book of Anecdotes and Budget of Fun; |Various 

While a one-step was in full swing some would-be wag suddenly turned off all the lights. Uncanny Tales |Various 

A distinguished wag about town says, the head coverings the ladies wear now-a-days, are barefaced false hoods. The Book of Anecdotes and Budget of Fun; |Various 

If all the world did not wag his way, so much the worse for cold-blooded mercenary superfluous beings. Ancestors |Gertrude Atherton 

The fellow she came with is Delmet the architect—a great wag—lazy, but full of fun—and genius. The Real Latin Quarter |F. Berkeley Smith