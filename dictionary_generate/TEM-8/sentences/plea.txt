A couple of years later, she was charged with larceny in a separate case from the first shoplifting incident and was offered a plea deal. A woman in ICE detention says her fallopian tube was removed without her consent |Nicole Narea |September 17, 2020 |Vox 

Following pleas from their governments, tourists from the Czech Republic, Poland, and Slovakia will also be able to enter Hungary if they had already booked a holiday there. As Europe’s COVID-19 numbers rise, Hungary becomes first EU country to reimpose blanket border restrictions |David Meyer |September 1, 2020 |Fortune 

In their pleas, they said that without government support, their restaurants would close for good. Is the Government Just Going to Watch the Restaurant Industry Die? |Elazar Sontag |August 28, 2020 |Eater 

Unlike most plea agreements, in which the judge remains free to decide the defendant’s sentence, Loughlin’s and Giannulli’s proposed prison terms were binding once accepted. Lori Loughlin gets two months in prison after judge accepts plea deal in college bribery scandal |radmarya |August 21, 2020 |Fortune 

As a result, California in 2014 became the first state to ban the plea. Harris as VP pick brings diversity, LGBTQ ally to Biden ticket |Chris Johnson |August 11, 2020 |Washington Blade 

He later accepted a plea deal that put him behind bars for 25 years. An Informant, a Missing American, and Juarez’s House of Death: Inside the 12-Year Cold Case of David Castro |Bill Conroy |January 6, 2015 |DAILY BEAST 

Schettino also tried to enter a plea bargain agreement, which ultimately was rejected by the Grosseto court. The Costa Concordia’s Randy Reckless Captain Takes the Stand |Barbie Latza Nadeau |December 2, 2014 |DAILY BEAST 

The mother also made a plea to the violent ones who wreak such havoc. 11 Children Shot in Milwaukee, One in Her Grandpa's Lap |Michael Daly |November 12, 2014 |DAILY BEAST 

It was an intimate and somber plea, like a parent opening an intervention with a wayward child. How to Run a Statewide Campaign on $38 |Michael Ames |November 12, 2014 |DAILY BEAST 

He could have gotten away with a greatly reduced fine and no time behind bears if he had simply entered a guilty plea. He Faces Jail for Rescuing Baby Eagles |Michael Daly |November 2, 2014 |DAILY BEAST 

It is probable he wished to provide written proof of a plea that he was an unwilling agent in the clutch of a mutinous army. The Red Year |Louis Tracy 

Nor can he hold the company on the plea of ignorance by declaring he has not read it, for it is his duty to read the receipt. Putnam's Handy Law Book for the Layman |Albert Sidney Bolles 

On the plea that they must hasten if the midday heat were to be avoided, they cut short the halt to less than an hour. The Red Year |Louis Tracy 

In short, insurgency ceased to be a valid plea; if it existed in fact, officially it had become a dead letter. The Philippine Islands |John Foreman 

For the popular voice accused him of outrages for which the utmost license of civil war would not furnish a plea. The History of England from the Accession of James II. |Thomas Babington Macaulay