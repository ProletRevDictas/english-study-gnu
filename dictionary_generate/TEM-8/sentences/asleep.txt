We fell asleep to the sound of the waves crashing against the breakwall. A Maui vacation in three acts |Alex Pulaski |February 12, 2021 |Washington Post 

We allow our smartphones to monitor where we go, what time we fall asleep and even whether we’ve washed our hands for a full 20 seconds. Can privacy coexist with technology that reads and changes brain activity? |Laura Sanders |February 11, 2021 |Science News 

Sometimes, when I am ready to turn in for the night, I will find him asleep in my bed! Miss Manners: Family member’s manners have gone to the dogs |Judith Martin, Nicholas Martin, Jacobina Martin |February 9, 2021 |Washington Post 

He has a reputation as being hard to work with, and reportedly falls asleep in meetings that don’t interest him. Why Larry Summers Still Triggers Washington. (It Isn’t His Economics.) |Philip Elliott |February 8, 2021 |Time 

By the time I came home, they were both asleep and then we would do the same thing over and over again. How French Baking Was Brought to Virginia |Eugene Robinson |February 5, 2021 |Ozy 

Dehydrated and feeling weary, Marino lay down beside another migrant under a tree and fell asleep. Drug Smuggler Sues U.S. Over Dog Bite |Caitlin Dickson |December 10, 2014 |DAILY BEAST 

And he was followed by each one of them until the seventh dwarf looked at his bed and saw Little Snow White lying there asleep. In New Brothers Grimm 'Snow White', The Prince Doesn't Save Her |The Brothers Grimm |November 30, 2014 |DAILY BEAST 

Inside, it seemed hopeless, for every chair in sight was occupied, and a dozen men were asleep on the floor. The Stacks: H.L. Mencken on the 1904 Baltimore Fire |H.L. Mencken |October 4, 2014 |DAILY BEAST 

Sometimes the political people at the White House just appear to be asleep. Immigration Reform? Not Until Hillary |Michael Tomasky |September 8, 2014 |DAILY BEAST 

The girl next to me, an attractive brunette in her 20s, spent much of the first act and most of the second half asleep. The Hell of the Hamptons: Why the Exclusive Hotspot Is a Mind-Numbing Drag |Robert Gold |August 18, 2014 |DAILY BEAST 

The Café tender was asleep in his chair; the porter had gone off; the sentinel alone kept awake on his post. Glances at Europe |Horace Greeley 

The sun shone and the birds sang, and the day was beautiful without when she at last fell asleep again. The Homesteader |Oscar Micheaux 

Here Badorful rolled over upon his side, and was instantly fast asleep. Davy and The Goblin |Charles E. Carryl 

Upon this he went to bed again, fell asleep, and dreamed a fourth time as before. The Life and Most Surprising Adventures of Robinson Crusoe, of York, Mariner (1801) |Daniel Defoe 

The child asleep again, he laid it on its bed, and then sat far into the night thinking barrenly. The Joyous Adventures of Aristide Pujol |William J. Locke