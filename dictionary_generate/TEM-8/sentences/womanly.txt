It’s my womanly body, my hip had spread to a nice level and a lot of it went to my butt. Nafessa Williams Advocates For Equality In New Y-FEAR Capsule |D'Shonda Brown |November 19, 2021 |Essence.com 

All the grit of his leading man handsomeness had been polished away, and with this new look, he became pretty, feminized, womanly. The Jennifer Lopez-Ben Affleck recoupling is the greatest gossip story we’ve had in years |Constance Grady |August 24, 2021 |Vox 

Any belly fat might redistribute itself in a more “womanly” way around the hips, thighs, and buttocks. Hormone Therapy Is More Than Just a Physical Process |Brandy Zadrozny |August 22, 2013 |DAILY BEAST 

In Family Weekend, then, Chenoweth gets what may be her most womanly film role yet. Kristin Chenoweth on Her Darker Role in ‘Family Weekend’ |Kevin Fallon |March 27, 2013 |DAILY BEAST 

Fuller had an instinct, “from a very early age ... that I was not born to the common womanly lot,” Marshall explains. Why Do Women Love Bad Men? A New Life of Margaret Fuller |Susan Cheever |March 22, 2013 |DAILY BEAST 

And yes, his clothes are even designed to fit women with womanly assets. The Birth of Unisex Couture: Rad Hourani |Misty White Sidell |January 25, 2013 |DAILY BEAST 

Every comment, every slight, every mean assessment of my body, my face, my physical womanly beautiful outside. Your Puffy-Face Moments, Inspired by Ashley Judd | |April 13, 2012 |DAILY BEAST 

Many of them were delicious in the role; one of them was the embodiment of every womanly grace and charm. The Awakening and Selected Short Stories |Kate Chopin 

He indicated a fair beautiful creature with a determined profile and deep womanly figure. Ancestors |Gertrude Atherton 

But today—after that terrible ordeal, she felt as if life held little for her, that she was now unfit to perform any womanly duty. The Homesteader |Oscar Micheaux 

He smiled with delight when he saw Jess coming down the stairs in her womanly fashion. The Box-Car Children |Gertrude Chandler Warner 

She was riding close beside him—an embodied friendliness—a soft and womanly Chloe, very different from the old. Marriage la mode |Mrs. Humphry Ward