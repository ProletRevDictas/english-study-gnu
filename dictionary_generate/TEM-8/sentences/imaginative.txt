While there, court records say, he developed the idea Judge Liam O’Grady described as “so creative, so imaginative that it had great power and as a result caused such danger to our country.” How a ‘diabolical’ former DEA staffer conned the intelligence community |Rachel Weiner |October 28, 2020 |Washington Post 

Some think the sightings are just imaginative people getting glimpses of giant sturgeon, but we want to believe. Be Very Afraid: A Virtual House of Horrors |Tracy Moran |October 25, 2020 |Ozy 

Ruthie’s life isn’t cushy, and it’s made all the more difficult by quite a few men as she grows up, but her worldview is fierce and imaginative. 3 New Novels Show a Natural World in Peril |Erin Berger |October 3, 2020 |Outside Online 

What we saw doesn't quite match up to a full-fledged Mario Kart game, but it looks like it could add a lot of creativity and imaginative play opportunities to the standard RC car experience. Going in-depth with Nintendo’s augmented reality Mario Kart RC car |Kyle Orland |October 2, 2020 |Ars Technica 

This is especially true for inquisitive, imaginative, and lovable nerds. Beautiful coffee table books for nerds |PopSci Commerce Team |September 30, 2020 |Popular-Science 

They were getting more imaginative,” a pawn shop owner thinks of his addict customers in “Back of Beyond. This Week’s Hot Reads: December 22, 2014 |Mythili Rao |December 22, 2014 |DAILY BEAST 

I also had great optimism that Guillermo would be able to realize these creatures in a unique and imaginative way. Vampires without Glitter or Girl Problems: Inside Guillermo del Toro’s ‘The Strain’ |Andrew Romano |July 14, 2014 |DAILY BEAST 

The food here also is delicious and imaginative, but the ambience is 180-degrees different. Holy Molé: Tucson’s Mexican Food with a Kick |Jane & Michael Stern |June 29, 2014 |DAILY BEAST 

Pretending and imaginative play also flourish, and imaginary friends are common companions to young schoolchildren. Diagnosing Jane, Louis C.K.’s Troubled Daughter on ‘Louie’ Who Can’t Separate Dreams From Reality |Russell Saunders |May 15, 2014 |DAILY BEAST 

Unless Jane becomes more unhinged in coming episodes, she was just an imaginative kid being a kid. Diagnosing Jane, Louis C.K.’s Troubled Daughter on ‘Louie’ Who Can’t Separate Dreams From Reality |Russell Saunders |May 15, 2014 |DAILY BEAST 

Some of them, more imaginative, declared that Mrs. Charmington was even a sleeping partner in the saponaceous firm. The Pit Town Coronet, Volume I (of 3) |Charles James Wills 

A less imaginative man than Aristide would have immediately acquainted the police with his discovery. The Joyous Adventures of Aristide Pujol |William J. Locke 

The brilliant imaginative mind has woven it into romance, making its colors brighter still with the sunlight of inspired phantasy. The Unsolved Riddle of Social Justice |Stephen Leacock 

The idea of the silence and solitude of the cloister inspired the highly-imaginative girl with a blaze of enthusiasm. Madame Roland, Makers of History |John S. C. Abbott 

Darling, don't you see—it's because you aren't a clod, because you're sensitive and imaginative that you experience fear. The Man from Time |Frank Belknap Long