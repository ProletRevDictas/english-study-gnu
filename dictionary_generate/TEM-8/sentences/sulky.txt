The Datto, in a sulky mood, at first refused to come, but on further pressure he changed his mind. The Philippine Islands |John Foreman 

"No, madame," Pauline answered quickly, and there was something almost sulky in her tone. A Butterfly on the Wheel |Cyril Arthur Edward Ranger Gull 

Mr. Blewitt was no match for my master: all the time he was fidgetty, silent, and sulky; on the contry, master was charmin. Memoirs of Mr. Charles J. Yellowplush |William Makepeace Thackeray 

The sight of our money produced lots of things; but our sulky host was quite spoiled by it. The Cradle of Mankind |W.A. Wigram 

After her first startled glance toward Bristow she stood with her head lowered and with an expression of sulky stubbornness. The Winning Clue |James Hay, Jr.