The bell dings, and out bursts another old classmate, jovial, puncturing the moment. What floor? |Sophia Nguyen |February 12, 2021 |Washington Post 

In 1954, she married Gerald Bilodeau, a high school classmate. Marie Mongan, champion of hypnobirthing, dies at 86 |Olesia Plokhii |February 11, 2021 |Washington Post 

When hearing that a male classmate at Harvard University had told Gorman that her writing was “too confident,” Taylor laughed. Amanda Gorman learned the power of poetry early on |Los Angeles Times |February 1, 2021 |Washington Post 

Gilmore said he later became aware that Sniffen’s relationship with his classmate continued after the trip and that over the years he thought about it every time Sniffen’s name was mentioned. Sexual Misconduct Allegations Prompt Another Alaska Attorney General to Resign |by Kyle Hopkins/ Anchorage Daily News |January 31, 2021 |ProPublica 

In Maryland, Melissa George’s middle-schooler has grown increasingly depressed amid the pandemic, she said — unable to focus on school-by-screen, failing classes, missing classmates and friends. Partly hidden by isolation, many of the nation’s schoolchildren struggle with mental health |Donna St. George, Valerie Strauss |January 21, 2021 |Washington Post 

I noticed a picture of her daughter, who was my classmate, and out of curiosity visited her page. 50 Shades of Iran: The Mullahs’ Kinky Fantasies about Sex in the West |IranWire, Shima Sharabi |January 1, 2015 |DAILY BEAST 

In 1950, Serna earned his Ph.D. from the University of Havana, where he had befriended a classmate named Fidel Castro. Obama’s One Hand Clap With Castro |Doug McIntyre |December 24, 2014 |DAILY BEAST 

Another classmate did tell police that she saw Hae at the school at 3:00 p.m . The Scoop on ‘Serial’: Making Sense of The Nisha Call, Asia's Letters, and Our Obsession |Emily Shire |December 11, 2014 |DAILY BEAST 

It began when a classmate raised her hand and stated that she was confused about the facts of the case. Dear White People: Well-Meaning Paternalism Is Still Racist |Chloé Valdary |December 9, 2014 |DAILY BEAST 

His college classmate, Bill Marshall, is a South Florida private eye. Elmore Leonard’s Rocky Road to Fame and Fortune |Mike Lupica |September 13, 2014 |DAILY BEAST 

Bill Chadwick, another classmate of mine, came up from downtown, and met us at the church door. The Idyl of Twin Fires |Walter Prichard Eaton 

Our Margot, the girl who had been my classmate, whom I had loved as a sister. Fifty-Two Stories For Girls |Various 

You know he stopped in Indianapolis to see a classmate who was practising there—met her at a party, I believe, and—good-by Deane! Fidelity |Susan Glaspell 

General Sill was a classmate of mine at the Military Academy, having graduated in 1853. The Memoirs of General P. H. Sheridan, Complete |General Philip Henry Sheridan 

Crook was a classmate of mine—at least, we entered the Military Academy the same year, though he graduated a year ahead of me. The Memoirs of General P. H. Sheridan, Complete |General Philip Henry Sheridan