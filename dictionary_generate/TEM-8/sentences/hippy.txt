The Taliban had tried hard to turn back the centuries in the country, which at one point in the 1970s was a key stop on the hippy trail and a place where some Afghan women wore miniskirts. Afghanistan: What’s at Stake? |Kate Bartlett |June 1, 2021 |Ozy 

Alison, meanwhile, had gone to a yoga retreat with her hippy-dippy mother. What On Earth Is ‘The Affair’ About? Season One’s Baffling Finale |Tim Teeman |December 22, 2014 |DAILY BEAST 

I wandered around aimlessly for a while, then gave the goose to an acquiescent hippy on a barge. The Life and Art of Radical Provocateur—and Commune Leader—Otto Muehl |Anthony Haden-Guest |September 22, 2014 |DAILY BEAST 

Regardless, that goes double for a hippy isle full of expats and pungent pot known as Bocas del Toro. A Man, a Plan, a Canal: Panama Turns 100 |Bill Schulz |August 17, 2014 |DAILY BEAST 

Surely he would have found her efforts to effect world peace through art and music a little impractical, and hippy-dippyish? The Price of Being a Patton: Wrestling With the Legacy of America’s Most Famous General |Tim Teeman |May 26, 2014 |DAILY BEAST 

The Clinton Foundation chose Tulsa, Oklahoma, for the first Too Small to Fail project because it was an early adopter of HIPPY. How a Kids Program Hillary Brought to Arkansas Could Unite Democrats |Eleanor Clift |April 25, 2014 |DAILY BEAST 

Miss Hippy's countenance fell, changed, and again became expressive of doubt—this time offensively. An American Girl in London |Sara Jeannette Duncan 

"I'd rather be in a vault, with the dead, than out here," observed Hippy. Grace Harlowe's Plebe Year at High School |Jessie Graham Flower 

"They are probably mama and papa and the whole family," replied Hippy. Grace Harlowe's Plebe Year at High School |Jessie Graham Flower 

Hippy painted the scenery and David supplied the electric lights. Grace Harlowe's Plebe Year at High School |Jessie Graham Flower 

Washington would not budge, so Hippy led him over to the caller. Grace Harlowe's Overland Riders Among the Kentucky Mountaineers |Jessie Graham Flower