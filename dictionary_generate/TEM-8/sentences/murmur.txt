Used this way, the fabric microphone might listen for murmurs. This new fabric can ‘hear’ sounds or broadcast them |Carolyn Wilke |April 29, 2022 |Science News For Students 

This gun operates at a whisper-soft murmur, so you never have to worry about disturbing those around you, especially if you share a small space. Relax and fight off soreness with these massagers that are on sale |Stack Commerce |October 5, 2021 |Popular-Science 

The game was a pitchers’ duel until, just like that, Pablo Sandoval swung the Atlanta Braves in front and turned Nationals Park into a gallery of light murmurs. Erick Fedde stumbles early, Nats’ bats fizzle late as Braves complete doubleheader sweep |Jesse Dougherty |April 8, 2021 |Washington Post 

It was the low-level murmur of two people who had forgotten to go on mute, followed by giggling. A Latina lawmaker spoke about racism on Zoom. Over giggles, people discussed her accent. |Rebecca Tan |March 3, 2021 |Washington Post 

Selling off the extras, I saw my neighbor marvel at the scent and murmur that he wished he could afford one. A Million Ways to Die in Prison |Daniel Genis |December 8, 2014 |DAILY BEAST 

Then in a kind and soothing murmur he ran over the important points with Vance, who stood like one stunned. Read ‘The King in Yellow,’ the ‘True Detective’ Reference That’s the Key to the Show |Robert W. Chambers |February 20, 2014 |DAILY BEAST 

The crowd on the floor responded with a half-hearted murmur of assent. Egypt’s Government Thugs Beat Me Up at the Rabaa Sit-In |Mike Giglio |August 14, 2013 |DAILY BEAST 

The mix of cooking segments, pop concerts, and celebrity interviews is met with an unappreciative murmur. ‘CBS This Morning’ Success Brings With It a Certain Swagger |David Freedlander |July 22, 2013 |DAILY BEAST 

They occurred without a murmur of protest from the United States. The Gaza Conflict's Winners and Losers |David Frum |November 24, 2012 |DAILY BEAST 

Bernard folded his hands together—almost devoutly—and stood gazing at her with a long, inarticulate murmur of satisfaction. Confidence |Henry James 

Her glance wandered from his face away toward the Gulf, whose sonorous murmur reached her like a loving but imperative entreaty. The Awakening and Selected Short Stories |Kate Chopin 

The leaves were motionless, the river crept past without a murmur, the dark hills rose out of the distant desert like a wave. The Wave |Algernon Blackwood 

At the store he would never have given in, but he was not accustomed to hearing so loud a murmur of approval greet the opposition. The Soldier of the Valley |Nelson Lloyd 

“Akhab Khan prevented those Shia dogs from shooting you and Mayne-sahib,” went on the low murmur. The Red Year |Louis Tracy