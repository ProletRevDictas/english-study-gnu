An EUA signals the FDA has actually vetted a test to some extent. The COVID-19 Charmer: How a Self-Described Felon Convinced Elected Officials to Try to Help Him Profit From the Pandemic |by Vianna Davila, Jeremy Schwartz and Lexi Churchill |September 25, 2020 |ProPublica 

We’ll have to wait and see how well Amazon’s service actually performs via the browser. Believe it or not, the house drone is not Amazon’s most interesting new feature |Aaron Pressman |September 25, 2020 |Fortune 

Though approval doesn’t necessarily mean the plants will actually be built or become operational. China pledges to be carbon neutral—but remains addicted to coal |Naomi Xu Elegant |September 24, 2020 |Fortune 

Still, I had never heard a mayor or city official state openly that aldermen weren’t actually legislators — that is, not until this month. When Is a Meeting Not a Meeting and a Lawmaker Not a Lawmaker? When It’s Lori Lightfoot’s Chicago. |by Mick Dumke |September 24, 2020 |ProPublica 

Get credit card points you’ll actually useI love the feeling of racking up credit card points, then using them on a free flight home to see my family. Save money during the pandemic with these tech-savvy solutions |Whitson Gordon |September 23, 2020 |Popular-Science 

Have you looked around the American Dental Association website for an explanation of how fluoridation actually works? Anti-Fluoriders Are The OG Anti-Vaxxers |Michael Schulson |July 27, 2016 |DAILY BEAST 

And, in the case of fluoride, at least, that doubt might actually be justified. Anti-Fluoriders Are The OG Anti-Vaxxers |Michael Schulson |July 27, 2016 |DAILY BEAST 

The program has not made a final selection on which upgrades will actually be included in future versions of the F-35. Pentagon Misfires in Stealth Jet Scandal |Dave Majumdar |January 8, 2015 |DAILY BEAST 

That would truly be a milestone to celebrate—until you see what that record “diversity” actually means. The Unbearable Whiteness of Congress |Dean Obeidallah |January 8, 2015 |DAILY BEAST 

One wants speech to be free, but one doesn't actually want to hear it. Politicians Only Love Journalists When They're Dead |Luke O’Neil |January 8, 2015 |DAILY BEAST 

It seems very strange that I shall actually know Liszt at last, after hearing of him so many years. Music-Study in Germany |Amy Fay 

She had left her chair, meaning to go indoors and prepare for supper before Tony actually arrived. The Wave |Algernon Blackwood 

On January 10, 1813, came the news that the Prussians had actually gone over to the enemy. Napoleon's Marshals |R. P. Dunn-Pattison 

In the meanwhile, Planner grew actually enamoured of the Pantamorphica Association. Blackwood's Edinburgh Magazine, No. CCCXXXIX. January, 1844. Vol. LV. |Various 

I am in favour of no one paying rates unless he has children actually at a Board School. Punch, or the London Charivari, Volume 107, November 3, 1894 |Various