“Very few people have the capacity to do serial sampling, but in Brazil it’s mandatory to save samples, so we could,” says Sabino. A city in Brazil where covid-19 ran amok may be a ‘sentinel’ for the rest of the world |Amy Nordrum |September 22, 2020 |MIT Technology Review 

In 2018, we saw the takedown of countless serial sexual harassers from positions of power because a number of women were fed up enough to take the risk and break their NDAs. NDAs bear blame for some of the worst corporate cover-ups. How that should change |jakemeth |September 18, 2020 |Fortune 

QuantumScape was founded in 2010 by CEO Jagdeep Singh, a serial entrepreneur who previously founded and sold companies including Lightera Networks, bought by Ciena in 1999 for $1 billion. Bill Gates–backed EV battery startup to go public using Wall Street’s latest buzzy trick |Aaron Pressman |September 3, 2020 |Fortune 

The bottles carrying the swab samples will contain no personal data, either, besides a serial number identifying the patient. Hong Kong’s new mass COVID testing scheme is free and voluntary—and some citizens are suspicious |eamonbarrett |August 26, 2020 |Fortune 

A self-styled hacker and coder, he worked for Microsoft in the early 1990s before becoming a serial entrepreneur. Can an A.I. hedge fund beat the market? |Jeremy Kahn |August 25, 2020 |Fortune 

But when a serial sex predator is playing fanboy, the gag reflex kicks in. Sleazy Billionaire’s Double Life Featured Beach Parties With Stephen Hawking |M.L. Nestel |January 8, 2015 |DAILY BEAST 

Hell, he says Koenig never referred to it as Serial or even as a podcast. The Deal With Serial’s Jay? He’s Pissed Off, Mucks Up Our Timeline |Emily Shire |December 31, 2014 |DAILY BEAST 

In all fairness to Jay, he told The Intercept that he never expected to be a major figure in Serial. The Deal With Serial’s Jay? He’s Pissed Off, Mucks Up Our Timeline |Emily Shire |December 31, 2014 |DAILY BEAST 

Interviews in Serial (including ones from Adnan) do acknowledge that Jay was known as a resident bad boy at Woodlawn High School. The Deal With Serial’s Jay? He’s Pissed Off, Mucks Up Our Timeline |Emily Shire |December 31, 2014 |DAILY BEAST 

But for me, this admittance of uncertainty and doubts grounds Serial in reality. Adnan Killed Her! No, Jay Did It! Serial’s Uncertain, True-to-Reality End |Emily Shire |December 18, 2014 |DAILY BEAST 

Yet it was perfect as regards the paper and printing—even to its black serial number. The Doctor of Pimlico |William Le Queux 

The magazine might pay as much as five hundred dollars for the serial rights—and with that start, they would surely be safe. Love's Pilgrimage |Upton Sinclair 

On the other hand, you can prepare to dispose of the serial rights of the Schooner Farallone: a most grim and gloomy tale. The Works of Robert Louis Stevenson - Swanston Edition Vol. 25 (of 25) |Robert Louis Stevenson 

Whereupon Brodrick inquired with positively formidable politeness, how the new serial was getting on. The Creators |May Sinclair 

This may be styled a serial arrangement of sentences, since in such a case each contributes to the topic only as one in a chain. English: Composition and Literature |W. F. (William Franklin) Webster