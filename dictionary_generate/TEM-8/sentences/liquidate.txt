Those that liquidated, such as Barneys New York, set redemption deadlines for outstanding cards. Americans are on track to buy more gift cards this holiday season than ever before |Rachel King |December 24, 2020 |Fortune 

Bankruptcy experts said Lord & Taylor’s decision to liquidate comes as a warning to other retailers in a similar position. Pandemic bankruptcies: A running list of retailers that have filed for Chapter 11 |Abha Bhattarai |December 4, 2020 |Washington Post 

In a letter to customers, chief executive Geoffroy van Raemdonck stressed that the retailer is not liquidating. Pandemic bankruptcies: A running list of retailers that have filed for Chapter 11 |Abha Bhattarai |December 4, 2020 |Washington Post 

Others, like Stage Stores and Lord & Taylor, have said they will liquidate all stores while searching for a buyer. Pandemic bankruptcies: A running list of retailers that have filed for Chapter 11 |Abha Bhattarai |December 4, 2020 |Washington Post 

“If you’re preventing these retailers from liquidating, that helps the whole industry, but yes, there are competitive concerns about how you make store closure decisions,” Tibone says. America’s Largest Shopping Mall Owner Gets a New Tenant: Itself |Daniel Malloy |August 20, 2020 |Ozy 

“Economic diversity,” by contrast, brings economic difference into higher education in order to liquidate the difference. When Diversity Fails the Poor |Jedediah Purdy |September 28, 2014 |DAILY BEAST 

It is not enough to liquidate the most brutal forms of power and tell people they are now free. Daenerys Goes to Washington: The Modern Politics of ‘Game of Thrones’ |Jedediah Purdy |April 8, 2014 |DAILY BEAST 

When a business runs into this sort of problem, we know what to do: liquidate and sell off the non-performing assets. Saving Detroit: When a Big City Stops Being Big |Megan McArdle |May 13, 2013 |DAILY BEAST 

As workers went on strike and the company threatened to liquidate, Hostess was essentially crippled. Twinkies Are Coming Back: The Metropoulos Brothers on the Brand |Daniel Gross |April 8, 2013 |DAILY BEAST 

Stockman both predicts imminent raging inflation and urges investors to liquidate their investors and hide in cash. When It's Time to Stop Talking About Politics |David Frum |March 31, 2013 |DAILY BEAST 

In due time, as Crothers did not liquidate, the firm became possessed of this tract. Blazed Trail Stories |Stewart Edward White 

The thing they all most fear is that some one will "start a run on the bank," force it to liquidate, and everyone will lose. The Behavior of Crowds |Everett Dean Martin 

It is believed entirely practicable to liquidate the entire debt in seventeen years from the first payment. Harper's New Monthly Magazine, No. IX.--February, 1851.--Vol. II. |Various 

The United States will liquidate every debt at the command of its honor, and every cent will be paid. The Works of Robert G. Ingersoll, Vol. 9 (of 12) |Robert G. Ingersoll 

As soon as I can I will liquidate my indebtedness to you, and meanwhile I remain, etc. The Life of Ludwig van Beethoven, Volume III (of 3) |Alexander Wheelock Thayer