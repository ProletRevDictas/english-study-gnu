Earlier this year, TMZ reported that storage units owned by Beyoncé's production company, Parkwood Entertainment, were raided by burglars who made off with an estimated $1 million in goods. Fire at mansion believed to be owned by Beyoncé and Jay-Z investigated as possible arson |Jennifer Hassan |July 23, 2021 |Washington Post 

In 2006, Martha Milete was shot in the chest by burglars in her Detroit home. A bioethicist warns that informed consent is endangered |Karen Tucker |March 12, 2021 |Washington Post 

Police once used WD-40 to extricate a naked burglar who had become wedged into ductwork. 20 ingenious uses for WD-40 |By Bill Heavey/Field & Stream |February 11, 2021 |Popular-Science 

Posing as an undercover detective, he persuades an elderly woman to give him her most precious valuables to thwart a supposed burglar. ‘Lupin’ is a thrilling heist series, but it goes deeper than that |Bethonie Butler |January 21, 2021 |Washington Post 

The burglar alarm to keep his parents out of his messy bedroom wasn’t his only invention. Honey, I Grew the Economy (Ep. 399) |Stephen J. Dubner |December 5, 2019 |Freakonomics 

The night Tre arrives, Furious—a Vietnam vet—shoots at a burglar. The 13 Coolest Movie Dads: ‘Taken,’ ‘Star Wars,’ ‘Die Hard,’ and More |Marlow Stern |June 15, 2014 |DAILY BEAST 

She recently caught a would-be burglar in the garage of their new home and chased him out into the street. Can ‘the Traitor’ Jesse Benton Unite the GOP? |Sam Youngman |March 28, 2014 |DAILY BEAST 

He was caught after setting off burglar alarms in the palace. Security Farce At Palace As Prince Andrew Stopped By Police After Break-In |Tom Sykes |September 9, 2013 |DAILY BEAST 

It is the burglar who bears the culpability for walking through an unlocked door, not the homeowner. How Social Scientists, and the Rest of Us, Got Seduced By a Good Story |Megan McArdle |April 30, 2013 |DAILY BEAST 

Police have denied earlier reports that Pistorius mistook his girlfriend for a burglar. Blade Runner’s Beauty Queen: Who Was Reeva Steenkamp? |Lizzie Crocker |February 14, 2013 |DAILY BEAST 

No burglar ever brags of his exploits; the poacher always boasts, and always receives applause. The Chequers |James Runciman 

The real fact is that Mr. Parson's father was a burglar of the fine old school. The Pit Town Coronet, Volume II (of 3) |Charles James Wills 

The burglar entered the room without noise, and the heavy breathing of the sleeper continued without intermission. The Pit Town Coronet, Volume II (of 3) |Charles James Wills 

Next came Coronado, as happy as a disappointed burglar whose cue it is to congratulate the rescuing policeman. Overland |John William De Forest 

As for the gentleman at the door he was encumbered with his hurt companion, who fell across his knees as he rushed at the burglar. It Is Never Too Late to Mend |Charles Reade