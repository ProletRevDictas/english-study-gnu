She need not sacrifice her whole identity to her children unless that is squarely her choice. It’s time to end the TV trope of the needy empty-nest mom |Michele Weldon |February 2, 2021 |Washington Post 

Crisis management is squarely in our wheelhouse as a company that handles real time escalations all day every day. The Business of Managing a DC Business During a Coup |Lisa Wise |January 15, 2021 |Washington Blade 

Congress must immediately transfer command of the District of Columbia National Guard from the President of the United States and put it squarely under the command and control of the District of Columbia. Arguments for D.C. statehood grow stronger following Capitol attack and plans for inauguration violence |Aric Jenkins |January 12, 2021 |Fortune 

If you keep phyllo on hand, and I do, this recipe falls squarely in cook-with-what-you-have category. Wrap leftover roasted root vegetables in phyllo for a quick, crispy galette |Ann Maloney |January 12, 2021 |Washington Post 

She climbed up and had a whispered conversation with Santa, sitting squarely at the intersection where magic and reality meet. When my daughter asked if Santa was real, my answer was yes |Vanessa McGrady |December 17, 2020 |Washington Post 

A worn couch sitting squarely before a wood veneer wall, accented by the head of a deer. #Setinthestreet: Your Street Corner Is Their Art Project |James Joiner |December 24, 2014 |DAILY BEAST 

The winner will be the one who most squarely gets on the side of the working class. Bush, Christie, Romney: Who’ll Be the GOP Class Warrior? |Lloyd Green |December 15, 2014 |DAILY BEAST 

All of their testimony has been accepted by the court, and most of it casts blame squarely on their captain. The Costa Concordia’s Randy Reckless Captain Takes the Stand |Barbie Latza Nadeau |December 2, 2014 |DAILY BEAST 

But one sequence in particular has divided audiences squarely in half. Christopher Nolan Explains Interstellar’s Big, Hotly Debated Twist |Marlow Stern |November 19, 2014 |DAILY BEAST 

“We tried to be evenhanded, and Poitras is squarely on the side of Snowden,” Condon says. Can Condon's Freak Show Win Broadway? |Tim Teeman |November 18, 2014 |DAILY BEAST 

Lawrence fired, but just as he did so Jerry's horse threw up his head and the ball struck him squarely between the eyes. The Courier of the Ozarks |Byron A. Dunn 

"'Scuse me," Jake said, pulling a shawl more squarely around her shoulders and straightening her up. The Cromptons |Mary J. Holmes 

The coachman gathers up his reins and shakes squarely down into his seat; the ostlers step back. The Portsmouth Road and Its Tributaries |Charles G. Harper 

He wore a gray snap-brim hat; it was set squarely on his head, precisely level. Hooded Detective, Volume III No. 2, January, 1942 |Various 

And so they were caught fairly and squarely by the deluge that swept upon them with a bewildering suddenness. The Outdoor Girls in the Saddle |Laura Lee Hope