An unusual mud-wrapped mummy is leading archaeologists to rethink how nonroyal Egyptians preserved their dead. An ancient Egyptian mummy was wrapped in an unusual mud shell |Maria Temming |February 3, 2021 |Science News 

As archaeologists, these scientists study human history and prehistory. Harsh Ice Age winters may have helped turn wolves into dogs |Bruce Bower |February 3, 2021 |Science News For Students 

Their skeletons, their genomes, and the isotopic signatures of their diets all look so much like wolves that archaeologists can’t reliably say who’s who. Dogs have been our best friends for at least 23,000 years |Kiona N. Smith |February 1, 2021 |Ars Technica 

The specimen marks a technological turn to manipulating objects with wide, flat stone surfaces, say Ron Shimelmitz, an archaeologist at the University of Haifa in Israel, and his colleagues. The oldest known abrading tool was used around 350,000 years ago |Bruce Bower |January 21, 2021 |Science News 

Some archaeologists, however, doubt that the artifacts are even stone tools and say they instead are just naturally broken rocks. These science claims from 2020 could be big news if confirmed |Cassie Martin |December 23, 2020 |Science News 

The archaeologist Sarah Nelson is in her eighties, and she would go dig in China this minute if she could get grant money. The Real-Life Raiders of the Lost Ark |Alex Belth |November 14, 2014 |DAILY BEAST 

It depends on the archaeologist and the circumstances, but I think they would all take their sweet time if they could. The Real-Life Raiders of the Lost Ark |Alex Belth |November 14, 2014 |DAILY BEAST 

Did you ever want to be an archaeologist when you were a kid? The Real-Life Raiders of the Lost Ark |Alex Belth |November 14, 2014 |DAILY BEAST 

Laurie Rush, the military archaeologist, admitted, “Our most exciting days are the days we discover we were wrong.” The Real-Life Raiders of the Lost Ark |Alex Belth |November 14, 2014 |DAILY BEAST 

The now-novelist grew up wanting to be an archaeologist without knowing what that meant. Sarah Waters: Queen of the Tortured Lesbian Romance |Tim Teeman |September 30, 2014 |DAILY BEAST 

An old fellow more archaeologist than judge, who found delight in the petty squabbles under his eyes. Repertory Of The Comedie Humaine, Complete, A -- Z |Anatole Cerfberr and Jules Franois Christophe 

This notable archaeologist began his career in the East as an officer in the Bombay army. Myths of Babylonia and Assyria |Donald A. Mackenzie 

Near her sat a much less remarkable person—Thomas Grealy, historian and archaeologist. Hyacinth |George A. Birmingham 

Generally speaking, the position of a European archaeologist in India is very sad. From the Caves and Jungles of Hindostan |Helena Pretrovna Blavatsky 

But I think perhaps Corhampton church is of more interest to the archaeologist than to the average tourist. A Leisurely Tour in England |James John Hissey