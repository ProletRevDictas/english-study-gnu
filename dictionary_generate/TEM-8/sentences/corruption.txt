Except, well, those countries are not safe — corruption, crime, violence, and lack of economic opportunity have driven hundreds of thousands to flee the countries in recent years. What to make of the DHS whistleblower’s shocking complaint |Alex Ward |September 11, 2020 |Vox 

When we asked Brian Kolfage last month to explain how his group We Build the Wall had spent the $25 million it had raised, plus address concerns of corruption when the private sector takes over the building of border walls, he scoffed. Private Border Wall Fundraisers Have Been Arrested on Fraud Charges |by Perla Trevizo, Jeremy Schwartz and Lexi Churchill |August 20, 2020 |ProPublica 

That, again, is Jason Robins from DraftKings, talking about corruption in sports. The Economics of Sports Gambling (Ep. 388 Rebroadcast) |Stephen J. Dubner |August 20, 2020 |Freakonomics 

The soldiers arrested the president following months of mass protests against corruption and escalating insecurity in Mali, where Islamic militants have been active since 2012. How Mali’s security crisis and corruption allegations brought the military back to power |Chidinma Irene Nwoye |August 19, 2020 |Quartz 

When people repeatedly do things that aren’t allowed—from jaywalking to engaging in business corruption—their social-credit score falls and they can be blocked from things like buying train and plane tickets or applying for a mortgage. Inside China’s unexpected quest to protect data privacy |Tate Ryan-Mosley |August 19, 2020 |MIT Technology Review 

She is the author of Dirty Entanglements: Corruption, Crime and Terrorism (Cambridge University Press). ISIS, Boko Haram, and the Growing Role of Human Trafficking in 21st Century Terrorism |Louise I. Shelley |December 26, 2014 |DAILY BEAST 

Islamic State brought “peace, autonomy, zero corruption, low crime-rate,” he Tweeted last month. The Scared Widdle Kitty of ISIS |Jacob Siegel |December 12, 2014 |DAILY BEAST 

The stench of corruption is settling over world soccer like a poisonous fog, and players are paying the price. Is Soccer Great Lionel Messi Corrupt? |Jack Holmes |December 8, 2014 |DAILY BEAST 

Both Rezko and Blagojevich have since been convicted on unrelated federal corruption charges. Obama’s Golf Buddy May Be a ‘Hostile Witness’ in Chicago Corruption Case |Ben Jacobs |December 3, 2014 |DAILY BEAST 

The ruling also cleared Mubarak along with his sons, Alaa and Gamal, of corruption charges relating to the sale of gas to Israel. Mubarak’s Acquittal Signals Complete Triumph of Military Over Arab Spring |Jamie Dettmer |November 29, 2014 |DAILY BEAST 

The mangled bodies were hurried to the catacombs, and thrown into an indiscriminate heap of corruption. Madame Roland, Makers of History |John S. C. Abbott 

One of them was the late Secretary of the Treasury, Guy, who had been turned out of his place for corruption. The History of England from the Accession of James II. |Thomas Babington Macaulay 

In Castile was ostentatiously displayed and lavishly spent great fortunes made in remote provinces by oppression and corruption. The History of England from the Accession of James II. |Thomas Babington Macaulay 

Its origin is involved in obscurity: but may it not be a corruption of the Latin ambages, or the singular ablative ambage? Notes and Queries, Number 194, July 16, 1853 |Various 

Amid the disintegration of society it was the sole conservative element—the salt which preserved it from corruption. The Catacombs of Rome |William Henry Withrow