These unique cookies are slightly chewy, with a deep, earthy flavor from the rye — and then you get a burst of fresh tart fruit that makes you want to take another bite. Raspberry and rye work wonders together in this tangy, complex cookie |Sarah Kieffer |December 2, 2020 |Washington Post 

The candymaker receives heaps of fan theories every day—from tart lemon to smooth vanilla—but no one has gotten it right. How mystery flavors confuse our taste buds |Stan Horaczek |October 16, 2020 |Popular-Science 

They had delicious planked steak and rhubarb tart in Salt Lake City, but bad fried chicken and awful pie in Winnemucca, Nevada, was the beginning of a sad coda to their journey. To Find Hope in American Cooking, James Beard Looked to the West Coast |John Birdsall |October 2, 2020 |Eater 

Combined with organic ashwagandha, ginger, and tart cherry for an even bigger anti-inflammatory kick, this daily supplement will help you feel better and recover faster. The Most Futuristic Workout Gear of 2020 |Hayden Carpenter |September 5, 2020 |Outside Online 

Native bees also boost tart cherries and blueberries and dominate pumpkins. Wild bees add about $1.5 billion to yields for just six U.S. crops |Susan Milius |August 4, 2020 |Science News 

Dessert is a slice of melt-in-your-mouth treacle tart with a dollop of perfectly tart clotted cream. Join The Mile High (Dining) Club |Allison McNearney |September 26, 2014 |DAILY BEAST 

She is routinely dismissed by Madrid wits as “a Danish tart.” Will Scandal Sink the Spanish Royal Family? |Tom Sykes |August 18, 2014 |DAILY BEAST 

It was a candy-colored teen comedy that cut the usual sugar-sweetness with tart dialogue and sharp writing. 'Glee' 100th Episode: The Sad Ballad of an Elderly Trainwreck |Kevin Fallon |March 19, 2014 |DAILY BEAST 

In schnaps, however, Subirer aromas are transformed into those of a caramelized pear tart, buttery, baked, and entirely pleasant. What to Drink When it’s Cold? The Glory of Austrian Schnaps |Jordan Salcito |January 25, 2014 |DAILY BEAST 

They taste of red and black berries, currants, cranberries, strawberries, mulberries and cherries, tart and sweet. The Drink All You Want Holiday Wine |Jordan Salcito |December 21, 2013 |DAILY BEAST 

Well, for once Mrs. Robin almost said something tart to the old gentleman. The Tale of Grandfather Mole |Arthur Scott Bailey 

After this, knobs of cheese are handed round on a plate, and there is a talk of a tart somewhere at some end of the table. Little Travels and Roadside Sketches |William Makepeace Thackeray 

Next a great dish of roasted fowl, cost me about 30s., and a tart, and then fruit and cheese. Diary of Samuel Pepys, Complete |Samuel Pepys 

The peach tart was a form of pie with golden-looking sauce peeping up between crisscross strips of rich puff paste. Maid Sally |Harriet A. Cheever 

When Sweep learned that, his kindly heart was touched; he gave Little Sweep the whole plum cake and kept but one tart for himself. The Green Forest Fairy Book |Loretta Ellen Brady