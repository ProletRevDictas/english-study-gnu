His mother, Helen, was a teacher, journalist and novelist, who found acclaim for feminist works like Evbu My Love, published as part of Macmillan’s iconic Pacesetter series. The Potential Oscar Film That’s Too Hot for the Nigerian Government |Daniel Malloy |December 16, 2020 |Ozy 

Just as she promised when announcing this music’s existence on Thursday morning, Swift’s “Evermore” is the aesthetic sequel to “Folklore,” an album released after a 16-hour heads-up in late July to resounding acclaim. Taylor Swift really can’t help herself |Chris Richards |December 11, 2020 |Washington Post 

Clubhouse, an audio-first social-media app used by many with Silicon Valley ties, was launched to critical acclaim earlier this year, only to devolve into the type of misogynistic vitriol that has seeped into every corner of the internet. A new social-media platform wants to enforce “kindness.” Can that ever work? |Tanya Basu |October 7, 2020 |MIT Technology Review 

In the meantime, BMW introduced its six-cylinder K 1600 touring bikes to worldwide acclaim and has enjoyed a sharply upward trajectory in sales and profits. 22 of the weirdest concept motorcycles ever made |By John Burns/Cycle World |September 10, 2020 |Popular-Science 

The Corvette team is familiar with delighting customers and critics alike, having launched the mid-engine Corvette to world acclaim and becoming one of the most awarded cars in automotive history. GM shifts Corvette engineering team to its electric and autonomous vehicle programs |Kirsten Korosec |August 28, 2020 |TechCrunch 

Despite the acclaim and the viral popularity, the band has never lost that independant creative spirit. OK Go Is Helping Redefine the Music Video For the Internet Age |Lauren Schwartzberg |December 15, 2014 |DAILY BEAST 

Yet, the ever-visionary Van Gogh still feels the possibility of acclaim after his imminent death. Decoding Vincent Van Gogh’s Tempestuous, Fragile Mind |Nick Mafi |December 7, 2014 |DAILY BEAST 

They may not receive public acclaim, but their pride in their work is as intense as their labors. Damien Hirst’s Army of Geppettos |Tim Teeman |December 2, 2014 |DAILY BEAST 

But the acclaim for The Spy had been so great that I was in for a hiding anyway, and knew it. The Stacks: How The Berlin Wall Inspired John le Carré’s First Masterpiece |John le Carré |November 8, 2014 |DAILY BEAST 

The show ran for five seasons, earning both popularity and acclaim in the process. The Childish Genius of Pee-wee’s Playhouse |Keith Phipps |October 23, 2014 |DAILY BEAST 

In the meantime, amid feasts and clamorous acclaim, Gent came slowly north with his staff of secretaries. The Red City |S. Weir Mitchell 

You would have all—the love of my wife, the rule of my folk, as well as the acclaim of these city swine. Joan of the Sword Hand |S(amuel) R(utherford) Crockett 

The Cæsar was a fugitive and a coward, and the people who had the upper hand were prepared to acclaim the hero of their choice. "Unto Caesar" |Baroness Emmuska Orczy 

Many were too full for utterance; they broke down in tears with their first attempt to join in the general acclaim. From Farm House to the White House |William M. Thayer 

Then all the knights of Cornwall gave loud acclaim that their knight had borne himself so well in those encounters. The Story of the Champions of the Round Table |Howard Pyle