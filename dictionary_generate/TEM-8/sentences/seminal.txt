The fall of 2014 proves a powerfully seminal time for Wolfe. ‘Catastrophist’ offers lessons from a pre-COVID plague |Patrick Folliard |February 5, 2021 |Washington Blade 

In one seminal study, a team from Stanford used lasers to measure brain activity from two people playing a collaboration game side by side. How Does Social Interaction Change Our Brains? Hyperscans Can Show Us |Shelly Fan |December 8, 2020 |Singularity Hub 

It turned out to be a seminal moment, not only for Fortune, but for the development of stakeholder capitalism. Towards a capitalism that better serves society |Alan Murray |December 8, 2020 |Fortune 

For those like Wedbush Securities’s Dan Ives, the big earnings beats are “another seminal moment in this cloud transformation underway and put more fuel into this rally in cyber security names across the board,” Ives wrote in a note Thursday. On cloud nine: Why these cloud security stocks are soaring |Anne Sraders |December 3, 2020 |Fortune 

It represents a seminal moment in the history of the internet. Dfinity’s valuation soars to $9.5Bn after revealing its governance system and token economics |Mike Butcher |September 30, 2020 |TechCrunch 

In the long sweep of LGBT equality, it could have stood as a seminal moment. Is Gay Marriage Going Away in 2016? |David Freedlander |December 4, 2014 |DAILY BEAST 

I had some real seminal experiences in [California] … I was a weird kid and it was a weird place and it just fit. The Gray Lady Gets an Artistic Makeover |Justin Jones |May 2, 2014 |DAILY BEAST 

Capital in the Twenty-First Century is already being hailed as a seminal work of economic thought, and with very good reason. This Week’s Hot Reads: April 21, 2014 |Thomas Flynn |April 22, 2014 |DAILY BEAST 

His Seven Pillars of Wisdom is one of the seminal war memoirs of this or any time. Lawrence of Arabia Became Popular as the Dashing Antithesis of the War in Europe |Jack Schwartz |December 21, 2013 |DAILY BEAST 

[the seminal pulp crime magazine which first published Hammett]. The Man With Stories to Tell |Allen Barra |December 8, 2013 |DAILY BEAST 

Without her aid, this seminal principle of mischief, this root of Upas, could not have been planted. Select Speeches of Daniel Webster |Daniel Webster 

Thereupon his anxiety became extreme, and simultaneously he experienced his first seminal emission. The Sexual Life of the Child |Albert Moll 

On one occasion, however, he had a seminal emission during the night in association with a feeling of anxiety. The Sexual Life of the Child |Albert Moll 

In his seminal dreams, the image of the rose always played a leading part. The Sexual Life of the Child |Albert Moll 

We may also, in this connexion, think of the seminal emissions sometimes observed in cases of suicidal hanging. The Sexual Life of the Child |Albert Moll