I generally prefer synthetic leather ear cup padding, as it’s less scratchy, but the fabric doesn’t only start to feel irksome when you’re sitting in a hot room, which is better than most. SteelSeries Arctis 9 Wireless gaming headset review: The Whole 9 Yards |Mike Epstein |November 18, 2021 |Popular-Science 

The idea that planets have to clear their orbits is particularly irksome, he says. The definition of planet is still a sore point – especially among Pluto fans |Lisa Grossman |August 24, 2021 |Science News 

That irksome window you can’t open because it has a ripped screen. How to tackle the home maintenance to-do list you ignored during the pandemic |Jura Koncius |June 24, 2021 |Washington Post 

However, there’s a difference between constructive complaining and irksome needling. Am I Having Sex With a Serial Killer? |Eugene Robinson |March 16, 2021 |Ozy 

As a male human resources leader, I find this statement irksome. The need for “women supporting women” shows the problem with the “boys’ club” in Indian offices |Vikas Bansal |March 8, 2021 |Quartz 

Her borderline apologetic view is, frankly, a bit irksome, though not wholly unexpected based on Transmormon. Thank God! To the Church, This Transgender Woman Is Just a Skank |Emily Shire |October 22, 2014 |DAILY BEAST 

But there is actually something more irksome than exploiting cancer for profits. The Misogynistic Companies Jumping On The Breast Cancer Bandwagon |Emily Shire |October 16, 2014 |DAILY BEAST 

For more than decade, flying has been made irksome rather than pleasurable by an ever-increasing fortress culture at airports. Twin Disasters Turn 2014 Into the Year of Flying Dangerously |Clive Irving |July 19, 2014 |DAILY BEAST 

For most New Yorkers, there are few things as irksome as strangers accosting you on the street. How ‘Billy on the Street’ Host Billy Eichner Hit the Mainstream |Kevin Fallon |February 12, 2013 |DAILY BEAST 

Even more irksome than the aphorisms is the obviousness of the advice. Do We Need a Guide to Marriage? |Chloë Schama |January 8, 2011 |DAILY BEAST 

They had no power of attention even to a story, and the stillness was irksome to such wild colts. The Daisy Chain |Charlotte Yonge 

They are as impertinent as those people who stop you only to bore you; but the former are perhaps less irksome. The 'Characters' of Jean de La Bruyre |Jean de La Bruyre 

There is a boundary even to human patience; and now, after many days, Max Bray began to find his position very irksome. By Birth a Lady |George Manville Fenn 

Their harness is not apparently irksome to them, and is not so heavy as one sees on the Portuguese oxen, for instance. Spanish Life in Town and Country |L. Higgin and Eugne E. Street 

It then grew very irksome to him to bear his irons, and he rarely went out to walk. My Ten Years' Imprisonment |Silvio Pellico