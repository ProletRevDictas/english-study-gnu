To economize — and to allow Robert the free time to write — they’ve moved their family to Berlin. ‘A Lonely Man’ is an elegant suspense novel in the tradition of the ‘The Third Man’ |Maureen Corrigan |May 13, 2021 |Washington Post 

But really—telling Ginger to give away the family dog in order to economize? Dogs and Presidential Candidates: Man's Best Friend Dominates the Race |Leslie Bennetts |December 18, 2011 |DAILY BEAST 

He would haggle in a bargain for a shilling, and economize in things beneath a wise man's notice or consideration. Blackwood's Edinburgh Magazine, No. CCCXXXIX. January, 1844. Vol. LV. |Various 

Being naturally anxious to economize the small funds I can devote to science, the request appeared to me a reasonable one. Decline of Science in England |Charles Babbage 

Even the poor who cannot have electricity or gas hardly need economize here with kerosene at its present rates. Girls and Women |Harriet E. Paine (AKA E. Chester} 

Now I have often noticed that women who are compelled to economize in little things are inclined to economize in all things. Girls and Women |Harriet E. Paine (AKA E. Chester} 

They economize in the family expenditure; they employ few or no servants, and do plain sewing, dressmaking, and millinery. The Education of American Girls |Anna Callender Brackett