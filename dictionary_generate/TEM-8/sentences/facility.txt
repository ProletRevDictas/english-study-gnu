If the rapid response test comes back positive and the PCR test result hasn’t been returned yet, the student would be put into an isolation facility, according to a university spokesperson. Even the most cautious schools are seeing outbreaks |Sy Mukherjee |September 17, 2020 |Fortune 

It’s as urgent as getting these manufacturing facilities up. Top health official says states need about $6 billion from Congress to distribute coronavirus vaccine |Lena H. Sun |September 16, 2020 |Washington Post 

The event featured maskless supporters standing shoulder-to-shoulder inside the industrial facility. Trump contradicts CDC director on vaccine; Biden says Americans shouldn’t trust Trump |Colby Itkowitz, Felicia Sonmez, John Wagner |September 16, 2020 |Washington Post 

It says 495 of the hospitals and other facilities participating in the survey earned a top score of 100 on their LGBTQ-related policies and practices. HRC examines hospital policies, impact of COVID on LGBTQ people |Lou Chibbaro Jr. |September 16, 2020 |Washington Blade 

Next up, Corning is trying 5G indoors at some of its own facilities. Verizon plans to offer indoor 5G networks by year-end |Aaron Pressman |September 16, 2020 |Fortune 

They put them in key positions within the facility where they can look out for their own. The Mexican Mafia Is the Daddy of All Street Gangs |Seth Ferranti |December 11, 2014 |DAILY BEAST 

He watched the pit grow bigger every month, despite the numerous reports he wrote about the facility. Two Texas Regulators Tried to Enforce the Rules. They Were Fired. |David Hasemyer, InsideClimate News |December 9, 2014 |DAILY BEAST 

After the federal investigation concluded, they authorized him to close the facility. Two Texas Regulators Tried to Enforce the Rules. They Were Fired. |David Hasemyer, InsideClimate News |December 9, 2014 |DAILY BEAST 

Opened in Sept. 2002, this “poorly managed” detention facility was the second site opened by the CIA after the 9/11 attacks. The Most Gruesome Moments in the CIA ‘Torture Report’ |Shane Harris, Tim Mak |December 9, 2014 |DAILY BEAST 

Kocurek became especially frustrated with a commercial waste facility in Jim Wells County. Two Texas Regulators Tried to Enforce the Rules. They Were Fired. |David Hasemyer, InsideClimate News |December 9, 2014 |DAILY BEAST 

We must have motif first, then technique to adapt and adjust expression and to develop facility in the active agents. Expressive Voice Culture |Jessie Eldridge Southwick 

The object of this practise is to attain facility in manipulating the elements while maintaining the smooth quality of the tone. Expressive Voice Culture |Jessie Eldridge Southwick 

The facility of manufacture and cheapness of those engines caused them to be much used in the mines, and also elsewhere. Life of Richard Trevithick, Volume II (of 2) |Francis Trevithick 

His letter of a few months before reveals the facility with which he moulded the steam-engine to his requirements. Life of Richard Trevithick, Volume II (of 2) |Francis Trevithick 

The fibre can be separated with great facility, though firmly attached at one end to the parent rock. Asbestos |Robert H. Jones