That action has scuppered some of the IAB-backed group’s plans. Activists are using ads to sneak real news to Russians about Ukraine |Chris Stokel-Walker |March 4, 2022 |MIT Technology Review 

Discussions to make testing free were scuppered in order to push people to book vaccines. Europe’s Brutal Fourth Wave Shows We’re Just Not Ready for Normal |Barbie Latza Nadeau |July 23, 2021 |The Daily Beast 

Supply issues could scupper recent gains in New Zealand and Australia. In the race between variants and vaccines, rich Asia-Pacific countries finally start their sprint |Michael E. Miller |June 4, 2021 |Washington Post 

As the equality movement found a renewed focus and determination, so its opponents ratcheted up their efforts to scupper it. How Robin Williams’ Mrs. Doubtfire Won the Culture Wars |Tim Teeman |August 13, 2014 |DAILY BEAST 

The minister fears that now even lesser frictions could scupper the new agreement. Taliban Slams Loya Jirga Bilateral Security Agreement |Ron Moreau & Sami Yousafzai |November 26, 2013 |DAILY BEAST 

The combers were crashing over the weather rail in solid cascades, and the scupper-ports were not large enough to carry it off. The Viking Blood |Frederick William Wallace 

Down from the forecastle roof tumbled Jack Cockrell and went sliding across the deck, heels over head, to fetch up in the scupper. Blackbeard: Buccaneer |Ralph D. Paine 

At that moment a tremendous sea struck the vessel, carrying the mate and myself into the lee scupper. Torrey's Narrative |William Torrey 

Stopping the scupper, the rolling of the vessel would wash the water and sand from one side to the other. Torrey's Narrative |William Torrey 

The rods had a sharp scupper on the outside of the big end so placed as also to throw the oil on this same thrust face. The Wright Brothers' Engines and Their Design |Leonard S. Hobbs.