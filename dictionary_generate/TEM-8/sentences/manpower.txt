We have already made significant security upgrades to include establishing a physical structure and increasing manpower to ensure the protection of Congress, the public and our police officers. House scraps plans for Thursday session after security officials warn of possible plot to breach Capitol |Felicia Sonmez, Colby Itkowitz, John Wagner |March 4, 2021 |Washington Post 

“We have already made significant security upgrades to include establishing a physical structure and increasing manpower to ensure the protection of Congress, the public and our police officers,” police said in the statement. Capitol Police say intelligence shows militia group may be plotting to breach the Capitol |Tom Jackman, Matt Zapotosky, Michael Brice-Saddler, Craig Timberg |March 4, 2021 |Washington Post 

Very few people who breached the Capitol were arrested, and one law enforcement official said the reason was simply limited manpower. Woman dies after shooting in U.S. Capitol; D.C. National Guard activated after mob breaches building |Washington Post Staff |January 7, 2021 |Washington Post 

That let tribes grow larger, and larger tribes meant more heads to innovate and remember ideas, more manpower, and better ability to specialize. When Did We Become Fully Human? What Fossils and DNA Tell Us About the Evolution of Modern Intelligence |Nick Longrich |October 18, 2020 |Singularity Hub 

Africa wasn’t just deprived of lost manpower and income, but also creativity, innovation, and relationships. What is owed Africa |Lynsey Chutel |October 13, 2020 |Quartz 

On the one hand, they are genuinely powerful, and Democrats rely on their money and manpower during elections. How Public Sector Unions Divide the Democrats |Daniel DiSalvo |December 29, 2014 |DAILY BEAST 

Nonviolent subjects were easier to rule and more likely to provide the revenue and manpower that would enable further conquest. War! What Is It Good For? A Lot |Nick Romeo |August 13, 2014 |DAILY BEAST 

And the tribes have the manpower and the will to fight, but they lack weapons to confront ISIS. Lessons From Fallujah, Then and Now |Dr. Daniel R. Green |July 26, 2014 |DAILY BEAST 

“That is why we have more manpower this year,” commander Qari Talha told The Daily Beast over his mobile phone. Kabul Airport Attack Comes as Pakistani Fighters Join Afghan Taliban |Sami Yousafzai |July 17, 2014 |DAILY BEAST 

Given the reality of their respective militaries looking at diminished sources of manpower, this is essential. Israel Needs Better War Technology |Lloyd Green |July 7, 2014 |DAILY BEAST 

Manpower is thus seen to be a direct function of leadership. Manpower |Lincoln Clarke Andrews 

During the month of February the drain upon the manpower of the British Empire caused by the war made itself apparent. The Seventh Manchesters |S. J. Wilson 

They test almost to breaking the endurance of man, but in the end superior manpower emerges the victor. Manpower |Lincoln Clarke Andrews 

The computer-independent bus system has not been expensive in manpower. On-Line Data-Acquisition Systems in Nuclear Physics, 1969 |H. W. Fulbright et al. 

The bill calls for the conscription of manpower for the work and whatever materials may be necessary, without compensation. Greener Than You Think |Ward Moore