So, they were still going through a lot of political struggle, and I volunteered in a township in a children’s hospital there. Actress, Mother, Activist Alyssa Milano on Life as a Triple Threat |Esabelle Lee |February 11, 2021 |Ozy 

Never mind that Soweto, a township near Johannesburg, is nearly 900 miles from Robben — not Robbens — Island, which is off the coast of Cape Town. The biggest Pinocchios of 2020 |Glenn Kessler |December 18, 2020 |Washington Post 

It was signed by a developer who stood to gain from zoning changes Karcher opposed on the township council. How Criminal Cops Often Avoid Jail |by Andrew Ford, Asbury Park Press |September 23, 2020 |ProPublica 

Pension records indicate two of them are still employed in other roles with their township. How Criminal Cops Often Avoid Jail |by Andrew Ford, Asbury Park Press |September 23, 2020 |ProPublica 

Building an insurer “for the townships, villages and informal settlements” became a passion project. An Insurance Solution for the World’s Poorest |Daniel Malloy |September 21, 2020 |Ozy 

You stand on an unsteady pontoon bridge spanning the Tigris River in a township called Adh Dhouloueya. Whatever You Do Someone Will Die. A Short Story About Impossible Choices in Iraq |Nathan Bradley Bethea |August 31, 2014 |DAILY BEAST 

And now this quiet township had also become the home of an ugly mosque controversy. When Bigotry Comes to Your Hometown |Dean Obeidallah |July 11, 2014 |DAILY BEAST 

In February 2011, during a winter storm, a tree fell into a creek in Franklin Township, New Jersey, and caused flooding. Government Has Made America Inept |Philip K. Howard |May 4, 2014 |DAILY BEAST 

The people of Fairview Township, Pa. can finally sleep at night knowing the Ku Klux Klan is watching over them. Fringe Factor: Boy Scouts Kick Out Gay Leader’s Entire Troop |Caitlin Dickson |April 27, 2014 |DAILY BEAST 

Mormons first came to England in 1837, just seven years after Joseph Smith founded the Church in Fayette Township, New York. Britain Puts Mormonism on Trial |Naomi Zeveloff |February 8, 2014 |DAILY BEAST 

Brockton was a part of Bridgewater until 1821, when it was incorporated as the township of North Bridgewater. Encyclopaedia Britannica, 11th Edition, Volume 4, Part 3 |Various 

But Mort knows just how many voters there are in every township and just how they line up election morning. A Hoosier Chronicle |Meredith Nicholson 

About the same time Warwick was divided and a new township set out under the name of Coventry. A short history of Rhode Island |George Washington Greene 

Abercrombie—a township in the parish of Abercrombie and county of Somerset. The History of Tasmania , Volume II (of 2) |John West 

Summerleas—a township in the parish of Kingborough and county of Buckingham. The History of Tasmania , Volume II (of 2) |John West