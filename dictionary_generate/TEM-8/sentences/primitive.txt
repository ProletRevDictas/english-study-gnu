Otherwise, don’t be scared to primitive-camp outside of a campground. Your Guide to Camping in Southern Utah |eriley |August 27, 2021 |Outside Online 

It distorts our view of nature and makes all the other species around us seem more primitive and somehow unfinished. Plants Feel Pain and Might Even See - Issue 104: Harmony |Peter Wohlleben |July 21, 2021 |Nautilus 

The result is a primitive wine, completely handmade, the product of prison inventiveness. Locked up in the Land of Liberty: Part III |Yariel Valdés González |July 21, 2021 |Washington Blade 

While there is some primitive camping inside the park boundaries, most visitors choose one of the privately owned campgrounds or “resorts” that sit adjacent to the park proper. The Ultimate New River Gorge National Park Travel Guide |awise |July 19, 2021 |Outside Online 

The majority of the island is protected, so expect a primitive beach-going experience. The Best Campground in Every State |thodgson |July 12, 2021 |Outside Online 

When we assign a primitive “not me” status to another individual or social group, it can—and does—take us down a destructive path. Ferguson, Immigration, and ‘Us Vs. Them’ |Gene Robinson |November 27, 2014 |DAILY BEAST 

We were careful with how we dealt with suspected patients and what we did with our primitive coverings, it was steamy. The Original Ebola Hunter |Abby Haglage |September 14, 2014 |DAILY BEAST 

We cannot keep judging mothers by a primitive, antiquated, simplistic standard. Postpartum Stigma: Why My Patient Committed Suicide |Jean Kim |August 5, 2014 |DAILY BEAST 

It also may relate to our still primitive understanding of the natural history of Ebola virus infection. Emory Will Wage High-Tech War on Ebola |Kent Sepkowitz |August 1, 2014 |DAILY BEAST 

When the group seized control of Gaza in 2007, its primitive rockets had a range of no more than 25 miles. Hamas Has Already Won Its Rocket War With Israel |Eli Lake |July 16, 2014 |DAILY BEAST 

Something remote and ancient stirred in her, something that was not of herself To-day, something half primitive, half barbaric. The Wave |Algernon Blackwood 

A writer has truthfully said in regard to associating the name and use of the plant with the primitive users of it. Tobacco; Its History, Varieties, Culture, Manufacture and Commerce |E. R. Billings. 

In this way child's play, like primitive art, shows a certain unconscious selectiveness. Children's Ways |James Sully 

A primitive savage makes a bow and arrow in a day: it takes him a fortnight to make a bark canoe. The Unsolved Riddle of Social Justice |Stephen Leacock 

“They have a primitive mode of conducting funerals here,” said Tom Brown when the major had left. Hunting the Lions |R.M. Ballantyne