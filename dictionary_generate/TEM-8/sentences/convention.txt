It’s usually a tangle of A- through D-list celebrities, former football greats and not-so-greats, all roaming the halls of a convention hall, hawking everything from rental cars to pot supplements. What’s the Super Bowl without the frenzy? For Leigh Steinberg: Misery. For players: Bliss. |Ben Strauss |February 4, 2021 |Washington Post 

Aside from protections for health, hygiene, and other living conditions, the convention specifies that no agreement between the parties supersedes its protections while occupation continues. Israel leads the world in vaccination rates, but a key group is missing from the data |By Yara M. Asi/The Conversation |February 3, 2021 |Popular-Science 

I know now, though, that enough people found each other over the Internet and discovered that they shared this passion that entire conventions dedicated to furries emerged. The country is being buffeted by groups that couldn’t exist 30 years ago |Philip Bump |January 27, 2021 |Washington Post 

After three marathon meetings, the party voted Saturday to stick with its plan to hold a convention. Former Carlyle executive Glenn Youngkin joins race for Virginia governor |Laura Vozzella |January 27, 2021 |Washington Post 

In February 1823, two-thirds of the state’s House of Representatives — the required quorum — passed a resolution calling for a convention to change the Illinois Constitution to legalize slavery. Loopholes have preserved slavery for more than 150 years after abolition |Caroline Kisiel |January 27, 2021 |Washington Post 

Rule 16(c) was a proposed change in the rules at the 1976 Republican Convention. The World’s Toughest Political Quiz |Jeff Greenfield |December 31, 2014 |DAILY BEAST 

The crowd inside the convention center is very white, fairly old, and presumably decently rich. Sneer and Clothing in Miami: Inside The $3 Billion Woodstock of Contemporary Art |Jay Michaelson |December 6, 2014 |DAILY BEAST 

Inside the Miami Beach Convention Center, there is a lot of good art—but more pretty art. Sneer and Clothing in Miami: Inside The $3 Billion Woodstock of Contemporary Art |Jay Michaelson |December 6, 2014 |DAILY BEAST 

Who helps build convention centers and adjacent hotels so cities can attract convention business? Democrats Are Petrified of Defending Government—but They Need to Start |Michael Tomasky |December 4, 2014 |DAILY BEAST 

“The social convention of not talking to a stranger was fairly rigid at the time,” Weber told me. The Secret World of Pickup Artist Julien Blanc |Brandy Zadrozny |December 1, 2014 |DAILY BEAST 

It is beyond the comprehension of any man not blinded by superstition, not warped by prejudice and old-time convention. God and my Neighbour |Robert Blatchford 

The French convention decreed that no quarters be given to British and Hanoverian soldiers. The Every Day Book of History and Chronology |Joel Munsell 

The armed Parisians again assembled with cannon around the convention, and demanded the arrest of the Brissotine party. The Every Day Book of History and Chronology |Joel Munsell 

A convention of delegates to revise the constitution of New York met at Albany. The Every Day Book of History and Chronology |Joel Munsell 

Madame Roland, in the name of her husband, drew up for the Convention the plan of a republic as a substitute for the throne. Madame Roland, Makers of History |John S. C. Abbott