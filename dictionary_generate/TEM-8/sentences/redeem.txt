If the company grows without raising additional equity funding, founders redeem most of the equity right, based on a pre-agreed return amount. 12 ‘flexible VCs’ who operate where equity meets revenue share |Walter Thompson |January 14, 2021 |TechCrunch 

It was an opportunity, eight months after the United States confirmed its first coronavirus case, to redeem the nation’s devastating failures in organizing a regimen of testing, contact tracing and equipping medical workers with protective gear. Vaccines were a chance to redeem failures in the U.S. coronavirus response. What went wrong? |Lena H. Sun, Isaac Stanley-Becker, Frances Stead Sellers, Laurie McGinley, Amy Goldstein, Christopher Rowland, Carolyn Y. Johnson |January 11, 2021 |Washington Post 

Tucker redeemed himself by connecting from 51 yards with just more than four minutes left. Lamar Jackson gets his first playoff victory as Ravens hold Titans’ Derrick Henry to 40 yards |Mark Maske |January 10, 2021 |Washington Post 

Even when customers use them, there’s often either a small balance left on gift cards that’s never redeemed, or they spend additional cash beyond the card balance to get the product they want. Americans are on track to buy more gift cards this holiday season than ever before |Rachel King |December 24, 2020 |Fortune 

Tap Network aims to solve this problem by allowing customers to spend those points through a broader network of rewards, which can usually be redeemed at a lower point level. Tap Network raises $4M for its customizable rewards program |Anthony Ha |December 18, 2020 |TechCrunch 

Sports drinks and coconut water, which is lower in sugar, can also redeem electrolytes lost while drinking, says White. 5 Hangover Cures to Save You After a Few Too Many |DailyBurn |December 19, 2014 |DAILY BEAST 

Sens. Rand Paul (R-KY) and Cory Booker (D-NJ) may have drawn wide attention and praise for their REDEEM Act. Why Isn’t Prison Justice on the Ballot This Tuesday? |Inimai Chettiar, Abigail Finkelman |November 1, 2014 |DAILY BEAST 

In all of this lies the chance, also, for FIFA to redeem itself. Best Way to Punish Putin? No World Cup |Tunku Varadarajan |July 20, 2014 |DAILY BEAST 

Now, thanks to a military man he fired, retired Gen. Stanley McChrystal, he has a chance to redeem himself. It’s Time for Obama to Heed McChrystal’s Call for the ‘Service Year’ |Jonathan Alter |June 23, 2014 |DAILY BEAST 

And that means it has to potential to redeem Christie—or make his already-hellish 2014 much, much worse. This Civil War Reenactor Controls Christie’s Fate |Olivia Nuzzi |March 5, 2014 |DAILY BEAST 

He had to do something, for although all his land had been foreclosed on, he had two years to redeem the same. The Homesteader |Oscar Micheaux 

And I will deliver thee out of the hand of the wicked, and I will redeem thee out of the hand of the mighty. The Bible, Douay-Rheims Version |Various 

Any person who is interested in a mortgaged estate has the right to redeem it; heirs, devisees, creditors. Putnam's Handy Law Book for the Layman |Albert Sidney Bolles 

But she seems able to take care of herself, and with that face and form, I guess she can redeem her fortunes any way she chooses. Ancestors |Gertrude Atherton 

The French war indemnity enabled him to redeem a considerable portion of the state debt and to remit certain taxes. Encyclopaedia Britannica, 11th Edition, Volume 5, Slice 2 |Various