“I was losing it,” Peter later recalled as we sat on their front porch on a far-too-warm November afternoon in Altadena, California, just below the San Gabriel Mountains. The Climate Crisis Is Worse Than You Can Imagine. Here’s What Happens If You Try. |by Elizabeth Weil |January 25, 2021 |ProPublica 

He got sicker and decided to drive himself to the hospital, but made it no farther than his front porch. Firefighter who nearly died of covid begs front-line workers: ‘Please, get the shots’ |Katie Mettler |January 22, 2021 |Washington Post 

A rear porch partially collapsed, and one firefighter was injured. Couple who died in Petworth fire recalled as dedicated to church and community |Peter Hermann, Dana Hedgpeth |January 22, 2021 |Washington Post 

He also mailed a bottle to Charlie Sheen, who never tasted it because it was stolen from his front porch, probably because it was tantalizingly labeled “Top Secret,” so O’Neill sent another. Gene Weingarten: This hot sauce is a killer |Gene Weingarten |January 21, 2021 |Washington Post 

Democrats and inauguration fans, trying to make the best of things, are looking for small ways to be part of the moment, even from their porches. Inaugurations are highly anticipated events in D.C. Now people must watch from their couches. |Emily Davies |January 20, 2021 |Washington Post 

He stands, one assumes on a porch, which overlooks a prairie. Huckabee 2016: Bend Over and Take It Like a Prisoner! |Olivia Nuzzi |January 8, 2015 |DAILY BEAST 

Davis jumped over a 4-foot porch wall and ran into a house, where he and others crammed themselves into a linen closet. ‘Argo’ in the Congo: The Ghosts of the Stanleyville Hostage Crisis |Nina Strochlic |November 23, 2014 |DAILY BEAST 

Gosta Peterson sits on the porch of his Long Island home and greets passersby. Gosta Peterson's Bohemian Rhapsody: Unpacking a Photographer's '60s Secrets |Lizzie Crocker |September 10, 2014 |DAILY BEAST 

On the porch, before I go, Peterson looks at me through the lens of a small digital camera before training it on his front lawn. Gosta Peterson's Bohemian Rhapsody: Unpacking a Photographer's '60s Secrets |Lizzie Crocker |September 10, 2014 |DAILY BEAST 

She taught little black boys to love themselves, and look beyond their own front porch to the hope of a broader horizon. Maya Angelou Knew How To Inspire As A Writer, Teacher, and Great Human Being |Joshua DuBois |May 28, 2014 |DAILY BEAST 

Sol laughed out of his whiskers, with a big, loose-rolling sound, and sat on the porch without waiting to be asked. The Bondboy |George W. (George Washington) Ogden 

The lady in black was reading her morning devotions on the porch of a neighboring bathhouse. The Awakening and Selected Short Stories |Kate Chopin 

Sarah was standing on the porch again wiping her hands on her apron, looking away toward the fields. The Bondboy |George W. (George Washington) Ogden 

The bells were clashing merrily from the village spire as the party passed out of the church porch. The Pit Town Coronet, Volume I (of 3) |Charles James Wills 

The fight was over and Lawrence rode up to the house, and was met on the porch by a white haired, fine looking old gentleman. The Courier of the Ozarks |Byron A. Dunn