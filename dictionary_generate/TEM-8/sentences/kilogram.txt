It’s primarily derived from fossil fuels by subjecting methane to steam, high heat, and pressure to break it into hydrogen and carbon dioxide, and costs about $1 per kilogram. Scientists Just Laid Out a Game Plan for Building a Clean Hydrogen Economy |Edd Gent |August 16, 2021 |Singularity Hub 

You can just eyeball it—oh, this is obviously pounds or kilograms. It took a pandemic, but the US finally has (some) centralized medical data |Cat Ferguson |June 21, 2021 |MIT Technology Review 

The labels estimate a product’s environmental impact from cradle to grave as a carbon equivalent reflecting the greenhouse gas emissions or CO2e spent in its creation, transportation, use and end of life, as measured in grams or kilograms of carbon. Companies bet carbon labels can help the climate. Will consumers catch on? |Jessica Wolfrom |June 17, 2021 |Washington Post 

So instead, we used 3D digital scanning technology which allowed us to virtually carry thousands of kilograms of dinosaur bones in one seven kilogram laptop. Lizard of Oz: Australia’s Biggest Dinosaur Discovered |Scott Hocknull and Rochelle Lawrence, The Conversation |June 7, 2021 |The Daily Beast 

In a paper in Science Advances, they report that producing new PDK would cost around $45 per kilogram and result in about 86 kilograms of CO2 equivalent per kilogram. ‘Infinitely Recyclable’ Plastic Could Help Solve Our Waste Crisis |Edd Gent |May 3, 2021 |Singularity Hub 

The European formula for Fireball has even less: under one gram per kilogram of propylene glycol. Europeans Recall Fireball Whiskey Over a Sweetener Also Used in Antifreeze |Tim Mak |October 28, 2014 |DAILY BEAST 

There is, of course, cheapness to be considered -- the dollar per kilogram bill for putting a payload into low earth orbit. Up to a Point: A 'Space Corvette' in Every Garage |P. J. O’Rourke |September 6, 2014 |DAILY BEAST 

Rhino horn is particularly lucrative—each kilogram can fetch up to $66,000. South Africa’s Great Rhino Airlift |Nina Strochlic |August 17, 2014 |DAILY BEAST 

This is a measure (in watts per kilogram) of how much of the emitted radiation is absorbed by biological tissue. Are Cellphones Really a Cancer Risk? |Sharon Begley |June 1, 2011 |DAILY BEAST 

A Calorie is the amount of heat required to raise the temperature of one kilogram of water from zero to one degree Centigrade. A Civic Biology |George William Hunter 

Half a kilogram (eighteen ounces) is considered the portion for each person. Rome |Mildred Anna Rosalie Tuker 

A Calorie is the amount of heat required to raise one kilogram of water 1° Centigrade or one pound of water 4° Fahrenheit. Better Meals for Less Money |Mary Green 

This work or its equivalent would be accomplished by a steam-engine in the course of burning one kilogram (two pounds) of coal. The Great Events by Famous Historians, v. 13 |Various 

They take the water which is used to determine the weight of the kilogram, keeping it at the temperature of 4C. Montessori Elementary Materials |Maria Montessori