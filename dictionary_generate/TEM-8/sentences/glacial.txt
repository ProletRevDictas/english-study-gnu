The satellite images seem to point clearly to such a landslide, rather than a typical glacial lake overflow, Shugar says. Three things to know about the disastrous flood in India |Carolyn Gramling |February 9, 2021 |Science News 

This research could provide a key piece of evidence in that case, by directly connecting emissions to the growth of the glacial lake. A looming climate disaster threatens the lives of 6,000 Peruvians |Philip Kiefer |February 9, 2021 |Popular-Science 

The ice here is effectively the remnants of what were probably more extensive glacial structures in the past. These might be the best places for future Mars colonists to look for ice |Neel Patel |February 8, 2021 |MIT Technology Review 

The astonishingly fast development of safe and effective vaccines is being stymied by the glacial pace of actual vaccinations while 3,000 Americans die each day. How to Fix the Vaccine Rollout - Issue 95: Escape |Melanie E. Moses |January 20, 2021 |Nautilus 

Researchers found that microbes collected from Icelandic glacial streams rich in dissolved hydrogen were better adapted to use the gas than those from a Canadian glacial stream where hydrogen was less plentiful. Glacier-dwelling bacteria thrive on chemical energy derived from rocks and water |Kate Baggaley |December 30, 2020 |Popular-Science 

Many thousands of years ago, glacial floods swept through the area and carved out the sloping sides of the current grounds. Brooklyn’s Gangster Graveyard |Nina Strochlic |October 23, 2014 |DAILY BEAST 

“Americans will never drive a small car,” said the man from GM with glacial confidence. Nationalism on Four Wheels |Clive Irving |October 18, 2014 |DAILY BEAST 

Folks are taking cold showers already, and the weather soon will be glacial. In Ukraine, Winter Is Coming |Anna Nemtsova |September 23, 2014 |DAILY BEAST 

My partner Brandon and I awake at the crack of dawn for a canoe ride on the milky blue glacial waters of Lake Louise. Our Trip to The Climate War's Ground Zero |Darren Aronofsky |September 19, 2014 |DAILY BEAST 

For a church that moves at a glacial pace, the murmurings of Bishops like Tobin are lightning fast and boldly subversive. A Coming American Schism Over Pope Francis? |Candida Moss |September 20, 2013 |DAILY BEAST 

Science teaches that man existed during the glacial epoch, which was at least fifty thousand years before the Christian era. God and my Neighbour |Robert Blatchford 

But scorn is far more volcanic than glacial and a poor barrier between sex and judgment. Ancestors |Gertrude Atherton 

Let dry, apply a cover-glass, and run glacial acetic acid underneath it. A Manual of Clinical Diagnosis |James Campbell Todd 

Still water runs deep, they say; and a glacial cap may conceal subterranean fires. Raw Gold |Bertrand W. Sinclair 

Further study will reveal the fact that the difference is due to the lack of oxidation in the case of the glacial detritus. Outlines of the Earth's History |Nathaniel Southgate Shaler