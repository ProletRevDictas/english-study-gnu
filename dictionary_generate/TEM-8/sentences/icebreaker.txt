Luke’s dinner, however, was not nearly as conducive to the icebreaker. Date Lab: She started the virtual date showing off her fancy tapas. He had ordered a burrito. |Prachi Gupta |March 4, 2021 |Washington Post 

During other months, less effort is needed by icebreakers to sustain transportation. India’s Dirty Arctic Energy Hunt |Charu Kasturi |January 11, 2021 |Ozy 

In one of my classes, they focused on fostering connections, with group chats and icebreakers to get to know each other. For college freshmen, pandemic results in a first-year experience unlike any other |Joe Heim, Nick Anderson, Danielle Douglas-Gabriel, Susan Svrluga |December 17, 2020 |Washington Post 

At the event, Katie set out notebooks, pens and Starbursts for an icebreaker activity at every desk. This AI whiz could be the next Elon Musk or Steve Jobs, but first she has to navigate being 18 |Taylor Telford |November 5, 2020 |Washington Post 

I spent a lot of time on icebreakers in February 1987 looking for life in the ocean below the Ross Ice Shelf, which is almost 200,000 square miles, about the size of France, and more than 2,000 feet thick. He Found ‘Islands of Fertility’ Beneath Antarctica’s Ice |Steve Nadis |July 20, 2020 |Quanta Magazine 

It functions as an icebreaker the same way trust-fall exercises do. The Case Against Cards Against Humanity: Is Max Temkin a Horrible Person? (Does It Matter?) |Arthur Chu |July 29, 2014 |DAILY BEAST 

This could replace the ice cream social as the college freshman icebreaker of choice. Viral Video of the Day: Slapping Brings People Together |Alex Chancey |June 25, 2014 |DAILY BEAST 

“I thought, ‘What could I do that would immediately try to be an icebreaker with the audience,’” Colbert says. Stars Who Entertain the Troops |Laura Colarusso |October 6, 2011 |DAILY BEAST 

As I was anxious to have the experience of smashing through the ice on an icebreaker, I went aboard the Corwin. In Search of a Siberian Klondike |Homer B. Hulbert