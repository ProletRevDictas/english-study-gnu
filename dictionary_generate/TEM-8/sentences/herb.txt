In one video showing viewers how she makes her chicken, Terri-Ann pulls out a glass jar of Walkerswood Jamaican Jerk Seasoning, a pre-blended mixture of spices and herbs which she says she swears by. Until I Can Go Back to My Favorite Restaurant, This Jerk Paste Is the Next Best Thing |Elazar Sontag |September 25, 2020 |Eater 

I looked to herbal medicine to help relieve my fatigue, and once I figured out what herbs I should be taking and in what amounts, I was absolutely blown away by the results. The founder of wellness startup Mab & Stoke on the growth of ‘pay what you can’ options during the pandemic |Rachel King |September 20, 2020 |Fortune 

Our Beauty Rest Tea, a mixture of CBD and herbs like chamomile and calendula that promote a sense of calm and help promote a nice sense of sleep, was the first product to sell out. Tower 28 Announces Winner Of The Clean Beauty Summer School Program |Hope Wright |September 17, 2020 |Essence.com 

I’m obsessed with herbs and spices, and love making sauces and condiments. Queery: Kerry Hallett |Staff reports |September 2, 2020 |Washington Blade 

Before setting out, the men consumed special herbs and sacrificed pigs. Why do we miss the rituals put on hold by the COVID-19 pandemic? |Sujata Gupta |August 14, 2020 |Science News 

The flowers and leaves of this herb are used to make medications and the supplement is popularly used for depression. Fish Oil, Turmeric, and Ginseng, Oh My! Are ‘Brain Foods’ B.S.? |Dr. Anand Veeravagu, MD |October 10, 2014 |DAILY BEAST 

Wonderful sausage: dense, herb-spangled links that are more lean than fatty, but nevertheless emphatically succulent. Welcome to Yooperland, A Little Slice of Finland in Michigan |Jane & Michael Stern |May 11, 2014 |DAILY BEAST 

Every moving thing that liveth shall be meat for you; even as the green herb have I given you all things. The Story of Noah's Ark From the Bible’s Book of Genesis |The Daily Beast |March 24, 2014 |DAILY BEAST 

And the herb has a huge following: almost 13 percent of Vermont residents report using in the last month. From Dry to High: Your Guide to State Pot Laws |Brandy Zadrozny |January 29, 2014 |DAILY BEAST 

Later, Herb decides that the story about how he made up that story is just as good. Intriguing, Humorous, Even Poetic: Peter Orner’s New Story Collection |Joseph Peschel |August 13, 2013 |DAILY BEAST 

Roman Pane who accompanied Columbus on his second voyage alludes to another method of using the herb. Tobacco; Its History, Varieties, Culture, Manufacture and Commerce |E. R. Billings. 

It was supposed by many on its discovery to grow like the engraving given—in form resembling a tree or shrub rather than an herb. Tobacco; Its History, Varieties, Culture, Manufacture and Commerce |E. R. Billings. 

How long shall the land mourn, and the herb of every field wither for the wickedness of them that dwell therein? The Bible, Douay-Rheims Version |Various 

The herb was only to be found in its highest perfection upon the southern coasts of the Mediterranean. A Cursory History of Swearing |Julian Sharman 

Rosin-weed has always seemed to me to be a harmless herb, which is shown also by its use among children as chewing gum. The Treatment of Hay Fever |George Frederick Laidlaw