Some news reports have cited unnamed sources saying that a woman participating in the trial experienced symptoms consistent with transverse myelitis, a spinal cord inflammatory syndrome. Here’s what pausing the AstraZeneca-Oxford coronavirus vaccine trial really means |Aimee Cunningham |September 9, 2020 |Science News 

They’ve been studied before for use in patients with ARDS, a syndrome that can be caused by a host of things including trauma and disease, since the 1960s. Corticosteroids can help COVID-19 patients with severe respiratory distress |Kat Eschner |September 3, 2020 |Popular-Science 

Examples of such outcomes include stress fractures, overtraining syndrome, or waking up one morning and realizing that running sucks and should be reserved as punishment for our most violent criminals. 17 Training Myths, Addressed by a Running Coach |Brendan Leonard |August 30, 2020 |Outside Online 

Around one in four people across the world will suffer from a psychiatric syndrome during their lifetime. Machines can spot mental health issues—if you hand over your personal data |Bobbie Johnson |August 13, 2020 |MIT Technology Review 

Working with clients who have autism, Down syndrome and other speech disorders, she needed face shields for her therapists that allowed their mouths to be visible but protected. This High Schooler Hands Out More PPE Than the Feds |Daniel Malloy |August 7, 2020 |Ozy 

Advanced maternal age dramatically increases the risk of maternal mortality as well as birth defects like Down Syndrome. Men Will Someday Have Kids Without Women |Samantha Allen |January 3, 2015 |DAILY BEAST 

Jolly somehow finds the time to also manage a restaurant and help kids who have Autism and Down syndrome ride horses. The Moms of Monster Jam Drive Trucks, Buck Macho Culture |Eliza Krigman |November 22, 2014 |DAILY BEAST 

We still have a long way to go, but it has improved dramatically since the post Vietnam syndrome when Vets were spit on. McCain’s 13 Favorite Soldiers |Sandra McElwaine |November 11, 2014 |DAILY BEAST 

In 1975, Sesame Street became the first children's program to feature someone with Down Syndrome. ‘Sesame Street’ Is Middle-Aged and Awesome |Emily Shire |November 10, 2014 |DAILY BEAST 

However, the current thinking is that Merrick may have suffered from Proteus syndrome. The True Story of ‘The Elephant Man’ |Russell Saunders |November 3, 2014 |DAILY BEAST 

The Ganser syndrome, or twilight state, has been enlarged upon, and several variations of this condition have been isolated. Studies in Forensic Psychiatry |Bernard Glueck 

I have recently observed the Ganser syndrome in an undoubted case of toxic-exhaustion psychosis. Studies in Forensic Psychiatry |Bernard Glueck 

I will not go mad, but I will go into the adrenal syndrome unless I can end this soon. The Issahar Artifacts |Jesse Franklin Bone 

This is the classical "damned if you do and damned if you don't" syndrome. Shock and Awe |Harlan K. Ullman 

We are in the classical "damned if we do and damned if we don't" syndrome. Shock and Awe |Harlan K. Ullman