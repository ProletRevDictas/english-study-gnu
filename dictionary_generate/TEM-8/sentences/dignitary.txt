His speech contained deft references to their ideas, like attacks on defunding the police and support for a flat income tax, that he occasionally sounded less like a foreign dignitary and more like a GOP candidate for office. Viktor Orbán laid out his dark worldview to the American right — and they loved it |Zack Beauchamp |August 5, 2022 |Vox 

The 47th Regiment Band played, and special trains unloaded dignitaries from New York and Brooklyn at the newly opened station. The Leader of New York’s “City of the Dead” Cashes In. Again. |by Carson Kessler |July 25, 2022 |ProPublica 

These included parades through London, royal banquets with visiting foreign monarchs and dignitaries, and firework displays. Parades, Pageants, and Afternoon Tea: The History of Britain's Royal Jubilee Celebrations |Eloise Barry |June 1, 2022 |Time 

Personal sanctions on foreign dignitaries can be a potent tool. U.S. and E.U. Are Going After Putin's Wealth. First They Need To Find It |Nik Popli |February 26, 2022 |Time 

He was treated as a celebrity and dignitary, more like a head of state than the head of an infamous defense contracting company. The Disneyfication of Atomic Power - Issue 107: The Edge |Jacob Darwin Hamblin |October 6, 2021 |Nautilus 

Tennis dignitary Chris Evert stands behind her, forced into rapturous laughter along with 14,000 others inside the arena. Tennis Star Li Na Says Goodbye to the Court…and Puts the Sport’s Rise in Asia in Question |Nicholas McCarvel |September 19, 2014 |DAILY BEAST 

We are the bridge between the foreign visiting dignitary and the U.S. government. Washington's Protocol Chief Capricia Marshall Steps Down |Sandra McElwaine |August 11, 2013 |DAILY BEAST 

“Large crowds flocked to the jail, where they greeted him like a visiting dignitary,” Gardner writes. This Week’s Hot Reads: July 22, 2013 |Cameron Martin |July 22, 2013 |DAILY BEAST 

Many of them, such as a visiting French dignitary fresh off a plane from Paris at the age of 103, proved that 90 is the new 30. John McCain’s Surprising Toast at Kissinger’s 90th Birthday Party |The Daily Beast |June 4, 2013 |DAILY BEAST 

Bespectacled and wearing a proper suit, Ban looked every bit the serious dignitary. Psy Brings ‘Gangnam Style’ to the United Nations & Ban Ki-moon |Kevin Fallon |October 25, 2012 |DAILY BEAST 

I desired him to accompany me to call on this dignitary, but he did not seem at all anxious for the job. British Highways And Byways From A Motor Car |Thomas D. Murphy 

The high-priest was a great dignitary, and generally belonged to the royal family. Beacon Lights of History, Volume I |John Lord 

His deportment at this solemn ceremony, as related by a church dignitary, was fully edifying. The History of England in Three Volumes, Vol.III. |E. Farr and E. H. Nolan 

It was not long before the curtain was drawn aside again, and one entered who seemed to be a dignitary of the court. The Devil-Tree of El Dorado |Frank Aubrey 

And this great dignitary pointed to me with scorn and said: "Number one foolo." My Life in Many States and in Foreign Lands |George Francis Train