We heard it everywhere across the country that many people took to the open roads thinking that they could speed, and speed significantly. Traffic counts fell during the coronavirus pandemic, but road fatalities still increased |Luz Lazo |February 12, 2021 |Washington Post 

However, temperatures should fall below freezing not long after dark, creating some slick spots on untreated roads and walkways. Snow expected tonight, mainly south of D.C., before possible ice on Saturday |Jason Samenow, Wes Junker |February 11, 2021 |Washington Post 

Temperatures are 31 to 33 degrees north of the Beltway, where you may encounter slick spots, especially on untreated roads. D.C.-area forecast: Snow and wintry mix taper off this morning, while ice concerns mount for Saturday |David Streit |February 11, 2021 |Washington Post 

Driving along the C&O Canal Scenic Byway makes for a great road trip. 4 Awesome Winter Road Trips to National Parks |Megan Michelson |February 11, 2021 |Outside Online 

At that point, temperatures may fall closer to freezing and snow accumulation may pick up, even on roads. Snow and wintry mix continue overnight, especially north of District |Jason Samenow, Wes Junker, Andrew Freedman |February 11, 2021 |Washington Post 

So I drove around the corner to the trailhead of the logging road that led back to the crash site. The 7-Year-Old Plane Crash Survivor’s Brutal Journey Through the Woods |James Higdon |January 7, 2015 |DAILY BEAST 

As he drove me back to the logging road, Frank told me about the area in his deep voice. The 7-Year-Old Plane Crash Survivor’s Brutal Journey Through the Woods |James Higdon |January 7, 2015 |DAILY BEAST 

Shrubs and small trees dot a parched landscape along the road from Turbat to the border. The Dangerous Drug-Funded Secret War Between Iran and Pakistan |Umar Farooq |December 29, 2014 |DAILY BEAST 

For many Republicans presidential hopefuls, the road to the nomination passes through the Hawkeye State. Can This Republican Bring the GOP Back to Its Senses on Immigration? |Tim Mak |December 29, 2014 |DAILY BEAST 

All the roads into Iraqi Kurdistan and toward Baghdad are closed and now the road toward Syria is also blocked. Has the Kurdish Victory at Sinjar Turned the Tide of ISIS War? |Niqash |December 27, 2014 |DAILY BEAST 

What need to look to right or left when you are swallowing up free mile after mile of dizzying road? The Joyous Adventures of Aristide Pujol |William J. Locke 

His wife stood smiling and waving, the boys shouting, as he disappeared in the old rockaway down the sandy road. The Awakening and Selected Short Stories |Kate Chopin 

She set off down Trafalgar Road in the mist and the rain, glad that she had been compelled to walk. Hilda Lessways |Arnold Bennett 

The first rail road opened in Brazil, the emperor and empress being present at the inauguration. The Every Day Book of History and Chronology |Joel Munsell 

The intricacies and abrupt turns in the road separated him from his immediate followers. The Pastor's Fire-side Vol. 3 of 4 |Jane Porter