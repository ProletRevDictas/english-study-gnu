We camped there over the weekend as we took part in the Pacific Northwest Bumblebee Atlas survey. We Crush, Poison, and Destroy Insects at Our Own Peril - Issue 95: Escape |John Hainze |January 20, 2021 |Nautilus 

The Atlas buddies are later joined by the dog-like Spot bot, and the ostrich-like Handle robot, which was designed to help out in a warehouse setting. The Boston Dynamics robots are surprisingly good dancers |Stan Horaczek |December 31, 2020 |Popular-Science 

Customers can use Stripe’s credit card fraud detection service, or its Atlas product to form a company in Delaware. Stripe wants to be a $100 billion one-stop shop for small business |John Detrixhe |December 24, 2020 |Quartz 

AI also helped build the most dynamic brain atlas to date, a “living map” that can continuously incorporate new data and capture individual differences. 2020 in Neuroscience, Longevity, and AI—and What’s to Come |Shelly Fan |December 22, 2020 |Singularity Hub 

Since then, more than 55,000 Americans have died of the virus Atlas advocated letting spread. The new tidal wave of coronavirus deaths has arrived |Philip Bump |December 10, 2020 |Washington Post 

He would navigate from the cockpit using a road atlas—while snorting cocaine off the map. The Secrets of Britain’s Wildest Aristocrats |Tom Sykes |October 20, 2014 |DAILY BEAST 

Think of the difference between a satellite image and a road atlas. The Best Map of Mars Yet |Matthew R. Francis |July 20, 2014 |DAILY BEAST 

The Delta IV can carry a larger payload into low earth orbit than the Atlas V, 60,779 lbs. Why Does the USA Depend on Russian Rockets to Get Us Into Space? |P. J. O’Rourke |June 22, 2014 |DAILY BEAST 

Think of the sky chart, the song map, the winter count, and the cloud atlas. Should Google Be Mapping Tribal Lands? |Grace-Yvette Gemmell |June 4, 2014 |DAILY BEAST 

“The lair of the laser loves all of you,” he tells a visiting Atlas Obscura tour group. New York’s Hologram King Is Also the City’s Last Pro Holographer |Nina Strochlic |May 27, 2014 |DAILY BEAST 

Sanson's Atlas: a very large atlas by a French geographer in use in Swift's time. Gulliver's Travels |Jonathan Swift 

Once, during the recreation hour, he was turning over the pages of his atlas. The Life &amp; Letters of Peter Ilich Tchaikovsky |Modeste Tchaikovsky 

We have been searching the atlas, and it seems difficult to fill the bill. The Works of Robert Louis Stevenson - Swanston Edition Vol. 25 (of 25) |Robert Louis Stevenson 

When he addresses himself to battle against the guardian angels, he stands like Teneriffe or Atlas; his stature reaches the sky. English: Composition and Literature |W. F. (William Franklin) Webster 

The mighty Atlas would never sustain it upon his broad shoulders if it did nobody good. The Inhumanity of Socialism |Edward F. Adams