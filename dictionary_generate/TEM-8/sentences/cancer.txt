LeBlanc considered herself lucky because no one in her family has had cancer. New Research Shows Disproportionate Rate of Coronavirus Deaths in Polluted Areas |by Lylla Younes, ProPublica, and Sara Sneath |September 11, 2020 |ProPublica 

Then you get down into patients who have established cardiac disease, who’ve got moderate to severe asthma, who have or have had cancer in the past, sickle-cell disease and things like that. Why Can’t Schools Get What the N.F.L. Has? (Ep. 431) |Stephen J. Dubner |September 10, 2020 |Freakonomics 

A redesigned set for the coronavirus era will allow for the contestants to be further apart and at a greater distance from Trebek, who has continued as host after a diagnosis last year of pancreatic cancer. ‘Jeopardy!’ is returning with a redesigned set—and a new role for Ken Jennings |radmarya |September 3, 2020 |Fortune 

As Boseman’s untimely death reminds us, colorectal cancer is a difficult and emotional disease for all people at any age. Chadwick Boseman’s Death From Colorectal Cancer Underscores An Alarming Increase In Cases Among Younger Adults As Well As Health Gaps For African Americans |LGBTQ-Editor |September 2, 2020 |No Straight News 

Physicians also prescribe birth control to patients who would face health risks if they were to become pregnant, including women who suffer from cardiac disease or are undergoing some cancer treatments. USPS delays threaten women’s access to birth control |ehinchliffe |August 27, 2020 |Fortune 

After four or five months of casual interaction, they realized they both had lost a young parent to cancer. Everyone at This Dinner Party Has Lost Someone |Samantha Levine |January 6, 2015 |DAILY BEAST 

A single father, he had been living abroad and returned when his mother was diagnosed with cancer. Everyone at This Dinner Party Has Lost Someone |Samantha Levine |January 6, 2015 |DAILY BEAST 

Family members say he developed also liver cancer after his capture. Final Chapter for Accused Africa Bomber |Jamie Dettmer |January 4, 2015 |DAILY BEAST 

My father has suffered two strokes and endured brain cancer since I was arrested and imprisoned. An American Marine in Iran’s Prisons Goes on Hunger Strike |IranWire |December 18, 2014 |DAILY BEAST 

Dean Sybil Todd passed away from pancreatic cancer before she could testify. I Was Gang Raped at a UVA Frat 30 Years Ago, and No One Did Anything |Liz Seccuro |December 16, 2014 |DAILY BEAST 

(b) Diseases of the stomach associated with deficient hydrochloric acid, as chronic gastritis and gastric cancer. A Manual of Clinical Diagnosis |James Campbell Todd 

To-day men of science are trying to conquer the horrors of cancer and smallpox, and rabies and consumption. God and my Neighbour |Robert Blatchford 

Since he died from cancer in the stomach, he could retain very little food. Assimilative Memory |Marcus Dwight Larrowe (AKA Prof. A. Loisette) 

The presence of lactic acid is the most suggestive single symptom of gastric cancer. A Manual of Clinical Diagnosis |James Campbell Todd 

Considerable numbers of pus-corpuscles have been found in some cases of gastric cancer. A Manual of Clinical Diagnosis |James Campbell Todd