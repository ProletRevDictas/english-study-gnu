I will be dreaming of a seaside vacation—and the tangy refreshment to go with it—as I listen to this breezy bossa nova-styled number. What’s the Song of the Summer for 2021? Here Are Our Predictions |Kat Moon |June 23, 2021 |Time 

David, whose father is dead, helps his mother run the family’s seaside boating-goods shop. A Teenager in Normandy Feels the Heat of First Love in Summer of 85 |Stephanie Zacharek |June 18, 2021 |Time 

Its cruise port and proximity to seaside towns attracted hordes of day-trippers, who spent less money and flooded the city center. Can Barcelona Fix Its Love-Hate Relationship With Tourists After the Pandemic? |Ciara Nugent |June 9, 2021 |Time 

Without Ocean Adventure, we wouldn’t be able to enjoy a local seaside getaway with dogs, kids and plenty of living space. Renting a vacation home for the first time? Don’t take anything for granted. |Candyce H. Stapen |April 22, 2021 |Washington Post 

Another summer palace being constructed in the seaside city of Marmaris is reported to cost an additional $85 million. The 'Badass Chief of Staff' of Turkey's Opposition Faces Years in Jail After Challenging Erdogan's Power. She's Not Backing Down |Joseph Hincks / Istanbul |February 24, 2021 |Time 

Tallinn feels palpably Scandinavian with its polished old-town brick, seaside positioning and glut of cool cafes. Next Stop, Quito: Our Top Cities for 2015 |Brandon Presser |December 19, 2014 |DAILY BEAST 

Two hundred girls are weaving in and out of dirty alleys in the seaside slum of West Point, Liberia. Meet the Liberian Girls Beating Ebola |Abby Haglage |October 29, 2014 |DAILY BEAST 

The glamour of the seaside resort has long since been eclipsed by spectacular violence. Trading Dime Bags for Salvador Dali |Jason McGahan |October 19, 2014 |DAILY BEAST 

In the tiny seaside town of Yacahts, Oregon, Buck Henderson is ready to die. Are Routine Scans Causing Cancer? |Dale Eisinger |September 17, 2014 |DAILY BEAST 

Set a year after the rest of the season, the staff takes an outing to the seaside. How Downton Abbey's Joanne Froggatt Navigated Anna's Controversial Rape Arc |Kevin Fallon |August 14, 2014 |DAILY BEAST 

In Seaside Park there is also a soldiers' and sailors' monument, and in the vicinity are many fine residences. Encyclopaedia Britannica, 11th Edition, Volume 4, Part 3 |Various 

And at his seaside lodgings he examined more narrowly the portion whence the pattern had been taken. A Thin Ghost and Others |M. R. (Montague Rhodes) James 

In fine weather there is always a charm about the seaside, even on the barest and least picturesque coast. Robin Redbreast |Mary Louisa Molesworth 

It is quite a little seaside place, where people who want to be quiet go for bathing in the summer. Robin Redbreast |Mary Louisa Molesworth 

It invited Rhoda, if her father would consent, to visit the Bodkins during the remainder of their stay at the seaside. A Charming Fellow, Volume II (of 3) |Frances Eleanor Trollope