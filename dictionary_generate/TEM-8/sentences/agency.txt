“Letting the most junior members set the agenda might be a good philosophy for a Montessori preschool, but it is no way to run a federal agency,” Barr said in a speech to an event hosted by conservative Hillsdale College. William Barr’s eyebrow-raising ‘robber barons’ metaphor for the Trump era |Aaron Blake |September 17, 2020 |Washington Post 

Using NBCU’s program would also have the advantage of being able to dig deeper into NBCU’s contribution to an advertiser’s business results, according to agency executives. NBCUniversal tests new measurement program to prove it can push product sales for advertisers |Tim Peterson |September 17, 2020 |Digiday 

Privacy and security were important considerations before committing to use the product, said Paul Davison, Vice Media Group vice president of agency development, for international in statement. ‘Re-architecting the entire process’: How Vice is preparing for life after the third-party cookie |Lara O'Reilly |September 17, 2020 |Digiday 

Although a longer term may help agencies recruit, it still would not provide the individual with the security of a permanent position, she said. Nonpermanent federal workers could be hired for up to 10 years under Trump proposal |Eric Yoder |September 16, 2020 |Washington Post 

The upfront held to its traditional calendar-year model, and most of the money committed has gone to TV networks for ads that will air on linear TV, according to agency executives. How the future of TV and streaming has – and hasn’t – been reshaped so far by 2020 |Tim Peterson |September 16, 2020 |Digiday 

Something like fluoride, which is too small for normal filters, yanks away that feeling of agency. Anti-Fluoriders Are The OG Anti-Vaxxers |Michael Schulson |July 27, 2016 |DAILY BEAST 

Wisner continues to feel angry toward the agency because she believes she was misled. Judge: Rehoming Kids Is Trafficking |Tina Traster |December 30, 2014 |DAILY BEAST 

“Mostly people on a budget use it,” Franz Dobersberger, managing director of a Bangkok travel agency, told The Daily Beast. The Presumed Crash of AirAsia Flight QZ8501 Is Nothing Like MH370 |Lennox Samuels |December 29, 2014 |DAILY BEAST 

Larson, as usual, instills gravitas and agency in an otherwise underwritten character. Brie Larson’s Hollywood Transformation |Marlow Stern |December 29, 2014 |DAILY BEAST 

Essentially, we are being left in a position where we are expected to just take agency promises at face value. No, North Korea Didn’t Hack Sony |Marc Rogers |December 24, 2014 |DAILY BEAST 

Having thus enrolled himself as one of the Agency Indians, he had a claim on this the Agency doctor. Ramona |Helen Hunt Jackson 

Yet he succeeded in getting many to take the agency and these received orders and sent for the books. The Homesteader |Oscar Micheaux 

Indeed, in many cases a general agency requires the employment of many persons to execute the business. Putnam's Handy Law Book for the Layman |Albert Sidney Bolles 

In many cases an agency is created for an indefinite period, and in these either party can terminate it whenever he desires. Putnam's Handy Law Book for the Layman |Albert Sidney Bolles 

The death of either agent or principal terminates the agency except in cases of personal interest. Putnam's Handy Law Book for the Layman |Albert Sidney Bolles