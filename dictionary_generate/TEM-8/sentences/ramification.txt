Financial ramifications will likely be felt by studios, filmmakers, theater owners, and more for months or even years. How the coronavirus outbreak is roiling the film and entertainment industries |Alissa Wilkinson |September 11, 2020 |Vox 

All of Twitter’s statements, for example, were written by employee resource groups—but, as the Washington Post has reported, this work was often unpaid, fell outside employees’ normal duties, and had potential negative ramifications for them. What’s missing from corporate statements on racial injustice? The real cause of racism. |Amy Nordrum |September 5, 2020 |MIT Technology Review 

She argued MTS should not deny her or others with mental health conditions that can have ramifications as dramatic as physical disabilities. MTS Frequently Overrules Doctors’ Orders on Reduced Fares for the Disabled |Lisa Halverstadt |August 31, 2020 |Voice of San Diego 

Anyone living in a liberal democracy should be concerned about the ramifications this has for freedoms and privacy. How China surveils the world |Tate Ryan-Mosley |August 19, 2020 |MIT Technology Review 

Lately, corporate boards have begun to worry about the ethical ramifications of turning so much power over to the machines. Corporate execs are starting to get skittish about AI |Nicolás Rivero |July 20, 2020 |Quartz 

In nothing do the sexes differ more materially than in the ramification of these organs, and their plumage. An Introduction to Entomology: Vol. III (of 4) |William Kirby 

These troublesome details are given in the hope that they may show how far the ramification of trade competition extends. Railroads: Rates and Regulations |William Z. Ripley 

During the years preceding 1250, the ramification of the ribs grew very complicated. How France Built Her Cathedrals |Elizabeth Boyle O'Reilly 

The peculiarities and variations which so often appear in the ramification need not be discussed here. Fungi: Their Nature and Uses |Mordecai Cubitt Cooke 

Consider how such ramification will appear in one of the bud groups, that of our old friend the oak. Modern Painters, Volume V (of 5) |John Ruskin