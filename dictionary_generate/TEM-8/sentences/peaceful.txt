Which is another reason for Putin to find some way to bring Belarusian protests to a peaceful end, with or without Lukashenko. Poison, Protest and the Power of Putin |Tracy Moran |September 4, 2020 |Ozy 

America’s government repeatedly fails to provide meaningful avenues for peaceful change. Kenosha’s looting is a symptom of a decrepit democracy |Aaron Ross Coleman |September 4, 2020 |Vox 

Most recently, some members of the US Congress wrote a petition calling on the US secretary of state to encourage the Ethiopian government to engage in an open dialog with the opposition for a peaceful transition. Ethiopia’s escalating regional political crisis has roots in its federal system |Mulugeta G Berhe |September 3, 2020 |Quartz 

Being in the redwoods and watching them burn, it’s amazing how peaceful it could be at times. How We Saved Our Neighborhood from a Wildfire |Dain Zaffke |September 2, 2020 |Outside Online 

I discovered—to my surprise—that I enjoyed how peaceful the airport could be at night. What I learned from 5 years of cleaning airplanes in the middle of the night |matthewheimer |August 30, 2020 |Fortune 

Instead, he is cruelly jailed solely for the peaceful expression of his beliefs. Behind Bars for the Holidays: 11 Political Prisoners We Want to See Free In 2015 |Movements.Org |December 25, 2014 |DAILY BEAST 

To give the truth its rights, you have to see that groups like TMOC were threatened by peaceful protest. The Monsters Who Screamed for Dead Cops |Jacob Siegel |December 23, 2014 |DAILY BEAST 

The petroleum industry has depicted fracking as a few antiseptic drills dug on peaceful farmland. New York’s Conservative Fracking Ban |Jay Michaelson |December 20, 2014 |DAILY BEAST 

In the past, union officials have appeared at numerous peaceful protests in New York and across the country. The High-Priced Union Rep Charged With Attacking a Cop |Jacob Siegel |December 19, 2014 |DAILY BEAST 

The demonstrations have been largely peaceful, with fewer arrests and clashes with police than in other cities. Eric Garner Protesters Have a Direct Line to City Hall |Jacob Siegel |December 11, 2014 |DAILY BEAST 

Every leader was killed, and every peaceful native whom the Spaniards met on their way was unmercifully treated. The Philippine Islands |John Foreman 

This information was balm to Louis, as it seemed to promise a peaceful termination to so threatening an affair. The Pastor's Fire-side Vol. 3 of 4 |Jane Porter 

Nevertheless, both our arrival that evening and our landing the next morning were very quiet and peaceful. The Jesuit Relations and Allied Documents, Vol. II: Acadia, 1612-1614 |Various 

The place wore an entirely peaceful air, strong and massive though it appeared. St. Martin's Summer |Rafael Sabatini 

Last night again there was all sorts of firing and fighting going on, throughout those hours peaceful citizens ear-mark for sleep. Gallipoli Diary, Volume I |Ian Hamilton