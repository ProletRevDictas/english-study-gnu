He demanded to know where she was and what she was doing if she was silent too long. She Was Afraid of Her Lawyer. Then the Text Messages Started. |by Samantha Hogan, The Maine Monitor |October 8, 2020 |ProPublica 

Richard is rarely as vicious as Emily can be — he’s more prone to silent lack of interest — but when he decides to talk, he can shut Lorelai up in a way Emily can never manage to do. Every episode of Gilmore Girls, ranked |Constance Grady |October 6, 2020 |Vox 

The employees who saw him steal from the hospital stayed silent even as police began investigating. Maine Hires Lawyers With Criminal Records to Defend Its Poorest Residents |by Samantha Hogan, The Maine Monitor, with data analysis by Agnel Philip |October 6, 2020 |ProPublica 

In private online forums they discuss their preferred lies for why their pretend call center seems so silent. Meet the Customer Service Reps for Disney and Airbnb Who Have to Pay to Talk to You |by Ken Armstrong, Justin Elliott and Ariana Tobin |October 2, 2020 |ProPublica 

Being small, generally silent creatures, they’re easy to miss. What hundreds of pickled frog carcasses can tell us about their enormous eyes |María Paula Rubiano A. |October 1, 2020 |Popular-Science 

By tradition, the speaker of the House never participates in debates in the House and remains silent. The YOLO Caucus' New Cry for Attention |Ben Jacobs |January 4, 2015 |DAILY BEAST 

As we waited for my plane to come in, we stayed silent for a long time. The Story Behind Lee Marvin’s Liberty Valance Smile |Robert Ward |January 3, 2015 |DAILY BEAST 

And that gets to the heart of what makes the game so incredible: By staying silent, it turns the player into the game master. Lost For Thousands of Strokes: 'Desert Golfing' Is 'Angry Birds' as Modern Art |Alec Kubas-Meyer |January 2, 2015 |DAILY BEAST 

Many will simply stay away from crowds and stay home this Christmas Eve, which could be a very silent night indeed. France’s Wave of Crazy-Terror Christmas Attacks |Christopher Dickey |December 24, 2014 |DAILY BEAST 

The house was eerily silent on a Friday morning after a huge party. I Was Gang Raped at a UVA Frat 30 Years Ago, and No One Did Anything |Liz Seccuro |December 16, 2014 |DAILY BEAST 

After a minute's pause, while he stood painfully silent, she resumed in great emotion. The Pastor's Fire-side Vol. 3 of 4 |Jane Porter 

Here began indeed, in the drab surroundings of the workshop, in the silent mystery of the laboratory, the magic of the new age. The Unsolved Riddle of Social Justice |Stephen Leacock 

The lovers got up, with only a silent protest, and walked slowly away somewhere else. The Awakening and Selected Short Stories |Kate Chopin 

No; there I stood, half-astonished, half-abashed while the Marquise continued on her knees and made her silent orisons. Blackwood's Edinburgh Magazine, Volume 60, No. 372, October 1846 |Various 

He was rather silent, they observed; but the young clergyman, who made the fourth at the table, was voluble by nature. Elster's Folly |Mrs. Henry Wood