He tried another, and the bleep this time was far more insistent (and more alarming). Borana Joins the Fight to Save Kenya’s Rhinos…and Wants You to Help Too |Joanna Eede |February 18, 2014 |DAILY BEAST 

If you listened closely during the bleep, you could faintly hear Jacqueline Bissett still giving her speech. Golden Globes Recap: All the Best, Weirdest Moments You (May Have) Missed |Kevin Fallon |January 13, 2014 |DAILY BEAST 

Did Miss New York call current Miss America 'fat as [bleep]'? Julianne Moore Cast in ‘Hunger Games,’ Miss New York Calls Miss America Fat |Culture Team |September 13, 2013 |DAILY BEAST 

Quinn hugged the woman and bellowed, “They may have to bleep ya!” As Christine Quinn Fades, Why Aren’t More Women Winning? |David Freedlander |September 8, 2013 |DAILY BEAST 

Scenes like these are replayed daily on TV shows like Who the (Bleep) Did I Marry? Paula Broadwell, Eminem, & More Spurned Lovers Who Went Ballistic |Paula Froelich |November 15, 2012 |DAILY BEAST