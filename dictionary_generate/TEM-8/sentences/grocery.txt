There have already been at least 100 grocery worker deaths and thousands of grocery workers infected or exposed to the virus. The Pandemic Hasn’t Gone Away – Neither Should Hazard Pay |Todd Walters and Brent E. Beltrán |August 20, 2020 |Voice of San Diego 

They see people shouting in grocery stores, unwilling to wear a mask to keep us all safe. ‘He is clearly in over his head’: Read Michelle Obama’s full speech denouncing Donald Trump |kdunn6 |August 18, 2020 |Fortune 

For groceries, people want to know if they can buy online and pick up in-store. SEO in the second half of 2020: Five search opportunities to act on now |Jim Yu |August 17, 2020 |Search Engine Watch 

A full month after falling ill, she attempted to go to grocery shopping and ended up in bed for days. Covid-19 “long haulers” are organizing online to study themselves |Tanya Basu |August 12, 2020 |MIT Technology Review 

Thanks to the pandemic, the term “essential workers” now means everyone from health care personnel to transit operators to grocery store employees who must go to work despite shutdowns. Is School Out Forever? |Daniel Malloy |August 9, 2020 |Ozy 

The police learned that Kemp worked in a grocery on Decatur Avenue. Shot Down During the NYPD Slowdown |Michael Daly |January 7, 2015 |DAILY BEAST 

Here they are semi-touching at a grocery store; she likes kombucha. All Your Internet Boyfriends Are Taken: Gosling, Cumberbatch, and now Joseph Gordon-Levitt |Melissa Leon |January 3, 2015 |DAILY BEAST 

He was born in an apartment above the grocery store owned by his immigrant parents in South Jamaica, Queens. Mario Cuomo, a Frustrating Hero to Democrats, Is Dead at 82 |Eleanor Clift |January 2, 2015 |DAILY BEAST 

The people you work with, or see at your grocery store, or your church? Obama Is Right on Race. The Media Is Wrong. |Keli Goff |December 29, 2014 |DAILY BEAST 

While grocery shopping a guy I had noticed following me earlier, walked by me really fast and said, ‘You look shorter in person.’ Porn Stars on the Year in Porn: Drone Erotica, Belle Knox, and Wild Sex |Aurora Snow |December 27, 2014 |DAILY BEAST 

Jess, whose heart was still sore from the blow she had received at Mr. Closewicks grocery, thought this was very kind of Griff. The Girls of Central High on the Stage |Gertrude W. Morrison 

Mothers money comes so very irregular that we have to run a bill at the grocery and the market and other places. The Girls of Central High on the Stage |Gertrude W. Morrison 

It was worse than being refused credit at Mr. Closewicks grocery store. The Girls of Central High on the Stage |Gertrude W. Morrison 

The establishment is really beautiful, having the appearance more of an apothecary store, than a Grocery House. The Condition, Elevation, Emigration, and Destiny of the Colored People of the United States |Martin R. Delany 

He throws out these materials and insists on his wife getting others at another grocery store. Essays In Pastoral Medicine |Austin Malley