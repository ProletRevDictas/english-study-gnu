You can say this verbatim because it doesn’t put his chef-feelings in play. Carolyn Hax: How to improve on ‘I cook, you clean’ without stirring the pot |Carolyn Hax |February 12, 2021 |Washington Post 

Also working in the Capitals’ favor has been an unexpected week off — the result of three games postponed because of coronavirus issues — after less than a month of play. For first time in three weeks, Capitals could be at full strength Sunday |Samantha Pell |February 11, 2021 |Washington Post 

There is at least some indication that Williams might also be struggling with her very next stroke following a return hit in play. How Serena Williams Could Finally Break The Grand Slam Record |Amy Lundy |February 10, 2021 |FiveThirtyEight 

So far during Big Ten play, 17 men’s basketball games have been postponed. Big Ten moves men’s basketball tournament to Indianapolis |Emily Giambalvo |February 9, 2021 |Washington Post 

He believed his front seven could stop running plays even with the safeties backed up. What went wrong for the Chiefs and Patrick Mahomes in a brutal Super Bowl defeat |Adam Kilgore |February 8, 2021 |Washington Post 

Have there been discussions with FX regarding an Archer movie, and how do you think that would play out? ‘Archer’ Creator Adam Reed Spills Season 6 Secrets, From Surreal Plotlines to Life Post-ISIS |Marlow Stern |January 8, 2015 |DAILY BEAST 

Father Joel Román Salazar died in a car crash in 2013; his death was ruled an accident, but the suspicion of foul play persists. Mexico’s Priests Are Marked for Murder |Jason McGahan |January 7, 2015 |DAILY BEAST 

He plays an aging punk rocker and I play the drummer from his old band. Coffee Talk with Fred Armisen: On ‘Portlandia,’ Meeting Obama, and Taylor Swift’s Greatness |Marlow Stern |January 7, 2015 |DAILY BEAST 

When fathers hold and play with their children, oxytocin and prolactin kick in, priming them for bonding. How Good Dads Can Change the World |Gary Barker, PhD, Michael Kaufman |January 6, 2015 |DAILY BEAST 

Ironically, the play deals with the ‘management’ of information by the Establishment. Harry’s Daddy, and Diana’s ‘Murder’: Royal Rumors In a New Play |Tom Sykes |January 4, 2015 |DAILY BEAST 

I assure you, no matter how beautifully we play any piece, the minute Liszt plays it, you would scarcely recognize it! Music-Study in Germany |Amy Fay 

But I hope at least to play to him a few times, and what is more important, to hear him play repeatedly. Music-Study in Germany |Amy Fay 

To fill up the time till Liszt came, our hostess made us play, one after the other, beginning with the latest arrival. Music-Study in Germany |Amy Fay 

Again the sallow fingers began to play with the book-covers, passing from one to another, but always slowly and gently. Bella Donna |Robert Hichens 

Her attachment to impressionism leads this artist to many experiments in color—or, as one critic wrote, "to play with color." Women in the fine arts, from the Seventh Century B.C. to the Twentieth Century A.D. |Clara Erskine Clement