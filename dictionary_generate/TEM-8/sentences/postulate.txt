Even people who postulate a creative God usually acknowledge that his existence shifts the big question rather than resolving it. “Why Does the World Exist?” by Jim Holt: Review |Anthony Gottlieb |July 17, 2012 |DAILY BEAST 

While the war lasted, so he laid down, there must—apart from the postulate of Unity—be a truce to party struggles. The Life of Mazzini |Bolton King 

He receives as a postulate that which I must have demonstrated. Blackwood's Edinburgh Magazine - Volume 55, No. 343, May 1844 |Various 

But I disagree with them all, because they postulate the idea that time is constantly being manufactured. The Day Time Stopped Moving |Bradner Buckner 

Mark Twain's early life, however imperfectly recorded, exemplifies this postulate. Mark Twain, A Biography, 1835-1910, Complete |Albert Bigelow Paine 

All theologians and some metaphysicians postulate a fifth state of life, the divine, placing it above the rest as their source. Communism and Christianism |William Montgomery Brown