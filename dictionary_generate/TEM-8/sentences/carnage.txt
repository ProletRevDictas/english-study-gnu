Excessive speeding was cited as a leading contributor to the carnage. Traffic counts fell during the coronavirus pandemic, but road fatalities still increased |Luz Lazo |February 12, 2021 |Washington Post 

So that’s a little something to keep in mind as we survey the carnage. The 11 Weirdest Golden Globe Nominations—And What Should Have Been Nominated Instead |Eliana Dockterman |February 3, 2021 |Time 

CNBC has a longer list if you want to feast your eyes on the carnage. In 2021 everyone gets 15 minutes of wealth |Alex Wilhelm |February 2, 2021 |TechCrunch 

No, he found it in one of Saddam Hussein’s Baghdad palaces in 2004, where the rubble and carnage of the early years of America’s war on terror took a toll on Trotter’s mind. This couple hit their stride after years of hard work. Then covid-19 hit them. |Petula Dvorak |December 3, 2020 |Washington Post 

I just came back and saw the carnage and went down and saw the same. Beirut explosion nearly destroys LGBTQ group’s offices |Michael K. Lavers |August 10, 2020 |Washington Blade 

Female members have been involved in the carnage for the past two years, but never in such an active role. The New Face of Boko Haram’s Terror: Teen Girls |Nina Strochlic |December 13, 2014 |DAILY BEAST 

They logged every incident and released depressing day-by-day accounts of the carnage. ISIS Fighters Are Killing Faster than Statisticians Can Count |Peter Schwartzstein |December 5, 2014 |DAILY BEAST 

Here's what we know so far about the man allegedly behind the carnage. What We Know About the FSU Shooter |Caitlin Dickson |November 20, 2014 |DAILY BEAST 

When it comes to videos of the carnage that has cost at least 200,000 lives in Syria, few sources have been completely reliable. Digital Doublethink: Playing Truth or Dare with Putin, Assad and ISIS |Christopher Dickey, Anna Nemtsova |November 16, 2014 |DAILY BEAST 

This astonishingly simple yet devastatingly graphic representation of mass carnage attracts many thousands of visitors every day. Blood and War: The Hard Truth About ‘Boots on the Ground’ |Clive Irving |September 22, 2014 |DAILY BEAST 

Only in the carnage of the head, the tilt of the chin, was the insolence expressed that had made her many enemies. Ancestors |Gertrude Atherton 

On either side the carnage had been terrible, and the pathways of the village were literally choked with the dead. The Every Day Book of History and Chronology |Joel Munsell 

The sight of the awful carnage affected even the warworn Marshal, and made him exclaim, "What a massacre!" Napoleon's Marshals |R. P. Dunn-Pattison 

The carnage was awful, and the charging columns halted, staggered, and then began to reel back. The Courier of the Ozarks |Byron A. Dunn 

But even in that moment he asked himself for the first time since the commencement of that carnage—to what purpose? St. Martin's Summer |Rafael Sabatini