The third quarter of 2015 was the first quarterly period in five years in which he sold no shares. Under Armour founder sold $138 million in stock during time period company allegedly misled investors about slowing sales |Douglas MacMillan |May 13, 2021 |Washington Post 

That number will likely rise given Nigeria has also just suffered its worst quarterly economic contraction in over a decade due to the Covid-19 pandemic. How Africa’s largest economy can protect the future of its promising tech sector |Yomi Kazeem |September 21, 2020 |Quartz 

If Gentleman's Quarterly comes by for a photo spread, Palmer won't have to put a single sock in a hamper. Will the Real Jim Palmer Please Stand Up |Tom Boswell |September 27, 2014 |DAILY BEAST 

This was the worst quarterly economic performance since the first quarter of 2009. How Obamacare Helped Crash the Economy |Daniel Gross |June 25, 2014 |DAILY BEAST 

A bar chart showing quarterly GDP growth resembles the teeth of a saw, not a picket fence. The U.S. Economy Had a Hiccup, Not a Heart Attack, This Year |Daniel Gross |May 29, 2014 |DAILY BEAST 

Bernheimer is a well-known American fairy-tale writer, and Hunt is the editor of the Denver Quarterly. An Edward Hopper Novel? |The Daily Beast |April 18, 2014 |DAILY BEAST 

Its nominal charter was publishing, more or less quarterly, a humor magazine. Doug Kenney: The Odd Comic Genius Behind ‘Animal House’ and National Lampoon |Robert Sam Anson |March 1, 2014 |DAILY BEAST 

A lady, as a writer in the 'Quarterly Review' observes, should 'not only have but know her plants.' The Ladies' Book of Etiquette, and Manual of Politeness |Florence Hartley 

It wasn't paid quarterly or half-yearly; the manner in which it was drawn was sufficiently original. The Pit Town Coronet, Volume III (of 3) |Charles James Wills 

With the wind quarterly, she soon smoked out to the first flagboat, where 'Navahoe' was leader and 'Calluna' second. Yachting Vol. 2 |Various. 

“The Quarterly” onslaught on “Jane Eyre” appeared, and all the good things that had been said were forgotten. McClure's Magazine, Vol. 1, No. 2, July, 1893 |Various 

It had arrived at maturity about the time the diabolical article appeared in “The Quarterly.” McClure's Magazine, Vol. 1, No. 2, July, 1893 |Various