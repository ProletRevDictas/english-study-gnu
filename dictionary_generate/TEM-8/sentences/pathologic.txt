Pathologic secondary headaches are more serious and likely require brain imaging to determine the main cause of the pain. How to Destroy Your Headaches |Dr. Anand Veeravagu, MD, Tej Azad |June 23, 2014 |DAILY BEAST 

A marked increase indicates some pathologic condition at the site of their origin. A Manual of Clinical Diagnosis |James Campbell Todd 

Slight lymphocyte leukocytosis occurs in many other pathologic conditions, but is of little significance. A Manual of Clinical Diagnosis |James Campbell Todd 

The reaction is frankly acid in health and in nearly all pathologic conditions. A Manual of Clinical Diagnosis |James Campbell Todd 

There are also grouped fractures, the pathologic anatomy of which is similar. Lameness of the Horse |John Victor Lacroix 

Whatever be the cure adopted, we may promise ourselves the joy of the doctor called in to follow a beautiful pathologic case. The Foundations of Science: Science and Hypothesis, The Value of Science, Science and Method |Henri Poincar