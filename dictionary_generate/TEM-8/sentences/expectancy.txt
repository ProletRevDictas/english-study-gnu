The only good news in the new report is that life expectancy typically bounces back quickly, because of the way it is calculated. Pandemic cut U.S. life expectancy by a year during the first half of 2020 |Lenny Bernstein |February 18, 2021 |Washington Post 

It’s the largest decline in life expectancy for the United States since the early 1940s. The COVID-19 death toll sent U.S. life expectancy plunging in 2020 |Aimee Cunningham |February 18, 2021 |Science News 

From 1960 to 1980 life expectancy in the United States steadily increased. How to fix what the innovation economy broke about America |Katie McLean |February 17, 2021 |MIT Technology Review 

Life expectancy in Ward 8 is 72 — and just 68 in some neighborhoods — Nesbitt said, compared to 89 in more affluent neighborhoods elsewhere in the city. D.C. officials knock on doors to reach seniors amid push for vaccine equity |Julie Zauzmer, Hannah Natanson, Rebecca Tan |February 4, 2021 |Washington Post 

Little research on that question existed, so they conducted their own analysis using 67 years of data about unemployment, life expectancy, and death rates from the Bureau of Labor Statistics and the Centers for Disease Control and Prevention. The COVID recession may kill more Americans than COVID-19 does |Geoffrey Colvin |January 6, 2021 |Fortune 

In Afghanistan, the United States has contributed to increasing life expectancy from 44 years to 61 years. Politics End In Halifax As Democratic and GOP Senators Seek Common Ground on National Security |Tim Mak |November 22, 2014 |DAILY BEAST 

Other studies have found that professional football players have a life expectancy in the mid-50s. Has Football Jumped the Shark? |Kevin Canfield |September 1, 2014 |DAILY BEAST 

In the wild, cheetahs have a life expectancy of 10 to 12 years, according to National Geographic. The $10 Billion Pet Cheetah and Chimp Industry |Sharon Adarlo |July 20, 2014 |DAILY BEAST 

Yes, there is a compelling relationship between life expectancy and income here in the U.S. and worldwide. Why U.S. Health Care Is So Expensive and So Pathetic |Kent Sepkowitz |June 18, 2014 |DAILY BEAST 

Keep in mind that the average life expectancy of an American is 78. Colbert Is Ivy League TV Compared to Fox News |Dean Obeidallah |June 3, 2014 |DAILY BEAST 

It was theatrical: he stood upon the stage, an audience watching him with intent expectancy, wondering upon his decision. The Wave |Algernon Blackwood 

When she thought that he was there at hand, waiting for her, she grew numb with the intoxication of expectancy. The Awakening and Selected Short Stories |Kate Chopin 

Winston nodded as he went on, wondering with a grim expectancy whether Courthorne had returned again. Winston of the Prairie |Harold Bindloss 

There is an air of anxious expectancy about him, with a look of Semitic shrewdness in the long, narrow face. Prison Memoirs of an Anarchist |Alexander Berkman 

And the group rose to its feet; it broke up with little movements and murmurs, in a restrained, dignified expectancy. The Creators |May Sinclair