Indeed, scientists have linked khat consumption to increased risk of strokes and heart diseases. How the Pandemic Is Saving Lives in the Horn of Africa |Eromo Egbejule |September 3, 2020 |Ozy 

However, you can bring them back to sharpness with just a few strokes along a sharpening stone. Three Questions to Ask Yourself Before Buying a Knife |Wes Siler |September 3, 2020 |Outside Online 

So they adopted its broad strokes—including its limits on data collection and its requirements on data storage and data deletion—and then loosened some of its language. Inside China’s unexpected quest to protect data privacy |Tate Ryan-Mosley |August 19, 2020 |MIT Technology Review 

By itself, such a lopsided stroke would lead to swimming in circles. Human sperm don’t swim the way that anyone had thought |Jack J. Lee |July 31, 2020 |Science News 

Instead of swimming straight by twirling their tails like propellers, human sperm flick their tails lopsidedly and roll to balance out the off-center strokes. Human sperm don’t swim the way that anyone had thought |Jack J. Lee |July 31, 2020 |Science News 

With every stroke, her leather boot creaked under the weight of her leg. Dungeons and Genital Clamps: Inside a Legendary BDSM Chateau |Ian Frisch |December 20, 2014 |DAILY BEAST 

This video remedies that injustice, showcasing an owl doing a butterfly stroke in Lake Michigan. Swimming Owls, Jane Krakowski’s Peter Pan Live! Audition, and More Viral Videos |The Daily Beast Video |December 7, 2014 |DAILY BEAST 

And finally, when you ask for your car, your dress, whatever it is you want, stroke his hand. Russia’s Gold Digger Academy |Peter Pomerantsev |November 11, 2014 |DAILY BEAST 

In a stroke, and if his words are genuine, Tim Cook has just become Gay Superman. Tim Cook: Why ‘I’m Gay’ Isn’t Enough |Tim Teeman |October 30, 2014 |DAILY BEAST 

In a stroke of genius, he enlisted Bundy to vouch for him on tape. Cliven Bundy’s Brokeback Mountain Moment |Olivia Nuzzi |October 19, 2014 |DAILY BEAST 

I hope the French Government will recognize this dashing stroke of d'Amade's by something more solid than a thank you. Gallipoli Diary, Volume I |Ian Hamilton 

She did shout for joy, as with a sweeping stroke or two she lifted her body to the surface of the water. The Awakening and Selected Short Stories |Kate Chopin 

Her pulse was beneath his fingers, and with every stroke of it he felt more keenly the mystery and cruelty of life. Bella Donna |Robert Hichens 

They were afraid that it was too small; they then put another of 14 inches by the side of the first, the same stroke. Life of Richard Trevithick, Volume II (of 2) |Francis Trevithick 

At Wheal Alfred they have a 64-inch cylinder; the air-pump is 20 inches, and the stroke is half that of the engine. Life of Richard Trevithick, Volume II (of 2) |Francis Trevithick