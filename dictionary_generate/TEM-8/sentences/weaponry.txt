Australia’s reaching for antitrust weaponry presents a model for other countries of how to use competition laws to unlock big enough payments for journalism to survive. How Australia May Have Just Saved Journalism From Big Tech |Robert Whitehead |February 22, 2021 |Time 

As far as weaponry went, the YPG and the YPJ had only AK‑47s, a random smattering of heavy weapons, and some PKMs, a machine gun designed de‑ cades earlier. The Women Who Fought to Defend Their Homes Against ISIS |Gayle Tzemach Lemmon |February 22, 2021 |Time 

In “The Mandalorian” we’ve seen Beskar armor and weaponry fill such a role. What we want from Ubisoft Massive’s open world Star Wars game |Mike Hume, Gene Park, Elise Favis |January 13, 2021 |Washington Post 

People today have brains two to three times larger than the ancients’, a difference that fuels the invention of sophisticated hunting weaponry. Has technology made us worse hunters? |By Henry Bunn/Outdoor Life |October 27, 2020 |Popular-Science 

A modern hunter with knowledge of whitetail behavior and sophisticated modern weaponry can successfully ambush deer. Has technology made us worse hunters? |By Henry Bunn/Outdoor Life |October 27, 2020 |Popular-Science 

There are thousands of people ready to join the fighting over there but we are short on logistical support and weaponry. Dutch Biker Gangs Vs. ISIS |Nadette De Visser, Christopher Dickey |December 9, 2014 |DAILY BEAST 

The trade flouts a March 2014 prohibition on all exports of weaponry and military equipment to Moscow. Ukraine Militias Warn of Anti-Kiev Coup |Jamie Dettmer |November 28, 2014 |DAILY BEAST 

Without the proper equipment to repair and operate the Mohajer-4 it may be more of a photo prop than a piece of weaponry. ISIS: We Nabbed an Iranian Drone |Jacob Siegel |November 17, 2014 |DAILY BEAST 

The MiG-21 does not carry a huge amount of weaponry and was originally designed to fight other aircraft. U.S. Fighter Jocks Pray The ‘ISIS Air Force’ Rumors Are True |Dave Majumdar |October 21, 2014 |DAILY BEAST 

They are well-equipped thanks to the modern weaponry and tanks the ISIS fighters captured from the Iraqi army. Why Does the Free Syrian Army Hate Us? |Jamie Dettmer |October 3, 2014 |DAILY BEAST 

Remind me to report the maintenance gang of this hunk for stocking unauthorized weaponry. Attrition |Jim Wannamaker 

And all your weaponry against God will be as nothing when he rains upon you discomfiture from the heavens. New Tabernacle Sermons |Thomas De Witt Talmage 

We have met requests for reasonable amounts of American weaponry from regional countries which are anxious to defend themselves. Complete State of the Union Addresses from 1790 to 2006 |Various 

With it he built his home, framing a fortress capable of withstanding all the weaponry of his time. The Way to the West |Emerson Hough 

His weaponry we may know exactly, for his rifle can be seen to-day, preserved by his descendants. The Way to the West |Emerson Hough