The only unofficial personage who had entered the guest room since it was cleared for pictures was Lottie Jackson. The Strange and Mysterious Death of Mrs. Jerry Lee Lewis |Richard Ben Cramer |January 11, 2014 |DAILY BEAST 

A congenial dinner with a famous personage in public health. Paul Farmer Is Revolutionizing Medicine |Tracy Kidder |May 6, 2010 |DAILY BEAST 

"I knew I had to write something on Yves because Yves was a celebrated and mythical personage," said Bergé. Remembering YSL |Robert Murphy |March 24, 2010 |DAILY BEAST 

“I knew I had to write something on Yves because Yves was a celebrated and mythical personage,” said Pierre Bergé. Remembering YSL |Robert Murphy |March 24, 2010 |DAILY BEAST 

Even compared with his personage in Casino Royale, the 007 of Quantum is basically a grunting eunuch. Has Bond Lost His Balls? |Matthew Oshinsky |November 3, 2008 |DAILY BEAST 

He was also the one and only personage in the drama, concentrating on himself the attention of the audience. The Joyous Adventures of Aristide Pujol |William J. Locke 

At last Deacon MacNab, the church treasurer and a personage of importance, got a chance to speak. The Recent Revolution in Organ Building |George Laing Miller 

There were eunuchs too, black frock-coated—and the chief eunuch, an important personage who ranks very high. Fifty Years of Railway Life in England, Scotland and Ireland |Joseph Tatlow 

It was a daring trick to play upon so exalted a personage, but Vera had not hesitated to do it. The Weight of the Crown |Fred M. White 

The rebel leader was deep in converse with a richly-attired personage whom Frank discovered afterwards to be the Vizier. The Red Year |Louis Tracy