In addition, Pike notes that “pandemic, and the time off, opened many people’s eyes to so many injustices, inequities and racism in our world.” As You Are Bar offers a place to belong |Evan Caplan |April 8, 2021 |Washington Blade 

She said Wednesday that she will “definitely debut” another new skill, a Yurchenko double pike on vault, before Tokyo. Simone Biles, the world’s most dominant gymnast, has something new for Tokyo |Emily Giambalvo |April 8, 2021 |Washington Post 

Pike has played a version of this role before, as the carrara-cool wife in Gone Girl, and she’s not bad at it. We Need More Female Villains Onscreen—But Not the Kind We Get in I Care a Lot |Stephanie Zacharek |February 19, 2021 |Time 

We will have proven the ability of this technology to scale up manufacturing, and that scale-up of manufacturing will have implications not just for other vaccines, also for other mRNA medicines coming down the pike. How to Fix the Incentives in Cancer Research (Ep. 449) |Stephen J. Dubner |January 28, 2021 |Freakonomics 

The gunslinging 5-foot-4-inch president galloped on the pike toward Bladensburg. In 1814, British forces burned the U.S. Capitol |Joel Achenbach |January 6, 2021 |Washington Post 

With lights flashing, the cruiser arrived at the Blooming Grove State Police barracks in Pike County. Killer Eric Frein Held in Murdered Cop’s Cuffs |Michael Daly |October 31, 2014 |DAILY BEAST 

Tall, dark, and handsome Ben Affleck against icy blonde Rosamund Pike. David Fincher’s Backseat Feminism |Teo Bugbee |October 9, 2014 |DAILY BEAST 

There's a scene in which a nude Amy Elliott-Dunne, played with committed gusto by Rosamund Pike, is washing off in the shower. Yes, Ben Affleck Goes Full-Frontal in ‘Gone Girl,’ Confronting One of Cinema’s Last Taboos |Marlow Stern |October 1, 2014 |DAILY BEAST 

Check out the popular bakery Piroshky Piroshky in Pike Place Market for some Eastern European stuffed delights. Where to Celebrate the Olympics by Drinking Russian Style |William O’Connor |February 6, 2014 |DAILY BEAST 

Many probably believed he would emerge from the side carrying a pike with the head of a banker. Wall Street CEOs Say It’s The Best of Times and The Worst of Times |Daniel Gross |November 12, 2013 |DAILY BEAST 

If ever the cool impudence was suddenly taken out of a man, this question seemed to take it out of Pike. Elster's Folly |Mrs. Henry Wood 

After that Pike was a little more cautious, and kept aloof for a time; but Val knew that he was still watched on occasion. Elster's Folly |Mrs. Henry Wood 

"I was just thinking the same thing yesterday—that your lordship was always meeting me," said Pike. Elster's Folly |Mrs. Henry Wood 

Pike's head suddenly appeared above the hurdles, and he began inquiring after her health. Elster's Folly |Mrs. Henry Wood 

You'll get interfered with in a way you won't like, Pike, one of these days, unless you mend your manners. Elster's Folly |Mrs. Henry Wood