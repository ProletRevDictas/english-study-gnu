Now that the once-private, lonesome pursuit of long-distance running is an increasingly public exercise, there’s more incentive than ever to chronicle our successes and failures for an expectant readership. How Strava Shapes Our Running Stories |mmirhashem |October 22, 2021 |Outside Online 

Here again, the plane was at once a lonesome vessel, the people aboard facing their singular fate, and yet somehow already attached to the larger drama, connected again by cellphones. Sept. 11, 2001: An ordinary work day, then surreal scenes of dread and death |David Maraniss |September 10, 2021 |Washington Post 

The out-and-back trail has parking lots on both ends and isn’t very strenuous—you can take the time to reflect and enjoy a lush hike by your lonesome. Ten Ideal Hikes for the Solo Adventurer |syadron |July 30, 2021 |Outside Online 

In this lonesome environment, they’re expected to plan their post-graduation future. Applying to College Was Never Easy for Most Students. The Pandemic Made It Nearly Impossible |Katie Reilly |March 31, 2021 |Time 

A quarter century later, Duvall has reteamed with Lonesome Dove screenwriter William D. Witliff for A Night in Old Mexico. Robert Duvall on His Storied Career, His New Movie, and Why He’s Ditching the GOP |Marlow Stern |March 13, 2014 |DAILY BEAST 

Many view the Duvall-starring miniseries, Lonesome Dove, as one of the greatest westerns ever. Robert Duvall on His Storied Career, His New Movie, and Why He’s Ditching the GOP |Marlow Stern |March 13, 2014 |DAILY BEAST 

It's a blog in search of lonesome, twangy music that sounds old even if it isn't. The Best Music Blogs |Howard Wolfson |January 26, 2014 |DAILY BEAST 

Many of your books, from Lonesome Dove to Horseman, Pass By, have been adapted for the screen. Larry McMurtry: How I Write |Noah Charney |April 24, 2013 |DAILY BEAST 

It only affected what I chose to write afterwards in terms of the other three books in the Lonesome Dove tetralogy. Larry McMurtry: How I Write |Noah Charney |April 24, 2013 |DAILY BEAST 

But there was nothing in the trough to eat, as yet, and the box seemed quite lonesome, for Squinty was all alone. Squinty the Comical Pig |Richard Barnum 

The little comical pig was rather lonesome after Slicko had left him, but he was no longer hungry, thanks to the acorns. Squinty the Comical Pig |Richard Barnum 

The sequestered spot, a seat beneath a plane tree, with a lonesome arc-lamp shining full upon it, was occupied. The Joyous Adventures of Aristide Pujol |William J. Locke 

I would not like to live in such a lonesome place, and old Rushmere, the only man on the premises of a night. The World Before Them |Susanna Moodie 

But after a while everything seemed still, and Fleetfoot began to feel lonesome. The Later Cave-Men |Katharine Elizabeth Dopp