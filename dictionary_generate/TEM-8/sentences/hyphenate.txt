The idea of the multi-hyphenate creator was something that came after I had started working in this industry, so it was eye-opening and exciting that all of these avenues were available to me to express myself. Natasha Rothwell Says Goodbye to Insecure—And Hello to the Future |Cady Lang |December 27, 2021 |Time 

Rihanna is the poster child for the millennial multi-hyphenate. Sally Rooney and the Art of the Millennial Novel |Cady Lang |September 3, 2021 |Time 

For the contemporary sports superstar as multi-hyphenate, Jordan is the blueprint and he’s set the bar high. Debating How Space Jam: A New Legacy Stacks Up Against the Original |Cady Lang |July 19, 2021 |Time 

Unlike Gibson, Tom Cruise is not a multi-hyphenate who can retreat behind the camera. Tom Cruise's Career Rehab Secrets |Kim Masters |March 11, 2010 |DAILY BEAST 

Index entries tend to not hyphenate words that are unhyphenated in the text. The Life of Isaac Ingalls Stevens, Volume II (of 2) |Hazard Stevens 

Frequently the printer made an mistake and forgot to hyphenate all or part of a word. Young Folks' Bible in Words of Easy Reading |Josephine Pollard 

Hyphenate compounds of great in phrases indicating degrees of descent; great-grandmother, great-great-grandfather. Compound Words |Frederick W. Hamilton 

Hyphenate compounds of life and world; life-history, world-influence, but (by exception) lifetime. Compound Words |Frederick W. Hamilton 

Hyphenate compounds of master; master-builder, master-stroke, but (by exception) masterpiece. Compound Words |Frederick W. Hamilton