If Huckabee runs, the hurdles he faced the last time out, namely geography and money, would still be there. Can Huckabee Convert the GOP’s Moneymen? |Lloyd Green |January 4, 2015 |DAILY BEAST 

The possibility that the same outcome could happen another way -- namely a guy asks me out -- keeps me from taking action. Random Hook-Ups or Dry Spells: Why Millennials Flunk College Dating |Ellie Schaack |January 1, 2015 |DAILY BEAST 

There is a notion of reincarnation; namely, that our children are really returning ancestors. Joseph Campbell on the Roots of Halloween |Joseph Campbell |October 31, 2014 |DAILY BEAST 

This is a common theme in the comedy being launched at ISIS; namely, making the point that ISIS is truly not Islamic. Middle East Goes Monty Python on ISIS |Dean Obeidallah |October 29, 2014 |DAILY BEAST 

He only wants to protect his immediate universe, namely his wife and son. The Walking Dead’s Luke Skywalker: Rick Grimes Is the Perfect Modern-Day Mythical Hero |Regina Lizik |October 28, 2014 |DAILY BEAST 

This slaughter is accompanied by the tabagie and what follows it—namely, the singing and dancing. The Jesuit Relations and Allied Documents, Vol. II: Acadia, 1612-1614 |Various 

And I am told that the governor ordained what had to be done, namely, to make no investigations against the dead woman. The Philippine Islands, 1493-1898, Volume XX, 1621-1624 |Various 

Inside the walls of Manila there is only one Spanish parochial church, namely, the cathedral. The Philippine Islands, 1493-1898, Volume XX, 1621-1624 |Various 

All these different signs prepared us for the most interesting moment of our voyage—namely, passing the line. A Woman's Journey Round the World |Ida Pfeiffer 

On the 2nd of November I saw a festival of another description—namely, a religious one. A Woman's Journey Round the World |Ida Pfeiffer