So if you’re a left-leaning politician who throws a leg over a rig, you’re setting yourself up as a weakling, or a hypocrite, or a reckless fool. Why Can’t Politicians Ride Bikes? |Eben Weiss |May 13, 2021 |Outside Online 

In the local TV interview he did, Thomas juxtaposed McCarthy directly with another central California Republican in the House, David Valadao, as “hypocrites and heroes.” Kevin McCarthy’s Gamble on a “Big Tent” GOP |Lissandra Villa |April 23, 2021 |Time 

We all know that Republicans are indeed hypocrites when it comes to debt and deficits. How Democrats should wage war on coming GOP obstructionism |Paul Waldman |November 30, 2020 |Washington Post 

Democrats have accused Republicans of being hypocrites, because of their refusal in 2016 to consider then-President Barack Obama’s nomination of Merrick Garland. Barrett confirmation hearing day four: ‘We have the votes,’ McConnell says of nomination |Seung Min Kim, Donna Cassata, Karoun Demirjian |October 15, 2020 |Washington Post 

There’s even less reason for LGBTQ voters to support him, no matter what the hypocrites at Log Cabin tell you. Vote for Biden (duh) |Kevin Naff |September 10, 2020 |Washington Blade 

Does wildlife campaigner Prince Charles's hunting habit make him a hypocrite? Prince Charles Photographed Shooting, Charges of Animal Cruelty and Royal Hypocrisy Reignited |Tom Sykes |December 1, 2014 |DAILY BEAST 

Arkansas Congressman Tom Cotton is a dangerous man and a hypocrite. Election 2014 Is Drowning in Dark Money |Robert Maguire |August 28, 2014 |DAILY BEAST 

Eric Cantor was a noxious, cookie-cutter, U.S. Chamber, GOP hypocrite. Now Let’s Replace All the Other Big-Spending Eric Cantors |Nick Gillespie |June 11, 2014 |DAILY BEAST 

Therefore, if a liberal makes too much money advocating on behalf of the poor, she or he becomes a hypocrite. That’s All a Nobel Prize Winner Gets Paid? |Michael Tomasky |April 18, 2014 |DAILY BEAST 

Your criticism of me as a hypocrite is lame, weak and not really thought out. Spike Lee Blasts The New York Times’ Story on Brooklyn Gentrification in Fiery Op-Ed |Marlow Stern |March 31, 2014 |DAILY BEAST 

The true man stands out in his native dignity and the gilding is rubbed off the hypocrite. Gospel Philosophy |J. H. Ward 

Thou hypocrite, cast out first the beam out of thy own eye, and then shalt thou see to cast out the mote out of thy brother's eye. The Bible, Douay-Rheims Version |Various 

But while she cared little for his adulations, she did not because of them consider him a scoundrel, nor necessarily a hypocrite. Overland |John William De Forest 

Three shall not enter Paradise—the scoffer, the hypocrite, and the slanderer. Hebraic Literature; Translations from the Talmud, Midrashim and Kabbala |Various 

But in the priestly city, where education consists in being taught to play the hypocrite and to lie, traitors abound. Rule of the Monk |Giuseppe Garibaldi