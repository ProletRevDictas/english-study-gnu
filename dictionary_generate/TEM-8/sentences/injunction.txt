Federal District Judge Yvonne Gonzalez Rogers heard arguments this morning regarding Epic's request for a temporary injunction in its case against Apple. Apple v. Epic hearing previews a long, hard-fought trial to come |Kyle Orland |September 28, 2020 |Ars Technica 

Separately, pending further review, a federal appeals court on Sunday stayed a lower court’s injunction that would allow mail ballots in Wisconsin to count if postmarked by Election Day and received up to six days later. Courts view GOP fraud claims skeptically as Democrats score key legal victories over mail voting |Elise Viebeck |September 28, 2020 |Washington Post 

The judge refused to grant an injunction against a November deadline for a sale. TikTok will not be banned at midnight, judge rules |Claire Zillman, reporter |September 28, 2020 |Fortune 

In granting the preliminary injunction, the judge said the plaintiffs were likely to succeed at a trial. Census must continue for another month, judge says |Verne Kopytoff |September 25, 2020 |Fortune 

“I will issue a preliminary injunction essentially in the form presented by the states,” Bastian said in court. Federal judge issues temporary injunction against USPS operational changes amid concerns about mail slowdowns |Elise Viebeck, Jacob Bogage |September 17, 2020 |Washington Post 

They prevailed last August, obtaining—follow me here—an injunction prohibiting the enforcement of those provisions. The Back Alley, Low Blow-Ridden Fight to Stop Gay Marriage in Florida Is Finally Over |Jay Michaelson |January 5, 2015 |DAILY BEAST 

The injunction, she argued, only applies to these four plaintiffs—not to anyone else. The Back Alley, Low Blow-Ridden Fight to Stop Gay Marriage in Florida Is Finally Over |Jay Michaelson |January 5, 2015 |DAILY BEAST 

It is, after all, only reviewing a decline of a stay of an injunction to stop withholding licenses. Gay Marriage Chaos Begins |Jay Michaelson |November 11, 2014 |DAILY BEAST 

The fact that some prescriptive rules are valuable does not mean that every grammatical injunction should be obeyed. Go Ahead, End With a Preposition: Grammar Rules We All Can Live With |Nick Romeo |November 3, 2014 |DAILY BEAST 

The winning injunction prevents the sale from taking place and almost ensures the lawsuit will go to trial. Kate Moss Advises Younger Sister to Drop Out of School; Rachel Roy Wins Preliminary Hearing With Parent Company |The Fashion Beast Team |May 19, 2014 |DAILY BEAST 

Now, quite alone and safe, she asked herself whether she had been a fool to obey Nigel's injunction and to trust her own beauty. Bella Donna |Robert Hichens 

But a trade dispute of long standing was not settled, even in the seventeenth century, by a royal injunction. A History of the Cambridge University Press |S. C. Roberts 

According to Walpole, an injunction was applied for to prevent the publication of the letters. Private Letters of Edward Gibbon (1753-1794) Volume 1 (of 2) |Edward Gibbon 

Up, and this morning comes Mr. Clerke, and tells me that the Injunction against Trice is dismissed again, which troubles me much. Diary of Samuel Pepys, Complete |Samuel Pepys 

Christianity, not satisfied with recommending the love of our neighbor, superadds the injunction of loving our enemies. Letters To Eugenia |Paul Henri Thiry Holbach