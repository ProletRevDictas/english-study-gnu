So when I see that, I knew that, Oh, I’m doing something that impinges on something personal to them, and perhaps they have been using these conventional measures. Is the U.S. Really Less Corrupt Than China? (Ep. 481) |Stephen J. Dubner |November 4, 2021 |Freakonomics 

Justice Department guidelines require approval from the attorney general to investigate or charge a member of the news media with a crime, to ensure that law enforcement does not impinge upon freedom of the press. Judge wants Justice Department to detail decision on charging Infowars correspondent accused in Jan. 6 riot |Rachel Weiner |August 24, 2021 |Washington Post 

What’s more, if my hike is already impinged by sprockets, I don’t really need them to quantify their annoyance as if doing me a grand favor. What Do Mountain Bikers Owe Hikers? |Sundog |June 15, 2021 |Outside Online 

If they rule in favor of the Catholic foster mothers and CSS, they risk impinging on the rights of LGBTQ Americans—and possibly others—not just among foster agencies, but in any government-sponsored program. The Most Powerful Court in the U.S. is About to Decide the Fate of the Most Vulnerable Children |Belinda Luscombe |May 28, 2021 |Time 

A lot of rank-and-file voters will take notice when big brands speak out against measures that impinge on Americans’ right to vote. Mitch McConnell Tries to Have it Both Ways on Corporate Cash |Philip Elliott |April 7, 2021 |Time 

Things are distant, but in so far as they impinge at all, not unpleasant. Elia Kazan to Tennessee Williams: You Gotta Suffer to Sing the Blues |Elia Kazan |May 1, 2014 |DAILY BEAST 

Nor does it impinge on "the fundamental right of privacy guaranteed by the United States Constitution." Our Dumb Puritan Laws: Sex Bans and Illegal Adultery |Kevin Bleyer |April 20, 2014 |DAILY BEAST 

Hines's pictures don't make us feel miserable enough, for the misery of their subjects to impinge fully on us. Child Labor ... of Love |Blake Gopnik |January 16, 2014 |DAILY BEAST 

What could come closer to the anti-retinal position of Duchamp than paintings so dark they can barely impinge on our retinas? Ad Reinhardt's Black (-on-Black) Humor |Blake Gopnik |December 2, 2013 |DAILY BEAST 

The new guidelines do not impinge on the free-trade agreement or other agreements governing cultural and sports exchanges. Business Is Personal: Why the EU's New Guidelines Could Hurt Israel's Economy |Bernard Avishai |July 17, 2013 |DAILY BEAST 

It is strange at such times how trivial things impinge on the consciousness with a shock as of something important and immense. Masterpieces of Mystery, Vol. 1 (of 4) |Various 

The imagination of the line is meant to be impressed by the spectacle of the heavy mass about to impinge on it. Battles of English History |H. B. (Hereford Brooke) George 

There the tone is straightened out, and made to impinge on the roof of the mouth at a precisely defined point. The Psychology of Singing |David C. Taylor 

It did not impinge on his own jealously guarded circle of activity, on his own task of bringing a fugitive to justice. The Shadow |Arthur Stringer 

Is such quick acceptance found now where Easterns and Westerns impinge? The Interest of America in Sea Power, Present and Future |A. T. Mahan