These trees grow right along the coastline, their long, stilt-like roots stretching down into the water. Soggy coastal soils? Here’s why ecologists love them |Alison Pearce Stevens |September 17, 2020 |Science News For Students 

That’s nearly two-thirds of the state’s 33 million acres of forests and trees, and six times the area that has burned so far this year. Suppressing fires has failed. Here’s what California needs to do instead. |James Temple |September 17, 2020 |MIT Technology Review 

Untie the rock, work the cord down the length of the branch until it’s at least five feet from the tree, and then tie one end to your food sack. This essential survival tool can save your life 10 different ways |By Tim MacWelch/Outdoor Life |September 15, 2020 |Popular-Science 

Instead, Reed Hastings thinks of it as a tree — but not in the way you might think. What if Your Company Had No Rules? (Bonus Episode) |Maria Konnikova |September 12, 2020 |Freakonomics 

The phones in the trees contain the Flex app and are synched with other phones belonging to the drivers. The bizarre reason Amazon drivers are hanging phones in trees near Whole Foods |Jeff |September 1, 2020 |Fortune 

Plenty of Jewish kids today grow up with a Christmas tree next to their menorah. Harry Potter and the Torah of Terror |Candida Moss, Joel Baden |January 4, 2015 |DAILY BEAST 

His most recommended plant was tree ivy—its juices sprayed up the nostrils. History's Craziest Hangover Cures |Justin Jones |December 30, 2014 |DAILY BEAST 

Bohac vowed to that when he came back next year there would be no confusion about any Christmas tree or Santa aprons. A Field General in the War on Christmas |David Freedlander |December 24, 2014 |DAILY BEAST 

If you Google “Muslim Christmas tree star” you will see a list of right-wing websites wetting their pants over this. Why Muslims Love Jesus Too |Dean Obeidallah |December 23, 2014 |DAILY BEAST 

Civilians left flowers as well as a tiny frosted Christmas tree that had two red ornaments. 'Please Don't Die!': The Frantic Battle to Save Murdered Cops |Michael Daly |December 22, 2014 |DAILY BEAST 

This tiny person spent little or none of his time in the tree-tops, but chose to stay near the ground. The Tale of Grandfather Mole |Arthur Scott Bailey 

Each tiny tree was a plume of leaves; the rows stretched out to the hilltop, and over. The Bondboy |George W. (George Washington) Ogden 

Wide and straight, well made and tree-lined throughout, it supplied the two great arteries of Indian life. The Red Year |Louis Tracy 

I saw every crook in the fence, every rut in the road, every bush and tree long before we came to it. The Soldier of the Valley |Nelson Lloyd 

It was supposed by many on its discovery to grow like the engraving given—in form resembling a tree or shrub rather than an herb. Tobacco; Its History, Varieties, Culture, Manufacture and Commerce |E. R. Billings.