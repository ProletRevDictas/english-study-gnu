They lived together, along with Laurel’s vaccinated husband and Sam’s unvaccinated boyfriend, in a tumbledown chalet above an artificial lake outside Charleston. Delta variant dangerous during pregnancy, CDC reports say |Andrew Jeong, Adela Suliman, Laurie McGinley, Paulina Villegas |November 19, 2021 |Washington Post 

The birds ignore the chalet, which has again gone unrented, like an Airbnb with a one-star rating. On the house: Wrens are nesting in my backyard bird box. I couldn’t be happier. |John Kelly |July 7, 2021 |Washington Post 

His locations have had to use creative Covid-driven workarounds just to bring in even a fraction of the typical sales, including glass-walled chalets on the sidewalk to give diners an indoor feel outdoors. New Year’s Eve will be a dud for restaurants |Karen Yuan |December 31, 2020 |Fortune 

So, the 47-year-old, along with his wife and their three children, left a pandemic-stricken Paris for the relative isolation of France’s Haute-Savoie region, where the family owns an early-1800s chalet. When specialty cheesemaking becomes a quarantine pastime |By Kat Craddock/Saveur |December 1, 2020 |Popular-Science 

All I wanted to do was return to the chalet and find Heidi, apron on, blond hair tied into her signature fun bun, telling us to dig in. Ease into Winter with Backcountry-Approved Comfort Food |Christina Bernstein |November 12, 2020 |Outside Online 

One lady claimed she had a “magical connection” to Harry after a video of Blondie played in a Swiss chalet she was staying in. The Cult of Blondie: Debbie Harry’s Very Special New York Picture Show |Tim Teeman |October 1, 2014 |DAILY BEAST 

Prince Harry and Cressida reportedly stayed in a luxury chalet in the mountains above the city of Almaty. Prince Harry And Cressida Go Skiing In Kazakhstan |Tom Sykes |March 25, 2014 |DAILY BEAST 

I got to work with Nicholas Braun, who I worked with on the film Chalet Girl. Ed Westwick on Life After Gossip Girl |Erin Cunningham |October 11, 2013 |DAILY BEAST 

Or perhaps it's your PTA asking you to donate the use of your ski chalet for a week to raise money for new playground equipment. A Problem in Economic Science |David Frum |February 12, 2012 |DAILY BEAST 

Why don't Ayers/Murdoch/the ski chalet donate the money themselves to avoid doing what's asked? A Problem in Economic Science |David Frum |February 12, 2012 |DAILY BEAST 

It was little more than a few rows of planks, with a chalet at one end—but a very welcome sight confronted him. A Maker of History |E. Phillips Oppenheim 

Laughing, Franz ran forward and arrived at the goat pen just in time to meet the Widow Geiser, who came from her chalet. Rescue Dog of the High Pass |James Arthur Kjelgaard 

On the evening of the first day's search they put up at a saeter or mountain chalet. Boyhood in Norway |Hjalmar Hjorth Boyesen 

She went to sleep dreaming of the "Chalet of the West Wind," and in the morning something throbbed in her pulses. Molly Brown's Sophomore Days |Nell Speed 

She walked before me into the living-room of the chalet, and I saw that she gripped her riding-whip very tightly in her hand. Tono Bungay |H. G. Wells