In one case he envisioned, the universe began to expand at a decelerating rate, but then reached an inflection point, whereupon it began expanding at a faster and faster rate. A century ago, Alexander Friedmann envisioned the universe’s expansion |Tom Siegfried |May 20, 2022 |Science News 

Williams likes 30-meter flies, a sprinter drill that can benefit distance runners as well, in which you steadily accelerate for about 30 meters, hit maximum pace for 30 meters, and then decelerate gradually. Why Marathoners Need to Build Their Sprint Speed |jbeverly |January 26, 2022 |Outside Online 

Many skeptics are saying that growth rate is going to continue to decelerate. Controversial Investment Guru Cathie Wood Wants to Help You Trust the Stock Market Again |Belinda Luscombe |January 16, 2022 |Time 

The company expects its consumer revenue growth to decelerate in 2021 and 2022, while its media and brand extensions revenue growth is expected to pick up or hold steady. Cheat Sheet: Forbes plans to go public via SPAC to invest in paid consumer products |Sara Guaglione |August 27, 2021 |Digiday 

Models in general have a hard time forecasting inflection points—peaks, or if things suddenly start accelerating or decelerating. All together now: the most trustworthy covid-19 model is an ensemble |Siobhan Roberts |May 28, 2021 |MIT Technology Review 

But by the early 1980s, Gallant's fast-paced life began to decelerate. The Man Who Created Disco Beauty |Isabel Wilkinson |March 31, 2010 |DAILY BEAST 

Arcot accelerated toward the planet for two hours, then began to decelerate. Islands of Space |John W Campbell 

If anything of the right size shows up, decelerate until we can get mass and albedo measurements. Rip Foster in Ride the Gray Planet |Harold Leland Goodwin 

Every so often comes the impression we are falling head-first; the colonel using ship's drive to decelerate the whole system. The Lost Kafoozalum |Pauline Ashwell 

Its motor is designed to decelerate that mass by 1,075 mph in order to allow it to assume a descending orbit. Far from Home |J.A. Taylor 

They went in fast, using her gravity to help them curve into a forced orbit as they strained to decelerate. Tulan |Carroll Mather Capps