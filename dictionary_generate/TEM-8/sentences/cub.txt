Behind the pulse pounding against his temples was the sickening realization that a sow with a cub in a den was among the worst possible scenarios for a bear encounter. It’s No Fun to Wake a Sleeping Bear |jversteegh |August 11, 2021 |Outside Online 

She was speaking with the anger of a mama bear whose cub has been injured and remains threatened. A 13-Year-Old Put This Homophobic Ex-Virginia Lawmaker to Shame |Michael Daly |June 25, 2021 |The Daily Beast 

Zookeepers said the cub is “curious yet cautious” and is trying new foods, including the sweet potatoes. Baby panda makes debut — online — at National Zoo |Dana Hedgpeth, Justin Wm. Moyer |January 27, 2021 |Washington Post 

Experts called Mei Xiang’s pregnancy “a miracle” because at the age of 22, she had a less than a 1 percent chance of having another cub. Baby panda makes debut — online — at National Zoo |Dana Hedgpeth, Justin Wm. Moyer |January 27, 2021 |Washington Post 

This dedication, Dearie said, is helping the panda cub gain a little more than a pound per week. Baby panda makes debut — online — at National Zoo |Dana Hedgpeth, Justin Wm. Moyer |January 27, 2021 |Washington Post 

Straight people have discovered, and co-opted, the gay “bear” and “cub.” How Straight World Stole ‘Gay’: The Last Gasp of the ‘Lumbersexual’ |Tim Teeman |November 12, 2014 |DAILY BEAST 

The end is here, but for cub detective Henry Palace, there is still one more case to solve—that of his missing sister. What Would You Do if the World Was Over? |William O’Connor |August 5, 2014 |DAILY BEAST 

Lewis waves at Tony Orion, a nominee up for Best Bear/Cub whose biceps have stretched his black mesh jersey to a shine. And The Escort of The Year Is… Backstage at The Sex Oscars |Scott Bixby |March 24, 2014 |DAILY BEAST 

The Toronto Zoo polar bear cub has been a fixture of this video blog since he first graced the world with his adorable presence. Adele Dazeem Sings, RT Anchor Quits, and More Viral Videos |The Daily Beast Video |March 8, 2014 |DAILY BEAST 

From Adele Dazeem to the cutest polar bear cub in the world, take a look at this week's best viral videos. Adele Dazeem Sings, RT Anchor Quits, and More Viral Videos |The Daily Beast Video |March 8, 2014 |DAILY BEAST 

His lordship would not intervene; he swore he hoped the cub would be flayed alive by Wilding. Mistress Wilding |Rafael Sabatini 

The woman distrusts me and she has sent for this insolent cub lawyer—Washburn, his name is. The Outdoor Girls in the Saddle |Laura Lee Hope 

The boys noticed that one of the cubs was dark brown like the mother, while the other was a cinnamon cub. Gold-Seeking on the Dalton Trail |Arthur R. Thompson 

I say, you young cub down there,” shouted the skipper to him from the hatchway, “come up and swab this deck. Eric, or Little by Little |Frederic W. Farrar 

The next moment a savage roar sounded close at hand, and he saw the mother bear running toward the wounded cub. Three Sioux Scouts |Elmer Russell Gregor