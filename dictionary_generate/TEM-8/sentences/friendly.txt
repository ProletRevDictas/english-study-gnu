A new swimmer-friendly wristband called “solo loop” uses stretchy silicone instead of clasps. Everything announced at Apple’s ‘Time Flies’ event today |rhhackettfortune |September 15, 2020 |Fortune 

To check whether your page is mobile-friendly, visit Google’s mobile-friendly test, and submit a URL. 8 major Google ranking factors — SEO guide |Sponsored Content: SEO PowerSuite |September 15, 2020 |Search Engine Land 

This was the early 1980s, so it was a more friendly time in a way. What if Your Company Had No Rules? (Bonus Episode) |Maria Konnikova |September 12, 2020 |Freakonomics 

Anyone from anywhere can participate, and the event agenda includes time zone-friendly programming for attendees in Europe and Asia. Five more reasons you should attend Disrupt 2020 |Alexandra Ames |September 11, 2020 |TechCrunch 

Products like animal-friendly alcohol-free sanitizers, imported strong-strapped masks, and immunity-boosting treats have become popular on e-commerce portals and stores in urban India. Personal protective equipment for pets is selling out in India |Niharika Sharma |September 9, 2020 |Quartz 

Even a relatively small 250-pound bomb could kill or injure friendly troops who are within 650 feet of the explosion. New U.S. Stealth Jet Can’t Fire Its Gun Until 2019 |Dave Majumdar |December 31, 2014 |DAILY BEAST 

By contrast, a gun will allow a pilot to attack hostile forces that are less than 300 feet from friendly ground forces. New U.S. Stealth Jet Can’t Fire Its Gun Until 2019 |Dave Majumdar |December 31, 2014 |DAILY BEAST 

A friendly twenty-something woman originally from Toronto said it best. The Craziest Date Night for Single Jews, Where Mistletoe Is Ditched for Shots |Emily Shire |December 26, 2014 |DAILY BEAST 

The resulting Wool Runners were comfortable, eco-friendly, machine-washable, and super cute—and sold out almost immediately. The Coolest Fashion Innovations of 2014 |Raquel Laneri |December 18, 2014 |DAILY BEAST 

The congregation was warm, friendly, and welcoming—traits, he says, he later came to believe they used to coax members in. Beaten By His Church for Being Gay |Justin Jones |December 16, 2014 |DAILY BEAST 

Could this be the safe old house in which childish days had passed, in which all around were always friendly and familiar faces? Checkmate |Joseph Sheridan Le Fanu 

He put out his hand in the most cordial and friendly way, and greeted me with the most winning smile in the world. Music-Study in Germany |Amy Fay 

Once he had galloped up to the open door, looked in, spoken in a friendly way to her, and ridden on. Ramona |Helen Hunt Jackson 

Villerias had another quarrel of this sort with the latter, after which they were quite friendly. The Philippine Islands, 1493-1898, Volume XX, 1621-1624 |Various 

Finding Mrs. Hartsel thus friendly, Felipe suddenly decided to tell her the whole story. Ramona |Helen Hunt Jackson