Now 30, he’s the author of five comedy specials which lean on a cutthroat, abrasive humor that makes his audience cringe as well as smirk. 18 Comics of Tomorrow |Sohini Das Gupta |August 1, 2021 |Ozy 

Plus, they’re more effective and less abrasive than toilet paper. The best bidet: Talk about a toilet upgrade |Billy Cadden |July 21, 2021 |Popular-Science 

We went at it with the most abrasive cleaning products we had on hand, shaking and queasy, scrubbing until it had vanished. The surprisingly elusive definition of ‘life’ |Jacob Brogan |March 26, 2021 |Washington Post 

Then, when it could not be easily wiped off, my industrious husband took the abrasive side of a sponge to it, leaving scratches that looked like a bear had dragged its claws across the surface. Solutions for the stains and scratches pandemic life has left on our furniture and rugs |Jura Koncius |March 25, 2021 |Washington Post 

If your floor steam cleaner has a scrubbing option, skip that—you don’t want to risk scratching the laminate surface with anything abrasive. Best steam mop: For gleaming, sanitized floors |PopSci Commerce Team |March 18, 2021 |Popular-Science 

In her newest EP Love Your Boyfriend, she takes the messaging of love songs and places it in an abrasive, sonic package. From Church of Christ to Pansexual Rapper |Tyler Gillespie |November 28, 2014 |DAILY BEAST 

The sound of the record, meanwhile, is ideal: not too smooth, not too abrasive. The Band’s ‘Rock of Ages’ Is the Greatest Live Album Ever |Andrew Romano |October 14, 2013 |DAILY BEAST 

He stayed away from the gym for a while and came back transformed, abrasive and rude when he had once been polite and respectful. Boston Suspects Tamerlan & Dzhokhar Tsarnaev, From Boxing to Bombs |Michael Daly |April 20, 2013 |DAILY BEAST 

There is a sense that House of Cards reflects the infighting and abrasive atmosphere of the political sphere at the time. Rewind: BBC’s Iconic Political Thriller ‘House of Cards’ Still Captivates |Jace Lacob |January 17, 2013 |DAILY BEAST 

In contrast to the abrasive and high-handed Zakir, Ibrahim is an admired figure among the insurgents. Pakistan Frees Wave of Taliban Prisoners |Ron Moreau |January 11, 2013 |DAILY BEAST 

Oil or other fluids used on work are apt to drop on it and when wet for a short time the abrasive is useless. The Boy Mechanic, Book 2 |Various 

Old Gilby, the pro, could be abrasive when a bone-head play disfigured the game he loved. The Syndic |C.M. Kornbluth 

After abrasive years on a dozen planets and habitable moons, the ugly savageries of Venus had only a quaint charm. Shock Treatment |Stanley Mullen 

No amount of the hardest known abrasive will even roughen its surface. The Skylark of Space |Edward Elmer Smith and Lee Hawkins Garby 

Saleratus Bill had carefully removed every abrasive possibility in the two rooms. The Rules of the Game |Stewart Edward White