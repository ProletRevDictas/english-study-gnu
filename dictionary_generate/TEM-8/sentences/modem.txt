A Cox spokesperson told Ars that James and similar customers can keep their 30Mbps upload speeds if they upgrade to a newer modem. Cox cuts some users’ uploads from 30Mbps to 10Mbps—here’s how to avoid it |Jon Brodkin |February 11, 2021 |Ars Technica 

Swarm’s offering uses embedded modems also designed and built by the company – the Swarm Tile, a tiny, low-powered modem that’s designed for maximum compatibility. Swarm’s low-cost satellite data network is now available to commercial clients |Darrell Etherington |February 9, 2021 |TechCrunch 

Our satellite communications link had less bandwidth than my dial-up modem in the 1990s and we were permitted to send text-only emails to friends and family at certain times and in certain locations so as not to risk being detected. How a Nuclear Submarine Officer Learned to Live in Tight Quarters - Issue 94: Evolving |Steve Weiner |December 30, 2020 |Nautilus 

A single modem as the source of your online connection often isn’t enough to get the power and consistency that you need. Best WiFi extenders: Five things to consider |Eric Alt |November 25, 2020 |Popular-Science 

Switching to its own chips could enable Apple to introduce faster computers more quickly, help lower its costs, and allow for new features like laptops with cellular modems and multiday battery life. What’s expected at Apple’s just announced virtual event on Nov. 10 |Aaron Pressman |November 2, 2020 |Fortune 

It seems a paltry acknowledgment of the man who created the modem Congress. Memo: The Aaron Sorkin Model of Political Discourse Doesn't Actually Work |Megan McArdle |April 23, 2013 |DAILY BEAST 

What's more, the 3.1 upgrade finally kills the iPhone's ability to be tethered to computers and used as a modem. iPhone's Bad Business |Douglas Rushkoff |September 18, 2009 |DAILY BEAST 

My son has agreed to give his daughter to the Prince of Modem, at which I very sincerely rejoice. The Memoirs of the Louis XIV. and The Regency, Complete |Elizabeth-Charlotte, Duchesse d'Orleans 

The modem reader may form his own estimate of the poet's art, and that estimate will probably not be high. Four Arthurian Romances |Chretien DeTroyes 

I published a few book on the Internet and they can be freely downloaded by anyone who has a computer or a modem. After the Rain |Sam Vaknin