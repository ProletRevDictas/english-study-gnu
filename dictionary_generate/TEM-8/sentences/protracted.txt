The sandwich chain recently had to defend its bread, too, after Ireland’s Supreme Court ruled that, as part of a protracted legal and tax battle, Subway’s hoagie-style rolls did not meet the country’s definition of a staple bread. Subway’s tuna is not tuna, but a ‘mixture of various concoctions,’ a lawsuit alleges |Tim Carman |January 27, 2021 |Washington Post 

While both companies felt they could have successfully fought the DOJ in court, protracted litigation made the merger route untenable, particularly for a startup like Plaid, which was founded in 2013. Why Visa and Plaid called off their $5.3 billion fintech deal |Jen Wieczner |January 13, 2021 |Fortune 

On December 2, after a protracted disagreement over the release of a research paper, Google forced out its ethical AI co-lead, Timnit Gebru. “I started crying”: Inside Timnit Gebru’s last days at Google—and what happens next |Karen Hao |December 16, 2020 |MIT Technology Review 

Douglas’s worry was that this scenario would set us up for a protracted constitutional crisis, one without any clear resolution. Seriously, will he ever go? |Sean Illing |November 12, 2020 |Vox 

That’s why, if you ask buyers what impact the protracted election has had on the mood of advertisers or if it has affected ad spend, you’ll hear that there hasn’t been much change. ‘This is the norm not the exception’: Why media buyers need to prepare for issues with Facebook’s Ads Manager |Kristina Monllos |November 9, 2020 |Digiday 

DNA tests were used to confirm Albert's status as father in both cases, following protracted legal battles. Princess Charlene Gives Birth To Twins Gabriella and Jacques |Tom Sykes |December 10, 2014 |DAILY BEAST 

“We need to be prepared for a protracted campaign in Gaza,” Netanyahu said Monday. Inside the Kerry-Israel Meltdown |Josh Rogin, Eli Lake |July 29, 2014 |DAILY BEAST 

Without taking gradual steps, an individual is at increased risk of protracted PTSD and depression. Bergdahl’s Bitter Homecoming: The Psychological Cost of War |Jean Kim |July 19, 2014 |DAILY BEAST 

Yet four years later, after a protracted series of court fights, Mindi does not have her daughter back. One Breakdown Can Mean Losing Your Kid Forever |ProPublica |May 30, 2014 |DAILY BEAST 

“Protracted handcuffing is liable to damage nerves that affect the functioning of the hands,” says the report. Palestinian Factions Made Peace in Israel’s Jails |Ben Hattem |April 24, 2014 |DAILY BEAST 

These dreamy, Madonna-like beauties are the result of the most severe and protracted study. Women in the fine arts, from the Seventh Century B.C. to the Twentieth Century A.D. |Clara Erskine Clement 

Now, after a weary march and a protracted fight in the burning sun, some of the men deliberately lay down to die. The Red Year |Louis Tracy 

The previous year had seen the English miners beaten after a protracted struggle. The Underworld |James C. Welsh 

It may be that this my lot may be protracted from month to month, even till I grow grey in my captivity. My Ten Years' Imprisonment |Silvio Pellico 

I watched Ganesha, and I joyed to see that his struggles were protracted beyond those of the others. Confessions of a Thug |Philip Meadows Taylor