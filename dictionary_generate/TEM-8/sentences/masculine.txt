Importantly, her contributions as a lawyer and a judge have done much to demonstrate how legal rules and approaches previously regarded as neutral and objective in reality reflected a masculine view of the world. Ruth Bader Ginsburg Forged A New Place For Women In The Law And Society |LGBTQ-Editor |September 23, 2020 |No Straight News 

Another hormone, testosterone, triggers the male body to develop masculine traits. Explainer: What is puberty? |Esther Landhuis |August 27, 2020 |Science News For Students 

Even when he’s grown up and Kathi is interviewing him for the assistant job, Lane writes, Charlie says his father’s “masculine voice is still screaming at me, in my head…even while here, auditioning for a new role in Hollywood’s royal court.” ‘A Star is Bored’ a delicious work of fiction |Kathi Wolfe |August 13, 2020 |Washington Blade 

It’s an exaggerated masculine pride that moves through public spaces and behind closed doors—almost constantly present in workplaces, on the street, and in popular culture. How men in Latin America are unlearning machismo |Victoria Stunt |July 9, 2020 |Quartz 

Thanks to avoiding manual labor his whole life, George’s hands are exquisitely smooth and creamy, delicate yet masculine. Science of ‘Seinfeld’ |LGBTQ-Editor |June 15, 2020 |No Straight News 

She told a story of a fellow employee who identifies as a butch dyke (a lesbian who takes on a more masculine identity). The LGBT Center That Changed Our Lives |Justin Jones |December 22, 2014 |DAILY BEAST 

In America, there are way more male filmmakers than female ones, and they want to tell more masculine stories. The Resurrection of Kristen Stewart |Marlow Stern |October 11, 2014 |DAILY BEAST 

No excessively masculine hero of a traditional cowboy detective story, The Dude is a different kind of man. Dudes and Maudes Abide at New York City Lebowski Fest |Rich Goldstein |August 25, 2014 |DAILY BEAST 

Then it says masculine and feminine roles are distinct in the Bible. Southern Baptist Convention: Trans People Don’t Exist |Jay Michaelson |June 12, 2014 |DAILY BEAST 

As a grad student, I believed that ‘masculine’ and ‘feminine’ were tools of patriarchal oppression. Why I Finally Let My Girls Be Girly |Andy Hinds |May 17, 2014 |DAILY BEAST 

He steeled himself, for he had had his experience of woman's wiles; and his faith in masculine supremacy as a habit did not waver. Ancestors |Gertrude Atherton 

“Well, you are a good creetur,” said that masculine female, looking up as her friend entered. The Garret and the Garden |R.M. Ballantyne 

She belonged to that ultra-modern school which scorns to sue masculine admiration, but which cannot dispense with it nevertheless. Dope |Sax Rohmer 

She learned to appreciate at its true value that masculine admiration which, in an unusual degree, she had the power to excite. Dope |Sax Rohmer 

And in the final battle, when the feminine principle is pitted against the masculine, I fancy we shall know how to win the day. Ancestors |Gertrude Atherton