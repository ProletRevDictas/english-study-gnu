Our family land signified an ancestral connection to the greater sacred cacao story, which I suddenly found myself belonging to, creating a new grounding in my career. Meet Four Craft Chocolate Makers Decolonizing the Industry |Jinji Fraser |October 22, 2020 |Eater 

Colonialism is strong, but I believe our ancestral ways are stronger. Meet Four Craft Chocolate Makers Decolonizing the Industry |Jinji Fraser |October 22, 2020 |Eater 

I am writing this column from my now socially distant home on the ancestral lands of the Osage, Miami, Sioux, and Haudenosauneega people. Happy Indigenous Peoples Day? |Ellen McGirt |October 12, 2020 |Fortune 

Patriarch Georges-Henri Meylan, who was CEO of Audemars Piguet for 21 years, entrusted the running of Moser to his then 35-year-old son Edouard Meylan, an engineer and Wharton MBA who was lured back to his ancestral industry. A Q&A with the provocative mind behind watchmaker H. Moser |Daniel Bentley |September 21, 2020 |Fortune 

Since then, Blakeslee’s excavations along the Walnut River have filled in gaps between ancestral Wichita sites. Drones find signs of a Native American ‘Great Settlement’ beneath a Kansas pasture |Bruce Bower |September 10, 2020 |Science News 

They will also oppose any attempts to hunt grizzlies in their recognized ancestral homelands. What It Takes to Kill a Grizzly Bear |Doug Peacock |November 23, 2014 |DAILY BEAST 

The Oklahoma Kiowa have joined this warpath; the Yellowstone is their ancestral homeland. What It Takes to Kill a Grizzly Bear |Doug Peacock |November 23, 2014 |DAILY BEAST 

That is to say, the ancestral genes, the ancestral strain of inheritance, appears again in these little children. Joseph Campbell on the Roots of Halloween |Joseph Campbell |October 31, 2014 |DAILY BEAST 

There is an ancestral homestead, but it has a meth lab in the barn. Book Bag: Gritty Stories From the Real Montana |Carrie La Seur |October 2, 2014 |DAILY BEAST 

The Nevilles' music, inspired by the ancestral rhythms of their city, is mostly pop, funk, and soul. The Stacks: The Neville Brothers Stake Their Claim as Bards of the Bayou |John Ed Bradley |April 27, 2014 |DAILY BEAST 

Needless to say, the Worcestershire baronet had returned to his ancestral acres a sadder but a wiser man. The Pit Town Coronet, Volume I (of 3) |Charles James Wills 

Some time after Bruce went north, he proceeded to Douglasdale again78 and placed an ambush near his ancestral castle. King Robert the Bruce |A. F. Murison 

Therefore is there, in me, at least, an insistent whisper for ancestral and long denied rights. Ancestors |Gertrude Atherton 

The one possessed the prestige of wealth, and rank, and ancestral power; the other, the energy of a vigorous and cultivated mind. Madame Roland, Makers of History |John S. C. Abbott 

Thus, as for religion, in order to satisfy the requirements of the definition, I must restrict myself to my ancestral religion. Third class in Indian railways |Mahatma Gandhi