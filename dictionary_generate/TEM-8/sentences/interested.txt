Because it’s voluntary, funders can spend a lot of money that has virtually no impact—it’s not what’s needed but what they’re interested in. Can Billionaires Really Save Us from Climate Disaster? |Heather Hansman |February 12, 2021 |Outside Online 

You can look at Google Trends as a mirror to show you what people are interested in at this moment. A comprehensive guide on using Google Trends for keyword research |Aayush Gupta |February 12, 2021 |Search Engine Watch 

There are students on both campuses who were interested in this project because they thought they were living in a bubble. An innovative Georgetown lab looks to theater to quell political fires |Peter Marks |February 12, 2021 |Washington Post 

Maybe you’re really interested in the person you’re talking to — or at least you want to signal that you’re interested. Can I Ask You a Ridiculously Personal Question? (Ep. 451) |Stephen J. Dubner |February 11, 2021 |Freakonomics 

Two months after Benvenuto’s proposal in 2019, the Housing Commission issued a request for proposals that said interested consultants should be prepared to kick off the study in fall 2019. Vacancy Tax Study Is Giving City Officials Déjà Vu |Lisa Halverstadt and Andrew Keatts |February 10, 2021 |Voice of San Diego 

There was a lot of positive feedback from people interested in non-gender binary people. Grindr’s Trans Dating Problem |David Levesley |January 9, 2015 |DAILY BEAST 

“I heard Jeffrey was interested in supporting science and I contacted him,” Krauss said. Sleazy Billionaire’s Double Life Featured Beach Parties With Stephen Hawking |M.L. Nestel |January 8, 2015 |DAILY BEAST 

With Vice, that was an example of you keeping yourself interested too, right? ‘Archer’ Creator Adam Reed Spills Season 6 Secrets, From Surreal Plotlines to Life Post-ISIS |Marlow Stern |January 8, 2015 |DAILY BEAST 

Monir is not interested in classic dances like tango or ballet. Iran’s Becoming a Footloose Nation as Dance Lessons Spread |IranWire |January 2, 2015 |DAILY BEAST 

Lawler is more interested in the more fascinating story of how the chicken spread. The History of the Chicken: How This Humble Bird Saved Humanity |William O’Connor |December 27, 2014 |DAILY BEAST 

The advantages and the drawbacks, if any, of the system may here be seen and judged of by all who are interested in the matter. Showell's Dictionary of Birmingham |Thomas T. Harman and Walter Showell 

I would certainly; and as I am much interested in the subject, I will willingly give you five dollars for your rule. The Book of Anecdotes and Budget of Fun; |Various 

But she is greatly interested in certain shops that she is buying out, and especially in her visits to her tailor. Confidence |Henry James 

The causes became apparent the day before, although those directly interested did not understand. The Homesteader |Oscar Micheaux 

I saw the folly of imagining that I could stand a chance against a man like Moeran, and, moreover, he interested me too deeply. Uncanny Tales |Various