Global warming has triggered the greatest loss of ice in recent history, opening up the polar region to increased shipping traffic and mining exploration, bringing new levels of noise to an environment that used to be acoustically pristine. Underwater Noise Pollution Is Disrupting Ocean Life—But We Can Fix It |Aryn Baker |February 5, 2021 |Time 

For now, SpaceX is only including laser links on polar satellites. SpaceX adds laser links to Starlink satellites to serve Earth’s polar areas |Jon Brodkin |January 26, 2021 |Ars Technica 

The result was so successful that Malden ended up changing its name to Polartec, and polar fleece now likely needs very little in the way of introduction. The Best Fleece-Lined Pants for the Outdoors |Wes Siler |January 15, 2021 |Outside Online 

In the atmosphere, waves can also break, but in this case the energy from those waves slows the polar vortex and heats the stratosphere. The polar vortex is about to split in two. But what does that actually mean? |Philip Kiefer |January 11, 2021 |Popular-Science 

“India is definitely looking toward the Arctic to augment the nation’s fossil fuel needs,” says Sulagna Chattopadhyay, president of Science and Geopolitics of Himalaya, Arctic and Antarctic, a policy and advocacy organization working on polar issues. India’s Dirty Arctic Energy Hunt |Charu Kasturi |January 11, 2021 |Ozy 

The weather, the conditions, you can imagine it—a polar bear in a desert, with a swimming pool 50 centimetres deep. More Bad Luck For Depressed Polar Bear |Abby Haglage |July 23, 2014 |DAILY BEAST 

Shaked spoke in these generalities initially—referring to two sets of people, two polar opposites on a pendulum. Knesset Member Walks Back On Facebook Post Calling Palestinian Kids ‘Little Snakes’ |Gideon Resnick |July 11, 2014 |DAILY BEAST 

Sykes suspects that the hairs come from either an unrecognized bear species, or an unknown hybrid of polar bear and brown bear. Sorry Bigfoot Truthers: Yetis Aren’t Real |Elizabeth Picciuto |July 3, 2014 |DAILY BEAST 

She would leave every day of shooting during the polar vortex just grinning from ear-to-ear. Lori Petty on ‘Orange Is the New Black,’ the Halcyon ‘90s, and Discovering Jennifer Lawrence |Marlow Stern |June 8, 2014 |DAILY BEAST 

The world of the military is to the writer admittedly “the polar opposite” of his own. Geoff Dyer at Sea: Unmoored but on Target |Melissa Holbrook Pierson |May 21, 2014 |DAILY BEAST 

Long before that, however, the sun had come back to gladden the Polar regions, and break up the reign of ancient night. The Giant of the North |R.M. Ballantyne 

The same would be the case if the polar axis of one sphere stood precisely at right angles to that of the other. Outlines of the Earth's History |Nathaniel Southgate Shaler 

Thus the wide habitability of the earth is an effect arising from the inclination of its polar axis. Outlines of the Earth's History |Nathaniel Southgate Shaler 

Let us conceive a particle of air situated immediately over the earth's polar axis. Outlines of the Earth's History |Nathaniel Southgate Shaler 

On the equatorial side this air is moving more rapidly than it is on the polar side. Outlines of the Earth's History |Nathaniel Southgate Shaler