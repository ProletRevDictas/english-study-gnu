Similarly, representatives from Quaker tell Eater that rice cakes were first launched as “a low-carb alternative to bread” in the mid-’80s. The Rise and Fall of the Rice Cake, America’s One-Time Favorite Health Snack |Brenna Houck |September 17, 2020 |Eater 

If you’re looking for something to help you get healthier and more active, or just an alternative to your current workout routine, look for tiktoks with specific instructions. Use TikTok to build the perfect workout |Sandra Gutierrez G. |September 17, 2020 |Popular-Science 

The technology essentially combines the performance benefits of the diesel engine design with the low costs and the low emissions associated with these alternative fuels, co-founder and CEO BJ Johnson said in a recent interview with TechCrunch. ClearFlame Engine Technologies takes aim at cleaning up diesel engines |Kirsten Korosec |September 17, 2020 |TechCrunch 

There are, today, a large number of groups that continue to spread misleading health information or push users to try alternative or untested cures. Facebook tries to clean up Groups with new policies |Sarah Perez |September 17, 2020 |TechCrunch 

The coronavirus pandemic is set to change the way millions of Americans can vote in November, as states expand access to mail-in voting as a safer alternative to in-person voting. Vote by mail: Which states allow absentee voting |Kate Rabinowitz, Brittany Mayes |September 17, 2020 |Washington Post 

Whatever the FBI says, the truthers will create alternative hypotheses that try to challenge the ‘official story.’ Was Sony Hit With a Second Hack? |Shane Harris |January 8, 2015 |DAILY BEAST 

It reminded me a bit of an alternative take on The Wolf of Wall Street—through the Toni and Candace lens. Coffee Talk with Fred Armisen: On ‘Portlandia,’ Meeting Obama, and Taylor Swift’s Greatness |Marlow Stern |January 7, 2015 |DAILY BEAST 

Already, 10 Republicans have declared they will vote for an alternative candidate and more seemed poised to join. Kamikaze Congress Prepares to Strike Boehner |Ben Jacobs |January 6, 2015 |DAILY BEAST 

No longer does it constitute a reliable, middle class-based alternative to the corporatist mindset of the Republicans. Time to Bring Back the Truman Democrats |Joel Kotkin |December 21, 2014 |DAILY BEAST 

Another dark horse, Tennessee Senator Al Gore, was finding little traction in his efforts to become a centrist alternative. Want President Hillary? Then Primary Her |Jeff Greenfield |November 24, 2014 |DAILY BEAST 

"I will," gruffly replied the man, with a look which showed that he was sorry to be forced to choose the second alternative. Blackwood's Edinburgh Magazine, Volume 60, No. 372, October 1846 |Various 

He had no alternative; he had as large interests in England as in Scotland, and armed opposition was out of the question. King Robert the Bruce |A. F. Murison 

Mr. Balfour, being an abstemious man, would not submit to the latter alternative, but consented to tell a story. The Book of Anecdotes and Budget of Fun; |Various 

Comyn chose the latter alternative; and the agreement was guaranteed by oaths and embodied in indentures duly sealed. King Robert the Bruce |A. F. Murison 

The future composer had no alternative but to study these works in pianoforte arrangements. The Life &amp; Letters of Peter Ilich Tchaikovsky |Modeste Tchaikovsky