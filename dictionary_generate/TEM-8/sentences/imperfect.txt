“Poker is the main benchmark and challenge program for games of imperfect information,” Sandholm told me on a warm spring afternoon in 2018, when we met in his offices in Pittsburgh. The Deck Is Not Rigged: Poker and the Limits of AI |Maria Konnikova |August 7, 2020 |Singularity Hub 

Apple’s counter to this is its SKAdNetwork, an imperfect application programming interface it launched two years ago, that developers can use to get basic data about their in-app ad campaign performance. Ad tech is in denial about Apple’s new app privacy rule |Lara O'Reilly |July 14, 2020 |Digiday 

Tired of technology that isolates us from one another, people are seeking out and placing greater value on physical, authentic, and imperfect experiences delivered by humans. 4 Non-Obvious Trends That Matter During This Pandemic |Vanessa Bates Ramirez |May 25, 2020 |Singularity Hub 

Layering imperfect interventions can, in a similar way, slow down transmission. How Do You Stay Safe Now That States Are Reopening? An Expert Explains How To Assess Risk When Reconnecting With Friends And Family |LGBTQ-Editor |May 18, 2020 |No Straight News 

The way that I would view it is that the world is imperfect because we haven’t used science in policy making. Policymaking Is Not a Science (Yet) (Ep. 405) |Stephen J. Dubner |February 13, 2020 |Freakonomics 

Even an imperfect messenger is capable of delivering news everyone needs to hear. Bill Cosby Foe Hannibal Buress Joked About Date Rape |Rich Goldstein |November 20, 2014 |DAILY BEAST 

Though the grand jury is an imperfect forum for resolving social issues, it works very well in finding truth. There’s No Conspiracy in Ferguson’s Secret Jury |Paul Callan |November 17, 2014 |DAILY BEAST 

There was a fear growing inside of me that my imperfect bruised college experience was a reflection of my own damaged self. Freshman Year Sucks—and That’s OK |Eleanor Hyun |November 12, 2014 |DAILY BEAST 

The problem was that, at least in Iowa, this model was imperfect. Did a Flawed Computer Model Sabotage the Democrats? |Ben Jacobs |November 10, 2014 |DAILY BEAST 

Himmler, for example, wanted to drop the imperfect British pounds on the United Kingdom by airplane. On the Trail of Nazi Counterfeiters |Dr. Kevin C. Ruffner |September 20, 2014 |DAILY BEAST 

Where these overtones are interfered with by any imperfection in the instrument the result is a harsh or imperfect sound. Expressive Voice Culture |Jessie Eldridge Southwick 

We suffer, nearly all of us, from a lack of quantitative grasp and from an imperfect grasp of form. The Salvaging Of Civilisation |H. G. (Herbert George) Wells 

A coquette is said to be an imperfect incarnation of Cupid, as she keeps her beau, and not her arrows, in a quiver. The Book of Anecdotes and Budget of Fun; |Various 

On the part of the believer, his faith and imperfect obedience, though necessary, are not a condition. The Ordinance of Covenanting |John Cunningham 

This description is only imperfect in this point that sufficient stress is not laid on the words fall off. Violins and Violin Makers |Joseph Pearce