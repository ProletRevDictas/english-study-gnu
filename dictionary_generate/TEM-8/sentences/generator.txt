YouTube was becoming a significant ad revenue generator in its own right. With Bezos out as Amazon CEO, Zuckerberg is the last man standing |Elizabeth Dwoskin |February 3, 2021 |Washington Post 

Make a meme with a meme generatorThe quickest way to make a meme is to use a meme generator. How to make a legendary meme |John Kennedy |January 22, 2021 |Popular-Science 

Microsoft on Thursday announced a new password generator for the recently released Edge 88. Chrome and Edge want to help with that password problem of yours |Dan Goodin |January 22, 2021 |Ars Technica 

It’s widely used in San Diego to heat homes and for cooking but it also powers a lot of the main energy generators used to create electricity for the region. San Diego’s Climate Challenges Will Still Be Here in 2021 – and Beyond |MacKenzie Elmer |January 1, 2021 |Voice of San Diego 

Amazon Web Services, a big profit generator for the company, has also experienced increased demand during the pandemic. World’s richest men added billions to their fortunes last year as others struggled |Christopher Ingraham |January 1, 2021 |Washington Post 

“Sometimes it takes 30 minutes, sometimes five minutes, for the generator to restart and the power to come back on,” he told me. Power Shortages Hit Gaza Maternity Ward |Jesse Rosenfeld |July 24, 2014 |DAILY BEAST 

The Dazeem sensation arguably hit its peak with the Slate Adele Dazeem Name Generator, a widget designed to Travoltify any name. John Travolta and the Birth of the Adele Dazeem Phenomenon |Amy Zimmerman |March 5, 2014 |DAILY BEAST 

We took the genre descriptions, broke them down to their key words… and built our own new-genre generator. The Daily Beast’s Best Longreads, January 4, 2013 | |January 4, 2014 |DAILY BEAST 

But the economic generator theory has been debunked over the past two decades. Bankruptcy Hasn’t Stopped Detroit’s Plan for Public Funding of New Sports Stadium |Evan Weiner |December 4, 2013 |DAILY BEAST 

Running on power from a generator, it is constantly mortared and is a frequent target of airstrikes. Syria’s Government Targets Hospitals |Emma Beals |October 1, 2013 |DAILY BEAST 

He applied voltage till his generator groaned, and watched in awe as meters climbed and climbed without any sign of stopping. Security |Poul William Anderson 

The generator hummed, the needles of the dials climbed, flickered, and steadied. Space Prison |Tom Godwin 

The generator was completed and installed on the nineteenth night. Space Prison |Tom Godwin 

The summer sun was hot the day the generator hummed into life. Space Prison |Tom Godwin 

"I didn't realize it would take such a large generator," he said after a silence. Space Prison |Tom Godwin