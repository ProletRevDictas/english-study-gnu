Shallal said artists began painting murals on the barriers Thursday, making them more visible to motorists while boosting the dining area’s ambiance. Busboys and Poets to launch ‘streatery’ east of the Anacostia River |Michael Brice-Saddler |February 12, 2021 |Washington Post 

Under the legislation, cities would no longer be able to seek license suspensions for motorists who have five or more unpaid red-light or speed camera tickets. Thousands of Illinois Drivers Would Get Their Licenses Back Under a Criminal Justice Reform Bill |by Melissa Sanchez |January 15, 2021 |ProPublica 

He crashed the vehicle in the woods in the Delaplane area of Fauquier County, and, at one point, he “encountered a passing motorist.” Shootings leave 3 dead in Prince William, Fauquier counties |Dana Hedgpeth |January 7, 2021 |Washington Post 

Opponents, including some who have said they’re considering a lawsuit, say the highway widening would destroy surrounding homes, parkland and streams, and would be unfair to motorists who can’t afford to pay tolls. Maryland director on controversial Beltway, I-270 widening plan retires |Katherine Shaver |January 7, 2021 |Washington Post 

Spend a couple hours on the bike, and while you may or may not have a negative encounter with a motorist, you’ll almost certainly finish the ride feeling happier than when you started it. How We Keep Riding After the Nevada Cycling Deaths |Eben Weiss |December 22, 2020 |Outside Online 

In Birmingham, three men were killed by a hit-and-run motorist while guarding their community from rioters. Cameron Gets Tough |William Underhill |August 10, 2011 |DAILY BEAST 

The guy is driving it off the lot and turns into the street when another motorist cuts him off. Alan Simpson's Rush Limbaugh Joke |Lloyd Grove |July 9, 2010 |DAILY BEAST 

The motorist, however, finds a different problem confronting him in making London his center. British Highways And Byways From A Motor Car |Thomas D. Murphy 

In no case should a motorist pay a bill at a London garage without a proper receipt. British Highways And Byways From A Motor Car |Thomas D. Murphy 

Though Edinburgh has unusually broad and well paved streets, it is a trying place for a motorist. British Highways And Byways From A Motor Car |Thomas D. Murphy 

Fetherston, although an ardent motorist himself, cursed the driver under his breath for bespattering them with mud. The Doctor of Pimlico |William Le Queux 

The officer simply takes the name and number and the motorist can call on the proper official himself. British Highways And Byways From A Motor Car |Thomas D. Murphy