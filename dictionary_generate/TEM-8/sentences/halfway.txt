It did occur to me that a single misstep could leave me broken and bloody on the rocks below, but I was now halfway up and my pride was on the line. A day’s drive from Chicago, exploring a very different Illinois |Carson Vaughan |February 12, 2021 |Washington Post 

The project, replacing deteriorating platforms at 20 stations, is more than halfway complete. Metro seeks bond sales to raise $360 million for capital projects |Justin George |February 10, 2021 |Washington Post 

Combine it with the ingenious gift of a bag of frozen tater tots and you’re halfway to having someone else make you actual hot dish, the greatest gift of all. From the Cut: 33 Valentine’s Day Gifts for the Foodie in Your Life |The Cut Staff |February 8, 2021 |Eater 

We estimated a median police salary for a typical officer by filtering pension data to find the base salaries of officers with 10 years on the job, that is, those who were halfway through a 20-year career. How We Found Pricey Provisions in New Jersey Police Contracts |by Agnes Chang, Jeff Kao and Agnel Philip, ProPublica, and Andrew Ford, Asbury Park Press |February 8, 2021 |ProPublica 

Fill the ramekin halfway with batter, add 1 tablespoon of the ganache and then cover with the rest of the batter. If loving a piping hot chocolate lava cake is wrong, I don’t want to be right |Becky Krystal |February 4, 2021 |Washington Post 

I meet Otis J. the night he arrives at “The Castle,” a West Harlem halfway house for newly-released convicts. His First Day Out Of Jail After 40 Years: Adjusting To Life Outside |Justin Rohrlich |January 3, 2015 |DAILY BEAST 

Halfway through his second term, Johnson has enjoyed a charmed life. Boris Johnson’s Churchill Man Crush |Michael F. Bishop |November 22, 2014 |DAILY BEAST 

The likes of Coffman are “probably” already halfway wards of the state anyhow. The FBI’s Bogus ISIS Bust |James Poulos |November 21, 2014 |DAILY BEAST 

Flying an Ebola patient halfway across the world is a difficult undertaking, to say the least. Was Flying Hero Doctor With Ebola to the U.S. the Wrong Call? |Abby Haglage |November 17, 2014 |DAILY BEAST 

It ends up halfway around the world, then back in New York. How to Get Away With Stealing $2 Million in Jewelry in the Heart of New York |John Surico |November 13, 2014 |DAILY BEAST 

Being only halfway in control of his own planetary system was no state to be found in by the first interstellar visitors. Fee of the Frontier |Horace Brown Fyfe 

Wentworth rang up from his rooms; they're probably halfway through lunch by now, and they'll be round in ten minutes. First Plays |A. A. Milne 

From where he sat, both figures were above him, sheltered halfway up the long sliding slope. The Wave |Algernon Blackwood 

The Black Hood took a couple of strides and then leaped from halfway down the steps. Hooded Detective, Volume III No. 2, January, 1942 |Various 

They leaned against the ladders easily about halfway up, their fluffy short hair gleaming in the sun. The Box-Car Children |Gertrude Chandler Warner