I do think that in some cases, boldness presupposes some degree of stability. Why a 'No Regrets' Philosophy Won't Get You Anywhere, According to Business Guru Daniel Pink |Belinda Luscombe |February 7, 2022 |Time 

Capitalism presupposes that you have things like competition, that you have things like price transparency, that you have things like efficient capital markets, that consumers are making choices. Congresswoman Katie Porter Fighting for a Better America? Better Believe It |Esabelle Lee |February 24, 2021 |Ozy 

In other words, the very engagement with rational argument and evidence presupposes facts about value. The Universe Knows Right from Wrong - Issue 89: The Dark Side |Philip Goff |September 9, 2020 |Nautilus 

Never presuppose what business partners are thinking, but reach out, stay in touch, and find out if you can be helpful to them. How this year’s 40 Under 40 are surviving the pandemic |jonathanvanian2015 |September 7, 2020 |Fortune 

It deserves remark, that these early generalizations did not, like scientific inductions, presuppose causation. A System of Logic: Ratiocinative and Inductive |John Stuart Mill 

All social judgments presuppose a system of recognized values. The New Society |Walther Rathenau 

More clearly even than petition does thanksgiving presuppose a personal being, capable of appreciating the feeling of gratitude. Elements of Folk Psychology |Wilhelm Wundt 

They actually presuppose its existence in the Church as the necessary condition of their own existence. Our Lady Saint Mary |J. G. H. Barry 

It is however to be understood that the different cases all presuppose the same total moment of momentum. Time and Tide |Robert S. (Robert Stawell) Ball