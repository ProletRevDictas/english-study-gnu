They need to change the conversation about their team, which is full of doubt and apathy. The Wizards’ reboot needs to start with trading both John Wall and Bradley Beal |Jerry Brewer |November 23, 2020 |Washington Post 

Men don’t have that kind of apathy for women like Dean does. Norm Crosby, comedian who mangled words with great extinction, dies at 93 |Harrison Smith |November 9, 2020 |Washington Post 

It reflects a lack of seriousness, some ambivalence and apathy on the part of the creative community and the inability of different companies to really set in place policies and procedures that would change the status quo. Despite some gains in the past year, Hollywood still has inclusion problems, study says |radmarya |September 10, 2020 |Fortune 

I don’t think most psychologists would use the word “apathy.” 5 Psychology Terms You’re Probably Misusing (Ep. 334 Rebroadcast) |Stephen J. Dubner |January 9, 2020 |Freakonomics 

We all like to throw around terms that describe human behavior — “bystander apathy” and “steep learning curve” and “hard-wired.” 5 Psychology Terms You’re Probably Misusing (Ep. 334 Rebroadcast) |Stephen J. Dubner |January 9, 2020 |Freakonomics 

The dire fatalism that dominated the discourse then is gone, replaced largely with a practiced apathy. Heart of Darkness: Into Afghanistan’s Taliban Valley |Matt Trevithick, Daniel Seckman |November 15, 2014 |DAILY BEAST 

The result is safe seats that lead to apathy and voter impotence, leading logically to ever-declining voter turnout. Hate Hyper-Partisanship? Support Redistricting Reform Now |John Avlon |November 3, 2014 |DAILY BEAST 

And one arena in which to stage that confrontation—with madness, apathy, family dysfunction, poverty, etc.—is the theater. Broadway Was Made for Tupac |Henry Louis Gates, Jr., Marcyliena Morgan |July 7, 2014 |DAILY BEAST 

Two basic characteristics not related to memory are apathy and indifference or callousness. Does Donald Sterling Have Dementia? And Does That Make Him Any Less of a Racist? |Robert Silverman |May 23, 2014 |DAILY BEAST 

Silence and apathy are key ingredients to a tasty helping of bigotry. Female Journalist Gets Rape Threats Over Comic Book Criticism |Tauriq Moosa |April 21, 2014 |DAILY BEAST 

I've been in a sort of mental apathy since I got back—the result, I suppose, of so much artistic excitement all summer. Music-Study in Germany |Amy Fay 

The narrative had excited him out of his apathy and physical exhaustion, the confession shaken the rigidity from his mind. Ancestors |Gertrude Atherton 

He studied Madame Roland with even more of stoical apathy than another man would study a book which he admires. Madame Roland, Makers of History |John S. C. Abbott 

The school-children, owing to a more liberal educational system, had lost the customary look of apathy. Mystery Ranch |Arthur Chapman 

From the fatal slumber of religious apathy into which the church was falling it was to be rudely awakened. The Catacombs of Rome |William Henry Withrow