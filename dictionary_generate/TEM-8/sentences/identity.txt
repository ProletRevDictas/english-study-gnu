They were spurred by their research, which revealed Ghanaian banks to be beset by widespread identity fraud and cybercrime and spent nearly $400 million a year to identify their customers. The race to build facial recognition tech for Africa is being led by this award-winning engineer |Audrey Donkor |September 17, 2020 |Quartz 

Now we have different sorts of status hierarchies for each identity—related to, for example, class or occupation—and a fire hose of social information layered on top of our personal relationships. The Dark Side of Smart - Facts So Romantic |Diana Fleischman |September 15, 2020 |Nautilus 

Acquiring these user identities is one step, the next is to connect a given user identity cohesively across all points in the brand’s digital ecosystem. Deep Dive: How the Summer of 2020 forced brand marketing to change for the better |jim cooper |September 14, 2020 |Digiday 

The information requested by these systems to verify a voter’s identity is often easily discoverable,and sometimes even a matter of public record in states that make voter files public. Voting by mail is more secure than the President says. How to make it even safer |matthewheimer |September 13, 2020 |Fortune 

The James Beard Foundation is experiencing something of an identity crisis. The Mess That Is the 2020 James Beard Awards, Explained |Elazar Sontag |September 11, 2020 |Eater 

But he, like many people using dating apps whatever their sexual identity, remains stoutly positive. Grindr’s Trans Dating Problem |David Levesley |January 9, 2015 |DAILY BEAST 

They are not the actual traffickers, Yazbek says, so generally the other refugees protect their identity. Ghost Ships of the Mediterranean |Barbie Latza Nadeau |January 6, 2015 |DAILY BEAST 

But those strands of his identity are all wound around the conspiracy that led him back to Gambia for the first time in 23 years. The Shadowy U.S. Veteran Who Tried to Overthrow a Country |Jacob Siegel |January 6, 2015 |DAILY BEAST 

Identity issues seem to have dogged Otis since his troubles began. His First Day Out Of Jail After 40 Years: Adjusting To Life Outside |Justin Rohrlich |January 3, 2015 |DAILY BEAST 

In a remote location with little means for economic development, the Brogpas have cultivating this identity to their advantage. The Himalayas’ Hidden Aryans |Nina Strochlic |January 3, 2015 |DAILY BEAST 

Now the Waterford and Limerick were to lose, not only the Ennis line, but all their lines and their own identity as well. Fifty Years of Railway Life in England, Scotland and Ireland |Joseph Tatlow 

There is a perfect identity of principle, both working to the same good end, between the existing corn-law and the new tariff. Blackwood's Edinburgh Magazine, No. CCCXXXIX. January, 1844. Vol. LV. |Various 

But in all probability, the messenger knew less about the identity of the Eye than Black Hood knew. Hooded Detective, Volume III No. 2, January, 1942 |Various 

Some are constantly being added, others are removed from her communion on earth, but her characteristic absolute identity remains. The Ordinance of Covenanting |John Cunningham 

At least he understood how Jack Carlson had died, even if the mystery of the identity of the Eye had deepened. Hooded Detective, Volume III No. 2, January, 1942 |Various