The campaign is “much larger in scope and complexity than seasonal influenza or other previous outbreak-related vaccination responses,” said the playbook for states from the Centers for Disease Control and Prevention. U.S. outlines sweeping plan to provide free COVID-19 vaccines |Rachel Schallom |September 16, 2020 |Fortune 

Various organizations have already tried to reduce the scope of the new law, and some worrying provisions, though none mentioned here, have been removed. Brazil’s “fake news” bill won’t solve its misinformation problem |Amy Nordrum |September 10, 2020 |MIT Technology Review 

Documents obtained by Voice of San Diego showed Zandvliet wrote many exemptions for reasons well outside the scope of accepted medical science. Vaccine Exemption Doctor on Probation, Can No Longer Write Exemptions |Will Huntsberry |September 4, 2020 |Voice of San Diego 

One indication of the scope of those ambitions came recently, when Walmart surprised everyone by teaming up with Microsoft to bid on social-media platform TikTok. America’s largest retailer is taking on Amazon Prime |Marc Bain |September 4, 2020 |Quartz 

My concerns are with the size, scope and speed of what is being reopened on Monday. Morning Report: MTS Rejects Many Who Applied for Disabled Fare Reductions |Voice of San Diego |August 31, 2020 |Voice of San Diego 

Special praise goes to Kudrow for the way she broadened the scope of Valerie Cherish in Season 2. ‘The Comeback’ Finale: Give Lisa Kudrow All of the Awards |Kevin Fallon |December 29, 2014 |DAILY BEAST 

Most of the actions taken by prior presidents were more limited in size, scope and benefits. Fact-Checking the Sunday Shows: November 23 |PunditFact.com |November 23, 2014 |DAILY BEAST 

The story of Alstory Simon has all the scope and scale, the cruel reversals, and pointless waste of proper tragedy. Wrongly Imprisoned for 15 Years Thanks to an Innocence Project |Jacob Siegel |November 13, 2014 |DAILY BEAST 

“We are talking a very broad scope here,” one senior committee staff member said. Congress Scouring Every U.S. Spy Program |Eli Lake |October 10, 2014 |DAILY BEAST 

I mean, most people on here are literally openly hoping for the leaks to continue and expand in scope and scale. ‘The Fappening’ Perpetuators Have a J.Law Come-to-Jesus Moment and ‘Cower With Shame’ |Marlow Stern |October 8, 2014 |DAILY BEAST 

Not only do children thus of themselves extend the scope of our commands, they show a disposition to make rules for themselves. Children's Ways |James Sully 

What are these numerous Acts of Parliament and what are their objects, scope, and intentions? Fifty Years of Railway Life in England, Scotland and Ireland |Joseph Tatlow 

Curt as is the cable it has yet scope to show up a little more of our great K.'s outfit. Gallipoli Diary, Volume I |Ian Hamilton 

A few words will suffice to explain the general scope of those alterations. Blackwood's Edinburgh Magazine, No. CCCXXXIX. January, 1844. Vol. LV. |Various 

On reaching the front the volunteer captain soon found scope for his pencil. Napoleon's Marshals |R. P. Dunn-Pattison