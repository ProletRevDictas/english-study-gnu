Firstly, the propositions of ethics are much harder to frame with precision than the truths of logic and mathematics. The Universe Knows Right from Wrong - Issue 89: The Dark Side |Philip Goff |September 9, 2020 |Nautilus 

Platonism about ethics is still a popular option, but its advocates face a deep problem accounting for moral knowledge. The Universe Knows Right from Wrong - Issue 89: The Dark Side |Philip Goff |September 9, 2020 |Nautilus 

Safety, ethics, and regulatory committees are speeding things up by prioritizing approval processes ahead of those for other vaccines and medicines. Oxford Scientists: These Are Final Steps We’re Taking to Get Our Coronavirus Vaccine Approved |Rebecca Ashfield |September 9, 2020 |Singularity Hub 

Follow Leave No Trace ethics when you disperse your dishwater. The Absolute Beginner's Guide to Camp Cooking |AC Shilton |September 5, 2020 |Outside Online 

We can debate the merits and ethics of such as approach and whether news publishers should be subject to the pressures of the “free market.” Google to launch ‘enhanced news storytelling’ project with licensed content |Greg Sterling |August 20, 2020 |Search Engine Land 

Mr. Bachner said it had been hard to introduce his work ethic and share his vision with the locals and his team. The Photographer Who Gave Up Manhattan for Marrakech |Liza Foreman |January 6, 2015 |DAILY BEAST 

There is reference after reference to the “black community,” “black worth ethic,” and adherence to the “black value system.” Reverend Jeremiah Wright Was Worse Than Scalise |Ron Christie |January 2, 2015 |DAILY BEAST 

The GOP should embrace the work ethic as its mantra, and this time act like they mean it. Bush, Christie, Romney: Who’ll Be the GOP Class Warrior? |Lloyd Green |December 15, 2014 |DAILY BEAST 

At that age I had little leverage other than work ethic, intense loyalty to the president, and the strength of my ideas. The Valerie Jarrett I Know: How She Saved the Obama Campaign and Why She’s Indispensable |Joshua DuBois |November 18, 2014 |DAILY BEAST 

We have that opportunity right now to do what we did when we were younger but with the minds we have now and the work ethic. Not a Liquid Dream: O-Town's Back, Baby. But Where’s Ashley? |Melissa Leon |August 25, 2014 |DAILY BEAST 

Eventually, long before they really develop a stabilized ethic, someone's going to collate that whole mess. Indirection |Everett B. Cole 

But there is reason (they say) in planting kale, and, even in ethic and religion, room for common sense. The Works of Robert Louis Stevenson - Swanston Edition Vol. 11 (of 25) |Robert Louis Stevenson 

There are many cases in which, by deviating from the strictly ethic code, you do not harm anyone, you only injure your own soul. Recollections Of My Childhood And Youth |George Brandes 

From his childhood honesty and morality were his darling attributes—he delighted in reposing under the ethic mantle. Sages and Heroes of the American Revolution |L. Carroll Judson 

But he always replied only with a superior smile when asked by reporters to put the philosophy and the triple ethic into words. Zero Data |Charles Saphro