The coroner said it was probably quick—a heart attack, maybe. My Father's Life Was Shaped by Racism. So Was His Death |Savala Nolan |July 16, 2021 |Time 

Lisa Lapointe, the province’s chief coroner, said in a statement last week that the extreme heat probably was “a significant contributing factor” in the increased number of deaths. Crushing heat wave in Pacific Northwest and Canada cooked shellfish alive by the millions |Sammy Westfall, Amanda Coletta |July 9, 2021 |Washington Post 

Thirteen of the 20 died in King County, which includes Seattle, the county coroner said. Historic heat wave in Pacific Northwest has killed hundreds in U.S. and Canada over the past week |Timothy Bella |July 1, 2021 |Washington Post 

That’s why Greene and others in his field examine how many people die in a given area during an unusually hot period, as opposed to just looking at those deaths that coroners or medical examiners code as related to hyperthermia. Heat is the silent killer we should all be worried about |Rachel Feltman |June 29, 2021 |Popular-Science 

CBP does not maintain a single, centralized tally of exposure deaths and human remains recovered, because the majority of forensic services are handled by county sheriffs and coroners in border districts. Huge border influx brings fears of grim summer for migrant deaths |Nick Miroff |June 3, 2021 |Washington Post 

Even the coroner determined that the cause of death was "homicide." Cops, CIA Share a Culture of Lawlessness |Cliff Schecter |December 12, 2014 |DAILY BEAST 

Garner was pronounced dead an hour later, and the city coroner ruled his death a homicide. Eric Garner Protests: ‘It’s Like Vietnam’ |Abby Haglage, Caitlin Dickson, Jacob Siegel, Chris Allbritton |December 5, 2014 |DAILY BEAST 

The coroner would also note the tiny hemorrhages that accompany strangulation. Indiana Serial Killer’s Confession Was Just the Start |Michael Daly |October 21, 2014 |DAILY BEAST 

The coroner said the call had been "clearly pressing on her mind" but that she had had "appropriate" support from the hospital. Kate Prank Call DJ Tells of Death Threats |Tom Sykes |October 13, 2014 |DAILY BEAST 

At last month's inquest, Coroner Fiona Wilcox concluded Mrs Saldanha had taken her own life. Kate Prank Call DJ Tells of Death Threats |Tom Sykes |October 13, 2014 |DAILY BEAST 

"One of the most extraordinary cases I have ever met with," the doctor told the coroner at the inquest. Uncanny Tales |Various 

"There was in the evidence given before the coroner a suggestion that the captain had dined somewhere in secret," he said. The Doctor of Pimlico |William Le Queux 

A woman having died suddenly at Waterford, the Coroner had, according to law, ordered an inquest. Friend Mac Donald |Max O'Rell 

Coroner Bogle demanded that the body should be viewed officially before the man-hunt should begin. Scattergood Baines |Clarence Budington Kelland 

Scattergood stepped forward as the coroner turned the face up to the light of the sun. Scattergood Baines |Clarence Budington Kelland