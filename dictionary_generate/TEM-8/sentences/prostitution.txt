Prostitution is not a crime in Brazil, and has been a recognized occupation since 2002. Rio de Janeiro’s Termas Centaurus: Justin Bieber’s Alleged Brothel |Culture Team |November 2, 2013 |DAILY BEAST 

Prostitution opponents cite scientific studies; supporters of legalization find flaws in the data. ‘Borgen’: The Television Show That Could Solve the Government Shutdown |Andrew Romano |October 6, 2013 |DAILY BEAST 

Prostitution in Italy is not illegal, unless it is with an underage minor. Ruby the Heart Stealer in Court for Berlusconi Trial |Barbie Latza Nadeau |January 15, 2013 |DAILY BEAST 

Prostitution is not a crime in Italy unless it is with a minor. As Berlusconi Returns, Ruby the Heart-Stealer Pops Up in Mexico |Barbie Latza Nadeau |December 11, 2012 |DAILY BEAST 

Prostitution in Italy is not a crime unless it is with a minor. Italy: Despite Bunga Bunga Trial, Berlusconi Ponders Run for President |Barbie Latza Nadeau |June 3, 2012 |DAILY BEAST 

Prostitution exists wherever a woman makes the selling of her charms a trade. Woman and Socialism |August Bebel 

Paul Kampffmeyer—Prostitution as a social class phenomenon and the social and political struggle against it. Woman and Socialism |August Bebel 

Prostitution disappears and monogamy, instead of going out of existence, at last becomes a reality—for men also. The Origin of the Family Private Property and the State |Frederick Engels 

Prostitution is not an easy or agreeable subject to treat; it will be disposed of, therefore, in the fewest words possible. Twentieth Century Socialism |Edmond Kelly 

Prostitution is generally the direct result of the disgrace put upon a woman by loss of virtue. Twentieth Century Socialism |Edmond Kelly