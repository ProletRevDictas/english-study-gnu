In 1977, Hassan Gouled Aptidon was close to being toppled as president of Djibouti for considering a khat ban. How the Pandemic Is Saving Lives in the Horn of Africa |Eromo Egbejule |September 3, 2020 |Ozy 

Engineers warned that could result in segments of the fence toppling into the river if not fixed. Federal Prosecutors Have Steve Bannon’s Murky Nonprofit in Their Sights |by Yeganeh Torbati |August 24, 2020 |ProPublica 

Engineers warned that could result in the fence toppling into the river if not fixed. Private Border Wall Fundraisers Have Been Arrested on Fraud Charges |by Perla Trevizo, Jeremy Schwartz and Lexi Churchill |August 20, 2020 |ProPublica 

By comparison, the S&P 500 is teetering at such rarefied heights that a sudden blast of bad news could send it toppling. Despite Warren Buffett’s selloff, bank stocks look like great buys in this market |Shawn Tully |August 18, 2020 |Fortune 

As they topple over they bury carbon, therefore increasing the seabed’s potential as a carbon sink. Tiny creatures collected 100 years ago confirm accelerating carbon uptake in Antarctic Ocean |John Barrat |March 3, 2011 |The Smithsonian Insider 

Obama has latched on to the failure of the embargo to topple the Castros as justification to shuffle the deck. Obama’s One Hand Clap With Castro |Doug McIntyre |December 24, 2014 |DAILY BEAST 

I push down on the pedal with my right leg and instead of propelling myself forward, I topple over sideways. You’re Never ‘Cured’ of an Eating Disorder |Carrie Arnold |December 20, 2014 |DAILY BEAST 

Others, like the Tehrike e Taliban Pakistan (TTP) vowed to topple the Pakistani state itself. Obama’s Deadly Informants: The Drone Spotters of Pakistan |Umar Farooq, Syed Fakhar Kakakhel |November 12, 2014 |DAILY BEAST 

President Obama has weighed the options and concluded America does more harm than good when it sets out to topple regimes. The CIA’s Wrong: Arming Rebels Works |Christopher Dickey |October 19, 2014 |DAILY BEAST 

Margaret Thatcher had served under the Heath regime as Education Secretary and witnessed the miners topple the Conservative Party. ‘Pride’: The Feel-Good Movie of the Year, and the Film Rupert Murdoch Doesn’t Want You to See |Marlow Stern |October 13, 2014 |DAILY BEAST 

As they turned into red-hot ashes and began to topple over one by one into the glowing pile, Jess laughed delightedly. The Box-Car Children |Gertrude Chandler Warner 

It is enough to make the tallest chimney of the neighbourhood topple over with envy. Friend Mac Donald |Max O'Rell 

There would be a report, two reports, and he would topple over backwards to lie crumpled up and motionless. The Outdoor Girls in the Saddle |Laura Lee Hope 

The loungers fully expected to see Tad topple over backwards with a bullet in his body. The Pony Rider Boys on the Blue Ridge |Frank Gee Patchin 

These congeal again, or are compressed into soft, filthy monumental masses, waiting their turn to topple into the waves at last. Over the Rocky Mountains to Alaska |Charles Warren Stoddard