The only thing basic about the shrimp-and-pork wonton soup seems to be the name of the dish. Tom Sietsema’s 8 favorite places to eat right now |Tom Sietsema |January 26, 2021 |Washington Post 

By early 2020, that number had jumped to at least 55 startups around the world trying to re-create at least 15 different types of animal flesh, including pork, shrimp, chicken, duck, lamb, even foie gras. Your first lab-grown burger is coming soon—and it’ll be “blended” |Katie McLean |December 18, 2020 |MIT Technology Review 

If tofu isn’t enough to keep you full, try subbing in shrimp. This Weekend: Escape This Messy World With a Memoir |Joshua Eferighe |November 6, 2020 |Ozy 

There were so many shrimp in the net they pulled it to the top. 14 wild edibles you can pull right out of the ocean |By Bob McNally/Field & Stream |October 19, 2020 |Popular-Science 

Those roots provide critical habitat for fish, shrimp and other marine animals. Soggy coastal soils? Here’s why ecologists love them |Alison Pearce Stevens |September 17, 2020 |Science News For Students 

Chicken satay and shrimp cocktail are also good options, as you can watch the skewers and tails stack up. 12 Thanksgiving Weight Loss Tips That Actually Work |DailyBurn |November 27, 2014 |DAILY BEAST 

Salmon burgers and shrimp burgers are available, too, but they are no match for the crab. Become a Fried Seafood Believer at South Beach Market |Jane & Michael Stern |April 20, 2014 |DAILY BEAST 

The cafeteria style stop is basically a comfort food joint—think baby back ribs, shrimp and grits, and gumbo—done well. Delayed? The Best Airport Restaurants to Eat at This Thanksgiving |Brandy Zadrozny |November 27, 2013 |DAILY BEAST 

I plan to get shrimp as well as the catfish and also the roast beef. New Orleans Celebrates Its Favorite Sandwich at the Oak Street Po-Boy Festival |Tyler Gillespie |November 26, 2013 |DAILY BEAST 

From 10, 20, 30 years of the shrimp boats doing this, the sharks will follow these boats for miles. Off the Hook: Eric Young’s Craziest Shark Catches (Video) |Anna Klassen |August 5, 2013 |DAILY BEAST 

Even the Chinese shrimp-pickers were lounging on the beach before their little shack village. Ancestors |Gertrude Atherton 

Under these circumstances, the learned counsel called on the jury to reduce the damages to a shrimp. The Book of Anecdotes and Budget of Fun; |Various 

It is, of course, a dead sea, for there is absolutely nothing living in it save a variety of shrimp of low organization. The Cradle of Mankind |W.A. Wigram 

"Left arm, sir, if you please," replied Shrimp in a shrill treble. Frank Fairlegh |Frank E. Smedley 

But, my dear Lawless, out of the host of servants at Heathfield, how do you know it was Shrimp who did it? Frank Fairlegh |Frank E. Smedley