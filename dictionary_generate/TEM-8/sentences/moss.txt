The more the virus replicates, the more opportunity it has to evolve around existing vaccines or natural immune responses to older variants, Moss says. Global inequity in COVID-19 vaccination is more than a moral problem |Jonathan Lambert |February 26, 2021 |Science News 

Moss provides credit cards and a spending platform to small and medium businesses in Germany. Corporate credit card platform Moss raises $25.5 million |Romain Dillet |January 15, 2021 |TechCrunch 

It’s Louisiana, so there’s some moss and the occasional palm tree. Heisman winner DeVonta Smith puts tiny town of Amite City in the spotlight |Chuck Culpepper |January 7, 2021 |Washington Post 

With Moss’s guidance, they both leap wittily from animation to three dimensions. Judy and Mickey would have loved the TikTok ‘Ratatouille’ — 2021’s answer to ‘Let’s put on a show!’ |Peter Marks |January 2, 2021 |Washington Post 

So for many tens of millions of years, plants on land had to be happy sticking like moss to the surfaces of rocks. Why Birds Can Fly Over Mount Everest - Issue 94: Evolving |Walter Murch |December 30, 2020 |Nautilus 

In New York, district attorneys have a tendency to grow moss-bound in their roles. Meet Dan Donovan, the Prosecutor Who Let Eric Garner’s Killer Walk |David Freedlander |December 5, 2014 |DAILY BEAST 

They agreed to let McKell and Moss join their tribe for a few days. London’s Pagan Counterculture Kings |Justin Jones |October 12, 2014 |DAILY BEAST 

By the time the CFDA awards rolled round in early 1994, Moss was a bona fide star. Sex, Drugs, and Kate Moss: Secrets of a Wild Supermodel |Tom Sykes |October 9, 2014 |DAILY BEAST 

Callahan claims that within weeks of leaving the Priory, Moss “got back on drugs.” Sex, Drugs, and Kate Moss: Secrets of a Wild Supermodel |Tom Sykes |October 9, 2014 |DAILY BEAST 

He pops from the screen as a charismatic, occasionally messianic “human prism,” as Moss calls him. The Pastor Who Scandalized His Town |Lloyd Grove |October 8, 2014 |DAILY BEAST 

He walked about, stumbling over sticks and stones and stumps, sometimes falling down on soft moss, and again on the hard ground. Squinty the Comical Pig |Richard Barnum 

It was the home that had sheltered her orphan childhood; she had never slept a night from under its moss-grown roof. The World Before Them |Susanna Moodie 

The large soft leaves and the pendent moss of the oaks were gray with dust, but the shade was cool and delicious. Ancestors |Gertrude Atherton 

Studying it very carefully, he thought he made out "Mrs." before the moss-blurred name. The Cromptons |Mary J. Holmes 

Jess quickly found a dry spot thick with moss between two stones. The Box-Car Children |Gertrude Chandler Warner