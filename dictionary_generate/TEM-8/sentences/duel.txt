It was during his attempts to decrease the size of the military budget that he called none other than the Minister President of Prussia, Otto von Bismarck, a liar, resulting in the sausage duel. Five Scientists on the Heroes Who Changed Their Lives - Issue 93: Forerunners |Alan Lightman, Hope Jahren, Robert Sapolsky, |December 2, 2020 |Nautilus 

This week, the two sides battled it out in a heated duel in one of the most prestigious science journals, Nature. Can We Trust AI Doctors? Google Health and Academics Battle It Out |Shelly Fan |October 20, 2020 |Singularity Hub 

There was hope that, if the stars aligned, we could see a duel between Kipchoge and Bekele with both men at their best. Eliud Kipchoge’s Streak Comes to an End in London |Martin Fritz Huber |October 5, 2020 |Outside Online 

What the researchers pieced together is that this seemingly harmonious partnership evolved through a duel at the cellular and genetic levels, one that left the ant eggs largely unviable on their own. How Two Became One: Origins of a Mysterious Symbiosis Found |Viviane Callier |September 9, 2020 |Quanta Magazine 

For example, a section in the Kentucky Constitution requires all public officials swear they have never participated in a duel. SDPD Is Punishing Speech Using a 102-Year-Old City Law |Kate Nucci |August 3, 2020 |Voice of San Diego 

It was a duel on a larger scale, with all the uncertainty and danger that implied. How Clausewitz Invented Modern War |James A. Warren |November 24, 2014 |DAILY BEAST 

That, then, makes this, for the third year running, duel between Julia Louis-Dreyfus and Amy Poehler. What's TV's Funniest Show? Our Emmy Awards Comedy Predictions |Kevin Fallon |August 21, 2014 |DAILY BEAST 

It is people like the al Wakeel family who pay the harshest price for this military duel. Israel’s Campaign to Send Gaza Back to the Stone Age |Jesse Rosenfeld |July 29, 2014 |DAILY BEAST 

Argentina and Belgium, earlier in the day, had fought out a fascinating duel, not unlike chess on turf. Costa Rica vs. the Netherlands: A Tale of Two Goalies |Tunku Varadarajan |July 5, 2014 |DAILY BEAST 

Drake has been following the case on behalf of Fahmy, who is duel Canadian-Egyptian national. Egyptian Court Hands Down Stiff Sentences for Al-Jazeera Journalists |Jesse Rosenfeld |June 23, 2014 |DAILY BEAST 

"But I don't quite see that," persisted Spunyarn, strong in his idea that the man who fights a duel is a fool. The Pit Town Coronet, Volume I (of 3) |Charles James Wills 

As soon as Michael made sure of the duel, he saw his confidential clerk. Blackwood's Edinburgh Magazine, No. CCCXXXIX. January, 1844. Vol. LV. |Various 

Button Gwinnett, one of the signers, died of a wound received in a duel. The Every Day Book of History and Chronology |Joel Munsell 

On the Sunday afternoon the conversation turned on the recent duel at Rome. The Pit Town Coronet, Volume II (of 3) |Charles James Wills 

But the young nobleman provoked Du Bousquier into a duel where the latter dangerously wounded him. Repertory Of The Comedie Humaine, Complete, A -- Z |Anatole Cerfberr and Jules Franois Christophe