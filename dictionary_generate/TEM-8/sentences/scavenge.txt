One Halloween when I was in elementary school, I scavenged through my closet in search of an outfit. Need a last-minute Halloween costume people will recognize? Look to video games. |Shannon Liao |October 29, 2021 |Washington Post 

What’s more, the presence of preserved Neandertal footprints at the site suggests that the ancient hominids foraged there and may have preyed upon young elephants or scavenged dead elephants or other creatures, Martin says. Fossil tracks may reveal an ancient elephant nursery |Sid Perkins |September 16, 2021 |Science News 

The best thing to do when you want new clothing is to scavenge the forgotten pieces from your own closet or shop secondhand. Bamboo fabric is less sustainable than you think |Sara Kiley Watson |September 7, 2021 |Popular-Science 

At Ice Lake last summer, people left unburied feces around the lake, lit fires in the fragile alpine environment with wood scavenged from historic mining structures, and trampled vegetation that may take hundreds of years to recover. Yes, Hiking Trails Are More Crowded than Ever |Svati Narula |July 25, 2021 |Outside Online 

They hunted when hunting made sense, scavenged when scavenging made sense, and otherwise explored, investigated, and took risks. The Mystery of the Falkland Islands' Striated Caracara |Paul Kvinta |April 2, 2021 |Outside Online 

And towns on the edge of their range have and will experience more interaction as the bears arrive to scavenge. How Climate Change Is Causing Chaos in the Animal Kingdom |Nina Strochlic |January 23, 2014 |DAILY BEAST 

In middle school, the young boy would scavenge nearby trash yards in the capital of Freetown to find parts for his inventions. Why the Clintons Love Sierra Leone’s Boy Genius |Nina Strochlic |September 26, 2013 |DAILY BEAST 

People have to scavenge or make everything, either by themselves or as part of a cooperative community. Where are the Bicycles in Post-Apocalyptic Fiction? |Megan McArdle |January 28, 2013 |DAILY BEAST 

Inspired us to scavenge for even more erotic bedtime reading. 50 Ways ‘Fifty Shades of Grey’ Has Changed the World |Lizzie Crocker |December 20, 2012 |DAILY BEAST 

He will scavenge any book in any language for another puzzle piece. Umberto Eco’s 'The Prague Cemetery' Brings to Life Ancient Hate |Daniel Levin |November 12, 2011 |DAILY BEAST 

It was not implied that it was part of the duty of the Bembridge green committee to scavenge the seashore. Fifty Years of Golf |Horace G. Hutchinson 

Neglect of local authority to scavenge after undertaking to do so, 5s. Cooley's Practical Receipts, Volume II |Arnold Cooley 

The symbiote might produce sugars, scavenge the blood of toxins—there are so many things it could do. Planet of the Damned |Harry Harrison 

There was a fair chance this early that he could scavenge something edible. Badge of Infamy |Lester del Rey