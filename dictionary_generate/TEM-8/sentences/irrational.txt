You have to strike a balance where you’re not telling people that what they’re saying is irrational, silly, crazy. Mobilizing the National Guard Doesn’t Mean Your State Is Under Martial Law. Usually. |by Logan Jaffe |September 17, 2020 |ProPublica 

It’s possible that today’s market is an indication of things to come, where fundamentals play a larger part in valuations, as opposed to the irrational exuberance that has persisted in recent months within tech. Jittery investors eye today’s big jobs report as markets rebound from an epic sell-off |Bernhard Warner |September 4, 2020 |Fortune 

John Coates, a Wall Street trader turned neuroscientist, considers the molecule of “irrational exuberance” in The Hour Between Dog and Wolf. The best books, journalists, and academics for keeping up with bubbles and trading |John Detrixhe |August 30, 2020 |Quartz 

Primarily, this problem is a byproduct of “irrational exuberance” in the early 2000s and the use of public sector banks by successive governments to propel the economy. The Modi government’s plan to overcome economic slump ignores a bitter lesson from the past |Prathamesh Mulye |August 18, 2020 |Quartz 

Only a few months ago, this type of behavior would have been considered excessive, irrational, even pathological, and certainly not healthy. Are We All OCD Now, With Obsessive Hand-Washing And Technology Addiction? |LGBTQ-Editor |June 15, 2020 |No Straight News 

The Samaritan guidelines are written around the assumption that suicide is a purely irrational act, an act spurred by illness. Cover-Ups and Concern Trolls: Actually, It's About Ethics in Suicide Journalism |Arthur Chu |January 3, 2015 |DAILY BEAST 

The fact that the virus is still alive has sustained many safety concerns, both rational and irrational, about its use. Powdered Measles Vaccine Could Be Huge for Developing World |Kent Sepkowitz |December 2, 2014 |DAILY BEAST 

The writing team behind these videos are some seriously mad comedy crackheads, and they manage some brilliantly irrational bits. There Are More 'Too Many Cooks' Where That First Fever Dream Came From |Kevin Fallon |November 11, 2014 |DAILY BEAST 

Farah, of course, is not alone in holding this acrobatically irrational view. Surprise! Leading ‘Birther’ Thinks Earth is 6,000 Years Young |Asawin Suebsaeng |October 26, 2014 |DAILY BEAST 

To a certain degree, there is an irrational sense of betrayal. Renee Zellweger's Face Gets More Medical Scrutiny Than Ebola |Emily Shire |October 21, 2014 |DAILY BEAST 

He was, I knew, a deep, thinly-covered tank of resentments and quite irrational moral rages. The New Machiavelli |Herbert George Wells 

Suddenly my irrational complaint was silenced as certain words of Saint Paul to the Corinthians reverberated in my mind. The Relief of Mafeking |Filson Young 

Animals, irrational animals, had told the secret, and birds of the air had carried the matter. The Fortunes of Nigel |Sir Walter Scott 

It seemed to me that he was foolish and irrational, altogether unlike himself. Paul Patoff |F. Marion Crawford 

It is an irrational practice, even when adopted by military tribunals. My Recollections of Lord Byron |Teresa Guiccioli