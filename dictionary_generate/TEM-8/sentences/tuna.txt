Their salmon is already sold out, but you can still get the tuna. From the Cut: 33 Valentine’s Day Gifts for the Foodie in Your Life |The Cut Staff |February 8, 2021 |Eater 

The complaint alleges the ingredient billed as “tuna” for the chain’s sandwiches and wraps contains absolutely no tuna. Subway’s tuna is not tuna, but a ‘mixture of various concoctions,’ a lawsuit alleges |Tim Carman |January 27, 2021 |Washington Post 

Shredded tuna turns into a wispy tan cloud when it’s deep-fried. Tom Sietsema’s 8 favorite places to eat right now |Tom Sietsema |January 26, 2021 |Washington Post 

“We do believe we can extract and utilize the resources of the moon, just as we can extract and utilize tuna from the ocean,” he said earlier this year. A dollar can’t buy you a cup of coffee but that’s what NASA intends to pay for some moon rocks |Christian Davenport |December 3, 2020 |Washington Post 

By the time I finished The Outlaw Ocean, I couldn’t open a can of tuna without imagining a trickle of human blood oozing out. The New Wave of Fishless Fish Is Here |Rowan Jacobsen |November 24, 2020 |Outside Online 

Champagne, which is also acidic, offers a nice complement to anything from tuna tartare to beef bourguignon. Champagne: You’re Drinking It All Wrong |Kayleigh Kulp |December 20, 2014 |DAILY BEAST 

While the President chomped on his tuna fish sandwich, the Blackhawk pilot explained the details of his crash. I Shot Bin Laden |Elliot Ackerman |November 16, 2014 |DAILY BEAST 

His cannabis-infused menus range from truffle tuna casserole and coconut chicken to French toast and omelets. Meet the Julia Child of Weed |Justin Jones |November 13, 2014 |DAILY BEAST 

The next step was to steal one of the six pound tuna cans from the warehouse. Tales of a Jailhouse Gourmet: How I learned to Cook in Prison |Daniel Genis |June 21, 2014 |DAILY BEAST 

The birdman cooked the flesh in his ‘kitchen’, which was a tuna can on top of an ‘eye’ just like mine. Tales of a Jailhouse Gourmet: How I learned to Cook in Prison |Daniel Genis |June 21, 2014 |DAILY BEAST 

I suppose you would like to come instead, and from what I hear I think I'll put off that trip and try tuna again to-morrow. The Boy With the U. S. Fisheries |Francis Rolt-Wheeler 

The sea fairly seemed to boil as the fin of the tuna cut through the water at the surface. The Boy With the U. S. Fisheries |Francis Rolt-Wheeler 

Once more the tuna came up to the surface with a rush in order to get slack enough for a plunge. The Boy With the U. S. Fisheries |Francis Rolt-Wheeler 

The tuna, although a wonderful leaper, hardly ever rises from the water after it is fast to the line. The Boy With the U. S. Fisheries |Francis Rolt-Wheeler 

I have caught many tuna with Sam, and hooked big ones, but these giants are still roving the blue deeps. Tales of Fishes |Zane Grey