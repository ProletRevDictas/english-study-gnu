You’ll need corrugated boxes in various sizes, unless you’re using boxes provided by your carrier. Mailing gifts this year? Here are tips from the pros on how to box up your items properly. |Jura Koncius |December 3, 2020 |Washington Post 

Inside, mismatched plastic trays sat carefully stacked on industrial metal shelves, stretching all the way from the concrete floor to the corrugated-steel ceiling. Inside Singapore’s huge bet on vertical farming |Katie McLean |October 13, 2020 |MIT Technology Review 

Rather, it can be made from “wood, corrugated metal, polycarbonate plastic, and standard framing hardware,” which allows for the cabins to be prototyped immediately and relatively cost-effectively at scale. What might COVID winter dining look like? IDEO and Chicago have some out-there ideas |Brett Haensel |October 9, 2020 |Fortune 

The dugout was covered with semi-circular sheets of corrugated iron, forming a vaulted roof. Life Under Air Strikes: Children Under Fire Will Never Forget — or Forgive |Clive Irving |August 3, 2014 |DAILY BEAST 

At the same time parts of the corrugated iron roof collapsed. Life Under Air Strikes: Children Under Fire Will Never Forget — or Forgive |Clive Irving |August 3, 2014 |DAILY BEAST 

Since the Nehers departed, the school got a corrugated iron roof and there is now a real road into the town. We Built a School in Boko Haram’s Heartland |Michael Daly |May 13, 2014 |DAILY BEAST 

It ran into a corrugated tin sheet boundary and a large genip tree. Uncovering Jamaica’s Jewish Past |Debra A. Klein |December 1, 2013 |DAILY BEAST 

There were trees and electrical poles strewn across the road and corrugated iron roofing that had been ripped off houses. Typhoon Haiyan Survivor Describes Utter Devastation in the Philippines |The Telegraph |November 11, 2013 |DAILY BEAST 

Its shores were long stretches of mud-flats, corrugated everywhere with thousands of clam-holes. The Rival Campers Afloat |Ruel Perley Smith 

And the parson went to live in the town, beside his church—in a corrugated iron house that was run up for him. Thirty Years in Australia |Ada Cambridge 

The corrugated iron roofs were dazzlingly white and smooth—two or three inches of snow in every groove. Thirty Years in Australia |Ada Cambridge 

The house was a rough, square, one-storeyed building, roofed over with corrugated iron. The Luck of Gerard Ridgeley |Bertram Mitford 

A corrugated iron roof had saved it, they said, although there was a good clearing on either side of the shanty. The Pioneers |Katharine Susannah Prichard