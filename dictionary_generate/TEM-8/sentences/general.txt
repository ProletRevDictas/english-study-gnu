The USPS lawyer told the court Thursday that the agency’s warning was not unusual and that a similar warning was issued before the 2016 general election. Federal judge issues temporary injunction against USPS operational changes amid concerns about mail slowdowns |Elise Viebeck, Jacob Bogage |September 17, 2020 |Washington Post 

In general, this is reviewing that wants to create a sport from it, that wants people to participate without knowing how to win. Christian Puglisi Is Closing His Influential Copenhagen Restaurants. COVID Is Only Partly to Blame |Rafael Tonon |September 17, 2020 |Eater 

The attorney general parroting the president’s false assertions about the reliability of mail-in voting. William Barr is the poster child for politicized law enforcement officials |Philip Bump |September 17, 2020 |Washington Post 

Fewer have taken action for the general election, as the move has become increasingly partisan and subject to litigation. Vote by mail: Which states allow absentee voting |Kate Rabinowitz, Brittany Mayes |September 17, 2020 |Washington Post 

The report says the data also show that LGBTQ people are 20 percent more likely than the general population to have experienced a reduction in work hours during the reopening period. HRC examines hospital policies, impact of COVID on LGBTQ people |Lou Chibbaro Jr. |September 16, 2020 |Washington Blade 

“They are hypocritical on this very issue,” Shearer said about Obama, Attorney General Eric Holder and other public officials. Harry Shearer on The Dangerous Business of Satire |Lloyd Grove |January 8, 2015 |DAILY BEAST 

It also contains some clunky passages of adultery, temptations of the flesh, and general sexual awkwardness. Powerful Congressman Writes About ‘Fleshy Breasts’ |Asawin Suebsaeng |January 7, 2015 |DAILY BEAST 

Closed courthouses, rogue clerks, and misleading statements from the attorney general as Florida welcomes same-sex marriage. The Back Alley, Low Blow-Ridden Fight to Stop Gay Marriage in Florida Is Finally Over |Jay Michaelson |January 5, 2015 |DAILY BEAST 

“We would just as soon stay away from a group that will create controversy,” the Cubs general manager Sam Bernabe told the paper. The Louisiana Racists Who Courted Steve Scalise |Jason Berry |January 3, 2015 |DAILY BEAST 

Your general reaction runs along the lines of: “When will these geezers give it up and go for a mall walk or something?” The World’s Toughest Political Quiz |Jeff Greenfield |December 31, 2014 |DAILY BEAST 

A Yankee, whose face had been mauled in a pot-house brawl, assured General Jackson that he had received his scars in battle. The Book of Anecdotes and Budget of Fun; |Various 

In the year of misery, of agony and suffering in general he had endured, he had settled upon one theory. The Homesteader |Oscar Micheaux 

To others the fierce desire for social justice obliterates all fear of a general catastrophe. The Unsolved Riddle of Social Justice |Stephen Leacock 

Finally, let me ask the general reader to put aside all prejudice, and give both sides a fair hearing. God and my Neighbour |Robert Blatchford 

General Lachambre, as the hero of Cavite, followed to receive the applause which was everywhere showered upon him in Spain. The Philippine Islands |John Foreman