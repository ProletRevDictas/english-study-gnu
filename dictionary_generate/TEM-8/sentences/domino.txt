Now watch this feat of strength as we stitch these two storylines together …Weber’s departure from the 79th Assembly District will set off another round of political dominoes to replace her. Morning Report: A Wild, High-Profile Day for San Diego Pols |Voice of San Diego |December 23, 2020 |Voice of San Diego 

Barta cited the relative toothlessness of BYU’s schedule, a schedule Athletic Director Tom Holmoe patched together after the harsh dominoes of the late summer found the independent Cougars down to three scheduled games. Alabama, Notre Dame, Clemson and Ohio State stay at top of College Football Playoff rankings |Chuck Culpepper |December 2, 2020 |Washington Post 

Global pizza chain Domino’s, for example, just had its 38th consecutive quarter of sales growth. How is the fast food industry surviving coronavirus? |Amanda Shendruk |November 22, 2020 |Quartz 

It led to a bunch of weird dominoes falling in a weird order. Billy Joe Shaver, singer-songwriter who inspired outlaw country, dies at 81 |Terence McArdle |October 29, 2020 |Washington Post 

It’s also working with a number of branded content partners like eToro, Domino’s Pizza and Utlilta. ‘The audience is still there’: How U.K. sports outlets pivoted to branded content, social video to keep fans engaged as seasons were delayed |Lucinda Southern |July 29, 2020 |Digiday 

The lack of food can also domino effect into peace and security—already there have been reports about stolen food aid. Liberia’s Ebola Famine |Abby Haglage, Nina Strochlic |November 13, 2014 |DAILY BEAST 

This first session set off a domino effect of unexpected proportions. China’s Last Foot-Binding Survivors |Nina Strochlic |July 2, 2014 |DAILY BEAST 

The former Soviet Union is witnessing an anti-gay, anti-Western, anti-civil society domino effect. Putin’s Post-Sochi LGBT Crackdown |Jay Michaelson |February 24, 2014 |DAILY BEAST 

The CVS move will undoubtedly create a domino effect among other pharmacy chains. The War on Smoking Didn’t Save My Mother’s Life, but It Could Save Many More |Joe Concha |February 5, 2014 |DAILY BEAST 

Depending on what Wildstein has, and on whom, he might set off a domino-effect of people flipping, and then who knows. Revenge of the Nerd: Wildstein Rats Out the Boss |Michael Tomasky |January 31, 2014 |DAILY BEAST 

He hands a lady out; her pale blue silk domino hides her effectually from the inquisitive gaze of the crowd. The Pit Town Coronet, Volume I (of 3) |Charles James Wills 

"Saperlotte," he hissed, and his out-stretched hand touched the pale blue domino on the shoulder. The Pit Town Coronet, Volume I (of 3) |Charles James Wills 

At the domino mask they fetched I hesitated, for anything like mummery of this sort was always repugnant to me. The Way of a Man |Emerson Hough 

Of all the habits there he could only identify one—the scarlet and orange domino Lady Lyndwood had told him she would wear. The Rake's Progress |Marjorie Bowen 

The orange and red domino left her partner and came straight to Marius Lyndwood. The Rake's Progress |Marjorie Bowen