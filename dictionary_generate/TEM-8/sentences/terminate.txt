Assemblywoman Lorena Gonzalez questioned whether an officer could be decertified before he or she was terminated, because members of the public would be able to bring complaints to POST directly. Sacramento Report: Jones, COVID-19 and the Irony of Remote Voting |Sara Libby and Jesse Marx |August 28, 2020 |Voice of San Diego 

Cowan said that Postal Service leaders had not eliminated overtime — but that mail sorting procedures had changed, effectively terminating it in certain ways anyway. Info About Local Post Office Operations Is Conflicting and Hard to Come By |Ashly McGlone and Kate Nucci |August 27, 2020 |Voice of San Diego 

This causes the qubits to lose their quantum state and decohere, terminating any actual quantum computing. Cosmic rays could pose a problem for future quantum computers |Neel Patel |August 26, 2020 |MIT Technology Review 

Computer scientists are generally interested in knowing whether a given rewriting system always terminates. Computer Scientists Attempt to Corner the Collatz Conjecture |Kevin Hartnett |August 26, 2020 |Quanta Magazine 

PG&E owed the firm millions of dollars when it abruptly terminated its business relationship this year. The Mystery House: How a Suspicious Multimillion Dollar Real Estate Deal Is Connected to California’s Deadliest Fire |by Scott Morris, Bay City News Foundation |August 26, 2020 |ProPublica 

And sometimes, they chose to terminate their pregnancies by having abortions. Is This the Return of Back Alley Abortions? |Sally Kohn |October 7, 2014 |DAILY BEAST 

Instead, the county filed to terminate his rights based on his mental illness. One Breakdown Can Mean Losing Your Kid Forever |ProPublica |May 30, 2014 |DAILY BEAST 

Abortifacients, by contrast, are used to terminate an existing pregnancy. Is Forced Religious Belief Coming to an Employer Near You? |Jamelle Bouie |November 27, 2013 |DAILY BEAST 

Has the IRS taken actions to terminate the offending employees? IRS Singled Out Conservative Groups for Extra Scrutiny |Megan McArdle |May 10, 2013 |DAILY BEAST 

Republic wanted to terminate its obligations and put workers in a 401(k) (or at least a more solvent Teamster pension plan). How the IRS Wrecked Your Pension |Megan McArdle |May 9, 2013 |DAILY BEAST 

And I would respectfully suggest that this interview must definitely terminate the matter one way or the other. The Pit Town Coronet, Volume I (of 3) |Charles James Wills 

He had hitherto lived for universal man:—his days should terminate on a different principle. The Pastor's Fire-side Vol. 3 of 4 |Jane Porter 

In many cases an agency is created for an indefinite period, and in these either party can terminate it whenever he desires. Putnam's Handy Law Book for the Layman |Albert Sidney Bolles 

To support his rôle as the friend of labor, he must needs terminate the sanguinary struggle. Prison Memoirs of an Anarchist |Alexander Berkman 

Hoping to terminate the thefts, Johnny complained to the overseer, though without accusing Jack. Prison Memoirs of an Anarchist |Alexander Berkman