“A space station is a space station,” says Namrata Goswami, a space policy and geopolitics expert and coauthor of Scramble for the Skies. Here’s what China wants from its next space station |Neel V. Patel |April 30, 2021 |MIT Technology Review 

The F-22, after decades of development, is finally becoming relevant to American security and geopolitics. America’s Advanced Stealth Jet Flies on 1990s Tech |Dave Majumdar |October 1, 2014 |DAILY BEAST 

In geopolitics, the doctrine of mutually assured destruction prevents the use of weapons of mass destruction against a foe. The App Bringing Out The Best/Worst in Washington’s Gays |Scott Bixby |May 31, 2014 |DAILY BEAST 

They go where the profits are, not where geopolitics dictates. Bringing U.S. Energy Policy Into the 21st Century |Will Marshall |April 1, 2014 |DAILY BEAST 

His book, he hopes, combines science, geopolitics, and larky history. BP, Putin, and the Power of Oil |Tim Teeman |March 9, 2014 |DAILY BEAST 

Everything I ever really needed to know about geopolitics I learned from watching the Olympics. The Olympics Are Already Two Days Old. This Is Your Test. |Kevin Bleyer |February 9, 2014 |DAILY BEAST 

Unfortunately possessed of strategic import, the Balkan was ravaged by geopolitics. After the Rain |Sam Vaknin 

The newcomers were all destitute, the refugees of the geopolitics of hate from both the Eastern block and from the Arab countries. After the Rain |Sam Vaknin 

These eunuchs of geopolitics do not dare to carry their military and economic clout to its logical and beneficial conclusion. After the Rain |Sam Vaknin