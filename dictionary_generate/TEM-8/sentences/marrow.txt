The other source of long-lasting antibody responses against the coronavirus is cells called plasmablasts that reside in bone marrow. 6 important questions about COVID-19 booster vaccines, answered |Purbita Saha |July 9, 2021 |Popular-Science 

So there seems to be this relationship between the diversification of our diet, eating more meat and marrow, scavenging, and hunting. What Made Early Humans Smart - Issue 102: Hidden Truths |Kevin Berger |June 30, 2021 |Nautilus 

Bone marrow transplants, which began in the 1980s for sickle-cell patients, are a cure, but finding a donor can be challenging. 50 years ago, urea showed promise as a sickle-cell treatment |Bethany Brookshire |December 17, 2020 |Science News 

The larger stone may have served as a platform on which the bones were smashed open with the smaller stone, possibly to remove marrow for eating or to obtain bone chunks suitable for shaping into tools. Two stones fuel debate over when America’s first settlers arrived |Bruce Bower |December 4, 2020 |Science News 

Some also had breaks where marrow was removed, the researchers say. This cave hosted the oldest known human remains in Europe |Bruce Bower |June 12, 2020 |Science News For Students 

In lieu of bone marrow transplants, scientists hope to use stem cells to serve as the future vectors of mutant CCR5 proteins. Gene Editing Could Erase HIV |Dr. Anand Veeravagu, MD, Michael Zhang |June 11, 2014 |DAILY BEAST 

In August 2012, anchor Robin Roberts took a leave of absence for a bone marrow transplant. This Week in Pop Culture History: Days of Our Lives, Arrested Development, and 24 Premiere |Chancellor Agard |November 3, 2013 |DAILY BEAST 

The bone marrow of the dead soldiers was depleted dramatically, and their lymph nodes had shriveled away. Sarin, Nitrogen Mustard, Cyanide & More: All About Chemical Weapons |Kent Sepkowitz |August 26, 2013 |DAILY BEAST 

The stem cells came from Hannah's bone marrow, extracted with a special needle inserted into her hip bone. The Power of Stem Cell Research Saves a Little Girl |Ilana Glazer |May 1, 2013 |DAILY BEAST 

The sample consists of the families of children who require bone marrow transplants. Don't Worry, Dads: Those Kids are Probably Yours |Megan McArdle |February 7, 2013 |DAILY BEAST 

Hardy and Hicks flung the huge marrow bones with which they happened to be engaged at the time. Hunting the Lions |R.M. Ballantyne 

Large mononuclear leukocytes probably originate in the bone-marrow or spleen. A Manual of Clinical Diagnosis |James Campbell Todd 

Myelocytes are the bone-marrow cells from which the corresponding granular leukocytes are developed. A Manual of Clinical Diagnosis |James Campbell Todd 

Polymorphonuclear leukocytes are formed in the bone-marrow from neutrophilic myelocytes. A Manual of Clinical Diagnosis |James Campbell Todd 

The same scents may also be used for pomatum, which should be made of perfectly pure lard, or marrow. The Ladies' Book of Etiquette, and Manual of Politeness |Florence Hartley