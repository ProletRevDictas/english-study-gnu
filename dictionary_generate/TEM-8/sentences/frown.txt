A deep frown will definitely take more muscles to pull off than a faint smile. Everything you’ve ever wanted to know about muscles |Claire Maldarelli and Sara Chodosh |October 3, 2021 |Popular-Science 

It also depends on how you define a frown or a smile, he says. Everything you’ve ever wanted to know about muscles |Claire Maldarelli and Sara Chodosh |October 3, 2021 |Popular-Science 

Surprisingly, that’s enough facial real estate to tell a sneer from a smile, or a laugh from a frown. New device tells smiles from frowns — even through a mask |Kathryn Hulick |December 4, 2020 |Science News For Students 

In the 1980s, Jean and Alastair Carruthers, a Canadian ophthalmologist and dermatologist, accidentally discovered the toxin’s anti-aging properties when they noticed patients receiving injections for facial spasm were also losing their frown lines. Can you get too much Botox? |By Matthew J. Lin/The Conversation |October 1, 2020 |Popular-Science 

“Lilly… Ledbetter…” we whisper to ourselves as we frown at men. Getting to Know the ‘Beyoncé Voter’ |Kelly Williams Brown |July 7, 2014 |DAILY BEAST 

Early the next morning, “Frown,” Jai Johany Johnson, is living up to his nickname in the hotel restaurant. Stacks: Hitting the Note with the Allman Brothers Band |Grover Lewis |March 15, 2014 |DAILY BEAST 

We watched her float about, a meandering frown, for two whole hours in the season premiere as she mourned the death of Matthew. ‘Downton Abbey’ Finale Review: The Depressing Demise of a Once-Great Show |Kevin Fallon |February 24, 2014 |DAILY BEAST 

I made choices that took me down a path that many people frown upon. A Porn Star’s Letter to Her Unborn Son |Aurora Snow |September 30, 2013 |DAILY BEAST 

Traditional matchmakers, eager to distinguish themselves from a dating service, would frown on this feature. Anna Gristina, the Accused Mommy Madam, and Her Matchmaking Defense |Tracy Quan |June 25, 2012 |DAILY BEAST 

The tiny frown reappeared between her eyes, lingered a trifle longer than before, and vanished. The Wave |Algernon Blackwood 

His brows came together in a frown, from which the Seneschal argued that his suggestion was not well received. St. Martin's Summer |Rafael Sabatini 

In fact, so much of her smooth brow as could be seen under a broad-brimmed straw hat was wrinkled in a decided frown. The Red Year |Louis Tracy 

Old David Arden stepped back a little, growing pale, with a sudden frown. Checkmate |Joseph Sheridan Le Fanu 

A frown momentarily darkened the cloudless brow of Aristide Pujol. The Joyous Adventures of Aristide Pujol |William J. Locke