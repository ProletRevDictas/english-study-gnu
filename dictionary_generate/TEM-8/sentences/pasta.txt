Don’t blame the baked feta pasta, blame the social media users who don’t — and sometimes can’t, given the constraints of the medium — include all of the specifics in a single TikTok video. TikTok’s viral baked feta pasta is worth the hype |Aaron Hutcherson |February 11, 2021 |Washington Post 

We assume eating the pink, sweet pasta together is intended to force couples into trauma bonding. Kraft Is Introducing ‘Candy Mac & Cheese’ for Valentine’s Day, as if We Need More to Fear |Jaya Saxena |January 29, 2021 |Eater 

From soups to Bagel Bites, hot chocolate, to single-serving portions of pasta, you’ll be dining comfortably and conveniently before you know it. The best microwaves: This way for buttery popcorn and tempting leftovers |PopSci Commerce Team |January 22, 2021 |Popular-Science 

For dinner, those chicken thighs are great on their own, or they can work their way into a pasta sauce, delicious though you’ve strayed slightly from the recipe. To Combat Cooking Burnout, Do It All at Once |Elazar Sontag |January 14, 2021 |Eater 

As soon as the broth comes to a boil, stir in the pasta and simmer until al dente, 4 to 6 minutes. Hearty and robust, Italian wedding soup is a perfect marriage of meat and vegetables |Daniela Galarza |January 7, 2021 |Washington Post 

At which point Barry orders the pasta in meat sauce, followed up by Tiramisu. Marion Barry: ‘I Did It My Way’ |Lloyd Grove |June 23, 2014 |DAILY BEAST 

Dried pasta and canned goods are prepared with nothing more than a pair of nail clippers. Tales of a Jailhouse Gourmet: How I learned to Cook in Prison |Daniel Genis |June 21, 2014 |DAILY BEAST 

Gluten-Free Pasta Tons of conventional pasta brands have joined the gluten-free ranks with their own varieties. How to Buy Gluten-Free Without Getting Duped |DailyBurn |April 12, 2014 |DAILY BEAST 

You have taken to gnawing on dried pasta, the only thing left in your larder after days of gorging. So You Are Enduring a Temporarily Paralyzing Winter Storm |Kelly Williams Brown |February 15, 2014 |DAILY BEAST 

WHERE TO EAT: Nudel Restaurant in Lenox for thoughtfully constructed pasta dishes. Get Cultured on Your Weekend Getaway: Best Trips for Art Lovers |Condé Nast Traveler |January 19, 2014 |DAILY BEAST 

But Señor Pasta had already formed his resolution, and it was not to mix at all in the affair, either as consulter or consulted. The Reign of Greed |Jose Rizal 

At seventeen, on hearing Pasta sing in Paris, she sought out the artist and solicited lessons. Heroes of the Telegraph |J. Munro 

She had brought him a box of Pasta Mack tabloids, and unfortunately there was not at that time a bath in the whole prison. A Woman's Part in a Revolution |Natalie Harris Hammond 

I went to the opera the other night and saw Pasta's "Medea" for the first time. Records of a Girlhood |Frances Ann Kemble 

Pasta and Rubini surpassed themselves in the splendor of their performance. Great Singers, First Series |George T. Ferris