This salad, which uses roasted cauliflower surrounded by crunchy, bright ingredients, is a perfect example of that. These 5 recipes featuring leftover roasted vegetables are proof you should always make extra |Kari Sonde |February 8, 2021 |Washington Post 

Break the cauliflower apart, and cut away and discard any stems. This cauliflower hater likes the vegetable one way only: Pickled |Jim Webster |February 3, 2021 |Washington Post 

Root vegetables — such as rutabagas, parsnips and turnips — are the first to come to mind, but from cauliflower to spring peas, there’s a wide range of vegetables primed for mashing. Potatoes aren’t the only vegetables you should be mashing |Aaron Hutcherson |January 20, 2021 |Washington Post 

Although it is worth cooking the cauliflower just to make this recipe, I can’t think of a roasted vegetable that wouldn’t work here instead, so feel free to substitute whatever you have on hand. A crunchy, flavorful salad makes great use of cauliflower or any leftover roasted vegetables |Ellie Krieger |January 14, 2021 |Washington Post 

In another three months, the same scene would unfold in the cauliflower field where Alcántar now stood, surrounded by tens of thousands of two- and three-leaf seedlings. How technology might finally start telling farmers things they didn’t already know |Katie McLean |December 18, 2020 |MIT Technology Review 

The entrée was smoked flat-iron beef with caramelized mint fennel, kabocha pumpkin, yellow cauliflower, and baby bok choy. My Night at the Golden Globe Awards |Ramin Setoodeh |January 14, 2013 |DAILY BEAST 

In a large bowl, toss together cauliflower and bread crumbs and serve on a warmed platter. Sam Sifton’s Thanksgiving Recipes |Sam Sifton |November 20, 2012 |DAILY BEAST 

Break cauliflower into florets and toss in a bowl with sage, lemon zest, sugar, and olive oil. Sam Sifton’s Thanksgiving Recipes |Sam Sifton |November 20, 2012 |DAILY BEAST 

Roasted Cauliflower with Anchovy Bread Crumbs It is important to note that this dish does not have an anchovy flavor. Sam Sifton’s Thanksgiving Recipes |Sam Sifton |November 20, 2012 |DAILY BEAST 

Cauliflower is delicious, potatoes can be good, and I've been thinking about experimenting with bok choy stems. Your Friday Gadget Chef Recipe: Two Day Soup |Megan McArdle |November 9, 2012 |DAILY BEAST 

There has never been such an abundance of cauliflower and peas, such rows of bean-poles, such salad-beds. Maezli |Johanna Spyri 

Small pieces of cauliflower and crotons of fried bread should garnish this dish. Dressed Game and Poultry la Mode |Harriet A. de Salis 

The red cabbage and the cauliflower are the altered descendants of a widely different sea-side plant. A Treatise on Sheep: |Ambrose Blacklock 

Peel onions, add cucumbers, cauliflower separated into flowerlets and sliced peppers. The New Dr. Price Cookbook |Anonymous 

He did not give them lentils to eat, but he gave them cauliflower au gratin and brown bread and cheese, and to drink, water. The Angel of Pain |E. F. Benson