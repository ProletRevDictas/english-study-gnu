Pollard got behind the wheel of a machine called a reverse hoe. Jimmy Breslin on JFK’s Assassination: Two Classic Columns |Jimmy Breslin |November 22, 2013 |DAILY BEAST 

I think that this is a very tough row to hoe, an a transformation that will take place over decades if it happens at all. What's the Use of a PhD? |Megan McArdle |February 21, 2013 |DAILY BEAST 

Simpson slumped around for a bit, and then, in an awkward coup de grace, proceeded to dance what she later called a "hoe-down." Hey Beyoncé, Whitney Houston Lip Synced, Too (Video) |The Daily Beast Video |January 22, 2013 |DAILY BEAST 

In a category called "About Cleveland" it reads: "im a hoe and I sleep with anybody and anything that has a DIKK." The Texas Gang Rape Dividing a Town |Christine Pelisek |March 13, 2011 |DAILY BEAST 

Her worn-out blue petticoat is lighted up by a moonbeam; in her hand she appears to have a hoe. Women in the fine arts, from the Seventh Century B.C. to the Twentieth Century A.D. |Clara Erskine Clement 

Whatever it was, the rebuke was convincing, for the woman dropped her hoe and went mumbling into the house. The Awakening and Selected Short Stories |Kate Chopin 

The mode of culture is to plow between the rows and hoe the plants carefully. Tobacco; Its History, Varieties, Culture, Manufacture and Commerce |E. R. Billings. 

He even tries to help her hoe those terrible rows of corn that has come up so beautifully and is growing so well. Dorothy at Skyrie |Evelyn Raymond 

The narrow or hilling hoe follows the operation of the sprouting hoe. Tobacco; Its History, Varieties, Culture, Manufacture and Commerce |E. R. Billings.