In this case, unlike the scenario of not being eaten, your hope is the bike thief will move on to easier prey. The most secure ways to lock up your bike |By Michael Frank/Cycle Volta |August 26, 2020 |Popular-Science 

Many barges were stuck at sea and became easy prey for pirates. Is Maritime Piracy Back from the Dead? |Eromo Egbejule |August 25, 2020 |Ozy 

This is the longest known prey of a marine reptile from the dinosaur age, and may be the oldest direct evidence of a marine reptile eating an animal larger than a human, researchers report August 20 in iScience. This ichthyosaur died after devouring a creature nearly as long as itself |Maria Temming |August 20, 2020 |Science News 

Bigger dingoes can hunt bigger prey, notes Letnic, which could have unknown impacts on Australian ecosystems. Culling dingoes with poison may be making them bigger |Jake Buehler |August 19, 2020 |Science News 

The new cytosine-converting enzyme, however, was as lethal to mammalian cells as it was to bacterial prey. A bacterial toxin enables the first mitochondrial gene editor |Jack J. Lee |July 13, 2020 |Science News 

And they prey on those that society will be least likely to believe. How I Stopped My Rapist |Natasha Alexenko |November 24, 2014 |DAILY BEAST 

As he relishes his triumph, a larger, grinning version of the man materializes in the background, eyeing his prey. ‘Interstellar’ Is Wildly Ambitious, Very Flawed, and Absolutely Worth Seeing |Marlow Stern |November 7, 2014 |DAILY BEAST 

When it comes to protecting birds of prey, Illinois state law can be an ass. He Faces Jail for Rescuing Baby Eagles |Michael Daly |November 2, 2014 |DAILY BEAST 

Yes, Levine plays the role of a stalker and Prinsloo that of his “prey,” but she never comes across as a victim. Sex, Blood and Maroon 5: Pop Culture’s Wounds Run Deep |Lizzie Crocker |October 3, 2014 |DAILY BEAST 

The trucking roads make it easier for predators to wipe out prey. Our Trip to The Climate War's Ground Zero |Darren Aronofsky |September 19, 2014 |DAILY BEAST 

When a hungry lion is watching for prey, the sight of any animal will make him commence stalking it. Hunting the Lions |R.M. Ballantyne 

He usually seizes his prey by the flank near the hind leg, or by the throat below the jaw. Hunting the Lions |R.M. Ballantyne 

Now it seemed to crouch as though ready to spring, and I could hear the savage growling as of some beast of prey. Uncanny Tales |Various 

They appeared and vanished about the corners of the Islands and promontories like birds swooping after prey. Ancestors |Gertrude Atherton 

It was monstrous that this English damask rose should fall a prey to so detestable a person as the Comte de Lussigny. The Joyous Adventures of Aristide Pujol |William J. Locke