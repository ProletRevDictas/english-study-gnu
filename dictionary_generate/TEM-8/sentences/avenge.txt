Bill Murray stars as the titular character, a wacky oceanographer who sets out to avenge the death of his partner Esteban by stalking the alleged shark behind the crime. The best things to do in the D.C. area the week of Aug. 26-Sept. 1 |Anying Guo, Hau Chu, Fritz Hahn, Kelsey Ables |August 26, 2021 |Washington Post 

We lose our humanity, the space to be bereft, the chance to avenge and honor our ancestors, to fight for their legacy to live among everyday consciousness. A vacation town promises rest and relaxation. The water knows the truth. |Nneka M. Okona |August 26, 2021 |Vox 

In the aftermath of a national tragedy, we are supposed to come together and say “never forget,” to agree on the heroes and the villains, on who was at fault and how their culpability must be avenged. What Mike Fanone Can't Forget |Molly Ball |August 5, 2021 |Time 

The first five episodes of the series found Assane avenging his father’s death—and the next five bring us right back into the action with the protagonist doing whatever it takes to protect the ones he loves most. What to Know About the Literary Origins of Netflix’s Buzzy Crime Drama Lupin |Annabel Gutterman |June 11, 2021 |Time 

Using Lupin’s tactics, Assane sets out to avenge an act of savage cruelty against his late father. Lupin's Return to Netflix Is Putting Omar Sy Back in the Spotlight, Whether He Wants It or Not |Vivienne Walt |June 8, 2021 |Time 

Protestors chanted “with our blood and our souls, we will avenge you, oh martyr.” A New Intifada? Israel’s Arab Citizen Uprising Spreads |Creede Newton |November 10, 2014 |DAILY BEAST 

Within days of 9/11 he was talking about a “crusade” to avenge the blow, without realizing how freighted that word was. Three Dicks: Cheney, Nixon, Richard III and the Art of Reputation Rehab |Clive Irving |July 27, 2014 |DAILY BEAST 

So I think that was personally felt too, my personal desire to avenge my own laziness and mistakes in life. The Book of B.J. Novak: An Absurdist, Scathingly Funny Literary Debut |Caryn James |February 6, 2014 |DAILY BEAST 

Marius is a Roman soldier whose father is killed and he goes to avenge the death. A Study in Xbox One Violence: Dead Rising 3 Vs. Ryse: Son of Rome |Alec Kubas-Meyer |December 1, 2013 |DAILY BEAST 

Now, 13 years later, Stan's little brother Matthew is back to avenge his death. ‘The Marshall Mathers LP 2’ Review: Eminem’s a Great Rapper With Nothing to Say |Rawiya Kameir |November 5, 2013 |DAILY BEAST 

But it should not be by embracing revenge through the treasons, whose arms were extended to receive and to avenge him. The Pastor's Fire-side Vol. 3 of 4 |Jane Porter 

It is their fate to experience this, as it becomes the task of the historian to avenge their memories. Madame Roland, Makers of History |John S. C. Abbott 

How boldly he acted during the Haymarket tragedy—publicly advised the use of violence to avenge the capitalist conspiracy. Prison Memoirs of an Anarchist |Alexander Berkman 

Certain of the fate of my grandfather, but lacking the energy to avenge him, he made to me this revelation on his death-bed. The Pilgrim's Shell or Fergan the Quarryman |Eugne Sue 

Then many of the men fell to 112 talking, & they said: Eirik the Earl will not fight to avenge his father. The Sagas of Olaf Tryggvason and of Harald The Tyrant (Harald Haardraade) |Snorri Sturluson