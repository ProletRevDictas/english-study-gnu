Former 49ers owner Eddie DeBartolo famously dubbed it a “pigsty.” San Fran Kisses Its 70,000-Person Toilet Goodbye |Jon Rochmis |August 15, 2014 |DAILY BEAST 

This week: We learn Versailles was a pigsty, Virginia Woolf despised writing articles, and money is very dangerous. Best of Brit Lit |Peter Stothard |June 18, 2009 |DAILY BEAST 

There was once a fisherman who lived with his wife in a pigsty, close by the seaside. Grimms' Fairy Tales |The Brothers Grimm 

Between us and the dwelling house there was a disused pigsty. Fifty-Two Stories For Girls |Various 

A portrait might have been finished, and with disengaged eyes Evelyn would survey what he would certainly call his pigsty. The Angel of Pain |E. F. Benson 

There were two windows: one looked full on the fir-trees; the other on the farm-yard, with the pigsty closing the view. Blackwood's Edinburgh Magazine, Volume 68, No. 421, November 1850 |Various 

The pigsty soon became too narrow.44 The animals obstructed the farmyard, broke down the fences, and went gnawing at everything. Bouvard and Pcuchet |Gustave Flaubert