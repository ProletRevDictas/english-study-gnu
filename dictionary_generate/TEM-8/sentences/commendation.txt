We’re looking to find any recommendation or commendation of the company, anywhere else online, on a site that isn’t owned or operated by the business. E-A-T auditing: How to level up your credibility game |Nichola Stott |July 20, 2022 |Search Engine Land 

He has already burned the letters of commendation his relatives received for their work with American contractors or allied militaries. These Afghans Won the Visa Lottery Two Years Ago — Now They’re Stuck in Kabul and Out of Luck |by Dara Lind |August 27, 2021 |ProPublica 

Some are stuck themselves, sending images of the badges and permission letters and commendations they’ve accumulated. You Have a Notification: ‘They Are About to Kill Us’ |Erik Nelson |August 23, 2021 |Ozy 

In India, Prime Minister Narendra Modi’s government has issued a commendation to a soldier who tied a Kashmiri man to his jeep and paraded him through a village. Be Very Afraid: A Virtual House of Horrors |Tracy Moran |October 25, 2020 |Ozy 

The governor was defiant, saying Obama “earned” his commendation. Obama Escapes Scandals in New Jersey, but What’s in It for Christie? |David Freedlander |May 29, 2013 |DAILY BEAST 

In a civilized world, I would have received a commendation of some sort. Turn Off Your Damn Cellphones |Justin Green |May 16, 2013 |DAILY BEAST 

Two legmen deserve special commendation for reporting under fire. 2010 Wackiest Candidate Awards |Samuel P. Jacobs |November 1, 2010 |DAILY BEAST 

Great preparations had been made, and the success must have been perfect to win so general and hearty a commendation. Glances at Europe |Horace Greeley 

Jane was highly gratified by this commendation, and most eagerly availed herself of his most valuable offer. Madame Roland, Makers of History |John S. C. Abbott 

The principles herein stated are not mere theories, but they have the commendation of having stood the test of use. The value of a praying mother |Isabel C. Byrum 

"Flora May," was the delighted answer, now that the aunt had committed herself by commendation. The Daisy Chain |Charlotte Yonge 

This establishment of a test workhouse for the able-bodied received at first the warm commendation of the Central Authority. English Poor Law Policy |Sidney Webb