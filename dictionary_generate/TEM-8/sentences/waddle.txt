As I head back to the car, I look up and watch the old folks slowly waddle to a nearby building for shelter. Pablo Escobar’s Private Prison Is Now Run by Monks for Senior Citizens |Jeff Campagna |June 7, 2014 |DAILY BEAST 

That fact can be confirmed by standing on any street corner in any city of the country and watching Americans waddle by. Free to Be Fat |Richard B. McKenzie |November 23, 2011 |DAILY BEAST 

One of them got a small ear in its bill and started away on a swift waddle with the rest of the flock trailing behind. The Red Cow and Her Friends |Peter McArthur 

All the duck-kind waddle; divers and auks walk as if fettered, and stand erect on their tails: these are the compedes of Linnæus. The Natural History of Selborne, Vol. 2 |Gilbert White 

His very walk, for all his drills, was the ponderous waddle of the stage rustic. Notes of a Camp-Follower on the Western Front |E. W. Hornung 

Very fat people waddle when they walk, though few of them realize it. How to Analyze People on Sight |Elsie Lincoln Benedict and Ralph Paine Benedict 

Corey seemed to prick up his ears, and began to waddle rapidly toward the entrance. Police Your Planet |Lester del Rey