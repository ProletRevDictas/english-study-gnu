In March, police arrested a group of wealthy businessmen and government officials who were about to dine on illegal tiger meat. China’s Internet Is Freer Than You Think |Brendon Hong |December 27, 2014 |DAILY BEAST 

When they do dine, the Hitchcocks sometimes use Limoges china marked “Plaza Athénée.” Alfred Hitchcock’s Fade to Black: The Great Director’s Final Days |David Freeman |December 13, 2014 |DAILY BEAST 

“When you attack public sector unions now, you are attacking the heart of the U.S. labor movement,” says Dine. The GOP and Police Unions: A Love Story |Eleanor Clift |December 12, 2014 |DAILY BEAST 

He refused to dine with people, because he did not like being agitated during meal times. The Death of the English Eccentric |Tom Sykes |November 25, 2014 |DAILY BEAST 

Lohse and his beleaguered fellow pledges were, he claims, forced to chug vinegar and to dine on the dreaded “vomlet.” An Ivy League Frat Boy’s Shallow Repentance |Stefan Beck |November 24, 2014 |DAILY BEAST 

After we had engaged our rooms, we drove back to the hotel where Liszt was staying, and where we were to dine immediately. Music-Study in Germany |Amy Fay 

“Master and Mr. Pickwick is a going to dine here at five,” replied the fat boy. The Posthumous Papers of the Pickwick Club, v. 2(of 2) |Charles Dickens 

We dine late, and as there are a good many boarders, it takes some time always to change the plates. Music-Study in Germany |Amy Fay 

At the office he very wisely told the story to the other gentlemen there, with whom he was to dine next day. The Book of Anecdotes and Budget of Fun; |Various 

I've heard my father say that at the big hotels at Atlantic City and other places they have a band play while the people dine. Dorothy at Skyrie |Evelyn Raymond