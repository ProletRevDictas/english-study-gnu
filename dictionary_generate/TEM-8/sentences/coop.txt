The team coupled four years of data from these chickens, kept in coops across the state to monitor the spread of diseases, with a world atlas of artificial night sky brightness. Dim lighting may raise the risk of a West Nile virus exposure |Bethany Brookshire |March 24, 2021 |Science News 

So is the literal “turkey point of view” offered by the GoPros attached to the turkeys as they run around the coop. A Turkey's View of Thanksgiving |The Daily Beast Video |November 26, 2014 |DAILY BEAST 

Not in hell, but in my own chicken coop, which does not cost $1500 to enter. What Did TJ Mean By “Pursuit of Happiness,” Anyway? |P. J. O’Rourke |June 8, 2014 |DAILY BEAST 

This renewable (I mean renewed every single damn day) bio-energy (otherwise known as fetid rot) helps keep the coop warm. What Did TJ Mean By “Pursuit of Happiness,” Anyway? |P. J. O’Rourke |June 8, 2014 |DAILY BEAST 

I had to chase the chickens back into the coop and close the door so I could finish with the Augean Stables of the chicken pen. What Did TJ Mean By “Pursuit of Happiness,” Anyway? |P. J. O’Rourke |June 8, 2014 |DAILY BEAST 

Meditations on the pursuit of happiness while cleaning the chicken coop. What Did TJ Mean By “Pursuit of Happiness,” Anyway? |P. J. O’Rourke |June 8, 2014 |DAILY BEAST 

Squinty could look out, but the slats were as close together as those in a chicken coop, and the little pig could not get out. Squinty the Comical Pig |Richard Barnum 

He sat in a confined little metal coop of a cabin, hardly enough in which to stand erect. The Stars, My Brothers |Edmond Hamilton 

The midget recovered Alfred's knife from the dust and walked over to the trailer that he noted had a wooden coop of slats aboard. David Lannarck, Midget |George S. Harney 

One of them struck a hen-coop on the Saratoga, in which one of the sailors kept a fighting cock. Stories of Our Naval Heroes |Various 

Then I went around and picked them off the branches until I had half a dozen plump ones stowed away in a coop. The Red Cow and Her Friends |Peter McArthur