Some people can test positive for the virus’s genetic material for months after they get well, and shed no infectious virus. A Hong Kong man got the new coronavirus twice |Erin Garcia de Jesus |August 26, 2020 |Science News For Students 

That cut allows the virus to fuse with the cell membrane and dump its genetic material into the cell. New treatments aim to treat COVID-19 early, before it gets serious |Tina Hesman Saey |August 24, 2020 |Science News 

Even though people probably detest mosquitoes more than moth larvae that can damage broccoli, the fact that the Florida Keys project involves genetic modification still stirs passion. Genetically modified mosquitoes have been OK’d for a first U.S. test flight |Susan Milius |August 22, 2020 |Science News 

Gene therapy trials are underway for several different genetic diseases, including sickle cell anemia, at least two different forms of inherited blindness, and Alzheimer’s, among others. A Year After Gene Therapy, Boys With Muscular Dystrophy Are Healthier and Stronger |Vanessa Bates Ramirez |July 30, 2020 |Singularity Hub 

Now, a child who shows up at a hospital with severe mycobacterial infection is tested for these genetic defects and receives injections of interferon gamma. Our Genes May Explain Severity of COVID-19 and Other Infections |Monique Brouillette |July 27, 2020 |Quanta Magazine 

The genetic material can grow quickly, but are typically riddled with errors or defects. Design Your Own Dinosaur: The Era of Custom DNA |Justin Jones |January 8, 2015 |DAILY BEAST 

But a 2011 study of genetic evidence from 30 ethnic groups in India disproved this theory. The Himalayas’ Hidden Aryans |Nina Strochlic |January 3, 2015 |DAILY BEAST 

Prevalence depends on context, and sometimes unique advantages outweigh the genetic costs. Mongooses, Meerkats, and Ants, Oh My! Why Some Animals Keep Mating All in the Family |Helen Thompson |December 29, 2014 |DAILY BEAST 

Cryobanks, which screen for genetic disorders and STDs, cost big bucks; see here for some of the charges. Have Sperm, Will Travel: The ‘Natural Inseminators’ Helping Women Avoid the Sperm Bank |Elizabeth Picciuto |November 29, 2014 |DAILY BEAST 

Mitochondrial intervention is the practice of replacing DNA that carries a genetic disease. Want Blue Eyes With That Baby?: The Strange New World of Human Reproduction |Eleanor Clift |November 24, 2014 |DAILY BEAST 

The most influential attempt at a genetic classification of the various historical forms of government was that of Aristotle. Elements of Folk Psychology |Wilhelm Wundt 

On the contrary, taking the genetic view of childhood should give us certain advantages. The Science of Human Nature |William Henry Pyle 

Hundreds of thousands of years of genetic weeding-out have produced things that would give even an electronic brain nightmares. Deathworld |Harry Harrison 

The intellectual nature of man is the same as that of angels who have no genetic connection with us. The Other Side of Evolution |Alexander Patterson 

He did not employ the comparative and genetic methods to which we owe the chief scientific achievements of the last half-century. The Wonders of Life |Ernst Haeckel