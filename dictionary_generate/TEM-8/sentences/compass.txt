I fundamentally feel that the Gallaudet Board of Trustees has lost its way and its moral compass. Racism allegations at Gallaudet U pushed two officials to resign, they say. The school says it’s now committing to permanent changes. |Lauren Lumpkin |October 29, 2020 |Washington Post 

It also includes a compass and flashlight, so it makes getting lost almost impossible. Power banks to keep you in charge |PopSci Commerce Team |October 28, 2020 |Popular-Science 

Under Tomé’s watch, the shipping giant is resetting its compass and choosing its path with care. How CEO Carol Tomé reset focus at UPS |kristenlbellstrom |October 23, 2020 |Fortune 

Using just a depth-sensing camera, GPS, and compass data, it learned to enter a space much as a human would, and find the shortest possible path to its destination without wrong turns, backtracking, or exploration. Facebook is training robot assistants to hear as well as see |Karen Hao |August 21, 2020 |MIT Technology Review 

While this “rectangular peg problem” seems like the kind of question a high school geometry student might settle with a ruler and compass, it has resisted mathematicians’ best efforts for decades. New Geometric Perspective Cracks Old Problem About Rectangles |Kevin Hartnett |June 25, 2020 |Quanta Magazine 

Muscovites call their favorite station “Ukho Moskvy” (Ear of Moscow) and see it as an institution, a compass for society. The Kremlin Is Killing Echo of Moscow, Russia’s Last Independent Radio Station |Anna Nemtsova |November 7, 2014 |DAILY BEAST 

He repeated it again, slowly: “He has no values…He has no moral compass whatsoever.” Will Chris Christie Bow to Iowa’s Pork Kings? |Olivia Nuzzi |November 1, 2014 |DAILY BEAST 

It radiates her inner light and compass, her disregard for status quo. Why Maya Angelou Loved Sherry, The Drink of Brilliant Renegades |Jordan Salcito |June 15, 2014 |DAILY BEAST 

He goes on to compass the very nature of memory by way of considering how we memorialize mass death. Geoff Dyer at Sea: Unmoored but on Target |Melissa Holbrook Pierson |May 21, 2014 |DAILY BEAST 

Boyd does have a moral compass—not yours or mine—but he does have one. Kentucky’s Finest Antihero: Walton Goggins on Justified’s Chameleon Villain |Allen Barra |February 11, 2014 |DAILY BEAST 

She habitually ate chocolates for their sustaining quality; they contained much nutriment in small compass, she said. The Awakening and Selected Short Stories |Kate Chopin 

The manual compass on these organs seldom extended higher than f2 or g3, though it often went down to GG. The Recent Revolution in Organ Building |George Laing Miller 

Later it was extended to F, 30 notes, which is the compass generally found in England. The Recent Revolution in Organ Building |George Laing Miller 

This was the point of compass revealed by the astrologer as most favourable to the young candidate for manly honours. Our Little Korean Cousin |H. Lee M. Pike 

With the exception of the Celestes, which go down to FF only, every stop is complete, of full compass. The Recent Revolution in Organ Building |George Laing Miller