Tormented by all-day nausea, I sought comfort in bed and on the yoga mat. Why I Live in Bike Shorts |Aleta Burchyski |September 13, 2020 |Outside Online 

With an acupressure mat, you get acupressure, sans precision. My Chronic Pain Was No Match for This Mat |Aleta Burchyski |September 4, 2020 |Outside Online 

Each mat goes for a blitzkrieg-like concentration—my model has 7,992 spikes, each one-eighth inch long. My Chronic Pain Was No Match for This Mat |Aleta Burchyski |September 4, 2020 |Outside Online 

The best option to take care of your joints and your floors is a good yoga mat. Working out at home? Here’s how to keep your house from smelling like a gym. |Harry Guinness |September 3, 2020 |Popular-Science 

This coating is actually a mat of nanofibers with a very large surface area, which increases its energy storage capacity. Scientists Found a Way to Turn Bricks Into Batteries |Edd Gent |August 17, 2020 |Singularity Hub 

If they were well thought through, with a clear plan of execution, she was in, and ready to go to the mat. The Valerie Jarrett I Know: How She Saved the Obama Campaign and Why She’s Indispensable |Joshua DuBois |November 18, 2014 |DAILY BEAST 

Zalwar Khan returns quickly and begins his morning prayers, spreading out a plastic mat and folding his arms over his chest. Heart of Darkness: Into Afghanistan’s Taliban Valley |Matt Trevithick, Daniel Seckman |November 15, 2014 |DAILY BEAST 

Besides, neither the Israelis nor the Palestinians are exactly rolling out the welcome mat for Kerry. Everyone Says John Kerry Should Stay Out of the Middle East |Josh Rogin |July 13, 2014 |DAILY BEAST 

It would just throw himself onto a huge, soft mat covered in a bit of sand, and then it would get up right away. Game of Thrones’ New Hunk Michiel Huisman on the Revamped Daario Naharis |Marlow Stern |April 22, 2014 |DAILY BEAST 

By remarkable contrast, younger musicians who really go to the mat for political controversy can become superstars. The GOP’s Real Ted Nugent Problem |James Poulos |February 22, 2014 |DAILY BEAST 

He had repeated till he was thrice weary the statement that "the Cat lay on the Mat and the Rat came in." Kipling Stories and Poems Every Child Should Know, Book II |Rudyard Kipling 

"Let me get the man something to eat," said Yung Pak as the monk seated himself upon a mat. Our Little Korean Cousin |H. Lee M. Pike 

He went back to his praying mat and bent again toward the west, where the Holy Kaaba enshrines the ruby sent down from heaven. The Red Year |Louis Tracy 

We here saw an opium-eater, lying stretched out upon a mat on the floor. A Woman's Journey Round the World |Ida Pfeiffer 

The mat shop is beginning to affect my health: the dust has inflamed my throat, and my eyesight is weakening in the constant dusk. Prison Memoirs of an Anarchist |Alexander Berkman