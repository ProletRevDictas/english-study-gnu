Now known as “Sir” to his students, Rohit, 49, is tall and dark-complexioned, with a firm yet calm poise. A New Martial Art Keeps Cult Leader’s Teachings Alive |Daniel Malloy |December 18, 2020 |Ozy 

The preteen Miss Thang has matured into a grown woman full of poise and compassion. 7 Moments We Loved At The 2020 Soul Train Awards |cmurray |November 30, 2020 |Essence.com 

Denver Coach Vic Fangio is known for exotic schemes and blitzes, and he made Tagovailoa, who had been defined by his poise and decisiveness, look like an uncertain rookie. What to know from NFL Week 11: Carson Wentz looks lost, and Taysom Hill is just getting started |Adam Kilgore |November 23, 2020 |Washington Post 

He completed 20 of 28 passes for 248 yards, throwing with poise and precision. What to know for NFL Week 9: Tua Tagovailoa arrives, and Tom Brady’s Buccaneers take a beating |Adam Kilgore |November 9, 2020 |Washington Post 

What’s more, this compact crossover has been fully redesigned, with all the poise and pluck of a Porsche Macan. A vote for eco-friendly rides |Joe Phillips |October 16, 2020 |Washington Blade 

Endowing the feverish, PR-patrolled world of presidential politics with thoughtfulness and poise—now that would be radical. Inside The Secret World of London’s National Gallery |Tim Teeman |November 8, 2014 |DAILY BEAST 

Zaks had to find the delicate poise between vivid restating and slavish reenactment. New York’s Greatest Show Or How They Did Not Screw Up ‘Guys and Dolls’ |Ross Wetzsteon |April 6, 2014 |DAILY BEAST 

This was the world of Gustave H. our narrator assumes, one of refinement, poise, and impeccable service. Wes Anderson’s Austrian Muse: Stefan Zweig |Lucy Scholes |February 26, 2014 |DAILY BEAST 

And Rush Limbaugh only served to help turn her into a model of responsible activism, engagement, and political poise. Martha Plimpton on Women’s Rights, Sandra Fluke, and Organization A is For |Martha Plimpton |November 5, 2012 |DAILY BEAST 

She's handled her transition from reality TV star to the real world with grace and poise. From ‘The Hills’ to Over the Hill: Lauren Conrad’s Premature Aging |Anna Klassen |September 24, 2012 |DAILY BEAST 

She was a woman of too much natural and acquired poise to remain askew under any shock. Ancestors |Gertrude Atherton 

The Hindu walks with a great deal of poise, in fact, very much like an elephant, but he also has the agility of the panther. Kari the Elephant |Dhan Gopal Mukerji 

He slowed down the car, but Miss Scovill sat upright and recovered her mental poise, though with evident effort. Mystery Ranch |Arthur Chapman 

The whole figure had the poise and lightness of a vision; yet in the face an exquisite human tenderness smiled out. The Dragon Painter |Mary McNeil Fenollosa 

Splendid was the emir in form and face, with broad shoulders and lordly height and poise. God Wills It! |William Stearns Davis