Interviews were conducted in Arizona, Florida and North Carolina as part of a joint project by the Kaiser Family Foundation and Cook Political Report. Election live updates: Trump returns to Wisconsin; Biden to face live audience at town hall |Colby Itkowitz, Felicia Sonmez, John Wagner |September 17, 2020 |Washington Post 

The project took data on the soil and slopes across California and then included wildfire risk and climate projections, and used that to show which roadways were vulnerable to post-fire debris flows. California wildfires may give way to massive mudslides |Ula Chrobak |September 17, 2020 |Popular-Science 

Launching a project to grow more palm oil on less land was the easy part, he knew. The Environmental Headache in Your Shampoo - Issue 90: Something Green |Anastasia Bendebury & Michael Shilo DeLay |September 16, 2020 |Nautilus 

We urge more investors to invest capital into high-impact projects where everyone succeeds as a result. Opportunity Zones haven’t fully reached their potential, but don’t write them off yet |jakemeth |September 16, 2020 |Fortune 

That project began in 2018 with Kerri Evelyn Harris's campaign, and the vote patterns today will reveal whether the left can make more gains with suburbanites. The Trailer: The First State goes last |David Weigel |September 15, 2020 |Washington Post 

I started just writing these songs, at first it felt like a project or something. Deer Tick's John McCauley on Ten Years in Rock and Roll |James Joiner |January 2, 2015 |DAILY BEAST 

Thus begins an episode of The Mindy Project centered around a guy trying to have butt sex with his girlfriend. Year of the Butt: How the Booty Changed the World in 2014 |Kevin Fallon |December 30, 2014 |DAILY BEAST 

Riots broke out in 1994, after Iranian authorities replaced a Sunni mosque in Mashad with a development project. The Dangerous Drug-Funded Secret War Between Iran and Pakistan |Umar Farooq |December 29, 2014 |DAILY BEAST 

Gurley was gunned down on Nov. 20, when a pair of cops was patrolling the rough housing project. Protesters Demand Justice For Gurley As Gap Grows Between Cops and NYC |M.L. Nestel |December 28, 2014 |DAILY BEAST 

Opechatesgays.com is one project of a much larger organization, EthicalOil.org—and here is where things get really interesting. How Canadian Oilmen Pinkwash the Keystone Pipeline |Jay Michaelson |December 28, 2014 |DAILY BEAST 

The worthy knight not being now alive to veto the project, a figure of him has been placed opposite the College in Edmund Street. Showell's Dictionary of Birmingham |Thomas T. Harman and Walter Showell 

Her black eyes were fixed intently on his face, but she was thinking, weighing in her mind some suddenly-formed project. The Red Year |Louis Tracy 

Very soon I induced my directors to adopt the view that the railway company must encourage and help the project. Fifty Years of Railway Life in England, Scotland and Ireland |Joseph Tatlow 

New York is like one of those nightmares a certain class of writers project and label 'Earth in the Year 2000.' Ancestors |Gertrude Atherton 

The project of a congress was accordingly abandoned, and everywhere recrimination gave place to rejoicing. The Eve of the Revolution |Carl Becker