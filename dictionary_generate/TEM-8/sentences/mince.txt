Ashley Dudarenok, founder, AlariceCompanies are using QR codes, mobile apps and third-party services to build consumer confidence in the origin and authenticity of the goods they sell, from luxury handbags to bags of frozen beef mince. The Unlikely New Champion of Consumer Rights: China |Charu Kasturi |December 17, 2020 |Ozy 

A high caloric dinner ensues, consisting of turkey, stuffing, gravy with giblets, mashed potatoes, squash, turnips, and green bean casserole, complemented by mince and pumpkin pies. Remembrance of Thanksgivings past |Valerie Blake |November 27, 2020 |Washington Blade 

The Omega includes accessories to make nut-butter and mince herbs. These pieces of kitchen gear make excellent gifts |PopSci Commerce Team |October 6, 2020 |Popular-Science 

But Marcouch does not mince his words: “The greatest insult of ISIS may even be toward the Muslims and Islam itself,” he tells us. ISIS’s Black Flags Are Flying in Europe |Nadette De Visser |July 28, 2014 |DAILY BEAST 

Musk was never one to mince words, but has recently unleashed aggressive broadsides on ULA. SpaceX’s Dragon V2 Will Land Exactly Where It Wants To |Zach Rosenberg |May 30, 2014 |DAILY BEAST 

Santa snacks on rice pudding in Denmark, sponge cake in Chile, Kulkuls in India, and mince pies in the U.K. 8 Facts You Never Knew About Christmas |Brandy Zadrozny |December 24, 2013 |DAILY BEAST 

Never one to mince her words, Leakes is as bawdy as they get on reality television. ‘Ebony’ Cover With NeNe Leakes Unleashes a Firestorm of Criticism |Karu F. Daniels |November 27, 2012 |DAILY BEAST 

Christine Lagarde doesn't mince words about economy during her first major policy address as IMF chief. The Woman in Charge |Eleanor Clift |September 16, 2011 |DAILY BEAST 

Place these an inch from each other; egg the paste all round and fold the edge of it over the balls of mince. Dressed Game and Poultry la Mode |Harriet A. de Salis 

Mince an onion; take about a dozen peppercorns, twenty juniper berries, three bayleaves, and put these into a gill of vinegar. Dressed Game and Poultry la Mode |Harriet A. de Salis 

Take a cold dressed rabbit, mince all the meat, mix in with it an equal quantity of bread soaked in milk squeezed dry. Dressed Game and Poultry la Mode |Harriet A. de Salis 

Mince two ounces of lean ham, one truffle, and six mushrooms; stir this mixture into a gill of white sauce. Dressed Game and Poultry la Mode |Harriet A. de Salis 

When it is half done, mince it with the bacon, season, and add half a clove of garlic and pound all smoothly in a mortar. Dressed Game and Poultry la Mode |Harriet A. de Salis