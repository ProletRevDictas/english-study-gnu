Well, those are 50 simple things that you can do to feel self-righteous and none of them are going to save the world. Two (Totally Opposite) Ways to Save the Planet (Ep. 346 Rebroadcast) |Stephen J. Dubner |July 29, 2021 |Freakonomics 

I do not want to offend another person, nor do I want to offend a righteous and loving God. Va. House of Delegates candidate defends anti-transgender comments |Michael K. Lavers |April 13, 2021 |Washington Blade 

The second is social distancing and masking, behaviors now weaponized by the governors of Texas and Mississippi, among others, with the implication that defying public health recommendations represents a righteous statement of freedom. COVID Experts: We’re Putting Out Campfires but the Forest Fire Rages - Facts So Romantic |Robert Bazell |March 17, 2021 |Nautilus 

She thought about how she could be more compassionate — less “self-righteous” — and stop pushing her brother away. After Capitol riots, desperate families turn to groups that ‘deprogram’ extremists |Paulina Villegas, Hannah Knowles |February 5, 2021 |Washington Post 

Instead of recasting the Lupin role yet again, creators George Kay and François Uzan dreamed up a righteous present-day outlaw who takes inspiration from the books. The 5 Best New Shows Our TV Critic Watched in January 2021 |Judy Berman |January 29, 2021 |Time 

From righteous fury to faux indignation, everything we got mad about in 2014—and how outrage has taken over our lives. The Daily Beast’s Best Longreads, Dec 15-21, 2014 |William Boot |December 21, 2014 |DAILY BEAST 

Perhaps more telling, state media called the attack on the studio “a righteous deed.” Kim Jong Un’s Kid Gloves Are Now Off |Gordon G. Chang |December 17, 2014 |DAILY BEAST 

In the Jewish community, Christians who saved Jews from the Holocaust are known as “righteous gentiles.” The Straight Hero of Cameroon’s Gays |Jay Michaelson |December 10, 2014 |DAILY BEAST 

As righteous and honest as his anger may be—“They basically just filmed my riffs,” says Meltzer— it is always tinged with hope. Brad Meltzer's Passion For Reuniting America With Its Historic Objects |Oliver Jones |November 7, 2014 |DAILY BEAST 

He was “v[ery] hot under the collar ... frustrated and mad, self-righteous.” How the Reagan White House Bungled Its Response to Iran-Contra Revelations |Malcolm Byrne |November 3, 2014 |DAILY BEAST 

Even so ye also outwardly appear righteous unto men, but inwardly ye are full of hypocrisy and iniquity. His Last Week |William E. Barton 

These are kept as his law, when obeyed, because of his authority as righteous moral Governor of all. The Ordinance of Covenanting |John Cunningham 

All the wrong wrought was his, and yet he sat there, calmly eying me, as though he were a righteous judge and I the culprit. The Soldier of the Valley |Nelson Lloyd 

The want of this on the part of the wicked being a curse, the enjoyment of it by the righteous is a privilege. The Ordinance of Covenanting |John Cunningham 

Surely the righteous shall give thanks (confess) unto thy name: the upright shall dwell in thy presence. The Ordinance of Covenanting |John Cunningham