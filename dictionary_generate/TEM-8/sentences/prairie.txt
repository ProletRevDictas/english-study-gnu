These underwater prairies also support near-shore and offshore fisheries, and protect coastlines as well as other marine habitats. How planting 70 million eelgrass seeds led to an ecosystem’s rapid recovery |Joseph Polidoro |October 14, 2020 |Science News 

The journey took them through Ohio to Illinois, across the prairie, then south toward the deserts. Gender Is What You Make of It - Issue 88: Love & Sex |Charles King |August 5, 2020 |Nautilus 

That has put prairies in danger, as people have plowed under many grasslands to grow crops. Scientists Say: Prairie |Bethany Brookshire |July 13, 2020 |Science News For Students 

Usually, when scientists talk about prairies, they are referring to the grasslands in North America. Scientists Say: Prairie |Bethany Brookshire |July 13, 2020 |Science News For Students 

There, prairies stretch from the area east of the Rocky Mountains to the Mississippi River. Scientists Say: Prairie |Bethany Brookshire |July 13, 2020 |Science News For Students 

He stands, one assumes on a porch, which overlooks a prairie. Huckabee 2016: Bend Over and Take It Like a Prisoner! |Olivia Nuzzi |January 8, 2015 |DAILY BEAST 

One year later and 10 blocks away, my mother came into the world, the granddaughter of those pioneers who had roamed the prairie. Those Kansas City Blues: A Family History |Katie Baker |October 24, 2014 |DAILY BEAST 

There, abandoned “ghost towns” populate the prairie fields and deserts, serving as a reminder of a not-so-distant past. The Ghost Towns of Arizona Live On |Justin Jones |July 10, 2014 |DAILY BEAST 

Harvey now lives in the central prairie province of Saskatchewan, Canadian news reports say, along with her husband and son. Canada’s Newest Refugee: A Florida Mom Convicted of Unlawful Sex With a Minor |Tim Mak |June 10, 2014 |DAILY BEAST 

Because there is always this about the land, about prairie and pond and mountain: they never go away. The Death of a Rodeo Cowboy |Peter Richmond |May 11, 2014 |DAILY BEAST 

This, thought I, is a dismal-looking outcome—two men and a dead horse left high and dry on the sun-flooded prairie. Raw Gold |Bertrand W. Sinclair 

Not while I had the open prairie underfoot and the summer sky above, and hands to strike a blow or pull a trigger. Raw Gold |Bertrand W. Sinclair 

"We'll be blamed lucky if we don't run into a prairie-fire before mornin'," Piegan grumbled. Raw Gold |Bertrand W. Sinclair 

One company also has irrigation works, and ready-made farms for settlers in the prairie provinces. Fifty Years of Railway Life in England, Scotland and Ireland |Joseph Tatlow 

The sergeant went out, and when the beat of hoofs sank into the silence of the prairie, Winston called Courthorne in. Winston of the Prairie |Harold Bindloss