Turkey has a relatively large defense budget of $15 billion but is mostly dependant on imports for its advanced platforms. Who Will Buy Israel's Military Hardware? |Yaakov Katz |June 18, 2013 |DAILY BEAST 

Health care costs are path dependant; you can't just hit the reset button and decide we'd rather do it West Germany's way. Do We Have a Spending Problem, or a Health Care Problem? |Megan McArdle |January 7, 2013 |DAILY BEAST 

The last tidings of him came from Ireland, where he was living as a dependant on some reduced family. Gerald Fitzgerald |Charles James Lever 

Isaacson had never before spoken so roughly, so almost ferociously to a dependant. Bella Donna |Robert Hichens 

On either side can be seen giant stalactites dependant from the roof, looking like mighty columns to support the dome. Miss Caprice |St. George Rathborne 

There was no pleading illness, for she was quite well; there must be no saying, "I will not go," for she was only a dependant. East Lynne |Mrs. Henry Wood 

I am here as a paid dependant and receiving really too high wages for any possible work I can give in return. Man and Maid |Elinor Glyn