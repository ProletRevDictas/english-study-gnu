We cannot allow the emblem of irresponsibility to attach to the conservative banner. Long before QAnon, Ronald Reagan and the GOP purged John Birch extremists from the party |Erick Trickey |January 15, 2021 |Washington Post 

Both counties have traditionally been rural emblems of the Old South, their picturesque central squares dominated by clock-tower-topped courthouses. In neighboring Georgia counties, election revealed a growing divide that mirrors the nation |Haisten Willis, Griff Witte |November 30, 2020 |Washington Post 

By using her spirit vision the Hunter to see gold traces of other emblems in the vicinity. ‘The Pathless’: Less than the sum of its parts |Christopher Byrd |November 23, 2020 |Washington Post 

Mustering the last reserves of her strength, the eagle gazes at a tower in the distance and summons from it a golden eagle emblem that she gives to the Hunter, which causes a gate to open in a nearby stone wall. ‘The Pathless’: Less than the sum of its parts |Christopher Byrd |November 23, 2020 |Washington Post 

Instead, the “RR” emblems remain perpetually upright and composed, as one would expect from a Rolls-Royce. The first Rolls-Royce SUV has tricks that might actually justify its price tag |Dan Carney |October 5, 2020 |Popular-Science 

In the latest, Frank is pushing 70 but he remains a fascinating emblem of his times. Richard Ford’s Artful Survivalist Guide: The Return of Frank Bascombe |Tom LeClair |November 4, 2014 |DAILY BEAST 

I personally prefer the Ukrainian official flag, and the emblem of Lviv—a kind looking lion—to a Swastika. Why Are Swastikas Hot In West Ukraine? |Anna Nemtsova |October 17, 2014 |DAILY BEAST 

Ehsan's fusion of artforms is a winning emblem of the happy marriage of fashion and art. She's Got the Look: How Pari Ehsan Marries Fashion and Art |Allison McNearney |August 26, 2014 |DAILY BEAST 

The Pahonia, emblem of the Belarusian nation drawn from the insignia of the Grand Duchy of Lithuania, is banned under Lukashenko. Forget Kim Jong Un—China’s New Favorite Dictator Is Belarus’s Aleksandr Lukashenko. |Kapil Komireddi |January 28, 2014 |DAILY BEAST 

This was the emblem of King Richard II, who was saved from killing a white deer, which in British legend is terribly bad luck. You Can Indeed Judge a Book By Its Cover |Brian Gresko |November 20, 2013 |DAILY BEAST 

Why did Ricetto, Bruno and Servetus in the hour of martyrdom turn with loathing from that sacred emblem, the crucifix? Gospel Philosophy |J. H. Ward 

Henry Clay Frick, in absolute control of the firm, incarnates the spirit of the furnace, is the living emblem of his trade. Prison Memoirs of an Anarchist |Alexander Berkman 

This is rather hard on the dog, who certainly cannot be considered the emblem of wickedness and hypocrisy. Friend Mac Donald |Max O'Rell 

Hence this Christian emblem became the object of scoffing and derision by the persecuting heathen. The Catacombs of Rome |William Henry Withrow 

His emblem is the human-headed winged lion seen at the entrance of royal palaces. Beacon Lights of History, Volume I |John Lord