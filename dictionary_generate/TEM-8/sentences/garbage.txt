Since the spring, the 54-year-old Montgomery County man has spent his daily walks into the District collecting garbage around the city, usually filling at least two trash bags with waste. The ‘garbage guy’ walks 12 miles a day around D.C. picking up trash: ‘I’ll pick up pretty much anything.’ |Sydney Page |February 11, 2021 |Washington Post 

There’s a certain sense of accomplishment that comes from dutifully sorting soda bottles, plastic bags and yogurt cups from the rest of the garbage. Chemists are reimagining recycling to keep plastics out of landfills |Maria Temming |January 27, 2021 |Science News 

Separating food waste from your garbage can mean more space at the landfill, so governments won’t have to spend money and land on a new one. Environment Report: Local Farms Want Your Table Scraps |MacKenzie Elmer |January 25, 2021 |Voice of San Diego 

So, if someone’s in a restaurant with me, and they eat a hamburger and they only eat half of it, I, in principle, will eat it, because it’s going to go in the garbage. The Downside of Disgust (Ep. 448) |Stephen J. Dubner |January 21, 2021 |Freakonomics 

Don’t attempt to burn or bury it, either, as food waste and garbage is more difficult to burn than you think, and fire pits are one of the first areas wildlife investigate. Once you know what happens to food you leave outdoors, you’ll stop doing it |Alisha McDarris |January 14, 2021 |Popular-Science 

“Angry Birds is a small fun game plus a lot of pointless garbage,” Smith tells me. Lost For Thousands of Strokes: 'Desert Golfing' Is 'Angry Birds' as Modern Art |Alec Kubas-Meyer |January 2, 2015 |DAILY BEAST 

“At worst, Eric picked up a garbage can, was told by police to put it down, and did,” his lawyer, Martin Stoler, insisted. Protesters Slimed This Good Samaritan Cop |Michael Daly |December 16, 2014 |DAILY BEAST 

Brabner talked about the “MTV people” coming to Cleveland to get pictures of Pekar emptying the garbage and going bowling. The Rustbelt Roars Back From the Dead |Joel Kotkin, Richey Piiparinen |December 7, 2014 |DAILY BEAST 

The kid moved again, slowly across the parking lot to the garbage bin. The Stacks: A Chicken Dinner That Mends Your Heart |Pete Dexter |December 7, 2014 |DAILY BEAST 

Then he took the chicken and walked over to his spot near the garbage and sat down to eat it. The Stacks: A Chicken Dinner That Mends Your Heart |Pete Dexter |December 7, 2014 |DAILY BEAST 

Above all, the doctor—the doctor and the purulent trash and garbage of his pharmacopoeia! The Pocket R.L.S. |Robert Louis Stevenson 

Foiled in this attempt they began to pelt them with garbage, so that soon their white robes were stained and filthy. Fair Margaret |H. Rider Haggard 

He would give me such words as "combustion," "garbage disposal," "bonded indebtedness" and so on. The Iron Puddler |James J. Davis 

The pigsties have no more smell than the stables, because the manure is removed, and no garbage is allowed to accumulate. The Hills and the Vale |Richard Jefferies 

He had a couple of dingy wash-boilers which he had picked up from the big garbage-dump near the race-track. Tramping on Life |Harry Kemp