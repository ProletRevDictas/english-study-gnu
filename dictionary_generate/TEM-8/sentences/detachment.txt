There’s little research on the best ways to use digital media during a pandemic and unprecedented levels of social detachment, but in my experience with coaching individuals virtually, the more present you can be for the conversation, the better. 7 Wellness Strategies to Build Resilience |Brad Stulberg |January 22, 2021 |Outside Online 

There is a peace about Aaron Rodgers now, a feeling different from his normal vibe of cool detachment. Aaron Rodgers, entering a showdown with Tom Brady, has gone from chippy to chill |Jerry Brewer |January 22, 2021 |Washington Post 

You need psychological detachment from the source of the stress. Stressed out? Video games can help—if you follow these tips. |Stan Horazek |January 20, 2021 |Popular-Science 

My complete acknowledgment of the loss almost 20 years later helps me understand my detachment from society and people at times. How To Find Freedom From Grief |Jasmine Grant |January 7, 2021 |Essence.com 

At first, Glenda found Varahn to be reserved, but she soon realized that what she had mistaken for detachment was simply Varahn’s way of taking in her new surroundings. “We Don’t Even Know Who Is Dead or Alive”: Trapped Inside an Assisted Living Facility During the Pandemic |by Ava Kofman |November 30, 2020 |ProPublica 

The pontiff blasts the selfishness, arrogance and detachment of the cardinals in Rome. Pope Francis Denounces the Vatican Elite’s 'Spiritual Alzheimer’s' |Barbie Latza Nadeau |December 23, 2014 |DAILY BEAST 

A detachment of six volunteers, led by Lt. Alexandre Rosenberg, planned to stop the train at Aulnay, in the suburbs of Paris. My Grandfather's War: Recovering the Art the Nazis Stole |Anne Sinclair |October 5, 2014 |DAILY BEAST 

No because I want to preserve as much as possible my detachment and impartiality. U.N. Gaza Investigator: 'Anti-Israel' Label Is a 'Slur' |Gideon Resnick |August 12, 2014 |DAILY BEAST 

But this detachment gives the biography a dutiful, going-through-the-motions tone. Clare Boothe Luce's Vapid Second Act |Wendy Smith |July 5, 2014 |DAILY BEAST 

That sense of detachment from the caprices of Mother Nature is pretty unique in human history. The Nile: Where Ancient and Modern Meet |William O’Connor |June 21, 2014 |DAILY BEAST 

Bidding a young bank manager take charge of the detachment, Frank led the newcomer rapidly to headquarters. The Red Year |Louis Tracy 

In each case the tiny detachment discovered blackened walls and unburied corpses. The Red Year |Louis Tracy 

A double detachment of soldiers was already there, with orders to support him in case of resistance. The Pastor's Fire-side Vol. 3 of 4 |Jane Porter 

Presently tea was brought, but even then she preserved, smiling, her soft but complete detachment. Bella Donna |Robert Hichens 

Some armed vessels were, however, speedily despatched from Rio, and a detachment of militia from St. Paul's. Journal of a Voyage to Brazil |Maria Graham