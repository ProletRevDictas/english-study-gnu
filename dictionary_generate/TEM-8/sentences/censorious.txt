He has also signaled opposition to the censorious “woke” culture that has come to dominate liberal discourse. What Elon Musk Really Believes |Molly Ball |April 26, 2022 |Time 

By 2001, the International Center for Human Rights and Democratic Development estimated, China spent $20 billion on censorious telecom equipment every year. Russia inches closer to its splinternet dream |WIRED |April 3, 2022 |Ars Technica 

Up until the mid-’80s, it looked like these censorious villains would triumph—and also, like today, most of the adult world hid in their homes in Sherman Oaks instead of fighting back. Why the OnlyFans Porn Mess Is a Wake-Up Call for Sex Workers |Cherie DeVille |August 26, 2021 |The Daily Beast 

Additionally, the Court handed a victory to public school student-athletes — and to students generally — who find themselves on the wrong end of overly censorious school administrators. 3 winners and 3 losers from the just-completed Supreme Court term |Ian Millhiser |July 2, 2021 |Vox 

Yet jollity and gloom are still at war in our censorious age. A History of American Fun |Stefan Beck |February 9, 2014 |DAILY BEAST 

But amid all the censorious protests against “self-censorship” an important legal principle has been ignored. Three Cheers for Censorship! |Lee Siegel |August 30, 2009 |DAILY BEAST 

After all, here was a babe equipped to face the exigencies of a censorious world; in looks and apparel a credit to any father. The Joyous Adventures of Aristide Pujol |William J. Locke 

They judged him by a censorious standard which took no account of genius. American Sketches |Charles Whibley 

Yes, and she is a curious being to pretend to be censorious—an awkward thing, without any one good point under the sun. The Ontario Readers: The High School Reader, 1886 |Ministry of Education 

He forgot the delicate and uncertain state of his marital affairs, forgot the censorious world, his ennui and doubt and regret. The Beauty |Mrs. Wilson Woodrow 

The Bishop is especially incensed at the censer; and waxes censorious about the wax lights. Punchinello, Vol. 1, No. 7, May 14, 1870 |Various