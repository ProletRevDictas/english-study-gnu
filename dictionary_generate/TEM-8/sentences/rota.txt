One in four desks is currently being used in the newsroom and follow a team rota system. ‘Safe-space’ signs and which direction to face: At 30% capacity, how News UK is returning to the office |Lucinda Southern |August 13, 2020 |Digiday 

In an email interview with The Daily Beast, the former Rota chief stressed that he believes in a free press. In Brazil, An Outspoken Colonel, a Journalist in Hiding and Mysterious Threats of Death |Dom Phillips |October 20, 2012 |DAILY BEAST 

Nearby, in Rota, there is also a major U.S. base, another attractive target. Unraveling Al Qaeda’s Plot Against Spain |Bruce Riedel |August 7, 2012 |DAILY BEAST 

The gorgeous rota looks like an alien lighthouse, with silvery lights spinning out of its core. Carsten Nicolai’s Skydiving Dance |Jimmy So |September 24, 2011 |DAILY BEAST 

As a foundation of their scheme, the cabal have established a sort of rota in the court. The Works of the Right Honourable Edmund Burke, Vol. I. (of 12) |Edmund Burke 

Rotatoria (rta, a wheel) or Rotifera (rota and fero, to bear). An Elementary Text-book of the Microscope |John William Griffith 

That an English sovereign should plead before the Rota at Rome was, of course, preposterous. The Divorce of Catherine of Aragon |J.A. Froude 

If the cause was tried at Rome, was it to be tried before the Cardinals in consistory or before the court of the Rota? The Divorce of Catherine of Aragon |J.A. Froude 

By this order the burden fell upon certain portions of the inhabitants exclusively and not pro rota upon the whole. Sages and Heroes of the American Revolution |L. Carroll Judson