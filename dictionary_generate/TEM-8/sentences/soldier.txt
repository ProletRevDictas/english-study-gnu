The shading of a cell indicates the number of soldiers placed. Can You Reach The Summit First? |Zach Wissner-Gross |September 11, 2020 |FiveThirtyEight 

That year, the Union Army furnished some 2,020 artificial legs and 1,441 artificial arms to its soldiers. Times of strife can lead to medical innovation—when governments are willing |By Jeffrey Clemens/The Conversation |September 9, 2020 |Popular-Science 

Take, for instance, the financial hit that the United States carries each time one of its former soldiers in Iraq or Afghanistan commits violent crimes back on the home front. The Hidden Costs of War: Vet Crimes |Nick Fouriezos |August 19, 2020 |Ozy 

Getting a haircut is supposed to be a risky act tantamount to being a soldier at war. Online Learning Is Here to Stay |Sara-Ellen Amster |August 7, 2020 |Voice of San Diego 

A soldier who loses a leg and a soldier who returns home safe to a new baby will generally, a year or two later, be roughly as happy as they were before those events. The scariest thing about global warming (and Covid-19) |David Roberts |July 7, 2020 |Vox 

A soldier in the service of ideals and aspirations that formed his core. Mario Cuomo, Always Moving Us Toward the Light |Mike Barnicle |January 4, 2015 |DAILY BEAST 

The story of the soldier returning home to a country he no longer recognizes is a very old one. A Veteran’s View: NYC Cold War Between Cops and City Hall |Matt Gallagher |December 29, 2014 |DAILY BEAST 

Sabrine reports that the latest demands by ISIS militants are three prisoners for every captive soldier. A Sunni-Shia Love Story Imperiled by al Qaeda |Ruth Michaelson |December 26, 2014 |DAILY BEAST 

In their midst stands a soldier with the Lebanese armed forces in a red beret, sporting an assault rifle and an unblinking stare. A Sunni-Shia Love Story Imperiled by al Qaeda |Ruth Michaelson |December 26, 2014 |DAILY BEAST 

The families had gathered that Sunday to remember Ali Bazzal, a soldier whom the Nusra Front declared they had executed on Dec. 6. A Sunni-Shia Love Story Imperiled by al Qaeda |Ruth Michaelson |December 26, 2014 |DAILY BEAST 

After relievedly giving the pistol to the nearest soldier, he stumbled quickly over to Brion and took his hand. Sense of Obligation |Henry Maxwell Dempsey (AKA Harry Harrison) 

Captain Duffield wrote two messages, giving one to Harry, and the other to the soldier who was to accompany him. The Courier of the Ozarks |Byron A. Dunn 

To Harry's surprise, the soldier detailed to go with him proved to be a boy, not much older than himself. The Courier of the Ozarks |Byron A. Dunn 

The other was the spirited portrait of Baron von Friedericks, a happy combination of cavalier and soldier in its manly strength. Women in the fine arts, from the Seventh Century B.C. to the Twentieth Century A.D. |Clara Erskine Clement 

Heavy firing continued all that afternoon, inflicting great loss on the rebels, whilst the Spaniards lost one soldier. The Philippine Islands |John Foreman