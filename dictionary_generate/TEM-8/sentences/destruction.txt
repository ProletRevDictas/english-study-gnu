This is gossipmongering and, as Republicans like to say, part of the politics of personal destruction. Outing a Celeb Sex Pest and Mourning Larry Flynt … Sorta |Eugene Robinson |February 11, 2021 |Ozy 

We should NOT be flying a flag of a group who wants to erase our history and bring mass destruction to our country through Communism. Marjorie Taylor Greene backs legislation to ban Pride flags at U.S. embassies |Chris Johnson |February 1, 2021 |Washington Blade 

Climate-change impacts are already a common thread of destruction and violence from the Sahel and South Sudan to Central America. The TIME 2030 Committee Offers 8 Solutions for a More Equitable and Sustainable Future |TIME Staff |January 22, 2021 |Time 

“When the books are written about our careers, one of the main things we’ll be judged on is whether we did enough to stop the destruction of life and property due to climate change,” he said. Buttigieg cites ‘generational opportunity’ on infrastructure at confirmation hearing |Ian Duncan, Michael Laris |January 21, 2021 |Washington Post 

Habitat destruction, loss of food plants, invasive species, climate change, and predation are primarily human-driven problems. We Crush, Poison, and Destroy Insects at Our Own Peril - Issue 95: Escape |John Hainze |January 20, 2021 |Nautilus 

“The United States had gone to war declaring it must destroy an active weapons of mass destruction program,” the Times reported. Political Memes That Absolutely Must Die in 2015 |Asawin Suebsaeng |January 1, 2015 |DAILY BEAST 

Some of the streets in this part of the town have seen large scale-destruction. The Brothers Who Ambushed ISIS |Mohammed A. Salih |December 27, 2014 |DAILY BEAST 

And the Gävle Goat, apparently a sensitive creature, took the destruction hard. Sweden’s Burning Christmas Goat |Nina Strochlic |December 25, 2014 |DAILY BEAST 

Gävle Goat must be dreading the imminent holiday and his fifty-fifty chance of destruction. Sweden’s Burning Christmas Goat |Nina Strochlic |December 25, 2014 |DAILY BEAST 

And suddenly you were in the throes of both creation and destruction. Is Bigger Better for St. Vincent? |David Yaffe |December 4, 2014 |DAILY BEAST 

The blind Samson of labor will seize upon the pillars of society and bring them down in a common destruction. The Unsolved Riddle of Social Justice |Stephen Leacock 

All things that are of the earth, shall return into the earth: so the ungodly shall from malediction to destruction. The Bible, Douay-Rheims Version |Various 

In the time of destruction they shall pour out their force: and they shall appease the wrath of him that made them. The Bible, Douay-Rheims Version |Various 

To fix on any one stage in such an evolution, detach it, affirm it, is to wrest a true scripture to its destruction. Solomon and Solomonic Literature |Moncure Daniel Conway 

It also occurs in diseases with extensive and rapid destruction of red blood-corpuscles. A Manual of Clinical Diagnosis |James Campbell Todd