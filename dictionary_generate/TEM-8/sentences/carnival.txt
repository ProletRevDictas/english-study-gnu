On the HBO show Euphoria, Alexa Demie’s character, Maddy Perez, wore a pair of bright purple pants with slits cut on the side to a carnival. Pants are fun now |Melinda Fakuade |March 5, 2021 |Vox 

“So far, Liberty has not agreed to any particular plan or contract,” the statement said, going on to add that the school would rent its parking areas to any political party if asked, as it has done in the past for carnivals, circuses and car shows. Liberty University surprised by Virginia GOP plans for drive-in convention on campus |Laura Vozzella |February 24, 2021 |Washington Post 

They likened the barriers to hockey penalty boxes or a carnival funhouse. Maryland lawmakers return to Annapolis for start of unusual 90-day session |Ovetta Wiggins, Erin Cox |January 13, 2021 |Washington Post 

A simple but effective analogy is to imagine the carnival game where balls are dropped onto a vertical board covered in wooden pegs. New Quantum Computer in China Claims Quantum Advantage With Light |Jason Dorrier |December 6, 2020 |Singularity Hub 

There are fireworks, barbecues, carnivals, parades, and a whole slew of activities. Ecommerce marketing this Independence Day will be tricky: Four must dos |Evelyn Johnson |June 23, 2020 |Search Engine Watch 

Lupher says the Carnival Magic tried to land in Cozumel, but that the Mexican authorities blocked them from the dock. Inside the Cruise Ship Quarantined Over Ebola Fear |Carol McKinley |October 17, 2014 |DAILY BEAST 

People aboard the Carnival Magic have another day and a half at sea before they reach Galveston, Texas. Inside the Cruise Ship Quarantined Over Ebola Fear |Carol McKinley |October 17, 2014 |DAILY BEAST 

Dispensable human warmth is enough of a sell, even without the dolls and magical pictures on sale at her traveling carnival. Patted Down by India’s Hugging Saint |Daniel Genis |July 20, 2014 |DAILY BEAST 

For those who did show up, the event resembled a political carnival. The Bizarro World Of Iowa’s GOP Convention |Ben Jacobs |June 23, 2014 |DAILY BEAST 

Was the hammer scene, for you, like that strength game at the carnival where you hit the block with the hammer? Game of Thrones’ Kit Harington on Jon Snow’s Heroism and Loss in the Battle of Castle Black |Marlow Stern |June 9, 2014 |DAILY BEAST 

At two o'clock, the general of the Carnival opens the public ball with the Mugnaia. The Pit Town Coronet, Volume I (of 3) |Charles James Wills 

If you wanted to let her go you did so; if not, you talked in the squeaky voice that is the recognized etiquette of the carnival. The Joyous Adventures of Aristide Pujol |William J. Locke 

If the work goes well I shall try to arrange for you both to come here in the Carnival Week, so that you may hear it. The Life &amp; Letters of Peter Ilich Tchaikovsky |Modeste Tchaikovsky 

The Turkish lines stretched away to his left; he had cleared their flank, and the battle raged in its mad carnival behind him. God Wills It! |William Stearns Davis 

The story goes that one night during the carnival he was wounded by some masqueraders, who mistook him for another person. Belgium |George W. T. (George William Thomson) Omond