I also felt a pang, watching Otto pull us out of our fretful dialogue. A Tool for Staying Grounded in This Era of Constant Uncertainty |Rebekah Taussig |February 14, 2022 |Time 

Fried potatoes are one of mankind’s greatest inventions, but deep-frying at home can be as fussy and fretful as clipping a frantic cat’s claws. How to make crispy air-fryer fries with no fuss and very little muss |Daniela Galarza |June 17, 2021 |Washington Post 

Selznick was appropriately fretful from the start about length. How 'Gone With the Wind' Got Made |Helen Anders |September 10, 2014 |DAILY BEAST 

Calls made by Ishihara for a “new military” are alarming to many Japanese, and the Chinese blogosphere has been fretful about it. Why Japan’s Right Turn Could Be Trouble for the U.S. |Kathryn Ibata-Arens |December 16, 2012 |DAILY BEAST 

Like George Bailey in It's a Wonderful Life, the fretful marionettes pondered what life would be like without Sarkozy. France’s Petty Politics Brings Christmas Early to Scandal Lovers |Tracy McNicoll |December 15, 2012 |DAILY BEAST 

This strong and capable woman is fretful about what to wear for a dinner freighted with emotion. Robin Givhan: Dressing for Power on USA’s ‘Political Animals' |Robin Givhan |August 8, 2012 |DAILY BEAST 

As for the rest of the party, many are growing increasingly fretful over the violence being done to the GOP brand. Gingrich Might Withdraw From Presidential Race for Right Appointment |Michelle Cottle |March 21, 2012 |DAILY BEAST 

Occasionally he turned his head to watch with keen eyes the fretful movements of a fly hovering above the water. Uncanny Tales |Various 

Beardsley was watching Arnold's fingers; there was something aimless and fretful as they pushed among the code-sealed tapes. We're Friends, Now |Henry Hasse 

“I fear there is an element of the morbid, in all this fretful revolt against the old-established destiny of our sex,” she said. The Daughters of Danaus |Mona Caird 

Mrs. Tynan's face flushed with sudden irritation and that fretful look came to her eyes which accompanies a lack of comprehension. You Never Know Your Luck, Complete |Gilbert Parker 

So that night his head was hot, and he was fretful; and in the morning he would not eat, and apparently had a fever. Love's Pilgrimage |Upton Sinclair