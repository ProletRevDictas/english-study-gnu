I found the hypnotism section particularly fascinating, not because it provided all the answers about this phenomenon but because there’s clearly so much that remains unknown. The 2021 Sweat Science Holiday Book List |mmirhashem |December 4, 2021 |Outside Online 

(Among more secular audiences, Noebel may be best known for his 1965 work, Communism, Hypnotism and The Beatles). Ted Cruz and Donald Trump a Sideshow at Fire and Brimstone Rally |Ben Jacobs |August 12, 2013 |DAILY BEAST 

They are in a total state of hypnotism by sports, by Hollywood. Anti-Islam Minister Terry Jones Says He Feels No Responsibility for U.S. Ambassador’s Death |Lynn Waddell |September 13, 2012 |DAILY BEAST 

Such men are so set in their habits, nothing but a miracle or hypnotism can save them. Wayside Courtships |Hamlin Garland 

Hypnotism has been employed against all kinds of sexual processes, both in adults and in children. The Sexual Life of the Child |Albert Moll 

To pass the weary time Jones and Hill dabbled in and experimented with hypnotism and telepathy. Eastern Nights - and Flights |Alan Bott 

They rejected hypnotism as in most cases too dangerous, but used a milder form which is known as "hypnoidization." The Book of Life: Vol. I Mind and Body; Vol. II Love and Society |Upton Sinclair 

Under hypnotism it was discovered to be a case of multiple personality. The Book of Life: Vol. I Mind and Body; Vol. II Love and Society |Upton Sinclair