I don’t have advice to cancel the event on the basis that it’s unsafe. Novak Djokovic’s five-set battle at Australian Open started with fans and ended without them |Matt Bonesteel |February 12, 2021 |Washington Post 

Michael breaks down music into 10 elements, which serve as the basis for each chapter. The tale of a bass player, sonic epiphanies and a quest to save ‘real music’ |Ben Ratliff |February 12, 2021 |Washington Post 

The neglect of students on the basis of race and income is long-standing and only addressing it will maximize latent talent, thereby benefiting all Americans. My great-grandmother Ida B. Wells left a legacy of activism in education. We need that now. |Michelle Duster |February 11, 2021 |Washington Post 

Instead, it aimed to determine whether a particular hiring tool grossly discriminates against candidates on the basis of race or gender. Auditors are testing hiring algorithms for bias, but there’s no easy fix |Amy Nordrum |February 11, 2021 |MIT Technology Review 

Because of the pandemic we started to FaceTime each other on a regular basis. Love at six feet |Julia Rothman, Shaina Feinberg |February 9, 2021 |Washington Post 

If Congress accurately reflected our nation on the basis of race, about 63 percent would be white, not 80 percent. The Unbearable Whiteness of Congress |Dean Obeidallah |January 8, 2015 |DAILY BEAST 

We see the Southern segregationists who threatened his life and that of his family on an almost daily basis. Dr. King Goes to Hollywood: The Flawed History of ‘Selma’ |Gary May |January 2, 2015 |DAILY BEAST 

Yet the email references the 1970s, “when police officers were ambushed and executed on a regular basis.” We Need Our Police to Be Better Than This |Nick Gillespie |December 31, 2014 |DAILY BEAST 

Removing choice is bullying and seems a horrid basis on which to anchor your relationship. Public Marriage Proposals Must Die |Tauriq Moosa |December 28, 2014 |DAILY BEAST 

So, I can deal with them on a daily basis, I know how it affects my body. Tim Howard’s Wall of Intensity |William O’Connor |December 22, 2014 |DAILY BEAST 

The m relates it to the nares or humming tone (which is the basis of all resonance in the voice). Expressive Voice Culture |Jessie Eldridge Southwick 

A resolute push for quite a short period now might reconstruct the entire basis of our collective human life. The Salvaging Of Civilisation |H. G. (Herbert George) Wells 

At the present time, certainly, no thought has ever occurred to Germans that they would not go back to a gold basis. Readings in Money and Banking |Chester Arthur Phillips 

But here in this little valley of the Kaw, he was cheered to see his race on a practical and sensible basis. The Homesteader |Oscar Micheaux 

The relative quantity of labor embodied in each object is the basis of its value. The Unsolved Riddle of Social Justice |Stephen Leacock