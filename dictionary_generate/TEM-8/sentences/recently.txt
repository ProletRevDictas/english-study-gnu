Houston Rockets guard James Harden and Russell Westbrook have both been floated in recent rumors, and the Oklahoma City Thunder traded Chris Paul to the Phoenix Suns earlier this week. NBA free agency live updates: The latest moves and news from the offseason |Ben Golliver |November 20, 2020 |Washington Post 

It was just 20 months ago that a16z closed its most recent pair of funds — a $2 billion late-stage fund, and a $740 million flagship early-stage fund. A16z is now managing $16.5 billion, after announcing two new funds |Natasha Mascarenhas |November 20, 2020 |TechCrunch 

The castle walls of Boletaria look more like a place of recent unrest and chaos, while the Tower of Latria’s upper and lower levels now make geographical sense. ‘Demon’s Souls’ review: The ideal PlayStation 5 launch game |Gene Park |November 20, 2020 |Washington Post 

General Motors, in its most recent sustainability report, said it investigated the allegations and ended its relationship with the supplier. Apple is lobbying against a bill aimed at stopping forced labor in China |Reed Albergotti |November 20, 2020 |Washington Post 

In response to a recent rise in positive tests among players and league personnel, as well as to a sharp increase across the nation, the NFL directed all its teams to move this week to a heightened level of pandemic-related protocols. Cowboys QB Andy Dalton says covid-19 ‘hit me hard’ after concussion |Des Bieler |November 20, 2020 |Washington Post 

But he told me recently that Malia would do my impression of him to him. Coffee Talk with Fred Armisen: On ‘Portlandia,’ Meeting Obama, and Taylor Swift’s Greatness |Marlow Stern |January 7, 2015 |DAILY BEAST 

I just recently rewatched all six Star Wars movies the other day… Oh wow, from the beginning? Patton Oswalt on Fighting Conservatives With Satire |William O’Connor |January 6, 2015 |DAILY BEAST 

This is the Mexico that has been called a “failed state” — most recently by Jose “Pepe” Mujica, the president of Uruguay. Why Mexicans Are Enraged by Obama’s Big Tuesday Meeting |Ruben Navarrette Jr. |January 6, 2015 |DAILY BEAST 

Recently, historians have been working hard to write books that are more accessible. Thank Congress, Not LBJ for Great Society |Julian Zelizer, Scott Porch |January 4, 2015 |DAILY BEAST 

Antoine himself had recently been arrested on a six-year-old warrant for a dime bag of weed. Ground Zero of the NYPD Slowdown |Batya Ungar-Sargon |January 1, 2015 |DAILY BEAST 

Genoa has but recently and partially felt the new impulse, yet even here the march of improvement is visible. Glances at Europe |Horace Greeley 

Even New Zealand Brigade which has been only recently engaged lost heavily and is to some extent demoralised. Gallipoli Diary, Volume I |Ian Hamilton 

Until very recently little has been known of the strange land in which the subject of this tale lives. Our Little Korean Cousin |H. Lee M. Pike 

This man had often escaped drowning, and only recently upon the blessed day of last Pentecost. The Jesuit Relations and Allied Documents, Vol. II: Acadia, 1612-1614 |Various 

An accident occurred on the Panama rail road, recently put in operation, by which 43 persons were killed, and 60 wounded. The Every Day Book of History and Chronology |Joel Munsell