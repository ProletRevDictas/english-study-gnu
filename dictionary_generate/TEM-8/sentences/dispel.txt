What drew me to them was that similar feeling around a focus on client value — and on dispelling all the smoke and mirrors in the industry. Media Buying Briefing: ‘We’re engineering around the value proposition’: Kepler Group’s global reach expands as it absorbs U.K.’s Infectious Media |Michael Bürgi |August 23, 2021 |Digiday 

My thinking was that if I had a YC stamp of approval, that could dispel some of the skepticism around my product. Why it’s so hard to make tech more diverse |Wudan Yan |June 30, 2021 |MIT Technology Review 

Braband said he thinks the bump will help dispel myths about women not being interested in math and science. After admissions changes, Thomas Jefferson High will welcome most diverse class in recent history, officials say |Hannah Natanson |June 24, 2021 |Washington Post 

It’s time to dispel the belief that the end of cookies means the end of personalized recommendations. Third-party cookie deprecation presents a new opportunity for marketers |Outbrain |June 7, 2021 |Digiday 

Societally, it’s been hard to dispel the idea that women are just “naturally better” than men at raising children and managing households. On this Mother’s Day, the crisis for working moms is hard to miss |Karla Miller |May 6, 2021 |Washington Post 

The police themselves do little to dispel or discourage this lionized portrayal. Prosecutor Used Grand Jury to Let Darren Wilson Walk |Tom Nolan |November 28, 2014 |DAILY BEAST 

He does, however, attempt to dispel some of the myths that have emerged from hearsay and rumor over the last century. How WWI Produced the Holocaust |J.P. O’Malley |November 21, 2014 |DAILY BEAST 

Swiss leaders also dispel the “slippery slope” idea by repeatedly rejecting substantial minimum wage increases. To Make Their Victory Durable, the GOP Must Fix the Minimum Wage |Dmitri Mehlhorn |November 6, 2014 |DAILY BEAST 

The beautiful pictures may partially dispel what an awful man Rembrandt seemed to be. Relishing Rembrandt’s Blockbuster London Show |Nancy Durrant |October 16, 2014 |DAILY BEAST 

But one look at the film is enough to dispel all notions of Svengali. The Legend With The Look: Remembering Lauren Bacall |Teo Bugbee |August 13, 2014 |DAILY BEAST 

"There's nothing like prompt action to dispel alarm," he whispered in my ear; and then turned to the rest of us. Three More John Silence Stories |Algernon Blackwood 

However, madame, I count upon it that you will be able to dispel such doubts as I am fostering. St. Martin's Summer |Rafael Sabatini 

But scarce had they arrived at it when Monsieur de Garnache's crisp voice came swiftly to dispel it. St. Martin's Summer |Rafael Sabatini 

I hope this verification will dispel your royal highness' unjust suspicions against me. Balsamo, The Magician |Alexander Dumas 

Human extravagances soon dispel, in the eyes of reason, the superiority which man arrogantly claims over other animals. Superstition In All Ages (1732) |Jean Meslier