For select residents of certain tribes and for specific tribal governments, reservation casinos provide perhaps the only example of gambling revenue actually meeting expectations as a tool of economic development. Sports gambling could be the pandemic’s biggest winner |Jonathan D. Cohen |February 5, 2021 |Washington Post 

Although it’s not clear whether the president-elect would save Oak Flat if the process were delayed until he takes office, he has promised to work more closely with tribal leaders. 'Oak Flat' Chronicles a Battle to Save Sacred Land |Erin Berger |January 14, 2021 |Outside Online 

The case, one of the largest battles between tribal governments and the United States in decades, will be reviewed by the Supreme Court this year. Supreme Court to review whether or not Mnuchin failed to distribute COVID relief to Native Americans swiftly enough |Nicole Goodkind |January 12, 2021 |Fortune 

For those 150,000 locations, Frontier will have to consult with the CWA, TURN, Cal Advocates, and tribal government leaders "to discuss the potential areas for deployment, including tribal lands and tribal communities," the settlement said. Frontier agrees to fiber-network expansion in plan to exit bankruptcy |Jon Brodkin |January 6, 2021 |Ars Technica 

“It could be somewhat random, but looking at those particular states, it could also reflect attention paid to tribal populations,” said Kate Miller, a senior scientist at Ariadne Labs. Covid-19 is devastating communities of color. Can vaccines counter racial inequity? |Isaac Stanley-Becker, Lena H. Sun |December 18, 2020 |Washington Post 

“He was a brave field commander and an expert in intelligence, and in organizing popular and tribal forces,” said the eulogist. What an Iranian Funeral Tells Us About the Wars in Iraq |IranWire |January 6, 2015 |DAILY BEAST 

In tribal areas, such legitimacy cannot be gained while Assad in power. Where Does Obama Stand on Assad? |Hassan Hassan |November 1, 2014 |DAILY BEAST 

Killer also noted that there are some tribal elections on the same day. South Dakota's Bizarre Four-Way (Senate Election, That Is) |Ben Jacobs |October 15, 2014 |DAILY BEAST 

The drones bombing the tribal area, angering many, are run by the Company. Why So Many Pakistanis Hate Their Nobel Peace Prize Winner |Chris Allbritton |October 10, 2014 |DAILY BEAST 

Allen worked closely with those tribal leaders during the counterinsurgency in Iraq in 2007 and 2008. Can Obama Keep His Generals in Check in the War Against ISIS? |Eli Lake, Josh Rogin |September 17, 2014 |DAILY BEAST 

The tribal ward headmanʼs district deputies together constitute the police force of the whole ward. The Philippine Islands |John Foreman 

Tribal ward headmen and their district deputies are not required to give bond. The Philippine Islands |John Foreman 

The district headman is the deputy of the tribal ward headman to whom he is immediately responsible. The Philippine Islands |John Foreman 

At any time, on certain conditions, a member of a tribal ward can apply for full citizenship in a municipality. The Philippine Islands |John Foreman 

There is to be no sovereign power, great or small, other than American, and tribal wards are to supersede dattoships. The Philippine Islands |John Foreman