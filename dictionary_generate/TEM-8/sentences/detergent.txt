If there are tough stains, you can use some mild detergent and presoak the blanket for 10 to 15 minutes in cool water. Best heated throw blanket: Bundle up with these electric blankets |PopSci Commerce Team |February 11, 2021 |Popular-Science 

However, if you wash your garment with detergent, then you will want to immediately reapply DWR, he advises. How to Wash Your Ski Kit |Joe Jackson |January 30, 2021 |Outside Online 

Bottles of laundry detergent—and beer—have subbed in as weights. How Olympic Athletes Are Coping with the Wait for the Postponed 2021 Games |Sean Gregory |January 12, 2021 |Time 

Finding a stain-removing companion for your typical cleaning products and detergents can often be a trial-and-error process that leaves your home interior or wardrobe at risk. Powerful stain removers to fight tough blemishes |PopSci Commerce Team |January 6, 2021 |Popular-Science 

I use ½ cup vinegar instead of detergent, and I add ½ cup baking soda to the load. Hints From Heloise: Hearing aids and masks |Heloise Heloise |January 1, 2021 |Washington Post 

A new report says laundry detergent pods are sending 20,000 kids to the hospital each year. Kids Eat the Darndest Things: Laundry Pods, Teething Necklaces, and More Of The Weirdest Stuff Sending Kids to the E.R. |Russell Saunders |November 14, 2014 |DAILY BEAST 

I now see that by the standards of modern American society, I do something comparable to concocting my own laundry detergent. The Art of Making Puff Pastry |Kate Weiner |February 23, 2011 |DAILY BEAST 

He swore under his breath and twisted the valve that was supposed to dispense detergent. The Bramble Bush |Gordon Randall Garrett 

He was thoroughly wet with the stuff and no amount of water and detergent would take it off. The Bramble Bush |Gordon Randall Garrett 

Hot water is itself a detergent; that is, it has the power of dissolving dirt. Household Administration |Various 

Such soap possesses very powerful detergent qualities, but it is apt to feel hard and be somewhat gritty in use. A Dictionary of Arts, Manufactures and Mines |Andrew Ure 

Its detergent action is sometimes supposed to be due to the free alkali, whereas a well-made soap is practically neutral. Scientific American Supplement, No. 561, October 2, 1886 |Various