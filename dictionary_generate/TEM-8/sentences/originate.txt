If we do one day discover life on Mars, it may be difficult to tell whether it originated there or drifted over from another planetary body. Clumps of bacteria could spread life between planets |Paola Rosa-Aquino |August 27, 2020 |Popular-Science 

Scientists still debate whether the SARS-CoV-2 virus originated in a bat or a pangolin. Can Vaccines for Wildlife Prevent Human Pandemics? |Rodrigo Pérez Ortega |August 24, 2020 |Quanta Magazine 

A conspiracy theory website, Gateway Pundit, spread the allegations, which originated with a group called My Party Was Changed Oregon. For Election Administrators, Death Threats Have Become Part of the Job |by Jessica Huseman |August 21, 2020 |ProPublica 

Perhaps the most exciting possibility is that this FRB and others may originate from magnetars—a still-mysterious type of neutron star thought to have an extremely powerful magnetic field. A strange rhythm in space |Katie McLean |August 19, 2020 |MIT Technology Review 

Many viruses, of course, originate in populations of wild animals that evolved to withstand their negative impacts. 3-D Printed Statues in Central Park Shine a Light on Women Scientists - Facts So Romantic |Mary Ellen Hannibal |August 18, 2020 |Nautilus 

In wearing it, Hurley helped originate a wider revolution, too. Happy 20th Birthday, Liz Hurley’s Safety-Pin Dress |Tim Teeman |December 12, 2014 |DAILY BEAST 

Where did this climate of fear originate within the Murdoch organization? Murdoch on the Rocks: How a Lone Reporter Revealed the Mogul's Tabloid Terror Machine |Clive Irving |August 25, 2014 |DAILY BEAST 

But where do air pollutants in northern China, in particular Beijing, originate? The Chinese Can’t Catch Their Breath |Brendon Hong |May 5, 2014 |DAILY BEAST 

The title will apparently remain “Late Show” and the program will still originate from The Ed Sullivan Theater. Stephen Colbert’s Groveling ‘Late Show’ Debut |Tom Shales |April 23, 2014 |DAILY BEAST 

And aside from that, it was likely unconstitutional because bills that raise revenue must originate in the House. Republicans' Cynical Oil Payoff |Michael Tomasky |May 17, 2011 |DAILY BEAST 

Large mononuclear leukocytes probably originate in the bone-marrow or spleen. A Manual of Clinical Diagnosis |James Campbell Todd 

A rumbling sound that did not originate in the thunder caps above jerked Black Hood's attention from the drain. Hooded Detective, Volume III No. 2, January, 1942 |Various 

It is difficult to believe that they originate at less than a hundred miles below the earth's surface. Outlines of the Earth's History |Nathaniel Southgate Shaler 

Not less than the power to originate the great movement that has taken place, is requisite ability to direct it aright. The Ordinance of Covenanting |John Cunningham 

Our thoughts should originate from sound sense and reasoning, and always be the result of our judgment. The 'Characters' of Jean de La Bruyre |Jean de La Bruyre