I learned to stuff down my emotions with handful after handful of cereal or clandestine candy bars, wrappers crumpled in my purse. How a Plus-Size Hiker Found Her Footing on the Trail |syadron |August 22, 2021 |Outside Online 

HighSpeedDaddyIt can be hard to find a lunch box that doesn’t look like a purse or a toy. Is it noon yet? The best lunch box, bento boxes, lunch bags for adults |Florie Korani |August 12, 2021 |Popular-Science 

I have a stick that stays on my vanity at home, and since it’s small enough to throw in a purse, I keep one in my pool bag too. This Anti-Chafe Stick Is the Product I Didn’t Know I Needed |Kristen Garaffo |July 30, 2021 |The Daily Beast 

This pick comes with scientific guidance on feeding and can fit in your purse. The best breast pump for comfortable, effective pumping wherever you are |Billy Cadden |July 26, 2021 |Popular-Science 

This item fits neatly in your bag or purse and provides the occasional dew when you need it the most. Best portable fans: Stay chill on a hot day with a cooling personal fan |Irena Collaku |July 21, 2021 |Popular-Science 

On one summer lunch hour, Donna Ann Levonuk, 50, lifted a tub of diaper cream priced at $43.98—and then stashed it in her purse. The Insane $11 Billion Scam at Retailers’ Return Desks |M.L. Nestel |December 19, 2014 |DAILY BEAST 

The padlocked door down the hall was now open, and I found my purse. I Was Gang Raped at a UVA Frat 30 Years Ago, and No One Did Anything |Liz Seccuro |December 16, 2014 |DAILY BEAST 

She retrieved a cigarette from her purse and lit it without moving her face away from the screen. I Watched a Casino Kill Itself: The Awful Last Nights of Atlantic City’s Taj Mahal |Olivia Nuzzi |December 8, 2014 |DAILY BEAST 

Available at La Boîte SHOLDIT Clutch Wrap Purse, $70 We can all agree the dorky passport holders and money bags have got to go. The Daily Beast’s 2014 Holiday Gift Guide: For the Anthony Bourdain in Your Life |Allison McNearney |November 29, 2014 |DAILY BEAST 

These are longer than traditional ads, mini-stories, designed to pull at heart- as well as purse strings. How Monty The Penguin Won Christmas: Britain’s Epic, Emotional Commercials |Tim Teeman |November 16, 2014 |DAILY BEAST 

Absently his hands wandered through the pockets, and found his purse and the money in an outside pocket. The Homesteader |Oscar Micheaux 

I turned round, thrust my purse into the lap of the nearest, and with a light heart led the lady back to the hotel. Blackwood's Edinburgh Magazine, Volume 60, No. 372, October 1846 |Various 

Ernest called out; "you have forgotten your money;" and he held out a purse, but the man was gone. Blackwood's Edinburgh Magazine, Volume 60, No. 372, October 1846 |Various 

A Cremona Violin is, to a rich amateur, a loadstone that is sure to attract the shining metal from the depths of his purse. Violins and Violin Makers |Joseph Pearce 

She did not smile as she opened a black purse and produced an envelope which she handed to Delancy. Hooded Detective, Volume III No. 2, January, 1942 |Various