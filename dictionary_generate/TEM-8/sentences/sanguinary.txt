He saw Gen. Braddock as he passed on to his defeat, and could give a succinct account of that sanguinary action. The Every Day Book of History and Chronology |Joel Munsell 

He was long engaged in sanguinary war with the Turks and the French, who pillaged and destroyed his frontier towns. The Every Day Book of History and Chronology |Joel Munsell 

Battle of Surcoign; British defeated by the French after a sanguinary conflict. The Every Day Book of History and Chronology |Joel Munsell 

This sanguinary mixture is generally believed to possess effervescing properties when stirred. The Garret and the Garden |R.M. Ballantyne 

"Atavism can hardly explain a roaming animal with teeth and claws and sanguinary instincts," interrupted Maloney with impatience. Three More John Silence Stories |Algernon Blackwood