There are some new insights and, frankly, I think that the new proofs are better. Conducting the Mathematical Orchestra From the Middle |Rachel Crowell |September 2, 2020 |Quanta Magazine 

They are escaping countries that quite frankly have failed them. Full Transcript: Tomi Lahren on ‘The Carlos Watson Show’ |Daniel Malloy |August 31, 2020 |Ozy 

Despite the new public awareness of the problems, designing and implementing fair and beneficial algorithms is frankly really hard. The UK exam debacle reminds us that algorithms can’t fix broken systems |Karen Hao |August 20, 2020 |MIT Technology Review 

If both sides were pushed out of their corners, they would both have to concede quite a bit, and we’d frankly all be safer. Republicans And Democrats See COVID-19 Very Differently. Is That Making People Sick? |Amelia Thomson-DeVeaux |July 23, 2020 |FiveThirtyEight 

We absolutely should be going around the world looking for partners to work with, as opposed to thinking that magically Beijing is going to start acting in a way that, quite frankly, it hasn’t for decades. Will Covid-19 Spark a Cold War (or Worse) With China? (Ep. 414) |Stephen J. Dubner |April 23, 2020 |Freakonomics 

Frankly, I don't think even Michael Patrick King, Lisa Kudrow, and the show's biggest fans expected it to be this good. ‘The Comeback’ Finale: Give Lisa Kudrow All of the Awards |Kevin Fallon |December 29, 2014 |DAILY BEAST 

We might as well begin with the most confusing and, frankly, suspect person in all of Serial. The Scoop on ‘Serial’: Making Sense of The Nisha Call, Asia's Letters, and Our Obsession |Emily Shire |December 11, 2014 |DAILY BEAST 

Only now, when we were able to talk frankly and at length, did I come to realize how profoundly it had affected him. Ted Hughes’s Brother on Losing Sylvia Plath |Gerald Hughes |December 2, 2014 |DAILY BEAST 

Frankly, I have never heard anyone else speak with such insight into Afghan affairs, post-US surge. Heart of Darkness: Into Afghanistan’s Taliban Valley |Matt Trevithick, Daniel Seckman |November 15, 2014 |DAILY BEAST 

“Essentially because of the cops, quite frankly,” Bratton answered. Bill Bratton Scolds Giggling Audience at American Justice Summit |Lloyd Grove |November 11, 2014 |DAILY BEAST 

But I am afraid you would very soon get tired of us, and I ought to tell you, frankly, that our little home is to be—a broken up. Confidence |Henry James 

He is quite right, and I was quite wrong, and I told him so frankly which made "all's well" in a moment. Gallipoli Diary, Volume I |Ian Hamilton 

If I am returned, my main object, I avow it frankly, will be to make them the standing order. Punch, or the London Charivari, Volume 107, November 3, 1894 |Various 

To attempt to cut out Mrs. Kaye I should need a little genuine enthusiasm; and frankly, your beloved prodigy does not inspire it. Ancestors |Gertrude Atherton 

He hesitated to take a cigarette—and now her bright eyes frankly mocked him, and said, "A cigarette commits you to nothing!" Bella Donna |Robert Hichens