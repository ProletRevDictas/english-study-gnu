For two years she has eschewed demands for a Medicare for All vote that centrist worried would repel swing voters in their GOP-leaning districts. Trump moves closer to Pelosi in economic aid talks, and House speaker must decide next move |Rachael Bade, Erica Werner |September 17, 2020 |Washington Post 

The film, which was supposed to be in theaters earlier this year, is coming out via video-on-demand on Friday as the coronavirus pandemic continues to upend theatrical release schedules. ‘Antebellum’ tackles the past head on in an effort to ‘move forward’ |radmarya |September 17, 2020 |Fortune 

Similarly, Barr is often accused of undermining core Justice Department norms, particularly with his interventions in the Roger Stone and Michael Flynn cases and in his demand for an investigation into the origins of the Russia investigation. William Barr’s eyebrow-raising ‘robber barons’ metaphor for the Trump era |Aaron Blake |September 17, 2020 |Washington Post 

Oil demand is in a slump and it won’t recover any time soon. There’s growing consensus that oil demand won’t make a comeback |eamonbarrett |September 17, 2020 |Fortune 

That leaves them reliant on portable air filtration devices, which are often effective but can also be hard to find right now due to coronavirus-driven demand. Wildfire smoke and COVID-19 are a one-two punch for indoor air quality across the U.S. |dzanemorris |September 17, 2020 |Fortune 

So we do demand justice and we do speak up and make demands. Mexico’s Priests Are Marked for Murder |Jason McGahan |January 7, 2015 |DAILY BEAST 

“Competition is there, of course, but I think there is enough business for everyone as long as the demand is there,” he says. Ghost Ships of the Mediterranean |Barbie Latza Nadeau |January 6, 2015 |DAILY BEAST 

In doing so he exposed the failure of other airlines in the region to see the huge pent-up demand for cheap travel. Annoying Airport Delays Might Prevent You From Becoming the Next AirAsia 8501 |Clive Irving |January 6, 2015 |DAILY BEAST 

Judging from current figures, there would be a substantial demand for this option, too. Men Will Someday Have Kids Without Women |Samantha Allen |January 3, 2015 |DAILY BEAST 

There is one time, however, when couple dancing is in high demand, and that is around weddings. Iran’s Becoming a Footloose Nation as Dance Lessons Spread |IranWire |January 2, 2015 |DAILY BEAST 

You see, I am the city undertaker, and the people are dying here so fast, that I can hardly supply the demand for coffins. The Book of Anecdotes and Budget of Fun; |Various 

Wordsworth has illustrated how an unwise and importunate demand for a reason from a child may drive him into invention. Children's Ways |James Sully 

Fajardo seconds the demand of the citizens of Manila that the Audiencia be suppressed, alleging that it does more harm than good. The Philippine Islands, 1493-1898, Volume XX, 1621-1624 |Various 

England proclaimed a rough indignation at the demand for Gibraltar, which Austria had made in behalf of Spain. The Pastor's Fire-side Vol. 3 of 4 |Jane Porter 

This demand was made with scornful seriousness; with a ruthless application to the feelings of a son. The Pastor's Fire-side Vol. 3 of 4 |Jane Porter