After a few years these trips to the woods became less glamourous and the pickeruppers more critical. Northern Nut Growers Association Report of the Proceedings at the Thirty-Eighth Annual Meeting |Northern Nut Growers Association 

The mountains, seen partly above and partly below it, wore a glamourous purple. 'way Down In Lonesome Cove |Charles Egbert Craddock (AKA Mary Noailles Murfree) 

It was a squalid sight, though the festive season of the year and that glamourous air peculiar to Indiana brooded it. Lippincott's Magazine, Vol. 26, August, 1880 |Various 

There is something eternally morning-glamourous about these lands as they rise from the sea. Sea and Sardinia |D. H. Lawrence 

To a child, at least, even the meanest of us may seem glamourous with magic and wisdom. Seeing Things at Night |Heywood Broun