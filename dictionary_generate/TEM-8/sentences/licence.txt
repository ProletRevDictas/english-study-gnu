No commercial licences will be granted by the ISA without a full environmental-impact assessment. A Climate Solution Lies Deep Under the Ocean—But Accessing It Could Have Huge Environmental Costs |Aryn Baker |September 7, 2021 |Time 

Adverts for unofficial services selling government documents such as travel permits and driving licences are against Google’s own rules. No more Shopping Ads for digital books; Tuesday’s daily brief |Carolyn Lyden |May 4, 2021 |Search Engine Land 

When I joined that family, that was the last time, until we came here, that I saw my passport, my driver’s licence, my keys. The Little-Known History Behind the People of Color Who Joined the Royal Family Long Before Meghan |Suyin Haynes |March 12, 2021 |Time 

Hulagu then gave his men licence to rape, kill and plunder with the caveat that Christians and Jews were to be spared. In Threatening Baghdad, Militants Seek to Undo 800 Years of History |Justin Marozzi |August 16, 2014 |DAILY BEAST 

Thus Ney returned to France in disgrace with his comrades, and hated by his enemies owing to the licence he allowed his soldiers. Napoleon's Marshals |R. P. Dunn-Pattison 

On July 19 a proclamation was issued forbidding the possession of firearms without licence. The Philippine Islands |John Foreman 

In 1904 there was only one drinking-saloon, kept by a Bohemian-born American, who paid $6,000 a year for his monopoly licence. The Philippine Islands |John Foreman 

Yet who could truthfully charge her with having obtained her divorce in order thereby to claim any fresh licence for herself? Marriage la mode |Mrs. Humphry Ward 

It contains a vigorous attack on the licence of the press and of the "impudent, rascally Printer." Private Letters of Edward Gibbon (1753-1794) Volume 1 (of 2) |Edward Gibbon