If I’m looking for unprofessional things, I’ll go to a social media site before I go to Vogue or Refinery29. Digiday Guide: Everything you need to know about Gen Z’s media consumption habits |Kayleigh Barber |June 23, 2021 |Digiday 

The chief executive of the private company hired to conduct the audit has echoed false allegations that the election was stolen, and the process has been widely criticized by election experts as insecure and unprofessional. Arizona secretary of state says Maricopa County should replace voting equipment because GOP-backed recount compromised its security |Rosalind Helderman |May 21, 2021 |Washington Post 

Reporting his unprofessional behavior to the House leadership didn’t help either, she said, nor did ceasing to wear a dress that frequently drew his attention. North Dakota’s House expelled a member for the first time. Women accused him of ‘disturbing’ harassment. |Andrea Salcedo, Tim Elfrink |March 5, 2021 |Washington Post 

Goodell said in a written statement in August that the league condemned the “unprofessional, disturbing and abhorrent behavior” detailed in the allegations made by the women. Beth Wilkinson close to completing Washington Football Team investigation, Roger Goodell says |Mark Maske |February 5, 2021 |Washington Post 

“The tone set by Secretary Wilkie was at minimum unprofessional and at worst provided the basis for VA leaders’ attempts to undermine the veteran’s credibility,” the report said. Watchdog finds VA Secretary Robert Wilkie questioned the credibility of a House aide who reported a sexual assault at hospital |Lisa Rein |December 11, 2020 |Washington Post 

It was time for a very unprofessional meeting between the two presidents. Fifty Shades of Presidential FanFiction |Amy Zimmerman |August 2, 2014 |DAILY BEAST 

His silence was “uncool and unprofessional,” as Dr. Tanning put it. My Therapist Dumped Me |Lizzie Crocker |April 4, 2014 |DAILY BEAST 

Bonet had often clashed with creator Bill Cosby and was unprofessional backstage. How Awkward Will Isaiah Washington’s Return to ‘Grey’s Anatomy’ Be? |Marina Watts |March 7, 2014 |DAILY BEAST 

Mainstream American press coverage of Russia,” Cohen writes, has been “shamefully unprofessional and politically inflammatory. How the ‘Realists’ Misjudged Ukraine |James Kirchick |March 3, 2014 |DAILY BEAST 

They felt it was unprofessional to leave one dollar bill behind. Why I Fear the Aryan Brotherhood—and You Should, Too |Anonymous |April 1, 2013 |DAILY BEAST 

A physician who applied for membership in a medical society was rejected because of unprofessional conduct. Putnam's Handy Law Book for the Layman |Albert Sidney Bolles 

It had always been considered one of his recommendations that he was so unprofessional in his appearance. In Connection with the De Willoughby Claim |Frances Hodgson Burnett 

He sent for engineers from various parts of the country and amazed them with the unprofessional boldness of his methods. In Connection with the De Willoughby Claim |Frances Hodgson Burnett 

The journalist said nothing, as it seemed unprofessional to admit ignorance of anything. Tales of Fantasy and Fact |Brander Matthews 

Some of Shakespeare's plays I have never read; while others I have gone over perhaps as frequently as any unprofessional reader. Speeches and Letters of Abraham Lincoln, 1832-1865 |Abraham Lincoln