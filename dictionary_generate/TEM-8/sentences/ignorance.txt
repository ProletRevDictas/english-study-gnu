Enough with the employee town halls, the mealy-mouthed apologies, the feigned ignorance, the purple-prosed press releases. Facebook: Shut It Down |Nick Fouriezos |September 13, 2020 |Ozy 

The goal is to create a more inclusive environment that acknowledges and brings out the strengths and unique skill sets of neurodiverse employees, by removing ignorance and educating both team leaders and colleagues. Deep Dive: How companies and their employees are facing the future of work |Digiday |September 1, 2020 |Digiday 

CEO Guru Gowrappan said the group is partnering with organizations to conduct training on removing ignorance and how to be supportive, empathetic and inclusive. Deep Dive: How companies and their employees are facing the future of work |Digiday |September 1, 2020 |Digiday 

Better a wide and honest ignorance than a narrow and suspect knowledge. The business advice Socrates would give if he wrote a management book today |jakemeth |August 25, 2020 |Fortune 

After the truth came out, Lantos, who had a close relationship with Hill & Knowlton, would plead ignorance — and insist that there were certainly human right violations taking place in Kuwait. The Great Lie of the First Gulf War |Mat Nashed |August 17, 2020 |Ozy 

Many young people are still shedding the ignorance of our parents. ‘Empire’ Review: Hip-Hop Musical Chairs with an Insane Soap Opera Twist |Judnick Mayard |January 8, 2015 |DAILY BEAST 

They already know the answer, but they know by feigning ignorance they can create all this debate about it. Patton Oswalt on Fighting Conservatives With Satire |William O’Connor |January 6, 2015 |DAILY BEAST 

How the hell does somebody show up at a David Duke organized event in 2002 and claim ignorance? No. 3 Republican Admits Talking to White Supremacist Conference |Tim Mak |December 30, 2014 |DAILY BEAST 

Whether it was actual ignorance, senility, or some obscure test, it's hard to know. Alfred Hitchcock’s Fade to Black: The Great Director’s Final Days |David Freeman |December 13, 2014 |DAILY BEAST 

It's insane that you are losing friends in real life because of their ignorance on the Internet. The Unbearable Whiteness of Protesting |Rawiya Kameir, Judnick Mayard |December 10, 2014 |DAILY BEAST 

Thou fell spirit of pride, prejudice, ignorance, and mauvaise honte! Blackwood's Edinburgh Magazine, Volume 60, No. 372, October 1846 |Various 

The universal ignorance of the working class broke down the aspiring force of genius. The Unsolved Riddle of Social Justice |Stephen Leacock 

The Italian trip was discussed, and considerable ignorance of geography was, as is usual, manifested by all present. The Pit Town Coronet, Volume I (of 3) |Charles James Wills 

She walked faster, and would not look at him; but he, in his ignorance, misinterpreted these signs egregiously. Ramona |Helen Hunt Jackson 

He has for millions of years looked down upon the ignorance, the misery, the crimes of men. God and my Neighbour |Robert Blatchford