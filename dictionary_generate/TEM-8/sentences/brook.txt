He brooked little tolerance for her passions — Welsh corgis and horse racing — and she let him pursue his hobbies alone. Prince Philip, royal consort to Queen Elizabeth II, dies at 99 |Adrian Higgins |April 9, 2021 |Washington Post 

A tree hangs above a brook where the water flows smoothly over a ford. Time Flows Toward Order - Issue 93: Forerunners |Julian Barbour |December 2, 2020 |Nautilus 

If the brook had no banks and the water no viscosity, that would create the condition that radiation finds in the vast voids of our expanding universe, and the patterns would remain as beautiful forever. Time Flows Toward Order - Issue 93: Forerunners |Julian Barbour |December 2, 2020 |Nautilus 

Before the banks have their effect in the brook, mechanical energy is first entirely within each falling drop but is then spread out in the circular waves. Time Flows Toward Order - Issue 93: Forerunners |Julian Barbour |December 2, 2020 |Nautilus 

More than 70 mountain streams drop off the ridgeline that makes up Shenandoah National Park, and many of those rivers host healthy populations of native brook trout, as well as rainbow and brown trout. The Ultimate Shenandoah National Park Travel Guide |Graham Averill |October 7, 2020 |Outside Online 

He had then driven to the Ukrainian Orthodox Church headquarters in South Bound Brook. Ukrainians in U.S. Warn: ‘Mr. Putin, Heroes Do Not Die’ |Michael Daly |April 1, 2014 |DAILY BEAST 

Unable to help her legally, as her case had already been dismissed at every level, Brook referred her to me. What Military Base Shootings Reveal About the Mental Health Debate |Caitlin Dickson |February 9, 2014 |DAILY BEAST 

This might, Brook feared, convince jury members that a translator like Ali would be unnecessary. The Pirate Negotiator |Caitlin Dickson |November 14, 2013 |DAILY BEAST 

Drury is also the author of Hunts in Dreams, The Driftless Area, and The Black Brook. The National Book Awards Longlist for Fiction | |September 19, 2013 |DAILY BEAST 

Brook finds recurring trends in St. Petersburg, Mumbai, Shanghai, and Dubai. This Week’s Hot Reads: February 25, 2013 |G. Clay Whittaker |February 25, 2013 |DAILY BEAST 

He hunted about until he had found some acorns, and then, coming to a little brook of water he took a long drink. Squinty the Comical Pig |Richard Barnum 

Squinty turned around, standing on the edge of the little brook, and waited, his heart beating faster and faster. Squinty the Comical Pig |Richard Barnum 

I knew that the poor girl from Kansas must get up with the sun, too, for her uncle was not the man to brook any dawdling. The Soldier of the Valley |Nelson Lloyd 

Water Street, formerly Water Lane, had a brook running down one side of it when houses were first built there. Showell's Dictionary of Birmingham |Thomas T. Harman and Walter Showell 

So Alan laid his stick across the narrow part, and then jumped over the brook, followed by Owen and Amy. The Nursery, July 1873, Vol. XIV. No. 1 |Various