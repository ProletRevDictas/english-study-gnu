Despite flirting with another devastating blown lead, Kyle Shanahan remains a symphonic play-caller. What to know from the NFL playoffs: Mike McCarthy is in trouble and Josh Allen is a bad man |Adam Kilgore |January 18, 2022 |Washington Post 

Early in his career, he focused on the symphonic repertoire, developing a particular expertise in the works of Ludwig van Beethoven, Johannes Brahms, Anton Bruckner, Gustav Mahler and, later, Dmitri Shostakovich. Bernard Haitink, acclaimed Dutch orchestra conductor, dies at 92 |Matt Schudel |October 24, 2021 |Washington Post 

Some of his writings have been adapted for a symphonic work by British composer Jonathan Harvey. Hans Küng, Catholic theologian who challenged papal authority, dies at 93 |Matt Schudel |April 9, 2021 |Washington Post 

We’re not afraid to make choices that look different from a traditional symphonic chorus. The challenges of conducting in a pandemic |Patrick Folliard |February 9, 2021 |Washington Blade 

That’s how long it takes for the symphonic Marvel Studios intro to reel you in. ‘WandaVision’ on Disney Plus gives Marvel Studios its first true power couple |David Betancourt |January 15, 2021 |Washington Post 

He is known to be difficult, because of his love of the Latinate, and his non-linear, digressive, even symphonic, narrative style. Nigeria’s Larger-Than-Life Nobel Laureate Chronicles a Fascinating Life |Chimamanda Adichie |August 9, 2014 |DAILY BEAST 

But Samuragochi received sole credit, and such an arrangement would be a complete no-no in the symphonic realm. Japan’s Beloved Deaf Composer is Neither Deaf Nor a Composer |Jimmy So |February 13, 2014 |DAILY BEAST 

“On Mr. Fox, at first there was talk about using a symphonic orchestra,” he continues. Meet Alexandre Desplat, Hollywood’s Master Composer |Andrew Romano |February 11, 2014 |DAILY BEAST 

In the 19th century, Franz Liszt set about translating some of the greatest works of literature into symphonic tone poems. Kronos Quartet and PEN: When Music and Literature Collide |Raquel Laneri |May 2, 2012 |DAILY BEAST 

When and how could he become acquainted with the symphonic masterpieces of the great German composers? The Life &amp; Letters of Peter Ilich Tchaikovsky |Modeste Tchaikovsky 

During his student years, Orpheus was the only one of Liszts symphonic poems which attracted him. The Life &amp; Letters of Peter Ilich Tchaikovsky |Modeste Tchaikovsky 

On this same occasion Tchaikovsky begged Vladimir Stassov to suggest a subject for a symphonic fantasia. The Life &amp; Letters of Peter Ilich Tchaikovsky |Modeste Tchaikovsky 

Tchaikovskys creative talents, which are occasionally apparent in his symphonic works, are completely lacking in The Oprichnik. The Life &amp; Letters of Peter Ilich Tchaikovsky |Modeste Tchaikovsky 

I have only just finished the composition of a new work, the symphonic fantasia, Francesca da Rimini. The Life &amp; Letters of Peter Ilich Tchaikovsky |Modeste Tchaikovsky