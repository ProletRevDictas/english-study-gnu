Social factors also are at the heart of any pandemic response, particularly before the development of a vaccine. Researchers identify social factors inoculating some communities against coronavirus |Christopher Ingraham |February 11, 2021 |Washington Post 

While the question of how Ethiopia deals with its debts—particularly through this program—is extremely important, it is just one aspect of the country’s recovery from the pandemic, and future prosperity. What African countries should be fighting for in negotiations with China |Hannah Ryder |February 11, 2021 |Quartz 

While everyone is still waiting on Census data to tell us what districts and communities may be shifting this year, there are already some examples of where these communities’ voices could be particularly important, Erikat said. African Communities Warn Language Issues Could Shut Them Out of Redistricting |Maya Srikrishnan |February 10, 2021 |Voice of San Diego 

The BlackLight Summit, scheduled for March 4-6, is a dance exhibition and discussion centering and affirming the narratives and experiences of marginalized groups, particularly from the BIPOC and LGBTQ communities. BlackLight BIPOC, LGBTQ summit preview Feb. 25 |Philip Van Slooten |February 9, 2021 |Washington Blade 

Another, particularly at the seed stage, is the complexity of rounds today. PSA: Most aggregate VC trend data is garbage |Danny Crichton |February 9, 2021 |TechCrunch 

“Having been a legislator and a mayor, I particularly enjoy being a chief executive,” he said. The Golden State Preps for the ‘Red Wedding’ of Senate Races |David Freedlander |January 9, 2015 |DAILY BEAST 

The simple, awful truth is that free speech has never been particularly popular in America. How the PC Police Threaten Free Speech |Nick Gillespie |January 9, 2015 |DAILY BEAST 

Ben is not Orthodox or particularly committed to adhering to traditional Jewish laws. My Week on Jewish Tinder |Emily Shire |January 5, 2015 |DAILY BEAST 

Can you talk about some of the books you read that you think are particularly good on the political history of the 1960s? Thank Congress, Not LBJ for Great Society |Julian Zelizer, Scott Porch |January 4, 2015 |DAILY BEAST 

The trend is particularly concentrated in the coastal states where women are wealthier, more educated, and more liberal. Men Will Someday Have Kids Without Women |Samantha Allen |January 3, 2015 |DAILY BEAST 

The segments of the corolla are pointed but on some varieties unequal, particularly that of Shiraz tobacco. Tobacco; Its History, Varieties, Culture, Manufacture and Commerce |E. R. Billings. 

Still, gambling seemed to be made particularly fascinating here, and he wanted to be fascinated, wanted it badly. Rosemary in Search of a Father |C. N. Williamson 

In Manila particularly, amidst the pealing of bells and strains of music, unfeigned enthusiasm and joy were everywhere evident. The Philippine Islands |John Foreman 

The wave-like movement of these animals is particularly graceful and cleverly done. Women in the fine arts, from the Seventh Century B.C. to the Twentieth Century A.D. |Clara Erskine Clement 

Particularly was this the case with Davout, who since 1809 had suspected that Berthier desired to ruin his reputation. Napoleon's Marshals |R. P. Dunn-Pattison