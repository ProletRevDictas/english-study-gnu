I always thought of trees like flagpoles, vertical, not horizontal and beneath us, holding us up. A Mother’s Day letter to my daughter, after a year that changed our family forever |Alexandra Moe |May 7, 2021 |Washington Post 

Other subjects of discussion were the proper length and brand of zip ties for detaining members of Congress and how to use a flagpole and other objects to attack police officers. Gallows or guillotines? The chilling debate on TheDonald.win before the Capitol siege. |Craig Timberg |April 15, 2021 |Washington Post 

Videos show the group pummeling the wood-and-glass doors with a helmet, feet and a flagpole. U.S. Capitol Police officer cleared of wrongdoing in fatal shooting of Ashli Babbitt during Capitol attack |Keith L. Alexander, Justin Jouvenal, Spencer Hsu |April 15, 2021 |Washington Post 

One group of officers was left stranded, separated from their riot gear, which sat unused on a parked bus near the Capitol while unprotected officers endured beatings with metal pipes and flagpoles. “I Don’t Trust the People Above Me”: Riot Squad Cops Open Up About Disastrous Response to Capitol Insurrection |by Joaquin Sapien and Joshua Kaplan |February 12, 2021 |ProPublica 

Once inside, they used pipes, flagpoles and other weapons to shatter windows and break furniture. How the rioters who stormed the Capitol came dangerously close to Pence |Ashley Parker, Carol D. Leonnig, Paul Kane, Emma Brown |January 15, 2021 |Washington Post 

She described the way she and Igor watched the Donetsk flag go up and down the flagpole in front of their window. Mom and Pop on Ukraine’s Battle Line |Anna Nemtsova |June 2, 2014 |DAILY BEAST 

Then the huddles break, and the 200 or so children join hands in a ring around the flagpole. See You at the Pole: Church Youth Gatherings Raise Legal Questions |Katherine Stewart |January 26, 2012 |DAILY BEAST 

The knots were so plentiful that the thread stood up like a gnarled flagpole. 'Are You Also With Fever?' |Dr. Abraham Verghese |February 11, 2009 |DAILY BEAST 

Mrs. Noah turned pale and the Weathercock shifted about uneasily on the top of the flagpole. The Cruise of the Noah's Ark |David Cory 

"Someone has been making a flagpole," said the Angel, running the toe of her shoe around the stump, evidently made that season. Freckles |Gene Stratton-Porter 

I painted a flagpole on a barn up in Massachusetts where there was four hundred dollars in gold hidden under the weather-vane. Careers of Danger and Daring |Cleveland Moffett 

Then it took a long jump straight down Wall Street, smashed a flagpole to slivers, and vanished. Careers of Danger and Daring |Cleveland Moffett 

And so you walk right up the building or church or flagpole, and the smoother the surface the easier you go up. Careers of Danger and Daring |Cleveland Moffett