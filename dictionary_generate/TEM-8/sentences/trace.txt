It’s such a nuisance that Friends of Acadia has a crew of volunteer ridge runners whose mission is dismantling outlaw cairns and educating hikers to leave no trace. The Ultimate Acadia National Park Travel Guide |Virginia M. Wright |February 8, 2021 |Outside Online 

Both are unlikely to turn up in the fossil record, and that’s normal for trace fossils. Giant worms may have burrowed into the ancient seafloor to ambush prey |Helen Thompson |January 22, 2021 |Science News 

Wage and Hour employees failed to so much as enter five of the 10 complaints in the department’s database, producing no trace that a complaint was ever filed. All a Gig-Economy Pioneer Had to Do Was “Politely Disagree” It Was Violating Federal Law and the Labor Department Walked Away |by Ken Armstrong, Justin Elliott and Ariana Tobin |January 22, 2021 |ProPublica 

Despite a few flurries Wednesday, Washington has yet to record a trace of snow so far this January. Increasing chances for a wintry mix early next week in the D.C. area |Jason Samenow, Wes Junker |January 21, 2021 |Washington Post 

They also found traces of blood on the stone knife he was carrying. Humans Have Gotten Nicer and Better at Making War - Issue 94: Evolving |Steve Paulson |January 6, 2021 |Nautilus 

Throughout all the stories of loss and pain with the Chief, there was barely a trace of emotion. The Story Behind Lee Marvin’s Liberty Valance Smile |Robert Ward |January 3, 2015 |DAILY BEAST 

“Almost all of our human activities leave a chemical trace in the water,” says Alm. The Secret to Tracking Ebola, MERS, and Flu? Sewers |Wudan Yan |November 29, 2014 |DAILY BEAST 

Recently, when whistleblowers finally surfaced, the Home Office officials could find no trace of the dossier. The Castration of Alan Turing, Britain’s Code-Breaking WWII Hero |Clive Irving |November 29, 2014 |DAILY BEAST 

He mounted a Trace Elliot amplifier on the back of the truck. Greil Marcus Talks About Trying to Unlock Rock and Roll in 10 Songs |Allen Barra |November 17, 2014 |DAILY BEAST 

And it is nearly impossible to trace each knockoff to each patient or to confirm how many were affected. Patients Screwed in Spine Surgery ‘Scam’ |The Center for Investigative Reporting |November 3, 2014 |DAILY BEAST 

In chronic interstitial nephritis it is small—frequently no more than a trace. A Manual of Clinical Diagnosis |James Campbell Todd 

They speak of a certain Norumbega and give the names of cities and strongholds of which to-day no trace or even report remains. The Jesuit Relations and Allied Documents, Vol. II: Acadia, 1612-1614 |Various 

Against the blue background of the sky, green hill-tops trace an undulant line. Bastien Lepage |Fr. Crastre 

A trace of bile may be present as a result of excessive straining while the tube is in the stomach. A Manual of Clinical Diagnosis |James Campbell Todd 

A trace of light had begun to soften the sky over the dome, but had not yet seeped down to ground level. Fee of the Frontier |Horace Brown Fyfe