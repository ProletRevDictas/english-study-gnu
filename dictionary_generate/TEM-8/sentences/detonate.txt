Any one of them could have—and still may—detonate the deal if the negotiated plan falls apart. Why Congress Deserves Its Own Emmy Category |Philip Elliott |July 15, 2021 |Time 

Another way would be to fly a grenade immediately to where the enemy is positioned, detonating it on contact. The US Marines are testing flying, remote-controlled grenades |Rob Verger |July 13, 2021 |Popular-Science 

The leak in Amuay, Venezuela, was slow but kept leaking for more than an hour before the vapor detonated. Engineers raise alarms over the risk of major explosions at LNG plants |Will Englund |June 3, 2021 |Washington Post 

Instead, a man detonated 80 pounds of explosives as part of an elaborate gender-reveal party stunt, the Kingston Police Department told the New Hampshire Union Leader. A huge explosion cracked house foundations in New Hampshire. An ‘extreme’ gender-reveal party was to blame. |Andrea Salcedo |April 23, 2021 |Washington Post 

The first H-bomb, detonated by the United States in 1952, was 1,000 times as powerful as the bomb dropped on Hiroshima. How matter’s hidden complexity unleashed the power of nuclear physics |Emily Conover |April 8, 2021 |Science News 

His target splits with a satisfying rumble, and then the fragments detonate as he strafes them with more bullets. ‘Asteroids’ & The Dawn of the Gamer Age |David Owen |November 29, 2014 |DAILY BEAST 

Operators on the ground chose to detonate the rocket shortly after launch once it was established that there were problems. Harry Potter Raps, The Catcalls Heard ‘Round the World and More Viral Videos |Alex Chancey |November 2, 2014 |DAILY BEAST 

He was referring to the lone wolves such as ISIS is now urging online to detonate pipe bombs in Times Square. The Loser Who Wanted to Be the ISIS Agent Next Door |Michael Daly |September 18, 2014 |DAILY BEAST 

Al-Shahzad failed to properly detonate his bomb and was reported to the New York police by a Muslim-American street vendor. Exclusive: Al Qaeda’s American Fighters Are Coming Home—And U.S. Intelligence Can’t Find Them |Eli Lake |May 20, 2014 |DAILY BEAST 

That a suicide bomber will detonate himself in the middle of Fifth Avenue? Nathaniel Rich: How I Write |Noah Charney |April 3, 2013 |DAILY BEAST 

He then unscrewed the fuze and threw it away before it could detonate the shell. The History of the 51st (Highland) Division 1914-1918 |Frederick William Bewsher 

They are very difficult to detonate, and if set on fire do not explode like gunpowder. Encyclopaedia Britannica, 11th Edition, Volume 10, Slice 6 |Various 

The depth-charge had fouled a trailing wire from some of my ‘stage scenery sky’ and been dragged along to detonate close astern. Sea-Hounds |Lewis R. Freeman 

I decided to send three balls down each, leave 12 in the cavern, then detonate them all at once. The Airlords of Han |Philip Francis Nowlan 

In fact guncotton in the colloid state may be hammered on an anvil, and, as a rule, only the portion struck will detonate or fire. Encyclopaedia Britannica, 11th Edition, Volume 10, Slice 1 |Various