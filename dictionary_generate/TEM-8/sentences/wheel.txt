An ATV’s power goes to zero as soon as the wheels lose traction, so naturally, you want more grip. Six ways to make your ATV even more rugged |By Tyler Freel/Outdoor Life |September 17, 2020 |Popular-Science 

But, we aren’t at a point where marketers can take our hands entirely off the steering wheel. Google’s search terms move will make millions in ad spend invisible to advertisers |Ginny Marvin |September 3, 2020 |Search Engine Land 

The wheels and legs do stick out a bit beyond the actual grill when it’s folded. 5 Portable Grills for All Your Outdoor Cooking Needs |Amy Marturana Winderl |September 2, 2020 |Outside Online 

In hub drive systems, the motor is housed in one of the wheels. Do You Want to Buy an E-Cargo Bike? Read This First. |Joe Lindsey |August 30, 2020 |Outside Online 

It would have to put an impossibly large number of customers behind the wheel. Tesla has a business model problem: It can never justify its current stock price by simply making cars |Shawn Tully |August 29, 2020 |Fortune 

They were racing toward the corner of Tompkins and Myrtle avenues with Johnson at the wheel when another call came over the radio. 'Please Don't Die!': The Frantic Battle to Save Murdered Cops |Michael Daly |December 22, 2014 |DAILY BEAST 

“They think Putin is the only evil in Russia and dream about getting rid of him,” he said, tightening his grip on the wheel. Think Putin’s Bad? Wait for the Next Guy |Anna Nemtsova |November 14, 2014 |DAILY BEAST 

Maybe the wheel will turn again, and heterosexuality will come to seem edgy. How Straight World Stole ‘Gay’: The Last Gasp of the ‘Lumbersexual’ |Tim Teeman |November 12, 2014 |DAILY BEAST 

It was only a matter of time that the wheel turned its full revolution. How Straight World Stole ‘Gay’: The Last Gasp of the ‘Lumbersexual’ |Tim Teeman |November 12, 2014 |DAILY BEAST 

The “little joker” was a small tin wheel with a wire attached to it, which fit inside the combination of any bank safe. The High Society Bank Robber of the 1800s |J. North Conway |October 19, 2014 |DAILY BEAST 

Never again would he sit behind that wheel rejoicing in the insolence of speed. The Joyous Adventures of Aristide Pujol |William J. Locke 

He deposited it on the vacant seat, clambered up behind the wheel, and started. The Joyous Adventures of Aristide Pujol |William J. Locke 

The non-elastic character of water made it unsuitable for a machine requiring a fly-wheel. Life of Richard Trevithick, Volume II (of 2) |Francis Trevithick 

To quote Mrs. Kaye, 'A Liberal peer is as useful as a fifth wheel to a coach, and as ornamental as whitewash.' Ancestors |Gertrude Atherton 

I think 6½ feet diameter for the fly, and 9½ inches diameter for the small wheel, will give speed enough to the drum. Life of Richard Trevithick, Volume II (of 2) |Francis Trevithick