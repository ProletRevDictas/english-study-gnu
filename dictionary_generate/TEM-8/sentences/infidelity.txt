Known for her love and discussion of celebrities during her “Hot Topics” segment, Williams found herself a hot topic when she divorced her husband of 22 years among allegations of infidelity in 2019. 8 Things We Learned About Wendy Williams From Her Lifetime Biopic & Documentary |Brande Victorian |January 31, 2021 |Essence.com 

North Carolina voter DeNeiro Saunders is a Christian who doesn’t like to excuse infidelity. Cal Cunningham lost. Katie Hill resigned. Are we still put off by sex scandals? |Lisa Bonos |November 12, 2020 |Washington Post 

Voters are usually of two minds when it comes to infidelity, Perry says. Cal Cunningham lost. Katie Hill resigned. Are we still put off by sex scandals? |Lisa Bonos |November 12, 2020 |Washington Post 

In theory, though, Americans are harsh critics of marital infidelity. Despite A Sexting Scandal, Democrat Cal Cunningham Is Favored In North Carolina’s Senate Race |Geoffrey Skelley (geoffrey.skelley@abc.com) |October 15, 2020 |FiveThirtyEight 

Democrats are confident they can use that cash to fight the GOP’s bid to attack him for infidelity. A covid-19 diagnosis, texts suggesting infidelity roil pivotal N.C. Senate race |Pam Kelley, Rachael Bade, Paul Kane |October 6, 2020 |Washington Post 

Amid accusations of infidelity, she told reporters in 1988 that she and the former priest were just fine. Adiós to the Diva Duchess |Barbie Latza Nadeau |November 20, 2014 |DAILY BEAST 

Was it difficult revisiting any of the darker memories, like his infidelity? All Eyes on Anjelica Huston: The Legendary Actress on Love, Abuse, and Jack Nicholson |Alex Suskind |November 10, 2014 |DAILY BEAST 

The public conversation about infidelity has undoubtedly become more nuanced. Why You’re Happily Married and Having an Affair |Ryan Selzer |November 2, 2014 |DAILY BEAST 

They succumbed to infidelity, remained materialistic, and acted selfishly. The Gospel According to Thomas Jefferson (And Tolstoy and Dickens) |Samuel Fragoso |October 26, 2014 |DAILY BEAST 

Lets rap about new feminism, hip-hop and infidelity," she begins, "You got a good girl / Why she messing with a bad guy? Elevator Music Beyoncé Doesn’t Want to Hear: Jay Z’s ‘Mistress’ Drops ‘Sorry Mrs. Carter’ |Amy Zimmerman |August 8, 2014 |DAILY BEAST 

The infidelity of her father and the piety of her mother contended, like counter currents of the ocean, in her bosom. Madame Roland, Makers of History |John S. C. Abbott 

Much more, many persons were ready to follow them into all the wild sophistries of infidelity. Gospel Philosophy |J. H. Ward 

She looked round the room, apparently recognizing with resentment the scene of Tanqueray's perpetual infidelity. The Creators |May Sinclair 

May there not be the greatest practical infidelity with the most artistic beauty and native reach of thought? Beacon Lights of History, Volume I |John Lord 

Upon what, then, is based the opinion that divorce is permissible in case of infidelity on the part of the woman? My Religion |Leo Tolstoy