An extra review of the warrant by a commanding officer could have assessed the risk associated with acting on information that was reportedly wrong and outdated. The policing reforms in the Breonna Taylor settlement, explained |Fabiola Cineas |September 17, 2020 |Vox 

Forty-three percent of Republicans and Republican-leaning independents in the Pew Research Center survey said voter fraud is a “major problem” associated with mail-in ballots. Trump contradicts CDC director on vaccine; Biden says Americans shouldn’t trust Trump |Colby Itkowitz, Felicia Sonmez, John Wagner |September 16, 2020 |Washington Post 

That also means that there is hope for companies which are associated with the most unsustainable practices. Volkswagen is the latest carmaker to tap the red-hot green-bond market to fund its EV ambitions |Bernhard Warner |September 16, 2020 |Fortune 

Potential transparency policies to improve patient safetyIn a research paper studying the risks associated with excipients, my co-author and I make three main recommendations to improve patient safety. The ‘inactive' ingredients in your pills could harm you |By Yelena Ionova/The Conversation |September 15, 2020 |Popular-Science 

On TikTok, audio is often a key component of hashtag challenges, with particular clips of music associated with specific challenges. Deep Dive: How the Summer of 2020 forced brand marketing to change for the better |jim cooper |September 14, 2020 |Digiday 

Surenos are told when to workout, who to associate with and how to distribute any funds they make from illegal activity. The Mexican Mafia Is the Daddy of All Street Gangs |Seth Ferranti |December 11, 2014 |DAILY BEAST 

Over at Sears, a sales associate makes just $8.44 an hour, $14,770. It’s Always Black Friday for Clerks |Michael Tomasky |November 28, 2014 |DAILY BEAST 

“He turned everybody he knew into somebody else he knew,” the former associate said. The Godfather of Right-Wing Radio |Caitlin Dickson |November 23, 2014 |DAILY BEAST 

She lived it — civil rights and other issues that you associate from the family. This Republican Loved Taxes & Modern Art |Scott Porch |November 19, 2014 |DAILY BEAST 

She now serves as an Associate Professor at Colorado State University and has authored several books on autism and animal science. The Most Inspiring Bits of Temple Grandin’s Reddit AMA |Emily Shire |November 18, 2014 |DAILY BEAST 

The reason we associate rhythm with the significance of time is that rhythm is a measurer of time. Expressive Voice Culture |Jessie Eldridge Southwick 

He did not know what the scent was, but it smelled rich and artificial, and he disliked to associate it with his new friend. Rosemary in Search of a Father |C. N. Williamson 

With this political subjection one is reluctant to associate a more sordid kind of obligation. King Robert the Bruce |A. F. Murison 

Generally persons who associate for charitable or benevolent purposes do not regard themselves in a legal sense as partners. Putnam's Handy Law Book for the Layman |Albert Sidney Bolles 

And vowing to Him in an individual capacity, will not be accepted for vowing and swearing to Him in a public associate character. The Ordinance of Covenanting |John Cunningham