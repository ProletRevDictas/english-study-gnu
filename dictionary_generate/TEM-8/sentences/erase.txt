Milestone moments like graduation and homecoming have been erased. The loneliness of an interrupted adolescence |Ellen McCarthy |February 11, 2021 |Washington Post 

One looming change is the death of the third party cookie, which threatens to erase everything brands thought they knew about harnessing data. Brand Summit Recap: Marketers face looming identity crisis |Digiday Editors |February 10, 2021 |Digiday 

Once barely touched, rural communities were experiencing multiple outbreaks, fueling a more than fivefold spike in infections that erased the racial gap seen until that point in the pandemic. 900,000 infected. Nearly 15,000 dead. How the coronavirus tore through D.C., Maryland and Virginia. |Rebecca Tan, Antonio Olivo, John D. Harden |February 5, 2021 |Washington Post 

The 26-minute documentary introducing the groundless theory went viral on Facebook in the spring before the company moved to erase it from its platform. Anti-vaccine protest at Dodger Stadium was organized on Facebook, including promotion of banned ‘Plandemic’ video |Isaac Stanley-Becker |February 2, 2021 |Washington Post 

This lets you erase parts of the second layer while still seeing how it will line up with what’s behind it. How to make a legendary meme |John Kennedy |January 22, 2021 |Popular-Science 

Do as Tumblr has done and scrub her last words off the Internet—erase everything she wanted the world to hear. Cover-Ups and Concern Trolls: Actually, It's About Ethics in Suicide Journalism |Arthur Chu |January 3, 2015 |DAILY BEAST 

Is this a mature expression of understandable judgment, or a bid to erase history while conflating fiction and reality? Newsflash: Bill Cosby Is Not Cliff Huxtable |Tim Teeman |November 20, 2014 |DAILY BEAST 

Its militants say explicitly they are out to erase the borders that Sykes-Picot established across most of the modern Middle East. Turkish President Declares Lawrence of Arabia a Bigger Enemy than ISIS |Jamie Dettmer |October 13, 2014 |DAILY BEAST 

Later, she told a local reporter that she had used a chemical to erase her fingerprints. The Mystery Woman Who Tried to Outdo Dillinger |Michael Daly |September 29, 2014 |DAILY BEAST 

I was worried that a movie about the case would erase Meredith for good. What It's Like to Watch Kate Beckinsale Play You in a Movie |Barbie Latza Nadeau |September 3, 2014 |DAILY BEAST 

But they could not erase the past; they could not control the more distant future. The Bronze Eagle |Emmuska Orczy, Baroness Orczy 

The test applied was to erase some particular letter of the alphabet from one page of a book. The Sexual Life of the Child |Albert Moll 

I pointed out where the ground had been smoothed over as though to erase the traces of a struggle. A Frontier Mystery |Bertram Mitford 

You worked your way outward on this run, and the High Council didn't see fit to erase those memories or inhibit them. The Colors of Space |Marion Zimmer Bradley 

In regard to the chemicals used to erase ink, much depends upon the ink. Disputed Handwriting |Jerome B. Lavay