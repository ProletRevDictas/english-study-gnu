“Amazon is throwing down their throat that the union is going to take your money,” said a pro-union worker at the Bessemer facility, who spoke on the condition of anonymity over fear of retribution. Amazon’s anti-union blitz stalks Alabama warehouse workers everywhere, even the bathroom |Jay Greene |February 2, 2021 |Washington Post 

The player is one of several members of Middle Tennessee’s football program to speak on the condition of anonymity to avoid possible retribution. A college football coach’s season at war with the coronavirus — and his own school |Kent Babb |January 19, 2021 |Washington Post 

Lynn, for example, said he is seeing calls for violent retribution continue to spread on conservative websites like Newsmax and on extremist social media apps like Gab and Parler. Americans across the political spectrum fear what the Capitol attack portends |Annie Gowen, Jenna Johnson, Holly Bailey |January 12, 2021 |Washington Post 

Beijing has opposed the resolution, dishing out retribution against Australia for its role in spearheading the movement. WHO team investigating the origin of COVID-19 will enter China after delays |Eamon Barrett |January 11, 2021 |Fortune 

This contrasts with the more traditional criminal justice approach of retribution, and firms like Designing Justice believe physical design can increase the effectiveness of this alternative approach. How physical spaces can be designed to foster peacemaking in the criminal justice system |Aric Jenkins |December 9, 2020 |Fortune 

Like many other Pakistani Taliban, Jamal has his own horror stories to tell, which he believes can justify any bloody retribution. Pakistani School Killers Want to Strike the U.S. |Sami Yousafzai, Christopher Dickey |December 17, 2014 |DAILY BEAST 

We are all guilty all the time and retribution will come for our unnamed sins. Alfred Hitchcock’s Fade to Black: The Great Director’s Final Days |David Freeman |December 13, 2014 |DAILY BEAST 

Soon, though, voices from off camera begin shouting for retribution, not justice, chanting “Burn this b**** down.” Michael Brown’s Stepfather Tells Crowd, ‘Burn This Bitch Down’ |Jack Holmes, The Daily Beast Video |November 25, 2014 |DAILY BEAST 

He came around at a time when a lot of urban artists were getting dicked around by labels and had no means of retribution. Method Man Talks Wu-Tang Clan Reunion, Fake Rappers, and the Suge Knight Shooting |Marlow Stern |September 15, 2014 |DAILY BEAST 

The lane closures on the bridge last September were seemingly done to exact political retribution, Shultz and company said. Dems' Unspeakably Lame Bridgegate Stunt |Olivia Nuzzi |September 8, 2014 |DAILY BEAST 

That evening Malcolm witnessed the plundering of Fattehpore, which was permitted in retribution for its recent rebellion. The Red Year |Louis Tracy 

As for my share, retribution has held its heavy hand upon me; it is upon me still, Heaven knows. Elster's Folly |Mrs. Henry Wood 

In the case of the latter, however, retribution was only visited upon the after-generation of smokers. A Cursory History of Swearing |Julian Sharman 

Moreover, there was danger of retribution: settlements not so far off; soldiers still nearer. Overland |John William De Forest 

Desolation broods in yonder sad city—solemn retribution hath avenged our dishonored banner! The Flag Replaced on Sumter |William A. Spicer