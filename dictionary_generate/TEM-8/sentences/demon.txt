Maxwell’s point was not that the demon was a lawbreaker, but that the second law was statistical. Top 10 science anniversaries to celebrate in 2021 |Tom Siegfried |February 3, 2021 |Science News 

A demon is trapped inside of Ha Ram’s body, and its strength grows as Ha Ram’s feelings for Hong Chun-Gi become stronger. The 15 Most Anticipated Korean Dramas of 2021 |Kat Moon |January 29, 2021 |Time 

It’s not that the team particularly loves these blood-sucking demons. Scientists Made a Biohybrid Nose Using Cells From Mosquitoes |Shelly Fan |January 26, 2021 |Singularity Hub 

I own it all and this is the right response when these demons do digging. Gab, the social network that has welcomed Qanon and extremist figures, explained |Travis Andrews |January 11, 2021 |Washington Post 

Huhn’s mom, Gail Fogelman, said she’s angry over Fiorica’s actions but she “doesn’t want to portray him as an evil demon.” DNA Testing Is Unearthing Local Fertility Fraud Cases |Jared Whitlock |January 5, 2021 |Voice of San Diego 

No matter how much he burrows into his mind, he must face the demon of death. The Walking Dead’s Luke Skywalker: Rick Grimes Is the Perfect Modern-Day Mythical Hero |Regina Lizik |October 28, 2014 |DAILY BEAST 

While Ichabod is checking for non-existent cell service, Abbie learns that Moloch is planning to release a demon army on earth. Naked Ben Franklin Christens the Campy Return of ‘Sleepy Hollow’ |Amy Zimmerman |September 23, 2014 |DAILY BEAST 

What a demon, a behemoth, evil just seems to be seeping through him. 13 Celebrities Who Dissed Justin Bieber |Kevin Fallon |August 7, 2014 |DAILY BEAST 

He fashioned BOB, the demon spirit who lived in the Black Lodge, but such an explanation satisfied precious few of us. ‘True Detective,’ Obsessive-Compulsive Noir, and ‘Twin Peaks’ |Jimmy So |March 14, 2014 |DAILY BEAST 

In a sharp voice, Larson commanded the attention of my demon. My $295 Skype Exorcism |Scott Bixby |February 6, 2014 |DAILY BEAST 

It may be noted in passing that in the three miracles in Matthew of exorcising a blinding demon the title “Son of David” is used. Solomon and Solomonic Literature |Moncure Daniel Conway 

And so saying, the mis-shapen little demon set up a hideous yell, and danced upon the ground as if frantic with rage. Oliver Twist, Vol. II (of 3) |Charles Dickens 

Tom meekly did as he was bid, but in his heart there raged the passions of a demon, and he swore Mark Grafton should die. The Courier of the Ozarks |Byron A. Dunn 

In The Demon I have found some beautiful things, but a good deal of padding, too. The Life &amp; Letters of Peter Ilich Tchaikovsky |Modeste Tchaikovsky 

"Either they or some demon changelings," answered the old man, rocking to and fro upon the mats. The Dragon Painter |Mary McNeil Fenollosa