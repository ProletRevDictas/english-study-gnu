Though shy at first, I learned to convey emotions without apprehension. She’s 90 and Italian. I’m in my 30s and half Indian. Here’s why we’re the closest of friends. |Raj Tawney |January 27, 2021 |Washington Post 

Winner was also left with no time to strategize with a lawyer before her speedy apprehension, she noted. Laura Poitras says she’s been fired by First Look Media over Reality Winner controversy. Now she’s questioning the company’s integrity. |Sarah Ellison |January 14, 2021 |Washington Post 

There seemed to have been a lot of apprehension about the blue wave—and we didn’t quite get the blue wave, if you will. It’s officially a blue wave. What that historically means for stocks |Anne Sraders |January 7, 2021 |Fortune 

It’s an inevitability of the pandemic, and it’s a constant source of apprehension. The Cleveland Browns were an NFL feel-good story. Then the coronavirus got jealous. |Jerry Brewer |January 6, 2021 |Washington Post 

At the time, there was mild apprehension—but much changed that week. Is indoor dining safe? Five health experts weigh in. |By Laurie Archbald-Pannone Et Al./The Conversation |January 6, 2021 |Popular-Science 

There may even be a physiological basis to our apprehension about the “other.” Ferguson, Immigration, and ‘Us Vs. Them’ |Gene Robinson |November 27, 2014 |DAILY BEAST 

Did you have any apprehension as far as playing Nancy again? Jessica Alba on 'Sin City,' Typecasting, and How Homophobia Pushed Her Away From the Church |Marlow Stern |August 18, 2014 |DAILY BEAST 

They never procure them without exertion, and they never indulge in them without apprehension. Why Kids Are Making Us Crazy |James Poulos |March 30, 2014 |DAILY BEAST 

Soon Arab-American and Muslim-American groups joined in expressing their apprehension. Hollywood’s Major Muslim Problem Doesn't End With 'Alice in Arabia' |Dean Obeidallah |March 22, 2014 |DAILY BEAST 

Of course, there is plenty to celebrate, but there is an unmistakable sense of apprehension hanging over the anniversary. World Wide Web Turns 25 |Nico Hines |March 12, 2014 |DAILY BEAST 

The mother played her accompaniments and at the same time watched her daughter with greedy admiration and nervous apprehension. The Awakening and Selected Short Stories |Kate Chopin 

Hilda took the letter with apprehension, as she recognized the down-slanting calligraphy of Sarah Gailey. Hilda Lessways |Arnold Bennett 

The apprehension that God will punish for not making fulfilment to him accompanies equally the oath and the vow. The Ordinance of Covenanting |John Cunningham 

He gave so violent a start, his face expressed so much of apprehension and dismay, that I stared at him blankly. Uncanny Tales |Various 

Not merely must there be a desire to perform the service; but there must be an enlightened apprehension of its nature. The Ordinance of Covenanting |John Cunningham