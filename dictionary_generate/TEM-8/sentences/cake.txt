In 1992, the rice cake and popcorn cake market was valued at $174 million and growing. The Rise and Fall of the Rice Cake, America’s One-Time Favorite Health Snack |Brenna Houck |September 17, 2020 |Eater 

CEO at Bandwagon, a fan identity and data and analytics startup in AustinKiddopia, which is optimized for preschool-aged kids, features fun lessons on ABCs and numbers but also includes skills-based things like baking a cake or making a pizza. Need to entertain your kids? These educational apps may help |Michal Lev-Ram, writer |September 13, 2020 |Fortune 

Try it out on other foods, like brownies, cakes, veggies, and more. Pizza cutters that will get you the slice of your dreams |PopSci Commerce Team |September 2, 2020 |Popular-Science 

Instead of laying a chip’s various “neighborhoods” next to each other in a 2D silicon sprawl, they’ve stacked them on top of each other like a layer cake. Moore’s Law Lives: Intel Says Chips Will Pack 50 Times More Transistors |Jason Dorrier |August 23, 2020 |Singularity Hub 

With proper preparations, online reputation management becomes a piece of cake. Online reputation management: Seven steps to success |Aleh Barysevich |June 3, 2020 |Search Engine Watch 

A big cake requires a big festival, and Augustus was happy to comply. One Cake to Rule Them All: How Stollen Stole Our Hearts |Molly Hannon |December 24, 2014 |DAILY BEAST 

The Stollen was paraded through the city of Dresden, and later an appointed “Stollen girl” cut the cake. One Cake to Rule Them All: How Stollen Stole Our Hearts |Molly Hannon |December 24, 2014 |DAILY BEAST 

Now, it is the most traditional and celebrated Christmas cake in Germany—and definitely not associated with fasting. One Cake to Rule Them All: How Stollen Stole Our Hearts |Molly Hannon |December 24, 2014 |DAILY BEAST 

He had a special knife designed to cut the dense loaf, and a ceremony to precede cutting the cake. One Cake to Rule Them All: How Stollen Stole Our Hearts |Molly Hannon |December 24, 2014 |DAILY BEAST 

The tasteless bread was transformed into a sweet cake that included ingredients, such as dried fruit and marzipan. One Cake to Rule Them All: How Stollen Stole Our Hearts |Molly Hannon |December 24, 2014 |DAILY BEAST 

They would feed him apples, potatoes and sometimes bits of cake that Bob's mother gave them. Squinty the Comical Pig |Richard Barnum 

It was with much amazement that they watched Henrietta absorb sandwiches, cake, eggs, and fruit. The Campfire Girls of Roselawn |Margaret Penrose 

She peered around the room for the last time, and then dropped two small clean towels and a cake of soap into the bag. The Box-Car Children |Gertrude Chandler Warner 

The celebrant sprinkled the victim with wine and salted cake, and made a symbolic gesture with the knife. The Religion of Ancient Rome |Cyril Bailey 

Then they all had coffee and cake, shook hands with Pete Senior, and went to their homes and laboratories. Old Friends Are the Best |Jack Sharkey