Unlike the approaches favored by other Justices, Breyer’s brand of well-tempered empiricism forced him to be candid about what informed his judgment. What the Supreme Court Loses With Justice Breyer's Retirement |Aziz Huq |June 30, 2022 |Time 

Roosevelt, the Emperor of Empiricism, never learned the lesson Keynes tried to teach. Stop Trying to Balance Budgets! |Harold Evans |June 28, 2010 |DAILY BEAST 

If President-elect Obama truly does forecast a new Age of Empiricism, then Larry is the best choice for Treasury. Larry Summers for Treasury |Peter Hopkins |November 13, 2008 |DAILY BEAST 

Throughout civilized Europe a sort of carnival of empiricism prevailed. Art in England |Dutton Cook 

Aristotle, by long odds the greatest naturalist of antiquity, laid the first philosophic basis for empiricism. An Epitome of the History of Medicine |Roswell Park 

To the vocal scientist, "wearing the voice into place" represents the depth of empiricism. The Psychology of Singing |David C. Taylor 

This change in the character of vocal instruction will not be in any sense a return to empiricism. The Psychology of Singing |David C. Taylor 

The term empiricism is, however, now applied to any philosophical system which finds all its material in experience. The New Gresham Encyclopedia |Various