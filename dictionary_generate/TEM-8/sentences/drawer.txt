If you’re like my family, you have a drawer full of masks, but they always seem to be dirty. Outdoorsy Gear Guy–Approved Valentine's Day Gifts |Joe Jackson |February 10, 2021 |Outside Online 

Sometimes it’s as simple as opening a drawer to climb up a desk, other times, puzzles involve multiple steps, such as manipulating an elevator to reach an unseen floor, distracting enemies, or luring foes into traps. ‘Little Nightmares II’ made me dread every moment. And I loved it. |Elise Favis |February 9, 2021 |Washington Post 

The other 15 or so tablespoons in the family’s flatware drawer are plain. In the fight against evil clones, food may be our greatest weapon |John Kelly |February 8, 2021 |Washington Post 

Unlike other articles of clothing, you can’t simply throw a belt in the dirty clothes at the end of the day—and getting them to stay rolled up in a drawer when not in use is nearly impossible. The Best Belt Racks To Keep Your Closet Organized |PopSci Commerce Team |January 22, 2021 |Popular-Science 

Look for treasures in roll-top desks, junk drawers, frames and freezers, as well as taped to toilet lids. How downsizing expert Matt Paxton helps Americans sort through their stuff |Jura Koncius |January 21, 2021 |Washington Post 

I get the bottle while he opens a desk drawer containing two glasses. Alfred Hitchcock’s Fade to Black: The Great Director’s Final Days |David Freeman |December 13, 2014 |DAILY BEAST 

And who else would let them gather dust in some drawer for nearly 50 years? Elvis Costello, Marcus Mumford, and Others Crowdsource A Dylan Album |Malcolm Jones |November 16, 2014 |DAILY BEAST 

But then I had to go back to earning real money and went to work for ‬Businessweek‪ and put the book in a drawer for a long time. How The Cold War Endgame Played Out In The Rubble Of The Berlin Wall |William O’Connor |November 9, 2014 |DAILY BEAST 

On the day when he pulled open the drawer to show me the gun, I wonder, what I was thinking? Don Carpenter Was a Novelist Both Lacerating and Forgiving |Louis B. Jones |July 14, 2014 |DAILY BEAST 

The gun Don used, he kept in the drawer of his writing table at the window, where he always worked. Don Carpenter Was a Novelist Both Lacerating and Forgiving |Louis B. Jones |July 14, 2014 |DAILY BEAST 

"I have a letter somewhere," looking in the machine drawer and finding the letter in the bottom of the workbasket. The Awakening and Selected Short Stories |Kate Chopin 

The letter was right there at hand in the drawer of the little table upon which Edna had just placed her coffee cup. The Awakening and Selected Short Stories |Kate Chopin 

Mademoiselle smoothed the letter out, restored it to the envelope, and replaced it in the table drawer. The Awakening and Selected Short Stories |Kate Chopin 

If the holder has been free from wrong in presenting the check, the bank cannot look to him, but to the drawer for repayment. Putnam's Handy Law Book for the Layman |Albert Sidney Bolles 

She had opened the drawer of her deskthe top right-hand drawerand was fumbling in it. The Girls of Central High on the Stage |Gertrude W. Morrison