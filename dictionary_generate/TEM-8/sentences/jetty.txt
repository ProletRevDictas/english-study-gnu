Carlsbad and its Tamarack State Beach has its own jetty to trap sand there, the Coast News reported. North County Report: Oceanside Moves Ahead on Groins Without Mayor’s Support |Kayla Jimenez |August 18, 2021 |Voice of San Diego 

The other options would be building two artificial reefs, or extending an existing jetty or continuing to slap sand on the beach. Environment Report: Oceanside Looking to Grow a Groin or Two |MacKenzie Elmer |August 2, 2021 |Voice of San Diego 

There are a few options on the table, like building jetties or an artificial reef designed to keep sand near the coast. Oceanside Seawall Dispute Hints at Looming Decisions Over Sea-Level Rise |MacKenzie Elmer |June 17, 2021 |Voice of San Diego 

The bulk of recreational diving for Florida lobsters is done in the Florida Keys, where spiny lobsters are found under small rocky shelves, near reefs, and around jetties. 14 wild edibles you can pull right out of the ocean |By Bob McNally/Field & Stream |October 19, 2020 |Popular-Science 

The Daily Pic: Robert Smithson found spirals before his "Jetty". The Spirit of Land Art |Blake Gopnik |August 22, 2012 |DAILY BEAST 

The mask also points to how early Smithson came to the main motif of his most famous work, the “Spiral Jetty” earthwork from 1970. The Spirit of Land Art |Blake Gopnik |August 22, 2012 |DAILY BEAST 

When the waves reached their violent peak, a BUD/S instructor standing on top of the jetty would signal with his flashlight. Inside Seal Team Six by Don Mann Excerpt |Don Mann |December 4, 2011 |DAILY BEAST 

From these homes one can see the lighthouse, the jetty, some small islands, but nothing more. Dinner Under the Midnight Sun |Sophie Menin |June 26, 2010 |DAILY BEAST 

It also authorised the construction and maintenance, as p. 150part of such railways, of any pier, quay or jetty. Fifty Years of Railway Life in England, Scotland and Ireland |Joseph Tatlow 

The half-dozen serviceable boats were gathered a couple of hundred yards away about a short wooden jetty. Mushroom Town |Oliver Onions 

The tide was creaming over the short thumb of a jetty, and the herd of small black cows was patrolling the beach. Mushroom Town |Oliver Onions 

The boats by the short thumb of a jetty had not been used for a week, and lay high up the beach. Mushroom Town |Oliver Onions 

Not only has this town no fort, but it has not even a jetty. Celebrated Travels and Travellers |Jules Verne