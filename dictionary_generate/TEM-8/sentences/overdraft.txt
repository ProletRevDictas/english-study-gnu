Banks could simply decline a charge if a customer lacked the funds, but instead lenders promote “overdraft protection” as a convenience that comes at a cost. JPMorgan Chase Bank Wrongly Charged 170,000 Customers Overdraft Fees. Federal Regulators Refused to Penalize It. |by Patrick Rucker, The Capitol Forum |December 14, 2020 |ProPublica 

Zions Bank customers also could get charged many overdraft fees for being just a few bucks short, examiners agreed. JPMorgan Chase Bank Wrongly Charged 170,000 Customers Overdraft Fees. Federal Regulators Refused to Penalize It. |by Patrick Rucker, The Capitol Forum |December 14, 2020 |ProPublica 

The suggested solutions range from targeting overdraft fee abuse to data use reform to public banking to improved education on the current financial system—and everything in between. What the unbanked need from the 2020 election |McKenna Moore |September 30, 2020 |Fortune 

Situations like this often result in crippling overdraft fees that turn swaths away from formal banking altogether. What the unbanked need from the 2020 election |McKenna Moore |September 30, 2020 |Fortune 

Chime provides banking services through a mobile app including no-fee accounts, debit cards, paycheck advances, and no overdraft fees. This fintech is now more valuable than Robinhood |Anne Sraders |September 18, 2020 |Fortune 

Those overdraft fees could be absolutely disastrous for poor people who didn't keep their balances straight. Did Financial Services Reform Inadvertently Put a Kink In Obamacare? |Megan McArdle |May 23, 2013 |DAILY BEAST 

He owes creditors £32,575 and public accounts state: “The bank overdraft is secured by a member of the director's family.” James Middleton's Company Loses Cash |Tom Sykes |March 1, 2013 |DAILY BEAST 

He suggested that the sum in question was related to making good an overdraft at JPMorgan in London. Jon Corzine Can’t Answer $1.2 Billion Question About MF Global at Hearing |Michael Daly |December 16, 2011 |DAILY BEAST 

The queen mother was said to have died with an overdraft at Coutts of more than £2 million; it was settled by her daughter. Juiciest Bits From the Royal Exposé |Tom Sykes |October 31, 2011 |DAILY BEAST 

So far, just being me has taken me to a 250-square-foot apartment and the overdraft protection black list. Get Famous or Your Money Back |Jessie Rosen |June 18, 2009 |DAILY BEAST 

The poor man has none of these, and therefore cannot obtain that overdraft which is one of the first essentials of bankruptcy. The Law and the Poor |Edward Abbott Parry 

But don't pretend that you have a thousand dollars in bank when you hold in your hands the statement of your overdraft. The Young Man and the World |Albert J. Beveridge 

It was the first relief from his misery the poor man had felt since he had read the letter about the overdraft in the morning. The Little City Of Hope |F. Marion Crawford 

If Overholt would kindly sign a note at sixty days for the overdraft it would be all right. The Little City Of Hope |F. Marion Crawford 

"All you have to do is to affix a half dozen ciphers to the remainder before you start the overdraft," said Jack. Jack and the Check Book |John Kendrick Bangs