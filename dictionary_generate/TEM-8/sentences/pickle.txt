Over the past six months, her business selling small-batch pickles, jams and radishes has tripled its sales. Small Business Saturday spotlights pandemic-inspired entrepreneurship |Emily Davies |November 28, 2020 |Washington Post 

Scientists have known that sea pickles have special cells called photosites, located as spots around their bodies, that allow them to glow. We finally know why sea pickles glow |María Paula Rubiano A. |October 22, 2020 |Popular-Science 

In the promotional video, Balvin says his preference is for “no pickles,” which sort of defeats the purpose of a Big Mac, but hey, if you want to consume the exact same food as J Balvin, you gotta make some sacrifices. McDonald’s Partners With J Balvin Following the Huge Success of the Travis Scott Meal |Jaya Saxena |October 6, 2020 |Eater 

It would be a very unpleasant chore, and again, that’s the pickle you’re trying to present. The most secure ways to lock up your bike |By Michael Frank/Cycle Volta |August 26, 2020 |Popular-Science 

Then, you’ll find yourself in a pickle if more than one blogger accepts it. Nine mistakes to avoid when contacting websites for backlinks |Raj Dosanjh |July 29, 2020 |Search Engine Watch 

You spice it with blues and skiffle music, and pickle it in alcohol and tobacco smoke. The Greatest Rock Voice of All Time Belonged to Joe Cocker |Ted Gioia |December 23, 2014 |DAILY BEAST 

Our backyard had a baseball diamond and a “Pickle” path worn into the lawn because of Frankie. ‘Tracing the Blue Light’: Read Chapter 1 of Eileen Cronin’s ‘Mermaid’ |Eileen Cronin |April 8, 2014 |DAILY BEAST 

A pinch hitter named Pickle Smith was announced for Jacksonville. The Great Paul Hemphill Celebrates the Long Gone Birmingham Barons |Paul Hemphill |March 29, 2014 |DAILY BEAST 

Chuck Strickler of Decatur, Michigan, found himself in a pickle right after September 11. New iPhone a Problem for People Who Lack Fingerprints |Winston Ross |September 12, 2013 |DAILY BEAST 

But soon after I arrived in Washington, D.C., I was in a pickle. Michelle Rhee: My Break With the Democrats |Michelle Rhee |February 5, 2013 |DAILY BEAST 

Now one is told he is somewhat of a pickle, but fables about royalty may always be received with more than a grain of salt. Spanish Life in Town and Country |L. Higgin and Eugne E. Street 

Pit your shovel in here an' lift this pickle, an' no' stand there gapin' like a grisly ghost at the door o' hell! The Underworld |James C. Welsh 

Very true,” said Edward; “we have nothing to conceal, and if he finds us in a pickle it is of no consequence. The Children of the New Forest |Captain Marryat 

Then when it had become sapless and hard, he cut it to shape, then “put it 178 to pickle,” as the saying goes. McClure's Magazine, Vol. 1, No. 2, July, 1893 |Various 

Because she got into a pretty pickle—there's a riddle for you. Frank Fairlegh |Frank E. Smedley