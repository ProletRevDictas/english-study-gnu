Media companies big and small have been on the hunt for direct consumer revenue as the Facebook-infused traffic era and “pivot to video” came to a close. ‘There’s no antagonist’: News outlets mull the possible end of their editorial and business-side ‘Trump Bump’ bonanza |Steven Perlberg |August 10, 2020 |Digiday 

Morning Brew expects to generate $20 million in revenue for 2020. ‘Let the buyers know you exist’: How Morning Brew plans to grow brand ad dollars from its base of direct response |Max Willens |August 5, 2020 |Digiday 

Marie Claire generated as much revenue from e-commerce in the first seven months of 2020 than for all of 2019, she said. ‘No brainer’: Marie Claire launches sampling business to boost revenue and data practice |Lucinda Southern |August 3, 2020 |Digiday 

The news and politics publisher Salon is likely to close out June and July with its revenues up year over year for both months, thanks to weeks of sustained highs in CPMs for its site inventory and RPM for its pages. ‘The sky hasn’t fallen’: Publishers’ programmatic revenues vault back up in June and July |Max Willens |July 30, 2020 |Digiday 

In 2019, annual revenue grew by 50%, so far this year ad revenue has grown by over 30% compared with the first six months of last year, the company said. Investments in journalism, algorithms has Axel Springer’s five-year-old aggregator app Upday up over 30% in ad revenue |Lucinda Southern |July 30, 2020 |Digiday 

Yes, publicizing tragedy gets clicks, gets ad revenue, gets notoriety, and can be done for all the wrong reasons. Cover-Ups and Concern Trolls: Actually, It's About Ethics in Suicide Journalism |Arthur Chu |January 3, 2015 |DAILY BEAST 

It not only generates revenue, but it decimates communities. ISIS, Boko Haram, and the Growing Role of Human Trafficking in 21st Century Terrorism |Louise I. Shelley |December 26, 2014 |DAILY BEAST 

In their effort to diversify their revenue, they have capitalized on traditional practices to new advantage. ISIS, Boko Haram, and the Growing Role of Human Trafficking in 21st Century Terrorism |Louise I. Shelley |December 26, 2014 |DAILY BEAST 

The Cuban government siphons off revenue from nearly every business transaction in the country. Obama’s One Hand Clap With Castro |Doug McIntyre |December 24, 2014 |DAILY BEAST 

Just wanted to place it in the context of slates needing picture choices that throw off revenue to make the numbers work. Inside Sony’s ‘Pineapple Express 2 Drama’: Leaked Emails Reveal Fight Over Stoner Comedy Sequel |William Boot |December 21, 2014 |DAILY BEAST 

The seed of the Nile in many waters, the harvest of the river is her revenue: and she is become the mart of the nations. The Bible, Douay-Rheims Version |Various 

All this was but an incident, to be sure, in the minister's general scheme for "ameliorating the revenue." The Eve of the Revolution |Carl Becker 

It may be that the Spanish authorities regarded the West Indian trade as a commercial system rather than as a means of revenue. The Eve of the Revolution |Carl Becker 

The revenue fox such a civil list would naturally be raised in America. The Eve of the Revolution |Carl Becker 

Trade and commerce drooped daily, and the revenue melted away rapidly every year. Blackwood's Edinburgh Magazine, No. CCCXXXIX. January, 1844. Vol. LV. |Various