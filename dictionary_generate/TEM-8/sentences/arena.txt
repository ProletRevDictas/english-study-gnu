Because of the pandemic, the former president will likely be unable to have the spectacular arena tour that Michelle Obama had, what was then an unprecedented launch for a political book. The first volume of Barack Obama’s long-awaited memoir finally has a release date |Rachel King |September 17, 2020 |Fortune 

More Than a Vote has been working with sports teams to make giant arenas available as voting venues. Trump contradicts CDC director on vaccine; Biden says Americans shouldn’t trust Trump |Colby Itkowitz, Felicia Sonmez, John Wagner |September 16, 2020 |Washington Post 

Unlike local titans Progressive Insurance and Sherwin Williams, TransDigm hasn’t sponsored any of the city’s sports arenas. The Big Corporate Rescue and the America That’s Too Small to Save |by Lydia DePillis, Justin Elliott and Paul Kiel |September 12, 2020 |ProPublica 

The league barred media from locker rooms and publicly considered having teams play to empty arenas before ultimately stopping play altogether. How the coronavirus outbreak is roiling the film and entertainment industries |Alissa Wilkinson |September 11, 2020 |Vox 

The team puffed six of these scents into arenas with lone locusts. A single chemical may draw lonely locusts into a hungry swarm |Jonathan Lambert |September 7, 2020 |Science News For Students 

This is a growing business arena, a market attracting lots of players. Your Local School Doesn’t Have to Suck |Michael S. Roth |December 17, 2014 |DAILY BEAST 

The traffic agent was kind enough to direct Sarah discreetly to a closed-off street running behind the arena. Synagogue Slay: When Cops Have to Kill |Michael Daly |December 10, 2014 |DAILY BEAST 

Were it not for them, nobody would have even built the brand new arena where Sarah saw a PRINCESS! Synagogue Slay: When Cops Have to Kill |Michael Daly |December 10, 2014 |DAILY BEAST 

These are cultural questions, not political ones, and they have to be solved in the cultural arena. Dear Evangelicals: You’re Being Had |Jay Michaelson |November 30, 2014 |DAILY BEAST 

We worked in the cultural arena instead, with pioneers like Ellen and Will & Grace. Dear Evangelicals: You’re Being Had |Jay Michaelson |November 30, 2014 |DAILY BEAST 

I am on fire to throw myself into the arena—however, there will be opportunities to make myself known and felt. Ancestors |Gertrude Atherton 

At Arles we witness a great combat of animals, in which the lion of Arles, along with four bulls, is turned loose in the arena. Frdric Mistral |Charles Alfred Downer 

A small palm tree was set in the midst of the arena,—the trunk bronze, the leaves one sheen of gold-foil. God Wills It! |William Stearns Davis 

Crossing the arena, one of the men carelessly hit at a bird turning wildly about in its efforts to escape, and killed it. Spanish Life in Town and Country |L. Higgin and Eugne E. Street 

He was tethered in the centre of the arena, by one of his hind legs, to a stump about twelve inches high. Spanish Life in Town and Country |L. Higgin and Eugne E. Street