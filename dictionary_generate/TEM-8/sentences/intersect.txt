Look for tangentially related topics to your business, where the search volumes might not be as high, but the topic still intersects with your audience and can bring relevant visitors. Search competition: Who are you really competing with? |Andrew Dennis |July 29, 2020 |Search Engine Land 

What they really wanted to know was whether every rotation of the Möbius strip intersects the original copy. New Geometric Perspective Cracks Old Problem About Rectangles |Kevin Hartnett |June 25, 2020 |Quanta Magazine 

In four-dimensional space, it is possible to embed the Klein bottle so that it doesn’t intersect itself. New Geometric Perspective Cracks Old Problem About Rectangles |Kevin Hartnett |June 25, 2020 |Quanta Magazine 

In 2015 that discrimination intersected with a rapidly spreading disease, during the Middle East respiratory syndrome outbreak. Tracing Homophobia In South Korea’s Coronavirus Surveillance Program |LGBTQ-Editor |June 18, 2020 |No Straight News 

It was surrounded by walls made of large tufa blocks, while being intersected by two main streets. Ostia Antica: Reconstruction and History of The Harbor City of Ancient Rome |Dattatreya Mandal |April 14, 2020 |Realm of History 

Their lives are falling apart, but they intersect in interesting, tragic, and instructive ways. Ted Thompson’s Debut Novel Features A 1 Percenter As Its Hero |Stefan Beck |May 6, 2014 |DAILY BEAST 

These poems exist in the place where human invention and logic intersect. Why Billy Collins Is America’s Most Popular Poet |Austen Rosenfeld |October 22, 2013 |DAILY BEAST 

And, of course, stories are handed down in the family of women whose lives intersect with the historical figures in the novel. Colum McCann Talks New Novel ‘TransAtlantic’ and Narrative4 |Phil Klay |June 14, 2013 |DAILY BEAST 

Within that broader system, different forms of discrimination intersect, feed off of, and reinforce each other. Women of the Wall's Opposition Lifts from the Settler Playbook |Sigal Samuel |May 21, 2013 |DAILY BEAST 

The estuary where religion and politics intersect is constantly changing. America’s Catholic Moment, and Its New Breed of Catholic Politicians |Michael Sean Winters |March 19, 2013 |DAILY BEAST 

They are for the most part straight, and intersect each other at approximate right angles. The Catacombs of Rome |William Henry Withrow 

(e) No part of the counter shall intersect a triangle or the produced perpendicular thereof shown on p. 186. Yachting Vol. 2 |Various. 

On the higher ridges which intersect the coast at short distances from the sea, the potatoe grows wild. Travels in Peru, on the Coast, in the Sierra, Across the Cordilleras and the Andes, into the Primeval Forests |J. J. von Tschudi 

They resemble "two circles joined together so as to intersect one another slightly," or "a long oval pinched in at the middle." Homer and His Age |Andrew Lang 

It is only where rivers intersect the plain that oases of luxuriant vegetation are formed. Travels in Peru, on the Coast, in the Sierra, Across the Cordilleras and the Andes, into the Primeval Forests |J. J. von Tschudi