We tend to get distracted by utopian visions or by a cynical fatalism that says, “Let’s just burn the whole thing down because nothing can improve it.” Steven Pinker Has His Reasons - Issue 108: Change |Brian Gallagher |November 17, 2021 |Nautilus 

We’re in even deeper trouble if healthy cynicism tips into lazy fatalism. Why We Shouldn't Write Off COP26 Before It's Even Started |Paul Polman |October 27, 2021 |Time 

Seasons upon seasons of coming up short can imbue a sense of fatalism in even the most optimistic supporters. This Year’s NBA Champion Will Bring A Title To A Cursed Sports City |Santul Nerkar (santul.nerkar@abc.com) |July 1, 2021 |FiveThirtyEight 

Keong Sim narrates the book in a serene, resonant voice, capturing the author’s wry humor and feelings of awe and fatalism. 3 great new audiobooks for your drive, your walk, your laundry folding ... |Katherine A. Powers |June 25, 2021 |Washington Post 

Rather than sparking inspiration, it speaks of blatant fatalism about what is worth saving, a preference for the lofty and unpopulated … with delusions of innovation and heroism. Why We Should Be Spending More on Space Travel |Jeffrey Kluger |April 12, 2021 |Time 

The dire fatalism that dominated the discourse then is gone, replaced largely with a practiced apathy. Heart of Darkness: Into Afghanistan’s Taliban Valley |Matt Trevithick, Daniel Seckman |November 15, 2014 |DAILY BEAST 

The Middle East was the source of an uncharacteristic vibe at Davos: fatalism. Political Tensions Takes Center Stage at World Economic Forum |Daniel Gross |January 27, 2013 |DAILY BEAST 

This is in marked contrast to the fatalism you see in places like Russia and China, where partisanship is frowned upon. Why Partisan Bickering Works |Tunku Varadarajan |February 23, 2010 |DAILY BEAST 

It had had, he owned, its temporary value, as the necessary rebellion against fatalism and immobility and privilege. The Life of Mazzini |Bolton King 

Jarvis discarded his fatalism, as he caught at this loophole. The Ghost Breaker |Charles Goddard 

No fatalism is long proof against the call of love and June. The Secret Witness |George Gibbs 

"The roses of Konopisht," he muttered, thinking of Marishka's fatalism. The Secret Witness |George Gibbs 

He is accused of a leaning to fatalism, which he heartily denied, but which seems to follow from his logical conclusions. Beacon Lights of History, Volume VI |John Lord