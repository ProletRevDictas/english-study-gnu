Will not one puff of that narcotic breath drowse deep all watching dragons, and make for him the sleeping beauties of his will? Tobacco; Its History, Varieties, Culture, Manufacture and Commerce |E. R. Billings. 

Slowly before the drowse of darkness, the noises quieted and the fires died down. The Way of an Indian |Frederic Remington 

It is as if all these years I had been in a drowse in my mind, and had suddenly sprung up throbbingly awake. The Diary of a Saint |Arlo Bates 

And it may be, as they sit there and drowse and dream, that the Hollow Tree People creep up close and watch them. The Hollow Tree Snowed-In |Albert Bigelow Paine 

Her rambling drowse naturally brought back the whole trip to Hillsborough and her conversation with Bertha. The Incendiary |W. A. (William Augustine) Leahy