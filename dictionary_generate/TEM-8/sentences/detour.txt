Sure, an IPO is usually regarded as a road to riches, but there’s many a speed bump, detour, and dead-end on that hoped-for path to bundles of cash — especially in the presently buoyant ad tech land. Cheatsheet: PubMatic plots path to growth as a public ad tech vendor |Seb Joseph |December 10, 2020 |Digiday 

So Mankiewicz’s decision to target Hearst was shocking, and the explanation for it requires a detour into Mankiewicz’s long and winding biography. The tangled history behind David Fincher’s Mank — and its link to Citizen Kane — explained |Alissa Wilkinson |December 4, 2020 |Vox 

She said a detour was set up around the area, but traffic was backed up for about three miles. I-81 closed near Harrisonburg after truck strikes overpass |Dana Hedgpeth |November 12, 2020 |Washington Post 

If you’re planning on attending or booking a separate trip, keep these adventure detours in mind. 27 Epic Trips to Start Planning Now |The Editors |September 8, 2020 |Outside Online 

In order to see why, we need to take a brief detour through the philosophy of mathematics. Animals That Can Do Math Understand More Language Than We Think |Erik Nelson |June 14, 2020 |Singularity Hub 

But this is also a parody of narcissism, just a little detour to eternity. Excuse Me For Not Dying: Leonard Cohen at 80 |David Yaffe |September 24, 2014 |DAILY BEAST 

The ice cream remains reason enough to detour off I-84 for a visit to this mid-20th century gem. The Real Cheeseburger Paradise |Jane & Michael Stern |June 22, 2014 |DAILY BEAST 

I will detour for a moment because this where I often see interviewers and pundits roll their eyes. Putin’s Sochi and Hitler’s Berlin: The Love Affair Between Dictators and the Olympic Games. |Garry Kasparov |February 7, 2014 |DAILY BEAST 

On the way to the West Bank, perhaps you could take a detour to visit some of the African neighborhoods in Tel Aviv. Alicia Keys: Come Visit Palestine |Maysoon Zayid |June 11, 2013 |DAILY BEAST 

I knew what I wanted to do, and college just felt like a detour. Shoshanna No More: Zosia Mamet of ‘Girls’ On Her New Off-Broadway Play |Janice Kaplan |February 17, 2013 |DAILY BEAST 

Leaving the main road a detour of a few miles enabled us to visit Crowland Abbey shortly before reaching Peterborough. British Highways And Byways From A Motor Car |Thomas D. Murphy 

Had we known of this at the time, a short detour would have taken us through its quaint streets. British Highways And Byways From A Motor Car |Thomas D. Murphy 

The much-desired inn soon appeared, but, to the surprise of Gorenflot, Chicot caused him to make a detour and pass round the back. Chicot the Jester |Alexandre Dumas, Pere 

Three armed with Winchesters made a long detour and dropped quietly into the sage-brush just beyond accurate pistol-range. Blazed Trail Stories |Stewart Edward White 

The enemy opened fire without delay, so the Yeomanry had to make a wide detour. The Relief of Mafeking |Filson Young