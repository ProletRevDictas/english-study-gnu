One of the biggest compliments I can have as a drummer is that someone is dancing to you. Charlie Watts, Rolling Stones drummer and band’s rhythmic mainstay, dies at 80 |Matt Schudel |August 24, 2021 |Washington Post 

He said the hugs and compliments he gave her were friendly and told investigators he’d had similar conversations with students about their sex lives before. Student Who Reported CSUSM Professor Says No One Told Her He Kept His Job |Kayla Jimenez |August 10, 2021 |Voice of San Diego 

Today, the word is frequently used with more nuance and intrigue, and some even use it as a compliment. Stop calling food ‘exotic’ |Daniela Galarza |July 8, 2021 |Washington Post 

He hugged them for too long, pressured them into private meetings in his office and made unwanted compliments about their appearance, records show. CSU San Marcos Tried to Fire a Professor for Misconduct – Then His Union Intervened |Kayla Jimenez |May 25, 2021 |Voice of San Diego 

In January, Elton John paid him one of the highest compliments imaginable, saying, “you’re going to be one of the biggest artists in the whole wide world.” Who Is The Kid Laroi? What to Know About the 17-Year-Old's Meteoric Rise |Andrew R. Chow |May 6, 2021 |Time 

For many men, being depicted by Channing Tatum in a film about your life would be a compliment. Wrestler Mark Schultz Hates the ‘Sickening and Insulting Lies’ of ‘Foxcatcher’ |Rich Goldstein |December 31, 2014 |DAILY BEAST 

Hawke, ever the charmer, kicks things off with a compliment: I really like you guys. Coffee Talk with Ethan Hawke: On ‘Boyhood,’ Jennifer Lawrence, and Bill Clinton’s Urinal Exchange |Marlow Stern |December 27, 2014 |DAILY BEAST 

People compliment me on Boyhood sometimes, and I think, “Well, it better have been good!” Coffee Talk with Ethan Hawke: On ‘Boyhood,’ Jennifer Lawrence, and Bill Clinton’s Urinal Exchange |Marlow Stern |December 27, 2014 |DAILY BEAST 

And the criticism is always poorly packaged as concern or some sad excuse for a compliment. How to Make It Through Thanksgiving Alive |Lizzie Crocker |November 26, 2014 |DAILY BEAST 

To be the very first moment that we see on an episode of The Good Wife was quite a compliment and very humbling. How Carrie Preston Became The Good Wife’s Favorite Scene Stealer |Kevin Fallon |October 20, 2014 |DAILY BEAST 

“To say that you would have more sense than the police, would be a poor compliment,” said the old lady. The Joyous Adventures of Aristide Pujol |William J. Locke 

The court paid him the high compliment of refusing his suit, declaring that he had himself inflicted sufficient punishment. The Book of Anecdotes and Budget of Fun; |Various 

It was a pretty compliment, and sincere I knew, for no one could meet him without recognising his frank outspoken nature. Fifty Years of Railway Life in England, Scotland and Ireland |Joseph Tatlow 

Marius frowned darkly, but before he could speak, Tressan was insinuating a compliment to the Marquise. St. Martin's Summer |Rafael Sabatini 

"And I can return the compliment," was my reply, as we all gathered round a brew of tea to exchange news and compare notes. Three More John Silence Stories |Algernon Blackwood