While Heather felt uneasy, she and Heidi decided to move forward with the service, followed by a reception at a nearby tavern. ‘I said goodbye to my sister through a computer screen’ |Holly Bailey |January 2, 2021 |Washington Post 

A 61-year-old man who’s been staying at the Convention Center since April said he and others have been feeling uneasy and were unsettled to see staff replaced and beds removed. Morning Report: The Coronavirus Is Ravaging the Convention Center Shelter |Voice of San Diego |December 18, 2020 |Voice of San Diego 

He said this week that he and others there have been uneasy about what might come next. The Convention Center Coronavirus Outbreak Was Inevitable |Lisa Halverstadt |December 17, 2020 |Voice of San Diego 

These are also awkward, especially as the controller rumbles and stiff, first-person camera angles make for an uneasy experience. ‘Cyberpunk 2077’ is a thrill ride through an ugly, unexamined world |Elise Favis |December 11, 2020 |Washington Post 

Despite the upheaval the coronavirus pandemic has brought, uneasy times also set a course for innovation. The 100 greatest innovations of 2020 |Popular Science Staff |December 2, 2020 |Popular-Science 

I have to admit that while I was watching this, I was cheering her on, but with a little uneasiness. Is Bigger Better for St. Vincent? |David Yaffe |December 4, 2014 |DAILY BEAST 

There was, in fact, no more uneasiness about the future of Baltimore at the time he killed himself. The Stacks: H.L. Mencken on the 1904 Baltimore Fire |H.L. Mencken |October 4, 2014 |DAILY BEAST 

There was only one thing which troubled me, I laughed at my own uneasiness, and yet it troubled me. Read ‘The King in Yellow,’ the ‘True Detective’ Reference That’s the Key to the Show |Robert W. Chambers |February 20, 2014 |DAILY BEAST 

So I was trying to reflect that and not avoid the uneasiness inherent in the depictions of different existences. Susan Minot on Africa, Joseph Kony, and the Limits of Writing About Love |Lea Carpenter |February 10, 2014 |DAILY BEAST 

More than anything, they reflect our uneasiness with the modern world, its complexity, and often its capriciousness. Enough Already: Syria Wasn’t a False-Flag Operation |Jamelle Bouie |September 10, 2013 |DAILY BEAST 

Each felt, it seemed, a slight uneasiness, as though of trouble coming that was yet not entirely atmospherical. The Wave |Algernon Blackwood 

Do the public funds exhibit the slightest symptoms of uneasiness or excitement? Blackwood's Edinburgh Magazine, No. CCCXXXIX. January, 1844. Vol. LV. |Various 

With well-trained waiters, you need give yourself no uneasiness about the arrangements outside of the parlors. The Ladies' Book of Etiquette, and Manual of Politeness |Florence Hartley 

The lad's uneasiness showed itself, but when they went back to the hotel about the supper hour Winston smiled at him. Winston of the Prairie |Harold Bindloss 

We set them down as pirates, and awaited the upshot with a considerable degree of uneasiness. A Woman's Journey Round the World |Ida Pfeiffer