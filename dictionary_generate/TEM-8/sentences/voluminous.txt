In my youth, I carried “The Magic Mountain,” Mann’s voluminous 1924 novel, into cafes. The life of this scribe is a real page-turner |Kathi Wolfe |September 30, 2021 |Washington Blade 

There are 79, believe it or not— 79 different files on different members of the Kuczynski family, including a pretty voluminous set on Ursula herself. The Mom Who Stole the Blueprints for the Atomic Bomb (The Freakonomics Radio Book Club Ep. 11) |Sarah Lyall |September 25, 2021 |Freakonomics 

Star RB Alvin Kamara will also be sure to get his, and there will be other mouths to feed in a New Orleans passing game likely to be less voluminous than it was with Brees. The fantasy football do-not-draft list for 2021 starts with Derrick Henry and Nick Chubb |Des Bieler |August 31, 2021 |Washington Post 

With multiple sizes, this model creates classic, bouncy, and voluminous curls. Best curling iron: Hair styling tools to create the curls you seek no matter your hair type |Irena Collaku |August 12, 2021 |Popular-Science 

For this album’s rollout, she has chosen an elegant, muted approach in which she sports voluminous blonde hair, plush fabrics and lies across Persian rugs. 4 Takeaways From Billie Eilish's New Album Happier Than Ever |Andrew R. Chow |July 30, 2021 |Time 

His papers are voluminous but arid, nothing of the man comes through. When New York City Hit Its Stride |Allen Barra |July 17, 2014 |DAILY BEAST 

But the biographical inattention to his voluminous body of written work nonetheless has been a strange oversight. Churchill Would Be Famous Today on the Strength of His Writing Alone |Anthony Paletta |June 16, 2014 |DAILY BEAST 

The singer covered parts of her naked body with moss, accessorizing with smokey eyeshadow and a voluminous haircut. Miranda Kerr Reportedly Won’t Walk in This Year’s Victoria’s Secret Fashion Show; Gucci Receives $144.2 Million in Counterfeit Damages |The Fashion Beast Team |October 18, 2013 |DAILY BEAST 

Consider one bold, green dress with a voluminous skirt that shimmered with what looked like the green scales of an alligator. Bottega Veneta Spring/Summer 2014: Palazzo Fashion |Liza Foreman |September 21, 2013 |DAILY BEAST 

Eventually I got to see the voluminous police dossier on the case, including the photographs taken at the scene by the paparazzi. The Night Princess Diana Died |Christopher Dickey |August 31, 2013 |DAILY BEAST 

Frederick Spanheim died; a noted divinity professor at Leyden, and a voluminous writer. The Every Day Book of History and Chronology |Joel Munsell 

Nathan Drake, an English physician, died; also a highly respectable and voluminous author. The Every Day Book of History and Chronology |Joel Munsell 

But the man inside these voluminous clothes is even still more eccentric. The Real Latin Quarter |F. Berkeley Smith 

He was one of the most voluminous writers of antiquity, and probably is the most learned man whose writings have come down to us. Beacon Lights of History, Volume I |John Lord 

His writings were very voluminous, and in his tranquil garden he led a peaceful life of study and enjoyment. Beacon Lights of History, Volume I |John Lord