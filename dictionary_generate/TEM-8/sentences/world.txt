With fires burning in so many disparate corners of the world, one might assume that wildfires have been growing every year. What wildfires in Brazil, Siberia, and the US West have in common |Lili Pike |September 17, 2020 |Vox 

Democratic-run New Jersey and New York, with death rates of 181 and 168, respectively, would lead the entire world. Trump says US Covid-19 deaths would be low if you excluded blue states. That’s wrong. |German Lopez |September 17, 2020 |Vox 

This makes TikTok the seventh most popular social media platform in the world. What you must know about TikTok for business |Connie Benton |September 17, 2020 |Search Engine Watch 

Coastal wetlands have been shrinking around the world for the last century. Soggy coastal soils? Here’s why ecologists love them |Alison Pearce Stevens |September 17, 2020 |Science News For Students 

It housed humans for 40,000 years while our species grew and grew around the world. The Environmental Headache in Your Shampoo - Issue 90: Something Green |Anastasia Bendebury & Michael Shilo DeLay |September 16, 2020 |Nautilus 

The world that Black Dynamite lives in is not the most PC place to be in. ‘Black Dynamite’ Presents Police Brutality: The Musical |Stereo Williams |January 9, 2015 |DAILY BEAST 

Have a look at this telling research from Pew on blasphemy and apostasy laws around the world. In Defense of Blasphemy |Michael Tomasky |January 9, 2015 |DAILY BEAST 

Allegations of transphobia are not new in the world of gay online dating. Grindr’s Trans Dating Problem |David Levesley |January 9, 2015 |DAILY BEAST 

People watch night soaps because the genre allows them to believe in a world where people just react off their baser instincts. ‘Empire’ Review: Hip-Hop Musical Chairs with an Insane Soap Opera Twist |Judnick Mayard |January 8, 2015 |DAILY BEAST 

Editorial and political cartoon pages from throughout the world almost unanimously came to the same conclusion. Politicians Only Love Journalists When They're Dead |Luke O’Neil |January 8, 2015 |DAILY BEAST 

Descending the Alps to the east or south into Piedmont, a new world lies around and before you. Glances at Europe |Horace Greeley 

All over the world the just claims of organized labor are intermingled with the underground conspiracy of social revolution. The Unsolved Riddle of Social Justice |Stephen Leacock 

There seems something in that also which I could spare only very reluctantly from a new Bible in the world. The Salvaging Of Civilisation |H. G. (Herbert George) Wells 

That it is a reasonable and proper thing to ask our statesmen and politicians: what is going to happen to the world? The Salvaging Of Civilisation |H. G. (Herbert George) Wells 

The "new world" was really found in the wonder-years of the eighteenth and early nineteenth centuries. The Unsolved Riddle of Social Justice |Stephen Leacock