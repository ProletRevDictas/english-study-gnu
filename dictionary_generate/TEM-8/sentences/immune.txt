To get the US to herd immunity — when enough of the US population is immune so the virus no longer poses a major threat — by the end of the summer, America likely needs to hit an average of 2 million or 3 million doses a day. Good news about America’s Covid-19 vaccine campaign |German Lopez |February 12, 2021 |Vox 

Someone who has had the vaccine and is immune to the virus may still test negative for certain antibodies, Murphy explained. Why you shouldn’t get a covid antibody test after your vaccine |Lindsey Bever |February 12, 2021 |Washington Post 

At the time, this was really about Prodigy and AOL, but no one really cared, because no one really thought very much about what the impact would be of making Prodigy immune from tort lawsuits. Twenty-Six Words Created the Internet. What Will It Take to Save It? |Stephen Engelberg |February 9, 2021 |ProPublica 

Republicans are the most likely to say that they don’t plan to be vaccinated against the virus, a key step toward achieving saturation of immune individuals and therefore protecting the country broadly. What you’re saying when you say that covering the coronavirus is partisan |Philip Bump |February 9, 2021 |Washington Post 

Members of a household, neighbors, or people living in long-term care facilities may be able to share the company of others who are also immune. Covid-19 vaccines are great — but you still need to wear a mask for now |Umair Irfan |February 9, 2021 |Vox 

With enough changing of the influenza RNA over time, the vaccine no longer provokes the “right” immune response. When You Get the Flu This Winter, You Can Blame Anti-Vaxxers |Kent Sepkowitz |January 1, 2015 |DAILY BEAST 

So too with a vaccine that provokes a specific immune response aimed at a specific RNA sequence. When You Get the Flu This Winter, You Can Blame Anti-Vaxxers |Kent Sepkowitz |January 1, 2015 |DAILY BEAST 

As a Washington attorney, he took on companies that seemed immune to change, even when they were ineffective. Your Local School Doesn’t Have to Suck |Michael S. Roth |December 17, 2014 |DAILY BEAST 

Their captors wore palm leaves, leopard skins, and magical relics to make themselves immune to bullets. ‘Argo’ in the Congo: The Ghosts of the Stanleyville Hostage Crisis |Nina Strochlic |November 23, 2014 |DAILY BEAST 

Also due to their unusual immune system, bats can remain healthy and able to travel even while infected. Bats’ Link to Ebola Finally Solved |Carrie Arnold |November 12, 2014 |DAILY BEAST 

He believes, he has an instinct, that here is the heel of the German Colossus, otherwise immune to our arrows. Gallipoli Diary, Volume I |Ian Hamilton 

School-children at times have what appears to be mere sore throat but which is really diphtheria in the naturally immune. Essays In Pastoral Medicine |Austin Malley 

She luxuriated in her little perilous letting-go—could toy with, and yet be immune from, a danger. In Accordance with the Evidence |Oliver Onions 

If so, then Lieutenant Jervis is immune to the virus and is not a transmitter or carrier of it. The Judas Valley |Gerald Vance 

Even today the Emperor was not immune from the charms of feminine beauty. The Secret Witness |George Gibbs