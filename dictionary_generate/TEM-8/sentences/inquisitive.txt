Their attitude toward foreign journalists, once they were in control of Kabul, was generally friendly and inquisitive. After the Fall: What Afghanistan Looks Like Since the Taliban Takeover |Lorenzo Tugnoli |August 4, 2022 |Washington Post 

Still, she hopes these inquisitive folks take her age—74, thank you—as a reminder that everyone still has time to hike, no matter how many years they’ve been at home, awaiting their turn. The Retirees Who Hiked Out of Suburbia and into the Long-Trail Record Books |awise |February 15, 2022 |Outside Online 

It’s inquisitive, curious, and a little rebellious about it, because that’s what teenagers are. The Daily Beast’s 21 Best TV Shows of 2021: From ‘Squid Game’ and ‘Succession’ to ‘Sex Education’ |Kevin Fallon |December 20, 2021 |The Daily Beast 

“Highly inquisitive, highly playful, always cheerful,” he recalls. For Next AG, Obama Picks a Quiet Fighter With a Heavy Punch |Michael Daly |November 8, 2014 |DAILY BEAST 

But what about the eloquent, book smart, interesting, quirky, inquisitive black woman, you ask? Lifetime’s ‘Girlfriend Intervention’: The Fairy Black Mothers TV Doesn’t Need |Phoebe Robinson |September 25, 2014 |DAILY BEAST 

Indeed, the crowd at my reading was attentive, inquisitive—and willing to spend money on printed books. Amazon Won’t Kill the Indie Bookstore |Bill Morris |July 30, 2014 |DAILY BEAST 

Friends say he was a highly inquisitive man and kept his ear close to the ground. 'In Cold Blood' in Ukraine |Jamie Dettmer |May 3, 2014 |DAILY BEAST 

As Gondry asks Chomsky about his childhood, the answer soon becomes clear: a skeptical, inquisitive, combative mind. What Is Michel Gondry Doing With Noam Chomsky? |Jimmy So |November 20, 2013 |DAILY BEAST 

Gaze not upon another man's wife, and be not inquisitive after his handmaid, and approach not her bed. The Bible, Douay-Rheims Version |Various 

Some were inquisitive enough to ask, Has a treaty been signed or a trick been played upon the rebels? The Philippine Islands |John Foreman 

He hands a lady out; her pale blue silk domino hides her effectually from the inquisitive gaze of the crowd. The Pit Town Coronet, Volume I (of 3) |Charles James Wills 

No more inquisitive cruisers ready to let fly a salvo at anything that stirs. Gallipoli Diary, Volume I |Ian Hamilton 

The man's nature was inquisitive, and he was indulging idle conjectures as to what might be the news this courier brought. St. Martin's Summer |Rafael Sabatini