Beekeeping tradition runs so deep that in the Slovenian language, saying one’s “ax fell into the honey” is a proverb used to describe a sudden stroke of good luck. The secret to healthy honey bees is hiding in Slovenia |Megan Zhang / Saveur |January 17, 2022 |Popular-Science 

These are not, contrary to what proverbs and conventional wisdom might suggest, rocks and stones, but living entities. Our Little Life Is Rounded with Possibility - Issue 102: Hidden Truths |Chiara Marletto |June 9, 2021 |Nautilus 

I think of the proverbs we have around second times—second choice, second place, second fiddle, eternal second. Does Birth Order Really Determine Personality? Here's What the Research Says |Lynn Berger |April 13, 2021 |Time 

Look for efficient pathsAccording to an old proverb, “There are many paths to the top of the mountain, but the view is always the same.” How buyers can implement supply path optimization for in-app media buying |Inmobi |April 5, 2021 |Digiday 

A consulting firm based in the United States paid its taxes there, while a factory based in Beijing would render unto China what is China’s, to modernize the old biblical proverb. Can These Creative Taxes Catch Up With a Changing World? |Nick Fouriezos |January 14, 2021 |Ozy 

This work takes a page from President Ronald Reagan, who liked to repeat the Russian proverb “trust but verify.” It’s Time to Nail the Iran Nuke Deal |Rep. Rush Holt, Kate Gould |October 15, 2014 |DAILY BEAST 

In the case of Kudo-kai case boss Nomura, the appropriate Japanese proverb appears to be: “The mouth is the gate of misfortune.” The Case of the Yakking Yakuza |Jake Adelstein |September 16, 2014 |DAILY BEAST 

The catch phrase, which Reagan borrowed from a Russian proverb, was “trust but verify.” Mike Leach Tackles Geronimo the Motivational Murderer |James A. Warren |August 17, 2014 |DAILY BEAST 

The Latin proverb “Times Change and We Change With Them” used to be memorized by generations of students of Latin. Liberals Need to Learn to Say No |Bernhard Schlink |July 10, 2014 |DAILY BEAST 

The rich,” according to a Spanish proverb, “laugh carefully. Mel Brooks Is Always Funny and Often Wise in This 1975 Playboy Interview |Alex Belth |February 16, 2014 |DAILY BEAST 

The variety of taste in snuff is accounted for by the proverb, "So many men to so many noses." Tobacco; Its History, Varieties, Culture, Manufacture and Commerce |E. R. Billings. 

M. Mon here quotes a Latin proverb:—'Qui plus castigat, plus amore ligat.' Chaucer's Works, Volume 1 (of 7) -- Romaunt of the Rose; Minor Poems |Geoffrey Chaucer 

Behold every one that useth a common proverb, shall use this against thee, saying: As the mother was, so also is her daughter. The Bible, Douay-Rheims Version |Various 

There is an obvious allusion in this line to the common proverb—'As fain as fowl of a fair morrow,' which is quoted in the Kn. Chaucer's Works, Volume 1 (of 7) -- Romaunt of the Rose; Minor Poems |Geoffrey Chaucer 

This is only another form of a proverb which also occurs as 'Well fights he who well flies.' Chaucer's Works, Volume 1 (of 7) -- Romaunt of the Rose; Minor Poems |Geoffrey Chaucer