The show works as well as it does because it’s richly observed, wittily scripted, brilliantly cast and subtly acted, with a sense of humor that’s both sophisticated and incisive in its skewering of academia’s particular brand of pretentiousness. Netflix’s Sharp Satire The Chair Throws Sandra Oh Into the Politicized Powder Keg of Higher Ed |Judy Berman |August 19, 2021 |Time 

The two hosts approach it like stand-up comedians but the actual commercial and cultural analysis is sharp and incisive. Pandemic playlists: Songs (and podcasts) that got us through coronavirus lockdown |Seb Joseph |July 26, 2021 |Digiday 

While he rallies late and fills the latter third with incisive insights about issues of race and gender and about Williams’s life in the public eye, his nonlinear writing often diffuses much of what originally made Williams so compelling. Serena Williams is more than a tennis player. ‘Seeing Serena’ offers a prismatic view of her impact. |Stuart Miller |June 16, 2021 |Washington Post 

Regardless of its imperfections, Jenkins’ vision is still executed in a thoughtful, incisive way that will hopefully serve as a blueprint for more shows and films like it in the future. How Barry Jenkins’ ‘The Underground Railroad’ Avoids the Trauma Porn of Slavery Movies Past |Kyndall Cunningham |May 14, 2021 |The Daily Beast 

It won’t be as incisive as it might have been if it had been undertaken the first week of January 2020 and everything was on the table, but I still think it’s not too late. Top researchers are calling for a real investigation into the origin of covid-19 |Rowan Jacobsen |May 13, 2021 |MIT Technology Review 

It is a joy to watch Shafer seamlessly work incisive commentary on contemporary life into a fast-paced spine-chiller. The Best Fiction of 2014: Ford, Ferrante, Klay, and More |William O’Connor |December 7, 2014 |DAILY BEAST 

His correspondence, much of which survives, is that of an incisive and articulate observer. Stonewall Jackson, VMI’s Most Embattled Professor |S. C. Gwynne |November 29, 2014 |DAILY BEAST 

Equally incisive were Danny Welbeck and Daniel Sturridge, and the three young Afro-Englishmen electrified the game. The Group of Life |Tunku Varadarajan |June 15, 2014 |DAILY BEAST 

John Jenkins describes Miller as an “incisive witness both to scientific acumen and religious belief.” Meet the Prizewinning Catholic Biologist Creationists Can’t Stand |Karl W. Giberson |April 6, 2014 |DAILY BEAST 

But unlike Bloom and Eagleton, his books have been, while erudite and incisive, unashamedly populist. John Sutherland‘s Enjoyable Little History of Literature |Malcolm Forbes |November 29, 2013 |DAILY BEAST 

Immediately her own reassumed a harsh, proud set, her voice became even more incisive and cold. Valley of the Croen |Lee Tarbell 

He has six incisive and two canine teeth in each jaw, without reckoning the grinders. Buffon's Natural History. Volume IX (of 10) |Georges Louis Leclerc de Buffon 

His speech was rather incisive, considering how little he had seen of Paul. Paul Patoff |F. Marion Crawford 

Next above him in age is the host; shrewd, brusque, incisive of speech and manner. The Stones of Paris in History and Letters, Volume I (of 2) |Benjamin Ellis Martin 

The little woman was so combative and incisive that this always seemed a necessary precaution on the part of that gentleman. Sevenoaks |J. G. Holland