He reaches behind him, moving Irene to the far corner of the space, carving out an even more private one. What floor? |Sophia Nguyen |February 12, 2021 |Washington Post 

Use your forefingers and thumbs to grab the grippy catches on each corner of the lens and give a gentle pull. These Goggles Let You Ski Fog-Free |Joe Jackson |February 12, 2021 |Outside Online 

A bored proctor sat in the corner, scrolling through her phone. The mid-pandemic return to school is totally weird for kids. And possibly lonely, too. |Petula Dvorak |February 11, 2021 |Washington Post 

He appears to have a greater awareness of when to run from one rim to the other in transition, versus when it’s better to fan out to the corner in those situations. Myles Turner’s Game Has Evolved. The Box Score Doesn’t Know It Yet. |Chris Herring (chris.herring@fivethirtyeight.com) |February 10, 2021 |FiveThirtyEight 

These developer activists need our support, encouragement and help pinpointing the most crucial problems to address, and they need the tools to bring solutions to every corner of the world. The rise of the activist developer |Walter Thompson |February 9, 2021 |TechCrunch 

So I drove around the corner to the trailhead of the logging road that led back to the crash site. The 7-Year-Old Plane Crash Survivor’s Brutal Journey Through the Woods |James Higdon |January 7, 2015 |DAILY BEAST 

But they do put it right around the corner near the time the video was shot. The Monsters Who Screamed for Dead Cops |Jacob Siegel |December 23, 2014 |DAILY BEAST 

They work in a world filled with a sense—real or imagined—of danger lurking around each corner and every hallway. Any Outrage Out There for Ramos and Liu, Protesters? |Mike Barnicle |December 22, 2014 |DAILY BEAST 

They were racing toward the corner of Tompkins and Myrtle avenues with Johnson at the wheel when another call came over the radio. 'Please Don't Die!': The Frantic Battle to Save Murdered Cops |Michael Daly |December 22, 2014 |DAILY BEAST 

They have pushed into just about every other corner of the Caribbean and Central America where airports exist. Goodbye, Bahamas. Hello, Havana! |Clive Irving |December 18, 2014 |DAILY BEAST 

But Lucy had noted, out of the corner of her watchful eye, the arrival of Miss Grains, indignant and perspiring. The Pit Town Coronet, Volume I (of 3) |Charles James Wills 

Cheap as they are, they are a poorer speculation than even corner lots in a lithographic city of Nebraska or Oregon. Glances at Europe |Horace Greeley 

As Davy stood in the road, in doubt which way to go, a Roc came around the corner of the house. Davy and The Goblin |Charles E. Carryl 

Now, he chose a small table in a corner of the balcony, close to the glass screen. Rosemary in Search of a Father |C. N. Williamson 

The conflict of these certainties left hopeless disorder in every corner of his being. The Wave |Algernon Blackwood