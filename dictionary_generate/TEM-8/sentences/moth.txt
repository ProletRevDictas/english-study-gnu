New research published Wednesday in the journal Science Advances finds that British roadside habitats under LED streetlights contain radically fewer moth caterpillars, and those that remain show signs of stress. Streetlights are making caterpillars grow up faster—and that’s a bad thing |Philip Kiefer |August 25, 2021 |Popular-Science 

Streetlights may make moth caterpillars plump up faster, but that’s not necessarily a good thing. Streetlights are making caterpillars grow up faster—and that’s a bad thing |Philip Kiefer |August 25, 2021 |Popular-Science 

So this study looked specifically at moth caterpillars, which only move a few feet as they grow up. Streetlights are making caterpillars grow up faster—and that’s a bad thing |Philip Kiefer |August 25, 2021 |Popular-Science 

For Heblich and his fellow researchers, the moth would be a useful indicator in a much larger story about pollution. This Is Your Brain on Pollution (Ep. 472) |Stephen J. Dubner |August 12, 2021 |Freakonomics 

In 2016, it arrived in Africa, and now researchers fear that its adult moth form could make its way to Europe. A European butterfly bonanza starts with caterpillars 4,000 miles away |Benji Jones |June 25, 2021 |Vox 

Blanche was a fragile white moth beating against the unbreakable sides of a 1000 watt bulb. Elia Kazan to Tennessee Williams: You Gotta Suffer to Sing the Blues |Elia Kazan |May 1, 2014 |DAILY BEAST 

Thomas Harris tipped his hat to Fowles in The Silence of the Lambs when he created the moth-loving antagonist Jame Gumb. How to Understand the Criminal Mind By Reading This Novel |Casey N. Cep |December 6, 2013 |DAILY BEAST 

For a contemporary manifestation of this moth-eaten brand of tyranny, look no further than Hamas-ruled Gaza. Hamas: The Palestinian Fashion Police |Hussein Ibish |April 9, 2013 |DAILY BEAST 

What does it mean for a Chinese tiger, stuffed by the English, to be left as moth-food today? An Asian Tiger, Choking on Dust |Blake Gopnik |October 5, 2012 |DAILY BEAST 

The (p. 325) moth, whose egg produces these larv, is a large white miller of unusual size and prolificness. Tobacco; Its History, Varieties, Culture, Manufacture and Commerce |E. R. Billings. 

Lay not up to yourselves treasures on earth: where the rust, and moth consume, and where thieves break through, and steal. The Bible, Douay-Rheims Version |Various 

I also saw the clerk busy folding up a moth-eaten velvet pall—not a sight for Christmas Day. A Thin Ghost and Others |M. R. (Montague Rhodes) James 

Spinning—he was for ever spinning, like a tireless moth through a fiery air; and the world went roaring past. When Valmond Came to Pontiac, Complete |Gilbert Parker 

White Moth—strands from an Ostrich, wings from a white Pigeon, a white hackle for legs, and a black head. The Teesdale Angler |R Lakeland