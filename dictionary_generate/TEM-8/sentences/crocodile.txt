In between encounters with crocodiles and hippos during their months in Africa, the researchers snorkeled and dived to gather examples of every cichlid they could. New Fish Data Reveal How Evolutionary Bursts Create Species |Elena Renken |December 1, 2020 |Quanta Magazine 

Mukherjee decided to visit the National Chambal Sanctuary to capture the fight for the survival of these crocodiles. This crocodile daddy giving 100 babies a ride puts your carpool to shame |María Paula Rubiano A. |October 9, 2020 |Popular-Science 

They tend to be darker than crocodiles and are less likely to be aggressive. Let’s learn about alligators and crocodiles |Bethany Brookshire |September 29, 2020 |Science News For Students 

Its features hint that crocodiles may have journeyed from Africa to the Americas. American crocs seem to descend from kin that crossed the Atlantic |Carolyn Wilke |August 25, 2020 |Science News For Students 

They are the first footprint evidence that some ancient ancestors of modern crocodiles walked on two legs. These crocodile ancestors lived a two-legged life |Carolyn Gramling |July 23, 2020 |Science News For Students 

Winston Churchill once said “An appeaser is one who feeds a crocodile—hoping it will eat him last.” Tea Party Cannibalizes Cantor |John Avlon |June 11, 2014 |DAILY BEAST 

On another trip, a defiant caiman (a South American crocodile) devours his mosquito net. Exploring the Amazon, While We Still Can |Darrell Hartman |May 15, 2014 |DAILY BEAST 

One of these heroes is an insect-loving contemporary of Charles Darwin, the other a crocodile-wrestling Steve Irwin acolyte. Exploring the Amazon, While We Still Can |Darrell Hartman |May 15, 2014 |DAILY BEAST 

The bags themselves were covetable items as ever, relying on highest-quality material: ostrich and crocodile. Anya Hindmarch and Stella McCartney Close London Fashion Week |Tom Sykes |September 17, 2013 |DAILY BEAST 

The insatiable crocodile of Israeli colonialism is far too fat. What "Undivided Jerusalem" Is Really About |Yousef Munayyer |September 12, 2012 |DAILY BEAST 

What we shall do if an Indian springs from behind the bushes, or a crocodile comes out of the sedge, I don't know. The Nursery, July 1873, Vol. XIV. No. 1 |Various 

As they looked toward the spot, the fearful head and jaws of a crocodile could be seen reaching up out of the water. Alila, Our Little Philippine Cousin |Mary Hazelton Wade 

No Indian sprang from the bush, no crocodile came out of the sedge; and the river was crossed without one of them being drowned. The Nursery, July 1873, Vol. XIV. No. 1 |Various 

All at once, a crocodile appeared close to the boat, capsized it, and with open jaws was ready to devour the man. Alila, Our Little Philippine Cousin |Mary Hazelton Wade 

Because in far distant times he saved the life of a Chinaman from the fury of a crocodile. Alila, Our Little Philippine Cousin |Mary Hazelton Wade