To be sure, some problems will require a qualified computer scientist, but citizen-developer toolkits enable laymen to manage simpler solutions on their own. Achieving flexibility with no- and low-code applications |Chuan |July 7, 2022 |MIT Technology Review 

As it stands, there is a mismatch between the rules scientists write by compared with those that laymen read by. How to Make Sense of Contradictory Science Papers - Issue 100: Outsiders |Haixin Dang & Liam Kofi Bright |June 2, 2021 |Nautilus 

Even a layman can see the unconnected letters and the strange angles of the note. The John Edwards Verdict Waiting Game |Diane Dimond |May 26, 2012 |DAILY BEAST 

Art Layman: Could it be that Adelson is really a closet Dem? The Results Are In! |Ryan Prior |February 28, 2012 |DAILY BEAST 

Reading such a thing might convince the layman that getting hammered is healthy for the heart. Is Alcohol Really Good for You? |Alizah Salario |May 16, 2011 |DAILY BEAST 

Nuclear jargon has evolved into a code that is indecipherable to the layman. Be Afraid, Very Afraid |Nathaniel Rich |March 15, 2011 |DAILY BEAST 

I was once present at a dispute between a layman and a clergyman, upon the subject of dreams. The Life and Most Surprising Adventures of Robinson Crusoe, of York, Mariner (1801) |Daniel Defoe 

Lawyers always underestimate the legal knowledge of an intelligent layman. Encyclopaedia Britannica, 11th Edition, Volume 5, Slice 2 |Various 

Of the writers who deal with music from the point of view of the cultivated layman, Aristotle is undoubtedly the most instructive. The Modes of Ancient Greek Music |David Binning Monro 

They agreed in language too technical for a layman to understand that the cause of Doris' blindness was gradually disappearing. The Hidden Places |Bertrand W. Sinclair 

The difference between maximum and minimum flow of most of our streams when stated in figures is startling to the layman. Proceedings of the Second National Conservation Congress |Various