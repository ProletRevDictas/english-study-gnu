Where cabin cleaning required much physical work, being a dispatcher required planning and strategizing for where and when to send cleaners. What I learned from 5 years of cleaning airplanes in the middle of the night |matthewheimer |August 30, 2020 |Fortune 

I started as a cleaner of aircraft cabins on the graveyard shift. What I learned from 5 years of cleaning airplanes in the middle of the night |matthewheimer |August 30, 2020 |Fortune 

Frontline workers like security guards, house cleaners, and grocery store clerks deserved greater respect long before the pandemic began. Why empowering frontline workers is a key element to a safe reopening |jakemeth |August 18, 2020 |Fortune 

Nevertheless he does have a perspective, gesturing toward the work going on at the now shuttered Cobalt, to the dry cleaners on the corner, “none of this would be possible without immigrants.” You know what Dupont needed? A Nordic restaurant |Brock Thompson |August 12, 2020 |Washington Blade 

In fact, an unexpected benefit of regulations is that brands are now working with cleaner data. Ad industry groups launch effort to push for addressable media standards |Ginny Marvin |August 4, 2020 |Search Engine Land 

So this pool cleaner would always come around and talk to her, and I figured it would be a good idea for a movie. The Renegade: Robert Downey Sr. on His Classic Films, Son’s Battle with Drugs, and Bill Cosby |Marlow Stern |November 26, 2014 |DAILY BEAST 

We wrote the film together, but it was about a guy pool cleaner. The Renegade: Robert Downey Sr. on His Classic Films, Son’s Battle with Drugs, and Bill Cosby |Marlow Stern |November 26, 2014 |DAILY BEAST 

Is filtered water that much cleaner and safer than tap water? Are Water Filters B.S.? |Michael Schulson |August 19, 2014 |DAILY BEAST 

Cars may be cleaner one year, but breakdowns might become more common, too. Leaky Ceilings, Catcalls, and Uncaged Pythons: 4 Hours on NYC’s Worst Subway |Kevin Zawacki |August 8, 2014 |DAILY BEAST 

And is politics really cleaner when stringent restrictions are put into place? Is Big Money Politics an Overblown Evil? |David Freedlander |August 2, 2014 |DAILY BEAST 

The ground lately in the shaft has been cleaner killas, and if any alteration, better ground. Life of Richard Trevithick, Volume II (of 2) |Francis Trevithick 

As the French farms are usually on a small scale, they are invariably kept cleaner than those in England and America. Travels through the South of France and the Interior of Provinces of Provence and Languedoc in the Years 1807 and 1808 |Lt-Col. Pinkney 

A small portion of hyposulphite of soda mixed with vinegar will make a good cleaner for teacups having tannin stains. The Boy Mechanic, Book 2 |Various 

But he made a gallant recovery with a vaccuum cleaner an' was aven with th' prisidint in four. Mr. Dooley Says |Finley Dunne 

But if a man wants to write why put it down some place where it's going to be swept up by the street cleaner the next day. The Boy Grew Older |Heywood Broun