Brad Rutter is trying to turn his trivia fame into entertainment. From Socially Isolated Nerd to Jeopardy! Bad Boy: A Thank You Note |Arthur Chu |November 27, 2014 |DAILY BEAST 

Hexagon, where she endeared herself to Japanese audiences for giving ridiculously answers to trivia questions. Masahiro Tanaka Is the Yankees' $155M Lethal Weapon and Strikeout Machine |Allen Barra |May 9, 2014 |DAILY BEAST 

But if the leader fails to gain acceptance, then the budget details are nothing more than wonk-trivia. Even Republicans Don’t Like the Ryan Budget |Lloyd Green |April 8, 2014 |DAILY BEAST 

Ted is a quirky guy that loves esoteric topics like Teddy Roosevelt trivia and the history of belts. Everything You Need to Know About 'How I Met Your Mother' |Chancellor Agard |March 31, 2014 |DAILY BEAST 

He has just a massive amount of political trivia…And I will bet you that he thought this was funny. This Civil War Reenactor Controls Christie’s Fate |Olivia Nuzzi |March 5, 2014 |DAILY BEAST 

The idea was to keep him busy, keep his mind on trivia, keep him from thinking about what was going on inside that reactor. The Bramble Bush |Gordon Randall Garrett 

The best description of London about this time is certainly Gay's "Trivia." London |Walter Besant 

Graphic pictures of the manners of coachmen may be found in Gay's Trivia, ii. The Sir Roger de Coverley Papers |Various 

But Opis, Trivia's sentinel, long ere now sits high on the hill-tops, gazing on the battle undismayed. The Aeneid of Virgil |Virgil 

And in a large collection of boy's stuff, one would not observe the absence of a Boy Scout knife and other trivia. The Fourth R |George Oliver Smith