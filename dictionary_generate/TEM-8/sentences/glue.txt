Imagine gluing together a bridge made out of toothpicks, only having to rip apart the glue to build a skyscraper with the same material. How a Memory Quirk of the Human Brain Can Galvanize AI |Shelly Fan |September 28, 2020 |Singularity Hub 

ImageNet is being updated and GLUE has been replaced by SuperGLUE, a set of harder linguistic tasks. Facebook wants to make AI better by asking people to break it |Will Heaven |September 24, 2020 |MIT Technology Review 

All you need to get started are popsicle sticks, rubber bands and some glue. Let’s learn about ancient technology |Bethany Brookshire |September 15, 2020 |Science News For Students 

“How emoji has become the glue that holds everything together has been one of the more interesting insights of the study,” says Christina Janzer, Slack’s senior director of research and analytics. The angst of remote workers is evident in their emoji |Anne Quito |July 28, 2020 |Quartz 

According to “How It Works,” envelope glue is made from gum arabic, which is a product of the hardened sap from acacia trees. Science of ‘Seinfeld’ |LGBTQ-Editor |June 15, 2020 |No Straight News 

You see the handwork, the glue, how the people in the agency were working on it. Frank Gehry Is Architecture’s Mad Genius |Sarah Moroz |October 27, 2014 |DAILY BEAST 

It is the “glue that holds often flaky single malts together,” as Broom puts it. Don't Be a Single-Malt Scotch Snob |Kayleigh Kulp |August 9, 2014 |DAILY BEAST 

Maliki no longer wanted to pay for the glue that kept it there. Iraq Is Not Our War Anymore. Let It Be Iran’s Problem. |Christopher Dickey |July 17, 2014 |DAILY BEAST 

She also was handy, hammering nails into walls and using a heat gun to chisel glue off the floor. The Wonderful Weirdness of Christine McConnell, Queen of Creepy Cookies |Tim Teeman |July 9, 2014 |DAILY BEAST 

If you tear that bond the rip leaves open scars where the glue once was. Brits Celebrate Phin Lyman, The Boy Virgin Who Says He’ll Wait for Love |Nico Hines |May 19, 2014 |DAILY BEAST 

"Jefferies' Marine Glue" can be procured at all times, the cost being about sixpence per pound. Notes and Queries, Number 178, March 26, 1853 |Various 

One part of marine glue, and two of best red sealing-wax, form a beautiful cement for glass baths. Notes and Queries, Number 178, March 26, 1853 |Various 

Another poison is of a pale glue color, which is supposed by Stanley to be made of crushed red ants. Man And His Ancestor |Charles Morris 

Have ready in a pan some stiffening made by pouring boiling water on a very small piece of glue. The Ladies' Book of Etiquette, and Manual of Politeness |Florence Hartley 

Across these was laid another layer at right angles, with perhaps a coating of glue or paste between them. The Private Life of the Romans |Harold Whetstone Johnston