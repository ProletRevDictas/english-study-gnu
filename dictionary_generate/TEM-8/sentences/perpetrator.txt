Nearly two in five said that nothing happened to that perpetrator. She reported sexual harassment by a former supervisor — and was fired soon after |Samantha Schmidt |February 8, 2021 |Washington Post 

We hear from the perpetrators, some of them remorseless and eager for more blood, others wondering what had taken hold of them in a moment of violence, worried about getting caught. Rebecca Sacks’s ‘City of a Thousand Gates’ is a kaleidoscopic debut that illuminates the Israeli-Palestinian conflict |Porter Shreve |February 4, 2021 |Washington Post 

Survivors shouldn’t also be forced to jump through hurdles to hold their perpetrators accountable. Evan Rachel Wood names Marilyn Manson in abuse allegations: ‘I am done living in fear of retaliation’ |Sonia Rao |February 2, 2021 |Washington Post 

In some cities, it’s been used to make predictions about who might be a victim or a perpetrator of violent crime. Morning Report: Law Enforcement Is Spending on ‘Predictive Policing’ |Voice of San Diego |February 2, 2021 |Voice of San Diego 

There needs to be accountability, she said, because forgiveness does not happen when a perpetrator wants to move on. AOC explains why Republicans can’t just tell her to forget about the insurrection and move on |Monica Hesse |February 2, 2021 |Washington Post 

Choosing not to pursue a perpetrator is not admittance of lies or false motives. The Right's Rape Trolls vs. Lena Dunham |Emily Shire |December 10, 2014 |DAILY BEAST 

However, Brunner was not the only perpetrator of the Holocaust mooching around the streets of the Syrian capital. Hitler’s Henchmen in Arabia |Guy Walters |December 7, 2014 |DAILY BEAST 

Ultimately, all it took was the mere mention of a lawyer for the perpetrator to delete the accounts and disappear completely. A Female Writer’s New Milestone: Her First Death Threat |Annie Gaus |December 1, 2014 |DAILY BEAST 

Another surveillance video, showing the perpetrator with hammer in hand, is here. Is Brooklyn Becoming Unsafe for Gays? It Depends On Which Ones |Jay Michaelson |October 18, 2014 |DAILY BEAST 

Nor is it surprising that the employer of the accused has neither fired the alleged perpetrator nor denounced the trial. Should Twitter Suspend LGBT Engineer Accused Of Raping Her Wife? |Emily Shire |October 8, 2014 |DAILY BEAST 

If an apology is made by the unlucky perpetrator of the accident, try to set him at his ease by your own lady-like composure. The Ladies' Book of Etiquette, and Manual of Politeness |Florence Hartley 

How frequently it has been said that an evil deed recoils upon the head of the perpetrator! Notes and Queries, Number 196, July 30, 1853 |Various 

If he was the perpetrator of this crime, how comes it that he was not detected and denounced by the young people I saw going out? The Circular Study |Anna Katharine Green 

Laurie viewed the perpetrator of the melancholy sounds with a cold, unrelenting eye. Marjorie Dean College Freshman |Pauline Lester 

Those cognisant of the facts concluded that the perpetrator of the mischief was discovered. The Mysteries of All Nations |James Grant