Thanks to Noether’s observation, mathematicians can now harness the power, structure and theorems of algebra to understand topology. Topology 101: The Hole Truth |David S. Richeson |January 26, 2021 |Quanta Magazine 

McKenzie and geophysicist Robert Parker used this theorem to calculate the dance of the lithospheric blocks — the plates. How the Earth-shaking theory of plate tectonics was born |Carolyn Gramling |January 13, 2021 |Science News 

Mathematics searches for new theorems to build upon the old. How Claude Shannon Invented the Future |David Tse |December 22, 2020 |Quanta Magazine 

The new local friendliness theorem requires duplicating the Wigner’s-friend setup. A New Theorem Maps Out the Limits of Quantum Physics |Anil Ananthaswamy |December 3, 2020 |Quanta Magazine 

In the decade or so since he started learning about this approach, it has helped him prove a wide range of theorems. Computer Scientists Break Traveling Salesperson Record |Erica Klarreich |October 8, 2020 |Quanta Magazine 

Infinite Monkey Theorem has also landed praise in national publications including  Wine Spectator as recently as last year. Taste Off: Super Bowl State Wines From Colorado and Washington |Jordan Salcito |February 1, 2014 |DAILY BEAST 

A few years ago, I attended a party hosted by The Infinite Monkey Theorem. Taste Off: Super Bowl State Wines From Colorado and Washington |Jordan Salcito |February 1, 2014 |DAILY BEAST 

This, however, is not a question of the method of the social science, but a theorem of the science itself. A System of Logic: Ratiocinative and Inductive |John Stuart Mill 

This was M. Comte's opinion; but it is by no means implied in his fundamental theorem. A System of Logic: Ratiocinative and Inductive |John Stuart Mill 

A modern historian aptly remarks that the medicine of the present "embraces nothing but a theorem of investigation by the senses." An Epitome of the History of Medicine |Roswell Park 

But as it seems not yet to have been stated clearly enough, I will here try to put my entire theorem into an unmistakable form. The Stones of Venice, Volume III (of 3) |John Ruskin 

But in the proof of this great theorem two influences were neglected, either of which is fatal to its validity. A Text-Book of Astronomy |George C. Comstock