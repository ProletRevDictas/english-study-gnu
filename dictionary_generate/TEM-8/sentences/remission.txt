Harrison’s cancer is now in remission, and while he still has another year of maintenance treatment, the goal is to get him back in the classroom — at least part-time — by the fall. A boy is battling cancer. His teacher visits him every day for a private lesson. |Sydney Page |May 20, 2021 |Washington Post 

Despite its remission, the chronic disease has caused recurring health issues over the ensuing decades. Jane Birkin is back with a new album, but her presence is everlasting |Jeff Weiss |March 19, 2021 |Washington Post 

The formula weighs factors such as age, and, say, whether a cancer patient is in remission or currently undergoing chemotherapy. Should Someone With Asthma Get a COVID-19 Vaccine Before Someone With Cancer? The Next Big Challenge in the Vaccine Rollout |Alice Park |February 10, 2021 |Time 

Instead, 90 ­percent of them went into remission immediately. Emil Freireich, a pioneer of chemotherapy and a ‘towering figure in oncology,’ dies at 93 |Emily Langer |February 4, 2021 |Washington Post 

In three weeks, after going through a whole full plant-based diet, my vision came back three months later, my diabetes went into remission, the nerve damage went away, and I dropped 35 pounds. A Leading NYC Mayoral Candidate Thinks Roof Farms Can Save America’s Cities |Amanda Kludt |January 14, 2021 |Eater 

It went into remission, but it would resurface in 2011; and Scott was able to beat it once again. Remembering ESPN’s Sly, Cocky, and Cool Anchor Stuart Scott |Stereo Williams |January 4, 2015 |DAILY BEAST 

But in June 2012, after six years of remission, Brown was diagnosed with cancer again. MTV’s Diem Brown Dies: When Reality TV Starts Getting Real |Kevin Fallon |November 14, 2014 |DAILY BEAST 

A very long remission and stability is considered a substantial success. Are Viruses the Next Cure for Cancer? |Kent Sepkowitz |May 15, 2014 |DAILY BEAST 

I think the research runs in different directions depending on the nature of the remission. A Q&A with Scott Stossel, Author of ‘My Age of Anxiety: Fear, Hope, Dread, and the Search for Peace of Mind’ |Jesse Singal |February 20, 2014 |DAILY BEAST 

“This drug appears to shut cluster headaches down and puts patients into remission,” says Halpern. Longtime Sufferers of Cluster Headaches Find Relief in Psychedelics |Valerie Vande Panne |February 5, 2014 |DAILY BEAST 

It would probably claim her without remission for the next seven years. The Creators |May Sinclair 

The remission of punishment was in the discretion of the Governor-in-chief: the 30 Geo. The History of Tasmania , Volume II (of 2) |John West 

This Chaim is only too ready to undergo, and he applies himself with even more ardor than before to get a remission of his sins. The History of Yiddish Literature in the Nineteenth Century |Leo Wiener 

In 468, the people again withdrew to the Janiculum, demanding the remission of debts, and crying out against usury. History of Julius Caesar Vol. 1 of 2 |Napoleon III, Emperor of the French, 1808-1873. 

Are you willing now to agree to the remission of the fine in consideration of Grettir's sentence being commuted? Grettir The Strong |Unknown