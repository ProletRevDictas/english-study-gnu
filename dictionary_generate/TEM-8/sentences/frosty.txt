If Jupiter was born at its current distance from the sun, its birthplace would have been a frosty 60 kelvins. Born in deep shadows? That could explain Jupiter’s strange makeup |Ken Croswell |August 6, 2021 |Science News For Students 

When I came inside, I drank an absolutely frosty cup of water out of the same Hydro Flask Tumbler I reviewed back in 2018. Our Gear Guy’s 5 Favorite Summer Items |wtaylor |August 4, 2021 |Outside Online 

You feel her heartbreak when she sings about a lover who has moved on in “Stone Cold,” her frosty pettiness in “Sorry Not Sorry,” her playful longing in “Cool For the Summer,” and, more recently, her resolve in “I Love Me.” Demi Lovato is confronting her demons in real time |Bethonie Butler |March 25, 2021 |Washington Post 

A case of nerves is perfectly understandable right before a wedding, but cold feet will never do—not even in the frostiest winter weather. Best heated socks: The absolute warmest socks for cold conditions |PopSci Commerce Team |February 25, 2021 |Popular-Science 

On the first frosty night, I read that three of four kits don’t survive their first winter, and half of those that do will perish within their second year. How a sickly squirrel offered me unexpected comfort |Pam Spritzer |February 8, 2021 |Washington Post 

But Byrne himself is the parodist, and he commands the stage by his hollow-eyed, frosty verve. The Stacks: Pauline Kael's Talking Heads Obsession |Pauline Kael |November 22, 2014 |DAILY BEAST 

So when you take that first frosty sip of your mint julep in celebration of the Kentucky Derby, enjoy it. The Storied Origins of the Classic Mint Julep |Dane Huckelbridge |May 3, 2014 |DAILY BEAST 

But even that often-frosty relationship has further chilled as the two sides hurled insults and accusations this week. Exclusive: Putin Halts All Talks With White House |Josh Rogin |April 25, 2014 |DAILY BEAST 

Thousands of people wrapped in flags marched in the frosty, smoky air. Inside the Kiev Crackdown |Anna Nemtsova |December 10, 2013 |DAILY BEAST 

The only frosty point came when West asked what sex the Royal Baby was – Harry's steely look left the question unanswered. How Dominic West Got Into Trouble With Prince Harry When He Asked Royal Baby Gender |Tom Sykes |November 7, 2013 |DAILY BEAST 

An' her eyes,—she had wonnerful eyes,—would shine like de stars frosty nights in Virginny. The Cromptons |Mary J. Holmes 

If the nights are very cold and frosty, the top of the plants may be covered with a light cloth or paper to protect the seed buds. Tobacco; Its History, Varieties, Culture, Manufacture and Commerce |E. R. Billings. 

Watch took charge of it at once, pressing his warm body against the frosty fleece, and licking its face and feet to warm them. The Nursery, December 1881, Vol. XXX |Various 

Bricks used during frosty weather should be quite dry, and those that have been exposed to rain or frost should never be employed. Encyclopaedia Britannica, 11th Edition, Volume 4, Part 3 |Various 

And on this path Miss Peyton was seen walking briskly to and fro in the frosty, but sunny air. The Atlantic Monthly, Volume 17, No. 101, March, 1866 |Various