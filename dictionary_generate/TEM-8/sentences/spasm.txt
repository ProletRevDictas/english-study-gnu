In Week 15, with the Lions out of playoff contention, Stafford played through torn cartilage in his rib cage that gave him body spasms. Does Matthew Stafford make sense for Washington? Here’s how the QB would fit. |Sam Fortier |January 26, 2021 |Washington Post 

Hauser also scored 21 points, connecting on 7 of 13 three-pointers, and added seven rebounds despite being bothered by back spasms before the game. Virginia stays perfect in the ACC with an easy win against Syracuse |Gene Wang |January 26, 2021 |Washington Post 

Meanwhile, multiple sclerosis patients have access to THC-based treatments in more than 30 countries to ease muscle spasms, expand mobility, decrease pain, improve sleep, and, overall, provide better quality of life. Four plants that are scientifically proven to be therapeutic |Sandra Gutierrez G. |January 25, 2021 |Popular-Science 

Gun sales surged, especially among first-time buyers, this year during the coronavirus outbreak and then spasms of racial unrest. Be Very Afraid: A Virtual House of Horrors |Tracy Moran |October 25, 2020 |Ozy 

Beyond cosmetic applications, Botox is a treatment for more than 20 medical conditions, including eye spasm, Bell’s palsy, headache, excessive sweating and urinary incontinence. Can you get too much Botox? |By Matthew J. Lin/The Conversation |October 1, 2020 |Popular-Science 

The only joy is the momentary spasm of sexual gratification; the only happiness that of (temporarily) allayed jealousy. David's Bookclub: Sodom and Gomorrah |David Frum |September 29, 2012 |DAILY BEAST 

A spasm of computer trouble yesterday delayed finishing some thoughts on Mitt Romney's USA Today op-ed about social safety nets. Social Safety Nets for a Flat Earth |David Frum |September 20, 2012 |DAILY BEAST 

Akin's view may be outrageous, but its outrageousness is not one man's mental spasm. Akin's Abortion View: More Widespread in GOP Than You Think |David Frum |August 20, 2012 |DAILY BEAST 

For three straight days, a spasm of violence has gripped Cairo, leaving 13 people dead and scores wounded. Egypt Clashes: A Revolution Hangover |Dan Ephron |May 6, 2012 |DAILY BEAST 

Essentially, this is no more than a spasm of mindless and brutal high summer destruction. London Burns as Riots Spread |William Underhill |August 9, 2011 |DAILY BEAST 

Here the Dimbula shot down a hollow, lying almost on her side—righting at the bottom with a wrench and a spasm. Kipling Stories and Poems Every Child Should Know, Book II |Rudyard Kipling 

It might be a spasm of pain, and his somewhat pale face became paler; but he did not speak. The Everlasting Arms |Joseph Hocking 

What he dreaded was the spasm of dying—the convulsion that was to snap the thousand silver strings in the harp of life. Julian Home |Dean Frederic W. Farrar 

Richard thought to see him fell the Greek to the stones; but his uplifted arm lowered, the spasm of madness passed. God Wills It! |William Stearns Davis 

But what a contrast to this spasm of local statesmanship the earlier years of that drink-sodden century display! English Poor Law Policy |Sidney Webb