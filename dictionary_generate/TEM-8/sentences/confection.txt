In fact, Mexico may have started producing cajeta long before Argentina began producing its confection. Learn to make cajeta, a Mexican confection with multilayered sweetness and history |Adriana Velez |April 12, 2021 |Washington Post 

Made with Valrhona chocolate, the intense confection becomes richer at the table when a server pokes a hole in its center and fills it with caramel sauce. A Potomac view meets polished cooking at Ada’s on the River in Alexandria |Tom Sietsema |March 26, 2021 |Washington Post 

She envisions opening up a “very pretty cakery” where people can pick up confections to share for celebrations. How Home Bakers Have Found Sweet Success During the Pandemic |Raisa Bruner |March 4, 2021 |Time 

The jiggly pastel-hued confections are made with flavors like peach, lychee, and persimmon. From the Cut: 33 Valentine’s Day Gifts for the Foodie in Your Life |The Cut Staff |February 8, 2021 |Eater 

Every author of fiction set in the past has to gauge how tolerant readers will be with pure invention, and mysteries are imaginative confections as a rule. Two historical mystery novels plunge readers into the past while keeping them guessing |Clare McHugh |February 8, 2021 |Washington Post 

"I shall consider myself Punk'd," recites a proud Ichabod in response to this unexpected lit confection. Naked Ben Franklin Christens the Campy Return of ‘Sleepy Hollow’ |Amy Zimmerman |September 23, 2014 |DAILY BEAST 

In comparison, The Quest is an over the top confection, with about as much genuine genre integrity as Your Highness. 'The Quest' Review: Behold, a Campy 'Game of Thrones' Reality Show |Amy Zimmerman |August 1, 2014 |DAILY BEAST 

And your debut feature, (500) Days of Summer, really struck me as a pop confection of sorts. Marc Webb Takes Us Inside ‘The Amazing Spider-Man 2’ and Discusses His Rise to the A-List |Marlow Stern |March 15, 2014 |DAILY BEAST 

Finally, a dance song dominates the summer but manages to forgo sugary pop confection. 10 ‘Song of the Summer’ Contenders From Daft Punk to Ciara (VIDEO) |Kevin Fallon |July 5, 2013 |DAILY BEAST 

The business they were running was a confection of several sources. The Best of Brit Lit |Peter Stothard |April 28, 2011 |DAILY BEAST 

The azucarilla is a confection not unlike "Edinburgh rock," but more porous and of the nature of a meringue. Spanish Life in Town and Country |L. Higgin and Eugne E. Street 

La vieille mre est sujett des maux d'estomac, et je lui ai apport un pot de confection d'hyacinthe. Baron d'Holbach |Max Pearson Cushing 

This confection was embossed with a hundred intricate designs, rich with silver; un-Amish as a Christmas tree. Blind Man's Lantern |Allen Kim Lang 

Confections deals with that very delightful and fascinating part of cookery--confection making. Woman's Institute Library of Cookery, Vol. 5 |Woman's Institute of Domestic Arts and Sciences 

They have an extensive use in cookery, both as a confection and an ingredient in cakes, puddings, and pastry. Woman's Institute Library of Cookery, Vol. 5 |Woman's Institute of Domestic Arts and Sciences