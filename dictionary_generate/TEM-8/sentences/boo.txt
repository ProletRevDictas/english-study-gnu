At almost every stop, and especially at stadiums home to teams whose playoff runs ended against Houston during the 2017 and 2018 postseasons, they have been met with boos, trash cans and more. Alex Cora, leading Red Sox into a promising future, answers for his past in Houston |Chelsea Janes |May 31, 2021 |Washington Post 

As he circled the bases and Fedde blew on his hands amid a constant wind, the stadium filled with boos. Another intentional walk has disastrous consequences for Nats in 5-3 loss to Braves |Jesse Dougherty |May 6, 2021 |Washington Post 

Last month, DeGioia’s first commencement pitch drew boos from many students and families. Georgetown University to hold pandemic commencement at Nationals Park |Nick Anderson |April 22, 2021 |Washington Post 

LOUIS — At Busch Stadium, the show begins with a few light boos echoing through a sunny and crisp afternoon. Juan Soto keeps hitting, Joe Ross keeps dealing and the Nats win their first series |Jesse Dougherty |April 14, 2021 |Washington Post 

The audience, which included more dog owners than professional academics, audibly gasped, and there was a smattering of boos. Eye-opening acts of empathy in the animal kingdom |Richard Schiffman |April 9, 2021 |Washington Post 

I started a blog called Boo Cancer, You Suck as a safe place for me to process what I was going through. Blogger Shares and Shames Cancer in ‘Lily’ |Amy Grantham |December 9, 2014 |DAILY BEAST 

I could call him my “boo,” but when I tried it out he rolled his eyes. What Should I Call the Man I Love? |Dushka Zapata |November 18, 2014 |DAILY BEAST 

We can thank Lisa Kudrow for the rise of celeb reality TV—Real Housewives, the Kardashians, Honey Boo Boo and its ilk. How Lisa Kudrow Pulled Off TV’s Ultimate ‘Comeback’ |Kevin Fallon |November 6, 2014 |DAILY BEAST 

As quickly as Honey Boo Boo came, there she goes—even more quickly. The Shocking Rise and Fall of ‘Honey Boo Boo’ |Kevin Fallon |October 24, 2014 |DAILY BEAST 

And just like that, on her “Frito feet,” there Honey Boo Boo goes. The Shocking Rise and Fall of ‘Honey Boo Boo’ |Kevin Fallon |October 24, 2014 |DAILY BEAST 

This is better than frightening them out of their skins by jumping out from behind a door and saying "Boo." Punch, or the London Charivari, Volume 158, April 28, 1920 |Various 

Sometimes I don't get around till ten or eleven o'clock in the great boo-black dark. Molly Make-Believe |Eleanor Hallowell Abbott 

Even the most reckless of gambling junkmen could not be expected to dare much of an investment in such a peek-a-boo game as that. Blow The Man Down |Holman Day 

When the doorbell rang, half an hour later, Mr. Fielding was on his hands and knees playing "peek-boo!" The Incubator Baby |Ellis Parker Butler 

"And say 'Boo' to naughty little girls who won't let me complete my diary," shouted Brand. The Pillar of Light |Louis Tracy