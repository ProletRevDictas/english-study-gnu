The newborn suffered terrible burns to her face before the family repented and took her home. The Taliban Tried to Kill Her. Now She Negotiates With Them |Kate Bartlett |June 1, 2021 |Ozy 

If Hiltz had had his way, he said, he “would not pardon rebels, especially the leaders” until they first supplicated themselves “in the dust of humiliation” and sincerely repented for the rebellion. The future in Afghanistan may be key to the well-being of America’s soldiers |Dillon Carroll |May 6, 2021 |Washington Post 

Nature shields us from the worst consequences of our own actions, forgiving the sins we refuse to repent. Humanity’s greatest ally against climate change is the Earth itself |Sarah Kaplan |April 22, 2021 |Washington Post 

As the years rolled on, Cronos repented his actions, but it was too late. Gaia, the Scientist - Issue 99: Universality |Hope Jahren |April 7, 2021 |Nautilus 

I need them to repent of what got them here and turn in a new direction. 'We Must Have a Third Reconstruction.' Read the Full Text of the Rev. William J. Barber II's Sermon at the Inaugural Prayer Service |William J. Barber II |January 21, 2021 |Time 

They were told, again as gently as such a message could be, that they were unbelievers but would soon repent. Abducted, Tortured, Indoctrinated: The Tale of a Teen Who Escaped ISIS |Yusuf Sayman |August 4, 2014 |DAILY BEAST 

A: You must repent for entertaining the notion of killing a person, especially as a means of resolving a dispute. Dear Rabbi: Should I Shoot Women of the Wall? |Sigal Samuel |June 6, 2013 |DAILY BEAST 

Two of the women repent, so they ascend to heaven, and one woman refuses, and is doomed to hell. The Controversial Abortion Horror Film |Marlow Stern |June 24, 2011 |DAILY BEAST 

But rather than hiding away in a dark monastery to repent, Cardinal Law is instead an exalted member of Vatican inner circles. The Cardinal Who Got Away |Barbie Latza Nadeau |May 11, 2010 |DAILY BEAST 

Investigating signs and ads, such as Repent and Sin No More! Warhol's Final Years |Paul Laster |October 8, 2009 |DAILY BEAST 

Mr. Pickwick—deepest obligations—life preserver—made a man of me—you shall never repent it, sir. The Posthumous Papers of the Pickwick Club, v. 2(of 2) |Charles Dickens 

When the father had finished, he stabbed his wife, telling her to repent of her sins and to confess to God who would pardon her. The Philippine Islands, 1493-1898, Volume XX, 1621-1624 |Various 

But later the Marshal had bitter cause to repent these triumphs won over his rival. Napoleon's Marshals |R. P. Dunn-Pattison 

But if the house had been transferred, A could not afterwards repent of his act and demand its return. Putnam's Handy Law Book for the Layman |Albert Sidney Bolles 

If it shall do evil in my sight, that it obey not my voice: I will repent of the good that I have spoken to do unto it. The Bible, Douay-Rheims Version |Various