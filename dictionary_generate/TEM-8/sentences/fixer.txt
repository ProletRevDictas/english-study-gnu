They are disrupters, fixers, doers, iconoclasts, problem solvers—people who in a year of crisis have leaped into the fray. How We Chose the 2021 TIME100 |Edward Felsenthal |September 15, 2021 |Time 

It’s to his publicist, Robyn, a stone-cold, devilishly creative fixer played by Anna Paquin. Flack Is the Most Delectable in a New Wave of Shows About the Workaholics Who Make Hollywood Run |Judy Berman |June 11, 2021 |Time 

Instead of pushing Ukraine to probe the Bidens, Giuliani launched a renegade investigation of his own, relying on a cast of sources and fixers in Kyiv to help him gather information on the Bidens. Exclusive: How an Accused Russian Agent Worked With Rudy Giuliani in a Plot Against the 2020 Election |Simon Shuster/Kyiv |May 28, 2021 |TIme 

Like Biden, Klain is a consummate fixer, with Georgetown and Harvard Law School degrees thrown in for good measure. Where There’s Trouble, You’ll Usually Find Joe Biden |Lloyd Green |October 21, 2014 |DAILY BEAST 

His fixer, who Barfi said was affiliated with the Islamist Tawhid brigade, was set free 15 days later. Obama Administration and Sotloff Family Battle Over Blame for Journalist’s Kidnapping |Josh Rogin |September 22, 2014 |DAILY BEAST 

She asked if I had heard anything about the fixer, X. I had not. Was U.S. Journalist Steven Sotloff a Marked Man? |Ben Taub |September 2, 2014 |DAILY BEAST 

A fine fixer, I had heard from correspondents who knew him, but there had been a problem recently. Was U.S. Journalist Steven Sotloff a Marked Man? |Ben Taub |September 2, 2014 |DAILY BEAST 

He said that he had secured the fixer, ‘X,’ through a fellow Western journalist, and not by writing to 30 Syrians via Facebook. Was U.S. Journalist Steven Sotloff a Marked Man? |Ben Taub |September 2, 2014 |DAILY BEAST 

Sometimes he had to call in a “fixer” to manufacture evidence, that the far-off ends of justice might not be defeated. The Shadow |Arthur Stringer 

One of the best illustrations of how a town's officials sell themselves is embodied in the vile character known as "the fixer." Notes of an Itinerant Policeman |Josiah Flynt 

It was once my duty to run a race with a "fixer," and try to get the ear of a mayor of a town before he did. Notes of an Itinerant Policeman |Josiah Flynt 

I've had 'Silk' Humphreys, the best fixer in the business, working on him all day, and he'll be neutral before night. The Skylark of Space |Edward Elmer Smith and Lee Hawkins Garby 

By using such a fixer, the value of the resulting manure would be much enhanced. Manures and the principles of manuring |Charles Morton Aikman