It’s a sport that is exhilarating, providing what the vets refer to as “bluebird days” filled with fresh air, exercise, and plain old fun. Best ski goggles: What to look for in a pair you’ll love |Eric Alt |January 20, 2021 |Popular-Science 

Cordova has also ramped up her efforts to vet potential guests. Airbnb hosts are trying to turn away inauguration rioters. One listing added a photo of Michelle Obama. |Ashley Fetters |January 12, 2021 |Washington Post 

The two former pro athletes—along with a team of industry vets from Armada, K2, and Line—are selling new skis and boards in part to get people to buy fewer of them. The Ski Company That Wants to Sell Fewer Skis |Heather Hansman |December 24, 2020 |Outside Online 

That vitriol toward Vrotsos is what caught the attention of others, including vets like Millinor, who went on social media to confront Kolfage in her defense. Veteran, War Hero, Defendant, Troll |by Jeremy Schwartz and Perla Trevizo |September 29, 2020 |ProPublica 

There wasn’t a consensus on how to properly vet accused chefs, nor were many of them given much time to respond to the accusations presented by the foundation. The Mess That Is the 2020 James Beard Awards, Explained |Elazar Sontag |September 11, 2020 |Eater 

It was like witnessing the last two weeks of the life of a blind and toothless dog you knew the vet was just itching to destroy. Dems, It’s Time to Dump Dixie |Michael Tomasky |December 8, 2014 |DAILY BEAST 

Instead, the time has been spent setting up a system to vet potential recruits. U.S. Hasn’t Even Started Training Rebel Army to Fight ISIS |Tim Mak |November 25, 2014 |DAILY BEAST 

The City of Phoenix does not now have a single homeless vet; they have constructed apartments for them. McCain’s 13 Favorite Soldiers |Sandra McElwaine |November 11, 2014 |DAILY BEAST 

Imagine being an Iraq vet who lost friends securing a place such as Fallujah only to see ISIS now seize it. It’s Time for Iraq and Afghanistan Veterans to Get a Parade of Their Own |Michael Daly |November 11, 2014 |DAILY BEAST 

I was taken into one by Maurice, a gnarled old Vietnam vet in a wooly hat. Here’s a Reform Even the Koch Brothers and George Soros Can Agree On |Tina Brown |November 10, 2014 |DAILY BEAST 

She was such a picture that I asked who she was, and found she was a high school mistress, the niece of old Cooper, the vet. The Romance of His Life |Mary Cholmondeley 

She's an Irish girl—was governess to some rich Jew in Edgbaston, and she married a vet. Happy House |Betsey Riddle, Freifrau von Hutten zum Stolzenberg 

Freckles tried to think connectedly, but there were too many places on the trail where the Angel's footprints were vet visible. Freckles |Gene Stratton-Porter 

I wrote at once to the vet, telling him to telegraph "Curable" or "Hopeless," and to act accordingly. Animal Ghosts |Elliott O'Donnell 

We had the vet out from Westchurch two or three times, but there was nothing much he could do. The History of Sir Richard Calmady |Lucas Malet