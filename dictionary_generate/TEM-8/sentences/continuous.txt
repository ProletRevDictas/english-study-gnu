It’s no secret that small businesses everywhere face a continuous struggle for survival as they compete against national chains and online competition. Myths and Shame Shouldn’t Guide Cannabis Regulations |John Bertsch |September 8, 2020 |Voice of San Diego 

During her FKT attempt, she received continuous en route support from Warren Doyle, a nine-time AT finisher and founder of the Appalachian Trail Institute—a preparatory program for aspiring thru-hikers. Inside an FKT Attempt on the Appalachian Trail |Martin Fritz Huber |September 3, 2020 |Outside Online 

Through Medicare, if a patient is terminally ill and has a life expectancy of six months or less, they can access on-call nursing assistance, medical equipment and prescriptions, as well as continuous care in crisis moments when symptoms flare. Sent Home to Die |by Annie Waldman and Joshua Kaplan |September 2, 2020 |ProPublica 

Our lineup of founders include Sonny Vu, whose last startup, Misfit, was acquired by Apple, and is currently the chief executive officer of continuous carbon-fiber 3D printing company Arevo. Presenting TechCrunch Disrupt’s Asia sessions |Catherine Shu |August 28, 2020 |TechCrunch 

The unit’s travel size doesn’t leave room for many features, but it can be set to deliver single, double, or triple pulses of water, or a continuous stream. Water flossers that get between your teeth |PopSci Commerce Team |August 27, 2020 |Popular-Science 

The director, Jonathan Demme, offers us a continuous rock experience that keeps building, becoming ever more intense and euphoric. The Stacks: Pauline Kael's Talking Heads Obsession |Pauline Kael |November 22, 2014 |DAILY BEAST 

There had been continuous problems with the Pratt & Whitney engines. The Sexy Dream of the 747 |Clive Irving |October 26, 2014 |DAILY BEAST 

That really hit me, the continuous flow of ideas without stopping. The Stacks: John Coltrane’s Mighty Musical Quest |Nat Hentoff |October 18, 2014 |DAILY BEAST 

The ensuing night gave me the grand migraine of my life, with throbs like the blows of an ax and continuous pinwheels. The Stacks: H.L. Mencken on the 1904 Baltimore Fire |H.L. Mencken |October 4, 2014 |DAILY BEAST 

After 57 years of continuous operation, the theater closed, was sold to a private company, and scheduled for demolition. How to Save Silent Movies: Inside New Jersey’s Cinema Paradiso |Rich Goldstein |October 2, 2014 |DAILY BEAST 

Messrs. Jennens and Bettridge commenced in 1816, and improvements in the manufacture have been many and continuous. Showell's Dictionary of Birmingham |Thomas T. Harman and Walter Showell 

That the first part of this was probably quite true we can readily agree to, also that the out-put was continuous. Antonio Stradivari |Horace William Petherick 

After 24 hours' heavy and continuous fighting a substantial success has been achieved. Gallipoli Diary, Volume I |Ian Hamilton 

The west shore of Gidley Island appeared to be fronted by a continuous reef, on which some patches of dry rocks were observed. Narrative of a Survey of the Intertropical and Western Coasts of Australia] [Volume 2 of 2] |Phillip Parker King 

His secret thoughts he buried beneath a continuous mental preoccupation with the vain and the trivial. The Man from Time |Frank Belknap Long