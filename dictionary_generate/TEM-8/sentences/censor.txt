China resumed broadcasting the awards in 2003, though censors occasionally cut footage of politically sensitive figures or comments. China’s Oscars boycott mixes politics with push to curb Hollywood dominance |Lyric Li, Steven Zeitchik |April 22, 2021 |Washington Post 

The absence of censorship allowed these services to tell a new kind of story—the story of India as it is, rather than one that was acceptable to the censor board. Netflix v Modi and the battle for Indian cinema’s soul |Konstantin Kakaes |March 24, 2021 |MIT Technology Review 

By 1920 India had several regional censor boards, whose members were told to be watchful for “sensitive issues” and “forbidden scenes,” writes Someswar Bhowmik. Netflix v Modi and the battle for Indian cinema’s soul |Konstantin Kakaes |March 24, 2021 |MIT Technology Review 

The film was approved by the censors, but mobs defaced public property, thrashed people, and threw Molotov cocktails. Netflix v Modi and the battle for Indian cinema’s soul |Konstantin Kakaes |March 24, 2021 |MIT Technology Review 

Meanwhile, self-publishers have always had to tread carefully on what they post or risk being targeted by censors who deem them illegal or inappropriate. New rule reins in China’s flourishing self-publishing space |Rita Liao |February 1, 2021 |TechCrunch 

The studio seemed to be satisfied with the results—although still opted to censor the death sequence in many foreign territories. Exclusive: Sony Emails Say State Department Blessed Kim Jong-Un Assassination in ‘The Interview’ |William Boot |December 17, 2014 |DAILY BEAST 

Still, was it possible that Russian authorities could censor the Internet and make Meduza inaccessible for Russian readers? Russia’s Freest Website Now Lives in Latvia |Anna Nemtsova |November 29, 2014 |DAILY BEAST 

The attempts to censor news in Mainland China about the protests backfired. Chinese Tourists Are Taking Hong Kong Protest Selfies |Brendon Hong |October 23, 2014 |DAILY BEAST 

Activists still have to reach the site on their own, escaping efforts to censor or monitor the internet in their home countries. New Web Platform Crowdsources Human Rights |Josh Rogin |July 10, 2014 |DAILY BEAST 

Should the company censor conversations around such gun photographs, banning talk of a sale or a price? People Are Using Instagram to Sell Their Guns...and It’s Mostly Legal |Brian Ries |October 22, 2013 |DAILY BEAST 

I do not care very much how you censor or select the reading and talking and thinking of the schoolboy or schoolgirl. The Salvaging Of Civilisation |H. G. (Herbert George) Wells 

And that treatise of Van de Water, the Belgian, on the sublimation of the sub-conscious by the negation of the self-censor. Hooded Detective, Volume III No. 2, January, 1942 |Various 

The Government Film Censor interprets his role chiefly as one of guiding parents. Report of the Special Committee on Moral Delinquency in Children and Adolescents |Oswald Chettle Mazengarb et al. 

It is not part of the censor's duty to see that his rulings are observed. Report of the Special Committee on Moral Delinquency in Children and Adolescents |Oswald Chettle Mazengarb et al. 

His very appointment as censor was due to the bottle-acquaintance that had sprung up with the regent Prince of Wales. A Cursory History of Swearing |Julian Sharman