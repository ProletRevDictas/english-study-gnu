“The intelligence community consensus is that Russia continues to try to influence our elections,” Wray said. Election live updates: Trump returns to Wisconsin; Biden to face live audience at town hall |Colby Itkowitz, Felicia Sonmez, John Wagner |September 17, 2020 |Washington Post 

Step one was trying to form consensus, and step two was tbd. The Trailer: The First State goes last |David Weigel |September 15, 2020 |Washington Post 

There’s a broad consensus among bond investors that if rates on longer-term government debt start to creep up, as they’ve occasionally threatened to, then the Fed can and will step in. America’s $20 trillion debt is getting cheaper as it grows |McKenna Moore |September 12, 2020 |Fortune 

The overwhelming consensus of the scientific community was to call into question the credibility of the president’s statement. On COVID-19 vaccines, Big Pharma knows to just say ‘no’ |matthewheimer |September 11, 2020 |Fortune 

At the same time, he added, “There’s already a sort of consensus developing that if any country develops a vaccine, of course they’ll keep a higher proportion for within their country.” More than manufacturing: India’s homegrown COVID vaccines could transform its pharma industry |Naomi Xu Elegant |September 6, 2020 |Fortune 

But there is no consensus about what the attrition of ISIS looks like. Pentagon Doesn’t Know How Many People It’s Killed in the ISIS War |Nancy A. Youssef |January 7, 2015 |DAILY BEAST 

It all began, the consensus seems to be, with the red jungle fowl. The History of the Chicken: How This Humble Bird Saved Humanity |William O’Connor |December 27, 2014 |DAILY BEAST 

The consensus leans toward forbidding it, though some people of knowledge think it permissible. ISIS Jihadis Get ‘Slavery for Dummies’ |Jamie Dettmer |December 9, 2014 |DAILY BEAST 

Only 27 percent accept the scientific consensus that anthropogenic climate change is real. Extreme Weather? Blame the End Times |Jay Michaelson |November 28, 2014 |DAILY BEAST 

Bipartisan consensus is like when my doctor and my lawyer agree with my wife that I need help. Up to a Point: Thanks to the Biggest Turkey, Uncle Sam |P. J. O’Rourke |November 27, 2014 |DAILY BEAST 

Not like New York, that never expresses an opinion until a sort of consensus has sweated up to the surface. Ancestors |Gertrude Atherton 

It is a fact implied in the consensus of the various parts of the social body. A System of Logic: Ratiocinative and Inductive |John Stuart Mill 

The consensus of classical opinion, then, agrees that the purpose of rhetoric is persuasive public speaking. Rhetoric and Poetry in the Renaissance |Donald Lemen Clark 

A consensus of the opinions of antiquarians is that the Swastika had no foothold among the Egyptians. The Swastika |Thomas Wilson 

Why, it would have taxed to the uttermost the experience and resources of any one among themselves, was the consensus of opinion. Forging the Blades |Bertram Mitford