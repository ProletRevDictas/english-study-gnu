As a result, Phobos appears large enough to blot out a large fraction of the sun as seen from the surface of Mars. These Images Expose the Dark Side of the Solar System - Issue 89: The Dark Side |Corey S. Powell |August 26, 2020 |Nautilus 

She and her colleagues used a model that compared fantasy players’ win fractions during the first half of a series of competitions against their win fractions over the second half. The Economics of Sports Gambling (Ep. 388 Rebroadcast) |Stephen J. Dubner |August 20, 2020 |Freakonomics 

China’s gross savings rate, as a fraction of its gross domestic product, dropped from a peak of 52 percent in 2008 to 46 percent in 2018, while India’s fell from 37 percent in 2007 to 30 percent in 2018. Could the Recession Revive the Savings Gene in China and India? |Pallabi Munsi |August 16, 2020 |Ozy 

There’s a precise way in which a matching Set triple can be thought of as an arithmetic progression, and just as with lists of whole numbers, you can ask what fraction of the cards you must lay down to be sure of finding at least one triple. Landmark Math Proof Clears Hurdle in Top Erdős Conjecture |Erica Klarreich |August 3, 2020 |Quanta Magazine 

Since 77 is an odd number, we cannot reduce this fraction to have a lower power of 2 in the denominator, so 5 or 6 generations will not work. How to Design (or at Least Model) Mixed Dog Breeds |Pradeep Mutalik |July 31, 2020 |Quanta Magazine 

The judges who handle arraignments at criminal court in all five boroughs have a small fraction of their usual caseloads. Shot Down During the NYPD Slowdown |Michael Daly |January 7, 2015 |DAILY BEAST 

Twitter mentions: 20,000, but still a tiny fraction of what was to come. How the World Turned on Bill Cosby: A Day-by-Day Account |Scott Porch |December 1, 2014 |DAILY BEAST 

No, on the contrary, only a fraction of this country, a part which I would call the best of Italy, is against the Mafia. Days of Mafia Mayhem Are Wracking Italy Once Again |Barbie Latza Nadeau |November 22, 2014 |DAILY BEAST 

It has a third of the budget and a fraction of the maritime vessels. Britain’s Let-Em-All-Die Policy |Nico Hines, Barbie Latza Nadeau |November 1, 2014 |DAILY BEAST 

While it provides only a small fraction of the power used—about 1.5 percent—it does represent an important investment and symbol. Charging Up |The Daily Beast |October 28, 2014 |DAILY BEAST 

The beauty, the mystery,—this fierce sunshine or something—stir——' She hesitated for a fraction of a second. The Wave |Algernon Blackwood 

We can thus disregard the first 16 and consider only the last two figures which constitute the fraction of a century. Assimilative Memory |Marcus Dwight Larrowe (AKA Prof. A. Loisette) 

"I've got one-fraction of an inch play, at any rate," said the garboard-strake, triumphantly. Kipling Stories and Poems Every Child Should Know, Book II |Rudyard Kipling 

Joseph had mentally spent his tiny fraction of the money a dozen times or more. Hooded Detective, Volume III No. 2, January, 1942 |Various 

Before turning up the eleventh card he paused for the fraction of a second. The Joyous Adventures of Aristide Pujol |William J. Locke