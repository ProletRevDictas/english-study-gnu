So it was meaningful when, in the 2000s, Vredenburg noticed that some of the amphibians he studied in California’s Sierra Nevada mountains survived the chytrid onslaught. When Evolution Is Infectious - Issue 90: Something Green |Moises Velasquez-Manoff |September 30, 2020 |Nautilus 

What makes amphibians unique is that they live a “double life.” Scientists Say: Amphibian |Bethany Brookshire |September 21, 2020 |Science News For Students 

By the time the amphibian is an adult, it usually has lungs, not gills. Scientists Say: Amphibian |Bethany Brookshire |September 21, 2020 |Science News For Students 

They shone blue or ultraviolet light on 32 species of amphibians. Lots of frogs and salamanders have a secret glow |Erin Garcia de Jesus |April 28, 2020 |Science News For Students 

The new finding suggests that this biofluorescence is widespread among amphibians. Lots of frogs and salamanders have a secret glow |Erin Garcia de Jesus |April 28, 2020 |Science News For Students 

As the amphibian taxied away without its passenger, Clyde Wendell came down the trail. The Missing Formula |Mildred A. Wirt, AKA Ann Wirt 

The amphibian was taxiing slowly through the water, its nose pointed directly toward the beach. The Missing Formula |Mildred A. Wirt, AKA Ann Wirt 

The amphibian coasted slowly in toward the beach, throttled down its motors and finally came to a halt. The Missing Formula |Mildred A. Wirt, AKA Ann Wirt 

You may be sure April has really come when this little amphibian creeps out of the mud and inflates its throat. A Year in the Fields |John Burroughs 

The only other amphibian at Chinaj known to breed in the pools is Bufo valliceps valliceps. Amphibians and Reptiles of the Rainforests of Southern El Peten, Guatemala |William E. Duellman