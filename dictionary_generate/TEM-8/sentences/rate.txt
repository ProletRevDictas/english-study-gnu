If Republican-run Mississippi, with its Covid-19 death rate of 93 per 100,000, was a country, it would be in the top three globally, after San Marino and Peru. Trump says US Covid-19 deaths would be low if you excluded blue states. That’s wrong. |German Lopez |September 17, 2020 |Vox 

Founders Pledge estimates that a donation to this group would avert CO2 at a rate of $1 per metric ton. Want to fight climate change effectively? Here’s where to donate your money. |Sigal Samuel |September 17, 2020 |Vox 

Publishers are broadly seeing upticks in programmatic ads rates over the last two months. ‘We’re about hiring journalists’: Insider Inc. launches third global news hub in Singapore |Lucinda Southern |September 17, 2020 |Digiday 

San Diego County’s coronavirus case rate is now surging, thanks in part to rising cases at San Diego State University. North County Report: Schools Are Reopening for Students Most in Need |Kayla Jimenez |September 16, 2020 |Voice of San Diego 

The region will fall to the worst tier of the state’s reopening system if the high rate continues for another week. North County Report: Schools Are Reopening for Students Most in Need |Kayla Jimenez |September 16, 2020 |Voice of San Diego 

Historically the reelection rate for members of Congress is in the area of 95 percent. The Unbearable Whiteness of Congress |Dean Obeidallah |January 8, 2015 |DAILY BEAST 

With a mortality rate of 70 percent, the more cases that arise, the deadlier this epidemic becomes. The Race for the Ebola Vaccine |Abby Haglage |January 7, 2015 |DAILY BEAST 

The accident rate in Asia has marred what was in 2014 a banner year for aviation safety. Annoying Airport Delays Might Prevent You From Becoming the Next AirAsia 8501 |Clive Irving |January 6, 2015 |DAILY BEAST 

At any rate, policy can enforce equal rights and foster equal opportunity. No Gods, No Cops, No Masters |James Poulos |January 1, 2015 |DAILY BEAST 

Albuquerque Economic Development, a private non-profit, estimates the five year growth rate at almost double the U.S. in general. After The Fall: Introducing The Anti-Villain |Rich Goldstein |December 21, 2014 |DAILY BEAST 

In future years the poor-rate (so-called) will include, in addition to these, all other rates levyable by the Corporation. Showell's Dictionary of Birmingham |Thomas T. Harman and Walter Showell 

At any rate his stirring advice and the dispatches he brought roused the military authorities at Meerut into activity. The Red Year |Louis Tracy 

If we turn again in a new direction, it will at any rate not be in the direction of a return to autocratic mediævalism. The Unsolved Riddle of Social Justice |Stephen Leacock 

Of course he was contemplating the application of a "two year old hickory," as he went on at the rate of two forty. The Book of Anecdotes and Budget of Fun; |Various 

His arm was drawn around the drum, and finally his whole body was drawn over the shaft, at a fearful rate. The Book of Anecdotes and Budget of Fun; |Various