At Belmont, jockeys must not let their horse run too hard too early, and conserve some energy for the half-mile-long backstretch. Why California Chrome’s Fairy Tale Didn’t End Happily Ever After |Michael Fensom |June 8, 2014 |DAILY BEAST 

I am seeking to conserve nothing; I am looking ahead—and I am quite confident that I am not alone. America Is Coming to Terms with Its Racial Past—Let’s Look Ahead Instead |John McWhorter |May 22, 2014 |DAILY BEAST 

It sent its last picture 13 years ago, just before shutting down its camera to conserve power. Voyager Is Sending Us the Sounds of Interstellar Space |Josh Dzieza |September 14, 2013 |DAILY BEAST 

Districts may also employ additional tactics to conserve resources. The Real Juvenile Offenders |Daniela Drake |June 22, 2013 |DAILY BEAST 

The agreement is hardly more than a list of ways that local communities can better conserve natural resources. GOP: Ditch the Agenda 21 Tinfoil Hat Brigade |David Frum |February 7, 2013 |DAILY BEAST 

It will not conserve Christianity, but may be purified by it, even if able to flourish without it. Beacon Lights of History, Volume I |John Lord 

Barrington back into the present, to conserve his energies, to make him a man of action again. The Light That Lures |Percy Brebner 

Boil together a few times, and then pour the conserve into cases. A Poetical Cook-Book |Maria J. Moss 

You have heard how women strive to conserve the lives of children, to make them strong mentally, morally and physically. Proceedings of the Second National Conservation Congress |Various 

There is simply one way to conserve our natural resources, and that is to educate the farmer (applause). Proceedings of the Second National Conservation Congress |Various