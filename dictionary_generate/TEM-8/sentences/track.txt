She was accepted at Spelman College, got back on track, and ended up enrolling at Cal Poly Pomona. Morning Report: Lincoln Abruptly Canceled AP Class |Voice of San Diego |September 17, 2020 |Voice of San Diego 

Potts, who had been training for an Ironman triathlon, remembers keeping track of the time on her heart monitor watch. The Woman Propositioned by Alaska’s Former Lieutenant Governor Tells Her Story for the First Time |by Kyle Hopkins and Michelle Theriault Boots, Anchorage Daily News |September 10, 2020 |ProPublica 

Clicks, ecommerce transactions, or goals are the way businesses keep track of them. SEO proposals: Particular challenges and how to avoid getting a silent no |SEOmonitor |September 10, 2020 |Search Engine Watch 

The digital yuan is on track to be the world’s first sovereign digital currency and is currently in pilot mode with four state-owned commercial banks. Smart stimulus: Cash as code |Claire Beatty |September 9, 2020 |MIT Technology Review 

San Diego Unified’s decision-making and the metrics the district tracks can have a huge ripple effect. The County Is Rethinking a Major Coronavirus Trigger |Will Huntsberry |September 9, 2020 |Voice of San Diego 

If you answered nine or more, you may have won a SONY Betamax and an eight-track operating system. The World’s Toughest Political Quiz |Jeff Greenfield |December 31, 2014 |DAILY BEAST 

Workers built a temporary rail track through the city to move the statue in a process that took three days. Florence Preps ‘David’ for the Big One |Barbie Latza Nadeau |December 25, 2014 |DAILY BEAST 

I was there to track down the family of one of the most notorious defectors in Cuban history. The Life and Hard Times Of The Family A Cuban Defector Left Behind |Brin-Jonathan Butler |December 19, 2014 |DAILY BEAST 

It looks like Amazon is on track to get additional Pentagon contracts as well. How Amazon Became Santa’s Sweatshop |Sally Kohn |December 11, 2014 |DAILY BEAST 

Its inclusion in Record of the Year is on track with the recent trend of all nominees being chart-toppers. 10 Biggest Grammy Award Snubs and Surprises: Meghan Trainor, Miley Cyrus & More |Kevin Fallon |December 5, 2014 |DAILY BEAST 

Suddenly, however, he became aware of a small black spot far ahead in the very middle of the unencumbered track. The Joyous Adventures of Aristide Pujol |William J. Locke 

No trail was so obtuse, no thicket so dense that members of that regiment would not track them to their lair. The Courier of the Ozarks |Byron A. Dunn 

We are so many around here that you'll have to get paper and pencil and mark us down to keep track of how many. The Homesteader |Oscar Micheaux 

The print of steel-rimmed hoofs showed in the soft loam as plainly as a moccasin-track in virgin snow. Raw Gold |Bertrand W. Sinclair 

Track of the count may be kept by placing a mark for each leukocyte in its appropriate column, ruled upon paper. A Manual of Clinical Diagnosis |James Campbell Todd