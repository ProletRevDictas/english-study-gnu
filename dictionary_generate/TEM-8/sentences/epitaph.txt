The fact that the epitaphs are painted directly onto or close by to the tombs makes them a valuable repository of information about the previous occupants of the tombs. Why Are There Christian Crosses in Jewish Graves From Ancient Rome? |Candida Moss |October 24, 2021 |The Daily Beast 

Rizzolli’s success in that September auction is just one of many examples that disprove the epitaphs written for the NFT market after it receded from its dizzying peak this spring. As the NFT Market Explodes Again, Artists Fend Off Old Art-World Power Structures |Andrew R. Chow |October 15, 2021 |Time 

The future of movies is unwritten—but let’s get through this harvest, a rich one, before we chisel the epitaph. How This Fall Could Shape the Future of Moviegoing |Stephanie Zacharek |September 14, 2021 |Time 

Still, infiltration of militants from Pakistan has not stopped completely, and experts point out it’s too early to write the epitaph of Kashmir’s militancy — at least 26 armed fighters have entered Kashmir this year. Is Kashmir’s Militancy Dead? |Charu Kasturi |August 6, 2020 |Ozy 

When he ended Vieux Carré with the stage direction, “The house is empty now,” Lahr somberly terms it “an augury and an epitaph.” John Lahr’s Biography Perfectly Captures Tennessee Williams’ Tortured Greatness |Wendy Smith |September 25, 2014 |DAILY BEAST 

In the unlikely event McConnell loses his reelection bid, I already know the title for his political epitaph: Shameless. Mitch McConnell is the Most Shameless Man Alive |Jamelle Bouie |February 18, 2014 |DAILY BEAST 

Weil believes they are a fitting epitaph to a man often described as the most influential British publisher of his generation. If You Had One Year to Live, How Would You Spend It? |Edward Platt |November 25, 2013 |DAILY BEAST 

So he entitled one of his earlier books, thus already authoring his own epitaph. Christopher Hitchens: A Young Contrarian Salutes Him |Max McGuinness |December 18, 2011 |DAILY BEAST 

A stone mason was employed to engrave the following epitaph on a tradesman's wife: "A virtuous woman is a crown to her husband." The Book of Anecdotes and Budget of Fun; |Various 

Here—stop and look—is the epitaph of one, a considerable fellow in his day, a barrister of the Middle Temple. The Portsmouth Road and Its Tributaries |Charles G. Harper 

He was sumptuously buried in Kensal Green, where a marble pedestal carries his portrait and his epitaph. Punch, or the London Charivari, Vol. 109, July 27, 1895 |Various 

The following epitaph given by Maitland commemorates a martyrdom of this reign. The Catacombs of Rome |William Henry Withrow 

While digging here in 1856, De Rossi found the important epitaph of Eusebius before given. The Catacombs of Rome |William Henry Withrow