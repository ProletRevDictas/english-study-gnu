To offer a free TurboTax-like service for bankruptcy to indebted Americans. This OZY Genius Helps People File for Bankruptcy |Pallabi Munsi |March 15, 2021 |Ozy 

Still, the comprehensive debt numbers provide a useful measure of America’s rise in the ranks of most heavily indebted nations. The U.S. now has a debt level that rivals Italy’s |Shawn Tully |January 14, 2021 |Fortune 

In the short term, much will depend on whether lawmakers pass measures to help indebted renters make back payments. The Housing Market Is Booming, But Millions of Americans Face Eviction—and the Gap Is Getting Worse |Emily Barone |December 4, 2020 |Time 

This week, government ministers of poor and indebted nations will appeal to their creditors for a much more ambitious debt relief effort as they grapple with the health care and economic consequences of the coronavirus pandemic. Everyone Needs Pandemic Stimulus. But Africa Also Needs Debt Relief |Fiona Zublin |October 13, 2020 |Ozy 

AT&T became the most indebted nonfinancial company in America. AT&T looks to sell DirecTV as investors question its huge bet on content |Geoffrey Colvin |October 7, 2020 |Fortune 

Many legendary Shakespearean thespians have been indebted to drink. Is That a Bottle of Wine I See Before Me? The Delights of Drunk Shakespeare |Tom Teodorczuk |June 1, 2014 |DAILY BEAST 

In making the Black Paintings, Reinhardt may have been as indebted to Duchamp as to Malevich and Barnett Newman. Ad Reinhardt's Black (-on-Black) Humor |Blake Gopnik |December 2, 2013 |DAILY BEAST 

The Western literary tradition is indebted to the disaster caused by a bad guest. ‘A Sustained Sense of Violation’: When Bad House Guests Invade Literature |Matt Seidel |July 23, 2013 |DAILY BEAST 

His voice caught the attention of everyone around him, including one fellow Palestinian, to whom Assaf will forever be indebted. Arab Idol Mohammad Assaf Is the Middle East’s Newest Ambassador |Itay Hod |June 27, 2013 |DAILY BEAST 

Zonen got Robson to admit he was forever indebted to Jackson for helping make his career in Hollywood. Behind the Michael Jackson Bombshell: How a Staunch Defender Suddenly Flipped |Diane Dimond |May 9, 2013 |DAILY BEAST 

He was already deeply indebted to his wife; not one of his three partners had proved to be such as he expected and required. Blackwood's Edinburgh Magazine, No. CCCXXXIX. January, 1844. Vol. LV. |Various 

He conspired against Richelieu, to whom he was indebted for much of his good fortune, and to whose resentment he fell a victim. The Every Day Book of History and Chronology |Joel Munsell 

You are very kind, sir,” he said; “my companions and I shall feel deeply indebted to you for this opportune assistance. Hunting the Lions |R.M. Ballantyne 

Singularly courteous and obliging on all occasions, I, personally, have been much indebted to him for help and advice. Fifty Years of Railway Life in England, Scotland and Ireland |Joseph Tatlow 

The public are indebted to him for the invention of the high-pressure steam-engine and the first railway steam-carriage. Life of Richard Trevithick, Volume II (of 2) |Francis Trevithick