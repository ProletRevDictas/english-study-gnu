Storey said Wright often returned applications “dripping in red pen.” Two Texas Regulators Tried to Enforce the Rules. They Were Fired. |David Hasemyer, InsideClimate News |December 9, 2014 |DAILY BEAST 

She survived by spending the night in the two-storey Barangay hall – the local community centre. Typhoon Haiyan: The Philippine Village that Lost Its Men |The Telegraph |November 17, 2013 |DAILY BEAST 

Fronting directly on the street is a two-storey wall, pierced by two doorways: entrance and exit to the compound. Argentina Doesn't Want the Truth, Ctd. |Ilana Glazer |February 8, 2013 |DAILY BEAST 

In her book Storey included notes of conversations she had had with Kate about Dickens and Ellen. Michael Slater on the Great Dickens Scandal |Michael Slater |November 8, 2012 |DAILY BEAST 

“This is not the news we wanted to hear,” said Jefferson County District Attorney Scott Storey. Jessica Ridgeway Is Dead: Colorado Kidnapping Ends in Tragedy |Christine Pelisek |October 12, 2012 |DAILY BEAST 

In the campanile of S. Alessio there are two arcades in each storey, each divided with a mid-wall shaft. Encyclopaedia Britannica, 11th Edition, Volume 5, Slice 2 |Various 

This storey was reached by staircases, access to which was gained through doors at each end of the faade. Ypres and the Battles of Ypres |Unknown 

Above these doors were the high windows of the upper storey, the Hall having two floors. Ypres and the Battles of Ypres |Unknown 

It towers above us, blocking out the sky as a street of six-storey houses would do. Round the Wonderful World |G. E. Mitton 

There is only one window in the fourth storey, which gives light to the belfry, and is very large. Bell's Cathedrals: The Cathedral Church of Carlisle |C. King Eley