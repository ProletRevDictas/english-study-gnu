Elsewhere, crude and Bitcoin continue to soar, and GameStop is up 10% in pre-market trading. Bitcoin, stocks and crude take off as the markets brace for a wave of stimulus checks |Bernhard Warner |February 8, 2021 |Fortune 

Light crude from Texas contains hydrocarbons with chains between five and 12 carbon atoms long. Keystone XL was supposed to be a green pipeline. What does that even mean? |Juliet Grable |February 5, 2021 |Popular-Science 

Europe’s energy stocks and crude prices continues to gain, as does Bitcoin. Bitcoin, Big Tech and crude gain as the focus shifts to more stimulus |Bernhard Warner |February 4, 2021 |Fortune 

In 2007 Vin de Silva and Robert Ghrist showed how to use homology to detect holes in the sensors’ coverage, based on just this crude information. Topology 101: The Hole Truth |David S. Richeson |January 26, 2021 |Quanta Magazine 

While teleportation is a solution, it is a crude one, and brings players out of the experience, reminding them that they are not physically present within the game world. Virtual reality has real problems. Here’s how game developers seek to delete them. |Derek Swinhart |January 21, 2021 |Washington Post 

Total oil production figures include crude oil, natural gas liquids, and other liquid energy products. Fact-Checking the Sunday Shows: Jan. 4 |PunditFact.com |January 5, 2015 |DAILY BEAST 

Originally conceived by author Clarence E. Mulford in 1904, Hopalong was crude, rough-talking, and dangerous. The Most Confusing Christmas Music Lyrics Explained (VIDEO) |Kevin Fallon |December 24, 2014 |DAILY BEAST 

Those higher construction costs will mean higher costs for companies who want to use the pipeline to ship their crude to market. Why the Keystone XL Pipeline May Not Be Built |Robert Bryce |November 19, 2014 |DAILY BEAST 

Three kids play cricket among the crude gravestones in a cemetery that is the largest in the province. Heart of Darkness: Into Afghanistan’s Taliban Valley |Matt Trevithick, Daniel Seckman |November 15, 2014 |DAILY BEAST 

The condensate is then supposed to be routed into the pipeline system that delivers the crude to the nearby refinery. Oil Tankers Leaking into Seattle’s Water |Bill Conroy |October 13, 2014 |DAILY BEAST 

This takes at first the crude device of a couple of vertical lines attached to the head (see Fig. 4). Children's Ways |James Sully 

They had swung back a hundred centuries towards original crude life. Hilda Lessways |Arnold Bennett 

This was a crude arrangement and often proved more of a hindrance than of a help to the player. The Recent Revolution in Organ Building |George Laing Miller 

In the center of the spot was a crude sign, projected in black lines upon the wall. Hooded Detective, Volume III No. 2, January, 1942 |Various 

Let us suppose that any one who denied the old crude errors of astrology was persecuted as a heretic. God and my Neighbour |Robert Blatchford