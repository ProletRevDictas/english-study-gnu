Too much time away from picking items off shelves to ship to consumers — time that is tracked by computers — can lead to reprimands that can slow raises and promotions, and even lead to termination. Amazon’s anti-union blitz stalks Alabama warehouse workers everywhere, even the bathroom |Jay Greene |February 2, 2021 |Washington Post 

At airports, warnings and reprimands to passengers who refuse to comply with federal security screeners gained teeth with the order. Transportation agencies wrestle with new federal mask mandate |Luz Lazo, Lori Aratani, Justin George |February 2, 2021 |Washington Post 

It’s therefore likely that Pack and his team will leave government before receiving any reprimands. Federal watchdog says “substantial likelihood of wrongdoing” at US broadcasting agency |Alex Ward |December 3, 2020 |Vox 

The board of overseers sanctioned Mason for unprofessional conduct in 2009 for exposing himself to both women, giving him one of its lowest public sanctions — a reprimand. Maine Hires Lawyers With Criminal Records to Defend Its Poorest Residents |by Samantha Hogan, The Maine Monitor, with data analysis by Agnel Philip |October 6, 2020 |ProPublica 

Browder said in a deposition for the case that he faced no internal reprimands or discipline, and that the shooting didn’t even come up in his yearly performance review. Police Review Board Was Denied Docs, Interviews in 2015 Shooting Review |Sara Libby |July 22, 2020 |Voice of San Diego 

Two years later, the reprimand was overturned, but Mia was unsuccessful in her bid to annul the adoptions. The Woody Allen Allegations: Not So Fast |Robert B. Weide |January 27, 2014 |DAILY BEAST 

Though Steele was never charged, he was given a career-ending reprimand. Black Hawk Down’s Long Shadow |Daniel Klaidman |October 9, 2013 |DAILY BEAST 

He failed to reprimand the MKs in question, implicitly suggesting that in the Likud, support for two states is optional at best. Ambassador Oren Tells AIPAC Likud Supports Two States |Sigal Samuel |March 4, 2013 |DAILY BEAST 

He Made a Positive Test Result ‘Go Away’ Did Armstrong pay to have a reprimand by the International Cycling Union disappear? Speed Read: Eight Shocking Bits From the USADA’s Lance Armstrong Report |Laura Colarusso, Nina Strochlic |October 11, 2012 |DAILY BEAST 

He and one other officer received only a letter of reprimand. When the Tragedy of Two Marines Killed In a Crash Becomes a Nightmare |Miranda Green |May 27, 2012 |DAILY BEAST 

One would think that the chorus should rather give the son a severe reprimand for speaking in so brutal a manner to his father. A Philosophical Dictionary, Volume 1 (of 10) |Franois-Marie Arouet (AKA Voltaire) 

Hoory Nicolas then began to converse with me, in the way of reprimand, for being in connexion with the English. Fox's Book of Martyrs |John Foxe 

More than once had Betsy's careless behavior drawn a reprimand upon her father. Napoleon's Young Neighbor |Helen Leah Reed 

Maciej did not finish his sentence, however, for the offended shoemaker began to reprimand him sternly. More Tales by Polish Authors |Various 

One of his first acts was severely to reprimand Puttkammer for misusing government influence at elections. Encyclopaedia Britannica, 11th Edition, Volume 11, Slice 8 |Various