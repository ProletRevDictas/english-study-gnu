Sheres said the area has a highly plastic clay and silt material that require a different approach to construction. 10-mile extension of 95 Express Lanes in Virginia unlikely to open next year |Luz Lazo |December 20, 2021 |Washington Post 

Manzini says migrants have overrun several local rivers, taxing water supplies and stirring up so much silt that the debris is obstructing three dams as well as many smaller streams in the area. Zimbabwe’s climate migration is a sign of what’s to come |Andrew Mambondiyani |December 17, 2021 |MIT Technology Review 

Madanhire says farming along the banks that way causes erosion and puts more silt and debris in the water for everyone downstream. Zimbabwe’s climate migration is a sign of what’s to come |Andrew Mambondiyani |December 17, 2021 |MIT Technology Review 

Over time, as the lake’s edge expanded and contracted with shifts in climate, it left behind distinct layers of clay, silt, and sand. 23,000-year-old footprints suggest people reached the Americas early |Kiona N. Smith |September 23, 2021 |Ars Technica 

They know that the artificial lifelines from Lake Powell and Lake Mead, which have existed for just a geological blink of an eye, are filling with silt and approaching dead pool. Should I Move to the Southwest, Even Though There’s a Drought? |mskenazy |September 1, 2021 |Outside Online 

The quality of water eventually becomes a concern, as reservoirs drop and salt and silt become more concentrated. America’s Axis of Drought |Kate Galbraith |March 4, 2014 |DAILY BEAST 

The subdead were coming, walking out of the silt formed fog. The Extinction Parade: An Original Zombie Story by Max Brooks |Max Brooks |January 14, 2011 |DAILY BEAST 

The only question is whether the oil will escape through fractures in the well's steel casing into the surrounding silt and rock. Did BP Free the Lockerbie Bomber? |Tom Bower |July 14, 2010 |DAILY BEAST 

The less its rate of fall and the greater the amount of silt it obtains from its tributaries, the more winding its course becomes. Outlines of the Earth's History |Nathaniel Southgate Shaler 

The floor of the valley was silt, sand and gravel—they would find nothing there. Space Prison |Tom Godwin 

Its very rich silt gives the lands on its banks the green charm of rich crops and pleasant trees. The Panjab, North-West Frontier Province, and Kashmir |Sir James McCrone Douie 

The sandy bed then becomes full from bank to bank, and the silt laden waters spill over into the cultivated lowlands beyond. The Panjab, North-West Frontier Province, and Kashmir |Sir James McCrone Douie 

The coffee was brown as floodwater silt, heavy with sugar, and very hot; and the cups had no handles. Blind Man's Lantern |Allen Kim Lang