Third, the combo of baobab and castor seed oils helps seal and smooth out dry hair and promote hair growth. Tower 28 Announces Winner Of The Clean Beauty Summer School Program |Hope Wright |September 17, 2020 |Essence.com 

His given name was Richard, but as soon as his hair came in, he was Red. Dawn of the Heliocene - Issue 90: Something Green |Summer Praetorius |September 16, 2020 |Nautilus 

You may not see it but you are eating it and washing your hair with it. The Environmental Headache in Your Shampoo - Issue 90: Something Green |Anastasia Bendebury & Michael Shilo DeLay |September 16, 2020 |Nautilus 

The state’s worst tier — the dreaded purple tier — would allow only hair salons and barber shops to continue operating indoors at full capacity. Morning Report: The Dreaded Purple Tier |Voice of San Diego |September 16, 2020 |Voice of San Diego 

So proper that his salt-and-pepper hair is never out of place. Spy Wars: The Hidden Foe America Must Defeat to Save Its Democracy |Charu Kasturi |September 13, 2020 |Ozy 

Cassandra, whose hair has already begun to fall out from her court-mandated chemotherapy, could face a similar outcome. Should Teens Have The Right To Die? |Brandy Zadrozny |January 8, 2015 |DAILY BEAST 

But his fingers moved through her silky strands of hair, and then down her neck. Powerful Congressman Writes About ‘Fleshy Breasts’ |Asawin Suebsaeng |January 7, 2015 |DAILY BEAST 

She used electrolysis to banish the prickly hair from her delicate face. Inside A Finishing School for Transwomen |Sharon Adarlo |December 27, 2014 |DAILY BEAST 

They dye their hair and alter their clothes, but not enough to attract attention from authorities. North Korea’s Secret Movie Bootleggers: How Western Films Make It Into the Hermit Kingdom |Lizzie Crocker |December 22, 2014 |DAILY BEAST 

Hangover Rx: “The old ‘hair of the dog’ is pretty much just a myth,” says White. 5 Hangover Cures to Save You After a Few Too Many |DailyBurn |December 19, 2014 |DAILY BEAST 

I pictured him as slim and young looking, smooth-faced, with golden curly hair, and big brown eyes. The Boarded-Up House |Augusta Huiell Seaman 

He was tall and of familiar figure, and the firelight was playing in the tossed curls of his short, fair hair. The Bondboy |George W. (George Washington) Ogden 

His hair was darker—almost brown save at the temples, where age had faded it to an ashen colour. St. Martin's Summer |Rafael Sabatini 

He frowned, and bent his head, and his long hair fell over his face, while the poor Stuttgardter sat there like a beaten hound. Music-Study in Germany |Amy Fay 

He stood before the glass hung above the wash bench and 369 smoothed his hair. The Bondboy |George W. (George Washington) Ogden