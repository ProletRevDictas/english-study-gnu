Normandy, which monopolised the glory, was to monopolise the spoil. The History of England |T.F. Tout 

I shall monopolise a good deal of your time, and I fancy they intend to be rather gay here. The Californians |Gertrude Franklin Horn Atherton 

But they cannot partake of a part of greatness, for that will not make them great, etc.; nor can each object monopolise the whole. Parmenides |Plato 

There will be moonlight there, and those people will monopolise the terrace when they have finished dinner. Adam Johnstone's Son |F. Marion Crawford 

The Old Gang made no attempt to monopolise the Executive by running a full ticket. The History of the Fabian Society |Edward R. Pease