Liu and his colleagues irradiated a solution containing PFBS and sulfite for an entire day, only to find that less than half of the pollutant in the solution had broken down. Just 3 ingredients can quickly destroy widely used PFAS ‘forever chemicals’ |Nikk Ogasa |June 3, 2022 |Science News 

The duo then chemically analyzed the irradiated oil to see how much was transformed into dissolved organic carbon. Sunlight helps clean up oil spills in the ocean more than previously thought |Carolyn Gramling |February 16, 2022 |Science News 

The Martian surface has long been an irradiated desert unsuitable for life. Iceland’s Eruptions Reveal the Hot History of Mars |Robin George Andrews |April 6, 2021 |Quanta Magazine 

Suddenly his whole heart seemed to irradiate light and color and music and sweet smelling things. Molly Make-Believe |Eleanor Hallowell Abbott 

Thus will the perfections of the Deity for ever blaze in the flames of perdition, and irradiate the temple of glory! Female Scripture Biographies, Vol. I |Francis Augustus Cox 

The shadows of the morning having disappeared, the brightness of eternal noon will irradiate our existence. Female Scripture Biographies, Vol. I |Francis Augustus Cox 

Then a carefully veiled kindliness of heart seemed to bubble to the surface and irradiate his face. The Terms of Surrender |Louis Tracy 

Deeper than tears, these irradiate the tophets with their glad heavens. Tablets |Amos Bronson Alcott