A boy wearing a face mask with the TikTok logo uses a mobile phone outside the downed shutters of a shop in Mumbai, India. It’ll Take More Than 15 Seconds to Rebuild Virality |Pallabi Munsi |December 2, 2020 |Ozy 

Photographer Frank Deschandol built a high-speed shutter out of an old hard drive because his camera’s own shutter would have been too slow to capture the moment. Check out the breathtaking winners of the 2020 Wildlife Photographer of the Year contest |Rachael Zisk |October 20, 2020 |Popular-Science 

When the cycle starts, the shutters on the top of the box are closed, which allows the reservoir that contains the muscle to fill with water vapor. Imitation Is the Sincerest Form of Environmentalism - Issue 90: Something Green |Anastasia Bendebury & Michael Shilo DeLay |October 7, 2020 |Nautilus 

The moisture causes the muscles to relax, which opens the shutters. Imitation Is the Sincerest Form of Environmentalism - Issue 90: Something Green |Anastasia Bendebury & Michael Shilo DeLay |October 7, 2020 |Nautilus 

When the coronavirus pandemic prompted Nasdaq’s operations to shutter offices earlier this year, it was not a completely unfamiliar scenario for staff. We are entering a golden age for borderless, global teams |Jackie Bischof |September 27, 2020 |Quartz 

Do they really not look around them when they hit the shutter, or is it all part of a ploy to attract more attention? Selfie Hall of Shame: Is Anywhere Safe From Sick Snaps? |Charlotte Lytton |October 11, 2014 |DAILY BEAST 

So she heard the shutter click and said ‘Oh no’ and came jogging over at me. Anthony Cumia: ‘I Have No Regrets’ |Lloyd Grove |July 9, 2014 |DAILY BEAST 

In his 2014 State of the Union address, Obama promised to shutter the prison built on Cuban soil by the end of the year. Bergdahl Deal Could Be First Step to Emptying Gitmo |Josh Rogin |June 2, 2014 |DAILY BEAST 

Is it too corny to think of Bailey capturing love with the click of a shutter? David Bailey’s ‘Stardust’ Shows a Keen Eye for Fine Faces |Chloë Ashby |February 8, 2014 |DAILY BEAST 

Blockbuster, which will soon shutter completely, was perhaps the most maddening retail chain ever. R.I.P. Blockbuster, You Frustratingly Magical Franchise, You |Kevin Fallon |November 6, 2013 |DAILY BEAST 

A very thin vacuum shutter forms a better interrupter of sound waves than a brick wall two or three feet in thickness. The Recent Revolution in Organ Building |George Laing Miller 

She partly opened the wooden shutter again and pointed to an upper story of the opposite building. The Red Year |Louis Tracy 

And once Mother Oriole found, caught in the shutter, little threads of Hepzebiah's hair. Seven O'Clock Stories |Robert Gordon Anderson 

The umpire's first decision was usually his last; they broke him in two with a bat, and his friends toted him home on a shutter. A Connecticut Yankee in King Arthur's Court, Complete |Mark Twain (Samuel Clemens) 

The next instant the click of the shutter in the camera announced that the prize was secure. Gold-Seeking on the Dalton Trail |Arthur R. Thompson