We also had a lot of luck, favorable weather, a capable crew, access to equipment, multiple escape routes, and an intimate knowledge of the terrain. How We Saved Our Neighborhood from a Wildfire |Dain Zaffke |September 2, 2020 |Outside Online 

Other gear, like chainsaws or Pulaski tools, comes down as cargo from the same plane from about 150 feet above the terrain. How aerial firefighters battle blazes from the skies |Rob Verger |August 27, 2020 |Popular-Science 

Perhaps, in the end, our galaxy, and even our universe, is simply the test tube for a vast chemical computation exploring a mathematical terrain of possibilities that stretches on to infinity. How Life Could Continue to Evolve - Issue 88: Love & Sex |Caleb Scharf |August 12, 2020 |Nautilus 

Like its predecessor on the Curiosity rover, the camera will take color, 3-D and panoramic images to help scientists understand the terrain and the mineralogy of the surrounding rocks. NASA’s Perseverance rover will seek signs of past life on Mars |Lisa Grossman |July 28, 2020 |Science News 

The video primarily shows off BigDog’s ability to balance on its own, right itself, and move over uneven terrain. The Robot Revolution Was Televised: Our All-Time Favorite Boston Dynamics Robot Videos |Jason Dorrier |July 19, 2020 |Singularity Hub 

If they cannot mass, they will not be as effective in taking terrain. Drone ‘Shortage’ Hampers ISIS War |Dave Majumdar |November 18, 2014 |DAILY BEAST 

It is this kind of terrain that makes this country so hard to secure. Heart of Darkness: Into Afghanistan’s Taliban Valley |Matt Trevithick, Daniel Seckman |November 15, 2014 |DAILY BEAST 

The drawback was that the terrain in front of Bradley made success very costly. Blood in the Sand: When James Jones Wrote a Grunt’s View of D-Day |James Jones |November 15, 2014 |DAILY BEAST 

But that was a decade ago, and Lurie has moved into less jokey terrain. The Gods of Punk Are Back in New York City |Anthony Haden-Guest |September 27, 2014 |DAILY BEAST 

As forbidding as this terrain is, there is another force at work on the ocean surface – the Antarctic Circumpolar Current. MH370 Debris Is Lost Forever, Can the Plane Be Found Without It? |Clive Irving |September 7, 2014 |DAILY BEAST 

It extends only a short distance into Virginia and consists mainly of rough, rugged terrain. Hallowed Heritage: The Life of Virginia |Dorothy M. Torpey 

This action provided Virginians with knowledge of the type of terrain and its potentiality along this important borderline. Hallowed Heritage: The Life of Virginia |Dorothy M. Torpey 

The original design of Wren had to be altered slightly because of the terrain of the country-side. Hallowed Heritage: The Life of Virginia |Dorothy M. Torpey 

Captain Melendez studied the terrain with a hurried glance, and it was far from being favourable. The Border Rifles |Gustave Aimard 

The terrain was fairly level, but a spirit level would have shown a marked tilt to the east. David Lannarck, Midget |George S. Harney