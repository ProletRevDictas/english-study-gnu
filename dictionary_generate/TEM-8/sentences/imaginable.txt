“Slavery, in every way imaginable, was central to the project of designing, funding, building, and maintaining the school,” it concluded. Va. House votes to force public colleges to reckon with ties to slavery, create scholarships or other programs |Nick Anderson |February 4, 2021 |Washington Post 

The successful part of the season, the stretch that has made the postseason imaginable, is the part where the team was his to maneuver as deliberately as he wished. Washington’s playoff hopes hinge completely on getting back the steady hand of Alex Smith |Les Carpenter |December 31, 2020 |Washington Post 

In some stores, sophisticated systems are tracking customers in almost every imaginable way, from recognizing their faces to gauging their age, their mood, and virtually gussying them up with makeup. Podcast: Attention, shoppers–you’re being tracked |Tate Ryan-Mosley |December 21, 2020 |MIT Technology Review 

The Republican cult of victimhood is dangerous because if you believe that you have been wronged by forces beyond your control, you may also believe that you are justified in fighting back by any means imaginable. Why the Republican cult of victimhood is so dangerous |Max Boot |December 11, 2020 |Washington Post 

“Such a violation of physical integrity is no longer imaginable today,” Dekker added in a statement. Dutch government apologies for forcible sterilizations of trans, intersex people |Parker Purifoy |December 3, 2020 |Washington Blade 

I disagree with Spencer on pretty much everything imaginable, but I concur on this. American Racist Richard Spencer Gets to Play the Martyr in Hungary |James Kirchick |October 7, 2014 |DAILY BEAST 

The one flavor you can always count on, however, is rich chocolate, the most chocolaty food imaginable. Dr. Mike’s Makes the Best Ice Cream on Earth |Jane & Michael Stern |July 27, 2014 |DAILY BEAST 

In 2012, he voted to reauthorize the Export-Import Bank, one of the purest excresences of crony capitalism imaginable. After Cochran’s Win: Red-State Socialism Must Be Stopped! |Nick Gillespie |June 27, 2014 |DAILY BEAST 

But it might just produce some of the more narcissistic and voyeuristic tech imaginable. We are Living in the Golden Age of the Most Narcissistic and Voyeuristic Tech Imaginable |Charles C. Johnson |January 18, 2014 |DAILY BEAST 

No imaginable circumstance could make a reasonable Ford-owner think, “Damn, I should have bought that Ferrari.” Obama’s Defiant Obamacare Defense in Boston |Michael Tomasky |October 31, 2013 |DAILY BEAST 

He insisted upon my staying a while, and we had the most amusing and entertaining conversation imaginable. Music-Study in Germany |Amy Fay 

An old weather-beaten bear-hunter stepped forward, squirting out his tobacco juice with all imaginable deliberation. Blackwood's Edinburgh Magazine, No. CCCXXXIX. January, 1844. Vol. LV. |Various 

The Americans will endeavour by all imaginable means to induce us to help them against Spain. The Philippine Islands |John Foreman 

He was kind enough to interest himself for us in the warmest manner imaginable. A Woman's Journey Round the World |Ida Pfeiffer 

In fact, to hear Skipper Worse utter the word Romarino was one of the most ludicrous things imaginable. Skipper Worse |Alexander Lange Kielland