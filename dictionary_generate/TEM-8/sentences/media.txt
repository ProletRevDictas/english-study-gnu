Direct-to-consumer brands are increasing their spending on Snapchat this year as part of the ongoing push to diversify media budgets. ‘More ad dollars move to Snapchat’: Why direct-to-consumer brands eye the platform as they diversify from Facebook |Kristina Monllos |February 26, 2021 |Digiday 

Tim Hortons didn’t disclose the amount of its latest fundraise but noted in a social media post that the proceeds will be used for opening more stores, building its digital infrastructure, brand presence and more. Tim Hortons marks two years in China with Tencent investment |Rita Liao |February 26, 2021 |TechCrunch 

Geetha Ranganathan, a media analyst for Bloomberg Intelligence, estimates that Paramount Plus will need to more than double that total to 40 million to 50 million to be successful. For ViacomCBS, another Paramount Plus challenge: How to hold on to old money while pursuing the new |Steven Zeitchik |February 25, 2021 |Washington Post 

Psychologists have even observed how people respond naturally and socially towards media artefacts like computers and televisions. Will Robots Make Good Friends? Scientists Are Already Starting to Find Out |Tony Prescott |February 25, 2021 |Singularity Hub 

SEO connects your media team to your user experience team, and collaboration between the two is necessary to bridge the gap in 2021 and beyond. SEO in 2021: What your organization’s executives and senior leaders must know |Eryck Dzotsi |February 25, 2021 |Search Engine Watch 

In 2011 LGBT media outlet Queerty took the app to task for allegedly deleting accounts that made reference to being trans. Grindr’s Trans Dating Problem |David Levesley |January 9, 2015 |DAILY BEAST 

Much of the media coverage around eating disorders surrounds celebrities and models. How Skinny Is Too Skinny? Israel Bans ‘Underweight’ Models |Carrie Arnold |January 8, 2015 |DAILY BEAST 

Unconfirmed reports in the French media claimed that the brothers were spotted at a gas station in northern France on Thursday. France Mourns—and Hunts |Nico Hines, Christopher Dickey |January 8, 2015 |DAILY BEAST 

Duke was a state representative whose neo-Nazi alliances were disgorged in media reports during his run for governor in 1991. The Price of Steve Scalise’s Silence |Jason Berry |January 7, 2015 |DAILY BEAST 

The media tend to frame situations like this as aberrations, but in this case, quite the opposite is the truth. Today’s GOP: Still Cool With Racist Pandering? |Michael Tomasky |January 7, 2015 |DAILY BEAST 

It is especially useful with cultures upon serum media, but is applicable also to the sputum. A Manual of Clinical Diagnosis |James Campbell Todd 

The gonococcus is distinguished by its failure to grow upon ordinary media. A Manual of Clinical Diagnosis |James Campbell Todd 

By far the most frequent exciting causes of acute otitis media are the pneumococcus and the streptococcus. A Manual of Clinical Diagnosis |James Campbell Todd 

The question of vernaculars as media of instruction is of national importance; neglect of the vernaculars means national suicide. Third class in Indian railways |Mahatma Gandhi 

No doubt this was due to the nature of the media in which he mainly worked, the masque and the 138 song-book. Encyclopaedia Britannica, 11th Edition, Volume 5, Slice 2 |Various