In Mongolian folk tales Przewalski’s horses were seen as the “riding mounts of the gods,” and Mongolians thus call the horses “takhi,” meaning spirit or holy. This Baby Horse Was Cloned Using DNA That Was Frozen for 40 Years |Vanessa Bates Ramirez |November 5, 2020 |Singularity Hub 

From erupting volcanoes in Italy, to remote Mongolian forests, to the Philippine Sea, this year’s winners of the Wildlife Photographer of the Year Award went above and beyond to capture images that displayed the beauty of the natural world. Check out the breathtaking winners of the 2020 Wildlife Photographer of the Year contest |Rachael Zisk |October 20, 2020 |Popular-Science 

She suspects that freedom for Mongolian women goes back at least 400 more years. Skeletons hint that ancient societies had women warriors |Bruce Bower |May 28, 2020 |Science News For Students 

Lee plans to look for female warriors in more Mongolian tombs. Skeletons hint that ancient societies had women warriors |Bruce Bower |May 28, 2020 |Science News For Students 

A Mongolian restaurant needed a prep cook to start right away. Science isn’t just for scientists |Silke Schmidt |March 5, 2020 |Science News For Students 

Under Mongolian law, any fossil specimen found in the Gobi Desert must stay in the possession of a Mongolian institution. Stopping the Million-Dollar Fossil Thieves: Illegal Trade Meets World of Insatiable Research |Scott Bixby |June 11, 2014 |DAILY BEAST 

Is he a bearded Mongolian warrior on horseback, decked out in lustrous jade and gold armor? ‘The Search for General Tso’: The Origins of America’s Favorite Chinese Dish, General Tso’s Chicken |Marlow Stern |April 19, 2014 |DAILY BEAST 

Week after week we are shaken to learn that French bistro—No, Mongolian—No, Genever gin—No, soba—is THE food trend of the moment. This Book Will Change the Way You Eat |Lucas Wittmann |December 19, 2013 |DAILY BEAST 

Certainly, similar conversations occur in miniscule Japanese apartments or Mongolian yurts, with appropriate substitutions. All Hail the French Meal |Amelia Smith |December 6, 2010 |DAILY BEAST 

The Tartars belong more especially to two distinct races, the Caucasian and the Mongolian. Michael Strogoff |Jules Verne 

The Tibetans themselves are a strong, well-built and hardy race—Mongolian in type. Mount Everest the Reconnaissance, 1921 |Charles Kenneth Howard-Bury 

It is very positively asserted by several authors that the civilization of Peru was of Mongolian origin. The Works of Hubert Howe Bancroft, Volume 5 |Hubert Howe Bancroft 

Their Mongolian physiognomy is unequivocal;—a Mongolian physiognomy but conjoined with a dark skin. The Ethnology of the British Colonies and Dependencies |Robert Gordon Latham 

I submit that all these points are Mongolian; and this is what Mr. Hodgson evidently thinks also. The Ethnology of the British Colonies and Dependencies |Robert Gordon Latham