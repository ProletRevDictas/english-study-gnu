She thereby declared herself less interested in making money than in making a difference. For Next AG, Obama Picks a Quiet Fighter With a Heavy Punch |Michael Daly |November 8, 2014 |DAILY BEAST 

They are only here to reap the rewards of the American safety net (such as it is) and thereby raise your taxes. Ebola, ISIS, the Border: So Much to Fear, So Little Time! |Gene Robinson |November 2, 2014 |DAILY BEAST 

The broader goal was to oust Saddam in order to build a beautiful democracy in the Middle East and thereby transform the region. Can America Still Win Wars? |Michael Tomasky |October 4, 2014 |DAILY BEAST 

Kanye demanded confirmation that they were indeed disabled thereby singling them out. American Association of People with Disabilities to Kanye: Concert Incident ‘Downright Offensive’ |Mark Perriello |September 15, 2014 |DAILY BEAST 

This strain of enterovirus seems unusually provocative in irritating lower airways, thereby causing airway narrowing. Midwest's 'Mystery Virus' Is Scary but Not Deadly |Kent Sepkowitz |September 8, 2014 |DAILY BEAST 

It did not in any way affect prices or wages, which were rendered neither greater nor less thereby. The Unsolved Riddle of Social Justice |Stephen Leacock 

With time this land had mounted to great values and the holders had been made well-to-do thereby. The Homesteader |Oscar Micheaux 

In the Method about to be given, the intellect is agreeably occupied, and thereby a Habit of Attention is promoted. Assimilative Memory |Marcus Dwight Larrowe (AKA Prof. A. Loisette) 

In a paroxysm of rage and fear, he gave the final order, and the Well of Cawnpore thereby attained its ghastly immortality. The Red Year |Louis Tracy 

It compels the Intellect to stay with the senses and thereby it abolishes mind-wandering. Assimilative Memory |Marcus Dwight Larrowe (AKA Prof. A. Loisette)