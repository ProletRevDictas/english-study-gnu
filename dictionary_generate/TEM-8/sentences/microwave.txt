For tight muscles, pop it in the microwave for about 20 seconds to get it nice and toasty. 4 mobility tools to help soothe and stretch your sore muscles |Stan Horaczek |February 5, 2021 |Popular-Science 

This cooker’s capacity is enough to accommodate an entire whole chicken or a 12-inch pizza, making it a great space saver and a worthy opponent to any oven or microwave. Best toaster oven: Save counter space and time with our toaster oven picks |Julian Cubilllos |February 5, 2021 |Popular-Science 

Heat in a shallow dish in the microwave on high for about 3 minutes or on the stove in a pan over low heat. Homemade condiments, dressings and toppings are a pathway to fast, flavorful cooking |Todd Kliman |February 2, 2021 |Washington Post 

She found it too troubling when she practiced reading with her daughter and noticed she could no longer read the word “microwave” like she once could. The racial disparities over who is returning to D.C. classrooms puts equity spotlight on reopening plan |Perry Stein |January 31, 2021 |Washington Post 

Melting down and remolding plastic is sort of like reheating pizza in the microwave — you get out basically what you put in, just not as good. Chemists are reimagining recycling to keep plastics out of landfills |Maria Temming |January 27, 2021 |Science News 

Microwave "pain rays" and acoustic crowd dispersal weapons already exist. Special Ops’ Weapons Wish List |Michael Peck |April 9, 2014 |DAILY BEAST 

The Army suggests that laser, microwave or acoustic weapons are the answer. Special Ops’ Weapons Wish List |Michael Peck |April 9, 2014 |DAILY BEAST 

He told the cops about the “microwave machine” whose “vibrations” kept him from being able to sleep. Aaron Alexis Was Hearing Voices a Month Before His Rampage |Michael Daly |September 18, 2013 |DAILY BEAST 

He was on an assignment in Newport when he called the police to report the voices and the big microwave machine. Aaron Alexis Was Hearing Voices a Month Before His Rampage |Michael Daly |September 18, 2013 |DAILY BEAST 

After a redesign last year, the toy now heats up like a traditional oven and looks more like a microwave. Easy-Bake Oven Mans Up: Hasbro to Manufacture Boy-Friendly Design |Abigail Pesta |December 18, 2012 |DAILY BEAST 

The microwave -- which always reeked of popcorn and spilled soup -- was right in there, on top of the miniature fridge. Little Brother |Cory Doctorow 

It's like a searchlight beam or a microwave beam, and it stays the same size like a pipe. Operation: Outer Space |William Fitzgerald Jenkins 

A tiny auto-beacon in its nose was set to send microwave signals at ten-second intervals. Operation: Outer Space |William Fitzgerald Jenkins 

This chitin diaphragm picks up the microwave like our ears pick up sound. The Kenzie Report |Mark Clifton 

He used his microwave generator—which at short enough range would short-circuit anything—upon the apparatus in the kiosk. The Pirates of Ersatz |Murray Leinster