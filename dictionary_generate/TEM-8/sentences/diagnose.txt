It’s hard not to self-diagnose when thinking about the seeds. Sunflower Seeds Are the Best Snack for the Anxious Mind |Emma Alpern |September 17, 2020 |Eater 

That was late February, when Malara diagnosed Italy’s first case of locally transmitted Covid-19. The new Covid-19 case surge in Europe, explained |Julia Belluz |September 17, 2020 |Vox 

Ultimately, he was diagnosed with a rare genetic autoimmune disorder. A Welcome Lifeline |Washington Regional Transplant Community |September 17, 2020 |Washington Blade 

Looking at the records of over 73 million patients in the US, of whom 12,033 had Covid-19, the study found those who had recently been diagnosed with a substance use disorder were significantly more at risk of Covid-19 than the average population. Substance abuse makes Covid-19 even more dangerous |Annalisa Merelli |September 16, 2020 |Quartz 

The most important thing is that now you understand how to find, diagnose, and fix this problem for your website. How to earn your place in Google’s index in 2020 |Bartosz Góralewicz |September 14, 2020 |Search Engine Land 

Levin can at least diagnose the problem and recognize that this leads to an evacuation of traditional politics. Relax—Both Parties Are Going Extinct |Nick Gillespie |November 4, 2014 |DAILY BEAST 

He was flying to a foreign country to visit gravely ill patients suffering from a disease he could no more diagnose than treat. The Original Ebola Hunter |Abby Haglage |September 14, 2014 |DAILY BEAST 

When Smith was asked how he could diagnose Obama, he admitted he is not a psychologist and was just speculating. Republican Thinks Obama’s Mental Illness Caused Border Crisis |Gideon Resnick |July 11, 2014 |DAILY BEAST 

Students study the hands to diagnose medical conditions, with the aid of technology of course. Art Goes High-Tech at These Four Innovative Exhibits |Justin Jones |May 29, 2014 |DAILY BEAST 

I felt relieved for the character, and slightly abashed for rushing to diagnose her with a mental illness. Diagnosing Jane, Louis C.K.’s Troubled Daughter on ‘Louie’ Who Can’t Separate Dreams From Reality |Russell Saunders |May 15, 2014 |DAILY BEAST 

If I diagnose or treat the fixed idea of a psychasthenic, the psychological factor itself represents the disturbance. Psychotherapy |Hugo Mnsterberg 

He could not diagnose her ailment; for who would have suspected that a young, beautiful, and rich woman was resolved to die? The Terms of Surrender |Louis Tracy 

The electrician soon learns to diagnose and prescribe for this, his most valuable charge. Heroes of the Telegraph |J. Munro 

It is for the teacher to correctly diagnose the case and administer the most efficient remedy. Piano Mastery |Harriette Brower 

Like doctors, we have to diagnose from circumstances—and even the greatest doctors are wrong at times. The Grell Mystery |Frank Froest