In his 2008 book, “Here Comes Everybody,” technologist Clay Shirky predicted how the Internet could be used to organize people without formal organizations. The country is being buffeted by groups that couldn’t exist 30 years ago |Philip Bump |January 27, 2021 |Washington Post 

Researchers, technologists, and activists told me that major social media companies have, for the entire lifetime of their history, chosen to do nothing, or to act only after their platforms cause abuse and harm. Of course you could have seen this coming |Abby Ohlheiser |January 8, 2021 |MIT Technology Review 

Officials also signed off on a privacy advisory board consisting of technologists and community members who can make recommendations. Terrible Year, Great Discoveries: Our Favorite Stories of 2020 |Randy Dotinga |December 24, 2020 |Voice of San Diego 

A group of former government technologists and IT experts rejected the judiciary’s estimate in a letter to the Judicial Conference this month. Lawmakers are trying to create a database with free access to court records. Judges are fighting against it. |Ann Marimow |December 4, 2020 |Washington Post 

She worries about the possibility of infecting her father, a motorcycle mechanic, or her mother, a medical-imaging technologist. College students hit the road after an eerie pandemic semester. Will the virus go home with them? |Nick Anderson, Susan Svrluga |November 22, 2020 |Washington Post 

Get to know E2E (end to end) encryption, says Dan Auerbach, a staff technologist at the Electronic Frontier Foundation. How to Keep the NSA at Bay: The Tricks From Privacy Experts |Winston Ross |June 8, 2013 |DAILY BEAST 

As an investor, a technologist, and an entrepreneur, Andreessen deserves a place in the World Wide Web Hall of Fame. HP Board Member Marc Andreessen, an Internet Pioneer, Deserves Some of the Blame for the Company’s Failures |Rob Cox |November 21, 2012 |DAILY BEAST 

All of which needed to happen while Andy waited by the phone: Dom, a technologist with the city, would call when it was time. Wedding Bells at N.Y. City Hall |Jessica Bennett |July 25, 2011 |DAILY BEAST 

Peter Hartwell is a distinguished technologist and the lead on HP Labs Central Nervous System for the Earth project (CeNSE). Sensing Our Planet |Daily Beast Promotions |October 20, 2010 |DAILY BEAST 

The account rendered by the technologist with respect to the light oils of the tar is thus a pretty good one. Coal |Raphael Meldola 

It has been expressly written for the general reader and for the technologist in other branches of industry. The Manufacture of Paper |Robert Walter Sindall 

The wish was expressed that I should stay in England and become a technologist, but I was too much attached to home. Appletons' Popular Science Monthly, January 1899 |Various 

The technologist began to cry suddenly in a low, sobbing voice. Human Error |Raymond F. Jones 

This method brings the technologist into competition with Nature, and we shall see the result. Coal |Raphael Meldola