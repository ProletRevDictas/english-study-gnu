User error accounts for the majority of gear failure in the field, so learning how your tent or bedding sets up before you leave home can save you heaps of time and stress when you’re depending on it. How to Pack for a Family Camping Trip |Joe Jackson |October 1, 2020 |Outside Online 

It’s notably lacking in the generous heaps of grated fresh ginger I know Sammy adds to his blend, but still, it’s excellent. Until I Can Go Back to My Favorite Restaurant, This Jerk Paste Is the Next Best Thing |Elazar Sontag |September 25, 2020 |Eater 

Where this has proved impossible, we’ve outsourced the process using barrels, jars, compost heaps, and industrial fermenters. The Fungal Evangelist Who Would Save the Bees - Issue 90: Something Green |Merlin Sheldrake |September 23, 2020 |Nautilus 

These include human bones, ancient buildings, ancient trash heaps and more. Scientists Say: Archaeology |Bethany Brookshire |August 10, 2020 |Science News For Students 

Another plus is that busy microbes in a compost heap put out a lot of heat. Greener than burial? Turning human bodies into worm food |Laura Sanders |April 3, 2020 |Science News For Students 

The problem runs far deeper, to an absurdly narrow legal definition of ‘corruption’ that throws democracy on the trash heap. Undo Citizens United? We’d Only Scratch the Surface |Jedediah Purdy |November 12, 2014 |DAILY BEAST 

Since then, Abilify has risen from the fifth-most-prescribed drug to the top of the heap. Mother’s Little Anti-Psychotic Is Worth $6.9 Billion A Year |Jay Michaelson |November 9, 2014 |DAILY BEAST 

And Ebola, in turn, has returned to the top of the local talking-points heap. Dallas: A Journal of the Plague City |Pete Freedman |October 17, 2014 |DAILY BEAST 

He snatched up the thrown chair and crashed it down onto the head of a charging older black man, who crumpled into a heap. Inside a Hospital for the Criminally Insane |Caitlin Dickson |September 15, 2014 |DAILY BEAST 

This regrettable action will, of course, ignite a racial gang war, leaving a heap of bodies in its wake. Inside 'Sons of Anarchy's' Final Season: Creator Kurt Sutter on the Most Brutal Season Yet |Annaliza Savage |September 10, 2014 |DAILY BEAST 

But here at Fort Walsh we're among a class of people that are a heap different from Texas cow-punchers. Raw Gold |Bertrand W. Sinclair 

At this point Harry entered and stood afar off, eying Punch, a disheveled heap in the corner of the room, with disgust. Kipling Stories and Poems Every Child Should Know, Book II |Rudyard Kipling 

If they'd been white men I would probably have been curled in a neat heap within two hundred yards. Raw Gold |Bertrand W. Sinclair 

You'll be a heap more sane when you get that old, wild-west notion, that every man should be a law unto himself, out of your head. Raw Gold |Bertrand W. Sinclair 

He gathered the heap and flung it into a corner, then caught up his hat and struck out for the loneliest part of the ranch. Ancestors |Gertrude Atherton