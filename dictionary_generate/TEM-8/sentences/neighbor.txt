It proves that Delaware can show America what is possible when neighbors come together. Sarah McBride wins primary, likely to be first out trans state senator in U.S. |Chris Johnson |September 16, 2020 |Washington Blade 

Everything else stayed the same—their commute, where they shopped for groceries, who their neighbors were, etc. How a vacation—or a pandemic—can help you adopt better habits now |matthewheimer |September 12, 2020 |Fortune 

Several of its neighbors in Louisiana’s industrial corridor also rank near the top of the list. New Research Shows Disproportionate Rate of Coronavirus Deaths in Polluted Areas |by Lylla Younes, ProPublica, and Sara Sneath |September 11, 2020 |ProPublica 

Other circular earthworks of the ancestral Wichita and neighboring groups in the southern Great Plains were also built at elevated spots, Casana’s team says. Drones find signs of a Native American ‘Great Settlement’ beneath a Kansas pasture |Bruce Bower |September 10, 2020 |Science News 

That makes us better off than many of our friends and neighbors. A California mountain community loses its heart |Mike Silverstein |September 10, 2020 |Washington Blade 

At his Tucson hacienda he is a gracious host and a good neighbor. The Story Behind Lee Marvin’s Liberty Valance Smile |Robert Ward |January 3, 2015 |DAILY BEAST 

After a pause she invited me in with a warm smile, as if I were a neighbor. The Life and Hard Times Of The Family A Cuban Defector Left Behind |Brin-Jonathan Butler |December 19, 2014 |DAILY BEAST 

Sure, your cubicle mate, neighbor, and aunt all own a Fitbit or JawBone fitness tracker. Nothing Says I Love You Like Data |The Daily Beast |December 8, 2014 |DAILY BEAST 

Selling off the extras, I saw my neighbor marvel at the scent and murmur that he wished he could afford one. A Million Ways to Die in Prison |Daniel Genis |December 8, 2014 |DAILY BEAST 

And Hayao Miyazaki, the 73-year-old director behind hits like My Neighbor Totoro and Spirited Away, is a walking legend. Anime King Hayao Miyazaki’s Cursed Dreams |Melissa Leon |December 2, 2014 |DAILY BEAST 

It was father against son, brother against brother, neighbor against neighbor. The Courier of the Ozarks |Byron A. Dunn 

She passed the box to her neighbor, who uttered similar expressions to her own. Tobacco; Its History, Varieties, Culture, Manufacture and Commerce |E. R. Billings. 

It is founded upon love of the neighbor, and a desire to be beloved, and to show love. The Ladies' Book of Etiquette, and Manual of Politeness |Florence Hartley 

When what does my gentleman do but whips out an 'oss pistil as long as my harm, and shoots my left 'and neighbor dead! The Book of Anecdotes and Budget of Fun; |Various 

The four shook hands solemnly with their new neighbor, then, with even a greater gusto, drank his health. Ancestors |Gertrude Atherton