We spoke with academic and industry experts to find out what communicative skills are necessary to navigate these new waters successfully and lead happy, productive teams. Breaking old habits: Hybrid-working setups call for different ways of communicating |Jessica Davies |August 27, 2021 |Digiday 

It didn’t deny that there have been times when YouTube was less communicative, less transparent or slower than some rivals to respond to content controversies. ‘YouTube magic dust’: How America’s second-largest social platform ducks controversies |Will Oremus |August 25, 2021 |Washington Post 

It functions as a communicative social tool, while at the same time, some shoes make it comfortable to walk around in. These Shoes Are Killing Me! (Ep. 296 Rebroadcast) |Stephen J. Dubner |May 20, 2021 |Freakonomics 

“Our idea is to be a communicative leader, an incredible partner and we really want to future-proof and transform our brand and rejuvenate our brand,” Schoerner added. Case Study: How BMW Group broke into the esports market |Sara Jerde |April 16, 2021 |Digiday 

A persona is an imaginary figure representing a segment of real people, and it is a communicative design technique aimed at enhanced user understanding. Making better decisions with big data personas |Martha Leibs |March 11, 2021 |MIT Technology Review 

Experiments showed that chimps, the animal closest to humans, were hopeless at reading what Hare calls ‘communicative intention’. The Genius of Dogs |David Frum |March 10, 2013 |DAILY BEAST 

Recurrent sociological interpretations emphasize the communicative value of crying. Why Do We Cry? |Michael Trimble |January 10, 2013 |DAILY BEAST 

“So settled, so still, so serene / Completely vegetative, cumcumbive, or potative, non-communicative, and green …” Bonkers. ‘American Horror Story’ Sings “The Name Game” and 12 Other Bizarre TV Musical Numbers (VIDEO) |Kevin Fallon |January 8, 2013 |DAILY BEAST 

Armstrong was only sporadically communicative during the last two weeks of her life. Why I Almost Killed Myself—And My Children |Daleen Berry |April 16, 2011 |DAILY BEAST 

Malcolm understood instantly that his native companion had found the ekka-wallah more communicative. The Red Year |Louis Tracy 

Seeing that his friend did not intend to be communicative the boy wisely changed the subject. The Garret and the Garden |R.M. Ballantyne 

Mazaroff had been too communicative in the hour of his supposed triumph, and he had told Varney everything. The Weight of the Crown |Fred M. White 

She was busy during every moment while in the country, and her relative was no more communicative than of yore. Ancestors |Gertrude Atherton 

He kept repeating to himself that, if he lost his temper, she would never become communicative. The Winning Clue |James Hay, Jr.