When she rose to leave, he reminded her of the effort he’d put into advocating for her over the years. The Woman Propositioned by Alaska’s Former Lieutenant Governor Tells Her Story for the First Time |by Kyle Hopkins and Michelle Theriault Boots, Anchorage Daily News |September 10, 2020 |ProPublica 

It reminded me of my colleagues in Tokyo a decade earlier, who would go out drinking after work and then come back to the office for a free car-service ride home so they wouldn’t have to take public transportation—not because they were working late. The complaints of the entitled workers of Silicon Valley |Adam Lashinsky |September 8, 2020 |Fortune 

They remind voters to mail back their absentee ballots and when Election Day voting begins and ends. Facebook’s Political Ad Ban Also Threatens Ability to Spread Accurate Information on How to Vote |by Jeremy B. Merrill for ProPublica |September 4, 2020 |ProPublica 

Import it to your Google Calendar, and it’ll remind you exactly when to sign up for, then cancel, each trial service to avoid being charged. Want a year of free streaming services? This guide has you covered |dzanemorris |August 26, 2020 |Fortune 

In some cases, she reminded the attorney general of the global pandemic, saying she needed to work at a second job to make ends meet or that she was caring for family members. Alaska’s Attorney General on Unpaid Leave After Sending Hundreds of “Uncomfortable” Texts to a Young Colleague |by Kyle Hopkins, Anchorage Daily News |August 25, 2020 |ProPublica 

But the ads are not just intended to remind the Google-curious that Paul exists and is thinking about running for president. Rand Paul’s Passive-Aggressive Trolling Campaign |Olivia Nuzzi |January 6, 2015 |DAILY BEAST 

They had also come “to remind America of the fierce urgency of now.” How Martin Luther King Jr. Influenced Sam Cooke’s ‘A Change Is Gonna Come’ |Peter Guralnick |December 28, 2014 |DAILY BEAST 

You remind us that men and women have imperfection in common, and are indivisible. Living Black & Gay in the ’50s |Alain Mabanckou |December 3, 2014 |DAILY BEAST 

Sciamma still wants to remind us that they are still children. ‘Girlhood’: Coming of Age in France’s Projects |Molly Hannon |November 25, 2014 |DAILY BEAST 

I remind Deen that his namesake died in an infamously horrible car crash, so he may want to cool it on texting and driving. My Bizarre Night With James Deen, Libertarian Porn Star |Emily Shire |November 12, 2014 |DAILY BEAST 

Other pictures by this artist remind one of the works of Botticelli. Women in the fine arts, from the Seventh Century B.C. to the Twentieth Century A.D. |Clara Erskine Clement 

She had just come to forty-nine, and was wondering if she might remind the fairy father of his duty, when the door opened. Rosemary in Search of a Father |C. N. Williamson 

But you may cynically remind me that nothing will come of the Determinists' protest against the evil social conditions. God and my Neighbour |Robert Blatchford 

They will be duly appreciated, and remind her constantly of the pleasures of your visit. The Ladies' Book of Etiquette, and Manual of Politeness |Florence Hartley 

"I haven't seen you since I came here, Mr. Courthorne, and now you remind me of another man I once had dealings with," he said. Winston of the Prairie |Harold Bindloss