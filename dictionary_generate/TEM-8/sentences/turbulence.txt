As with many companies in the media sector, Taboola and Outbrain’s businesses suffered turbulence as the coronavirus took hold in March and advertisers reined in their spending in the initial months of the pandemic. Why the Taboola-Outbrain deal fell apart and what it means for publishers |Lara O'Reilly |September 9, 2020 |Digiday 

Unlike every other instance of turbulence that has ever been observed on Earth, Irvine’s blob isn’t a messy patch in a flowing stream of liquid, gas or plasma, or up against a wall. An Unexpected Twist Lights Up the Secrets of Turbulence |David H. Freedman |September 3, 2020 |Quanta Magazine 

Irvine and Matsuzawa tightly control the loops that are the blob’s building blocks and study the resulting confined turbulence up close and at length. An Unexpected Twist Lights Up the Secrets of Turbulence |David H. Freedman |September 3, 2020 |Quanta Magazine 

The blob is a cloud of turbulence in a large water tank in the lab of the University of Chicago physicist William Irvine. An Unexpected Twist Lights Up the Secrets of Turbulence |David H. Freedman |September 3, 2020 |Quanta Magazine 

He argued that central banks had gone from “volatility machines” to hyperactive suppressors of economic and financial turbulence. Hedge Fund ‘Pirates’ Set Sail Again |Daniel Malloy |August 21, 2020 |Ozy 

Turbulence begins, and the only thing in the world we want is to not be on this damn plane. The Malaysian Air Tragedy Reawakens a Primal Fear |Kelly Williams Brown |July 19, 2014 |DAILY BEAST 

Could his recent turbulence also explain why Efron spends most of his upcoming movie, Neighbors, with his body on display? Zac Efron’s Eyes Are Up Here, Ladies |Tim Teeman |April 15, 2014 |DAILY BEAST 

No matter how they died, there is certainly turbulence ahead in North Korea—and the Chinese are running for cover. North Korea's Monstrous Murder Methods |Gordon G. Chang |January 5, 2014 |DAILY BEAST 

Returning to the U.S., I was mired in ideological turbulence. My Former Life As a Kahanist Posterboy |Zack Parker |June 17, 2013 |DAILY BEAST 

I saw two fold-out seats used by flight attendants during takeoff, landing and turbulence. The National-Security Diaper Scramble |Philip Shishkin |April 25, 2013 |DAILY BEAST 

From the depths of his mind came a warning, a restless unease that took root and blossomed into turbulence. We're Friends, Now |Henry Hasse 

The year 1846 closed over the Iberian peninsula in discord, turbulence, and woe. The History of England in Three Volumes, Vol.III. |E. Farr and E. H. Nolan 

Calumny, with its hundred tongues, exaggerated the turbulence of the people, and invented wild tales of violence. The Loyalists of America and Their Times, Vol. 1 of 2 |Egerton Ryerson 

The easygoing paternal rule was to come to an end, and a long period of bloodshed and turbulence was to succeed. Argentina |W. A. Hirst 

To this desperate resolve he was driven by the increasing turbulence of Italian affairs. A History of the Nineteenth Century, Year by Year |Edwin Emerson