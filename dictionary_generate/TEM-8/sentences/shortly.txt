Corado said she arrived on the scene and approached the two women shortly after the incident began. D.C. police investigating threats against Casa Ruby |Lou Chibbaro Jr. |August 26, 2020 |Washington Blade 

Their about-face came shortly after the judge rejected their bid to dismiss the case over allegations of misconduct by federal agents. Lori Loughlin gets two months in prison after judge accepts plea deal in college bribery scandal |radmarya |August 21, 2020 |Fortune 

He still visits the country most summers, and was there shortly before the coronavirus pandemic began. Backing Police Who Lay Down Their Arms in Belarus |Nick Fouriezos |August 18, 2020 |Ozy 

The new program went live for residents of the Isle of Wight on Thursday, August 13, and will shortly become available for people living in the London borough of Newham, according to the UK’s health department. England has started testing a contact tracing app—again |Charlotte Jee |August 13, 2020 |MIT Technology Review 

Alibaba bought the paper in 2015, and brought its paywall down shortly after. South China Morning Post CEO Gary Liu on navigating a perilous time for Hong Kong |Pierre Bienaimé |July 14, 2020 |Digiday 

Shortly after dawn, there was another outbreak of deadly force. France Mourns—and Hunts |Nico Hines, Christopher Dickey |January 8, 2015 |DAILY BEAST 

“We broke off shortly after because we were more ambitious,” says Lean. The Cult of Yung Lean: ‘I’m Building An Anarchistic Society From the Ground Up’ |Marlow Stern |January 4, 2015 |DAILY BEAST 

Crain posted a cash bond of $102.50 apiece shortly before 1:30 P.M., and they returned to the Castle Hotel. How Martin Luther King Jr. Influenced Sam Cooke’s ‘A Change Is Gonna Come’ |Peter Guralnick |December 28, 2014 |DAILY BEAST 

Shortly after the base fell militants started tweeting photographs of MANPADS, both the Russian-made SA-16 and SA-18. Did ISIS Shoot Down a Fighter Jet? |Jamie Dettmer, Christopher Dickey |December 24, 2014 |DAILY BEAST 

Shortly thereafter, Facebook announced a similar initiative, although their plan calls for the use of drones instead of balloons. Silicon Valley Sets Its Sights on Africa |Christian Borys |December 22, 2014 |DAILY BEAST 

His lordship retired shortly to his study, Hetton and Mr. Haggard betook themselves to the billiard-room. The Pit Town Coronet, Volume I (of 3) |Charles James Wills 

He returned shortly, to meet his mother standing in the doorway, with pale, affrighted face. Ramona |Helen Hunt Jackson 

Shortly afterwards a few acceptances were reported, principally against securities. Readings in Money and Banking |Chester Arthur Phillips 

Ten thousand of the best troops in Mexico entered Texas and were shortly to be followed by ten thousand more. Blackwood's Edinburgh Magazine, No. CCCXXXIX. January, 1844. Vol. LV. |Various 

Shortly after she came to her lesson limping, and remarked that she felt very uncomfortable. Children's Ways |James Sully