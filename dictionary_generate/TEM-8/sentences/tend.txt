FanGraphs analyst Jeff Zimmerman found pitchers who maintain their velocity tend to maintain their performance. Jacob DeGrom Just Keeps Throwing Faster |Travis Sawchik |September 17, 2020 |FiveThirtyEight 

Such businesses, according to the Federal Reserve’s Small Business Credit Survey, tend to be minority-owned. Opportunity Zones haven’t fully reached their potential, but don’t write them off yet |jakemeth |September 16, 2020 |Fortune 

“Historically, corrections in the put-call ratio have tended to have sharp but short-lived market impacts,” the strategists wrote. Why Goldman Sachs and Deutsche Bank think the September stocks sell-off has run its course |Bernhard Warner |September 14, 2020 |Fortune 

As tends to happen when demand skyrockets but supply doesn't, prices on a wide range of items went up. Price gouging and defective products rampant on Amazon, reports find |Kate Cox |September 11, 2020 |Ars Technica 

Most advertisers tend to be wowed by Amazon’s ability to drive direct response. ‘Amazon is a brand play for us’: How Buick is building a long-term partnership around Amazon’s ad business |Seb Joseph |September 10, 2020 |Digiday 

There are reasons that European countries tend to avoid fluoride. Anti-Fluoriders Are The OG Anti-Vaxxers |Michael Schulson |July 27, 2016 |DAILY BEAST 

Tend to your own garden, to quote the great sage of free speech, Voltaire, and invite people to follow your example. How the PC Police Threaten Free Speech |Nick Gillespie |January 9, 2015 |DAILY BEAST 

The media tend to frame situations like this as aberrations, but in this case, quite the opposite is the truth. Today’s GOP: Still Cool With Racist Pandering? |Michael Tomasky |January 7, 2015 |DAILY BEAST 

They excite people, and primaries tend to be dominated by voters who are the most excited. The Devil in Mike Huckabee |Dean Obeidallah |January 6, 2015 |DAILY BEAST 

We tend to think not, but the rise of King, Kennedy, and Lincoln was unlikely, too. No Gods, No Cops, No Masters |James Poulos |January 1, 2015 |DAILY BEAST 

This relation carefully noticed will tend to hold the lines together. Assimilative Memory |Marcus Dwight Larrowe (AKA Prof. A. Loisette) 

My own opportunities have been very limited, yet so far as they go they tend to maintain the justice of this remark. Glances at Europe |Horace Greeley 

She did not yet know how necessary climbing might be, in her new country life, but her aspirations did not tend that way. Dorothy at Skyrie |Evelyn Raymond 

Shooting guerrillas after they were caught and burning houses did not tend to make those left less cruel. The Courier of the Ozarks |Byron A. Dunn 

Such a course would tend only to bloody and interminable anarchy. Blackwood's Edinburgh Magazine, No. CCCXXXIX. January, 1844. Vol. LV. |Various