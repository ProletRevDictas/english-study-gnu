She is content with having her routine exude her Blackness because “America is always reminding” her of it. Her Name’s ‘French.’ Her Humor’s Universal. |Toyloy Brown III |July 31, 2021 |Ozy 

They fed us, decent enough I must add, and made us wait for hours in that cubicle that was beginning to exude a smell that was not very pleasant. Locked up in the Land of Liberty: Part I |Yariel Valdés González |July 7, 2021 |Washington Blade 

We chose the historic Carolina Hotel, which opened in 1901 and exudes Southern charm, to start our days, and had dinner at a different restaurant each night. In North Carolina, a visit to ‘the cradle of American golf’ |Chris Lindsley |June 25, 2021 |Washington Post 

She exudes youth pastor energy but sprinkles in her own personal brand of Bible-thumping theatricality that makes it nearly impossible to believe that she isn’t carrying out an extreme act of satire. Why Is TikTok Turning a Hateful Radical Evangelist into a Viral Star? |Hannah Jackson |June 7, 2021 |The Daily Beast 

I willed myself to exude the confidence I’d started gathering at home. My Pandemic Baby Is Pulling Us Out of Our Cozy Cave. But How Will the World See a Disabled Mother Like Me? |Rebekah Taussig |April 28, 2021 |Time 

“Her compositions exude a rigor and tightness,” said Lori Bookstein, whose Chelsea gallery has represented Malcolm since 2003. The Artist Formerly Known as Janet Malcolm |Lauren Du Graf |June 6, 2013 |DAILY BEAST 

The rocker chicks from Deap Vally—akin to an all-girl White Stripes duo—exude a badass demeanor in this retro-fitted video. Selena Gomez, Lil Wayne & More Best Music Videos of the Week (VIDEO) |Jean Trinh |May 10, 2013 |DAILY BEAST 

Like the other Obama surrogates hitting the airwaves today, Gibbs was doing his best to conceal nervousness and exude confidence. Robert Gibbs Plans a Good Cry |Lloyd Grove |November 6, 2012 |DAILY BEAST 

But the clothes exude a confidence and dignity that is more eloquent than any political treatise. Chanel, Armani, and Givenchy Present Their Haute-Couture Collections in Paris |Robin Givhan |July 4, 2012 |DAILY BEAST 

They exude strength, but the romance and sensuality of fashion are not lost. Chanel, Armani, and Givenchy Present Their Haute-Couture Collections in Paris |Robin Givhan |July 4, 2012 |DAILY BEAST 

We elephants never fear anyone or hate anyone and that is why we exude no stench, but a tiger has to live by killing. Kari the Elephant |Dhan Gopal Mukerji 

Sea-sand is excluded from the mortar employed, on account of its tendency to imbibe and exude moisture. Chambers's Journal of Popular Literature, Science, and Art |Various 

At night they could exude a vapor which was capable of dissolving the material from which the clothing had been made. Student Body |Floyd L. Wallace 

This liquid may be seen to exude, under different circumstances, from the trunk of the gnat, like a drop of very clear water. The Insect World |Louis Figuier 

Flies feed principally on fluids which exude from the bodies of animals; that is, sweat, saliva, and other secretions. The Insect World |Louis Figuier