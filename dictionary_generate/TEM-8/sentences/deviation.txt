I plugged into my calculator the mean, the standard deviation and the number of plants in each group. How to grow your own science experiment |Bethany Brookshire |December 9, 2020 |Science News For Students 

In the set of graphs tracking the pandemic above, you may have noticed similar blips in recent days, deviations probably due to data gaps because of the Thanksgiving holiday. The pandemic’s death toll is a tsunami anyone could see coming — if they looked |Philip Bump |December 3, 2020 |Washington Post 

Then I put my mean, standard deviation and number of samples into this online calculator. Build ice towers with bottled water and ice |Bethany Brookshire |September 16, 2020 |Science News For Students 

The previous version had referred to “deviations from its maximum level.” Low interest rates may be here to stay, following Fed Chief Powell’s new approach on inflation |Lee Clifford |August 27, 2020 |Fortune 

We often have this notion that male bodies are the default and women’s bodies are a deviation from that default. Males Are the Taller Sex. Estrogen, Not Fights for Mates, May Be Why. |Christie Wilcox |June 8, 2020 |Quanta Magazine 

If you are pro-life in Missouri it means being so without deviation, Molendorp says. Abortion in Missouri Is the Wait of a Lifetime |Justin Glawe |November 12, 2014 |DAILY BEAST 

To be in the adult industry some level of sexual deviation has to occur. Porn Stars Are People Too, Dammit: Lisa Ann’s Notre Dame Date and the Trolling of David Gregory |Aurora Snow |October 25, 2014 |DAILY BEAST 

And if you feel that way then you will look at every deviation as betrayal of fate, calling, etc. Sunday Q&A: Josef Joffe on the Myth of American Decline |Michael Moynihan |November 17, 2013 |DAILY BEAST 

“The people of Egypt are against this deviation being carried out by the leaders of the army,” he added. In Deeply Polarized Egypt, Revolution's Social Justice Slogans are Seldom Heard |Jesse Rosenfeld |October 10, 2013 |DAILY BEAST 

Palestinian Muslims, for their part, are extremely wary of any deviation from the status quo. Why Jews Should Be Allowed to Pray On the Temple Mount |Zack Parker |June 25, 2013 |DAILY BEAST 

Thus the coming of man indicated, in two directions, an extraordinary deviation from the ordinary course of animal development. Man And His Ancestor |Charles Morris 

Occasionally (as at Birdoswald) there was a deviation, and the older work survived. Encyclopaedia Britannica, 11th Edition, Volume 4, Part 3 |Various 

The sharp ridge in the center provides for a deviation of the water jet as it flows on the bucket. The Boy Mechanic, Book 2 |Various 

It would be impolitic to jeopardize his whole ambition by any deviation from the letter of the Erfurt agreement. The Life of Napoleon Bonaparte |William Milligan Sloane 

This deviation of art from its true and high vocation took place everywhere, and even in connection with Christianity. Tolstoy on Shakespeare |Leo Tolstoy