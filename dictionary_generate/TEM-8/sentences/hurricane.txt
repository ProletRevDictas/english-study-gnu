Sociologists estimate that people remember the worst effects of a hurricane for just seven years, and that 85 percent of US coastal residents haven’t actually experienced a direct hit from a major hurricane before. Hurricane Laura is the strongest storm to hit Louisiana in more than a century |Sara Chodosh |August 28, 2020 |Popular-Science 

These storms have winds rivaling the strength of a hurricane or tornado, but push forward in one direction instead of rotating. What’s behind August 2020’s extreme weather? Climate change and bad luck |Carolyn Gramling |August 27, 2020 |Science News 

“It seems that hurricanes are immensely important,” Donihue says. Analyze This: Hurricanes may help lizards evolve better grips |Carolyn Wilke |August 26, 2020 |Science News For Students 

A hurricane around the center of a galaxy called J0230 has winds that howl at one-fifth of the speed of light. Let’s learn about hurricanes |Bethany Brookshire |July 15, 2020 |Science News For Students 

For instance, more water now gets dumped on cities during hurricanes. Let’s learn about rain |Bethany Brookshire |June 17, 2020 |Science News For Students 

“We dealt with so many problems when we were shooting in New York, like trying to shoot during Hurricane Sandy,” says Esco. ‘Free The Nipple’: (You Gotta) Fight for Your Right to Go Topless |Lizzie Crocker |December 12, 2014 |DAILY BEAST 

The absent turkey had been blown clean away in the hurricane force winds, I concluded. Confessions of a Turkey Killer |Tom Sykes |November 26, 2014 |DAILY BEAST 

Leaving the moribund Eddie, Frank crosses paths with a black deliveryman, and they talk about hurricane survivors. Richard Ford’s Artful Survivalist Guide: The Return of Frank Bascombe |Tom LeClair |November 4, 2014 |DAILY BEAST 

Ann is only one of many “hurricane conspirators” who believe the storm has changed everything. Richard Ford’s Artful Survivalist Guide: The Return of Frank Bascombe |Tom LeClair |November 4, 2014 |DAILY BEAST 

Inhofe said the two disasters were different because the hurricane drew so many moochers. If You Think D.C. Is Awful Now, Wait Until Wednesday |Jonathan Alter |November 4, 2014 |DAILY BEAST 

Maybe it didn't feel good to be on the hurricane deck of a good horse once more! Raw Gold |Bertrand W. Sinclair 

In a few days a new home was ready and the terrible hurricane forgotten by the carefree, happy little boy. Alila, Our Little Philippine Cousin |Mary Hazelton Wade 

You also remember that last summer a hurricane destroyed the boy's home, and a new one had to be built. Alila, Our Little Philippine Cousin |Mary Hazelton Wade 

They are, however, much less energetic, and often of greater size than the hurricane whirl. Outlines of the Earth's History |Nathaniel Southgate Shaler 

Many a wild hurricane has spent its force on this tree of life, but has only caused it to strike its roots deeper. Gospel Philosophy |J. H. Ward