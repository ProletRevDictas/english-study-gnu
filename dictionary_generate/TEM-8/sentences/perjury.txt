In 2019, the board recommended police agencies drop a requirement that people identify themselves and sign their complaints under penalty of perjury, which has a chilling effect. Why Complaints Against the Sheriff’s Department Are Surging |Jesse Marx |December 22, 2020 |Voice of San Diego 

These were sworn testimonials of what people saw, statements subject to perjury. If there is any real evidence of fraud, Mr. President, it’s time to put up |Philip Bump |November 28, 2020 |Washington Post 

For Murphy, Nielsen’s untruths in front of Congress amounted to potential perjury — a criminal charge. What to make of the DHS whistleblower’s shocking complaint |Alex Ward |September 11, 2020 |Vox 

He demanded the journalists “immediately correct the record” and included two depositions attesting under penalty of perjury that the version of the document NBC 7 published was doctored. Morning Report: Hotel Workers Want Their Jobs Back |Voice of San Diego |September 8, 2020 |Voice of San Diego 

San Diego’s district attorney could also choose to press charges that might be related to safeguarding public funds or perjury. Morning Report: Report Finds Evidence of Fraud in Sweetwater Budget Mess |Voice of San Diego |June 23, 2020 |Voice of San Diego 

The charges included corruption, perjury, bid-fixing and fraud. Madonna, Carla Bruni & Obama Abandoned Pledges To Rebuild L'Aquila After The Quake |Barbie Latza Nadeau |November 18, 2014 |DAILY BEAST 

Then, after the headlines came out, the sources recanted, and they have since been convicted (in Syrian courts) of perjury. Digital Doublethink: Playing Truth or Dare with Putin, Assad and ISIS |Christopher Dickey, Anna Nemtsova |November 16, 2014 |DAILY BEAST 

The woman was acquitted of perjury, which could have landed the mother of three 15 years in jail. The Democrats' Katherine Harris Strategy |Patricia Murphy |September 6, 2014 |DAILY BEAST 

He was convicted of perjury, served 30 days, and went back to a swashbuckling career in contraband. The Weirdest Story About a Conservative Obsession, a Convicted Bomber, and Taylor Swift You Have Ever Read |David Weigel |August 30, 2014 |DAILY BEAST 

Beaird seemed to be either admitting perjury or committing it. The Day Ferguson Cops Were Caught in a Bloody Lie |Michael Daly |August 15, 2014 |DAILY BEAST 

He was voluble in his declarations that they would “put the screws” to Ollie on the charge of perjury. The Bondboy |George W. (George Washington) Ogden 

So he bore down on the solemn declaration that she stood face to face with a prison term for perjury. The Bondboy |George W. (George Washington) Ogden 

This unheard-of despotism, this horrible political perjury, was certainly not merited by the good and generous Brazil. Journal of a Voyage to Brazil |Maria Graham 

This thing called Secession originated in falsehood, theft and perjury. Portrait and Biography of Parson Brownlow, The Tennessee Patriot |William Gannaway Brownlow 

They were subject to a single will; moved often by perjury, and sometimes by passion. The History of Tasmania , Volume II (of 2) |John West