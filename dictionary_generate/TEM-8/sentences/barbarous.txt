Rome’s story then does not warn us of the danger of barbarous outsiders toppling a society from within. Rome Didn't Fall When You Think It Did. Here's Why That Fabricated History Still Matters Today |Edward J. Watts |October 6, 2021 |Time 

The collective trauma created by these barbarous acts is impossible to imagine, both in the U.S. and in the Middle East. One Former Hostage Says Negotiate With ISIS, And Pay Ransoms If You Must |Sarah Shourd |September 6, 2014 |DAILY BEAST 

Orwellian, Malthusian, barbarous, depraved…I think you get me. Conservatives Find Typo in Obamacare, Try to Kill People With It |Michael Tomasky |July 22, 2014 |DAILY BEAST 

They wanted members of the public to see the consequence of what can only be described as their barbarous acts. U.K. Beheading Trial’s Grisly Start |Nico Hines |November 30, 2013 |DAILY BEAST 

However, very little information is gleaned from these barbarous methods. ‘Zero Dark Thirty’ Doesn’t Promote Torture |Marlow Stern |December 11, 2012 |DAILY BEAST 

The Barbarous Years, the long-awaited companion to Voyagers to the West, is an even greater achievement. “The Barbarous Years”: What 17th-Century America Really Looked Like |R.B. Bernstein |November 22, 2012 |DAILY BEAST 

I have been accused of showing irreverence towards these barbarous kings and priests. God and my Neighbour |Robert Blatchford 

He wrote verses with elegance in French, Spanish and Italian, and was a polisher of his native language in a barbarous age. The Every Day Book of History and Chronology |Joel Munsell 

Edward sent him to London, 'fettered on a hackney,' to undergo the same barbarous death as his heroic brother. King Robert the Bruce |A. F. Murison 

Curiosity induced Mr. Cunningham and myself to view this barbarous feast and we landed about ten minutes after it had commenced. Narrative of a Survey of the Intertropical and Western Coasts of Australia] [Volume 2 of 2] |Phillip Parker King 

Their garb, their gestures, their salutations, had a wild and barbarous character. The History of England from the Accession of James II. |Thomas Babington Macaulay