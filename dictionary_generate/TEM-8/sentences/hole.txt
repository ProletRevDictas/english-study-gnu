Turns out the watering hole is locally owned and has been around for more than 80 years. The Last Bar Standing? |Eugene Robinson |September 15, 2020 |Ozy 

Many security holes can be fixed in time for November if states have the technical expertise to do so. Voting by mail is more secure than the President says. How to make it even safer |matthewheimer |September 13, 2020 |Fortune 

It performed best in the crashing waves but could also be surfed in some of the small holes off the current. The Gear You Need to Bring on a 225-Mile River Trip |Mitch Breton |September 6, 2020 |Outside Online 

It’s also important to remember that we’re still very, very deep in the hole we fell into in April, and that job growth in the private sector is slowing. The Easy Part Of The Economic Recovery Might Be Over |Amelia Thomson-DeVeaux |September 4, 2020 |FiveThirtyEight 

Some sheaths include holes in their perimeter, allowing you weave paracord through them to create easily customizable mounts. Three Questions to Ask Yourself Before Buying a Knife |Wes Siler |September 3, 2020 |Outside Online 

His monotonous music is, really, like the audio soundtrack to a k-hole. The Cult of Yung Lean: ‘I’m Building An Anarchistic Society From the Ground Up’ |Marlow Stern |January 4, 2015 |DAILY BEAST 

I rolled him over to see where it came out, and there was no big hole in the back. The Story Behind Lee Marvin’s Liberty Valance Smile |Robert Ward |January 3, 2015 |DAILY BEAST 

Well over a thousand holes in, I average less than four strokes per hole. Lost For Thousands of Strokes: 'Desert Golfing' Is 'Angry Birds' as Modern Art |Alec Kubas-Meyer |January 2, 2015 |DAILY BEAST 

Instead of going for the hole, I hit the ball directly into the water. Lost For Thousands of Strokes: 'Desert Golfing' Is 'Angry Birds' as Modern Art |Alec Kubas-Meyer |January 2, 2015 |DAILY BEAST 

There is only sand, a white ball, and a flag indicating the hole. Lost For Thousands of Strokes: 'Desert Golfing' Is 'Angry Birds' as Modern Art |Alec Kubas-Meyer |January 2, 2015 |DAILY BEAST 

Before he could finish the sentence the Hole-keeper said snappishly, "Well, drop out again—quick!" Davy and The Goblin |Charles E. Carryl 

Kind of a reception-room in there—guess I know a reception-room from a hole in the wall. Boys and Girls Bookshelf; a Practical Plan of Character Building, Volume I (of 17) |Various 

Squinty, several times, looked at the hole under the pen, by which he had once gotten out. Squinty the Comical Pig |Richard Barnum 

Madame and myself had just been regretting that we should have to pass the evening in this miserable hole of a town. Blackwood's Edinburgh Magazine, Volume 60, No. 372, October 1846 |Various 

And if he was worried about Farmer Green's cat, why didn't he dig a hole for himself at once, and get out of harm's way? The Tale of Grandfather Mole |Arthur Scott Bailey