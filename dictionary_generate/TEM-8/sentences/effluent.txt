During the dry season, the filtration side is filled with partially cleaned effluent from a sewage treatment plant. The architect making friends with flooding |Erica Gies |December 21, 2021 |MIT Technology Review 

Heavy rainfall would seal up the soil until effluent had nowhere to go but up onto lawns or back into homes. Battling America’s ‘dirty secret’ |Sarah Kaplan |December 17, 2020 |Washington Post 

It says its plan could be done for way cheaper – $80 million all in – and could capture 80 percent of the effluent before it heads to the ocean. Morning Report: Turning Border Sewage Into Local Water |Voice of San Diego |November 20, 2020 |Voice of San Diego 

This wouldn’t provide nearly as much water, but the water it would provide could still be cheaper than water we currently import from the Colorado River, and it could prevent some untreated effluent from flowing into the Pacific, Tetzlaff said. Morning Report: Turning Border Sewage Into Local Water |Voice of San Diego |November 20, 2020 |Voice of San Diego 

Only seven percent of the world’s wastewater is fully treated to remove nitrogen, and when the urine-tinged effluent is released into water bodies it acts as a pollutant, causing algal blooms and fish die-offs. Your pee could be the golden ticket to a greener world |Ula Chrobak |November 19, 2020 |Popular-Science 

Yet overall the media absolutely adores cruise-ship stories, especially if they can be connected to effluent. In Defense of Cruises: Ignore the Carnival Sewage Disaster |Andrew Roberts |February 15, 2013 |DAILY BEAST 

No sludge is left, everything being turned into the harmless effluent. Marvels of Scientific Invention |Thomas W. Corbin 

Namely, it is an attempt to exhibit apparent nature as an effluent from the mind because of causal nature. The Concept of Nature |Alfred North Whitehead 

The meeting point of these two natures is the mind, the causal nature being influent and the apparent nature being effluent. The Concept of Nature |Alfred North Whitehead 

The effluent is fit for all the varied uses of a dye works, and is stated to be perfectly capable of sustaining fish life. Scientific American Supplement, No. 787, January 31, 1891 |Various 

The cuttle is the only animal that I know of that would cause this by the effluent current from its "syphon tube." Sea Monsters Unmasked and Sea Fables Explained |Henry Lee