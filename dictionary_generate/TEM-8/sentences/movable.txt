A movable diaphragm is attached to a fixed plate, and both are charged and have electrodes attached. The shape of things to come: Different types of microphones and when to use them |Mike Levine |August 26, 2021 |Popular-Science 

After trading eight veterans at the deadline, leaving them with a 40-man roster full of many movable pieces, they are already much more willing to take a flier on waivers and see what happens. After big moves at trade deadline, Nationals’ waiver claims show a shift in their thinking |Jesse Dougherty |August 24, 2021 |Washington Post 

The sprayer is more movable than other bidets, which makes it optimal for all-around cleaning. The best bidet: Talk about a toilet upgrade |Billy Cadden |July 21, 2021 |Popular-Science 

So classrooms are divided by movable walls, allowing students to combine for some lessons, and for students to interact with different teachers. The new child tax credit could lift more than 5 million kids out of poverty. Can it help them learn, too? |Moriah Balingit |July 13, 2021 |Washington Post 

Oftentimes, there’s therapeutic components, behavior changes, and movable touchpoints. Musculoskeletal medical startups race to enter personalized health tech market |Natasha Mascarenhas |June 25, 2021 |TechCrunch 

Somewhere in there is what activists call the “movable middle.” Is Caring About Climate Change an Essential Part of LGBT Identity? |Jay Michaelson |September 21, 2014 |DAILY BEAST 

For many years he wrote the Movable Buffet blog and print column for the Los Angeles Times. Are Gigolos Real? |Richard Abowitz |May 9, 2011 |DAILY BEAST 

For many years Abowitz wrote Movable Buffet blog and print column for Los Angeles Times. Why Porn Stars Love Twitter |Richard Abowitz |January 25, 2011 |DAILY BEAST 

Abowitz is perhaps best known for writing the Movable Buffet blog and continuing print column for Los Angeles Times. Top 5 Reasons Porn-for-Profit Is Dying |Richard Abowitz |January 10, 2010 |DAILY BEAST 

Richard Abowitz is on the staff of Las Vegas Weekly and writes the Movable Buffet blog and column for Los Angeles Times. The Men in the Mirror |Richard Abowitz |October 14, 2009 |DAILY BEAST 

This Sin Sin Wa disturbed sufficiently to reveal a movable slab in the roughly paved floor. Dope |Sax Rohmer 

The movable hour circle and driving wheel of the Crossley reflector has two sets of graduations. Photographs of Nebul and Clusters |James Edward Keeler 

Old and new measurements, tonnage, time allowances and movable ballast, are all a sealed book to me. The Pit Town Coronet, Volume II (of 3) |Charles James Wills 

The altar, probably a small movable one of wood, if any at all, must have stood before the presbyter. The Catacombs of Rome |William Henry Withrow 

The movable bridge in its closed position must be proportioned like a fixed bridge, but it has also other conditions to fulfil. Encyclopaedia Britannica, 11th Edition, Volume 4, Part 3 |Various