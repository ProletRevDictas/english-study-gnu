Guests are pretty hearty and they also quarantine six months out of the year because of the weather, but these are the same people that grill outside in the winter and have fire pits in their backyards. A Looming Menace for Restaurants: Winter Is Coming |Brenna Houck |October 15, 2020 |Eater 

This annual celebration of Bavarian craft beer, food and lederhosen features hearty fare for the inner bear. OktoBearFest starts Sept. 21 at Red Bear |Philip Van Slooten |September 11, 2020 |Washington Blade 

With an above-average size, this pan can hold hearty amounts of soup, sauces, or side dishes. The best stockpots for your kitchen |PopSci Commerce Team |September 9, 2020 |Popular-Science 

The people here are just hearty and skilled in so many ways, with tractors, chainsaws, grading, knowing flora and fauna. How We Saved Our Neighborhood from a Wildfire |Dain Zaffke |September 2, 2020 |Outside Online 

Pre-lockdown, I always rewarded myself for going to the gym with a hearty takeout meal immediately afterward. This Delicious Japanese Breakfast Costs Under $1 |Wes Siler |September 2, 2020 |Outside Online 

“Gronkowski” itself never manages to sound more erotic than the name of a hearty Polish stew or a D-list WWE performer. ‘A Gronking to Remember’ Speed Read: 8 Naughtiest Bits |Emily Shire |January 7, 2015 |DAILY BEAST 

Asked what kind of support he got from rank-and-file Democrats, he paused before replying with a hearty laugh. Repubs Should Take It From Kucinich: Impeachment Isn’t Worth It |Eleanor Clift |December 5, 2014 |DAILY BEAST 

On vacation in Crete, Michlin and his wife were looking for a good, hearty meal. The Airbnb of Home-Cooked Meals |Itay Hod |November 3, 2014 |DAILY BEAST 

So the question was, how do we design a chip hearty, while keeping it thin? The Latest in High-Tech Chips | |September 18, 2014 |DAILY BEAST 

Such men complain about being seen as “pieces of meat,” but the complaints rarely seem hearty, or fully meant. Zac Efron’s Eyes Are Up Here, Ladies |Tim Teeman |April 15, 2014 |DAILY BEAST 

Mr. Brown seizes the proffered member, and gives it as hearty a pressure as the publicity of the occasion will permit. Physiology of The Opera |John H. Swaby (AKA "Scrici") 

Great preparations had been made, and the success must have been perfect to win so general and hearty a commendation. Glances at Europe |Horace Greeley 

He had a red, jolly face, divided unequally by a great black moustache, and his manner was hearty. The Joyous Adventures of Aristide Pujol |William J. Locke 

The nod of assent was given, and the permission put in force with hearty good will. The Book of Anecdotes and Budget of Fun; |Various 

This was a very strong expression of approbation, and an uncommonly hearty welcome from a person of Mr. Sikess temperament. Oliver Twist, Vol. II (of 3) |Charles Dickens