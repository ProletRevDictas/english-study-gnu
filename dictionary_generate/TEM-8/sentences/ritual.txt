Scans hint at Egyptian ritual in the snake, which had rock structures in its open mouth, possibly the mineral natron used by ancient Egyptians to slow decomposition. X-rays reveal what ancient animal mummies keep under wraps |Helen Thompson |August 20, 2020 |Science News 

Another viewing feels fitting, like the conclusion to a ritual. The first murder |Katie McLean |August 19, 2020 |MIT Technology Review 

Many people, for instance, are turning to their immediate family members to fill that ritual void. Why do we miss the rituals put on hold by the COVID-19 pandemic? |Sujata Gupta |August 14, 2020 |Science News 

A connection may have existed between human sacrificial ceremonies that were intended to appease Inca deities and events held at Lake Titicaca, including the submerging of ritual offerings, the researchers suggest. A submerged Inca offering hints at Lake Titicaca’s sacred role |Bruce Bower |August 3, 2020 |Science News 

Ancient Americans ventured deep into caves along a stretch of Mexico’s Yucatán Peninsula to mine a red pigment that could have had both practical and ritual uses, researchers say. Underwater caves once hosted the Americas’ oldest known ochre mines |Bruce Bower |July 3, 2020 |Science News 

The family was taking some private moments for a closing of the coffin in keeping with Chinese ritual. Funeral Protest Is Too Much for NYPD Union Boss |Michael Daly |January 5, 2015 |DAILY BEAST 

What ritual did some celebrities start engaging in over the summer? Michael Tomasky’s Year-End Quiz: Test Your 2014 News Knowledge |Michael Tomasky |December 26, 2014 |DAILY BEAST 

Kanchanaburi, Thailand — At the Kanchanaburi train station each morning, the same ritual unfolds. Riding Thailand’s WWII Death Railway |Liza Foreman |December 21, 2014 |DAILY BEAST 

Followers had traveled many miles to mourn the loss, and aid in the ritual washing, dressing, and honoring of the body. Jail Threats for Sierra Leone Ebola Victims’ Families |Abby Haglage |December 10, 2014 |DAILY BEAST 

The line of questioning is a regular ritual conducted between Sen. Dianne Feinstein and the press corps. CIA Torture Report ‘Days’ Away, Feinstein Says |Tim Mak |December 2, 2014 |DAILY BEAST 

Upon seeing the said ritual, I ordered it to be published, and it was done on the day of Sts. The Philippine Islands, 1493-1898, Volume XX, 1621-1624 |Various 

Organisation ensues, and the general conceptions of state-deities and state-ritual are made more definite and precise. The Religion of Ancient Rome |Cyril Bailey 

Once again temples are shut and marriages forbidden, but the ritual is of a very different nature. The Religion of Ancient Rome |Cyril Bailey 

In the matter of ritual and observance, state-organisation—and its absence—are alike significant. The Religion of Ancient Rome |Cyril Bailey 

There he saw the ceremony of ordination performed, and expressed warm approbation of the Anglican ritual. The History of England from the Accession of James II. |Thomas Babington Macaulay