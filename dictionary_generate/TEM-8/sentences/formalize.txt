In addition to formalizing that the vice president would assume the duties of the presidency in the event of a president’s death, removal from office or resignation, it instituted a process by which a living president could be removed from office. How removal under the 25th Amendment works: A beginner’s guide |Philip Bump |October 9, 2020 |Washington Post 

Fifteen years ago, the IGBC asked if he’d help them formalize the program by using the bears that got there in the first place by getting into garbage. These Bears Have a Job, and It's Destroying Coolers |Emma Walker |October 9, 2020 |Outside Online 

The 25th Amendment formalizes that the vice president takes over the duties of the presidency in the event of a president’s death, inability to perform his duties or resignation from office. Citing 25th Amendment, Pelosi, Raskin move to create panel that could rule on president’s fitness for office |Felicia Sonmez |October 8, 2020 |Washington Post 

States have adopted and formalized voter registration systems at different times, and some just allowed people to register on Election Day when they came in to vote. How to fix America’s voter registration system so more people can vote |Jen Kirby |October 6, 2020 |Vox 

Coq users have formalized a lot of mathematics in its language, but that work has been decentralized and unorganized. Building the Mathematical Library of the Future |Kevin Hartnett |October 1, 2020 |Quanta Magazine 

Maps have long served to formalize authority over peoples and their lands and resources. Should Google Be Mapping Tribal Lands? |Grace-Yvette Gemmell |June 4, 2014 |DAILY BEAST 

She sought to finalize and formalize the split by filing for divorce in Brooklyn. Threats Preceded High-Rise Suicide, New York Mom Says |Michael Daly |December 24, 2013 |DAILY BEAST 

The battle will now focus on the drafting of Egypt's new constitution, which will formalize the emerging allocation of power. Good News, Bad News |Hussein Ibish |June 25, 2012 |DAILY BEAST 

Of course any effort to impose such a system would simply be to formalize what already exists, and has since 1967. Beware "Creative Alternatives" |Hussein Ibish |May 17, 2012 |DAILY BEAST 

The first step is to formalize something resembling a brand. NBC, You've Blown It Again! |Jace Lacob |October 26, 2010 |DAILY BEAST 

But this priest was standing in the corridor and was rather insistent that he formalize some prayers at that point. Warren Commission (7 of 26): Hearings Vol. VII (of 15) |The President's Commission on the Assassination of President Kennedy 

Yet strangely and unexpectedly the attempt to formalize his faith almost shook his faith out of him again. The Devil's Garden |W. B. Maxwell 

"Formalize our mating as soon as she is able to get out of bed," Kennon replied. The Lani People |J. F. Bone