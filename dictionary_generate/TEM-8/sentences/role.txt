This will occur when admins leave and no one else assumes the admin role. Facebook tries to clean up Groups with new policies |Sarah Perez |September 17, 2020 |TechCrunch 

The group also played a big role in securing an agreement on forestry in the Paris agreement. Want to fight climate change effectively? Here’s where to donate your money. |Sigal Samuel |September 17, 2020 |Vox 

Carrey, who rose to stardom in the 1990s, will take over the role from SNL cast member Jason Sudeikis who played Biden during the Obama administration. Trump contradicts CDC director on vaccine; Biden says Americans shouldn’t trust Trump |Colby Itkowitz, Felicia Sonmez, John Wagner |September 16, 2020 |Washington Post 

There would be a spotlight on Gary Harris, formerly one of the league’s best role players, whose return from injury has given the club a much-needed defensive stopper on the wing. When It Comes To Playoff Disappointment, The Clippers Are In A League Of Their Own |Chris Herring (chris.herring@fivethirtyeight.com) |September 16, 2020 |FiveThirtyEight 

Those receptors have played an important role in our history, she adds. Stinky success: Scientists identify the chemistry of B.O. |Alison Pearce Stevens |September 15, 2020 |Science News For Students 

Genetics alone does not an eating disorder make, generally speaking, and Bulik points out that environment still plays a role. How Skinny Is Too Skinny? Israel Bans ‘Underweight’ Models |Carrie Arnold |January 8, 2015 |DAILY BEAST 

“I have to think her body type played a role,” said Rachel Greenblatt, a Lecturer in Jewish Studies at Harvard University. Why Was Bess Myerson the First and Last Jewish Miss America? |Emily Shire |January 7, 2015 |DAILY BEAST 

Acting legend talks about what role is closest to her heart. Gena Rowlands on Her Favorite ‘Woman’ |The Daily Beast Video |January 3, 2015 |DAILY BEAST 

David Prowse, the actor who portrayed Darth Vader, wished to come back but had to turn down the role because of ill health. Juiciest ‘Star Wars: The Force Awakens’ Rumors (and Some Debunked Ones) |Rich Goldstein |January 3, 2015 |DAILY BEAST 

The role of private investigators has stirred controversy in the investigation. U.S. Spies Say They Tracked ‘Sony Hackers’ For Years |Shane Harris |January 2, 2015 |DAILY BEAST 

Many of them were delicious in the role; one of them was the embodiment of every womanly grace and charm. The Awakening and Selected Short Stories |Kate Chopin 

After all, Garnache's appearance was hardly suggestive of the role of Perseus which had been thrust upon him. St. Martin's Summer |Rafael Sabatini 

But don't adopt the role of inquisitor—because I'm as good as dead, and dead men tell no tales. Raw Gold |Bertrand W. Sinclair 

Virginia continued to play a key role in international events during this period. Hallowed Heritage: The Life of Virginia |Dorothy M. Torpey 

The Government Film Censor interprets his role chiefly as one of guiding parents. Report of the Special Committee on Moral Delinquency in Children and Adolescents |Oswald Chettle Mazengarb et al.