But, amid outcries over disinformation, hate speech and concerns about biased decisions to kick people off social media platforms, Democratic and Republican lawmakers alike have proposed changing Section 230 over the past year. Cheat sheet: U.S. lawmakers propose Section 230 reforms to regulate online paid speech |Kate Kaye |February 10, 2021 |Digiday 

His death was recorded by a bystander and quickly went viral, sparking national outcry. Still Can’t Breathe |by Topher Sanders, ProPublica, and Yoav Gonen, THE CITY, video by Lucas Waldron, ProPublica |January 21, 2021 |ProPublica 

In New York, legislators faced an outcry over the expansive scope of the liability protections and four months later scaled them back. The Nursing Home Didn’t Send Her to the Hospital, and She Died |by Sean Campbell |January 8, 2021 |ProPublica 

Nonetheless, a controversy that had begun a year previously with an outcry about the treatment of women had ended up focusing on whether progress on gender integration should be reversed. The Army can’t repeat the mistakes of the 1990s if it wants to end sexual assault |David Fitzgerald |December 17, 2020 |Washington Post 

After losing a lease for a vacant property in Ward 5 near the National Arboretum amid public outcry, Core purchased a structure in Ward 7, at 3701 Benning Road NE. Proposed historic designation for ‘forgotten’ D.C. slaughterhouse may block new halfway house |Justin Wm. Moyer |December 2, 2020 |Washington Post 

When cities started adding chlorine to their water supplies, in the early 1900s, it set off public outcry. Anti-Fluoriders Are The OG Anti-Vaxxers |Michael Schulson |July 27, 2016 |DAILY BEAST 

Lately, Richard Dawkins seems to scan the world for sore spots, take a good poke, and revel in the ensuing outcry. Richard Dawkins Would Fail Philosophy 101 |Elizabeth Picciuto |August 28, 2014 |DAILY BEAST 

There will be an international outcry, if, as reported, the black box is on its way to Moscow. Why Was Malaysia Airlines Flight 17 Flying Through a War Zone? |Clive Irving |July 17, 2014 |DAILY BEAST 

But sadly a panel featuring people who have spewed the worst anti-Muslim hate causes little outcry. The Heritage Hate Panel Features Two Leading Islamophobes |Dean Obeidallah |June 17, 2014 |DAILY BEAST 

In Syria he made chemical weapons a “red line,” and then folded in the face of public outcry and congressional opposition. Obama Is the New Dubya |Lloyd Green |June 17, 2014 |DAILY BEAST 

Notwithstanding the unseemly hour, the people came running out at the outcry and clamor especially those from the nearest houses. The Philippine Islands, 1493-1898, Volume XX, 1621-1624 |Various 

The outcry against his dismission was falling in thunder tones on the ear of the king. Madame Roland, Makers of History |John S. C. Abbott 

True a wonderful outcry has been raised about the antagonism between the records of the rocks, and the records of the Bible. Gospel Philosophy |J. H. Ward 

Here was something which pointed directly to Indian handiwork, and Lowell in imagination could hear a great outcry going up. Mystery Ranch |Arthur Chapman 

Gilbert uttered an outcry in astonishment and quickly drew back, for this sleepy and yawning girl was Nicole. Balsamo, The Magician |Alexander Dumas