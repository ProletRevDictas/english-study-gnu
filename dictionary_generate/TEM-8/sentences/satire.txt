Thus far, most professionalized deepfakes have been based on famous people and made with clear, constructive goals, so they are legally protected in the US under satire laws. Inside the strange new world of being a deepfake actor |Karen Hao |October 9, 2020 |MIT Technology Review 

There are currently no real guidelines on how to label deepfakes, for example, or where the line falls between satire and misinformation. Inside the strange new world of being a deepfake actor |Karen Hao |October 9, 2020 |MIT Technology Review 

Over the years, Francis has deepfaked politicians like Boris Johnson and celebrities like Kim Kardashian, all in the name of education and satire. Inside the strange new world of being a deepfake actor |Karen Hao |October 9, 2020 |MIT Technology Review 

Good political satire that responds to the tumult of the times is possible. Saturday Night Live will not save you |Emily VanDerWerff |October 2, 2020 |Vox 

Art does not have to be political satire to help us reckon with the world. Why art matters at the end of the world |Constance Grady |September 25, 2020 |Vox 

And the fact that satire unnerves the intolerant is evidence of its positive power. Why We Stand With Charlie Hebdo—And You Should Too |John Avlon |January 8, 2015 |DAILY BEAST 

The 289-page satire follows Morris Feldstein, a pharmaceutical salesman who gets seduced by a lonely receptionist. Powerful Congressman Writes About ‘Fleshy Breasts’ |Asawin Suebsaeng |January 7, 2015 |DAILY BEAST 

The premise was simple: satire is devastating against tyrants. The Sony Hack and America’s Craven Capitulation To Terror |David Keyes |December 19, 2014 |DAILY BEAST 

We prefer to wave away the warning signs; like The Interview, Mulholland Drive was comfortably downplayed as over-the-top satire. Pyongyang Shuffle: Hollywood In Dead Panic Over Sony Hack |James Poulos |December 19, 2014 |DAILY BEAST 

But now, the controversy surrounding the political satire has gotten serious. Exclusive: Sony Emails Say State Department Blessed Kim Jong-Un Assassination in ‘The Interview’ |William Boot |December 17, 2014 |DAILY BEAST 

He was judge of the admiralty court of Pennsylvania; his writings abound with wit, humor and satire. The Every Day Book of History and Chronology |Joel Munsell 

Other caricatures of the period more justly include ministers in their satire. The Portsmouth Road and Its Tributaries |Charles G. Harper 

Page after page—full of caustic satire, humorous sally and profound epigram—fairly bristles with merriment. The Joyous Adventures of Aristide Pujol |William J. Locke 

As the drink takes effect our parlour customers attempt satire, and their efforts are always of a strongly personal nature. The Chequers |James Runciman 

The syllogisms were overthrown by their satire, and their arguments evaporated in their vituperation. The History of England in Three Volumes, Vol.III. |E. Farr and E. H. Nolan