The NFL announced that Sunday's game between the Tennessee Titans and the Pittsburgh Steelers has been delayed after several Titans players and staffers tested positive for the coronavirus. Steelers-Titans NFL game delayed after coronavirus outbreak |Axios |September 30, 2020 |Axios 

In announcing the layoffs, Disney appeared to partially cast blame on the California state government, saying that the cuts were “exacerbated in California by the State’s unwillingness to lift restrictions that would allow Disneyland to reopen.” Disney Is Laying Off 28,000 Domestic Theme Park Employees |Jenny G. Zhang |September 30, 2020 |Eater 

Irène Mathieu, University of VirginiaThis spring, after stay-at-home orders were announced and schools shut down across the nation, many families stopped going to their pediatrician. Your Child’s Vaccines: What You Need To Know About Catching Up During The COVID-19 Pandemic |LGBTQ-Editor |September 30, 2020 |No Straight News 

Last week, the platform announced it was testing Story Pins, which combine multiple pages of images, videos, voiceover and overlaid text. ‘We want to drive more transactions’: As e-commerce sales accelerate, more media dollars are going to Pinterest |Seb Joseph |September 30, 2020 |Digiday 

Last week, shortly after the committee announced the hearing, the agency informed Engel he would not attend. U.S. broadcasting agency executive director ignores House subpoena |Kaela Roeder |September 29, 2020 |Washington Blade 

“All members of the Court and the public announce the untimely death of Lucia Aielli,” the announcement said. Days of Mafia Mayhem Are Wracking Italy Once Again |Barbie Latza Nadeau |November 22, 2014 |DAILY BEAST 

Any speculation about what happens should Clinton announce a candidacy, Sefl said, is just speculation. Is Ready for Hillary Ready to Fold—or Work With Candidate Clinton? |David Freedlander |November 13, 2014 |DAILY BEAST 

Thunderous sounds announce its arrival, piercing the silence that accompanies sundown in the swampland near Boystown, Liberia. Rage Against the Ebola Crematorium |Abby Haglage |November 11, 2014 |DAILY BEAST 

My daughter took this as an opportunity to announce, “I have two moms.” Is Polygamy the Next Gay Marriage? |Sally Kohn |September 12, 2014 |DAILY BEAST 

To announce that ten thousand “troops” are to be sent abroad distracts from the living reality of what is going on. What Did You Do in the Targeted Action, Daddy? |John McWhorter |September 12, 2014 |DAILY BEAST 

Captain Vane came from the observatory, his face blazing with excitement and oily with heat, to announce the fact. The Giant of the North |R.M. Ballantyne 

Cabral dispatched a small vessel to Lisbon to announce his discovery, and then, without making any settlement, proceeded to India. Journal of a Voyage to Brazil |Maria Graham 

They announce to us successive generations of animals and plants; but they do not tell us how long these generations lived. Gospel Philosophy |J. H. Ward 

Mr. Monterey will confer with us at noon, and before school is dismissed to-day we will announce the winner. The Girls of Central High on the Stage |Gertrude W. Morrison 

Besides I am ready to play now, and you can announce a concert within a week if you like. The Fifth String |John Philip Sousa