There aren’t any kind of like aberrant cracks that could, you know, get bigger and start to lead to a bigger destruction in the hull. Podcast: How democracies can reclaim digital power |Anthony Green |October 15, 2020 |MIT Technology Review 

That sort of messy destruction can leave behind cracks or structural damage that propagates through the rest of the spacecraft hull or pierce through the ammonia coolant system. Astronauts on the ISS are hunting for the source of another mystery air leak |Neel Patel |September 30, 2020 |MIT Technology Review 

The largest pieces were identified and tracked, but debris that was less than 10 centimeters in length—pieces that still pose a threat to spacecraft hull—was allowed to zip through space undetected. Astronauts on the ISS are hunting for the source of another mystery air leak |Neel Patel |September 30, 2020 |MIT Technology Review 

This fills with water as your weight presses down on the hull. This motorized kayak can drive itself |By Nate Matthews/Outdoor Life |August 28, 2020 |Popular-Science 

The ship sat for over a week before cracks emerged in its hull. Mauritius is reeling from a devastating oil spill and fears of an ecological disaster |Adam Moolna |August 12, 2020 |Quartz 

And, thanks to a transparent hull, exploring the deep and spotting rare marine life is practically a cinch. The Most Exciting New Hotels, Restaurants, and Submarines of 2014 |Charlie Gilbert |December 29, 2014 |DAILY BEAST 

Jimbo and I walked up its ramp and into the hull, which looked like the gutted inside of a school bus. I Shot Bin Laden |Elliot Ackerman |November 16, 2014 |DAILY BEAST 

Four of them carried a thick black nylon body bag, two to a side, and loaded it into the middle of the hull. I Shot Bin Laden |Elliot Ackerman |November 16, 2014 |DAILY BEAST 

Meanwhile, the rest of hull is wide at the waterline and slopes inward. Can the Navy's $12 Billion Stealth Destroyer Stay Afloat? |Dave Majumdar |October 22, 2014 |DAILY BEAST 

Having received a patent on the technology in 1986, Hull founded 3D Systems to commercialize his discoveries. Pioneers in Printing |The Daily Beast |October 21, 2014 |DAILY BEAST 

In fact—on account of conditions beyond my choice and control—I spent too much time on the wrong side of the hull shields. Fee of the Frontier |Horace Brown Fyfe 

Again, mebbe it would–if the hull thing that happened next was accidentally a-purpose. Alec Lloyd, Cowpuncher |Eleanor Gates 

A party of American marines boarded her, hauled down the Spanish flag, and tried to save the hull, but it was too far consumed. The Philippine Islands |John Foreman 

I'd be tickled to have the hull town come out an' see me cuttin' figger eight's in the clouds. Motor Matt's "Century" Run |Stanley R. Matthews 

An dem pieces yo orated den was a hull lot nicer dan wat Mars Chet is sayin. The Girls of Central High on the Stage |Gertrude W. Morrison