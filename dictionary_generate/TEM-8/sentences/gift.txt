A source familiar told me that before that trip Kushner purchased a Torah with his own money as a gift to Bahrain's King Hamad Bin Isa Al-Khalifa. Behind the scenes of the U.S.-brokered Israel-Bahrain agreement |Barak Ravid |September 11, 2020 |Axios 

A new and improved keyboard could make a great gift for those hard-working hands of yours. Serious upgrades for your computer keyboard |PopSci Commerce Team |September 2, 2020 |Popular-Science 

If your friend or family member has to postpone, Forrest recommends scheduling a phone date with them, offering to help contact guests, and sending them a gift on their original wedding date to make them feel special. How to update your guests about your pandemic wedding plans |Brooke Henderson |August 29, 2020 |Fortune 

She recommends still sending a wedding gift and a thoughtfully written card that explains why you’re RSVP’ing no. How to turn down a wedding invitation during the coronavirus pandemic |Brooke Henderson |August 23, 2020 |Fortune 

Additionally, the pandemic has created financial challenges with millions unemployed, so gift giving might be even more stressful. A guide to giving gifts for postponed and shrunken weddings |Brooke Henderson |August 20, 2020 |Fortune 

The kids had a gift for him too, a tee shirt with ‘Baseball Spoken Here’ stenciled across the front. Mario Cuomo, Always Moving Us Toward the Light |Mike Barnicle |January 4, 2015 |DAILY BEAST 

While the chicken today might be the least exotic bird one can think of, it was once a gift that wowed kings. The History of the Chicken: How This Humble Bird Saved Humanity |William O’Connor |December 27, 2014 |DAILY BEAST 

A Christmas Carol revived and reinvented it around the gift of giving. How Dickens and Scrooge Saved Christmas |Clive Irving |December 22, 2014 |DAILY BEAST 

Then the gift card is shopped online in a gray market to collect cold currency. The Insane $11 Billion Scam at Retailers’ Return Desks |M.L. Nestel |December 19, 2014 |DAILY BEAST 

Moraca pointed to another form of return fraud, involving gift cards. The Insane $11 Billion Scam at Retailers’ Return Desks |M.L. Nestel |December 19, 2014 |DAILY BEAST 

The living (value £250) is in the gift of trustees, and is now held by the Rev. M. Parker, Vicar. Showell's Dictionary of Birmingham |Thomas T. Harman and Walter Showell 

At the end of the first year, however, she resigned this privilege because she did not wish to accept the conditions of the gift. Women in the fine arts, from the Seventh Century B.C. to the Twentieth Century A.D. |Clara Erskine Clement 

This gift of rice was especially pleasing to the traveller, as no dish is held in higher honour in Korea. Our Little Korean Cousin |H. Lee M. Pike 

It is certain that I then had a bad cough nearly always; and this I am sure was what decided the form of his parting gift to me. Fifty Years of Railway Life in England, Scotland and Ireland |Joseph Tatlow 

His methodical mind hated the idea of disorder; administration came to him as Nature's gift. Napoleon's Marshals |R. P. Dunn-Pattison