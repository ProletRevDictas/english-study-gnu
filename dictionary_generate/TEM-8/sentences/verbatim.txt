The Ministry of Truth’s employees were in charge of writing down the most negative things about Big Brother they could, by transcribing verbatim what came out of his mouth, and that seemed unfair to Winston. Here’s what I think happens in Orwell’s books based on how I’ve heard ‘Orwellian’ used |Alexandra Petri |January 12, 2021 |Washington Post 

This is one of those films I hope people will know thoroughly and verbatim. ‘Lovers Rock’—The Story Behind The Music In Steve McQueen’s Tribute To Reggae |cmurray |November 28, 2020 |Essence.com 

At least three federal documents, including a 2019 report, repeat almost verbatim the project application’s claims for visitation, job creation and revenue generation. The Elk, the Tourists and the Missing Coal Country Jobs |by R.G. Dunlop, Kentucky Center for Investigative Reporting |October 22, 2020 |ProPublica 

You may want to sacrifice smooth readability occasionally for long-tail keywords verbatim. Five excellent tips to optimize SEO for Bing – not Google |Justin Staples |October 16, 2020 |Search Engine Watch 

Cajon Valley Union officials told me the same thing, almost verbatim, after they were able to bring some students back to campus. The Learning Curve: School Is Now an Antidote Few Families Can Access |Will Huntsberry |September 24, 2020 |Voice of San Diego 

I have it on good authority these quotes are 100 percent accurate, if not 100 percent verbatim. Forget the Resolutions; Try a Few Declarations |Kevin Bleyer |January 1, 2015 |DAILY BEAST 

But most of all he loved Monty Python and would regularly repeat their hilarious sketches verbatim. When Gary Wright Met George Harrison: Dream Weaver, John and Yoko, and More |Gary Wright |September 29, 2014 |DAILY BEAST 

He stuck them in his pocket, and would later copy them, almost verbatim, into his novel. American Dreams, 1933: Miss Lonelyhearts by Nathanael West |Nathaniel Rich |April 29, 2013 |DAILY BEAST 

These and other famous pieces of revolutionary rhetoric repeat lines from the play more or less verbatim. Who Was the Real Cato? |David Frum |December 20, 2012 |DAILY BEAST 

Check out this devastating Obama ad on that point, with regular citizens reading verbatim Romney's explanation. The Emerging Theory on Mitt's Taxes: It's 2009 |Michael Tomasky |July 18, 2012 |DAILY BEAST 

And the conversations I can reproduce almost verbatim, for, according to my invariable habit, I kept full notes of all he said. Three More John Silence Stories |Algernon Blackwood 

His chief guide is Julius Cæsar, whom he frequently quotes verbatim. The Geography of Strabo, Volume III (of 3) |Strabo 

He (Stopford) wrote it down, in the ante-room, the moment he left the presence and I may take it as being as good as verbatim. Gallipoli Diary, Volume 2 |Ian Hamilton 

His editorial was the "Courier's" leader, and it appeared verbatim et literatim. A Hoosier Chronicle |Meredith Nicholson 

Mr. Drury can not claim to have recorded verbatim Prof. Vaughn's remarks, but has endeavored to give the substance. Etidorhpa or the End of Earth. |John Uri Lloyd