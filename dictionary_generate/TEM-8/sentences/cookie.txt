Many brands are now waking up to the deprecation of cookies. Media Briefing: Facebook pivots away from politics –publishers say ‘we’re just along for the ride’ |Tim Peterson |February 11, 2021 |Digiday 

These are included to support items that might otherwise crumble or melt through the oven’s internal rack during the cooking process, such as cookies, pizza, or egg-based dishes. Best toaster oven: Save counter space and time with our toaster oven picks |Julian Cubilllos |February 5, 2021 |Popular-Science 

The penguin, according to my unscientific interpretation, was trying to share its seafood meal with me, like splitting a cookie with a friend. In ‘The Whispering Land,’ a British naturalist collects travel tales — and animals with tails — in Argentina |Andrea Sachs |February 4, 2021 |Washington Post 

Now, its attempt to replace the cookie is attracting regulatory attention. Why Google’s approach to replacing the cookie is drawing antitrust scrutiny |Kate Kaye |February 2, 2021 |Digiday 

That high fiber content weighs down breads and results in cookies that are toothsome, to put it gently. We Found a High-Fiber Flour That's Actually Tasty |AC Shilton |February 1, 2021 |Outside Online 

There was also the grapefruit diet, the cabbage soup diet, and the cookie diet. Why Your New Year’s Diet Will Fail |Carrie Arnold |December 30, 2014 |DAILY BEAST 

And “om nom nom nom” is more of a dig at Cookie Monster and Instagram foodies than it is at anyone else. Feminist, Bae, Turnt: Time’s ‘Worst Words’ List Is Sexist and Racist |Samantha Allen |November 13, 2014 |DAILY BEAST 

A personal favorite is “C Is For Cookie” for guiding me through a 1994 playground debate over how to spell the word. ‘Sesame Street’ Is Middle-Aged and Awesome |Emily Shire |November 10, 2014 |DAILY BEAST 

In the early 1900s, stores in Mexican towns and cities began selling cookie-and-sugar calaveras, or skulls. New Orleans’ Carnivalesque Day of the Dead |Jason Berry |November 1, 2014 |DAILY BEAST 

Cookie Monster has always been one of the most beloved features of that PBS childhood staple, Sesame Street. Cookie Monster Stars in Harry Potter Parody |Jack Holmes, The Daily Beast Video |October 28, 2014 |DAILY BEAST 

You know this is the first day of school and you can't run for a cookie if you get hungry. Patchwork |Anna Balmer Myers 

She had a kettle of doughnuts a frying, and a whole lot of cookie paste ready to cut out and bake. A California Girl |Edward Eldridge 

They had dismissed him, scornfully, stolen cookie in hand—but maybe it would be a bigger cookie than they dreamed! The Colors of Space |Marion Zimmer Bradley 

Place one teaspoonful of filling on each cookie, cover with another cookie, press edges together. The New Dr. Price Cookbook |Anonymous 

"Stow that drivel, cookie," growled a voice which I recognized as belonging to the older Fleming. The Pirate of Panama |William MacLeod Raine