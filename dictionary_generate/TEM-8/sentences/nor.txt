Perhaps though, no city was as silently-failing-nor-publically-rehabilitating as Las Vegas. A Tech Millionaire Bets on the Urban Revival of Downtown Las Vegas |Sarah Kunst |January 16, 2014 |DAILY BEAST 

We are introduced successively to the Palestinian, the Assimilator, and the Neither-here-nor-there. The History of Yiddish Literature in the Nineteenth Century |Leo Wiener 

Abnormal, ab-nor′mal, adj. not normal or according to rule: irregular—also Abnor′mous. Chambers's Twentieth Century Dictionary (part 1 of 4: A-D) |Various 

I have experienced many sandstorms in Takla-makan and the Lob-nor desert, but hardly any so bad as this was. Trans-Himalaya, Vol. 2 (of 2) |Sven Hedin 

He gave me also much valuable information about the country round Nam-tso or Tengri-nor, where he was born. Trans-Himalaya, Vol. 2 (of 2) |Sven Hedin 

Swinburne dismisses him in two lines: Maximilian is a good-natured, neither here-nor-there kind of youth. The Life of Ludwig van Beethoven, Volume I (of 3) |Alexander Wheelock Thayer