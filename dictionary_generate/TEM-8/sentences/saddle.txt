That may sound like “The Handmaid’s Tale” with saddles instead of bonnets, but North is working entirely in her own realm. In Anna North’s riveting ‘Outlawed,’ there’s nothing more dangerous than a childless woman |Ron Charles |January 7, 2021 |Washington Post 

Another tweak is incorporating a low-friction material on the outside of the seat, or saddle, designed to avoid any chafing that kids might experience. With no pedals, Specialized’s ultralight kid’s bike makes learning to ride easy |Rob Verger |December 4, 2020 |Popular-Science 

They used the path integral mostly as a vehicle to identify the saddle points. The Most Famous Paradox in Physics Nears Its End |George Musser |October 29, 2020 |Quanta Magazine 

This stem does a great job of absorbing high-speed vibrations that will wear you down after several hours in the saddle. New Gravel Bike Accessories for a Smoother Ride |Josh Patterson |September 28, 2020 |Outside Online 

They can take the wheels and the saddle off the bike, but nabbing the frame would require more work. The most secure ways to lock up your bike |By Michael Frank/Cycle Volta |August 26, 2020 |Popular-Science 

Certainly Weaver has been the burr under Palmer's saddle for almost his entire career. Will the Real Jim Palmer Please Stand Up |Tom Boswell |September 27, 2014 |DAILY BEAST 

We will not rest till we find out what she ordered and how that famous butt is holding up after four days in the saddle. Pippa Eating At McDonalds in Missouri RIGHT NOW! |Tom Sykes |June 18, 2014 |DAILY BEAST 

With the big kettledrums on either side of the saddle, and all that. Adam Hochschild on Keeping Company With His Dying Father |Adam Hochschild |June 14, 2014 |DAILY BEAST 

But today, skyrocketing costs price way too many young people out of a higher education, or saddle them with unsustainable debt. Full Text and Video of President Obama's 2013 State of the Union Address |Justin Green |February 13, 2013 |DAILY BEAST 

Mrs. Buller cooked a braised saddle of veal and delicious it was too served with a rich gravy flavored with claret. A Real-Life ‘Downton Abbey’ Affair |Margaret Powell |January 13, 2013 |DAILY BEAST 

Fatigue he never knew, and on one occasion he was said to have spent thirteen days and nights in the saddle. Napoleon's Marshals |R. P. Dunn-Pattison 

"I call you," the policeman said, and stripping the saddle and bridle from his sweaty horse, turned him loose to graze. Raw Gold |Bertrand W. Sinclair 

We had been twelve hours in the saddle, and had ridden over nearly a hundred miles of ground. Blackwood's Edinburgh Magazine, No. CCCXXXIX. January, 1844. Vol. LV. |Various 

Also our six-shooters reposed in their scabbards, the four belts hooked over the horn of MacRae's saddle. Raw Gold |Bertrand W. Sinclair 

I pulled the saddle off my horse, slapped it down on the dirt floor, and went stalking up to the long cabin. Raw Gold |Bertrand W. Sinclair