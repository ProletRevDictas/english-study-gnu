As he explains in the prologue to “A Zoo in My Luggage,” “To me extirpation of an animal species is a criminal offence, just as the destruction of something else that we cannot recreate or replace, such as a Rembrandt or the Acropolis, would be.” In ‘The Whispering Land,’ a British naturalist collects travel tales — and animals with tails — in Argentina |Andrea Sachs |February 4, 2021 |Washington Post 

With a paranoid urgency, Prokopios writes in the prologue that he wants to come clean and tell us what actually happened. What the QAnon of the 6th Century Teaches Us About Conspiracies |Roland Betancourt |February 3, 2021 |Time 

So the movie started with the prologue of the son telling the story of what happened to dad. Rolling With James Brolin: Hollywood’s Quiet Giant |Eugene Robinson |January 29, 2021 |Ozy 

I’ve yet to spend a lot of time with the latest version of that device, but if past is any prologue, it’s a solid choice for those looking for an Android-compatible Apple alternative at a good price. Fitbit Sense review |Brian Heater |September 24, 2020 |TechCrunch 

The problems associated with getting people to wear masks, which is a much simpler solution than visiting a health care professional and receiving a shot, is prologue to what we’ll see when it comes to getting people vaccinated, he explained. Health care executives fear public distrust of COVID-19 vaccine will lead to continued spread of disease |Nicole Goodkind |July 8, 2020 |Fortune 

Here is a title that, in its prologue, tasks players with fighting a horde of angels on top of a moving jet. Bayonetta Is Nintendo’s Graphic, Ass-Kicking Barbie |Alec Kubas-Meyer |October 24, 2014 |DAILY BEAST 

“The past is prologue,” says a Democratic House leadership aide. Budget Deal on the Horizon? |Eleanor Clift |December 6, 2013 |DAILY BEAST 

It also features a scene that is shockingly reminiscent of the prologue in The Dark Knight Rises. ‘Call of Duty: Ghosts’ Review: The Juggernaut Franchise Might Be Drying Up |Alec Kubas-Meyer |November 12, 2013 |DAILY BEAST 

These moves are positive, but if past is prologue, any real change could take years to enact. America’s One-Child Policy |Brandy Zadrozny |July 17, 2013 |DAILY BEAST 

But in a nation that has existed for more than 5,000 years, the past is more than a prologue. What Egyptians Really Want |Christopher Dickey |July 3, 2013 |DAILY BEAST 

Trewely is here three syllables, which is the normal form; cf. Prologue, 761; Kn. Chaucer's Works, Volume 1 (of 7) -- Romaunt of the Rose; Minor Poems |Geoffrey Chaucer 

Both in the present passage and in the Pardoner's Prologue the verb to erme is used with the same sb., viz. Chaucer's Works, Volume 1 (of 7) -- Romaunt of the Rose; Minor Poems |Geoffrey Chaucer 

And this was as it should be; the intent of that little prologue was merely to whet the appetite for the real play. The Girls of Central High on the Stage |Gertrude W. Morrison 

Used ludicrously to mean a feat of horsemanship in l. 50 of the Manciple's Prologue. Chaucer's Works, Volume 1 (of 7) -- Romaunt of the Rose; Minor Poems |Geoffrey Chaucer 

The dropped word is clearly here, which rimes with manere in the Miller's Prologue, and elsewhere. Chaucer's Works, Volume 1 (of 7) -- Romaunt of the Rose; Minor Poems |Geoffrey Chaucer