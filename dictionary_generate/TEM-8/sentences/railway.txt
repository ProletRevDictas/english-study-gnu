US policymakers have earmarked 7% of the $1 trillion infrastructure bill making its way through the Senate for greater broadband access, nearly as much as it’s investing in the nation’s railways. The US is ready to pay for broadband like the essential service it is |Camille Squires |August 5, 2021 |Quartz 

Today the water is still bottled in town, but the old railway that delivered travelers into town has been converted into a 20-mile-long rail trail that’s perfect for running. The Runner’s Guide to Texas |kklein |July 16, 2021 |Outside Online 

We laid railway tracks and paved roadways that spanned continents. Why Richard Branson's Spaceflight Is a Very Big Deal |Jeffrey Kluger |July 11, 2021 |Time 

Texas Central Railway, a private railway company, is in talks to build a high-speed line connecting Dallas and Houston. Amtrak's Boss Has a Plan to Make You Love Trains Again. Will it Work? |Patrick Lucas Austin |May 3, 2021 |Time 

According to a plan issued by the State Council in February 2021, China will be home to 200,000 kilometers of railways by 2035. What’s bigger than a megacity? China’s planned city clusters |Ling Xin |April 28, 2021 |MIT Technology Review 

All in all, approximately 13,000 Allied POWs and 90,000 Asian laborers perished while working on the railway. Riding Thailand’s WWII Death Railway |Liza Foreman |December 21, 2014 |DAILY BEAST 

Manttan is keen to carry out research on that Burmese side of the railway as his father worked on that section. Riding Thailand’s WWII Death Railway |Liza Foreman |December 21, 2014 |DAILY BEAST 

A dam now in place on the Thai side of the line prevents the railway from being reconstructed in its entirety, he explains. Riding Thailand’s WWII Death Railway |Liza Foreman |December 21, 2014 |DAILY BEAST 

They recorded 10,549 graves on or near the railway in 144 cemeteries, failing to locate only 52 graves. Riding Thailand’s WWII Death Railway |Liza Foreman |December 21, 2014 |DAILY BEAST 

Two years later, Death Metal Angola is readying for its premiere, and the railway film remains unfinished. Death Metal Angola: Heavy Metal in War-Torn Africa |Nina Strochlic |November 21, 2014 |DAILY BEAST 

On his arrival at the local railway station he was met by his lordship in person. The Pit Town Coronet, Volume I (of 3) |Charles James Wills 

Be that as it may, the Railway Clearing House, as a practical entity, came into being in 1842. Fifty Years of Railway Life in England, Scotland and Ireland |Joseph Tatlow 

Four-wheeled railway carriages are, I was going to say, a thing of the past; but that is not so. Fifty Years of Railway Life in England, Scotland and Ireland |Joseph Tatlow 

From pre-natal days I was destined for the railway service, as an oyster to its shell. Fifty Years of Railway Life in England, Scotland and Ireland |Joseph Tatlow 

Eighteen hundred and fifty-one was a period of anxiety to the Midland and to railway companies generally. Fifty Years of Railway Life in England, Scotland and Ireland |Joseph Tatlow