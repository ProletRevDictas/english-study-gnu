Athletes have powerful and important and informed voices, and they should be encouraged to use them. The pregame national anthem — in all its roiling contradictions — still has something to offer |Barry Svrluga |February 11, 2021 |Washington Post 

So now with Swarm you can have connectivity in those regions outside of cell, and provide that value, and much better knowledge and user data analytics to make much more informed business decisions, save water, save energy, save all those things. Swarm’s low-cost satellite data network is now available to commercial clients |Darrell Etherington |February 9, 2021 |TechCrunch 

When I cracked the book open again, I was more informed about wildlife conservation issues and eco-tourism. In ‘The Whispering Land,’ a British naturalist collects travel tales — and animals with tails — in Argentina |Andrea Sachs |February 4, 2021 |Washington Post 

Here, we’ll detail each of them and discuss the potential benefits and risks, with context to help you make more informed decisions. Google Ads Auto Applied Recommendations: Every setting, explained |Chris Boggs |February 4, 2021 |Search Engine Land 

Having an informed understanding of Sickle Cell and the silent damage it causes, in addition to pain, keeps Jordan focused and motivated to proactively monitor her health as she faces this unpredictable disease. Making Moves With Sickle Cell & Living Beyond A Diagnosis |Steven Psyllos |February 1, 2021 |Essence.com 

The Brazilian press said the White House informed Brasilia about the rapprochement minutes before the statement was made public. Venezuela Says Goodbye to Its Lil Friend, While the Rest of the Continent Cheers |Catalina Lobo-Guererro |December 20, 2014 |DAILY BEAST 

He was informed indeed he had, however the island was infinite. The Life and Hard Times Of The Family A Cuban Defector Left Behind |Brin-Jonathan Butler |December 19, 2014 |DAILY BEAST 

The White House was informed by the committee on Monday that the report would be released the following day. CIA Offers New Security Checks for ‘Torture Report’ Spies |Shane Harris, Kimberly Dozier |December 9, 2014 |DAILY BEAST 

According to The Times, the British government was informed. Britain Has Lost Its Marbles: Elgin Loan Will Appease Putin |Geoffrey Robertson |December 5, 2014 |DAILY BEAST 

They informed us that the money and Medicaid payments we received were predicated on a mistake. Medicaid Will Give You Money for At-Home Care, but You Might Wait Years |Elizabeth Picciuto |December 2, 2014 |DAILY BEAST 

The major-general kept him well informed of every movement of the enemy, and pointed out the dangerous isolation of Davout. Napoleon's Marshals |R. P. Dunn-Pattison 

He was ordered, however, to keep the Emperor daily informed of the state of public opinion in Spain. Napoleon's Marshals |R. P. Dunn-Pattison 

"I'll take charge of this, Captain Dobson," he brusquely informed the red-faced numskull. Raw Gold |Bertrand W. Sinclair 

A printed square of card-board on her writing-table had informed her that the dinner-hour was half-past eight. Ancestors |Gertrude Atherton 

Setting aside timidity, we find that well-informed persons are sometimes‌ good listeners, but no talkers. Assimilative Memory |Marcus Dwight Larrowe (AKA Prof. A. Loisette)