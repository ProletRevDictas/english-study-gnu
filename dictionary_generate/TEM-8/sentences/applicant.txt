It would also require companies that use the tools to tell applicants which characteristics their system used to make a decision. Auditors are testing hiring algorithms for bias, but there’s no easy fix |Amy Nordrum |February 11, 2021 |MIT Technology Review 

Yale has repeatedly denied the allegations, claiming it has complied with decades of Supreme Court precedent, and doesn’t discriminate against applicants based on race. DOJ Dismisses Discrimination Case Against Yale University |Shani Parrish |February 5, 2021 |Essence.com 

In this highly connected world, our military recruiters have access to a wide range of screening tools to discern the background of all applicants. The U.S. Military Needs to Fight Extremism in Its Own Ranks. Here's How |James Stavridis |February 5, 2021 |Time 

Ricci said Maryland law makes it “nearly impossible” for an applicant to get a faster response because it is one of four so-called mitigation states that require adjudicators to determine if there are factors that led to a denied claim. Maryland Democrats recommend legislative fixes to state’s ‘broken’ unemployment system |Ovetta Wiggins |February 4, 2021 |Washington Post 

In January, the City Council decided not to interview all of the applicants and instead chose to interview the top five candidates selected by each Council member. North County Report: Appointment of New Oceanside Leader Making Waves |Kayla Jimenez |February 4, 2021 |Voice of San Diego 

The chosen applicant will also, like any gainfully employed citizen, receive payment and vacation time. Swiss Town: Have Cave, Want (Social and Outgoing) Hermit |Nina Strochlic |May 9, 2014 |DAILY BEAST 

The perfect applicant also needs to be more outgoing than is expected of a traditional hermit. Swiss Town: Have Cave, Want (Social and Outgoing) Hermit |Nina Strochlic |May 9, 2014 |DAILY BEAST 

If the visa is rejected without explanation, the applicant is on the list. Exclusive: Putin Imposes Secret Sanctions on Pro-Gay Obama Campaign Donors |Josh Rogin |May 2, 2014 |DAILY BEAST 

If the applicant indicates that he or she has used illegal drugs, state workers are required to give the individual a drug test. Stop Drug Testing for Welfare Benefits, It's a Dumb Policy |Jamelle Bouie |March 13, 2014 |DAILY BEAST 

Tough love and discipline are the lessons this View host offered her wide-eyed applicant. Watch the 13 Best ‘Glee’ Guest Appearances |Anna Klassen |September 26, 2013 |DAILY BEAST 

The unhappy applicant was naturally obliged to temporarily retire from the game, at all events for that night. The Pit Town Coronet, Volume I (of 3) |Charles James Wills 

When these things are done, the certificate of entry is delivered to the applicant and the entry is made. Putnam's Handy Law Book for the Layman |Albert Sidney Bolles 

The reduction to practice must be by the applicant for a patent, or by his agent; to do this by a third party will not suffice. Putnam's Handy Law Book for the Layman |Albert Sidney Bolles 

Anne was in the hall when he entered, talking to a poor applicant who was waiting to see the Rector. Elster's Folly |Mrs. Henry Wood 

They assume the non-responsibility of the community for anything beyond keeping the destitute applicant alive. English Poor Law Policy |Sidney Webb