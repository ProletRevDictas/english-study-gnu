Every year, there’s any number of similar coronations—or, I guess, condemnations. ‘Alter Ego’ Is the Most Dystopian Reality Competition Yet |Kevin Fallon |September 23, 2021 |The Daily Beast 

That has given the primary the feel of a coronation, although Carroll Foy argued that “not being able to break 50 percent” revealed the former governor's vulnerability. The Trailer: Make history, or bring back TMac? Virginia Democrats are leaning toward the latter. |David Weigel |May 20, 2021 |Washington Post 

Was her coronation as the future queen of pop soul made in haste? The Swedish Queen of Soulful Pop: Mapei Won’t Wait for You to Listen |Caitlin Dickson |October 16, 2014 |DAILY BEAST 

Apparently Democrats do want a coronation but is it because they do not have better choices? Does America Really Want to Coronate Hillary? |Myra Adams |March 14, 2014 |DAILY BEAST 

Therefore, the question “Do Democrats really want a Hillary Clinton coronation?” Does America Really Want to Coronate Hillary? |Myra Adams |March 14, 2014 |DAILY BEAST 

Then the logical follow up, “Will American voters accept a White House coronation?” Does America Really Want to Coronate Hillary? |Myra Adams |March 14, 2014 |DAILY BEAST 

On February 23, Coronation Day, El Chapo arrived with three bands. Drug Cartel Beauty Queens Face an Ugly End |Michael Daly |February 26, 2014 |DAILY BEAST 

From Lisbon the ambassador was summoned to attend the coronation of the Emperor and to take his place among the Marshals. Napoleon's Marshals |R. P. Dunn-Pattison 

The night was passed at anchor off the northernmost Coronation Island. Narrative of a Survey of the Intertropical and Western Coasts of Australia] [Volume 2 of 2] |Phillip Parker King 

In conformity with precedents, the coronation was distinguished by the grant of new honours. The History of England in Three Volumes, Vol.III. |E. Farr and E. H. Nolan 

Each of those named took the posts which, at a real coronation, etiquette would have assigned to them. Chicot the Jester |Alexandre Dumas, Pere 

It was stated in the house of commons shortly after the coronation that the expenses incurred for the coronation of George IV. The History of England in Three Volumes, Vol.III. |E. Farr and E. H. Nolan