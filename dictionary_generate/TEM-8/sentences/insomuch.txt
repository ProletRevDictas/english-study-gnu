And he gave him no answer, not even to one word: insomuch that the governor marvelled greatly. His Last Week |William E. Barton 

Molire was naturally of a reserved and taciturn temper; insomuch that his friend Boileau used to call him the Contemplateur. The Mirror of Literature, Amusement, and Instruction, No. 358 |Various 

Insomuch that he was one of the greatest antiquarians of the age. English Book Collectors |William Younger Fletcher 

Insomuch that we exhorted Titus, that as he had made a beginning before, so he would also complete in you this grace also. Expositor's Bible: The Second Epistle to the Corinthians |James Denney 

Take good heed that you fail not, insomuch as you fear to displease the king my son, and myself. History of the Rise of the Huguenots |Henry Baird