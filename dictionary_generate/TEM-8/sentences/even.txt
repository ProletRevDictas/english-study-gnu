Alcohol and sugar, even in moderate amounts, are not only sinful but poisonous. How Taryn Toomey’s ‘The Class’ Became New York’s Latest Fitness Craze |Lizzie Crocker |January 9, 2015 |DAILY BEAST 

This is even more striking in Submission than in his previous books. Houellebecq’s Incendiary Novel Imagines France With a Muslim President |Pierre Assouline |January 9, 2015 |DAILY BEAST 

Even internally in the House, women are not getting their fair shake. The Unbearable Whiteness of Congress |Dean Obeidallah |January 8, 2015 |DAILY BEAST 

Weiss is likely to get confirmed even as Warren and a handful of other progressive Democrats vote no. Sen. Warren’s Main Street Crusade to Pressure Clinton |Eleanor Clift |January 8, 2015 |DAILY BEAST 

For many years afterward it was a never-ending topic of conversation, and is more or less talked of even to this day. New York’s Most Tragic Ghost Loves Minimalist Swedish Fashion |Nina Strochlic |January 8, 2015 |DAILY BEAST 

Some were even re-arrested for the same nefarious purpose, and the daily papers published their names on each occasion. The Philippine Islands |John Foreman 

Even as they gazed they saw its roof caught up, and whirled off as if it had been a scroll of paper. The Giant of the North |R.M. Ballantyne 

I presume the twenty-five or thirty miles at this end is unhealthy, even for natives, but it surely need not be so. Glances at Europe |Horace Greeley 

Genoa has but recently and partially felt the new impulse, yet even here the march of improvement is visible. Glances at Europe |Horace Greeley 

He was the strangest-looking creature Davy had ever seen, not even excepting the Goblin. Davy and The Goblin |Charles E. Carryl