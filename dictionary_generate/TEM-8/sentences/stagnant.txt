We graduated into the Great Recession, burdened with debt and rewarded with stagnant wages, and endured the slowest economic growth faced by any generation in US history. What giving birth during a pandemic taught me about progress |Katie McLean |February 24, 2021 |MIT Technology Review 

Starting in the 1990s, states sought to replicate the tribal model, gradually enabling nonreservation casinos to promote stagnant economies. Sports gambling could be the pandemic’s biggest winner |Jonathan D. Cohen |February 5, 2021 |Washington Post 

Another issue will be staffing, which has suffered in recent years from stagnant funding and a hiring freeze. All a Gig-Economy Pioneer Had to Do Was “Politely Disagree” It Was Violating Federal Law and the Labor Department Walked Away |by Ken Armstrong, Justin Elliott and Ariana Tobin |January 22, 2021 |ProPublica 

That means that some stagnant lid planets could create an atmosphere and even have temperate climates with liquid water, at least for a time. How the Earth-shaking theory of plate tectonics was born |Carolyn Gramling |January 13, 2021 |Science News 

Baker points out that this means workers may have to take in stagnant air that has been breathed in by people from multiple different households, upping their risk of catching a virus that may be floating around. When it comes to COVID-19 risk, what counts as ‘outdoor’ dining? |Sara Kiley Watson |January 11, 2021 |Popular-Science 

Of course, declining or stagnant wage growth started well before this president took office. Time to Bring Back the Truman Democrats |Joel Kotkin |December 21, 2014 |DAILY BEAST 

Wages are stagnant and middle-class household incomes continue to decline. Voters Remind D.C. That the Economy Still Sucks |Stuart Stevens |November 6, 2014 |DAILY BEAST 

Cory Gardner and others hammered on stagnant wages for the middle class. How’d the GOP Win? By Running Left |Sally Kohn |November 6, 2014 |DAILY BEAST 

Views on the controversial subject, Pew notes, have been more or less stagnant since 2005. The Beautiful Newlywed Who Made the Right Change Its Mind on Physician-Assisted Death |Samantha Allen |October 10, 2014 |DAILY BEAST 

The stagnant pool of green water at the bottom of the ditch rises slightly. Millions of Refugees from Syria’s War Are Clinging to Life In Toxic Conditions |Christopher Looney |April 14, 2014 |DAILY BEAST 

But he forgot the stagnant town, the bald-headed man at the club window, the organ and "The Manola." Bella Donna |Robert Hichens 

A germ flies from a stagnant pool, and the laughing child, its mother's darling, dies dreadfully of diphtheria. God and my Neighbour |Robert Blatchford 

He was a refuge from herself; in his imperious demands her memory slept, her depths were stagnant. Ancestors |Gertrude Atherton 

A stagnant pool among some reeds caught the reflection of the sunset and changed on the instant into raw gold. Uncanny Tales |Various 

It was low and flat, and was traversed by broad ditches, generally full of stagnant water. The Portsmouth Road and Its Tributaries |Charles G. Harper