The hulking platforms of the Cold War, upon which we continued to invest a great deal of financial and operational resources, were suddenly far less relevant. I Was Deeply Involved in War in Afghanistan for More Than a Decade. Here's What We Must Learn |James Stavridis |August 16, 2021 |Time 

His downtown operation, a hulking leaden-blue building with giant fans, could be so exquisitely stinky that we kids would hold our breath and pray that the stoplight stayed green whenever we had to pass it. A Magical Realm of Crabs and Chickens |jversteegh |July 8, 2021 |Outside Online 

The overall effect is reminiscent of a cheerful, hulking bubble. Is Kanye West’s zipperless puffer the start of Gap’s revival? |Marc Bain |June 8, 2021 |Quartz 

Today, purpose-built expedition ships are a far cry from the hulking research vessels of old, and for new ships, luxury is most often the norm. Antarctica cruises are booming. But can the continent handle it? |Elizabeth Heath |April 15, 2021 |Washington Post 

Today, the hulking structure sits abandoned across the street from the tavern like a gray wooden phantom. One-Horse Town |Kieran Dahl |March 31, 2021 |Eater 

Police Officer Daniel Pantaleo then sought to bring the hulking Garner down by yoking him around the neck. Eric Garner Was Just a Number to Them |Michael Daly |December 5, 2014 |DAILY BEAST 

A hulking defender breaks into the backfield and takes him down with a vicious clothesline tackle. Two New Films Preach Our Nation’s Corrosive Gridiron Gospel |Steve Almond |September 20, 2014 |DAILY BEAST 

The beasts are huge, hulking, fast and unpredictable—tons of muscle, horn and thundering hooves. Chicago’s Running of the Bulls |Hampton Stevens |July 26, 2014 |DAILY BEAST 

Patinkin imbues Saul with a hulking presence that fills entire rooms. Give Mandy Patinkin an Emmy Nomination for ‘Homeland,’ Already! |Jason Lynch |July 17, 2013 |DAILY BEAST 

The man who wrote about hulking linebackers nibbling melon in the Texas dusk. An Open Letter to Buzz Bissinger |Sean Macaulay |March 27, 2013 |DAILY BEAST 

A Dutchman—what you would call a Swede—a hulking beggar, came up from the fo'c'sle very much the worse for wear. Jaffery |William J. Locke 

And I understood how it had come to pass that our hulking old ogre had fallen in love with her so desperately. Jaffery |William J. Locke 

Careless Tom, or Hulking Tom (not necessarily in disapproval). Lives of the Most Eminent Painters Sculptors and Architects |Giorgio Vasari 

Who told her John had the fever—a great, strong, hulking fellow like that? John Ingerfield and Other Stories |Jerome K. Jerome 

Mr. Wansley surveyed in silence the hulking, disordered figure now coming forward from the after companion. Cursed |George Allan England