A Scranton news station has a hotline where people call in with random comments and then it airs them. This week in TikTok: How does TikTok know I have ADHD? |Rebecca Jennings |February 9, 2021 |Vox 

The recommended method is to visit a local hospital website or call a hotline. People are fed up with broken vaccine appointment tools — so they’re building their own |Tanya Basu |February 1, 2021 |MIT Technology Review 

For people without computers, some local health authorities are offering phone hotlines to schedule vaccine appointments. A big hurdle for older Americans trying to get vaccinated: Using the internet |Rebecca Heilweil |January 27, 2021 |Vox 

Eligible residents must log on to a city website or call a hotline to reserve an appointment — and all available spots are generally snagged within minutes. As D.C. region sets record for coronavirus deaths, Hogan expands vaccine access in Maryland |Erin Cox, Julie Zauzmer, Meagan Flynn |January 26, 2021 |Washington Post 

When the pandemic closed schools in the spring, the district set up a hotline for students, teachers and parents who were having trouble at home and needed help. Partly hidden by isolation, many of the nation’s schoolchildren struggle with mental health |Donna St. George, Valerie Strauss |January 21, 2021 |Washington Post 

Every shift at LLGS, a counseling hotline which celebrated its 40th birthday earlier this year, was three hours long. Sex, Suicide, and Homework: The Secret World of the Telephone Hotline |Tim Teeman |November 20, 2014 |DAILY BEAST 

Hotline shows you something very moving about watching a teacher patiently explaining a math problem down the phone. Sex, Suicide, and Homework: The Secret World of the Telephone Hotline |Tim Teeman |November 20, 2014 |DAILY BEAST 

The web page for that episode does include a link to a suicide hotline number. The Mommy Blogger Who Tried to Kill Her Autistic Daughter Talks to Dr. Phil |Elizabeth Picciuto |October 1, 2014 |DAILY BEAST 

Roberta Valente, a consultant who works with the Hotline, said laws are struggling to keep up with the changing digital landscape. Digital Harassment Is the New Means of Domestic Abuse |Keli Goff |February 10, 2014 |DAILY BEAST 

His hotline settles the question about where shame belongs in religious communities. Rebel Rabbi Exposes Child Molesters |Moral Courage |January 27, 2014 |DAILY BEAST