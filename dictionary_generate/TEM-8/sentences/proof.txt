The fact that it didn’t is, to me, proof that what happened to Taylor—being gunned down in her own bed, by men who are still walking free—is not just going to just drift away as the media moves on to the next headline. Breonna Taylor’s Vanity Fair portrait is more than a magazine cover |kristenlbellstrom |August 25, 2020 |Fortune 

“Theoretically, those with poor immune systems may have a higher chance of reinfection, but we don’t have the proof yet,” To said. What the first confirmed COVID-19 reinfection tells us about a future vaccine |Naomi Xu Elegant |August 25, 2020 |Fortune 

She said she first notified the authority on June 19, the day she lost her job, but was asked to bring proof that she no longer worked a second job at a cleaning company and to provide updated paycheck stubs. She Was Sued Over Rent She Didn’t Owe. It Took Seven Court Dates to Prove She Was Right. |by Danielle Ohl, Capital Gazette, and Talia Buford and Beena Raghavendran, ProPublica |August 25, 2020 |ProPublica 

As Hollywood eases back into production while coronavirus cases rise, TV networks’ and streaming services’ interest in so-called “corona-proof” programming has taken on a new twist. ‘Covid-proof production’: TV networks, streamers seek out show formats that can adapt to another shooting shutdown |Tim Peterson |August 14, 2020 |Digiday 

The image below clearly indicates the benefits of social proof. Studying the anatomy of a successful high-conversion landing page |Yasmine Dehimi |June 22, 2020 |Search Engine Watch 

Park employees helped John quit tobacco by way of a butts-proof glass enclosure, a drastic change in diet, and regular exercise. Zebra Finches, Dolphins, Elephants, and More Animals Under the Influence |Bill Schulz |December 31, 2014 |DAILY BEAST 

Although often this is considered proof positive of guilt at trial, it is not an uncommon occurrence in false confessions. How the U.S. Justice System Screws Prisoners with Disabilities |Elizabeth Picciuto |December 16, 2014 |DAILY BEAST 

And if you want proof of what the country is really all about, just walk through the National September 11 Memorial Museum. Dick Cheney vs. ‘Unbroken’ |Mike Barnicle |December 15, 2014 |DAILY BEAST 

They're also proof that no matter how fancy you are, you can't escape the urge to watch two girls make out. High-End Pervs Film Benedict Cumberbatch and Reese Witherspoon Sucking Face |Amy Zimmerman |December 11, 2014 |DAILY BEAST 

Without proof of this kind, the story of the lost legions is just a legend. The Chinese Town Descended From Romans? |Nina Strochlic |December 4, 2014 |DAILY BEAST 

Here convincing proof was given of Mme. Mesdag's accuracy, originality of interpretation, and her skill in the use of color. Women in the fine arts, from the Seventh Century B.C. to the Twentieth Century A.D. |Clara Erskine Clement 

Proof was given to him, of Elizabeth having admitted Ripperda to private political discussions in the Altheim apartments. The Pastor's Fire-side Vol. 3 of 4 |Jane Porter 

Wasn't the dead man stretched in the shadow convincing proof of their capacity for pure devilishness? Raw Gold |Bertrand W. Sinclair 

For my part, I scarcely know what to say; inasmuch as I do not care either to affirm or deny a thing of which I have no proof. The Jesuit Relations and Allied Documents, Vol. II: Acadia, 1612-1614 |Various 

If I am proof against my own heart, in so dear a cause, shall I not be proof against the poor allurements of vanity and sense? The Pastor's Fire-side Vol. 3 of 4 |Jane Porter