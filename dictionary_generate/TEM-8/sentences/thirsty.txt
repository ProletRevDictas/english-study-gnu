Right now, Pauline says, farmers measure soil moisture to see if their plants are thirsty. Student scientists work to help all of us survive a warmer world |Bethany Brookshire |October 21, 2020 |Science News For Students 

Fuel-thirsty four-engine jets, like the Boeing 747 and Airbus A380, are decidedly passé these days, while aircraft with just two engines represent both the present and future of air travel. The world’s biggest jet engine, explained |Rob Verger |October 14, 2020 |Popular-Science 

His measurements astounded him—the force the spores generated as they cycled between small and thirsty and massive and moist was enough to act like a powerful, humidity-controlled muscle. Imitation Is the Sincerest Form of Environmentalism - Issue 90: Something Green |Anastasia Bendebury & Michael Shilo DeLay |October 7, 2020 |Nautilus 

This one is designed to hold onto moisture, and is best for thirsty plants like cucumbers. Indoor potting mixes for a thriving houseplant jungle |PopSci Commerce Team |September 24, 2020 |Popular-Science 

Millions took up the invitation, replacing hardy prairie grass with thirsty crops like corn, wheat and cotton. Climate Change Will Force a New American Migration |by Abrahm Lustgarten, photography by Meridith Kohut |September 15, 2020 |ProPublica 

For thirsty residents in dry counties, that means another long drive for a beer. Will Arkansas’ Prohibition Finally End? |Jack Holmes |November 1, 2014 |DAILY BEAST 

We are thirsty for his signature wit, warmth, and homespun wisdom. Dear Dick (Cheney): Advice From the Former Veep |Kelly Williams Brown |June 21, 2014 |DAILY BEAST 

And when I did finally get out of bed, I was so thirsty I made my way to the kitchen crawling along the floor. Get Into Bed with Tracey Emin for $2 Million: The Sale of a British Art Icon |Tim Teeman |May 28, 2014 |DAILY BEAST 

Today, she takes the formula from me and sucks down every last drop of liquid like a desert-thirsty nomad. When An Adopted Child Won’t Attach |Tina Traster |May 2, 2014 |DAILY BEAST 

But the problem was that among that active part there were a small number of  “power thirsty commercially interested” activists. Putin’s Men in Ukraine: We’re Not Giving In |Anna Nemtsova |April 18, 2014 |DAILY BEAST 

Then he felt thirsty, so he looked around until he had found another spring of cool water, where he drank as much as he needed. Squinty the Comical Pig |Richard Barnum 

Meeting the thirsty bring him water, you that inhabit the land of the south, meet with bread him that fleeth. The Bible, Douay-Rheims Version |Various 

And that which was dry land, shall become a pool, and the thirsty land springs of water. The Bible, Douay-Rheims Version |Various 

When night came they found themselves far from any stream; all were thirsty and there was no water. Alila, Our Little Philippine Cousin |Mary Hazelton Wade 

This made the eaters very thirsty, and quite ready to drink quantities of palm wine afterward. Alila, Our Little Philippine Cousin |Mary Hazelton Wade