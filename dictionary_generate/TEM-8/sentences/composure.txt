We came out and made a few of them to start the game, but we kept our composure. Georgetown’s seniors lead the way in a statement victory over Creighton |Kareem Copeland |February 4, 2021 |Washington Post 

It looks like Williams takes a second to maintain her composure, but reminds Omarosa that she is in fact a guest on her show. A Look Back At Wendy Williams’ Most Talked About Interviews |Steven Psyllos |February 2, 2021 |Essence.com 

The process to select officers was created by Admiral Hyman Rickover—the engineering visionary and noted madman who put the first nuclear reactor in a submarine—to assess both technical acumen and composure under stress. How a Nuclear Submarine Officer Learned to Live in Tight Quarters - Issue 94: Evolving |Steve Weiner |December 30, 2020 |Nautilus 

If “Borat Subsequent Moviefilm” is forcing me to pick a hero, I’ll side with the ostensible rubes at a cotillion ball or a hard-working babysitter rather than the actors trying to shock them into breaking their composure. You should side with Borat’s victims, not mock them |Sonny Bunch |October 30, 2020 |Washington Post 

So we need to keep our composure and ask questions and seek to understand the key factors that are contributing to us being in this situation. 7 life lessons on leadership during COVID and beyond |Sponsored Content: Integrate |October 26, 2020 |Search Engine Land 

He recalled being angry and trying to keep his composure as he strode past Cosby into the front hall. ‘I Saved My Friend From Bill Cosby’ |Lloyd Grove |December 3, 2014 |DAILY BEAST 

Through it all, though, Holder has maintained his composure and remained focused on his legal and constitutional responsibilities. Eric Holder’s Legacy: Bold on Equality, Less So on Civil Liberties |Geoffrey R. Stone |September 26, 2014 |DAILY BEAST 

I regained my composure and stepped fully inside the crowded hallway. Inside a Hospital for the Criminally Insane |Caitlin Dickson |September 15, 2014 |DAILY BEAST 

At several points during his narrative, he has to stop to regain his composure. The Crash and Churn of Lawrence O’Donnell |Lloyd Grove |June 20, 2014 |DAILY BEAST 

Throughout her shows, she has had to stop singing to regain her composure. Miley Cyrus Pays Bizarre Tribute to Her Dead Dog, Floyd, at a Brooklyn Concert |Marina Watts |April 7, 2014 |DAILY BEAST 

It nettled him; it broke down his habitual composure, and it was a relief to him when the conference came to a close. The Homesteader |Oscar Micheaux 

And when he reached her side, she had so entirely regained composure that he could hardly believe it was the same person. The Wave |Algernon Blackwood 

“It is quite understandable, dear,” Jessie said, with more composure than her chum could display at the moment. The Campfire Girls of Roselawn |Margaret Penrose 

He smiled, and Barker, who had recovered his composure in the doctor's presence, bowed silently and went out. Three More John Silence Stories |Algernon Blackwood 

At first Baroudi continued to stand in the sun, and she looked up at him with composure from her place of shadow. Bella Donna |Robert Hichens