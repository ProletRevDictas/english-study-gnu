As an example of good science-and-society policymaking, the history of fluoride may be more of a cautionary tale. Anti-Fluoriders Are The OG Anti-Vaxxers |Michael Schulson |July 27, 2016 |DAILY BEAST 

But on Thursday Boxer triggered a Golden State political earthquake, announcing that she would not seek a fifth term in 2016. The Golden State Preps for the ‘Red Wedding’ of Senate Races |David Freedlander |January 9, 2015 |DAILY BEAST 

As this list shows, punishments typically run to a short-ish jail sentence and/or a moderately hefty fine. In Defense of Blasphemy |Michael Tomasky |January 9, 2015 |DAILY BEAST 

Yes, Byrd—dead four-and-a-half years now—was a Kleagle in the Ku Klux Klan. Steve Scalise and the Right’s Ridiculous Racial Blame Game |Michael Tomasky |January 2, 2015 |DAILY BEAST 

Parents who want to transfer custody of a child to someone other than a relative must seek permission from a judge. Judge: Rehoming Kids Is Trafficking |Tina Traster |December 30, 2014 |DAILY BEAST 

Poor Squinty ran and tried to hide under the straw, for he knew the boy was talking about him. Squinty the Comical Pig |Richard Barnum 

Hence arise factions, dissensions, and loss to their religious interests and work; and these intruders seek to rule the others. The Philippine Islands, 1493-1898, Volume XX, 1621-1624 |Various 

She also practises etching, pen-and-ink drawing, as well as crayon and water-color sketching. Women in the fine arts, from the Seventh Century B.C. to the Twentieth Century A.D. |Clara Erskine Clement 

How much of the imagination, how much of the intellect, evaporates and is lost while we seek to embody it in words! Pearls of Thought |Maturin M. Ballou 

Instinctively he tried to hide both pain and anger—it could only increase this distance that was already there. The Wave |Algernon Blackwood