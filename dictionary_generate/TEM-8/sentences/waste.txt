A lot of time and energy is wasted among analysts in debating how exactly to characterize skewed maps that result from residential segregation. America needs a democratic revolution |Matthew Yglesias |September 17, 2020 |Vox 

Not surprisingly, the vast waste deposits produced during the Great Acceleration figure prominently in the search for a suitable stratigraphic section to place the GSSP that will mark the start of the Anthropocene. Dawn of the Heliocene - Issue 90: Something Green |Summer Praetorius |September 16, 2020 |Nautilus 

Some of the ones my other friend got were round, which wastes space. How (and Why) to Execute the Perfect Canoe Portage |Alex Hutchinson |September 9, 2020 |Outside Online 

“If you don’t find out what the answer is, you’re kind of wasting your time,” he says. Top 10 tips on how to study smarter, not longer |Kathiann Kowalski |September 9, 2020 |Science News For Students 

You do not have to waste your time on the same project again and use it on another channel or place to reach your target audience quickly and easily. How businesses can use YouTube to tackle the COVID-19 business crisis |Catherrine Garcia |September 7, 2020 |Search Engine Watch 

When twelve people are killed by violence, whoever they are, for whatever reason, that is a tragedy and a waste. Trolls and Martyrdom: Je Ne Suis Pas Charlie |Arthur Chu |January 9, 2015 |DAILY BEAST 

First, though, he has to be shocked into recognizing the barren waste of his spiritual life – by spirits. How Dickens and Scrooge Saved Christmas |Clive Irving |December 22, 2014 |DAILY BEAST 

He said he watched waste haulers back up to the pit and unleash torrents of watery muck. Two Texas Regulators Tried to Enforce the Rules. They Were Fired. |David Hasemyer, InsideClimate News |December 9, 2014 |DAILY BEAST 

Kocurek became especially frustrated with a commercial waste facility in Jim Wells County. Two Texas Regulators Tried to Enforce the Rules. They Were Fired. |David Hasemyer, InsideClimate News |December 9, 2014 |DAILY BEAST 

They also used the powers of their separate agencies to cite waste haulers for spilling sludge along roadways. Two Texas Regulators Tried to Enforce the Rules. They Were Fired. |David Hasemyer, InsideClimate News |December 9, 2014 |DAILY BEAST 

It is the principal waste-product of metabolism, and constitutes about one-half of all the solids excreted—about 30 gm. A Manual of Clinical Diagnosis |James Campbell Todd 

Then the croupier tears open two packets of new cards, flinging the old ones into a waste-paper basket at his side. The Pit Town Coronet, Volume I (of 3) |Charles James Wills 

Don't waste your valuable time looking for the biggest angleworm in the garden! The Tale of Grandfather Mole |Arthur Scott Bailey 

In a literal sense, too,” added Tom Brown, “for it will be sold as waste-paper and be made up into matches. Hunting the Lions |R.M. Ballantyne 

Because in the night Ar of Moab is laid waste, it is silent: because the wall of Moab is destroyed in the night, it is silent. The Bible, Douay-Rheims Version |Various