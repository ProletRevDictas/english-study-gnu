It’s jolly good fun—and a great way to get your blood flowing after a long day on the slopes. The Best At-Home Après Gear of 2022 |jversteegh |October 26, 2021 |Outside Online 

These weren’t people jollied by the guy with the funny hair. How the long-dead public-television painter Bob Ross became a streaming phenomenon (and kicked up plenty of dirt in the process) |Steven Zeitchik |July 30, 2021 |Washington Post 

There aren’t any non-Jewish publishers anywhere, they control the media—jolly clever thing to do—that’s why the president of the United States has to sell all this stuff to Israel. What to Know About Children's Author Roald Dahl's Controversial Legacy |Megan McCluskey |March 18, 2021 |Time 

I was just screaming, walking up hallways, just this big jolly queen. The Story Behind Big Freedia’s Breakout Beyoncé Sample |Daniel Malloy |January 27, 2021 |Ozy 

Chairman Neil Chatterjee, a former policy adviser to Senate Majority Leader Mitch McConnell, shared the news in a jolly Twitter post. It’s His Land. Now a Canadian Company Gets to Take It. |by Lee van der Voo for ProPublica |October 1, 2020 |ProPublica 

The incident sparked his belief in Santa, but he would have to wait nearly two decades before dressing up as Jolly St. Nick. Kerry Bentivolio: The Congressman Who Believes in Santa Claus |Ben Jacobs |December 24, 2014 |DAILY BEAST 

Like Jolly, most of the women raced other motorized vehicles before making it into Monster Jam. The Moms of Monster Jam Drive Trucks, Buck Macho Culture |Eliza Krigman |November 22, 2014 |DAILY BEAST 

Jolly somehow finds the time to also manage a restaurant and help kids who have Autism and Down syndrome ride horses. The Moms of Monster Jam Drive Trucks, Buck Macho Culture |Eliza Krigman |November 22, 2014 |DAILY BEAST 

Jolly, who entered the racing world when she was eight years old, remembers being taunted as a kid. The Moms of Monster Jam Drive Trucks, Buck Macho Culture |Eliza Krigman |November 22, 2014 |DAILY BEAST 

Jolly and Creten, who are both married to Monster Jam drivers (Neil Elliott and Jimmy Creten, respectively), have kids. The Moms of Monster Jam Drive Trucks, Buck Macho Culture |Eliza Krigman |November 22, 2014 |DAILY BEAST 

Mrs. Jolly Robin had often wished—when she was trying to feed a rapidly-growing family—that she could hunt forp. The Tale of Grandfather Mole |Arthur Scott Bailey 

And then Jolly Robin would feel ashamed that he had even thought of being so cruel to an infant bird, even if he was a Cowbird. The Tale of Grandfather Mole |Arthur Scott Bailey 

And then, probably, Jolly Robin would laugh and tell her not to mind, for there ought to be worms enough for everybody. The Tale of Grandfather Mole |Arthur Scott Bailey 

Then a shower of dirt flew into their faces and both Jolly Robin and his wife tumbled over backward. The Tale of Grandfather Mole |Arthur Scott Bailey 

While the refined exquisite was giving his order, a jolly western drover had listened with opened mouth and protruding eyes. The Book of Anecdotes and Budget of Fun; |Various