Allergic reactions to the coronavirus vaccine are rare and far outweighed by the benefits. Many who have received the coronavirus vaccine wonder: What can I safely do? |Laurie McGinley, Lenny Bernstein |February 1, 2021 |Washington Post 

I don’t think taking another gap year would outweigh future earnings for me, but I know people had to make that tough decision. The pandemic’s lasting effects on young medical workers |Terry Nguyen |February 1, 2021 |Vox 

The value of the early implant will be enormous and outweigh the risks, is his take. Elon Musk busts Clubhouse limit, fans stream to YouTube, he switches to interviewing Robinhood CEO |Mike Butcher |February 1, 2021 |TechCrunch 

The senior knew he may never play for his new team, but the chance to prove himself on a big stage outweighed the uncertainty. In search of top competition, high school basketball powerhouses are still crisscrossing the country |Kyle Melnick |January 21, 2021 |Washington Post 

In the long run, I believe the benefits will far outweigh the costs, especially if we care about helping people who have been struggling for a very long time. Janet Yellen tells lawmakers it’s ‘critically important to act now’ on economic relief plan |Erica Werner, Jeff Stein |January 19, 2021 |Washington Post 

Prevalence depends on context, and sometimes unique advantages outweigh the genetic costs. Mongooses, Meerkats, and Ants, Oh My! Why Some Animals Keep Mating All in the Family |Helen Thompson |December 29, 2014 |DAILY BEAST 

They outnumber and may even outweigh all other forms of life. Why Did It Take So Long For Complex Life To Evolve On Earth? Blame Oxygen. |Matthew R. Francis |November 2, 2014 |DAILY BEAST 

Still, given the lethal nature of Ebola, many believe the potential benefits far outweigh the risk. Blood Is Ebola’s Weapon and Weakness |Abby Haglage |October 26, 2014 |DAILY BEAST 

But the risks of eating clay outweigh the purported benefits. You Probably Shouldn’t Try to Lose 20 Pounds by Eating Clay |Kent Sepkowitz |June 24, 2014 |DAILY BEAST 

Caulkins suggests that the current failures of prohibition are “overstated” and that the benefits may outweigh the costs. Economists Slam the War on Drugs in a New London School of Economics Report |Abby Haglage |May 6, 2014 |DAILY BEAST 

If the reasons in favour of sobriety seem to him to outweigh the reasons in favour of drink, he will keep sober. God and my Neighbour |Robert Blatchford 

Nor did the safety of the state, which would outweigh all such considerations, require the step. The Political History of England - Vol. X. |William Hunt 

Nevertheless the comfort of an employer ought not to outweigh justice to an employee. Wanted, a Young Woman to Do Housework |C. Helene Barker 

That money and the honor he could acquire must have been tempting to the waif, but it did not outweigh his human sympathy. Mark Twain, A Biography, 1835-1910, Complete |Albert Bigelow Paine 

Still, he could not believe that her desire for revenge would outweigh all her maternal feelings. Ruth Hall |Fanny Fern