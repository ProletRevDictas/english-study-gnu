I ate mouthful after mouthful, maybe filling back in the parts of me that had been carefully excised days before. How Ice Cream Became My Own Personal Act of Resistance |Taylor Harris |November 10, 2021 |Time 

They roam through tree-shaded meadows, tearing up mouthfuls of clover while nursing their calves in tranquility. The Cow That Could Feed the Planet |Aryn Baker/Maastricht, Netherlands |November 2, 2021 |Time 

With that in mind, some of the best dental chews for dogs are made with a mouthful of chemical compounds and vitamins designed to offer healthy benefits. Best dental chews for dogs to brush up on oral health |Andrew Waite |September 23, 2021 |Popular-Science 

A mouthful of ingredients goes into making Greenies dental treats, which have been manufactured in Kansas City since 1996. Best dental chews for dogs to brush up on oral health |Andrew Waite |September 23, 2021 |Popular-Science 

Between mouthfuls of sandwich, he swore we’d end up packing everything growing out back anyway—there wasn’t really a point to tending them now. ‘When the Rain Stops:’ a New Short Story by Bryan Washington |Bryan Washington |April 16, 2021 |Time 

He took a final mouthful of orange soda and glanced back at his girlfriend, Hutchins. Money, Murder, and Adoption: The Wild Trial of the Polo King |Jacqui Goddard |October 28, 2014 |DAILY BEAST 

It was 4:30 in the morning, I had a mouthful of blood and raw chicken and it just—you get yourself into a very strange place. ‘The Walking Dead’ Star Andrew Lincoln on the Terminus Cannibals Theory & Season Finale |Melissa Leon |March 31, 2014 |DAILY BEAST 

The Swedish plane has got a mouthful of a name: the JAS 39E Gripen. The Planet’s Best Stealth Fighter Isn’t Made in America |Bill Sweetman |March 24, 2014 |DAILY BEAST 

It was a mouthful of a lesson, but after learning it once, you never had to memorize it again. China’s Schools Teaches Kids to Take Tests, Obey the State, and Not Much More |Junheng Li |November 30, 2013 |DAILY BEAST 

Food in Calabria was pride, self-sufficiency and community all mixed together in one mouthful. A Young Chef Travels to Calabria, Italy, and Learns the Old Ways of Cooking |Curtis Stone |November 28, 2013 |DAILY BEAST 

Haggard was in a state of suppressed excitement, and he couldn't eat a mouthful. The Pit Town Coronet, Volume I (of 3) |Charles James Wills 

He saw a large sheath-knife, and secured that in his own belt; then he took a mouthful of wine, and went to his post. The Chequers |James Runciman 

Here was porridge enough to last a small boy a lifetime, and he could not stop to taste one mouthful! Stories the Iroquois Tell Their Children |Mabel Powers 

With the disappearance of the last mouthful on her plate, Tilly drew a long breath. The Sunbridge Girls at Six Star Ranch |Eleanor H. (Eleanor Hodgman) Porter 

Young Joe tried to smile, with a slice of chicken in one hand and a spoonful of preserves in the other, and a mouthful of both. The Rival Campers |Ruel Perley Smith