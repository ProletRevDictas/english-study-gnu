Hugh Newell Jacobsen’s work could be difficult to characterize and often left critics floundering for shorthand descriptions. Hugh Newell Jacobsen, award-winning modernist architect, dies at 91 |Kathy Orton |March 4, 2021 |Washington Post 

Since then, GameStop has become shorthand for short investors getting “squeezed” as they scramble to cover their losses. Even after GameStop, Tesla remains the most shorted stock in the world |Michael J. Coren |March 2, 2021 |Quartz 

Users celebrated their “tendies,” a shorthand for chicken tenders that meant profits. The GameStop stock craze is about a populist uprising against Wall Street. But it’s more complicated than that. |David J. Lynch |February 1, 2021 |Washington Post 

So employers might consider the idea of hiring immigrants as shorthand for hiring people who’ve worked outside traditional systems to advance, who’ve grappled with trauma or uncertainty, who’ve seen some life. Why an immigrant mindset is such a valuable asset during COVID |Mitra Kalita |December 2, 2020 |Fortune 

They should only be used as a shorthand for personnel, not necessarily for a formation. In modern defensive fronts, the names have been changed |Richard Johnson |November 23, 2020 |Washington Post 

She said Wright, whom the girl refers to as the shorthand “J,” also sensed that the heat was on. The Navy ‘Hero’ Who Pimped an HIV-Positive Teen |M.L. Nestel |December 11, 2014 |DAILY BEAST 

In doing these interviews and talking about her these past seasons, have you developed a shorthand for describing her personality? How Carrie Preston Became The Good Wife’s Favorite Scene Stealer |Kevin Fallon |October 20, 2014 |DAILY BEAST 

That serves as shorthand for Mercedes, BMW, Audi and Porsche. Nationalism on Four Wheels |Clive Irving |October 18, 2014 |DAILY BEAST 

Throw in the period setting and “like Mad Men during WWII” shorthand becomes inevitable. WGN’s ‘Manhattan’ Is Summer’s Best New Show. But Will Anyone Watch? |Kevin Fallon |July 27, 2014 |DAILY BEAST 

The media, forever starved for lazy shorthand, have gone along with the labeling too. The New Right-Wing Idol: Working Moms |Tim Teeman |July 16, 2014 |DAILY BEAST 

By this new species of shorthand we might have embodied this very article in half a dozen sprightly etchings! Blackwood's Edinburgh Magazine, No. CCCXXXIX. January, 1844. Vol. LV. |Various 

Is it possible that money orders could be sent to someone just by using initials or some shorthand name? Warren Commission (10 of 26): Hearings Vol. X (of 15) |The President's Commission on the Assassination of President Kennedy 

He takes the precaution to station a shorthand writer in a concealed part of the room. A Cursory History of Swearing |Julian Sharman 

It seems strangely like old times to me to be making these jottings in Pitman's shorthand. In Accordance with the Evidence |Oliver Onions 

"I'm needing another shorthand man, and I can afford to pay a good bit more than that," he growled. The Wreckers |Francis Lynde