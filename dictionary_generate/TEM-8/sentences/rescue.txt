Storm surges can flood buildings, trapping people inside or forcing them onto roofs, where they wait for rescue. Soggy coastal soils? Here’s why ecologists love them |Alison Pearce Stevens |September 17, 2020 |Science News For Students 

There’s no domestic champion that’s going to come to Huawei’s rescue on the semiconductor side. ‘A real uphill battle’: Why China will struggle to counter U.S.’s attack on Huawei |Veta Chan |September 10, 2020 |Fortune 

He has called in the entire state’s National Guard to help rescue people stuck in the storm’s path. Hurricane Laura is the strongest storm to hit Louisiana in more than a century |Sara Chodosh |August 28, 2020 |Popular-Science 

Yet old property hands are closely watching the implications both for Simon and its rivals, especially as it eyes yet more rescue bids. America’s Largest Shopping Mall Owner Gets a New Tenant: Itself |Daniel Malloy |August 20, 2020 |Ozy 

He knows what it takes to rescue an economy, beat back a pandemic, and lead our country. ‘He is clearly in over his head’: Read Michelle Obama’s full speech denouncing Donald Trump |kdunn6 |August 18, 2020 |Fortune 

He was killed by his captors during the U.S. rescue attempt in Yemen in December. ISIS Fight Has a Spy Shortage, Intel Chair Says |Kimberly Dozier |January 2, 2015 |DAILY BEAST 

As night fell, the rescue operation slowed and sea conditions worsened. ‘We’re Going to Die’: Survivors Recount Greek Ferry Fire Horror |Barbie Latza Nadeau |December 29, 2014 |DAILY BEAST 

It took 12 hours to rescue just 100 passengers overnight Sunday. ‘We’re Going to Die’: Survivors Recount Greek Ferry Fire Horror |Barbie Latza Nadeau |December 29, 2014 |DAILY BEAST 

Then they set sail for open water, where they were assured someone would rescue them. Inside the Smuggling Networks Flooding Europe with Refugees |Barbie Latza Nadeau |December 15, 2014 |DAILY BEAST 

But the ships deployed already have been involved in the rescue of more than 1,000 people during their first month of operation. Inside the Smuggling Networks Flooding Europe with Refugees |Barbie Latza Nadeau |December 15, 2014 |DAILY BEAST 

From this one source of misery, where was a promise or a chance of a final rescue? Blackwood's Edinburgh Magazine, No. CCCXXXIX. January, 1844. Vol. LV. |Various 

Would Hodson, knowing the exceeding importance of his mission, have turned to rescue a servant or raise a fallen horse? The Red Year |Louis Tracy 

The porter of the firm mercifully interposed to rescue Mr Brammel from his dilemma. Blackwood's Edinburgh Magazine, No. CCCXXXIX. January, 1844. Vol. LV. |Various 

Others now rushed to the rescue, the artillery men came back, and once more the guns were thundering their defiance. The Courier of the Ozarks |Byron A. Dunn 

Then, in a bitter temper, she stooped again to rescue the bit of discolored paper that had fallen with the pearls. The Red Year |Louis Tracy