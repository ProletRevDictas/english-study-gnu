This fancy spice pack pairs with four different spirits—vodka, tequila, aquavit, and gin—to ensure the perfect morning pick-me-up. The Daily Beast’s 2014 Holiday Gift Guide: For the Don Draper in Your Life |Allison McNearney |November 29, 2014 |DAILY BEAST 

Tsongas and Rep. Mike Michaud (D-ME) have led the charge for an all-American recruit shoe in Congress. New Balance Lobbies Congress to Make the U.S. Military's Only Running Shoe |Tim Mak |September 8, 2014 |DAILY BEAST 

But instead of a witty pop song, we have yet more woe-is-me-feel-my-pain from an overpaid, over-cosseted celebrity. Why We Should Hate 'Haters Gonna Hate' |Tim Teeman |August 25, 2014 |DAILY BEAST 

I would rather not say it annoys me because it sounds so aggrieved and me-centered. The Author Of The Summer's Hit Paranoid Fantasy Opens Up |William O’Connor |August 15, 2014 |DAILY BEAST 

Played in reverse that becomes ‘Here’s me/Here I am/What we have lost/I am the messenger of love. The Stacks: The Judas Priest Teen Suicide Trial |Ivan Solotaroff |June 28, 2014 |DAILY BEAST 

And yet these years spent in cafés and in studios have not turned them out into the world a devil-me-care lot of dreamers. The Real Latin Quarter |F. Berkeley Smith 

Great fire at Eastport, Me., by which the larger portion of the business part of the town was destroyed. The Every Day Book of History and Chronology |Joel Munsell 

Desertion doesn't mean a sea of water between, it means an ocean of self-will and love-me-first between. You Never Know Your Luck, Complete |Gilbert Parker 

For he's the soul of honor, Thyrsis; and he can't help how he feels about me-any more than I can help it. Love's Pilgrimage |Upton Sinclair 

Her silk parasol, of the blue of a forget-me-not, rested against her knee, and at her breast was a cream-tinted rose. The Rake's Progress |Marjorie Bowen