When Deng Jiaxian died in 1986, Yang wrote an emotional eulogy for his friend, who had devoted his life to China’s nuclear defense. China’s path to modernization has, for centuries, gone through my hometown |Yangyang Cheng |June 30, 2021 |MIT Technology Review 

While Manchin isn’t yet ready to offer a eulogy for the filibuster, you can see in the West Virginian the same level of frustration with Washington that threatened his decision to seek a second term in 2018. Why Kyrsten Sinema Could Control the Future of the Filibuster |Philip Elliott |June 4, 2021 |Time 

Jordan was often asked to give a speech, a toast, a eulogy — and never disappointed. How Vernon Jordan became a one-of-a-kind Washington presence |Roxanne Roberts |March 3, 2021 |Washington Post 

McClenton, himself a Baptist pastor, mustered the strength to preach his son’s eulogy. A Temp Worker Died on the Job After FedEx Didn’t Fix a Known Hazard. The Fine: $7,000. |by Wendi C. Thomas, MLK50: Justice Through Journalism |December 23, 2020 |ProPublica 

Millions of people are dying, but mobile phones are a vehicle to make those people more real, to use these spaces to create eulogies, to record and take pictures. The way we express grief for strangers is changing |Tanya Basu |December 3, 2020 |MIT Technology Review 

In his eulogy for Rosa Parks, Jesse Jackson gave a history lesson to the American people—and to then President George W. Bush. How We Got to Ferguson—a Reading List |David Masciotra |August 23, 2014 |DAILY BEAST 

“I will, at any point in my life, other than giving a eulogy, try to make people laugh,” he said. Pol’s ‘Spoof Ad’ Aims for LOLZ—and Senate Seat |Olivia Nuzzi |April 10, 2014 |DAILY BEAST 

This eulogy was given at Arlington National Cemetery two weeks later. A Man to Believe In: Eulogy for Marine Master Sergeant Aaron Torian |Elliot Ackerman |March 5, 2014 |DAILY BEAST 

“Any of her teen children with a license were fair game to recruit as well,” her son, Thomas, would say in his eulogy. Remembering Ma Laureys, the Mother of 10 Christie Slandered to Win His First Election |Michael Daly |January 23, 2014 |DAILY BEAST 

The eulogy ends before it begins and Thackeray is barely alluded to again, let alone revered. John Sutherland‘s Enjoyable Little History of Literature |Malcolm Forbes |November 29, 2013 |DAILY BEAST 

As she made herself comfortable in his deepest chair she heard the girlish shallow voice launch out into a eulogy of the scenery. Ancestors |Gertrude Atherton 

That greatly excited my rivalry, and I succeeded in finding some reasons for eulogy that she had forgotten. Charles Baudelaire, His Life |Thophile Gautier 

I pulled myself hastily into a more popular strain with a gross eulogy of my opponent's good taste. The New Machiavelli |Herbert George Wells 

But just eulogy of the dead is the appropriate duty of those who were the associates and friends of the founder of this school. Thoughts on Educational Topics and Institutions |George S. Boutwell 

Bradford in written eulogy ascribes to him "ye tender love & godly care of a true pastor." William Bradford of Plymouth |Albert Hale Plumb