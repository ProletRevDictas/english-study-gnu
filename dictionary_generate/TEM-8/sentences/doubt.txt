When you’re concerned with all things at one time, things become less obvious — and that’s what constitutes doubt. Don’t Blame The Refs For All Of These Replay Reviews |Jared Dubin |September 17, 2020 |FiveThirtyEight 

I mention this not to sow doubt in your mind, not to scare you, but rather to prepare you. ‘How I Built This’ host Guy Raz on insights from some of the world’s most famous entrepreneurs |Rachel King |September 15, 2020 |Fortune 

It’s thrown her dreams of opening a full-service dine-in restaurant or a franchising strategy into doubt. The Big Corporate Rescue and the America That’s Too Small to Save |by Lydia DePillis, Justin Elliott and Paul Kiel |September 12, 2020 |ProPublica 

Explaining the reasoning behind each data point and action in your SEO proposal, and arguing from first principles, should leave less room for doubt and more for critical thinking. SEO proposals: Particular challenges and how to avoid getting a silent no |SEOmonitor |September 10, 2020 |Search Engine Watch 

Without a doubt, there’s a link between the sound and meaning for animals. Talking Is Throwing Fictional Worlds at One Another - Issue 89: The Dark Side |Kevin Berger |September 9, 2020 |Nautilus 

And, in the case of fluoride, at least, that doubt might actually be justified. Anti-Fluoriders Are The OG Anti-Vaxxers |Michael Schulson |July 27, 2016 |DAILY BEAST 

Stephanie Giorgio, a classical musician, credits The Class for helping her cope with anxiety, focus, fear, and self-doubt. How Taryn Toomey’s ‘The Class’ Became New York’s Latest Fitness Craze |Lizzie Crocker |January 9, 2015 |DAILY BEAST 

He no doubt had heard by then that some of the cops had ignored his request and turned their backs. Funeral Protest Is Too Much for NYPD Union Boss |Michael Daly |January 5, 2015 |DAILY BEAST 

Their confrontation at dinner was, without a doubt, the highlight of the episode. ‘Downton Abbey’ Review: A Fire, Some Sex, and Sad, Sad Edith |Kevin Fallon |January 5, 2015 |DAILY BEAST 

But self-doubt, while a healthy quality for human beings to have, is alas not a plus for politicians. Mario Cuomo: An OK Governor, but a Far Better Person |Michael Tomasky |January 2, 2015 |DAILY BEAST 

There was no doubt thought of his own loss in this question: yet there was, one may hope, a germ of solicitude for the mother too. Children's Ways |James Sully 

Elyon is the name of an ancient Phœnician god, slain by his son El, no doubt the “first-born of death” in Job xviii. Solomon and Solomonic Literature |Moncure Daniel Conway 

The patache was never seen again, and there is not much doubt that it was lost with all hands on board. The Philippine Islands, 1493-1898, Volume XX, 1621-1624 |Various 

Her feet felt rooted to the floor in the wonder and doubt of this strange occurrence. The Bondboy |George W. (George Washington) Ogden 

But I doubt if he feels any particular emotion himself, when he is piercing you through with his rendering. Music-Study in Germany |Amy Fay