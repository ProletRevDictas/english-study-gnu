I generally do not bother the officers because they do not usually answer my questions and only offer me evasive answers and lies. Locked up in the Land of Liberty: Part III |Yariel Valdés González |July 21, 2021 |Washington Blade 

Some make the virus more evasive when faced with antibodies and other immune system cells. New study on delta variant reveals importance of receiving both vaccine shots, highlights challenges posed by mutations |Joel Achenbach |July 8, 2021 |Washington Post 

PFAS have been evasive, found in everyday products like drinking water, and now, even amid growing calls for cleaner and safer beauty products, scientists have found them in makeup. ‘Forever chemicals’ could be lurking in your lipstick |Sara Kiley Watson |June 19, 2021 |Popular-Science 

Whenever I asked questions about me, or them, or Germany—or whatever, they were extremely evasive and even as a kid my intuition indicated that something just was not right about that. He Robbed a Taco Joint With a Toy Water Gun for $264. He Got Life in Prison. |Kate Briquelet |May 31, 2021 |The Daily Beast 

Derkach has always denied this, and he grew evasive when I pressed about his ties to Moscow. Exclusive: How an Accused Russian Agent Worked With Rudy Giuliani in a Plot Against the 2020 Election |Simon Shuster/Kyiv |May 28, 2021 |TIme 

The law caught up with him; he was unwisely evasive about exactly which drugs he sold. The Weirdest Story About a Conservative Obsession, a Convicted Bomber, and Taylor Swift You Have Ever Read |David Weigel |August 30, 2014 |DAILY BEAST 

He did not however say that Putin lied, but rather that the Russian president's response was "suspiciously narrow" and evasive. Sorry, Snowden: Putin Lied to You About His Surveillance State—And Made You a Pawn of It |Eli Lake |April 18, 2014 |DAILY BEAST 

I've never seen London Mayor Boris Johnson look so awkward, evasive, and non-credible as in this BBC interview. Boris Johnson's Awfully Embarrassing Interview |David Frum |March 26, 2013 |DAILY BEAST 

Newton escaped the world through nuttiness, Darwin through elaborate evasive courtesies and by farming out the politics to Huxley. Galileo's Test of Truth |David Frum |February 15, 2013 |DAILY BEAST 

The rhetoric was mostly as empty of substance and evasive on details as a Paul Ryan budget. Obama Must Fight One More Campaign: To Keep Senate & Win House in 2014 |Robert Shrum |February 8, 2013 |DAILY BEAST 

With his evasive singularity was mingled a certain exotic odour like the distant perfume of a country well loved of the sun. Charles Baudelaire, His Life |Thophile Gautier 

The weaknesses of Fieldas revealed in his two independent comedieswere of a nature more evasive, less capable of definition. The Fatal Dowry |Philip Massinger 

This time the monk was caught; but, in accordance with the habit of his brethren, his answer was as it was meant to be, evasive. The Border Rifles |Gustave Aimard 

Nevertheless, mystified as he was, he concealed the details of their trip under an evasive answer when he returned to his room. The Woman Gives |Owen Johnson 

To our questions they returned evasive answers or were silent, and finally asked by what authority we had overhauled them. Famous Adventures And Prison Escapes of the Civil War |Various