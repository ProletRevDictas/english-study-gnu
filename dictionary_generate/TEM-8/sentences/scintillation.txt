Miriam retorted with the first scintillation of gaiety she had shown on this occasion. The Tragic Muse |Henry James 

With the spell of her voice, of her eyes, of her radiant face upon him, Cameron's scintillation faded and snuffed out. Corporal Cameron |Ralph Connor 

The Diana pond was skinned with ice; goddess and golden nymphs caught every scintillation of cold sunlight as we trundled past. Witching Hill |E. W. Hornung 

The room was filled with a luminous rosy light, alive with scintillation of diamond brilliancy. A Son of Perdition |Fergus Hume 

The presence of moisture in the atmosphere intensifies scintillation, and this is usually regarded as a prognostication of rain. The Astronomy of Milton's 'Paradise Lost' |Thomas Orchard