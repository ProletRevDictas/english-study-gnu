Cruz stayed sober for a while, but when Raeann’s first birthday after the accident passed, he relapsed. Victor Ray Cruz Wanted to Turn His Life Around — Then He Got the Coronavirus |Maya Srikrishnan and Adriana Heldiz |October 22, 2020 |Voice of San Diego 

Van Halen, sober since 2008, lost one-third of his tongue to a cancer that eventually drifted into his esophagus. Guitar legend Eddie Van Halen dies of cancer at 65 |radmarya |October 6, 2020 |Fortune 

After the officers left, he got back up and asked the attendant if he could leave, since he was sober. Those Ticketed for Seditious Language Say Their Only Crime Was Talking Back |Kate Nucci |September 9, 2020 |Voice of San Diego 

Some in recovery — even those clean and sober for decades — say they have doubled or tripled their recovery meeting attendance during the pandemic. How the Pandemic Is a Boon to Recovering Addicts |Fiona Zublin |August 24, 2020 |Ozy 

On the other hand, use the rel values such as ugc to tag your user-generated content, and sponsored for the paid links — this will help Google make a sober assessment of your ranking. Eight HTML elements crucial for SEO |Aleh Barysevich |May 5, 2020 |Search Engine Watch 

I try to stay sober enough to maneuver back to West Hollywood. Alfred Hitchcock’s Fade to Black: The Great Director’s Final Days |David Freeman |December 13, 2014 |DAILY BEAST 

He manages to talk one of them into a glass or two, but for the most part he remains sober. Alfred Hitchcock’s Fade to Black: The Great Director’s Final Days |David Freeman |December 13, 2014 |DAILY BEAST 

And things seemed fine between the couple, with the crooner appearing sober and healthier-looking, with a new crew cut. Creed Singer Scott Stapp’s Fall From Grace: From 40 Million Albums Sold to Living in a Holiday Inn |Marlow Stern |November 27, 2014 |DAILY BEAST 

Today, however, the 36-year-old musician is sober—and even showers once in a while. Julian Casablancas Enters the Void: On the Strokes’ Friction, Why He Left NYC, and Starting Over |Marlow Stern |October 9, 2014 |DAILY BEAST 

She's well spoken, educated, and sober—a far cry from the one-time face of the adult world, Jenna Jameson. Porn Keeps Up with the Kardashians: Belle Knox on the Mainstreaming of Adult Stars |Aurora Snow |September 27, 2014 |DAILY BEAST 

This does not seem strange when we know what a sober, serious, dignified man Yung Pak's father was. Our Little Korean Cousin |H. Lee M. Pike 

In sober truth it might as well lay one open to the charge of being an ornithologist. The Unsolved Riddle of Social Justice |Stephen Leacock 

Darry Drew was one of those quiet, gentlemanly fellows, who seem rather too sober for their years. The Campfire Girls of Roselawn |Margaret Penrose 

To which Mr. Sikes replied, he was joking;as if he had been sober, there would have been strong reason to suppose he was. Oliver Twist, Vol. II (of 3) |Charles Dickens 

That counter-blast of passion and that plain speaking from a quarter so unexpected served, in part at least, to sober him. St. Martin's Summer |Rafael Sabatini