With misinformation and disinformation about the pandemic, “cheap” and “deep” fakes of elected officials, and targeted ads and emotionally exploitative social media algorithms, it can begin to feel like all communication is manipulation. The Dark Side of Smart - Facts So Romantic |Diana Fleischman |September 15, 2020 |Nautilus 

The state’s watchdog, however, argued that the company could falsely spread its rates over a fake population, giving the illusion that customers were getting a cost cut. Environment Report: State Throws Cold Water on Pricing Scheme |MacKenzie Elmer |September 14, 2020 |Voice of San Diego 

She has coiffed hair and long fake eyelashes, but still puts in the same work everybody else does, taking orders and doing deliveries. The Big Corporate Rescue and the America That’s Too Small to Save |by Lydia DePillis, Justin Elliott and Paul Kiel |September 12, 2020 |ProPublica 

You see how good we’re doing relative to other countries and other parts of the world, but the fake news doesn’t like saying that, they don’t like telling you that. Trump keeps lying about how the US Covid-19 situation compares to other countries. Here are the facts. |Aaron Rupar |September 11, 2020 |Vox 

However, the new “fake news” bill would bypass the framework, allowing legislators to create a mechanism that could be used to restrict that freedom for millions of Brazilians. Brazil’s “fake news” bill won’t solve its misinformation problem |Amy Nordrum |September 10, 2020 |MIT Technology Review 

Just how many fake nodes would be needed in order to pull off a successful Sybil attack against Tor is not known. The Attack on the Hidden Internet |Marc Rogers |December 29, 2014 |DAILY BEAST 

He cast her as Hope, an ex-addict with an impressive pair of fake chompers—the result of years of drug abuse. Jena Malone’s Long, Strange Trip From Homelessness to Hollywood Stardom |Marlow Stern |December 22, 2014 |DAILY BEAST 

There are many of us who need to talk and be reached out to, even if we use fake Facebook accounts for our safety. What It’s Like to Be an Atheist in Palestine |Waleed al-Husseini, Movements.Org |December 8, 2014 |DAILY BEAST 

A call made to police beforehand described Rice as “a guy with a pistol” on a swing set, but said it was “probably fake.” The 14 Teens Killed by Cops Since Michael Brown |Nina Strochlic |November 25, 2014 |DAILY BEAST 

Crooks can use it to apply for credit, file fake claims with insurers, or buy drugs and medical equipment that can be resold. How Your Pacemaker Will Get Hacked |Kaiser Health News |November 17, 2014 |DAILY BEAST 

And then all motion in that portion of the great fake would suddenly cease. Mushroom Town |Oliver Onions 

Within six months, if you're not sandbagged or jailed on fake libel suits, you'll have a unique bibliography of swindles. Average Jones |Samuel Hopkins Adams 

"I'll bet he's got some kind of a fake story to tell," suggested Will. The Call of the Beaver Patrol |V. T. Sherman 

It might just as well have been any other patent medicine, or any fake cure. The Eugenic Marriage, Vol. 3 (of 4) |W. Grant Hague 

If you happen to drift into the fake places, nothing more serious would happen than getting stuck good and hard. Paris Vistas |Helen Davenport Gibbons