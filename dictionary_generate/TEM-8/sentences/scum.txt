Biofilms form dental plaque and pond scum, but they can also create beautiful, tie-dye formations and protect shipwrecks from deteriorating. Turns out kombucha SCOBYs could make good water filters |Maggie Galloway |January 26, 2022 |Popular-Science 

Some common types are pond scum and even dental plaque, and biofilm can even preserve shipwrecks by coating the metal exterior and preventing corrosion. These sophisticated bacteria communities assemble in tie-dye formation |Maggie Galloway |January 11, 2022 |Popular-Science 

These freshwater blooms may look like scum, foam, mats or even paint floating atop the water. Pond scum can release a paralyzing pollutant into the air |Tracy Vonder Brink |June 7, 2021 |Science News For Students 

On Earth, such sediments have preserved evidence of ancient life in the form of fossilized mats of microscopic pond scum called stromatolites. NASA rover Perseverance lands on Mars in mission to search for past life |Joel Achenbach, Sarah Kaplan, Ben Guarino |February 18, 2021 |Washington Post 

Intensely satisfying to use, these bad boys easily bust through even the grimiest, most built-up of messes—the soap scum on tubs, fat caked grill tops, and, of course, the toughest grout stains. Grout scrub brushes that tackle even the toughest stains |PopSci Commerce Team |January 19, 2021 |Popular-Science 

Scum it off with your spoon, and lay it in anotherPage 117 dish. The Closet of Sir Kenelm Digby Knight Opened |Kenelm Digby 

The home missions class furnished the inspiration for it, and called it "Scum o' the Earth," an impromptu immigration pageant. John Wesley, Jr. |Dan B. Brummitt 

Let all this boil together a full hour, and keep it constantly skimmed, as long as any Scum will rise upon it. The Closet of Sir Kenelm Digby Knight Opened |Kenelm Digby 

Scum like you has ter come ter the mark sooner or later, and come yer have. Indian and Scout |F. S. Brereton 

On Scum exist the lowest conditions of life found in any stellar world. Life in a Thousand Worlds |William Shuler Harris