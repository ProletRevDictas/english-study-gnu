After missing the playoffs in a disappointing 2019 campaign, Chicago had a resurgent regular season in 2020 but quickly crashed out of the postseason. This Trade Deadline Could Break Up The Cubs’ Almost-Dynasty |Neil Paine (neil.paine@fivethirtyeight.com) |July 16, 2021 |FiveThirtyEight 

Adjusting to an old issue — and a resurgent one The data that landed on Mike Schmidt’s desk in early August 2020, when he took over as Multnomah County district attorney, was a carry-over from the early demonstrations and broad arrests. Anarchists and an increase in violent crime hijack Portland’s social justice movement |Scott Wilson |May 31, 2021 |Washington Post 

Through Sunday’s games, only one team — the resurgent Boston Red Sox — is on pace for at least 100 wins, while two — the Colorado Rockies and the Tigers — are on pace for fewer than 60. Who’s Good And Bad In MLB This Year? We Don’t Really Know. |Neil Paine (neil.paine@fivethirtyeight.com) |May 10, 2021 |FiveThirtyEight 

Indianapolis is suddenly hanging on to the final playoff spot and faces a resurgent Texans team in two of its next three games. How the NFL playoff picture is shaping up in Week 12 |Des Bieler |November 30, 2020 |Washington Post 

Later, Eater Los Angeles’ Mona Holmes discusses why a resurgent downtown restaurant scene has been completely hobbled by the fallout from the virus. SF’s Japantown and LA’s Downtown In Crisis |Amanda Kludt |November 9, 2020 |Eater 

To target the resurgent Nazi movement, X-2 recruited some 13 agents in Munich and had another dozen under consideration. On the Trail of Nazi Counterfeiters |Dr. Kevin C. Ruffner |September 20, 2014 |DAILY BEAST 

Atlanta soon flourished, becoming the resurgent urban center of an otherwise impoverished South. Atlanta’s Fall Foretold The End Of Civil War Bloodshed |Marc Wortman |September 1, 2014 |DAILY BEAST 

But will 9,800 troops be enough to keep a resurgent al Qaeda out? Will Obama's 10,000 Troops in Afghanistan Be Enough to Stop Al Qaeda? |Eli Lake, Josh Rogin |May 27, 2014 |DAILY BEAST 

The Crimean Tatars, however, who were shipped away to Kazakhastan by Stalin after WWII, have no desire to join a resurgent Russia. Kiev Defiant at Russian Aggression |Vijai Maheshwari |March 5, 2014 |DAILY BEAST 

Toll Brothers is just a smaller part of a larger story of a resurgent housing market in 2012. Toll Brothers Rings Up Profits in Housing Comeback |Matthew Zeitlin |December 4, 2012 |DAILY BEAST 

He said it almost with a sneer, but nothing could crush the resurgent glow in her heart. The Highgrader |William MacLeod Raine 

But young Charley was more susceptible than most, and this—on the impulse of the next tide resurgent—saved him from his type. The Killer |Stewart Edward White 

The idea that Milly might even now be resurgent fluttered Tims's pulses with a mixed emotion. The Invader |Margaret L. Woods 

With the unheeded warning resurgent and clamoring in his ears, Smith knelt horror-stricken beside the fallen man. The Real Man |Francis Lynde 

Its resurgent dirge stirs vague forebodings which root in the calamitous experience of the race. The Mystery of The Barranca |Herman Whitaker