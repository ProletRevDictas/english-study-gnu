It was doubtful we were going to make it to March 1, but with the money that came in we’ve probably got March 1 taken care of. Could GoFundMe campaigns save our cultural collections? Las Vegas’s Pinball Hall of Fame is banking on it. |Kate Silver |February 26, 2021 |Washington Post 

Whether his time behind bars had a wider effect is doubtful. Bee theft is almost a perfect crime—but there’s a new sheriff in town |Andrew Zaleski |February 9, 2021 |Popular-Science 

McCaffrey has been limited to only three games and is doubtful to play Sunday with a thigh injury. Washington football preview: Ron Rivera’s Carolina reunion secondary to playoff push |Nicki Jhabvala, Sam Fortier |December 26, 2020 |Washington Post 

It’s a delicious promise, but whether American workers — battered, politically polarized and out of work in great numbers — can take on a fight of this nature at this moment is doubtful. A call to revolutionize the workplace — by working less |Sheila McClear |December 18, 2020 |Washington Post 

Political pundits used other terms to describe swing states, such as “doubtful” states. Why Swing States Are a Thing |Olivia B. Waxman |November 3, 2020 |Time 

It is doubtful that any Churchill-like figure—were one available—could thrive. Boris Johnson’s Churchill Man Crush |Michael F. Bishop |November 22, 2014 |DAILY BEAST 

But from the moment the OSCE team arrived at the site, it began to look doubtful the inquiry had any chance to succeed. In the Killing Fields of Ukraine with Children Who Saw the MH17 Horror |Anna Nemtsova |July 20, 2014 |DAILY BEAST 

Whether the vote in a rump referendum over the weekend genuinely reflected public opinion in the eastern-most regions is doubtful. Ukraine’s Fighting Words |Igor Kossov |May 13, 2014 |DAILY BEAST 

But it is doubtful anyone will ever prove who poisoned Arafat, if poisoned he was. Arafat’s Polonium Poisoning Mystery Resurfaces |Christopher Dickey |November 7, 2013 |DAILY BEAST 

Stunned, and then immediately doubtful—and, honestly, can you blame us? Israel and Palestine to Resume Peace Talks |Emily L. Hauser |July 19, 2013 |DAILY BEAST 

The place was well defended by earthworks and natural parapets, and for several hours the issue of the contest was doubtful. The Philippine Islands |John Foreman 

Nothing doubtful or "reputed" ever arrived in the huge packing-cases consigned to Walls End Castle. The Pit Town Coronet, Volume I (of 3) |Charles James Wills 

It is to be feared that like the sauce of sauces in the hands of the inexperienced cook, the result is more than doubtful. The Pit Town Coronet, Volume I (of 3) |Charles James Wills 

It is doubtful whether the huge commercial premium that greets success to-day does good or harm. The Unsolved Riddle of Social Justice |Stephen Leacock 

For a considerable time it appeared very doubtful how the battle would end. Hunting the Lions |R.M. Ballantyne