We should be hunting for signals in the ultraviolet, X-ray and gamma-ray frequencies. Readers ask about malaria parasites, searching for alien intelligence and more |Science News Staff |January 24, 2021 |Science News 

Another key feature that will keep your humidifier cleaner longer is the use of ultraviolet light to mitigate the formation of mold and bacteria. The best humidifier: Fight dry air (and a dry nose) all winter long |PopSci Commerce Team |January 15, 2021 |Popular-Science 

Putting surfaces that reflect ultraviolet light on glass can help daytime fliers spot windows before they fly into them. Around the world, birds are in crisis |Alison Pearce Stevens |December 3, 2020 |Science News For Students 

If Juno saw a lightning strike at the same place as one of these ultraviolet flashes, “that would prove it,” she says. Jupiter may have ‘sprites’ or ‘elves’ in its atmosphere |Maria Temming |December 1, 2020 |Science News For Students 

It soaks up x-rays and ultraviolet energy from the sun, protecting those of us on the ground from these harmful rays. Explainer: Our atmosphere — layer by layer |Beth Geiger |December 1, 2020 |Science News For Students 

This ultraviolet camera shows people what the sun is really doing to their skin. Muppet Beastie Boys, Robin Williams Tribute, and More Viral Videos |The Daily Beast Video |August 17, 2014 |DAILY BEAST 

All of these instruments orbit Earth, beyond the atmosphere that blocks X-rays and most ultraviolet light. The Supermassive Black Hole Smokescreen |Matthew R. Francis |June 22, 2014 |DAILY BEAST 

One important note: only certain mushrooms provide this benefit, thanks to being grown under ultraviolet light. 6 Healthy Foods to Fight the Flu, Beat Stress and More |DailyBurn |February 5, 2014 |DAILY BEAST 

PhoneSoap disinfects your cell phone for $50 bucks with ultraviolet light in less than four minutes. We are Living in the Golden Age of the Most Narcissistic and Voyeuristic Tech Imaginable |Charles C. Johnson |January 18, 2014 |DAILY BEAST 

If this sounds a little strange, just think about the health implications of sitting under ultraviolet light for hours at a time. Conservatives Have No Idea What ‘Racist’ Means |Jamelle Bouie |August 7, 2013 |DAILY BEAST 

On a hunch I dropped in an aluminum alkyl, and then pushed the polymerization along with both ultraviolet and heat. The Professional Approach |Charles Leonard Harness 

The difficulty is a real one since the ultraviolet rays have a destructive effect even in the absence of oxygen. The Organism as a Whole |Jacques Loeb 

In the "X" circuit, the negative was grounded along an ultraviolet beam from the ship's repeller-ray generator. The Airlords of Han |Philip Francis Nowlan 

The apparatus is not one for producing violet or ultraviolet rays in the scientific meaning of those words. The Propaganda for Reform in Proprietary Medicines, Vol. 2 of 2 |Various 

We utilize a carbon arc lamp, which has considerable ultraviolet light in it. Warren Commission (4 of 26): Hearings Vol. IV (of 15) |The President's Commission on the Assassination of President Kennedy