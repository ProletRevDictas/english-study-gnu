Local fauna also came into greater view, less fearful of cities when the hum of engines and voices died down. How the pandemic has helped and hurt animals around the world |Ula Chrobak |March 4, 2021 |Popular-Science 

Barton also noted in his letter that the area, in a post-Civil War survey, was dubbed the “Canadian Valley” because of both the climate and the high concentration of far-north flora and fauna. Skiing West Virginia’s ‘Canadian Valley’ in a banner season |John Briley |February 18, 2021 |Washington Post 

A pruner should be your go-tool for trimming and shaping your flora and fauna while preserving plant tissue and extending the life of your greenery. Plant pruners to shape up your saplings |PopSci Commerce Team |January 19, 2021 |Popular-Science 

The land holds some of the best-preserved Apache archaeological sites, as well as untouched flora and fauna like old-growth trees and threatened species like ocelots, which Redniss draws in realistic detail. 'Oak Flat' Chronicles a Battle to Save Sacred Land |Erin Berger |January 14, 2021 |Outside Online 

In Brazil, for instance, an app allows residents to report dead and afflicted fauna in hopes of identifying emerging outbreaks. The fight to stop the next pandemic starts in the jungles of Borneo |Brian Barth |December 2, 2020 |Popular-Science 

The area is a disturbed wetland, invaded by non-native melaleuca trees that have crowded out native flora and fauna. How to Catch a Giant Python |Catharine Skipp |February 28, 2010 |DAILY BEAST 

You think of the rainforest as this incredibly abundant place of fauna and animals and flora. Raiders of the Lost City |The Daily Beast |February 24, 2009 |DAILY BEAST 

I kept my word and now I am beginning to make acquaintance with the flora and fauna of my little wood. Marguerite |Anatole France 

The fauna is not abundant except in large mammals, which are very numerous on the drier steppes. Encyclopaedia Britannica, 11th Edition, Volume 4, Part 3 |Various 

It was obviously unfair to expect her to be familiar with the flora and fauna of every part of the great Australian Continent. The Adventures of Louis de Rougemont |Louis de Rougemont 

In the fauna of the region that I had traversed I had noted changes corresponding to those in the flora. The Collected Works of Ambrose Bierce |Ambrose Bierce 

I may conclude this chapter by a brief view of the Fauna of the higher vertebral animals. Travels in Peru, on the Coast, in the Sierra, Across the Cordilleras and the Andes, into the Primeval Forests |J. J. von Tschudi