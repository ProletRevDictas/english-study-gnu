Clean Origin diamonds are also graded on the 4 Cs of cut, color, clarity, and carat. A Commitment to Love: Clean Origin Diamonds |Sponsor |November 16, 2021 |Washington Blade 

Millions of carats in diamonds have been exported from Namibia since 1908. As Germany Acknowledges Its Colonial-Era Genocide in Namibia, the Brutal Legacy of Diamond Mining Still Needs a Reckoning |Steven Press |June 10, 2021 |Time 

The magnificent $6000 ring glittered with a two-carat diamond surrounded by smaller diamonds, all set in silver. The Strange and Mysterious Death of Mrs. Jerry Lee Lewis |Richard Ben Cramer |January 11, 2014 |DAILY BEAST 

This tea caddy is rather splendid Much of the pottery is gilded in 22 carat gold leaf. New Royal China Alert |Tom Sykes |May 20, 2013 |DAILY BEAST 

The largest D color flawless diamond ever auctioned, the 101.73 carat jewel is expected to fetch at least $20 million. 'Absolute Perfection' Diamond Up for Auction |William O’Connor |May 14, 2013 |DAILY BEAST 

Thus 18 carat gold has one-fourth of alloy, and so on with lower qualities down to 12, which is in reality only gold by courtesy. Encyclopaedia Britannica, 11th Edition, Volume 15, Slice 4 |Various 

The gold coin of the realm is 22 carat; that is, it contains one-twelfth of alloy to harden it to stand wear and tear. Encyclopaedia Britannica, 11th Edition, Volume 15, Slice 4 |Various 

When first offered for sale, the price was fourpence a carat; now it is from fifteen to twenty shillings. Chambers's Journal of Popular Literature, Science, and Art |Various 

A little tin god has a pleasant time of it, no doubt, until the coming of the eighteen carat gold idol. The Rise of Roscoe Paine |Joseph C. Lincoln 

So he tied above their best reach three strands of “carat” cord to the main rope. Bonaventure |George Washington Cable