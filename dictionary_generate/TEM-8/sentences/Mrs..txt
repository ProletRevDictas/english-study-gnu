Mrs. Kouachi works at a nursery and has worn the veil since she made the pilgrimage to Mecca in 2008. France Mourns—and Hunts |Nico Hines, Christopher Dickey |January 8, 2015 |DAILY BEAST 

There was the empathetic way she dealt with the revelation that Mrs. Baxter is a former criminal. ‘Downton Abbey’ Review: A Fire, Some Sex, and Sad, Sad Edith |Kevin Fallon |January 5, 2015 |DAILY BEAST 

J Crew did not give back the money it incidentally made off of Mrs. Obama. One Vogue Cover Doesn’t Solve Fashion’s Big Race Problem |Danielle Belton |January 2, 2015 |DAILY BEAST 

“I feel a shaking of the ground I stand on,” Carson tells Mrs. Hughes with trepidation. What Downton’s Fashion Really Means |Katie Baker |January 2, 2015 |DAILY BEAST 

The campaign was known to palace insiders as “Operation Mrs. PB.” Pulled Documentary Says William Felt ‘Used’ by Charles’ Push for Camilla |Tom Sykes |December 30, 2014 |DAILY BEAST 

Mrs. Wurzel was quite right; they had been supplied, regardless of cost, from Messrs. Rochet and Stole's well-known establishment. The Pit Town Coronet, Volume I (of 3) |Charles James Wills 

Bernard stood there face to face with Mrs. Vivian, whose eyes seemed to plead with him more than ever. Confidence |Henry James 

But Mrs. Dodd, the present vicar's wife, retained the precious prerogative of choosing the book to be read at the monthly Dorcas. The Pit Town Coronet, Volume I (of 3) |Charles James Wills 

Old Mrs. Wurzel and the buxom but not too well-favoured heiress of the house of Grains were at the head of the table. The Pit Town Coronet, Volume I (of 3) |Charles James Wills 

He reached forward and took her hands, and if Mrs. Vivian had come in she would have seen him kneeling at her daughter's feet. Confidence |Henry James