This network, furthermore, gets more mileage out of the First Amendment than most, if not all, other media outlets. Rupert Murdoch blasts ‘wave of censorship’ and ‘woke orthodoxy’ |Erik Wemple |January 26, 2021 |Washington Post 

Simply put, a small polling error in the GOP’s direction wouldn’t be that surprising and furthermore, it would be enough to give Loeffler and Perdue the advantage. The Case For Republicans In Georgia vs. The Case For Democrats |Nathaniel Rakich (nathaniel.rakich@fivethirtyeight.com) |January 4, 2021 |FiveThirtyEight 

The IBWC says no treaty requires it to test and furthermore, it doesn’t have the money for it. New Snapshot of What’s in the Tijuana River Is as Gross as You’d Expect |MacKenzie Elmer |November 2, 2020 |Voice of San Diego 

Officials in Alexandria, which serves roughly 16,000 students in Northern Virginia, furthermore inserted a new “Fair Treatment” statement into the 18-page document. Alexandria City Public Schools approves revisions to contract with police |Hannah Natanson |October 30, 2020 |Washington Post 

Furthermore, mixed race children are the fastest growing population in the country. Obama Is Right on Race. The Media Is Wrong. |Keli Goff |December 29, 2014 |DAILY BEAST 

Furthermore, checking online IP reputation services reveals that they have been used by malware operators in the past. No, North Korea Didn’t Hack Sony |Marc Rogers |December 24, 2014 |DAILY BEAST 

Furthermore, data confirm a connection between education level and police behavior. Are College Educated Police Safer? |Keli Goff |December 1, 2014 |DAILY BEAST 

Furthermore, a person with norovirus has about 70 billion viral particles per gram of stool. A Doctor Explains Why Cruise Ships Should Be Banned |Kent Sepkowitz |November 19, 2014 |DAILY BEAST 

Furthermore, they would not highlight such a piece no matter how carefully worded unless the sources were clearly authoritative. Before Ditching His Top Aides, Obama Should Look in the Mirror |Leslie H. Gelb |November 2, 2014 |DAILY BEAST 

And furthermore, I imagine something else about this—quite unlike the old Bible—I imagine all of it periodically revised. The Salvaging Of Civilisation |H. G. (Herbert George) Wells 

Furthermore, the parsons had had to accept the same amount of tobacco when the prices had previously declined. Hallowed Heritage: The Life of Virginia |Dorothy M. Torpey 

Furthermore, a note is payable on demand when it is thus stated, or is payable at sight or on presentation. Putnam's Handy Law Book for the Layman |Albert Sidney Bolles 

She was furthermore attired in an old Paisley shawl belonging to her grandmother—what better way to advertise a grandmother? Ancestors |Gertrude Atherton 

Furthermore, an acceptance may be qualified as to time, acceptance of payment in part only and in other ways. Putnam's Handy Law Book for the Layman |Albert Sidney Bolles