There are obviously massive environmentalism issues that need to be addressed. Christopher Nolan Uncut: On ‘Interstellar,’ Ben Affleck’s Batman, and the Future of Mankind |Marlow Stern |November 10, 2014 |DAILY BEAST 

Will he be passionate about environmentalism like Charles, or global humanitarian causes like his late grandmother Diana? Happy Birthday, Prince George! Will the Wee Royal Be the First King of the 22nd Century? |Emma Woolf |July 21, 2014 |DAILY BEAST 

The pollution that 1970s environmentalism targeted was also more concrete. Green Politics Has to Get More Radical, Because Anything Less Is Impractical |Jedediah Purdy |April 26, 2014 |DAILY BEAST 

The environmentalism of the early 1970s was not just a pragmatic response to a technical problem. Green Politics Has to Get More Radical, Because Anything Less Is Impractical |Jedediah Purdy |April 26, 2014 |DAILY BEAST 

So the environmentalism that once worked so well is stymied by climate change. Green Politics Has to Get More Radical, Because Anything Less Is Impractical |Jedediah Purdy |April 26, 2014 |DAILY BEAST