The new colour scheme will be visible in the dark theme and the chat bubbles seem to be greener compared to the existing colour. WHATSAPP FOR ANDROID, DESKTOP TO GET NEW FEATURES |noreply@blogger.com (Unknown) |January 19, 2022 |TechCrunch 

They were dressed in sober colours for today’s brief event, with Harry wearing a suit and tie. Harry and Meghan Make First Joint Public Appearance Since Quitting Royal Family |Tom Sykes |September 23, 2021 |The Daily Beast 

Investigating genetic links between grapheme–colour synaesthesia and neuropsychiatric traits. The Beauty of Crossed Brain Wires - Issue 104: Harmony |Sidney Perkowitz |August 18, 2021 |Nautilus 

I designed a pattern for the colour work that would signify everything about these games! Tom Daley Has Unveiled His Olympic Knitting Masterpiece |Melissa Locker |August 6, 2021 |Time 

The human body comes in a huge variety of shapes, sizes and colours, yet people outside the perceived norm have often been seen as threatening, ridiculous or hateful. Transgender, Transhuman: Technological Advances Offer Increased Choices But Also Create New Prejudices |LGBTQ-Editor |April 9, 2020 |No Straight News 

You know the cartoon segment that used to be in colour in rancid old newspapers? The Rancid Ballad of Johnny Rotten: His Memoir Seethes With Anger—And Charm |Legs McNeil |November 20, 2014 |DAILY BEAST 

Soul Survivor, a Christian organization based out of the UK, filmed one of their ‘Colour Chaos’ events with GoPros. Viral Video of the Day: Other Uses for GoPro |Alex Chancey |August 26, 2014 |DAILY BEAST 

Matisse (1869–1954) called it “cutting directly into colour” and the process itself is as fascinating as the results. This Summer, Get Thee To London For The RSC’s Henry IV |Emma Woolf |April 28, 2014 |DAILY BEAST 

She was wonderfully beautiful, but her colour was too deep and her lovely eyes were too bright. Read ‘The King in Yellow,’ the ‘True Detective’ Reference That’s the Key to the Show |Robert W. Chambers |February 20, 2014 |DAILY BEAST 

I knew every volume by its colour and examined them all, passing slowly around the library and whistling to keep up my spirits. Read ‘The King in Yellow,’ the ‘True Detective’ Reference That’s the Key to the Show |Robert W. Chambers |February 20, 2014 |DAILY BEAST 

The sleeves of his doublet which protruded from his leather casing were of the same colour and material as his trunks. St. Martin's Summer |Rafael Sabatini 

His hair was darker—almost brown save at the temples, where age had faded it to an ashen colour. St. Martin's Summer |Rafael Sabatini 

Later on, I believe, a child is wont to have his favourite colour, and to be ready to defend it against the preferences of others. Children's Ways |James Sully 

Liking for a single colour is a considerably smaller display of mind than an appreciation of the relation of two colours. Children's Ways |James Sully 

Tressan was monstrous ill-at-ease, and his face lost a good deal of its habitual plethora of colour. St. Martin's Summer |Rafael Sabatini