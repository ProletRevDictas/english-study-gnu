The concept of a weighted blanket is pretty self-explanatory. What the science actually says about weighted blankets |Rahul Rao |January 26, 2021 |Popular-Science 

The weather and calendar widgets are mostly self-explanatory, offering useful information at a glance. Kicking off the new year with a screen cleanse |rhhackettfortune |January 5, 2021 |Fortune 

Both pages in this case had good heading use, although it could be argued that the headings are more explanatory and easy to skim on the post that did well. Some early observations on the Google December core update |Marie Haynes |December 24, 2020 |Search Engine Land 

America is enduring roughly a Pearl Harbor’s worth of death per day, minus the explanatory power of a military attack from the sky. A Kraken is loose in America |Dan Zak |December 10, 2020 |Washington Post 

The first and last ones are sort of self explanatory—active apps and sites currently have access to your data, while revoked ones used to be able to access your data but can’t anymore. Used Google, Facebook, or Apple as a login? Here’s how to log out. |Sandra Gutierrez G. |October 27, 2020 |Popular-Science 

For centuries scientists included God as a part of their explanatory package. 2014: Revenge of the Creationists |Karl W. Giberson |December 27, 2014 |DAILY BEAST 

Then he integrated his findings into broader theories with deep explanatory power. Why Aristotle Deserves A Posthumous Nobel |Nick Romeo |October 18, 2014 |DAILY BEAST 

This means that the camera has to be static and I have to include some explanatory inter-titles. Inside ‘Maidan’: Sergei Loznitsa on His Ukrainian Uprising Doc and Putin’s ‘Fascist’ Regime |Richard Porton |May 24, 2014 |DAILY BEAST 

I went on to argue that the explanatory deficiencies of ID are overwhelming, extending far beyond bad and sinister design. My Debate With an ‘Intelligent Design’ Theorist |Karl W. Giberson |April 21, 2014 |DAILY BEAST 

One of the more recent ones, for example, is titled “God Hates Malaysia,” and is pretty self-explanatory. This Man Is The Future of Westboro Baptist Church |Caitlin Dickson |March 24, 2014 |DAILY BEAST 

It is perhaps explanatory of the way things went for Sara Lee from that time on that he quite forgot his newspapers. The Amazing Interlude |Mary Roberts Rinehart 

The critical and explanatory notes which accompanied it gave the book a high value. Encyclopaedia Britannica, 11th Edition, Volume 5, Slice 2 |Various 

This participial construction is frequent in Milton as in Latin: it is equivalent to an explanatory clause. Milton's Comus |John Milton 

Many explanatory remarks are inserted to elucidate the story. The Translations of Beowulf |Chauncey Brewster Tinker 

After a brief explanatory conference, the minister thoughtfully went his way. The Fiend's Delight |Dod Grile