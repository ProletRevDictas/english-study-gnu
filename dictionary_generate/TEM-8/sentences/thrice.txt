Here, twenty-odd folks share one toilet – a privilege for which a few others next door would pay thrice their rent, just to halve the number of fellow users they have to share their own toilets with. As Hong Kong Marks 25 Years of British Handover, Its 'Cage Homes' Remain a Stark Reminder of Its Inequities |Brian Wong |June 30, 2022 |Time 

Also part of the SpaceX lineup is the thrice-flown Falcon Heavy rocket, with 27 engines, and its massive 33-engine Super Heavy, which should have its first flight this year. Billionaires Are Racing to Space—and the Climate is Paying the Price |Jeffrey Kluger |June 28, 2022 |Time 

Considering that we have to eat food thrice per day just to stay alive, it’s probably time to consider making that process a little less complicated at least some of the time. Don’t Let Snobbery Keep You From the Beautiful Simplicity of Chili’s 2 for $25 Meal |Amy McCarthy |October 28, 2021 |Eater 

Riders cram themselves into carriages running at thrice their capacity, in a self-organized human jigsaw that would sometimes find you dangling outside the permanently open doors of carriage, where at least you could breathe freely. How I Escaped My Troubles Through Science - Issue 104: Harmony |Subodh Patil |August 25, 2021 |Nautilus 

Wiskus, carrying a full 15-hour class load while practicing 20 hours per week, joined thrice-weekly Zoom calls with alumni to discuss strategy and review talking points for the flurry of interview requests that followed. Can men’s college gymnastics be saved? Minnesota is trying, even on its way out. |Liz Clarke |April 16, 2021 |Washington Post 

I return home after several days in the hospital and start thrice weekly physical therapy. You’re Never ‘Cured’ of an Eating Disorder |Carrie Arnold |December 20, 2014 |DAILY BEAST 

Those people have now been thrice betrayed--by Mubarak, by Morsi, and yesterday by the military. Coming Clean on Egypt |Michael Tomasky |August 15, 2013 |DAILY BEAST 

Seriously, the Tea Party favorite has thrice run for a Delaware Senate seat and lost. Christine Quinn’s Lackluster Book Debut & More Political-Memoir Flops |Brandy Zadrozny |June 20, 2013 |DAILY BEAST 

Thrice married and twice divorced, Tony exemplified a certain Hollywood archetype. The Beautiful Bromance Between Filmmakers Tony Scott and Ridley Scott |Chris Lee |August 21, 2012 |DAILY BEAST 

South Carolina evangelical voters cast their ballots overwhelmingly for a thrice-married admitted adulterer. Religious-Right Leaders Say Santorum, But Voters Flock to Gingrich |John Avlon |January 22, 2012 |DAILY BEAST 

He had repeated till he was thrice weary the statement that "the Cat lay on the Mat and the Rat came in." Kipling Stories and Poems Every Child Should Know, Book II |Rudyard Kipling 

Walking thrice round it, he at each time gravely repeated: "If she dies she dies, but if she lives she lives." The Book of Anecdotes and Budget of Fun; |Various 

Among others, an Abb thrice lifted his fork to his mouth, and thrice laid it down, with an eager stare of surprise. The Book of Anecdotes and Budget of Fun; |Various 

The average quantity to begin with for a child of ten or twelve years has been twenty grains thrice daily. A Statistical Inquiry Into the Nature and Treatment of Epilepsy |Alexander Hughes Bennett 

Child, thrice child, only remember that I love you, and don't let anything disturb you. Camille (La Dame aux Camilias) |Alexandre Dumas, fils