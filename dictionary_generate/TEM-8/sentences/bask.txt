The animals spend five hours a day grazing on grasses and the rest of their time basking in the cool waters of the Magdalena River and surrounding lakes. Invasion of the hippos: Colombia is running out of time to tackle Pablo Escobar’s wildest legacy |Sarah Kaplan |January 11, 2021 |Washington Post 

For those times when you just want to sit back and bask in the highest-quality resolution and the most immersive sound, you need to pick your hardware carefully. How to get a true 4K experience on Netflix |David Nield |December 17, 2020 |Popular-Science 

Spas and bathhouses were but a memory this year, but you can bask in hot hugs from the comfort of your own home. The ultimate guide to queer gift giving 2020 |Mikey Rox |December 4, 2020 |Washington Blade 

In “The Bracebridge Dinner,” as everyone comes to the Independence Inn for an aggressively historically inaccurate “period dinner,” we get to bask in the feeling of being part of this community. Every episode of Gilmore Girls, ranked |Constance Grady |October 6, 2020 |Vox 

Although few things are more pleasurable than basking on granite slabs after a polar plunge in the High Sierra, the dapples on my shoulders from years of sun exposure indicate that I should do otherwise. In Praise of the Adventure Dress |Alison Van Houten |August 22, 2020 |Outside Online 

Five days, also, in which to bask in their own impressive achievements. Bring It On! Team USA Progresses to Round 2 |Tunku Varadarajan |June 26, 2014 |DAILY BEAST 

He'll talk up his Senate ambitions for awhile, bask in the political limelight, and then stick with his current shtick. Senator Geraldo Rivera? Seems Unlikely the Fox News Pundit Will Run |Howard Kurtz |February 2, 2013 |DAILY BEAST 

Even on Labor Day, while we bask in the blinding sun one last time. Labor Day: What Does ‘Labor’ Mean, Anyway? |D.W. Gibson |September 3, 2012 |DAILY BEAST 

Dmitri loved participating in interviews and documentaries, and again would bask in the role of generous host. Remembering Dmitri Nabokov, the Novelist’s Son and Literary Executor |Brian Boyd |May 10, 2012 |DAILY BEAST 

Wooldridge, forty, was the nineteenth-ranking official in an agency that did not exactly bask in White House attention. Cheney in History |David Frum |March 26, 2012 |DAILY BEAST 

And there, they say, two bright and agéd snakes Who once were brigadiers of infantry Bask in the sun. Punch, or the London Charivari, Volume 158, April 28, 1920 |Various 

We will bask in the warmth of a cheerful blaze this evening, and toast our toes before the glowing coals. Italian Days and Ways |Anne Hollingsworth Wharton 

After his exertions in the rain and mud, it was delightful to bask in warmth and comfort and rest his aching limbs. The Girl From Keller's |Harold Bindloss 

When she came to the room where she had left him she found no chance to “bask.” Dorothy's Travels |Evelyn Raymond 

He knew they had seen him disappearing and, airman like, they would remain awhile to bask in the sunlight and "dry off." Tam O' The Scoots |Edgar Wallace