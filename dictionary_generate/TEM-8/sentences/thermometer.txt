Look at a thermometer, and you’ll know a lot of what’s worth knowing about any sufficiently large number of molecules around you. A Physicist’s Approach to Biology Brings Ecological Insights |Gabriel Popkin |October 13, 2020 |Quanta Magazine 

This smart meat thermometer has two sensors in its single wireless probe—one for internal temperature up to 212 degrees Fahrenheit and another for ambient temperature up to 527 degrees. These pieces of kitchen gear make excellent gifts |PopSci Commerce Team |October 6, 2020 |Popular-Science 

Before entering the school, students and visitors have their temperature checked with a forehead-scanning thermometer. The Learning Curve: One School District Stayed Open – and Didn’t Have Problems |Will Huntsberry |August 13, 2020 |Voice of San Diego 

In a straightforward race down the thermometer, the hot object would first have to reach the original temperature of the warm object, suggesting that a higher temperature could only add to the cooling time. A new experiment hints at how hot water can freeze faster than cold |Emily Conover |August 7, 2020 |Science News 

The department even provides thermometers, following up with cases and contacts each day to ask about their temperature and symptoms. The U.S. largely wasted time bought by COVID-19 lockdowns. Now what? |Jonathan Lambert |July 1, 2020 |Science News 

Turn the heat down to 325°F and continue cooking until internal temperature reads 140°F on a thermometer. Make Carla Hall’s Roasted Pork Loin With Cranberries |Carla Hall |December 24, 2014 |DAILY BEAST 

It's almost impossible to read the thermometer of public opinion when it comes to LaBeouf's recent melt down/revelation. How Likable Is Alec Baldwin After His ‘New York Magazine’ Confessional? |Amy Zimmerman |February 26, 2014 |DAILY BEAST 

Every time the thermometer drops, another anti-science politician mocks climate change as a fallacy. From Snowy Atlanta to Sunny Sochi, It's All About Global Weirding |Scott Bixby |February 12, 2014 |DAILY BEAST 

Most recently, in 2010, iPod creator Tony Fadell launched Nest, a smart thermometer that adjusts to your living habits. Apple Branches Out. How’d They Do? |Nina Strochlic |December 3, 2012 |DAILY BEAST 

If you have a cooking thermometer, cook until the temperature reaches 175 degrees. Ricotta Cheese Recipe: How to Make Your Own |Petrit Husenaj |March 15, 2011 |DAILY BEAST 

One day in April the thermometer suddenly rose to eighteen above the freezing-point of Fahrenheit. The Giant of the North |R.M. Ballantyne 

He is as sensitive to every change in ones voice as the thermometer is to changes in the atmosphere. Tessa Wadsworth's Discipline |Jennie M. Drinkwater 

The warmth of the weather now began rapidly to increase; the thermometer at noon ranged as high as 79 degrees. Narrative of a Survey of the Intertropical and Western Coasts of Australia] [Volume 2 of 2] |Phillip Parker King 

The thermometer now ranged between 87 and 89 degrees and the weather was consequently extremely oppressive and sultry. Narrative of a Survey of the Intertropical and Western Coasts of Australia] [Volume 2 of 2] |Phillip Parker King 

The weather very cold, though the thermometer is at 56°, barometer 29–08; a very heavy swell. Journal of a Voyage to Brazil |Maria Graham