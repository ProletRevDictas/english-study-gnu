Her song “VBS” takes its story from Dacus’ own experiences at Christian camp, where darker undercurrents of restlessness and nihilism course below her piety. The 10 Best Songs of 2021 |Cady Lang |December 5, 2021 |Time 

She breaks rules when it’s the only way to reassert the importance of filial piety. The history of Mulan, from a 6th-century ballad to the live-action Disney movie |Constance Grady |September 4, 2020 |Vox 

“It seems that the different standard is (based on) the length of the beard and outwardly display of piety,” Hamdani said. Disco Mullah Blasphemy Row Highlights Pakistan’s Hypocrisy |Shaheen Pasha |December 21, 2014 |DAILY BEAST 

And he is to give this permission only to a priest “who has piety, knowledge, prudence and integrity of life.” Pope Francis Gives Blessing to Exorcist Conference |Barbie Latza Nadeau |October 29, 2014 |DAILY BEAST 

His policies helped engender the rise of an intolerant and severe nationalism that conflates piety with patriotism. Why So Many Pakistanis Hate Their Nobel Peace Prize Winner |Chris Allbritton |October 10, 2014 |DAILY BEAST 

Once they win, these principles get absorbed into common sense—and, of course, get betrayed left and right, like any civic piety. Climate Change Needs the Politics of the Impossible |Jedediah Purdy |April 6, 2014 |DAILY BEAST 

Before, his actions had been closely aligned church policies, which were basically a CYA masquerading as piety. Donald Wuerl: America’s Candidate for Pope? |Christopher Dickey |March 10, 2013 |DAILY BEAST 

Certainly captain Merveilles and his people showed unusual piety. The Jesuit Relations and Allied Documents, Vol. II: Acadia, 1612-1614 |Various 

This stubborn resistance lent all the more lustre to the piety of our benignant Rulers. The Jesuit Relations and Allied Documents, Vol. II: Acadia, 1612-1614 |Various 

I came here, as I supposed, a fairly good Christian, with an average amount of piety and an average number of faults. The Soldier of the Valley |Nelson Lloyd 

And for this reason the first help should be given to this State, and not to what has the pretext of piety. The Jesuit Relations and Allied Documents, Vol. II: Acadia, 1612-1614 |Various 

To me the national affectation of piety and holiness resembles a white shirt put on over a dirty skin. God and my Neighbour |Robert Blatchford