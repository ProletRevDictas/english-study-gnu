Place your hand on one edge of the skin, and using your thumb and forefinger, pinch the edge of the skin and lift it off the meat. Puerto Rican pernil is a standout Thanksgiving roast. Just guard that crispy skin. |Monti Carlo |November 18, 2021 |Washington Post 

The glove has ­touchscreen-compatible panels on the thumb and forefinger. The Best Men’s Running Gear of 2022 |jversteegh |October 26, 2021 |Outside Online 

The emphasis was his: a high quivering shout, a forefinger stabbing upward. Yeshayahu Leibowitz Is Not The Name Of A Street |Gershom Gorenberg |April 18, 2013 |DAILY BEAST 

There is more of the uplifted forefinger and the reiterated point than I should have allowed myself in an essay. The Salvaging Of Civilisation |H. G. (Herbert George) Wells 

A muscle twitched in his corded neck; Arnold eased his long frame into a chair, rubbed thumb and forefinger at his eyes. We're Friends, Now |Henry Hasse 

Then Sogrange's voice and the beat of his forefinger upon the table stiffened him into sudden alertness. The Double Four |E. Phillips Oppenheim 

The garon laughed; held up one hand, with the forefinger crooked. Blackwood's Edinburgh Magazine, Volume 67, Number 414, April, 1850 |Various 

Meanwhile Carie talked busily to herself, gesticulating with one small forefinger. The Story of the Big Front Door |Mary Finley Leonard