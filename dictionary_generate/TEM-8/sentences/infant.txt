Childcare for infants can cost on average up to $24,000 a year, which is just shy of half the national average family income. Millennials and Gen Zers need better childcare. Can either Biden or Trump deliver? |matthewheimer |August 29, 2020 |Fortune 

Amnesty later discovered that even those 19 infants had died before the Iraqi invasion. The Great Lie of the First Gulf War |Mat Nashed |August 17, 2020 |Ozy 

The 53 infants that had a one-month telemedicine visit continued to show no signs of the illness. Masks help new moms with COVID-19 safely breastfeed their babies |Aimee Cunningham |July 23, 2020 |Science News 

Puopolo is the lead author of AAP’s guidance, which now states that a baby is at low risk of infection when staying with the mother after delivery if she wears a mask and cleans hands before holding her infant. Masks help new moms with COVID-19 safely breastfeed their babies |Aimee Cunningham |July 23, 2020 |Science News 

As infants, we observe our parents and teachers, and from them we learn how to walk, talk, read—and use smartphones. How the Brain Builds a Sense of Self From the People Around Us |Sam Ereira |July 17, 2020 |Singularity Hub 

Within six days, however, the infant was admitted to a pediatric hospital with diarrhea, bluish skin, and respiratory failure. Are Water Births Toxic to Babies? |Brandy Zadrozny |December 12, 2014 |DAILY BEAST 

By the 1950s the rapid assignment of gender to an ambiguously gendered infant had become standard. Intersexuality and God Through the Ages |Candida Moss |November 9, 2014 |DAILY BEAST 

Increased access to reproductive healthcare has resulted in better maternal and infant health outcomes. What Brazil’s Dilma Rousseff Can Teach Hillary Clinton |Heather Arnet |October 29, 2014 |DAILY BEAST 

Two bowls were set before the infant—one containing gold and jewels, the other hot coals. Jon Stewart and 'Meet The Press' Would Have Been One Unhappy Marriage |Lloyd Grove |October 9, 2014 |DAILY BEAST 

An infant too young to have received his first round of shots gasps for air after having been infected with pertussis. Hey Anti-Vaxxers, Watch NOVA: Vaccines--Calling the Shots |Russell Saunders |September 11, 2014 |DAILY BEAST 

Soon after that, I wrote you in regard to the condition in which we found this infant Church and Colony. The Jesuit Relations and Allied Documents, Vol. II: Acadia, 1612-1614 |Various 

And then Jolly Robin would feel ashamed that he had even thought of being so cruel to an infant bird, even if he was a Cowbird. The Tale of Grandfather Mole |Arthur Scott Bailey 

Two or three more infant deaths intervened before the birth of Marcella. The Pastor's Fire-side Vol. 3 of 4 |Jane Porter 

There was a picture of Madame Lebrun with Robert as a baby, seated in her lap, a round-faced infant with a fist in his mouth. The Awakening and Selected Short Stories |Kate Chopin 

Seen in this light, infant mortality and the cruel wastage of disease were viewed with complacence. The Unsolved Riddle of Social Justice |Stephen Leacock