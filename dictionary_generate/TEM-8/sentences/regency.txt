In 1999, El Elegante threw a fashion benefit for more than 600 guests on the beach at the Hyatt Regency Hotel in Acapulco. Trading Dime Bags for Salvador Dali |Jason McGahan |October 19, 2014 |DAILY BEAST 

Earlier this week, Regency Centers, a shopping center developer, sold $250 million in so-called green bonds. The Bond Market Goes Green |The Daily Beast |May 27, 2014 |DAILY BEAST 

It began, for them all, with the urge to seek some form of liberty and escape the stultifying conventions of Regency England. The Man Who Invented Vampires and the Creepiest Literary Gathering Ever |Emma Garman |November 24, 2013 |DAILY BEAST 

Scher set up a face-to-face meeting with John Edwards on September 18 at the Regency Hotel in New York City. The Man Who Confronted Edwards About His Dangerous Infidelity Game |Diane Dimond |May 5, 2012 |DAILY BEAST 

Over drinks at the Regency that afternoon it was mutually decided that Miss Hunter would no longer travel with the candidate. The Man Who Confronted Edwards About His Dangerous Infidelity Game |Diane Dimond |May 5, 2012 |DAILY BEAST 

And in case of the prince's death, the regency to remain in the hands of the Princess Maria Leopoldina. Journal of a Voyage to Brazil |Maria Graham 

I, as the king's mother and a member of the council of the regency,—I protest against what appears to me a crime of lese-majeste. Catherine de' Medici |Honore de Balzac 

It will be recollected that in the last chapter the appointment of a council of regency at Lahore was recorded. The History of England in Three Volumes, Vol.III. |E. Farr and E. H. Nolan 

In Ireland the parliament met in 1789 when the regency question was still before the English parliament. The Political History of England - Vol. X. |William Hunt 

The prince promised to accept the regency, and stated his objections to the restrictions. The Political History of England - Vol. X. |William Hunt