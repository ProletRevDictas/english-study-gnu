After a “low” point of 26% growth in June, sales in the US have accelerated the past two months—a sign that video games continue to surge in popularity even as quarantines end and travel restrictions loosen. The pandemic has turned everyone into gamers |Adam Epstein |September 16, 2020 |Quartz 

Together, they show that throughout the coronavirus crisis, the meatpacking industry has repeatedly turned to the agency for help beating back local public health orders and loosening regulations to keep processing lines running. Emails Show the Meatpacking Industry Drafted an Executive Order to Keep Plants Open |by Michael Grabell and Bernice Yeung |September 14, 2020 |ProPublica 

Using their wealth and power in the pursuit of profits, corporations led the way in loosening the external constraints that protected workers and other stakeholders against overreaching. 50 years later, Milton Friedman’s shareholder doctrine is dead |jakemeth |September 13, 2020 |Fortune 

But, with shelter-at-home restrictions loosened so that shows can be shot in studios and on location, networks’ and streamers’ interest in programs produced exclusively remotely “has absolutely cooled off,” said one producer. ‘Covid-proof production’: TV networks, streamers seek out show formats that can adapt to another shooting shutdown |Tim Peterson |August 14, 2020 |Digiday 

The men’s suit business has been in decline for years in the US, thanks to the century-long trend of clothes getting more casual and even the most conservative office places loosening up dress codes. Can men’s suits survive Covid-19? |Marc Bain |August 4, 2020 |Quartz 

Two years later, he had released all Soviet dissidents from prison and was beginning to loosen the reins on Soviet bloc countries. How the Fall of the Berlin Wall Radicalized Putin |Masha Gessen |November 9, 2014 |DAILY BEAST 

Not even the knowledge gleaned from all the books in the world can loosen the grasp of human sadness. How Depression Could Save Your Life |Nick Romeo |March 4, 2014 |DAILY BEAST 

Add a dash of milk to loosen, although you want the mixture thick. Cat Cora’s Valentine’s Day Menu for Single People |Cat Cora |February 13, 2014 |DAILY BEAST 

Past attempts to loosen the grip of taxi cartels have been thwarted by the industry. Uber and Its Enemies |Jim Epstein |February 10, 2014 |DAILY BEAST 

Spurred by this success, Austin and Benedetto plan to loosen control over the guest list even more for future events. A Most Illegal Adventure with New York City’s Wildest Underground Event Planners |Nina Strochlic |December 16, 2013 |DAILY BEAST 

With great deliberation and much formality Wu-pom Nai proceeded to loosen the boy's heavy plaits of hair. Our Little Korean Cousin |H. Lee M. Pike 

Nothing is more annoying than to have the hair loosen or the head-dress fall off in a crowded ball room. The Ladies' Book of Etiquette, and Manual of Politeness |Florence Hartley 

They learned to loosen the bark of a tree without breaking it except along one edge. The Later Cave-Men |Katharine Elizabeth Dopp 

If thus used two or three days in succession it will effectually loosen tartar, even of long standing. The Ladies' Book of Etiquette, and Manual of Politeness |Florence Hartley 

Inside it was a clock, which could be set to run a certain time and then loosen a sort of gunlock. Stories of Our Naval Heroes |Various