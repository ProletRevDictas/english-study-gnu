You know, there was training 10,000 folks to replant kelp forests and eel grasses. Is the Future of Farming in the Ocean? (Ep. 467) |Stephen J. Dubner |June 24, 2021 |Freakonomics 

The research is the first to show how this dual jaw action allows snowflake eels to feed just as effectively on land as in the water. Moray eels enjoy surf ‘n turf with a surprise second set of jaws |Claire Maldarelli |June 22, 2021 |Popular-Science 

A weekly night dive is also available, a great chance to encounter octopuses and eels. With covid protocols, a Caribbean fly-fishing haven is back in business |Chris Santella |April 2, 2021 |Washington Post 

There are, however, favorable aspects of the license approved by the FERC, Prost said, such as the programs to promote mussel restoration, fish and eel passage, turtle management and waterfowl nesting. New 50-year Conowingo Dam license gets federal approval |Christine Condon |March 31, 2021 |Washington Post 

Brown pointed to research on groupers cooperating with moray eels to hunt. Octopuses Find New Hunting Buddies - Issue 95: Escape |Brandon Keim |January 28, 2021 |Nautilus 

Raw eel seemed to be popular during and after the Middle Ages. History's Craziest Hangover Cures |Justin Jones |December 30, 2014 |DAILY BEAST 

Today, Eel Pie Island is home to a mellower bunch: retirees, artists, and the like. The Isle Where the Rolling Stones Began |Nina Strochlic |September 5, 2014 |DAILY BEAST 

These are just a few of the famous visitors to Eel Pie Island, a centuries-old refuge for musicians, hippies, and writers. The Isle Where the Rolling Stones Began |Nina Strochlic |September 5, 2014 |DAILY BEAST 

There are literally restaurants that focus entirely on a single ingredient, like eel for example. Fresh Picks |Donatella Arpaia |August 11, 2009 |DAILY BEAST 

Stanley Hall also went for pastime, and Billy Towler slid into the boat like an eel, without leave, just as it pushed off. The Floating Light of the Goodwin Sands |R.M. Ballantyne 

Your Caroline, so enticing five hours before in this very chamber where she frisked about like an eel, is now a junk of lead. The Petty Troubles of Married Life, Complete |Honore de Balzac 

Swaying tufts of vegetation marked the rapid passage of eel-like bodies. Blazed Trail Stories |Stewart Edward White 

We succeeded in taking an eel, a few crabs, and a small quantity of snails. A Narrative of the Shipwreck, Captivity and Sufferings of Horace Holden and Benj. H. Nute |Horace Holden 

The lighter man was slippery as an eel, as hard to hit as a Corbett. The Highgrader |William MacLeod Raine