For people who rely solely on Medicare or Medicaid, or don’t even have health insurance, these are the only places where they can afford to see a doctor. The doctor will Zoom you now |Katie McLean |February 24, 2021 |MIT Technology Review 

Among those suggestions was making Housing Choice Vouchers an entitlement benefit, akin to Medicaid and Social Security. Millions of Tenants Behind on Rent, Small Landlords Struggling, Eviction Moratoriums Expiring Soon: Inside the Next Housing Crisis |Abby Vesoulis/Seattle |February 18, 2021 |Time 

In fact, in a 2019 article I co-authored with then-University of Pennsylvania researcher Kalind Parish, we found that poorer residents in states where Medicaid had been expanded were notably more supportive of the ACA after its implementation. The Democrats Have An Ambitious Agenda. Here’s What They Should Learn From Obamacare. |Dan Hopkins |February 17, 2021 |FiveThirtyEight 

Republicans control the governments in most states that have not expanded Medicaid, and even the three states with Democratic governors have to contend with Republican-led legislatures. Democrats' Push to Expand Medicaid Depends on Red States Taking the Bait |Abigail Abrams |February 14, 2021 |Time 

He pointed out that the state’s 2018 expansion of Medicaid coverage help more Virginians be able to afford health care prior to the pandemic. Northam cites nondiscrimination law in State of the Commonwealth address |Philip Van Slooten |January 14, 2021 |Washington Blade 

Now that I am free, I have Medicaid and doctors no longer assume I am malingering. A Million Ways to Die in Prison |Daniel Genis |December 8, 2014 |DAILY BEAST 

Medicaid is required to cover people in skilled nursing facilities, that is, institutions. Medicaid Will Give You Money for At-Home Care, but You Might Wait Years |Elizabeth Picciuto |December 2, 2014 |DAILY BEAST 

More importantly, Medicaid served as a secondary insurance to his primary insurance. Medicaid Will Give You Money for At-Home Care, but You Might Wait Years |Elizabeth Picciuto |December 2, 2014 |DAILY BEAST 

However, since he was living at home, our income and assets disqualified him from SSI and Medicaid. Medicaid Will Give You Money for At-Home Care, but You Might Wait Years |Elizabeth Picciuto |December 2, 2014 |DAILY BEAST 

As of 2012, there are over 523,000 people across the country on Medicaid waiver lists; over 309,000 of those people have I/DD. Medicaid Will Give You Money for At-Home Care, but You Might Wait Years |Elizabeth Picciuto |December 2, 2014 |DAILY BEAST 

Social Security and Medicare and Medicaid are commitments of conscience, and so it is our duty to keep them permanently sound. State of the Union Addresses of George W. Bush |George W. Bush 

But we must not abandon our fundamental obligations to the people who need Medicare and Medicaid. Complete State of the Union Addresses from 1790 to 2006 |Various 

This plan will balance the budget and invest in our people while protecting Medicare, Medicaid, education and the environment. Complete State of the Union Addresses from 1790 to 2006 |Various