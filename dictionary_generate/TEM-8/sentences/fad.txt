It is clear data privacy is not a fad going away anytime soon. The downfall of adtech means the trust economy is here |Walter Thompson |November 23, 2020 |TechCrunch 

To the uninitiated, bowfishing may seem like the latest gear-intensive fad. Don’t have a boat? Try bowfishing. |By Natalie Krebs/Outdoor Life |November 3, 2020 |Popular-Science 

When I hear people talking about serious political movements as fads, my sense is that they are trying to discredit those movements, which, in turn, makes me wonder if their desire to discredit them comes from feeling threatened. What the public is getting right — and wrong — about police abolition |Fabiola Cineas |October 30, 2020 |Vox 

Now, five months later, doubters have cast it off as a political fad, an idea that’s gimmicky and fleeting. What the public is getting right — and wrong — about police abolition |Fabiola Cineas |October 30, 2020 |Vox 

And, more importantly, establish which are here to stay and which are just a fad with no long-term implications. How to (re)build an SEO agency today – Part 3: Changing business models |Sponsored Content: SEOmonitor |September 22, 2020 |Search Engine Land 

I just wanted to work, and thought it was a fad that would go away. David Lynch on Transcendental Meditation, ‘Twin Peaks,’ and Collaborating With Kanye West |Marlow Stern |October 6, 2014 |DAILY BEAST 

Bored with her gym and curious about the new fad, Angel added a few Pop Physique classes to her fitness routine. #ButtSchool - How Porn Stars Work Out: Pop Physique Promises the Perfect Derriere |Aurora Snow |August 23, 2014 |DAILY BEAST 

The proliferation of zany burger toppings came next as an inevitable by-product of the high-end burger fad. Have We Reached ‘Peak Burger’? The Crazy Fetishization of Our Most Basic Comfort Food |Brandon Presser |July 31, 2014 |DAILY BEAST 

I remain unshaken in my belief that, for many people, eating gluten free is merely participating in yet another food fad. Celiac or Not, Gluten Free Dish Soap Is Ridiculous |Russell Saunders |July 16, 2014 |DAILY BEAST 

But even if blue passes as just another fad, it will be only until we become senior citizens. Tangled Up in Blue: Young Stars and Their Blue Rinses |Erin Cunningham |July 9, 2014 |DAILY BEAST 

There's such a fad for nature study these days that almost everybody this year has ordered the 'Gray-Plush Squirrel' series. Molly Make-Believe |Eleanor Hallowell Abbott 

People had laughed at his fad, but now he was more pleased with himself as a result of it than ever before. The Winning Clue |James Hay, Jr. 

My collection is now great; but I fear I shall tire of the fad before completing it. The Letters of Ambrose Bierce |Ambrose Bierce 

Every private inclination is a fad, and even fads have their fixed forms. The New Society |Walther Rathenau 

"Oh, it's a sort of school fad," said the Tennessee Shad, as Doc disappeared. The Varmint |Owen Johnson