Reporters at the Press and ProPublica wanted to determine how prevalent these terms are across the state. How We Found Pricey Provisions in New Jersey Police Contracts |by Agnes Chang, Jeff Kao and Agnel Philip, ProPublica, and Andrew Ford, Asbury Park Press |February 8, 2021 |ProPublica 

The latter is an anti-vaxx belief so prevalent, it led one Wisconsin pharmacist to allegedly tamper with vials of the vaccine. Why Fights Over The COVID-19 Vaccine Are Everywhere On Facebook |Kaleigh Rogers |January 22, 2021 |FiveThirtyEight 

The takeaway is, this is a variant that’s becoming more prevalent, and we need to lean in and understand more about it. Another coronavirus variant linked to growing share of cases, several large outbreaks, in California |Fenit Nirappil |January 18, 2021 |Washington Post 

That number would likely be about sufficient to achieve herd immunity, but vaccine skepticism is much more prevalent among Republicans. Pence got the Covid-19 vaccine to build confidence in it |Aaron Rupar |December 18, 2020 |Vox 

The condition is especially prevalent in South Asia and sub-Saharan Africa. One man’s crusade to end a global scourge with better salt |Katie McLean |December 18, 2020 |MIT Technology Review 

There is another prevalent practice in which family members push the victim to the limit until she kills herself. The Dishonor of Honor Killings |Sara Stewart |October 20, 2014 |DAILY BEAST 

Yet the sense is that anti-LGBT hate crimes are becoming more prevalent, and more terrifying. Is Brooklyn Becoming Unsafe for Gays? It Depends On Which Ones |Jay Michaelson |October 18, 2014 |DAILY BEAST 

Rape continues to be prevalent worldwide, particularly in areas of conflict and crisis. Time for U.S. to Support Abortion for Rape Victims in Other Countries |Cecile Richards |October 17, 2014 |DAILY BEAST 

The view remains prevalent that Africans need to be saved from themselves. Why the US-Africa Summit Was Important and Why It Wasn't Enough |John Prendergast |August 9, 2014 |DAILY BEAST 

Sober and muted colors including shades of gray, one described in a local paper as ‘Battleship,’ were prevalent. How World Wars Made Females More Androgynous |Liza Foreman |July 22, 2014 |DAILY BEAST 

They were sure that the white troops in Meerut would soon arrive and put an end to the prevalent anarchy. The Red Year |Louis Tracy 

In the town the European mode of living is entirely prevalent—more so than in any other place abroad that I have seen. A Woman's Journey Round the World |Ida Pfeiffer 

Between 10° and 20° South latitude we again met with very peculiar prevalent winds. A Woman's Journey Round the World |Ida Pfeiffer 

It is certainly a lucky thing for travellers that this strange custom is not prevalent everywhere. A Woman's Journey Round the World |Ida Pfeiffer 

Very hot; very limp with the prevalent disease but greatly cheered up by the news of yesterday evening's battle at Helles. Gallipoli Diary, Volume I |Ian Hamilton