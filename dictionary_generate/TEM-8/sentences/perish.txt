The story is also a tale of resilience and persistence, as Chihiro gradually draws on her inner strength to endure this land where humans are designed to perish. How Spirited Away Changed Animation Forever |Kat Moon |July 20, 2021 |Time 

So I’m hoping that when I get back on the road, that part of me will not ever perish again. The Outdoor Inspirations of Thomas Rhett’s New Album |smurguia |July 12, 2021 |Outside Online 

It became part of the local culture, a last stop for unwanted cats, who either learned to survive from hunting and food left by visitors or perished. People have abandoned hundreds of cats on a deserted Brazilian island. Officials aren’t sure how to save them. |Terrence McCoy |June 4, 2021 |Washington Post 

Her colleague, Second Lieutenant Carol Ann Drazba, also perished. Eight Women’s Names Are Among the Thousands on the Vietnam Memorial Wall. Here’s What to Know About Them |Francine Uenuma |May 27, 2021 |Time 

One America, King said, was “overflowing with the milk of prosperity and the honey of opportunity,” while in the other, people were “perishing on a lonely island of poverty in the midst of a vast ocean of material prosperity.” COVID-19 Reminded Us Of Just How Unequal America Is |Neil Lewis Jr. (nlewisjr@cornell.edu) |March 29, 2021 |FiveThirtyEight 

Actually seeing someone perish is horrible for people to see—whether you have covered tough stories in the past or not. The Photojournalist Who Stared Down Ebola |Abby Haglage |November 8, 2014 |DAILY BEAST 

Otherwise, it might be advisable—perish the thought—to start reading newspapers again. What’s Got CNN’S Anchors So Riled? |Lloyd Grove |February 19, 2014 |DAILY BEAST 

By all accounts he had expected to perish with the other Elders. Confessions of a Death Camp Collaborator: Claude Lanzmann’s ‘The Last of the Unjust’ |Jimmy So |February 7, 2014 |DAILY BEAST 

World War II offers innumerable opportunities for her to perish during the Blitz. Life After Life |Malcolm Jones |April 7, 2013 |DAILY BEAST 

And those who really feel that they will perish unless they have 32 ounces of Mountain Dew Code Red can simply buy two. Michael Tomasky: Mayor Bloomberg Is Right to Declare a War on Sugar |Michael Tomasky |June 2, 2012 |DAILY BEAST 

The inheritance of the children of sinners shall perish, and with their posterity shall be a perpetual reproach. The Bible, Douay-Rheims Version |Various 

Let him that escapeth be consumed by the rage of the fire: and let them perish that oppress thy people. The Bible, Douay-Rheims Version |Various 

It is certain that if this retreat, from which the girls go out married, were to fail, they would perish and be lost. The Philippine Islands, 1493-1898, Volume XX, 1621-1624 |Various 

For my name's sake I will remove my wrath far off: and for my praise I will bridle thee, lest thou shouldst perish. The Bible, Douay-Rheims Version |Various 

For the nation and the kingdom that will not serve thee, shall perish: and the Gentiles shall be wasted with desolation. The Bible, Douay-Rheims Version |Various