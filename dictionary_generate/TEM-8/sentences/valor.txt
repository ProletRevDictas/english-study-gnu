For your valor will be on the increase, but our energy is already running out. The Mighty Macedonian Phalanx: 6 Things You Should Know |Dattatreya Mandal |July 29, 2022 |Realm of History 

War, which is always messy, brutal and chaotic, is represented by a scene of individual valor, which is an important but limited truth. The viral photo of the last soldier in Afghanistan is powerful — and that’s why it’s deceptive |Philip Kennicott |August 31, 2021 |Washington Post 

No doubt there is plenty more they could say, if they chose, but for the Californian Royals, discretion now may be the more lucrative part of valor. Will Harry and Meghan Markle’s Baby Daughter Lead to a Royal Reconciliation? |Tom Sykes |June 6, 2021 |The Daily Beast 

Even the valor of tragedy is denied to Daisy, “a woman born with a voice that lacks a tragic register.” Carol Shields’s Tale Of Secondhand Life |Nathaniel Rich |October 26, 2014 |DAILY BEAST 

The founder of Valor por Tamaulipas disputes this version of events and has characterized it as disinformation. She Tweeted Against the Mexican Cartels. They Tweeted Her Murder. |Jason McGahan |October 21, 2014 |DAILY BEAST 

Understandably the narcos wanted to know the real identities of Felina and her compañeros at Valor por Tamaulipas. She Tweeted Against the Mexican Cartels. They Tweeted Her Murder. |Jason McGahan |October 21, 2014 |DAILY BEAST 

The founder of Valor por Tamaulipas confirmed that the photos are of Felina. She Tweeted Against the Mexican Cartels. They Tweeted Her Murder. |Jason McGahan |October 21, 2014 |DAILY BEAST 

Ground war remained, inevitably, a place of high sacrifice as well as valor. Blood and War: The Hard Truth About ‘Boots on the Ground’ |Clive Irving |September 22, 2014 |DAILY BEAST 

De Valor resumed it, when he raised the rebel standard on the Alpuxara mountains. The Pastor's Fire-side Vol. 3 of 4 |Jane Porter 

He distinguished himself by his valor against the Saracens, under Charlemagne. The Every Day Book of History and Chronology |Joel Munsell 

This battle lasted four hours; the Spaniards performed prodigies of valor, and were victorious. The Every Day Book of History and Chronology |Joel Munsell 

To the arts of the politician he added the valor of the warrior, and exterminated the Saracens who invaded Italy. The Every Day Book of History and Chronology |Joel Munsell 

Thus Arnold, though defeated, gained by his valor the fruit of victory, for the British gave up their plan of holding the lake. Stories of Our Naval Heroes |Various