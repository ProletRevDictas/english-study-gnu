Gynaecology owes much to the efforts of American schools and practitioners. An Epitome of the History of Medicine | Roswell Park 

Such teaching would have much to do with making advances in gynaecology and obstetrics possible. The Century of Columbus | James J. Walsh 

Gynaecology may be said to be one of the most ancient branches of medicine. Encyclopaedia Britannica, 11th Edition, Volume 12, Slice 7 | Various