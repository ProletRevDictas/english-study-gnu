Space is symmetric, and yet the laws of motion would not show any symmetry; they would have to distinguish between right and left. The Foundations of Science: Science and Hypothesis, The Value of Science, Science and Method |Henri Poincar 

Keratosis palmaris et plantaris (symmetric keratodermia), as regards the local condition, is a somewhat similar affection. Essentials of Diseases of the Skin |Henry Weightman Stelwagon 

In this way each part of the fan is aggraded and its symmetric form is preserved. The Elements of Geology |William Harmon Norton 

In symmetric folds (Figs. 169 and 180) the dips of the rocks on each side the axis of the fold are equal. The Elements of Geology |William Harmon Norton 

In some fishes with vertebrated tail fins the fin is symmetric (Fig. 300), and this seems to be the primitive type. The Elements of Geology |William Harmon Norton