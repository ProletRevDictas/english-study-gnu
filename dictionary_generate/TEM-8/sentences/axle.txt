When it encounters slippery conditions, it can lock together the speeds of the front and rear axles, then, with that diff, also the speeds of both wheels on the rear axle. Five Off-Road Vehicles to Look Forward to in 2021 |Wes Siler |January 1, 2021 |Outside Online 

This comes from the balanced center of gravity—you’re not too far over the front axle, nor too high or far back. The Specialized Diverge Is a Gravel Bike for All |Andy Cochrane |November 8, 2020 |Outside Online 

Pfau and Colville say that one of the Hummer EV's three motors is located on the front axle, driving the front wheels through a locking differential. A First Look at the 2022 GMC Hummer EV (It's Awesome) |Wes Siler |October 21, 2020 |Outside Online 

It seems certain that without power to distribute through traditional axles that all four wheels of the Hummer EV will be individually suspended. A First Look at the 2022 GMC Hummer EV (It's Awesome) |Wes Siler |October 21, 2020 |Outside Online 

Remove the front wheelEspecially when a bike has quick-release axles, it’s always a good idea to remove the front wheel and lock it through the main triangle and to a fixed object. The most secure ways to lock up your bike |By Michael Frank/Cycle Volta |August 26, 2020 |Popular-Science 

When we finally made it into the village, the axle on one of our trucks split in half. A Man to Believe In: Eulogy for Marine Master Sergeant Aaron Torian |Elliot Ackerman |March 5, 2014 |DAILY BEAST 

This intricate sequence wrapped us around the axle more than once during design and testing in the years preceding launch. Curiosity’s Mars Landing Narrated Moment by Moment by Flight Director Keith Comeaux |Keith Comeaux |August 7, 2012 |DAILY BEAST 

He starts by tying a cable onto the rear axle of the car, fastening the other end to a post. April Fool’s! Hollywood’s Biggest Pranks |Anna Klassen |April 1, 2012 |DAILY BEAST 

The success of the whole structure was extremely sensitive to the size of the axle. The Hardest Part of Inventing the Wheel? Not the Round Part. |David Frum |March 15, 2012 |DAILY BEAST 

The sensitivity of the wheel-and-axle system to all these factors meant that it could not have been developed in phases, he said. The Hardest Part of Inventing the Wheel? Not the Round Part. |David Frum |March 15, 2012 |DAILY BEAST 

There must be a fly-wheel, with a notch to carry the rope, and also a small notch wheel on the drum-axle. Life of Richard Trevithick, Volume II (of 2) |Francis Trevithick 

The "reach" went on with the forward axle, and the back wheels spun around, dashed across the road, and smashed into the fence. Motor Matt's "Century" Run |Stanley R. Matthews 

The axle, which is likewise of wood, is never greased, and thus causes the demoniacal kind of music to which I alluded. A Woman's Journey Round the World |Ida Pfeiffer 

I got that in 1841, through the breaking of my near hind axle as we came down through Guildford town. The Portsmouth Road and Its Tributaries |Charles G. Harper 

Fig. 36 gives the loads per axle and the distribution of loads in some exceptionally heavy modern British locomotives. Encyclopaedia Britannica, 11th Edition, Volume 4, Part 3 |Various