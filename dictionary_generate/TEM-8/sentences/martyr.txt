In death, Babbitt has become a martyr to many on the far right. U.S. Capitol Police officer cleared of wrongdoing in fatal shooting of Ashli Babbitt during Capitol attack |Keith L. Alexander, Justin Jouvenal, Spencer Hsu |April 15, 2021 |Washington Post 

Since the fatal shooting, Lemp has become a martyr among anti-government extremists around the country. Body-cam video in fatal Duncan Lemp raid confirms police did not record shooting |Dan Morse |January 11, 2021 |Washington Post 

Those were individuals who were just trying to live, not trying to be martyrs or tokens for political platforms or politicians. The way we express grief for strangers is changing |Tanya Basu |December 3, 2020 |MIT Technology Review 

The gains of the civil rights era, earned through mass protest and the blood of martyrs, again prompted a conservative backlash. This year’s elections will decide whether America witnesses a third Reconstruction |David Love |October 29, 2020 |Washington Post 

Another thing I think is really important is that right now we expect people to be sort of martyrs if they enter into government service and then they turn around and become lobbyists to make a lot of money. Why Is This Man Running for President? (Ep. 362 Update) |Stephen J. Dubner |December 19, 2019 |Freakonomics 

Conservative Muslim women in Turkey hailed Esme as a martyr and a symbol of female strength and resistance. Allah, Mom, and Baklava: Turkish President Uses Mothers and Kids as Political Pawns |Xanthe Ackerman |November 27, 2014 |DAILY BEAST 

Protestors chanted “with our blood and our souls, we will avenge you, oh martyr.” A New Intifada? Israel’s Arab Citizen Uprising Spreads |Creede Newton |November 10, 2014 |DAILY BEAST 

He informed her that Hamzat had “become a shahid,” a martyr, and he was dead. The Secret Life of an ISIS Warlord |Will Cathcart, Vazha Tavberidze, Nino Burchuladze |October 27, 2014 |DAILY BEAST 

“Haters gonna hate” makes the person who says it into an automatic martyr, persecuted, misunderstood, maligned. Why We Should Hate 'Haters Gonna Hate' |Tim Teeman |August 25, 2014 |DAILY BEAST 

With that, writes historian Edward Larson, “The fundamentalist movement acquired a martyr.” The Scopes Monkey Trial 2.0: It’s Not About the Stupid Science-Deniers |Michael Schulson |July 21, 2014 |DAILY BEAST 

Joan Boughton, a widow, was burned for heresy; said to be the first female martyr of England. The Every Day Book of History and Chronology |Joel Munsell 

It would not take two minutes to convert him from the inquisitor to the martyr at the stake. Tessa Wadsworth's Discipline |Jennie M. Drinkwater 

Mrs. Stone's children were notoriously healthy, but she was of the stuff of which the modern martyr is made. Ancestors |Gertrude Atherton 

To his bourgeois mind, for all his imitation of the Chicago martyr, my words must have sounded knavish. Prison Memoirs of an Anarchist |Alexander Berkman 

The martyr thanked him for his kindness, and promised to pray for him when she came into God's presence. Mary, Help of Christians |Various