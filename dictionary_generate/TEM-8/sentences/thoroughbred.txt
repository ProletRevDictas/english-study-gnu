The growth of the bush circuit is also problematic for horses on the regulated quarter horse racing circuit, the dustier cousin of thoroughbred racing popular in the American southwest and Mexico. A horse track with no rules |Gus Garcia-Roberts |August 5, 2022 |Washington Post 

Billionaires can not only deduct the costs of buying, owning and training thoroughbred horses, but they can treat all manner of pastimes and side pursuits as businesses, and then tap those businesses as an extra source of deductions. When You’re a Billionaire, Your Hobbies Can Slash Your Tax Bill |by Paul Kiel, Jesse Eisinger and Jeff Ernsthausen |December 8, 2021 |ProPublica 

Fireman was new to the endeavor, but he’d already spent millions building a professional horse-racing operation, including dropping almost $1 million on a single thoroughbred, a descendant of a Preakness winner. When You’re a Billionaire, Your Hobbies Can Slash Your Tax Bill |by Paul Kiel, Jesse Eisinger and Jeff Ernsthausen |December 8, 2021 |ProPublica 

The sport’s royalty, including the billionaire owners of thoroughbreds, was well represented. When You’re a Billionaire, Your Hobbies Can Slash Your Tax Bill |by Paul Kiel, Jesse Eisinger and Jeff Ernsthausen |December 8, 2021 |ProPublica 

“It’s a thoroughbred, all-electric racing plane,” says Matheu Parr, who leads the project for Rolls-Royce. Rolls-Royce’s zippy electric airplane wants to break speed records—and power air taxis of the future |Rob Verger |September 27, 2021 |Popular-Science 

De la Renta was a confident thoroughbred, never needing to scream for attention. Fashion Designer Oscar de la Renta, American Great, Dead at 82 |Tim Teeman |October 21, 2014 |DAILY BEAST 

They acquired Lucky Pulpit for the bargain price of $2,000—about as cheap as a thoroughbred comes these days. Why California Chrome’s Fairy Tale Didn’t End Happily Ever After |Michael Fensom |June 8, 2014 |DAILY BEAST 

A thoroughbred horse stampeding across the Churchill Downs turn. McConnell Campaign’s Huge Duke Whopper |Michael Tomasky |March 25, 2014 |DAILY BEAST 

The mix of  kickboxing, yoga and Pilates would surely whip anyone into shape, let alone a thoroughbred supermodel. Anderson Cooper Adopts The Chanel Hula-Hoop Bag; Lagerfeld talks Eating Disorders |The Fashion Beast Team |October 12, 2012 |DAILY BEAST 

Occasionally you will encounter a thoroughbred Weenie, a type of Weenie who is to the weaseling, weaselish manner born. Monsters vs. Weenies |Lee Siegel |December 29, 2009 |DAILY BEAST 

If he had not the air of a thoroughbred, he had none of the plebeian clumsiness of the cart-horse. Paul Patoff |F. Marion Crawford 

But in his eye was the gay inextinguishable gleam of the thoroughbred. The Highgrader |William MacLeod Raine 

Brought him a high-stepping, fiery, thoroughbred colt which was the admiration and envy of all Riveredge. Three Little Women |Gabrielle E. Jackson 

A well-bred horse is essential, and other things being equal, if clean thoroughbred, so much the better. The Sportswoman's Library, v. 2 |Various 

That old ramshackle of a lumber nag whose every rib you can count through her skin is your beautiful thoroughbred? Boyhood in Norway |Hjalmar Hjorth Boyesen