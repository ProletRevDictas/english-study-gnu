In spite of their differences with Cardona, both large teachers unions in Connecticut are supporting his nomination. In Connecticut, Miguel Cardona led a full-court press for schools to reopen |Laura Meckler, Nick Anderson |February 2, 2021 |Washington Post 

So what surprised me was there was still an enormous volume of users coming through, in spite of all this competition that was out there. PPC award winners talk strategies for competitive times, industries, and marketplaces |Carolyn Lyden |January 29, 2021 |Search Engine Land 

Disregarding that promise, her father’s wife sells her out of spite. Sadeqa Johnson’s ‘Yellow Wife’ chronicles one tenacious enslaved woman’s survival in the antebellum South |Ellen Morton |January 12, 2021 |Washington Post 

These stories have a remarkable richness — not in spite of the pandemic, but because of it. The Highlight’s best reads of the year |Lavanya Ramanathan |December 24, 2020 |Vox 

In spite of all the hours spent talking, there are some things you don’t tell Fatima about. Dark spaces on the map |Katie McLean |December 18, 2020 |MIT Technology Review 

Like I said, in spite of or because of my circumstances, I was able to accomplish my dreams. Tim Howard’s Wall of Intensity |William O’Connor |December 22, 2014 |DAILY BEAST 

Much of the drama that transpires towards the end of the story is due to the pure love itself, not in spite of it. The Japanese Women Who Love Gay Anime |Brandon Presser |December 6, 2014 |DAILY BEAST 

In spite of that, Stewart had never appeared on the Report until last night, when he joined to discuss his new film, Rosewater. Pot-Smoking Grannies, Jimmy Fallon Covers U2, and More Viral Videos |The Daily Beast Video |November 23, 2014 |DAILY BEAST 

But Boehner and the Republicans refused, completely out of cowardice and to spite Obama. For Obama, Hell Week Has Arrived |Michael Tomasky |November 15, 2014 |DAILY BEAST 

Finally, if they go to war abroad again, it will allow them to grow defense spending in spite of budget caps. Assuming GOP Does Take the Senate, Dems Have Nothing to Fear |Veronique de Rugy |November 1, 2014 |DAILY BEAST 

"I verily believe they're gone to look at my button," cried Davy, beginning to laugh, in spite of his fears. Davy and The Goblin |Charles E. Carryl 

She looked up at him with sad and eloquent eyes, which softened his heart in spite of himself. Rosemary in Search of a Father |C. N. Williamson 

In spite of this, the garden studio was not wholly forsaken, and nearly every day she accomplished something there. Women in the fine arts, from the Seventh Century B.C. to the Twentieth Century A.D. |Clara Erskine Clement 

Hence their presence elsewhere, in spite of their passionate attachment to their free native hills. Glances at Europe |Horace Greeley 

Mindful of the military nature of the occasion he appeared in his old army overcoat, in spite of the heat. The Soldier of the Valley |Nelson Lloyd