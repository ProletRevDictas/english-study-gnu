There were three elements in the primers and probes, numbered one, two, and three. “We did the worst job in the world”: Lawrence Wright on America’s botched Covid-19 response |Sean Illing |February 9, 2021 |Vox 

In this video, they cover how to use beacons, probes, and shovels to help you understand how to implement these tools in an emergency. Avalanche-Gear Basics for Backcountry Skiing |Outside Editors |February 5, 2021 |Outside Online 

Say we found evidence of DNA on Mars—Perseverance has no way to sequence it, and as of yet there’s no way any Martian probe could be fitted with the necessary equipment to do so. We’re living in a golden age of sample return missions |Neel Patel |February 2, 2021 |MIT Technology Review 

The probes emphasized Metro’s need to create, update and reinforce safety standards — something the agency has pledged after the release of a September audit that included 21 safety failures or concerns within its Rail Operations Control Center. Metrorail system still has work to do on safety, board says |Justin George |January 27, 2021 |Washington Post 

Today it said it’s unpausing its multi-year probe to keep on prodding. UK resumes privacy oversight of adtech, warns platform audits are coming |Natasha Lomas |January 22, 2021 |TechCrunch 

However, the probe stayed in contact with the Rosetta orbiter and has already sent back some photos. Earthlings, We Landed on a Comet |Matthew R. Francis |November 12, 2014 |DAILY BEAST 

Then finally, the probe had to be released at the right moment and on the right course to land at that spot. Earthlings, We Landed on a Comet |Matthew R. Francis |November 12, 2014 |DAILY BEAST 

The probe appears to be sitting at the bottom of a "cliff" on the comet, but beyond that it's hard to tell. Earthlings, We Landed on a Comet |Matthew R. Francis |November 12, 2014 |DAILY BEAST 

UPDATE: Since publication yesterday, the Philae team learned that the probe bounced two times and apparently landed on its side. Earthlings, We Landed on a Comet |Matthew R. Francis |November 12, 2014 |DAILY BEAST 

Earthlings, we can celebrate the accomplishment of landing a probe on a new world. Earthlings, We Landed on a Comet |Matthew R. Francis |November 12, 2014 |DAILY BEAST 

It was risky work to leave the valuable convoy for an instant, but Malcolm felt that he must probe this mystery. The Red Year |Louis Tracy 

I have not been able to perceive a hole sufficient even to admit a small probe. Buffon's Natural History. Volume IX (of 10) |Georges Louis Leclerc de Buffon 

One by one they examine, they compare, they probe, and all in silence. The Diamond Coterie |Lawrence L. Lynch 

For a month she subjected Mr. Matthew Weyburn to the microscope of her observation and the probe of her instinct. Lord Ormont and his Aminta, Complete |George Meredith 

The slightest probe of such a feeling toward a man so infinitely beneath him, he would have felt degrading. There and Back |George MacDonald