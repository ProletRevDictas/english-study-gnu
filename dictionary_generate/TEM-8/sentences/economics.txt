Levitt talks with her about the early days of Google, how her background in economics shapes the company’s products, and why YouTube’s success has created a range of unforeseen and serious issues. “Hey, Let’s Go Buy YouTube!” | People I (Mostly) Admire Ep. 5: Susan Wojcicki |Steven D. Levitt |October 17, 2020 |Freakonomics 

This year’s winner for fresh thinking in economics, for example, went to Mariana Mazzucato, an economics professor at University College London, for “reimagining the role of the state and value in economics.” A Nobel Pursuit Gone Wrong? |Tracy Moran |October 11, 2020 |Ozy 

Still, Nobel’s gold medals have become an international symbol of unassailable excellence in the fields of physics, medicine, chemistry, economics, literature and the pursuit of peace. A Nobel Pursuit Gone Wrong? |Tracy Moran |October 11, 2020 |Ozy 

If the economics profession wants to respond in a more diverse manner, as many in the field have earnestly professed, one statistic to have come out of the pandemic gives cause for concern. Econ 3.0? What economists can contribute to (and learn from) the pandemic |Claire Beatty |September 28, 2020 |MIT Technology Review 

Its economics department includes someone you should all be familiar with, my Freakonomics friend and co-author. Speak Softly and Carry Big Data (Ep. 395) |Stephen J. Dubner |October 31, 2019 |Freakonomics 

That is decidedly not to say that politics and economics are irrelevant. No Gods, No Cops, No Masters |James Poulos |January 1, 2015 |DAILY BEAST 

Today many in the economics and urban planning professions consider such factors close to irrelevant. The Rustbelt Roars Back From the Dead |Joel Kotkin, Richey Piiparinen |December 7, 2014 |DAILY BEAST 

Even with my B.A. in English I can understand the economics involved: lots of cheap labor cheapens labor. The Liberal Case Against Illegal Immigration |Doug McIntyre |November 25, 2014 |DAILY BEAST 

It seems to me that we are dealing with more than bottom-line economics and bottom-squeezing ergonomics. Flying Coach Is the New Hell: How Airlines Engineer You Out of Room |Clive Irving |November 25, 2014 |DAILY BEAST 

The “stretched” cabins in new 737s and A320s transform their economics. Flying Coach Is the New Hell: How Airlines Engineer You Out of Room |Clive Irving |November 25, 2014 |DAILY BEAST 

The economics of war, therefore, has thrown its lurid light upon the economics of peace. The Unsolved Riddle of Social Justice |Stephen Leacock 

This, as a piece of pure economics, does not interest the individual employer a particle. The Unsolved Riddle of Social Justice |Stephen Leacock 

It is the one which is sometimes called in books on economics the case of an unique monopoly. The Unsolved Riddle of Social Justice |Stephen Leacock 

Professor Farnsworth, of the economics department, had invited me on a motor trip for the holidays. The Idyl of Twin Fires |Walter Prichard Eaton 

Potlatch is a wonderful day for children, a glorious introduction to the science of economics. The Great Potlatch Riots |Allen Kim Lang