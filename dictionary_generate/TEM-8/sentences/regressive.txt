Krishnan says the “regressive opposition” to his work “only strengthens my resolve.” No Dirty Dancing: India’s Classical Dancers Break Caste Taboos |Charu Kasturi |December 4, 2020 |Ozy 

Other concerns center on fuel taxes being regressive – having higher impacts on low-income communities. A Sixth Big Move for SANDAG’s New Transportation Vision |Joe Bettles |November 30, 2020 |Voice of San Diego 

This crisis is particularly regressive—it’s regressive within countries, with the poorest segments of the population being hit. Don’t expect a quick COVID recovery for the global economy |Erika Fry |October 27, 2020 |Fortune 

Many who remained felt that Lighthizer represented an old guard, with regressive views of how the economy should work. Robert Lighthizer Blew Up 60 Years of Trade Policy. Nobody Knows What Happens Next. |by Lydia DePillis |October 13, 2020 |ProPublica 

We certainly know that gambling on, for example, lottery tickets — highly regressive, especially scratch-off tickets. The Economics of Sports Gambling (Ep. 388 Rebroadcast) |Stephen J. Dubner |August 20, 2020 |Freakonomics 

A wave of critics, though, has been denouncing it as anti-democratic and regressive. God Save the United States From This Anti-Democratic Court |Jedediah Purdy |June 22, 2014 |DAILY BEAST 

Nipple also posted the rather regressive question “Did gender equality go to far?” Why Tracking Hookups and Orgasms Like Footsteps and Calories May Not Provide You with Personal Insight |Emily Shire |March 27, 2014 |DAILY BEAST 

The media, much of it in the hands of regressive establishment figures, eagerly cheered them on. Egypt: Al Jazeera Had ‘Marriott Cell’ Of Terror |Alastair Beach |February 6, 2014 |DAILY BEAST 

Finally, while sales taxes are regressive, you can make a pretty good case that taxes on e-commerce are progressive. Three Cheers for the Internet Tax! |Daniel Gross |April 24, 2013 |DAILY BEAST 

But he has quietly gone from being an outlier to being only one of five consistently regressive votes. Impeach the Supreme Court Justices If They Overturn Health-Care Law |David R. Dow |April 3, 2012 |DAILY BEAST 

To advocate a moral standard higher for women than for men is regarded now as reactionary and regressive. Feminism and Sex-Extinction |Arabella Kenealy 

Formal decay begins when a language becomes historical, and it often gives rise to remarkable cases of regressive metamorphosis. Basque Legends |Wentworth Webster 

The two limits are then called the “progressive” and “regressive” differential coefficients. Encyclopaedia Britannica, 11th Edition, Volume 14, Slice 5 |Various 

This influence, a form of regressive assimilation, is known as i-umlaut (pronounced om-lowt). Anglo-Saxon Grammar and Exercise Book |C. Alphonso Smith 

Dream symbols are the most easily demonstrable, and after them, certain peculiarities of regressive dream representations. A General Introduction to Psychoanalysis |Sigmund Freud