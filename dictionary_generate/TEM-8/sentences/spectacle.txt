This is not high production value YouTube, or YouTube driven by spectacle or personality. The Soothing Bento YouTube Videos That Will Inspire You to Get Back in the Kitchen |Meghan McCarron |January 15, 2021 |Eater 

More meta-comedy than action spectacle, it’s the rare superhero story that could potentially appeal to viewers, like me, whose eyes glaze over when battle scenes run longer than a few minutes. WandaVision Deserves the Hype. But It Penalizes Casual Marvel Viewers With Confusion |Judy Berman |January 14, 2021 |Time 

To be sure, the football spectacle changed to accommodate the realities of the war. This isn’t the first time Army played Navy in a moment of crisis |Randy Roberts |December 11, 2020 |Washington Post 

Wilkie speculated in an email that Takano was “laying the grounds for a spectacle.” Watchdog finds VA Secretary Robert Wilkie questioned the credibility of a House aide who reported a sexual assault at hospital |Lisa Rein |December 11, 2020 |Washington Post 

Signing now would help him avoid a spectacle this season and focus on basketball and his family, which have been his priorities throughout his career. The Bucks bungled free agency. Now they wait for Giannis Antetokounmpo to decide. |Ben Golliver |November 23, 2020 |Washington Post 

Even by the already money-drenched standards of American politics, the Eldridge campaign was a jaw-dropping spectacle to behold. The Rise and Fall of Chris Hughes and Sean Eldridge, America’s Worst Gay Power Couple |James Kirchick |December 9, 2014 |DAILY BEAST 

In 1881, along came Bailey, operator of another circus, and two circuses joined to give rise to the first three-ring spectacle. We’re All Carnies Now: Why We Can’t Quit the Circus |Anthony Paletta |November 27, 2014 |DAILY BEAST 

Had they been in the West Bank, the spectacle would hardly have attracted notice. Intifada 3.0: Growing Unrest and a Plot to Kill an Israeli Minister |Creede Newton |November 21, 2014 |DAILY BEAST 

The plot of the film runs secondary to the spectacle, and is denser than a TED conference. ‘Interstellar’ Is Wildly Ambitious, Very Flawed, and Absolutely Worth Seeing |Marlow Stern |November 7, 2014 |DAILY BEAST 

Today, the quaint spectacle of a stage-managed fairy-tale celebration strikes many of us as a load of garbage. What Republicans Need Right Now Is a Good Internal Fight |James Poulos |November 6, 2014 |DAILY BEAST 

In the evening, St. Peter's and its accessories were illuminated—by far the most brilliant spectacle I ever saw. Glances at Europe |Horace Greeley 

Thus all about us is the moving and shifting spectacle of riches and poverty, side by side, inextricable. The Unsolved Riddle of Social Justice |Stephen Leacock 

Children, like uneducated adults, have been known to take a spectacle on the stage of a theatre too seriously. Children's Ways |James Sully 

No one has ever seen so strange a spectacle and I very much doubt if any one will ever see it again. Gallipoli Diary, Volume I |Ian Hamilton 

As pointed out above, the action in a child's play is not intended as a dramatic spectacle. Children's Ways |James Sully