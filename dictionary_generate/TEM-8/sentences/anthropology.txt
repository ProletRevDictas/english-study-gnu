Her field is very new, this combination of biochemistry and cultural anthropology. Dark spaces on the map |Katie McLean |December 18, 2020 |MIT Technology Review 

Machine learning researchers should stop ducking responsibility by claiming such considerations belong to other fields—data science or anthropology or political science. A.I. needs to get real—and other takeaways from this year’s NeurIPS |Jeremy Kahn |December 15, 2020 |Fortune 

So, those fields plus computer science — with their interest in artificial intelligence — and anthropology, all kind of converge in this one interdisciplinary field, which wants to use different methods and approaches to tackle the questions of mind. Forget Everything You Know About Your Dog (Ep. 436) |Stephen J. Dubner |October 22, 2020 |Freakonomics 

Tyler Faith, an archeology and anthropology expert at the University of Utah who was not involved in the study, says this idea could possibly extend to other species that met their ends as the ice age melted away. Climate change probably contributed to the woolly rhino’s rapid demise |Sara Kiley Watson |August 25, 2020 |Popular-Science 

To do anthropology well, you had to alienate yourself from everything familiar, to take a slow train toward another way of seeing reality. Gender Is What You Make of It - Issue 88: Love & Sex |Charles King |August 5, 2020 |Nautilus 

But I jumped off the science track early, and took only one class in anthropology in college. The Real-Life Raiders of the Lost Ark |Alex Belth |November 14, 2014 |DAILY BEAST 

Richard Kurin was a 19-year-old anthropology student in India when he experienced his material culture epiphany. We All Have a Rosebud in Our Pasts |Sam Roberts |October 15, 2014 |DAILY BEAST 

Hobbes lacked the data of archaeology and anthropology to inform his theories about the dangerous nature of pre-state existence. War! What Is It Good For? A Lot |Nick Romeo |August 13, 2014 |DAILY BEAST 

Beyond Baghdad the line drawn between Syria, now the property of France, and Iraq was more cartography than anthropology. Gertrude of Arabia, the Woman Who Invented Iraq |Clive Irving |June 17, 2014 |DAILY BEAST 

“He is in a row,” says Erin Kimmerle, an associate professor of anthropology with USF, who is leading the excavation. The Bone Collectors Get to Work at Florida’s Dozier School |Christine Pelisek |September 6, 2013 |DAILY BEAST 

Boulanger's ideas on philosophy, mythology, anthropology and history are of extraordinary interest today. Baron d'Holbach |Max Pearson Cushing 

But famous as he was as a surgeon, his name is associated most closely with the modern school of anthropology. Encyclopaedia Britannica, 11th Edition, Volume 4, Part 3 |Various 

It was a work of observation, insight, and genius, and was a landmark in the progress of the science of anthropology. The New Stone Age in Northern Europe |John M. Tyler 

The study of prehistoric anthropology in Russia, a vast territory, is still in its infancy. The New Stone Age in Northern Europe |John M. Tyler 

Among these are several on anthropology, on political economy, and even on Darwinism. The History of Yiddish Literature in the Nineteenth Century |Leo Wiener