The final word on cordless vacuumsThere’s no doubt that getting rid of wires and power sockets makes life a lot easier, and these days, the best cordless vacuum cleaners have suction power that easily compares to their plugin-powered counterparts. Best cordless vacuum: Suck up debris without getting yourself tangled |Charlotte Marcus |January 19, 2021 |Popular-Science 

It is more like a USB socket, with both communication and power capabilities. What to know before you buy an electric vehicle |Dan Carney |January 8, 2021 |Popular-Science 

Electric vehicles are not appliancesWe’ve come to expect that we plug electric devices into a socket and they just work. What to know before you buy an electric vehicle |Dan Carney |January 8, 2021 |Popular-Science 

In August, one cable that supported this dome slipped out of a socket. Star of science and movies, the Arecibo radio telescope is dead |Lisa Grossman |December 9, 2020 |Science News For Students 

If you’ve ever traveled to South Africa and tried to use your multi-country adapter to recharge your phone or laptop, you may have been surprised that your adapter could not fit into the country’s unique sockets. South Africa’s unusual electrical plugs and sockets are headed for retirement |Brian Browdie |October 17, 2020 |Quartz 

It turns out that a nail clipper, divided into two halves and hooked up directly into a power socket will boil water. Tales of a Jailhouse Gourmet: How I learned to Cook in Prison |Daniel Genis |June 21, 2014 |DAILY BEAST 

In 1996, Smart was severely beaten in prison by two inmates, who broke her eye socket and left her with a metal plate in her head. Did Sexpot Schoolteacher Pamela Smart Hire Teens to Kill Her Husband? |Marlow Stern |January 19, 2014 |DAILY BEAST 

During a trip to Dallas for Super Bowl 2011 festivities, she writes that Russell “knocked her jaw out of the socket.” Real Housewife Taylor Armstrong Tells All. Or Does She? |Diane Dimond |February 7, 2012 |DAILY BEAST 

When it was over, Keith had been stabbed in the shoulder, and Brandon had a fractured eye socket and orbital wall. Arnold Pardoned My Son’s Attacker |Bruce Henderson |May 11, 2011 |DAILY BEAST 

Fixed to the tumbler inboard there is a small bar which fits into a socket attached to the covering board. Yachting Vol. 2 |Various. 

Pour water in an earthenware jar, place the plates in it and turn the plug in a lamp socket. The Boy Mechanic, Book 2 |Various 

The candle was burning to the socket; the moonlight lay on the floor between them, in a shifting, widening patch. The Rake's Progress |Marjorie Bowen 

In testing a fixture, the plug A is turned into a socket of some source of current, and a lamp is turned into the socket B. The Boy Mechanic, Book 2 |Various 

After attaching the socket to the wall with screws the board was easily put in place as shown in Fig. 2. The Boy Mechanic, Book 2 |Various