The visionaries, makers and investors building the future and changing the way we move everything around the world. Last day to save on passes to TC Sessions: Mobility 2020 |Alexandra Ames |September 11, 2020 |TechCrunch 

Because there is a whole sisterhood of women who are ambitious, visionaries, and who recognize the change that we need to see in the world. Book recommendations from Fortune’s 40 under 40 in government and policy |Rachel King |September 10, 2020 |Fortune 

Fortune published its latest 40 Under 40 list of the innovators, leaders, and otherwise commendable visionaries this week. The ‘under 40’ crowd leading revolutions in health care |Sy Mukherjee |September 3, 2020 |Fortune 

He lacks Edison’s business acumen and penchant for self-promotion, but is armed with a visionary idea and relentless ambition. Ethan Hawke stars in ‘Tesla,’ a quirky biopic about the iconic inventor |Maria Temming |August 19, 2020 |Science News 

When the generations that come after us, our charge is that we will be enough, and be visionary enough, for them to be proud of what we’ve done. Morning Report: Report Finds Evidence of Fraud in Sweetwater Budget Mess |Voice of San Diego |June 23, 2020 |Voice of San Diego 

They double down on the plot device of a lone visionary opposed by conventional hierarchies. Why Can’t Movies Capture Genius? |Clive Irving |December 14, 2014 |DAILY BEAST 

They were jeered for their efforts, but today they look visionary. How Antioch College Got Rape Right 20 Years Ago |Nicolaus Mills |December 10, 2014 |DAILY BEAST 

Yet, the ever-visionary Van Gogh still feels the possibility of acclaim after his imminent death. Decoding Vincent Van Gogh’s Tempestuous, Fragile Mind |Nick Mafi |December 7, 2014 |DAILY BEAST 

Ken Russell, the now sadly deceased British film director, told me he considered Jarman a visionary. The Queer Genius of Film Director Derek Jarman |Tim Teeman |November 1, 2014 |DAILY BEAST 

I wanted to talk about your career as well, since you were honored here with the Persol Tribute to Visionary Talent Award. Frances McDormand on 'Olive Kitteridge,' Dropping LSD, and Her Beef With FX's 'Fargo' |Marlow Stern |September 3, 2014 |DAILY BEAST 

The prophets had long been painting the visionary dawn with pigments of that glorious sunset. Solomon and Solomonic Literature |Moncure Daniel Conway 

The danger which seemed so terrible to many honest friends of liberty he did not venture to pronounce altogether visionary. The History of England from the Accession of James II. |Thomas Babington Macaulay 

This visionary was in reality a philosopher, that is to say, an experimenter and a manipulator of general ideas. Repertory Of The Comedie Humaine, Complete, A -- Z |Anatole Cerfberr and Jules Franois Christophe 

It is important, first of all, to note that this power of the visionary could not be put directly into play. Repertory Of The Comedie Humaine, Complete, A -- Z |Anatole Cerfberr and Jules Franois Christophe 

The man who brought these calamities on his country was not a mere visionary or a mere swindler. The History of England from the Accession of James II. |Thomas Babington Macaulay