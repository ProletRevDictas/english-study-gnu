Annahita Nezami, a London psychologist, did her doctorate on the therapeutic value of the Overview Effect. Can Astronaut Thinking Heal America at Last? |Leslie dela Vega |January 28, 2021 |Ozy 

Darden transferred to NASA’s male-dominated engineering division and later earned an engineering doctorate. The NASA Engineer Who’s a Mathematician at Heart |Susan D'Agostino |January 19, 2021 |Quanta Magazine 

I, and many of my colleagues, have a master’s degree, and also a doctorate. COVID-19 testing scientists are the unsung heroes of the pandemic |By Rodney E. Rohde/The Conversation |December 16, 2020 |Popular-Science 

When they’re a bit drunk one night in December, Harper, a journalist, asks Abby, who’s getting a doctorate in art history, to go home with her to meet her family over Christmas. Hulu’s ‘Happiest Season’ provides queer holiday cheer |Kathi Wolfe |December 11, 2020 |Washington Blade 

Judy, who has led the district since 2014, rose from a life sciences teacher to a principal to Emanuel’s superintendent, earning a doctorate in educational leadership and administration. Two School Districts Had Different Mask Policies. Only One Had a Teacher on a Ventilator. |by Annie Waldman and Heather Vogell |November 23, 2020 |ProPublica 

The “doctorate” Duke claims is from an anti-Semitic Ukranian “diploma mill” as described by the State Department. The Price of Steve Scalise’s Silence |Jason Berry |January 7, 2015 |DAILY BEAST 

Dr. Grenci obtained her doctorate in clinical sexology in 2007. Whip It: Secrets of a Dominatrix |Justin Jones |November 25, 2014 |DAILY BEAST 

Now Hunter wanted to confer an honorary doctorate on me, and I needed to find the words to properly convey how honored I felt. A College Degree Worth the Wait |Eleanor Clift |June 1, 2014 |DAILY BEAST 

One old soldier is on the path to a doctorate in mathematics and another is a trained underwater welder. Memorial Days After Mourning Has Passed |Alex Horton |May 25, 2014 |DAILY BEAST 

Four bronze stars later, he earned a chemistry doctorate and joined the FBI. Crime Fighter’s Dilemma: My Country or My Family? |Moral Courage |April 21, 2014 |DAILY BEAST 

Copernicus made his medical studies in Ferrara and Padua, and obtained his doctorate with honors from Ferrara. Catholic Churchmen in Science |James J. Walsh 

He studied at the École Polytechnique and at the École des Mines, and later received his doctorate in mathematics in 1879. The Foundations of Science: Science and Hypothesis, The Value of Science, Science and Method |Henri Poincar 

She had already earned her doctorate, while I was still struggling with the tag ends of my thesis. Card Trick |Walter Bupp AKA Randall Garrett 

A brilliant mathematician, he had taken his doctorate without difficulty, and his thesis had even attracted some attention. Doctor Claudius, A True Story |F. Marion Crawford 

He might obtain the doctorate in both branches of law in ten years (Rashdall i. 221-222). Encyclopaedia Britannica, 11th Edition, Volume 10, Slice 1 |Various