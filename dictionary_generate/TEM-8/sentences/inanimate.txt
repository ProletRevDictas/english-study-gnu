Having written for Saturday Night Live, Torres’ signature style is deadpan humor, whether in stand-up or while inventing lifelike stories for inanimate objects. 18 Comics of Tomorrow |Sohini Das Gupta |August 1, 2021 |Ozy 

I hate to personify an inanimate object, but I think we owe the sandwich a debt of gratitude. The 25 best sandwiches in the D.C. area |Tim Carman |June 16, 2021 |Washington Post 

Some biologists think of them as quasi-alive, while others refer to them as inanimate tiny particles or arrangements of matter. The Vast Viral World: What We Know (and Don’t Know) - Issue 99: Universality |Lauren E. Oakes |April 7, 2021 |Nautilus 

That perspective considers all forms of matter, both animate and inanimate. Is Life Special Just Because It’s Rare? - Issue 95: Escape |Alan Lightman |January 20, 2021 |Nautilus 

While inanimate matter doesn’t evolve like animate matter, inanimate matter does behave. Electrons May Very Well Be Conscious - Issue 94: Evolving |Tam Hunt |December 30, 2020 |Nautilus 

He sits in dark corners of the narrative, a bit inanimate, like a broken chair marring a finely furnished room. Can Joyce Carol Oates Write a War Novel? |Elliot Ackerman |January 30, 2014 |DAILY BEAST 

But it is important to recall that even inanimate objects contain stories. How WWII Soldiers Saved Italy’s Art From the Nazis |Noah Charney |May 25, 2013 |DAILY BEAST 

She has been reduced to porter, to Sherpa, to something even less–some inanimate bit of set decoration. The Language of Margaret Thatcher’s Handbags |Robin Givhan |April 8, 2013 |DAILY BEAST 

“Thank God for my computer,” says the star, who often films personal “video diaries” with her inanimate friend. Six Things We Learned From the Beyoncé Documentary |Anna Klassen |February 16, 2013 |DAILY BEAST 

Firearms, after all, are inanimate objects, incapable of inflicting harm on their own initiative. Angry Gun-Control Debate Does Damage to Both the Right and the Left |Michael Medved |January 23, 2013 |DAILY BEAST 

And then the whole meaning—or the lack of meaning—of their inanimate lives was revealed to him. The Joyous Adventures of Aristide Pujol |William J. Locke 

As the Mother of Sorrows she will weep over His inanimate body taken down from the cross. Mary, Help of Christians |Various 

Imagine his agony at the sight of his mother,—pale, inanimate, and from time to time writhing under a convulsive chill. The Seven Cardinal Sins: Envy and Indolence |Eugne Sue 

How a human touch colors the inanimate world with the communicated warmth of its enchantment! The Woman Gives |Owen Johnson 

The homelessness of men, and even of inanimate vessels, cast away upon strange shores, came strongly in upon my mind. The Works of Robert Louis Stevenson, Volume XXI |Robert Louis Stevenson