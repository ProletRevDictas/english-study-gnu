It’s not just kids whose needs have fueled economic spending in this era of tumult. Beyond GameStop: Are You Ready for an Economic Moonshot? |Charu Kasturi |January 31, 2021 |Ozy 

Notwithstanding the recent tumult, we remain one country, not two. Madeleine Albright: 'Us vs. Them' Thinking Is Tearing America Apart. But Here's Why I'm Still Hopeful About the Future |Madeleine Albright |January 15, 2021 |Time 

Newspaper headlines cut to the heart of the tumult in sharp banner headlines. Save the Sarcasm for Other Democracies. America Is Fine |Debasish Roy Chowdhury |January 11, 2021 |Time 

It’s a reality at the center of fresh tumult in the food media world. Indian Americans: The New Voices Bringing Diversity to Food Writing |Shaan Merchant |January 7, 2021 |Ozy 

Now O’Meara finds herself staving off not just friend requests but also a tumult of inquiries from people wanting to riff on “And the People Stayed Home.” The story behind ‘And the People Stayed Home,’ the little poem that became so much more |Nora Krug |December 10, 2020 |Washington Post 

The tumult was such that young Sarah had cause to worry that she might not get even a glimpse of Will and Kate. Synagogue Slay: When Cops Have to Kill |Michael Daly |December 10, 2014 |DAILY BEAST 

Could it be that after holding on to very relative stability during three years of regional tumult, Lebanon now faced all-out war? Beirut Letter: In Lebanon, Fighting ISIS With Culture and Satire |Kim Ghattas |September 22, 2014 |DAILY BEAST 

Amid some media tumult, the first President Bush had to come out and say in essence, hey, kidding. Dick Cheney’s Awfulness Is Here to Stay |Michael Tomasky |July 15, 2014 |DAILY BEAST 

Jordan also became famous off the court, both for his gambling and for tumult in his personal life. Speed Read: The Juiciest Bits of a New Michael Jordan Biography |William O’Connor |May 6, 2014 |DAILY BEAST 

He was, however, also caught up in the tumult of his ailing marriage to Ava Gardner. The Week in Death: George Jacobs, Sinatra’s Domestic Confidant |The Telegraph |February 23, 2014 |DAILY BEAST 

Call ye the name of Pharao king of Egypt, a tumult time hath brought. The Bible, Douay-Rheims Version |Various 

And from all sides in wild confusion flewThe dust and leaves, the branches and the stones,With hideous tumult, inconceivable. The Poems of Giacomo Leopardi |Giacomo Leopardi 

At the end of the opera the Emperors portrait was brought on the stage, and an indescribable tumult followed. The Life &amp; Letters of Peter Ilich Tchaikovsky |Modeste Tchaikovsky 

At that moment she had noticed the change in the man she had so gradually grown to love, and her heart was beating in wild tumult. The Doctor of Pimlico |William Le Queux 

His object in making such a tumult around the boat was evidently to learn whether the men on board were asleep. Motor Matt's "Century" Run |Stanley R. Matthews