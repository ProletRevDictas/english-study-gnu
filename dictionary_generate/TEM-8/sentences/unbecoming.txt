A faculty committee deemed his use of the Fifth Amendment “conduct unbecoming an institute professor” and said it showed a lack of candor on his part. How an MIT Marxist weathered the Red Scare |Simson Garfinkel ’87, PhD ’05 |June 29, 2022 |MIT Technology Review 

Fits of rage may be more common during the holidays, but they are no less unbecoming. How to Make It Through Thanksgiving Alive |Lizzie Crocker |November 26, 2014 |DAILY BEAST 

Pounding is charged with one count each of assault, adultery, and conduct unbecoming an officer. Commando Colonel Accused of Exposing his Lover to HIV |Jacob Siegel |November 19, 2014 |DAILY BEAST 

To them, Her Imperial Majesty and Queen Empress was behaving in a manner unbecoming. Queen Victoria’s Secret Scottish Sex Castle |Clive Irving |August 17, 2014 |DAILY BEAST 

Chasing after mere favorability and openness is an unbecoming act of desperation. The House GOP’s Big Immigration Fail |James Poulos |June 5, 2014 |DAILY BEAST 

Eventually though, Wiles was suspended for "conduct unbecoming to a Promoter of the Miss Gay America pageant system." From Drag Queen To GOP Candidate |Ben Jacobs |May 5, 2014 |DAILY BEAST 

Swearing, as I have said, was not considered low or vulgar or unbecoming a gentleman. Fifty Years of Railway Life in England, Scotland and Ireland |Joseph Tatlow 

To come in, flushed from a hurried toilette, to meet your first callers, is unbecoming as well as rude. The Ladies' Book of Etiquette, and Manual of Politeness |Florence Hartley 

Choosing to die nobly rather than to fall into the hands of the wicked, and to suffer abuses unbecoming his noble birth. The Bible, Douay-Rheims Version |Various 

It is full of crazy ideas that are most unbecoming in a young girl, and I dont consider such things proper for you to read. Those Dale Girls |Frank Weston Carruth 

Yes, I knowconduct unbecoming an officer and a gentlemana plain case, too, it seems to me. A Horse's Tale |Mark Twain