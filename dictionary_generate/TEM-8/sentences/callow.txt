That was what I sought when, as a callow and hormonal high school student, I first discovered Lawrence. A case for D.H. Lawrence as a father of modern travel writing |Walter Nicklin |September 2, 2021 |Washington Post 

Anyone going through Prozac Nation can certainly find plenty of callow moments when Wurtzel does whine. Thank You, Elizabeth Wurtzel: ‘Prozac Nation’ Turns 20 |Nicolaus Mills |July 31, 2014 |DAILY BEAST 

But now that veneer is gone, and what remains is a callow man-child at odds with himself. What's Happened to Don Draper? Why Everyone’s Favorite ‘Mad Men’ Stud Needs His Mojo Back |Lizzie Crocker |April 16, 2014 |DAILY BEAST 

This is clearly not a boast; it seems, rather, a shamed admission of petty, callow cruelty. In Defense of Jonathan Franzen |Michelle Goldberg |September 26, 2013 |DAILY BEAST 

The last thing we should do is help the callow—and dangerous—Kim continue his rule. North Korea’s Dangerous Hostage Game |Gordon G. Chang |May 3, 2013 |DAILY BEAST 

In this callow atmosphere, Brooke Astor would never be an icon, but she is remembered with nostalgia. Brooke Astor’s Estate Is Auctioned, and a Friend Recalls Her Fondly |Barbara Goldsmith |September 29, 2012 |DAILY BEAST 

There lay the callow brood marked out by Nature and man, for her ministrations. The Daughters of Danaus |Mona Caird 

Next morning early they sent in their 'callow' verses to the great man, and followed shortly themselves. The Age of Erasmus |P. S. Allen 

When he was haled into court, despite his callow years, he came with insolent confidence, as one above the law. The Code of the Mountains |Charles Neville Buck 

"I wonder the rebels had the courage to pursue you," said a very callow youth named Graves. In Hostile Red |Joseph Altsheler 

As I looked at the two callow things in the grass, a dismay and weak helplessness quite overcame me. Roof and Meadow |Dallas Lore Sharp