The McLaren is a low-slung angular spaceship packed with power. Anyone can drive a supercar, but truly tapping its potential is another matter |Rob Verger |July 28, 2021 |Popular-Science 

His angular, expressive face and bald head simultaneously project wit, wisdom, curiosity, delight, and dead seriousness. In the World of Ultralight Hiking, Everything Weighs Something |jversteegh |July 14, 2021 |Outside Online 

His design for Jurong Town Hall, which now houses Singapore’s Trade Association Hub, is angular and sleek, a fitting design for a key engine of the country’s modern economy. A legacy of landmark buildings in Malaysia and Singapore |Kara Baskin |June 30, 2021 |MIT Technology Review 

The placements were “rounded and organic,” a massive departure from conventional chip designs with angular edges and sharp corners. A Google AI Designed a Computer Chip as Well as a Human Engineer—But Much Faster |Shelly Fan |June 15, 2021 |Singularity Hub 

Since the pandemic, he’s embraced his more angular, experimental side in collaborations with FKA Twigs and Headie One. The Best Albums of 2021 So Far |Raisa Bruner |May 27, 2021 |Time 

You, too, will be zipping along to the angular guitars and zigzagging, herky-jerky vocals. The 10 Best Albums of 2014: Taylor Swift, Sia, Run the Jewels, and More |Marlow Stern |December 28, 2014 |DAILY BEAST 

Traditional coach seats gave the illusion of comfortable padding but were angular, not reflecting body shapes. Flying Coach Is the New Hell: How Airlines Engineer You Out of Room |Clive Irving |November 25, 2014 |DAILY BEAST 

He has a long white beard and a massive angular nose that could be the work of Gutzon Borglum. Heart of Darkness: Into Afghanistan’s Taliban Valley |Matt Trevithick, Daniel Seckman |November 15, 2014 |DAILY BEAST 

As she emerged from her teen years, she developed an angular face and striking cheekbones. The Famous Parents Modeling Club |Erin Cunningham |May 28, 2014 |DAILY BEAST 

His angular cheeks, thick glasses, and carefully combed hair incarnate elegance, vision, and, unfortunately, personal agony. The Making of Fashion Legend Yves Saint Laurent |Sarah Moroz |January 14, 2014 |DAILY BEAST 

He was tall, angular, and emaciated, and his features were cast in a most irregular mould. The Portsmouth Road and Its Tributaries |Charles G. Harper 

Conglomerate, containing angular fragments of yellowish-grey quartz-rock, in a base of compact epidote. Narrative of a Survey of the Intertropical and Western Coasts of Australia] [Volume 2 of 2] |Phillip Parker King 

Angular and bony, with slightly stooped shoulders, his face is a mass of minute wrinkles seamed on yellow parchment. Prison Memoirs of an Anarchist |Alexander Berkman 

Angular debris fallen from above varying in thickness from one to ten feet. Encyclopaedia Britannica, 11th Edition, Volume 4, Part 3 |Various 

He had not taken the precaution to remove from his couch a number of angular stones, which did not by any means conduce to sleep. Toilers of the Sea |Victor Hugo