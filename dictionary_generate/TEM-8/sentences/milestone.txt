The oil and gas industry just passed another milestone in the global energy transition. As Exxon exits the Dow, technology takes over the major stock indices |Michael J. Coren |August 25, 2020 |Quartz 

While barocaloric materials are less susceptible to fatigue than elastocaloric materials, the new milestones required colossal pressures of thousands of atmospheres. The Shape-Shifting Squeeze Coolers |Marcus Woo |August 24, 2020 |Quanta Magazine 

During the last quarter, The New York Times reported the same milestone. ‘It’s less dire than it seemed to be’: How The Wall Street Journal’s digital ads business has weathered the downturn |Lucinda Southern |August 20, 2020 |Digiday 

More than 4,800 alumni and friends gathered online May 29–31 to celebrate class milestones, explore alumni achievements and faculty research, compete in a trivia face-off, and more. Take me back to Tech |Tate Ryan-Mosley |August 19, 2020 |MIT Technology Review 

The market spent the past few days within striking distance of a new high, but falling short of the milestone, until the last minutes of trading Tuesday. S&P 500 hits a new record, erasing last of pandemic losses |Verne Kopytoff |August 18, 2020 |Fortune 

That would truly be a milestone to celebrate—until you see what that record “diversity” actually means. The Unbearable Whiteness of Congress |Dean Obeidallah |January 8, 2015 |DAILY BEAST 

A multi-faceted approach to ensuring students reach the critical milestone of grade level reading by fourth grade. What It Takes to Fix American Education |Jonah Edelman |November 23, 2014 |DAILY BEAST 

The regal technological milestone was passed as the Queen attended the opening of  a new gallery at the Science Museum in London. One Has Tweeted! |Tom Sykes |October 24, 2014 |DAILY BEAST 

In fact, with my most recent birthday, I passed the milestone of having come out of the closet over half my lifetime ago. Doctors Are Failing Their Gay Patients |Russell Saunders |September 27, 2014 |DAILY BEAST 

The moment he was finally able to loop a knot by himself was a milestone, his first step to becoming a man. Miami’s Chris Bosh Goes High Fashion |Justin Jones |August 13, 2014 |DAILY BEAST 

The force of such idealization helped to carry forward the human race to a new milestone on the path of progress. The Unsolved Riddle of Social Justice |Stephen Leacock 

In 1771 a Roman milestone of the time of Hadrian (76-138) was discovered at a spot two miles from Leicester. The Towns of Roman Britain |James Oliver Bevan 

He saw my look and said, "Oops, I mean this milestone in paper technology once it is announced to the world." The Professional Approach |Charles Leonard Harness 

And I ask myself again, Is75 this a new phase of life into which I have entered,—a new milestone left behind? The Wasted Generation |Owen Johnson 

This has been interpreted as the inscription of a certain Notus; but others have regarded it as simply a Roman milestone. The Cornwall Coast |Arthur L. Salmon