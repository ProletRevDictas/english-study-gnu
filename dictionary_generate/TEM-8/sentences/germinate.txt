Most are edible and germinate in the conventional way, starting life as a seedling and growing upward. The Incredible Fig - Issue 104: Harmony |Meg Lowman |July 28, 2021 |Nautilus 

They’ll germinate in roughly two weeks, with the first harvest six weeks later. Grow your own indoor garden with this deal on a hydroponic system |Quinn Gawronski |July 9, 2021 |Popular-Science 

One of those seeds gets carried across an ocean to a new, unvegetated continent where it germinates and becomes the founder species for plant life. A Wrinkle in Nature Could Lead to Alien Life - Issue 99: Universality |Caleb Scharf |April 21, 2021 |Nautilus 

Oreskes describes how major science advances germinated and weaves those accounts with deeply researched stories of backstabbing colleagues, attempted coups at oceanographic institutions and daring deep-sea adventures. A new book explores how military funding shaped the science of oceanography |Alka Tripathy-Lang |April 16, 2021 |Science News 

The post-Harden Rockets exist as a shell-encased seed, hurt by injury but ready to germinate. The Post-Harden Rockets Have A Different Style — And Lots Of Possibilities |Louis Zatzman |February 19, 2021 |FiveThirtyEight 

Texas may be a testing ground, but it is in Silicon Valley that ideas germinate and incubate. The GOP’s Huge, Growing Modernity Gap |Lloyd Green |June 9, 2013 |DAILY BEAST 

But without a reasonable expectation that security will materialize, better governance will not germinate. It's All or Nothing in Afghanistan |Vanda Felbab-Brown |October 11, 2009 |DAILY BEAST 

That sent to Sind, though said to have been carefully sown, also failed to germinate. Tobacco; Its History, Varieties, Culture, Manufacture and Commerce |E. R. Billings. 

More thinking, and a greater experience of life, may cause him to germinate agreeably in a few years. Our Churches and Chapels |Atticus 

Does anyone know for sure how to get pawpaw seed to germinate? Northern Nut Growers Association Thirty-Fourth Annual Report 1943 |Various 

This is a seed of such force and vitality, that it does not ask our leave to germinate. A Plea for Captain John Brown |Henry David Thoreau 

The spores of a heartwood-inhabiting fungus cannot germinate and thrive unless they fall upon the heartwood of the tree. Our National Forests |Richard H. Douai Boerker