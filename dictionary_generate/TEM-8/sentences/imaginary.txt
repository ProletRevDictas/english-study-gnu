Before the pandemic, every time I left the house, I put on an imaginary “mask” so I could interact with people. I’m autistic. I’m hoping I can wear a mask for the rest of my life. |Christine M. Condo |February 2, 2021 |Washington Post 

To the child, although they know that it is an imaginary being, the friend apparently has a mind of its own—sometimes saying it’s too busy to play, for example. Can You Treat Loneliness By Creating an Imaginary Friend? - Facts So Romantic |Jim Davies |January 15, 2021 |Nautilus 

After turning around and flexing, Beal locked eyes with Russell Westbrook on the sideline, and both players cradled an imaginary bundle in their arms — rock the baby. At last, Wizards get their first win by cruising to a rout of the Timberwolves |Ava Wallace |January 2, 2021 |Washington Post 

It’s the imaginary line that goes longways through the football. Researchers reveal the secret to the perfect football throw |Harini Barath |December 15, 2020 |Science News For Students 

That’s why he won, not because of some imaginary manufacturing of votes in Philadelphia. Texas has managed the near-impossible: Its presidential election lawsuit just got worse |Philip Bump |December 11, 2020 |Washington Post 

I have an imaginary place, and these are the people who live there. Tony Earley's Imaginary Friends |Mindy Farabee |September 2, 2014 |DAILY BEAST 

Did you make up any imaginary friends while you were in the caves? Scott Haze on Playing a Necrophiliac in ‘Child of God’ and Naked Paintballing with James Franco |Melissa Leon |August 3, 2014 |DAILY BEAST 

Pretending and imaginative play also flourish, and imaginary friends are common companions to young schoolchildren. Diagnosing Jane, Louis C.K.’s Troubled Daughter on ‘Louie’ Who Can’t Separate Dreams From Reality |Russell Saunders |May 15, 2014 |DAILY BEAST 

It could be said, however, that the imaginary slide show monologue begat the very real novel. The Climax of ‘Portnoy’s Complaint’ |Robert Hofler |May 14, 2014 |DAILY BEAST 

Having celebrities is like having imaginary friends, which, come to think of it, most of our friends are. Welcome to Showbiz Sharia Law |P. J. O’Rourke |May 4, 2014 |DAILY BEAST 

He was continually presenting innumerable imaginary fivers to little people. The Pit Town Coronet, Volume I (of 3) |Charles James Wills 

And then at last the man himself took to forgetting the imaginary writer and poured out words of love, warm, true, and passionate. The Joyous Adventures of Aristide Pujol |William J. Locke 

It is the fate of all authors or chroniclers to create imaginary friends, and lose them in the course of art. The Posthumous Papers of the Pickwick Club, v. 2(of 2) |Charles Dickens 

He believed in demons, spirits, and dragons, and in nearly every house were idols in honour of the imaginary deities. Our Little Korean Cousin |H. Lee M. Pike 

These stories represent early traditions and may easily be true, though they may be merely imaginary. King Robert the Bruce |A. F. Murison