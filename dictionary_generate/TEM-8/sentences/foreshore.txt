A rapier and a dagger found on the Thames foreshore show us that swordfights routinely broke out on the streets of London. This Week’s Hot Reads: Sept. 30, 2013 |Thomas Flynn, Jimmy So |September 30, 2013 |DAILY BEAST 

Hotter, more desolate than ever, lay that black griddle of the foreshore on which Angus Jones was now condemned to wander with me. Where the Pavement Ends |John Russell 

The tide was out at that time, and the banks of the Orwell are to this day a marvellous acreage of muddy foreshore at low water. The Rivers of Great Britain: Rivers of the East Coast |Various 

The long waste of foreshore lay moaning under the dawn and the sea; the ocean was a flat dark strip with a white edge. Sons and Lovers |David Herbert Lawrence 

The water was low, exposing the foreshore, and there was a careless porter sitting on a bale of goods. Rivers of Great Britain. The Thames, from Source to Sea. |Anonymous 

Along the sandy foreshore of the bay there was the same stillness: heaven and earth and ocean lay as if under an enchantment. Dodo's Daughter |E. F. Benson