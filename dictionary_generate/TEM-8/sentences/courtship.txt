My first love went to art school, and early in our courtship he invited me to a student show of his photography. Such a pretty face |Aubrey Gordon |February 26, 2021 |Vox 

In this recording, a male superb lyrebird ends its complicated courtship song and dance with an uncanny imitation of a raucous mobbing flock. A single male lyrebird can mimic the sound of an entire flock |Jake Buehler |February 25, 2021 |Science News 

I can still enjoy the romance, root for the protagonists, celebrate the courtships. How ‘Bridgerton’ flipped the script on ‘The Duke and I’ |Vanessa Riley |January 12, 2021 |Washington Post 

They would mount others and display courtship behaviors exactly like a male would. Catherine Dulac Finds Brain Circuitry Behind Sex-Specific Behaviors |Claudia Dreifus |December 14, 2020 |Quanta Magazine 

As we read these letters, which cover the couple’s early courtship, from 1941 to 1945, we can look for clues as to what went wrong — though the details are notably one-sided. What Kurt Vonnegut’s rapturous love letters reveal about him as a writer — and husband |Susan Keselenko Coll |December 3, 2020 |Washington Post 

Everyone showed lots of skin and courtship perfumed the air. What Downton’s Fashion Really Means |Katie Baker |January 2, 2015 |DAILY BEAST 

They married on December 19, 2013 after a romantic “Walt Disney” kind of courtship, they both said. Inside A Finishing School for Transwomen |Sharon Adarlo |December 27, 2014 |DAILY BEAST 

However, the Republican base will be far less forgiving of Paul for his criticism of police policy and his courtship of Sharpton. GOP Won’t Forgive Rand for Cop Critique |Lloyd Green |December 23, 2014 |DAILY BEAST 

One of the lefties she met was a sociology instructor named Philip Rieff, whom she married after a 10-day courtship. Still Desperately Seeking Susan Sontag |Allen Barra |September 26, 2014 |DAILY BEAST 

What made for an intoxicating courtship, however, resulted in a troubled marriage. The Stacks: Grateful Dead I Have Known |Ed McClanahan |August 30, 2014 |DAILY BEAST 

There is no dramme of manhood to suspect,On such thin ayrie circumstance as thisMeere complement and courtship. The Fatal Dowry |Philip Massinger 

I was riding by the ranch of Mr. Blank, who had wooed and won our cook after a courtship that was as brief as it was fervid. A Fortune Hunter; Or, The Old Stone Corral |John Dunloe Carteret 

Well, being safe again, he was a devoted lover again, and he must get on with his courtship. Overland |John William De Forest 

There was nothing romantic in the courtship, but at the same time it was far from commonplace. Frederick Chopin as a Man and Musician |Frederick Niecks 

The girl "falls in love" with some one, and the courtship begins. As A Chinaman Saw Us |Anonymous