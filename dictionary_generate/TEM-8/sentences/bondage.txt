Such complexity is the essence of this incisive but unimaginatively adapted story, which subverts the overly simplistic binaries of bondage and liberty, oppressed and oppressor, abolitionist and enslaver. The Long Song Is a Brilliant Tale of Slavery’s End in Jamaica, Frustratingly Told |Judy Berman |January 29, 2021 |Time 

It started with a much older law, one that shaped a more complex legacy of human bondage. Loopholes have preserved slavery for more than 150 years after abolition |Caroline Kisiel |January 27, 2021 |Washington Post 

Republicans, based in the South, shot back that their rivals were nothing less than monarchists — stifling free speech, repressing the people and endangering slavery by recognizing the Haitian rebels who had thrown off their bondage. The election that foreshadowed 2020 |James Morone |January 11, 2021 |Washington Post 

Examples include freedom from slavery, bondage, and oppression. Why a Little Discipline Goes a Long Way |Brad Stulberg |January 6, 2021 |Outside Online 

Washington kept him and the other enslaved members of the president’s house in bondage by circumventing the Pennsylvania abolition law that allowed them to petition for freedom if they remained in the state more than six months. George Washington’s 1795 Thanksgiving celebrated liberty. But the chef behind the feast had none. |Ramin Ganeshram |November 19, 2020 |Washington Post 

Appearances mostly occurred in crime novels like “Bondage and boyflesh” by Newt Jennings. A Brief History of the Phrase 'F*ck the Police' |Rich Goldstein |August 23, 2014 |DAILY BEAST 

But the craziest thing I did was the stuff I did for Device Bondage, which is actually super fun. Sasha Grey on Her Novel ‘The Juliette Society,’ James Deen, and More |Marlow Stern |August 27, 2013 |DAILY BEAST 

Porn star Holly Sampson, whose work includes Descent Into Bondage and Diary of a Horny Housewife. Tiger's Thing for White Girls |Elizabeth Gates |December 8, 2009 |DAILY BEAST 

The day will come, however, when people will think of him as the man who wrote Of Human Bondage. When Winter Comes to Main Street |Grant Martin Overton 

Freedom gotten by the Sword is an 140 established Bondage to some part or other of the Creation. The Digger Movement in the Days of the Commonwealth |Lewis H. Berens 

Because it brings in Kingly Bondage again, and is the occasion of all quarrels and oppressions. The Digger Movement in the Days of the Commonwealth |Lewis H. Berens 

No man shall either give hire or take hire for his work; for this brings in Kingly Bondage. The Digger Movement in the Days of the Commonwealth |Lewis H. Berens 

Bondage, serfdom, even slavery, seemed to be sanctioned by the Bible. The Influence of the Bible on Civilisation |Ernst Von Dobschutz