Construction is being supervised by the Metropolitan Washington Airports Authority, which had estimated the project would be ready to hand over to Metro this spring. Metro board expresses wariness over increased debt but gives tentative approval |Justin George |February 11, 2021 |Washington Post 

Some students are doing all-virtual learning from their classrooms, supervised by an aide. D.C. completes a week of in-person classes: Low attendance, frustrated teachers, confident principals and happy students |Perry Stein |February 7, 2021 |Washington Post 

The New Haven school board, supervising some 20,600 students, worried about the safety of buildings and buses, about protecting staff and students, about possible spikes in infections, and more. In Connecticut, Miguel Cardona led a full-court press for schools to reopen |Laura Meckler, Nick Anderson |February 2, 2021 |Washington Post 

When his mother is outside in the panda area, Xiao Qi Ji likes to come out of the den and “supervise” the keepers “as they clean and get the habitat ready for Mei Xiang’s return,” officials said. Baby panda makes debut — online — at National Zoo |Dana Hedgpeth, Justin Wm. Moyer |January 27, 2021 |Washington Post 

American women with children are also three times as likely to have lost work, according to the Pew Center, as mothers are often tasked with more child care, or supervised at-home schooling. Women, Particularly Women of Color, Hit Hardest by Loss of Hospitality Jobs |Jaya Saxena |January 12, 2021 |Eater