It was a metaphorical statement of giving and withdrawing consent for a show rooted in a literal representation of Coel being assaulted. How Michaela Coel Tells Awkward Truths |Eromo Egbejule |August 30, 2020 |Ozy 

The mathematically manipulated results are passed on and augmented through the stages, finally producing an integrated representation of a face. The Bias in the Machine - Issue 89: The Dark Side |Sidney Perkowitz |August 19, 2020 |Nautilus 

I hope this list—a representation of the most consequential changes taking places in our world—is similarly useful for you. Introducing Quartz’s new Obsessions |Katherine Bell |August 13, 2020 |Quartz 

“Given the moment we are in, I can only hope our institutions really understand what this failure of representation means to our city,” he said. Few Latino Residents Are Applying to Take Part in Redistricting |Andrew Keatts and Maya Srikrishnan |July 8, 2020 |Voice of San Diego 

The voters don’t want to have an elected city attorney on the, and representation said, that’s fine. Politics Report: Vacation Rentals? It’s Just Like Old Times! |Scott Lewis and Andrew Keatts |July 4, 2020 |Voice of San Diego 

With all that said, representation of each of these respective communities has increased in the new Congress. The Unbearable Whiteness of Congress |Dean Obeidallah |January 8, 2015 |DAILY BEAST 

As this excellent piece in Mother Jones describes, however, Holsey had outrageously poor representation during his trial. How the U.S. Justice System Screws Prisoners with Disabilities |Elizabeth Picciuto |December 16, 2014 |DAILY BEAST 

During that time days, Livvix went through court hearings without legal representation. The Strange Case of the Christian Zionist Terrorist |Creede Newton |December 14, 2014 |DAILY BEAST 

What do you think prompted the change in comic book representation of LGBTQ characters? Gail Simone’s Bisexual Catman and the ‘Secret Six’ |Rich Goldstein |December 6, 2014 |DAILY BEAST 

Barbie is an unrealistic, unhealthy, insulting representation of female appearance. Sexism Begins in the Toy Aisle |Nancy Kaffer |November 29, 2014 |DAILY BEAST 

With less intelligent children traces of this tendency to take pictorial representation for reality may appear as late as four. Children's Ways |James Sully 

As observation widens and grows finer, the first bald representation becomes fuller and more life-like. Children's Ways |James Sully 

The child now aims at constructing a particular linear representation, that of a man, a horse, or what not. Children's Ways |James Sully 

He had heard it hinted that allowing the colonies representation in Parliament would be a simple plan for making taxes legal. The Eve of the Revolution |Carl Becker 

But sufficient can be discerned for the grasping of the idea, which seems to be a representation of the Nativity. The Portsmouth Road and Its Tributaries |Charles G. Harper