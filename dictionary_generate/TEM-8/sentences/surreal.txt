Among the many things we’ve learned during these surreal times is how to appreciate exploring our own backyards, local parks, and neighborhoods. 11 Great Microadventures You Can Do Now |The Editors |October 1, 2020 |Outside Online 

There is something surreal about being in the biggest economic downturn since the 1930s, and yet day after day hearing extraordinary stories of business success. Extraordinary stories of business success defy economic downturn |Alan Murray |September 23, 2020 |Fortune 

It is very surreal, but let’s get back to SEO because that is what Pam Auugst of Pam Ann Marketing and I spoke about that day. Video: Pam Auugst on SEO automation & XML sitemaps |Barry Schwartz |August 24, 2020 |Search Engine Land 

This fall, what passes for education will be a surreal world of inequality. Public Schools May Be Open in the Fall – to Those Who Can Afford it |Will Huntsberry |August 5, 2020 |Voice of San Diego 

More than half a billion years ago, during the Ediacaran Period, a surreal world of life overran the ocean floor. What Is an Individual? Biology Seeks Clues in Information Theory. |Jordana Cepelewicz |July 16, 2020 |Quanta Magazine 

“It was a little bit surreal to know that this was unfolding there,” Coren said. CNN's Overnight Sydney Star |Lloyd Grove |December 16, 2014 |DAILY BEAST 

The insurrectionists seemed actors in a surreal episode of revolutionary play-acting in which the curtain was about to fall. East Ukraine: Back in the USSR |Jamie Dettmer |November 19, 2014 |DAILY BEAST 

A slow, surreal tide of deformation has appeared throughout the city. Silicon Valley Mansions, Swallowed Alive |Geoff Manaugh |November 8, 2014 |DAILY BEAST 

There followed a series of rooms and scenes, showing various horrors or surreal scenes. Sex, Blood, and Screaming: Blackout’s Dark Frights |Tim Teeman |October 7, 2014 |DAILY BEAST 

But Fox always manages to push the boundaries and make things just a little surreal. The Fox News Apology Tour |Dean Obeidallah |October 1, 2014 |DAILY BEAST