Robinhood has gained notoriety during the pandemic by attracting a massive customer base of younger investors. Robinhood agrees to pay $65 million settlement following SEC claims about trading |Lee Clifford |December 17, 2020 |Fortune 

It’s unclear what direct impact the notoriety had on the governments of Trinidad and Tobago or Venezuela. A stranded oil tanker at risk of spilling in the Caribbean looks to be safe — for now |Jariel Arvin |October 22, 2020 |Vox 

Another gained five minutes of notoriety in May as a gym owner arrested for defying public health orders. Morning Report: The Rise of Private, Non-School Schooling Options |Voice of San Diego |August 6, 2020 |Voice of San Diego 

It sometimes takes years to gain the type of authority and notoriety that makes other websites willingly backlink to you as a resource. Nine mistakes to avoid when contacting websites for backlinks |Raj Dosanjh |July 29, 2020 |Search Engine Watch 

Not only do backlinks send more traffic to your site, but they help you gain notoriety as an authority in your industry. Nine mistakes to avoid when contacting websites for backlinks |Raj Dosanjh |July 29, 2020 |Search Engine Watch 

Yes, publicizing tragedy gets clicks, gets ad revenue, gets notoriety, and can be done for all the wrong reasons. Cover-Ups and Concern Trolls: Actually, It's About Ethics in Suicide Journalism |Arthur Chu |January 3, 2015 |DAILY BEAST 

Scalise was a state representative old enough to remember the notoriety of Farrell and Knight from years before. The Louisiana Racists Who Courted Steve Scalise |Jason Berry |January 3, 2015 |DAILY BEAST 

Does it matter whether Taylor Swift wants me to inflate my Internet notoriety by doing a dumb thing where I lip sync to her music? Death of the Author by Viral Infection: In Defense of Taylor Swift, Digital Doomsayer |Arthur Chu |December 3, 2014 |DAILY BEAST 

Whether they win or lose, contestants can be assured of one thing: notoriety as a sex worker. Inside ‘The Sex Factor’: Where 16 Men and Women Vie For Porn Immortality |Aurora Snow |November 22, 2014 |DAILY BEAST 

Fueled by atrocity and a blitzkrieg of gains in Iraq and Syria, the Islamic State has enjoyed a meteoric climb to notoriety. Watching ISIS Come to Power Again |Elliot Ackerman |September 7, 2014 |DAILY BEAST 

Samuel Jessup died; an opulent English grazier, of pilltaking notoriety. The Every Day Book of History and Chronology |Joel Munsell 

Who was the second, who has attained such notoriety in connexion with Nelson's name; and when and where were they married? Notes and Queries, Number 177, March 19, 1853 |Various 

Why, the reduced price of provisions is a matter of universal notoriety, and past all question. Blackwood's Edinburgh Magazine, No. CCCXXXIX. January, 1844. Vol. LV. |Various 

This event gave Grace Darling the notoriety which her noble conduct so well merited. The Childhood of Distinguished Women |Selina A. Bower 

Mr. Minton has acquired a notoriety, even in that proud city, which makes his house one of the most popular resorts. The Condition, Elevation, Emigration, and Destiny of the Colored People of the United States |Martin R. Delany