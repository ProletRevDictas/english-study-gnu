In a conference call with investors on Tuesday, Salesforce CEO Marc Benioff gushed with optimism. Salesforce shares soar 13% on strong growth despite the pandemic |jonathanvanian2015 |August 25, 2020 |Fortune 

Meanwhile, Edward Jones’ Logan Purk believes the stock is “priced appropriately,” but there’s “a lot of optimism priced in,” he tells Fortune. As Apple stock tops $500, bulls cite these key reasons it could still go higher |Anne Sraders |August 24, 2020 |Fortune 

In the months following the Business Roundtable announcement, our optimism proved to be warranted. Why a year later, the Business Roundtable’s updated statement of purpose is more relevant than ever |jakemeth |August 19, 2020 |Fortune 

Trading for Mike Conley was a huge source of optimism in Salt Lake City before the season began, but the 32-year-old missed 23 games, and his player efficiency rating dropped to its lowest point in a dozen seasons. Can Donovan Mitchell Reach His Potential In The Bubble? |Michael Pina |August 3, 2020 |FiveThirtyEight 

In place of that optimism is a renewed sense of uncertainty. The second wave of media layoffs is here |Max Willens |July 17, 2020 |Digiday 

Within a few swipes, I was already feeling that burst of romantic optimism you need the first day of the (Christian) new year. My Week on Jewish Tinder |Emily Shire |January 5, 2015 |DAILY BEAST 

Whatever the future holds for Africa, optimism certainly abounds. Silicon Valley Sets Its Sights on Africa |Christian Borys |December 22, 2014 |DAILY BEAST 

The optimism across the hemisphere was obvious, but many challenges remain. Venezuela Says Goodbye to Its Lil Friend, While the Rest of the Continent Cheers |Catalina Lobo-Guererro |December 20, 2014 |DAILY BEAST 

And one has to fight against that and create some haven for optimism. The Gospel According to Nick Denton—What Next For The Gawker Founder? |Lloyd Grove |December 14, 2014 |DAILY BEAST 

Miraculously, Malala survived, and her courage, wisdom, and optimism have continued to transfix and inspire the world. Promoting Girls’ Education Isn’t Enough: Malala Can Do More |Paula Kweskin |December 9, 2014 |DAILY BEAST 

And a rampant ache in my head, seconded by a medium-sized gash in the scalp, didn't make for an access of optimism at that moment. Raw Gold |Bertrand W. Sinclair 

They foreclose without mercy, but that does not frighten their old patrons, who have the perennial optimism of the country. Ancestors |Gertrude Atherton 

Whilst I am by nature optimistic, I must confess that in these latter days my optimism occasionally receives a shock. Fifty Years of Railway Life in England, Scotland and Ireland |Joseph Tatlow 

The situation would have been hopeless to anybody not possessed of Scattergood's optimism and resource. Scattergood Baines |Clarence Budington Kelland 

Fat Boy, our clarinetist who doubles on Martian horn-harp, made a feeble attempt at optimism. The Holes and John Smith |Edward W. Ludwig