The 2024 landing itself is slated to utilize hardware built by private companies, most notably the lunar lander for taking humans to the surface. NASA will pay for moon rocks excavated by private companies |Neel Patel |September 10, 2020 |MIT Technology Review 

Hence, utilizing the avenue of YouTube for your business can be an open-ended option for organizations to help with their marketing and product promotions. How businesses can use YouTube to tackle the COVID-19 business crisis |Catherrine Garcia |September 7, 2020 |Search Engine Watch 

Elaine Swann, the founder of The Swann School of Protocol, says guests should utilize RSVP opportunities provided by the couple. How to turn down a wedding invitation during the coronavirus pandemic |Brooke Henderson |August 23, 2020 |Fortune 

Healium, founded in 2013, utilizes augmented and virtual reality and wearable technology to enable users to “see their feelings.” 5 companies that want to track your emotions |jakemeth |August 22, 2020 |Fortune 

For example, 10 Lovelace employees will be audited weekly to verify they are not utilizing ZIP codes or geographic locations to screen or test patients. Federal Investigation Finds Hospital Violated Patients’ Rights by Profiling, Separating Native Mothers and Newborns |by Bryant Furlow, New Mexico In Depth |August 22, 2020 |ProPublica 

Nor does the jet have the ability to capture high-definition video, utilize an infra-red pointer. Pentagon Misfires in Stealth Jet Scandal |Dave Majumdar |January 8, 2015 |DAILY BEAST 

“If HumaneWatch helps me get where I want to go, then I want to utilize them,” she said. The Sleazy War on the Humane Society |Center for Public Integrity |August 18, 2014 |DAILY BEAST 

Perhaps worst of all, public-sector unions utilize mandatory dues to promote bad policy. Why Progressives Shouldn’t Support Public Workers Unions |Dmitri Mehlhorn |July 11, 2014 |DAILY BEAST 

We must utilize the reality of Obamacare to bring black America into a new relationship with the health-care system. America Is Coming to Terms with Its Racial Past—Let’s Look Ahead Instead |John McWhorter |May 22, 2014 |DAILY BEAST 

Silicon chips are typically two-dimensional, Boahen explained, limiting the number of dedicated currents they can utilize. The Computer That Replicates a Human Brain |Dale Eisinger |May 1, 2014 |DAILY BEAST 

And as that miracle hadn't become necessary then, I thought it might be a good idea to utilize the plant now. A Connecticut Yankee in King Arthur's Court, Complete |Mark Twain (Samuel Clemens) 

As a therapeutic measure, society should utilize psychological knowledge as a new method of control. Taboo and Genetics |Melvin Moses Knight, Iva Lowther Peters, and Phyllis Mary Blanchard 

Happy the person who knows how to utilize the Law so that it serves the purposes of grace and of faith. Commentary on the Epistle to the Galatians |Martin Luther 

A college is not designed to train and discipline the mind, but to utilize science, and become a school of technology. Beacon Lights of History, Volume VI |John Lord 

In Leptar, however, where you carry these jewels, there is no way at all to utilize them for anything but evil. The Jewels of Aptor |Samuel R. Delany