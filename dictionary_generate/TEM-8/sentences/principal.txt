Rickie Stanley, Hill’s former teammate at Mount Vernon High and now the assistant principal at the school, sensed something was wrong. A rare disease, a covid diagnosis, a painful decision: The death of basketball coach Lew Hill |Dave Sheinin |February 11, 2021 |Washington Post 

The teenager was an “all-around good soul,” said Damon Monteleone, principal of Richard Montgomery High School in Rockville. Montgomery high school senior, athlete, slain, Howard County police say |Martin Weil |February 8, 2021 |Washington Post 

I got promoted at a very young age to assistant principal, and yeah, I was off and running. The Educator Fighting to Rectify the Achievement Gap |Esabelle Lee |February 6, 2021 |Ozy 

The principal treatments for cancer at the time consisted of surgery and radiation. Emil Freireich, a pioneer of chemotherapy and a ‘towering figure in oncology,’ dies at 93 |Emily Langer |February 4, 2021 |Washington Post 

However, as a former teacher and principal and in my experience as an educator, I can tell you, when I have 15 students in front of me versus 28, I am able to give more specialized attention to those 15 students. Five key moments from education secretary nominee Miguel Cardona’s confirmation hearing |Valerie Strauss |February 3, 2021 |Washington Post 

The leak suggests that Mr. Obama remains blind to the principal cause of his foreign policy woes. Before Ditching His Top Aides, Obama Should Look in the Mirror |Leslie H. Gelb |November 2, 2014 |DAILY BEAST 

The Spire rests on the same basic principal as the old-school fountains. Font of Invention | |September 18, 2014 |DAILY BEAST 

But younger members of the community who encountered the man as a principal had a different tale to tell. This 'Holy Guy'—and Grandfather of 100—Is Accused of Sexually Abusing a Student |Batya Ungar-Sargon |September 9, 2014 |DAILY BEAST 

He recalled one event, when he was playing with a friend, Areah, when the principal came outside and found them. This 'Holy Guy'—and Grandfather of 100—Is Accused of Sexually Abusing a Student |Batya Ungar-Sargon |September 9, 2014 |DAILY BEAST 

In an ultra-Orthodox enclave of upstate New York, a former student has accused a principal of sex acts. This 'Holy Guy'—and Grandfather of 100—Is Accused of Sexually Abusing a Student |Batya Ungar-Sargon |September 9, 2014 |DAILY BEAST 

This is the first and principal point at which we can stanch the wastage of teaching energy that now goes on. The Salvaging Of Civilisation |H. G. (Herbert George) Wells 

"The Smoker," and "Mother and Daughter," a triptych, are two of her principal pictures. Women in the fine arts, from the Seventh Century B.C. to the Twentieth Century A.D. |Clara Erskine Clement 

It is the principal waste-product of metabolism, and constitutes about one-half of all the solids excreted—about 30 gm. A Manual of Clinical Diagnosis |James Campbell Todd 

The principal room or "hall" will accommodate about 1,000 persons, the remaining portion of the premises being let off in offices. Showell's Dictionary of Birmingham |Thomas T. Harman and Walter Showell 

Thomas Barnes, principal editor of the Times newspaper, died in London, aged 56. The Every Day Book of History and Chronology |Joel Munsell