So it's really a take-what-you-can-get kind of situation with him. ‘Scandal’ Review: Olivia Pope Has Lost Her Damn Mind |Kevin Fallon |September 26, 2014 |DAILY BEAST 

Played in reverse that becomes ‘Here’s me/Here I am/What we have lost/I am the messenger of love. The Stacks: The Judas Priest Teen Suicide Trial |Ivan Solotaroff |June 28, 2014 |DAILY BEAST 

If this seems like a small, so-what kind of thing to you, I submit two thoughts. South Carolina Republicans Snub Desegregation Judge |Michael Tomasky |April 21, 2014 |DAILY BEAST 

That is, when things are going well, no one feels compelled to spend money on a what-if project. What to Do With the Thousands Displaced by Hurricane Sandy? |Malcolm Jones |November 8, 2012 |DAILY BEAST 

TV pool asked Romney at least five times whether he would eliminate FEMA as president/what he would do with FEMA. Romney Won't Talk FEMA |Michael Tomasky |October 30, 2012 |DAILY BEAST 

Coals!what would he do with coals?Toast his cheese with em, and then come back for more. Oliver Twist, Vol. II (of 3) |Charles Dickens 

Ay, murmured the sick woman, relapsing into her former drowsy state, what about her?what aboutI know! Oliver Twist, Vol. II (of 3) |Charles Dickens 

Consequently, while the direction of the what-not-how-now line is definitely fixed, their actual positions remain unestablished. Punch, or the London Charivari, Vol. 152, May 16, 1917. |Various 

"Nothing what-ever," she answered, with an urbanity that defied the suggestion of malice. When Valmond Came to Pontiac, Complete |Gilbert Parker 

But, brother sinner, pray explain How 'tis that you are not in pain?What power hath work'd a wonder for your toes? The Book of Humorous Verse |Various