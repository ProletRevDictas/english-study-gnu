Companies like Hayungs’ that offered telehealth services in particular made spectacular revenue gains through the first months of the pandemic. The COVID-19 Charmer: How a Self-Described Felon Convinced Elected Officials to Try to Help Him Profit From the Pandemic |by Vianna Davila, Jeremy Schwartz and Lexi Churchill |September 25, 2020 |ProPublica 

The unapologetic Elon Musk continues to grab headlines on a near-daily basis that switch between spectacular achievements and openly challenging his critics. Can a person be “too nice” to be a strong leader? |Kevin Walkup |September 25, 2020 |Quartz 

Ketel One Vodka created a spectacular at home viewing experience with the help of the Emmy nominee Billy Porter, Cocktail Courier and mixologist Charles Joly. Emmy’s big night was virtually a first |Susan Hornik |September 21, 2020 |Washington Blade 

A lot of the big companies are agreeing to put their best people on both therapeutics and vaccines, so that’s pretty spectacular. When Bill Gates thinks we’ll have a COVID-19 vaccine—and why that won’t be the end of the pandemic |cleaf2013 |September 21, 2020 |Fortune 

In all likelihood, this is going to be spectacular next spring. What the Photos of Wildfires and Smoke Don’t Show You |by Elizabeth Weil and Lisa Larson-Walker |September 21, 2020 |ProPublica 

There were the bevvy of performances promoting Beyoncé, too, which were characteristically spectacular. Bow Down, Bitches: How Beyoncé Turned an Elevator Brawl Into a Perfect Year |Kevin Fallon |December 31, 2014 |DAILY BEAST 

Most of them were large birds—hawks, egrets, and even peacocks—for spectacular visual effect as they took flight. The Buddhist Business of Poaching Animals for Good Karma |Brendon Hong |December 28, 2014 |DAILY BEAST 

Today, the train chugs north out of Kanchanaburi over the famous bridge before it hits a spectacular bend in the river. Riding Thailand’s WWII Death Railway |Liza Foreman |December 21, 2014 |DAILY BEAST 

The Macallan 18 had long being a personal favorite, one of the first spectacular single malts that I ever had the chance to try. A Whisky Connoisseur Remembers That First Sip of The Macallan | |December 10, 2014 |DAILY BEAST 

But perhaps the most spectacular lot in the sale is a silver jug, a birthday present to Churchill from his War Cabinet in 1942. Churchill’s Secret Treasures for Sale: A British PM’s Life on the Auction Block |Tom Teodorczuk |December 8, 2014 |DAILY BEAST 

Niagara Falls' spectacular skew-wise splashing toward the Canadian side didn't set many hearts at ease, either. Old Friends Are the Best |Jack Sharkey 

I pumped Dr. Hudson's hand and assured him that we had indeed made spectacular history, and together we could make millions. Nine Men in Time |Noel Miller Loomis 

Then on to the corrals, where some spectacular broncho busting was staged for the sole benefit of the visitors. The Outdoor Girls in the Saddle |Laura Lee Hope 

Paris, hungry for the spectacular, constructs it indifferently out of anything, civil war as readily as the burial of a statesman. The Nabob |Alphonse Daudet 

A smock covered her into shapelessness, and her spectacular hair was bound up in a kerchief, but she still looked good. Security |Poul William Anderson