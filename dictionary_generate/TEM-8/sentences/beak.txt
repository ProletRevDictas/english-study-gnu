In the British Virgin Islands, O’Connell watched as 60 flamingoes held their necks erect and beaks high, moving their heads in a back-and-forth pattern called head-flagging. In the animal kingdom, rituals that connect, renew and heal |Barbara King |January 22, 2021 |Washington Post 

The bendable nose bridge also allowed me to dial in the fit above my beak so it wouldn’t slip down. The Exercise Mask Our Gear Guy Swears By |Joe Jackson |December 11, 2020 |Outside Online 

A new fossil find reveals an unexpected bird from that time—one with a whopping, great toucan-like beak. Cretaceous birds were thought to have small bills—except this one |Scott K. Johnson |November 29, 2020 |Ars Technica 

That’s a big contrast with modern birds, which have a wild variety of beak shapes befitting their many different ecological niches. Cretaceous birds were thought to have small bills—except this one |Scott K. Johnson |November 29, 2020 |Ars Technica 

The new fossil is a nicely preserved head of a crow-sized bird with a strikingly long, tall and narrow beak. Cretaceous birds were thought to have small bills—except this one |Scott K. Johnson |November 29, 2020 |Ars Technica 

The key part of the costume, beyond the head-to-toe fabric, was the beak. It’s Not Time to Worry About China’s Plague Just Yet |Kent Sepkowitz |July 23, 2014 |DAILY BEAST 

And a sharp chicken beak is nothing to be trifled with either. What Did TJ Mean By “Pursuit of Happiness,” Anyway? |P. J. O’Rourke |June 8, 2014 |DAILY BEAST 

The Duck Dynasty congressman got caught sticking his beak in the wrong place. Duck Dynasty Congressman In Sex Scandal |Ben Jacobs |April 7, 2014 |DAILY BEAST 

Leroy had survived, but had an injured wing and a scuffed beak. The Twister Stole My Pet: How Cats, Dogs, and a Donkey Survived Oklahoma |Christine Pelisek |May 23, 2013 |DAILY BEAST 

I also like a bird's beak knife, for fiddly decorative things like making radish flowers and skinning apples in one long peel. The 2012 Holiday Kitchen Gift Guide |Megan McArdle |December 13, 2012 |DAILY BEAST 

Here there was a scuffling sound in the basket, and the Roc rapped on the cover with her hard beak, and cried, "Hush!" Davy and The Goblin |Charles E. Carryl 

Jehosophat kicked at him with his wet feet, and tried to grab the fat red nose that hung down over the turkey's beak. Seven O'Clock Stories |Robert Gordon Anderson 

A little tar and ashes in his beak was a greater kindness to him than a charge of bird shot. The Red Cow and Her Friends |Peter McArthur 

The peculiarity of their beak consists in the lower mandible being considerably longer than the other into which it shuts. In the Wilds of Florida |W.H.G. Kingston 

To prevent the water rushing into its throat as it skims the surface with its beak, the bird is provided with a very small gullet. In the Wilds of Florida |W.H.G. Kingston