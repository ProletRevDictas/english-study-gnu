Another friend blocked her niece, whose routine ranting had crossed the line into shaming and bullying anyone who voted for the other party. Politics and conspiracy theories are fracturing relationships. Here’s how to grieve those broken bonds. |Jeff Schrum |February 11, 2021 |Washington Post 

A man well known for his strict bed-at-8 routine was still awake, trying to put an edge on his teammates. Tom Brady still has the itch, and it’s taken him places no football player has been |Sally Jenkins |February 8, 2021 |Washington Post 

No matter who you are or your walk of life, there are areas of opportunity in your home and routine alike for a more streamlined experience. The best, most practical Valentine’s Day gifts for any kind of partner |PopSci Commerce Team |February 8, 2021 |Popular-Science 

He responded to eight routine police incidents on that shift, according to town records. How the Police Bank Millions Through Their Union Contracts |by Andrew Ford, Asbury Park Press, and Agnes Chang, Jeff Kao and Agnel Philip, ProPublica |February 8, 2021 |ProPublica 

The first day, some students wanted to take naps midday like they did at home, and the school had to review routines and school-day structures with the students. D.C. completes a week of in-person classes: Low attendance, frustrated teachers, confident principals and happy students |Perry Stein |February 7, 2021 |Washington Post 

For now, Sabrine continues her daily routine of visits to the protest camp, to political leaders and taking care of the twins. A Sunni-Shia Love Story Imperiled by al Qaeda |Ruth Michaelson |December 26, 2014 |DAILY BEAST 

Royal Christmases have a rhythm and routine—but this year Will, Kate, and baby George have their own, more relaxed plans. Prince George’s Christmas: Better Than Yours |Tom Sykes |December 24, 2014 |DAILY BEAST 

For OK Go, the four-piece band from Chicago, mainstream success started with eight treadmills and a choreographed dance routine. OK Go Is Helping Redefine the Music Video For the Internet Age |Lauren Schwartzberg |December 15, 2014 |DAILY BEAST 

“This [investigation] is part of routine procedure following the death of any firefighter,” he told The Daily Beast. The Mystery Death Of A Female Firefighter |Christopher Moraff |December 13, 2014 |DAILY BEAST 

I'm pleased with my decision to avoid the routine script problems in favor of the spicy stuff. Alfred Hitchcock’s Fade to Black: The Great Director’s Final Days |David Freeman |December 13, 2014 |DAILY BEAST 

They don't seem to think there would be much good gained by begging for special favours through routine channels. Gallipoli Diary, Volume I |Ian Hamilton 

They are easily seen with the one-sixth objective in the routine microscopic examination. A Manual of Clinical Diagnosis |James Campbell Todd 

The morning to sleep, the afternoon to business and the evening to enjoyment, seems the usual routine with the favored classes. Glances at Europe |Horace Greeley 

He was a good country practitioner, and, I suppose, knew the ordinary routine of his work quite well. Uncanny Tales |Various 

An occasional visit to her parents, and to her old friends the nuns, was all that interrupted the quiet routine of daily duties. Madame Roland, Makers of History |John S. C. Abbott