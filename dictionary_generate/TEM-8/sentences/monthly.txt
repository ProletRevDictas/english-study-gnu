In 1998, Susan Casey, then the editor of Women Outside, a monthly Outside magazine offshoot, sent Susan Orlean to Maui, Hawaii, with the vague direction to track down a list of young women thought to be serious surfers. Resurfacing ‘Life’s Swell,’ the Story That Produced ‘Blue Crush’ |jversteegh |November 4, 2021 |Outside Online 

Dinner Thursday through Saturday, brunch weekends, monthly Sunday supper. 2021 Fall Dining Guide |Tom Sietsema |October 6, 2021 |Washington Post 

This monthly series will chronicle the history of the American century as seen through the eyes of its novelists. The 2014 Novel of the Year |Nathaniel Rich |December 29, 2014 |DAILY BEAST 

Meanwhile CBS announced a similar deal this year that will offer their catalogue of shows online for a monthly fee. Binge Watching is the New Bonding Time |The Daily Beast |December 10, 2014 |DAILY BEAST 

The income of a software engineering intern at Facebook is comparable, with monthly incomes of over $6,500. Silicon Valley Interns Make a Service Worker’s Yearly Salary In Three Months |Samantha Allen |November 25, 2014 |DAILY BEAST 

In their dissolution agreement, Anderson was ordered to pay $255 in monthly child support. Ohio Elementary School Teacher Charged With Raping Her Son |Nina Strochlic |November 11, 2014 |DAILY BEAST 

Even so, these sales account for less than one percent of the monthly sales total. Charging Up |The Daily Beast |October 28, 2014 |DAILY BEAST 

But Mrs. Dodd, the present vicar's wife, retained the precious prerogative of choosing the book to be read at the monthly Dorcas. The Pit Town Coronet, Volume I (of 3) |Charles James Wills 

The usual monthly lists of accessions have been sent out during the recess. Report of the Chief Librarian for the Year 1924-25 |General Assembly Library (New Zealand) 

A general settlement took place monthly, after which a new period began—by the borrowers with joyous unconcern. Fifty Years of Railway Life in England, Scotland and Ireland |Joseph Tatlow 

It turned out that Trevithick had taken and paid for the house at six-monthly periods, instead of yearly periods. Life of Richard Trevithick, Volume II (of 2) |Francis Trevithick 

The potatoe is always in season, being planted every month, and consequently producing a monthly crop. Journal of a Voyage to Brazil |Maria Graham