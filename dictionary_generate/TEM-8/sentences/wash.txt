Just wash, press, and frame a sack to add a special touch to any room in your house. Five cool ways to upcycle old coffee sacks |Harry Guinness |August 27, 2020 |Popular-Science 

With a capacity of eight place settings, six wash settings, and even a reminder to refill your detergent, it’s a good addition to your kitchen. Small dishwashers that fit in almost any kitchen |PopSci Commerce Team |August 25, 2020 |Popular-Science 

Synthetics like polyester and nylon make their way into the environment from washing machines—which pull off and flush microfibers from the fabric—and of course, they also line landfills. Scientists Gene-Hack Cotton Plants to Make Them Every Color of the Rainbow |Jason Dorrier |August 11, 2020 |Singularity Hub 

Two years prior, they’d said the building could use a power wash and little else. How the City Came to Lease a Lemon |Lisa Halverstadt and Jesse Marx |August 10, 2020 |Voice of San Diego 

Their powerful surges of water can uproot trees, topple buildings, carry boats inland and wash away beaches. Scientists Say: Tsunami |Carolyn Wilke |June 15, 2020 |Science News For Students 

A limited edition export stout known as the Indra Kunindra came to wash it down. Dinner at Nitehawk Cinema: ‘Christmas Vacation’ and a Beer in a Pear Tree |Rich Goldstein |December 12, 2014 |DAILY BEAST 

Christian is the son of Mexican immigrants whose father works at a car wash and mother works at McDonalds. Forget the Kids Who Can’t Get In; What About Those Who Don’t Even Apply? |Jonah Edelman |December 9, 2014 |DAILY BEAST 

First up is the larger wash still, its capacity ranging from 25,000 to 30,000 liters. When It Comes to Great Whisky, The Size of Your Still Matters | |December 9, 2014 |DAILY BEAST 

But since the government has now permitted the River God to leave the U.K., that excuse can no longer wash. Britain Has Lost Its Marbles: Elgin Loan Will Appease Putin |Geoffrey Robertson |December 5, 2014 |DAILY BEAST 

Brush the pastry with egg wash and sprinkle with fleur de sel and pepper. Make These Barefoot Contessa Chicken Pot Pies |Ina Garten |November 29, 2014 |DAILY BEAST 

He stood before the glass hung above the wash bench and 369 smoothed his hair. The Bondboy |George W. (George Washington) Ogden 

Why, he ordered his chamber-maid to bring him some soap and warm water, that he might wash the sour krout off his hands. The Book of Anecdotes and Budget of Fun; |Various 

Then said Nqong from his bath in the salt-pan, "Come and ask me about it to-morrow, because I'm going to wash." Kipling Stories and Poems Every Child Should Know, Book II |Rudyard Kipling 

On the wash-stand a spangled white tulle hat lay drowning in a basin half full of water. Rosemary in Search of a Father |C. N. Williamson 

Shiv steered into the wash room, and the doors dropped back into place. Hooded Detective, Volume III No. 2, January, 1942 |Various