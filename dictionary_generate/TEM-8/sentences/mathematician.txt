All told, mathematicians have now found two individual tetrahedra and three infinite families of tetrahedra that fill space. Undergraduates Hunt for Special Tetrahedra That Fit Together |Kevin Hartnett |February 9, 2021 |Quanta Magazine 

The work answers a question about an ancient shape thanks to a cutting-edge innovation that provides mathematicians with a new technique for finding solutions to certain equations. Tetrahedron Solutions Finally Proved Decades After Computer Search |Kevin Hartnett |February 2, 2021 |Quanta Magazine 

Rejewski, a 27-year-old mathematician, worked for the Cipher Office of Polish intelligence. Hackers thrive on the weak link in cybersecurity: People |Gershom Gorenberg |February 1, 2021 |Washington Post 

You might think that by then mathematicians knew almost all there was to know about polyhedra. Topology 101: The Hole Truth |David S. Richeson |January 26, 2021 |Quanta Magazine 

However, she soon discovered that her role as a mathematician was limited to performing time-consuming calculations by hand. The NASA Engineer Who’s a Mathematician at Heart |Susan D'Agostino |January 19, 2021 |Quanta Magazine 

One of the most impressive achievements, says University of Chicago mathematician Benson Farb, is that she linked these findings. Iranian Math Genius Mirzakhani Unveiled by President Rouhani |IranWire |August 18, 2014 |DAILY BEAST 

The Turing test is named for computer scientist, mathematician, logician, and philosopher Alan Turing. The AI That Wasn’t: Why ‘Eugene Goostman’ Didn’t Pass the Turing Test |Elizabeth Lopatto |June 10, 2014 |DAILY BEAST 

Christopher John Francis Boone is a teenage “mathematician with some behavioral difficulties.” Book Bag: How to Survive—Five Stories About Unlikely Survivors |Claire Cameron |February 11, 2014 |DAILY BEAST 

I had done my duty not only as a writer, but also a mathematician: exhausted all possibilities. A Mathematically Impossible Novel: Manil Suri Explains “The City of Devi” |Manil Suri |March 15, 2013 |DAILY BEAST 

Clearly her lack of appreciation for my proof resulted from her not being a mathematician. A Mathematically Impossible Novel: Manil Suri Explains “The City of Devi” |Manil Suri |March 15, 2013 |DAILY BEAST 

John Bonnycastle died; an English mathematician, whose works are in use in this country. The Every Day Book of History and Chronology |Joel Munsell 

Michael Angelo Ricci, an Italian cardinal, died; celebrated as a mathematician. The Every Day Book of History and Chronology |Joel Munsell 

Edward Holyoke, president of Harvard college, died; an excellent mathematician and natural philosopher. The Every Day Book of History and Chronology |Joel Munsell 

Jean Marie Ampere, famed as a mathematician and natural philosopher, died. The Every Day Book of History and Chronology |Joel Munsell 

Whenever I had occasion to go to the eminent mathematician it was not his world-wide reputation which impressed me most. Urania |Camille Flammarion