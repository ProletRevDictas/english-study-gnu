Not only does the ball shrink into a peach pit whenever it slaps against Leonard’s mighty hands, but everytime he steps on the court, he brings a game that’s been molded to dominate areas of the floor, placing an insoluble strain on the opposition. If Kawhi Turns His Back To The Basket, Watch Out |Michael Pina |September 3, 2020 |FiveThirtyEight 

He’s the leading figure in remaking old-line exchanges dominated by traders shouting bids from “open outcry pits” into electronic platforms. First he took energy trading and the NYSE electronic. Now Jeff Sprecher of ICE shares his plans to digitize your mortgage |Shawn Tully |September 2, 2020 |Fortune 

“Any suggestion that my clients or I have any responsibility in the city’s decision to buy this money pit is a convenient political deflection,” he wrote in a statement. The Deal Before the 101 Ash St. Debacle Helps Explain How We Got Here |Lisa Halverstadt and Jesse Marx |August 24, 2020 |Voice of San Diego 

Remains of fire pits were found not far from Border Cave’s former grass beds. The oldest known grass beds from 200,000 years ago included insect repellents |Bruce Bower |August 13, 2020 |Science News 

He thinks the arrangement of the pits — in a circle surrounding the henge — might mean they marked the boundary to some important space. Underground mega-monument found near Stonehenge |Avery Elizabeth Hurt |August 11, 2020 |Science News For Students 

So what of the photograph of what the Senate report described as a “well-used waterboard” with buckets around it, at the Salt Pit? CIA Interrogation Chief: ‘Rectal Feeding,’ Broken Limbs Are News to Me |Kimberly Dozier |December 11, 2014 |DAILY BEAST 

He watched the pit grow bigger every month, despite the numerous reports he wrote about the facility. Two Texas Regulators Tried to Enforce the Rules. They Were Fired. |David Hasemyer, InsideClimate News |December 9, 2014 |DAILY BEAST 

He said he watched waste haulers back up to the pit and unleash torrents of watery muck. Two Texas Regulators Tried to Enforce the Rules. They Were Fired. |David Hasemyer, InsideClimate News |December 9, 2014 |DAILY BEAST 

In several reports he urged the pit operators to safeguard the birds. Two Texas Regulators Tried to Enforce the Rules. They Were Fired. |David Hasemyer, InsideClimate News |December 9, 2014 |DAILY BEAST 

Abarca allegedly battered Hernandez, who was then dumped in the pit. Mexico’s First Lady of Murder Is on the Lam |Michael Daly |October 29, 2014 |DAILY BEAST 

With the management of these, however, the Earl of Pit Town did not trouble himself. The Pit Town Coronet, Volume I (of 3) |Charles James Wills 

At length the great Pit Town collection was housed as it deserved to be. The Pit Town Coronet, Volume I (of 3) |Charles James Wills 

Old Pit Town knows lots of good people, and would give us letters, I suppose. The Pit Town Coronet, Volume I (of 3) |Charles James Wills 

Certes le capitaine Merveilles et ses gens monstrerent leur pit non vulgaire. The Jesuit Relations and Allied Documents, Vol. II: Acadia, 1612-1614 |Various 

We never see such horrors now; and I actually envied Pit Town the possession of that picture. The Pit Town Coronet, Volume I (of 3) |Charles James Wills