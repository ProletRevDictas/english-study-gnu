While the balance sheet here favors the peacemakers — the Gandhis, Kings and Mandelas — the book includes, as it must, despots and demagogues. An anthology of great speeches, from the inspirational to the ominous |Jeff Shesol |July 9, 2021 |Washington Post 

The usurping king is a despot posing as a benefactor, exerting an authoritarian rule and setting the people close to him against each other to prove their loyalty, while his queen turns a blind eye to his increasingly obvious misdeeds. ‘Hamlet/Horatio’ queers the Bard |John Paul King |May 13, 2021 |Washington Blade 

We have seen it in the rise of other despots around the world. Marjorie Taylor Greene is delusional, dangerous |Peter Rosenstein |February 4, 2021 |Washington Blade 

But Stephen Kotkin's new biography reveals a learned despot who acted cunningly to take advantage of the times. Kotkin Biography Reveals Stalin's Evil Pragmatism |William O’Connor |November 30, 2014 |DAILY BEAST 

This is the sort of delusion that sets in when a despot confuses himself with the state after too long in power. Putin’s Sochi and Hitler’s Berlin: The Love Affair Between Dictators and the Olympic Games. |Garry Kasparov |February 7, 2014 |DAILY BEAST 

As the fiery despot, the fine character actor Simmons has never been given a role this juicy, and knocks it out of the park. ‘Whiplash’ Is Sundance’s Hottest Film, A Music-Themed Drama Starring Miles Teller and J.K. Simmons |Marlow Stern |January 24, 2014 |DAILY BEAST 

Hundreds have “martyred” themselves fighting Syrian despot Bashar al Assad. The Coming of Al Qaeda 3.0 |Bruce Riedel |August 6, 2013 |DAILY BEAST 

From the start, we see him as he is: a despot and a swindler, a Dallas blue-blood with FBI ties, fleeing a violent past. This Week’s Hot Reads, July 15, 2013 |Sarah Stodola, Jen Vafidis, Damaris Colhoun |July 15, 2013 |DAILY BEAST 

Should not every rational prince perceive that the despot is but an insane man who injures himself? Superstition In All Ages (1732) |Jean Meslier 

The idea of a terrible God who was represented as a despot, must necessarily have rendered His subjects wicked. Superstition In All Ages (1732) |Jean Meslier 

This powerful chieftain was an absolute despot ruling over a tribe of fierce warriors, who knew no will but his. Robert Moffat |David J. Deane 

There he lay, with his face upon the ground, humbly awaiting the stern despot's permission to move. Jack Harkaway's Boy Tinker Among The Turks |Bracebridge Hemyng 

At last de B. ran upon "tyrant or despot," which he commuted for "emperor." The Mirror of Literature, Amusement, and Instruction, No. 358 |Various