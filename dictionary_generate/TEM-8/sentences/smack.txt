This year’s top engineering feats smack of the sort of sci-fi future-gazing you can find in retro issues of Popular Science. The 100 greatest innovations of 2020 |Popular Science Staff |December 2, 2020 |Popular-Science 

Kyra often smacks herself on the side of the head with her hand or bites her palm so hard she draws blood, said her mother, Ka Wade. People with Developmental Disabilities Were Promised Help. Instead, They Face Delays and Denials. |by Amy Silverman for Arizona Daily Star, with data analysis by Alex Devoid, Arizona Daily Star |November 5, 2020 |ProPublica 

Then imagine a raging river where the water smacks into the piling and becomes turbulent. This weird-looking plane could someday be a fast, clean option for air travel |Rob Verger |September 28, 2020 |Popular-Science 

DART will travel to the 780-meter asteroid Didymos, where, in the fall of 2022, it will smack into Didymos’s 160-meter moonlet Dimorphos at well over 14,000 miles per hour. The World’s Space Agencies Are on a Quest to Deflect a (Harmless) Asteroid |Jason Dorrier |September 27, 2020 |Singularity Hub 

To better understand the Hyades cluster, Oh and Evans compared the speed of stars smack in the center to those escaping from it. Milky Way’s tidal forces are shredding a nearby star cluster |Ken Croswell |August 18, 2020 |Science News For Students 

To the uninitiated, this might smack of poor taste and inappropriate timing. In One Corner of Syria, Christmas Spirit Somehow Manages to Survive |Peter Schwartzstein |December 25, 2014 |DAILY BEAST 

On the first day of shooting, Kallai and his film crew found themselves smack-dab in the middle of a war zone. Trans in the Holy Land: ‘Marzipan Flowers,’ Tal Kallai, and the Shattering of Israel’s LGBT Taboos |Itay Hod |November 4, 2014 |DAILY BEAST 

His brother Sidronio immediately took over, and the Windy City reported no shortage of smack. Mexico’s First Lady of Murder Is on the Lam |Michael Daly |October 29, 2014 |DAILY BEAST 

In the SMU study it was found that children lasted about 10 minutes after a smack before they started misbehaving again. The Adrian Peterson Beating and the Christian Right's Love of Corporal Punishment |Amanda Marcotte |September 16, 2014 |DAILY BEAST 

Yes, Hillary Clinton talked some smack on Barack Obama to Jeff Goldberg in that interview. So How Hawkish Is Hillary Clinton? |Michael Tomasky |August 13, 2014 |DAILY BEAST 

Scotch shrewdness has occasionally a certain smack of mild hypocrisy, which, however, does no harm to anyone. Friend Mac Donald |Max O'Rell 

The little bullet-headed Jim was drafted off to the workhouse school, and from thence to a small fishing-smack. The Chequers |James Runciman 

The new smack was flying a flag at her masthead, but Jim could not read well enough to make out the inscription on the flag. The Chequers |James Runciman 

On one grey Sunday morning a pretty smack came creeping through the fleet. The Chequers |James Runciman 

The skipper of the smack invited Jim to go below, and handed him a steaming mug of tea. The Chequers |James Runciman