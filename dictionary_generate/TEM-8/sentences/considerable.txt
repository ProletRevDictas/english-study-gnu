This drove considerable performance improvements and allowed for a robust test-and-learn environment. Case study: Schneider Electric’s digital transformation through centralized search |Evan Kent and Kimberly Dutcher |February 11, 2021 |Search Engine Watch 

If a considerable number of established businesses are hyperlinking to your content, it’s very likely that you’re producing engaging, informative, and impactful content. A small business’ step-by-step guide to dominating local search in 2021 |Joseph Dyson |February 10, 2021 |Search Engine Watch 

Despite these advances, designing robots to work in unknown or inhospitable environments, like exoplanets or deep ocean trenches, still poses a considerable challenge for scientists and engineers. We’re Teaching Robots to Evolve Autonomously—So They Can Adapt to Life Alone on Distant Planets |Emma Hart |February 4, 2021 |Singularity Hub 

Dwelling on the problematic, as tempting as it is, overshadows the considerable triumph represented by the impressive score of Schitt’s Creek. Nominees (and 2 winners) for 2021 Golden Globes announced |Troy Masters |February 3, 2021 |Washington Blade 

Prime Minister Benjamin Netanyahu promised to get Israelis above the age of 16 vaccinated by the end of March—which would come in time for Israel’s fourth election in just two years, and one in which Netanyahu is facing considerable pressure. Israel leads the world in vaccination rates, but a key group is missing from the data |By Yara M. Asi/The Conversation |February 3, 2021 |Popular-Science 

We were on it for forty minutes of the film, a considerable part of our schedule. Alfred Hitchcock’s Fade to Black: The Great Director’s Final Days |David Freeman |December 13, 2014 |DAILY BEAST 

That is thanks in considerable part to the cops, no matter what else is said about a few of them. Synagogue Slay: When Cops Have to Kill |Michael Daly |December 10, 2014 |DAILY BEAST 

The series was cancelled after one season, but Leto had already proven his considerable talent. Renaissance Man Jared Leto Defies Categorization |The Daily Beast |December 8, 2014 |DAILY BEAST 

According to James Madison, there was “a considerable pause.” Why We Can’t Quit Calling Presidents ‘Kings’ |Kevin Bleyer |November 22, 2014 |DAILY BEAST 

He finishes off the task he has set himself here with considerable precision and skill. How WWI Produced the Holocaust |J.P. O’Malley |November 21, 2014 |DAILY BEAST 

It is, however, true, that in this respect the German hexametrist has a considerable advantage over the English. Blackwood's Edinburgh Magazine, Volume 60, No. 372, October 1846 |Various 

Thus he continued to rush over the frozen sea during a considerable part of that night. The Giant of the North |R.M. Ballantyne 

“We shall make Mr. Pickwick pay for peeping,” said Fogg, with considerable native humour, as he unfolded his papers. The Posthumous Papers of the Pickwick Club, v. 2(of 2) |Charles Dickens 

That was a considerable sensible commandment of yourn, always to shoot the foremost of the Mexicans when they attacked. Blackwood's Edinburgh Magazine, No. CCCXXXIX. January, 1844. Vol. LV. |Various 

A considerable proportion of the industrial and commercial news is now written to an end. The Salvaging Of Civilisation |H. G. (Herbert George) Wells