He scratched his head in vexation, sat down, and as he did so, saw that his coat hung also upon the chair. The Homesteader |Oscar Micheaux 

He stamped his foot to the ground in vexation, and recurred to his original determination. Blackwood's Edinburgh Magazine, No. CCCXXXIX. January, 1844. Vol. LV. |Various 

Garnache need not plague himself with vexation that his rash temper alone had wrought his ruin now. St. Martin's Summer |Rafael Sabatini 

Rabecque swore angrily and bitterly, and his vexation had two entirely separate sources. St. Martin's Summer |Rafael Sabatini 

Mrs. Chester promptly obeyed, surprised by the mingled mirth and vexation expressed by her husband's face. Dorothy at Skyrie |Evelyn Raymond