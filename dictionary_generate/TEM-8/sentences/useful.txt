We went through the house and started to gather up all the duplicates we could find of various useful things, including 10 tablecloths! Hints From Heloise: New scam tied to coronavirus pandemic |Heloise Heloise |February 12, 2021 |Washington Post 

Nor did the researchers focus on individual differences that might be useful to know, like ethnicity or gender differences. Can I Ask You a Ridiculously Personal Question? (Ep. 451) |Stephen J. Dubner |February 11, 2021 |Freakonomics 

His elegant career, through age 36, strongly resembles that of Andy Pettitte, Mike Mussina, Tim Hudson and Justin Verlander, who still had useful years left. The Nationals, like the rest of us, want to forget about 2020. That might be pretty smart. |Thomas M. Boswell |February 10, 2021 |Washington Post 

Google even mentions the value of alt text in images, saying that alt text provides them with useful information about the image that they can use to help determine the best image to return for a user’s query. Image SEO: Best practices and tips for optimization |Michael McManus |February 8, 2021 |Search Engine Watch 

To do anything useful with such a system, you need to be able to arbitrarily connect any two qubits in the network no matter how far apart they are. Connecting Distant Qubits Just Brought Distributed Quantum Computing Closer |Edd Gent |February 8, 2021 |Singularity Hub 

I still do find it a tremendously useful device to invent a character and have the character sing the song. Belle & Sebastian Aren’t So Shy Anymore |James Joiner |January 7, 2015 |DAILY BEAST 

But even when the jet will be able to shoot its gun, the F-35 barely carries enough ammunition to make the weapon useful. New U.S. Stealth Jet Can’t Fire Its Gun Until 2019 |Dave Majumdar |December 31, 2014 |DAILY BEAST 

However useful they might be for external purposes, they will always be dangerous internally. Pakistan’s Dance With Terrorists Just Backfired and Killed 132 Children |Chris Allbritton |December 17, 2014 |DAILY BEAST 

For the aficionado or the neophyte, Comics is a useful overview of a richly creative period in a burgeoning art. The Best Coffee Table Books of 2014 |Robert Birnbaum |December 13, 2014 |DAILY BEAST 

For those unfamiliar with Michals, an annotated biography and useful essays are included. The Best Coffee Table Books of 2014 |Robert Birnbaum |December 13, 2014 |DAILY BEAST 

It will be a busy session; and I want to see if I can't become a useful public man. Elster's Folly |Mrs. Henry Wood 

It is especially useful with cultures upon serum media, but is applicable also to the sputum. A Manual of Clinical Diagnosis |James Campbell Todd 

In connection with this step the practice of melodies is useful, if one has musical taste. Expressive Voice Culture |Jessie Eldridge Southwick 

To keep the frond in position it may be useful to put a book on the paper as it is spread out. How to Know the Ferns |S. Leonard Bastin 

It has since been enlarged, and is now much more ornamental as well as being useful. Showell's Dictionary of Birmingham |Thomas T. Harman and Walter Showell