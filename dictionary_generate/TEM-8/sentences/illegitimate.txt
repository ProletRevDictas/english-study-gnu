Anything they propose — whether it be policy, unity or accountability — is going to be viewed as illegitimate by a decent portion of Americans. Our Radicalized Republic |Maggie Koerth (maggie.koerth-baker@fivethirtyeight.com) |January 25, 2021 |FiveThirtyEight 

Some will dismiss my arguments as illegitimate simply because they come from the hated mainstream media. The lie that lingers: 3 in 10 Americans falsely believe the election was riddled with fraud |Philip Bump |January 19, 2021 |Washington Post 

By encouraging their followers to see their political opponents as illegitimate — and letting them follow that logic where it leads — Republicans have been flirting with this kind of anti-democratic behavior for years. The case for consequences |Zack Beauchamp |January 12, 2021 |Vox 

If we were to wake up tomorrow and decree that all NDAs are now defunct, companies would have both legitimate and illegitimate concerns. NDAs bear blame for some of the worst corporate cover-ups. How that should change |jakemeth |September 18, 2020 |Fortune 

Yanzhong Huang, senior fellow for global health at the Council on Foreign Relations, says such fears are “not entirely illegitimate” given reports of similar incidents in mainland China. Hong Kong’s citywide COVID-19 testing has become a barometer of public trust |eamonbarrett |September 9, 2020 |Fortune 

She was also an autodidact, an illegitimate girl from the provinces whose intelligence became the stuff of legend. Sor Juana: Mexico’s Most Erotic Poet and Its Most Dangerous Nun |Katie Baker |November 8, 2014 |DAILY BEAST 

You should be so proud of yourself working for an illegitimate rag like the Daily Beast. Dear Moon Landing Deniers: Sorry I Called You Moon Landing Deniers |Olivia Nuzzi |July 29, 2014 |DAILY BEAST 

In 1483 the princes were publicly declared illegitimate by a cleric. Three Dicks: Cheney, Nixon, Richard III and the Art of Reputation Rehab |Clive Irving |July 27, 2014 |DAILY BEAST 

But a large majority also view the government in Kiev as illegitimate. Ukraine Foreign Minister Speaks of Mistrust—and a Truce |Jamie Dettmer |April 19, 2014 |DAILY BEAST 

Britain is not illegitimate because it has a cross on its flag and an Anglican head of a state. The Real Problem With the American Studies Association's Boycott of Israel |Peter Beinart |December 17, 2013 |DAILY BEAST 

Estates sufficient to support the highest rank in the peerage were distributed among his illegitimate children. The History of England from the Accession of James II. |Thomas Babington Macaulay 

To the class of mothers of illegitimate children the Commissioners devoted much attention. English Poor Law Policy |Sidney Webb 

We find it advising that the mothers of illegitimate children should, on this ground alone, not be granted outdoor relief. English Poor Law Policy |Sidney Webb 

The population tends slowly to increase; about 45% of the births are illegitimate, and males are more numerous than females. Encyclopaedia Britannica, 11th Edition, Volume 4, Part 3 |Various 

Her baby, when born, is exposed as being illegitimate, but found by a goatherd and brought up—becoming the all but deified Atys. Ancient Faiths And Modern |Thomas Inman