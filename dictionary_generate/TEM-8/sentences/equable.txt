He is an equable man who treats people fairly and has many friends. Bob Rubin's House of Cards |Jeff Madrick |November 29, 2008 |DAILY BEAST 

The care shown in rearing insures a perfect straightness of stem, and an equable diameter of about an inch or an inch and a half. Tobacco; Its History, Varieties, Culture, Manufacture and Commerce |E. R. Billings. 

The warmth of the atmosphere in which you live will be always equable and genial, without tempests, without a possible squall. Honorine |Honore de Balzac 

The male then becomes inert; equable sharing of the expense of forces, just division of labour. The Natural Philosophy of Love |Remy de Gourmont 

The Loud Concrete is simply the equable concrete uttered with greater fulness of breath and loudness of tone. The Ontario Readers: The High School Reader, 1886 |Ministry of Education 

A medium rate of utterance is, with respect to time, the natural expression of an equable flow of thought. The Ontario Readers: The High School Reader, 1886 |Ministry of Education