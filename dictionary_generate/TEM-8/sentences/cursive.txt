Ancient Egyptian hieroglyphic and cursive documents describe recipes for several perfumes. Ancient ‘smellscapes’ are wafting out of artifacts and old texts |Bruce Bower |May 4, 2022 |Science News 

The two-time second-team all-American had a backdrop set up at her draft party featuring a wall of ivy outlined with flowers and her name written in cursive in lights. Easy as 1-2-3: Charli Collier, Awak Kuier, Aari McDonald top WNBA draft |Kareem Copeland |April 16, 2021 |Washington Post 

Moxie kicks off as a shout-out to riot grrrl spirit, only to give us an ending written in the cursive script of an inspirational mug. Moxie Invokes Fierce, Riot Grrrl Spirit, Only to End in a Noncommittal Group Hug |Stephanie Zacharek |March 3, 2021 |Time 

The work has the word “Libération” written in cursive across its top. In a gray, empty Paris, this corner shop’s colorful posters transport you wherever you want to go |Lily Radziemski |February 19, 2021 |Washington Post 

Thorns support is for everyone, and there are no pastel colors or condescending cursive. Portland Is Ground Zero for the Best Women’s Soccer in the World |Evelyn Shoop |June 30, 2014 |DAILY BEAST 

The letter is four pages long, written on binder paper, in careful cursive. Life as an Undocumented Immigrant |Jose Antonio Vargas |April 28, 2014 |DAILY BEAST 

He did not tell his wife that he left a note behind for her: a very detailed message, written in his careful, flowing cursive. The Man Oswald First Tried to Kill Before JFK |Bill Minutaglio, Steven L. Davis |October 3, 2013 |DAILY BEAST 

My Facebook page still reflects that—Modest Mouse and Cursive and El-P. How to Stay on Facebook and Protect Your Privacy at the Same Time |Jesse Singal |July 20, 2012 |DAILY BEAST 

But the neat cursive writing was not in Arabic; it was in Hebrew. Ultra-Orthodox Jews Vandalize Jerusalem’s Holocaust Memorial |Alex Klein |June 11, 2012 |DAILY BEAST 

I think cursive has also been used to describe the roundness of writing as opposed to an angular shape. Warren Commission (7 of 26): Hearings Vol. VII (of 15) |The President's Commission on the Assassination of President Kennedy 

Can you explain the meaning of the term "cursive" apart from your use in this instance? Warren Commission (7 of 26): Hearings Vol. VII (of 15) |The President's Commission on the Assassination of President Kennedy 

In the more cursive or Hieratic writing the horned serpent appears as ; in the later Demotic as and . The Contemporary Review, Volume 36, November 1879 |Various 

The earlier copies are all in uncial or "capital" letters, cursive or "lower-case" letters being a later invention. Library of the World's Best Literature, Ancient and Modern, Vol. VIII |Various 

The closely-packed script has come down to us, the writing fine, like Greek cursive. The White Plumes of Navarre |Samuel Rutherford Crockett