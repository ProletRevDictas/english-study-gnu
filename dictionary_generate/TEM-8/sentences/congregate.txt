The study recommended that airports work with qualified HVAC engineers to determine how they can augment current systems, particularly in areas where travelers tend to congregate. Airports have taken steps to reduce coronavirus transmission but risks still remain, study says |Lori Aratani |February 12, 2021 |Washington Post 

The coronavirus has been particularly virulent in places where people congregate — churches, nursing homes, prisons, close-quarters work environments and the like. Researchers identify social factors inoculating some communities against coronavirus |Christopher Ingraham |February 11, 2021 |Washington Post 

“It’s unwise to bring disparate people together in a congregate setting during the time of the pandemic, and the Super Bowl is no exception to that,” says John Swartzberg, an infectious disease specialist at the University of California, Berkeley. Even at half capacity, the Super Bowl could cause COVID-19 outbreaks |Kate Baggaley |February 5, 2021 |Popular-Science 

Galaxies congregate in superclusters on scales vastly greater than anything experts had considered before the 20th century. Einstein’s theory of general relativity unveiled a dynamic and bizarre cosmos |Elizabeth Quill |February 3, 2021 |Science News 

The meeting usually would have taken place in the Gertrude-Lafrance cafeteria, but by the spring it had become part of the Quebec medical facility’s red zone, making it an unsafe place to congregate. His team is going to the Super Bowl. He’s staying on the coronavirus front lines. |Adam Kilgore |February 1, 2021 |Washington Post 

A few children, settler children, congregate near what appears to have been the bus station. Inside Hebron, Israel’s Heart of Darkness |Michael Tomasky |November 21, 2014 |DAILY BEAST 

During the Iranian iteration, one event allowed customers to congregate with a local dining in Iran. Eating With The Enemy: Conflict Kitchen’s Political Cuisine |Justin Jones |July 30, 2014 |DAILY BEAST 

An NYPD official says an AP reporter called to ask where people of Chechen descent might congregate in New York City. NYPD on the Real ‘Enemies Within’: Going Undercover With Jihadis |Michael Daly |September 9, 2013 |DAILY BEAST 

The whole point of a bohemia is that people congregate in a relatively well-defined area. Literary City: Jay McInerney’s New York |Henry Krempels |January 26, 2013 |DAILY BEAST 

Yet in the film, the dwarves, hobbit and wizard all congregate to a single tree that remains untouched by the fire. ‘The Hobbit’: 19 Changes from J.R.R. Tolkien’s Novel to Peter Jackson’s Movie |Anna Klassen |December 14, 2012 |DAILY BEAST 

Patriots were inclined to congregate about the Lion d'Or and to ask awkward questions. The Light That Lures |Percy Brebner 

At its upper end, below my windows, all the cats of the neighbourhood congregate as soon as darkness gathers. Masterpieces of Mystery, Vol. 1 (of 4) |Various 

There was no help for it, and men and women had to congregate in these barns together. Auld Licht Idylls |J. M. Barrie 

The trouble was in the outfield—where the trouble in such contests are sure to congregate. The Varmint |Owen Johnson 

The term is now used loosely of any locality in a city or country where Jews congregate. Encyclopaedia Britannica, 11th Edition, Volume 11, Slice 8 |Various