If he thinks that he can get a deal he likes, I'd expect to see a c'mon-guys-we-can-do-this conciliatory exhortation. What to Look for In Tonight's State of the Union |Megan McArdle |February 12, 2013 |DAILY BEAST 

Thinking about fairness comes more naturally to liberals, and they/we care passionately about it. Michael Tomasky: Economic Fairness Isn’t Enough for Obama’s Game Plan |Michael Tomasky |April 12, 2012 |DAILY BEAST 

And MSNBC (which put out a we-wish-him-well statement) was no longer pleased by Buchanan. Buchanan: Too Radioactive for MSNBC |Howard Kurtz |February 16, 2012 |DAILY BEAST 

The problem is that Scott is unpopular—awkward, should-we-be-seen-with-him-in-public unpopular. 10 GOP Endorsements That Still Matter in 2012’s Presidential Election |John Avlon |December 7, 2011 |DAILY BEAST 

To be sure, the game has anything but a “we-the-people” buzz, despite its schlubby attire. What Obama's Golf Fashion Reveals |Robin Givhan |June 20, 2011 |DAILY BEAST 

Have we?the old man reared himself suddenly in bed, and raised two thin gnarled arms. The Romance of His Life |Mary Cholmondeley 

I then pointed to Wade and Ikewna, and then to We-we and myself, offering my arm. Left on Labrador |Charles Asbury Stephens 

I then put the same question to We-we, pointing to the other. Left on Labrador |Charles Asbury Stephens 

Ah-tn-we-tuck, the Cock Turkey; repeating his prayer from the stick in his hand, described above. Adventures of the Ojibbeway and Ioway Indians in England, France, and Belgium; Vol. I (of 2) |George Catlin 

Ah-quee-we-zaints, the Boy Chief; a venerable man of 72 years. Adventures of the Ojibbeway and Ioway Indians in England, France, and Belgium; Vol. I (of 2) |George Catlin