Head back into those lodgepole thickets and you’ll come to a clearing where a giant looms. At Glacier’s Edge, the Flames Have Always Come for My Family Cabin |jversteegh |August 20, 2021 |Outside Online 

They drive about a mile and park the bus in a bamboo thicket. The ballad of the Chowchilla bus kidnapping |Kaleb Horton |July 23, 2021 |Vox 

You can harness the wind and waves along the Gulf of Mexico, paddle through canyons, thickets, and pristine backcountry, or leap from granite cliffs into a crisp Hill Country lake. The 5 Best Water Adventures in Texas |kklein |July 8, 2021 |Outside Online 

Both parties hoped the mutual engagement of civil rights organizations, police groups, and key lawmakers could steer the talks though the political thicket that emerged after Floyd’s death. Police reform negotiations bog down on Capitol Hill as crime rises and midterms loom |Mike DeBonis |June 24, 2021 |Washington Post 

To get to the Burger King location where I sampled the chain’s new, upgraded chicken sandwich, I traversed a veritable thicket of chicken-y goodness. Burger King’s new spicy chicken sandwich is a worthy competitor, but it won’t dethrone Popeyes |Emily Heil |June 4, 2021 |Washington Post 

The abandoned barracks of the Liberian Army lay just beyond in the tropical thicket. The Life of a Liberian Child with Ebola |Sarah Crowe |November 5, 2014 |DAILY BEAST 

Just so with Dorian Satoshi Nakamoto, whose identity seems increasingly lost in a cyber thicket that no one can penetrate. Mysteries Continue to Swirl Around the Identity of Bitcoin’s Creator |Jake Adelstein |March 11, 2014 |DAILY BEAST 

After a while, as we were arguing about the Thicket, it occurred to us that all in the house save Arch and me had gone to bed. ‘The Land of the Permanent Wave’ Is Bud Shrake’s Classic Take on ‘60s Texas |Edwin Shrake |February 2, 2014 |DAILY BEAST 

Sports Illustrated sent Shrake down at his insistence to do a piece on the beautiful and haunting Big Thicket area of East Texas. ‘The Land of the Permanent Wave’ Is Bud Shrake’s Classic Take on ‘60s Texas |Edwin Shrake |February 2, 2014 |DAILY BEAST 

Biologists view the Big Thicket with profound wonder, and ecologists regard its passing with despair. ‘The Land of the Permanent Wave’ Is Bud Shrake’s Classic Take on ‘60s Texas |Edwin Shrake |February 2, 2014 |DAILY BEAST 

We got off our horses and stooped over the man, forgetting for the moment that danger might lurk in the surrounding thicket. Raw Gold |Bertrand W. Sinclair 

No trail was so obtuse, no thicket so dense that members of that regiment would not track them to their lair. The Courier of the Ozarks |Byron A. Dunn 

A girl was moved to pity by a picture of a lamb caught in a thicket, and tried to lift the branch that lay across the animal. Children's Ways |James Sully 

The eyes of the huge brute opened instantly, and he had half risen before the loud report of the gun rang through the thicket. Hunting the Lions |R.M. Ballantyne 

And out of this thicket, alas, no two people ever emerge hand in hand in concord. The Unsolved Riddle of Social Justice |Stephen Leacock