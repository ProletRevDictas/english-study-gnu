Over the past month, Fuller reached out to several Nationals super fans on social media in search of the ticket, to no avail. A Cubs fan needed one ticket to complete his epic collection. A Nats fan came through. |Scott Allen |December 26, 2020 |Washington Post 

I have tried to discuss this matter with him, but to no avail. Miss Manners: Husband’s rude eating habits driving wife up the wall |Judith Martin, Nicholas Martin, Jacobina Martin |December 18, 2020 |Washington Post 

The Centers for Disease Control and Prevention asked people not to travel for Thanksgiving, to avoid juicing infection rates, to little avail. A Kraken is loose in America |Dan Zak |December 10, 2020 |Washington Post 

Several member states reportedly tried to swap the order of speakers to address the lack of women on Day 1, but to no avail. The UN General Assembly is the ultimate ‘manel’ |Claire Zillman, reporter |September 23, 2020 |Fortune 

What some politicians and activists had long sought to do to no avail — place working parents and their child care crisis on the center stage of American politics — the virus has done in a matter of months. Why It Took So Long For Politicians To Treat The Child Care Crisis As A Crisis |Clare Malone (clare.malone@fivethirtyeight.com) |July 16, 2020 |FiveThirtyEight 

According to the friend, Brinsley rang his ex-girlfriend, an Air Force reservist named Shaneka Thompson, to no avail. Alleged Cop Killer’s Blood-Soaked Screenplay |M.L. Nestel |December 24, 2014 |DAILY BEAST 

The left had long tried to resist it through a diverse mix of organizations, devoted to different goals, and all to no avail. The Left’s Answer to ALEC |Ben Jacobs |December 15, 2014 |DAILY BEAST 

Women have been trying to wield Internet shame against men for years now to little avail. Online Shaming Gives Creeps the Spotlight They Deserve |Samantha Allen |September 23, 2014 |DAILY BEAST 

In Germany, sex workers get to avail themselves of the same social-welfare infrastructure as all other German workers. Why It's Time to Legalize Prostitution |Cathy Reisenwitz |August 15, 2014 |DAILY BEAST 

This woke her husband, who questioned and tried to console her, to no avail. Knocking on Heaven's Door: True Stories of Unexplained, Uncanny Experiences at the Hour of Death |Patricia Pearson |August 11, 2014 |DAILY BEAST 

But such refuge, he knew, could avail him nothing if the bear should scent him out and search for him. The Giant of the North |R.M. Ballantyne 

I would gladly avail myself of your offer, but the Residency will be invested in less than an hour. The Red Year |Louis Tracy 

Age and usage were to be of no avail in bringing this wretched piece of workmanship up to the standard of the average. Antonio Stradivari |Horace William Petherick 

Neither kisses, nor words of comfort, nor the promise to return soon, were of any avail. The Life &amp; Letters of Peter Ilich Tchaikovsky |Modeste Tchaikovsky 

All these titles would avail but little were they not supported by immense riches. A Philosophical Dictionary, Volume 1 (of 10) |Franois-Marie Arouet (AKA Voltaire)