Using both my real name and a Wikipedia han­dle, I deleted the assertion from the article, only to watch it reappear. You Can Look It Up: The Wikipedia Story |Walter Isaacson |October 19, 2014 |DAILY BEAST 

“I would disappear in a puff of smoke and then reappear,” a former Dorothy recalled during her return to the Land of Oz. Follow the Yellow Brick Road…to North Carolina |Nina Strochlic |February 12, 2014 |DAILY BEAST 

Instantly, nearly $7 billion in value disappeared, only to reappear over the next several minutes. Welcome to the Anarchy Economy |Daniel Gross |April 23, 2013 |DAILY BEAST 

“They can disappear and reappear in an instant,” said one man who has travelled with the Al Qaeda in the Islamic Maghreb (AQIM). No Quick Fix For Mali: French Troops Can’t End Crisis |William Lloyd George |January 30, 2013 |DAILY BEAST 

Xi may reappear at any moment, putting some (though not all) the rumors to rest. China Roiled by Rumors and Questions About Absent Heir Apparent Xi Jinping |Melinda Liu |September 11, 2012 |DAILY BEAST 

It reappears during a relapse, and thus helps to distinguish between a relapse and a complication, in which it does not reappear. A Manual of Clinical Diagnosis |James Campbell Todd 

In an occasional horse the long-lost stripes of the zebra-like ancestor reappear. Man And His Ancestor |Charles Morris 

After Ariel, Ferdinand and Miranda should reappear; this time in a phase of glowing passion. The Life &amp; Letters of Peter Ilich Tchaikovsky |Modeste Tchaikovsky 

Before the exercises of the forenoon were concluded, she was summoned to see a visitor, and did not reappear before intermission. Alone |Marion Harland 

Chicot was bargaining for some horses, when he saw the monk reappear, carrying the saddles and bridles of the mules. Chicot the Jester |Alexandre Dumas, Pere