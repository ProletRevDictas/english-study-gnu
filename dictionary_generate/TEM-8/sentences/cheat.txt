This comes in part from the fact that the Cullinan has a cheat sheet. The first Rolls-Royce SUV has tricks that might actually justify its price tag |Dan Carney |October 5, 2020 |Popular-Science 

They were salt-packed and full of “preservatives” and that ghastly enemy, MSG, but more than anything, they were the tools of cheats. The Redemption of the Spice Blend |Jaya Saxena |September 10, 2020 |Eater 

“There should be a cheat sheet out there for what test to use when,” Wells said. Spit vs. Swab? Scientists say new studies support use of ‘saliva tests’ for COVID |Lee Clifford |September 5, 2020 |Fortune 

For example, when you’re gearing up for a big promotional launch, create a cheat sheet of pre-written social copy and send over several variations that fit different channels. Five content promotion strategies SaaS marketers should implement today |Izabelle Hundrev |August 28, 2020 |Search Engine Watch 

Using that formula, which is a bit of a cheat, e-commerce is now closer to 21%. E-commerce explodes: 45% growth in Q2 |Greg Sterling |August 19, 2020 |Search Engine Land 

Cheat, in other words—on God, on our fellow man, ultimately, on ourselves. McConaughey’s ‘Stand’—And Ours |James Poulos |December 5, 2014 |DAILY BEAST 

If a Queen did cheat, her crimes fade into insignificance compared to the extensive philandering engaged in by medieval monarchs. The Sex Life of King Richard III's Randy Great Great Great Grandfather |Tom Sykes |December 4, 2014 |DAILY BEAST 

Clients who are wary of online transactions are liable to see escorts with print ads as less likely to cheat or scam  them. The Importance of Adult Classifieds |Hazlitt |September 6, 2014 |DAILY BEAST 

Vennare adds that cheat days can occasionally do more harm than good. When Is It OK to Cheat? The Pros and Cons of Cheat Days |DailyBurn |July 14, 2014 |DAILY BEAST 

And if so, is it possible to “cheat” without feeling the effects or seeing them on the scale? When Is It OK to Cheat? The Pros and Cons of Cheat Days |DailyBurn |July 14, 2014 |DAILY BEAST 

He turned to Miller, and said haughtily in his imperfect English, “Did you see the cheat, you?” The Joyous Adventures of Aristide Pujol |William J. Locke 

And thirdly he knew that his adversary would cheat if he could and that his adversary suspected him of fraudulent designs. The Joyous Adventures of Aristide Pujol |William J. Locke 

She had submitted to giving up the salmon, but the devil himself should not cheat her out of her dessert. Skipper Worse |Alexander Lange Kielland 

I soon find out when they are trying to cheat me; then they come smirking and smiling with 'Guten Abis.' Skipper Worse |Alexander Lange Kielland 

The tongue can't cheat the brain, and right now reading is out of the question. My Wonderful Visit |Charlie Chaplin