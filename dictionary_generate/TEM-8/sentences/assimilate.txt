They had worried about being able to assimilate into a culture so different from the one they had left behind. ‘The Harness Maker’s Dream:’ The Unlikely Ranch King of Texas |Nick Kotz |September 20, 2014 |DAILY BEAST 

Their stories were told again and again in an attempt to assimilate the tragedy, to comprehend the incomprehensible. The Resilient City: New York After 9/11 |John Avlon |September 11, 2014 |DAILY BEAST 

As prejudices waned, it became easier and ultimately desirable for Jews to fully assimilate. The Ghost Hotels of the Catskills |Brandon Presser |August 25, 2014 |DAILY BEAST 

Our bodies have a tendency to assimilate to the cognitive enhancements of tea, which can eventually lead to addiction. Forget 5-Hour Energy: Tea Is a Better Buzz |Gregory Ferenstein |July 22, 2014 |DAILY BEAST 

The 21 percent of students whose parents are immigrants will have less of a chance to assimilate. The Wingnut War On Common Core Is A Plot To Destroy Public Schools |Caitlin Dickson |May 7, 2014 |DAILY BEAST 

But the Oriental we can't assimilate, for all our ostrich-like digestion, and what we can't assimilate we won't have. Ancestors |Gertrude Atherton 

We assimilate anything white so quickly it is a wonder an immigrant remembers the native way of pronouncing his own name. Ancestors |Gertrude Atherton 

At this moment he was in the act of despoiling both ancient and modern philosophy of all their wealth in order to assimilate it. Repertory Of The Comedie Humaine, Complete, A -- Z |Anatole Cerfberr and Jules Franois Christophe 

These gardens are rather like oriental flower-plots, but they assimilate well with the climate. Journal of a Voyage to Brazil |Maria Graham 

Poetry is unable, under pain of death or decay, to assimilate itself to morals or science. Charles Baudelaire, His Life |Thophile Gautier