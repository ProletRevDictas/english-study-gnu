I actually found it quite pleasurable, and it prepared me for this strange, gypsy lifestyle of an actor. Jena Malone’s Long, Strange Trip From Homelessness to Hollywood Stardom |Marlow Stern |December 22, 2014 |DAILY BEAST 

The family held together in the gypsy jet stream that is military life. Shaq, Year One |Charles P. Pierce |May 24, 2014 |DAILY BEAST 

The son of a schoolteacher and a bookkeeper, Hoskins had gypsy blood in him from his Romani grandmother. Remembering Bob Hoskins, the Burly British Star of ‘Who Framed Roger Rabbit,’ Who Died at 71 |Lorenza Muñoz |April 30, 2014 |DAILY BEAST 

With three others, they now constitute the San Miguel Five and play a combination of Afro-Latin, classical, and gypsy jazz. The Second Life of San Miguel de Allende |Michele Willens |February 26, 2014 |DAILY BEAST 

Her moniker in headlines quickly transformed to “Mystery Gypsy.” American Gypsies Are a Persecuted Minority That Is Starting to Fight Back |Nina Strochlic |December 22, 2013 |DAILY BEAST 

She was a thin, dark-eyed creature, with a gypsy face and a quantity of gray hair wound about on the top of her head. Country Neighbors |Alice Brown 

Her gypsy face shone radiant out of her black cloth hood, and Ronald's was no less luminous. Penelope's Experiences in Scotland |Kate Douglas Wiggin 

Besides a tarantass, drawn by good Siberian horses, will always go faster than a gypsy cart! Michael Strogoff |Jules Verne 

But the stars and the Gypsy brethren forbid the banns, so they part eternally. Lavengro |George Borrow 

Which is a pity; a Gypsy Quakeress would be a charming fancy. Lavengro |George Borrow