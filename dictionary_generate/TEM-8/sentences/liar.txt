“When you meet with the FBI, who I cross-examine and call liars all the time, you can imagine it was a very difficult meeting,” Silvert said. A prosecutor and police chief were adored in their community. Then their scheme unraveled. |Kim Bellware |December 3, 2020 |Washington Post 

Instead of labeling lie after lie, perhaps social media should focus on figuring out how to reduce the audience for liars. Twitter and Facebook warning labels aren’t enough to save democracy |Geoffrey Fowler |November 9, 2020 |Washington Post 

They figured if everyone was lying, at least they could elect a liar who claimed he’d lie for them. Political cynicism has given way to love in Christian America |jakemeth |October 28, 2020 |Fortune 

Fact-checking worked because, in theory, politicians could be shamed by being called out as liars. The RNC weaponized exhaustion |Zack Beauchamp |August 28, 2020 |Vox 

His eye is not on the facts at all, as the eyes of the honest man and of the liar are, except insofar as they may be pertinent to his interest in getting away with what he says. CNN fact-checked Trump’s RNC speech on air. It took 3 minutes. |German Lopez |August 28, 2020 |Vox 

The root of the word irony is in the Greek eironeia, “liar.” Up to a Point: They Made Me Write About Lena Dunham |P. J. O’Rourke |December 13, 2014 |DAILY BEAST 

Liar "I sometimes wish I could be anonymous and just walk down the street like everybody else." The Beyoncé Manifesto: Quotes on Nihilism and Feminism |Amy Zimmerman |December 12, 2014 |DAILY BEAST 

This is an agency that spied on the very committee investigating it and whose current director is an admitted liar. After Torture Report, Our Moral Authority As a Nation Is Gone |Nick Gillespie |December 11, 2014 |DAILY BEAST 

Their last hope for saving humanity, Eugene, turned out to be a liar with no idea how to stop the zombie virus. ‘Walking Dead’ Showrunner Scott Gimple Teases ‘Darker, Weirder’ Times Ahead |Melissa Leon |December 2, 2014 |DAILY BEAST 

What should have been a moment of reckoning for a selfish, serial liar instead ended with us pitying him. The Walking Dead’s ‘Self Help’: A Grim Show Displays Its Comedy Streak, and A Major Reveal |Melissa Leon |November 10, 2014 |DAILY BEAST 

While he grieved over the loss of our little one, you conceived a vile plot to 'get even,' Oh, you—liar! The Homesteader |Oscar Micheaux 

I let him go on, exulting in the discovery that he was a liar, for I knew that it pushed me a step towards recovery. Ancestors |Gertrude Atherton 

I am grateful—voyons—if anybody ever says Aristide Pujol is ungrateful, he is a liar. The Joyous Adventures of Aristide Pujol |William J. Locke 

But the Whig chiefs were not men to be duped by the professions of so notorious a liar. The History of England from the Accession of James II. |Thomas Babington Macaulay 

In vain you may know him to the core—know him a liar, a comedian—he manages always to get the better of you with his stories. The Nabob |Alphonse Daudet