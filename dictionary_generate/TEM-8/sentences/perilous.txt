We see this freedom isn’t free at all—it comes, sometimes, at perilous cost. Rebecca Hall's Passing Is a Complex, Moving Story About Racial Identity—and a Sundance Standout |Stephanie Zacharek |February 3, 2021 |Time 

Because many of these tech companies do not have unions, most of the workers who lead or participated in the protests were not labor activists, but workers responding to the perilous situation in which they found themselves. The Challenges Posed By COVID-19 Pushed Many Workers to Strike. Will the Labor Movement See Sustained Interest? |Abigail Abrams |January 17, 2021 |Time 

Here are some made-up game modes that make ghost hunting both more perilous and more ridiculous. Are you a ‘Phasmophobia’ pro? Here are some alternate rules to keep the scares fresh. |Elise Favis |January 11, 2021 |Washington Post 

Both men have a good sense for understanding their players and maintaining optimism through perilous times. Pete Carroll knows 7-9 can be the start of something big |Les Carpenter |December 18, 2020 |Washington Post 

The Justice Department is suing Google — but it’s the government’s power to police Big Tech that’s on trialThe Google lawsuit comes at a perilous time for Paxton, in particular. Texas leads Republican attorneys general in new antitrust lawsuit against Google, targeting its advertising empire |Tony Romm |December 16, 2020 |Washington Post 

Satirists occupy a perilous position—to skewer dogma and cant, and to antagonize the establishment while needing its protection. Harry Shearer on The Dangerous Business of Satire |Lloyd Grove |January 8, 2015 |DAILY BEAST 

It is a tricky and perilous path, but there are no realistic alternatives. There’s Only One Way to Beat ISIS: Work With Assad and Iran |Leslie H. Gelb |October 18, 2014 |DAILY BEAST 

But if Mare Nostrum ends, it could be a tragic day for migrants making the perilous crossing. Are European Rescuers Enticing Migrants to Their Deaths? |Barbie Latza Nadeau |September 7, 2014 |DAILY BEAST 

Unfortunately, this can result in sending a well-intentioned but perilous message. 'Genie, You're Free': Suicide Is Not Liberation |Russell Saunders |August 12, 2014 |DAILY BEAST 

And now Persecuted holds up a mirror to the perilous situation facing increasingly disenfranchised Christians in modern America. ‘Persecuted’ Is the Christian Right’s Paranoid Wet Dream |Candida Moss |July 22, 2014 |DAILY BEAST 

The road had been built for bringing down lumber, and for six miles it was at perilous angles. Ramona |Helen Hunt Jackson 

This rather is, I should deem, the more perilous, and a plainer and better object for philosophical attack. Notes and Queries, Number 177, March 19, 1853 |Various 

On their perilous journey an attack of measles increased their discomforts. Life of Richard Trevithick, Volume II (of 2) |Francis Trevithick 

Moreover, when we come to pay off, the crew will receive a bonus, in consideration of the long and perilous voyage. Skipper Worse |Alexander Lange Kielland 

He had access to the ocean only in a latitude in which navigation is, during a great part of every year, perilous and difficult. The History of England from the Accession of James II. |Thomas Babington Macaulay