In front of this strange structure are two blank-faced, well-dressed models showing off the latest in European minimalism. New York’s Most Tragic Ghost Loves Minimalist Swedish Fashion |Nina Strochlic |January 8, 2015 |DAILY BEAST 

As the protagonist gets herself off in front of her impotent husband, she moans “Oh, Gronky.” ‘A Gronking to Remember’ Speed Read: 8 Naughtiest Bits |Emily Shire |January 7, 2015 |DAILY BEAST 

Granted, James is in an office in the Pentagon, and not on the front lines. Pentagon Doesn’t Know How Many People It’s Killed in the ISIS War |Nancy A. Youssef |January 7, 2015 |DAILY BEAST 

The next phase of the trial consists of vaccinating Ebola workers on the front lines. The Race for the Ebola Vaccine |Abby Haglage |January 7, 2015 |DAILY BEAST 

Hmm, who are these people standing in front of the machines at the gym, neither occupying them nor not occupying them? How to Survive the New Year ‘Gympocalypse’ |Tim Teeman |January 6, 2015 |DAILY BEAST 

Off went the officers again, some distance to the front, and then back again to their men, and got them on a little further. Blackwood's Edinburgh Magazine, No. CCCXXXIX. January, 1844. Vol. LV. |Various 

The doors (Indian bungalows have hardly any windows, each door being half glass) were open front and back. The Red Year |Louis Tracy 

She got up and stood in front of the fire, having her hand on the chimney-piece and looking down at the blaze. Confidence |Henry James 

Then they all passed out through the great front door to the wide old veranda. The Boarded-Up House |Augusta Huiell Seaman 

He saw a large building, in front of which were long, slender strips of shining steel. Squinty the Comical Pig |Richard Barnum