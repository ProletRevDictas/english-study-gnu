For instance, researchers recently revamped a court summons form and sent text reminders to get more people to attend mandatory court appointments in New York City. Nudge theory’s popularity may block insights into improving society |Sujata Gupta |February 16, 2022 |Science News 

They were more likely to receive jury summonses by mail because many didn’t move as frequently as lower-income renters, whose jury notices often went to outdated addresses. It's a 'Nightmare Being Replayed' as a Cop Faces Trial in George Floyd's Death |Janell Ross |March 18, 2021 |Time 

About 45 minutes past our interview time, the studio flack summons me. Brie Larson’s Hollywood Transformation |Marlow Stern |December 29, 2014 |DAILY BEAST 

They say, ‘If we catch you out there, we’re going to write you a summons. He Faces Jail for Rescuing Baby Eagles |Michael Daly |November 2, 2014 |DAILY BEAST 

The cops brought the same charge of simple assault in releasing Palmer with Complaint-Summons 000728. Ray Rice Should Have Remembered His 'Kindness' Anti-Bullying Wristband |Michael Daly |September 10, 2014 |DAILY BEAST 

On July 8, a summons was sent to Zhang alerting her to the lawsuit, and on July 11, Nan Shi lost her job. Female Yahoo Exec Accused of Sex Abuse Fires Back |Nina Strochlic |July 18, 2014 |DAILY BEAST 

This summons all the proximate Beyoncé voters, as we reply in a full-throated roar, “ALLLLLL THE SINGLE LAAAAADIES!” Getting to Know the ‘Beyoncé Voter’ |Kelly Williams Brown |July 7, 2014 |DAILY BEAST 

While Louis was reading these dispatches, he received a summons from Elizabeth, to attend her immediately. The Pastor's Fire-side Vol. 3 of 4 |Jane Porter 

She had sent away her attendants, bolted the door against her mother, and sat waiting her summons. Elster's Folly |Mrs. Henry Wood 

I am cast down by grief at this evil news, and the summons from Court has brought me in all haste from Milan. St. Martin's Summer |Rafael Sabatini 

Her gentle summons was answered by a tall powdered footman in blue and silver livery. The World Before Them |Susanna Moodie 

Vergniaud, notwithstanding the terrific agitations of the hour, immediately attended the summons of Madame Roland. Madame Roland, Makers of History |John S. C. Abbott