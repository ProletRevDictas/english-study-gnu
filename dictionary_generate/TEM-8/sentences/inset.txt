There was often an inset of a leering, grinning man holding a cigar observing the event. Beer bottles and cans hidden behind the drywall? I’ll drink to that. |John Kelly |September 22, 2021 |Washington Post 

Its controls are pretty sparse, with buttons for playback control and voice assistant access, and inset into the oh-so-slightly indented rubber top, making me prefer using my phone to adjust it rather than touching the Roam itself. Sonos’ Roam Is a Pretty, Portable and Pricey Speaker That Could Enhance Your Summer Plans |Patrick Lucas Austin |July 6, 2021 |Time 

The jewelry—rock crystals inset in silver—was bold but not ostentatious thanks to its antique finish and heirloom quality. Milan Fall Fashion Week 2012: Raf Simons’s Last Collection at Jil Sander |Robin Givhan |February 25, 2012 |DAILY BEAST 

The intricate perforations of the lamp were inset with colored glass, and the result was a subdued and warm illumination. Dope |Sax Rohmer 

No helicopters swung their blades above; there were only the bulge of a conning tower and the heavy inset glasses of the lookouts. Astounding Stories, May, 1931 |Various 

She was twisting at an inset handle around which faint lines indicated the door edge. Valley of the Croen |Lee Tarbell 

Jason asked, handing the outcast a flat gold case inset with a single large diamond. Deathworld |Harry Harrison 

Randall saw that it was lit by squares inset in the walls that glowed with crimson light. Astounding Stories, April, 1931 |Various