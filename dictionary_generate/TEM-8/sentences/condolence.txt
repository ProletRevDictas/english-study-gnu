Your entire world has crumpled, and without the benefit of traveling to see people and receive their personal condolences, you are quite naturally continuing to grieve. Ask Amy: Adopted sister keeps birth family a secret |Amy Dickinson |January 31, 2021 |Washington Post 

Each stopped to offer condolences to Miller’s wife, Patti, and his one of his daughters, Melanie. Maryland Senate President Miller lies in state: ‘Lion of the Senate’ |Ovetta Wiggins |January 22, 2021 |Washington Post 

I had assumed that he would at least have sent some form of condolences when she died. Miss Manners: Former colleague’s omission is puzzling |Judith Martin, Nicholas Martin, Jacobina Martin |January 22, 2021 |Washington Post 

We also reached out to the family on multiple occasions afterward to again offer condolences and seek an opportunity to discuss what happened. A Temp Worker Died on the Job After FedEx Didn’t Fix a Known Hazard. The Fine: $7,000. |by Wendi C. Thomas, MLK50: Justice Through Journalism |December 23, 2020 |ProPublica 

The pre-2020 formula for dealing with death online meant memorializing the Facebook account of the deceased, maybe opening an online condolence book with a funeral home, perhaps a GoFundMe page to raise money for expenses. The way we express grief for strangers is changing |Tanya Basu |December 3, 2020 |MIT Technology Review 

To Hitchcock, this is not a sweet wire from an old colleague but a condolence letter on the occasion of his own death. Alfred Hitchcock’s Fade to Black: The Great Director’s Final Days |David Freeman |December 13, 2014 |DAILY BEAST 

The letters of condolence when she died in March at the age of 87 included one from an African American who had become president. Is Al Sharpton Running New York City Hall From the White House? |Michael Daly |November 7, 2014 |DAILY BEAST 

She rarely dropped names of her famous friends, except Prince Charles', who issued a public note of condolence after her death. Melissa Rivers: Life After Joan—A Funny, Moving Celebration on a Special 'Fashion Police' |Tim Teeman |September 20, 2014 |DAILY BEAST 

Before Mr. Chen left the museum, he signed a condolence book by the corner library. The Tiananmen Square Museum That’s Shocking China’s Tourists |Brendon Hong |May 30, 2014 |DAILY BEAST 

This year, the condolence book from the June 4th Museum will be burned in a ceremony of remembrance. The Tiananmen Square Museum That’s Shocking China’s Tourists |Brendon Hong |May 30, 2014 |DAILY BEAST 

In paying your visits of condolence, show, by your own quiet gravity, that you sympathize in the recent affliction of your friend. The Ladies' Book of Etiquette, and Manual of Politeness |Florence Hartley 

Pay visits, both of condolence and congratulation, within a week after the event which calls for them occurs. The Ladies' Book of Etiquette, and Manual of Politeness |Florence Hartley 

Letters of Condolence are exceedingly trying, both to read and to write. The Ladies' Book of Etiquette, and Manual of Politeness |Florence Hartley 

It lowered its voice in passing and made its calls of condolence in dark clothes and a general air of gloom. The Amazing Interlude |Mary Roberts Rinehart 

A few words of condolence and sympathy were offered, and they separated to prepare for dinner. Newton Forster |Captain Frederick Marryat