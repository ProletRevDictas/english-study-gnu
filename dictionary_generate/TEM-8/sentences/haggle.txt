Haggle over the numbers and details, attack the Senate for their failure to pass a budget, and then cut the best deal possible. What Can the GOP Accomplish During Obama's 2nd Term? |David Frum |January 10, 2013 |DAILY BEAST 

Finally, the Hagel haggle highlighted a critical lesson for the pro-Israel community. Why The Hagel Trial Balloon Popped |Gil Troy |December 25, 2012 |DAILY BEAST 

The Republican rep wants lawmakers to extend the Bush tax cuts, and haggle over the rest later. The Independent Rundown, November 28 |Matthew DeLuca |November 29, 2012 |DAILY BEAST 

We're just left to haggle over price: Should the successful pay forward 36% of their success or 39% or 28% or what. Why "You Didn't Build That" Stings the Successful |David Frum |July 27, 2012 |DAILY BEAST 

He would haggle in a bargain for a shilling, and economize in things beneath a wise man's notice or consideration. Blackwood's Edinburgh Magazine, No. CCCXXXIX. January, 1844. Vol. LV. |Various 

After he had married her, he'd sell out this pile of junk and let somebody else haggle with the Injuns and cowpunchers. Mystery Ranch |Arthur Chapman 

To have to refuse one who has made you a gift of her beauty a trifling article; to haggle over such matters, like a miser! Toilers of the Sea |Victor Hugo 

Hence there was time for the United States to consider the question of a purchase and to haggle a little over the price. Over the Rocky Mountains to Alaska |Charles Warren Stoddard 

Otherwise people would get silly ideas and begin to haggle over the price. Bunker Bean |Harry Leon Wilson