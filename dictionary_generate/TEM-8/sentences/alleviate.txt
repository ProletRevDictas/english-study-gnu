None of these alleged fixes simultaneously alleviate the technology’s true physical and digital harms. Covid-19 has led to a worrisome uptick in the use of electronic ankle monitors |Amy Nordrum |October 8, 2020 |MIT Technology Review 

By contrast, states that regularly conduct elections by mail have much more specific guidelines to alleviate overcrowding. More States Are Using Ballot Drop Boxes. Why Are They So Controversial? |Nathaniel Rakich (nathaniel.rakich@fivethirtyeight.com) |October 5, 2020 |FiveThirtyEight 

Below, Kessler and other grief experts, human resources executives, mental-health professionals, and physicians explain how employers can help their workers—and their companies—alleviate this year’s crushing burden of grief. 5 better ways to help your employees mourn at the office |Maria Aspan |September 27, 2020 |Fortune 

I don’t have all the answers, but part of the solution no doubt is alleviating pressure for parents while finding at-home learning methods that are actually effective for kids. How parents can add purpose and structure to their kids’ online learning |Tara Chklovski |August 20, 2020 |Quartz 

More people than ever are paying attention to the county because of its role with public health and its vast reserves that many hoped could be deployed to alleviate the mental health and homeless crisis in San Diego. Politics Report: East County GOP Rivals Court the Libs |Scott Lewis |June 27, 2020 |Voice of San Diego 

Practicing yoga, studies show, can alleviate symptoms of depression and anxiety. 9 Ways to Cope With Seasonal Affective Disorder |DailyBurn |December 5, 2014 |DAILY BEAST 

Nor will it alleviate the life-threatening concerns of those living in the conflict zone. Middle East Goes Monty Python on ISIS |Dean Obeidallah |October 29, 2014 |DAILY BEAST 

“We can use rooftop solar to alleviate areas where supply is restrained,” he said. Panel Discussion |The Daily Beast |September 8, 2014 |DAILY BEAST 

Most of them remain chained to their smartphones, scrutinizing the market to alleviate deal-making withdrawal symptoms. The Hell of the Hamptons: Why the Exclusive Hotspot Is a Mind-Numbing Drag |Robert Gold |August 18, 2014 |DAILY BEAST 

After a cocktail of PTSD meds failed to alleviate his symptoms, Kiernan says he was looking for a way out. Why Did America’s Only Pot Researcher Suddenly Get Fired? |Abby Haglage |July 10, 2014 |DAILY BEAST 

They require, and in many instances they merit, all that can be done to alleviate a situation of servitude. The Ladies' Book of Etiquette, and Manual of Politeness |Florence Hartley 

I had no conception of sorrows she could not alleviate; and I remember thinking—foolish child that I was! Alone |Marion Harland 

Alas, it may be, she will impose on herself some rigid penance, in the hope that God may alleviate the sufferings of her brother! My Ten Years' Imprisonment |Silvio Pellico 

There are compensating circumstances which should alleviate our sorrow. The Transformation of Job |Frederick Vining Fisher 

The cough persisted in spite of all efforts of specialists to alleviate it. The Eugenic Marriage, Vol. 3 (of 4) |W. Grant Hague