That finding could guide treatments for future astronauts en route to such faraway places as Mars. Space travel may harm health by damaging cells’ powerhouses |Jack J. Lee |February 5, 2021 |Science News For Students 

As my unused passport gathers ever more dust, food’s power to summon faraway places to my Vermont kitchen has only grown. Take a culinary journey with these new travel-inspired cookbooks |Jen Rose Smith |January 22, 2021 |Washington Post 

Access to knowledge can be shifted away from the faraway monitors in our pocket, to its relevant real-world location. How a Software Map of the Entire Planet Could Change the World Forever |Aaron Frank |December 27, 2020 |Singularity Hub 

That could mean a whole new income stream for these travel industry gig workers, who will likely see their in-person customer base return — but now have the resources to cater to faraway clients as well. Your Guide for When Travel Returns |Daniel Malloy |December 27, 2020 |Ozy 

Traveling to faraway lands can boost creativity and, when you’re back at work, heighten your productivity. Stuck at home? Trick your brain into treating a staycation like the real thing. |Eleanor Cummins |December 22, 2020 |Popular-Science 

But for these clerks and secretaries, war is a faraway, almost abstract concept. ‘Zero Motivation’: the Funny Side of the IDF |Melissa Leon |December 8, 2014 |DAILY BEAST 

Demand is highest in places like China and Southeast Asia, where sudden wealth is fueling the urge to travel to faraway places. The New Fear of Flying After MH370 |Clive Irving |March 31, 2014 |DAILY BEAST 

Through his art, Bailey drew attention to poverty and despair in faraway places. David Bailey’s ‘Stardust’ Shows a Keen Eye for Fine Faces |Chloë Ashby |February 8, 2014 |DAILY BEAST 

Some wandered in a genial trance wearing the faraway, slightly shell-shocked look of the recently colonically irrigated. My Week At An Austrian Fat Camp |Owen Matthews |October 27, 2013 |DAILY BEAST 

The many Nick Palmers, all buried far too young, killed in our faraway wars. War Is the New Peace: American Vets Reflect on Syria |John Kael Weston |September 10, 2013 |DAILY BEAST 

She jist looked at ye wi' her big black e'en sae vexed-like and faraway lookin', an' never spoke hardly. The Underworld |James C. Welsh 

There were times when she babbled of faraway scenes, of Williamsburg and her old home, of the streets of Norfolk and Richmond. A Virginia Scout |Hugh Pendexter 

However, their guide, mentor, and boss had a faraway look in his eye—seemed impatient to get going. David Lannarck, Midget |George S. Harney 

But Molly did not laugh, as he himself had laughed on that faraway, dreamlike evening in his rooms. Molly Make-Believe |Eleanor Hallowell Abbott 

As he rode he sang, while he sang he worshiped, but the god he tried to glorify was a dim and faraway mystery. Freckles |Gene Stratton-Porter