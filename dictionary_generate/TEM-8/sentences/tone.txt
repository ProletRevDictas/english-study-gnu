It’s very difficult to imagine the first installment in a studio comedy based on an original concept, especially a comedy that has the tone of this one, coming out today, at least in theaters. The essential kindness of Bill and Ted |Alissa Wilkinson |August 28, 2020 |Vox 

US developer HireVue says its software speeds up the hiring process by 90 percent by having applicants answer identical questions and then scoring them according to language, tone, and facial expressions. Algorithms Workers Can’t See Are Increasingly Pulling the Management Strings |Tom Barratt |August 28, 2020 |Singularity Hub 

Hamilton had spent decades prior to 1980 sifting through weather station data for hints of the lowest atmospheric tones. Global Wave Discovery Ends 220-Year Search |Charlie Wood |August 13, 2020 |Quanta Magazine 

The tone of what I’m told is the Silent Generation really changed in the 60s. What Are 7 Words To Describe The Silent Generation? |Candice Bradley |August 4, 2020 |Everything After Z 

Just as AI systems can be trained to tell the difference between a picture of a dog and one of a cat, they can learn to differentiate between an angry tone of voice or facial expression and a happy one. Cars Will Soon Be Able to Sense and React to Your Emotions |Vanessa Bates Ramirez |July 29, 2020 |Singularity Hub 

Music is a huge part of the tone of Black Dynamite overall—going back to the original 2009 movie on which the series is based. ‘Black Dynamite’ Presents Police Brutality: The Musical |Stereo Williams |January 9, 2015 |DAILY BEAST 

Although Huckabee's condescending tone - like that of an elementary school history teacher - makes it difficult to take seriously. Huckabee 2016: Bend Over and Take It Like a Prisoner! |Olivia Nuzzi |January 8, 2015 |DAILY BEAST 

“Call me when the plane leaves the ground,” she said, in a tone that implied she knew her husband well. Mario Cuomo, a Frustrating Hero to Democrats, Is Dead at 82 |Eleanor Clift |January 2, 2015 |DAILY BEAST 

He has struck a promising tone these last few days with his rhetoric about trying to “see each other.” Memo to Cops: Criticisms Aren’t Attacks |Michael Tomasky |December 28, 2014 |DAILY BEAST 

Ramos would help set the tone of the day when he greeted the arriving students outside the school. In The Shadow of Murdered Cops |Michael Daly |December 26, 2014 |DAILY BEAST 

That which is called nasality is caused by the failure of the tone to reach freely the anterior cavities of the nares. Expressive Voice Culture |Jessie Eldridge Southwick 

Drone: the largest tube of a bag-pipe, giving forth a dull heavy tone. Gulliver's Travels |Jonathan Swift 

And once more, she found herself desiring to be like Janet--not only in appearance, but in soft manner and tone. Hilda Lessways |Arnold Bennett 

What the ear hears is the fundamental pitch only; the overtones harmonize with the primary or fundamental tone, and enrich it. Expressive Voice Culture |Jessie Eldridge Southwick 

This may be done by taking the humming tone and bringing to bear upon it a strong pressure of energy. Expressive Voice Culture |Jessie Eldridge Southwick