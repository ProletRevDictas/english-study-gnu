The fewer tests we do, the fewer people have that bonus push to really stay in lockdown. The CDC’s new COVID-19 testing guidelines could make the pandemic worse |Sara Kiley Watson |August 27, 2020 |Popular-Science 

Okay, fair disclaimer, this last step is more of a bonus step. Testimonial link building: Using real experiences for success |Rameez Ghayas Usmani |July 15, 2020 |Search Engine Watch 

At Facebook, employees were paid bonuses of up to $15,000, just to live near its headquarters in Menlo Park, California. Virtually indispensable: How work-from-home skills became a boon for the post-office reality |Shareen Pathak |July 10, 2020 |Digiday 

There is the added bonus that it will also provide any necessary feedback. How to win at SEO with FAQ schema markup |Abhishek Shah |June 12, 2020 |Search Engine Watch 

How students perform on these tests can determine school funding, teacher bonuses, whether a principal is fired, and even whether a school will be shut down. America’s Math Curriculum Doesn’t Add Up (Ep. 391) |Steven D. Levitt |October 3, 2019 |Freakonomics 

And as a bonus, they send home more than $20 billion in remittances each year. Why Mexicans Are Enraged by Obama’s Big Tuesday Meeting |Ruben Navarrette Jr. |January 6, 2015 |DAILY BEAST 

A recent U.S. study found men get a “daddy bonus” —employers seem to like men who have children and their salaries show it. How Good Dads Can Change the World |Gary Barker, PhD, Michael Kaufman |January 6, 2015 |DAILY BEAST 

When they steal things, they want to get all the bonus points. The Insane $11 Billion Scam at Retailers’ Return Desks |M.L. Nestel |December 19, 2014 |DAILY BEAST 

Prosecutors say the two then promised that employee a $10,000 bonus, which was promptly paid. Obama’s Golf Buddy May Be a ‘Hostile Witness’ in Chicago Corruption Case |Ben Jacobs |December 3, 2014 |DAILY BEAST 

And bonus points for the school bus that burst into flames with the comic timing of a Simpsons gag. The Walking Dead’s ‘Self Help’: A Grim Show Displays Its Comedy Streak, and A Major Reveal |Melissa Leon |November 10, 2014 |DAILY BEAST 

Moreover, when we come to pay off, the crew will receive a bonus, in consideration of the long and perilous voyage. Skipper Worse |Alexander Lange Kielland 

Hale bought all of old Judd's land, formed a stock company and in the trade gave June a bonus of the stock. The Trail of the Lonesome Pine |John Fox, Jr. 

Hollister's crew, working on a bonus for work performed, kept the bolts of cedar gliding down the chute. The Hidden Places |Bertrand W. Sinclair 

Nor was Jimmy at all displeased when he found at the end of the week that he had been given a nice bonus for his work. The Flying Reporter |Lewis E. (Lewis Edwin) Theiss 

Agents were appointed in each likely State, with sub-agents who were paid a bonus on every actual settler. The Canadian Dominion |Oscar D. Skelton