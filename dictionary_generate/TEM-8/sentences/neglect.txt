In addition to facing neglect and abuse, Stewart lost her access to counseling because of the move, she said. Her Stepfather Admitted to Sexually Abusing Her. That Wasn’t Enough to Keep Her Safe. |by Nadia Sussman |September 18, 2020 |ProPublica 

The result is an unconscionable neglect of some of the world’s most vulnerable people. Why I signed a letter to Ivanka Trump urging her to protect human trafficking survivors |jakemeth |September 2, 2020 |Fortune 

These skills develop poorly in young kids who face trauma, such as physical abuse or neglect. Puberty may reboot the brain and behaviors |Esther Landhuis |August 27, 2020 |Science News For Students 

Instead, state inspectors were told to focus on infection control and “immediate jeopardy” complaints linked to allegations of resident abuse, neglect or death. Confirmed Nursing Home Complaints Plummet During Pandemic |Jared Whitlock |August 25, 2020 |Voice of San Diego 

Her death was its own tragedy, a function of the medical neglect of the elderly that has become all too common in our nation. Departure from convention—mom, baseball, the postal worker, and patriotism |jakemeth |August 19, 2020 |Fortune 

“Too often the injustices neglect nameless faces and stories,” Dandolo writes in an email. Of Cuban Spies, a Baby, and a Filmmaker: The Strange Tale of the Cuban Five |Nina Strochlic |December 28, 2014 |DAILY BEAST 

Those who neglect or mistrust him may be punished--indeed may deserve to be. Will the Real Jim Palmer Please Stand Up |Tom Boswell |September 27, 2014 |DAILY BEAST 

As Victoria spoke, my heart broke as it does every time I hear stories of patient neglect. Why Your Doctor Feels Like a 'Beaten Dog' |Daniela Drake |September 11, 2014 |DAILY BEAST 

Police in Hampshire must now decide whether to extradite the Kings back to England and file kidnapping and neglect charges. Desperate Parents Arrested After Fleeing Britain For Other Treatment Options for Son in Europe |Barbie Latza Nadeau |September 2, 2014 |DAILY BEAST 

Now their son has been taken from them and they face criminal charges of neglect and child endangerment. Desperate Parents Arrested After Fleeing Britain For Other Treatment Options for Son in Europe |Barbie Latza Nadeau |September 2, 2014 |DAILY BEAST 

I think that there has been neglect and laxity in the matter of not driving out the Japanese. The Philippine Islands, 1493-1898, Volume XX, 1621-1624 |Various 

“Ill-usage” expresses the date of the death of Columbus in 1506, as he died in great neglect. Assimilative Memory |Marcus Dwight Larrowe (AKA Prof. A. Loisette) 

He could not complain of the neglect of mankind, or of the ingratitude of those he served. Blackwood's Edinburgh Magazine, No. CCCXXXIX. January, 1844. Vol. LV. |Various 

The position was irremediable; Tom's neglect and inefficiency were established beyond question. The Wave |Algernon Blackwood 

But in spite of this the neglect rankled, and from that day he was no longer the blindly devoted follower of Napoleon. Napoleon's Marshals |R. P. Dunn-Pattison