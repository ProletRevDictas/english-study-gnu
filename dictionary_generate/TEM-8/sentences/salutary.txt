The Sussex’s ferocious response to such intrusions has been remarkable, and a salutary reminder to the press that Harry and Meghan will go to extreme measures to protect their children’s privacy. How Harry and Meghan Made Sure Archie and Lilibet Grow Up in Total Privacy |Tom Sykes |October 8, 2021 |The Daily Beast 

Nevertheless, it is not surprising that a new generation seeks to reclaim something salutary from a shameful era. ‘Generation War’ Lets World War II Germans Off Too Easily |Jack Schwartz |January 26, 2014 |DAILY BEAST 

After all, not committing fouls is a good, even salutary, thing. The GOP’s Three Fiscal Lies |Michael Tomasky |March 23, 2013 |DAILY BEAST 

The exposure from Girls surely will be salutary by getting OCD the disease out in the open. ‘Girls’ Shows Us the Real OCD With Hannah’s Brutal Q-Tip Scene |Kent Sepkowitz |March 11, 2013 |DAILY BEAST 

But this salutary revolution, like so many revolutions, overstepped, and resulted in the Great Inversion. Why Can't We Talk About Culture? |Gil Troy |August 3, 2012 |DAILY BEAST 

But Cathcart also thought this was a salutary day for Fleet Street and the British Press. Rupert Murdoch’s Stunning Exit: A ‘Saigon Moment’ |Peter Jukes |July 21, 2012 |DAILY BEAST 

This is a salutary rule of the law, which the courts everywhere do not hesitate to enforce. Putnam's Handy Law Book for the Layman |Albert Sidney Bolles 

I hope having a younger sister, and outgrowing baby charms may be salutary. The Daisy Chain |Charlotte Yonge 

In spite of herself, Marguerite got accustomed to this new existence, whose salutary effects she already realized. Camille (La Dame aux Camilias) |Alexandre Dumas, fils 

The Church adjudged Simone heretic, and condemned her for salutary penance to the bread of suffering and the water of affliction. The Merrie Tales Of Jacques Tournebroche |Anatole France 

I was very much surprised to find a planet where Nature had not forgotten this salutary provision. Urania |Camille Flammarion