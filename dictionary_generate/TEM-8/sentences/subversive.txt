The TV veteran — who as someone who worked on Fox’s “Married … With Children” is familiar with subversive TV comedy — said he understands the motivation for bringing on someone like Musk. Elon Musk is being brought in to save SNL’s sagging ratings. He could sink the show in other ways. |Steven Zeitchik |May 7, 2021 |Washington Post 

In an era when audiences have seen the Wicked Witch of the West transformed into an animal rights activist and Harley Quinn depicted as a sympathetic anti-hero, not every villain needs to be recast as a misunderstood subversive. Her crazy driving is a key element of Cruella de Vil’s evil. Here’s why. |Genevieve Carpio |April 2, 2021 |Washington Post 

His impersonation saw him charged as a subversive, and he had to be rescued by the American vice consul. 12 Crazy True Stories About Children’s Authors |Kate Bartlett |April 1, 2021 |Ozy 

At the same time, I was learning about science and reading subversive people like Bertrand Russell. The Charmed Life of Frank Wilczek - Issue 98: Mind |Nell Freudenberger |March 17, 2021 |Nautilus 

There are fights, resistance, and subversive behaviors, but through all that, there is a broad commonality. Celebrating a long life with HIV |Rob Watson |December 11, 2020 |Washington Blade 

Submission is less a novel of ideas than a political book, and of the most subversive kind. Houellebecq’s Incendiary Novel Imagines France With a Muslim President |Pierre Assouline |January 9, 2015 |DAILY BEAST 

Miller traces his irreverent and subversive streak to a psychedelic experience during the particularly sweltering summer of 1991. DJ Spooky Wants You To Question Everything You Know About Music, Technology, and Philosophy |Oliver Jones |December 27, 2014 |DAILY BEAST 

Of course, you can read this just as a brilliant, subversive coda to a horror movie. Grief: The Real Monster in The Babadook |Tim Teeman |December 19, 2014 |DAILY BEAST 

Hipgnosis were at their most subversive when taking photos of the band. The Golden Age of Rock Album Covers |Ted Gioia |December 5, 2014 |DAILY BEAST 

These subversive narratives were not the solution I sought to the dissonance between my expected and actual college experience. Freshman Year Sucks—and That’s OK |Eleanor Hyun |November 12, 2014 |DAILY BEAST 

The worthy man is too loyal to the set rules of his acknowledged leaders, to harbour a notion so subversive and dangerous. A Cursory History of Swearing |Julian Sharman 

His lofty realism was subversive of popular superstitions, when logically carried out. Beacon Lights of History, Volume I |John Lord 

It gives assurance, at least, that no particularly wild schemes or subversive changes shall be made. Spanish Life in Town and Country |L. Higgin and Eugne E. Street 

No question was decided in the academy without the opinion of Aristotle, though it was often subversive of that of Plato. Great Men and Famous Women. Vol. 3 of 8 |Various 

After all, lefthandedness was impious in religion, subversive to discipline in military affairs and unlisted in business. Seeing Things at Night |Heywood Broun