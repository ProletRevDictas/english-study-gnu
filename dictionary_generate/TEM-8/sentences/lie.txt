They will learn that deadly incompetence, based on lies and lunacy and costing countless lives, means nothing. There’s nothing conservative about CPAC |Michael Gerson |February 25, 2021 |Washington Post 

That woman from George-a is foolish, full of fantasy and lies. Style Invitational Week 1425: Picture this — a cartoon caption contest |Pat Myers |February 25, 2021 |Washington Post 

Gilbert was given a lie detector test along with another police officer, who considered himself a friend of Lewis’ — the same officer who would call me many years later. The Murder Chicago Didn’t Want to Solve |by Mick Dumke |February 25, 2021 |ProPublica 

Another that comes to mind is how the insurrection contributed to fringe conservative media having to reckon with the fact that spreading lies about a stolen election has real-world impacts. How Has The Nation Changed Since The Insurrection At The Capitol? |Micah Cohen (micah.cohen@fivethirtyeight.com) |February 11, 2021 |FiveThirtyEight 

A lot of the problems that we’re facing are the lies that probably are going to be protected by the First Amendment. Twenty-Six Words Created the Internet. What Will It Take to Save It? |Stephen Engelberg |February 9, 2021 |ProPublica 

My doctor insisted that once I filed this piece I lie down on my bed and not get out. I Tried to Warn You About Sleazy Billionaire Jeffrey Epstein in 2003 |Vicky Ward |January 7, 2015 |DAILY BEAST 

I lie and nod my head yes while wiping the tears on my gray fleece sleeve. You’re Never ‘Cured’ of an Eating Disorder |Carrie Arnold |December 20, 2014 |DAILY BEAST 

“I knew it was a lie from the beginning,” Patrick told WLOS. Beaten By His Church for Being Gay |Justin Jones |December 16, 2014 |DAILY BEAST 

It is, in fact, legal for police to lie to suspects during interrogations. How the U.S. Justice System Screws Prisoners with Disabilities |Elizabeth Picciuto |December 16, 2014 |DAILY BEAST 

But he drew me close  And he swallowed me down,  Down a dark slimy path  Where lie secrets that I never want to know […]. On Torture, Chuck Johnson & Sondheim |James Poulos |December 13, 2014 |DAILY BEAST 

They are ovoid in shape, and lie in pairs, end to end, often forming short chains. A Manual of Clinical Diagnosis |James Campbell Todd 

However this be, it is hard to say that these fibs have that clear intention to deceive which constitutes a complete lie. Children's Ways |James Sully 

The "bad form" of telling a lie to the head-master is a later illustration of the same thing. Children's Ways |James Sully 

The word of the law shall be fulfilled without a lie, and wisdom shall be made plain in the mouth of the faithful. The Bible, Douay-Rheims Version |Various 

The hut was barely high enough to let him sit up, and long enough to let him lie down—not to stretch out. The Giant of the North |R.M. Ballantyne