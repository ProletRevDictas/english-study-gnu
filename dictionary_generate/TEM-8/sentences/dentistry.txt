However, Moradian-Oldak expresses reservations about its practical use in dentistry any time soon. This new synthetic tooth enamel is even harder than the real thing |Lauren J. Young |February 8, 2022 |Popular-Science 

The family scraped together enough money to send him to the private Shifa University in Kabul to study dentistry. The story of an Afghan man who fell from the sky |Gerry Shih, Niha Masih, Dan Lamothe |August 26, 2021 |Washington Post 

San Antonio dentist Joshua Austin authored a controversial editorial for a dentistry publication on this issue. Inside American dentistry’s identity crisis |Rachel Schallom |October 27, 2020 |Fortune 

Bolden’s grandfather had his own dentistry practice, and she was specifically interested in how businesses can reduce their carbon footprint. The Mom, Student … and Breakout Rapper |Joshua Eferighe |September 28, 2020 |Ozy 

“The golden age of Parisian smiles nurtured, and was nurtured by, the rise of dentistry as a vocation,” writes Jones. The French Court’s Royal Ban on Smiles |William O’Connor |December 14, 2014 |DAILY BEAST 

My father completed the transition out of dentistry at almost exactly the same moment as my mother started at “As It Happens.” My Eulogy for My Father, Murray Frum |David Frum |May 31, 2013 |DAILY BEAST 

To keep your pearly whites bright and shiny, visit Dr. David Poiman at the NY Center for Esthetic and Laser Dentistry. Gal With a Suitcase: New York's Best Pampering Spots |Jolie Hunt |February 11, 2011 |DAILY BEAST 

Why not, say, $97.5 million, as the Orthodontic Education Community gave the University of Colorado School of Dentistry in 2003? The $100 Million Status Symbol |Rebecca Dana |September 23, 2010 |DAILY BEAST 

The highly expensive but most advantageous service of dentistry may be paid for by the guardians. English Poor Law Policy |Sidney Webb 

It was oddly like the feeling of a dentist's reception-room; only it was for me to do the dentistry with clumsy, cruel hands. The New Machiavelli |Herbert George Wells 

Knowing dentistry, I saw the possibilities of disguise by wearing differently shaped sets of teeth. The Winning Clue |James Hay, Jr. 

Besides, what he has to say about dentistry occurs in typical medieval form. Old-Time Makers of Medicine |James J. Walsh 

By day the lad was learning dentistry, his father's profession—it was then a trade—and the two went to London to practice. The Stones of Paris in History and Letters, Volume I (of 2) |Benjamin Ellis Martin