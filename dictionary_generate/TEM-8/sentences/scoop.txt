These clear glass tumblers are usually the vessels for heaping scoops of strawberry ice cream with a fudge swirl, whipped cream, and sprinkles, but they are equally useful for spicing up an after-work drink. How to Bring This Portland Restaurant’s Colorful Outdoor Oasis to Your Home |Emma Orlow |October 30, 2020 |Eater 

The journalist has enough information for a big scoop about London’s corruption, but is afraid to hit “publish” because the authorities might retaliate against a captive friend. ‘Watch Dogs: Legion’ review in progress: Virtual London is legit, but story’s a snooze so far |Gene Park |October 28, 2020 |Washington Post 

This shovel features a 15-inch-wide, 19-inch-long, rust-proof aluminum scoop that can handle debris, dirt, and rubble. Shovels you’ll definitely dig |PopSci Commerce Team |October 18, 2020 |Popular-Science 

Amy Coney Barrett’s hearings are set to begin today, New Zealand’s election approaches, and we’ve got the scoop on what to expect at Fortune’s Most Powerful Women Next Gen Summit this week. Inside the Fortune MPW Next Gen Summit |ehinchliffe |October 12, 2020 |Fortune 

This box also comes with five capsule covers and a scoop to transfer grinds into the pods. Gear to make every day feel like National Coffee Day |PopSci Commerce Team |September 29, 2020 |Popular-Science 

With a 1¾-inch ice cream scoop (or two spoons), scoop round balls of dough onto the prepared sheet pans. Make These Barefoot Contessa Salty Oatmeal Chocolate Chunk Cookies |Ina Garten |November 28, 2014 |DAILY BEAST 

The children desperately scoop the water out with their hands. Whatever You Do Someone Will Die. A Short Story About Impossible Choices in Iraq |Nathan Bradley Bethea |August 31, 2014 |DAILY BEAST 

In stand mixer, mix cookie mix following the directions on the box, scoop 11 cookies onto baking sheet, place in oven. Epic Meal Empire’s Meat Monstrosities: From the Bacon Spider to the Cinnabattleship |Harley Morenstein |July 26, 2014 |DAILY BEAST 

McClatchy was the winner and got the scoop, and I will live with that. Jill Abramson Talks Obama Secrecy and Her New York Times Firing |Eleanor Clift |July 10, 2014 |DAILY BEAST 

She refuses to speak on the record about an issue because she has already guaranteed that scoop to another magazine. Duke Porn Star Belle Knox Is Building Her Brand One Strip Club at a Time |Emily Shire |May 6, 2014 |DAILY BEAST 

Jess laid the dog down on a bed of moss as she spoke, and started energetically to scoop up piles of the fragrant needles. The Box-Car Children |Gertrude Chandler Warner 

Only blue ground showed, and there was a power scoop digging out more. The Flaming Mountain |Harold Leland Goodwin 

The scoop which he used for the purpose would, he thought, be sufficient for throwing the water overboard. Toilers of the Sea |Victor Hugo 

Musakbut kug usa ka kumkum bugas, I will scoop out a handful of rice. A Dictionary of Cebuano Visayan |John U. Wolff 

We throw some pennies to another group, and the one nearest the coin picks it up by making a scoop of his flipper-like palm. Gardens of the Caribbees, v. 1/2 |Ida May Hill Starr