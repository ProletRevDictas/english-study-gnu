As the animal clung to the rocky bottom, it exhaled an air bubble on its snout and appeared to repeatedly suck the air in and out of the bubble. How some lizards breathe underwater |Sharon Oosthoek |July 27, 2021 |Science News 

It has a slender tapering snout and a large number of teeth. Ancient creature revealed as lizard, not a teeny dinosaur |Carolyn Gramling |July 21, 2021 |Science News For Students 

The lizard exhaled an air bubble around its snout as the animal clung to the rocky bottom. A bubble of air lets some lizards breathe underwater |Sharon Oosthoek |July 14, 2021 |Science News For Students 

While underwater, all of these lizards carried a bubble of air around their snouts. A bubble of air lets some lizards breathe underwater |Sharon Oosthoek |July 14, 2021 |Science News For Students 

To get to the sugar cube, the mice just had to poke in their snouts and lick. User experience is the difference between mediocre and next-level search marketing |Carolyn Lyden |June 16, 2021 |Search Engine Land 

As his grizzled snout suggests, Orlando is 11 years old, which translates to 77 in dog years. Strangers Rally to Help Blind Man Keep His Guide Dog |Michael Daly |December 19, 2013 |DAILY BEAST 

Had the automatic snout poking through the steel grille of the rear of the cage. Hooded Detective, Volume III No. 2, January, 1942 |Various 

Despite the speed of his dive, they were gaining on him, coming up fast; one snout that ended in a cupped depression was plain. Astounding Stories, May, 1931 |Various 

With a roast apple in his snout, and a ribbon—a blue—no, a pink ribbon decorating his ornery little tail. The Woman Gives |Owen Johnson 

He walks on all fours, and his length, from the snout to the origin of his tail, is about a foot and a half. Buffon's Natural History. Volume IX (of 10) |Georges Louis Leclerc de Buffon 

As I was dipping my tin mug into the lake, a huge snout suddenly rose, and very nearly caught my hand, as well as the mug. In the Wilds of Florida |W.H.G. Kingston