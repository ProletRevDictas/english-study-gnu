Though he claimed to be a sheikh, he had none of the qualifications. The Sydney Astrologer Turned Islamic Radical |Jacob Siegel |December 16, 2014 |DAILY BEAST 

Zubaydah indentified Khalid Sheikh Mohammed as the mastermind of the 9/11 attacks. The Luxury Homes That Torture and Your Tax Dollars Built |Michael Daly |December 12, 2014 |DAILY BEAST 

Self-described 9/11 mastermind Khalid Sheikh Mohammed was also kept at Cobalt after his March 2003 in Pakistan. Inside the CIA’s Sadistic Dungeon |Tim Mak |December 9, 2014 |DAILY BEAST 

YPG chief Esmat al-Sheikh told Reuters that ISIS controls only about a quarter of the town. U.S. Planes are Blowing the Hell out of ISIS at Kobani, But … |Jamie Dettmer |October 9, 2014 |DAILY BEAST 

One example is Muhammed Asel, kreef to the Yazidi and a Muslim Arab sheikh from a village on the north side of the mountain. On the Ground, Collaborators With ISIS Could Be Its Big Weakness |Christine van den Toorn |August 30, 2014 |DAILY BEAST 

It is made famous through its connection with an act of cruelty on the part of Sheikh Nadir. A Woman's Journey Round the World |Ida Pfeiffer 

Pilgrimage to Sheikh Adi is incumbent on every Yezidi; but he is not commanded to pray; and he leaves that duty to his priests. The Cradle of Mankind |W.A. Wigram 

Upon another occasion, when the Sheikh came to call upon us, his four attendants were credited with having consumed a whole sheep! The Cradle of Mankind |W.A. Wigram 

Of late years, a family quarrel has rather diminished the power of Sheikh Taha. The Cradle of Mankind |W.A. Wigram 

Of a surety it is smuggled tobacco from the warehouse of the Sheikh; or maybe hashish, and worth much gold. The Cradle of Mankind |W.A. Wigram