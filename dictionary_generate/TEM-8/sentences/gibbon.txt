Though slow to adopt it, the gibbons increasingly traveled a bridge made of two ropes that was installed across the 15-meter gap, researchers report October 15 in Scientific Reports. A rope bridge restored a highway through the trees for endangered gibbons |Carolyn Wilke |October 19, 2020 |Science News 

Eventually, the scientists observed the gibbons crossing the bridge about as frequently as the animals had traveled that stretch of forest before the landslide. A rope bridge restored a highway through the trees for endangered gibbons |Carolyn Wilke |October 19, 2020 |Science News 

Occasionally, gibbons scampered across without holding on with their upper limbs. A rope bridge restored a highway through the trees for endangered gibbons |Carolyn Wilke |October 19, 2020 |Science News 

The tooth is the oldest known fossil from a gibbon ancestor, says Gilbert, of Hunter College at the City University of New York. A stray molar is the oldest known fossil from an ancient gibbon |Bruce Bower |September 8, 2020 |Science News 

They have been predicting “the fall of America” for years, in the way that Gibbon described the fall of Rome. Only the French Would Be Smug About the Recession |Janine di Giovanni |December 11, 2008 |DAILY BEAST 

Another famous man was born at Putney: Edward Gibbon, the historian. The Portsmouth Road and Its Tributaries |Charles G. Harper 

Gibbon was, in fine, exceedingly human, and his person was almost grotesque. The Portsmouth Road and Its Tributaries |Charles G. Harper 

Of the apes, though many can stand upright, the gibbon is the only one that attempts to walk in this position. Man And His Ancestor |Charles Morris 

Their motion is like that of the gibbon when in haste, a series of jumps or swings between the supporting arms. Man And His Ancestor |Charles Morris 

Their attitude is in all cases an approach toward the erect one, which posture is attained by the gibbon. Man And His Ancestor |Charles Morris