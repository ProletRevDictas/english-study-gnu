We’ve delayed requisitions for event professionals, for example. Marketing Briefing: ‘Put more effort elsewhere’: How the Facebook outage could renew calls for diversification in ad spending |Kristina Monllos |October 5, 2021 |Digiday 

A diverse number of menaces soon began to claim estates, mainly related to war requisition. The Sad Fate of ‘Downton Abbey’ Estates: ‘Felling the Ancient Oaks’ |Anthony Paletta |June 22, 2012 |DAILY BEAST 

The gentleman's hat was soon in requisition, and he left with the impression that 'great is the truth, and it will prevail.' The Ladies' Book of Etiquette, and Manual of Politeness |Florence Hartley 

He had also been allowed to draw by requisition fifty days' rations, a box of ammunition, and four mules. Overland |John William De Forest 

One of the medical officers went to make a requisition for hospital accommodation, and got through the business very well. The British Expedition to the Crimea |William Howard Russell 

He had promised the King that with four regiments he would play the lion, and troops beyond his requisition were hourly expected. The Loyalists of America and Their Times, Vol. 1 of 2 |Egerton Ryerson 

They drove down to the sawmill, delivered their requisition, and had their wagon loaded with newly-sawn plank. Si Klegg, Book 2 (of 6) |John McElroy