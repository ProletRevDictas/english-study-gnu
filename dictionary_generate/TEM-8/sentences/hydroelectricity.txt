Some states have abundant hydroelectric power, as in the Pacific Northwest. The politics of energy generation don’t always line up with actual energy generation |Philip Bump |February 17, 2021 |Washington Post 

Meanwhile, 62 percent of the energy capacity will come from fossil fuels, including coal and natural gas, and another 17 percent from hydroelectric power. How much will Africa capitalize on cheap renewable energy as its power grid grows? |Carolyn Gramling |January 25, 2021 |Science News 

Because the proposed operation would run primarily on hydroelectric power, they estimate that putting to use just 30% of the most reactive tailings from the mines would make the operation carbon neutral. Asbestos could be a powerful weapon against climate change (you read that right) |James Temple |October 6, 2020 |MIT Technology Review 

The Rule does allow exceptions for hydroelectric projects, mines, and community interties—in fact, each of the 57 projects applied for in the Tongass has been permitted. Loggers could soon slice through one of the most important forests in the US |By Bjorn Dihle/ Outdoor Life |September 30, 2020 |Popular-Science 

Its sites also include British Columbia and Quebec in Canada—two provinces with massive hydroelectric capacity. There’s (digital) gold in them thar hills: Crypto giant DCG is betting $100 million on mining Bitcoin in North America |Jeff |August 27, 2020 |Fortune