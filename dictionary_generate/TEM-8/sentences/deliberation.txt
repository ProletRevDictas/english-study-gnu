The Ivy League, however, does not appear prepared to deviate from its deliberation. Ivy League sports were shut down quickly by coronavirus. A restart is proving much slower. |Glynn A. Hill |February 10, 2021 |Washington Post 

If you don’t win, no one has to see the professor’s letter or any deliberation over your nomination until there have been another 12 presidential elections. You, too, can become a Nobel Peace Prize nominee |Philip Bump |February 1, 2021 |Washington Post 

The trade agreement was confirmed Saturday night by a person familiar with the deliberations. Lions agree to trade Matthew Stafford to Rams for Jared Goff, draft picks |Mark Maske |January 31, 2021 |Washington Post 

The Philadelphia Eagles are working to complete a deal with Indianapolis Colts offensive coordinator Nick Sirianni to hire him as their next head coach, according to a person familiar with the deliberations. Eagles working on a deal to hire Colts’ Nick Sirianni as new head coach |Mark Maske |January 21, 2021 |Washington Post 

The deliberations on whether superstar quarterback Patrick Mahomes will return to the Kansas City Chiefs’ lineup for Sunday’s AFC championship game do not involve Coach Andy Reid. Under NFL’s concussion protocols, decision on Patrick Mahomes’s playing status is out of Chiefs’ hands |Mark Maske |January 20, 2021 |Washington Post 

On Nov. 13, after years of deliberation, an advisory panel finally recommended lifting the ban, sort of. The Outrageous Celibacy Requirement for Gay Blood Donors |Jay Michaelson |November 22, 2014 |DAILY BEAST 

After 90 minutes of deliberation, the jury found that the evidence was insufficient and acquitted Lizzie. Would You Stay in Lizzie Borden’s Ax-Murder House? |Nina Strochlic |October 30, 2014 |DAILY BEAST 

His sexual life, just like his barbarism, was the result of deliberation, not appetites run amok. Nothing Was Banal About Eichmann’s Evil, Says a Scathing New Biography |Michael Signer |October 11, 2014 |DAILY BEAST 

Distasteful those ads might be, but restrictions on political speech should be exercised with great deliberation and caution. Is Big Money Politics an Overblown Evil? |David Freedlander |August 2, 2014 |DAILY BEAST 

Not many doctors are likely to reach for the pocketbook on that scale without quite a bit of deliberation. The New Face of American Medicine: Liberal Women |Dale Eisinger |June 9, 2014 |DAILY BEAST 

An old weather-beaten bear-hunter stepped forward, squirting out his tobacco juice with all imaginable deliberation. Blackwood's Edinburgh Magazine, No. CCCXXXIX. January, 1844. Vol. LV. |Various 

With great deliberation and much formality Wu-pom Nai proceeded to loosen the boy's heavy plaits of hair. Our Little Korean Cousin |H. Lee M. Pike 

There was no moment of deliberation, no interval of repose between the thought and its fulfillment. The Awakening and Selected Short Stories |Kate Chopin 

She adjusted it, sticking the hat pin through the heavy coil of hair with some deliberation. The Awakening and Selected Short Stories |Kate Chopin 

He began now to watch Ando, and found himself annoyed by the deliberation of his friend's motions. The Dragon Painter |Mary McNeil Fenollosa