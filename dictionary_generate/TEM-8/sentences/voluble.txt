After the Las Guijas incident, Lenihan would eventually leave the Border Patrol for good, joining the ranks of a small but voluble coterie of ex-Border Patrol agents such as Jenn Budd and bestselling author Paco Cantú. The Border Patrol Agent Who Threw Away His Badge |Gabbriel Schivone |November 29, 2021 |The Daily Beast 

Neither SpaceX nor its voluble CEO, however, has had anything to say about it in the six days since the award was announced, unlike other participants in the bid. Why SpaceX is silent about winning a huge military contract |Tim Fernholz |August 13, 2020 |Quartz 

In late 2007, the voluble Texas hedge-funder threw down $110 million against the subprime-mortgage market and made a killing. Wall Street’s Biggest Players Avoiding Bets on Eurozone Financial Crisis |Alex Klein |July 7, 2012 |DAILY BEAST 

Joe Scarborough Another wild card would be Joe Scarborough, the voluble morning-talk-show host on MSNBC. Six Dark Horses Romney Could Pick for His Running Mate |Ben Jacobs |April 22, 2012 |DAILY BEAST 

In his voluble, guns-blazing manner, Adrover made the hollowness of New York Fashion Week, which ends Thursday, apparent. Designing for the One Percent at New York Fashion Week |Robin Givhan |February 14, 2012 |DAILY BEAST 

The likeliest person to defeat the voluble Georgian is Gingrich himself. A Gingrich Surge Forces Obama’s Team to Rethink Strategy |Eleanor Clift |December 8, 2011 |DAILY BEAST 

He was voluble in his declarations that they would “put the screws” to Ollie on the charge of perjury. The Bondboy |George W. (George Washington) Ogden 

He was rather silent, they observed; but the young clergyman, who made the fourth at the table, was voluble by nature. Elster's Folly |Mrs. Henry Wood 

As they walked along, he listened with trembling, half-incredulous hope to Jos's interpretation of Aunt Ri's voluble narrative. Ramona |Helen Hunt Jackson 

Passed the box-office coming up, continued this voluble enlightener; nothing left but a few seats in the top gallery. The Fifth String |John Philip Sousa 

Elbowing her way in she caught sight of her gown held aloft by Mr. Bills, and heard his voluble "Going, going, at fifty cents." The Cromptons |Mary J. Holmes