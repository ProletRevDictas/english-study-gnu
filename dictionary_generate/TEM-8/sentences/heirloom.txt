Better yet, get to a local farmers market and take advantage of heirloom varieties, like blue potatoes or hubbard squash, swapping them out for mainstream grocery store produce. Your guide to cooking a sustainable holiday meal |By Allie Wist/Saveur |December 17, 2020 |Popular-Science 

Set your socially distanced dessert table in style with Molly Hatch’s modern heirloom ceramic plates and mugs featuring matching icon prints of candy canes, Christmas trees, gingerbread folx, holly, ice skates, and snowflakes. The ultimate guide to queer gift giving 2020 |Mikey Rox |December 4, 2020 |Washington Blade 

That can vary, depending on the item — people are likelier to retrieve a family heirloom than they are a buzzsaw. It’s easy to assume pawnshops are doing great in the pandemic. It’s also wrong. |Emily Stewart |November 30, 2020 |Vox 

These are heirloom-quality items that I take pride in maintaining with warm beeswax. The Gear We Relied On for Our 2020 Ski Test |Marc Peruzzi |October 15, 2020 |Outside Online 

Skeptics of the hologenome idea—that microbes and host comprise a single evolutionary unit—argue that microbes aren’t always passed like heirlooms between generations. When Evolution Is Infectious - Issue 90: Something Green |Moises Velasquez-Manoff |September 30, 2020 |Nautilus 

The distillery creates “small batch heirloom spirits handcrafted in New Mexico.” Your ‘Craft’ Rye Whiskey Is Probably From a Factory Distillery in Indiana |Eric Felten |July 28, 2014 |DAILY BEAST 

Walking through rows of Swiss chard, heirloom tomatoes, leeks, and artichokes, he can tell you exactly when each is at its peak. A California Tavern With an Artichoke Obsession |Jane & Michael Stern |June 1, 2014 |DAILY BEAST 

They had to infect the perfectly adequate data with the totally improbable idea of a 400-year-old heirloom elk antler tool. Incontrovertible Evidence Proves the First Americans Came From Asia |Doug Peacock |March 27, 2014 |DAILY BEAST 

And then heirloom varieties tend to be more nutritious—and delicious—than modern, overbred varieties. This Book Will Change the Way You Eat |Lucas Wittmann |December 19, 2013 |DAILY BEAST 

Unfortunately, most heirloom apple trees have long since been ripped out of the ground. What the Pilgrims Drank on Thanksgiving |Jordan Salcito |November 28, 2013 |DAILY BEAST 

And instead of taking care of this precious heirloom, he goes and locks it away in a safe. The Joyous Adventures of Aristide Pujol |William J. Locke 

And if the monumental record of their virtues be a just one, why did they heirloom on posterity this bitter heritage of swearing? A Cursory History of Swearing |Julian Sharman 

The dog is an heirloom and will descend to the Gishes of the next generation, in the direct line of inheritance. The Fiend's Delight |Dod Grile 

These spoons long remained an heirloom in the clergyman's family to testify the truth of the story. The Science of Fairy Tales |Edwin Sidney Hartland 

"It is an heirloom of my mother's family, your Grace," returned its owner in a constrained, half-hearted way. The Court Jester |Cornelia Baker