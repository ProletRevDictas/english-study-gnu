Thus, the current indicated by the ammeter is a closely approximate measure of the conductivity of the solution. The Elements of Qualitative Chemical Analysis, vol. 1, parts 1 and 2. |Julius Stieglitz 

The instrument may be used as either a voltmeter or as an ammeter, and its operation will be identical in each case. The Boy Mechanic, Book 2 |Various 

A relay snapped, and instantly the ammeter jumped to read 4500 amperes. Islands of Space |John W Campbell 

The elder Dr. Arcot glanced in surprise at the heavy-duty ammeter in a control panel. Islands of Space |John W Campbell 

With the help of the ammeter any desired temperature can be obtained and maintained, up to about 200° C. The Elements of Bacteriological Technique |John William Henry Eyre