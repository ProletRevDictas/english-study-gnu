Lenders and creditors, if successful in court, can then garnish a person’s wages or seize their assets. Student loan companies return to court to collect on private education debt |Danielle Douglas-Gabriel |January 21, 2021 |Washington Post 

Consumers and creditors depend on our members’ services to help resolve disputes and unpaid debts and in order to help keep the credit system running smoothly. Debt collectors, payday lenders collected over $500 million in federal pandemic relief |Peter Whoriskey, Joel Jacobs, Aaron Gregg |January 15, 2021 |Washington Post 

If disaster strikes again, we’ll be putting our fate in the hands of our foreign creditors, headed by Japan and China. The U.S. now has a debt level that rivals Italy’s |Shawn Tully |January 14, 2021 |Fortune 

Chapter 7 bankruptcy usually entails an appointed trustee selling off a debtor’s assets to pay that person’s creditors. Evander Kane, who has made more than $50 million in his NHL career, files for bankruptcy |Matt Bonesteel |January 12, 2021 |Washington Post 

Given the strong level of support from our lenders and creditors, we expect to complete the process before the end of this year. Pandemic bankruptcies: A running list of retailers that have filed for Chapter 11 |Abha Bhattarai |December 4, 2020 |Washington Post 

He had already been involved with The Source, having been a creditor of the company. It Was All a Dream: Drama, Bullshit, and the Rebirth of The Source Magazine |Alex Suskind |October 14, 2014 |DAILY BEAST 

Both you and the creditor would have been better off with moderate inflation than an outright breach. Japan's Bold, Limited New Economic Plan |Megan McArdle |April 4, 2013 |DAILY BEAST 

But if your family doctor serves Medicare, he or she too is a federal contractor - and potential creditor. Who Doesn't Get Paid if We Cross the Debt Ceiling? |David Frum |January 14, 2013 |DAILY BEAST 

In Europe the principal divide that has opened is among countries, with debtor nations pitted against creditor nations. Top Economists on How to Fix the Economy | |October 12, 2011 |DAILY BEAST 

The response of China, the United States' largest creditor, has been the harshest yet. S&P: Debt Downgrade Analysis Was 'Objective' | |August 7, 2011 |DAILY BEAST 

Some weeks after, the creditor chanced to be in Boston, and in walking up Tremont street, encountered his enterprising friend. The Book of Anecdotes and Budget of Fun; |Various 

Only a creditor who owns a demand or provable claim can vote at creditors' meetings. Putnam's Handy Law Book for the Layman |Albert Sidney Bolles 

Unless some creditor objects and specifies his ground of objection, the petition will be granted. Putnam's Handy Law Book for the Layman |Albert Sidney Bolles 

It is immaterial to whom the transfer is made if the purpose be to prefer one creditor to another. Putnam's Handy Law Book for the Layman |Albert Sidney Bolles 

If a portion of a creditor's debt is secured and a portion is unsecured, he may vote on the unsecured portion. Putnam's Handy Law Book for the Layman |Albert Sidney Bolles