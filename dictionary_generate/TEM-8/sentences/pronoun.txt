They raged that they may be reacting to a riot today, but tomorrow they’ll block you for using the wrong pronouns. The Right’s Message to Silicon Valley: 'Free Speech for Me, But Not for Thee' |David French |January 16, 2021 |Time 

She and her husband had carefully named their offspring according to family tradition, and now one of them wanted a new name and new pronouns that sounded wrong to Hassouri’s ears. A mother can’t know everything |Terri Schlichenmeyer |October 9, 2020 |Washington Blade 

It can disambiguate pronouns, translate, infer, analogize, and even perform some forms of common-sense reasoning and arithmetic. Welcome to the Next Level of Bullshit - Issue 89: The Dark Side |Raphaël Millière |September 9, 2020 |Nautilus 

This is the task of identifying all the proper names in a document and figuring out which pronouns in the text refer to which people or which organizations. Can A.I. understand poetry? |Jeremy Kahn |August 18, 2020 |Fortune 

Since the Ghostbusters are a group, they’d be described using the pronoun them. Can You Correct These Grammatically Incorrect Song Lyrics? |Brigid Walsh |August 5, 2020 |Everything After Z 

Oh, the heaven and hell wrought by the casual use of a pronoun. Owning Up to Possession’s Downside |Samantha Harvey |December 14, 2014 |DAILY BEAST 

He placed particular emphasis on the pronoun when he spoke the title aloud. Can Bill Bratton Solve De Blasio’s NYPD Dilemma? |Michael Daly |December 5, 2013 |DAILY BEAST 

There's UP and US, and UT—an old name for the first (and last) tone, do, and WE (the funnest pronoun) and WO, which is woe. National Scrabble Day: A Poem So You’ll Know All 101 Two-Letter Words |David Bukszpan |April 13, 2013 |DAILY BEAST 

The pronoun is deliberate: the Sims was one of the first games to be played by women in significant numbers. SimCity Is Smarter Than You (Even If You’re an Urban Planner) |Josh Dzieza |February 26, 2013 |DAILY BEAST 

He asks, “What magic is there in the pronoun ‘my,’ that should justify us in overturning the decisions of impartial truth?” Why Favoritism Is Virtuous: The Case Against Fairness |Stephen T. Asma |December 7, 2012 |DAILY BEAST 

The subject pronoun, when unemphatic, is not expressed, but understood from the termination of the verb. Frdric Mistral |Charles Alfred Downer 

The relative object pronoun is often repeated as a personal pronoun, so that the verb has its object expressed twice. Frdric Mistral |Charles Alfred Downer 

The third personal pronoun—he, she, it—in all its cases is especially uncertain in its references. English: Composition and Literature |W. F. (William Franklin) Webster 

That is my excuse for the free use of the personal pronoun, not to make prominent the person, but to emphasize the reality. Silver Chimes in Syria |W. S. Nelson 

The disjunctive forms of the pronoun are also sometimes preserved before verbs and adjectives. The Indian in his Wigwam |Henry R. Schoolcraft