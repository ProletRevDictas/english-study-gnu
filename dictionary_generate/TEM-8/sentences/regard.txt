Especially with regard to medical services, it’s dangerous to have it just on an app … it limits the number of people who can use it. India is betting on glitchy software to inoculate 300 million people by August |Lindsay Muscato |February 10, 2021 |MIT Technology Review 

We knew going into the season that the NHL’s all-Canadian North Division would be very fun and very competitive, and in that regard it has not disappointed. Which Of The NHL’s Best Teams So Far Are For Real? |Neil Paine (neil.paine@fivethirtyeight.com) |February 5, 2021 |FiveThirtyEight 

In that regard, Ellsberg has a new bone to pick with the Times. Seeing the Pentagon Papers in a New Light |by Stephen Engelberg |February 3, 2021 |ProPublica 

With regard to bread or other recipes, any recipe developer worth their salt will tell you that you need to pay attention to much more than timing. How to help your dough rise to the occasion in winter |Becky Krystal |February 1, 2021 |Washington Post 

Previously, advertisers created blocklists of keywords to exclude, but this option could give you more control over where ads are placed in the Facebook News Feed, especially in regard to sensitive content. Facebook testing brand safety topic exclusions for advertisers |Carolyn Lyden |January 29, 2021 |Search Engine Land 

I think the response of the French government so far has been pretty appropriate in that regard. Harry Shearer on The Dangerous Business of Satire |Lloyd Grove |January 8, 2015 |DAILY BEAST 

Your letter highlights so many of the harsh realities trans people face, specifically in regard to how society rejects us. Dear Leelah, We Will Fight On For You: A Letter to a Dead Trans Teen |Parker Molloy |January 1, 2015 |DAILY BEAST 

Fracking, in this regard, is no different from gypsum mining, or some kinds of industrial agriculture. New York’s Conservative Fracking Ban |Jay Michaelson |December 20, 2014 |DAILY BEAST 

Mahoney helped author the I-STOP legislation based on data his office collected in regard to fraud. No More Paper Prescriptions: Docs Fight Fraud by Going Electronic |Dale Eisinger |December 18, 2014 |DAILY BEAST 

And with regard to taking on Hillary Clinton, he does no better than any of the rest of them. Be the Smarter Bush Brother, Jeb: Don’t Run! |Michael Tomasky |December 17, 2014 |DAILY BEAST 

No man should regard the subject of religion as decided for him until he has read The Golden Bough. God and my Neighbour |Robert Blatchford 

The Spaniards, indeed, feigned to regard them only as a remnant of the rebels who had joined the pre-existing brigand bands. The Philippine Islands |John Foreman 

Soon after that, I wrote you in regard to the condition in which we found this infant Church and Colony. The Jesuit Relations and Allied Documents, Vol. II: Acadia, 1612-1614 |Various 

Condition of the archbishopric of Manila in regard to the affairs of ecclesiastical and secular government. The Philippine Islands, 1493-1898, Volume XX, 1621-1624 |Various 

None the less it is the most nearly true of all the broad generalizations that can be attempted in regard to mankind. The Unsolved Riddle of Social Justice |Stephen Leacock