He also sees the potential for CRISPR to address other incurable human diseases, like Huntington’s and Alzheimer’s, that may have genetic components. CRISPR breaks ground as a one-shot treatment for a rare disease |Claire Maldarelli |July 28, 2021 |Popular-Science 

While most genetic disorders are incurable, doctors do their best to manage them. Data Crunchers to the Rescue - Issue 102: Hidden Truths |Lela Nargi |June 9, 2021 |Nautilus 

Cracking their code could be critical to understand our biological ecosystem, but even more tantalizingly, phages may be the answer to a host of currently incurable diseases. How to listen to all of Vox’s Earth Month podcasts |Lauren Katz |April 16, 2021 |Vox 

When I was diagnosed with an incurable form of NHL in January 2002, my only treatment option at the time was chemotherapy. Why we shouldn’t give up on bipartisanship, even now |jakemeth |November 3, 2020 |Fortune 

Mengnan was told it was incurable, but that there was one medicine, Remicade, that might help. Will US Health Care Follow in China’s Bloody Footsteps? |Daniela Drake |September 21, 2014 |DAILY BEAST 

The truth is that a horrific, incurable virus is ravaging Guinea—and at least three other countries in West Africa. The Fear That Killed Eight Ebola Workers |Abby Haglage |September 20, 2014 |DAILY BEAST 

Women Living with Incurable STDs, argues that women are more often shunned when it comes to an STI. The Silent Shame of HPV |Emily Shire |August 29, 2014 |DAILY BEAST 

It is technically “incurable” but incurable in the same way that colds have no cure and sprained ankles have no cure. Suffering From An ‘Incurable Respiratory Disease’ This Winter? Relax. It’s Just RSV. |Kent Sepkowitz |January 14, 2014 |DAILY BEAST 

Despite being told that her disease was incurable, Carr refused to accept that there was nothing she could do about it. So You Wanna Be Vegan? Start Here. |Alessandra Rafferty |February 23, 2013 |DAILY BEAST 

They spoke like this because they are accustomed to abandon altogether those whom they have once judged incurable. The Jesuit Relations and Allied Documents, Vol. II: Acadia, 1612-1614 |Various 

That struck the people in wrath with an incurable wound, that brought nations under in fury, that persecuted in a cruel manner. The Bible, Douay-Rheims Version |Various 

This accident led his parents to reflect upon the childs incurable tendency and consider the question of his musical education. The Life &amp; Letters of Peter Ilich Tchaikovsky |Modeste Tchaikovsky 

Even Bishop Ken said of him that he showed zeal to make the schism incurable. The English Church in the Eighteenth Century |Charles J. Abbey and John H. Overton 

Stricken by an incurable anaemia, he would remain for weeks without leaving his house, without doing any work. The Nabob |Alphonse Daudet