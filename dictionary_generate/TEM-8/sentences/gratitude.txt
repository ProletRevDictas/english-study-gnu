Burn leaders generally aren’t celebrating for helping avoid a fire in the same way firefighters receive gratitude for their work, points out Keane. How we can burn our way to a better future |Ula Chrobak |October 2, 2020 |Popular-Science 

As I understand, for yogis, saying namaste is a moment of contemplating the virtues associated with yoga – including peacefulness, compassion, and gratitude and how to bring those into one’s daily life. Why ‘Namaste’ Has Become The Perfect Pandemic Greeting |LGBTQ-Editor |September 30, 2020 |No Straight News 

In a recent interview, Amay expresses gratitude and a hopeful yet savvy look forward in uncertain times. Non-binary actor wins Helen Hayes Award |Patrick Folliard |September 24, 2020 |Washington Blade 

Paul plans to write a letter to show his gratitude to his kidney donor and hopes they can all meet someday. A Welcome Lifeline |Washington Regional Transplant Community |September 17, 2020 |Washington Blade 

As you said, your gratitude may start to kick in a little bit more. Introducing “No Stupid Questions” (Ep. 422) |Stephen J. Dubner |June 18, 2020 |Freakonomics 

Should we cancel gatherings, reunions, excursions, or throw ourselves into them with even more gratitude for one another? The Media's Pro-Torture Cheerleaders |Jedediah Purdy |December 10, 2014 |DAILY BEAST 

But the task is a little more fun when you can send your gratitude on cute note cards inspired by traditional Islamic designs. The Daily Beast’s 2014 Holiday Gift Guide: For the Angelina Jolie in Your Life |Allison McNearney |November 29, 2014 |DAILY BEAST 

Every other kind of influence-buying, such as spending to achieve influence, gratitude, access, etc., is just fine. Undo Citizens United? We’d Only Scratch the Surface |Jedediah Purdy |November 12, 2014 |DAILY BEAST 

“Relief and gratitude was the reaction,” Noonan reported afterward. Killer Eric Frein Held in Murdered Cop’s Cuffs |Michael Daly |October 31, 2014 |DAILY BEAST 

At one point she even expressed gratitude to the Cuban government for its help in fighting the lethal epidemic. Samantha Power Praises Cuba for Ebola Response |Lloyd Grove |October 31, 2014 |DAILY BEAST 

"I most humbly thank your lordship," replied the butler with an air of profound gratitude, as he chuckled in his sleeve. The Pit Town Coronet, Volume I (of 3) |Charles James Wills 

A very little crust thrown to the very hungry is always accepted with gratitude. The Pit Town Coronet, Volume I (of 3) |Charles James Wills 

The smiling face of man was blotted out; gratitude, virtue, were annihilated; and life had no longer an object! The Pastor's Fire-side Vol. 3 of 4 |Jane Porter 

He at once bowed himself to the ground in gratitude, and in words of the most humble sort returned his thanks. Our Little Korean Cousin |H. Lee M. Pike 

I owe you a large debt of gratitude, which I want to work out—so do not talk of sending me away. The World Before Them |Susanna Moodie