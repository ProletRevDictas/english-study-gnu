Tom Sykes examines the evidence: a pair of Doc Martens, some Dungarees, and that infamous scrunchie. Dissecting Cressida Bonas's Style: How Prince Harry's Girl Dresses |Tom Sykes |October 15, 2013 |DAILY BEAST 

Hard to get more can-do blue collar than a steel-town mayor in dungarees. The Mayors Who Can Revive America |Tina Brown |September 20, 2010 |DAILY BEAST 

Ramos' bubb was spinning once more, but he was wearing just dungarees. The Planet Strappers |Raymond Zinke Gallun 

But the fists he carried in the pockets of his dungarees bulged like coconuts, and his hairy arms were looped brown cables. Where the Pavement Ends |John Russell 

"I had an idee that thwart would pull loose," Mr. Gibney remarked, as he got up and rubbed the seat of his dungarees. Captain Scraggs |Peter B. Kyne 

Durned if I don't feel like jumpin' into a suit of dungarees an' helpin' him out in that engine room, Gib. Captain Scraggs |Peter B. Kyne 

From the wing-struts we transferred to a Navy dory, manned by enlisted men commanded by a blank-faced ensign in dungarees. Cue for Quiet |Thomas L. Sherred