Most impressive of all, however, is young up-and-comer Reid Miller, who gives a charismatic and refreshingly unaffected performance as Jadin. Wahlberg walks for redemption in disappointing ‘Joe Bell’ |John Paul King |July 22, 2021 |Washington Blade 

Surprisingly, running economy was essentially unaffected by cutting the plate. The Science of Track and Field’s New Super Spikes |mmirhashem |July 16, 2021 |Outside Online 

I got caught in heavy rain wearing the Versa 3, and it was completely unaffected. Fitbit Versa 3 review: A well-rounded smartwatch for fitness |Billy Cadden |June 14, 2021 |Popular-Science 

We didn’t find any increase in mortality on marathon days in those people who lived in nearby ZIP codes that were unaffected by the race route, which makes a lot of sense. Introducing a New “Freakonomics of Medicine” Podcast (Ep. 465) |Stephen J. Dubner |June 10, 2021 |Freakonomics 

De’Medici is cited as the first to create a sealed design, unaffected by air pressure. Melting Butter, Poisonous Mushrooms and the Strange History of the Invention of the Thermometer |Phil Jaekl |June 1, 2021 |Time 

A control group—a population of gorillas unaffected by Ebola, separated by a river and 20 kilometers—was also observed. How Gorillas Are Outsmarting Ebola |Melissa Leon |September 1, 2014 |DAILY BEAST 

Almost all of the U.S. sanctions regime would be totally unaffected. Stay the Dogs of War on Iran |Leslie H. Gelb |November 18, 2013 |DAILY BEAST 

In the morning both appeared at a food festival, seemingly unaffected. Food Fight! The Seven Biggest Rivalries Inside the Food Network |Thomas Flynn |September 26, 2013 |DAILY BEAST 

Until the last few weeks, Aleppo was relatively unaffected by the uprising. As Syrian Troops Pour Into Aleppo, Rebels Warn of Drawn-Out War |The Telegraph |July 26, 2012 |DAILY BEAST 

But young males, 18 to 34, were relatively unaffected by all of his ranting and raving. Charlie Sheen: Anger Management on FX, DirecTV, Fiat |Maria Elena Fernandez |July 23, 2012 |DAILY BEAST 

A trim maid then brought in the tea equipage, and Georgie did the honours with her usual unaffected grace. The Pit Town Coronet, Volume I (of 3) |Charles James Wills 

It was very unfortunate that the whole establishment stood in unaffected awe of the redoubted Mr Bellamy. Blackwood's Edinburgh Magazine, No. CCCXXXIX. January, 1844. Vol. LV. |Various 

It was not often a former pupil returned to visit them in this simple, unaffected way. Three More John Silence Stories |Algernon Blackwood 

This depth of emotion was entirely unaffected, and secured for her the peculiar reverence of the sacred sisters. Madame Roland, Makers of History |John S. C. Abbott 

Her intelligence was great, and her modesty and unaffected manners interested all who knew her. The Childhood of Distinguished Women |Selina A. Bower