“Based on the foregoing, the highest and best use of the property, as improved, is consistent with the existing use as a hotel development,” the appraisal found. Appraisals Undermine Housing Commission’s Defense of Hotel Purchases |Andrew Keatts |June 30, 2021 |Voice of San Diego 

So why might the inspirational woman be foregoing future cover shoots? Burberry Responds to Toxic Chemical Allegations; Carey Mulligan Jokes About Destroying Oscar Dress |The Fashion Beast Team |January 24, 2014 |DAILY BEAST 

And finally, Step 7: “If you think all of the foregoing is mad, just watch and learn.” Fringe Factor: 7 Easy Steps to Jailing Obama |Caitlin Dickson |January 5, 2014 |DAILY BEAST 

Attack Syrian government military targets with cruise missiles, drones, or with the foregoing plus piloted U.S. aircraft. Obama’s New Syria Options |Leslie H. Gelb |August 25, 2013 |DAILY BEAST 

Those foregoing slogans probably need some fine-tuning, but here's one I think is perfect for you: "The Audacity of Compromise." Larry Flynt: Obama: No, You Can't! |Larry Flynt |April 12, 2011 |DAILY BEAST 

Remember, the foregoing examples are exclusively taken from radio commentary spoken in 2009. Rush the Race-Baiter |Conor Friedersdorf |October 16, 2009 |DAILY BEAST 

Many so-called "humming tones" are given for practice, but in accepting them observe whether the foregoing principle is obeyed. Expressive Voice Culture |Jessie Eldridge Southwick 

As before suggested, let the pupil recite the foregoing ten events forwards and the reverse way several times from memory. Assimilative Memory |Marcus Dwight Larrowe (AKA Prof. A. Loisette) 

The foregoing are all the Canadian mines now in work, as far as I have been able to learn, certainly all of any importance. Asbestos |Robert H. Jones 

The hollowing of the "shell" is seemingly less delicate, but this may be taken as a natural result of the foregoing. Antonio Stradivari |Horace William Petherick 

Nothing could well be more interesting or more suggestive to every one connected with the asbestos industry than the foregoing. Asbestos |Robert H. Jones