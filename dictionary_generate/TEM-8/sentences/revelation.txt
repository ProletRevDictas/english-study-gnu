That revelation spooked investors, who sent the shares crashing down by more than one-third over a two-day stretch. The year’s hottest e-commerce stock is up more than 1,500%. Its founder cashed out before the rally |Bernhard Warner |August 18, 2020 |Fortune 

The revelation of Ikhrata’s March donation to Lawson-Remer comes the same week that he’s unveiling his new, 50-year plan for the future of transportation in the county. Morning Report: SANDAG Head Wades Into Supes Race |Voice of San Diego |August 14, 2020 |Voice of San Diego 

The single most important lesson from these revelations is that companies that trade in personal data cannot be trusted to store and manage it. The EU is launching a market for personal data. Here’s what that means for privacy. |Amy Nordrum |August 11, 2020 |MIT Technology Review 

The plate showed what was then called the Andromeda nebula, but Hubble’s new distance measurement led to the universe-altering revelation that the nebula was, in fact, the Andromeda galaxy, and that countless other galaxies existed beyond our own. Social Distancing From the Stars |Emily Levesque |August 11, 2020 |Quanta Magazine 

The news about what went wrong with the deal has been a slow drip of revelations over the last year. Morning Report: How the City Came to Lease a Lemon |Voice of San Diego |August 11, 2020 |Voice of San Diego 

There was the empathetic way she dealt with the revelation that Mrs. Baxter is a former criminal. ‘Downton Abbey’ Review: A Fire, Some Sex, and Sad, Sad Edith |Kevin Fallon |January 5, 2015 |DAILY BEAST 

The revelation that, at age 42, Ben Affleck has one hell of an ass. Year of the Butt: How the Booty Changed the World in 2014 |Kevin Fallon |December 30, 2014 |DAILY BEAST 

It was also the most shocking revelation of 2014: Beyoncé Knowles is not perfect. Butts, Brawls, and Bill Cosby: The Biggest Celebrity Scandals of 2014 |Kevin Fallon |December 27, 2014 |DAILY BEAST 

Quite why anyone is as shocked and surprised by this “revelation” as some are claiming, is beyond me. Meet Zoella—The Newbie Author Whose Book Sales Topped J.K. Rowling |Lucy Scholes |December 11, 2014 |DAILY BEAST 

And, he adds, God promises in Revelation 11:18 that “I will destroy those who destroy the Earth.” Extreme Weather? Blame the End Times |Jay Michaelson |November 28, 2014 |DAILY BEAST 

The supernaturalist alleges that religion was revealed to man by God, and that the form of this revelation is a sacred book. God and my Neighbour |Robert Blatchford 

Each religion claims that its own Bible is the direct revelation of God, and is the only true Bible teaching the only true faith. God and my Neighbour |Robert Blatchford 

Is the Bible revelation so clear and explicit that no difference of opinion as to its meaning is possible? God and my Neighbour |Robert Blatchford 

He knew that the whole fabric of crime was due to the human reading of His "revelation" to man. God and my Neighbour |Robert Blatchford 

The recognition did not lessen the reality, the poignancy of the revelation by any suggestion or promise of instability. The Awakening and Selected Short Stories |Kate Chopin