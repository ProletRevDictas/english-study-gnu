To get the gavels and move things along, Democrats may have to give up much of their power and capitulate to McConnell’s request on the filibuster. Why Mitch McConnell Is Filibustering to Protect the Filibuster |Philip Elliott |January 22, 2021 |Time 

“There are not 13 votes for this pile of crap Mnuchin is capitulating on,” said a third Senate GOP aide familiar with the discussions. Senate Republicans fume as Mnuchin gives ground to Pelosi in search of deal |Erica Werner, Jeff Stein |October 22, 2020 |Washington Post 

Even the liberal Chief Justice Centlivres capitulated, siding with the government’s appointees. What Happened When South Africa’s Supreme Court Became Unbalanced |Fiona Zublin |October 16, 2020 |Ozy 

SDG&E, Berkshire Hathaway and other prospective bidders pressured the mayor to remove the clause, and he capitulated. San Diego Is Headed Toward Another Bad Deal Unless Leaders Intervene |Brian Pollard |October 1, 2020 |Voice of San Diego 

These big cuts were coming and everyone in Sacramento capitulated. How Los Angeles and San Diego Unified Started Driving State Education Policy |Will Huntsberry |July 29, 2020 |Voice of San Diego 

Much like Jamie, he acknowledges—but will not capitulate to—the circumscribed world they create. This Week’s Hot Reads: December 22, 2014 |Mythili Rao |December 22, 2014 |DAILY BEAST 

But instead of fighting the trend, too many of us simply capitulate—lazy, credulous fools that we are. Walmart Lifts Black Friday’s Curse |James Poulos |November 26, 2014 |DAILY BEAST 

He knew his best friend, Chief Taylor, would stand by him and that Stilts would have to capitulate. ‘Tracing the Blue Light’: Read Chapter 1 of Eileen Cronin’s ‘Mermaid’ |Eileen Cronin |April 8, 2014 |DAILY BEAST 

Nor is the stubborn, shrewd prime minsiter known to capitulate easily, or to misread public sentiment. Why Did Netanyahu Release Palestinian Prisoners? |Abraham Katsman |August 6, 2013 |DAILY BEAST 

As he is walking out the door, the Japanese call him back, capitulate, and a happy medium is agreed on. ‘A Hijacking,’ the Somali Pirate Movie Without Tom Hanks, Is Fantastic |Tom Sykes |July 15, 2013 |DAILY BEAST 

General Augusti was personally inclined to capitulate, but was dissuaded from doing so by his officers. The Philippine Islands |John Foreman 

He stuck to the desperate situation by strength of arm, rather than capitulate with his conscience. Balsamo, The Magician |Alexander Dumas 

All the castle had opened its heart to Mary,—even Sebastian; though the churchman did not capitulate without a struggle. God Wills It! |William Stearns Davis 

When the Congress of Ghent assembled in 1576, the castle was occupied by a Spanish garrison, who refused to capitulate. Belgium |George W. T. (George William Thomson) Omond 

Monarchies may capitulate, republics die and bear their testimony even to martyrdom. The Life of Mazzini |Bolton King