Some things never change as we wring out the old year and ring in the new one. New Year’s Eve, Babylon Style |Candida Moss |December 31, 2014 |DAILY BEAST 

To wring all that can be wrung from metaphor, note what our elected and appointed officials are not dressed as. Election Day Is Scarier Than Halloween |P. J. O’Rourke |November 1, 2014 |DAILY BEAST 

It would wring our gizzards intolerably to see so much good stuff going to waste. The Stacks: H.L. Mencken on the 1904 Baltimore Fire |H.L. Mencken |October 4, 2014 |DAILY BEAST 

This makes what Obama and John Kerry manage to wring out of the Russians in the next two days absolutely crucial. Did Obama Just Change His Luck on Syria? |Michael Tomasky |September 11, 2013 |DAILY BEAST 

It could find other ways to wring costs of its operations, like using less packaging or electricity. Sad Face at Walmart |Daniel Gross |May 16, 2013 |DAILY BEAST 

It was difficult, with the mean appliances of the time, to wring subsistence from the reluctant earth. The Unsolved Riddle of Social Justice |Stephen Leacock 

They like things on a small scale and know how to wring a dollar out of every five-cent piece. Ancestors |Gertrude Atherton 

Rinse it through a clean, lukewarm water; wring it lengthways, and stretch it well. The Ladies' Book of Etiquette, and Manual of Politeness |Florence Hartley 

If the latter, of course the owner could wring the cocks neck and the incident would be closed. The Cradle of Mankind |W.A. Wigram 

But unless he loses his voice before long I shall have to wring his neck—no easy job—or do without my usual amount of sleep. The Red Cow and Her Friends |Peter McArthur