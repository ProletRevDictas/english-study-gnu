Leave it to Canada to come up with a show so gentle and funny, it made America fall in love with family sitcoms again. The New Class of Comfort TV: 16 Shows to Watch When You Run Out of Friends and The Office |Eliana Dockterman |February 10, 2021 |Time 

“Golden Girls,” the sitcom that ran seven years starting in 1985, featured Bea Arthur as Dorothy, who respected boundaries with her two grown children, while caring for her own mother daily. It’s time to end the TV trope of the needy empty-nest mom |Michele Weldon |February 2, 2021 |Washington Post 

Each episode tackles a new decade of family sitcom, from I Love Lucy in the 1950s to Modern Family in the 2010s. Wandavision Offers Hope That Originality Can Survive the Era of the Ever-Expanding Franchise |Eliana Dockterman |January 16, 2021 |Time 

The whole show seems to nod, meta-textually, to the fact that star Elizabeth Olsen grew up on sitcom sets since her sisters Mary-Kate and Ashely Olsen starred in Full House. Wandavision Offers Hope That Originality Can Survive the Era of the Ever-Expanding Franchise |Eliana Dockterman |January 16, 2021 |Time 

The next phase of Marvel’s superhero cinematic universe—the most expensive and highest-grossing film series of all time—will begin with a television sitcom. Disney has a lot riding on the launch of Marvel’s very weird new show |Adam Epstein |January 14, 2021 |Quartz 

Early this year, Cosby had a sitcom in development for NBC and a stand-up special in development for Netflix. How the World Turned on Bill Cosby: A Day-by-Day Account |Scott Porch |December 1, 2014 |DAILY BEAST 

I ended up doing a show in Los Angeles, and HBO said they liked it and asked if I could think about it as a sitcom. Stephen Merchant Talks ‘Hello Ladies’ movie, the Nicole Kidman Cameo, and Legacy of ‘The Office’ |Marlow Stern |November 22, 2014 |DAILY BEAST 

Bill Cosby, it seems, can only be seen in two registers: sainted family man of a much-loved sitcom, or fallen, tarnished villain. Newsflash: Bill Cosby Is Not Cliff Huxtable |Tim Teeman |November 20, 2014 |DAILY BEAST 

A stand-up comedian in a self-titled sitcom revolving around his musings about nothing? Is John Mulaney the Next Seinfeld? |Kevin Fallon |October 5, 2014 |DAILY BEAST 

“A regular TV sitcom, a Broadway show and a meaty movie role,” Rivers said. Joan Rivers: 'Death Is Like Plastic Surgery' |Tim Teeman |September 4, 2014 |DAILY BEAST 

They were looking at one another like a couple of googly-eyed kids at the end of a date in a sitcom. Makers |Cory Doctorow