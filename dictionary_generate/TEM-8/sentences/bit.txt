This one is perhaps a bit too complex to capture in a sentence or two, so see our previous coverage of NDB here. Here are the 19 companies presenting at Alchemist Accelerator Demo Day XXV today |Greg Kumparak |September 17, 2020 |TechCrunch 

As a result, the waves travel a bit faster when the water is warmer. Underwater earthquakes’ sound waves reveal changes in ocean warming |Carolyn Gramling |September 17, 2020 |Science News 

She is juggling a lot and is plowing through, but wants to take care of herself naturally and sometimes a bit indulgently. Tower 28 Announces Winner Of The Clean Beauty Summer School Program |Hope Wright |September 17, 2020 |Essence.com 

Elsewhere, the software feels a bit zippier than on the Series 5, thanks to Apple’s improved S6 processor chip. Apple Watch Series 6 first impressions: A stretchy addition looks great |Aaron Pressman |September 17, 2020 |Fortune 

At the time of the 1790 census, Virginia had a bit less than 13 times the population of Delaware. America needs a democratic revolution |Matthew Yglesias |September 17, 2020 |Vox 

In a bit of foreshadowing, he repeated that opinion in November. Jeb Bush’s Unseen Anti-Gay Marriage Emails |Jackie Kucinich |January 9, 2015 |DAILY BEAST 

Even the hot Jewish women I mentioned above did something a bit more “intellectual” than pageantry: acting. Why Was Bess Myerson the First and Last Jewish Miss America? |Emily Shire |January 7, 2015 |DAILY BEAST 

His peers remember him as a bright man who spoke softly and occasionally came across as a bit shy. Mexico’s Priests Are Marked for Murder |Jason McGahan |January 7, 2015 |DAILY BEAST 

She narrowed her eyes, bit her lip as if to chew over the question, and whisked some stray blond hairs away from her face. Powerful Congressman Writes About ‘Fleshy Breasts’ |Asawin Suebsaeng |January 7, 2015 |DAILY BEAST 

It reminded me a bit of an alternative take on The Wolf of Wall Street—through the Toni and Candace lens. Coffee Talk with Fred Armisen: On ‘Portlandia,’ Meeting Obama, and Taylor Swift’s Greatness |Marlow Stern |January 7, 2015 |DAILY BEAST 

After a bit of waiting, Mac decided that the smoke was floating from a certain direction, and we began to edge carefully that way. Raw Gold |Bertrand W. Sinclair 

The sudden pall of darkness in this strange house of mystery was just a tiny bit awesome. The Boarded-Up House |Augusta Huiell Seaman 

Things looked anxious for a bit, but by this morning's dawn all are dug in, cool, confident. Gallipoli Diary, Volume I |Ian Hamilton 

Nogués and his brave lads have done their bit indeed for the glory of the Army of France. Gallipoli Diary, Volume I |Ian Hamilton 

Kum Kale has been a brilliant bit of work, though I fear we have lost nearly a quarter of our effectives. Gallipoli Diary, Volume I |Ian Hamilton