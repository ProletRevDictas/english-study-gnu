He is a possible solution to the Nationals’ need for middle-of-the-order offense — and not just another pawn in Pittsburgh’s low-cost rebuild. In Josh Bell, the Nationals added a big bat. Now Mike Rizzo might search for another. |Jesse Dougherty |December 26, 2020 |Washington Post 

Perry Lewin has been in the pawn industry for 28 years, but he’s never quite seen a year like this one. It’s easy to assume pawnshops are doing great in the pandemic. It’s also wrong. |Emily Stewart |November 30, 2020 |Vox 

Many pawn businesses are multigenerational not only in ownership but in customers. It’s easy to assume pawnshops are doing great in the pandemic. It’s also wrong. |Emily Stewart |November 30, 2020 |Vox 

Continuing to prop him up in the media as a political pawn leaves them unable to do so. David Dorn’s Daughters Rebuke Politicizing Of Father’s Death At RNC |Hope Wright |August 28, 2020 |Essence.com 

Some scientists fear that the vaccine could become a pawn in American election politics and have cautioned against distributing an untested injection. Russia says it has a covid vaccine called “Sputnik-V” |Antonio Regalado |August 11, 2020 |MIT Technology Review 

They were getting more imaginative,” a pawn shop owner thinks of his addict customers in “Back of Beyond. This Week’s Hot Reads: December 22, 2014 |Mythili Rao |December 22, 2014 |DAILY BEAST 

“Congo was clearly just a pawn in the global chessboard of West vs. East,” Holm says. ‘Argo’ in the Congo: The Ghosts of the Stanleyville Hostage Crisis |Nina Strochlic |November 23, 2014 |DAILY BEAST 

Barely Legal Pawn stars the duo as unscrupulous, possibly deranged pawnshop employees. Shark-Eating Monsters, the Kid Emmys, and More Viral Videos |The Daily Beast Video |August 24, 2014 |DAILY BEAST 

With Suzanne, Vee saw someone who could be a pawn in her chess game. ‘Orange Is the New Black’ Star Uzo Aduba on Her Journey From Track Phenom to Crazy Eyes |Marlow Stern |June 11, 2014 |DAILY BEAST 

Yet, she is still very much a pawn in the system—her body is actively sold to the highest bidder. The Abused Wives of Westeros: A Song of Feminism in ‘Game of Thrones’ |Amy Zimmerman |April 30, 2014 |DAILY BEAST 

She never dreamed that she herself was a pawn in the game that was intended to bring Nana Sahib to Delhi. The Red Year |Louis Tracy 

Rat this pawn of the Eye may have been, but even a cornered rat will fight with the courage of a lion. Hooded Detective, Volume III No. 2, January, 1942 |Various 

Afterwards she married one of the most noted scamps in Paris, who wanted a pretty wife as a pawn in some game of his. The Weight of the Crown |Fred M. White 

To be checkmated by an 'errant' pawn in the very middle of the board is a most ignominious way of losing the game. Chaucer's Works, Volume 1 (of 7) -- Romaunt of the Rose; Minor Poems |Geoffrey Chaucer 

In default of the usual bazaars OLeary had returned with the spoils of half a dozen pawn-shops. The Woman Gives |Owen Johnson