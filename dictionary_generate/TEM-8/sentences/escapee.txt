The overwhelming majority of escapees are caught within the first month. An Alabama Correctional Officer Helped a Murder Suspect Escape. The Jailbreak Highlights a Bigger Problem |Josiah Bates |May 12, 2022 |Time 

The escapees took cover behind a law enforcement vehicle as gunshots and a loud boom rang out from the synagogue. ‘I was not going to let him assassinate us’: How Texas synagogue hostages used security training to flee |Bryan Pietsch |January 18, 2022 |Washington Post 

The escapees quietly left the homes of their enslavers, made their way to the Southwest docks and boarded the Pearl. Desperate for freedom, 77 enslaved people tried to escape aboard the Pearl. They almost made it. |Gillian Brockell |April 16, 2021 |Washington Post 

Nearly all of the escapees were sold South, including Mary and Emily Edmonson and their four brothers. Desperate for freedom, 77 enslaved people tried to escape aboard the Pearl. They almost made it. |Gillian Brockell |April 16, 2021 |Washington Post 

The ship was towed back to Washington under the point of several guns, and as the escapees were carted off to jail, an angry crowd harassed them. Desperate for freedom, 77 enslaved people tried to escape aboard the Pearl. They almost made it. |Gillian Brockell |April 16, 2021 |Washington Post 

Years ago, as an escapee of the George W. Bush administration, I wrote a whole book about it. Republicans Allowed Karl Rove to Mislead Them Again |Matt Latimer |November 17, 2012 |DAILY BEAST 

When the goat squeezed under the dog leaped over and continued to herd the escapee toward the pen. Rescue Dog of the High Pass |James Arthur Kjelgaard 

Jink, here, has a theory that it's some escapee from the paper-doll factory, with a machete. Police Operation |H. Beam Piper