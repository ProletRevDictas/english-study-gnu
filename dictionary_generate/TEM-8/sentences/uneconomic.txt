Cheap gas has also rendered government-preferred energy sources like solar even more uneconomic than they were beforehand. David's Bookclub: The New New Deal |David Frum |December 1, 2012 |DAILY BEAST 

This is only possible at the most uneconomic position of the valve as regards cut off. Scientific American Supplement, No. 787, January 31, 1891 |Various 

Put out at a time when money was scarce, the loan would have been unpatriotic and uneconomic. The War After the War |Isaac Frederick Marcosson 

Ugliness is the aesthetic, or theoretical aspect of sin; in its practical aspect sin is uneconomic, un-moral. Beauty and the Beast |Stewart A. McDowall 

My grandfather had, I have heard, a theory that small holdings of land were uneconomic. The Red Hand of Ulster |George A. Birmingham 

Announcements of this kind may be classified, it seems to me, as economic, uneconomic and illegitimate. A Librarian's Open Shelf |Arthur E. Bostwick