Predictions for a catch-up rally have repeatedly failed over the years, and an uptick in virus cases and travel restrictions threaten an already fragile economic recovery. Looking for alternatives to the U.S., investors bet on a recovering Europe |kdunn6 |August 24, 2020 |Fortune 

When we learn to be comfortable with ourselves, we are in a position to see and appreciate how others are different from us rather than just relying on them to support our fragile sense of self. That chatbot I’ve loved to hate |Tate Ryan-Mosley |August 19, 2020 |MIT Technology Review 

The quantum states at the heart of today’s quantum computers are fragile things. New Algorithm Paves the Way Towards Error-Free Quantum Computing |Edd Gent |August 14, 2020 |Singularity Hub 

This impending paradigm shift could be a threat to the stability of our fragile system, but only if it is not fully anticipated. The Global Work Crisis: Automation, the Case Against Jobs, and What to Do About It |Peter Xing |August 6, 2020 |Singularity Hub 

He used a somewhat similar imaging tool last summer in the Gulf of Alaska and can think of plenty of fragile, filmy creatures that he would love to scan. Larvaceans’ underwater ‘snot palaces’ boast elaborate plumbing |Susan Milius |June 15, 2020 |Science News 

Domestically, the prime minister maintains the dubious line that he is the only man who can keep the still-fragile peace. Cambodia’s Smoke-and-Mirrors Democracy |David Shaftel |January 9, 2015 |DAILY BEAST 

The gym—a fragile collective of human ecology at the best of times—has suddenly become even more tense. How to Survive the New Year ‘Gympocalypse’ |Tim Teeman |January 6, 2015 |DAILY BEAST 

It's about the delicate fabric of the universe and how our fragile insides crumble when that fabric is torn. Alfred Hitchcock’s Fade to Black: The Great Director’s Final Days |David Freeman |December 13, 2014 |DAILY BEAST 

He was coming out of a relationship and was in a fragile place himself. Blogger Shares and Shames Cancer in ‘Lily’ |Amy Grantham |December 9, 2014 |DAILY BEAST 

The Houthis have done exactly this and have shaken the already fragile government of Yemen to its foundations. Yemen’s a Model All Right—For Disaster |Michael Shank , Casey Harrity |November 14, 2014 |DAILY BEAST 

Bang went the fragile bulb, as it splintered into a thousand atoms, and the mercury shot in sparkling globules over the table. The Pit Town Coronet, Volume I (of 3) |Charles James Wills 

A fragile arm twined itself about his neck and he kissed her on the lips. The Joyous Adventures of Aristide Pujol |William J. Locke 

But children came and died too quickly for her health and fragile beauty, and the storms of life beset her. Ancestors |Gertrude Atherton 

A cigar should be handled daintily; it is a fragile, graceful creature—don't mar its beauty. Tobacco; Its History, Varieties, Culture, Manufacture and Commerce |E. R. Billings. 

That rare and curious being called I is more fragile than any porcelain jar. Gallipoli Diary, Volume I |Ian Hamilton