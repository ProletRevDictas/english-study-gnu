She can finally walk the quadrangle, live in a dorm, go to class with a backpack, study in a library. Campus celebration and covid fear: Colleges reopen for a second fall under the pandemic shadow |Nick Anderson, Lauren Lumpkin, Susan Svrluga, Danielle Douglas-Gabriel |August 26, 2021 |Washington Post 

In 2007 Colvin led the sale of Dennis Publishing to Quadrangle. Reboot America! Participants |The Daily Beast |September 24, 2010 |DAILY BEAST 

Lloyd Grove talks to him about the Quadrangle mess, dishing on Rahm, and what's wrong with Washington. The Car Czar Fights Back |Lloyd Grove |September 20, 2010 |DAILY BEAST 

Quadrangle was not the only investment house to pay placement fees to Morris or funnel money into Chooch. Questions for Obama's Car Czar |Edward Jay Epstein |April 21, 2009 |DAILY BEAST 

Nor did Rattner's Quadrangle fund limit its placement to the New York State Pensions Fund. Questions for Obama's Car Czar |Edward Jay Epstein |April 21, 2009 |DAILY BEAST 

Afterward, Quadrangle paid a $1.1 million placement fee to a small firm, Searle Co., with which Morris was affiliated. Questions for Obama's Car Czar |Edward Jay Epstein |April 21, 2009 |DAILY BEAST 

He found the man, with his wife and children, in a large corner room opening on the inner court of the Mission quadrangle. Ramona |Helen Hunt Jackson 

In this beautiful quadrangle, one of the most interesting features is the chapel at the southwest corner. British Highways And Byways From A Motor Car |Thomas D. Murphy 

When he left the chamber of death, it was to walk across the quadrangle of the residence to the sexton's house. A Thin Ghost and Others |M. R. (Montague Rhodes) James 

Strictly a yard or quadrangle; hence a house which is built around a quadrangle: often Government House. The Cradle of Mankind |W.A. Wigram 

It stands in a quadrangle, roofed in, and above rise some of those curious elongated domes we saw from the boat. Round the Wonderful World |G. E. Mitton