In 2010, scientists dissatisfied with DSM’s often fuzzy, symptom-based definitions of mental disorders rebelled and tried to define mental conditions based on brain and behavioral measures. Psychology has struggled for a century to make sense of the mind |Bruce Bower |August 11, 2021 |Science News 

They always embarrass those who give them, and dissatisfy those who receive them. Reflections |Francois Duc De La Rochefoucauld 

To an Arab Sheik, loudest in importunity, he said: "What has happened since yesterday to dissatisfy thee with life?" The Prince of India, Volume II |Lew. Wallace 

Peter, very soon after our engagement you began to dissatisfy me because I realized that I should never satisfy you. The Dull Miss Archinard |Anne Douglas Sedgwick 

Mr. Pope had reason to be dissatisfy'd with the O in the second Line, and to reject it; for Homer has nothing of it. Letters Concerning Poetical Translations |William Benson 

The alarming way that each one did not throw away what was taken away did not dissatisfy every one. Matisse Picasso and Gertrude Stein |Gertrude Stein