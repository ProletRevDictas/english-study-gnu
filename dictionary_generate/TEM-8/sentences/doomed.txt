Unsurprisingly, despite the overall doom and gloom, some industries, which fit in this transformed layout really well, actually skyrocketed. Jingle all the way: What will 2021 mean to the advertising world? |Alex Zakrevsky |December 25, 2020 |Search Engine Watch 

With a deadly, highly contagious face cancer tearing through devil populations, forecasts over the past decade or so spelled imminent doom for the iconic marsupial. A highly contagious face cancer may not wipe out Tasmanian devils after all |Jonathan Lambert |December 10, 2020 |Science News 

Combine the pandemic with more than a quarter of a million dead, high unemployment, a health care system in crisis and a recession, and you’d presumably have a formula for entrepreneurial doom. Don’t Panic — You Could Just Go Into Business |Joshua Eferighe |December 6, 2020 |Ozy 

Look to other social media sites you’ve ignoredWhile Facebook and Twitter are the most obvious doom scrolling culprits, all of this goes for other sites and social networks, so you may also want to rethink how you use those platforms. Social media can be toxic. Here’s how to make sure your feeds aren’t. |Whitson Gordon |December 1, 2020 |Popular-Science 

Yet the drink managed to retain some passionate devotees, even as rumors of its impending doom circulated on and off over the years. The Rise And Fall Of Tab – After Surviving The Sweetener Scares, The Iconic Diet Soda Gets Canned |LGBTQ-Editor |November 29, 2020 |No Straight News 

Trying to fine-tune all that to a desired end is not only a form of madness but doomed to failure. How Naive is Elizabeth Warren? |Nick Gillespie |December 18, 2014 |DAILY BEAST 

Has she been doomed by the science of 2014 to a life of sexual misery? Was 2014 the Year Science Discovered The Female Orgasm? |Samantha Allen |December 6, 2014 |DAILY BEAST 

Imagine if you were doomed forever to live inside a youthful mistake. Here’s a Reform Even the Koch Brothers and George Soros Can Agree On |Tina Brown |November 10, 2014 |DAILY BEAST 

The Arab spring failed, but it demonstrated the ancient regimes are doomed unless they change profoundly, which is very unlikely. Why’s Al Qaeda So Strong? Washington Has (Literally) No idea |Bruce Riedel |November 9, 2014 |DAILY BEAST 

Ultimately, the changing threat and enormous price tag doomed the program and only three ships will be built at exorbitant cost. Can the Navy's $12 Billion Stealth Destroyer Stay Afloat? |Dave Majumdar |October 22, 2014 |DAILY BEAST 

“The British Raj is doomed,” she muttered, lowering her voice, and bringing her magnificent eyes close to his. The Red Year |Louis Tracy 

The poor woman forced back her tears in order to smile upon the unfortunate son whom she knew to be doomed. Bastien Lepage |Fr. Crastre 

I believe we thought so; and I know that Ennis, who was thus doomed to a further period of single blessedness, thought the same. Fifty Years of Railway Life in England, Scotland and Ireland |Joseph Tatlow 

But in all this, we were doomed to disappointment, sad, sad disappointment. The Condition, Elevation, Emigration, and Destiny of the Colored People of the United States |Martin R. Delany 

Louis the Goon Engel was a mere walk-on in the piece, a spear-carrier doomed to death. Hooded Detective, Volume III No. 2, January, 1942 |Various