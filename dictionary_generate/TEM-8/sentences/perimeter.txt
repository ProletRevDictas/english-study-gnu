Rollins walked the perimeter because anyone who wasn’t essential staff himself included — was prohibited from going inside. He Wanted to Fix Rural America’s Broken Nursing Homes. Now, Taxpayers May Be on the Hook for $76 Million. |by Max Blau for Georgia Health News |September 22, 2020 |ProPublica 

Khudobin wasn’t just making stops on low-quality shots from the perimeter — he was stopping everything. The Dallas Stars Are Putting Their Regular-Season Shortcomings Behind Them |Terrence Doyle |September 21, 2020 |FiveThirtyEight 

Even if defenses wanted to ignore perimeter players and load up in the paint, the Aces don’t give them any time to send that help. The Aces Don’t Need Threes To Win |Mike Prada |September 18, 2020 |FiveThirtyEight 

The sheet was 1 meter long — meaning the perimeter of your pen could be at most 1 meter — and weighed 1 kilogram, while each post weighed k kilograms. Can You Cover The Globe? |Zach Wissner-Gross |August 28, 2020 |FiveThirtyEight 

He handles the ball for us, he posts up, he’s on the perimeter, he’s the Defensive Player of the Year. LeBron And AD Dominate Like Kobe And Shaq. But Can They Win Like The Classic Lakers? |Robert O'Connell |August 24, 2020 |FiveThirtyEight 

An agent on the outer perimeter radios in that the motorcade is in sight. Behind the Scenes With a ‘Site Agent’: The Secret Service’s Hardest Job |Marc Ambinder |October 2, 2014 |DAILY BEAST 

Outside the lodge, running along its perimeter, was a small ditch lined by posts topped by a chest-high wooden beam. Spirit Tripping With Colombian Shamans |Chris Allbritton |August 24, 2014 |DAILY BEAST 

SWAT teams moved in and tear gas canisters were heard from the perimeter of what had essentially become a militarized zone. 'What You Gonna Do, Kill Us All?' Protesters Ask Ferguson Police |Justin Glawe |August 12, 2014 |DAILY BEAST 

On Wednesday night, as the main stage acts raged, Capt. Chris Slayman cruised the perimeter of the Gathering. A Report From the Misunderstood Gathering of the Juggalos |Steve Miller |July 28, 2014 |DAILY BEAST 

When the Stalwart vanguard reached the perimeter, their ranks broke in confusion. The GOP’s Last Identity Crisis Remade U.S. Politics |Michael Wolraich |July 24, 2014 |DAILY BEAST 

Loud-speaker trucks roamed along the perimeter, reassuring the people. The Flaming Mountain |Harold Leland Goodwin 

The line bounding a circle is termed its circumference or periphery and sometimes the perimeter. Mechanical Drawing Self-Taught |Joshua Rose 

The point which the mirror occupies on the arc of the perimeter, indicates the squint angle. Schweigger on Squint |C. Schweigger 

Four tangential slots (C) are cut into the perimeter of the pulley (B), and in each is a hardened steel roller (D). Practical Mechanics for Boys |J. S. Zerbe 

I was surprised to find out that this city within its protective wall—the perimeter—is the only one on the planet. Deathworld |Harry Harrison