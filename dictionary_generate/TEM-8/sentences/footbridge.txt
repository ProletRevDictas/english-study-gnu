At first glance, it looks like a simple footbridge, a gentle arch. This Swiss bridge proves it’s possible to reuse concrete |Rahul Rao |October 18, 2021 |Popular-Science 

“The idea that the walls of a building could become a footbridge—to our knowledge, this is completely new,” says Corentin Fivet, an architect and structural engineer at EPFL, and one of the bridge-builders. This Swiss bridge proves it’s possible to reuse concrete |Rahul Rao |October 18, 2021 |Popular-Science 

Fire officials on Wednesday had identified the location as Polk Street, which is about a half-mile north — the site of another footbridge. Collapsed D.C. pedestrian bridge was in poor condition before hit by truck, didn’t meet height standards |Luz Lazo, Dan Rosenzweig-Ziff |June 24, 2021 |Washington Post 

At my hotel, which was connected by a footbridge to the state’s largest hospital, the nonprofit Sanford Medical Center, young people mingled mask-free in the lobby, shouting gleefully over a case of Bud Lights. “Those of Us Who Don’t Die Are Going to Quit”: A Crush of Patients, Dwindling Supplies and the Nurse Who Lost Hope |by J. David McSwane |December 30, 2020 |ProPublica 

He remembered a footbridge the other volunteers had guessed at. Uncovering the Secrets of St. Kitts |Debra A. Klein |June 21, 2014 |DAILY BEAST 

In a clever twist of new-meets-old, the MuCEM is connected to the nearby 17th-century Fort St. Jean by footbridge. Watch Out, Paris! Marseille Is on the Rise |Anna Watson Carl |July 23, 2013 |DAILY BEAST 

Then I wandered on down, through the Rhodes grass, under the avocado trees, and across the wooden footbridge. My Parents' Brothel |Douglas Rogers |December 6, 2009 |DAILY BEAST 

Occasionally, over a narrow stream, a frail footbridge would be built. Our Little Korean Cousin |H. Lee M. Pike 

They were going to a distant meadow to toss hay and had to cross an angry little river on a footbridge made of one slender plank. Mighty Mikko |Parker Fillmore 

A low whistle sounded, and Mr. Rogers stepped into view on the footbridge. The Adventures of Harry Revel |Sir Arthur Thomas Quiller-Couch 

We can't be expected to jump off a footbridge which already has a White Horse on it. Mr Punch's Pocket Ibsen - A Collection of Some of the Master's Best Known Dramas |F. Anstey 

After about two hours, however, orders came to cross the river by the Eterpigny footbridge. The Story of the 6th Battalion, The Durham Light Infantry |Unknown