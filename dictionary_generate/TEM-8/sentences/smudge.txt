While this is great for finding smudges on the paint, it’s terrible for emphasizing the distance between the walls. Lighting tricks that will make small rooms feel gigantic |Hugh Neill |September 22, 2021 |Popular-Science 

The amFilm Tempered Glass Screen Protector is resistant to scratches, smudges, and even liquids without greatly affecting the touchscreen properties of the Switch’s screen. Level-up leisure time with the best Nintendo Switch accessories |Nicholas Ware |September 22, 2021 |Popular-Science 

ZEISS wipes remove smudges from fingerprints, dirt, grime, and bacteria. Choose the best screen cleaner to fight smudges and grime |Laura Davis |September 20, 2021 |Popular-Science 

Tiny drops of sweat create the fingerprint smudges used to identify us. ‘The Joy of Sweat’ will help you make peace with perspiration |Bethany Brookshire |July 13, 2021 |Science News 

On a recent morning, Mahi had a smudge of baby powder visible on her neck after taking a bath. These twins are 5 years old. They lost both parents to covid-19. |Joanna Slater |June 17, 2021 |Washington Post 

Dylan has gone for a stroke-and-smudge technique, blurring his pastels to create an atmospheric, out-of-focus, effect. Bob Dylan: Face Value Opens at London’s National Portrait Gallery |Chloë Ashby |August 26, 2013 |DAILY BEAST 

What happened with Broadwell will be seen as a smudge on his record. Like Jill Kelley, Paula Broadwell Eyes Comeback After Petraeus Scandal |Howard Kurtz |January 23, 2013 |DAILY BEAST 

Already the patch of brush in which lay the renegade Policemen was hidden in the smudge, shut away from our sight. Raw Gold |Bertrand W. Sinclair 

The pale glow of light which came from that powder smudge on Carlson's lapel was no longer visible! Hooded Detective, Volume III No. 2, January, 1942 |Various 

High on the left lapel of his dark suit coat was a white smudge made by some sort of powder. Hooded Detective, Volume III No. 2, January, 1942 |Various 

In the dooryard, a dull fire smoked in a tin pan,—a "smudge" to drive off the mosquitoes. A Yankee from the West |Opie Read 

This produced a grayish smudge, but a second and third application made a good black. Marjorie's Busy Days |Carolyn Wells