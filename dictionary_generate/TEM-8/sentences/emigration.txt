After the war, they got “faux married” for emigration papers, thus changing their names, and then, they changed them again to suit the languages of the countries where they ended up. Why the Stories of Jewish Women Who Fought the Nazis Remained Hidden for So Long |Judy Batalion |April 8, 2021 |Time 

These conflicts generated huge surges in emigration from Central America, establishing the migration patterns that persist today. The Situation at the U.S.-Mexico Border Can't Be 'Solved' Without Acknowledging Its Origins |Julia G. Young |March 31, 2021 |Time 

Chuseok, too, is a kind of new year for my mother and her emigration from Korea. The Meaning of Chuseok During a Pandemic Year |Nina Yun |October 9, 2020 |Eater 

Emigration, which hit epic levels in the 1980s and 1990s, seems to have tapered off. The Israel Debate In South Africa |Peter Beinart |January 29, 2013 |DAILY BEAST 

“This is not just a blip in emigration,” according to the Bundesagentur fur Arbeit report. We Are All Germans Now: Europeans Travel North Looking for Jobs |Barbie Latza Nadeau |September 8, 2012 |DAILY BEAST 

Some like Sam, who praised the policy, said it encourages gay emigration. ‘Outed’ by the Military, Some Gays Fleeing Iran |Omid Memarian, Roxana Saberi |July 28, 2012 |DAILY BEAST 

Either way, emigration is the end result for some gay Iranians. ‘Outed’ by the Military, Some Gays Fleeing Iran |Omid Memarian, Roxana Saberi |July 28, 2012 |DAILY BEAST 

(Agencies, Ynet) Dramatic drop of 35% in Israeli emigration to US—Lowest number since 2003. Erdogan: “We Will Act Against Assad” |Orly Halpern |April 11, 2012 |DAILY BEAST 

Emigration is now proceeding with gigantic strides, and is destined for some time to continue. Glances at Europe |Horace Greeley 

But this great re-emigration produced evils of no common magnitude in Brazil. Journal of a Voyage to Brazil |Maria Graham 

For this reason they introduced the subject of emigration to Canada, and a proper institution for the education of the youth. The Condition, Elevation, Emigration, and Destiny of the Colored People of the United States |Martin R. Delany 

This important inquiry we shall answer, and find a remedy in when treating of the emigration of the colored people. The Condition, Elevation, Emigration, and Destiny of the Colored People of the United States |Martin R. Delany 

The train of baggage, which is always sent on before on these occasions, resembles a small emigration party. A Woman's Journey Round the World |Ida Pfeiffer