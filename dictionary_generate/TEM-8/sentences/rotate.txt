He rotated in for fellow Serb Filip Krajinovic in the second set, later crediting his return to the cheering crowd. Serena Williams, Novak Djokovic, out of quarantine, thankful to play in front of thousands of fans |Glynn A. Hill |January 30, 2021 |Washington Post 

All of them allow players to rotate the barrel and adjust the position of that ridge. Nerf’s newest blaster shoots spinning balls for dramatic curves |Stan Horaczek |January 27, 2021 |Popular-Science 

Being able to rotate your collection as you rotate through seasonal colors is a slick, stylish option for organization. Nail polish organizers to keep your collection in check |PopSci Commerce Team |January 20, 2021 |Popular-Science 

Allowing for dynamic keyword inclusion and rotating ad copy narratives is just the beginning of fully automated contextual success. 2021 Search marketing: The year of automation |Merkle Inc. |January 20, 2021 |Search Engine Watch 

Google Adwords rotates ads automatically within the ad group and displays, more often, the better-performing one. Ways to get the most out of your Google Adwords PPC strategy |Jacob M. |January 19, 2021 |Search Engine Watch 

According to his suit, Carleton would rotate four new boys into his home every semester. Headmasters Behaving Badly |Emily Shire |November 29, 2014 |DAILY BEAST 

In an airline seat, the hips and pelvis rotate forward and the S curve flattens. Flying Coach Is the New Hell: How Airlines Engineer You Out of Room |Clive Irving |November 25, 2014 |DAILY BEAST 

As the panels rotate to track the sun, they produce more than enough power to move skiiers up the mountain. Solar Powered Ski Lift |The Daily Beast |November 24, 2014 |DAILY BEAST 

Rotate those chairs, and senators might rotate back to their states earlier than usual. A Conservative Explains Why The GOP Could Lose |Bill Whalen |November 2, 2014 |DAILY BEAST 

Each facility has a Rabbi, though some of the smaller ones have traveling Rabbis that rotate from joint to joint. A Jewish Ex-Con Recalls Keeping Kosher with the Faithful in Prison |Daniel Genis |May 11, 2014 |DAILY BEAST 

We must add that this vast world, like the Sun, does not rotate all in one period. Astronomy for Amateurs |Camille Flammarion 

The most curious fact is that these satellites do not rotate like those of the other planets. Astronomy for Amateurs |Camille Flammarion 

It would rotate for hours like on a spit—almost no friction. The Planet Strappers |Raymond Zinke Gallun 

The ortho molecules rotate with odd rotational quantum numbers, while the para molecules rotate with even quantum numbers. Unwise Child |Gordon Randall Garrett 

It is only necessary to rotate the desired jet into position in order to connect it with both gas and air supplies. A Handbook of Laboratory Glass-Blowing |Bernard D. Bolas