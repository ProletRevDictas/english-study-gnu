His discourse is now more detailed: submission, which is the meaning of islam in Arabic, gives him a kind of enjoyment. Houellebecq’s Incendiary Novel Imagines France With a Muslim President |Pierre Assouline |January 9, 2015 |DAILY BEAST 

As far as I can tell, this magazine spent as much time making fun of French politicians as it did of Muslims or Islam. Harry Shearer on The Dangerous Business of Satire |Lloyd Grove |January 8, 2015 |DAILY BEAST 

In Dresden, Germany, anti-Islam rallies each week draw thousands of demonstrators. Police Hunt for Paris Massacre Suspects |Tracy McNicoll, Christopher Dickey |January 7, 2015 |DAILY BEAST 

The town, known in Arabic as Ayn al-Arab, is so significant to ISIS that the group calls it Ayn al-Islam. The Brothers Who Ambushed ISIS |Mohammed A. Salih |December 27, 2014 |DAILY BEAST 

In any case, Pakistan as a nation has been unforgiving of any slights against Islam. Disco Mullah Blasphemy Row Highlights Pakistan’s Hypocrisy |Shaheen Pasha |December 21, 2014 |DAILY BEAST 

And the prince called Allah to witness that their troubles were at an end; that three years should see them masters of all Islam. God Wills It! |William Stearns Davis 

Many went boldly to the Moslem camp, and confessed Islam in return for a bit of bread. God Wills It! |William Stearns Davis 

Again and again have the Sultans shown their inability to defend the frontiers of Islam. The Contemporary Review, January 1883 |Various 

He had been reared in the religion of Mahomet, and with the faith he held the customs of Islam. A German Pompadour |Marie Hay 

The enterprise of Islam underwent several ebbs and flows over this region. Encyclopaedia Britannica, 11th Edition, Volume 11, Slice 8 |Various