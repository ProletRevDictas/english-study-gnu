The University of Illinois is requiring all faculty, staff and students to participate in screening testing twice a week, using a rapid saliva-based test. America Doesn’t Have a Coherent Strategy for Asymptomatic Testing. It Needs One. |by Caroline Chen |September 1, 2020 |ProPublica 

Under the terms of the deal, University of Arizona will create a non-profit entity called University of Arizona Global Campus that will maintain its own accreditation, faculty, and academic programs. Public universities are buying the for-profit schools their professors criticize |Michelle Cheng |August 23, 2020 |Quartz 

The reporting prompted the university to end the use of confidentiality clauses when professors are fired and change policy to prevent faculty and administrators from arguing that academic freedom shields them in sexual misconduct cases. ProPublica Selects Six Public Broadcasting Projects for Local Reporting Network |by ProPublica |August 21, 2020 |ProPublica 

She completed her training at the University of Washington and joined the faculty in 1982, eventually being promoted to research professor. Training clinicians to spot heart failure in covid-19 patients |Tate Ryan-Mosley |August 19, 2020 |MIT Technology Review 

Others, including the University of North Carolina system, are developing worst-case scenario plans, where drops in enrollment could lead to employee furloughs, faculty cuts and suspended athletic programs. The (Deferred) Class of 2020 |Sandya Kola |August 9, 2020 |Ozy 

In all these cases, the students and even faculty members plead ignorance. Hitler is Huge in Thailand: Chicken Joints, T-Shirts, and A Govt.-Issued Propaganda Video |Marlow Stern |December 10, 2014 |DAILY BEAST 

All students and faculty in the UT community should support the cause of fairness in admissions. The University of Texas’s Machiavellian War on Its Regent |David Davis |October 27, 2014 |DAILY BEAST 

Is it the faculty of reason, or perhaps, the faculty for discourse? The Bioethicist Turned Butcher |Elizabeth Picciuto |September 28, 2014 |DAILY BEAST 

I find faculty learning about their specific, specialized research areas, but also about the wider society and natural world. The Elite American College Pile-On |Michael S. Roth |September 15, 2014 |DAILY BEAST 

Benson concluded, “If we are not willing to hire such faculty, they are not willing to fund us.” Koch Foundation to College: We’ll Give You Millions—if You Teach Our Libertarian Ideology |Center for Public Integrity |September 12, 2014 |DAILY BEAST 

We were speaking of the faculty of mimicry, and he told me such a funny little anecdote about Chopin. Music-Study in Germany |Amy Fay 

There is no ground for the assertion that a spiritual faculty exists apart from the reason. God and my Neighbour |Robert Blatchford 

This seems to amount to a claim that religious people possess an extra sense or faculty. God and my Neighbour |Robert Blatchford 

The faculty of reason, then, has excelled this boasted faculty of spiritual discernment in its own religious sphere. God and my Neighbour |Robert Blatchford 

But the Christian first invents this faculty, and then tells us that by this faculty religion is to be judged. God and my Neighbour |Robert Blatchford