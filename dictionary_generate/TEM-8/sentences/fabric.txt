Researchers largely concur that the power of rituals rests within a larger social fabric. Why do we miss the rituals put on hold by the COVID-19 pandemic? |Sujata Gupta |August 14, 2020 |Science News 

The world of gravitons only becomes apparent when you zoom in to the fabric of space-time at the smallest possible scales, which requires a device that can harness truly extreme amounts of energy. How the Bits of Quantum Gravity Can Buzz |Thomas Lewton |July 23, 2020 |Quanta Magazine 

“The only way to add parks would be to destroy the historic fabric of the neighborhood and that’s never acceptable,” Torio said. If Your Local Park Sucks, This Is Probably Why |MacKenzie Elmer |July 6, 2020 |Voice of San Diego 

The effectiveness of fabric masks was in question early on, but studies now suggest that these masks can help curb transmission of the virus — if most people wear them. Here’s what we’ve learned in six months of COVID-19 — and what we still don’t know |Erin Garcia de Jesus |June 30, 2020 |Science News 

The structure of the molecules that make up those fabrics lets them attract electrons or give them up, Guha explains. Science offers recipes for homemade coronavirus masks |Kathiann Kowalski |May 14, 2020 |Science News For Students 

It's about the delicate fabric of the universe and how our fragile insides crumble when that fabric is torn. Alfred Hitchcock’s Fade to Black: The Great Director’s Final Days |David Freeman |December 13, 2014 |DAILY BEAST 

These are palpable, identifiable matters that are ingrained into the very fabric of The Babadook. ‘The Babadook’ Is the Best (and Most Sincere) Horror Movie of the Year |Samuel Fragoso |November 30, 2014 |DAILY BEAST 

The $1,000 dress did not photograph particularly well, either, thanks to the mixture of sheer and non-sheer fabric. Kate Middleton, the Preggers Fashion Princess |Tom Sykes |November 14, 2014 |DAILY BEAST 

Galeria is a collage of quotations: columns, chrome black tables, panels with English paisley fabric. Russia’s Gold Digger Academy |Peter Pomerantsev |November 11, 2014 |DAILY BEAST 

You even went out and bought the fabric for your own Oscar dress, which would be unthinkable for an actress to do today. All Eyes on Anjelica Huston: The Legendary Actress on Love, Abuse, and Jack Nicholson |Alex Suskind |November 10, 2014 |DAILY BEAST 

The villain Longcluse, and the whole fabric of his machinations, may be dashed in pieces by a word. Checkmate |Joseph Sheridan Le Fanu 

He knew that the whole fabric of crime was due to the human reading of His "revelation" to man. God and my Neighbour |Robert Blatchford 

Such a theory is ridiculous; but upon it depends the entire fabric of Christian theology. God and my Neighbour |Robert Blatchford 

Christianity is a fabric of impossibilities erected upon a foundation of error. God and my Neighbour |Robert Blatchford 

And so the whole fabric of geological chronology vanishes into a mere unproved notion, based upon an if. Gospel Philosophy |J. H. Ward