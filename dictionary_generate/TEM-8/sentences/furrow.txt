Imprinted on a knobby rock about as big as an orange were the folds, furrows and even blood vessels of a brain. Fossils and ancient DNA paint a vibrant picture of human origins |Erin Wayman |September 15, 2021 |Science News 

Her eyebrows furrow with pain, her mouth falls open in shock, her hand reaching out to be saved. Hallucinating Away a Heroin Addiction |Abby Haglage |May 4, 2014 |DAILY BEAST 

He was fidgety, furrow-faced, almost entirely unsmiling, and largely inarticulate. New York's Insane Political Circus |Tunku Varadarajan |October 18, 2010 |DAILY BEAST 

"He wouldn't be likely to notice you if you crept along the bottom of a furrow," Mr. Blackbird assured Grandfather Mole. The Tale of Grandfather Mole |Arthur Scott Bailey 

I picked up the handles and lifted the plough around, setting the point to the new furrow. The Idyl of Twin Fires |Walter Prichard Eaton 

In fact, there had never been an owner for the land nor a furrow turned here since the dawn of creation. A Fortune Hunter; Or, The Old Stone Corral |John Dunloe Carteret 

Oar and keel, pebble and arrow, wind and current, are alike powerless to make a furrow that shall last. A Charming Fellow, Volume II (of 3) |Frances Eleanor Trollope 

Behind him wavered a long, deep-gouged furrow-trail, pitiful attest of suffering. Blazed Trail Stories |Stewart Edward White