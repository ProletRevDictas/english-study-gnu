In keeping with the expert consensus on decarbonization, the crux of China’s odyssey is electrifying its economy as much as possible, from switching to electric vehicles to using electricity instead of coal for some industrial production. China’s commitment to become carbon neutral by 2060, explained |Lili Pike |September 25, 2020 |Vox 

That’s the crux of Mulan, in which our heroine realizes, through pretending to be a perfect bride and pretending to be a perfect soldier and failing at both, that gender is ultimately performative. Toward a queer Disney canon |Emily VanDerWerff |September 4, 2020 |Vox 

That’s the whole crux of the fight that’s happening right now with Tik Tok. Podcast: Want consumer privacy? Try China |Michael Reilly |August 19, 2020 |MIT Technology Review 

The crux of this issue is in transitioning away from the view that work gives life meaning and life is about using work to survive, towards a view of living a life that itself is fulfilling and meaningful. The Global Work Crisis: Automation, the Case Against Jobs, and What to Do About It |Peter Xing |August 6, 2020 |Singularity Hub 

It seems like the neighbor cells are the crux, in addition to the encoding neurons themselves, the team explained. Towards ‘Eternal Sunshine’? New Links Found Between Memory and Emotion |Shelly Fan |July 28, 2020 |Singularity Hub 

The crux of the problem remains on this side of the Pacific. Obama and Xi Jinping Say They’ll Work Together to Save Environment |Ben Leung |November 12, 2014 |DAILY BEAST 

The crux of the matter is not the date of the next elections, but ensuring that elections are free, fair, and clean. Thailand: Into the Void |Lennox Samuels |March 21, 2014 |DAILY BEAST 

That, he says, is at the crux of why Pope Francis wants to train more exorcists. Vatican and Pope Francis Seek New Demon Exorcists |Barbie Latza Nadeau |January 8, 2014 |DAILY BEAST 

And yet, despite the banter, the crux of the issue is the feasibility of it all. IVF for Just $300 Could Be a Reality Soon |Randi Hutter Epstein |August 31, 2013 |DAILY BEAST 

The crux is new representation: of body, of proportion, of aesthetic ideals. Monsters of Fashion Exhibition Opens in Paris |Sarah Moroz |February 20, 2013 |DAILY BEAST 

The very word (crux) was used among them as a curse, especially in the form ad (malam) crucem. The Private Life of the Romans |Harold Whetstone Johnston 

Ah, that was it—that was the crux of the whole matter; and he remembered now that never once had she reproached him with that. Katharine Frensham |Beatrice Harraden 

Priests are portrayed in adoration of the crux ansata before phallic monuments. The Sex Worship and Symbolism of Primitive Races |Sanger Brown, II 

Every Sunday morning proved the crux of her experience, and Mrs. Caukins' nerves were correspondingly shaken. Flamsted quarries |Mary E. Waller 

The crux of the whole matter is not exhaustion, but a loss of control over the nervous forces. Nervous Breakdowns and How to Avoid Them |Charles David Musgrove