Rather, I understood such zealous procedures as necessary security precautions. An infuriating indictment of Israel |Randy Rosenthal |November 5, 2021 |Washington Post 

Although zealous in defense of their own rights and freedoms, they are less concerned about the rights and freedoms of those who are not like them. Our constitutional crisis is already here |Robert Kagan |September 23, 2021 |Washington Post 

Since no amount of logical arguments, cajoling or bribery will change the minds of zealous conspiracy theorists, Fauci probably won’t convince skeptics that the NIH bigwig is a paragon of public service. The Heavy Burden of Being Dr. Anthony Fauci |Nick Schager |September 10, 2021 |The Daily Beast 

To get it right, it’s important not to be over zealous on the number of channels used, within whatever company tools are used to communicate — whether it’s Microsoft Teams, Salesforce, Google Docs. Experimentation, meeting moderators and asynchronous working: What Microsoft has learned from its hybrid-working model |Jessica Davies |August 26, 2021 |Digiday 

It’s easy to get over-zealous and buy grilling tools that you’ll never use. Optimal Grilling Accessories and Grilling Tools For The Best Backyard BBQ |Irena Collaku |June 25, 2021 |Popular-Science 

These labels matter, but so does our over-zealous urge to dole them out and endlessly dwell on them. Lena Vs. The Feminist Police |Amy Zimmerman |November 9, 2014 |DAILY BEAST 

Americans have a right to fear over-zealous and unwarranted surveillance by the NSA. Ebola, ISIS, the Border: So Much to Fear, So Little Time! |Gene Robinson |November 2, 2014 |DAILY BEAST 

The extremely zealous President Kevin Baugh has been issuing war bonds to raise funds in case fighting becomes necessary. So You Want to Rule a Kingdom? A Wacky History of One-Man Nations |Nina Strochlic |July 17, 2014 |DAILY BEAST 

Zealous populist patriots might pal around on principle, but banding together effectively is another matter. Elections Could Be the Beginning of the End for Europe |Tracy McNicoll, Nadette De Visser |May 21, 2014 |DAILY BEAST 

Some of them were brought up in the faith; some are zealous converts. Teenage Girls Seduced by the Syrian Jihad? |Christopher Dickey |April 21, 2014 |DAILY BEAST 

He was so zealous a partisan of democracy, and of Cromwell, that the authorities frequently placed him in a straight jacket. The Every Day Book of History and Chronology |Joel Munsell 

Paul Egede died, aged 81; author of an Account of Greenland, and a zealous missionary there. The Every Day Book of History and Chronology |Joel Munsell 

He found himself among kinsmen who were zealous Roman Catholics. The History of England from the Accession of James II. |Thomas Babington Macaulay 

Vernon was a zealous Whig, and not personally unacceptable to the chiefs of his party. The History of England from the Accession of James II. |Thomas Babington Macaulay 

But, though Portland was an unreasonable and querulous friend, he was a most faithful and zealous minister. The History of England from the Accession of James II. |Thomas Babington Macaulay