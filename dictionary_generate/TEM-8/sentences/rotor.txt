Standing 866 feet tall, the turbine only has a few feet on the Haliade-X’s height, but its rotor is the differentiator at 794 feet across. The World’s Biggest Wind Turbine Is Being Built in China |Vanessa Bates Ramirez |August 25, 2021 |Singularity Hub 

It stretches 65 feet long, if you count the spinning rotors—a far-cry from the hummingbirds McCabe evoked. What it’s like to rescue someone at sea from a Coast Guard helicopter |Rob Verger |July 20, 2021 |Popular-Science 

He spent considerable time describing the carbon-sleeved rotors for the motor, which Musk claims is a first for a production electric motor due to the difficulty of pulling it off. Elon Musk reveals the Tesla Model S Plaid |Rebecca Bellan |June 11, 2021 |TechCrunch 

On Earth, most helicopters and drones have rotors that spin at about 400-500 revolutions per minute. A Helicopter Flew on Mars for the First Time. A Space Physicist Explains Why That’s Such a Big Deal |Gail Iles |April 23, 2021 |Singularity Hub 

Those rotors are longer than what a similar vehicle would need to fly on Earth. Ingenuity helicopter makes history by flying on Mars |Lisa Grossman |April 20, 2021 |Science News For Students 

So serious is this heat that it can distort a major APUS engine component, the rotor shaft, and cause significant damage. Planes in Flames: Why Does It Keep Happening? |Clive Irving |July 15, 2013 |DAILY BEAST 

Next he says a double rotor Chinook landed inside the compound. Osama Was My Neighbor |Ron Moreau, Sami Yousafzai |May 2, 2011 |DAILY BEAST 

The coil represents the stationary part, the stator (Fig. 445) and the cup the rotating part, the rotor, of an induction motor. Physics |Willis Eugene Tower 

The difference between the rate of rotation of the rotor and that of the magnetic field is called the "slip." Physics |Willis Eugene Tower 

The rotor circuits are, therefore, closed upon themselves in normal operation. The New Gresham Encyclopedia |Various 

For starting, a polyphase resistance completes the circuits of the rotor winding. The New Gresham Encyclopedia |Various 

The currents in the rotor winding are induced by the action of the rotating magnetic field set up by the stator currents. The New Gresham Encyclopedia |Various