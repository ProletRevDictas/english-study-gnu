In the case of Mischief’s Putin and Kim deepfakes, however, the actors have remained anonymous for “personal security reasons,” the team said, because of the controversial nature of manipulating the images of dictators. Inside the strange new world of being a deepfake actor |Karen Hao |October 9, 2020 |MIT Technology Review 

A majority of the years when I lived there and in their lifetime, it was ruled by a dictator. Ilhan Omar: Capitalism Cannot Deliver Social Justice |Nick Fouriezos |October 1, 2020 |Ozy 

If hundreds of thousands of people get in the streets and won’t go home, it becomes harder and harder for a dictator to hold on. Why You Should Care About Belarus |Tracy Moran |August 17, 2020 |Ozy 

Either out of the chaos in the absence of a fallen dictator, it’s conceivable that a person like Svetlana could come to power. Why You Should Care About Belarus |Tracy Moran |August 17, 2020 |Ozy 

I do not mind the fact that he sat down with dictators like Kim Jong-un. Why Is This Man Running for President? (Ep. 362 Update) |Stephen J. Dubner |December 19, 2019 |Freakonomics 

You were basically the guy to do every dictator or crazy character, from Gaddafi and Ahmadinejad to Bin Laden. Coffee Talk with Fred Armisen: On ‘Portlandia,’ Meeting Obama, and Taylor Swift’s Greatness |Marlow Stern |January 7, 2015 |DAILY BEAST 

And for Larry Flynt, this might be a monumental opportunity to stick it to the dictator the best way he knows how. Kim Jong Un, Avert Your Eyes: Sony’s ‘The Interview’ Gets the Porn Parody Treatment |Aurora Snow |December 20, 2014 |DAILY BEAST 

And, the Chilbosan would make a hell of a comedy movie; “Fawlty Towers” meets the “Great Dictator.” Inside the ‘Surprisingly Great’ North Korean Hacker Hotel |Michael Daly |December 20, 2014 |DAILY BEAST 

He added, “We cannot have a society in which some dictator someplace can start imposing censorship here in the United States.” Exclusive: Sony Emails Reveal Studio Head Wants Idris Elba For the Next James Bond |William Boot |December 19, 2014 |DAILY BEAST 

MIAMI — Fidel Castro seized power in January 1959 after waging a guerilla war against then-dictator Fulgencio Batista. Cuba Is A Kleptocracy, Not Communist |Romina Ruiz-Goiriena |December 19, 2014 |DAILY BEAST 

The dictator Tubertus Posthumus gained a victory over the qui and Volsci, inconsiderable but noxious enemies of the commonwealth. The Every Day Book of History and Chronology |Joel Munsell 

Civil war in Paris; barricades erected, and a terrible slaughter of the people; general Cavaignac declared dictator. The Every Day Book of History and Chronology |Joel Munsell 

Mazzini was made a Triumvir, and henceforth became little less than dictator. The Life of Mazzini |Bolton King 

Pallavicino, the pro-dictator, Manin's old co-worker and Garibaldi's friend, courteously appealed to him to leave. The Life of Mazzini |Bolton King 

At the head of the group stood Goethe, who was then the dictator of public opinion in esthetic questions. Tolstoy on Shakespeare |Leo Tolstoy