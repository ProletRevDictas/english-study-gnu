The face of this gregarious, appealing world traveler had become familiar to almost everybody, even those who had never seen Parts Unknown or any of his other shows. Roadrunner Illuminates Anthony Bourdain the Man—But Are Its Means Totally Legit? |Stephanie Zacharek |July 16, 2021 |Time 

His father Charles, a gregarious Cuban whose parents were Turkish and Polish transplants, owned a steel-wool factory and expected to lose it in Castro’s imminent nationalization of businesses. Can One Agency Keep the U.S. Safe and Still Be Humane? The New DHS Chief Thinks So |Alana Abramson |May 12, 2021 |Time 

As a young girl, she looked up to Willie Wood, the gregarious, hard-hitting defensive back. More football leads to worse CTE, scientists say. Consider NFL great Willie Wood. |Rick Maese |March 12, 2021 |Washington Post 

Instead, Bond speculates that gregarious females might suffer less stress. Having more friends may help female giraffes live longer |Susan Milius |February 25, 2021 |Science News 

He projected strength even while forced to follow orders, and was well liked and gregarious though in the end a mystery even to many who spent time with him. The Murder Chicago Didn’t Want to Solve |by Mick Dumke |February 25, 2021 |ProPublica 

Alexander is everything Turing is not—gregarious, flirty, and, you guessed it, charming. From ‘The Good Wife’ to ‘The Imitation Game’: Matthew Goode Wages His Charm Offensive |Kevin Fallon |November 24, 2014 |DAILY BEAST 

Dubya, for all his manifest faults, is a very gregarious guy. Harry Shearer on Being Nixon, ‘The Simpsons Movie’ Sequel, and Why Obama Should Return His Nobel |Marlow Stern |October 21, 2014 |DAILY BEAST 

Onscreen, Teller is a bit like a young Vince Vaughn—gregarious, charming, and a tad suspicious. Miles Teller’s Movie Star Moment: From the Brink of Death to ‘Whiplash’ |Marlow Stern |October 14, 2014 |DAILY BEAST 

In person, Reiner is gregarious and very chatty, regaling you with great anecdotes from his back catalogue. Rob Reiner on the State of Romcoms, ‘The Princess Bride’s’ Alternate Ending, and the Red Viper |Marlow Stern |July 27, 2014 |DAILY BEAST 

He was gregarious and sociable, enjoying the company of entourages whenever he went to Cannes or some other film festival. My Friend, Roger Ebert: Pulitzer Prize Winner Tom Shales on the Moving Documentary ‘Life Itself’ |Tom Shales |July 6, 2014 |DAILY BEAST 

Less marked instances appear in the elephants, in some of the birds, and in certain other gregarious animals. Man And His Ancestor |Charles Morris 

Mr. Bradlaugh had to hold together a different species, with leaping legs, butting horns, and a less gregarious tendency. Reminiscences of Charles Bradlaugh |George W. Foote 

Such a lot of fuss is made in the world by ignoring the great fact that man is by nature both gregarious and polygamous. The Way of a Man |Emerson Hough 

There are immeasurable differences between the gregarious man and the man who lives closest to nature. Z. Marcas |Honore de Balzac 

What do you understand Trotter to mean by the gregarious instinct as a mechanism controlling conduct? Introduction to the Science of Sociology |Robert E. Park