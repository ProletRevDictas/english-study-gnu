In the experiment, a laser blast vaporized a target, sending a burst of plasma careening into a cloud of hydrogen gas. Physicists caught protons ‘surfing’ on shock waves |Emily Conover |August 26, 2021 |Science News 

When that powerful laser burst hits the cylinder, X-rays stream out, vaporizing the capsule’s exterior and imploding the fuel within. With a powerful laser blast, scientists near a nuclear fusion milestone |Emily Conover |August 18, 2021 |Science News 

Bitcoin clawed back some of its declines Thursday after suffering brutal losses in a broad sell-off that vaporized several billion dollars in the cryptocurrency market. Bitcoin claws back as Wall Street breaks losing streak |Hamza Shaban |May 20, 2021 |Washington Post 

He puts the chicken in the branches of a tree, walks back 20 feet, aims and vaporizes the tree and the chicken. Gene Weingarten: I’ve got a few gender-reveal ideas for parents |Gene Weingarten |May 20, 2021 |Washington Post 

Chemicals may break down in the soil, vaporize into the atmosphere or leach away. Nanoscale nutrients can protect plants from fungal diseases |Shi En Kim |May 3, 2021 |Science News 

Then, in May 2016, Scotland would become independent—and suddenly, bam, those 40-odd Labour-held seats would vaporize. An Independent Scotland Will Hurt Labour |Michael Tomasky |September 17, 2014 |DAILY BEAST 

Even the things he supposedly got out of this deal could vaporize. Obama Gives It All Away |Michael Tomasky |July 31, 2011 |DAILY BEAST 

The crude petroleum is distilled as rapidly as possible with fire heat to vaporize off the naphthas and the burning oils. Aviation Engines |Victor Wilfred Pag 

These vaporize in the arc producing the flaming arc light of a bright yellow color, and give more light than the ordinary lamp. Physics |Willis Eugene Tower 

We simply apply heat to melt the ice and then more heat to vaporize the water. The Popular Science Monthly, September, 1900 |Various 

We put in more dampers, but it kept going up and up, and I thought it might vaporize any minute. The Star Lord |Boyd Ellanby 

The hot gases given off by the burning fuel travel around this tank and vaporize the water which it contains. Gas-Engines and Producer-Gas Plants |R. E. Mathot