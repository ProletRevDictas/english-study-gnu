Eventually, users will be able to hail a scooter, which will travel up to several blocks to their location, according to Tortoise co-founder and president Dmitry Shevelenko. Spin bets its scooter future on 3 wheels and remote-control tech |Kirsten Korosec |January 27, 2021 |TechCrunch 

“Adding mopeds, coupling that with bikes and scooters, allows riders to choose a single platform to get them from point to point and not be restricted as to where they want to go around the city,” Arroyo said. More mopeds may be coming to the District this spring |Luz Lazo |January 27, 2021 |Washington Post 

While the batteries are expected to cost a similar amount to conventional lithium-ion technology, so far the company has only demonstrated them in an electric scooter. New Fast-Charging, Low-Cost Batteries Could Be a Game-Changer for Electric Cars |Edd Gent |January 25, 2021 |Singularity Hub 

That’s the idea behind a consortium of bike and scooter manufacturers coming together to develop and test new safety software that would allow forms of micromobility to communicate with nearby cars. Ford is researching safety tech allowing bikes, e-scooters to ‘talk’ to cars |Dalvin Brown |January 13, 2021 |Washington Post 

Cities also took issue with how often people dumped scooters in places they didn’t belong. Ford’s Spin unit to boost e-scooters with software that alerts pedestrians of sidewalk riders |Dalvin Brown |December 17, 2020 |Washington Post 

It was the last vehicle, aside from the occasional scooter, that we would see until the end of the trip. A Little Too Off the Beaten Path in Burma |Katya Cengel |June 2, 2014 |DAILY BEAST 

Scooter Braun came across an earthbound angel on YouTube in 2008. Justin Bieber’s No Good, Very Bad Year: Alleged Prostitutes, Reckless Driving, and More |Amy Zimmerman |December 13, 2013 |DAILY BEAST 

I hopped on my scooter and roared down to the store and stopped to grab some bananas (they were on the way). Grand Theft Auto V Review: The Best Game Ever?! |Winston Ross |September 17, 2013 |DAILY BEAST 

I know your manager, Scooter Braun, also represents Justin Bieber. Psy on New Single ‘Gentleman,’ Kim Jong-un, Justin Bieber & More |Marlow Stern |April 29, 2013 |DAILY BEAST 

He then whistles at another young-looking girl, hands her a FFF flyer, and then pouts when she speeds away on her scooter. Meet the Germans Having Sex to Save the World |Marlow Stern |March 13, 2013 |DAILY BEAST 

The two chums owned a small iceboat which went, on Lake Luna, by the name of scooter. The Girls of Central High on the Stage |Gertrude W. Morrison 

Like most of them, Frank Nelsen took the scooter up alone, from the start. The Planet Strappers |Raymond Zinke Gallun 

The scooter wavered, and they landed hard, even at reduced speed. The Planet Strappers |Raymond Zinke Gallun 

A scooter pulled up alongside them almost at once, with a gun-carrying mobster riding it. Police Your Planet |Lester del Rey 

He found Randolph waiting in a scooter outside the precinct house after he'd reported his results. Police Your Planet |Lester del Rey