According to Mnuchin, a standalone PPP bill would be the “easiest” way to resume helping businesses. Why lawmakers may choose a more targeted approach for the second round of COVID stimulus |Aric Jenkins |September 16, 2020 |Fortune 

Also, the side handles and lid make carrying it much easier. Attractive laundry hampers that make your dirty clothes look a little better |PopSci Commerce Team |September 16, 2020 |Popular-Science 

Second, clinicians and patients should have easy access to that information, including amounts and potential adverse effects. The ‘inactive' ingredients in your pills could harm you |By Yelena Ionova/The Conversation |September 15, 2020 |Popular-Science 

It has fewer options than Zoom, which makes it easier for kids to operate, but it’s even easier to use with these handy shortcuts. Make online classes easier with these laptop shortcuts |Sandra Gutierrez G. |September 15, 2020 |Popular-Science 

Once home values begin a one-way plummet, it’s easy for economists to see how entire communities spin out of control. Climate Change Will Force a New American Migration |by Abrahm Lustgarten, photography by Meridith Kohut |September 15, 2020 |ProPublica 

Talking about death is never easy, but with food, comfort, and familiarity, a new kind of dinner party is making it easier. Everyone at This Dinner Party Has Lost Someone |Samantha Levine |January 6, 2015 |DAILY BEAST 

The first thing they told us was that the traffickers are now using Turkish ports, which are relatively easy to reach from Syria. Ghost Ships of the Mediterranean |Barbie Latza Nadeau |January 6, 2015 |DAILY BEAST 

You know, when I was younger, I used to make problems for myself, like it was too easy. The Story Behind Lee Marvin’s Liberty Valance Smile |Robert Ward |January 3, 2015 |DAILY BEAST 

They selected an “easy mark” who turned out to be an off-duty NYC Housing Authority cop named James Carragher. His First Day Out Of Jail After 40 Years: Adjusting To Life Outside |Justin Rohrlich |January 3, 2015 |DAILY BEAST 

This will make it easy to pour the flour mixture into the stand mixer. Make ‘The Chew’s’ Carla Hall’s Sticky Toffee Pudding |Carla Hall |December 28, 2014 |DAILY BEAST 

A constant sense of easy balance should be developed through poising exercises. Expressive Voice Culture |Jessie Eldridge Southwick 

Big Reginald took their lives at pool, and pocketed their half-crowns in an easy genial way, which almost made losing a pleasure. The Pit Town Coronet, Volume I (of 3) |Charles James Wills 

The first jolt had like to have shaken me out of my hammock, but afterwards the motion was easy enough. Gulliver's Travels |Jonathan Swift 

His Indian repute had not preceded him to such degree as to make the way easy for him through the London crowd. Kipling Stories and Poems Every Child Should Know, Book II |Rudyard Kipling 

The endless miles of railways, the vast apparatus of the factories, the soaring structures of the cities bear easy witness to it. The Unsolved Riddle of Social Justice |Stephen Leacock