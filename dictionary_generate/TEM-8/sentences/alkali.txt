It starts by trapping atoms—often alkali metals—in a magnetic cage, then shooting a laser at them. How the world’s first ‘quantum tornadoes’ came to be |Rahul Rao |January 17, 2022 |Popular-Science 

For example, the far-left column contains sodium and the other so-called alkali metals. Explainer: What is a metal? |Katie Grace Carpenter |January 12, 2022 |Science News For Students 

However incidental case reports show that milk-alkali syndrome still pops up from time-to-time, and often it stems from taking far too much calcium carbonate, or Tums. What happens if you eat too many Tums? |Claire Maldarelli |December 5, 2021 |Popular-Science 

Hydrolysis of polyester is a chemical reaction that occurs when polyesters decompose, it can occur with alkalis and acids, hence the need for stomach acids from a cow’s rumen to potentially dissolve plastics. Microbes found in cow stomachs could help solve our plastic problem |Sara Kiley Watson |July 13, 2021 |Popular-Science 

Both alkaline and alkali come from the Arabic word “al-qili.” Scientists Say: Alkaline |Bethany Brookshire |May 3, 2021 |Science News For Students 

What appears to be ground glass is only the little crystals or small particles of alkali that have not been dissolved. Tobacco; Its History, Varieties, Culture, Manufacture and Commerce |E. R. Billings. 

He was one of the men to whom alkali is a constant poison, and his lips were always cracked and bleeding. Mystery Ranch |Arthur Chapman 

He had come away in the sour mood of a thirsty man who finds an alkali spring sparkling deceptively under a rock. Cabin Fever |B. M. Bower 

Talpers flushed angrily, and then grinned, until his alkali-cracked lips glistened in the lamplight. Mystery Ranch |Arthur Chapman 

Now, cations other than the alkali ions are liable to interfere with tests designed for alkaline or neutral solutions. The Elements of Qualitative Chemical Analysis, vol. 1, parts 1 and 2. |Julius Stieglitz