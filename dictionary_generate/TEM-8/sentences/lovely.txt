This wireless speaker not only looks lovely but has a handful of really interesting features, the most interesting of which has to be letting you, at any time, rewind up to two hours with the spin of a dial. Teenage Engineering’s OB-4 ‘magic radio’ is a weird and beautiful wireless speaker |Devin Coldewey |October 1, 2020 |TechCrunch 

Experiment with your drawing skills for hours on end with these pens' comfortable rubber grip, or make your letters to friends pop with lovely pastel accents. Add some verve to your life with these colorful pens |PopSci Commerce Team |September 11, 2020 |Popular-Science 

The people are lovely, and there are some amazing places to wine and dine. Carissa Moore's 5 Favorite Surf Towns |Megan Michelson |September 9, 2020 |Outside Online 

Essentially, whether existing in our world is painful or lovely can be understood in aesthetic terms. Solarpunk Is Growing a Gorgeous New World in the Cracks of the Old One |Carin Ism |September 6, 2020 |Singularity Hub 

There is this lovely idea that academic research that’s done rigorously and challenged by peers and referees will be so robust that it can just be put into play as policy. Speak Softly and Carry Big Data (Ep. 395) |Stephen J. Dubner |October 31, 2019 |Freakonomics 

When he does, here is a gentleness in his voice, a reflective and lovely quality that no movie he has been in has ever captured. The Story Behind Lee Marvin’s Liberty Valance Smile |Robert Ward |January 3, 2015 |DAILY BEAST 

Note: The egg wash both affixes the pastry to the dish and makes a lovely browned crust. Make These Barefoot Contessa Chicken Pot Pies |Ina Garten |November 29, 2014 |DAILY BEAST 

The Daily Beast spoke with the lovely actress about the holiday movie, in theaters Christmas Day, and much more. Anna Kendrick on Feminism, #GamerGate, and the Celebrity Hacking Attack |Marlow Stern |November 25, 2014 |DAILY BEAST 

From reports it must once have been a lovely old city with stone houses and a medieval quarter. Blood in the Sand: When James Jones Wrote a Grunt’s View of D-Day |James Jones |November 15, 2014 |DAILY BEAST 

Was it Shakespeare, in mad pursuit of a lovely boy and that voluptuous Dark Lady? Sor Juana: Mexico’s Most Erotic Poet and Its Most Dangerous Nun |Katie Baker |November 8, 2014 |DAILY BEAST 

The afternoon was a lovely one—the day was a perfect example of the mellowest mood of autumn. Confidence |Henry James 

Within the past thirty years civilization has rapidly taken possession of this lovely region. Among the Sioux |R. J. Creswell 

At the same time he realised that she had never seemed so adorably lovely, so exquisite, so out of his reach. The Wave |Algernon Blackwood 

Louis could not help seeing the lovely group, through the half-obscuring draperies of the open door. The Pastor's Fire-side Vol. 3 of 4 |Jane Porter 

Eve, too, lovely as she is, seems to bear no likelihood of resemblance to Milton's superb mother of mankind. Women in the fine arts, from the Seventh Century B.C. to the Twentieth Century A.D. |Clara Erskine Clement