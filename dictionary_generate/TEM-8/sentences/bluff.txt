The bluff has happened so many times and it still hasn’t been banned that I now don’t think it’s going to happen. ‘Ignore all of the politics’: Confessions of a DTC exec on the continued uncertainty of TikTok |Kristina Monllos |October 2, 2020 |Digiday 

The house is balanced on a granite bluff, looking out to sea. The Fungal Evangelist Who Would Save the Bees - Issue 90: Something Green |Merlin Sheldrake |September 23, 2020 |Nautilus 

Blakeslee was inspired by publications of an archaeologist who excavated at the same bluff site more than 60 years ago and suspected it had been a central part of Etzanoa. Drones find signs of a Native American ‘Great Settlement’ beneath a Kansas pasture |Bruce Bower |September 10, 2020 |Science News 

Hike the two-mile Headland Trail to the top of a bluff overlooking the Zuni Mountains and the volcanic craters of El Malpais National Monument. 27 Epic Trips to Start Planning Now |The Editors |September 8, 2020 |Outside Online 

In a recent test of bluffing in poker, computer face recognition failed miserably. The Deck Is Not Rigged: Poker and the Limits of AI |Maria Konnikova |August 7, 2020 |Singularity Hub 

Ten days later, when the dust had settled, MSF President Joanne Liu called their bluff. Why New York’s Ebola Case Will Hurt Infected Patients Everywhere |Abby Haglage |October 24, 2014 |DAILY BEAST 

It is not easy work, but it calls the bluff of those who would say “we have to take scripture seriously.” Meet the Young, Evangelical, Pro-Gay Movement |Gene Robinson |September 21, 2014 |DAILY BEAST 

We end the tour on a bluff overlooking a mine in the distance. Our Trip to The Climate War's Ground Zero |Darren Aronofsky |September 19, 2014 |DAILY BEAST 

He was bluff, inspirational to the men, a brilliant tactician. The Price of Being a Patton: Wrestling With the Legacy of America’s Most Famous General |Tim Teeman |May 26, 2014 |DAILY BEAST 

On a bluff overlooking the sea, he pitched a tent and lived there for the next year in near total seclusion. Doug Kenney: The Odd Comic Genius Behind ‘Animal House’ and National Lampoon |Robert Sam Anson |March 1, 2014 |DAILY BEAST 

“Mr. Pickwick, I thank you most heartily for all your kindness to my son,” said old Mr. Winkle, in a bluff straightforward way. The Posthumous Papers of the Pickwick Club, v. 2(of 2) |Charles Dickens 

At the offer of a smaller sum the Count would possibly bluff. The Joyous Adventures of Aristide Pujol |William J. Locke 

There is also a conspicuous high bluff on the principal island, which appears to have been seen by the French. Narrative of a Survey of the Intertropical and Western Coasts of Australia] [Volume 2 of 2] |Phillip Parker King 

By and by, a straggling birch bluff rose blackly across their way, but nobody swung wide. Winston of the Prairie |Harold Bindloss 

It is situated in a wonderfully picturesque position, on a rocky bluff overlooking the River Wye. British Highways And Byways From A Motor Car |Thomas D. Murphy