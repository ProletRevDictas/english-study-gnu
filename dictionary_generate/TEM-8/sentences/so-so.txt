But so-called jungle primaries are notoriously hard to predict or poll. The Golden State Preps for the ‘Red Wedding’ of Senate Races |David Freedlander |January 9, 2015 |DAILY BEAST 

“Gently rolling hills” roll not-so-gently under my tires, but the English countryside scenery is soporific. Biking With the Bard |Kara Cutruzzula |December 28, 2014 |DAILY BEAST 

Now is hardly the time to be doing anything on the so-called peace process other than trying to avoid an explosion on the ground. Why We Should Delay The Israel-Palestinian Peace Process |Aaron David Miller |December 19, 2014 |DAILY BEAST 

It also demonstrated that the so-called “trend” of natural went beyond trend. Goodbye To A Natural Hair Guru: Miss Jessie's Cofounder Titi Branch Dead At 45 |Danielle Belton |December 16, 2014 |DAILY BEAST 

In recent years news outlets have documented the rise of so-called “birth tourism” here in America. The Progressive Case Against Birthright Citizenship |Keli Goff |December 15, 2014 |DAILY BEAST 

Many so-called "humming tones" are given for practice, but in accepting them observe whether the foregoing principle is obeyed. Expressive Voice Culture |Jessie Eldridge Southwick 

In future years the poor-rate (so-called) will include, in addition to these, all other rates levyable by the Corporation. Showell's Dictionary of Birmingham |Thomas T. Harman and Walter Showell 

Perhaps his almost perfectly spontaneous love of tiny flowers is already a considerable advance on his so-called prototype. Children's Ways |James Sully 

The so-called war credit banks are designed to serve this purpose. Readings in Money and Banking |Chester Arthur Phillips 

Her active intellect and love of freedom sympathized with the speculations of the so-called philosopher. Madame Roland, Makers of History |John S. C. Abbott