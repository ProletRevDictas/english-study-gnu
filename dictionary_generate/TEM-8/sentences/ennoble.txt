Buddhism failed to ennoble the daily occupations of life, and produced drones and idlers and religious vagabonds. Beacon Lights of History, Volume I |John Lord 

You will have cares,—and even those will ennoble the world to you, and you to the world. An Old Man's Love |Anthony Trollope 

This hero of her romance, this artist whom she was to ennoble by her love, was not even an honest man. Marion Arleigh's Penance |Charlotte M. Braeme 

I want to recover faith in my mission, in my power to ennoble human souls. Mr Punch's Pocket Ibsen - A Collection of Some of the Master's Best Known Dramas |F. Anstey 

He believes that the Novel should strengthen life, not undermine it; ennoble, not defile it; for it is good tidings, not evil. Essays on Modern Novelists |William Lyon Phelps