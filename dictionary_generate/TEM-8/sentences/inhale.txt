N95 masks have already been shown to both protect people wearing them from inhaling coronavirus-bearing droplets, and reduce the risk of them spreading the virus by breathing. LG unveils a new high-tech ‘smart mask.’ But does it protect against COVID? |dzanemorris |August 28, 2020 |Fortune 

This would require inhaling hydrogen instead of oxygen, metabolizing it with acetylene instead of glucose, and exhaling methane instead of carbon dioxide. The 5 best places to explore in the solar system—besides Mars |Neel Patel |August 17, 2020 |MIT Technology Review 

Some companies are repurposing these drugs, which could be inhaled. We Don’t Have to Despair - Issue 88: Love & Sex |Robert Bazell |August 12, 2020 |Nautilus 

Nowadays, inhaling six or seven hot dogs a minute for 10 minutes mostly leads to digestive problems. When it comes to downing hot dogs, science says there’s a limit |Jonathan Lambert |August 7, 2020 |Science News For Students 

Gilead announced July 8 that it soon would begin a clinical trial to test an inhaled form of the drug. Remdesivir is looking even better at fighting COVID-19 |Tina Hesman Saey |July 20, 2020 |Science News For Students 

If you tend to inhale your food without realizing how much you consumed, opt for nibbles like in-shell pistachio nuts. 12 Thanksgiving Weight Loss Tips That Actually Work |DailyBurn |November 27, 2014 |DAILY BEAST 

TB, for example, is spread when I inhale the exhaled breath of a person with active disease. The CDC Was Wrong About How to Stop Ebola |Kent Sepkowitz |October 1, 2014 |DAILY BEAST 

The loftiest noses among the winemakers inhale the finest of scents from the tasting glass and proclaim another masterpiece. Napa’s Earthquake Is Not The Only Thing Shaking The Vineyards |Clive Irving |August 31, 2014 |DAILY BEAST 

They emit an odor that no human being should inhale, and yet you have. Whatever You Do Someone Will Die. A Short Story About Impossible Choices in Iraq |Nathan Bradley Bethea |August 31, 2014 |DAILY BEAST 

So of course, Jimmy Fallon took the opportunity to ruin it by forcing Freeman to inhale helium and pitch up his voice. Viral Video of the Day: Morgan Freeman Sucks Helium |Alex Chancey |July 25, 2014 |DAILY BEAST 

The helmsman led me to the side of the vessel, and told me to hold my head overboard, and inhale the air. A Woman's Journey Round the World |Ida Pfeiffer 

I straddled his neck and pushed his head into the milk so that he was forced either to drink it or inhale it. The Red Cow and Her Friends |Peter McArthur 

As soon as conscious breathing was necessary it was my custom deliberately to inhale on one step and exhale on the next. Mount Everest the Reconnaissance, 1921 |Charles Kenneth Howard-Bury 

"It's awful the way I inhale," said Slops with a melancholy sigh. The Varmint |Owen Johnson 

He felt his forehead begin to burn, so he arose to approach the window and inhale the fresh night breeze. The Reign of Greed |Jose Rizal