A visit to a British museum accompanied by a callous curator ends badly when Sayet learns that the building’s inventory includes numerous indigenous skulls stored in unlabeled boxes. ‘Where We Belong’ explores connection between past, present |Patrick Folliard |July 1, 2021 |Washington Blade 

The Israeli government has attempted to use social media to bolster its support, too, but in a particularly callous way. How Online Activism and the Racial Reckoning in the U.S. Have Helped Drive a Groundswell of Support for Palestinians |Sanya Mansoor |May 21, 2021 |Time 

Prosecutors who opposed the release wrote in a court filing that the “cold and callous nature of this crime reflects the defendant’s depravity.” Released early after a murder conviction, D.C. man is charged in new homicide |Paul M. Duggan, Keith L. Alexander, Peter Hermann |May 12, 2021 |Washington Post 

At the same time, the cops who are called on to keep order frequently behave like a callous, occupying force, and, all too often, the money corrupts them, too. The elite Baltimore cops who became criminals |Michael Fletcher |March 26, 2021 |Washington Post 

The singer’s callous responses to autistic people’s concerns on Twitter did nothing to increase my trust in her ability to treat us like full-fledged human beings onscreen. Sia's Golden Globe-Nominated Music Isn’t Just Offensive. It’s Also Bad Art—and the Distinction Matters |Sarah Kurchak |February 25, 2021 |Time 

Is this the picture of a callous culture that chews these young men up and spits them out? Liberals’ College Hoops Pity Party |Michael Tomasky |March 21, 2014 |DAILY BEAST 

The deal, critics charge, was at best a bad one and at worst, a callous political move. Did Christie Go Easy on a Human Trafficker Just to Bust a Small-Time Pol? |Olivia Nuzzi |March 17, 2014 |DAILY BEAST 

The problem is that, when exposed to the political limelight, Carson's “gifted hands” have become careless, callous. Ben Carson Was a Role Model for Black Teens Until He Sold Out to the Right |Joshua DuBois |March 16, 2014 |DAILY BEAST 

Perhaps the most callous Israeli response, however, came from economy minister Naftali Bennett. Blame America First—And Only |Ali Gharib |December 5, 2013 |DAILY BEAST 

With each passing disaster our skin grows a little more callous, our “thoughts and prayers” more cynical. Gun Crazy: No More 'Thoughts and Prayers' |Joshua DuBois |September 29, 2013 |DAILY BEAST 

The callous Justice passed on to the next stye, immersed in thought. The Pit Town Coronet, Volume I (of 3) |Charles James Wills 

It isn't in the average man to be utterly callous to the suffering of another, even if that other richly deserves his pain. Raw Gold |Bertrand W. Sinclair 

But where the trade is once admitted, no wonder the heart becomes callous to the individual sufferings of the slaves. Journal of a Voyage to Brazil |Maria Graham 

The brutal callous indifference of the whole thing was most strongly marked. The Weight of the Crown |Fred M. White 

The Hindoo devotee is exceedingly tender of the lives of animals, while he is often callous to human suffering. Man And His Ancestor |Charles Morris