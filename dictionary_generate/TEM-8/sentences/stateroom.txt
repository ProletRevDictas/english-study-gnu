When my negative result came back I was allowed to board and went to find my stateroom on deck 11. Blogging my first overseas vacation since COVID |Peter Rosenstein |October 19, 2021 |Washington Blade 

Then back to the stateroom to finish unpacking and change for dinner with good friends in one of the specialty restaurants, EDEN. Blogging my first overseas vacation since COVID |Peter Rosenstein |October 19, 2021 |Washington Blade 

Each morning I have had coffee, a bagel and orange juice delivered to the stateroom. Blogging my first overseas vacation since COVID |Peter Rosenstein |October 19, 2021 |Washington Blade 

Then it was back to my stateroom again to prepare for another tough night of food and drinking. Blogging my first overseas vacation since COVID |Peter Rosenstein |October 19, 2021 |Washington Blade 

Shortly after entering my beautiful stateroom there was a knock at the door and my stateroom attendant, Lenie, had come to introduce herself. Blogging my first overseas vacation since COVID |Peter Rosenstein |October 19, 2021 |Washington Blade 

"No, thanks," the stranger said, taking his bag and shutting himself into his stuffy little stateroom. The Cromptons |Mary J. Holmes 

This was not a bad idea, although the stranger shuddered as he thought of his ill-smelling stateroom and short berth. The Cromptons |Mary J. Holmes 

If you are obliged to pass the night upon a steamboat secure, if possible, a stateroom. The Ladies' Book of Etiquette, and Manual of Politeness |Florence Hartley 

You may not be able to obtain a stateroom upon all occasions when traveling, and must then sleep in the ladies' cabin. The Ladies' Book of Etiquette, and Manual of Politeness |Florence Hartley 

The immense stateroom was especially furnished for the occasion at a cost, it is said, of about one hundred thousand pounds. British Highways And Byways From A Motor Car |Thomas D. Murphy