The move comes at a time when hype around medical AI is at a peak, with inflated and unverified claims about the effectiveness of certain tools threatening to undermine people’s trust in AI overall. New standards for AI clinical trials will help spot snake oil and hype |Will Heaven |September 11, 2020 |MIT Technology Review 

That could make it tough for the Browns, even with an additional playoff slot in the AFC — although Cleveland’s talent looks better on paper than it did last year, despite the relative reduction in hype. What To Watch For In An Abnormally Normal 2020 NFL Season |Neil Paine (neil.paine@fivethirtyeight.com) |September 9, 2020 |FiveThirtyEight 

Stock splits tend to draw attention to companies that conduct them—and increased hype can translate into more stock sales. Investors riding high on Apple and Tesla stock splits could get clipped, data shows |rhhackettfortune |August 31, 2020 |Fortune 

It seems as if just about everyone is getting into the SPAC hype. Paul Ryan jumps onto the SPAC train |Lucinda Shen |August 21, 2020 |Fortune 

Separately, here’s a spot of Friday feedback, regarding my essay yesterday about the hype around 5G and its geopolitical importance. Corporate values under a Biden presidency |David Meyer |August 21, 2020 |Fortune 

For OK Go the music video is medium for personal creativity, hype, and branding. OK Go Is Helping Redefine the Music Video For the Internet Age |Lauren Schwartzberg |December 15, 2014 |DAILY BEAST 

His stories about his tenure in Washington hype his success in fixing housing problems in “inner cities.” Andrew Cuomo Ignores Rural New York |David Fontana |November 8, 2014 |DAILY BEAST 

In short, my $18-plus-cost-of-replacement-filters Brita water system may not live up to the hype. Are Water Filters B.S.? |Michael Schulson |August 19, 2014 |DAILY BEAST 

But as the hype surrounding the movie heightens, many are curious about the writer behind the story. A Trailblazer in YA Dystopian Fiction: An Interview With 'The Giver' Author Lois Lowry |Marianne Hayes |August 12, 2014 |DAILY BEAST 

These are the standard selling points of the craft-distilling movement, with its locavore lingo, terroir talk, and handmade hype. Your ‘Craft’ Rye Whiskey Is Probably From a Factory Distillery in Indiana |Eric Felten |July 28, 2014 |DAILY BEAST 

To bear the victor's hard commands, or bring The weight of waters from Hype'ria's spring. Mosaics of Grecian History |Marcius Willson 

But let us hype they distributed some of their superfluous coin among these hapless exiles to purchase food and a night's lodging. Grandfather's Chair |Nathaniel Hawthorne 

It has to be withheld from hype-trainees, otherwise they might deliberately flunk their course. Next Door, Next World |Robert Donald Locke 

I saw images of the ship riding along beside me, out there in the hype. Next Door, Next World |Robert Donald Locke 

What should be held true – the hype or the dismal statistics? After the Rain |Sam Vaknin