More than a thousand homes, drainage chambers, community toilets, help centers, and drinking water tanks in the slum now have plus codes. Slum dwellers in India get unique digital addresses |Shoma Abhyankar |April 28, 2021 |MIT Technology Review 

Until recently, Dashrath shared a common address with everyone around her—that of the slum itself. Slum dwellers in India get unique digital addresses |Shoma Abhyankar |April 28, 2021 |MIT Technology Review 

It will add more urban residents by 2050 than any other country, according to a 2014 UN estimate, and its slums are growing faster than its cities. Slum dwellers in India get unique digital addresses |Shoma Abhyankar |April 28, 2021 |MIT Technology Review 

According to estimates, more than half of Nairobi’s four million people live in informal settlements, sometimes called slums. A dirty and growing problem: Too few toilets |Stephanie Parker |September 24, 2020 |Science News For Students 

The volunteers of the organisation provide a training program under which these women from slums, villages, govt. Menstruation Comes With Innumerable Taboos In India |LGBTQ-Editor |May 29, 2020 |No Straight News 

Two hundred girls are weaving in and out of dirty alleys in the seaside slum of West Point, Liberia. Meet the Liberian Girls Beating Ebola |Abby Haglage |October 29, 2014 |DAILY BEAST 

More serious still, the slum dwellers face enormous risk from unsafely built environments. Welcome to the Billion-Man Slum |Joel Kotkin |August 25, 2014 |DAILY BEAST 

Some argue that these migrants are better off than previous slum dwellers since they ride motorcycles and have cellphones. Welcome to the Billion-Man Slum |Joel Kotkin |August 25, 2014 |DAILY BEAST 

Hell, it worked for Tokyo in the 20th—after that city was decimated by Allied bombers, it was basically one big slum. Great Cities are Born Filthy |Will Doig |July 13, 2014 |DAILY BEAST 

With a group of young men in the slum he formed Rock Angels, a drag act performing dance, music and drama. Uganda Gays Face New Wave of Fear Under Anti-Gay Bill |Caelainn Hogan |February 24, 2014 |DAILY BEAST 

I, followed the fates of my little slum-boys—and what I saw was that Tammany Hall was getting them. The Profits of Religion |Upton Sinclair 

Many are the shocking sights and sad experiences I have witnessed in street and slum work. Prisons and Prayer: Or a Labor of Love |Elizabeth Ryder Wheaton 

This slum must be our rendezvous when all's over; for hark ye, my lads, I'll not budge an inch till Luke Bradley be set free. Rookwood |William Harrison Ainsworth 

Puffs of energy had raised high buildings over there; over there an eccentric subsidence had left behind it a slum. The Women of Tomorrow |William Hard 

In nine cases out of ten they are lads of normal impulses whose possibilities have all been smothered by the slum. Heroes of To-Day |Mary R. Parkman