The new estimate also does not fully account for drug overdose deaths, which have risen in 2020 according to provisional counts. The COVID-19 death toll sent U.S. life expectancy plunging in 2020 |Aimee Cunningham |February 18, 2021 |Science News 

Regulators granted provisional approval to Sinovac’s CoronaVac, doses of which have already been ordered by Turkey, Ukraine, and Brazil. Clubhouse China, ETF shorts, marijuana shortages |Jordan Lebeau |February 7, 2021 |Quartz 

Flat jobs growth and a provisional decline in restaurant jobs could pressure lawmakers to reach a stimulus deal soon Nationwide Restaurant Hiring Hits a Wall as Stimulus Talks Pick Up Steam |Ryan Sutton |December 4, 2020 |Eater 

Elections officials make sure your mail-in ballot was not counted before they process your provisional ballot. Maryland voting guide: What to know about early voting, mail-in ballots |Erin Cox |October 29, 2020 |Washington Post 

The latest change to the provisional deadline, spotted earlier by Reuters, could be the result of one of the parties asking for more time. EU’s Google-Fitbit antitrust decision deadline pushed into 2021 |Natasha Lomas |October 16, 2020 |TechCrunch 

The spirit of the novel is one of doubt and questioning; its knowledge is provisional and its perspectives multiple. The Birth of the Novel |Nick Romeo |November 27, 2014 |DAILY BEAST 

Most radical was the 1856 Provisional Constitution drafted by northern abolitionists, including John Brown. One U.S. Constitution Just Wasn’t Enough |Tom Arnold-Forster |July 4, 2014 |DAILY BEAST 

The book opens in December of 2003 with the story of an ambush of the convoy of Coalition Provisional Authority chief Paul Bremer. Who Should Kill? Looking for Answers in Erik Prince’s Memoir |Brian Castner |November 22, 2013 |DAILY BEAST 

Some students waited in line for seven hours to get provisional ballots. Was 2012 the Worst Year Ever for Voting Rights? |Eliza Shapiro |August 22, 2013 |DAILY BEAST 

It is better to at least guarantee the Palestinians a state even if its borders are provisional. Preparing for Plan B |Yossi Beilin |July 22, 2013 |DAILY BEAST 

The next three months were spent almost entirely in establishing provisional juntas in the different capitals. Journal of a Voyage to Brazil |Maria Graham 

One of the provisional junta of government is the greatest slave merchant here. Journal of a Voyage to Brazil |Maria Graham 

These papers were received by the junta of Provisional Government, at whose head was the Bishop. Journal of a Voyage to Brazil |Maria Graham 

The actual bridge must have the section of all members greater than those in the provisional design in the ratio k/(1-k). Encyclopaedia Britannica, 11th Edition, Volume 4, Part 3 |Various 

To some extent the policy of the Provisional Government excused his change of attitude. The Life of Mazzini |Bolton King