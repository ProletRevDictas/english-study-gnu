“Personal hotspots can get speeds of up to 60 Mb/s down, whereas hotel Wi-Fi can be as slow as 1.5 Mb/s,” Sesar said. How ‘Ethical’ Hotel Chain Marriott Gouges Guests in the Name of Wi-Fi Security |Kyle Chayka |December 31, 2014 |DAILY BEAST 

But whereas we used to be satisfied gazing on that perfection as it stood up on a pedestal, now we want it down among us. Bow Down, Bitches: How Beyoncé Turned an Elevator Brawl Into a Perfect Year |Kevin Fallon |December 31, 2014 |DAILY BEAST 

Whereas other brands purchase their barrels from big producers more or less off the rack, The Macallan starts in the forest. How Much Do Whisky Casks Really Affect Taste? | |December 10, 2014 |DAILY BEAST 

Oh, and that his profile on Twitter and his name elsewhere say “Ian Miles Cheong” whereas mine says “Arthur Chu.” Rage Against GamerGate’s Hate Machine: What I Got For Speaking Up |Arthur Chu |November 17, 2014 |DAILY BEAST 

Japanese distillers often use a combination of different types of stills and different casks, whereas the Scots cannot. Watch Out, Scotland! Japanese Whisky Is on the Rise |Kayleigh Kulp |November 16, 2014 |DAILY BEAST 

Whereas Lessard had acted the martinet with MacRae, he took another tack and became the very essence of affability toward me. Raw Gold |Bertrand W. Sinclair 

Such an act would be of a piece with Nigel's character, whereas a liaison—and yet Nigel was no saint. Bella Donna |Robert Hichens 

One black key is made to serve, for instance, for D sharp and for E flat, whereas the two notes are in reality not identical. The Recent Revolution in Organ Building |George Laing Miller 

It 67 was that the piece which reads smoothly seldom acts well; whereas a play that gets over the footlights usually reads poorly. The Girls of Central High on the Stage |Gertrude W. Morrison 

Moreover, Rita liked him, whereas she had never sincerely liked and trusted Sir Lucien. Dope |Sax Rohmer