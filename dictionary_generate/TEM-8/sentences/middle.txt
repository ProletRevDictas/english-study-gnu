Tropical storms Teddy and Vicky are still out in the middle of the Atlantic. A nearly unprecedented cluster of tropical storms are brewing in the Atlantic |Sara Chodosh |September 15, 2020 |Popular-Science 

We’re are in the middle of a global pandemic, but we’re also in the midst of a racial pandemic where racism is being normalized. Oceanside Is Rethinking Its Police Chief Hiring Process Following Community Concerns |Kayla Jimenez |September 14, 2020 |Voice of San Diego 

So, I think we’re getting to the place where we’re in the middle of this storm, which will allow us to see much clearer who we are, who we want to be, and who we are destined to be. Debbie Allen’s Grandmother Love Doubled |Joi-Marie McKenzie |September 11, 2020 |Essence.com 

The study found that a tool made by Lunit AI and used in certain hospitals in South Korea finished in the middle of the pack of radiologists it was tested against. New standards for AI clinical trials will help spot snake oil and hype |Will Heaven |September 11, 2020 |MIT Technology Review 

Right in the middle of the paragraph, Hightower breaks away from the subject of affidavits for two sentences. The Learning Curve: San Diego Unified Is Terrified of Kids Opting Out |Will Huntsberry |September 10, 2020 |Voice of San Diego 

According to Pew, 14 of the 20 countries in the Middle East and North Africa have blasphemy laws. In Defense of Blasphemy |Michael Tomasky |January 9, 2015 |DAILY BEAST 

In the middle of all of that past suffering and present-day conflict, this Cosby bomb was dropped. Phylicia Rashad and the Cult of Cosby Truthers |Stereo Williams |January 8, 2015 |DAILY BEAST 

And, especially when it comes to the middle, personality counts. Why This Liberal Hearts Huckabee |Sally Kohn |January 6, 2015 |DAILY BEAST 

The same picture emerges from middle class men in the U.S., Canada, and the Nordic countries. How Good Dads Can Change the World |Gary Barker, PhD, Michael Kaufman |January 6, 2015 |DAILY BEAST 

As a white, educated, Western, middle-class male, I possess most of the unearned privilege the world has to offer. In 2015, Let’s Try for More Compassion |Gene Robinson |January 4, 2015 |DAILY BEAST 

She looked so sweet when she said it, standing and smiling there in the middle of the floor, the door-way making a frame for her. Music-Study in Germany |Amy Fay 

Suddenly, however, he became aware of a small black spot far ahead in the very middle of the unencumbered track. The Joyous Adventures of Aristide Pujol |William J. Locke 

The Seven-score and four on the six middle Bells, the treble leading, and the tenor lying behind every change, makes good Musick. Tintinnalogia, or, the Art of Ringing |Richard Duckworth and Fabian Stedman 

When we speak against one capital vice, we ought to speak against its opposite; the middle betwixt both is the point for virtue. Pearls of Thought |Maturin M. Ballou 

Dinner occurred in the middle of the day, and about nine in the evening was an informal but copious supper. Hilda Lessways |Arnold Bennett