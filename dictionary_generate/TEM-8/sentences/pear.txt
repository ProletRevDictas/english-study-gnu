You can throw whole apples and pears in there without thinking twice, and like magic, juice will appear before your eyes. Best juicer: All your homemade juicing needs are just a squeeze away |Irena Collaku |July 14, 2021 |Popular-Science 

Fruit fanThis works best with firmer fruits such as apples and pears. How to garnish cocktails, now that you’re clinking glasses with friends again |M. Carrie Allan |May 21, 2021 |Washington Post 

Look for flavors of apple and pear, and pair with salty appetizers. The pandemic bubble may be starting to burst. Celebrate with bubbles all under $20. |Dave McIntyre |April 16, 2021 |Washington Post 

The kitchen would warm with the scent of pears and lemons roasting with spices. Smaller bowl, same punch: Scaling down New Year’s drinks for one or two |M. Carrie Allan |December 18, 2020 |Washington Post 

Feel around the stem, and if the pear gives a little right there at the top, you’ve got the just-right Goldilocks of pears. This cocktail can keep you warm around the fire pit — and you can make it there, too |M. Carrie Allan |November 20, 2020 |Washington Post 

At the most basic level, most ciders are produced using a blend of sweet, sour, and bitter apple and pear varieties. Wine, Watch Out! These Ciders Are Just as Good |Jordan Salcito |July 19, 2014 |DAILY BEAST 

In schnaps, however, Subirer aromas are transformed into those of a caramelized pear tart, buttery, baked, and entirely pleasant. What to Drink When it’s Cold? The Glory of Austrian Schnaps |Jordan Salcito |January 25, 2014 |DAILY BEAST 

Subirer, for example, is a small bitter pear that grows in the Alps, near Lech. What to Drink When it’s Cold? The Glory of Austrian Schnaps |Jordan Salcito |January 25, 2014 |DAILY BEAST 

Its name roughly translates to “pig pear,” referencing the only animals contented to eat it. What to Drink When it’s Cold? The Glory of Austrian Schnaps |Jordan Salcito |January 25, 2014 |DAILY BEAST 

PEGG: For most British people, the first time they get drunk is with cider—or Thunderbird pear wine. My London Getaway With Simon Pegg and Nick Frost, Stars of ‘The World’s End’ |Marlow Stern |November 18, 2013 |DAILY BEAST 

It bears beautiful yellow blossoms in summer, after which comes the fruit, a prickly pear, not good to eat. The Nursery, November 1881, Vol. XXX |Various 

Occasionally there was a figure which had lost its capital, and so looked like a broken pillar, a sugar loaf, a pear. Overland |John William De Forest 

The condensers were pear shaped in section, and built of mild steel plates. Loss of the Steamship 'Titanic' |British Government 

Near to the kopje there was a garden surrounded by low trees and a hedge of prickly pear. The Relief of Mafeking |Filson Young 

The little cottage was shut in on one side by a hedge of aloes and prickly pear and on the other by high cliffs and precipices. Napoleon's Young Neighbor |Helen Leah Reed