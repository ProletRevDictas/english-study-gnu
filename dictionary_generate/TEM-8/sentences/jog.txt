On a warmer afternoon jog, it was extremely breathable and comfortable over a tank. The Gear Our Editors Loved in January |The Editors |February 3, 2021 |Outside Online 

If you’re out for a jog with no one around or on a walk outside with a friend, a simple two- or three-layer cloth mask is fine. Everyone should be wearing N95 masks now |Joseph G. Allen |January 26, 2021 |Washington Post 

Another time my mom had taken him to the beach and he went for a jog. When Your Father Falls Apart |Eugene Robinson |December 23, 2020 |Ozy 

If you’re using your wireless earbuds while you’re on the move—maybe at work, or during a jog on a hot day—they’ll be coming into contact with your sweat due to their small size and location on your head. Best wireless earbuds: Five things to consider |PopSci Commerce Team |October 30, 2020 |Popular-Science 

Whether it’s a windy, riverside bike ride or a jog up a steep San Francisco hill, chances are, your mind—not to mention your body—will thank you for it. The Simple Dutch Cure for Stress - Facts So Romantic |Alice Fleerackers |October 26, 2020 |Nautilus 

He would be Cory Booker, who has promised to lead a campaign jog to a local ice cream parlor. The Ugly Truth About Cory Booker, New Jersey’s Golden Boy |Olivia Nuzzi |October 20, 2014 |DAILY BEAST 

It begins with forgetting lines in lectures and losing track of where she is on a jog, and gets worse. Oscar Season Kicks Off in Toronto: Channing Tatum, Kristen Stewart, and More Court Awards Glory |Marlow Stern |September 14, 2014 |DAILY BEAST 

If we are not yet having full-scale runs on Cypriot banks, we've at least worked up to a pretty brisk jog. After Cyprus Bank Bailout, Depositors Race to Withdraw Their Cash. Is the Rest of Europe Next? |Megan McArdle |March 17, 2013 |DAILY BEAST 

He has also said he used to jog past the house bin Laden was hiding in. Don't Trust Musharraf |Bruce Riedel |October 22, 2011 |DAILY BEAST 

If you absolutely must jog your memory on all the details, you can check out handy FAQ from the spring. 8 Questions About the New Rapture |David A. Graham |October 20, 2011 |DAILY BEAST 

How I do wish sometimes to give Ritchie a jog, when there is some stumbling-block that he sticks fast at. The Daisy Chain |Charlotte Yonge 

Thus fortified with wisdom, he calmly looks the evil in the face, and lets it not disturb his little jog-trot existence. Friend Mac Donald |Max O'Rell 

In order to get this off his pole, he would jog one end of the pole on the ground until the “biscuit” would slide off. The Wonder Book of Knowledge |Various 

Jack Harvey, waiting a moment longer to rest, started off on an easy jog-trot back to camp. The Rival Campers |Ruel Perley Smith 

From time to time the slave-drivers would jog them along with a few lashes from a four-cornered "hippo" hide kiboko, or whip. In Africa |John T. McCutcheon