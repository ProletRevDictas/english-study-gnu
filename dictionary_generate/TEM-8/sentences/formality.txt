Aside from establishing the level of formality, those giving weddings must rely on the judgment of the participants. Miss Manners: Declining help from the unmasked |Judith Martin, Nicholas Martin, Jacobina Martin |February 8, 2021 |Washington Post 

Currently, thousands of troops are stationed inside and outside the building, and the situational response is taking on a formality and sophistication akin to a military operation. Police are flying surveillance over Washington. Where were they last week? |Tate Ryan-Mosley |January 18, 2021 |MIT Technology Review 

In reality, Pence does have a role to play when Congress meets to certify the Electoral College’s votes on January 6, but it is more a formality than anything. Louie Gohmert’s failed election lawsuit, briefly explained |Cameron Peters |January 2, 2021 |Vox 

While it’s only a formality at this point, the state Legislature still needs to confirm Assemblywoman Shirley Weber as secretary of state. Morning Report: Political Shuffle Begins |Scott Lewis |December 28, 2020 |Voice of San Diego 

With less formality and more announcements than any other prestige show, the video games industry has an awards show that tries to be its mirror. The Game Awards hosted Zoom calls with hundreds of fans. They were surprisingly orderly. |Gene Park |December 10, 2020 |Washington Post 

It was both stylish and somber while being suitably grand for the formality of the occasion. Kate Middleton, the Preggers Fashion Princess |Tom Sykes |November 14, 2014 |DAILY BEAST 

Or so the chapter titles formally name him, in a nod, perhaps, to his pained formality. A Different Kind of Vietnam Story |J.T. Price |October 9, 2014 |DAILY BEAST 

Early marriage is mostly a formality, but girls traditionally leave school at that time. The Sad Hidden Plight of Child Grooms |Nina Strochlic |September 18, 2014 |DAILY BEAST 

FDR wanted to project easy grace rather than stiff formality—especially when communicating complicated matters. FDR: King of All Media |John Avlon |September 2, 2014 |DAILY BEAST 

These cities want to be the next London, and when they look at that city, they see order and formality. Great Cities are Born Filthy |Will Doig |July 13, 2014 |DAILY BEAST 

It is not, however, the incident in itself that is now referred to, but only the formality ascribed to it in the narrative. Solomon and Solomonic Literature |Moncure Daniel Conway 

Accustomed to a written character, their eyes became wearied by the crabbedness and formality of type. Blackwood's Edinburgh Magazine, No. CCCXXXIX. January, 1844. Vol. LV. |Various 

With great deliberation and much formality Wu-pom Nai proceeded to loosen the boy's heavy plaits of hair. Our Little Korean Cousin |H. Lee M. Pike 

By-and-by all pretence of formality and order is put aside and the battle really begins. Punch, or the London Charivari, Volume 158, April 28, 1920 |Various 

Before I could reply, the door flew open without the formality of a knock, and old Mrs. Bolum ran in. The Soldier of the Valley |Nelson Lloyd