Reading this book is to be reassured that our struggles are connected and systemic—not a result of our individual efforts. 9 new books to read in September |Rachel King |September 1, 2020 |Fortune 

You go to bed, reassuring yourself that sleep will efface the image from your mind. The first murder |Katie McLean |August 19, 2020 |MIT Technology Review 

“The best thing we can do to prepare for the start of the school year is to reassure employees that, collectively, we are taking precautions to prevent the spread of the virus,” said Borne in her ministry’s statement. As COVID-19 rates rise, France will require most people to wear masks at work |David Meyer |August 18, 2020 |Fortune 

Using conversation language as Apple has done so will let visitors know exactly what to expect when clicking on it if you opt to use a form, keep it short while including a privacy statement reassuring visitors that their data is safe. Studying the anatomy of a successful high-conversion landing page |Yasmine Dehimi |June 22, 2020 |Search Engine Watch 

She cradled Kitty and reassured Kitty, and wasn’t sure if Kitty — who was at that point in extraordinary physical duress — realized she was in a friend’s arms. 5 Psychology Terms You’re Probably Misusing (Ep. 334 Rebroadcast) |Stephen J. Dubner |January 9, 2020 |Freakonomics 

But political parties officials continue to reassure the country that the campaigns will not endanger anyone. Liberian Senatorial Elections Defy Ebola |Wade Williams |December 1, 2014 |DAILY BEAST 

As I hasten to reassure these exasperated moms and dads, I had to be in the office anyway. The Doctor’s Note Must Die! |Russell Saunders |September 16, 2014 |DAILY BEAST 

He miraculously survived, and his first impulse was to reassure his horrified aunt that he was OK. Brooklyn Shooting Hits Close to Bill de Blasio’s Park Slope Home |Michael Daly |July 1, 2014 |DAILY BEAST 

These reforms were mostly designed to reassure Sunnis and Kurds that Maliki would not become an autocrat. How Iran and America Can Beat ISIS Together |Ben Van Heuvelen |June 21, 2014 |DAILY BEAST 

Mid-level State Department officials have visited to reassure the government that the U.S. government stands beside them. Is This Putin’s Next Target? |Josh Rogin |May 23, 2014 |DAILY BEAST 

The general glanced suspiciously at the door to reassure himself that it was closed. The Doctor of Pimlico |William Le Queux 

"Reassure yourself," answered Marius, with a sneer, a greyness that was of jealous rage overspreading his face. St. Martin's Summer |Rafael Sabatini 

I beg you to put aside all such suspicions, and to reassure the Grand Duke, who is very much annoyed, so Rubinstein tells me. The Life &amp; Letters of Peter Ilich Tchaikovsky |Modeste Tchaikovsky 

Every possibility of escape must first be exhausted, I reassure my troubled conscience. Prison Memoirs of an Anarchist |Alexander Berkman 

She was just about to call out to reassure them when a sound in the passageway behind her made her hold her breath in suspense. The Adventure Girls at K Bar O |Clair Blank