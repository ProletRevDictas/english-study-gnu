Former cheerleader and marketing manager Tiffani Johnston told the Committee that Snyder put his hand on her thigh under the table at a team dinner, and later, pressed his hand into her back to coerce her to ride in a limousine with him. The NFL Keeps Fumbling and Congress Keeps Punting |Abby Vesoulis |February 8, 2022 |Time 

A white stretch limousine took Tupac off to a private plane that awaited at the local airport. Tupac Shakur’s Race-Killer Prison Pal Talks |Michael Daly |July 29, 2014 |DAILY BEAST 

"She looked me in the eye," one limousine driver recalled, surprised and grateful. The Day the Fairytale Died |Marilyn Johnson |July 12, 2014 |DAILY BEAST 

“The ride arrived, and it was a limousine-type car with a Virginia plate,” he recalled. Inside Uber’s Political War Machine |Olivia Nuzzi |June 30, 2014 |DAILY BEAST 

Abdi worked with his brother in a mobile phone store, as a DJ, and most recently, a limousine driver. Barkhad Abdi: From Limo Driver to Oscar Contender |Tim Teeman |February 23, 2014 |DAILY BEAST 

Knowing of their poverty, Jackson even sent a limousine to drive the entire family. Gavin Arvizo’s New Beginning: Jackson Abuse Accuser Gets Married at 24 |Diane Dimond |December 9, 2013 |DAILY BEAST 

Mr. Cordyce smiled about his eyes as he closed his desk, ordered his limousine, and went out and locked the door of his office. The Box-Car Children |Gertrude Chandler Warner 

In the street a luxurious limousine was tooting for a ramshackle prairie schooner to turn to one side. The Sunbridge Girls at Six Star Ranch |Eleanor H. (Eleanor Hodgman) Porter 

Ill bet some day Ill see you rolling down the avenue in a fine limousine just like Mrs. Van Astorbilt. The Woman Gives |Owen Johnson 

It was warm in his limousine, which was electrically heated. The Daffodil Mystery |Edgar Wallace 

He would find her and the green limousine chap with whom he would have a reckoning. The Secret Witness |George Gibbs