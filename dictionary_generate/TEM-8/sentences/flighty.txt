There have been any number of similar statements in recent years, claims that some people are simply too uninformed or flighty to be allowed to cast a ballot. How to tell if you are casting a ‘quality’ vote |Philip Bump |March 11, 2021 |Washington Post 

Like many modern skis loaded with technology, it’s stable as hell when the sidecut is engaged but can get a little flighty when you run it straight. Long-Term Review: The Best Skis, Period |Heather Schultz and Marc Peruzzi |March 4, 2021 |Outside Online 

Fitness clubs have long relied on long-term membership agreements to prevent flighty customers from abandoning their New Year’s resolutions without a financial penalty. These 5 major gym chains still make it annoyingly hard to quit |jakemeth |November 29, 2020 |Fortune 

During the last presidential debate, Neves went so far to refer to Rousseff as “flighty.” What Brazil’s Dilma Rousseff Can Teach Hillary Clinton |Heather Arnet |October 29, 2014 |DAILY BEAST 

Plus, Joaquin spent the entirety of his last film falling in love with a flighty blonde who's tied to an technological device. Marry, Screw, Kill: Lindsay Lohan Sex List Scandal Edition |Amy Zimmerman |March 12, 2014 |DAILY BEAST 

Evan Rachel Wood stars as a flighty barista in ‘A Case of You,’ premiering at Tribeca. Evan Rachel Wood on Tribeca Film ‘Case Of You,’ Coming Out as Bisexual, Her Pregnancy, and More |Marlow Stern |April 24, 2013 |DAILY BEAST 

News and documentary production requires minute-by-minute decision-making—flighty ditherers need not apply. Why I'll Miss Bill Moyers |Randy Bean |April 29, 2010 |DAILY BEAST 

She grew up an orphan in Japanese internment camps and has a flighty streak. 3 Great Reads for Thanksgiving |Taylor Antrim |November 25, 2009 |DAILY BEAST 

I never looked upon Hephzibah Wallis as flighty; in fact, she was undoubtedly the steadiest of all my girls. The Pit Town Coronet, Volume II (of 3) |Charles James Wills 

Weve got to live somehow, and if you get a good job stick to it, say Inone of your highty flighty notions. Chains |Elizabeth Baker 

She is sweetly pretty, though they say rather a flirt, and flighty in her ways. Wood Magic |Richard Jefferies 

She was flighty and frivolous, evasive and obstinate, fond of pleasures not always innocent. The Stones of Paris in History and Letters, Volume I (of 2) |Benjamin Ellis Martin 

He did write to Mr. Wharton, but in doing so he altogether laid aside that flighty manner which for a while had annoyed her. The Prime Minister |Anthony Trollope