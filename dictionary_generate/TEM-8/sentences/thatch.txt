Six women raise their hands, flashing thick thatches of unshorn underarm hair. For today’s feminist writers, sex makes a comeback |Meredith Maran |June 17, 2021 |Washington Post 

Vehicles are still unusual, but homes now are made of brick and wood and have metal roofs instead of thatch. A Little Too Off the Beaten Path in Burma |Katya Cengel |June 2, 2014 |DAILY BEAST 

By these standards, Eric Schneiderman is a mere fly buzzing around a thatch-roofed pompadour. Inside the Donald Trump–Eric Schneiderman TV Food Fight |Lloyd Grove |August 29, 2013 |DAILY BEAST 

His comic strip, Thatch, appeared daily in more than 150 newspapers from 1994-1998. Obama vs. FDR |Jeff Shesol |July 27, 2010 |DAILY BEAST 

The dog of a Feringhi whom I served has had it hidden these two months in the thatch of his house near the Alumbagh. The Red Year |Louis Tracy 

He coolly twitched the flame-coloured thatch away and disclosed a close crop of black hair. The Weight of the Crown |Fred M. White 

The night was windy, the March weather had dried the thatch, and the whole place was burned to the ground in a few minutes. Ireland Under the Tudors, Vol. II (of 3) |Richard Bagwell 

I also put a straw thatch over the hut, proudly using my own straw which I had grown with blood. The Adventures of Louis de Rougemont |Louis de Rougemont 

Sometimes the roof was a very thick layer of long grass, laid on rude rafters, and held down by poles to form a kind of thatch. The Story of the Thirteen Colonies |H. A. (Hlne Adeline) Guerber