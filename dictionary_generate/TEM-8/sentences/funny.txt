The hashtag itself also has to be memorable, funny or otherwise worth sharing. Deep Dive: How the Summer of 2020 forced brand marketing to change for the better |jim cooper |September 14, 2020 |Digiday 

Maybe there are a few poems or funny stories written by the contributors. The Joy of Cooking Other People’s ‘Secret Family Recipes’ |Amy McCarthy |September 11, 2020 |Eater 

Given how ubiquitous Uber and other ride-sharing services have become, it’s funny to think they weren’t even around ten years ago. Uber Wants to Go All-Electric by 2030. It Won’t Be Easy |Vanessa Bates Ramirez |September 10, 2020 |Singularity Hub 

In quarantine, though, his show has become something to see most nights, even when it’s not particularly funny. One Good Thing: Stephen Colbert is looser, funnier, and angrier in quarantine |Emily VanDerWerff |September 4, 2020 |Vox 

As you might expect, the Sense also comes equipped with an ECG sensor that can help monitor your heart rhythm if you feel like something funny is going on. The new Fitbit knows when you’re stressed—and how to help you chill |Stan Horaczek |August 26, 2020 |Popular-Science 

Not only had the iconic comedian sexually assaulted many, many women, Maher argued, “I never thought he was funny.” Bill Maher: Hundreds of Millions of Muslims Support Attack on ‘Charlie Hebdo’ |Lloyd Grove |January 8, 2015 |DAILY BEAST 

The taste of metal cutlery after years of plastic can also taste funny. His First Day Out Of Jail After 40 Years: Adjusting To Life Outside |Justin Rohrlich |January 3, 2015 |DAILY BEAST 

But as is her way, Kaling defended why the episode was not only funny, but necessary. Year of the Butt: How the Booty Changed the World in 2014 |Kevin Fallon |December 30, 2014 |DAILY BEAST 

He was funny and self-effacing, though prone to fits of anger. ‘All Good Cretins Go to Heaven’: Dee Dee Ramone’s Twisted Punk Paintings |Melissa Leon |December 15, 2014 |DAILY BEAST 

Pryor was famous for being funny, even as his life was far from funny. How Richard Pryor Beat Bill Cosby and Transformed America |David Yaffe, Scott Saul |December 10, 2014 |DAILY BEAST 

May looked along at the dimpled grace, And then at the saint-like, fair old face, “How funny!” Boys and Girls Bookshelf; a Practical Plan of Character Building, Volume I (of 17) |Various 

We were speaking of the faculty of mimicry, and he told me such a funny little anecdote about Chopin. Music-Study in Germany |Amy Fay 

He looked up, half shutting his one funny eye, and cocking one ear up, and letting the other droop down. Squinty the Comical Pig |Richard Barnum 

I am an easiful old pagan, and I am not angry with you at all—you funny, little champion of the Most High. God and my Neighbour |Robert Blatchford 

He walked first to one side, and then the other, rooting in the dirt with his funny, rubbery nose. Squinty the Comical Pig |Richard Barnum