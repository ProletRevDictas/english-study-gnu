He held onto the ball — in the end zone — for too long, and afraid he was going to be pulled down for a safety, he threw a backbreaking interception. Rams demolish overmatched Cardinals, giving Matthew Stafford his first career playoff win |Mark Maske, Jerry Brewer |January 18, 2022 |Washington Post 

Signs of the toxic and backbreaking work the enslaved had to do at the primitive ironmaking site emerged. Faces of the dead emerge from lost African American graveyard |Michael Ruane |July 9, 2021 |Washington Post 

Advocates for robotic farming tout increased automation as a step toward improving efficiency while freeing people from monotonous, backbreaking tasks that few laborers want to do. Farmers have more mouths to feed. Bring in the robots. |Dalvin Brown |April 22, 2021 |Washington Post 

Today, instead of men performing backbreaking work on creaking machinery, unkempt grass bends in the light breeze. The Grape King from Shogunate Japan |Debra A. Klein |April 3, 2014 |DAILY BEAST 

According to political lore, patriot-Americans do the backbreaking work that keeps the country humming. A 'Black President' Is of No Value to America |Rich Benjamin |November 4, 2008 |DAILY BEAST 

It will take some backbreaking investigation to get the whole story, because the files show nothing on any of them. The Electronic Mind Reader |John Blaine 

All they had to look forward to from the D'zertanoj was backbreaking labor and an early death. The Ethical Engineer |Henry Maxwell Dempsey 

The combination of heat, hard, backbreaking work, and the necessity for hurry made haying a particularly fatiguing time. Frying Pan Farm |Elizabeth Brown Pryor