What’s missing from this gratuitous adaptation, which credits Watanabe as a consultant, is the atmosphere. Netflix's Live-Action Cowboy Bebop Misunderstands What Made the Original a Classic |Judy Berman |November 15, 2021 |Time 

Slott said that he worked with Kane on a particularly violent Two-Gun Kid issue which was censored before publishing but was then published in its gratuitous entirety in a special Gil Kane tribute issue. Punisher Writer Reveals Hero Has "Mountains" of Unpublished Stories |noreply@blogger.com (Unknown) |October 7, 2021 |TechCrunch 

While she missed Phoenix’s first-round nail-biter over New York, Sophie Cunningham entered the game and impersonated her mentor, right down to the made threes and gratuitous technical. Why The Underdogs Have A Shot In The WNBA Semifinals |Howard Megdal |September 28, 2021 |FiveThirtyEight 

One fellow entertainer suggested the conversion was a “gratuitous” effort to curry favor with Jewish nightclub owners and audiences. The history fueling doubts over Britney Spears’s religious conversion |Rebecca Davis |August 26, 2021 |Washington Post 

It seemed gratuitous and counter-intuitive in a story that had already inflicted more than enough suffering. How Dickens and Scrooge Saved Christmas |Clive Irving |December 22, 2014 |DAILY BEAST 

And, in a gratuitous show of homicidal prowess, Moses kills two assassins he meets while wandering in the desert of Sinai. Meet Moses the Swashbuckling Israelite |James Romm |December 14, 2014 |DAILY BEAST 

Initially, I thought, “OK, they have to throw in a wave… that looks gratuitous.” Neil deGrasse Tyson Breaks Down ‘Interstellar’: Black Holes, Time Dilations, and Massive Waves |Marlow Stern |November 11, 2014 |DAILY BEAST 

But God forbid a TV series premieres pushing out gratuitous emotion and gratuitous feeling. ‘Red Band Society’ Is Really Freaking Sad (And May Be TV’s Best New Drama) |Kevin Fallon |September 17, 2014 |DAILY BEAST 

The Israeli soldiers, my age, sat impassively on a bench while babies cried and a man complained about gratuitous humiliation. Gaza, You're No Good For My Marriage |Josh Robin |August 9, 2014 |DAILY BEAST 

A baronet scientifically skilled in pugilism, enjoyed no pleasure so much as giving gratuitous instructions in his favorite art. The Book of Anecdotes and Budget of Fun; |Various 

“I dare say that looked very much like a gratuitous impertinence from—the packer,” he observed. The Gold Trail |Harold Bindloss 

Such are the ideas which the dogma of gratuitous predestination gives of Divinity! Superstition In All Ages (1732) |Jean Meslier 

Much difficulty arose, in the distribution of gratuitous supplies of food, from the routine of the public offices. The History of England in Three Volumes, Vol.III. |E. Farr and E. H. Nolan 

In consequence, they have imagined many gratuitous suppositions to explain the union of the soul with the body. Letters To Eugenia |Paul Henri Thiry Holbach