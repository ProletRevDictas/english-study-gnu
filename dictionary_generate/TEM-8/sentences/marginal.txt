Some of the folks arguing against the change said that this disastrous year of pandemic-style school is the wrong time to prioritize more days off, after F’s skyrocketed and learning has been marginal. Not every student is Christian. So why don’t all school districts recognize that? |Petula Dvorak |March 4, 2021 |Washington Post 

Even if life had originated only on Earth, it need not remain a marginal, trivial feature of the cosmos. If Aliens Exist, Here’s How We’ll Find Them - Issue 97: Wonder |Martin Rees & Mario Livio |February 24, 2021 |Nautilus 

Beckmann encourages injured athletes to keep in mind that marginal differences in body weight are typical across a season for most people, and similar to a postseason break, your body will normalize once you’ve returned to full training. How to Eat When You’re Injured |Becky Wade |February 17, 2021 |Outside Online 

In hindsight, we should’ve communicated this in our snowfall map, something we’ll try to do next time when temperatures are marginal for accumulation. After Sunday’s slush fest, another winter storm threatens by Wednesday night |Jason Samenow, Wes Junker |February 8, 2021 |Washington Post 

These objects that general relativity predicted, that were mathematical curiosities, became real, then they were marginal. Einstein’s theory of general relativity unveiled a dynamic and bizarre cosmos |Elizabeth Quill |February 3, 2021 |Science News 

Both are considered marginal figures in the House GOP caucus and have no real base of support for their respective bids. The YOLO Caucus' New Cry for Attention |Ben Jacobs |January 4, 2015 |DAILY BEAST 

Dynamism is increasingly driven not by economies of scale but by competitively driven marginal improvements. Relax—Both Parties Are Going Extinct |Nick Gillespie |November 4, 2014 |DAILY BEAST 

The play of Sunday, the play of being angels in the choir, is not just a peripheral secondary marginal realm of activity. Joseph Campbell on the Roots of Halloween |Joseph Campbell |October 31, 2014 |DAILY BEAST 

Coping with drought and marginal soils was a continual struggle. ‘The Harness Maker’s Dream:’ The Unlikely Ranch King of Texas |Nick Kotz |September 20, 2014 |DAILY BEAST 

And those who are—like Pat Buchanan—are more marginal figures than ever. The Conservative Case for Unions After the Harris v. Quinn Decision |James Poulos |July 2, 2014 |DAILY BEAST 

This section is not expressly confined to wandering persons, but the marginal note confines it to the "occasional poor." English Poor Law Policy |Sidney Webb 

Postmarginal vein a little longer than the slender, curved stigmal, about a third the length of the marginal. Journal of Entomology and Zoology, March 1917 |Various 

The marginal reading, "Strike thee with six plagues" or "draw thee back with a hook of six teeth" is incorrect. The Prophet Ezekiel |Arno C. Gaebelein 

He made frequent marginal notes along the pages of the world's moral history—notes not always quotable in the family circle. Mark Twain, A Biography, 1835-1910, Complete |Albert Bigelow Paine 

The only change made here is the placing of Aubreys marginal notes among the footnotes: the spelling is Aubreys spelling. Ballads of Mystery and Miracle and Fyttes of Mirth |Frank Sidgwick