One option is to use the differences in ideology, political orientation and values as segmentation variables and see how they align with the core segments that the firm is targeting. Why ethical dilemmas are putting brands and their media buying in the spotlight |Seb Joseph |January 21, 2021 |Digiday 

This past week, this year’s new members of Congress got an orientation like no other. Style Conversational Week 1419: A smile for the Capitol? |Pat Myers |January 14, 2021 |Washington Post 

The ill-fated streaming service drummed up huge buzz at CES 2020, promising to revolutionize content by allowing users to seamlessly switch between vertical and horizontal orientation as they watched content on their phones. CES trends to look out for at this year’s virtual conference |Stan Horaczek |January 6, 2021 |Popular-Science 

The rule text change makes it clear everyone regardless of gender identification or orientation is subject to ethics regulations. House of Representatives poised to swap out "he" and "she" for gender-neutral terms |Kadia Goba |January 2, 2021 |Axios 

He is the author of the book Eco, Ego, Eros, that explores panpsychism across various fields, and of the General Resonance Theory of consciousness, which is panpsychist in orientation. Electrons May Very Well Be Conscious - Issue 94: Evolving |Tam Hunt |December 30, 2020 |Nautilus 

I was told that God hated me, and despite trying very hard to change my sexual orientation, I found that I could not. Do LGBTs Owe Christians an Olive Branch? Try The Other Way Around |Jay Michaelson |December 14, 2014 |DAILY BEAST 

This is an inverse Pietà, and something of a sexual anarchist; she ardently refuses to be oriented in an orientation. Is Bigger Better for St. Vincent? |David Yaffe |December 4, 2014 |DAILY BEAST 

“Mainstream feminism is riddled with classism, racism, and sexual orientation discrimination,” she wrote. Will White Feminists Finally Dump Lena Dunham? |Samantha Allen |November 4, 2014 |DAILY BEAST 

Edwards crudely framed the state of play in the South as a matter of race, sexual orientation, and class; in other words, culture. The 2014 Election Is Yet Another Scrum in the Culture Wars |Lloyd Green |October 27, 2014 |DAILY BEAST 

What kind of groupie FYIs does the NBA provide during rookie orientation? The Chicago Bulls’ Joakim Noah Sounds Off on Weed, the Weather, and Winning |Bill Schulz |October 19, 2014 |DAILY BEAST 

Psychology has formulated plenty of such general statements, and they serve well for a first orientation. Psychotherapy |Hugo Mnsterberg 

For the purposes of orientation a visit to the museum should precede a tour of the fields. Manasses (Bull Run) National Battlefield Park-Virginia |Francis F. Wilshin 

The settlers still haven't grown used to this planet, though we have orientation talks every night. Deathworld |Harry Harrison 

This excellent orientation supplements the brief descriptive remarks attached to the titles. A History of Bibliographies of Bibliographies |Archer Taylor 

This orientation could be the source of a new evangelism that would make its witness heard in the depth and detail of human life. Herein is Love |Reuel L. Howe