Or, in the desert, when the ground is hot, and air is cool, and there is air turbulence, it might cause a shimmer at the horizon that looks like a pool of water. Want art you can’t look away from? These 5 visual illusions will entrance your mind |Corinne Iozzio |July 21, 2021 |Popular-Science 

The moon was coming up, and its mystic shimmer was casting a million lights across the distant, restless water. The Awakening and Selected Short Stories |Kate Chopin 

He saw the tips of the fir trees shimmer, and heard them whisper as the breeze turned their needles towards the light. Three More John Silence Stories |Algernon Blackwood 

The golden shimmer of Edna's satin gown spread in rich folds on either side of her. The Awakening and Selected Short Stories |Kate Chopin 

She looked down, noticing the black line of his leg moving in and out so close to her against the yellow shimmer of her gown. The Awakening and Selected Short Stories |Kate Chopin 

Farther down the river there was a flash of something white amidst the pale green shimmer of the flood. The Gold Trail |Harold Bindloss