Eszterhas describes how Gibson often would digress from the topic at hand to rant about Grigorieva. 17 Craziest Bits From Joe Eszterhas Ebook About Mel Gibson, ‘Heaven and Mel’ |Caleb Baer |June 6, 2012 |DAILY BEAST 

But I digress, enough with the “man who wears two masks” nonsense, as if Banville must justify writing mystery novels. Can Pulp Win the Booker? |Allen Barra |September 7, 2011 |DAILY BEAST 

Do not digress; tell one story at a time; let no incident into your story which cannot answer the question, “Why are you here?” English: Composition and Literature |W. F. (William Franklin) Webster 

I will digress a bit and explain how these stone-quarries were discovered. Ten Books on Architecture |Vitruvius 

While we are on that subject, just to digress for a moment, what was his attitude toward riding in open cars? Warren Commission (7 of 26): Hearings Vol. VII (of 15) |The President's Commission on the Assassination of President Kennedy 

Let me now, however, turn to my tale, from which it is my intention in future to digress as seldom as possible. Sir Jasper Carew |Charles James Lever 

It may be of interest to digress here briefly in order to speak of these little known though common forms of life. Mount Rainier |Various