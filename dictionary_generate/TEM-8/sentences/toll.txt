In Sweden, the government’s decision to adopt a light-touch strategy to tackle the pandemic pushed its death toll per capita many times higher than in the rest of the Nordic region. Europe is at a turning point as COVID cases spike, and fragile governments feel the heat |Bernhard Warner |August 20, 2020 |Fortune 

In contrast, income growth has stalled or even turned negative among the hundreds of millions in the low- to middle-income population as the pandemic took a toll on the jobs market. Haves and Have-Nots: Pandemic Recovery Explodes China’s Wealth Gap |Daniel Malloy |August 19, 2020 |Ozy 

Even as the number of confirmed covid-19 cases in Jammu and Kashmir crossed 13,000 and the death toll passed 200 in mid-July, the government refused to restore 4G internet speeds. How India became the world’s leader in internet shutdowns |Katie McLean |August 19, 2020 |MIT Technology Review 

In early June, it forced the health ministry to start publishing comprehensive data on covid-19 deaths again, after the ministry stopped doing so in what was widely seen as an attempt to cover up the rapidly rising death toll. Brazil is sliding into techno-authoritarianism |Tate Ryan-Mosley |August 19, 2020 |MIT Technology Review 

The incident took a severe toll on public confidence in vaccination. Butterfly Effect: The Unscientific Vaccine |Charu Kasturi |August 13, 2020 |Ozy 

The death toll, which experts believe has been significantly undercut by secret burials, stands at 7,905. The Race for the Ebola Vaccine |Abby Haglage |January 7, 2015 |DAILY BEAST 

In France, the death toll has been lower: One young man killed in the city of Nantes. France’s Wave of Crazy-Terror Christmas Attacks |Christopher Dickey |December 24, 2014 |DAILY BEAST 

The latest reported death toll is 80 children and 46 adults, but that is expected to rise. Taliban: We Slaughtered 100+ Kids Because Their Parents Helped America |Sami Yousafzai |December 16, 2014 |DAILY BEAST 

While the look worked for some, the combination of heat and chemicals took a toll on the hair of others. Goodbye To A Natural Hair Guru: Miss Jessie's Cofounder Titi Branch Dead At 45 |Danielle Belton |December 16, 2014 |DAILY BEAST 

“The amount of literal brainwork needed to do his job too such a toll on him that it sent him to an early grave,” Goode says. From ‘The Good Wife’ to ‘The Imitation Game’: Matthew Goode Wages His Charm Offensive |Kevin Fallon |November 24, 2014 |DAILY BEAST 

On this the royal band of music would strike up its liveliest airs, and a great bell would toll its evening warning. Our Little Korean Cousin |H. Lee M. Pike 

Jack's keeper offered the right toll, but the toll-bar man would not take it. The Nursery, July 1873, Vol. XIV. No. 1 |Various 

A country girl, riding by a turnpike-road without paying toll, the gate-keeper hailed her and demanded his fee. The Book of Anecdotes and Budget of Fun; |Various 

Sixty, nay fifty, years ago, there were six toll-houses and turnpike bars between London and Portsmouth. The Portsmouth Road and Its Tributaries |Charles G. Harper 

On leaving Conway we crossed the suspension bridge, paying a goodly toll for the privilege. British Highways And Byways From A Motor Car |Thomas D. Murphy