Though there is no easy path to a title, the 76ers must figure out the ways they’ve obstructed their own. Can The Sixers Find A Way To Win It All With Embiid And Simmons? |James L. Jackson |September 28, 2020 |FiveThirtyEight 

Aaron Hayward was charged with felonious assault, three counts of assault, aggravated menacing, failure to comply with a police order, three counts of resisting arrest, criminal damaging and obstructing official business. The Startling Reach and Disparate Impact of Cleveland Clinic’s Private Police Force |by David Armstrong |September 28, 2020 |ProPublica 

Of course, it’s a little bit difficult when he’s been obstructed at every turn by the left and by the right. Full Transcript: Tomi Lahren on ‘The Carlos Watson Show’ |Daniel Malloy |August 31, 2020 |Ozy 

If you attach your taillight to your seatpost or seatstays, be sure it’s not being obstructed by a saddlebag or your tire. What You Need to Know About Bike Lights |Aaron Rickel |August 26, 2020 |Outside Online 

In order to eliminate obstructed seating and add layers of revenue-generating suites and club levels, many seats in modern stadiums have been pushed further up and away from the playing surface. MLB’s Newest Ballpark Is A Shift Away From Retro-Era Stadiums |Travis Sawchik |July 16, 2020 |FiveThirtyEight 

He gave money to a group seeking to obstruct equality for gay people. In Gay Rights Fights, Bullies Love to Play the Victim |Tim Teeman |April 4, 2014 |DAILY BEAST 

The first filibuster took place in 1837 and then became an increasingly employed strategy to obstruct the passage of legislation. Senate Democrats Didn’t Go Far Enough to Kill the Filibuster |Dean Obeidallah |November 22, 2013 |DAILY BEAST 

Merkley said that the power of minority to obstruct judicial nominations had really been ended in that standoff. The Filibuster Fight Ain’t Over |Ben Jacobs |November 21, 2013 |DAILY BEAST 

It's not much different from what they've done, or haven't done, all along: obstruct Obama. The Obama Scandals Are Desperate Measures by the GOP |Robert Shrum |May 17, 2013 |DAILY BEAST 

And the very political forces that you are trying to end run would rise up and obstruct at every turn. Should People Be Forced to Buy Liability Insurance for their Guns? |Megan McArdle |December 28, 2012 |DAILY BEAST 

Dalton was passed without difficulty, and beyond we stopped again to cut wires and to obstruct the track. Famous Adventures And Prison Escapes of the Civil War |Various 

All B—— turned out, but did not obstruct my view, for I was at the large first-floor window and not ten yards away. A Thin Ghost and Others |M. R. (Montague Rhodes) James 

Some qualities favour, others obstruct the realisation of a first conception. Frederick Chopin as a Man and Musician |Frederick Niecks 

The bib must not extend too far into the lead pipe or it will obstruct the flow of water. Elements of Plumbing |Samuel Dibble 

The branch should not extend into the run of pipe enough to obstruct the bore of it. Elements of Plumbing |Samuel Dibble