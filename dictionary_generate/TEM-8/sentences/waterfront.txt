The project also may help reduce conflict among oyster farmers, waterfront property owners and watermen, said Tom Miller, director of the Chesapeake Biological Laboratory at the University of Maryland Center for Environmental Science. Solar-powered barge could take oyster farming deeper into Chesapeake |Christine Condon |January 11, 2021 |Washington Post 

The brothers had spent years in jail for refusing to leave waterfront property that had been in their family for a century, land that had been bought by developers without their knowledge. ProPublica Wins Two NABJ Salute to Excellence Awards |by ProPublica |December 21, 2020 |ProPublica 

Stay at the elegant waterfront Stonesthrow Boutique Hotel overlooking Long Sands Beach. New Rules of Adventure: The Northeast |Outside Editors |December 15, 2020 |Outside Online 

Outside the county of San Diego’s headquarters along the waterfront, protestors often demonstrate frustrations that things like live events and youth sports competitions are not allowed. San Diegans Are Worried, But They Have Not Lost Trust |Scott Lewis and Kara Grant |November 3, 2020 |Voice of San Diego 

The event at The Wharf brings business specials, pride-themed fitness classes, and more to the waterfront neighborhood. Blade holds Coming Out Day celebration |Parker Purifoy |October 10, 2020 |Washington Blade 

Venetians sip their coffee in quiet squares and walk their dogs along the waterfront with nary a tourist in sight. After the Wedding: George Clooney and Amal Alamuddin in Venice |Barbie Latza Nadeau |September 28, 2014 |DAILY BEAST 

The vision for the waterfront restaurant (which has yet to name a chef) is boathouse-chic. Celebrities, Take Note: Anguilla Is Back From the Brink |Debra A. Klein |May 6, 2014 |DAILY BEAST 

After HUAC and Waterfront, he writes of his exhaustion and doubts. Elia Kazan Was a Brilliant, Needy Pen Pal |Caryn James |April 30, 2014 |DAILY BEAST 

Lennon had got the sailing bug on Long Island where he and Ono had a house on the waterfront. How John Lennon Rediscovered His Music in Bermuda |The Telegraph |November 3, 2013 |DAILY BEAST 

His new novel, The Big Crowd, is set amidst the violence and corruption of the crime-ridden New York waterfront of the 1940s. Mad, Bad, and Dangerous to Live In: Kevin Baker’s New York |Allen Barra |September 23, 2013 |DAILY BEAST 

One of his most famous art works is an etched view of the waterfront at Richmond. Hallowed Heritage: The Life of Virginia |Dorothy M. Torpey 

After dusk, the crew poured en masse to the nearest waterfront saloon with me. Tramping on Life |Harry Kemp 

In the moonlight, the ruined piers spread along the waterfront to either side of them, some even slanting into the silvered water. The Jewels of Aptor |Samuel R. Delany 

I been along the waterfront long enough t' know that th' lad that picks up a floater gets a reward o' ten dollars from th' city. Captain Scraggs |Peter B. Kyne 

The wide thoroughfare running along the waterfront presented a scene of bewildering confusion. The Secret Wireless |Lewis E. Theiss