It’s still heavy and, at almost $3,800 it is definitely not cheap, but it’s a lot more portable, versatile, and less expensive than a flats boat. This motorized kayak can drive itself |By Nate Matthews/Outdoor Life |August 28, 2020 |Popular-Science 

Easily link the light strands from end-to-end for a more versatile design. Twinkle lights that instantly cheer up your home |PopSci Commerce Team |August 27, 2020 |Popular-Science 

It can be used to understand real-world customer behavior, purchase intent, brand affinity and as a versatile targeting tool for online and mobile campaigns. Foursquare becomes first company to receive MRC location-data accreditation |Greg Sterling |August 21, 2020 |Search Engine Land 

Even head coach Nicki Collen said she thought of Laney as more of a substitute energy source, primarily wanting the versatile 6-footer as a defensive option off the bench. These 3 Breakout Stars Are Making The Most Of The WNBA Bubble |Howard Megdal |August 14, 2020 |FiveThirtyEight 

Off the bench, the Sun have four more newcomers, including 3-point shooter Kaleena Mosqueda-Lewis, and head coach Curt Miller believes his backups are more versatile and athletic than in previous seasons. Will The Mystics Repeat? Will The Storm Surge? What To Watch In The WNBA Bubble. |Jenn Hatfield |July 23, 2020 |FiveThirtyEight 

Since Westlake was as prolific as he was versatile, this all took a while. Donald E. Westlake, The Man With The Getaway Face |Malcolm Jones |October 25, 2014 |DAILY BEAST 

Ever the versatile bigot, Ransdell hates gays, African Americans, and immigrants, in addition to Jewish people. White Supremacist Runs For Senate in Kentucky |Olivia Nuzzi |September 21, 2014 |DAILY BEAST 

Served in lieu of morning pancakes or bread at supper, ployes are nothing if not versatile. On the Canadian Border, It's Pancakes for Every Meal |Jane & Michael Stern |July 6, 2014 |DAILY BEAST 

Regular viewers of The Tonight Show with Jimmy Fallon know that The Roots are one of the most versatile bands working today. Viral Video of the Day: Harry Potter and the Legendary Roots Crew |Alex Chancey |June 19, 2014 |DAILY BEAST 

The former stand-up comedian has evolved into one of the most versatile entertainers in showbiz. Jamie Foxx on ‘The Amazing Spider-Man 2,’ Donald Sterling’s Racism, and Bill O’Reilly’s TV Act |Marlow Stern |April 28, 2014 |DAILY BEAST 

So did my versatile friend, joyously confident in his powers, start on his glorious career as a private detective. The Joyous Adventures of Aristide Pujol |William J. Locke 

How defiant and versatile were the expletives of the old French nobility, we may learn from the pages of Brantôme. A Cursory History of Swearing |Julian Sharman 

The trailer was unhooked and carefully backed in through a passage laid out by the versatile Fisheye. David Lannarck, Midget |George S. Harney 

In the years past, a versatile routing agent could and did avoid many minor financial losses by routing the show to other fields. David Lannarck, Midget |George S. Harney 

Hannah too is versatile; and leaps from adoration to envy with wonderful facility. My New Curate |P.A. Sheehan