You may be able to simply train yourself to wake up and end the dream, or overcome the very vivid feelings of fear and fright by telling yourself that it’s a dream. I taught myself to lucid dream. You can too. |Neel V. Patel |August 25, 2021 |MIT Technology Review 

The green creatures in The Sunken Land, on the other hand, conjure up the visceral fright of something slithering and unfamiliar brushing past your skin when you enter a lake. Politics and the pandemic have changed how we imagine cities |Joanne McNeil |April 28, 2021 |MIT Technology Review 

With such fright back there, it’s small solace that the contenders lack defending champion Dustin Johnson, the No. Hideki Matsuyama’s sublime day at the Masters gives him a four-shot lead and a shot at history |Chuck Culpepper, Scott Allen |April 11, 2021 |Washington Post 

Much of the heavy lifting for frights is done by the environments, especially in the world of the dead that takes a Lovecraftian aesthetic, with slippery tentacles or long arms reaching down from above. ‘The Medium’ review: A disjointed, unfulfilling puzzle horror game |Elise Favis |February 1, 2021 |Washington Post 

To add to the fright, some children live in unstable homes where food is hard to come by. Voice Poll: Residents Deadlocked Between Competing Coronavirus School Fears |Will Huntsberry |November 2, 2020 |Voice of San Diego 

Finally he grabs that blanket [and] I counted eight on one double mattress, eight children held together—dying of fright. Introducing Tzipi Livni to the Occupation |Avner Gvaryahu |October 1, 2013 |DAILY BEAST 

Take this sentence: "With the snake in sight, the horse reeled his paws in fright." Why Do We Want Prices in Health Care? |Megan McArdle |February 27, 2013 |DAILY BEAST 

Kate got a fright when she thought she'd mislaid a family engagement ring that Prince William gave her in 2010. Pip's Pranks |Tom Sykes |April 20, 2012 |DAILY BEAST 

He talks to Gordon Marino about reliving his darkest moments—and how he faced down his stage fright. Mike Tyson Takes the Stage in a Vegas One-Man Show |Gordon Marino |April 8, 2012 |DAILY BEAST 

First, she had to overcome her stage fright to go head-to-head with the big boys. Dana Loesch: On Twitter, the Tea Party, and Her Rise in Media |Tricia Romano |March 20, 2012 |DAILY BEAST 

Liszt looked at it, and to her fright and dismay cried out in a fit of impatience, "No, I won't hear it!" Music-Study in Germany |Amy Fay 

The performers and the nuns nearly died of fright, believing that their last hour had surely come. The Red Year |Louis Tracy 

I am thankful that prolonged mourning is out of date; it made a fright of me and was getting on my nerves. Ancestors |Gertrude Atherton 

Winifred, naturally a high-spirited and lively girl, soon recovered from the fright of that fateful Sunday evening. The Red Year |Louis Tracy 

Frantic with fright, she implored her Maker to have mercy on her, remarking at the same time, "The devil has got me at last." The Book of Anecdotes and Budget of Fun; |Various