It is possible that life may have gained a foothold, given the evidence that the planet once had a much more benign environment. The Four Most Promising Worlds for Alien Life in the Solar System |Gareth Dorrian |September 24, 2020 |Singularity Hub 

My two personal oncology physicians were the ones who advised a conservative strategy of monitoring those nodules in my neck, which could prove to be benign. My cancer might be back—and I wonder if unnecessary radiation caused it in the first place |jakemeth |September 22, 2020 |Fortune 

As the election approaches, the building trend of employee activism against employers seems to be taking a benign turn—but don’t imagine that America’s employers and workers are now united in peace and love. The activist employee hasn’t gone away |Geoffrey Colvin |September 21, 2020 |Fortune 

These other ingredients, which are combined with the therapeutic one, are often sourced from around the world before landing in your medicine cabinet and are not always benign. The ‘inactive' ingredients in your pills could harm you |By Yelena Ionova/The Conversation |September 15, 2020 |Popular-Science 

Sometimes doctors inject patients with benign chemicals that enhance the contrast visible in the image. Facebook and NYU researchers discover a way to speed up MRI scans |Jeremy Kahn |August 18, 2020 |Fortune 

I always saw the horrific side of this seemingly benign environment. Tim Burton Talks ‘Big Eyes,’ His Taste For the Macabre, and the ‘Beetlejuice’ Sequel |Marlow Stern |December 17, 2014 |DAILY BEAST 

Unstoppable or not, John H has seen little in his 38 years to persuade him progress is benign. The Golden West Up for Grabs: ‘Painted Horses’ Is the Next Great Western Novel |Wendy Smith |November 28, 2014 |DAILY BEAST 

It is not some benign magical sleep as shown on TV, where people wake up a year later and are instantly back to normal. Understanding Tracy Morgan’s Traumatic Brain Injury |Jean Kim |November 20, 2014 |DAILY BEAST 

Eric lobbies for an industry of benign usefulness, non-partisan in nature, and over which no cloud of serious controversy looms. Up to a Point: In Defense of Lobbyists |P. J. O’Rourke |October 25, 2014 |DAILY BEAST 

To use a relatively benign example, they are to the Pentagon what Silicon Valley startups were to Eastman Kodak. Why ISIS Keeps Running Circles Around Us, Just Like Al Qaeda Did Before 9/11 |Christopher Dickey |September 11, 2014 |DAILY BEAST 

Benign respdit pater ipse Membertou neophytos se esse, verum imperarem; in mea potestate esse omnia. The Jesuit Relations and Allied Documents, Vol. II: Acadia, 1612-1614 |Various 

Ab eo igitur tpore Patres nostros perhumaniter habuit, atque honorific in omnibus, mensque benign accepit. The Jesuit Relations and Allied Documents, Vol. II: Acadia, 1612-1614 |Various 

He surveyed the crowd in the court-room with calm indifference, and seldom glanced at the gray-bearded, benign-looking judge. Mystery Ranch |Arthur Chapman 

"No passion, Colombaik," put in the benign Joan, again interrupting the impetuous young man. The Pilgrim's Shell or Fergan the Quarryman |Eugne Sue 

He looked very benign as he quoted these verses in the pulpit on Sunday morning, with a half smile, as of pleased meditation. When Valmond Came to Pontiac, Complete |Gilbert Parker