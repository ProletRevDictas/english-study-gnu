If a person is adequately informed — granted, a questionable if — then they are within their rights to sell their data, or exchange it for a service or product, he says. Can privacy coexist with technology that reads and changes brain activity? |Laura Sanders |February 11, 2021 |Science News 

He had been listed as questionable on the Buccaneers’ injury report. Super Bowl highlights: Bucs celebrate championship, Tom Brady wins MVP |Des Bieler, Mark Maske, Chuck Culpepper |February 8, 2021 |Washington Post 

If you’re thinking this sounds like a questionable idea, you’re not alone. Scientists Want to Fight Climate Change by Blocking the Sun With Dust |Vanessa Bates Ramirez |January 28, 2021 |Singularity Hub 

“These are vetting efforts that identify any questionable behavior in the past, or any potential link to questionable behavior not just related to extremism,” said Jonathan Rath Hoffman, chief Pentagon spokesman. 12 members of the National Guard removed from inauguration duty |Dan Lamothe, Alex Horton, Paul Sonne |January 19, 2021 |Washington Post 

They forced a conversation on the issue, even if their motivations were sometimes questionable and their solution flawed. What the 1798 Sedition Act got right — and what it means today |Kaitlyn Carter |January 14, 2021 |Washington Post 

The motives were most always harmless, and only sometimes ethically questionable. Design Your Own Dinosaur: The Era of Custom DNA |Justin Jones |January 8, 2015 |DAILY BEAST 

Stott argues that they were somewhat questionable figures to begin with. Nightmares in Face Paint: Why We’ll Always Be Afraid of Clowns |Oliver Jones |October 18, 2014 |DAILY BEAST 

There is a whole genre of books not sold in regular stores but usually on street stalls in questionable neighborhoods. Prisoners Get Cultural Fix with 8-Tracks and Bootleg Cassettes |Daniel Genis |August 18, 2014 |DAILY BEAST 

I should have know this was a lie when the new number I was given had a questionable history as well. Where Cellphone Numbers Go to Die |Jon Methven |August 16, 2014 |DAILY BEAST 

I had recently attended a wedding because a text message had referenced some questionable behavior. Where Cellphone Numbers Go to Die |Jon Methven |August 16, 2014 |DAILY BEAST 

Mixed with much that is questionable or erroneous, the New Testament contains some truth and beauty. God and my Neighbour |Robert Blatchford 

The organization of man renders it questionable if his primeval ancestor was arboreal to any similar extent. Man And His Ancestor |Charles Morris 

If any word is used which falls the least short of this, Tillotson's remark becomes altogether questionable. The English Church in the Eighteenth Century |Charles J. Abbey and John H. Overton 

Diard's mongrel position, which he himself made still more questionable, brought him great troubles. Juana |Honore de Balzac 

It is questionable whether the Oriental nations had any philosophy distinct from religion. Beacon Lights of History, Volume I |John Lord