“The Cook of the Halcyon,” by Andrea CamilleriBefore he died in 2019 at age 93, novelist and film director Andrea Camilleri wrote in an author’s note that he had adapted the 27th Inspector Salvo Montalbano mystery from an unproduced screenplay. Five thrillers to read now — and soon |Richard Lipez |March 5, 2021 |Washington Post 

Otherwise, this category looks about how we expect the Oscars screenplay race to shape up. Golden Globe nominations 2021: ‘The Crown’ leads TV nominations while ‘Mank’ dominates movies |Emily Yahr, Bethonie Butler, Sonia Rao |February 3, 2021 |Washington Post 

Longtime Wilson collaborator Ruben Santiago-Hudson wrote the screenplay, and the film was produced by Tony award-winner Denzel Washington, who played the lead in Wilson’s Fences on stage and on screen. Viola Davis And Chadwick Boseman’s Love Letter To The Blues Is A Must-See |cmurray |December 18, 2020 |Essence.com 

Citizen Kane was nominated for nine Oscars, including Best Picture, but it only won for its screenplay, which seems to underline Kael’s point a little if you take it as read that the best films in each individual category always win. The tangled history behind David Fincher’s Mank — and its link to Citizen Kane — explained |Alissa Wilkinson |December 4, 2020 |Vox 

Davis and Susan Sarandon were asked to give him some feedback on the screenplay. Does Hollywood Still Have a Princess Problem? (Ep. 394) |Stephen J. Dubner |October 24, 2019 |Freakonomics 

Corden has actually been attached to Into the Woods since the first reading of the screenplay two-and-a-half years ago. New ‘Late Late Show’ Host James Corden Would Like to Manage Your Expectations |Kevin Fallon |December 18, 2014 |DAILY BEAST 

Will the freedom you mentioned writing the novel bleed into your work writing your next screenplay? David Cronenberg: Why Frustrated Novelists Hate the Screenplay |Craig Hubert |October 13, 2014 |DAILY BEAST 

Does the process of writing a novel differ wildly from writing a screenplay? David Cronenberg: Why Frustrated Novelists Hate the Screenplay |Craig Hubert |October 13, 2014 |DAILY BEAST 

Star Wars was meant to be watched as a film, not read as a screenplay. Book Bag: 5 Novels Shakespeare Sort of Wrote |Lois Leveen |October 10, 2014 |DAILY BEAST 

If anyone pitched this as a screenplay, no one would have bought it. Excuse Me For Not Dying: Leonard Cohen at 80 |David Yaffe |September 24, 2014 |DAILY BEAST