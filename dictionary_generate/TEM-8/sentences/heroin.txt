Police said he was under the influence of heroin and found heroin and more than 200 pills in his vehicle. Chiefs assistant coach Britt Reid involved in car accident that injured two children |Mark Maske |February 8, 2021 |Washington Post 

Historically, Houry says, the greatest concern surrounding synthetic opioids was that they might be contaminating other opioids, such as heroin. The pandemic has had a dramatic effect on drug overdoses |Kat Eschner |December 22, 2020 |Popular-Science 

So-called “drug tourism” hasn’t really been a problem for Portugal, but it happened in Switzerland after officials in the 1980s and 1990s began officially “ignoring” heroin in Zurich’s Platzspitz Park. Oregon Just Decriminalized All Drugs – Here’s Why Voters Passed This Groundbreaking Reform |LGBTQ-Editor |December 10, 2020 |No Straight News 

She’s cleaning the same streets on which she once used PCP and heroin. ‘It’s going to take a village’: Community group prepares to deliver 3,000 meals before Thanksgiving |Lauren Lumpkin |November 22, 2020 |Washington Post 

Looking ahead, Maze wonders if such epigenetic changes might also occur in response to other addictive drugs, including heroin, alcohol and nicotine. The Epigenetic Secrets Behind Dopamine, Drug Addiction and Depression |R. Douglas Fields |October 27, 2020 |Quanta Magazine 

The correspondent does a stand-up next to a burning pile of heroin and gets a taste of its effect. BBC Reporter Gets High On The Job |Jack Holmes, The Daily Beast Video |December 23, 2014 |DAILY BEAST 

Like many rock stars of the time, Ramone lived there on-and-off for a time; he even detoxed from heroin there once. ‘All Good Cretins Go to Heaven’: Dee Dee Ramone’s Twisted Punk Paintings |Melissa Leon |December 15, 2014 |DAILY BEAST 

Keith Green finds Ramone at the Chelsea, trying to kick heroin for good. ‘All Good Cretins Go to Heaven’: Dee Dee Ramone’s Twisted Punk Paintings |Melissa Leon |December 15, 2014 |DAILY BEAST 

Back in 2001, that “definition” of female beauty basically fell into two categories: Playboy chic, or heroin chic. Masters of Alt Sex: SuicideGirls Hits Puberty and Wants to Invade Your TV Set |Marlow Stern |December 9, 2014 |DAILY BEAST 

It remains a Schedule I narcotic to this day, considered as dangerous and addictive by the federal government as heroin and MDMA. Pot-Smoking Grannies, Jimmy Fallon Covers U2, and More Viral Videos |The Daily Beast Video |November 23, 2014 |DAILY BEAST 

For those who have opium, cocaine, veronal, or heroin to sell can always find a ready market in London and elsewhere. Dope |Sax Rohmer 

At one time Parke, Davis & Co. admitted that the preparation owed its chief value to heroin. The Propaganda for Reform in Proprietary Medicines, Vol. 1 of 2 |Various 

Because of its activity the most prominent action would be that characteristic of heroin hydrochlorid. The Propaganda for Reform in Proprietary Medicines, Vol. 1 of 2 |Various 

Morphine is the chief active principle, and codeine and heroin are the chief derivatives of morphine. Habits that Handicap |Charles B. Towns 

Codeine is one eighth the strength of morphine; heroin is three times as strong as morphine. Habits that Handicap |Charles B. Towns