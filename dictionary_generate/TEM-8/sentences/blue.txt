Among only developed countries, blue states would be in the top five and red states would be in the top 10. Trump says US Covid-19 deaths would be low if you excluded blue states. That’s wrong. |German Lopez |September 17, 2020 |Vox 

Even in a blue-leaning state like Minnesota, the best Democratic gerrymander likely secures five safe seats while the best Republican one secures six. America needs a democratic revolution |Matthew Yglesias |September 17, 2020 |Vox 

Meanwhile, the blue tilt of currently GOP-held Senate seats in Maine and North Carolina is putting Democratic control of the Senate into play for the first time since they lost it in the 2014 midterms. Our Forecast: A Brewing Current Could Lift Biden … or Swamp Him |Nick Fouriezos |September 17, 2020 |Ozy 

If you take the blue states out, we’re at a level that I don’t think anybody in the world would be at. Trump blames blue states for the coronavirus death toll — but most recent deaths have been in red states |Philip Bump |September 16, 2020 |Washington Post 

Cosmetically, the watches come in gray, silver, gold, blue, and red metallic finishes. Everything announced at Apple’s ‘Time Flies’ event today |rhhackettfortune |September 15, 2020 |Fortune 

Clad in a blue, striped button-down, a silver watch adorning his left wrist, Huckabee beams on the cover. Huckabee 2016: Bend Over and Take It Like a Prisoner! |Olivia Nuzzi |January 8, 2015 |DAILY BEAST 

Yeah, the “Giant man-puppy” that is Gronkowski won't hold a sexual candle to the blue-eyed dreamboat. ‘A Gronking to Remember’ Speed Read: 8 Naughtiest Bits |Emily Shire |January 7, 2015 |DAILY BEAST 

GOP leaders refused; they saw that Duke was pulling blue-collar Democrats to the party. The Louisiana Racists Who Courted Steve Scalise |Jason Berry |January 3, 2015 |DAILY BEAST 

It denotes the person that puts on the badge, puts on the blue uniform, and goes into the streets to put their life at risk. Cop Families Boo De Blasio at NYPD Graduation |Michael Daly |December 30, 2014 |DAILY BEAST 

They looked up into the blue sky as the helicopters flew over in a lost man formation. Choking Back Tears, Thousands of Cops Honor Fallen Officer Ramos |Michael Daly |December 28, 2014 |DAILY BEAST 

In pursuing his alchemical researches, he discovered Prussian blue, and the animal oil which bears his name. The Every Day Book of History and Chronology |Joel Munsell 

Mary is fair as the morning dew— Cheeks of roses and ribbons of blue! Boys and Girls Bookshelf; a Practical Plan of Character Building, Volume I (of 17) |Various 

His nose was hooked and rather large, his eyes were blue, bright as steel, and set a trifle wide. St. Martin's Summer |Rafael Sabatini 

The most promising of the methods which have been devised are cryoscopy, the methylene-blue test, and the phloridzin test. A Manual of Clinical Diagnosis |James Campbell Todd 

He said no more in words, but his little blue eyes had an eloquence that left nothing to mere speech. St. Martin's Summer |Rafael Sabatini