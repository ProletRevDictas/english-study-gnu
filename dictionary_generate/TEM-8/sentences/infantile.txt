In fact, most people can’t remember events from the first few years of their lives—a phenomenon researchers have dubbed infantile amnesia. Why Can’t You Remember the First Years of Your Life? What Scientists Know About ‘Infantile Amnesia’ |Vanessa LoBue |July 24, 2022 |Singularity Hub 

I think it’s partly because the audience for a lot of that stuff is kids – and the culture in general is a bit infantile in this era. A conversation with Bruce LaBruce |John Paul King |September 23, 2021 |Washington Blade 

The idea that we can enjoy the benefits of society while owing nothing in return is literally infantile. Sebastian Junger: What We Talk About When We Talk About Freedom |Nathan S. Webster |May 31, 2021 |The Daily Beast 

The United States got an F — and soon 550,000 dead — because the ignorant or infantile among us, wanting what they wanted when they wanted it, defeated the nation’s adults. Everyone has feelings about crowd size at ballgames. Let’s stick with facts. |Thomas M. Boswell |March 18, 2021 |Washington Post 

The seeds of future nightmares may be planted early in life, during what’s known as the “infantile amnesia” period, which lasts from birth to about age three and a half, and is a time of life during which virtually no enduring memories are formed. Scientists Are Learning to Read—and Change—Your Nightmares |Jeffrey Kluger |August 6, 2020 |Time 

At one point, Valle offered an apology to anybody who had been “hurt, shocked or offended by my infantile actions.” Why NYPD Couldn’t Cook The ‘Cannibal Cop’ |Michael Daly |July 2, 2014 |DAILY BEAST 

It was sweet and funny and self-deprecating, nothing like the infantile behavior she had just displayed. Supermodel Iman Walked Out on Me |Kevin Sessums |November 10, 2010 |DAILY BEAST 

It's how we project our own catastrophe of infantile adulthood. 2009: The Most Impulsive Year Ever |Lee Siegel |December 18, 2009 |DAILY BEAST 

He led her by the hand, and confided to her infantile spirit all his thoughts, his illusions, his day-dreams. Madame Roland, Makers of History |John S. C. Abbott 

The Scherzo is neither good nor bad; the trio is so innocent that it would be almost too infantile for a Sniegourotchka. The Life &amp; Letters of Peter Ilich Tchaikovsky |Modeste Tchaikovsky 

He listened and heard soft breathing that stopped just short of being an infantile snore. Cabin Fever |B. M. Bower 

She was apparently about six years of age, and the picture of infantile innocence and loveliness. Adrift on the Pacific |Edward S. Ellis 

In respect of one symptom or several, many individuals may remain throughout life in an infantile condition. The Sexual Life of the Child |Albert Moll