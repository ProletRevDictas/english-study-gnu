Giridharadas’s message at the time was largely that the generosity of the global elite is somewhat laughable — that many of the same players who say they want to help society are creating its most intractable problems. Writer Anand Giridharadas on tech’s billionaires: “Are they even on the same team as us?” |Connie Loizos |September 25, 2020 |TechCrunch 

“Even brief experiences of awe yield a host of benefits including an expanded sense of time and enhanced feelings of generosity, well-being, and humility,” the paper notes. Regular doses of awe can do wonders for emotional health |Kat Eschner |September 22, 2020 |Popular-Science 

The deal’s terms were a red flag for some analysts, and not just because of its generosity to GM. Why GM may escape from Nikola’s wreckage unharmed |dzanemorris |September 21, 2020 |Fortune 

Boerner Horvath has also, once again, benefited from the generosity of colleagues facing less competitive races. Sacramento Report: The Debate Over SB 145 Sure Escalated Quickly |Voice of San Diego |September 18, 2020 |Voice of San Diego 

To understand the seeming generosity of the elder birds, Ducouret and her team observed 27 broods of barn owls across the Switzerland countryside. Barn owlets share food with their younger siblings in exchange for grooming |Pratik Pawar |June 16, 2020 |Science News 

This is comedy based on a cold humor, detached, euphemistic, devoid of any generosity. Houellebecq’s Incendiary Novel Imagines France With a Muslim President |Pierre Assouline |January 9, 2015 |DAILY BEAST 

So Pope Francis has found a way to turn the generosity of others into good luck for a few. Pope Francis Raffles Off His Swag to Help the Poor |Barbie Latza Nadeau |November 18, 2014 |DAILY BEAST 

So not only were they anti-Clinton, but they were anti-American small business and anti-generosity! Hillary’s Outside Enforcers Are Led by a Former Foe |David Freedlander |July 10, 2014 |DAILY BEAST 

I was, in fact, one of the lucky recipients of the inexhaustible generosity documented in the film. My Friend, Roger Ebert: Pulitzer Prize Winner Tom Shales on the Moving Documentary ‘Life Itself’ |Tom Shales |July 6, 2014 |DAILY BEAST 

His reckless, open-hearted generosity eventually caught up with him. When Downtown Was Cool: Mario Batali, Simon Doonan, Wynton Marsalis Remember the Good Old Days |The Daily Beast |April 10, 2014 |DAILY BEAST 

He come July six, for don't you mind how they called him Cevery out of pity and generosity for the Spayniards? The Soldier of the Valley |Nelson Lloyd 

His unbounded generosity won for him the admiration of all his race, who graciously recognized him as their Maguinoó. The Philippine Islands |John Foreman 

Stanhope subscribed to the reasonableness of the Duke's first impressions, as the immediate effect of such supposed generosity. The Pastor's Fire-side Vol. 3 of 4 |Jane Porter 

As in the old days of the Rusholme Road, Batterby flung his money about with unostentatious generosity. The Joyous Adventures of Aristide Pujol |William J. Locke 

Altogether an odd and difficult character, but with a generosity and high courage that made her very lovable. Three More John Silence Stories |Algernon Blackwood