Glass delivers a far more urgent book, but she shares Bennett’s visual focus and pointillist detail, her penchant for lyrical interludes, and a romance for nature manifested in metaphors. Emma Glass’s ‘Rest and Be Thankful’ powerfully describes what it means to be a health-care worker |Pete Tosiello |December 2, 2020 |Washington Post 

Besides being a refreshing interlude, it has helped me discover new poets that I wouldn’t have read otherwise. Everything Our Editors Loved in October |The Editors |November 3, 2020 |Outside Online 

Its short chapters, interspersed with interludes of photos, pack a wallop of poignancy, beauty, love – even joy. Chronicling the ‘new normal’ amid pandemic |Kathi Wolfe |October 1, 2020 |Washington Blade 

For those awaiting the “matrix in the matrix,” as Musk had hinted on Twitter, the cute-animal interlude was not exactly what they hoped for. Elon Musk’s Neuralink is neuroscience theater |David Rotman |August 30, 2020 |MIT Technology Review 

Some of the little interludes I talk about voting, or I talk about this or that. Reasons to Be Cheerful (Ep. 417) |Stephen J. Dubner |May 7, 2020 |Freakonomics 

The cops subsequently pulled the surveillance camera footage and noted the interlude in minutest detail. Rob Ford’s Web of Criminal Friends |Michael Daly |November 22, 2013 |DAILY BEAST 

They seem to be something more like a brief interlude between his early acts of violence and their later, depraved culmination. Don’t Call Navy Yard Gunman Aaron Alexis a Veteran |Jacob Siegel |September 18, 2013 |DAILY BEAST 

It was an anti-climactic interlude in a trial that has had many moments of drama. Whitey Bulger Refuses to Testify |T.J. English |August 4, 2013 |DAILY BEAST 

A hazy choral interlude follows, followed by more nasty beats and acerbic lyrics. Praise ‘Yeezus’: Kanye West’s New Album Is an Eclectic Tour de Force |Marlow Stern |June 15, 2013 |DAILY BEAST 

“It can be an interlude which changes the present dialogue about the momentum of the campaign,” Jamieson added. Biden’s Mission Impossible: Stop Obama Free Fall With Ryan Debate |David Freedlander |October 10, 2012 |DAILY BEAST 

Gradually and imperceptibly the interlude melted into the soft opening minor chords of the Chopin Impromptu. The Awakening and Selected Short Stories |Kate Chopin 

It is hard to say just when the lights went down on Sara Lee's quiet stage and the interlude began. The Amazing Interlude |Mary Roberts Rinehart 

Mr. Shiel readily made this statement, and thus ended this ridiculous interlude. The History of England in Three Volumes, Vol.III. |E. Farr and E. H. Nolan 

Then, by way of interlude, on the 28th March, came the notorious Dahlgren raid. Four Years in Rebel Capitals |T. C. DeLeon 

Before Father Irwin was questioned, however, there was a delightful interlude. My New Curate |P.A. Sheehan