Long before that, Scaife had lived the life of a dilettante. The Great Inheritors: How Three Families Shielded Their Fortunes From Taxes for Generations |by Patricia Callahan, James Bandler, Justin Elliott, Doris Burke and Jeff Ernsthausen |December 15, 2021 |ProPublica 

One minute they are a king, the next a murderer, a jaded dilettante, a passionate and forsaken lover. After 1,000 pages, you’ll hunger for more Highsmith |Kathi Wolfe |December 11, 2021 |Washington Blade 

The standard critique is that guiding companies are hauling rich, inexperienced dilettantes up the mountain who create traffic jams and make bad decisions, putting everyone at greater risk. Why Do Climbers Really Die on Everest? |Alex Hutchinson |May 19, 2021 |Outside Online 

Delivering water helps “dispel the nagging guilt of a slumming dilettante, but only a little” Hardin writes, while wondering whether it’s possible to make a difference in a place “where the devastation extends back decades.” In ‘Standpipe,’ David Hardin offers poignant, fleeting reflections on the Flint water crisis |Kerri Arsenault |January 21, 2021 |Washington Post 

Suggestions ranged from the dilettante purse designer played by Jennifer Coolidge in the original series to Leslie Jones, who seemed to have Samantha confused with Sabrina the Teenage Witch. Sex and the City Is Nothing Without Samantha Jones |Judy Berman |January 11, 2021 |Time 

The fired host unloads on Current TV, accusing Al Gore of being a dilettante and co-owner Joel Hyatt of blackmail. Keith Olbermann Files a No-Holds-Barred Lawsuit Over Firing by Current TV |Howard Kurtz |April 5, 2012 |DAILY BEAST 

But the director publicly clashed with Norton (calling him a “narcissistic dilettante”). Hollywood’s Craziest Director Tony Kaye, Seeks Redemption, With ‘Detachment’ |Chris Lee |March 15, 2012 |DAILY BEAST 

He fully admits his chronicle of Galliano's shows from 2004 to 2010 was “the work of a dilettante.” Paris' Sad Galliano Expo |Tracy McNicoll |June 21, 2011 |DAILY BEAST 

I finally feel like I can call myself a writer now, rather than writing being just something I do on the side, as a dilettante. Molly Ringwald Grows Up |Rebecca Dana |May 3, 2010 |DAILY BEAST 

This deficiency in technique must even debar him from claiming any higher signification than that of a clever dilettante. The History of Modern Painting, Volume 1 (of 4) |Richard Muther 

He had dropped in in a dilettante spirit to hear the spirited debate, and the judges were greatly honored. Through the Wall |Cleveland Moffett 

With increase of reading we have fallen into a fireside, dilettante culture of ideas as an intellectual pleasure. More Pages from a Journal |Mark Rutherford 

Do not suspect that I impose on you the task of writing letters to answer my dilettante questions. George Eliot's Life, Vol. II (of 3) |George Eliot 

I believe this to be the test to distinguish the mere dilettante from the artist of real genius. The Aesthetical Essays |Friedrich Schiller