Vaccine tourism, in some ways, is a manifestation of these global inequalities. Don’t hate the vaccine tourists, hate the vaccine game |Jen Kirby |February 12, 2021 |Vox 

I can participate in this historic global moment, despite the fact that I left the country 31 years ago. Ruby Corado backs transgender Central American Parliament candidate |Michael K. Lavers |February 11, 2021 |Washington Blade 

As the global specialist in energy management and automation, it is the business’s mission to be a digital partner for sustainability and efficiency. Case study: Schneider Electric’s digital transformation through centralized search |Evan Kent and Kimberly Dutcher |February 11, 2021 |Search Engine Watch 

It was hard to miss the commercials, some of which claimed the company’s students—some as young as six—were being chased by global investors and bagging jobs at Google because of the skills they’d acquired through WhiteHat Jr classes. What India’s most controversial ed-tech entrepreneur would do differently next time |Ananya Bhattacharya |February 10, 2021 |Quartz 

They stepped up like never before to tackle numerous global issues, demonstrating they not only love solving incredibly hard problems, but can do it well and at scale. The rise of the activist developer |Walter Thompson |February 9, 2021 |TechCrunch 

At some point during his busy schedule, Israel found the time to write a book, titled The Global War on Morris. Powerful Congressman Writes About ‘Fleshy Breasts’ |Asawin Suebsaeng |January 7, 2015 |DAILY BEAST 

We face a lot of problems at this moment in our national and global history. In 2015, Let’s Try for More Compassion |Gene Robinson |January 4, 2015 |DAILY BEAST 

Instead, local life is defined by cultural products that are more national or more global—think of the Sunday New York Times. Will Texas Stay Texan? |David Fontana |December 29, 2014 |DAILY BEAST 

As Puar further pointed out, this notion of a global gay identity is easily manipulated. How Canadian Oilmen Pinkwash the Keystone Pipeline |Jay Michaelson |December 28, 2014 |DAILY BEAST 

World peace, religious tolerance, and an end to global poverty, hunger, and disease. Santa Fails One More Time |P. J. O’Rourke |December 27, 2014 |DAILY BEAST 

After Test Mike, the implications of fallout obviously were global. Atoms, Nature, and Man |Neal O. Hines 

Work goes on constantly, whole staffs laboring to maintain a coherent and convincing picture of a global war. The Defenders |Philip K. Dick 

They are also adapted to the dynamics of change and to the global nature of human existence. The Civilization of Illiteracy |Mihai Nadin 

The global community of tele-viewing is splitting into smaller and smaller groups. The Civilization of Illiteracy |Mihai Nadin 

Augmented by worldwide networking, this pragmatics has become global in scope. The Civilization of Illiteracy |Mihai Nadin