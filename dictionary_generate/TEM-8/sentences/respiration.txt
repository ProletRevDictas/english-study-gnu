Cellular respiration is a chemical process that takes place inside cells and produces energy. Scientists Say: Respiration |Bethany Brookshire |January 11, 2021 |Science News For Students 

In our lungs, respiration can refer to the act of breathing. Scientists Say: Respiration |Bethany Brookshire |January 11, 2021 |Science News For Students 

Of course, particles emitted during respiration may pass through thin material, but a low pressure drop may aid diffusion, with particles floating around inside the gaiter until they get stuck on fabric or your skin. What You Need to Know About Wearing a Face Mask Outside |Joe Lindsey |September 30, 2020 |Outside Online 

While most dives lasted around an hour, 5 percent exceeded about 78 minutes, suggesting it takes more than twice as long as thought for the whales to switch to anaerobic respiration. A beaked whale’s nearly four-hour-long dive sets a new record |Erin Garcia de Jesus |September 23, 2020 |Science News 

Some ROS are produced in the normal course of organisms’ respiration, metabolism and immunological defense, sometimes for specific functions and sometimes as byproducts. Why Sleep Deprivation Kills |Veronique Greenwood |June 4, 2020 |Quanta Magazine 

And some reptiles add a fourth function to the overworked cloacal repository–that of respiration as well. What the Man With No Ass Crack Can Teach Darwinists and Creationists |Kent Sepkowitz |January 14, 2014 |DAILY BEAST 

From higher up, at the level of the hidden bed, came the regular plaintive respiration of Sarah Gailey. Hilda Lessways |Arnold Bennett 

It is produced abundantly when vegetable matters are burnt, as also during respiration, fermentation, and many other processes. Elements of Agricultural Chemistry |Thomas Anderson 

On examining the respiration and pulse, I have never been able to detect any characteristic abnormality. A Statistical Inquiry Into the Nature and Treatment of Epilepsy |Alexander Hughes Bennett 

His eyes are closed, and from the parted lips there issues the regular respiration of sound sleep. Prison Memoirs of an Anarchist |Alexander Berkman 

After twelve minutes of artificial respiration the lungs and heart began to act. Essays In Pastoral Medicine |Austin Malley