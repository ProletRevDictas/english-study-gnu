The scene was heavily cordoned off to traffic and anyone not with the police, press, or residents. Police Hunt for Paris Massacre Suspects |Tracy McNicoll, Christopher Dickey |January 7, 2015 |DAILY BEAST 

At the same time anyone could carry anything—even drugs—easily. Ghost Ships of the Mediterranean |Barbie Latza Nadeau |January 6, 2015 |DAILY BEAST 

The injunction, she argued, only applies to these four plaintiffs—not to anyone else. The Back Alley, Low Blow-Ridden Fight to Stop Gay Marriage in Florida Is Finally Over |Jay Michaelson |January 5, 2015 |DAILY BEAST 

Anyone who tries to draw attention to threats instead of quietly burying them is worsening the problem. Cover-Ups and Concern Trolls: Actually, It's About Ethics in Suicide Journalism |Arthur Chu |January 3, 2015 |DAILY BEAST 

Liberals distrust business and anyone with power—better to tell them exactly what to do. Red Tape Is Strangling Good Samaritans |Philip K. Howard |December 27, 2014 |DAILY BEAST 

But just wishing never made anyone larger or taller, not even a pig, and Squinty stayed the same size. Squinty the Comical Pig |Richard Barnum 

"Never better pleased to see anyone in my life," said Blair, nearly shaking Lawrence's arm off. The Courier of the Ozarks |Byron A. Dunn 

Nor is there anyone of that order who talks of going back to those kingdoms without the most urgent reason making it necessary. The Philippine Islands, 1493-1898, Volume XX, 1621-1624 |Various 

In all my life I had never seen a handsomer man, and I don't suppose anyone else there had either. Uncanny Tales |Various 

If Wee Willie Winkie took an interest in anyone, the fortunate man was envied alike by the mess and the rank and file. Kipling Stories and Poems Every Child Should Know, Book II |Rudyard Kipling