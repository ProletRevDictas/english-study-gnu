These infections are primarily detected during winter and spring in temperate climates and are found year-round in tropical and subtropical areas. COVID-19 may one day come and go like the flu, but we’re not there yet |Kate Baggaley |September 16, 2020 |Popular-Science 

Something like a tenth of the people who live in the South and the Southwest — from South Carolina to Alabama to Texas to Southern California — decide to move north in search of a better economy and a more temperate environment. Climate Change Will Force a New American Migration |by Abrahm Lustgarten, photography by Meridith Kohut |September 15, 2020 |ProPublica 

The team still doesn’t know if the gas actually originates at the “temperate” heights observed in the Venusian clouds, or whether it’s produced closer to the surface and then rises. Gas spotted in Venus’s clouds could be a sign of alien life |Neel Patel |September 14, 2020 |MIT Technology Review 

In temperate climates, workers of this species forage alone, often for beetles. What you need to know about ‘murder hornets’ |Susan Milius |July 20, 2020 |Science News For Students 

Prairies are a type of temperate grassland, similar to savannahs or steppes. Scientists Say: Prairie |Bethany Brookshire |July 13, 2020 |Science News For Students 

Houston, where I have been working as a consultant, hardly qualifies as one of the most physically attractive or temperate cities. Battle of the Upstarts: Houston vs. San Francisco Bay |Joel Kotkin |October 5, 2014 |DAILY BEAST 

Many Indians regard it as a quasi-mythical place, a land of lush hills, temperate climate, martial men, and handsome women. Lush Places: The Scotland of India |Tunku Varadarajan |March 25, 2014 |DAILY BEAST 

From a lazy young man about town, I had become active, energetic, temperate, and above all—oh, above all else—ambitious. Read ‘The King in Yellow,’ the ‘True Detective’ Reference That’s the Key to the Show |Robert W. Chambers |February 20, 2014 |DAILY BEAST 

Oregonians suffer through them in anticipation of the blissfully sunny and temperate summer. Hold Up, Hipsters: Stop Obsessing Over Oregon |Nina Strochlic |January 10, 2014 |DAILY BEAST 

In conversation, he is no less articulate, but he is decidedly more temperate, cheerful, even conciliatory. Our Crazy Quest for Immortality |Malcolm Jones |May 14, 2011 |DAILY BEAST 

The climate of those mountains is cold rather than temperate, and less healthful than sickly. The Philippine Islands, 1493-1898, Volume XX, 1621-1624 |Various 

The same change occurs, though to a much smaller extent, in the soil in temperate climates. Elements of Agricultural Chemistry |Thomas Anderson 

He was of frugal and temperate habits, a wiry man at the height of his physical powers, with lean flanks and a deep chest. Uncanny Tales |Various 

While the tobacco of the tropics is the finest in flavor, the more temperate regions produce the finest and best colored leaf. Tobacco; Its History, Varieties, Culture, Manufacture and Commerce |E. R. Billings. 

Doubtless the varieties grown in the tropics will be much finer than the varieties grown in a more temperate region. Tobacco; Its History, Varieties, Culture, Manufacture and Commerce |E. R. Billings.