Some brands of planners have optional covers that offer your content extra protection from spills, tears, and creases. Best daily planners to boost your productivity |Rowan Lyons |August 21, 2021 |Popular-Science 

Over time, hard hooks made of metal can create dents and creases in the padding of headphones with a cushioned or leather head strap. The best headphone stands for audiophiles, gamers, and anyone with a desk |Irena Collaku |July 23, 2021 |Popular-Science 

The occasions when the Wizards have felt ascendant — truly ascendant — in this century take longer to pry from the creases in your brain than they do to count. These Wizards are many things as they enter the playoffs. Afraid is not one of them. |Barry Svrluga |May 21, 2021 |Washington Post 

He could have left the puck behind the net and reentered the crease the way he came out. Ilya Samsonov’s short Caps career has seen highs and lows. His Game 3 packed in both. |Samantha Pell |May 20, 2021 |Washington Post 

The Penguins’ power play ranked fourth in the NHL this season thanks in large part to how well Pittsburgh created shots in the high-danger areas such as the slot or crease. What to know about the 2021 Stanley Cup playoffs |Matt Bonesteel, Neil Greenberg |May 14, 2021 |Washington Post 

Is that pillow crease line that used to go away an hour after I got out of bed now just that line I have on my face? Some 4 a.m. Brainstorming on How to Make Obama Tougher Than Putin |Annabelle Gurwitch |May 23, 2014 |DAILY BEAST 

Turns out the fungi in our inguinal crease are not the same ones on our heel pad or behind our ear. Happy Summer. You’re Covered in Fungus. |Kent Sepkowitz |July 5, 2013 |DAILY BEAST 

That zone is a fault line, a crease in the planet floor between the North American plate and the Continental plate. Hurricane Sandy Will Be Dwarfed by an Earthquake |Winston Ross |November 5, 2012 |DAILY BEAST 

As he told the story, his lidded eyes would crease into a warm, delighted look. Remembering the Man Who Brought Jaws—and Me—to the Shelves |Christopher Buckley |December 23, 2008 |DAILY BEAST 

En I reck'n de wives quarrels considable; en dat 'crease de racket. Adventures of Huckleberry Finn, Complete |Mark Twain (Samuel Clemens) 

Keep the solution boiling until the segments are cooked so soft that folding them leaves no crease. Philippine Mats |Hugo H. Miller 

The clip is slipped over the fold inside of the hat which forms the bottom part of the crease. The Boy Mechanic, Book 2 |Various 

Average Jones smiled with almost affectionate admiration at the crease along the knee of his carefully pressed trousers. Average Jones |Samuel Hopkins Adams 

Now it sparkled gently in his glass and he sighed, letting a smile crease his lean homely face. Security |Poul William Anderson