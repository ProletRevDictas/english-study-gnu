Or, if you hate sharpening, you can consider opting for mechanical pencils. Excellent pencils for school, work, and beyond |PopSci Commerce Team |September 16, 2020 |Popular-Science 

To make sure that their germs stay with them, students shouldn’t share art supplies, pencils or paper, Kaushal says. Here’s how COVID-19 is changing classes this year |Bethany Brookshire |September 8, 2020 |Science News For Students 

Perron worked out the first six dimensions with pencil and paper, but by the 1990s, researchers had learned how to translate Keller’s conjecture into a completely different form — one that allowed them to apply computers to the problem. Computer Search Settles 90-Year-Old Math Problem |Kevin Hartnett |August 19, 2020 |Quanta Magazine 

Smooth, continuous curves are the ones you’d likely draw if you sat down with pencil and paper. New Geometric Perspective Cracks Old Problem About Rectangles |Kevin Hartnett |June 25, 2020 |Quanta Magazine 

There’s a famous story about the fact that no one person knows how to make a pencil. What Happens When Everyone Stays Home to Eat? (Ep. 412) |Stephen J. Dubner |April 9, 2020 |Freakonomics 

So, I had a pen and a pencil and started scribbling and drawing, and I felt good about it. The Renegade: Robert Downey Sr. on His Classic Films, Son’s Battle with Drugs, and Bill Cosby |Marlow Stern |November 26, 2014 |DAILY BEAST 

And then one pencil drawing of my face, and one pencil drawing of the Genie from Aladdin. How Aidy Bryant Stealthily Became Your Favorite ‘Saturday Night Live’ Star |Kevin Fallon |October 31, 2014 |DAILY BEAST 

Then he tagged me in one of his posts, and it was a horrific pencil drawing of my face. How Aidy Bryant Stealthily Became Your Favorite ‘Saturday Night Live’ Star |Kevin Fallon |October 31, 2014 |DAILY BEAST 

If you saw it in front of you, your pencil would drop, your tablet would fall from your lap. How Mork Melted the Fonz: Henry Winkler Recalls Robin Williams’s Storming ‘Happy Days’ Debut |Tim Teeman |August 12, 2014 |DAILY BEAST 

Collectively, they span almost every technique and medium—from color pencil and markers to oil sticks and watercolor. O.J., Martha, Jagger, and Manson: Capturing Celebrities in the Dock |Justin Jones |May 29, 2014 |DAILY BEAST 

A little boy aged two years and four months was deprived of a pencil from Thursday to Sunday for scribbling on the wall-paper. Children's Ways |James Sully 

And sure enough when Sunday came, and the pencil was restored to him, he promptly showed nurse his picture. Children's Ways |James Sully 

We are so many around here that you'll have to get paper and pencil and mark us down to keep track of how many. The Homesteader |Oscar Micheaux 

He had written his name with pencil upon a leaf which he tore from his pocket-book, and sent it to the Marchioness. The Pastor's Fire-side Vol. 3 of 4 |Jane Porter 

During my schooldays I spent many happy hours alone with book or pen or pencil. Fifty Years of Railway Life in England, Scotland and Ireland |Joseph Tatlow