Unemployment benefitsOverall, 10 million people in the United States are currently unemployed, and about 40 percent of these people have been out of work for more than six months. Tax season 2021: A tornado is coming |Michelle Singletary |February 12, 2021 |Washington Post 

“Correcting this misclassification and counting those who have left the labor force since last February as unemployed would boost the unemployment rate to close to 10 percent in January,” Powell said Wednesday. Fed chair: Unemployment rate was closer to 10 percent, not 6.3 percent, in January |Rachel Siegel |February 10, 2021 |Washington Post 

The Democrats’ priorities are incredibly distorted given that many small businesses are struggling and millions of Americans are unemployed. Federal workers could get more paid leave if covid-19 prevents them from working |Eric Yoder |February 10, 2021 |Washington Post 

A lot of us are still working, but our hours have been so drastically affected by covid that we might as well be unemployed. Cutting off stimulus checks to Americans earning over $75,000 could be wise, new data suggests |Heather Long |January 26, 2021 |Washington Post 

Millions of Americans are still unemployed months into the pandemic, but there is one segment of the economy where hiring is booming. Covid-19 is powering the fastest growing segment of the US jobs market |Karen Ho |January 15, 2021 |Quartz 

Around half the Baluch in the province are unemployed, a result, say rights groups, of longstanding marginalization by Tehran. The Dangerous Drug-Funded Secret War Between Iran and Pakistan |Umar Farooq |December 29, 2014 |DAILY BEAST 

A new WPA would have helped create jobs and provided some training to underemployed or unemployed youth. Time to Bring Back the Truman Democrats |Joel Kotkin |December 21, 2014 |DAILY BEAST 

The unemployed have a right to be anxious about the ravages on their families exacted by their unemployment. Ebola, ISIS, the Border: So Much to Fear, So Little Time! |Gene Robinson |November 2, 2014 |DAILY BEAST 

Since the spill, the number of unemployed residents in Louisiana and Alabama has only increased. Deepwater Horizon: Life Drowning in Oil |Samuel Fragoso |November 2, 2014 |DAILY BEAST 

A few held signs addressed to George, asking him to “adopt an unemployed worker.” An Affair to Remember for George and Amal |Barbie Latza Nadeau |September 29, 2014 |DAILY BEAST 

Governmental care of the unemployed, the infant and the infirm, sounds like a chapter in socialism. The Unsolved Riddle of Social Justice |Stephen Leacock 

There was no attempt to set all the unemployed to work, and no desire to confine to them the staff that was engaged. English Poor Law Policy |Sidney Webb 

The Unemployed Workmen Act carries this contrary policy of discrimination according to merit into the class of the able-bodied. English Poor Law Policy |Sidney Webb 

The loss of trade brought Bruges face to face with the 'question of the unemployed' in a very aggravated form. Belgium |George W. T. (George William Thomson) Omond 

In 1905 the Unemployed Workmen Act created a rival authority for relieving the able-bodied man. English Poor Law Policy |Sidney Webb