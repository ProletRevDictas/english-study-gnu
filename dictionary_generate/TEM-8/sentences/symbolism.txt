Beyond the practical importance of a caregiving czar, the position would also carry great symbolism. Melinda Gates calls for a caregiving ‘czar’ |Claire Zillman, reporter |December 4, 2020 |Fortune 

It’s unclear what effect the resolution will have beyond symbolism, given that the Virginia Department of Health is charged with enforcing the mandates, not local officials. Rural Virginia county officials pass resolution rejecting ‘tyranny’ of governor’s coronavirus restrictions |Gregory S. Schneider |December 3, 2020 |Washington Post 

Foods eaten around the new year are rife with symbolism and often served to visiting friends and family. These Chinese walnut cookies are buttery and crumbly, with a five-spice streusel filling |Hetty McKinnon |December 2, 2020 |Washington Post 

To sweeten the symbolism, the oil and gas giant ExxonMobil had just been ejected from the Dow Jones index, marking the fossil fuel industry’s ignominious decline during the energy transition. Tesla’s bumpy road to today’s stratospheric stock price |Michael J. Coren |September 25, 2020 |Quartz 

There’s such powerful symbolism behind the cowry that tremendously moved me. Lafalaise Dion Takes ESSENCE On A Personal Journey |Nandi Howard |September 4, 2020 |Essence.com 

The fact that he was celebrating another loss for the star-crossed city of Detroit only enhances the symbolism. Will Chris Christie Regret His Cowboy Hug? |Matt Lewis |January 5, 2015 |DAILY BEAST 

But what he did was reasonably brave and freighted with all the symbolism of which he was well aware. Steve Scalise and the Right’s Ridiculous Racial Blame Game |Michael Tomasky |January 2, 2015 |DAILY BEAST 

The exhibition also provides useful insight into the importance of symbolism, particularly when it comes to religious painting. The Virgin Mary Lookbook |William O’Connor |December 7, 2014 |DAILY BEAST 

The move gets an “A” for symbolism and a “D” for everything else. Sony Blames North Korea for Hacking, but Washington Left Them Completely Vulnerable |Gordon G. Chang |December 3, 2014 |DAILY BEAST 

Some critics complained that his symbolism was obscure and was lost on the audience. Was the Unabomber a Eugene O’Neill Fan? |Robert M. Dowling |November 6, 2014 |DAILY BEAST 

The rude symbolism was softened and toned to an almost poetical refinement, and gave to the harmless revels a touch of Arcady. When Valmond Came to Pontiac, Complete |Gilbert Parker 

This origin, as I have elsewhere endeavoured to show, is due largely to symbolism, which is merely another term for metaphor. Archaic England |Harold Bayley 

The authorities superstitious “custom” and it is probable that in symbolism may also be found the origin of totemism. Archaic England |Harold Bayley 

Symbolism was undeniably a primitive mode of thinging thought or expressing abstract ideas by things. Archaic England |Harold Bayley 

We shall deal more fully with the cult and symbolism of the dog in a future chapter entitled “The Hound of Heaven”. Archaic England |Harold Bayley