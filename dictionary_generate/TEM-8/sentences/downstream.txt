Antitrust authorities around the world would need to weigh the position a merged Exxon and Chevron would have in both the upstream and downstream sectors. Exxon and Chevron CEOs reportedly discussed a mega merger last year |Katherine Dunn |February 1, 2021 |Fortune 

Beyond some threshold—or weight—the cell fires a signal to its own network of downstream connections. New ‘Liquid’ AI Learns Continuously From Its Experience of the World |Jason Dorrier |January 31, 2021 |Singularity Hub 

ERNIE-ViL also achieved state-of-the-art performance on five vision-language downstream tasks. These five AI developments will shape 2021 and beyond |Jason Sparapani |January 14, 2021 |MIT Technology Review 

“If the downstream impact or the byproduct of this site is some sort of unity between the parties, great,” Evans told the Washington Post in October. Can Captain America serve two dramatically different versions of America? |Aja Romano |January 12, 2021 |Vox 

Looking downstream, though, it’s not clear how long their sin will sting. In the Long Game, Even Insurrection May Not Disqualify Presidential Hopes |Philip Elliott |January 8, 2021 |Time 

It could be that those downstream abnormalities in cell development were due to improper signaling from the cerebellum. Early Brain Injury Might Be the Root of Autism |Russell Saunders |September 7, 2014 |DAILY BEAST 

There is now a black dot in a cluster of reeds about two hundred meters downstream. Whatever You Do Someone Will Die. A Short Story About Impossible Choices in Iraq |Nathan Bradley Bethea |August 31, 2014 |DAILY BEAST 

The civil war has reached a fever pitch and your battalion has recovered eight bodies floating downstream this week alone. Whatever You Do Someone Will Die. A Short Story About Impossible Choices in Iraq |Nathan Bradley Bethea |August 31, 2014 |DAILY BEAST 

The mother continues to row frantically, but the boat begins to drift slowly downstream. Whatever You Do Someone Will Die. A Short Story About Impossible Choices in Iraq |Nathan Bradley Bethea |August 31, 2014 |DAILY BEAST 

You wanted to be upstream with the artists, not downstream with the consumers. Apple Wants Beats So It Can Control You |Ted Gioia |May 12, 2014 |DAILY BEAST 

That's a dam downstream aways en the B-line waters a couple o' hundred acres. David Lannarck, Midget |George S. Harney 

Then, retrieving their net, they once more fastened Raf into it and turned downstream, as intent as ever upon reaching the sea. Star Born |Andre Norton 

Downstream a yellow spot broke out like a candle flame against black velvet. The Hidden Places |Bertrand W. Sinclair 

Archie Lawanne came back downstream with two grizzly pelts, and Hollister met Bland for the first time. The Hidden Places |Bertrand W. Sinclair 

But when he turned and set out on a run for that shake cabin four hundred yards downstream, Lawanne followed at his heels. The Hidden Places |Bertrand W. Sinclair