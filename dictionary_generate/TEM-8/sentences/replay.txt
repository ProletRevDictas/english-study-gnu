Verizon plans to add superfast 5G mobile service to 28 NFL stadiums this year so that fans the teams playing can more easily watch video replays and see multiple camera angles on plays. Verizon to expand fast 5G to NFL stadiums, music venues, and UPS delivery drones |Aaron Pressman |January 12, 2021 |Fortune 

Wims just had made a 28-yard catch on the previous play after a successful instant replay challenge by the Bears. Saints advance to set up a Drew Brees-Tom Brady showdown in second round |Mark Maske |January 11, 2021 |Washington Post 

What came as a surprise to the authors wasn’t that adding replay to an algorithm boosted its ability to retain its previous trainings. How a Memory Quirk of the Human Brain Can Galvanize AI |Shelly Fan |September 28, 2020 |Singularity Hub 

In a way, replay provides us with additional simulated learning trials to practice our learnings and stabilize them into a library of memories from which new experiences can build upon rather than destroy. How a Memory Quirk of the Human Brain Can Galvanize AI |Shelly Fan |September 28, 2020 |Singularity Hub 

During a replay of the 2014 Arsenal versus Hull City game, the video had 400,000 YouTube views, three-quarters of which were live views, thanks to the Arsenal football club amplifying the stream. ‘The audience is still there’: How U.K. sports outlets pivoted to branded content, social video to keep fans engaged as seasons were delayed |Lucinda Southern |July 29, 2020 |Digiday 

The agony of being so close to our goal but failing gnaws at our insides while we replay the events over and over in our heads. Heart of Darkness: Into Afghanistan’s Taliban Valley |Matt Trevithick, Daniel Seckman |November 15, 2014 |DAILY BEAST 

Too much of it seems to have been a replay on a larger scale of an incident back in May 2013. Mexico’s First Lady of Murder Is on the Lam |Michael Daly |October 29, 2014 |DAILY BEAST 

Video replay of the crash shows Chilton weaving his way through an exploding tide of debris. Prince Harry at Silverstone Grand Prix To See Lewis Hamilton Win |Tom Sykes |July 7, 2014 |DAILY BEAST 

You can go back to rewinding your TiVos to replay that scene over and over again now. Inside True Blood’s Steamy Gay Sex Scene |Kevin Fallon |June 30, 2014 |DAILY BEAST 

It makes sense that Democrats would want the coming election to be a replay of the last one. Win Women, Win the Midterms |Kristen Soltis Anderson |April 15, 2014 |DAILY BEAST 

Senator Crane, a doggedly determined man, had listened to the replay of Brent Taber's top-secret conference again and again. Ten From Infinity |Paul W. Fairman 

It was a replay of the old, 11th century, religious fragmentation. After the Rain |Sam Vaknin 

My replay was, alluding to our fight in the river, "How did N'yamyonjo's men fare?" The Discovery of the Source of the Nile |John Hanning Speke