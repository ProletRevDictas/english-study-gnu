Such a view, which itself must reduce the burden of suffering, is also not in conflict with the use of medication to treat the symptoms. How colonialism and capitalism helped place a stigma on mental illness |Balaji Ravichandran |February 12, 2021 |Washington Post 

With the disclaimer that you need to talk to your doctor before taking any kind of medication, the short answer is that it’s okay to take over-the-counter pain relievers after being vaccinated. Will over-the-counter medications affect doses of the coronavirus vaccines? | |February 9, 2021 |Washington Post 

Keep taking your allergy medications as your doctor prescribed, and if you have an EpiPen, bring it to your appointment just in case. How to prepare for getting the COVID-19 vaccine |Tara Santora |February 8, 2021 |Popular-Science 

Angela in New YorkWith the disclaimer that you need to talk to your doctor before taking any kind of medication, the short answer is that it’s okay to take over-the-counter pain relievers after being vaccinated. Can OTC pain relievers reduce vaccine effectiveness? It might depend on when you take them. |Angela Fritz |February 3, 2021 |Washington Post 

Moore was not believed to have been vaccinated against the virus because of the medication he was receiving for the pneumonia, British media reported. Capt. Tom Moore dies after covid diagnosis. The 100-year-old raised millions for Britain's NHS. |Jennifer Hassan, William Booth |February 2, 2021 |Washington Post 

Medication can now be taken in a single pill rather than a complex cocktail of tablets. The New Face of HIV Is Gay & Young |Adebisi Alimi |December 1, 2014 |DAILY BEAST 

As in, we have no idea why this medication seems to help people with bipolar disorder. Mother’s Little Anti-Psychotic Is Worth $6.9 Billion A Year |Jay Michaelson |November 9, 2014 |DAILY BEAST 

What if she were one of the rare sufferers of unending pain, and every safe medication had already been given? U.K. Courts Grant Mother Right to End Her 12-Year-Old Disabled Daughter’s Life |Elizabeth Picciuto |November 4, 2014 |DAILY BEAST 

They collected money for helmets, bulletproof vests, medication and even vehicles. Corruption Eats Away at Ukraine Military |Charles McPhedran |October 21, 2014 |DAILY BEAST 

The 71-year-old had not taken his heart medication, and he suffered a heart attack. Vogue Photographer Erwin Blumenfeld: Secrets of a Fashion Legend |Tim Teeman |September 14, 2014 |DAILY BEAST 

The greater portion of our life consists in devising means and medication to relieve us of our states of ill health and disease. Tyranny of God |Joseph Lewis 

In this day of self-medication this condition is all the greater menace to the diabetic. The Propaganda for Reform in Proprietary Medicines, Vol. 1 of 2 |Various 

The drainage tubes and medication tubes came out; the tangle of wires around him was removed, and the electrodes with them. Space Viking |Henry Beam Piper 

At any rate, the consequences of the medication were most disastrous. Neuralgia and the Diseases that Resemble it |Francis E. Anstie 

It may sadly want topical medication; but how is he to apply it? Cooley's Cyclopdia of Practical Receipts and Collateral Information in the Arts, Manufactures, Professions, and Trades..., Sixth Edition, Volume I |Arnold Cooley