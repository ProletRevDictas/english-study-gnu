The sales clerk just laughed, telling him he was out of bullets and had only six guns in his inventory. Fearing violence and political uncertainty, Americans are buying millions more firearms |Hannah Denham, Andrew Ba Tran |February 4, 2021 |Washington Post 

Some clerks are prominent in conservative media, and among law professors. Ginni Thomas apologizes to husband’s Supreme Court clerks after Capitol riot fallout |Robert Barnes |February 2, 2021 |Washington Post 

Later, clerks published smaller, single sheets or annual subscriptions that Londoners would carry with them as part of the daily news. Covid-19 dashboards are vital, yet flawed, sources of public information |Jacqueline Wernimont |January 26, 2021 |Washington Post 

In Chula Vista, a city with a significantly larger budget than National City and Imperial Beach, the clerk simply tells the mayor and Council members how many comments were received and gives them a brief overview of their contents. Chula Vista Police Chief Says She Didn’t Know Department Shared Data With Feds |Gustavo Solis |January 20, 2021 |Voice of San Diego 

As a high-ranking clerk, Howard appears to have been involved in policy. ‘Purity’ was one of Hollywood’s most controversial films. Meet its D.C. writer. |John Kelly |January 11, 2021 |Washington Post 

Judge Hinkle said “the Constitution requires the Clerk to issue such licenses.” The Back Alley, Low Blow-Ridden Fight to Stop Gay Marriage in Florida Is Finally Over |Jay Michaelson |January 5, 2015 |DAILY BEAST 

Bob Cratchit, the clerk who is the father of Tiny Tim and who meekly serves Scrooge, is paid fifteen shillings a week. How Dickens and Scrooge Saved Christmas |Clive Irving |December 22, 2014 |DAILY BEAST 

As a way to be more available to needy souls outside the church, Williams took a clerk job at Walgreens pharmacy. Exposed: The Gay-Bashing Pastor’s Same-Sex Assault |M.L. Nestel |December 21, 2014 |DAILY BEAST 

On October 5, 2013, Wilson pointed a pistol at a postal clerk. Post Office Robbers More Wanted Than ISIS |M.L. Nestel |December 13, 2014 |DAILY BEAST 

But instead of talking to us and resolving the issue, or getting a manager involved, the clerk calls the cops. The Day I Used Eric Garner’s Voice |Joshua DuBois |December 5, 2014 |DAILY BEAST 

“You appear to feel it so,” rejoined Mr. Pickwick, smiling at the clerk, who was literally red-hot. The Posthumous Papers of the Pickwick Club, v. 2(of 2) |Charles Dickens 

The office of clerk of the court is about to be sold, having been placed at fifteen hundred pesos. The Philippine Islands, 1493-1898, Volume XX, 1621-1624 |Various 

Hilda suggested that the ticket-clerk should be interrogated, but the aperture of communication with him was shut. Hilda Lessways |Arnold Bennett 

My father took me to the office in which I was to make a start and presented me to the chief clerk. Fifty Years of Railway Life in England, Scotland and Ireland |Joseph Tatlow 

Except the chief clerk, whose salary was about £160, I do not believe there was another whose pay exceeded £100 a year. Fifty Years of Railway Life in England, Scotland and Ireland |Joseph Tatlow