I had seen pictures before of the Temple, with its conical towers set on top of a mountain. Fighting Back With Faith: Inside the Yezidis’ Iraqi Temple |Michael Luongo |August 21, 2014 |DAILY BEAST 

But Amy has scant hope Madonna will send her conical brassieres to her shop for cleaning and repair. Madonna, My Neighbor |Ralph Gardner, Jr. |April 22, 2009 |DAILY BEAST 

When in the city they wore a rough felt conical hat and dark blue cotton robe. Our Little Korean Cousin |H. Lee M. Pike 

The head and thorax are of the colour of the wings, their sides and the conical abdomen being rather lighter. Narrative of a Survey of the Intertropical and Western Coasts of Australia] [Volume 2 of 2] |Phillip Parker King 

Jack was painting squares of alternate black and white on a buoy of a conical shape. The Floating Light of the Goodwin Sands |R.M. Ballantyne 

Later, the square tower was crowned with an octagonal turret, sometimes with a conical roof, as in Cremona and Modena cathedrals. Encyclopaedia Britannica, 11th Edition, Volume 5, Slice 2 |Various 

In the 16th century an octagonal lantern in two strings crowned with a conical roof was added. Encyclopaedia Britannica, 11th Edition, Volume 5, Slice 2 |Various