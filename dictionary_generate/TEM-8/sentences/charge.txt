I ask that we reach out to Corporation Council and ask them to deny the request based on the fact that the case is still active, as it is currently being investigated for possible criminal charges to be brought forth by the AG’s office. Officials Worked To Deliberately Delay Release Of Daniel Prude Body Cam Footage |mharris |September 17, 2020 |Essence.com 

It suggests that he doesn’t take any responsibility for deaths in the states that have a different political party in charge. Trump says US Covid-19 deaths would be low if you excluded blue states. That’s wrong. |German Lopez |September 17, 2020 |Vox 

Cohen had already pleaded guilty to those charges, so they ended up standing. The stunning hypocrisy of Bill Barr |Andrew Prokop |September 17, 2020 |Vox 

Schutzenhofer, the club’s general manager, did not respond to questions about the charges. Trump’s businesses charged Secret Service more than $1.1 million, including for rooms in club shuttered for pandemic |David Fahrenthold, Josh Dawsey |September 17, 2020 |Washington Post 

I was able to top up my charge by almost 50 percentage points in half an hour. Apple Watch Series 6 first impressions: A stretchy addition looks great |Aaron Pressman |September 17, 2020 |Fortune 

That is a fact recorded by the doctor in charge of the ambulance at the inquest. Harry’s Daddy, and Diana’s ‘Murder’: Royal Rumors In a New Play |Tom Sykes |January 4, 2015 |DAILY BEAST 

The added charge for access to hotel Wi-Fi is not only exploitative but increasingly irrelevant. How ‘Ethical’ Hotel Chain Marriott Gouges Guests in the Name of Wi-Fi Security |Kyle Chayka |December 31, 2014 |DAILY BEAST 

This was very blunt and surprising to hear from any official in charge of an aviation disaster. Did Bad Weather Bring Down AirAsia 8501? |Clive Irving |December 29, 2014 |DAILY BEAST 

I was convicted a year later and sentenced to death—a charge later overturned by the Supreme Court when it called for a retrial. An American Marine in Iran’s Prisons Goes on Hunger Strike |IranWire |December 18, 2014 |DAILY BEAST 

As late as the fifth century, powerful aristocratic women took charge of the commemoration of the dead in Rome. First Anglican Woman Bishop A Return to Christian Roots |Candida Moss |December 18, 2014 |DAILY BEAST 

He was voluble in his declarations that they would “put the screws” to Ollie on the charge of perjury. The Bondboy |George W. (George Washington) Ogden 

The mother's lips could not finish the charge she was about to put upon her innocent child. The Pastor's Fire-side Vol. 3 of 4 |Jane Porter 

There was a time when Aristide Pujol, in sole charge of an automobile, went gaily scuttering over the roads of France. The Joyous Adventures of Aristide Pujol |William J. Locke 

Louis was not less astonished at this charge, than the Empress had been at the communication which aroused it. The Pastor's Fire-side Vol. 3 of 4 |Jane Porter 

The Countess drew a beautiful miniature from its case, which lay on the sofa near her, and presented it to her young charge. The Pastor's Fire-side Vol. 3 of 4 |Jane Porter