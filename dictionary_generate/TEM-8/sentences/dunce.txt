Cooking rice on the stovetop can make you feel like a dunce in the kitchen. Best rice cooker for perfect rice every time |Cemile Kavountzis |October 13, 2021 |Popular-Science 

This time, Super Duper Loser Kevin Dopart suggested perhaps a dunce cap, or a jester’s hat, with the title “Clowning Achievement.” Style Conversational Week 1413: Our new not-so-big top -- the Clowning Achievement trophy |Pat Myers |December 3, 2020 |Washington Post 

But such branding ostracizes that behavior, like the film should be forced to wear a dunce cap and publicly shamed in the corner. Guardians of the Galaxy’s Chris Pratt Is the Everydude Superhero |Kevin Fallon |August 1, 2014 |DAILY BEAST 

The penalty for a miss is the same as in the sprint (Dunce Lap). Why the Biathlon Makes Bonds of Us All |Brett Singer |February 16, 2014 |DAILY BEAST 

Like the pursuit, there are four stops for shooting, but in lieu of a Dunce Lap each miss adds a full minute to your total time. Why the Biathlon Makes Bonds of Us All |Brett Singer |February 16, 2014 |DAILY BEAST 

If you miss you have to hit the 150m penalty loop, or as I like to call it, the Dunce Lap. Why the Biathlon Makes Bonds of Us All |Brett Singer |February 16, 2014 |DAILY BEAST 

There are two shooting stops, one prone, one standing, Dunce Lap penalty for misses. Why the Biathlon Makes Bonds of Us All |Brett Singer |February 16, 2014 |DAILY BEAST 

Pedantic, unimaginative and presumptuous, Theobald was the logical choice for a Dunce King in 1728. A Letter from Mr. Cibber to Mr. Pope |Colley Cibber 

Tyndale himself, who invented such beautiful words in his translations, was the first to use the word dunce. Stories That Words Tell Us |Elizabeth O'Neill 

I'm afraid from this you'll guess that I must have been a dunce at school myself. The Nicest Girl in the School |Angela Brazil 

Tutors I could get by shoals, but a fellow-dunce is inestimable.' The Heir of Redclyffe |Charlotte M. Yonge 

He had no mercy on a fool or a dunce, and turned in disgust from those who loved trifles and lies. Beacon Lights of History, Volume VI |John Lord