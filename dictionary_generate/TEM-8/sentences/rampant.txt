Mail-in ballots — and elections generally — are subject to rampant fraud. Five falsehoods spurring Republican concern about the election |Philip Bump |October 15, 2020 |Washington Post 

Over the past week, young Nigerians have ramped up widespread protests—online and offline—against rampant brutality by local police. How a youth-led digital movement is driving Nigeria’s largest protests in a decade |Yomi Kazeem |October 13, 2020 |Quartz 

It spoke to the fact that there’s a ton of rampant speculation right now bordering on conspiracy theorizing, when the truth is that we don’t really know much. Our misinformation problem is about to get much, much worse |Sean Illing |October 6, 2020 |Vox 

Various Prospect facilities in California have had bedbugs in patient rooms, rampant water leaks from the ceilings and what one hospital manager acknowledged to a state inspector “looks like feces” on the wall. Investors Extracted $400 Million From a Hospital Chain That Sometimes Couldn’t Pay for Medical Supplies or Gas for Ambulances |by Peter Elkind with Doris Burke |September 30, 2020 |ProPublica 

Misinformation about voting, the election, and both candidates for the presidency is already rampant on Facebook and every other media platform, and it's being spread by actors both foreign and domestic. Facebook’s plan to prevent election misinformation: Allowing it, mostly |Kate Cox |September 3, 2020 |Ars Technica 

With prescription drug abuse rampant in the U.S., New York is taking steps to stop it. No More Paper Prescriptions: Docs Fight Fraud by Going Electronic |Dale Eisinger |December 18, 2014 |DAILY BEAST 

They want to take control of New York City schools away from Mayor Bill de Blasio and let privatization run rampant. Hunger Games Comes to New York State’s Public Schools |Zephyr Teachout |November 26, 2014 |DAILY BEAST 

How this will all shake out is a topic of rampant speculation. High Rents Are Killing the Restaurant Capital |Will Doig |October 28, 2014 |DAILY BEAST 

When Booker assumed office, his half-a-million constituents were grappling with high unemployment and rampant poverty. The Ugly Truth About Cory Booker, New Jersey’s Golden Boy |Olivia Nuzzi |October 20, 2014 |DAILY BEAST 

With under-reporting rampant, the real numbers are probably much higher. In Sierra Leone, the Plague Is Closing in Around Us |Ned Eustace |October 13, 2014 |DAILY BEAST 

And a rampant ache in my head, seconded by a medium-sized gash in the scalp, didn't make for an access of optimism at that moment. Raw Gold |Bertrand W. Sinclair 

The tumult and license which usually characterise a general election were more than ordinarily rampant and intolerant. The History of England in Three Volumes, Vol.III. |E. Farr and E. H. Nolan 

It is however but a cheap substitute for authority, and belongs of right to a rampant jingoism of a past age. A Cursory History of Swearing |Julian Sharman 

"Tommy Atkins" was the rage for the moment, and what may be called "Absent-minded Beggarism" was rampant. The Relief of Mafeking |Filson Young 

There is no evidence before the Government that a widespread conspiracy is rampant in the West of Ireland. Is Ulster Right? |Anonymous