“Violence towards this community was truly extensive and indiscriminate in terms of age and sex,” says biological anthropologist Marta Mirazón Lahr of the University of Cambridge. Hunter-gatherers first launched violent raids at least 13,400 years ago |Bruce Bower |May 27, 2021 |Science News 

That injury pattern more likely arose from periodic, indiscriminate raids rather than a single battle, in which the dead would have consisted mainly of male fighters, the researchers say. Hunter-gatherers first launched violent raids at least 13,400 years ago |Bruce Bower |May 27, 2021 |Science News 

The criminals’ tactics have evolved from indiscriminate “spray and pray” campaigns seeking a few hundred dollars apiece to targeting specific businesses, government agencies and nonprofit groups with multimillion-dollar demands. The Colonial pipeline ransomware hackers had a secret weapon: self-promoting cybersecurity firms |Renee Dudley, Daniel Golden |May 24, 2021 |MIT Technology Review 

“We have to have the police officers that we need, and they have to be able to do their jobs,” the mayor said, expressing worry about recommendations from a commission to change police tactics to avoid indiscriminate stops by police. Woman, child shot in dispute over scooter remain in critical condition, D.C. police say |Peter Hermann, Emily Davies |May 19, 2021 |Washington Post 

The videos that emerged, taken by courageous residents, are too harrowing to watch, showing summary executions of already surrendered, apprehended or wounded suspects, along with indiscriminate shooting. A Deadly Police Raid in Rio Show How Bolsonaro's Policies Are Wreaking Havoc in Brazil |David Miranda |May 8, 2021 |Time 

The pro-Russian separatists and their allies inside Russia have become indiscriminate with some of their heavy weapons. MH17 Black Box ‘Sent to Moscow for Investigation’ |Anna Nemtsova |July 17, 2014 |DAILY BEAST 

Or did a band of reactionary theocrats put their progressive brethren to the indiscriminate sword? Don't Let the Maccabees Win |Matt Lerner |December 4, 2013 |DAILY BEAST 

College rankings are ubiquitous, confusing, and often indiscriminate. The Daily Beast’s Down + Dirty Guide to Colleges Methodology | |October 16, 2013 |DAILY BEAST 

Their fire was indiscriminate at best, targeting civilians at worst. Neocon Scholar: Keep Bloodying The Brotherhood |Ali Gharib |August 16, 2013 |DAILY BEAST 

For decades, mainstream Palestinian thought has regarded the indiscriminate killing of Jews as legitimate. Hazineh Calls for the Murder of Jews and the Mainstream Press Stays Silent |Martin Krossel |August 15, 2013 |DAILY BEAST 

A few continuing to fire after the main body had surrendered, an indiscriminate slaughter ensued. The Every Day Book of History and Chronology |Joel Munsell 

The mangled bodies were hurried to the catacombs, and thrown into an indiscriminate heap of corruption. Madame Roland, Makers of History |John S. C. Abbott 

Our two friends, however, found indiscriminate joy in everything; I have their letters to prove it. Jaffery |William J. Locke 

On the other hand, the beginner finds himself using words that have lost, their meaning through indiscriminate usage. English: Composition and Literature |W. F. (William Franklin) Webster 

The progress of the fusion of races is shown by the lists of names, which are both Saxon and Norman in indiscriminate order. The Influence and Development of English Gilds |Francis Aiden Hibbert