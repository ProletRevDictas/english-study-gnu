The world’s largest virtual currency by market capitalisation—bitcoin—is the most traded cryptocurrency in India. Bitcoin is still a clear favourite among India’s crypto investors |Prathamesh Mulye |September 15, 2020 |Quartz 

The team didn’t do much of anything to address any of these flaws at last month’s trade deadline, under the theory that the existing talent on hand would turn it around eventually. The Nationals Are Running Out Of Time |Neil Paine (neil.paine@fivethirtyeight.com) |September 11, 2020 |FiveThirtyEight 

Home chefs are trading intel on who makes the best adobo and why Old Bay is good for more than just crab boils. The Redemption of the Spice Blend |Jaya Saxena |September 10, 2020 |Eater 

In fact, they alienated Rodgers by trading up to draft another quarterback in the first round instead of bringing on receiving help for him. What To Watch For In An Abnormally Normal 2020 NFL Season |Neil Paine (neil.paine@fivethirtyeight.com) |September 9, 2020 |FiveThirtyEight 

Lehner, who was acquired by Vegas from Chicago ahead of the NHL’s trade deadline in February, has earned the majority of his team’s starts throughout the postseason. An Expensive Goalie Is Not Your Ticket To The Stanley Cup |Julian McKenzie |September 9, 2020 |FiveThirtyEight 

Its graceful hotels and beautiful restaurants are totally dependent on the tourist trade. Is Putin Turning to Terrorism in Ukraine? |Anna Nemtsova |January 6, 2015 |DAILY BEAST 

Dance instructors run a lucrative trade offering private lessons to couples before their wedding receptions, typically the tango. Iran’s Becoming a Footloose Nation as Dance Lessons Spread |IranWire |January 2, 2015 |DAILY BEAST 

Rebels in Africa trade in children to fund their conflicts and obtain child soldiers. ISIS, Boko Haram, and the Growing Role of Human Trafficking in 21st Century Terrorism |Louise I. Shelley |December 26, 2014 |DAILY BEAST 

The Canterbury Tales was, Strohm writes, “one of the volumes around which the new trade would organize itself.” A Year In The Life of The Canterbury Tales’ Storied Beginnings |Wendy Smith |December 25, 2014 |DAILY BEAST 

There was really only one good reason to maintain the embargo: Trade with Cuba strengthens the Castros. Obama’s One Hand Clap With Castro |Doug McIntyre |December 24, 2014 |DAILY BEAST 

The result of the restoration of trade, banking, and credit to earlier and more normal conditions has been steadily apparent. Readings in Money and Banking |Chester Arthur Phillips 

The doctrine of international free trade, albeit the most conspicuous of its applications, was but one case under the general law. The Unsolved Riddle of Social Justice |Stephen Leacock 

But they have tied their credit system in the bonds of narrow banking laws and their trade in those of a cramping tariff. Readings in Money and Banking |Chester Arthur Phillips 

So far we have not made great progress in securing Europe's Latin-American trade. Readings in Money and Banking |Chester Arthur Phillips 

Soon after its cultivation began in France, Spain, and Portugal, the tobacco trade was farmed out. Tobacco; Its History, Varieties, Culture, Manufacture and Commerce |E. R. Billings.