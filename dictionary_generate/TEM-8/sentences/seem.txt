The story of fluoridation reads like a postmodern fable, and the moral is clear: a scientific discovery might seem like a boon. Anti-Fluoriders Are The OG Anti-Vaxxers |Michael Schulson |July 27, 2016 |DAILY BEAST 

Again, the difference can seem subtle and sound more like splitting hairs, but the difference is important. How Skinny Is Too Skinny? Israel Bans ‘Underweight’ Models |Carrie Arnold |January 8, 2015 |DAILY BEAST 

To make it work almost everything else about these shows has to seem factual which is why many look like a weird Celebrity Sims. ‘Empire’ Review: Hip-Hop Musical Chairs with an Insane Soap Opera Twist |Judnick Mayard |January 8, 2015 |DAILY BEAST 

Many of those who have become cops in New York seem to have ceased to address such minor offenses over the past few days. Shot Down During the NYPD Slowdown |Michael Daly |January 7, 2015 |DAILY BEAST 

A recent U.S. study found men get a “daddy bonus” —employers seem to like men who have children and their salaries show it. How Good Dads Can Change the World |Gary Barker, PhD, Michael Kaufman |January 6, 2015 |DAILY BEAST 

She was growing a little stout, but it did not seem to detract an iota from the grace of every step, pose, gesture. The Awakening and Selected Short Stories |Kate Chopin 

Accordingly, the question "How far does the note issue under the new system seem likely to prove an elastic one?" Readings in Money and Banking |Chester Arthur Phillips 

A good many children seem to be like savages in distinguishing those to whom one is bound to speak the truth. Children's Ways |James Sully 

I do not know what I think; all my thoughts seem whirling round as leaves do in brooks in the time of the spring rains. Ramona |Helen Hunt Jackson 

They don't seem to think there would be much good gained by begging for special favours through routine channels. Gallipoli Diary, Volume I |Ian Hamilton