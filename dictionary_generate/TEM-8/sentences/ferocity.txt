Since then, though, streamers say hate raids have only grown in number and ferocity. Twitch hate raids are more than just a Twitch problem, and they’re only getting worse |Nathan Grayson |August 25, 2021 |Washington Post 

In India, the ferocity of the second wave left hospitals too full to treat the sick. These twins are 5 years old. They lost both parents to covid-19. |Joanna Slater |June 17, 2021 |Washington Post 

Jones’s short stories, she writes, “remind us — so gently it is easy to overlook their underlying ferocity — that we are all just tiny figures inside the sweep of an often violent history.” ‘B-Side Books’ adds to your must-read stack with the best books you’ve never heard of |Abby McGanney Nolan |June 10, 2021 |Washington Post 

Cloud providers can, and do, protect their customers’ data with the same ferocity as they protect their own. The rise of cybersecurity debt |Ram Iyer |June 4, 2021 |TechCrunch 

Many have assumed that this is a newfound feminist ferocity, but from ancient Queen Pwa Saw, to the first woman surgeon Daw Saw Sa, who qualified in 1911, Myanmar women have always been as strong as, if not stronger than, our men. Myanmar's Women Are Fighting for a New Future After a Long History of Military Oppression |MiMi Aye |June 1, 2021 |Time 

Phone lines would catch fire from the velocity and ferocity of his words. David Garth, the Consultant Who Talked Up to Voters |Jeff Greenfield |December 15, 2014 |DAILY BEAST 

He was just seamlessly being this person—the ferocity and intensity was incredible. The Brit Who Stormed Broadway |Tim Teeman |December 7, 2014 |DAILY BEAST 

But when protests in Syria turned to civil war in June 2011, that activism took on a new ferocity. The Kardashian Look-Alike Trolling for Assad |Noah Shachtman, Michael Kennedy |October 17, 2014 |DAILY BEAST 

They are clashing with Israeli police with a ferocity not seen since the second Intifada. The Seeds of the Next Intifada |Jesse Rosenfeld |July 7, 2014 |DAILY BEAST 

The German Panzers fought with suicidal ferocity, storming the hill until it was rimmed with a bulwark of bodies. The Deadly Trap Behind D-Day’s Beaches |Clive Irving |June 5, 2014 |DAILY BEAST 

Some peculiar lines between these contracted brows gave a character of ferocity to this forbidding and sensual face. Checkmate |Joseph Sheridan Le Fanu 

By this illustration the native ferocity of the eighteenth-century caricaturists is glaringly exemplified. The Portsmouth Road and Its Tributaries |Charles G. Harper 

The eyes had something of the ferocity but also the fidelity of a well-trained watch-dog. A Butterfly on the Wheel |Cyril Arthur Edward Ranger Gull 

His intensely black eyes, blacker even than the eyes of Coronado, had a stare of absolutely indescribable ferocity. Overland |John William De Forest 

There was hardly a face among that gang of wild riders which did not outdo the face of Texas Smith in degraded ferocity. Overland |John William De Forest