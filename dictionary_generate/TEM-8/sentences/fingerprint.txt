Red pigment marks shaped like human fingerprints dot the inside of the shell, near its wide opening where someone trimmed the edge. Humans made a horn out of a conch shell about 18,000 years ago |Bruce Bower |February 10, 2021 |Science News 

If we look closely at the images produced by telescopes, we see the fingerprints of quantum mechanics. A curious observer’s guide to quantum mechanics, pt. 4: Looking at the stars |Ars Staff |January 31, 2021 |Ars Technica 

Smudges left by their fingerprints may not be your only touch screen issue. Best 2 in 1 laptops: Work hard, play hard with these versatile picks |PopSci Commerce Team |January 27, 2021 |Popular-Science 

Although the four Afghans had names that were similar to those of known Taliban fighters who had left DNA or fingerprints on bombs used to attack Americans, they weren’t same men or they weren’t Taliban, according to the Army. An unusual mission in Afghanistan, and the troops who suffered to carry it out |Greg Jaffe |January 15, 2021 |Washington Post 

Citizens were asked to collect evidence, take photographs and process fingerprints. Maryland police officer dies after being struck by car |Dan Morse |January 14, 2021 |Washington Post 

Five days later authorities were able to match a fingerprint at the post office to Wilson sending him to an early retirement. Post Office Robbers More Wanted Than ISIS |M.L. Nestel |December 13, 2014 |DAILY BEAST 

In the end he was left with a disappointing product that bore little of his creative fingerprint. David Fincher’s Backseat Feminism |Teo Bugbee |October 9, 2014 |DAILY BEAST 

When they subsequently managed to fingerprint her, she seemed to have outdone Dillinger. The Mystery Woman Who Tried to Outdo Dillinger |Michael Daly |September 29, 2014 |DAILY BEAST 

But the Social Security people required an FBI fingerprint check. The Mystery Woman Who Tried to Outdo Dillinger |Michael Daly |September 29, 2014 |DAILY BEAST 

As with the fingerprint scanner of the iPhone 5s, the larger screen means that the phone will cost more. iWatch, iPhone 6, and More: What Apple Has Up Its Sleeve |Kyle Chayka |September 7, 2014 |DAILY BEAST 

Luckily, we've got a way to tell if you've got it right: beneath the key will be a much shorter number, called the 'fingerprint'. Little Brother |Cory Doctorow 

On it, in huge white luminous letters, were her public key and her fingerprint and email address. Little Brother |Cory Doctorow 

Some inks are more soluble in the solution used for fingerprint tests than others? Warren Commission (7 of 26): Hearings Vol. VII (of 15) |The President's Commission on the Assassination of President Kennedy 

“They all forget something,” said Fosdick, as Drew hurried out through the door with a bow toward the staring fingerprint man. Whispering Wires |Henry Leverage 

Matching of trace impurities is often called a “fingerprint” method. The Atomic Fingerprint |Bernard Keisch