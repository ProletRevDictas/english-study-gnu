The issue is about more than just the heightened risk of a sudden death tilting the balance of power, or a senile leader making important decisions. Should Aging Government Leaders Have to Pass Cognitive Tests to Serve? |Charlotte Alter |October 21, 2021 |Time 

You realize the person's become hopelessly senile, a fact you have to forgive if you ever want to enjoy your time with them. 'Glee' 100th Episode: The Sad Ballad of an Elderly Trainwreck |Kevin Fallon |March 19, 2014 |DAILY BEAST 

The General in command of the station was a feeble old man, suffering from senile decay. The Red Year |Louis Tracy 

He bewailed the loss of his automobile that had perished of senile decay at Aix-en-Provence. The Joyous Adventures of Aristide Pujol |William J. Locke 

A sickening revolt seized him, aggravated by the smiles of the old woman, who dipped and courtesied before him in senile delight. The Circular Study |Anna Katharine Green 

The sight of his senile weariness flashed the irony of the whole wild dream into Valmond's mind. When Valmond Came to Pontiac, Complete |Gilbert Parker 

To Average Jones' inquiring gaze on this summer day it opposed the secrecy of a senile indifference. Average Jones |Samuel Hopkins Adams