Magistrates sit in judgment on cases involving petty thefts, drunken driving, domestic violence, assaults and disorderly conduct. Facing New Complaint, South Carolina Magistrate Removed as County’s Chief Judge |by Joseph Cranney and Avery G. Wilks, The Post and Courier |July 9, 2021 |ProPublica 

He lost his license after a drunken-driving arrest in 2012, a bad decision that he’s still embarrassed about. Tyler Mahan Coe created the ‘War and Peace’ of country music podcasts. Surrender to it. |Geoff Edgers |April 15, 2021 |Washington Post 

The ruling, which lawyers involved said is the first of its kind in Virginia, applies only to the case of a man accused of violating court restrictions related to a drunken driving conviction. Ruling challenges Va.’s tradition of judges, police officers prosecuting misdemeanor cases |Justin Jouvenal |April 9, 2021 |Washington Post 

I’m told it feels like a Catholic wake where we think about the good times — the tailgating, a drunken celebration of life – and the bad times — the devastating art of loss the Chargers perfected. Environment Report: The Chargers Stadium’s Circle of Life |MacKenzie Elmer |March 29, 2021 |Voice of San Diego 

The Real World is notorious for its conflicts—which, by the turn of the millennium, tended to mean drunken physical altercations between shirtless, bellowing muscle bros or sorority types in bandage dresses pulling each other’s hair out. The Real World Homecoming: New York Is a Bittersweet Glimpse at Everything Reality TV Could've Been |Judy Berman |March 4, 2021 |Time 

Think of it as Game of Thrones—if you subtract the sex and violence and add drunken revelry and singing. ‘Galavant’: A Drunken, Horny Musical Fairy Tale |Melissa Leon |January 5, 2015 |DAILY BEAST 

And we do mean drunken—in the keep your kids at home, pull the shades kind of drunken. Before the Bros, SantaCon Was as an Anti-Corporate Protest |David Freedlander |December 12, 2014 |DAILY BEAST 

Instead, it reminded me of a headline that once ran in The Onion: “Drunken Man Makes Interesting Point About Society.” Karen Armstrong’s New Rule: Religion Isn’t Responsible for Violence |Patricia Pearson |October 29, 2014 |DAILY BEAST 

“Adam and I rented a pink Cadillac and spent a super drunken weekend at Graceland about 20 years ago,” says Thompson. ‘Archer’ Season 6 Preview: Cast and Crew on Rebranding and Dropping ISIS |Marlow Stern |October 27, 2014 |DAILY BEAST 

And Hitler looking like such a lout, a drunken lout, with that sort of ignorant sneer. Martin Amis Talks About Nazis, Novels, and Cute Babies |Ronald K. Fried |October 9, 2014 |DAILY BEAST 

Major Foster hastily collected sixty men and charged on the guns—so shamelessly abandoned by the order of a drunken commander. The Courier of the Ozarks |Byron A. Dunn 

The speech came to an abrupt end when, losing her balance, she fell to the ground, and lay there in drunken contentment. The Garret and the Garden |R.M. Ballantyne 

The drunken Man staggered to his feet, and hiccupped vehemently in the face of the assembled Gods. Kipling Stories and Poems Every Child Should Know, Book II |Rudyard Kipling 

By the old law a drunken man who made a contract was still liable, and required to fulfill as a penalty for his conduct. Putnam's Handy Law Book for the Layman |Albert Sidney Bolles 

And at that moment Sangree arrived wrapped in a blanket and carrying his gun; he was still drunken with sleep. Three More John Silence Stories |Algernon Blackwood