This 12-pack of 8-ounce cold brew cans comes in four varieties, including nitro and double espresso. Great coffee beans for cold brew |PopSci Commerce Team |September 15, 2020 |Popular-Science 

You can set the machine to brew any of its four drink size options, and when you’re finished it automatically shuts itself off nine seconds after the last brew. The best quick-brew coffee machines |PopSci Commerce Team |September 10, 2020 |Popular-Science 

Most days, the morning fog carries the smell of the lightning-sparked fires, an acrid brew of millions of incinerated trees and buildings now burning across California. How to breathe easier in America’s Smoke Belt |Michael J. Coren |September 9, 2020 |Quartz 

Located on the corner of W 12th Street and Central Parkway in Cincinnati’s Historic Over the Rhine neighborhood, it is a unique one-of-a-kind place to enjoy a local craft brew and relax with friends. Cincinnati – A Big City with a Small Town Feel |LGBTQ-Editor |August 17, 2020 |No Straight News 

However, that home brew “didn’t do much for us puny humans either.” Why elephants and armadillos might easily get drunk |Susan Milius |June 4, 2020 |Science News For Students 

And that solution came from a homemade brew Branch and her sister created together. Goodbye To A Natural Hair Guru: Miss Jessie's Cofounder Titi Branch Dead At 45 |Danielle Belton |December 16, 2014 |DAILY BEAST 

I paid a visit Istmo Brew Pub and found their beers undrinkable. House of the Witch: The Renegade Craft Brewers of Panama |Jeff Campagna |November 30, 2014 |DAILY BEAST 

In 2005, Istmo Brew Pub opened as the first of its kind in Panama City. House of the Witch: The Renegade Craft Brewers of Panama |Jeff Campagna |November 30, 2014 |DAILY BEAST 

A boiling brew is scooped into makeshift bowls—hollowed out coconut halves. Bye Bye Latté, Hello Guayusa: Why The Amazon Holds the Secret to a Cleaner, Healthier Caffeine |Brandon Presser |August 29, 2014 |DAILY BEAST 

Ayahuasca/yagé is a psychoactive brew made from the Banisteriopsis caapi vine. Spirit Tripping With Colombian Shamans |Chris Allbritton |August 24, 2014 |DAILY BEAST 

"And I can return the compliment," was my reply, as we all gathered round a brew of tea to exchange news and compare notes. Three More John Silence Stories |Algernon Blackwood 

Now, in the kitchen, a great searching among jars and boxes on high shelves told of preparation for the occasional brew. The Dragon Painter |Mary McNeil Fenollosa 

“If you will excuse me for a few moments, I will make myself fit to brew you some tea,” said my wife, holding open the door. The Idyl of Twin Fires |Walter Prichard Eaton 

She was stirring the pot diligently, now and then sprinkling in what looked like a brown dust, and watching the brew intently. When Valmond Came to Pontiac, Complete |Gilbert Parker 

As a rule, one or two studies would club together to brew, instead of preparing solitary banquets. The Gold Bat |P. G. Wodehouse