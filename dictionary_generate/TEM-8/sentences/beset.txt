In a suburb beset by racial inequities, lawmakers aren’t sure. In Maryland’s largest suburb, an ambitious police overhaul faces roadblocks |Rebecca Tan, Dan Morse |February 4, 2021 |Washington Post 

Yet because this outfit has been so beset by so many off-the-field issues for so long, the progress off the field right now is as important. Washington’s front office looks nothing like it ever has before. That’s a reason for hope. |Barry Svrluga |February 4, 2021 |Washington Post 

Cohen’s takeover of the Mets from the Wilpon family was cast as a new era for a franchise often beset by dysfunction and internal intrigue. Mets fire GM Jared Porter after ESPN details lewd, harassing texts to female reporter |Dave Sheinin, Cindy Boren |January 19, 2021 |Washington Post 

Procedural fixes helped prevent the problems that had beset the 1800 election. The election that foreshadowed 2020 |James Morone |January 11, 2021 |Washington Post 

There were reports that Project Titan had been beset by internal strife and management disagreements over exactly what kind of car to make and how far toward autonomous driving the company should push. Is Apple teaming up with Hyundai on a self-driving, electric car? Don’t bet on it |Jeremy Kahn |January 8, 2021 |Fortune 

I suspect that the new Tarzan will run into the same problems that beset the recent Lone Ranger remake. Can Tarzan of the Apes Survive in a Post-Colonial World? |Ted Gioia |November 23, 2014 |DAILY BEAST 

We may be beset by deep political divisions, but the moderate, outward-looking center is large; not a fringe section of society. Beirut Letter: In Lebanon, Fighting ISIS With Culture and Satire |Kim Ghattas |September 22, 2014 |DAILY BEAST 

Stringer depicts a country that is beset by complications of both past imperialism and current globalism. This Week’s Hot Reads: Feb. 24, 2014 |Thomas Flynn |February 24, 2014 |DAILY BEAST 

India, for example, is still very poor and beset by many ethnic and religious divides, yet has a thriving democracy. Why Democracy Will Struggle In The Arab World In 2014 |Balint Szlanko |January 12, 2014 |DAILY BEAST 

The downstairs portion of the Democratic base is increasingly beset by debt and disillusion. The GOP’s Backdoor Impeachment Scheme |Lloyd Green |October 14, 2013 |DAILY BEAST 

He was beset by his sensitive dislike to mix in other people's affairs, but almost angrily he overcame it. Bella Donna |Robert Hichens 

But while everything seemed so favourable the crafty Gascon from the first foresaw the dangers which beset his path. Napoleon's Marshals |R. P. Dunn-Pattison 

But children came and died too quickly for her health and fragile beauty, and the storms of life beset her. Ancestors |Gertrude Atherton 

An armed force beset the palace of the Tuilleries, and demanded the arrest of the Brissotine party. The Every Day Book of History and Chronology |Joel Munsell 

He was extremely anxious to make no mistakes at the outset of his new career, beset with difficulties enough. Ancestors |Gertrude Atherton