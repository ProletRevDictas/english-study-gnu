Even weirder, the source of these proteins relies on viral genes domesticated eons ago by our own genome through evolution. Surprise! Our Bodies Have Been Hiding a Trojan Horse for Gene Therapy |Shelly Fan |August 24, 2021 |Singularity Hub 

Unlike their domesticated counterparts, the wolf puppies spent 12 to 24 hours a day in human care from about 10 days after birth up to and throughout the testing period. Puppies beat out young wolves in one important skill |Monroe Hammond |July 26, 2021 |Popular-Science 

Until now, SibFox was the closest anyone in the US had gotten to receiving a domesticated fox. Can I have a pet fox? |empire |July 1, 2021 |Popular-Science 

In fact, dogs are such great friends that humans probably domesticated them not once, but twice. Let’s learn about dogs |Bethany Brookshire |May 4, 2021 |Science News For Students 

As he did, he began a one-man crusade to scour the planet collecting crop varieties that were disease resistant and might also shed light on the evolution of domesticated plants. The Botanist Who Defied Stalin - Issue 99: Universality |Lee Alan Dugatkin |April 21, 2021 |Nautilus 

Humans spent a long time domesticating cattle, and what they were trying to do, in essence, was de-domesticate them. ‘Nazi Cows’ Tried to Kill British Farmer |Tom Sykes |January 6, 2015 |DAILY BEAST 

As Sandra Bullock has found out, any attempt to domesticate them will end in a resounding failure. Enough with the Overalls! |Sean Macaulay |April 7, 2010 |DAILY BEAST 

By marginalizing certain political tendencies, the European approach makes it harder to domesticate them. How Glenn Beck Saves Lives |Reihan Salam |June 19, 2009 |DAILY BEAST 

I know a pretty woman from a plain one, I hope, even though I dont personally want to domesticate the recording angel. The Romance of His Life |Mary Cholmondeley 

The hunter is thought to have been seized, one fine day, with an impulse to domesticate animals instead of hunting them. Elements of Folk Psychology |Wilhelm Wundt 

His place was well named for he was a great horticulturist, the first to domesticate the Catawba grape. A Portrait of Old George Town |Grace Dunlop Ecker 

They have now begun to domesticate certain species of Meliponas, by introducing them into earthen pots or wooden cases. The Insect World |Louis Figuier 

They seem somewhat like the buffalo and other wild animals that we have never been able to domesticate. Seventy Years on the Frontier |Alexander Majors