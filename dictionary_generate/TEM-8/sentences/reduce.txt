Facebook is also today reminding users of its rules to reduce the spread of groups tied to violence. Facebook tries to clean up Groups with new policies |Sarah Perez |September 17, 2020 |TechCrunch 

So in addition to reducing emissions at home, we need to make it likelier that those countries will reduce their emissions, too. Want to fight climate change effectively? Here’s where to donate your money. |Sigal Samuel |September 17, 2020 |Vox 

Google announced at the start of September it would reduce the visibility of search terms shown in reports for advertisers. How much does Google’s new search term filtering affect ad spend transparency? Here’s how to find out |Frederick Vallaeys |September 16, 2020 |Search Engine Land 

Now Israel can claim it has more friends in the region, possibly reducing the pressure on it regarding its relations with Palestinians. Trump announces that Bahrain will normalize relations with Israel |Alex Ward |September 11, 2020 |Vox 

In an interview with VOSD’s Scott Lewis this week, a top aide to Mayor Kevin Faulconer noted that the estimate now includes a 45 percent contingency and could be reduced as general contracting firm Kitchell digs in further. Morning Report: Downtown Jail Outbreak Goes From Bad to Worse |Voice of San Diego |September 11, 2020 |Voice of San Diego 

Having a criminal record can reduce the likelihood of getting a callback or job offer by 50 percent. His First Day Out Of Jail After 40 Years: Adjusting To Life Outside |Justin Rohrlich |January 3, 2015 |DAILY BEAST 

We kind of reduce things to the lowest common denominator, in some ways for good and in some ways not for good. Daphne Merkin on Lena Dunham, Book Criticism, and Self-Examination |Mindy Farabee |December 26, 2014 |DAILY BEAST 

Heat the rum in a small skillet over medium until reduce by half. Carla Hall’s Christmas Day Treat: Rum Balls |Carla Hall |December 25, 2014 |DAILY BEAST 

Like background check laws across the country, it will help keep guns out of dangerous hands, reduce gun crime, and save lives. The NRA’s Twisted List for Santa |John Feinblatt |December 23, 2014 |DAILY BEAST 

The studio took him at his word and jumped at the chance to close down, or at least reduce, his costly operation. Alfred Hitchcock’s Fade to Black: The Great Director’s Final Days |David Freeman |December 13, 2014 |DAILY BEAST 

However, in the visit that I shall make in this archbishopric, I shall try to reduce them to as few settlements as possible. The Philippine Islands, 1493-1898, Volume XX, 1621-1624 |Various 

He will rattle on in Spanish till Herr S. gets desperate, and tries to reduce him to order. Music-Study in Germany |Amy Fay 

This would reduce the available time for direct manual labour at his disposal. Antonio Stradivari |Horace William Petherick 

I will take care to reduce the weight if possible, so as to be carried on the backs of mules. Life of Richard Trevithick, Volume II (of 2) |Francis Trevithick 

But a bank can retain a dividend that has been declared to reduce the indebtedness of the owner to the bank for his stock. Putnam's Handy Law Book for the Layman |Albert Sidney Bolles