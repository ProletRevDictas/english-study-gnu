“It’s 2021 and we are still fighting for bits and pieces of equality,” Prince wrote. NCAA Accused of 'Disrespectful' Treatment Towards Women's March Madness Teams in Viral TikTok |Megan McCluskey |March 19, 2021 |Time 

MBS, as the prince is known, “viewed Khashoggi as a threat to the Kingdom,” the report asserts, and “broadly supported using violent measures if necessary to silence him.” Here's Why the U.S. Didn't Sanction Mohammed bin Salman for His Role in the Jamal Khashoggi Killing |W.J. Hennigan |March 4, 2021 |Time 

The prince will also no longer serve as captain general of the Royal Marines, a role passed to him from his grandfather, Prince Philip, who had held the post for 64 years. Prince Harry and Meghan lose their patronages, won’t return as ‘working royals’ |William Booth, Karla Adam |February 19, 2021 |Washington Post 

“We’ve been hearing from a number of publishers in Europe that they want to make sure they’re tracking as little as possible, while still trying to understand how their content is being received,” Prince said. Cloudflare’s privacy crusade continues with a challenge to one of Google’s big data sources |David Meyer |September 29, 2020 |Fortune 

“There’s going to be a substantial opportunity for companies to create the tools to allow different types of computing paradigms to exist to support all the different regulatory regimes that are out there,” Prince said. Cloudflare’s privacy crusade continues with a challenge to one of Google’s big data sources |David Meyer |September 29, 2020 |Fortune 

The new claims present numerous big problems for Prince Andrew. Buckingham Palace Disputes Sex Allegations Against Prince ‘Randy Andy’ |Tom Sykes |January 4, 2015 |DAILY BEAST 

Prince may have pranced around like a carefree libertine onstage, but in rehearsal he was more drill sergeant than sprite. Speed Read: The Juiciest Bits From the History of ‘Purple Rain’ |Jennie Yabroff |January 1, 2015 |DAILY BEAST 

Prince George and his parents are enjoying their first Christmas in their magnificent country home. Prince George’s Christmas: Better Than Yours |Tom Sykes |December 24, 2014 |DAILY BEAST 

And in a big departure from established royal protocol, Prince George might even get a—gasp—present to open on Christmas Day. Prince George’s Christmas: Better Than Yours |Tom Sykes |December 24, 2014 |DAILY BEAST 

The biggest misfire here, though, was the notion that anyone would believe that this dude looked at all like Prince Harry. The Biggest Bombs of 2014: ‘Sex Tape,’ Mariah Carey’s Vocals, ‘How I Met Your Mother’ and More |Kevin Fallon |December 19, 2014 |DAILY BEAST 

And it would be hard indeed, if so remote a prince's notions of virtue and vice were to be offered as a standard for all mankind. Gulliver's Travels |Jonathan Swift 

At the end of the campaign the Emperor justly rewarded his lieutenant by creating him Prince of Wagram. Napoleon's Marshals |R. P. Dunn-Pattison 

He professed both to abominate and despise all mystery, refinement, and intrigue, either in a prince or a minister. Gulliver's Travels |Jonathan Swift 

You will follow the suite of my daughter to Spain, and you will become the bosom Counsellor of the wife of your Prince? The Pastor's Fire-side Vol. 3 of 4 |Jane Porter 

Thereon the major-general took on himself to nominate Prince Eugne as Murat's successor. Napoleon's Marshals |R. P. Dunn-Pattison