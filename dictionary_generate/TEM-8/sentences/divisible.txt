The task is easily divisible into portions, so you always know exactly what fraction of the task is left. How High Should You Climb Up The Tower? |Zach Wissner-Gross |July 1, 2022 |FiveThirtyEight 

The same is true for the National Archives, Office of Presidential Personnel and Office of Personnel Management who are buried in years easily divisible by four. How To Swap Presidents Without an Insurrection |Philip Elliott |January 20, 2022 |Time 

Mathematicians want to understand the sizes of collections of vertices with other numeric properties in common — like large groups of vertices, none of which is connected to a number of other vertices that’s evenly divisible by 3 or 5. Mathematicians Answer Old Question About Odd Graphs |Kevin Hartnett |May 19, 2021 |Quanta Magazine 

You may recognize that they’re all prime — evenly divisible only by themselves and 1 — but these particular primes are even more unusual. Mathematicians Find a New Class of Digitally Delicate Primes |Steve Nadis |March 30, 2021 |Quanta Magazine 

In this case, you can use the tricks you learned in school for determining if a number is divisible by a given digit. Celebrating the Playful Magic of John Horton Conway |Pradeep Mutalik |October 15, 2020 |Quanta Magazine 

It turns out that 60 is a wonderful number because it is divisible by one, two, three, four, five, and six. How Long Is a Year? Is the Earth Slowing Down? And Other Questions About Time |Neil Shubin |January 6, 2013 |DAILY BEAST 

The mere fact that it divides itself, or imparts itself to others, shows that it was already divisible before the division. Plotinos: Complete Works, v. 3 |Plotinos (Plotinus) 

Every year of which the figure is divisible by four is a leap-year. Astronomy for Amateurs |Camille Flammarion 

Still another pretty stitch, easily adjusted to any garment, is as follows: Chain a number of stitches divisible by 3, turn. Handbook of Wool Knitting and Crochet |Anonymous 

In Denmark the peat deposits of this age are divisible into five layers, characterised by different dominant forms of trees. The Principles of Stratigraphical Geology |J. E. Marr 

The largest dock is divisible by a central caisson so that four ships can be docked at one time. Encyclopaedia Britannica, 11th Edition, Volume 11, Slice 8 |Various