Development of an AR headset at Apple seems to have hit a snag or two under current project lead Mike Rockwell, though the report does not outline exactly which obstacles have emerged. Apple hardware chief Dan Riccio stepped down to focus on AR/VR |Samuel Axon |February 8, 2021 |Ars Technica 

I babied mine and still saw a few small snags while bushwhacking. Do You Actually Need a 1,000-Fill Down Jacket? |Ryan Wichelns |December 23, 2020 |Outside Online 

It was inspiring to see how our customers reacted when we’d hit snags, like a shipping delay. This new sneaker brand is meant for health care workers |Rachel King |December 6, 2020 |Fortune 

The snag is that the nonmedical part of the world has to buy in, too. New guidance on brain death could ease debate over when life ends |Laura Sanders |August 10, 2020 |Science News 

By May, those efforts seemed to hit a snag when the Housing Commission decided not to move forward with 10 hotel properties it had initially eyed. Morning Report: The Seditious Language Law’s Origin Story |Voice of San Diego |August 5, 2020 |Voice of San Diego 

He was 19, and managed to snag a summer internship with New Line Cinema. ‘Mockingjay’s’ Mastermind: Francis Lawrence on the Book vs. Movie, ISIS Parallels, and More |Marlow Stern |November 23, 2014 |DAILY BEAST 

The wire is long gone, but a rusted snag remains entombed in the bark. How the Kings of Fracking Double-Crossed Their Way to Riches |ProPublica |March 13, 2014 |DAILY BEAST 

This is a snag because Chan lives across the border, where the Hong Kong Dollar is used. Inside China's Underground Black Market Banks |Brendon Hong |February 26, 2014 |DAILY BEAST 

Over the last four years, however, the process has hit a snag. As GOP Senators Block Obama’s Nominees, Democrats Prepare ‘Nuclear Option’ |Jamelle Bouie |May 30, 2013 |DAILY BEAST 

Luz gets away and hires Malone to take her over the border, where Thacker and others are waiting to snag her. This Week’s Hot Reads: May 20, 2013 |Cameron Martin, Jessica Ferri, Jimmy So |May 20, 2013 |DAILY BEAST 

We made an extra steering-oar, too, because one of the others might get broke on a snag or something. Adventures of Huckleberry Finn, Complete |Mark Twain (Samuel Clemens) 

Imbedded in this bar was a long white snag, a tree trunk whose naked arms, thrusting far down stream, had literally impaled us. The Way of a Man |Emerson Hough 

After that I waded back to the snag carefully, and once more ordered the young woman to come to me. The Way of a Man |Emerson Hough 

At length I crept to the snag and beat against it with my cane. Birds of the Rockies |Leander Sylvester Keyser 

Down the road arose sharp words of command, and the burning top of a tall pine snag threw its light upon bayonets in the highway. An Arkansas Planter |Opie Percival Read