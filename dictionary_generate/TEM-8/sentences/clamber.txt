The paleontologists reached it via an hour-long clamber through an underground river. The challenge of dinosaur hunting in deep caves |John Pickrell |May 19, 2020 |Science News For Students 

Just gaining entry was difficult, and in many cases firefighters had to clamber up and lower themselves through windows. Amazing Grace in the Bronx: Inside the Metro-North Train-Wreck Rescue |Michael Daly |December 2, 2013 |DAILY BEAST 

We get down on all fours and clamber along a 40-foot fallen log. Exercising Like a Caveman: A.J. Jacobs Gets Primal |A.J. Jacobs |April 10, 2012 |DAILY BEAST 

We had now at one moment to wade through plains of sand, and the next to clamber over the rocks by wretched paths. A Woman's Journey Round the World |Ida Pfeiffer 

They were still more surprised when they saw a number of men clamber out from under the float. Stories of Our Naval Heroes |Various 

To clamber over the tender into the adjacent waggon was a simple matter. The Story of the Cambrian |C. P. Gasquoine 

He took up a heavy walking-stick, and started to clamber down out of the buggy. Cursed |George Allan England 

At last both clamber slowly to an eminence where a long steel pipe has been erected. The Home of the Blizzard |Douglas Mawson