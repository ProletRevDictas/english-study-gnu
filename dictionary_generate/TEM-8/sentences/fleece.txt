This looks like a normal crewneck sweater, but it’s made from Polartec Power Air, a fleece material that sheds few fibers and moves easily when layered under other garments. How to Layer for Outdoor Dining This Fall and Winter |Wes Siler |October 14, 2020 |Outside Online 

Often called microplastics, these fibers come from washing fleece and nylon fabrics. Washing your jeans too much might pose risks to the environment |Sharon Oosthoek |October 12, 2020 |Science News For Students 

This soft, snuggly, ankle-length fleece robe might help—it’s made from 330 GSM microfiber fleece to warm you up. Clothing and accessories that make great gifts |PopSci Commerce Team |October 6, 2020 |Popular-Science 

This fleece has been my go-to cozy layer after crisp fall runs. The Gear Our Editors Loved in September |The Editors |October 4, 2020 |Outside Online 

I gave up on fleeces a few years ago, because most brands made theirs too thin, so they weren’t very warm and eventually wore out. 7 of Our Favorite Men's and Women's Fall Layers |Jakob Schiller |October 3, 2020 |Outside Online 

I lie and nod my head yes while wiping the tears on my gray fleece sleeve. You’re Never ‘Cured’ of an Eating Disorder |Carrie Arnold |December 20, 2014 |DAILY BEAST 

But rather than receive practical assistance from the WWP, he got a branded fleece beanie. Wounded Warrior Project Under Fire |Tim Mak |September 26, 2014 |DAILY BEAST 

They could be pajama bottoms, sweats, fleece kind of things. Chang-rae Lee: How I Write |Noah Charney |January 22, 2014 |DAILY BEAST 

He tweeted this picture of himself giving Harry a 'royal fleece'. Prince Harry's U.S. Tour: Rolling Updates and all the News and Pictures as It Happens |Tom Sykes |May 15, 2013 |DAILY BEAST 

But he soon discovers that his newfound clout came with a fleece attached. Chris Christie: What Lap Band? |Caitlin Dickson |May 8, 2013 |DAILY BEAST 

Watch took charge of it at once, pressing his warm body against the frosty fleece, and licking its face and feet to warm them. The Nursery, December 1881, Vol. XXX |Various 

Dealers in wool, acting as selling agents for owners, and buying agents for fleece merchants of Berry. Repertory Of The Comedie Humaine, Complete, A -- Z |Anatole Cerfberr and Jules Franois Christophe 

Then he shoved the weapon into Denton's hand, and hurried him over the shingle with the remark, 'Now chuck off the fleece, Peter. Menotah |Ernest G. Henham 

In the sheep, it may be returned in its fleece, its carcass, or its progeny; and in the swine only by its progeny and flesh. Domestic Animals |Richard L. Allen 

Fig. 20 is a spirited cut of a variety of the Merino without dewlap, and with a long and somewhat open fleece. Domestic Animals |Richard L. Allen