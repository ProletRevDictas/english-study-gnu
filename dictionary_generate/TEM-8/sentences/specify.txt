If you’re following a recipe and there’s a broiling step included, it should specify where in the oven to put your pan. Don’t Fear the Broiler |Elazar Sontag |February 11, 2021 |Eater 

It has not specified what information the passport will contain, nor has it even issued a call for tenders yet. Why Denmark’s “corona passport” is more of a promise than a plan |Bobbie Johnson |February 10, 2021 |MIT Technology Review 

However, this understates the true burden attributed to police because some towns didn’t specify how the total was divided among their employees. How We Found Pricey Provisions in New Jersey Police Contracts |by Agnes Chang, Jeff Kao and Agnel Philip, ProPublica, and Andrew Ford, Asbury Park Press |February 8, 2021 |ProPublica 

Moor said he didn’t have a problem with officers receiving gold badges, but he felt the contract should specify a size and price. How the Police Bank Millions Through Their Union Contracts |by Andrew Ford, Asbury Park Press, and Agnes Chang, Jeff Kao and Agnel Philip, ProPublica |February 8, 2021 |ProPublica 

However, the department has refused to specify what those changes might entail. Hawaii’s Beaches Are Disappearing. New Legislation Could Help ... if It’s Enforced. |by Sophie Cocke, Honolulu Star-Advertiser |February 7, 2021 |ProPublica 

He declined to specify which details had raised this concern. Fired From Los Alamos for Pushing Obama's Nuclear Agenda |Center for Public Integrity |July 31, 2014 |DAILY BEAST 

Carney declined to specify how the decision to indict Chinese officials publicly was coordinated with the White House. #ShotsFired in U.S.-China Cyberwar |Jacob Siegel, Josh Rogin |May 20, 2014 |DAILY BEAST 

Miftakhov did not specify what ‘things’ were and (the officer) did not ask him to specify. Pennsylvania Student Proves You Could Buy Ingredients for a WMD on Amazon |Michael Daly |January 28, 2014 |DAILY BEAST 

People blindly following instructions may not realize they have microwaves with a wattage lower than the instructions specify. Be Afraid of Your Food: An Epidemiologist’s Sensible Advice |Amanda Kludt |March 16, 2013 |DAILY BEAST 

But I would like to see someone specify how far we could cut. How Much Can We Cut Defense? |Megan McArdle |January 10, 2013 |DAILY BEAST 

Yet he forebore to specify his injuries; saying, that to name them, would be to stigmatize the whole human race. The Pastor's Fire-side Vol. 3 of 4 |Jane Porter 

There were plenty of ladylike things girls could do, she said, but did not give herself the trouble to specify. Those Dale Girls |Frank Weston Carruth 

I need only specify Miss Ross and friend, name unknown—to assure you of the high respectability of the assemblage. Alone |Marion Harland 

They did not specify the “it,” but they were quite convinced of the oddity. The Mystery of a Turkish Bath |E.M. Gollan (AKA Rita) 

In conclusion, since everybody likes to have a feast now and then, I specify that my diet regimen allows for holidays. The Book of Life: Vol. I Mind and Body; Vol. II Love and Society |Upton Sinclair