The points which seem to mark the ictus, or rhythmical accent, are found on the first syllable of each of these two feet. The Modes of Ancient Greek Music |David Binning Monro 

And in saying this I am not misled by the points of resemblance in the rhythmical accompaniment of these dances. Frederick Chopin as a Man and Musician |Frederick Niecks 

His speeches have a freedom and a rhythmical flow which captivate the hearer. The Contemporary Review, January 1883 |Various 

Deepest in his nature, the most innate of all his faculties, was the faculty of song, of rhythmical utterance. Beacon Lights of History, Volume XIII |John Lord 

She walked with rhythmical step, her gloved hands hidden in her muff, her eyes downcast, as though she were immersed in thought. The conquest of Rome |Matilde Serao