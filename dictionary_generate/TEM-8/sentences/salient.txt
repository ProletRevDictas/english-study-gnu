Perhaps the most salient point regarding Davis is that he could stay healthy and atop the Falcons’ depth chart and still put his fantasy managers at a deficit. Yes, the running back dead zone in fantasy drafts is real. And you should be afraid. |Des Bieler |August 24, 2021 |Washington Post 

Those two ingredients—foam and plate—are also the salient features of the Vaporfly and comparable marathon shoes, but there are some differences. The Science of Track and Field’s New Super Spikes |mmirhashem |July 16, 2021 |Outside Online 

Dopamine doesn’t just make us feel good, it also serves the purpose of telling our brain what is “salient.” Meditation isn’t always calming. For a select few, it may lead to psychosis. |Claire Maldarelli |June 21, 2021 |Popular-Science 

As our nation continues to open, we can hope that tempers will cool, tensions will ease, and a person’s positions on the virus will become less salient to their partisan identity. How Can We Escape the COVID-19 Vaccine Culture Wars? |David French |June 8, 2021 |Time 

The software was designed to estimate which part of a photo would be considered most “salient” or important to see first and was trained with human eye-tracking data, Twitter said. Twitter drops automated image-cropping tool after determining it was biased |Dalvin Brown |May 20, 2021 |Washington Post 

And, with an estimated 70 million gay people in China, there has never been a more salient time to turn the corner. China’s Gay Hook-Up App Is a Cash Cow |Charlotte Lytton |November 6, 2014 |DAILY BEAST 

He is also known as El H, but of all his nicknames El Elegante is the most salient. Trading Dime Bags for Salvador Dali |Jason McGahan |October 19, 2014 |DAILY BEAST 

One salient example was Ilyas Kashmiri, one of the highest-ranking al Qaeda commanders. Al Qaeda’s Desperation Could Be India’s Nightmare |Tunku Varadarajan |September 6, 2014 |DAILY BEAST 

But in the aggregate, immigration reform is not a salient issue for white evangelicals. Even Conservative Evangelical Support Couldn’t Save Immigration Reform |Jacob Lupfer |July 6, 2014 |DAILY BEAST 

When I sit down someday to write my memoirs and try to characterize this era, I will note three salient political features. Karl Rove May Be Evil, but He’s No Genius |Michael Tomasky |May 15, 2014 |DAILY BEAST 

She is skilful in seizing salient characteristics, and her chief aim is to preserve the individuality of her sitters and models. Women in the fine arts, from the Seventh Century B.C. to the Twentieth Century A.D. |Clara Erskine Clement 

His head leaned towards her with its salient thrust, its poise of impetus and forward flight. The Creators |May Sinclair 

It may not be unwise to review the grounds that we have been going over, and to glance at the salient points. Landholding In England |Joseph Fisher 

But General Lee abandoned the salient after dark, and put his whole force in the base line. Historic Fredericksburg |John T. Goolrick 

In this salient, now known as the “Bloody Angle,” occurred one of the most terrible hand-to-hand conflicts of modern warfare. Historic Fredericksburg |John T. Goolrick