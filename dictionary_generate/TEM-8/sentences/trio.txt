According to the announcement, Apple became aware of a trio of security issues that may have been “actively exploited” before the patch. You should update your iPhone and Chrome browser ASAP |Stan Horaczek |February 8, 2021 |Popular-Science 

The chosen trio were Alicia Scott of Range Beauty, Arah Sims of Kyutee Nails, and Tomi Alisha of NaturAll Club. Cantu Beauty Announces Winners of Elevate Workshop Series |Nandi Howard |February 5, 2021 |Essence.com 

Without Thompson to round out the winningest trio in NBA history,13 Golden State isn’t likely to be considered a title contender this season, but no team will want to face Green, Curry and company in the playoffs. Draymond Green Isn’t Scoring, But He’s Doing Everything Else For The Warriors |James L. Jackson |February 4, 2021 |FiveThirtyEight 

To mark the honor, “Lions in Winter” showcases work by the trio, whose styles are distinct but altogether compatible. In the galleries: Immersive exhibit explores a wonderland in blue |Mark Jenkins |January 22, 2021 |Washington Post 

Understandable for a team with a new coach and a new star trio that has faced some injury issues but not good enough. With Kyrie Irving back, rookie coach Steve Nash has to figure out how to get his stars aligned |Ben Golliver |January 21, 2021 |Washington Post 

The trio formed the Sad Boys collective, with Sherm and Gud on production and Lean manning the mic. The Cult of Yung Lean: ‘I’m Building An Anarchistic Society From the Ground Up’ |Marlow Stern |January 4, 2015 |DAILY BEAST 

On Wednesday afternoon, the trio turned themselves in to the police. The Monuments Men of Occupy Hong Kong |Brendon Hong |December 4, 2014 |DAILY BEAST 

Before she headlined the Playboy resorts, Joan Rivers was the funny gal in a spirited trio that played in hip nightclubs. Joan Rivers: The Playboy Bunnies Weren’t Sluts! |Patty Farmer |November 7, 2014 |DAILY BEAST 

The Playboy publicity posters for the act called them “a spirited trio.” Joan Rivers: The Playboy Bunnies Weren’t Sluts! |Patty Farmer |November 7, 2014 |DAILY BEAST 

Additionally, the so-called DDG-1000 can also carry either a pair of helicopters or a single helicopter and a trio of drones. Can the Navy's $12 Billion Stealth Destroyer Stay Afloat? |Dave Majumdar |October 22, 2014 |DAILY BEAST 

When Christian had completed his work, the trio returned to headquarters, Mr. Peck leaving again to "do a little thinking." Hooded Detective, Volume III No. 2, January, 1942 |Various 

Then in 1823 she made her debut at the Opera in a trio skit with Mariette and Tullia. Repertory Of The Comedie Humaine, Complete, A -- Z |Anatole Cerfberr and Jules Franois Christophe 

The Scherzo is neither good nor bad; the trio is so innocent that it would be almost too infantile for a Sniegourotchka. The Life &amp; Letters of Peter Ilich Tchaikovsky |Modeste Tchaikovsky 

Ten minutes later, after the sergeant had changed into plain clothes, the trio set out along the flat, muddy road for Asheldham. The Doctor of Pimlico |William Le Queux 

Xenophon is the last of the trio of the Greek historians whose writings are classic and inimitable. Beacon Lights of History, Volume I |John Lord