So if this bears out, the more people who are vaccinated in a community, including children, the more likely transmission will drop overall. Fauci: Vaccines for Kids as Young as First Graders Could Be Authorized by September |by Caroline Chen |February 11, 2021 |ProPublica 

McMillin said fast-tracking the platform project did not raise the project’s overall budget. Metro seeks bond sales to raise $360 million for capital projects |Justin George |February 10, 2021 |Washington Post 

Google’s Danny Sullivan said that if manual actions for News or Discover won’t impact your overall Google Search performance. Google might have to subsidize journalism, but not like this… |George Nguyen |February 10, 2021 |Search Engine Land 

Over the summer, as cases surged, so did Republican concern — but Americans overall were 20 points more likely to express concern than were Republicans. What you’re saying when you say that covering the coronavirus is partisan |Philip Bump |February 9, 2021 |Washington Post 

For instance, the startup’s games have been downloaded 250 million times overall since the creation of the company in 2018. Hyper casual game publisher Homa Games raises $15 million |Romain Dillet |February 9, 2021 |TechCrunch 

Music is a huge part of the tone of Black Dynamite overall—going back to the original 2009 movie on which the series is based. ‘Black Dynamite’ Presents Police Brutality: The Musical |Stereo Williams |January 9, 2015 |DAILY BEAST 

According to James, by 2014 his business did close to $2 million in overall sales. Career-Minded Women Turn to Male Escorts For No-Strings Fun and (Maybe) Sex |Aurora Snow |January 3, 2015 |DAILY BEAST 

Are you more pessimistic about the overall public education crisis given this current environment? Dr. Howard Fuller's Injustice Education |Campbell Brown |December 21, 2014 |DAILY BEAST 

Overall, Paris Magnum reaches both too widely and too thinly in trying to convey a sense of spectrum. A History of Paris in 150 Photographs |Sarah Moroz |December 14, 2014 |DAILY BEAST 

But overall the legal system has done little to say “this was against the law.” The U.S. Will Torture Again—and We’re All to Blame |Michael Tomasky |December 12, 2014 |DAILY BEAST 

"Wait," said Chief Inspector Kerry, and went swinging in, carrying his overall and having the malacca cane tucked under his arm. Dope |Sax Rohmer 

He replaced the receiver, took up a wet oilskin overall from the back of a chair and the cane from the mantleshelf. Dope |Sax Rohmer 

It was ajar, and Kerry, taking an electric torch from his overall pocket, flashed the light upon the name-plate. Dope |Sax Rohmer 

He was slight, wore a workman's overall suit, and he had a lunch box under his arm. Hooded Detective, Volume III No. 2, January, 1942 |Various 

Rapidly he removed his reefer and his waistcoat, folded them, and placed them neatly beside his overall. Dope |Sax Rohmer