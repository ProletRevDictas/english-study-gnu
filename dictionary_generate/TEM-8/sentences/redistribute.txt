When the University of Maryland’s Student Government Association decided to redistribute more than $400,000 to support classmates during the pandemic, there were no excuses, no delays, no political arguments. U-Md. student government will distribute $400,000 to students facing hardships, mental health challenges |Lauren Lumpkin |December 3, 2020 |Washington Post 

Publishers such as NowThis, Complex and Insider mostly use TikTok as a place to syndicate and redistribute content they’ve made for other platforms. ‘A stamp of legitimacy’: TikTok turns up its branded content spending and profile with publishers |Max Willens |July 31, 2020 |Digiday 

She advocated Friday for redistributing the power such groups have accumulated. Morning Report: MTS Doled Out Violations Disproportionately |Voice of San Diego |July 27, 2020 |Voice of San Diego 

Currently, brands are massively redistributing their spending into CTV and OTT programmatic environments. Hottest user-centric video advertising trends of 2020: CTV, vertical, and social formats |Ivan Guzenko |June 12, 2020 |Search Engine Watch 

To avoid losses in online sales, 63% of respondents confirm their SEO budgets were redistributed. Digital marketing during COVID-19 times: Data-driven insights |Eugene Lata |June 10, 2020 |Search Engine Watch 

Any belly fat might redistribute itself in a more “womanly” way around the hips, thighs, and buttocks. Hormone Therapy Is More Than Just a Physical Process |Brandy Zadrozny |August 22, 2013 |DAILY BEAST 

Most people ally generosity on the side of fairness because fairness seeks to redistribute benefits and properties. Why Favoritism Is Virtuous: The Case Against Fairness |Stephen T. Asma |December 7, 2012 |DAILY BEAST 

They will use their majority to pillage the makers and redistribute to the takers. Fellow Conservatives, Ease Off the Doom and Gloom |David Frum |November 12, 2012 |DAILY BEAST 

Democrats could use a sunny day as an excuse to destroy the free market, redistribute income and pander to lobbyists. Coulter: Ok For Me, Not For You |David Frum |August 10, 2012 |DAILY BEAST 

Romney, on account of his wealthy personal life, has to tiptoe around policies that redistribute wealth upward. What's Wrong With Perry's Tax Plan |David Sessions |October 25, 2011 |DAILY BEAST 

Should we be forced to redistribute men and material on arrival, we are in for another spell of delay. Gallipoli Diary, Volume I |Ian Hamilton 

Ethan mustn't run away with the idea that the Tallmadge accumulations were only waiting for a lavish hand to redistribute. The Open Question |Elizabeth Robins 

This movement puzzled the newcomer until he suddenly realized that it was merely to redistribute the rouge on them. The Shadow |Arthur Stringer 

A general social re-organization is needed which will redistribute forces, immunize, divert and nullify. Human Nature and Conduct |John Dewey 

It is when we come to the question of the proportions in which we are to redistribute that controversy begins. Preface to Androcles and the Lion |George Bernard Shaw