I worked very loyally for him to do everything I could for him. Former Pence aide says she will vote for Biden because of Trump’s ‘flat out disregard for human life’ during pandemic |Josh Dawsey |September 17, 2020 |Washington Post 

Both are recovering well after their surgeries and are already back to work. A Welcome Lifeline |Washington Regional Transplant Community |September 17, 2020 |Washington Blade 

The NBCU spokesperson said the company would work with each advertiser to decide how the data would be used and managed. NBCUniversal tests new measurement program to prove it can push product sales for advertisers |Tim Peterson |September 17, 2020 |Digiday 

It works with the full-sized Smart Keyboard and the new Logitech keyboards. Apple just announced a new iPad, iPad Air, and Apple Watch Series 6 |Stan Horazek |September 15, 2020 |Popular-Science 

At Fortune, we’ve worked to make business better since our founding 90 years ago. Announcing Fortune Connect, our new membership community |Alan Murray |September 15, 2020 |Fortune 

If anything the work the two cops and the maintenance guy were doing deserves more respect and probably helped a lot more people. Trolls and Martyrdom: Je Ne Suis Pas Charlie |Arthur Chu |January 9, 2015 |DAILY BEAST 

Why, some might be asking, am I being so harsh on their work so soon after they died? Trolls and Martyrdom: Je Ne Suis Pas Charlie |Arthur Chu |January 9, 2015 |DAILY BEAST 

“I love my job and I love my city and I am committed to the work here,” he said in a statement. The Golden State Preps for the ‘Red Wedding’ of Senate Races |David Freedlander |January 9, 2015 |DAILY BEAST 

So it might be me projecting my desires onto Archer to want to just get away from work for a few weeks. ‘Archer’ Creator Adam Reed Spills Season 6 Secrets, From Surreal Plotlines to Life Post-ISIS |Marlow Stern |January 8, 2015 |DAILY BEAST 

To make it work almost everything else about these shows has to seem factual which is why many look like a weird Celebrity Sims. ‘Empire’ Review: Hip-Hop Musical Chairs with an Insane Soap Opera Twist |Judnick Mayard |January 8, 2015 |DAILY BEAST 

Sleek finds it far harder work than fortune-making; but he pursues his Will-o'-the-Wisp with untiring energy. The Pit Town Coronet, Volume I (of 3) |Charles James Wills 

With him one is at high pressure all the time, and I have gained a good many more ideas from him than I can work up in a hurry. Music-Study in Germany |Amy Fay 

In fact, except for Ramona's help, it would have been a question whether even Alessandro could have made Baba work in harness. Ramona |Helen Hunt Jackson 

The sad end of the mission to King M'Bongo has been narrated in the body of this work. The Pit Town Coronet, Volume I (of 3) |Charles James Wills 

Entrez donc, 'tis the work of one of your compatriots; and here, though a heretic, you may consider yourself on English ground. Blackwood's Edinburgh Magazine, Volume 60, No. 372, October 1846 |Various