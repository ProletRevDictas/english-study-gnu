The broad digital transformation taking place in R&D is allowing researchers to automate time-consuming manual processes and opening new research horizons in thorny problems that have failed to elicit breakthroughs. Digital innovation in the pharmaceuticals and chemicals industries |Jason Sparapani |January 26, 2021 |MIT Technology Review 

That’s concerning, but vaccinations may elicit a broader, more powerful immunity than a passing infection, so it’s impossible to say they won’t still work. We could know soon whether vaccines work against a scary new coronavirus variant |Antonio Regalado |January 23, 2021 |MIT Technology Review 

So it’s really engineered to elicit a protective immune response. Already had COVID-19? You still need a vaccine. |Kate Baggaley |January 15, 2021 |Popular-Science 

The direct listing came to the fore in 2018, when Spotify sought to tackle the first-day stock price pop that often comes with an IPO and elicits criticism that money is left on the table for the companies themselves. Roblox soars ahead of its public market debut |Lucinda Shen |January 7, 2021 |Fortune 

The demos are typically impressive, but somewhat clinical and usually elicit an endless stream of Terminator references in just about every blog post and internet comment surrounding them. The Boston Dynamics robots are surprisingly good dancers |Stan Horaczek |December 31, 2020 |Popular-Science 

With all due respect to his athletic skill, Gronkowski is not high on the list of NFL players that elicit carnal thoughts. ‘A Gronking to Remember’ Speed Read: 8 Naughtiest Bits |Emily Shire |January 7, 2015 |DAILY BEAST 

When a reporter asked him a question, it would often elicit a series of Jesuitical responses. President Cuomo Would’ve Been a Lion |Jonathan Alter |January 2, 2015 |DAILY BEAST 

One agent in particular developed a rapport with Zubaydah and managed to elicit an all-important bit of intelligence. The Luxury Homes That Torture and Your Tax Dollars Built |Michael Daly |December 12, 2014 |DAILY BEAST 

The Golden Globes TV nominations reliably elicit a WTF reaction. 15 Enraging Golden Globe TV Snubs and Surprises: Amy Poehler, 'Mad Men' & More |Kevin Fallon |December 11, 2014 |DAILY BEAST 

In the end, it was found that students working under Protess had used false pretenses in trying to elicit witness statements. Wrongly Imprisoned for 15 Years Thanks to an Innocence Project |Jacob Siegel |November 13, 2014 |DAILY BEAST 

There will be an amicable settlement; and my word will be a knot in the chain of satisfactory evidence they will elicit. Alone |Marion Harland 

Eleanor looked at Jane very sharply, but the sewing-girl's face was averted, so that questioning looks could elicit no answers. All He Knew |John Habberton 

Some of his walls are still pointed out, and the large stones he lifted elicit surprise. Yorkshire Oddities, Incidents and Strange Events |S. Baring-Gould 

This was said to elicit if there might be some variance in the statement of Lady Eleanor and her servant. The Knight Of Gwynne, Vol. II (of II) |Charles James Lever 

But, starting from that point, an exploration of hours failed to elicit the slightest trace. Forging the Blades |Bertram Mitford