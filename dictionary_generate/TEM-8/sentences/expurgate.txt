The resolutions of the loyalists were curiosities, and the secretary did not always expurgate bad spelling, etc. Civil War and Reconstruction in Alabama |Walter L. Fleming 

Why must Northern publishers expurgate and emasculate the literature of the world before it is permitted to reach them? The Atlantic Monthly, Volume 1, Issue 2, December, 1857 |Various 

The dreamer sees a worshipper—his wife—enter, to palliate or expurgate her soul of some ugly stain. The Browning Cyclopdia |Edward Berdoe 

His speech was two or three words longer, but they are inappropriate at the end of a chapter, and I expurgate. The Cavalier |George Washington Cable 

They would expurgate it from their vocabulary if they could. 'Boy Wanted' |Nixon Waterman