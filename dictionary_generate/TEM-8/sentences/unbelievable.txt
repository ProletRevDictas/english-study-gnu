There couldn’t have been a more challenging game than that, and he won in such unbelievable fashion. Doug Williams’s MVP performance still ranks among the greatest in Super Bowl history |Scott Allen |February 5, 2021 |Washington Post 

It was unbelievable to me that folks who call themselves patriots would actually be trying to overthrow the government and stop us from doing our constitutional duty to certify the ballots. Sen. Tammy Duckworth’s experiences as a soldier and wheelchair user shaped her response to the Capitol riots |Emma Hinchliffe |January 14, 2021 |Fortune 

This is just another consequence of a truly shocking and unbelievable day in American history. Coronavirus cases among lawmakers who sheltered in lockdown show one vaccine dose may not immediately protect against infection |Ben Guarino |January 13, 2021 |Washington Post 

Even if it was just an hour to Germany, the hour time difference, the going through airports on both sides led to unbelievable tiredness… just for a 60-minute presentation. ‘Flying isn’t all that necessary’: Grounded business execs express relief at suspension of non-stop travel |Jessica Davies |January 12, 2021 |Digiday 

“It feels really unbelievable,” said Roudabeh Kishi, director of research and innovation with the nonprofit Armed Conflict Location & Event Data Project. The Police’s Tepid Response To The Capitol Breach Wasn’t An Aberration |Maggie Koerth (maggie.koerth-baker@fivethirtyeight.com) |January 7, 2021 |FiveThirtyEight 

Animals jumped through unbelievable hoops and seemingly appeared out of thin air. How the Circus Got a Social Conscience |Justin Jones |November 7, 2014 |DAILY BEAST 

Manson seemed to breathe soundlessly, to walk with unbelievable silence over creaky floors. Gay Talese on Charlie Manson’s Home on the Range |Gay Talese |October 31, 2014 |DAILY BEAST 

Coltrane, a man of almost unbelievable gentleness made human to us lesser mortals by his very occasional rages. The Stacks: John Coltrane’s Mighty Musical Quest |Nat Hentoff |October 18, 2014 |DAILY BEAST 

And though the theology of Left Behind is strange and unbelievable, it also could, in the right hands, inspire a hell of a story. ‘Left Behind’ Review: Nicolas Cage’s Bible Movie Is God-Awful |Matthew Paul Turner |October 3, 2014 |DAILY BEAST 

“That is an unbelievable amount of money—as in, I literally did not believe that,” Oliver said. ‘Last Week Tonight’ Does Real Journalism, No Matter What John Oliver Says |Asawin Suebsaeng |September 29, 2014 |DAILY BEAST 

It was a pool-side scene, with hotel and tropical palms against an unbelievable blue sky. We're Friends, Now |Henry Hasse 

Too much that was unbelievable by old standards had happened around him. The Planet Strappers |Raymond Zinke Gallun 

It was almost unbelievable to herself that her life could be permeated by a thing Edith knew nothing about. Fidelity |Susan Glaspell 

The thing sounds unbelievable, and ridiculous; but she wanted to keep me forever at the age of thirteen and a half. Mountain |Clement Wood 

"It's more than interesting, it's marvelous, it's unbelievable," answered the detective quietly. Through the Wall |Cleveland Moffett