Their art is supplemented, and sometimes contrasted, by pieces from an international competition juried by Manganelli and Montalto. In the galleries: Rejuvenating the obsolete into unconventional art |Mark Jenkins |February 12, 2021 |Washington Post 

Goldman said he has supervised versions of the program at international theater conferences — and has been queried about using the technique in marriage therapy. An innovative Georgetown lab looks to theater to quell political fires |Peter Marks |February 12, 2021 |Washington Post 

However, once these companies start gaining international traction and need to build an infrastructure outside of their home country, they need to raise significant amounts to afford so. South African VC firm Knife Capital gets first commitment for its $50M fund, to invest in 10-12 Series B rounds |Tage Kene-Okafor |February 11, 2021 |TechCrunch 

Then came Daishen Nix, Jonathan Kuminga and international projects Kai Sotto of the Philippines and Princepal Singh of India. An NBA experiment lets draft prospects skip college, stay home and get paid to play |Michael Lee |February 11, 2021 |Washington Post 

Remember that international travelers have to now get tested and quarantine after they land in the US. How to celebrate Valentine’s Day in a pandemic |Sara Kiley Watson |February 10, 2021 |Popular-Science 

First, his credentials: He did international mergers and acquisitions at Lazard, a financial and asset management firm. Sen. Warren’s Main Street Crusade to Pressure Clinton |Eleanor Clift |January 8, 2015 |DAILY BEAST 

Together, they crossed over the International Bridges on foot into Juarez to conduct some business. An Informant, a Missing American, and Juarez’s House of Death: Inside the 12-Year Cold Case of David Castro |Bill Conroy |January 6, 2015 |DAILY BEAST 

While the world fixated on Ukraine and Syria, a near-genocide ripped through central Africa, to little international fanfare. The Year’s Most Forgotten Humanitarian Crisis |Nina Strochlic |January 1, 2015 |DAILY BEAST 

Despite its ranking at the bottom of most international development indexes, the conflict is shrouded by confusion. The Year’s Most Forgotten Humanitarian Crisis |Nina Strochlic |January 1, 2015 |DAILY BEAST 

Amnesty International and Reporters Without Borders cannot be accessed without a virtual private network. China’s Internet Is Freer Than You Think |Brendon Hong |December 27, 2014 |DAILY BEAST 

In 1883 she served with many distinguished artists on the art jury of the International Exhibition at Amsterdam. Women in the fine arts, from the Seventh Century B.C. to the Twentieth Century A.D. |Clara Erskine Clement 

Of course, the usual international operations for obtaining gold were denied to Germany. Readings in Money and Banking |Chester Arthur Phillips 

The doctrine of international free trade, albeit the most conspicuous of its applications, was but one case under the general law. The Unsolved Riddle of Social Justice |Stephen Leacock 

Since the outbreak of the war New York has assumed a position of leadership in international banking. Readings in Money and Banking |Chester Arthur Phillips 

Violation of the immunity due to those who come with this mission, duly accredited, in the form prescribed by international law. The Philippine Islands |John Foreman