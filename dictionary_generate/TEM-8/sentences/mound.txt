Excavate snow inside the mound until you reach the base of every stick. Winter survival shelters you should know how to build |By Tim MacWelch/Outdoor Life |December 21, 2020 |Popular-Science 

The lasting image of Strasburg’s 2020 season was him standing behind the mound at Camden Yards, shaking his hand as if he couldn’t feel it. Stephen Strasburg should be ‘full-go’ for spring training. The Nats will need him. |Jesse Dougherty |December 17, 2020 |Washington Post 

At one point Turner removed his mask again to pose on the mound with the trophy, alongside his wife, Kourtney, leaning over to kiss her. MLB investigating after Justin Turner ‘refused to comply’ with officials during celebration |Dave Sheinin, Jesse Dougherty |October 28, 2020 |Washington Post 

Inside each mound, which is a few meters tall, millions of millimeter-sized termites live. A Scientist Who Delights in the Mundane |Steve Nadis |October 26, 2020 |Quanta Magazine 

One NL scout8 said deGrom had unusual upside late in his career because he started so late on the mound, converting from shortstop to pitcher in his final season at Stetson University. Jacob DeGrom Just Keeps Throwing Faster |Travis Sawchik |September 17, 2020 |FiveThirtyEight 

Scans of many of these have been amassed by Vieira on his Facebook page, Stone Builders, Mound Builders and the Giants of Ancient. Hunting for a Real-Life Hagrid |Nina Strochlic |November 13, 2014 |DAILY BEAST 

Yezidis play a sort of basketball game here, balling cloth up and tossing it onto the top of the mound. Fighting Back With Faith: Inside the Yezidis’ Iraqi Temple |Michael Luongo |August 21, 2014 |DAILY BEAST 

Here Luqman also pointed out a curious structure, a mound rising on the side of the wall in front of all the urns. Fighting Back With Faith: Inside the Yezidis’ Iraqi Temple |Michael Luongo |August 21, 2014 |DAILY BEAST 

Salama al Sersawi leans on a bench, waiting to get his mound of matted hair reined in. The Gaza Paradox: Hamas Has Little Support, but the War Has a Lot |Jesse Rosenfeld |August 7, 2014 |DAILY BEAST 

The salmon is presented atop a mound of sautéed vegetables: mushrooms, peppers, squash, onions, leafy greens, and herbs. Spaghetti for Breakfast?! Not So Crazy at This Idaho Farm Café |Jane & Michael Stern |August 4, 2014 |DAILY BEAST 

On three sides the surrounding ground rose steeply, forming an irregular horseshoe mound that opened to the west. Uncanny Tales |Various 

It was doubtless the case with the Mound Builders, of whom these communities were probably the descendants. Man And His Ancestor |Charles Morris 

This morning when I went I found them cutting into a mound of what looked like solid white chalk. The Sinn Fein rebellion As I Saw It. |Mrs. Hamilton Norway 

She called her father, who had remained a few steps away from the little mound; but when he reached her it had all disappeared. Urania |Camille Flammarion 

Farnham soon came up to the mound; but they did not begin to move till Ranger shouted out that all were safely over. Digby Heathcote |W.H.G. Kingston