The couple, also known as the Duke and Duchess of Sussex, will produce an array of works including scripted and documentary series, documentary films, scripted features and children’s programming, the company said in an emailed statement Wednesday. Prince Harry and Meghan have signed a wide-ranging Netflix production deal |radmarya |September 2, 2020 |Fortune 

In 2016, Anat Zalmanson-Kuznetsov, the daughter of Edward Kutznetsov and Sylva Zalmanson, the only woman charged in the hijacking, directed Operation Wedding, a documentary about the event. The Failed Hijacking That Remade the Soviet Union |Eromo Egbejule |August 24, 2020 |Ozy 

Among the projects in the works is a feature-length documentary that will not feature Shopify’s brand nor any merchants that use its e-commerce platform, North said. I want my DTC TV? Shopify debuts reality TV show |Tim Peterson |August 18, 2020 |Digiday 

A typical week might include doing a research roundup for our Monday show, helping book a guest for a Thursday show and pulling archival audio for a long-term documentary project. We’re Looking For A Temporary Producer For Our Politics Podcast |Galen Druke |August 5, 2020 |FiveThirtyEight 

I read A Brief History of Time, and I saw some documentaries about Hawking. This Cosmologist Knows How It’s All Going to End |Dan Falk |June 22, 2020 |Quanta Magazine 

The documentary also follows the fortunes of Consuelo Yznaga, later Duchess of Manchester. The Real-Life ‘Downton’ Millionairesses Who Changed Britain |Tim Teeman |December 31, 2014 |DAILY BEAST 

The claim is one of a series of allegations made in a controversial documentary that the BBC has now pulled. Pulled Documentary Says William Felt ‘Used’ by Charles’ Push for Camilla |Tom Sykes |December 30, 2014 |DAILY BEAST 

This came across in the Showtime Omit the Logic documentary—in which you were a commentator—and it comes across here. How Richard Pryor Beat Bill Cosby and Transformed America |David Yaffe, Scott Saul |December 10, 2014 |DAILY BEAST 

I was looking to make a documentary, I wanted to change my career, and I knew right away this was it. This Fashion World Darling Is Homeless |Erica Wagner |December 2, 2014 |DAILY BEAST 

She also captured the entire episode in her riveting, award-worthy documentary Citizenfour, which is in theaters now. Laura Poitras on Snowden's Unrevealed Secrets |Marlow Stern |December 1, 2014 |DAILY BEAST 

There is no documentary evidence to fix the exact date, but it is generally assumed to have been 907. Bell's Cathedrals: A Short Account of Romsey Abbey |Thomas Perkins 

The first documentary evidence of the existence of the merchant gild appears in 1242. Encyclopaedia Britannica, 11th Edition, Volume 4, Part 3 |Various 

To do this has rendered my history, to a large extent, documentary, instead of being a mere popular narrative. The Loyalists of America and Their Times, Vol. 1 of 2 |Egerton Ryerson 

Briefly it would be necessary for him to go to London, to secure documentary evidence. The Hidden Places |Bertrand W. Sinclair 

"My husband is coming from London," she asserted, searching in her reticule for documentary evidence. Punch, or the London Charivari, Volume 147, August 12, 1914 |Various