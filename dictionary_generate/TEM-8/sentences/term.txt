Meanwhile, you can’t currently report potential violations of those terms without handing over your email address. You’ve been invited to Clubhouse. Your privacy hasn’t. |Sara Morrison |February 12, 2021 |Vox 

You point out that using the term "mansplaining" overgeneralizes, so I won't call this behavior "momsplaining." Miss Manners: Practice saying ‘I’ve got it, thanks’ |Judith Martin, Nicholas Martin, Jacobina Martin |February 12, 2021 |Washington Post 

He said he hopes that he and his staff have built up “some trust and credibility” in terms of their vetting of players. Trevor Bauer, unorthodox star with an unorthodox deal, gets an unorthodox Dodgers intro |Chelsea Janes |February 12, 2021 |Washington Post 

The term “AI audit” can mean many different things, which makes it hard to trust the results of audits in general. Auditors are testing hiring algorithms for bias, but there’s no easy fix |Amy Nordrum |February 11, 2021 |MIT Technology Review 

It sounds like a small adjustment, but it can make a big difference in terms of driving feel. Audi’s e-Tron GT charges up fast, but turns even faster |Stan Horaczek |February 10, 2021 |Popular-Science 

But on Thursday Boxer triggered a Golden State political earthquake, announcing that she would not seek a fifth term in 2016. The Golden State Preps for the ‘Red Wedding’ of Senate Races |David Freedlander |January 9, 2015 |DAILY BEAST 

Term limits could be a prescription to speed change along. The Unbearable Whiteness of Congress |Dean Obeidallah |January 8, 2015 |DAILY BEAST 

Wrapees was the term marines used for the Japanese because they had wrapping round their legs. The Story Behind Lee Marvin’s Liberty Valance Smile |Robert Ward |January 3, 2015 |DAILY BEAST 

This was later repurposed in Europe as an explanation for racial superiority, and the term “Aryan” came to define a white race. The Himalayas’ Hidden Aryans |Nina Strochlic |January 3, 2015 |DAILY BEAST 

He won re-election twice as governor of New York, and had the hubris to run for a fourth term before being defeated in 1994. Mario Cuomo, a Frustrating Hero to Democrats, Is Dead at 82 |Eleanor Clift |January 2, 2015 |DAILY BEAST 

So he bore down on the solemn declaration that she stood face to face with a prison term for perjury. The Bondboy |George W. (George Washington) Ogden 

All changes are to be Rang either by walking them (as the term is) or else Whole-pulls, or Half-pulls. Tintinnalogia, or, the Art of Ringing |Richard Duckworth and Fabian Stedman 

These practical demonstrations occurred usually in the opening enthusiasm of the term. The Salvaging Of Civilisation |H. G. (Herbert George) Wells 

I shall show how it is possible thus to prolong life to the term set by God. The Mediaeval Mind (Volume II of II) |Henry Osborn Taylor 

But men, through neglecting the rules of health, pass quickly to old age, and die before reaching that term. The Mediaeval Mind (Volume II of II) |Henry Osborn Taylor