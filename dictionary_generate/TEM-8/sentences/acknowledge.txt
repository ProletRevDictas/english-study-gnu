What was extremely disturbing to us was we began to realize that there was a settlement that came out of each period that had to do with restoring racial hierarchy or even just never even acknowledging a conflict around it and building on top of it. RBG, minority rule, and our looming legitimacy crisis |Ezra Klein |September 25, 2020 |Vox 

Finally, Becerra stepped in, urging Hayungs to get on with his presentation, but never acknowledging all he’d done to help the businessman. The COVID-19 Charmer: How a Self-Described Felon Convinced Elected Officials to Try to Help Him Profit From the Pandemic |by Vianna Davila, Jeremy Schwartz and Lexi Churchill |September 25, 2020 |ProPublica 

The researchers acknowledged in their article that they do not have precise enough dating to verify how many species, especially land animals and plants, actually died out in the time frame that the researchers investigated. A volcanic eruption may have helped the dinosaurs take over the world |Rachael Zisk |September 24, 2020 |Popular-Science 

Executive Chairman Bill Ford, whom Newsom praised in his remarks, was among the first Detroit executives to acknowledge the role gas-burning cars play in climate change. California to ban new gasoline cars by 2035, a first in U.S. |Verne Kopytoff |September 23, 2020 |Fortune 

In the plea agreement, Garmo acknowledged that between 2013 and 2019 he made straw purchases of “off-roster” handguns and rifles that only members of law enforcement can possess, then transferred 98 of them to people he knew. Sheriff’s Department Made Excuses for Captain Who Pleaded Guilty to Arms Dealing |Jesse Marx |September 23, 2020 |Voice of San Diego 

Every once in a while, they act swiftly and acknowledge the problem. Today’s GOP: Still Cool With Racist Pandering? |Michael Tomasky |January 7, 2015 |DAILY BEAST 

You have to acknowledge your age and position in life, for me quite a lot of those emotionally fueled songs were hormone songs. Belle & Sebastian Aren’t So Shy Anymore |James Joiner |January 7, 2015 |DAILY BEAST 

Interviews in Serial (including ones from Adnan) do acknowledge that Jay was known as a resident bad boy at Woodlawn High School. The Deal With Serial’s Jay? He’s Pissed Off, Mucks Up Our Timeline |Emily Shire |December 31, 2014 |DAILY BEAST 

As Kurdish leaders acknowledge, liberating Mosul is beyond the capability of the peshmerga and government forces will be needed. Iraqi Kurds Get Their Groove Back, End Siege of Mount Sinjar |Jamie Dettmer |December 20, 2014 |DAILY BEAST 

All seem to acknowledge that sexual assault is a serious problem that requires campus-wide education. Fraternities in a Post-UVA World |Samantha Allen |December 12, 2014 |DAILY BEAST 

And when he answered it, he was obliged to acknowledge that she had made upon his nature a definite impression. Bella Donna |Robert Hichens 

He would impeach all his partners, acknowledge his errors, and promise once more to reform. Blackwood's Edinburgh Magazine, No. CCCXXXIX. January, 1844. Vol. LV. |Various 

This is certainly handsome, and I acknowledge the courtesy, though I shall not accept the invitation. Glances at Europe |Horace Greeley 

They obeyed, and the sieur made them acknowledge the authority of his son, as Vice-Admiral in the said lands of the West. The Jesuit Relations and Allied Documents, Vol. II: Acadia, 1612-1614 |Various 

"Gilbert, I am glad you acknowledge the folly of your conduct," said the farmer, breaking the painful silence. The World Before Them |Susanna Moodie