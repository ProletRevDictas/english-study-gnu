“WOW,” he wrote on Twitter the morning of February 7, posting side-by-side satellite shots of a dark area of possible “massive dust deposition,” contrasted against the same snowy, pristine region just the day before. Three things to know about the disastrous flood in India |Carolyn Gramling |February 9, 2021 |Science News 

The only thing that happened on his side was he had to come to some depositions. New York City Paid an NBA Star Millions After an NYPD Officer Broke His Leg. The Officer Paid Little Price. |by Mike Hayes for ProPublica |January 12, 2021 |ProPublica 

As part of its investigation, ProPublica obtained years of Evenflo’s testing videos, thousands of pages of sworn depositions by company employees and marketing materials that, until then, had largely been shielded by court secrecy orders. Congressional Investigation Finds Many Booster Seat Makers “Endangered” Children’s Lives After Review of “Meaningless Safety Testing” |by Patricia Callahan |December 11, 2020 |ProPublica 

It was not mere legal theater when judges appointed commissioners to carry out months of fact-finding, including traveling to obtain witness depositions. The enslaved families who went to court to win their freedom |Alison LaCroix |December 11, 2020 |Washington Post 

These are salts that, based on geological, paleontological, and geochemical evidence, haven’t been re-dissolved and re-crystallized since their initial deposition. Preserving a Sense of Wonder in DNA - Issue 92: Frontiers |Virat Markandeya |October 28, 2020 |Nautilus 

That assertion, given by Shore in a pre-trial deposition, would have been too prejudicial to present to the jury, the court ruled. Money, Murder, and Adoption: The Wild Trial of the Polo King |Jacqui Goddard |October 28, 2014 |DAILY BEAST 

Also, at the time of his deposition Thicke had been off of pills for two months, because his wife left him in February. Hey, Hey, Hey Robin Thicke Was Too Vicodin Wasted to Really Write ‘Blurred Lines’ |Amy Zimmerman |September 16, 2014 |DAILY BEAST 

Evans used a “pressure point control technique,” one of the officers testified in his deposition. I Was Beaten By Chicago’s Dirtiest Cop, Lawsuit Contends |Justin Glawe |September 16, 2014 |DAILY BEAST 

In his infamous deposition video, you can clearly see that the “thug life” façade is stripped away. Method Man Talks Wu-Tang Clan Reunion, Fake Rappers, and the Suge Knight Shooting |Marlow Stern |September 15, 2014 |DAILY BEAST 

A brief bio is to be found in a deposition she gave in the criminal case before the felony charge was dropped. From Ferguson Cop Embroiled in a Brutality Suit to City Councilwoman |Michael Daly |August 20, 2014 |DAILY BEAST 

The last of the criers did not last long after deposition from office, Jacob's last words being uttered in 1881. Showell's Dictionary of Birmingham |Thomas T. Harman and Walter Showell 

This gain in those parts of the river's curvings where deposition tends to take place may be accelerated by tree-planting. Outlines of the Earth's History |Nathaniel Southgate Shaler 

He was taken from prison and placed on the throne, on the deposition of his brother, Mahomet, 1687. The Every Day Book of History and Chronology |Joel Munsell 

Cuthbert's importunity in the matter is clearly set forth in60 a deposition by Henry Johnson, one of Alleyn's tenants. Shakespearean Playhouses |Joseph Quincy Adams 

It is on your deposition and on the dates, as I have on the others, but that fact will appear from the record. Warren Commission (10 of 26): Hearings Vol. X (of 15) |The President's Commission on the Assassination of President Kennedy