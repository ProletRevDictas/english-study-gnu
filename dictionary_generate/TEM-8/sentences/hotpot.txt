“I love spicy food, and my friends tell me that Sichuan hotpot has become very popular in Addis Abeba,” he said. 'Made in China' Now Being Made in Africa | Brendon Hong | August 23, 2014 | DAILY BEAST 

With time I learned to disassemble the entire hotpot and mount the heating coil on a roast beef can with a whole punched in it. Tales of a Jailhouse Gourmet: How I learned to Cook in Prison | Daniel Genis | June 21, 2014 | DAILY BEAST 

My throat is peeling, my tongue is burning: this is no hotpot. The Legend of Ulenspiegel, Vol. II (of 2) | Charles de Coster 

After some hesitation Sylvia decided on hotpot and fig pudding. The Third Class at Miss Kaye's | Angela Brazil British Dictionary definitions for hot pot hotpot / ( ˈhɒtˌpɒt ) / noun British a baked stew or casserole made with meat or fish and covered with a layer of potatoes Australian slang a heavily backed horse Collins English Dictionary - Complete & Unabridged 2012 Digital Edition © William Collins Sons & Co. Ltd. 1979, 1986 © HarperCollins Publishers 1998, 2000, 2003, 2005, 2006, 2007, 2009, 2012