He will also get up to grab things he forgot, adjust the heat, get a sweatshirt, open the window, etc. Miss Manners: Practice saying ‘I’ve got it, thanks’ |Judith Martin, Nicholas Martin, Jacobina Martin |February 12, 2021 |Washington Post 

Multiple photographs taken at the Capitol show a man identified as Packer wearing a sweatshirt with the words “Camp Auschwitz” above a skull and crossbones, the FBI concluded. Here are some of the people charged since a mob breached the Capitol |Washington Post Staff |January 15, 2021 |Washington Post 

After he left, Abraham Gomez arrived, wearing a sweatshirt and ripped jeans. On a covid Christmas, a new routine for feeding the homeless |Paul Schwartzman |December 25, 2020 |Washington Post 

It’s become a ritual to pull it out when temperatures start dropping below sweatshirt weather, and it’s my staple outer layer until it gets really cold. The Gear Our Editors Loved in November |The Editors |December 4, 2020 |Outside Online 

He called me his “little minx” and loved the way his sweatshirts swallowed me up. What ‘The Crown’ got right in portraying Princess Diana’s bulimia: It let her voice be heard |Amanda Long |November 20, 2020 |Washington Post 

Instead, show your love and holiday spirit this season in this heart sweatshirt from Madewell. The Daily Beast’s 2014 Holiday Gift Guide: For the Taylor Swift in Your Life |Allison McNearney |November 29, 2014 |DAILY BEAST 

The Boston firefighter took a grey BFD sweatshirt from a locker and presented it to Flaherty. Boston and New York’s Bravest Are Brothers Bonded by Tragedy |Michael Daly |April 15, 2014 |DAILY BEAST 

Sternberg showed a sweatshirt wrapped around a hips-style skirt, oversized faux furs, and thigh-length cardigans. The Coolest Kids on the Block: Public School, Opening Ceremony, and Band of Outsiders |Erin Cunningham |February 10, 2014 |DAILY BEAST 

Abu Hassar nodded and took a small vial of perfume from his Adidas sweatshirt. The Fourth War: My Lunch with a Jihadi |Elliot Ackerman |January 21, 2014 |DAILY BEAST 

And one of them was a guy wearing these small Janis Joplinesque sunglasses and an old Les Miserables sweatshirt. You Should Have Been There: Dispatches From Miami Art Basel |Anthony Haden-Guest |December 8, 2013 |DAILY BEAST 

She dressed in jeans and an oversized UW sweatshirt, with a laptop perched on one knee. Makers |Cory Doctorow 

Then he hitched up his sweatshirt over the hairy pale expanse of his own belly and tipped to one side. Someone Comes to Town, Someone Leaves Town |Cory Doctorow 

Kurt swiped at his moist eyes with the sleeve of his colorless grey sweatshirt. Someone Comes to Town, Someone Leaves Town |Cory Doctorow 

She took a step toward him, her jacket opening to reveal a shapeless grey sweatshirt stained with food and—blood? Someone Comes to Town, Someone Leaves Town |Cory Doctorow