At the time, a team of restorers were tending to medieval frescoes at the Camposanto Monumentale in town. Grime-loving bacteria could save priceless art |Erin Fennessy |September 20, 2021 |Popular-Science 

Between archaeological evidence, written records, and fresco paintings, not to mention scientific experimentation, the story of Panis Quadratus slowly revealed itself. Eat like an ancient Roman by recreating bread from Pompeii |Alisha McDarris |September 9, 2021 |Popular-Science 

Faherty, makers of my favorite shirts, crushed it with these shorts that are perfect for walks on the beach or dining al fresco. Save 35% on Outdoor Gear From Huckberry Right Now |Daniel Modlin |July 23, 2021 |The Daily Beast 

Crossnore’s longtime mission supports at-risk children, and the fresco is filled with kids sitting with Jesus — some modeled on Long’s sons. Following a trail of frescoes in the mountains of North Carolina |Barbara Noe Kennedy |May 7, 2021 |Washington Post 

While I had seen frescoes throughout Italy and France, I had never heard of any in the United States. Following a trail of frescoes in the mountains of North Carolina |Barbara Noe Kennedy |May 7, 2021 |Washington Post 

And Pope Alexander VI had the painter Pinturicchio disguise his mistress as the Virgin Mary in one fresco. Great Renaissance Art Thrived Amid Filth |Nick Romeo |December 3, 2014 |DAILY BEAST 

Dining facilities include al fresco picnic tables and bucolic fields adjacent to the pastures. The Secret to This Ice Cream: Pampered Cows |Jane & Michael Stern |May 18, 2014 |DAILY BEAST 

Authorities had not noticed that missing fresco, which had been taken from the House of the Orchard, until it was returned. Pompeii Made It Through a Volcano, but Can It Survive Vandals? |Barbie Latza Nadeau |March 22, 2014 |DAILY BEAST 

As I read this, I imagined a fresco depicting the economic section of the document. Pope Francis Declares Consumers and Capitalists Need to Help the Poor |Daniel Gross |November 26, 2013 |DAILY BEAST 

This 13th-century fresco of a lion was painted near Burgos in Spain, probably by an itinerant English artist from Winchester. Caught by the Tale at the Cloisters |Blake Gopnik |March 15, 2013 |DAILY BEAST 

Beneath the portico, numbers of servants and retainers were lounging about, enjoying the fresco. Blackwood's Edinburgh Magazine, Volume 60, No. 372, October 1846 |Various 

In thisPg 89 church is a remarkable altar fresco which was executed by the late Lord Leighton. British Highways And Byways From A Motor Car |Thomas D. Murphy 

On the outer walls of the principal temple are wretched daubs in fresco, representing the state of eternal punishment. A Woman's Journey Round the World |Ida Pfeiffer 

The walls and ceiling were often covered with fresco paintings, frequently of elegant design, to be hereafter described. The Catacombs of Rome |William Henry Withrow 

The following fresco from the Catacomb of St. Priscilla is a characteristic example. The Catacombs of Rome |William Henry Withrow