The End of Gangs By Sam Quinones, Pacific-Standard Los Angeles gave America the modern street gang. The Daily Beast’s Best Longreads, Dec 29-Jan 4, 2014 |William Boot |January 4, 2015 |DAILY BEAST 

Still, at least Macklemore sounds like a white guy from the Pacific Northwest. The Cultural Crimes of Iggy Azalea |Amy Zimmerman |December 29, 2014 |DAILY BEAST 

Owing to its popularity as a pet, it has spread across the Pacific to China. The Buddhist Business of Poaching Animals for Good Karma |Brendon Hong |December 28, 2014 |DAILY BEAST 

He was a young Army Air Force lieutenant whose plane crashed in the Pacific in May 1943. Dick Cheney vs. ‘Unbroken’ |Mike Barnicle |December 15, 2014 |DAILY BEAST 

To break her self-destructive cycle and heal, she decides to hike 1,100 miles of the Pacific Crest Trail solo. Exclusive: The Making of Reese Witherspoon’s Golden Globe-Nominated ‘Wild’ |Marlow Stern |December 12, 2014 |DAILY BEAST 

That his friend had withdrawn, was a pledge of his pacific wishes; and, with a lightened countenance, Louis rose from his knee. The Pastor's Fire-side Vol. 3 of 4 |Jane Porter 

The shrieking trade-winds and the dense white fogs were hibernating somewhere out in the Pacific. Ancestors |Gertrude Atherton 

It looked so calm and peaceful that he said, "I will call it 'Pacific,' for I have never seen the like before." Alila, Our Little Philippine Cousin |Mary Hazelton Wade 

At this spot, according to our works on geography, the Atlantic Ocean changes its name and assumes that of the Pacific. A Woman's Journey Round the World |Ida Pfeiffer 

That extraordinary but unfortunate man was the first European whose eyes rested on the broad Pacific. Journal of a Voyage to Brazil |Maria Graham