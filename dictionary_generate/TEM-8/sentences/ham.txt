Thankfully, someone was there to capture this “jit going ham,” as the cameraman put it. Slow Motion Tiger Jump, a Tornado at the Rose Bowl and More Viral Videos |The Daily Beast Video |January 4, 2015 |DAILY BEAST 

Mister Ham in need of cash: That is something a lot of people will not believe. The Stacks: Sell the Overcoat, Keep the Dignity |Paul Hemphill |December 22, 2014 |DAILY BEAST 

And this week it was Mister Ham, General Delivery, United States. The Stacks: Sell the Overcoat, Keep the Dignity |Paul Hemphill |December 22, 2014 |DAILY BEAST 

“Maybe you need a good overcoat for Christmas,” Mister Ham was saying. The Stacks: Sell the Overcoat, Keep the Dignity |Paul Hemphill |December 22, 2014 |DAILY BEAST 

Nobody bothered to tell Mister Ham about it until the following August. The Stacks: Sell the Overcoat, Keep the Dignity |Paul Hemphill |December 22, 2014 |DAILY BEAST 

“Ham and eggs, dry toast and shrimps,” said the keen-eyed traveller in reply to the reiterated question. The Floating Light of the Goodwin Sands |R.M. Ballantyne 

He reached over, with astonishing suddenness in one so bulky, and twirled the secretary about with his ham of a hand. Scattergood Baines |Clarence Budington Kelland 

A pair of carvers, laid with my cover, tell me that I shall have to carve the ham which is here eaten with the chicken. Friend Mac Donald |Max O'Rell 

My host has before him a fine joint of beef, there are two chicken in front of my hostess, and I am placed opposite a boiled ham. Friend Mac Donald |Max O'Rell 

Every “biscuit” or “ham” has been cut in two to find out whether the native has loaded it in any way. The Wonder Book of Knowledge |Various