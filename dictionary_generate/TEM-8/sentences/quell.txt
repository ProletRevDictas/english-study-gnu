The Terps’ tournament aspirations haven’t evaporated, but Maryland’s performance did little to quell the concerns brought on by its loss to Penn State on Friday. Maryland misses a chance to boost its NCAA tournament hopes with a loss to No. 4 Ohio State |Emily Giambalvo |February 9, 2021 |Washington Post 

These platforms have occasionally shown that they are capable of quelling disinformation and removing or flagging misleading content, but they are not consistent in enforcing their own policies and protecting LGBTQ people. 5 things social media platforms can do to combat anti-LGBTQ disinformation |Brianna January |January 7, 2021 |Washington Blade 

Chancellor Angela Merkel called an emergency meeting to speed up Germany’s Covid-19 vaccine program in an effort to quell tensions within her government over claims the rollout has been too slow. Merkel pushes to speed up vaccine rollout, as German lockdown tightens |kdunn6 |January 6, 2021 |Fortune 

Testing has been expanded in the region, as well as other efforts to help quell the spread of the virus, but South Bay residents continue to be impacted more by the virus than other parts of San Diego County. ‘I’ve Lost Everything That Basically Brings Someone Joy’ |Brittany Cruz-Fejeran and Maya Srikrishnan |December 23, 2020 |Voice of San Diego 

Tijuana’s business community had also been increasingly involved in trying to quell the violence, but has been hit hard financially by the closures caused by the pandemic. Border Report: What’s Behind Tijuana’s Violent Year |Maya Srikrishnan |December 15, 2020 |Voice of San Diego 

This is where Schwarz comes in: to quell concerns, advise about procedures, and follow up with loved ones. The Nurse Coaching People Through Death by Starvation |Nick Tabor |November 17, 2014 |DAILY BEAST 

My question is simply this: where are the Reverends Al Sharpton and Jesse Jackson in seeking to quell potential violence? As Michael Brown Grand Jury Winds Down, Is Ferguson on the Brink of War? |Ron Christie |November 16, 2014 |DAILY BEAST 

Question: Does anyone believe this coalition will use a 48-hour window to quell violence? As Michael Brown Grand Jury Winds Down, Is Ferguson on the Brink of War? |Ron Christie |November 16, 2014 |DAILY BEAST 

Afterwards, a slew of major NBA reporters did their best to quell the giddy, growing mob. LeBron James Returns to Cleveland: How 'The Decision 2.0' Happened |Robert Silverman |July 11, 2014 |DAILY BEAST 

People who knew Jack in real life did little to quell the rumors. Death of Hero Hacker Barnaby Jack Ruled a Drug Overdose |Brandy Zadrozny |January 4, 2014 |DAILY BEAST 

For several months he remained under a political cloud, charged with incompetency to quell the Philippine Rebellion. The Philippine Islands |John Foreman 

The exertions of the city authorities, who had notice of the meditated riot, were unable to prevent or quell it. The Every Day Book of History and Chronology |Joel Munsell 

Monsieur de Tressan was here, as ill-luck would have it, and Gaubert implored him to send soldiers thither to quell the riot. St. Martin's Summer |Rafael Sabatini 

She caught her underlip between two rows of white teeth to quell the groan of helplessness. The Adventure Girls at K Bar O |Clair Blank 

I dare not envy many a man:Who runs his life-race well; Whose brave, undaunted peasant bloodDeath's menace cannot quell. Charles Baudelaire, His Life |Thophile Gautier