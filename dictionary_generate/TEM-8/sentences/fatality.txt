New York and New Jersey in particular saw hundreds of deaths a day in April, quickly contributing to the country’s total number of fatalities. Trump contradicts CDC director on vaccine; Biden says Americans shouldn’t trust Trump |Colby Itkowitz, Felicia Sonmez, John Wagner |September 16, 2020 |Washington Post 

New York and New Jersey in particular recorded hundreds of deaths a day in April, quickly contributing to the country’s total number of fatalities. Trump blames blue states for the coronavirus death toll — but most recent deaths have been in red states |Philip Bump |September 16, 2020 |Washington Post 

If we compare the necessary spread to achieve herd immunity with the fatality rate, we get a wide range of possible death tolls from the virus. The problem with Trump’s ‘herd mentality’ line isn’t the verbal flub. It’s the mass death. |Philip Bump |September 16, 2020 |Washington Post 

The country’s chief medical officers said Sunday that the Covid-19 fatality rate among those ages 5 to 14 is lower than most seasonal flu infections. Boris Johnson is urging parents to send their children back to school this fall |kdunn6 |August 24, 2020 |Fortune 

Just this past week, California, Florida and Texas, along with a handful of other states, saw record spikes in fatalities. Some Republicans Have Gotten More Concerned About COVID-19 |Dhrumil Mehta (dhrumil.mehta@fivethirtyeight.com) |July 31, 2020 |FiveThirtyEight 

Just as Obama was heading back to his house that night, a 14-year-old named Kevin Diaz became the latest Chicago gun fatality. Brooklyn Shooting Hits Close to Bill de Blasio’s Park Slope Home |Michael Daly |July 1, 2014 |DAILY BEAST 

The Good Wife introduced its potentially fatal fatality into a world already in flux. Life After TV Death: How Shows Like ‘Game of Thrones’ Kill Your Favorite Characters |Phillip Maciak |April 15, 2014 |DAILY BEAST 

They're neither rising nor falling, and the highest fatality shooting took place while the assault-weapons ban was in place. Department of Awful Statistics: Are Mass Shootings Really On the Rise? |Megan McArdle |January 28, 2013 |DAILY BEAST 

The gun-fatality rate for blacks far exceeds that for whites. Is Gun Control Racist? |Adam Winkler |October 10, 2011 |DAILY BEAST 

A 26-year-old man was the first fatality of the riots, found shot in his car. Cameron: Rioters Will Pay | |August 11, 2011 |DAILY BEAST 

Many, however, are not aware of the fatality attending its use by the brute creation. Tobacco; Its History, Varieties, Culture, Manufacture and Commerce |E. R. Billings. 

By a strange fatality, they were generally purblind, and always shyed most fearfully when an Opposition coach approached them. Blackwood's Edinburgh Magazine, No. CCCXXXIX. January, 1844. Vol. LV. |Various 

She had fallen in love; fallen with the fatality of the Lemprieres, and with the fine precipitate sweep of her own genius. The Creators |May Sinclair 

You would not believe me: you went this morning to St. Catherine's, and by a fatality the prince was there and saw you.' Chicot the Jester |Alexandre Dumas, Pere 

Misfortune, Fatality, had willed that a drop of water thicker than the surrounding medium should pass through one of the mollusks. Urania |Camille Flammarion