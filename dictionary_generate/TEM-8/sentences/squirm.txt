I started to squirm in my chair and Jimbo put his hand back on my shoulder to settle me down. I Shot Bin Laden |Elliot Ackerman |November 16, 2014 |DAILY BEAST 

Even with several judges on her payroll, Mandelbaum was not able to squirm out of the charges. Meet 'The Queen of Thieves' Marm Mandelbaum, New York City's First Mob Boss |J. North Conway |September 7, 2014 |DAILY BEAST 

His relationship with ex-wife Effi Barry is squirm-inducing. Speed Read: Marion Barry’s Crazy Memoir |William O’Connor |June 18, 2014 |DAILY BEAST 

The negotiator added that she told him she “liked to watch them squirm around after they had been shot.” The First Modern School Shooter Feels Responsible for the Rest |Michael Daly |May 30, 2014 |DAILY BEAST 

Moments like these could cause ticket-buyers to squirm or, perhaps, reflect on their own capacity to overlook and forgive. Woody Allen’s ‘Bullets Over Broadway’ Musical and the Moral Responsibility of an Artist |Brian Spitulnik |April 10, 2014 |DAILY BEAST 

He was losing hold of himself, and roaring like a bull and flinging out taunts that made 'em squirm. Danny's Own Story |Don Marquis 

She sat down to compose a letter which should make Mr. Robert Ross, alias wretch, squirm in agony. The White Shield |Myrtle Reed 

I was afraid of my life he would clutch at my skirts as he fell or squirm up against me after he was down. Hyacinth |George A. Birmingham 

Whereupon Andy smoked relishfully and in silence, and from the tail of his eye watched his audience squirm with impatience. The Happy Family |Bertha Muzzy Bower 

He tried to withdraw the key, but now Macklin began to squirm worse than ever, and he had hard work to master the fellow. The Missing Tin Box |Arthur M. Winfield