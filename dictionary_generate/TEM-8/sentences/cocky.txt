Her portrait shows a young woman who’s self-assured but not cocky, just uncertain enough about life and the world to help us navigate it too. TIME's Best Portraits of 2021 |TIME Staff |December 16, 2021 |Time 

Among other remarks, Rogozin has said that NASA should not get too cocky about its newfound access to space in case SpaceX's Crew Dragon vehicle "breaks." Russia’s space chief wishes his oligarchs invested in space like Branson and Musk |Eric Berger |July 13, 2021 |Ars Technica 

During the second snow of the next year, we got cocky and took on too many customers. These Teen Brothers Are The Geniuses Behind This Popular Mumbo Sauce Brand |Kimberly Wilson |April 12, 2021 |Essence.com 

It’s Bugs Bunny, in fact, who seems to channel Gashouse Gang star pitcher Dizzy Dean, with his cocky persona and homespun personality. ‘Baseball Bugs’ at 75: How a Looney Tunes classic left its mark on America’s pastime |Frederic J. Frommer |February 25, 2021 |Washington Post 

Where are you going to, Henny-penny, Cocky-locky, and Ducky-daddles? Boys and Girls Bookshelf; a Practical Plan of Character Building, Volume I (of 17) |Various 

Peter and Prince were the dearest dogs, and Cocky was a parrot that could say the most amusing things. Greyfriars Bobby |Eleanor Atkinson 

I got to know of this affair through Robbins' backing up of Cocky, and telling Urquhart that nobody was afraid of him. A Holiday in Bed and Other Sketches |J. M. Barrie 

“Cocky,” he said bravely, without a quiver of fear or flight, when Michael had charged upon him at sight to destroy him. Michael, Brother of Jerry |Jack London 

So Kwaque remained in the two rooms, cooking and housekeeping for his master and caring for Michael and Cocky. Michael, Brother of Jerry |Jack London