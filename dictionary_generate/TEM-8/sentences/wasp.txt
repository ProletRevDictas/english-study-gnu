They were called La Red Avispa (The Wasp Network) and claim to have successfully foiled a number of threats against the island. Of Cuban Spies, a Baby, and a Filmmaker: The Strange Tale of the Cuban Five |Nina Strochlic |December 28, 2014 |DAILY BEAST 

I was like, “In what world do I look like I would play the WASP-y wife?” Eliza Coupe Finds Her ‘Happy Ending’ With ‘Benched’ |Kevin Fallon |October 28, 2014 |DAILY BEAST 

We later learn that the model, who is the heroine, is a Connecticut Wasp disappointed in her arrival in the big city. Punks, UFOs, and Heroin: How ‘Liquid Sky’ Became a Cult Movie |Daniel Genis |June 2, 2014 |DAILY BEAST 

In his memoir, Belfort describes Danny Porush (the real Azoff) as a Jewish Long Islander with WASP pretensions. Finally! ‘The Wolf of Wall Street’ Is Hollywood’s First 1990s Period Piece |Andrew Romano |December 23, 2013 |DAILY BEAST 

A WASP-y Smith alumna named Piper Chapman (Taylor Schilling) has a lesbian affair after college. You’ve Gotta Binge on the New Netflix Series ‘Orange Is the New Black’ |Andrew Romano |July 11, 2013 |DAILY BEAST 

Soon after he had some lively service in the Wasp, and captured a British privateer with the little sloop Sachem. Stories of Our Naval Heroes |Various 

Of the 110 men on the Frolic there were not twenty alive and unhurt, while on the Wasp only five were dead and five wounded. Stories of Our Naval Heroes |Various 

The game was up with the Wasp and her prize, for the new ship was the Poictiers, a great seventy-four ship-of-the-line. Stories of Our Naval Heroes |Various 

The honey in this pot was once a wasp's nest, but since being transformed it has become sweet and delicious. The Tin Woodman of Oz |L. Frank Baum 

The first thing the Wasp found at sea was a mighty gale of wind, that blew "great guns" for two days. Stories of Our Naval Heroes |Various