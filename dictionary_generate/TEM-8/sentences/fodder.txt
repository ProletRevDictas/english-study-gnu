Epidemiological concepts like “herd immunity” and “aerosol transmission” are now fodder for casual conversations. The Year in Biology |John Rennie |December 23, 2020 |Quanta Magazine 

That’d be fodder for bears like David Einhorn, who called an “enormous” bubble in tech stocks and said his firm has added bearish wagers. Wednesday’s FAAMG-led sell-off was no blip, tech bears say |Bernhard Warner |October 29, 2020 |Fortune 

So all that said, Briggs is clearly the underdog but Mara has given him a lot of fodder and it has helped him string together a network of her critics that may cause the night to be closer than we all would have thought. Your San Diego Election Questions, Answered |Megan Wood |October 20, 2020 |Voice of San Diego 

The mystery of that leak was great fodder for conspiracy theorists, but the fact that the hole was accidentally made by a drill was lucky. Astronauts on the ISS are hunting for the source of another mystery air leak |Neel Patel |September 30, 2020 |MIT Technology Review 

So I think policy disputes aren’t likely to become big fodder. Will The First Presidential Debate Shake Up The Race? |Sarah Frostenson (sarah.frostenson@abc.com) |September 29, 2020 |FiveThirtyEight 

This video should give Disney enough fodder to create an action-packed Dumbo/Lion King crossover sequel. Young Elephant Takes on 14 Lions and Survives! |Alex Chancey, The Daily Beast Video |November 13, 2014 |DAILY BEAST 

On the downside, it rewards them with fodder for nightmares. Jimmy Kimmel Pranks Kids (Again), Taylor Swift’s 1989 Aerobics, and More Viral Videos |The Daily Beast Video |November 9, 2014 |DAILY BEAST 

We Americans, as the postmodern conservative professor Peter Lawler has put it, refuse to see ourselves as mere "history fodder." Meet The Democrats’ Secret Savior Against Cuomo Corporatism |James Poulos |September 14, 2014 |DAILY BEAST 

There is a long history of the mammary glands as comedy fodder. Women, It's Time to Reclaim Our Breasts |Emily Shire |September 9, 2014 |DAILY BEAST 

So, after toiling away for two decades, Elba has finally crossed over from critically acclaimed actor to bona fide tabloid fodder. Idris Elba’s Battle of the Bulge: Moose Knuckles and Sexist Double Standards |Keli Goff |August 11, 2014 |DAILY BEAST 

He shall give his mind to turn up furrows, and his care is to give the kine fodder. The Bible, Douay-Rheims Version |Various 

Many of the inhabitants came out of their houses and gave it fodder, and every passer-by turned out of the way for it. A Woman's Journey Round the World |Ida Pfeiffer 

Clover, lucerne, ryegrass and similar grasses have been introduced to improve and vary the fodder. Encyclopaedia Britannica, 11th Edition, Volume 4, Part 3 |Various 

Millet and rice are the staple crops; the former furnishing food both for man and beast, for its long stalks are excellent fodder. The Cradle of Mankind |W.A. Wigram 

Praise the liberal Maruts, and may they delight on the path of this man here who praises them, like cows in fodder. Sacred Books of the East |Various