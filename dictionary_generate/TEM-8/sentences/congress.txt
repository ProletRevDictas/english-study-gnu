Pauli wrote to Jung reporting a dream he had about a physics congress with many participants. The Synchronicity of Wolfgang Pauli and Carl Jung - Issue 93: Forerunners |Paul Halpern |November 18, 2020 |Nautilus 

Historically the reelection rate for members of Congress is in the area of 95 percent. The Unbearable Whiteness of Congress |Dean Obeidallah |January 8, 2015 |DAILY BEAST 

The breakdown of the 114th Congress is 80 percent white, 80 percent male, and 92 percent Christian. The Unbearable Whiteness of Congress |Dean Obeidallah |January 8, 2015 |DAILY BEAST 

With all that said, representation of each of these respective communities has increased in the new Congress. The Unbearable Whiteness of Congress |Dean Obeidallah |January 8, 2015 |DAILY BEAST 

This Congress will welcome more women than ever before at 19 percent of the House and 20 percent of the Senate. The Unbearable Whiteness of Congress |Dean Obeidallah |January 8, 2015 |DAILY BEAST 

How far has Congress really evolved on race when in 50 years it has gone from one black senator to two? The Unbearable Whiteness of Congress |Dean Obeidallah |January 8, 2015 |DAILY BEAST 

In nine days he returned, bringing us the thanks of congress, and fresh orders. Blackwood's Edinburgh Magazine, No. CCCXXXIX. January, 1844. Vol. LV. |Various 

He was a member of the first provincial congress, and eighteen years lieutenant governor of the state of New York. The Every Day Book of History and Chronology |Joel Munsell 

Our army, under the command of General Houston, was in front of Harrisburg, to which place the congress had retreated. Blackwood's Edinburgh Magazine, No. CCCXXXIX. January, 1844. Vol. LV. |Various 

Congress declared the authority of England over the thirteen colonies abolished. The Every Day Book of History and Chronology |Joel Munsell 

During the revolutionary war he was commissary-general to the Pennsylvania division, and printer to congress. The Every Day Book of History and Chronology |Joel Munsell