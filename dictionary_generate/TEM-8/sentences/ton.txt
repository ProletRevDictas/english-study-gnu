Unfortunately, there are not a ton of studies to support the idea that BCAA supplementation improves athletic performance in the short term, meaning that drinking BCAAs before your workout likely won’t make you squat more weight or swim faster. BCAA supplements can enhance your workout, but should you take them? |Amy Schellenbaum |September 10, 2020 |Popular-Science 

While there’s not a ton tech can do to make people feel safe at crowded polling stations or up the Postal Service’s budget, tech can help with disinformation, and Microsoft is trying to do so. Microsoft’s New Deepfake Detector Puts Reality to the Test |Vanessa Bates Ramirez |September 4, 2020 |Singularity Hub 

You can afford to make those big acquisitions when you’ve got a ton of cash on hand, when you’re one of just two companies sharing a huge market. America’s Hidden Duopoly (Ep. 356 Rebroadcast) |Stephen J. Dubner |September 3, 2020 |Freakonomics 

They’ll also know exactly what supplies they need and won’t want to grapple with the tons of items that well-intentioned people send to the area. Hurricane Laura is the strongest storm to hit Louisiana in more than a century |Sara Chodosh |August 28, 2020 |Popular-Science 

In terms of search, buyers may still have tons of questions about using, or troubleshooting, your product or service. Why marketers should break SEO strategies into pre- and post-purchase |The Content Studio @ Search Engine Land |August 17, 2020 |Search Engine Land 

But there's a ton of value for me in my background and my history, and losing it would be a shame. My Week on Jewish Tinder |Emily Shire |January 5, 2015 |DAILY BEAST 

Some of the things that Jay lied about to the cops actually make a ton of sense. The Deal With Serial’s Jay? He’s Pissed Off, Mucks Up Our Timeline |Emily Shire |December 31, 2014 |DAILY BEAST 

For those in the resource world, every ton of junk that goes into a landfill represents wasted energy. Garbage In, Power Out |The Daily Beast |November 24, 2014 |DAILY BEAST 

Red squirrels cache the pinecones (saving the bears a ton of work). What It Takes to Kill a Grizzly Bear |Doug Peacock |November 23, 2014 |DAILY BEAST 

So I went in, met Michael Bay, and did the video, and it won an MTV Video Music Award and got me a ton of work. ‘Mockingjay’s’ Mastermind: Francis Lawrence on the Book vs. Movie, ISIS Parallels, and More |Marlow Stern |November 23, 2014 |DAILY BEAST 

The price of salt at one period of the long Peninsular war rose to £30 per ton, being retailed in Birmingham at 4l. Showell's Dictionary of Birmingham |Thomas T. Harman and Walter Showell 

If you think the fly-wheel is not sufficiently heavy for his engine, add half a ton more to the ring. Life of Richard Trevithick, Volume II (of 2) |Francis Trevithick 

At Thetford the cost may be put at from $20 to $25 per ton, the latter probably being nearer the average. Asbestos |Robert H. Jones 

Bergin dug a big hole behind that ole vacant shack of hisn, and buried about a ton of tin cans. Alec Lloyd, Cowpuncher |Eleanor Gates 

A ton of silk goods is worth from ten to fifteen thousand dollars. The Atlantic Monthly, Volume 17, No. 101, March, 1866 |Various