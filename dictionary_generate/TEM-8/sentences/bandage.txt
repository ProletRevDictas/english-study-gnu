So we’re now more than a year out from the release of After Hours’ first singles, all the bandages have come off, and he’s just totally botched plastic surgery. How one hit song won The Weeknd a Super Bowl halftime show |Aja Romano |February 5, 2021 |Vox 

Filled with 69 items, including bandages, dressings, gels, forceps, a thermometer, medication, wound care, and dehydration packets, this tool kit can take care of minor and major injuries or health concerns. The Gear I Carry as a Female Road-Tripper |Alex Temblador |January 24, 2021 |Outside Online 

Even as the Tide downshifted in the fourth quarter – by which time Smith wore a heavy bandage on his dislocated finger and a T-shirt on his back, the game in the bag – they rang up 621 yards. After all the disruption in college football, it was all domination for Alabama |Barry Svrluga |January 12, 2021 |Washington Post 

After cleaning up the blood and applying a bandage, he determined she was physically OK. Behind the Scenes of Emily Harrington's Historic Climb |Andrew Bisharat |November 6, 2020 |Outside Online 

It has everything you need, including bandages, an emergency blanket, duct tape, and essential medications, all weighing in at less than four ounces. Our Favorite Gear for Any Type of Traveler |Janna Irons |September 8, 2020 |Outside Online 

His arm bore a hospital bandage applied after he gave blood to try to help save his family. A Child’s Funeral in Gaza |Jesse Rosenfeld |July 18, 2014 |DAILY BEAST 

EBay has reported a 200 percent increase in bandage dress sales since spring of last year. Kim Kardashian Stopped Wearing Bodycon, and So Should You |Erin Cunningham |June 24, 2014 |DAILY BEAST 

One of the friends at the table, Aleksei Kniadliakovsky, had a fresh bandage around his head. Pussy Riot Whips Sochi |Anna Nemtsova |February 21, 2014 |DAILY BEAST 

Get in, put a bandage over the problem, submit your bill to the insurance company, and get out. Is Psychiatry Being Stumped By the Mental Illnesses It Has to Treat? |Matthew Tiffany |August 10, 2013 |DAILY BEAST 

To make matters worse, many platform offenders pair the look with an equally outdated bandage dress. Time to Abandon the Platform Heel |Misty White Sidell |June 20, 2013 |DAILY BEAST 

His left arm, rudely bandaged in a shawl, hung heavy and useless at his side, and the bandage was saturated with blood. Oliver Twist, Vol. II (of 3) |Charles Dickens 

And for those there will be a chair by the fire, and something hot, or perhaps a clean bandage. The Amazing Interlude |Mary Roberts Rinehart 

"I don't care to retrace all of mine," said Mr. Bradford, whose pale face wore a smile beneath its bandage. Gold-Seeking on the Dalton Trail |Arthur R. Thompson 

And then he raised his bandage, and finding himself in front of a house, examined it attentively. Chicot the Jester |Alexandre Dumas, Pere 

And he raised his bandage again, and, approaching the door next to that against which Bussy was standing, began again to examine. Chicot the Jester |Alexandre Dumas, Pere