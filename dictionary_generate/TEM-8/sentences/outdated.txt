This includes coronavirus-related staffing issues, outdated IT systems, delayed refunds and unprocessed returns. Tax season 2021: A tornado is coming |Michelle Singletary |February 12, 2021 |Washington Post 

Shierholz said the CBO’s estimates showed that the benefits to low-wage workers outweighed the costs, but she was critical of the calculation of those costs, saying the CBO had relied on an outdated methodology. CBO report finds $15 minimum wage would cost jobs but lower poverty levels |Eli Rosenberg |February 8, 2021 |Washington Post 

What we really are is a generation told to stick with outdated, one-size-fits-all modes of schooling that no longer apply to a world that has more ways to learn than ever before. Traditional school isn’t always the way to go, and I wish my parents had seen that earlier |Kenneth R. Rosen |February 5, 2021 |Washington Post 

I finally removed the outdated set of winter-compound all-terrain tires from my Ford Ranger and replaced them with these more snow-and-ice-specific Toyos. The Gear Our Editors Loved in January |The Editors |February 3, 2021 |Outside Online 

This put significant strain on local governments, which are often understaffed, have limited technical capabilities, and must deal with outdated tech systems. This is how America gets its vaccines |Cat Ferguson |January 27, 2021 |MIT Technology Review 

Others fear that giving them the force of regulation could be more harmful because they would become outdated quickly. How Your Pacemaker Will Get Hacked |Kaiser Health News |November 17, 2014 |DAILY BEAST 

“The problem is, some on both sides are using outdated science, some from decades ago,” Stafford continued. Rand Paul’s Plan B for Pro-Life Critics |Olivia Nuzzi |October 5, 2014 |DAILY BEAST 

So specious, in fact, that they are increasingly seen to be rationales to cover outdated forms of prejudice. Catholic University’s Harvey Milk Ban Reflects A Church In Transition |Jay Michaelson |October 3, 2014 |DAILY BEAST 

There are plenty of other outdated stereotypes in “Tom and Jerry”—sexist attitudes, for example—but Amazon chose to focus on race. Is ‘Tom and Jerry’ Really Racist? |Lizzie Crocker |October 2, 2014 |DAILY BEAST 

This makes the notion that race thwarts success increasingly outdated. Class Issues, Not Race, Will Likely Seal the Next Election |Joel Kotkin |September 7, 2014 |DAILY BEAST 

Cavalry is outdated, nowadays, but in rocky mountain country they can have uses where tanks can't go. The Invaders |William Fitzgerald Jenkins 

The crazy quilt of outdated, clumsy old buildings that was the local Starmen's Enclave. Starman's Quest |Robert Silverberg 

He recognized the latest model lie-detector, a rather outdated narco-synthesizer, a Class B Psychocomputer. Evil Out of Onzar |Mark Ganes 

You left the ship, it being outdated, battered, useless and drifting in normal interstellar where it would never be found. Spillthrough |Daniel F. Galouye 

Something in the outdated slang made him feel—almost patriotic. Pagan Passions |Gordon Randall Garrett