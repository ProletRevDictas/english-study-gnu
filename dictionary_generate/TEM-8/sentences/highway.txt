Because of protests in Virginia last year, Hanley said, state police pulled law enforcement resources away from highway safety initiatives to focus mostly on targeting the most dangerous behaviors. Traffic counts fell during the coronavirus pandemic, but road fatalities still increased |Luz Lazo |February 12, 2021 |Washington Post 

You get more cushioning while driving down apot-hole-laden road with groceries in the trunk, and a stiffer ride when you’re cruising down the highway and want maximum performance. Audi’s e-Tron GT charges up fast, but turns even faster |Stan Horaczek |February 10, 2021 |Popular-Science 

De-icing salts have contributed to bridge failures and cause cracking and other forms of weathering in highway surfaces. Fish blood could hold the answer to safer de-icing solutions during snowstorms |By Monika Bleszynski/The Conversation |February 1, 2021 |Popular-Science 

There were no immediate reports of any serious crashes on major highways in the area. Icy road conditions hit some parts of D.C. region |Dana Hedgpeth |February 1, 2021 |Washington Post 

As expected, the highways look like they are in good shape, but secondary and side roads are snow-covered or getting that way. Let it snow! A few inches likely to accumulate today; more tomorrow into Tuesday? |Brian Jackson, Dan Stillman |January 31, 2021 |Washington Post 

It was a brick wall that we turned into the on-ramp of a highway. Coffee Talk with Fred Armisen: On ‘Portlandia,’ Meeting Obama, and Taylor Swift’s Greatness |Marlow Stern |January 7, 2015 |DAILY BEAST 

Cruce operates the Iron Hill Campground on the other side of the highway. The 7-Year-Old Plane Crash Survivor’s Brutal Journey Through the Woods |James Higdon |January 7, 2015 |DAILY BEAST 

Hundreds of cops saluting as the bodies were rolled out with a full escort by highway patrol. Anger at The Cop Killer - And The Police |M.L. Nestel |December 21, 2014 |DAILY BEAST 

Last week members helped shut down the West Side Highway, a major roadway in New York. Eric Garner Protesters Have a Direct Line to City Hall |Jacob Siegel |December 11, 2014 |DAILY BEAST 

Highway safety flares provided light as the clans joined by loss sought solace in prayer and song. The Cleveland Cops Who Fired 137 Shots and Cried Victim |Michael Daly |December 2, 2014 |DAILY BEAST 

An automobilist must exercise reasonable or ordinary care to avoid injury to other persons using the highway. Putnam's Handy Law Book for the Layman |Albert Sidney Bolles 

This rule however does not apply to travelers walking along a rural highway. Putnam's Handy Law Book for the Layman |Albert Sidney Bolles 

He must keep a reasonably careful lookout for other travelers in order to avoid collision; also for defects in the highway. Putnam's Handy Law Book for the Layman |Albert Sidney Bolles 

A big touring car stood in the narrow lane, headed toward the broad highway from which Jessie and Amy had come. The Campfire Girls of Roselawn |Margaret Penrose 

Go not out into the fields, nor walk in the highway: for the sword of the enemy, and fear is on every side. The Bible, Douay-Rheims Version |Various