Driven by young Burmese who came of age with internet access and the comparative freedoms of the last decade, the civil disobedience movement is being organized online, particularly on Facebook, which is hugely popular in the country. Myanmar's Creatives Are Fighting Military Rule With Art—Despite the Threat of a Draconian New Cyber-Security Law |Suyin Haynes |February 12, 2021 |Time 

This image has popped up in other comparative contexts, but this is by far the most visible this photo has ever been. She Photographed Police Abuse at a 2014 BLM March Then Watched the Image Go Viral During Capitol Riot |by Lisa Larson-Walker |January 11, 2021 |ProPublica 

Aircraft or spacecraft—either way it was thousands or even millions of pounds of explosive, high-speed hardware controlled by a comparative fly-weight bit of human. Why Chuck Yeager Claimed He Had No 'Right Stuff' |Jeffrey Kluger |December 9, 2020 |Time 

Forty-six states have abandoned that standard in favor of a comparative negligence standard, which dictates how responsibility for a collision would be shared between those involved. D.C. Council weighs making it easier for e-scooter riders to collect damages after crashes |Luz Lazo |November 20, 2020 |Washington Post 

Despite that comparative drop-off, Rapinoe still finished in the top five in the NWSL in goals per 90 minutes in 2017 and 2018. Sue Bird And Megan Rapinoe Have Gotten Better Together |Jenn Hatfield |November 10, 2020 |FiveThirtyEight 

Egypt has a comparatively low number of HIV cases compared to the rest of Africa, with just 11,000 infected people nationwide. Sisi Is Persecuting, Prosecuting, and Publicly Shaming Egypt’s Gays |Bel Trew |December 30, 2014 |DAILY BEAST 

The administration is, however, keeping expectations for enrollment numbers comparatively modest this time around. The White House Reactivates Its Army of Celebrity Obamacare Endorsers |Asawin Suebsaeng |November 17, 2014 |DAILY BEAST 

Comparatively, during those same years CBP flagged 21 percent of migrants from other countries for credible fear interviews. U.S. Extends Protection To Honduran Immigrants |Caitlin Dickson |October 16, 2014 |DAILY BEAST 

We ignore the comparatively free elections held in Iran, elections that bring the likes of a Hassan Rouhani to the presidency. Here’s What the U.S. Has to Do to Deal With the Mad Middle East |Leslie H. Gelb |July 16, 2014 |DAILY BEAST 

In post-bust New York, rents were comparatively low and so the model worked. The Cupcake Boom’s Sugar High Finally Crashes |Daniel Gross |July 8, 2014 |DAILY BEAST 

They are still comparatively supple, and any misplaced pinnæ may be re-arranged without any difficulty. How to Know the Ferns |S. Leonard Bastin 

Wheat gives place to Rye about the same time, and the Potato, at first comparatively rare, becomes universal. Glances at Europe |Horace Greeley 

Non-meerschaum smokers may not know what a delicate task this is, but once well begun the rest is comparatively easy. Fifty Years of Railway Life in England, Scotland and Ireland |Joseph Tatlow 

The female elephant killed by our hunters at this time was a comparatively small one. Hunting the Lions |R.M. Ballantyne 

Once satisfied that it was just and honourable, and it was comparatively child's work to arrange the modus operandi. Blackwood's Edinburgh Magazine, No. CCCXXXIX. January, 1844. Vol. LV. |Various