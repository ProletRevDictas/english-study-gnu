With World War I raging, a band struck up the song during the seventh-inning stretch, resulting in thunderous applause. The Dallas Mavericks will resume playing the national anthem before games |Timothy Bella |February 10, 2021 |Washington Post 

When Stewart walks into an upscale Pittsburgh steakhouse with a female date, the other diners erupt in applause — a standing ovation because he’s out on the town with a woman. Thank you, Kordell Stewart, for thoughtful response to ‘the rumor’ |Kevin Naff |February 5, 2021 |Washington Blade 

She also, according to Republicans in the room, apologized for putting her colleagues in a difficult spot — prompting a round of applause. House to vote on removing GOP’s Marjorie Taylor Greene from her committees |Felicia Sonmez, John Wagner, Colby Itkowitz |February 4, 2021 |Washington Post 

It connects directly to a smartphone and allows wannabe broadcasters to add effects like applause and musical cues to their productions. Gifts that make video calls look and feel more glamorous |Stan Horaczek |December 9, 2020 |Popular-Science 

Díaz-Canel, therefore, did not have to win the applause of the masses with government proposals. My vote on behalf of those who are excluded |Yariel Valdés González |October 27, 2020 |Washington Blade 

He led the packed cathedral in applause for Ramos and Liu and asked Bratton to bring a message to the men and women of the NYPD. 'Please Don't Die!': The Frantic Battle to Save Murdered Cops |Michael Daly |December 22, 2014 |DAILY BEAST 

A call from the stage for President Peña Nieto to resign drew the loudest applause. Mexican Protesters Look to Start a New Revolution |Jason McGahan |November 21, 2014 |DAILY BEAST 

And one person started clapping, and then the whole room erupted in applause. ‘Pride’: The Feel-Good Movie of the Year, and the Film Rupert Murdoch Doesn’t Want You to See |Marlow Stern |October 13, 2014 |DAILY BEAST 

Their speech was met “very warmly, with applause,” according to bishops briefing the press afterwards. The Vatican's Same-Sex Synod: The Bishops Hear About Reality. Do They Listen? |Barbie Latza Nadeau |October 12, 2014 |DAILY BEAST 

“For someone to scream, that is like applause to us,” Harlacher said. New York’s Scariest Night Out: The Ghosts, Rats, and Lunatics of ‘Nightmare New York’ |Justin Jones |October 4, 2014 |DAILY BEAST 

General Lachambre, as the hero of Cavite, followed to receive the applause which was everywhere showered upon him in Spain. The Philippine Islands |John Foreman 

Frantic applause, several times repeated, which drowned the voice of the orator. The Philippine Islands |John Foreman 

When he came out on the stage the applause was tremendous, and enough in itself to excite and electrify one. Music-Study in Germany |Amy Fay 

A boy on the stage danced very finely and obtained much applause. The Book of Anecdotes and Budget of Fun; |Various 

A hideous yell of applause rose from the multitude, and again he plunged his saber into the carriage. Madame Roland, Makers of History |John S. C. Abbott