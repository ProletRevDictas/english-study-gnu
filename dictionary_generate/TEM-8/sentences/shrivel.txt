Over time, the fruit will start to soften and shrivel, forming an elegantly craggy surface with deep grooves in the folded skin. The Art of Hoshigaki at Home |Jamie Feldmar |January 7, 2021 |Eater 

To see, say, frost-shriveled red rose hips near a clump of giant snowdrops in December bloom is wonderful, but the sight also seems to signify winter’s altered state. Climate change has altered the winter season and the gardener’s sense of it |Adrian Higgins |January 6, 2021 |Washington Post 

Some females will take on multiple live-in boyfriends, with her body providing them nutrients as they shrivel into lazy sacks of sperm to fertilize her eggs. Egg yolk color doesn’t mean what you think it does |PopSci Staff |October 15, 2020 |Popular-Science 

As stores shuttered and revenue shriveled, it would have been easy for brands and media companies across the board to simply focus on keeping the doors open. Digiday Resilience Awards winners: How brands protected their workers and gave back to the world in a time of crisis |Digiday Awards |October 8, 2020 |Digiday 

Delayed action will only allow more small businesses to shrivel and die, and our streets to therefore become less welcoming and safe. Myths and Shame Shouldn’t Guide Cannabis Regulations |John Bertsch |September 8, 2020 |Voice of San Diego 

Ours is the Caiman model, a 6x6 behemoth that weighs in at over 15 tons and makes Humvees shrivel up with feelings of inadequacy. Why Does My Kids’ Elementary School Need a Tank? |Andy Hinds |September 13, 2014 |DAILY BEAST 

And then you had the early feminists who were making their young husbands shrivel up because they could never do anything right. Gail Sheehy Books Passage to the Past |Lizzie Crocker |September 3, 2014 |DAILY BEAST 

For some time, those contractors have faced the reality of a GOP increasingly willing to shrivel defense budgets. The Dems’ Fork in the Road |David Frum |November 12, 2013 |DAILY BEAST 

Presidential power, like a muscle, can strengthen if exercised effectively—or shrivel. Obama and Syria: Fighting the Wimp Factor |Gil Troy |September 18, 2013 |DAILY BEAST 

However, if asked to bet whether right-wing apoplexy will grow or shrivel, the smart money obviously has to say the former. The Best Republican Efforts Are Not Enough to Defund Obamacare |Michael Tomasky |August 23, 2013 |DAILY BEAST 

The man on the floor would shrivel up and say, "Oh, please don't, Bill; I hain't ever goin' to tell." Adventures of Huckleberry Finn, Complete |Mark Twain (Samuel Clemens) 

Or what is it wholly unsettlesThy sequence of shower and shine, And maketh thy pushings and petalsTo shrivel and pine? The Book of Humorous Verse |Various 

They says his voice is like thunder, an' lightnin' shoots fr'm his eyes that wud shrivel th' likes iv ye an' me to a cinder.' Mr. Dooley Says |Finley Dunne 

As soon as the fish are nearly ready for spawning, all their digestive parts shrivel up, so that they can't eat. The Boy With the U. S. Fisheries |Francis Rolt-Wheeler 

It looked as though the towns would shrivel up, because of the tremendously high wages demanded by the men who were needed there. Historic Adventures |Rupert S. Holland