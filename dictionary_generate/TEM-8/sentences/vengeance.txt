“Worry is back with a vengeance,” said Grant Steadman, president of North America for Dunnhumby, in a press release. Americans are more worried than ever about coronavirus—but are doing less to combat it |Beth Kowitt |December 17, 2020 |Fortune 

In that pocket, tree limbs snapped with a vengeance, creating 100,000 power outages. Ice storms: Inside wintertime’s dreaded, frozen mess |Jeffrey Halverson |December 2, 2020 |Washington Post 

Ezio Auditore’s initial arc is one of vengeance, but we see him grow beyond that. All the ‘Assassin’s Creed’ games, ranked |Elise Favis, Gene Park |November 11, 2020 |Washington Post 

A strict lockdown successfully contained the disease, but it came back with a vengeance in the fall. 50 million world Covid-19 cases: The biggest outbreaks, explained |Christina Animashaun |November 9, 2020 |Vox 

He’s a relentless force of holy retribution, mysterious in his origins and unrelenting in his taste for vengeance against the demons who’ve wronged him. Doom Eternal Is the Best Doom Game Ever Made, Period |Matthew Gault |March 19, 2020 |Time 

But all these groups are reaching a point where vengeance takes priority over politics or, much less, public relations. Pakistani School Killers Want to Strike the U.S. |Sami Yousafzai, Christopher Dickey |December 17, 2014 |DAILY BEAST 

Despite a dizzying number of women coming forward against her husband, Camille Cosby refuses to sharpen her blade of vengeance. Why Didn’t Camille Dump Bill Cosby? |Amanda Marcotte |December 17, 2014 |DAILY BEAST 

U.S. airstrikes continue, but militants from the so-called Islamic State are still attacking with a vengeance on every front. Did ISIS Attack Kobani from Turkey? |Jamie Dettmer |November 30, 2014 |DAILY BEAST 

In January, if the GOP wins Senate control, he will go after Obama and the EPA with a vengeance. If You Think D.C. Is Awful Now, Wait Until Wednesday |Jonathan Alter |November 4, 2014 |DAILY BEAST 

However, there is at least a sense that security is back on the agenda with a vengeance. Shocked by Ukraine Violence, NATO Prepares to Face Down Putin |Leo Cendrowicz |October 12, 2014 |DAILY BEAST 

He could not bear to open his dreadful situation to his Uncle David, nor to kill himself, nor to defy the vengeance of Longcluse. Checkmate |Joseph Sheridan Le Fanu 

The teeth of beasts, and scorpions, and serpents, and the sword taking vengeance upon the ungodly unto destruction. The Bible, Douay-Rheims Version |Various 

And they sought out all iniquities, till vengeance came upon them, and put an end to all their sins. The Bible, Douay-Rheims Version |Various 

Thy nakedness shall be discovered, and thy shame shall be seen: I will take vengeance, and no man shall resist me. The Bible, Douay-Rheims Version |Various 

If he had set out to arouse emotion in these two sluggish breasts he had done so with a vengeance. The Joyous Adventures of Aristide Pujol |William J. Locke