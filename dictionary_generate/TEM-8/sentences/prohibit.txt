There are several state laws on the books prohibiting people from violating public health orders with fines of up to $1,000. Despite Crackdown Announcement, Not Much COVID-19 Enforcement Is Happening |Jesse Marx |February 11, 2021 |Voice of San Diego 

On Thursday, LeBlanc also unveiled a policy that will prohibit university departments, student groups or guests from selling or giving away certain plastics, including beverage bottles and eating utensils. George Washington University commits to single-use-plastic ban |Lauren Lumpkin |February 11, 2021 |Washington Post 

That’s because of a US federal law known as the Passenger Vessels Service Act that prohibits foreign-registered ships from sailing between two American ports without stopping at a foreign port. The cruise industry has received yet another blow, this time from Canada |Karen Ho |February 8, 2021 |Quartz 

By law, the organization is prohibited from attempting to influence policy. “We Have Counties in Deep Trouble”: Oregon Lawmakers Seek to Reverse Timber Tax Cuts That Cost Communities Billions |by Rob Davis, The Oregonian/OregonLive, and Tony Schick, Oregon Public Broadcasting |February 5, 2021 |ProPublica 

Studios began postponing their releases or moving them to streaming services, governments limited or prohibited attendance, and consumers largely stayed home. Forget investors: AMC itself may have been bailed out by the actions of wallstreetbets |Steven Zeitchik |February 2, 2021 |Washington Post 

At that point, the Library of Congress can once again decide to prohibit consumers from unlocking their cell phones. Nazis, Sunscreen, and Sea Gull Eggs: Congress in 2014 Was Hella Productive |Ben Jacobs |December 29, 2014 |DAILY BEAST 

No problem—Congress is about to prohibit us from buying them. Why Does the USA Depend on Russian Rockets to Get Us Into Space? |P. J. O’Rourke |June 22, 2014 |DAILY BEAST 

His first demand was that the country's official constitution be rewritten to prohibit extradition. Pablo Escobar’s Private Prison Is Now Run by Monks for Senior Citizens |Jeff Campagna |June 7, 2014 |DAILY BEAST 

And the health law might not prohibit it, opening a door to potential erosion of employer-based coverage. Hold On to Your Health Care |Kaiser Health News |May 7, 2014 |DAILY BEAST 

And yet Louisiana does not prohibit a far stranger peccadillo: sex with corpses. Our Dumb Puritan Laws: Sex Bans and Illegal Adultery |Kevin Bleyer |April 20, 2014 |DAILY BEAST 

He should not consider himself to be called upon to prohibit only some practices clearly evinced to be sinful. The Ordinance of Covenanting |John Cunningham 

The state may prohibit a telegraph company from transmitting racetrack news. Putnam's Handy Law Book for the Layman |Albert Sidney Bolles 

Small wonder that the rules of the Board of Control prohibit the use of the stern blast under one thousand feet. Astounding Stories, May, 1931 |Various 

After having thus prepossessed our minds, they next prohibit our examining the things so important to be known. Letters To Eugenia |Paul Henri Thiry Holbach 

As, in a savage state, most possessions are those which are useful in war, he would prohibit theft. Ancient Faiths And Modern |Thomas Inman