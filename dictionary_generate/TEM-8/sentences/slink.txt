Meanwhile, in the distance, a group of wild horses ran right across the road as a coyote slunk out from a nearby ditch. Seeing Big Vistas at Theodore Roosevelt National Park |Emily Pennington |January 21, 2021 |Outside Online 

Four years earlier, Senators Cruz, Paul, Marco Rubio and Lindsey Graham similarly chased the presidency but slinked back to the Senate in defeat. In the Long Game, Even Insurrection May Not Disqualify Presidential Hopes |Philip Elliott |January 8, 2021 |Time 

The bodily systems that thrived and repaired themselves to ensure that we pass on healthy genes cease to function well and leave us to slink to the finish line the best we can. Outwitting the Grim Reaper - Issue 94: Evolving |Kevin Berger |December 23, 2020 |Nautilus 

Mamet slinks away with the entire show, as she manages to deliver the chill and severity of an attorney looking out for her friend while never seeming soulless. The absurd, sadistic joy of The Flight Attendant |Alex Abad-Santos |December 18, 2020 |Vox 

One by one I pick them off, and slink back into the shadows. In Ghost of Tsushima, Honor Is Your Greatest Foe |Patrick Lucas Austin |July 14, 2020 |Time 

Democrats can't slink away, or crouch, or cut and run against their own record. Democrats Must Run on Obamacare in November |Robert Shrum |March 17, 2014 |DAILY BEAST 

Jones, ever the charmer, proceeds to slink behind the desk and begin fake-hammering away at the keyboard. Felicity Jones Is Bound for Stardom |Marlow Stern |December 29, 2013 |DAILY BEAST 

We began to slink outside, tentatively crossing the terrace. Our Visit From Irene |India Hicks |August 27, 2011 |DAILY BEAST 

Truth is a torch, but one of enormous size; so that we slink past it in rather a blinking fashion for fear it should burn us. Pearls of Thought |Maturin M. Ballou 

And then I can paddle over to town nights, and slink around and pick up things I want. Adventures of Huckleberry Finn, Complete |Mark Twain (Samuel Clemens) 

The 199 Earls bade the whole multitude of their array slink down alongside the river. The Sagas of Olaf Tryggvason and of Harald The Tyrant (Harald Haardraade) |Snorri Sturluson 

In the face of such a defiance the clerical party must fight fairly, or slink away as cravens. Ancient Faiths And Modern |Thomas Inman 

Poor Patti would get up in obedience to the order and slink out of the door with her ears drooping. The Animal Story Book |Various