A Puritan minister incited fury by pushing inoculation against a smallpox epidemicCritics argued it was playing God, and it was banned in several colonies. Mandatory immunization for the military: As American as George Washington |Gillian Brockell |August 26, 2021 |Washington Post 

Onesimus, an enslaved man in Boston, taught the procedure to Puritan minister Cotton Mather, who in turn urged doctors to inoculate the public during a 1721 smallpox outbreak. Vaccine hesitancy is nothing new. Here’s the damage it’s done over centuries |Tara Haelle |May 11, 2021 |Science News 

A Puritan minister incited fury by pushing inoculation against a smallpox epidemicAcross the Charles River in Boston, where the smallpox outbreak had begun, the board of health chairman wasn’t so mild. Smallpox ‘virus squads’ and the mandatory vaccinations upheld by the Supreme Court |Gillian Brockell |April 1, 2021 |Washington Post 

Indeed, puritan Japan is decades behind the puritan United States when it comes to sex. Japan’s Hypocritical Vagiphobia |Lizzie Crocker |July 16, 2014 |DAILY BEAST 

At least in premodern Europe and Puritan North America, witch-hunting follows certain patterns. Will Saudi Arabia Execute Guest Workers for 'Witchcraft'? |Michael Schulson |March 29, 2014 |DAILY BEAST 

Like the Puritan ancestors he never succeeded in escaping, he found fault with just about everything, especially himself. The Man Who Knew Russia Best: George Kennan’s Revealing Diaries |James A. Warren |March 10, 2014 |DAILY BEAST 

Eventually, Charles I will be overthrown, and the puritan dictator Oliver Cromwell will take power. ‘A Field in England’ Is a Psychedelic Cinematic Trip |Andrew Romano |February 9, 2014 |DAILY BEAST 

The mindsets of both Cavalier and Puritan took root in the New World, and the experiment launched in 1776 continues. America’s Long-Simmering, Semi-Civil Civil War |Lloyd Green |October 2, 2013 |DAILY BEAST 

Pipes continued to appear upon the stage until its abolition (in company with the Prayer Book) by the Puritan rulers. Tobacco; Its History, Varieties, Culture, Manufacture and Commerce |E. R. Billings. 

The Puritan Mission has civilised him and hundreds on hundreds more, and I wish the parsons had done just half as much. The Chequers |James Runciman 

The persecution under which Jonson suffered was due to the steady growth of Puritan principles. A Cursory History of Swearing |Julian Sharman 

The fires of the Puritan faction had smouldered out; those of the Jacobite frenzy had hardly had time to rekindle. A Cursory History of Swearing |Julian Sharman 

At this period the alchemist is represented by his descendant as a Puritan impregnated with the secret doctrine of Robert Fludd. Devil-Worship in France |Arthur Edward Waite