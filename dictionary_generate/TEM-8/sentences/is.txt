At least for now, Modern Family is taking the less-is-more approach, and resisting turning it into The Lily Show. Stop Hating on ‘Modern Family’ (But Also Stop Giving It Emmys) |Jason Lynch |October 15, 2014 |DAILY BEAST 

But instead of a witty pop song, we have yet more woe-is-me-feel-my-pain from an overpaid, over-cosseted celebrity. Why We Should Hate 'Haters Gonna Hate' |Tim Teeman |August 25, 2014 |DAILY BEAST 

Others, such as Redemption Rye, present their MGP whiskey as-is. Your ‘Craft’ Rye Whiskey Is Probably From a Factory Distillery in Indiana |Eric Felten |July 28, 2014 |DAILY BEAST 

The book provides a no-detail-is-too-small approach to the scandal and its players. Speed Read: The Juiciest Bits From a New Book on the Duke Lacrosse Scandal |William O’Connor |April 8, 2014 |DAILY BEAST 

Despite such enforced breaks, Cruz wowed with his the-end-is-nigh message. Ted’s Excellent Adventure: How Cruz Rocked the Value Voters Summit |Michelle Cottle |October 11, 2013 |DAILY BEAST 

"Buy something for your wife that-is-to-be," he said to his grand-nephew, as he handed him the folded paper. The Pit Town Coronet, Volume I (of 3) |Charles James Wills 

She was very angry with Crozier, for it was absurd, that look of deprecating homage, that "Hush-she-is-coming" in his eyes. You Never Know Your Luck, Complete |Gilbert Parker 

This has not been the case; our soldiers have been received as enemies, our military honor is-engaged. Rule of the Monk |Giuseppe Garibaldi 

Oh, misery!Is wedlock treason to that purity,Which is the jewel and the soul of wedlock?Elizabeth! The Saint's Tragedy |Charles Kingsley 

Thus they encamped near the conifer, and called the place Toha-a-muk-is after the spruce they were afraid to touch. Indian Legends of Vancouver Island |Alfred Carmichael