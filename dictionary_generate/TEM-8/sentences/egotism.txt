Miranda keeps a sort of tender distance away from Larson’s perspective, so that we have room to critique both his egotism and his music, which is juvenile, frequently mediocre, and only occasionally brilliant. The intertwined legacies of Jonathan Larson and Lin-Manuel Miranda |Constance Grady |November 19, 2021 |Vox 

Morgan chalked this up to a combination of male pride and egotism. Tarzan Wasn’t for Her - Issue 100: Outsiders |Erika Lorraine Milam |June 2, 2021 |Nautilus 

For all his egotism and irascibility, Churchill was a good man as well as a great one. Boris Johnson’s Churchill Man Crush |Michael F. Bishop |November 22, 2014 |DAILY BEAST 

It also protects the individual against egotism and delusions of grandeur. New Year’s Reading List: Books to Transform Your Sad Life |David Masciotra |January 1, 2014 |DAILY BEAST 

Instead of displaying what would have been, in context, a healthy egotism as the mug fell, Leno looked as vulnerable as a child. Jay Leno's Painful Comeback |Lee Siegel |March 2, 2010 |DAILY BEAST 

Success in our politics often requires a voracious, antinomian egotism, a sense that rules are for others. Palin Has Really Gone Rogue |Michelle Goldberg |July 2, 2009 |DAILY BEAST 

As regards money, from the moment I left Russia I have not ceased to reproach myself for my unfeeling egotism. The Life &amp; Letters of Peter Ilich Tchaikovsky |Modeste Tchaikovsky 

Is she more exempt from egotism, does she dislike others less, and has she fewer worldly affections? The 'Characters' of Jean de La Bruyre |Jean de La Bruyre 

To their last day Jenkins's clients went about, showed themselves, cheated the devouring egotism of the crowd. The Nabob |Alphonse Daudet 

He knew that the old man had no sentiments beyond egotism, and a family pride which mainly, if not entirely, sprang from it. Overland |John William De Forest 

Compromise in small or great seemed cowardice, and there was no doubt a strain of egotism in his obstinacy. The Life of Mazzini |Bolton King