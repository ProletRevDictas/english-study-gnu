Faced with that handicap, even with good writing and performances, it’s hard for a movie to be anything but a bummer. Wahlberg walks for redemption in disappointing ‘Joe Bell’ |John Paul King |July 22, 2021 |Washington Blade 

If the horses hold true to form, this one won’t be tough to handicap. Road to the Kentucky Derby arrives at Wood Memorial, Santa Anita Derby and Blue Grass Stakes |Neil Greenberg |April 2, 2021 |Washington Post 

Floor plates will extend from the center to the outside perimeter in various configurations, offering what looks to be a warren of green zones and sitting areas, as well as handicap access to the exterior of the building. The Helix is a distraction. Amazon’s new headquarters will change more than just its Arlington neighborhood. |Philip Kennicott |February 18, 2021 |Washington Post 

That could prove a severe handicap as the president-elect’s team prepares to take office amid a slew of threats from digital adversaries, including Russia, China and Iran. Georgia announces hand audit |Felicia Sonmez, Colby Itkowitz, John Wagner, Derek Hawkins |November 12, 2020 |Washington Post 

Dwayne’s at a little bit of a handicap because he’s only been in the system a year, where Kyle’s been in it for three. Dwayne Haskins remains Washington’s third-string QB, but Ron Rivera hasn’t given up on him |Nicki Jhabvala |November 4, 2020 |Washington Post 

“Lack of forward firing ordnance in a CAS supporting aircraft is a major handicap,” he added. New U.S. Stealth Jet Can’t Fire Its Gun Until 2019 |Dave Majumdar |December 31, 2014 |DAILY BEAST 

Unprepared, and a laughingstock because of his handicap, Yarvi is bullied on every front—even by his mother. A Fantasy Titan Invades the YA Kingdom |William O’Connor |July 18, 2014 |DAILY BEAST 

If the rest of us have had trouble catching up to Robespierre and crew, well, we are starting from a bit of a handicap. Why Deny the Obvious: Hollywood’s Backward Stance on Abortion |Teo Bugbee |June 10, 2014 |DAILY BEAST 

The handicap, after some needling back and forth, was fixed at eight strokes. Portrait of the Consummate Con Man |John Lardner |May 17, 2014 |DAILY BEAST 

McLean listened to the stranger's mild appeal for a ten-stroke handicap. Portrait of the Consummate Con Man |John Lardner |May 17, 2014 |DAILY BEAST 

An absence of ducats, coupled with the necessity of getting my handicap down to ten, has prevented my speaking ere this. First Plays |A. A. Milne 

We have laboured under a terrible handicap owing to an almost fatal departure from the Swadeshi spirit. Third class in Indian railways |Mahatma Gandhi 

The event of the season was the handicap race for the Cup given by the Queen, which took place on August 10. Yachting Vol. 2 |Various. 

He might yet accomplish big things although he was under a terrific handicap—and he might not. The Winning Clue |James Hay, Jr. 

It is a great handicap having no one to look after things down there. Mount Everest the Reconnaissance, 1921 |Charles Kenneth Howard-Bury