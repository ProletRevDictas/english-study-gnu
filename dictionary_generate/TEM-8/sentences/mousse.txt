Dessert is a rich mousse made from donated chocolate and almost-past-its-prime cream. Massimo Bottura Wants You to Stop Wasting Your Food |Aryn Baker |January 7, 2022 |Time 

For two quarters each, we eat like queens—chocolate mousse on a sugar cone for her, hot fudge swirled through banana ice cream for me. How Ice Cream Became My Own Personal Act of Resistance |Taylor Harris |November 10, 2021 |Time 

It all comes together to make a beautiful disc of mousse on soft milk bread, without collapsing into any sort of nonsense about authenticity or purity. The Four Cookbooks That Got Me Out of My Pandemic Cooking Rut |Max Watman |June 25, 2021 |The Daily Beast 

Her mother makes a dozen or so cakes, most in fruit flavors, and I could make a case for every slice on display — raspberry, apricot, caramel mousse — that I’ve tried. This new Azerbaijani bakery offers stories as good as its pastries |Tom Sietsema |February 26, 2021 |Washington Post 

In the bathroom she wet my hair and then puffed it out with some mousse. Inside Gaddafi’s Harem: The Story of a Girl’s Abduction |Annick Cojean |August 29, 2013 |DAILY BEAST 

Lay the smoked salmon slices out, on the work surface, fill with a spoonful of the mousse and roll up to make roulades. We Are Off to the Races!! |Lydia Brownlow |June 7, 2011 |DAILY BEAST 

This list will also likely include petit fours, mini éclairs, trifle, and chocolate and lemon mousse. Royal Wedding Plans So Far for Prince William |Barbie Latza Nadeau, Jacqueline Williams |February 22, 2011 |DAILY BEAST 

Banana ice cream, frozen chocolate mousse, a web of spun sugar, almond cake, chocolate crunches. High-Concept Desserts from Star Chefs |Jennie Yabroff |January 15, 2011 |DAILY BEAST 

A summer meal might include gougéres, gazpacho, tuna persille, chicken, two cheese courses, and chocolate mousse. A Burgundy Wine Feast |Sophie Menin |July 4, 2010 |DAILY BEAST 

Malgr les efforts du bateau de sauvetage et des lignes envoyes au moyen du fusil porte-amarre, quatre hommes et le mousse ont pri. Contes Franais |Douglas Labaree Buffum 

As the pads retain cold as well as they do heat, the ice does not melt, and the mousse gradually freezes itself. Living on a Little |Caroline French Benton 

Last of all came the vanilla mousse, each glass topped by a big strawberry. Living on a Little |Caroline French Benton 

When cream was used, it was made into mousse, and of course frozen in the tireless stove. Living on a Little |Caroline French Benton 

In addition to being attractive, caramel mousse is so delicious that it appeals to practically every one. Woman's Institute Library of Cookery, Vol. 4 |Woman's Institute of Domestic Arts and Sciences