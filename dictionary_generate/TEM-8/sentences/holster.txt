The pistol was in a distinctive holster depicting the American flag and the words “We the People.” Man accused of leaving loaded gun on Capitol grounds during Jan. 6 riot |Rachel Weiner |August 3, 2022 |Washington Post 

Look for a bear spray canister or horn that comes with a belt holster for easy, hands-free carrying and access at a moment’s notice. 8 Ways to Stay Safer Running Alone |dlewon |August 1, 2022 |Outside Online 

While the sergeant was speaking with Golden, he said he noticed the bottom of a holster peeking out from under Golden’s jacket, according to the filing. DEA Imposter ‘Tricked’ Woman Into Bogus Training Program, Feds Say |Justin Rohrlich |February 4, 2022 |The Daily Beast 

Baldwin was practicing drawing the gun from its holster when it went off. Cinematographer Halyna Hutchins Was Killed by a Prop Gun on the Film Set of Rust. Here's What We Know So Far |Andrew R. Chow |October 22, 2021 |Time 

He requested that his handcuffs be adjusted and then made “a clear attempt” to snatch another officer’s Glock 17 from the holster, prosecutors say. Baltimore Cop Stashed 15-Year-Old Stepson’s Body in Wall: Prosecutor |Allison Quinn |July 8, 2021 |The Daily Beast 

In Quarles, police captured a rape suspect, noticed he was wearing an empty holster, and asked him where his gun was. Miranda Warning Withheld From Bombing Suspect for More Than Two Days |Paul Campos |April 23, 2013 |DAILY BEAST 

Thomas testified that he noticed that Herring was wearing a gun holster around his ankle. Lesley Herring: The Hollywood Murder Case With No Body |Christine Pelisek |March 28, 2013 |DAILY BEAST 

I can imagine walking into a mall and feeling the weight of a revolver in an ankle holster. Don’t Arm America: A Soldier’s Reply to Connecticut Shooting |Tony Schwalm |December 18, 2012 |DAILY BEAST 

I took off the sheath, the holster, so to speak, of the taser and I loaded the taser. L.A. Riots Anniversary: Stacey Koon’s Disturbing Testimony |Christine Pelisek |April 28, 2012 |DAILY BEAST 

After affixing the Crimson Trace laser sight, like the one Perry uses, the gun required a larger holster. Ranger Rick and the Coyote |Carol Flake Chapman |September 10, 2011 |DAILY BEAST 

The guard who had ridden up in front snatched at his shoulder holster as he turned in the direction of Delancy's fire. Hooded Detective, Volume III No. 2, January, 1942 |Various 

The man lowered his revolver, tucked it away in the swinging holster, and turned to Jos. Motor Matt's "Century" Run |Stanley R. Matthews 

He had dropped the reins and was struggling to draw a revolver from his right holster with his left hand. The Courier of the Ozarks |Byron A. Dunn 

The white-haired man deftly removed Talpers's revolver from its holster and put it on the table. Mystery Ranch |Arthur Chapman 

From his right side hung a leather holster that held the bolts needed for his weapon. The Pilgrim's Shell or Fergan the Quarryman |Eugne Sue