On March 20, 2019, fish caught in Ohio’s Cuyahoga River were declared safe to eat by federal environmental regulators. 51 Years Later, the Cuyahoga River Burns Again |Wes Siler |August 28, 2020 |Outside Online 

The new work provides important context for data being collected on fish stocks. Species may swim thousands of kilometers to escape ocean heat waves |Carolyn Gramling |August 10, 2020 |Science News 

Sims and Berni wonder how these ideas might be explored in vertebrates like mice and zebra fish. Random Search Wired Into Animals May Help Them Hunt |Liam Drew |June 11, 2020 |Quanta Magazine 

For example, fishes who start living and evolving in unlit caves often lose their eyes, because the costs of developing them outweigh their advantages. Evolution: Why It Seems to Have a Direction and What to Expect Next |Matthew Wills |June 10, 2020 |Singularity Hub 

This makes the online world an exceptionally volatile environment, where big fishes swallow the small ones. SEO on a shoestring budget: What small business owners can do to win |Ali Faagba |June 4, 2020 |Search Engine Watch 

When Chérif got out of prison, he worked at the fish counter of a supermarket. France Mourns—and Hunts |Nico Hines, Christopher Dickey |January 8, 2015 |DAILY BEAST 

“The government just wanted to catch the big fish [in the Juarez cartel] and they ignored everything in between,” Lozoya said. An Informant, a Missing American, and Juarez’s House of Death: Inside the 12-Year Cold Case of David Castro |Bill Conroy |January 6, 2015 |DAILY BEAST 

Kocurek documented the scene with notes and diagrams, and called the U.S. Fish and Wildlife Service. Two Texas Regulators Tried to Enforce the Rules. They Were Fired. |David Hasemyer, InsideClimate News |December 9, 2014 |DAILY BEAST 

A U.S. Fish and Wildlife officer corroborated another account. Two Texas Regulators Tried to Enforce the Rules. They Were Fired. |David Hasemyer, InsideClimate News |December 9, 2014 |DAILY BEAST 

A Fish and Wildlife special agent collected the bodies of two birds at the site, a redhead duck and a mourning dove. Two Texas Regulators Tried to Enforce the Rules. They Were Fired. |David Hasemyer, InsideClimate News |December 9, 2014 |DAILY BEAST 

He must be The saltest fish that swims the sea.And, oh!He has a secret woe! Boys and Girls Bookshelf; a Practical Plan of Character Building, Volume I (of 17) |Various 

He looked up from his fish and replied, somewhat cuttingly, "By contesting a borough and getting elected." Ancestors |Gertrude Atherton 

Smoking, the angry and fuming king protests, had made our manners as rude as those of the fish-wives of Dieppe. Tobacco; Its History, Varieties, Culture, Manufacture and Commerce |E. R. Billings. 

But what if I catch the fish by using a hired boat and a hired net, or by buying worms as bait from some one who has dug them? The Unsolved Riddle of Social Justice |Stephen Leacock 

The Taube has been bothering us again, but wound up its manœuvres very decently by killing some fish for our dinner. Gallipoli Diary, Volume I |Ian Hamilton