Soon after, a person called 911 and reported a gray sedan was “driving erratically” on the Beltway just west of the bridge, according to Virginia State Police. Driver hits multiple vehicles, leads police on chase in Virginia and Maryland before crashing along Beltway |Dana Hedgpeth |March 5, 2021 |Washington Post 

In several cases, investigators said, the arsonist drove a generic silver sedan. Man accused of being ‘serial arsonist’ in Maryland is former police chief, investigators say |Katie Mettler |March 3, 2021 |Washington Post 

For the average car, the average sedan, it’s not going to be able to fit three car seats in the back. How Much Do We Really Care About Children? (Ep. 447) |Stephen J. Dubner |January 14, 2021 |Freakonomics 

The first version of the sedan—the $169,00 Dream Edition—also offers 1,080 horsepower, over 150 mph, and a range of up to 517 miles according to EPA’s estimates. The 100 greatest innovations of 2020 |Popular Science Staff |December 2, 2020 |Popular-Science 

Though Ford doesn’t disclose the exact breakdown, Fortune estimates that of the 159,000 vehicles that Ford sold in China in Q2, roughly half were sedans, led by the midsize Focus and compact Escort, and SUVs. Ford, just admit it: You’re a truckmaker now |Shawn Tully |September 21, 2020 |Fortune 

Hoech approached the line of police and SWAT vehicles in his sedan late Tuesday, turned around, and parked. 'Go Ahead and Shoot Me': The Veteran Who Defied Ferguson's Cops |Justin Glawe |August 13, 2014 |DAILY BEAST 

Early Thursday morning, a sedan ran over a crowd of people outside of the Mohawk in Downtown Austin, Texas, during SXSW. Car Crashes Into Crowd at SXSW, 23 People Transported to Hospital, Multiple Fatalities |Marlow Stern |March 13, 2014 |DAILY BEAST 

As I settled into the backseat of the cozy sedan, Frank struck up conversation. The Model Diaries: The Rush of Rejection in Paris |Anonymous |December 26, 2013 |DAILY BEAST 

The Infiniti sedan then made a right turn toward the West Front of the Capitol. Notes From a Shootout |Ben Jacobs |October 3, 2013 |DAILY BEAST 

We could see the big crumpled Mercedes sedan, but not very well. The Night Princess Diana Died |Christopher Dickey |August 31, 2013 |DAILY BEAST 

This cross between a wheelbarrow and a sedan-chair was supported and trundled along the street by four bearers. Our Little Korean Cousin |H. Lee M. Pike 

A big gray sedan stood in the middle of the road, the motor idling. Hooded Detective, Volume III No. 2, January, 1942 |Various 

The overhead doors beneath a sign which advertised car washing by steam ran up on their track as the gray sedan came into sight. Hooded Detective, Volume III No. 2, January, 1942 |Various 

The big gray sedan carrying Delancy and his pals, hit the suburban town at a scant seventy miles an hour. Hooded Detective, Volume III No. 2, January, 1942 |Various 

But that sudden spurt of speed on the part of the gray sedan was a dead give-away. Hooded Detective, Volume III No. 2, January, 1942 |Various