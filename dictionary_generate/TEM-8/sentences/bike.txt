There’s also the risk that your dog pulls the bike off balance. The right way to walk your dog |John Kennedy |August 26, 2020 |Popular-Science 

When I heard a friend had several bikes ripped off in the Bay Area, he blamed himself. The most secure ways to lock up your bike |By Michael Frank/Cycle Volta |August 26, 2020 |Popular-Science 

While conventional bike sales declined slightly in 2018 and 2019, e-bikes soared more than 50%, according to data from market research firm NPD. Bikes are hot but e-bikes are on fire |Michael J. Coren |July 23, 2020 |Quartz 

They worked out on a treadmill or bike, starting at 30 minutes per day. Our gut microbes love a good workout |Silke Schmidt |May 21, 2020 |Science News For Students 

The personal computer, the mountain bike, the artificial pancreas — none of these came from some big R&D lab, but from users tinkering in their homes. Honey, I Grew the Economy (Ep. 399) |Stephen J. Dubner |December 5, 2019 |Freakonomics 

I know that one day in the near (ish) future, we will return to our usual hikes and bike rides. You’re Never ‘Cured’ of an Eating Disorder |Carrie Arnold |December 20, 2014 |DAILY BEAST 

My bike ride that mid-October day starts like so many others. You’re Never ‘Cured’ of an Eating Disorder |Carrie Arnold |December 20, 2014 |DAILY BEAST 

I was really nervous when we started shooting, but once we started, it was just like riding a bike. Adrian Grenier Talks the Economy, the ‘Entourage’ Movie, and the HBO Series’ Alleged ‘Misogyny’ |Marlow Stern |October 28, 2014 |DAILY BEAST 

Going hands-free is just one of the perks of a place where the only form of transportation is by carriage, bike, or tractor. The Crazy Medieval Island of Sark |Liza Foreman |October 4, 2014 |DAILY BEAST 

Jealous of her young male friend who was permitted to ride his bike around shirtless, she once ripped off her top, too. Speed Read: Lena Dunham’s Most Shocking Confessions From ‘Not That Kind of Girl’ |Kevin Fallon |September 26, 2014 |DAILY BEAST 

"It was very good of you to help the kiddie with her bike," said Johnny, as he splashed the petrol into the tank. The Imaginary Marriage |Henry St. John Cooper 

To 1902 belong the first illustrations of the motor-bicycle and of "trailers" attached to the "push-bike." Mr. Punch's History of Modern England Vol. IV of IV. |Charles L. Graves 

If you buy a Kryptonite bike lock and it can be defeated with a Bic pen, you're not getting very good security for your money. Little Brother |Cory Doctorow 

And Jimmy, the mean cur, not to have got her that shop, when she had such a splendid idea: Lady Godiva on a bike! The Bill-Toppers |Andre Castaigne 

But he said all that the much-abused "bike" needed was a brake. My Little Sister |Elizabeth Robins