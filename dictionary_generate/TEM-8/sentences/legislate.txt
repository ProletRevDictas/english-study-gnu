Months after face coverings emerged as a tension point in the pandemic — and as some Americans still refuse to wear them — Republicans in a number of statehouses have refused to cover their faces while legislating. Iowa’s House speaker said he can’t make lawmakers wear masks — but he did enforce a ban on jeans |Teo Armus |February 4, 2021 |Washington Post 

I think what it shows is the huge crisis we’re in as a result of failure to legislate. ‘Disinformation can be a very lucrative business, especially if you’re good at it,’ media scholar says |Joe Heim |January 19, 2021 |Washington Post 

Mitch McConnell has made it very clear he does not want to legislate. The Trailer: The 10 crucial counties, revisited |David Weigel |November 10, 2020 |Washington Post 

The stakeholder approach would use antitrust to accomplish what shareholders did not sign up for, what many officers and directors have not seen fit to implement, and what state capitols and Congress have failed to legislate. FTC commissioner: Is antitrust the next stakeholder capitalism battleground? |jakemeth |September 26, 2020 |Fortune 

The government has said, however it will legislate to end deals like “buy one get one free” for HFSS products. ‘Just waiting for the knockout blow’: U.K. government’s junk food ad ban yet another hit for already battered publishers |Lara O'Reilly |July 28, 2020 |Digiday 

And it might not only be in Britain that politicians rush to legislate. ISIS Has a Message. Do We? |Jamie Dettmer |December 8, 2014 |DAILY BEAST 

No politician has the right to legislate for the awful things that can happen during pregnancy. Tennessee Voters Face a Loaded Abortion Question |Eleanor Clift |October 4, 2014 |DAILY BEAST 

Are we in danger, in the rush to legislate, of ruining the moment? Does California’s College Rape Bill Go Too Far In Regulating Sex? |Emma Woolf |June 23, 2014 |DAILY BEAST 

These are excuses offered up by a party that is too divided to govern and legislate. In Passover Phone Conversation, Eric Cantor Slams Obama |Eleanor Clift |April 17, 2014 |DAILY BEAST 

Now that all efforts to legislate gun control are stalled, it may be time for mental-health legislation to stand on its own. The House’s GOP Psychologist May Finally Get a Mental-Health Bill Passed |Eleanor Clift |April 12, 2014 |DAILY BEAST 

The empire rules the army and can legislate over and control a prodigious amount of national subjects. Great Men and Famous Women. Vol. 4 of 8 |Various 

For suspending our own legislatures, and declaring themselves invested with power to legislate for us in all cases whatsoever. Key-Notes of American Liberty |Various 

Hence He could legislate for man's thoughts, as well as his deeds. Autobiography of Frank G. Allen, Minister of the Gospel |Frank G. Allen 

But the Radicals were in power to legislate and crush agriculture, and "I've got a miser for my brother-in-law," said the farmer. Rhoda Fleming, Complete |George Meredith 

Are they not every day incurring new and useless expenses in consequence of allowing them to legislate and plan for themselves? The Portland Sketch Book |Various