Above that is cultured, or fermented, plant milk cheese, which is made by adding probiotics and enzymes to nut or oat milk in order to create curds and whey. Vegan Cheese Is Ready to Compete With Dairy. Is the World Ready to Eat It? |Alicia Kennedy |April 1, 2021 |Eater 

I skimped a little when layering the curd, worrying it would drip out the sides — a mistake since it was being covered up with the meringue anyway. Six Recipes That Got Us Through Another Week |Eater Staff |March 19, 2021 |Eater 

Eventually, most of the water will evaporate, leaving a thick batch of curds in the pot. How to make your own kashk, a creamy, tangy staple of Iranian cuisine |Naz Deravian |March 12, 2021 |Washington Post 

In the summer, I opt for homemade raspberry jam, while in the winter, I often fill them with lemon or tangerine curd. This twist on the classic Linzer cookie adds gentle sweetness and texture with corn flour |Roxana Jullapat |December 2, 2020 |Washington Post 

Meringue topping shouldn’t be reserved for lemon curd fillings. Tart roasted cranberries and a billowy meringue topping take this pie in a bright, bold direction |Erin Jeanne McDowell |November 12, 2020 |Washington Post 

It was my first time seeing the process for making a stretched curd cheese, and I was completely mesmerized. Elegy to Italian Cheese |Jesse Dart |May 21, 2011 |DAILY BEAST 

Two birds with one stone: any excuse to make lemon curd, and a reliable recipe for a killer portable dessert. 5 Recipes From Jody Adams |Cookstr.com |January 12, 2010 |DAILY BEAST 

New varieties of cheese are formed by mixing vegetable substances with the curd. Domestic Animals |Richard L. Allen 

The stomach of the newly killed animal contains a quantity of curd derived from the milk on which it has been fed. Domestic Animals |Richard L. Allen 

In Ayrshire and Limburg, on the other hand, the curd is always left in the stomach and salted along with it. Domestic Animals |Richard L. Allen 

If the milk be warmer the curd is hard and tough, if colder, it is soft and difficult to obtain free from the whey. Domestic Animals |Richard L. Allen 

The time necessary for the complete fixing of the curd varies also from 15 minutes to an hour or even an hour and a half. Domestic Animals |Richard L. Allen