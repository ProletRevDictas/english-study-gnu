As for the cadre of grooms into whose care the show’s 335 horses were entrusted, it too had been decimated, for during World War I, a horse cavalry was still a vital component of the forces. The Circus Was Once America’s Top Entertainment. Here’s Why Its Golden Age Began to Fade |Les Standiford |June 15, 2021 |Time 

Immune systems are like sentinels, patrolling the body for invaders and calling in the cavalry once a pathogen is detected. The mere sight of illness may kick-start a canary’s immune system |Jonathan Lambert |June 8, 2021 |Science News 

It’s a far cry from the government’s optimism in early December, when the arrival of vaccines was described as the “scientific cavalry” coming to the rescue. U.K. goes back on lockdown as hospitals face breaking point |Rachel King |January 4, 2021 |Fortune 

The Army has museums dedicated to the infantry, the cavalry and the artillery. Army museums have tens of thousands of artifacts. They’re looking to downsize. |Michael Ruane |December 3, 2020 |Washington Post 

Because Forrest enlisted as a private and fought in the often-neglected western theater, his skills as a cavalry commander were overlooked for much of the war. The Fight Over Monuments of Confederate General Nathan Bedford Forrest Holds a Lesson About Whiteness in America |Connor Towne O’Neill |July 13, 2020 |Time 

A newsagent further down on Nathan Road  told The Daily Beast that he recognized a number of retired cops in the cavalry charge. Hong Kong’s Triads Attack Protestors |Ben Leung |October 4, 2014 |DAILY BEAST 

A similar pattern occurred when metal swords, armor, cavalry charges and dense infantry ranks developed. War! What Is It Good For? A Lot |Nick Romeo |August 13, 2014 |DAILY BEAST 

He fought with the Soviets, then led the cavalry and B-52 bombers to rout the Taliban. The Warlord Who Defines Afghanistan: An Excerpt From Bruce Riedel’s ’What We Won’ |Bruce Riedel |July 27, 2014 |DAILY BEAST 

Dostum famously led Uzbek cavalry charges supported by U.S. B-52 bombers to defeat the Taliban. The Warlord Who Defines Afghanistan: An Excerpt From Bruce Riedel’s ’What We Won’ |Bruce Riedel |July 27, 2014 |DAILY BEAST 

They rode as far as Tours on the Loire until stopped by the French cavalry of Charles Martel. Is This Hemingway’s Pamplona or a Lot of Bull? |Clive Irving |July 13, 2014 |DAILY BEAST 

In this position, the line of cavalry formed the chord of the arc described by the river, and occupied by us. Blackwood's Edinburgh Magazine, No. CCCXXXIX. January, 1844. Vol. LV. |Various 

Fanning and Wharton were to make head against the infantry and cavalry. Blackwood's Edinburgh Magazine, No. CCCXXXIX. January, 1844. Vol. LV. |Various 

But the cavalry officer melted imperceptibly out of her existence. The Awakening and Selected Short Stories |Kate Chopin 

They found a few belated sowars of the 3d Cavalry, who took refuge in a wood, and the artillery opened fire at the trees! The Red Year |Louis Tracy 

Infantry and cavalry approached the island, quite unsuspicious of its being occupied. Blackwood's Edinburgh Magazine, No. CCCXXXIX. January, 1844. Vol. LV. |Various