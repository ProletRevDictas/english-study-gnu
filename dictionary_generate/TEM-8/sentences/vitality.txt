The power in these stories rests in their veracity, vitality and vulnerability. Dantiel W. Moniz’s ‘Milk Blood Heat’ thrums with life while considering death |Michele Filgate |February 1, 2021 |Washington Post 

This demonstrates that vitality is a critical part of sustaining success in bad times as well as good times. Finding advantage in adversity: How the Future 50 positioned themselves for growth, even in 2020 |lbelanger225 |December 3, 2020 |Fortune 

We are in a demented Chuseok-like season where we think of famine and hunger under our claims of harvest, where we think of sickness and death for the vitality of our health and lives. The Meaning of Chuseok During a Pandemic Year |Nina Yun |October 9, 2020 |Eater 

San Diego’s restrictions on the number and location of cannabis dispensaries inhibit the economic vitality of existing commercial areas, and are counterproductive to the fundamental urban design goal to create safe and lively streets. Myths and Shame Shouldn’t Guide Cannabis Regulations |John Bertsch |September 8, 2020 |Voice of San Diego 

The next article in this series will demonstrate how artificial intelligence is converging with genetics and pharmaceuticals to transform how we approach longevity, aging, and vitality. A Renaissance of Genomics and Drugs Is Extending Human Longevity |Peter H. Diamandis, MD |June 26, 2020 |Singularity Hub 

Yet their work lives on, and hardly seems to have lost any of its vitality during the intervening years. The Golden Age of Rock Album Covers |Ted Gioia |December 5, 2014 |DAILY BEAST 

As if to prove their continuing vitality, the other elders choose to write about younger or even much younger characters. Richard Ford’s Artful Survivalist Guide: The Return of Frank Bascombe |Tom LeClair |November 4, 2014 |DAILY BEAST 

Jung says that we lose our vitality in playing the role if we identify with it. Joseph Campbell on the Roots of Halloween |Joseph Campbell |October 31, 2014 |DAILY BEAST 

If you play a life role as though it were a mythological game, there is vitality and wonder in it. Joseph Campbell on the Roots of Halloween |Joseph Campbell |October 31, 2014 |DAILY BEAST 

I know many of them, and they have a wonderful vitality of personality. Joseph Campbell on the Roots of Halloween |Joseph Campbell |October 31, 2014 |DAILY BEAST 

Then, as the atmosphere of the room surged back, tense with vitality, her mind leapt forward in welcome. Uncanny Tales |Various 

Further sign of vitality it never showed as the line was never made. Fifty Years of Railway Life in England, Scotland and Ireland |Joseph Tatlow 

Let us suppose its first connection with vitality to be in the simplest form of animated matter—that of the protoplasm. Gospel Philosophy |J. H. Ward 

Such opinions, when rich in vitality and warmth of conviction, have a very important function to fulfil. The English Church in the Eighteenth Century |Charles J. Abbey and John H. Overton 

It suffers from impaired vitality, and uncertain aim; two deadly sicknesses. The Works of Robert Louis Stevenson - Swanston Edition Vol. 25 (of 25) |Robert Louis Stevenson