We can’t change the climate, we can’t change the seasons, so what we have to do is adapt to transmission. COVID-19 may one day come and go like the flu, but we’re not there yet |Kate Baggaley |September 16, 2020 |Popular-Science 

Outside of these hot spots, transmission in China has remained very limited since March. China has quietly vaccinated more than 100,000 people for Covid-19 before completing safety trials |Lili Pike |September 11, 2020 |Vox 

Only a few bacteria make it into the true germline, but it’s enough to guarantee transmission to the next generation of ants. How Two Became One: Origins of a Mysterious Symbiosis Found |Viviane Callier |September 9, 2020 |Quanta Magazine 

The Creek Fire in the Sierra Nevada Mountains, which has scorched more than 78,000 acres, knocked out transmission from a hydro plant on Saturday. California faces widespread power cuts after weeks of destructive wildfires |kdunn6 |September 8, 2020 |Fortune 

Doing it all without having to shut down electricity service from time to time will require overhauling the state’s antiquated distribution and transmission systems, which could take years and cost billions. In defense of California |James Temple |September 4, 2020 |MIT Technology Review 

Education controls the transmission of values and molds the spirit before dominating the soul. Houellebecq’s Incendiary Novel Imagines France With a Muslim President |Pierre Assouline |January 9, 2015 |DAILY BEAST 

They seemed a little creepy to me, particular the song “Transmission.” Greil Marcus Talks About Trying to Unlock Rock and Roll in 10 Songs |Allen Barra |November 17, 2014 |DAILY BEAST 

These people believe education about HIV transmission mitigates the fears that performers carry. Risky Business or None of Your Business? Gay XXX Films and the Condom Question |Aurora Snow |November 1, 2014 |DAILY BEAST 

After all, an enormous amount is known about Ebola and its transmission. New York & New Jersey’s Ebola Quarantines Are an Insane Overreaction |Kent Sepkowitz |October 26, 2014 |DAILY BEAST 

This is evident in the next infrastructure boom unfolding before our eyes: electricity transmission lines. Electricity Superhighway | |October 16, 2014 |DAILY BEAST 

He intends that he shall take the name of Arden, and earn the transmission of the title, or the distinction of a greater one. Checkmate |Joseph Sheridan Le Fanu 

Asbestos rope is used for fire escapes and similar purposes, as well as for the transmission of power over places exposed to heat. Asbestos |Robert H. Jones 

The inherited form of hay fever is explained by the well-known transmission of anaphylaxis to the offspring. The Treatment of Hay Fever |George Frederick Laidlaw 

The order ought to be explained by the transmission of a cheque of a value corresponding to that of the stocks to be bought. Social Comptabilism, Cheque and Clearing Service &amp; Proposed Law |Ernest Solvay 

There are special envelopes for the transmission of these advices which are sold by the Office to the possessors of cheque books. Social Comptabilism, Cheque and Clearing Service &amp; Proposed Law |Ernest Solvay