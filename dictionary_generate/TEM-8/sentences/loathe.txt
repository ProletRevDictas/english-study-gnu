Its upright, narrow body rolled on skinny tires, and its layered design was loved or loathed, depending on the customer. BMW abandons the i3, the car that could have birthed a bright electric future |Tim De Chant |July 13, 2021 |Ars Technica 

I prefer their loose silhouette to skin-tight leggings, especially on crampy period days when I want to exercise but loathe restrictive elastic waistbands. The Gear Our Editors Loved in June |The Editors |July 2, 2021 |Outside Online 

They are loathed by many postal workers, who say they broil during the summer and shiver in the winter when the heating system is inadequate. USPS trucks don’t have air bags or air conditioning. They get 10 mpg. And they were revolutionary. |Jacob Bogage |May 24, 2021 |Washington Post 

This image was nourished by the British press he came to loathe. Prince Philip, royal consort to Queen Elizabeth II, dies at 99 |Adrian Higgins |April 9, 2021 |Washington Post 

It was one of the few spinoffs tolerated by the author, who generally loathed the merchandising of her work. Beverly Cleary, beloved author who chronicled schoolyard scrapes and feisty kids, dies at 104 |Harrison Smith, Becky Krystal |March 26, 2021 |Washington Post 

Republicans loathe public sector unions—unless they represent cops or firefighters. The GOP and Police Unions: A Love Story |Eleanor Clift |December 12, 2014 |DAILY BEAST 

I may loathe what Richard Spencer has to say, but I will defend, unequivocally, his right to say it. American Racist Richard Spencer Gets to Play the Martyr in Hungary |James Kirchick |October 7, 2014 |DAILY BEAST 

Consider Spirit Airlines, the dirt-cheap carrier everyone loves to loathe. Solution to Seat Rage: No More Reclining |Will Doig |September 4, 2014 |DAILY BEAST 

As well as cheese and crustaceans, many people apparently loathe vegetables—which I cannot comprehend. Brits Are Very Fussy Eaters |Emma Woolf |August 5, 2014 |DAILY BEAST 

But at least Obama does seem to genuinely loathe the sucking up required to grease the wheels. Is Big Money Politics an Overblown Evil? |David Freedlander |August 2, 2014 |DAILY BEAST 

But deep in her heart she would loathe him, as only women can hate for a failing they never forgive. The Amazing Interlude |Mary Roberts Rinehart 

I began to loathe the food, and the horrible cruelty to the women frequently sickened me. The Adventures of Louis de Rougemont |Louis de Rougemont 

You don't seem to realise what an utter beast he's been, and how we all loathe him for treating you—yes, you—like this. Happy House |Betsey Riddle, Freifrau von Hutten zum Stolzenberg 

I hate him—I hate him as much as I loathe myself for ever condescending to follow him. Riders of the Silences |John Frederick 

I shall begin to hate my books and to loathe my little cabin. My New Curate |P.A. Sheehan