I have dipped in and out when I can afford to feel angry or dispirited. The Trouble with Global Climate Summits |abarronian |November 17, 2021 |Outside Online 

Even partisans locked into their choices were probably dispirited at what they were witnessing. Election live updates: Debate commission says it will change structure to ensure more ‘orderly discussion’ |John Wagner, Felicia Sonmez, Amy B Wang |September 30, 2020 |Washington Post 

Only two years ago, with awful economic numbers and a dispirited opposition, he was riding high. What Republicans Need Right Now Is a Good Internal Fight |James Poulos |November 6, 2014 |DAILY BEAST 

Cameras panning to American fans showed dispirited faces, furrowed brows. Bring It On! Team USA Progresses to Round 2 |Tunku Varadarajan |June 26, 2014 |DAILY BEAST 

While fans of snubbed teams will be furious, or dispirited, or both, Wellman will crush in the aftermath of Tourney selection. Meet the Man to Hate on Selection Sunday |Matt Gallagher |March 16, 2014 |DAILY BEAST 

Such efforts have reenergized a movement that seemed, until recently, to be dispirited. Political Parity’s Drive to Help Women Win |Leslie Bennetts |January 19, 2012 |DAILY BEAST 

It is a measure of just how dispirited the Democratic base is that its members were not sure that Obama had even this much in him. Obama's Finally Ready to Rumble |Eric Alterman |September 8, 2010 |DAILY BEAST 

He acted dejected and dispirited, and if he could have talked would have asked the meaning of it all. The Courier of the Ozarks |Byron A. Dunn 

Full of fears, anxiety, and mistrust, it was a very dispirited Rabecque that now slowly followed Monsieur Gaubert into the inn. St. Martin's Summer |Rafael Sabatini 

He was now evidently exhausted by toil, and dispirited by disappointment. Blackwood's Edinburgh Magazine, No. CCCXXXIX. January, 1844. Vol. LV. |Various 

I can hardly say why I have written this incoherent note; except, that I am dispirited, and thirst to talk to you. Alone |Marion Harland 

To a lost wanderer, and especially to a dispirited woman, such magnitude was not sublime, but terrifying. Overland |John William De Forest