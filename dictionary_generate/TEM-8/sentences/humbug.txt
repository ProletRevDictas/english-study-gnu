In the press, there were months of ridicule, with one politician describing it as “one of the greatest humbugs, frauds, and absurdities ever known.” The metaverse is a new word for an old idea |Genevieve Bell |February 8, 2022 |MIT Technology Review 

Only a humbug would complain about this “Alice in Wonderland” like befuddlement. ‘Liar’s Dictionary’ a fab, queer tale for lovers of language |Kathi Wolfe |January 29, 2021 |Washington Blade 

There is, inevitably, a lot of self-serving humbug and a lot of tedious pap. South Africa Kicks Off Week Of Mourning Mandela |Mark Gevisser |December 9, 2013 |DAILY BEAST 

No one outside Limbaughland or Trumpville, Potemkin villages where no one will vote for Obama anyway, credited the humbug. Robert Shrum on the Vice Presidential Debate: Biden’s Win Was a Big F@$&ing Deal |Robert Shrum |October 12, 2012 |DAILY BEAST 

We all bow to the seor, and I wonder if he is really the private secretary, or a private humbug, waiting around to ensnare us. Gardens of the Caribbees, v. 1/2 |Ida May Hill Starr 

Did the manufacturers of Alleotone feel downcast over the exposure of their humbug? The Propaganda for Reform in Proprietary Medicines, Vol. 1 of 2 |Various 

In brief, Dowds scientific method is nothing more than unscientific humbug. The Propaganda for Reform in Proprietary Medicines, Vol. 1 of 2 |Various 

You will find him in these pages, just the same humbug Wizard as before. Dorothy and the Wizard in Oz |L. Frank Baum 

"The Wizard of Oz has always been a humbug," agreed Dorothy. Dorothy and the Wizard in Oz |L. Frank Baum