The charges “stem from a SLED investigation into misappropriated settlement funds in the death of Gloria Satterfield” the statement read. What to Know About the Multiple Criminal Investigations into Alex Murdaugh |Josiah Bates |October 20, 2021 |Time 

As Haitian nationals saw poverty, violence and corruption increase, billions in stolen or misappropriated aid funds fueled a legendary industry of disaster capitalism. Haiti Isn’t Cursed. It is Exploited. |Malaika Jabali |September 22, 2021 |Essence.com 

The company argued that the new systems cannot be misappropriated easily by government action—and emphasized repeatedly that opting out was as easy as turning off iCloud backup. Apple defends its new anti-child-abuse tech against privacy concerns |Patrick Howell O'Neill |August 6, 2021 |MIT Technology Review 

“No, I did not,” answered Johnson, one of five clients from whom Avenatti allegedly misappropriated funds. Avenatti Grills Paraplegic Client He Allegedly Stole From: ‘Did You Appreciate Me?’ |Kate Briquelet, Meghann Cuniff |July 22, 2021 |The Daily Beast 

The investigation also found no one misappropriated any funds “for personal gain,” according to the excerpt. Emails Back Up Misspending Allegation at Lincoln High |Will Huntsberry |March 10, 2021 |Voice of San Diego 

Those who collect taxes without being duly authorized by Government, or misappropriate public funds. The Philippine Islands |John Foreman 

A dihydroxyl derivative of one of the latter is in use under the somewhat misappropriate name of “alizarin black.” Coal |Raphael Meldola 

How is it to be known whether the parents misappropriate the fund of a child, or favour one more than another? Comrades |Thomas Dixon 

I asked her if it would not be very easy for him to misappropriate a scudo now and then. The Story of My Life, volumes 4-6 |Augustus J. C. Hare 

Am I willing to be a thief and misappropriate their physical, mental and moral heritage? Facts And Fictions Of Life |Helen H. Gardener