Of course, our original snowfall prediction made Tuesday, was farther off in many areas. Snow expected tonight, mainly south of D.C., before possible ice on Saturday |Jason Samenow, Wes Junker |February 11, 2021 |Washington Post 

I nearly always use the oven’s top rack when broiling, because the farther the food is from the heat, the more I start to wonder why I didn’t just bake instead. Don’t Fear the Broiler |Elazar Sontag |February 11, 2021 |Eater 

As the first wave exits off the coast, the front would probably sink a little farther to the south, putting the area deeper in the cold air. After Sunday’s slush fest, another winter storm threatens by Wednesday night |Jason Samenow, Wes Junker |February 8, 2021 |Washington Post 

That’s because farther-out planets probably formed where it’s cold, and there was more low-density material like frozen water, rather than rock, to begin with. Two exoplanet families redefine what planetary systems can look like |Lisa Grossman |February 5, 2021 |Science News 

Fairfax County is offering free transportation to vaccination sites for some residents who live farther away. D.C. officials knock on doors to reach seniors amid push for vaccine equity |Julie Zauzmer, Hannah Natanson, Rebecca Tan |February 4, 2021 |Washington Post 

The Freedom author went to a deserted island to write Farther Away. Roughing It With Jonathan Franzen’s ‘Farther Away’ |Chris Wallace |April 28, 2012 |DAILY BEAST 

Farther down the highway, a smaller group chanted pro-death slogans and tossed insults at the supporters of Graney. John Grisham's Debut Short Story |John Grisham |October 26, 2009 |DAILY BEAST 

Farther down the gravel-walk strolls a young Frenchman and his fiancée—the mother of his betrothed inevitably at her side! The Real Latin Quarter |F. Berkeley Smith 

Farther from the center they become parabolic, but they are quite good over the entire plate, 3¼ by 4¼ inches. Photographs of Nebul and Clusters |James Edward Keeler 

Farther down the river there was a flash of something white amidst the pale green shimmer of the flood. The Gold Trail |Harold Bindloss 

Farther forward, a doorway leading to the companion-way, and past the officers' quarters to the main deck. Fifty Contemporary One-Act Plays |Various 

Farther back were fields of caramels, and all the land seemed well cultivated and carefully tended. The Tin Woodman of Oz |L. Frank Baum