Current law allows waivers for some elite athletes and classical musicians, but doesn’t include popular entertainers. South Korean boy band BTS built an army. Now, they might get to defer military service |kdunn6 |September 4, 2020 |Fortune 

Most of the time, this is exactly what a musician tries to do—especially if the musician is Jackson Stell, who creates music under the name Big Wild. Why Big Wild's Songs Feel Like Adventures |Outside Editors |September 3, 2020 |Outside Online 

As such, Neil Young’s copyright lawsuit is likely to provide an important test case for whether musicians are able to stop politicians using their songs. Trump and ‘Hallelujah’: Why it’s so hard to stop campaigns from playing songs, even when artists object |Jeff |August 31, 2020 |Fortune 

It doesn’t mean that the person I was around campers was fake, any more than a teacher is fake when he stands in front of a classroom, or a musician is when she performs onstage. Social Media Feels Increasingly Toxic. What Do I Do? |Blair Braverman |August 29, 2020 |Outside Online 

Whether by talk show or battle-of-the-band style, musicians, comedians, and artists are using livestreaming services and distributing content. The Best Streaming Shows Quarantine Has to Offer |Joshua Eferighe |August 14, 2020 |Ozy 

Stephanie Giorgio, a classical musician, credits The Class for helping her cope with anxiety, focus, fear, and self-doubt. How Taryn Toomey’s ‘The Class’ Became New York’s Latest Fitness Craze |Lizzie Crocker |January 9, 2015 |DAILY BEAST 

The talented musician is being held at the 71st Precinct in Brooklyn and due to be arraigned Thursday morning. Rapper Bobby Shmurda Arrested at New York’s Notorious Quad Studios |M.L. Nestel |December 17, 2014 |DAILY BEAST 

Brooklyn musician Bobby Shmurda, whose ‘Shmoney Dance’ went viral, was arrested today on ‘gang conspiracy’ charges, police said. Rapper Bobby Shmurda Arrested at New York’s Notorious Quad Studios |M.L. Nestel |December 17, 2014 |DAILY BEAST 

A well-dressed young man engaged him in conversation, and mentioned that he was a musician. Death Metal Angola: Heavy Metal in War-Torn Africa |Nina Strochlic |November 21, 2014 |DAILY BEAST 

As one musician described, through their songs “Now [people] will hear music instead of gunshots and bombs.” Death Metal Angola: Heavy Metal in War-Torn Africa |Nina Strochlic |November 21, 2014 |DAILY BEAST 

The Brothers crowded round their visitor, and he joined in their talk and even heard himself thanking the gifted musician. Three More John Silence Stories |Algernon Blackwood 

Of course, I shall not resign my present position until I am sure that I am no longer a clerk, but a musician. The Life &amp; Letters of Peter Ilich Tchaikovsky |Modeste Tchaikovsky 

He was something of a musician, too, and played the accompaniments for the girls who sang in The Spring Road. The Girls of Central High on the Stage |Gertrude W. Morrison 

The musician not showing any visible appreciation of the managers metaphor, Perkins immediately proceeded to uncock his eye. The Fifth String |John Philip Sousa 

He had more in common with Kossmann, an excellent musician and a man of culture. The Life &amp; Letters of Peter Ilich Tchaikovsky |Modeste Tchaikovsky