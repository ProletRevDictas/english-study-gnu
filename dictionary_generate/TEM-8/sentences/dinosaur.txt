Science News, January 30, 1971Through the years paleontologists have evolved a picture of the appearance and habits of various dinosaurs…. 50 years ago, scientists made the case for a landlubbing Brontosaurus |Bethany Brookshire |January 29, 2021 |Science News 

This is dinosaur country, where ancient oceans left sculpted bluffs and exposed fossils. In Big Sky country, a pandemic-era fly-fishing getaway |Carl Fincke |January 21, 2021 |Washington Post 

We’re only talking about one set of fossilized dinosaur privates, which limits the scope of any mate signalling takeaways, the study explains. This fossilized butthole gives us a rare window into dinosaur sex |Ellie Shechet |January 20, 2021 |Popular-Science 

Walmart is now valued at more than $400 billion and has shed much of its reputation as a digital dinosaur in the business world. Walmart’s e-commerce chief is leaving to build “a city of the future” |Jason Del Rey |January 15, 2021 |Vox 

The super-efficient lungs of many dinosaurs could deliver oxygen to every part of their massive bodies. Why Birds Can Fly Over Mount Everest - Issue 94: Evolving |Walter Murch |December 30, 2020 |Nautilus 

Unlike figure skating, where Russia's Evgeni Plushenko is considered a dinosaur at 31, the age of Olympic curlers is more varied. Curling: Your New Olympic Addiction, Explained |Brett Singer |February 13, 2014 |DAILY BEAST 

I had the idea to write about her when I was at a small dinosaur museum in Dorset. How I Write: Tracy Chevalier |Noah Charney |November 13, 2013 |DAILY BEAST 

In 1993, adults and kids alike were dressing as Barney the Dinosaur. The Most Popular Halloween Costumes Through the Years: 1985-2013 |Kevin Fallon |October 31, 2013 |DAILY BEAST 

The 68-million-year-old remnants of dinosaur feet were pushed upward by tectonic activity. 294 Dinosaurs Once Walked on This Wall in Bolivia |Nina Strochlic |October 24, 2013 |DAILY BEAST 

The people behind a prank TV show tricked a man into thinking he was being chased by a dinosaur. The Office Raptor, Morgan Freeman Defines ‘Twerking,’ & More Viral Videos |The Daily Beast Video |August 31, 2013 |DAILY BEAST 

Slowly, as though it had seen all it wanted to, the dinosaur turned and went back into the swamp. The Lost Warship |Robert Moore Williams 

Few of these fossil Dinosaur skulls are available for comparison, and those differ among themselves. Dragons of the Air |H. G. Seeley 

Before he could pick himself up the Dinosaur had swung about and buried all three horns, to the sockets, in his throat and chest. In the Morning of Time |Charles G. D. Roberts 

The advantage of bulk lay altogether with the Dinosaur, the three-horned King of all the Lizard kind. In the Morning of Time |Charles G. D. Roberts 

The female Dinosaur, the more instantly malignant of the two, hurled herself upon the trunk of the tree. In the Morning of Time |Charles G. D. Roberts