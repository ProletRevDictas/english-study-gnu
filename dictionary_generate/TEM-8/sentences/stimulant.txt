Some of the most commonly faked drugs include opioids such as Oxycontin, Percocet, Vicodin, and Xanax, as well as stimulants used to treat ADHD like Adderall, counterfeit versions of which can contain pure meth. Flood of Fake, Fentanyl-Laced Pills Is Killing Americans: DEA |Justin Rohrlich |September 27, 2021 |The Daily Beast 

Almost half of the brands tested included more than one prohibited stimulant. Prohibited, unlisted, even dangerous ingredients turn up in dietary supplements |Christie Aschwanden |June 30, 2021 |Washington Post 

This time, he and his colleagues analyzed 17 brands of sports and weight-loss supplements sold in the United States, and they detected nine prohibited stimulants in them. Prohibited, unlisted, even dangerous ingredients turn up in dietary supplements |Christie Aschwanden |June 30, 2021 |Washington Post 

In France, ibogaine was sold and prescribed as an antidepressant and stimulant called Lambarene for more than 30 years until the 1960s, when the government outlawed the sale of ibogaine. Inside Ibogaine, One of the Most Promising and Perilous Psychedelics for Addiction |Mandy Oaklander |April 5, 2021 |Time 

In a 2017 mouse study, for example, wounds healed faster in animals that were previously exposed to an inflammatory stimulant. ‘Trained Immunity’ Offers Hope in Fight Against Coronavirus |Esther Landhuis |September 14, 2020 |Quanta Magazine 

Then there is the nicotine: a stimulant that for the addict also has the added effect of calming the nerves. My (Electronic) Cigarette Addiction |Eli Lake |January 31, 2013 |DAILY BEAST 

What's interesting about the study's finding is that it dodges the trap that snares much of the research on stimulant medication. Busting the Adderall Myth |Casey Schwartz |December 21, 2010 |DAILY BEAST 

Because cigarettes can be either a stimulant or a relaxant, the game has two modes. Get Therapy Through Your iPhone |Josh Dzieza |September 11, 2010 |DAILY BEAST 

For a fuller effect, try a cup of coffee on the side, as caffeine is a traditional and effective stimulant. Five Aphrodisiac Foods |Sarah Whitman-Salkin |February 9, 2010 |DAILY BEAST 

This is bad news, since cortisol also acts as an appetite stimulant. Do Your Genes Make You Fat? |Arthur Agatston, M.D. |January 4, 2010 |DAILY BEAST 

Niopo is a powerful stimulant, a small portion of it producing violent sneezing in persons unaccustomed to its use. Tobacco; Its History, Varieties, Culture, Manufacture and Commerce |E. R. Billings. 

As, during the whole pepper-harvest, they feed wholly on this stimulant, they become exceedingly irritable. The Book of Anecdotes and Budget of Fun; |Various 

Taken internally, camphor is a nerve stimulant, a diaphoretic and a feeble antipyretic. Encyclopaedia Britannica, 11th Edition, Volume 5, Slice 2 |Various 

But to Charity the heat was a stimulant: it enveloped the whole world in the same glow that burned at her heart. Summer |Edith Wharton 

The stick, or whip, is needed rather to indicate the precise movement desired, than as a stimulant or means of punishment. Domestic Animals |Richard L. Allen