It would have also established a primary and run-off election for special elections to fill a vacancy or for a recall election and at-large numbered seats as well, Navarro said. North County Report: Next Oceanside Mayor May Not Have Much Support |Kayla Jimenez |September 2, 2020 |Voice of San Diego 

So when I first heard the story about how Mazda6 sedans were apparently overrun with yellow sac spiders to the point of requiring a recall of more than 100,000 vehicles, I kind of accepted it at face value. Your car is probably full of spiders |PopSci Staff |September 2, 2020 |Popular-Science 

The agency also asked Allergan to make more of an effort to locate and warn tens of thousands of women that they might be affected by the recall of their breast implants. Three more women have died from cancer linked to Allergan’s recalled breast implants, FDA says |Maria Aspan |August 24, 2020 |Fortune 

San Diego students went on strike, and voters took advantage of a newfound power – the recall – to boot the board members out of office. ‘Keep Your Mouth Shut’: Why San Diego Banned ‘Seditious’ Talk in 1918 |Randy Dotinga |August 4, 2020 |Voice of San Diego 

Unprompted brand recall is a metric that usually works well for the most popular brands. Seven brand health metrics and how to measure them |Aleh Barysevich |July 24, 2020 |Search Engine Watch 

One was a Quaker school, whose name he can no longer recall, in upstate New York. The Story Behind Lee Marvin’s Liberty Valance Smile |Robert Ward |January 3, 2015 |DAILY BEAST 

Recall how Clinton returned to Arkansas from the campaign trail to preside over the execution of a mentally disabled man. President Cuomo Would’ve Been a Lion |Jonathan Alter |January 2, 2015 |DAILY BEAST 

That action ignited protests that rocked Wisconsin and spurred a recall—only the second recall of a governor in U.S. history. The Next Phase of the Koch Brothers’ War on Unions |Carl Deal and Tia Lessin |December 22, 2014 |DAILY BEAST 

I don't recall ever seeing him in the commissary, and who would forget? Alfred Hitchcock’s Fade to Black: The Great Director’s Final Days |David Freeman |December 13, 2014 |DAILY BEAST 

I do not recall what sort of aeroplane Mr. Hughes had at the time; however, it was quite comfortable, as I recall. Alfred Hitchcock’s Fade to Black: The Great Director’s Final Days |David Freeman |December 13, 2014 |DAILY BEAST 

The associations of place recall her strange interview with Mr. Longcluse but a few months before. Checkmate |Joseph Sheridan Le Fanu 

He stood listening to what I was saying, and I recall that when I turned slightly and saw his face, it was terrible! The Homesteader |Oscar Micheaux 

And now I can recall that his eyes closed, and from his lips I caught a sigh, and then he rolled to the floor. The Homesteader |Oscar Micheaux 

I often recall the farewell lunch we had together at the Restaurant de Paris, in the Escolta. The Philippine Islands |John Foreman 

On the third day after the declaration of his recall, Ripperda took his official leave, and presented his son in his new office. The Pastor's Fire-side Vol. 3 of 4 |Jane Porter