With so many clubs in the playoff hunt at the deadline, there are a lot more teams than usual who can at least make the case for a big splash ahead of the stretch run. Separating MLB’s Buyers From Sellers At The Weirdest Trade Deadline Ever |Neil Paine (neil.paine@fivethirtyeight.com) |August 28, 2020 |FiveThirtyEight 

Taken together, it’s all looking fairly dull on this side of the Atlantic—a business-as-usual vibe, even. Investors continue to push global stocks into record territory |Bernhard Warner |August 24, 2020 |Fortune 

They’re guaranteeing that local businesses hoping to expand will be forced to pay tolls to access Amazon’s customer base, above and beyond the usual fees Amazon extracts from users of its platform. Why are local governments paying Amazon to destroy Main Street? |jakemeth |August 23, 2020 |Fortune 

Rather than doing seven shows in a season, there will do three or four, and they’ll be extended to make more money because distanced audiences will be a third of their usual size. Hammerly isn’t ready to give up on theater |Patrick Folliard |August 20, 2020 |Washington Blade 

Meanwhile, the world is expecting even less, far less, from value than usual. The champ’s big comeback: Why beaten-down value stocks are poised to thrive |Shawn Tully |August 18, 2020 |Fortune 

The judges who handle arraignments at criminal court in all five boroughs have a small fraction of their usual caseloads. Shot Down During the NYPD Slowdown |Michael Daly |January 7, 2015 |DAILY BEAST 

Larson, as usual, instills gravitas and agency in an otherwise underwritten character. Brie Larson’s Hollywood Transformation |Marlow Stern |December 29, 2014 |DAILY BEAST 

I know that one day in the near (ish) future, we will return to our usual hikes and bike rides. You’re Never ‘Cured’ of an Eating Disorder |Carrie Arnold |December 20, 2014 |DAILY BEAST 

But, under the hawkish eye of the media and through a heavily active social media presence, she carried on as usual. Nicki Minaj Bares Her Own Vulnerability on ‘The Pinkprint’ |Rawiya Kameir |December 16, 2014 |DAILY BEAST 

The broader issue is also playing out without the usual toxicity. Dear GOP: Fix the Damn Justice System! |Jonathan Alter |December 7, 2014 |DAILY BEAST 

We had six field-pieces, but we only took four, harnessed wit twice the usual number of horses. Blackwood's Edinburgh Magazine, No. CCCXXXIX. January, 1844. Vol. LV. |Various 

As usual the dinner was recherché, for the Pandemonium chef enjoyed a world-wide reputation. The Pit Town Coronet, Volume I (of 3) |Charles James Wills 

This habit and the fact that she cares more for color than for drawing are the usual criticisms of her pictures. Women in the fine arts, from the Seventh Century B.C. to the Twentieth Century A.D. |Clara Erskine Clement 

The lady in black, creeping behind them, looked a trifle paler and more jaded than usual. The Awakening and Selected Short Stories |Kate Chopin 

Toward the close of it were the usual number of toasts in honour of Liszt, to which he responded in rather a bored sort of way. Music-Study in Germany |Amy Fay