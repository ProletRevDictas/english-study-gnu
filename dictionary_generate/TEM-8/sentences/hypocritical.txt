Just as it was hypocritical for Newsom to attend that dinner party, it’s hypocritical to claim that people know to social distance and to insist you follow the science while hosting indoor holiday parties. McEnany has mastered the ‘all of the credit, none of the blame’ approach to the pandemic |Philip Bump |December 2, 2020 |Washington Post 

“It seems hypocritical to throw him into the fire when we’ve been putting up with so much worse,” Brandon Barber, 32, an unaffiliated voter who works in finance and served as an Army medic in Iraq, said of Cunningham. In N.C., voters call Cunningham’s infidelity reckless, shocking. They still vote for the Democrat. |Rachael Bade, Pam Kelley |October 21, 2020 |Washington Post 

It’s hypocritical if he expects the population to be able to do that, because the task force he constituted is recommending it, yet he’s not doing it. Trump and his top aides have stopped wearing masks after a brief effort |Rachel Schallom |September 3, 2020 |Fortune 

Being hypocritical and miserly are what Falwell is being roasted for. Who Among Us Has Not Seduced the Pool Boy? |Eugene Robinson |August 30, 2020 |Ozy 

If they don’t, DTC startups risk coming across as hypocritical if they try to market themselves as championing diversity, inclusion and transparency, but customers learn their retail employees had a different experience. By being too customer-obsessed, DTC startups are failing their retail employees |Anna Hensel |August 21, 2020 |Digiday 

“They are hypocritical on this very issue,” Shearer said about Obama, Attorney General Eric Holder and other public officials. Harry Shearer on The Dangerous Business of Satire |Lloyd Grove |January 8, 2015 |DAILY BEAST 

Similarly, the shocking demand among some Republicans for greater action—for ground troops, even—is equally hypocritical. Will the House GOP Stop the War on ISIS? |Michael Tomasky |September 12, 2014 |DAILY BEAST 

The double standard of sexualization is hypocritical at best and ineffectively vindictive at worse. Full Frontal Disney: Feminism's Nudity Double Standard |Emily Shire |August 20, 2014 |DAILY BEAST 

Meanwhile, the hypocritical pageants that rejected her continue to see their cultural relevance—and TV ratings—decline. Miss America Hypocrisy: The Vanessa Williams Nude Photo Shaming |Amanda Marcotte |July 23, 2014 |DAILY BEAST 

On spending and economic issues, he was atrocious and hypocritical in all the ways that a Republican can be. Now Let’s Replace All the Other Big-Spending Eric Cantors |Nick Gillespie |June 11, 2014 |DAILY BEAST 

God rejects the hypocritical fasts of the Jews: recommends works of mercy, and sincere godliness. The Bible, Douay-Rheims Version |Various 

Pope-holy; properly an adjective, meaning 'holy as a pope,' hence, hypocritical. Chaucer's Works, Volume 1 (of 7) -- Romaunt of the Rose; Minor Poems |Geoffrey Chaucer 

He even joined with some show of interest (of course hypocritical) in the conversation. The Floating Light of the Goodwin Sands |R.M. Ballantyne 

Calvin gave birth, by the sternness of his doctrines and his executions, to that form of hypocritical sentiment called "cant." Catherine de' Medici |Honore de Balzac 

The hypocritical ceremony continued some time, when the warriors began talking among themselves. The Life of Kit Carson |Edward S. Ellis