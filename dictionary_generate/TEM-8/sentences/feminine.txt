Often, I came across feminine characters on the street that walked with an exaggerated animation, hips swaying as though they were models, sauntering down a catwalk. ‘Cyberpunk 2077’ is a thrill ride through an ugly, unexamined world |Elise Favis |December 11, 2020 |Washington Post 

Women’s cycling apparel maker Machines for Freedom is known for its boldly feminine, eye-catching floral prints. Best Women's Mountain Bike Gear for Fall Riding |Gloria Liu |November 7, 2020 |Outside Online 

At one point in my teens I was seen as a person who was too feminine. A non-binary Cuban artist is born again in Spain |Yariel Valdés González |October 16, 2020 |Washington Blade 

In that case, Ann Hopkins received advice from her employer that, if she wanted to make partner at the firm, she should act more feminine. The Supreme Court Decision To Grant Protections To LGBT Workers Is An Important Expansion Of The Civil Rights Act |LGBTQ-Editor |June 18, 2020 |No Straight News 

I think that a lot of people thought, well, you’re saying girls can’t be feminine. Does Hollywood Still Have a Princess Problem? (Ep. 394) |Stephen J. Dubner |October 24, 2019 |Freakonomics 

The looks were slightly more feminine (and by slightly, we really mean slightly). What, and Who, You'll Be Wearing in 2015 |Justin Jones |December 27, 2014 |DAILY BEAST 

All other issues—racial, feminine, even environmental—need to fit around this central objective. Time to Bring Back the Truman Democrats |Joel Kotkin |December 21, 2014 |DAILY BEAST 

These crimes of fashion proved the men were feminine and thus gay and therefore worthy of incarceration. The Straight Hero of Cameroon’s Gays |Jay Michaelson |December 10, 2014 |DAILY BEAST 

In Northanger Abbey, Jane Austen defends the novel against critics who dismiss it as frivolous and feminine. The Birth of the Novel |Nick Romeo |November 27, 2014 |DAILY BEAST 

“I had this feminine, classical image [of myself] that would have totally been destroyed,” she said. Whip It: Secrets of a Dominatrix |Justin Jones |November 25, 2014 |DAILY BEAST 

This unreasoning, feminine obstinacy so wrought upon him that he permitted himself a smile and a lapse into irony and banter. St. Martin's Summer |Rafael Sabatini 

Under all man's dreams of eternal gods and eternal heavens lies man's passion for the eternal feminine. God and my Neighbour |Robert Blatchford 

The plain furniture was stiffly arranged, and there was no litter of clothing or small feminine belongings. Rosemary in Search of a Father |C. N. Williamson 

She made me a profound and graceful curtsey—feminine homage to my budding manhood. Fifty Years of Railway Life in England, Scotland and Ireland |Joseph Tatlow 

Her eyes are in a measure open now, but it is too late, and she rebels in the usual futile feminine way. Ancestors |Gertrude Atherton