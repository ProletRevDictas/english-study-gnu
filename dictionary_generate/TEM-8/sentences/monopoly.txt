The Open Markets Institute, a nonprofit group that advocates against corporate monopolies, released a paper Monday criticizing Amazon’s employee surveillance practices. After public outcry, Amazon deletes listings for 2 intelligence jobs that involved tracking ‘labor organizing threats’ |rhhackettfortune |September 1, 2020 |Fortune 

Microsoft was legally found to be a monopoly 20 years ago, in violation of the Sherman Antitrust Act, and ordered broken into two companies. Apple, Amazon, Google and Facebook set to preview antitrust defenses before Congress |Greg Sterling |July 28, 2020 |Search Engine Land 

That system is currently owned by San Diego Gas and Electric, which enjoyed an energy monopoly in the region until recently. Environment Report: The Latest Power Struggles for SDG&E and Sempra |MacKenzie Elmer |June 29, 2020 |Voice of San Diego 

Because SDG&E is an energy monopoly in our city, they can do what they want and have proven to put their profits over our city’s best interests. Franchise Fee Deal Is a Chance for the City to Make Much-Needed Changes |Pia Piscitelli |June 23, 2020 |Voice of San Diego 

Another article from 2007 talking about how MySpace had an unbreakable monopoly in terms of social networking. Can You Hear Me Now? (Ep. 406) |Stephen J. Dubner |February 20, 2020 |Freakonomics 

Most of the vendors were, like this woman, honorary Jews for the night, not that Jews have a monopoly on potato pancakes. I Ate Potato Pancakes Til I Plotzed |Emily Shire |December 17, 2014 |DAILY BEAST 

For decades, these two industrial brewers have basked in a sort of shared-monopoly over the Panamanian beer racket. House of the Witch: The Renegade Craft Brewers of Panama |Jeff Campagna |November 30, 2014 |DAILY BEAST 

Mattson says the government bogarts this stuff, gathered at taxpayer expense, and maintains “a monopoly on the data.” What It Takes to Kill a Grizzly Bear |Doug Peacock |November 23, 2014 |DAILY BEAST 

Ma wrote online, “Let the users decide who wins the game, not monopoly and power.” Alibaba’s Dark Side: Censoring Customers |Brendon Hong |November 18, 2014 |DAILY BEAST 

And lest you be deceived, primary elections are no partisan monopoly. Reality Check: There Are No Swing Voters |Goldie Taylor |November 13, 2014 |DAILY BEAST 

In 1622 a monopoly of the importation of tobacco was granted to the Virginia and Somers Island, companies. Tobacco; Its History, Varieties, Culture, Manufacture and Commerce |E. R. Billings. 

He continued its sale, however, as a kingly monopoly, allowing only those to engage in it who paid him for the privilege. Tobacco; Its History, Varieties, Culture, Manufacture and Commerce |E. R. Billings. 

From its first cultivation in these countries it has been a government monopoly. Tobacco; Its History, Varieties, Culture, Manufacture and Commerce |E. R. Billings. 

Let it be observed also that we have hitherto been speaking as if all things were produced under a monopoly. The Unsolved Riddle of Social Justice |Stephen Leacock 

It is the one which is sometimes called in books on economics the case of an unique monopoly. The Unsolved Riddle of Social Justice |Stephen Leacock