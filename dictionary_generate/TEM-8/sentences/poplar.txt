That space-age stuff reinforces a lightweight caruba and poplar core with a big, even flex profile. Runners-Up Review: These Powder Skis Almost Made Our 2022 Winter Buyer’s Guide |agintzler |January 9, 2022 |Outside Online 

The flex profile of the caruba and poplar wood core is also incredibly inviting. Runners-Up Review: The All-Mountain Skis That Almost Made Our 2022 Winter Buyer’s Guide |agintzler |December 17, 2021 |Outside Online 

She would pass the summer thaws on her grandparents’ farm in northern Minnesota, where her grandfather grew hybrid poplars for paper pulp and her grandmother taught her to cook the food they raised. Emily Ford Hiked 1,200 Miles in the Dead of Winter |Grayson Haver Currin |May 4, 2021 |Outside Online 

The researchers are now testing poplar Algobats on real cricket fields. Why sports are becoming all about numbers — lots and lots of numbers |Silke Schmidt |May 21, 2020 |Science News For Students 

The poplar trees that line the avenues between the cellblocks are bare. My Visit To Hell |Christopher Buckley |January 30, 2009 |DAILY BEAST 

A little way beyond the poplar-grove Piegan drew rein, and held up one hand. Raw Gold |Bertrand W. Sinclair 

Far away a gap in the poplar trees showed a German observation balloon, a tiny dot against the sky. The Amazing Interlude |Mary Roberts Rinehart 

They climbed the broken staircase and stared toward the break in the poplar trees, from the roofless floor above. The Amazing Interlude |Mary Roberts Rinehart 

Small poplar trees were quickly felled in the neighboring forest, and their branches lopped off. Gold-Seeking on the Dalton Trail |Arthur R. Thompson 

Having reached this spot, they lost no time in cutting slender poles of poplar and attaching the lines. Gold-Seeking on the Dalton Trail |Arthur R. Thompson