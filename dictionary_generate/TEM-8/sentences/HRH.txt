Like Zara, she will have no royal title nor be an HRH as she is descended through the maternal line. Zara's Royal Baby Named Mia Grace | Tom Sykes | January 23, 2014 | DAILY BEAST 

She does not carry the honorific title HRH as she is descended through the female line. Zara Phillips Gives Birth To A Baby Girl | Tom Sykes | January 17, 2014 | DAILY BEAST 

HRH desires me to point out that he was not at Wynyard and also to say that he has not shot an owl for the last 25 years. Royal Cover-Up as Prince of Wales Shoots Owl (In 1896) | Tom Sykes | February 27, 2013 | DAILY BEAST 

Zara's allowed to do things like this because her mum very wisely renounced her HRH on her behalf. Gross Out! Horse Sneeze in New Zara Philips Land Rover Ad! | | September 7, 2012 | DAILY BEAST British Dictionary definitions for H.R.H. HRH abbreviation for His ( or Her) Royal Highness Collins English Dictionary - Complete & Unabridged 2012 Digital Edition © William Collins Sons & Co. Ltd. 1979, 1986 © HarperCollins Publishers 1998, 2000, 2003, 2005, 2006, 2007, 2009, 2012