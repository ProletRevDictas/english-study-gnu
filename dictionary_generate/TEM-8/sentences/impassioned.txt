Zeyn Joukhadar, formerly Jennifer Zeynab Joukhadar, has just published the impassioned “The Thirty Names of Night” which, like “The Map of Salt and Stars,” centers on Syrian families in their war-torn homeland and in America. Zeyn Joukhadar’s ‘The Thirty Names of Night’ is a poetic portrait of a trans man’s search for a rare bird — and his own identity |Carol Memmott |November 23, 2020 |Washington Post 

His impassioned speech thrust the furniture factory worker into the spotlight, putting him on Fox News and earning him a speaking platform with the National Rifle Association outreach board. Two Years Ago, a Factory Worker. Today, a Republican Star |Nick Fouriezos |November 19, 2020 |Ozy 

Then he became a viral internet sensation by giving an impassioned speech at a Greensboro City Council meeting in support of the Second Amendment, declaring “I am the majority!” What to Expect From the Election Turbulence to Come |Nick Fouriezos |November 4, 2020 |Ozy 

Wherever the trolley would stop, she’d disembark and deliver an impassioned speech while standing on top of a Moxie box borrowed from the nearest drugstore. “I burned with indignation” |Katie McLean |October 20, 2020 |MIT Technology Review 

Early in American history, the filibuster permitted an impassioned minority to hold the floor, ensuring they could make their case, no matter the impatience of the majority. The definitive case for ending the filibuster |Ezra Klein |October 1, 2020 |Vox 

You can clink your wine glass and deliver an impassioned speech about conquering the demons that kept you confined in the closet. How to Make It Through Thanksgiving Alive |Lizzie Crocker |November 26, 2014 |DAILY BEAST 

There were impassioned heroes rallying together to become better than they thought they could be. ‘Newsroom’ Premiere: Aaron Sorkin Puts CNN on Blast Over the Boston Bombing |Kevin Fallon |November 10, 2014 |DAILY BEAST 

Emma Watson this week impressed many with an impassioned U.N. speech about gender inequality. Celebrities, STFU About Your ‘Privacy’ |Tim Teeman |September 24, 2014 |DAILY BEAST 

In an impassioned call to action, he urged American doctors, nurses, and health care professionals to join Africa in its fight. CDC: 'Window Is Closing' on Containing Ebola |Abby Haglage |September 2, 2014 |DAILY BEAST 

Then on March 23, Romero delivered a truly impassioned homily. Why Pope Francis Wants to Declare Murdered Archbishop Romero a Saint |Christopher Dickey |August 24, 2014 |DAILY BEAST 

General Stanhope then repeated to him, all that the impassioned resentment of Ripperda had excited him to avow. The Pastor's Fire-side Vol. 3 of 4 |Jane Porter 

Socialism, like every other impassioned human effort, will flourish best under martyrdom. The Unsolved Riddle of Social Justice |Stephen Leacock 

Meanwhile he wrote an impassioned letter to Napoleon urging him to seek no more wars of conquest. Napoleon's Marshals |R. P. Dunn-Pattison 

Books of impassioned poetry, and descriptions of heroic character and achievements, were her especial delight. Madame Roland, Makers of History |John S. C. Abbott 

There were no impassioned affections glowing in her bosom and impelling her to his side. Madame Roland, Makers of History |John S. C. Abbott