Over four days, we tallied 160,000 vertical feet of descents—per person—while the weather gods delivered snow and brutal cold. The Gear We Relied On for Our 2020 Ski Test |Marc Peruzzi |October 15, 2020 |Outside Online 

Snakes, it appears, may have also whispered beyond the grave, serving as a messenger between the gods and a worshipper. X-rays reveal what ancient animal mummies keep under wraps |Helen Thompson |August 20, 2020 |Science News 

Venerated as the god of mercy, compassion, and even pets, the deity is revered as a Bodhisattva. 12 Major Japanese Gods and Goddesses You Should Know About |Dattatreya Mandal |May 6, 2020 |Realm of History 

Another popular myth pertains to how Amaterasu locked herself in a cave after having a violent altercation with Susanoo, the storm god. 12 Major Japanese Gods and Goddesses You Should Know About |Dattatreya Mandal |May 6, 2020 |Realm of History 

In an interesting note, Yebisu is also the god of jellyfishes, given his initial boneless form. 12 Major Japanese Gods and Goddesses You Should Know About |Dattatreya Mandal |May 6, 2020 |Realm of History 

It is the summit of human happiness: the surrender of man to God, of woman to man, of several women to the same man. Houellebecq’s Incendiary Novel Imagines France With a Muslim President |Pierre Assouline |January 9, 2015 |DAILY BEAST 

“Personally, I deal with manners of righteousness and God,” he says. New York’s Most Tragic Ghost Loves Minimalist Swedish Fashion |Nina Strochlic |January 8, 2015 |DAILY BEAST 

My body used for his hard pleasure; a stone god gripping me in his hands. ‘A Gronking to Remember’ Speed Read: 8 Naughtiest Bits |Emily Shire |January 7, 2015 |DAILY BEAST 

Yet, for god knows what reason, his name is never brought up in the “Great American Filmmaker” conversation. Oscars 2015: The Daily Beast’s Picks, From Scarlett Johansson to ‘Boyhood’ |Marlow Stern |January 6, 2015 |DAILY BEAST 

The mythic origin of the feast was the creation of the world by the god Marduk. New Year’s Eve, Babylon Style |Candida Moss |December 31, 2014 |DAILY BEAST 

The supernaturalist alleges that religion was revealed to man by God, and that the form of this revelation is a sacred book. God and my Neighbour |Robert Blatchford 

Each religion claims that its own Bible is the direct revelation of God, and is the only true Bible teaching the only true faith. God and my Neighbour |Robert Blatchford 

Elyon is the name of an ancient Phœnician god, slain by his son El, no doubt the “first-born of death” in Job xviii. Solomon and Solomonic Literature |Moncure Daniel Conway 

But if God made man, then God is responsible for all man's acts and thoughts, and therefore man cannot sin against God. God and my Neighbour |Robert Blatchford 

He felt himself the meanest, vilest thing a-crawl upon this sinful earth, and she—dear God! St. Martin's Summer |Rafael Sabatini