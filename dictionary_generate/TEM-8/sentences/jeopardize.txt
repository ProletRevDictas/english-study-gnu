College administrators have enacted inadequate protocols that have jeopardized the safety of their communities. Donald Trump has failed to protect me and other students from COVID-19 |jakemeth |September 3, 2020 |Fortune 

All asked that their names be withheld, to not jeopardize their employment. Sent Home to Die |by Annie Waldman and Joshua Kaplan |September 2, 2020 |ProPublica 

If the agency is right, it could jeopardize SDG&E’s safety certificate and associated access to funding covering damage claims. Morning Report: Power Company Challenged on Fire Prevention Plans |Voice of San Diego |August 25, 2020 |Voice of San Diego 

Combined with the interruption of outpatient services in hospitals and clinics, and socioeconomic changes that can lead to relapse, has experts worried the progress made so far on tackling the opioid crisis may be jeopardized. Covid-19 is undoing a decade of progress on the opioid epidemic |Annalisa Merelli |August 11, 2020 |Quartz 

Nonetheless, linear’s current importance can compromise networks’ willingness to make moves in streaming that may jeopardize their linear businesses. TV networks begin to signal willingness to prioritize streaming over linear |Tim Peterson |July 29, 2020 |Digiday 

Air traffic controllers and pilots together take great care not to fly in conditions that can jeopardize an airplane. Did Bad Weather Bring Down AirAsia 8501? |Clive Irving |December 29, 2014 |DAILY BEAST 

The principle is clear: the design must have multiple redundancies so that no single failure can jeopardize the airplane. Virgin Galactic’s Flight Path to Disaster: A Clash of High Risk and Hyperbole |Clive Irving |November 1, 2014 |DAILY BEAST 

So many of the big media players are afraid to jeopardize access, or personal relationships. How TMZ Claims Its Celebrity Scalps, Like Ray Rice |Lloyd Grove |September 10, 2014 |DAILY BEAST 

You couldn't write an honest assessment for what percentage of your daughter's life you were willing to jeopardize. Whatever You Do Someone Will Die. A Short Story About Impossible Choices in Iraq |Nathan Bradley Bethea |August 31, 2014 |DAILY BEAST 

Republicans have also raised concerns that the release of the report could jeopardize security at U.S. facilities overseas. You're About to See What Obama Calls 'Torture' |Josh Rogin, Eli Lake |August 1, 2014 |DAILY BEAST 

It is necessary that I shall be considered a patriot of patriots, nothing must jeopardize such a character at the present time. The Light That Lures |Percy Brebner 

It would be impolitic to jeopardize his whole ambition by any deviation from the letter of the Erfurt agreement. The Life of Napoleon Bonaparte |William Milligan Sloane 

This was so, but only in so far as his actions would not jeopardize the peace of his own nation. The Fire People |Ray Cummings 

After all, if Diogenes chose to jeopardize his head, what was it to them? The Proud Prince |Justin Huntly McCarthy 

An investigation would be decidedly humiliating to her, and also jeopardize her position at Hamilton. Marjorie Dean College Freshman |Pauline Lester