Only female couples who have conceived in an Irish clinic with a non anonymous donor and a child born in Ireland are covered. Still fighting for parental rights in Ireland |Ranae von Meding |February 14, 2021 |Washington Blade 

That evening he and an Army sergeant who stayed with him tried to boil an Irish stew in Stanton’s fireplace. The war secretary who barricaded himself in his office during an impeachment trial |Ronald G. Shafer |January 26, 2021 |Washington Post 

Facebook then appealed that ruling, saying the newly introduced GDPR now definitely meant only the Irish regulator could take it to court. A Facebook case in Belgium could open the floodgates for GDPR privacy suits |David Meyer |January 13, 2021 |Fortune 

The Irish actor Gabriel Byrne has had a rich and varied career. Gabriel Byrne’s ‘Walking with Ghosts’ is a revelation in unexpected ways |Keith Donohue |January 12, 2021 |Washington Post 

Optimum Nutrition is an Irish owned company with Illinois roots that focuses on whey-based protein supplements to support sports nutrition. Best protein powder: Better nutrition in a bottle |Carsen Joenk |January 11, 2021 |Popular-Science 

The month of May will see an Irish referendum on the legalization of same-sex marriage. ‘Only God’ Can Stop Gay Marriage |Tim Mak |January 6, 2015 |DAILY BEAST 

After years at the head of a parochial school classroom, he could no longer distinguish one blond Irish Catholic kid from another. Obama’s One Hand Clap With Castro |Doug McIntyre |December 24, 2014 |DAILY BEAST 

A notably large Irish contingent took part in the infamous draft riots because they did not want to compete for jobs with blacks. This Week's Riots Are Part of America's Long History of Racial Rage |Sharon Adarlo |November 29, 2014 |DAILY BEAST 

It is a reasonable assumption, considering his roots in the Republican Party, in the Marines, and his proud Scots-Irish roots. Hillary Gets a Challenger and He’s a Marine |David Freedlander |November 21, 2014 |DAILY BEAST 

Irish immigrants were more likely to vote Democratic, and German immigrants voted Republican. What Lincoln Could Teach Fox News |Scott Porch |November 6, 2014 |DAILY BEAST 

The Irish Peasantry are idle, the English say truly enough; but who inquires whether there is any work within their reach? Glances at Europe |Horace Greeley 

While a counsellor was pleading at the Irish bar, a louse unluckily peeped from under his wig. The Book of Anecdotes and Budget of Fun; |Various 

Two Irish soldiers being stationed in a borough in the west of England, got into a conversation respecting their quarters. The Book of Anecdotes and Budget of Fun; |Various 

Moore cared not for it; indeed, I think that Irish gentlemen smoke much less than English. Tobacco; Its History, Varieties, Culture, Manufacture and Commerce |E. R. Billings. 

An Irish clergyman insisted that it was the little hamlet of Auburn, in the county of Westmeath. The Book of Anecdotes and Budget of Fun; |Various