He justified his small pharmacy by saying that they allergy medications, and that the infirmary had prescribed them for him. Locked up in the Land of Liberty: Part III |Yariel Valdés González |July 21, 2021 |Washington Blade 

In the beginning of May, none of the six infirmary beds at Serrana’s Santa Casa Hospital and the city’s Basic Healthcare Unit are occupied, and the waiting line for beds both in infirmaries and intensive care units has zeroed. How a small city in Brazil may reveal how fast vaccines can curb COVID-19 |Meghie Rodrigues |May 5, 2021 |Science News 

Over one hundred women volunteered their services at the infirmaries. Fire on the Bay: 115 Years Ago This Month, a Deadly Explosion Rocked a Navy Ship |Randy Dotinga |July 14, 2020 |Voice of San Diego 

The captain replied, “Well, he was a mess, so the first thing we did was get him to the infirmary to get him cleaned up.” Tom Hanks on His Riveting Oscar Moment That Ends ‘Captain Phillips’ |Marlow Stern |October 15, 2013 |DAILY BEAST 

He can barely make out what the woman in the infirmary is saying. Tom Hanks on His Riveting Oscar Moment That Ends ‘Captain Phillips’ |Marlow Stern |October 15, 2013 |DAILY BEAST 

Her affiliations are with Mt. Sinai and New York Eye and Ear Infirmary. Gal With a Suitcase: New York's Best Pampering Spots |Jolie Hunt |February 11, 2011 |DAILY BEAST 

She remained in the infirmary after returning to jail, and Carter declined to elaborate on the nature of her medical condition. Did Julie Schenecker Kill Her Kids? |Amy Green |February 8, 2011 |DAILY BEAST 

Public charities, such as the Infirmary and the Lancaster School received annual subscriptions until the companies came to an end. The Influence and Development of English Gilds |Francis Aiden Hibbert 

Barrent had been taken to the infirmary, where his injuries were patched up. The Status Civilization |Robert Sheckley 

Then there was the infirmary full of occupants on that merry New Year's night. Mystic London: |Charles Maurice Davies 

I believe you were the magistrate who attended to take down the deposition of a poor man who died in the Infirmary last night.' North and South |Elizabeth Cleghorn Gaskell 

When he was allowed to leave the infirmary, it was not to go to his solitary cell, but to share the dormitory with the others. Robert Annys: Poor Priest |Annie Nathan Meyer