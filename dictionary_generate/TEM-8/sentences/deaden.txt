These are hull- and turret-mounted packs of explosives that are set off when hit by heavy enemy attacks, exploding outward and letting the counter-blast deaden the impact of the attacker’s weapon. The tanks, rockets, and other weapons that Russia has in its arsenal |Kelsey D. Atherton |February 17, 2022 |Popular-Science 

One asked for both a deadened baseball and rosters limited to 12 pitchers — which I wholeheartedly endorse because, if there are fewer pitchers to use, each pitcher has to do more and there can’t be as many game-stalling switches. Long games, late nights: Once again, MLB’s playoffs are a slog |Barry Svrluga |October 28, 2021 |Washington Post 

Placebos can deaden pain signals coming from the nerves, they learned. Discovering the power of placebos |Kathryn Hulick |September 9, 2021 |Science News For Students 

For bulletproof armor to be effective, it needs to distribute and deaden the energy of bullets quickly, it needs to be durable against repeat impacts, and it needs to be light and useful enough that people actually wear it. Microscopic mesh could be the key to lighter, stronger body armor |Charlotte Hu |August 22, 2021 |Popular-Science 

It turns out that deadening some of the pain of mountain biking also dims some of the satisfaction. Do Kids Need E-Mountain Bikes? |agintzler |July 24, 2021 |Outside Online 

Are you talking about your battle with rheumatoid arthritis and the need to deaden the pain of it? Kathleen Turner's New Broadway High |Kevin Sessums |April 17, 2011 |DAILY BEAST 

The dining room, even in the heat of summer, should be carpeted, to deaden the noise of the servants' feet. The Ladies' Book of Etiquette, and Manual of Politeness |Florence Hartley 

Hay and straw were laid upon the bridges to deaden the sound of the artillery wheels. The Boys of '61 |Charles Carleton Coffin. 

An attempt to imagine a body destitute of thy potency, would be to bankrupt and deaden the material universe. Etidorhpa or the End of Earth. |John Uri Lloyd 

Even concussion of the brain had failed to deaden the memory of that awful night. Quin |Alice Hegan Rice 

Her heart answered to the same emotions that quicken or deaden the beat of other breasts. Tin-Types Taken in the Streets of New York |Lemuel Ely Quigg