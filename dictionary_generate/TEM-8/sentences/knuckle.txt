Use your tape and measure the width of your hand at your knuckles. How to knit your own Bernie mittens |Sandra Gutierrez G. |January 26, 2021 |Popular-Science 

He also did not turn himself into law enforcement, she said, and instead was found at his sister’s home in Macon, 75 miles from home, with brass knuckles, multiple guns and hundreds of rounds of ammunition. Prosecutors want defense attorney, ex-Sacramento GOP leader charged in Capitol siege to remain detained |Rachel Weiner, Spencer Hsu |January 21, 2021 |Washington Post 

As a variant, keep your eyes looking forward and glide your knuckles down toward your shoulder in a straight line. A step-by-step guide to giving yourself a massage |Sandra Gutierrez G. |January 14, 2021 |Popular-Science 

Glide your knuckles down slowly, applying constant pressure. A step-by-step guide to giving yourself a massage |Sandra Gutierrez G. |January 14, 2021 |Popular-Science 

I’ve knocked on wood so many times since March that my knuckles are covered in callouses. Should I be giving my little girl a bigger dose of reality about the pandemic? |Scott Neumyer |October 30, 2020 |Washington Post 

The Spy Who Came In from the Cold, my third book, changed my life and put me on bare-knuckle terms with my abilities. The Stacks: How The Berlin Wall Inspired John le Carré’s First Masterpiece |John le Carré |November 8, 2014 |DAILY BEAST 

If you think of yourself as more than a knuckle-dragging Neanderthal, you can stand up and be respectful. Hey, Creeps, ‘Compliments’ Are Harassment, Too |Tauriq Moosa |November 5, 2014 |DAILY BEAST 

How could it be that this word, and not “what” or “why,” has caused a bare-knuckle brawl at such a stratospheric social level? How One Lawsuit Shows What’s Wrong With America |James Poulos |October 20, 2014 |DAILY BEAST 

I like the term knuckle dragger, but I don't want to cast any undeserved slurs at Neanderthals. No Denial From Bret Stephens Re. Yeshiva University Panel Slurs |Lisa Goldman |October 29, 2013 |DAILY BEAST 

In Silicon Valley it's a war for talent—an all-out knuckle-drag war. E-Verify mandate: 'Pain in the neck' for Main Street |CNBC |July 18, 2013 |DAILY BEAST 

He had their undivided attention in a moment, without the rapping of Miss Carringtons hard knuckle on the table top. The Girls of Central High on the Stage |Gertrude W. Morrison 

Bobby said that that knuckle of Gee Gees middle finger had been abnormally developed by continued bringing the class to order. The Girls of Central High on the Stage |Gertrude W. Morrison 

By chance he heard us warned for guard, and at once went to his tent and returned with a ham knuckle. Letters of Lt.-Col. George Brenton Laurie |George Brenton Laurie 

Three were used at a time, thrown from the fritillus, as were the knuckle-bones (Fig. 132), but the sides counted that came up. The Private Life of the Romans |Harold Whetstone Johnston 

If this rounding is carried to excess we get the Knuckle thread shown at (d). An Introduction to Machine Drawing and Design |David Allan Low