But maybe you have to start somewhere else — with Lamont Waltman Marvin, Monty, his father, the Chief, the old man. The Story Behind Lee Marvin’s Liberty Valance Smile |Robert Ward |January 3, 2015 |DAILY BEAST 

The detectives are still at it, seeking to account for a period of time when Brinsley may well have paused to sit somewhere. Exclusive: Inside a Cop-Killer’s Final Hours |Michael Daly |December 31, 2014 |DAILY BEAST 

Maybe I have come more to terms with, somewhere over the years, that people will think whatever they think. Daphne Merkin on Lena Dunham, Book Criticism, and Self-Examination |Mindy Farabee |December 26, 2014 |DAILY BEAST 

How a car would be sent to collect him and he would be taken somewhere. Victim: I Watched British MPs Rape and Murder Young Boys |Nico Hines |December 18, 2014 |DAILY BEAST 

(Somewhere, on another cloud, live gigabytes of photos from these very parties). I’m a Digital Hoarder |Lizzie Crocker |December 17, 2014 |DAILY BEAST 

The King of Delhi had a hunting-lodge somewhere in the locality, but he had never seen the place. The Red Year |Louis Tracy 

The lovers got up, with only a silent protest, and walked slowly away somewhere else. The Awakening and Selected Short Stories |Kate Chopin 

He decided not to return home directly; he wanted to go somewhere, but did not care to stay in Chicago. The Homesteader |Oscar Micheaux 

I have never known a trader in philanthropy who was not wrong in his head or heart somewhere or other. Pearls of Thought |Maturin M. Ballou 

"I have a letter somewhere," looking in the machine drawer and finding the letter in the bottom of the workbasket. The Awakening and Selected Short Stories |Kate Chopin