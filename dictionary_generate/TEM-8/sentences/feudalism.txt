That Silicon Valley is moving away from capitalism toward feudalism, with tech CEOs as feudal lords, and this is a good thing. Occupying the Throne: Justine Tunney, Neoreactionaries, and the New 1% |Arthur Chu |August 1, 2014 |DAILY BEAST 

Then she and Red got into an argument about Putin, the Russian character and when Tsarism and feudalism truly ended in Russia. Vegan Strippers Let It All Hang Out |Kelly Williams Brown |March 29, 2014 |DAILY BEAST 

The economy in California and elsewhere likely will determine the viability of neo-feudalism. California’s New Feudalism Benefits a Few at the Expense of the Multitude |Joel Kotkin |October 5, 2013 |DAILY BEAST 

Yet except for occasional rumbling from the left, neo-feudalism likely represents the future. California’s New Feudalism Benefits a Few at the Expense of the Multitude |Joel Kotkin |October 5, 2013 |DAILY BEAST 

The spirit of feudalism and of the old chivalry had all but departed, but had left a vacuum which was not yet supplied. The English Church in the Eighteenth Century |Charles J. Abbey and John H. Overton 

If it were a republic to-morrow, it would be a monster in legislation—half-jacobinism, half-feudalism. The History of England in Three Volumes, Vol.III. |E. Farr and E. H. Nolan 

Though feudalism as a form of government is no longer fashionable, it still survives in spirit. Ways of War and Peace |Delia Austrian 

In the act has been seen the formal acceptance and date of the introduction of feudalism, but it has a very different meaning. Landholding In England |Joseph Fisher 

The system (if such a word can be applied at all) was in fact a bad form of feudalism without its advantages. Is Ulster Right? |Anonymous