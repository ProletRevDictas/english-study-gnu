The team used optics techniques to bend the tube into a circular shape, creating a vortex ring. Scientists created ‘smoke rings’ of light |Emily Conover |June 10, 2022 |Science News 

Milestones in optics and imaging technology have enabled astronomers to observe a great deal of the universe’s history. The James Webb telescope will soon be hunting for ‘first light’ |Paola Rosa-Aquino |December 20, 2021 |Popular-Science 

At the brand’s optics lab in France, designers machine-simulated 20 million possible combinations of wavelength filters. The Best Sunglasses of Winter 2022 |jversteegh |October 26, 2021 |Outside Online 

This may be more of an optics issue, restoring confidence with the American people, and in my state, we actually do believe there was tremendous fraud. Jake Tapper Grills Texas Lawmaker on State’s Strict Voter Restriction Bill |Justin Baragona |May 30, 2021 |The Daily Beast 

He mined the staff of Tessera, an optics business in North Carolina, to build a manufacturing team. Magic Leap tried to create an alternate reality. Its founder was already in one |Verne Kopytoff |September 26, 2020 |Fortune 

Irritated members of Congress say that the authorization of the train-and-equip mission is merely about optics. U.S. Hasn’t Even Started Training Rebel Army to Fight ISIS |Tim Mak |November 25, 2014 |DAILY BEAST 

But, having himself campaigned by hitting others for their lack of involvement, Perry obviously understands the optics of crisis. Rick Perry Avoided Ebola Like the Plague |Olivia Nuzzi |October 16, 2014 |DAILY BEAST 

This prompted a lengthy discussion of optics and theatre among the panelists. Todd Brings Goatee and Game to MTP Debut |Lloyd Grove |September 7, 2014 |DAILY BEAST 

In terms of the optics, let us not forget that the United States is facing a new and extremely dangerous enemy in ISIS. Yes, Obama Was Right to Golf After Foley |Daniel G. Hill |August 30, 2014 |DAILY BEAST 

And despite the urgent optics of the border kids crisis, our D.C. politicos would rather demagogue the issue than deal with it. Border Kids Crisis—Impotent Congress |John Avlon |July 10, 2014 |DAILY BEAST 

Scientists tell us that from the point of view of optics the human eye is a clumsy instrument poorly contrived for its work. The Unsolved Riddle of Social Justice |Stephen Leacock 

The Optics were not published till 1704, but had been composed many years previously. Notes and Queries, Number 196, July 30, 1853 |Various 

My two big toes placed simultaneously over both his optics caused a halt so abrupt as almost to unseat me. The Adventures of Louis de Rougemont |Louis de Rougemont 

"Peleg, you're a sight for tired optics," said Tom, giving the man's hand a squeeze that made him wince. The Rover Boys on the Farm |Arthur M. Winfield (AKA Edward Stratemeyer) 

The Professor of Astronomy was to teach astronomy, optics, navigation, and cosmography. Art in England |Dutton Cook