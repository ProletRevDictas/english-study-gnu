From eyeshadow palettes, skin care sets, fragrances, and candles, here’s what you should be shopping if you’re looking for last-minute perfection. 10 Beauty Gifts That Are Actually Still In Stock |cmurray |December 18, 2020 |Essence.com 

The palette emphasizes green and blue, and the surfaces are layered and scraped to suggest continual flux. In the galleries: Up to his elbows in watery works and lustrous prints |Mark Jenkins |December 18, 2020 |Washington Post 

The Mink Printer by MinkCustom makeup in 15 secondsThere’s a certain giddiness that comes from sampling makeup hues in the store, but publicly shared palettes became a lot less appealing this year. The 100 greatest innovations of 2020 |Popular Science Staff |December 2, 2020 |Popular-Science 

The cornucopia of palettes somehow never feels like too much, but rather, it functions almost like a sewn-together quilt that uses a kaleidoscope of fabrics that come together beautifully. How to Bring This Portland Restaurant’s Colorful Outdoor Oasis to Your Home |Emma Orlow |October 30, 2020 |Eater 

With this plugin, you will be able to create consistent color themes, appealing visuals, and come up with perfect palettes. Top 15 Chrome extensions for social media marketers |Bhavik Soni |September 23, 2020 |Search Engine Watch 

The image, with all of its sketchy lines and minimal color palette, had to be rendered in a matter of seconds. O.J., Martha, Jagger, and Manson: Capturing Celebrities in the Dock |Justin Jones |May 29, 2014 |DAILY BEAST 

Jagged walls of rock, a palette of blacks and greys, loom over us. Want to Write a Book? Go to Iceland |Adam LeBor |May 26, 2014 |DAILY BEAST 

Your work always seems to have a strict palette of black, white, and gold. Lina Viktor Is the Artist Who Paints With Gold |Erin Cunningham |May 23, 2014 |DAILY BEAST 

The color palette in Batman Begins is something I brought to the party, too—that rusty, sodium-vapor color. How ‘Transcendence’ Director Wally Pfister Became Christopher Nolan’s Secret Weapon |Andrew Romano |April 17, 2014 |DAILY BEAST 

For me the notion of mixing the warm light of fire with the cool light of dusk, that created a color palette. How ‘Transcendence’ Director Wally Pfister Became Christopher Nolan’s Secret Weapon |Andrew Romano |April 17, 2014 |DAILY BEAST 

Make a dash at the white, put it in the middle of the palette, and then tone it down to the green? The Painter in Oil |Daniel Burleigh Parkhurst 

You can't mix colors with any degree of certainty if the palette is smeared with all sorts of tints. The Painter in Oil |Daniel Burleigh Parkhurst 

Have your palette set the same way always, so that your brush can find the color without having to hunt for it. The Painter in Oil |Daniel Burleigh Parkhurst 

Never put a new color on your palette unless you feel the actual need of it, or have a special reason for it. The Painter in Oil |Daniel Burleigh Parkhurst 

The painter's hand paused between palette and canvas, and his face was turned toward the speaker in wonder. The Shepherd of the Hills |Harold Bell Wright