Police picked up the flute from the pawnshop on Wednesday, Rabin said. ‘FLUTE EMERGENCY’: A musician forgot his $22,000 instrument on a Chicago train. A homeless man found it. |Andrea Salcedo |February 5, 2021 |Washington Post 

Retrieving the flute had little to do with its monetary worth, he said. ‘FLUTE EMERGENCY’: A musician forgot his $22,000 instrument on a Chicago train. A homeless man found it. |Andrea Salcedo |February 5, 2021 |Washington Post 

The flute is my livelihood and I’m trying every possible thing I can do to get it back. ‘FLUTE EMERGENCY’: A musician forgot his $22,000 instrument on a Chicago train. A homeless man found it. |Andrea Salcedo |February 5, 2021 |Washington Post 

Meanwhile, Gabe Coconate, the owner of West Town Jewelry & Loan, told the Chicago Sun-Times he had already called police on Monday after his wife recognized the flute on a news report. ‘FLUTE EMERGENCY’: A musician forgot his $22,000 instrument on a Chicago train. A homeless man found it. |Andrea Salcedo |February 5, 2021 |Washington Post 

A six-pack of 12-ounce cans works out to the equivalent of nine champagne flutes, which is great for a group. The Best Canned Cocktails Available Now |AC Shilton |July 17, 2020 |Outside Online 

If you drink from a flute, do so from a tulip-shape one to concentrate the notes, Simonetti-Bryan says. Champagne: You’re Drinking It All Wrong |Kayleigh Kulp |December 20, 2014 |DAILY BEAST 

By the time of the recording session, Brian had become quite agile with the flute and suggested adding it to the song. ‘The Prince of Chocolate City’: When Gil Scott-Heron Became A Music Icon |Marcus Baram |November 15, 2014 |DAILY BEAST 

Dodge was on his way to study the flute in Paris, but he decided to buy the bike, anyway. Pryor Dodge's Two-Wheeled Obsession Is Now a Museum of Bike History |Anthony Haden-Guest |September 15, 2014 |DAILY BEAST 

Despite the sheer hilarity of the music itself, Detweiler claims that the flute drops are not an intentional joke. The Mystery of FluteDrop: D.J. Detweiler Pairs Miley Cyrus With Woodwinds |Gideon Resnick |March 5, 2014 |DAILY BEAST 

At age 5, Desplat began to play the piano; his attention eventually turned to flute. Meet Alexandre Desplat, Hollywood’s Master Composer |Andrew Romano |February 11, 2014 |DAILY BEAST 

The flute and the psaltery make a sweet melody, but a pleasant tongue is above them both. The Bible, Douay-Rheims Version |Various 

The flute, a component part of the organ, is one of the most ancient of musical instruments. The Recent Revolution in Organ Building |George Laing Miller 

By blowing across this ring a fair but somewhat feeble Flute tone is produced. The Recent Revolution in Organ Building |George Laing Miller 

The most admirable instruments of this characteristic have been variously compared to a flute or to the female voice. Violins and Violin Makers |Joseph Pearce 

We have worked this out for all classes of tone—string, flute and diapason—and the law holds good in every instance. The Recent Revolution in Organ Building |George Laing Miller