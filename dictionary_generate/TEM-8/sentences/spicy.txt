The result is a spicy, aromatic wine with scents of lemongrass, orange peel and cardamom, plus ample mouthwatering acidity to make you come back for more. This $15 pinot grigio from Italy’s mountains is bracing and edgy |Dave McIntyre |October 30, 2020 |Washington Post 

Its neutral taste pairs nicely with seasonings across the spectrum, from creamy to herbal to spicy, and the nooks and crannies of meat plus the cup of the shells help mussels capture and deliver flavor in every bite. Mussels are simple to prepare, and add tasty drama to the table |Martha Holmberg |October 29, 2020 |Washington Post 

Two promising entries off to fast starts are spicy Canada Dry Bold, and Dr Pepper & Cream Soda. Keurig is a machine: How the beverage giant is leveraging A.I. to fuel growth |Shawn Tully |October 19, 2020 |Fortune 

Just because I am brown and from Southeast Asia, does not mean that I love spicy food. The American dating dilemma for immigrants |Tausif Sanzum |October 9, 2020 |Washington Blade 

The mouthfeel is warm, chewy, crispy, and slightly spicy, and the soupy consistency made for a hydrating dish. The Best Ready-to-Eat Backpacking Meals |Jenny McCoy |October 8, 2020 |Outside Online 

I'm pleased with my decision to avoid the routine script problems in favor of the spicy stuff. Alfred Hitchcock’s Fade to Black: The Great Director’s Final Days |David Freeman |December 13, 2014 |DAILY BEAST 

That seemed to imply a spicy sex life, I say to him the next day. Gay Activist David Mixner: I Mercy Killed 8 People |Tim Teeman |October 29, 2014 |DAILY BEAST 

“I love spicy food, and my friends tell me that Sichuan hotpot has become very popular in Addis Abeba,” he said. 'Made in China' Now Being Made in Africa |Brendon Hong |August 23, 2014 |DAILY BEAST 

Seeing how the sausage is made is always interesting—even if the sausage isn't particularly spicy. Generic and Superficial ‘Tyrant’ Amerisplains the Middle East |Andrew Romano |June 25, 2014 |DAILY BEAST 

Stewed apples, dusted with cinnamon, are an ideal companion to spicy food. Charlottesville Is Swimming in Finger Lickin’ Gas Station Fried Chicken |Jane & Michael Stern |May 26, 2014 |DAILY BEAST 

She saw his eyes light up with eager pleasure as he placed the flowers close to his pillow, and inhaled their spicy fragrance. They Looked and Loved |Mrs. Alex McVeigh Miller 

Farther along, striped grass, mints, herbs and balsams made the air heavy with spicy odors when the dew was on the grass. Maid Sally |Harriet A. Cheever 

In ten minutes more he was in the spicy glooms of the spruce-woods. The Watchers of the Trails |Charles G. D. Roberts 

How inferior to these the trees of Lebanon in sacrifice, or all the spicy mountains of Arabia in a blaze! Female Scripture Biographies, Vol. I |Francis Augustus Cox 

To the farmer's peremptory call he returned the spicy repartee of a cheerful bark. Greyfriars Bobby |Eleanor Atkinson