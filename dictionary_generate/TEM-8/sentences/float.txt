While the rest of us quarantine at home, influencers will lounge on pool floats, host meet-and-greets with fans, party together. How a TikTok house destroyed itself |Rebecca Jennings |October 1, 2020 |Vox 

Near a good halfway point for float trips on the Current River, two sides of this dreamy tiny house in the Ozarks are bordered by Shannondale State Forest. Our Favorite Hipcamp in Every State |Alison Van Houten |October 1, 2020 |Outside Online 

Achim Randelhoff, an oceanographer at Université Laval in Quebec City, and colleagues deployed autonomous submersible floats in Baffin Bay that can measure photosynthetic activity and algae concentrations underwater. Trapped under ice, light-loving algae grow in the dark Arctic winter |Jonathan Lambert |September 25, 2020 |Science News 

Here’s a list of some of my favorite water gear that helps me enjoy the remaining hot days, whether that’s taking a canyoneering trip or doing a lazy float with the kids. The Gear That Lets Me Enjoy the Last Days of Summer |Graham Averill |September 15, 2020 |Outside Online 

If you don’t feel the need for speed, the Super Mable makes a great pool or off-shore float. The best boating tubes |PopSci Commerce Team |September 3, 2020 |Popular-Science 

In the meantime, Epstein has tried to use his charitable projects to float him back to the top. Sleazy Billionaire’s Double Life Featured Beach Parties With Stephen Hawking |M.L. Nestel |January 8, 2015 |DAILY BEAST 

You can go as deep as you like, or float about on the surface. D’Angelo’s ‘Black Messiah’ Was Worth Waiting 15 Years For |James Joiner |December 16, 2014 |DAILY BEAST 

He allows the subject to float over to Hitchcock with a calm directness that I admire. Alfred Hitchcock’s Fade to Black: The Great Director’s Final Days |David Freeman |December 13, 2014 |DAILY BEAST 

They haven't been dead long enough to float, but that will come in time. Whatever You Do Someone Will Die. A Short Story About Impossible Choices in Iraq |Nathan Bradley Bethea |August 31, 2014 |DAILY BEAST 

But miraculously they must float in the heavens so far away from us, their beautiful light will continue to shine on us forever. Billy Crystal's Tribute to Robin Williams at the Emmys Was Perfect |Kevin Fallon |August 26, 2014 |DAILY BEAST 

One frequently wishes to ascertain the specific gravity of quantities of fluid too small to float an urinometer. A Manual of Clinical Diagnosis |James Campbell Todd 

The cloud coals grow fainter—now purple; and now in ashes they float away into the chill blue. The Soldier of the Valley |Nelson Lloyd 

A huge float comes along, depicting the stone age and the primitive man, every detail carefully studied from the museums. The Real Latin Quarter |F. Berkeley Smith 

Slowly did they float through the darkness of the night, appearing like the work of fairy hands. A Woman's Journey Round the World |Ida Pfeiffer 

Seeds of plants incased in their often dense envelopes may, because they float, be independently carried great distances. Outlines of the Earth's History |Nathaniel Southgate Shaler