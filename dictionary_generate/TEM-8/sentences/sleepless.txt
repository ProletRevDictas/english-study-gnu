The past four nights had been a sleepless blur as she watched the terror of Afghans desperate to escape all over television and social media—including her own family members. 'Completely Helpless.' Afghan Americans Scramble to Help Family Back Home |Vera Bergengruen |August 17, 2021 |Time 

All the fear and sleepless nights and being afraid of tomorrow and returning to detention. A Belarusian Olympic Athlete Found Protection in Japan. Most Refugees Do Not |Tim Hornyak/Tokyo |August 5, 2021 |Time 

It can be a great swap if you want to go really ultralight, but in buggy regions prone to mosquitoes, this may only result in a lot of sleepless nights. Ultralight backpacking hacks no one tells you about |Sandra Gutierrez |July 29, 2021 |Popular-Science 

That way, you’re set up and ready to go when the crazy sleepless nights begin. The best breast pump for comfortable, effective pumping wherever you are |Billy Cadden |July 26, 2021 |Popular-Science 

Pregnancy exhausted me, and I barely made it through the initial sleepless months of having a newborn. I Tried to Live Off Women-Owned Businesses. Turns Out, Men Still Run Everything |Alana Semuels |July 21, 2021 |Time 

It was a sleepless night in Chechnya for two lawyers from the Russian town of Orenburg, Sergei Babinets and Dmitriy Dimitriyev. Putin’s Favorite Acolyte Terrorizes Human Rights Activists |Anna Nemtsova |December 14, 2014 |DAILY BEAST 

Mama, I understand your many sleepless nights / When you sit and think about father / Or how you tried to be the perfect wife. Beyonce’s New “7/11” and “Ring Off” Will Give You Reason to Live (And Dance) |Kevin Fallon |November 21, 2014 |DAILY BEAST 

I blame a sleepless night or free drinks at a cocktail party. Mommy’s Little Secret? Coffee And Booze. |Sally Kohn |May 11, 2014 |DAILY BEAST 

And behind the sleepless Moms will come binders full of freshly scrubbed lawyers looking to turn a buck on the news. Could Tylenol Cause ADHD? |Kent Sepkowitz |February 25, 2014 |DAILY BEAST 

Now we are learning that it is not just sleepless nights looking after Prince George that may have unsettled his mood. Hacking Trial Delivers an Unwelcome Christmas Present For William and Kate |Tom Sykes |December 19, 2013 |DAILY BEAST 

She moved slightly, like a dreamer in pain, as again she faced the creed she had hated through many a sleepless night. Uncanny Tales |Various 

So excited was he at the thought of the great honour that was to be his that he spent almost a sleepless night. Our Little Korean Cousin |H. Lee M. Pike 

There were no more sleepless nights, fearing an attack from the dreaded rebel or the volunteer. The Philippine Islands |John Foreman 

The story was brought to a proper and blissful conclusion; still Sue was sleepless. Tessa Wadsworth's Discipline |Jennie M. Drinkwater 

They spent sleepless nights, and it was especially at such times that they would sing hymns. Skipper Worse |Alexander Lange Kielland