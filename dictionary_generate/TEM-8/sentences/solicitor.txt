She previously served in the solicitor general’s office and has argued nine times before the Supreme Court, including twice last year. Elizabeth Prelogar confirmed as solicitor general ahead of Supreme Court battles over abortion, guns |Ann Marimow |October 29, 2021 |Washington Post 

The Post also reports that “it is possible that when he reveals his decision, he also will announce picks for deputy attorney general, associate attorney general and solicitor general.” It takes a team to fix the Justice Department |Jennifer Rubin |December 27, 2020 |Washington Post 

Although justices haven’t yet responded to the request, they’re likely to allow the acting solicitor general to participate given the federal implications of the case. Supreme Court sets Nov. 4 to hear if Catholic agency can reject LGBTQ parents |Chris Johnson |August 19, 2020 |Washington Blade 

Olson was later successfully nominated for the post of Solicitor General by Bush in 2001. How Gay Marriage Was Won: Prop 8’s Destruction Captured In HBO Movie |Tim Teeman |June 6, 2014 |DAILY BEAST 

Her solicitor also said that Lauren was “upset and embarrassed” by her actions, which she said were “out of character”. Designer’s Niece In Yuppie Air Rage |Tom Sykes |January 8, 2014 |DAILY BEAST 

Some of her books were purchased from her solicitor before the property was sold. What Can You Learn About Writers From Their Personal Libraries? |Richard Oram |September 17, 2013 |DAILY BEAST 

Justice Elena Kagan was recused from the case because she participated in the suit as solicitor general. Affirmative Action Lives! What Happened at the Supreme Court |Adam Winkler |June 24, 2013 |DAILY BEAST 

Studies show that, in the past, the solicitor general won approximately 70 percent of its cases in the Supreme Court. Obama’s Terrible, Awful, Horrible Year at the Supreme Court |Adam Winkler |June 21, 2013 |DAILY BEAST 

Yes, well, you tell the solicitor that you will take the fifty thousand pounds, but you don't want the name. First Plays |A. A. Milne 

I understood, Mr. Clifton, that you were the solicitor employed to wind up the affairs of the late Mr. Antony Clifton. First Plays |A. A. Milne 

Such were the recollections of the family solicitor many years after the events had passed. Life of Richard Trevithick, Volume II (of 2) |Francis Trevithick 

Unfortunate Dabbler, now upon his mettle, declared that "should he ever want satisfaction, his solicitor should get it for him." The Pit Town Coronet, Volume II (of 3) |Charles James Wills 

The following year found Campbell solicitor-general, a knight and member for Dudley, which he represented till 1834. Encyclopaedia Britannica, 11th Edition, Volume 5, Slice 2 |Various