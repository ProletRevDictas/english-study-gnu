Atlanta has started bolstering its defenses against climate change, but in some cases this has only exacerbated divisions. Climate Change Will Force a New American Migration |by Abrahm Lustgarten, photography by Meridith Kohut |September 15, 2020 |ProPublica 

Much like the last time this club looked worthy of a title, in 2018, this defense is versatile enough to switch its assignments to guard ball screens. Everything Should Be On The Table For The Houston Rockets. Even James Harden’s Future. |Chris Herring (chris.herring@fivethirtyeight.com) |September 14, 2020 |FiveThirtyEight 

That key defense won’t change as more suits are filed, Kammer said. Got interruption insurance? These companies found it’s useless in the age of COVID-19 |Bernhard Warner |September 12, 2020 |Fortune 

Tensions have been mounting locally between federal defense attorneys and prosecutors over practices during the pandemic. Federal Jail Downtown Now Has One of the Country’s Worst COVID Outbreaks |Maya Srikrishnan |September 10, 2020 |Voice of San Diego 

Matthews and his colleagues hypothesize that the plants might produce hexyl glucoside chemicals as a defense against the pests. How does a crop’s environment shape a food’s smell and taste? |Carolyn Beans |September 10, 2020 |Science News 

I went into the audition as Fericito, the Venezuelan percussionist, and then I did a self-defense expert. Coffee Talk with Fred Armisen: On ‘Portlandia,’ Meeting Obama, and Taylor Swift’s Greatness |Marlow Stern |January 7, 2015 |DAILY BEAST 

There is just no way of selling this picture with an innocent defense like, “she just asked for a snap.” Buckingham Palace Disputes Sex Allegations Against Prince ‘Randy Andy’ |Tom Sykes |January 4, 2015 |DAILY BEAST 

But, but … there was a token black girl in the background, Target cried in its defense! One Vogue Cover Doesn’t Solve Fashion’s Big Race Problem |Danielle Belton |January 2, 2015 |DAILY BEAST 

But the fun starts when conservatives stop playing defense and go on offense. Steve Scalise and the Right’s Ridiculous Racial Blame Game |Michael Tomasky |January 2, 2015 |DAILY BEAST 

And from there, the letter asked for money for a legal defense fund. The Monsters Who Screamed for Dead Cops |Jacob Siegel |December 23, 2014 |DAILY BEAST 

And all the while she fought him, she punctuated her blows with words, some abusing him, others in defense of her father. The Homesteader |Oscar Micheaux 

That, in the light of present conditions, is the most important thing for the necessary maintenance and defense of these islands. The Philippine Islands, 1493-1898, Volume XX, 1621-1624 |Various 

Everything was placed in the best state of defense that time allowed, so that any attack of the enemy could be repulsed. The Philippine Islands, 1493-1898, Volume XX, 1621-1624 |Various 

I sent an order and what was necessary for the fortification at Oton, and had that port put in a state of defense. The Philippine Islands, 1493-1898, Volume XX, 1621-1624 |Various 

Les rivieres de Seine & d'Aulbe rendent le lieu de cette Baronnie autant agreable, que fort & avantageux la defense. The Jesuit Relations and Allied Documents, Vol. II: Acadia, 1612-1614 |Various