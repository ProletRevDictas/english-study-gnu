The bar has been a cornerstone of American culture from the very beginning. Bar Rescue: Pandemic Edition |jakemeth |September 15, 2020 |Fortune 

A cornerstone of American elections has been the peaceful transition of power, but as research from the Transition Integrity Project and others underscores, there are multiple ways to contest an election. What If Trump Loses And Won’t Leave? |Geoffrey Skelley (geoffrey.skelley@abc.com) |September 14, 2020 |FiveThirtyEight 

Many of the cornerstones of the TV advertising calendar have been canceled or disrupted, and there’s nothing to fill the gap. Deep Dive: How the Summer of 2020 forced brand marketing to change for the better |jim cooper |September 14, 2020 |Digiday 

GM is already building a nearly 3-million-square-foot factory that will mass produce Ultium battery cells and packs, the cornerstone of the company’s strategy to bring those electric vehicles to market in the next three years. GM shifts Corvette engineering team to its electric and autonomous vehicle programs |Kirsten Korosec |August 28, 2020 |TechCrunch 

Cook has turned the app store into the cornerstone of a services division that he set out to expand four years ago. Apple CEO Tim Cook is fulfilling another Steve Jobs vision |Rachel Schallom |August 24, 2020 |Fortune 

Alexander Stephens, vice president of the Confederacy, summed up the Southern attitude in his 1861 Cornerstone Speech. Steve Scalise Shows There’s a Fine Line Between Confederate & Southern |Lloyd Green |January 2, 2015 |DAILY BEAST 

The cornerstone of our democracy is that justice is to be colorblind in its administration. As Michael Brown Grand Jury Winds Down, Is Ferguson on the Brink of War? |Ron Christie |November 16, 2014 |DAILY BEAST 

Giants are the cornerstone of the myths, legends, and traditions of almost every culture on Earth. Hunting for a Real-Life Hagrid |Nina Strochlic |November 13, 2014 |DAILY BEAST 

Back then, property was understood by universal consensus as a foundational cornerstone of human liberty and a life worth living. How Young People Are Destroying Liberty |James Poulos |October 11, 2014 |DAILY BEAST 

The character-building cornerstone of American life has lately come under fire for ills ranging from racism to concussions. Has Football Jumped the Shark? |Kevin Canfield |September 1, 2014 |DAILY BEAST 

To lose our privileges would be to lose the very cornerstone of our liberty. The Status Civilization |Robert Sheckley 

The Saratoga trunk is not the best cornerstone for the home: so much we may take for granted. America To-day, Observations and Reflections |William Archer 

They went as a team and gave me about as much chance to escape as if I'd been a horned toad sealed in a cornerstone. Highways in Hiding |George Oliver Smith 

Pizarro was now very busy in developing the new country he had conquered, and in laying the cornerstone of a nation. The Spanish Pioneers |Charles F. Lummis 

Confidence—a justified confidence—is therefore the cornerstone of morale. Manpower |Lincoln Clarke Andrews