Protection will also be needed if variants that are more virulent become predominant in the United States, experts say. As delays hamper some second coronavirus vaccine doses, a debate rages: Prioritize one shot or two? |Meryl Kornfield |February 26, 2021 |Washington Post 

Beginning late Wednesday night, some short-range models predicted that sleet would be the predominant form, after an initial burst of snow. Sleet vs. snow: The reason behind Thursday’s icy mess |Jeffrey Halverson |February 19, 2021 |Washington Post 

He also talks up the potential of Skeleton’s ultracapacitors in maintaining electrical-grid stability as relatively unpredictable renewables become more predominant. Graphene gets real: Meet the entrepreneurs bringing the wonder substance to market |David Meyer |December 13, 2020 |Fortune 

Meeting it will require a sweeping overhaul of the country’s energy system—a transition complicated by the fact that the country is still rapidly building new coal-fired power plants, by far the country’s predominant energy source. China is rapidly becoming the leading nuclear energy superpower |Tim McDonnell |September 30, 2020 |Quartz 

As a physical good with a complicated manufacturing process, it seemed to be under more threat than streaming, the predominant way people now listen to music. Vinyl sales rock on in spite of Covid-19 |Dan Kopf |August 21, 2020 |Quartz 

Man vs. Nature is the predominant theme of the film, and I always tried to go back to that imagery. 'Godzilla' Director Gareth Edwards Says Godzilla Is a 'God' Protecting Mankind Against Climate Change |Marlow Stern |May 14, 2014 |DAILY BEAST 

The predominant focus of cancer drug development today is on “targeted therapies” that are both innovative and lucrative. How Big Pharma Holds Back in the War on Cancer |ProPublica |April 23, 2014 |DAILY BEAST 

The industry was shifting and writers were becoming predominant in the success of shows. ‘Hocus Pocus’ Turns 20: Meet the Voice Behind Binx the Talking Cat |Kevin Fallon |October 31, 2013 |DAILY BEAST 

The predominant school of thought holds that the markets are irrationally acting—and crashing—in response to the news. Let’s All Please Stop Overreacting to Bernanke’s Remarks |Daniel Gross |June 20, 2013 |DAILY BEAST 

In an election season in which the economy is the predominant issue, the report has assumed even greater importance. Jobs Report: Dan Gross on the Dueling Data |Daniel Gross |August 3, 2012 |DAILY BEAST 

In the parish churches, many of which are of great interest, the predominant styles are Decorated and Perpendicular. Encyclopaedia Britannica, 11th Edition, Volume 3, Slice 4 |Various 

Then began the present aspect of field and forest; and modern types of animals were introduced and became predominant. Gospel Philosophy |J. H. Ward 

In the lowest animals touch was the predominant, if not the only sense, taste perhaps being associated with it. Man And His Ancestor |Charles Morris 

We do not indeed know how the predominant character of the Mes was shown—whether, for example, the melody ended on the Mes. The Modes of Ancient Greek Music |David Binning Monro 

Marie Touchet, who was very young when brought to court, came at an age when all the noblest sentiments are predominant. Catherine de' Medici |Honore de Balzac