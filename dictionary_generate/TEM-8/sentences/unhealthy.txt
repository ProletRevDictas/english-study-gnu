Eating disorders are a very real issue for a lot of young people and to see Kim Kardashian actively encouraging her fans to develop an unhealthy relationship with food is terrifying and gravely concerning. Bachelor Stars Are Promoting a Birth Control App on Instagram That Experts Say Uses One of the Least Effective Contraceptive Methods |Megan McCluskey |February 4, 2021 |Time 

I would have never dreamed that she would have lied to us like this so early on and sought out such unhealthy negative attention. Good kids sometimes lie to their parents. Shame and harsh punishments will only push them away. |Meghan Leahy |January 20, 2021 |Washington Post 

Participants tend to be interested in movement, eating well and avoiding unhealthy habits — and they support other community members’ exercise, Anderson found. Local cross-country skiing can smooth a bumpy winter’s ups and downs |Erin E. Williams |January 7, 2021 |Washington Post 

There are also cases in which it’s been effective to nudge people to stop unhealthy behaviors. Why Paying People To Get The Coronavirus Vaccine Won’t Work |LGBTQ-Editor |December 12, 2020 |No Straight News 

Avoid impulse purchasing unhealthy snacks or items you won’t realistically use. As stay-at-home restrictions rise, here are ways to cope |Washington Post Staff, Elizabeth Chang, Mari-Jane Williams, Becky Krystal, Kendra Nichols, Caitlin Moore, Stephanie Merry, Missy Rosenberg, Katherine Lee |December 2, 2020 |Washington Post 

Most of us have an unhealthy relationship with anger, writes author and psychologist Andrea Brandt. Can Self-Help Books Really Make a New You? |Lizzie Crocker |December 29, 2014 |DAILY BEAST 

Barbie is an unrealistic, unhealthy, insulting representation of female appearance. Sexism Begins in the Toy Aisle |Nancy Kaffer |November 29, 2014 |DAILY BEAST 

Jordan Younger knew her raw vegan diet had become an unhealthy obsession when she started avoiding going out with friends. Orthorexia: When Healthy Eating Becomes an Obsession |DailyBurn |October 25, 2014 |DAILY BEAST 

You rigidly avoid any food you deem to be “unhealthy,” such as those containing fat, preservatives, additives or animal products. Orthorexia: When Healthy Eating Becomes an Obsession |DailyBurn |October 25, 2014 |DAILY BEAST 

More than 15 million Americans eat unhealthy food because nutritious meals are too expensive. Free Market Failure: Raising a Kid Is a Rigged Game in the USA |Monica Potts |August 25, 2014 |DAILY BEAST 

I presume the twenty-five or thirty miles at this end is unhealthy, even for natives, but it surely need not be so. Glances at Europe |Horace Greeley 

Where the dampness is excessive the fronds take on an unhealthy appearance, and mould may appear. How to Know the Ferns |S. Leonard Bastin 

That community which does not improve in the region where the means of healthful increase are afforded, is in an unhealthy state. The Ordinance of Covenanting |John Cunningham 

What an agitation, and at the same time what an unhealthy stimulus to his over-sensibility! The Life &amp; Letters of Peter Ilich Tchaikovsky |Modeste Tchaikovsky 

He was in the position of a borrower driving a risky trade, or of a would-be insurer who leads an unhealthy life. Ireland Under the Tudors, Vol. II (of 3) |Richard Bagwell