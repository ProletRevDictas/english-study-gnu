As it was, the area’s roadsides yesterday looked like a child’s play yard with toy autos scattered about. How a surprise snowstorm almost spoiled Kennedy’s inauguration 60 years ago |Kevin Ambrose, Jason Samenow |January 19, 2021 |Washington Post 

Everybody’s setup is different, but a laptop and a hot spot, road maps or dedicated GPS, and a roadside emergency kit are a good place to start. Your New Adventure Travel Bucket List |The Editors |December 21, 2020 |Outside Online 

A lot of revenue in cities and states comes from things like tolls and roadside parking, and of course taxes. The road to smart city infrastructure starts with research |Richard Dal Porto |November 30, 2020 |TechCrunch 

That fact was clear on our recent trip to the Adirondack Mountains, where we saw clusters of cars, many with out-of-state license plates, gathered around roadside trailheads like so many flies around a drop of honey. Autumn in the Adirondacks is a mountain do |Debra Bruno |October 30, 2020 |Washington Post 

Maybe you’re crouching down to gaze at the view out the passenger window, or dreaming of possible routes on roadside cliffs. I Hit a Cyclist with My Car |Brooke Warren |October 28, 2020 |Outside Online 

Stasio and his team found U.S. forces under relentless assault from insurgents, roadside bombers, and mortar attacks. How the NSA Became a Killing Machine |Shane Harris |November 9, 2014 |DAILY BEAST 

The U.S. hackers sent fake text messages to insurgent fighters and roadside bombers. How the NSA Became a Killing Machine |Shane Harris |November 9, 2014 |DAILY BEAST 

De Merode slipped from his seat and dove toward the roadside and into the forest. A Belgian Prince, Gorillas, Guerrillas & the Future of the Congo |Nina Strochlic |November 6, 2014 |DAILY BEAST 

It also kills roadside trees, pollutes streams and wells, and destroys gardens. Book Bag: Beguiling if Unlikely Travel Books |Sean Wilsey |September 4, 2014 |DAILY BEAST 

On Sunday, two election workers were killed by a roadside bomb that also destroyed some ballots. Would You Risk Your Life to Vote? It Looks Like 7 Million Afghans Did. |Dean Obeidallah |April 7, 2014 |DAILY BEAST 

For these people, under the older dispensation, there was nothing but the poorhouse, the jail or starvation by the roadside. The Unsolved Riddle of Social Justice |Stephen Leacock 

Shall I sit by the roadside and ask every man who passes by if he wants to hire himself out 'on shares'? Dorothy at Skyrie |Evelyn Raymond 

One is reproduced here, chiefly with the object of showing the pleasing roadside humour of hanging criminals in chains. The Portsmouth Road and Its Tributaries |Charles G. Harper 

At every step she took, her golden curls bobbed against her cheek, and so limping she sat down on a bank by the roadside. Honey-Bee |Anatole France 

All the way the scenery is pretty, but with no very striking features, and villas dot the roadside for a considerable distance. The Portsmouth Road and Its Tributaries |Charles G. Harper