I love how it can take any flavor and change how you perceive it. Why you should be adding salt to your cocktails |By Céline Bossart/Saveur |September 4, 2020 |Popular-Science 

We know that if you have a high perceived risk of the disease, you’re more likely to take the vaccine. A third of Americans might refuse a Covid-19 vaccine. How screwed are we? |Brian Resnick |September 4, 2020 |Vox 

Even with secure recordings, there’s still the somewhat murky issue of just how the algorithm perceives your speech patterns. Amazon’s new fitness tracker listens to your voice to figure out your mood |Stan Horaczek |September 2, 2020 |Popular-Science 

It may be, in part, due to the perceived purpose of the test. America Doesn’t Have a Coherent Strategy for Asymptomatic Testing. It Needs One. |by Caroline Chen |September 1, 2020 |ProPublica 

In the race for San Diego City Council, District 9, the perceived front-runner, Kelvin Barrios, has repeatedly demonstrated extremely poor judgment. Kelvin Barrios Must Drop Out of the D9 Race |Janessa Goldbeck |August 28, 2020 |Voice of San Diego 

People always have to perceive the problems before them, including many unexpected nuances, and decide how to handle them. Red Tape Is Strangling Good Samaritans |Philip K. Howard |December 27, 2014 |DAILY BEAST 

On the other hand, patients may not perceive much downside to taking the medications, even if they may not help much. Without Education, Antibiotic Resistance Will Be Our Greatest Health Crisis |Russell Saunders |December 19, 2014 |DAILY BEAST 

They still saw white policemen killing unarmed African Americans in what they perceive as cold blood and without repercussions. Sharpton Recalls Civil Rights Struggle in DC March Against Police Violence |Ben Jacobs |December 13, 2014 |DAILY BEAST 

They want Americans to perceive Washington as broken, especially heading into 2016. Inside the Democrats’ Godawful Midterm Election Wipeout |Michael Tomasky |November 5, 2014 |DAILY BEAST 

It becomes a hapless gesture of uninformed social media departments who perceive the potential of engagement without consequences. Jameis Winston's Inevitable #epicfail |Gideon Resnick |August 11, 2014 |DAILY BEAST 

His enemies in the cabinet were quick to perceive when their devices had taken effect on the King and Queen. The Pastor's Fire-side Vol. 3 of 4 |Jane Porter 

I perceive no immediate reason for the evacuation of Peking as far as the supply of game is concerned. Punch, or the London Charivari, Volume 107, November 3, 1894 |Various 

This danger Garnache, however, was no less quick to perceive, and with a dismaying promptness did he take his measures. St. Martin's Summer |Rafael Sabatini 

They have an old Cathedral here (now Presbyterian) of which the citizens seem quite proud, I can't perceive why. Glances at Europe |Horace Greeley 

I can perceive none, even though the steamships should still proceed to Liverpool as heretofore. Glances at Europe |Horace Greeley