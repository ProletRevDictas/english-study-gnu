Rand Paul had the benefit of observing both what made his father likable and popular, and what made him an also-ran. Rand Paul’s Daddy Issues |Olivia Nuzzi |July 28, 2014 |DAILY BEAST 

Dozens of other former members and congressional also-rans, both Democrats and Republicans, are squatting on six-figure surpluses. Ex-Politicians Keeping $100 Million in Private Slush Funds |Dave Levinthal, Center for Public Integrity |May 22, 2014 |DAILY BEAST 

The Duke and Duchess of Gloucester and the Duke and Duchess of (confusingly, also-named) Kent were lower-profile Palace neighbors. Malice in the Palace: Prince George’s Treacherous New Digs |Tina Brown |July 27, 2013 |DAILY BEAST 

Ten years ago, Apple began its transformation from an also-ran PC maker to a world-beating tech titan. Welcome to the Anarchy Economy |Daniel Gross |April 23, 2013 |DAILY BEAST 

For a time he wrote criticism for Mawaqif, a publication founded by the Syrian poet (and perennial Nobel also-ran) Adonis. Elias Khoury: Profile of the Essential Arab Novelist Today |Jacob Silverman |August 3, 2012 |DAILY BEAST 

Gandhi himself signed this page on the following day, giving the date also-August 27, 1935. Autobiography of a YOGI |Paramhansa Yogananda 

Five reminders of his homely mug and not a solitary one of the also-rans! The Kingdom Round the Corner |Coningsby Dawson 

He also-458- administered for them the government of Reggio and Modena, their two chief subject cities. Renaissance in Italy: Italian Literature |John Addington Symonds 

Down it went, rapidly, even as they stared, until it hung just off the also-falling asteroid. The Passing of Ku Sui |Anthony Gilmore 

Talk of Pompeii—why, this puts it quite among the "also-rans." War Letters of a Public-School Boy |Paul Jones.