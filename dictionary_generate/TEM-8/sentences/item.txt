I called our local battered women's shelter, and they said they'd love to have the items. Hints From Heloise: New scam tied to coronavirus pandemic |Heloise Heloise |February 12, 2021 |Washington Post 

Sometimes folks looking to sell put items on the Internet and haven’t spelled certain key words correctly. Replacing pieces of flatware or china can be a challenge. Here’s how to track them down. |Laura Daily |February 11, 2021 |Washington Post 

After her death, her husband and son put some of her favorite items in the room where she did yoga. Tiffany Shackelford, 46, was known as a unique, fun ‘force’ to friends, family |Dana Hedgpeth |February 11, 2021 |Washington Post 

Here are a handful of great gear items that will do just that. Outdoorsy Gear Guy–Approved Valentine's Day Gifts |Joe Jackson |February 10, 2021 |Outside Online 

Roughly 88 percent of their diet is deer followed by coyote, raccoon, and smaller prey items. Four wild animals that are thriving in cities |By Ryan Chelius/Outdoor Life |February 9, 2021 |Popular-Science 

They would go to the store with a repackaged and shrink-wrapped broken item inside a new box and return it for full value. The Insane $11 Billion Scam at Retailers’ Return Desks |M.L. Nestel |December 19, 2014 |DAILY BEAST 

The item on federal prisoners was disturbing in a different way. The GOP’s Hidden Ban on Prison Abortions |Harold Pollack |December 13, 2014 |DAILY BEAST 

Another noteworthy item from this study: Women who started having sex earlier found greater satisfaction in college. Was 2014 the Year Science Discovered The Female Orgasm? |Samantha Allen |December 6, 2014 |DAILY BEAST 

“They are very friendly and curious,” The Evening Independent wrote in 1979 in an item emphasizing their popularity and cuteness. Lovable ‘Madagascar’ Penguins Are Known to Rape and Torture in Real Life |Asawin Suebsaeng |November 26, 2014 |DAILY BEAST 

Do not move on to the next item on this list until you have thoroughly answered these questions. Justin Bieber Isn’t Even 21, Yet Makes More Money Than Meryl Streep |Amy Zimmerman |November 25, 2014 |DAILY BEAST 

My mother now tells me that she knew of this mistake, an error of the New York paper in copying the item from a Southern journal. The Boarded-Up House |Augusta Huiell Seaman 

After her death he gave up society, so that this item of expenditure diminished perceptibly. Skipper Worse |Alexander Lange Kielland 

Nor can her knowledge of musical literature have been extensive, for her pupil could not remember a single item in her repertory. The Life &amp; Letters of Peter Ilich Tchaikovsky |Modeste Tchaikovsky 

Suppose that some newspaper should give that item of news, don't you think folks would get the book, when it was published? Mike Marble |Uncle Frank 

Your composing-room door is locked, and the present item of news destined for your readers is not likely to leak out. The Weight of the Crown |Fred M. White