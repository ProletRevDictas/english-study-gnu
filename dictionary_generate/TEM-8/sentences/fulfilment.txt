[sic] You journey now to Vienna in fulfilment of your long frustrated wishes. Beethoven in Love: The Woman Who Captivated the Young Composer |John Suchet |January 26, 2014 |DAILY BEAST 

But no one in Spain and few in Manila as yet could foresee how the fulfilment of the Agreement would be bungled. The Philippine Islands |John Foreman 

Let this be your acknowledgment of past favours—the fulfilment of your sacred promise. Blackwood's Edinburgh Magazine, No. CCCXXXIX. January, 1844. Vol. LV. |Various 

The promises of Bellamy and Planner were as far from fulfilment as ever; their performance as vigorous and disastrous as at first. Blackwood's Edinburgh Magazine, No. CCCXXXIX. January, 1844. Vol. LV. |Various 

The oath is sworn to himself; but He, and those whom he hath vested with office, will demand the fulfilment of it. The Ordinance of Covenanting |John Cunningham 

To my care Six Stars had intrusted her young, and I should be proud of that trust and earnest in its fulfilment. The Soldier of the Valley |Nelson Lloyd