As the auctioneer noted at the opening of the bid, Alice Neel is having a moment. “A Fascinating, Sexy, Intellectually Compelling, Unregulated Global Market.” (Ep. 484) |Stephen J. Dubner |December 2, 2021 |Freakonomics 

Sprenkle graduated from high school in May and has been working at his family’s tire shop while studying to be an auctioneer and taking care of his grandparents. ‘A rush to get shots’ |Ariana Eunjung Cha, Rose Hansen, Jacqueline Dupree |July 30, 2021 |Washington Post 

For that reason, an original Banksy print called Morons, a lampooning of the auctioneer market, was able to sell as a digital token for $380,000 in March even after it was completely burned in a livestreamed video. Harnessing the Power of Gold |Sean Culligan |June 1, 2021 |Ozy 

In 2008, Gilkes joined Phillips, eventually becoming its chief auctioneer. William, Kate, and Jay Z’s Favorite Art Star: Alexander Gilkes' World of Rock Stars and Royalty |Tim Teeman |December 10, 2014 |DAILY BEAST 

The auctioneer talks about knowing and employing royalty, and celebrity big spenders. William, Kate, and Jay Z’s Favorite Art Star: Alexander Gilkes' World of Rock Stars and Royalty |Tim Teeman |December 10, 2014 |DAILY BEAST 

Working there gave him his first experience of attention (he was much younger than your typical auctioneer). William, Kate, and Jay Z’s Favorite Art Star: Alexander Gilkes' World of Rock Stars and Royalty |Tim Teeman |December 10, 2014 |DAILY BEAST 

So when Chicago auctioneer Gabe Fajuri first got a cold call from Collins describing the box, he was skeptical. Get a Piece of Houdini Before He Disappears |Nina Strochlic |August 22, 2014 |DAILY BEAST 

"$450,000," called the auctioneer, pointing to a bidder in the crowd at Christie's. Leonardo DiCaprio’s Big Christie’s Auction Brings in $38.8 Million |Isabel Wilkinson |May 14, 2013 |DAILY BEAST 

If the auctioneer could afterward do this he might change the name, substitute another, and so perpetrate a fraud. Putnam's Handy Law Book for the Layman |Albert Sidney Bolles 

The seller or owner therefore is not bound by any terms stated by the auctioneer differing from those given to him. Putnam's Handy Law Book for the Layman |Albert Sidney Bolles 

I don't talk loud, and anyhow, they're listening to the auctioneer. Patchwork |Anna Balmer Myers 

From the landing already he could hear the voice of the auctioneer. The Child of Pleasure |Gabriele D'Annunzio 

The highest offer has me, your ladyship; he's but a poor auctioneer that knocks down his ware when only one bidder is present. Rookwood |William Harrison Ainsworth