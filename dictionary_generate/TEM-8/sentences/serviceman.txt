Among other things, a “Dear John” issued servicemen a rare license to emote. How World War II's ‘Dear John’ Letters Changed American Society |Susan L. Carruthers |February 14, 2022 |Time 

A vivacious and attractive blonde, Mary had no shortage of male admirers, and at the age of 19 she married a former serviceman. From Hollywood Socialite to Catholic Missionary |The Telegraph |October 27, 2013 |DAILY BEAST 

Twelve years ago, Connie Gruber received news that every wife of an armed serviceman dreads. When the Tragedy of Two Marines Killed In a Crash Becomes a Nightmare |Miranda Green |May 27, 2012 |DAILY BEAST 

To his left, in the gloaming, was a man staring straight at the crowd like a secret serviceman looking for assassins. The Man Who Won't Apologize |Nicholas Wapshott |October 23, 2011 |DAILY BEAST 

The top of my head lifted from its moorings and shifted just enough for me to name that infernal serviceman and all his issue. Cue for Quiet |Thomas L. Sherred 

The serviceman had come out and collected it, clucking in dismay at the mess the extinguisher had left. Cue for Quiet |Thomas L. Sherred 

He flipped off the switch impatiently, and looked at the young Secret Serviceman with wondering eyes. Man of Many Minds |E. Everett Evans 

It came over—I thought he had the radio in the car, Secret Serviceman, and he had talked to someone. Warren Commission (1 of 26): Hearings Vol. I (of 15) |The President's Commission on the Assassination of President Kennedy 

How did the serviceman view his condition, how did he convey his desire for redress, and what was his reaction to social change? Integration of the Armed Forces, 1940-1965 |Morris J. MacGregor, Jr.