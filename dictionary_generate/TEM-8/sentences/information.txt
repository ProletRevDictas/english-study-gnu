Many fishing guides are still operating safe trips to local waters, but you can also find information on local fisheries on Fishbrain, a website and app that uses crowdsourcing to offer incredible detail on local fishing holes. Three Family-Friendly Adventures to Try This Fall |Outside Editors |September 17, 2020 |Outside Online 

Ask for help if you need information, and report any problems you encounter. ProPublica’s Pandemic Guide to Making Sure Your Vote Counts |by Susie Armitage |September 16, 2020 |ProPublica 

He was running for president, and he had the same data and the same information. Trump’s increasingly overt effort to pretend Biden is actually president |Aaron Blake |September 16, 2020 |Washington Post 

Schilling pointed out nothing in the Politifact assessment determined the ad was false, adding the American Principles Project is seeking more information from Facebook and will seek options to appeal. Facebook slaps fact check on ads stoking fears about transgender kids in girls sports |Chris Johnson |September 15, 2020 |Washington Blade 

Even though it affected a smaller region, it gave us some information because it showed how many girls were in school before that and how many returned, and for those that did not return, why that was the case. Malala Yousafzai tells the business community: Education is the best way to guard against future crises |Michal Lev-Ram, writer |September 15, 2020 |Fortune 

Like many trans users, Transartist often gets used as a source of information more than anything else. Grindr’s Trans Dating Problem |David Levesley |January 9, 2015 |DAILY BEAST 

The FBI has also been searching its records for any information that could assist the French investigation, a spokesperson added. U.S. Spies See Al Qaeda Fingerprints on Paris Massacre |Shane Harris, Nancy A. Youssef |January 8, 2015 |DAILY BEAST 

The email appears to have been a relatively common attempt to gain personal information from a wide range of unwitting victims. Was Sony Hit With a Second Hack? |Shane Harris |January 8, 2015 |DAILY BEAST 

The new information consisted of Internet protocol addresses that Comey said are “exclusively used” by North Korea. Was Sony Hit With a Second Hack? |Shane Harris |January 8, 2015 |DAILY BEAST 

The United States government might not release that information for years, if ever. Was Sony Hit With a Second Hack? |Shane Harris |January 8, 2015 |DAILY BEAST 

This information was balm to Louis, as it seemed to promise a peaceful termination to so threatening an affair. The Pastor's Fire-side Vol. 3 of 4 |Jane Porter 

Probably his Private Secretary, considering you a new man, will have failed to furnish the necessary information. Punch, or the London Charivari, Volume 107, November 3, 1894 |Various 

Such a refusal would lead to quick enquiry—enquiry to information—information to want of confidence and speedy ruin. Blackwood's Edinburgh Magazine, No. CCCXXXIX. January, 1844. Vol. LV. |Various 

At the sound of Blanche's name he jumped up and took his usual tone; he knew all about his wife, and needed no information. Confidence |Henry James 

Taken for guerrillas, every Southern sympathizer was eager to give them all the information possible. The Courier of the Ozarks |Byron A. Dunn