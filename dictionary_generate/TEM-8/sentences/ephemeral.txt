There’s no ephemeral rush of terror from scaling icy chimneys or descending hillsides. He escaped the cacophony by strapping on snowshoes and slipping into the Great North Woods |Miles Howard |January 15, 2021 |Washington Post 

Premium over ephemeral seems to be a differentiation strategy that’s worked so far for Triller. ‘Not a place for takeovers’: Pepsi amps up Triller marketing plans |Seb Joseph |January 12, 2021 |Digiday 

Honk introduces a real-time, ephemeral messaging app aimed at Gen Z — Instead of sending texts off into the void and hoping for a response, friends on Honk communicate via messages that are shown live as you type. Daily Crunch: Telegram prepares to monetize |Anthony Ha |December 23, 2020 |TechCrunch 

We also knew that given the ephemeral nature of transits, the odds of an Earth-size planet transiting a sun-like star were only about 1 in 200. My satellite would fit in a small suitcase. |Katie McLean |December 18, 2020 |MIT Technology Review 

For RVers, this stretch of canyon country is a perfect winter journey thanks to the smaller crowds and ephemeral views of dazzling snow on red sandstone. The best winter road trips for RVs, from Michigan’s Upper Peninsula to the Florida Keys |Heather Balogh Rochfort |October 29, 2020 |Washington Post 

Following an all too predictable cycle of the hyperactive 21st century, focus on the explosion was ephemeral. Deepwater Horizon: Life Drowning in Oil |Samuel Fragoso |November 2, 2014 |DAILY BEAST 

Whatever his approval ratings, the Russian leader knows how ephemeral they can be. Putin’s Patriotism is Phony, His Desperation is Real |Andrew Nagorski |April 4, 2014 |DAILY BEAST 

Most news articles were just as ephemeral, but it has become harder to pretend. Richard Ben Cramer Dies at 62 |Justin Green |January 8, 2013 |DAILY BEAST 

It may all be ephemeral, because most of politics is ephemeral, a cynical means to the end of getting elected. Buzz Bissinger: Why I’m Voting for Mitt Romney |Buzz Bissinger |October 8, 2012 |DAILY BEAST 

But as stimulus goes, the I-stimulus strikes me as a pretty narcissistic and ephemeral one. The Annoying iPhone 5 Frenzy: Don’t Believe the Economic Stimulus Hype |Daniel Gross |September 24, 2012 |DAILY BEAST 

Do not suppose that I mean to recommend poor music, or feeble, ephemeral compositions. The Ladies' Book of Etiquette, and Manual of Politeness |Florence Hartley 

The Widow Lawton ornamented her kitchen floor in a manner as ephemeral, though less expensive. The Atlantic Monthly, Volume 17, No. 101, March, 1866 |Various 

Another ephemeral paper, L'Italia del Popolo, was launched on its short career. The Life of Mazzini |Bolton King 

They were, no doubt, chiefly of a pantomimic and ephemeral kind. Art in England |Dutton Cook 

But like all dunghill products, the life of these was ephemeral. Four Years in Rebel Capitals |T. C. DeLeon