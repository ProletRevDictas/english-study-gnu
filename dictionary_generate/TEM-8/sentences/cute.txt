We found twinkle lights that offer a cute decor touch and additional perks, like waterproof features and multiple brightness modes. Twinkle lights that instantly cheer up your home |PopSci Commerce Team |August 27, 2020 |Popular-Science 

If you thought this doc was going to be a compilation of cats annoying their owners and misbehaving in that cute way we can’t resist, you’re so absolutely wrong. This Weekend: You’ll Never Bathe the Same Way Again |Joshua Eferighe |August 21, 2020 |Ozy 

Reels videos, Instagram told them, tended to succeed when they focused on “things that work on Instagram anyway” like dancing, “fluffy cute dogs,” and visual videos that “transcend language” for an international audience. A guide to the TikTokish apps that want to be the next TikTok |Abby Ohlheiser |August 6, 2020 |MIT Technology Review 

This also means the neighborhood is full of delectable bakeries and cute cafes. Chicago: A Midwestern Jewel for the LGBTQ Community |LGBTQ-Editor |July 11, 2020 |No Straight News 

The cute puppy pictured below is the latest addition to my extended family. How to Breed a Pomsky and Other Questions About Dogs and Sex |Pradeep Mutalik |June 18, 2020 |Quanta Magazine 

Wiseman as Samuel is alternately Devil-child and a cute young kid. Grief: The Real Monster in The Babadook |Tim Teeman |December 19, 2014 |DAILY BEAST 

The resulting Wool Runners were comfortable, eco-friendly, machine-washable, and super cute—and sold out almost immediately. The Coolest Fashion Innovations of 2014 |Raquel Laneri |December 18, 2014 |DAILY BEAST 

We coo over how cute our cat is and minimize the drudgery of cleaning the litter box. Why Didn’t Camille Dump Bill Cosby? |Amanda Marcotte |December 17, 2014 |DAILY BEAST 

Legs McNeil, of Punk magazine fame, once called him “cute” and “charming.” ‘All Good Cretins Go to Heaven’: Dee Dee Ramone’s Twisted Punk Paintings |Melissa Leon |December 15, 2014 |DAILY BEAST 

Of those, 1,980 used the term “cute”, while just 12 mentioned the word “ugly”. The Adorable New Prince George Photos |Tom Sykes |December 15, 2014 |DAILY BEAST 

Took in her cute little face whilst she looked straight back at me. Alec Lloyd, Cowpuncher |Eleanor Gates 

Look at this lovely photo of Jessie and Julia, and isn't the frame cute! Tabitha at Ivy Hall |Ruth Alberta Brown 

The very same thing that happened to the chick seed—they burst and out come hundreds of cute little fish minnows. The Mother and Her Child |William S. Sadler 

The man is a pertickler friend o' mine, an' the boy is a cute little chap, an' he can pray better nor any minister in Sevenoaks. Sevenoaks |J. G. Holland 

I used to git kind o' mad at 'im, an' promise to foller 'im, but he's so 'cute, I sort o' like 'im. Sevenoaks |J. G. Holland