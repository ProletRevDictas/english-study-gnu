Since the 1950s, fluoride has adapted itself to the prevailing concerns of the time. Anti-Fluoriders Are The OG Anti-Vaxxers |Michael Schulson |July 27, 2016 |DAILY BEAST 

But give the Kingdom credit for its sense of mercy: The lashes will be administered only 50 at a time. In Defense of Blasphemy |Michael Tomasky |January 9, 2015 |DAILY BEAST 

“I think for trans men who are dating every time they hook up they have another coming out,” Sandler said. Grindr’s Trans Dating Problem |David Levesley |January 9, 2015 |DAILY BEAST 

Eric Garcetti succeeded Villaraigosa and has received high marks in his first year and a half on the job. The Golden State Preps for the ‘Red Wedding’ of Senate Races |David Freedlander |January 9, 2015 |DAILY BEAST 

As far as I can tell, this magazine spent as much time making fun of French politicians as it did of Muslims or Islam. Harry Shearer on The Dangerous Business of Satire |Lloyd Grove |January 8, 2015 |DAILY BEAST 

It ended on a complaint that she was 'tired rather and spending my time at full length on a deck-chair in the garden.' The Wave |Algernon Blackwood 

The vision—it had been an instantaneous flash after all and nothing more—had left his mind completely for the time. The Wave |Algernon Blackwood 

About this time the famous Philippine painter, Juan Luna (vide p. 195), was released after six monthsʼ imprisonment as a suspect. The Philippine Islands |John Foreman 

I hate to be long at my toilette at any time; but to delay much in such a matter while travelling is folly. Blackwood's Edinburgh Magazine, Volume 60, No. 372, October 1846 |Various 

It is the principal waste-product of metabolism, and constitutes about one-half of all the solids excreted—about 30 gm. A Manual of Clinical Diagnosis |James Campbell Todd