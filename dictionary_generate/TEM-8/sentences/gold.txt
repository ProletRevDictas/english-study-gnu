True neutralizing-antibody assays, which are the gold standard, use a live SARS-CoV-2 virus, which means they have to be conducted in a specialized lab with heightened safety and security protocols. Scientists to Wall Street: You don’t really understand how COVID vaccine tests work |Jeremy Kahn |August 24, 2020 |Fortune 

In an Olympic event, an official’s mistake could send the wrong player or team home with the gold medal. A robot referee can really keep its ‘eye’ on the ball |Kathryn Hulick |August 20, 2020 |Science News For Students 

That said, fleeing into “safe haven” assets like gold or treasuries may be premature. Here’s what could happen to stock markets if the Trump-Biden election results are contested |Anne Sraders |August 18, 2020 |Fortune 

“The gold spigot hasn’t launched yet,” Sawtelle said of TikTok. ‘There is a battle going on’: TikTok-Instagram rivalry for creators heating up |Tim Peterson |August 3, 2020 |Digiday 

To ensure that the X chromosome was as accurate as possible, the researchers combined nanopore sequencing with results from a further two gold-standard sequencing technologies and approaches for mapping the genome. For the First Time, Scientists Fully Sequenced the Human X Chromosome |Edd Gent |July 20, 2020 |Singularity Hub 

In straight relationships with an age gap, words like ‘gold-digger’ and ‘trophy wife’ get thrown around. Freaking Out About Age Gaps in Gay Relationships Is Homophobic |Samantha Allen |January 9, 2015 |DAILY BEAST 

And more than anything, I wanted a souvenir for my father, so I rolled him back, and he had gold teeth. The Story Behind Lee Marvin’s Liberty Valance Smile |Robert Ward |January 3, 2015 |DAILY BEAST 

He headed west in 1860 for health reasons and to join the gold rush in Colorado. My Love Letter to the Stetson |Mark McKinnon |December 24, 2014 |DAILY BEAST 

As far as finally being acknowledged herself with that elusive Academy gold, well, Moore says she would not take it for granted. Julianne Moore Is Oscar Gold in ‘Still Alice’ |Marlow Stern |December 24, 2014 |DAILY BEAST 

While panning for gold, he made himself a large hat from the hides he had collected on his trip. My Love Letter to the Stetson |Mark McKinnon |December 24, 2014 |DAILY BEAST 

When she arrived she made a regular entry into the city in a coach all gold and glass, drawn by eight superb plumed horses. Music-Study in Germany |Amy Fay 

On his head was the second-hand hat of some parvenu's coachman, gold lace, cockade and all. The Pit Town Coronet, Volume I (of 3) |Charles James Wills 

A leather swordbelt, gold-embroidered at the edges, carried a long steel-halted rapier in a leather scabbard chaped with steel. St. Martin's Summer |Rafael Sabatini 

M was a Miser, and hoarded up gold; N was a Nobleman, gallant and bold. Boys and Girls Bookshelf; a Practical Plan of Character Building, Volume I (of 17) |Various 

He accuses the latter of various illegal and crafty acts, among them sending contraband gold and jewels to Mexico. The Philippine Islands, 1493-1898, Volume XX, 1621-1624 |Various