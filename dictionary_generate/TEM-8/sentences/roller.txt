Adenovirus needs to be grown as a single layer that’s adhered to something, such as the walls of a roller bottle, which looks very much like a water bottle. Ramping up COVID-19 vaccine production is harder than it seems |Kat Eschner |February 19, 2021 |Popular-Science 

He uses the third-most possessions per game as a roller in the pick-and-roll, and has been more efficient than Anthony Davis in such plays. The Post-Harden Rockets Have A Different Style — And Lots Of Possibilities |Louis Zatzman |February 19, 2021 |FiveThirtyEight 

In roller derby, sometimes you get hit so hard that you don’t think you’ll ever get up again. The money we didn’t spend in 2020 |Vox Staff |December 31, 2020 |Vox 

Tools for working out knotsSometimes stress creates muscle knots so intense, an ice roller just won’t cut it. Gifts for the most stressed out people you know |Rachel Feltman |December 15, 2020 |Popular-Science 

The extra swag doesn’t have to be about roller skating, but we think this option is pretty great. Glide into a new roller skater’s heart with these cool gifts |Rachel Feltman |December 2, 2020 |Popular-Science 

They're made to make a lot of money and to get teenagers in a kind of experience, a roller coaster ride. ‘The Babadook’ Is the Best (and Most Sincere) Horror Movie of the Year |Samuel Fragoso |November 30, 2014 |DAILY BEAST 

People would just be shouting, like you go to church, a holy roller church or something like that. The Stacks: John Coltrane’s Mighty Musical Quest |Nat Hentoff |October 18, 2014 |DAILY BEAST 

My seven-year-old son still can't go on all the roller coasters he wants, because he is a few inches too short. 9-Year Old With an Uzi? America Is Tougher on Toys Than Guns |Cliff Schecter |August 28, 2014 |DAILY BEAST 

His cadence is a steady beat rather than a roller coaster, and his words sparing and simple. What Would You Do if the World Was Over? |William O’Connor |August 5, 2014 |DAILY BEAST 

His Wednesday is going to be a roller-coaster ride from Rush Limbaugh to Fox to Laura Ingraham to who knows what. Eric Cantor’s Primary Loss Is a Political Earthquake. And It’s Awful. |Michael Tomasky |June 11, 2014 |DAILY BEAST 

An amount of slack in the chain caused the balls to knock on passing this roller before entering the pump bottom. Life of Richard Trevithick, Volume II (of 2) |Francis Trevithick 

But before this extreme is reached, the momentum of the revolving balance carries the roller pin entirely out of the fork. The Wonder Book of Knowledge |Various 

I hitched the horse to my improvised drag and smoothed it again, several times, in default of a roller. The Idyl of Twin Fires |Walter Prichard Eaton 

They were of elm, with lignum vit roller sheaves, and were bound inside with iron, and had swivel eyes. Loss of the Steamship 'Titanic' |British Government 

Roller shades on the door and window and an electric door bell completed a very neat and practical playhouse. The Boy Mechanic, Book 2 |Various