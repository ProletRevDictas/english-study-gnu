Whenever stress strikes, they’ll be ready to gently massage their head, neck, face, shoulders, or anywhere else that could use a cooling balm. Gifts for the most stressed out people you know |Rachel Feltman |December 15, 2020 |Popular-Science 

Opportunities to be transported around the world through the pages of a good read have been a balm for adventure seekers. The Best Adventure Travel Books of 2020 |Heather Greenwood Davis |December 15, 2020 |Outside Online 

This consistent practice has offered a balm and a conscious choice to alter my perspective. How parents are turning toward gratitude in the pandemic |Rudri Bhatt Patel |December 10, 2020 |Washington Post 

Aleta Burchyski, copy editorI love gifting fancy lip balm—it’s something that everyone needs but few will buy for themselves. The Gear Our Editors Loved in November |The Editors |December 4, 2020 |Outside Online 

The energy capsules include organic beet and maca powder as well as organic matcha tea, while the sleep capsules include organic lemon balm and goji berries—and neither capsule is habit-forming. Gift Guide: The best beauty and wellness presents of 2020 |Rachel King |November 1, 2020 |Fortune 

On her new lip balm collection, what Tyra taught her and how being a mom inspires her management decisions. Q&A With Designer Rachel Roy |Cynthia Allum |November 3, 2014 |DAILY BEAST 

Before long, they began making shoe polish and, the big moneymaker: lip balm. Burt’s Bees Co-Founder Burt Shavitz on the Doc ‘Burt’s Buzz,’ and Losing Millions |Marlow Stern |September 11, 2013 |DAILY BEAST 

They introduced the lip balm, a combination of beeswax and sweet almond oil, in 1991. Burt’s Bees Co-Founder Burt Shavitz on the Doc ‘Burt’s Buzz,’ and Losing Millions |Marlow Stern |September 11, 2013 |DAILY BEAST 

From Crystal Pepsi to Cheetos Lip Balm, see the culinary innovations we probably could have done without. The 21 Worst Food Ideas Ever | |September 7, 2013 |DAILY BEAST 

But what about odd concoctions we could have done without, like Cheetos lip balm? The 21 Worst Food Ideas Ever | |September 7, 2013 |DAILY BEAST 

Every word that now fell from the agitated Empress was balm to the affrighted nerves of her daughter. The Pastor's Fire-side Vol. 3 of 4 |Jane Porter 

This information was balm to Louis, as it seemed to promise a peaceful termination to so threatening an affair. The Pastor's Fire-side Vol. 3 of 4 |Jane Porter 

The effect was that of a bird of Paradise bringing balm to our overwrought nerves. Gallipoli Diary, Volume I |Ian Hamilton 

The little foolish words, so sweetly commonplace, fell like balm upon an open wound. The Wave |Algernon Blackwood 

She felt a great sense of relief, as if a balm were laid at evening upon the morning's wound. Bella Donna |Robert Hichens