The early-stage startup has invented tech that attaches to the trolley and uses cameras to detect and label products, adding them to a virtual cart where shoppers can checkout without ever interacting with a human or waiting in a line. New Zealand-based Imagr thinks camera-based AI is the future of shopping trolleys |Rebecca Bellan |August 27, 2021 |TechCrunch 

There was a period in the late 1980s, Dave Schumacher, the former San Diego Association of Governments principle planner, told us in 2013, that regional planners contemplated building a trolley spur to the airport. The Airport Connects San Diego to the World, But Not to the Trolley |Andrew Keatts |August 23, 2021 |Voice of San Diego 

The Metropolitan Transit System is on the verge of hiring a new security firm to help patrol bus and trolley stations that has touted a record free of troubling use-of-force incidents. MTS’s Security Firm of Choice Has a More Complicated Record Than it Claimed |Lisa Halverstadt and Jesse Marx |June 22, 2021 |Voice of San Diego 

The Housing Commission also ran ads on billboards, bus benches, in community newspapers and inside trolleys and buses in both English and Spanish. Fearing Money Left on the Table, Officials Push Rental Assistance Program |Maya Srikrishnan |April 21, 2021 |Voice of San Diego 

The agency once said it would improve trolley service along the Blue Line in the South Bay, and to construct a new Purple Line along the I-805 corridor. SANDAG Moving Closer to ‘Coming to Jesus’ Moment on Killed Projects |Andrew Keatts |April 1, 2021 |Voice of San Diego 

Mubarak was present, wheeled in on a hospital trolley and wearing his trademark sunglasses. Mubarak’s Acquittal Signals Complete Triumph of Military Over Arab Spring |Jamie Dettmer |November 29, 2014 |DAILY BEAST 

There, Orange Scott ran the interurban, a turn-of-the-century electric trolley line that connected the boomtown with its exurbs. Those Kansas City Blues: A Family History |Katie Baker |October 24, 2014 |DAILY BEAST 

Four men in army uniform are seen loading massive safes onto a trolley. Photographs Expose Russian-Trained Killers in Kiev |Jamie Dettmer |March 30, 2014 |DAILY BEAST 

A male suicide bomber was also believed to have detonated the Monday blast, which decimated the back half of a trolley bus. Up To Speed: 4 Things To Know About The Russia Bombings | |December 30, 2013 |DAILY BEAST 

Nico steered a luggage trolley piled high with Pippa's bags through the arrivals hall. Did Pippa Get Engaged In India? |Tom Sykes |December 11, 2013 |DAILY BEAST 

The great Dam at Assouan was just completed and we traversed its entire length on a trolley propelled by natives. Fifty Years of Railway Life in England, Scotland and Ireland |Joseph Tatlow 

Presently the thought of the cool trolley-run to the Lake grew irresistible, and they struggled out of the theatre. Summer |Edith Wharton 

The trolley is an articulated frame 77 ft. long in five sections coupled together with pins. Encyclopaedia Britannica, 11th Edition, Volume 4, Part 3 |Various 

I thought how at college I used to hear from my chamber the screech of trolley cars rounding a curve and biting my nerves. The Idyl of Twin Fires |Walter Prichard Eaton 

To reach it from the outer sections of the district, the tracks used by nine lines of trolley cars must be crossed. The Leaven in a Great City |Lillian William Betts