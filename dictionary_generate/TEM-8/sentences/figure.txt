Additionally, since NBCU’s program will be measuring against product sales, advertisers will be able to judge for themselves whether NBCU’s results align with their internal figures. NBCUniversal tests new measurement program to prove it can push product sales for advertisers |Tim Peterson |September 17, 2020 |Digiday 

Instead, the country has seen at least 193,000 deaths, a figure that is probably an underestimation. Trump blames blue states for the coronavirus death toll — but most recent deaths have been in red states |Philip Bump |September 16, 2020 |Washington Post 

As previously reported by Modern Retail, Walmart is currently focused on customer retention and experts expect the membership program to help boost those figures. How Walmart is advertising its new loyalty program, Walmart+ |Kristina Monllos |September 11, 2020 |Digiday 

San Diego Unified has not made similar figures available, but we know the district is worried. The Learning Curve: San Diego Unified Is Terrified of Kids Opting Out |Will Huntsberry |September 10, 2020 |Voice of San Diego 

Kushner, the president’s son-in-law and one of the most influential figures inside his administration, is said to be quoted extensively in Rage. 5 revelations from ‘Rage,’ Bob Woodward’s new book about Trump |reymashayekhi |September 10, 2020 |Fortune 

There were rumors of shrieks and flashes emanating from the well, and reports of a figure in white. New York’s Most Tragic Ghost Loves Minimalist Swedish Fashion |Nina Strochlic |January 8, 2015 |DAILY BEAST 

Even other men of color considered Revels a curious figure, for Mississippi had never had a large free black population. The Black Man Who Replaced Jefferson Davis in the Senate |Philip Dray |January 7, 2015 |DAILY BEAST 

The people who are involved in the violence, they figure out ways to remain here at all costs and continue causing trouble. Mexico’s Priests Are Marked for Murder |Jason McGahan |January 7, 2015 |DAILY BEAST 

Several of them disputed the figure of six million Jewish deaths in the Holocaust. The Louisiana Racists Who Courted Steve Scalise |Jason Berry |January 3, 2015 |DAILY BEAST 

But the last national figure to wield ancient personal authority in an explicitly religious way was Robert F. Kennedy. No Gods, No Cops, No Masters |James Poulos |January 1, 2015 |DAILY BEAST 

He was tall and of familiar figure, and the firelight was playing in the tossed curls of his short, fair hair. The Bondboy |George W. (George Washington) Ogden 

Their opportunities and earnings are relatively small, and in order to live they must figure closely. Readings in Money and Banking |Chester Arthur Phillips 

It was when the face and figure of a great tragedian began to haunt her imagination and stir her senses. The Awakening and Selected Short Stories |Kate Chopin 

Tressan fell suddenly to groaning and wringing his hands a pathetic figure had it been less absurd. St. Martin's Summer |Rafael Sabatini 

Her tall figure—she was taller than he by at least three inches—was beautiful in its commanding, yet not vulgar, self-possession. Bella Donna |Robert Hichens