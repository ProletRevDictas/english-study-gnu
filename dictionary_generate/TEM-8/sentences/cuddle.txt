I like to tell couples to “preserve the cuddle,” but if your different sleep schedules are incompatible, it’s ok to go your separate ways when it’s time for lights out. 'Separate Beds Are a Bad Sign' and 5 Other Sleep Myths That Are Hurting Your Relationship |Wendy Troxel |June 2, 2021 |Time 

Every night, before bed, I spend a couple of quiet, peaceful hours enjoying cuddle time with her on the couch she wasn’t supposed to be allowed on. How a Pandemic Puppy Saved My Grieving Family |Nicole Chung |April 6, 2021 |Time 

For now, it’s crucial to weigh the risk you pose to yourself and others when you go in for a cuddle. Can we ever hug again? |Leigh Cowart |March 12, 2021 |Popular-Science 

During a client call my 6-year old woke up and decided she wanted a cuddle and came down to my office. Broken windows, ‘cuddling breaks’ and interrupted video calls: Parents share realities of juggling work while homeschooling kids |Jessica Davies |February 26, 2021 |Digiday 

Country farms are the stuff of quarantine cottagecore dreams, where animals offer themselves for therapeutic cuddles and no one needs to shuffle off the sidewalk to maintain social distancing. Pick Your Poison |Nick Mancall-Bitel |September 3, 2020 |Eater 

Even her brother, Sheriff, who tried to pick her up to cuddle her, was pushed away with a firm “no” and a shriek. The Life of a Liberian Child with Ebola |Sarah Crowe |November 5, 2014 |DAILY BEAST 

Couples cuddle on ice benches lined with deer pelts for added comfort and warmth. Minus5 is New York’s First Ice Bar |Kelsey Meany |July 24, 2013 |DAILY BEAST 

Almost as clearly, this seems not to have gone beyond an occasional cuddle. Early Signs of General Petraeus’s Extramarital Affair |John Barry |November 11, 2012 |DAILY BEAST 

Trawick appears in one of the photos; they cuddle as he holds a guitar. Britney Spears and Jason Trawick: He’s Her Fiancé, Manager, and Conservator |Ramin Setoodeh |September 11, 2012 |DAILY BEAST 

Announcing the newly improved, perfectly flawed, censure-free Capricorn with added cuddle flavor. Horoscopes: May 8-14 |Starsky + Cox |May 7, 2011 |DAILY BEAST 

Cuddle up against me, darling, and try and go to sleep then. Daybreak |Florence A. Sitwell 

After the late dinner, there was nothing to do but cuddle up in the corner of the sofa with his books. Dew Drops, Vol. 37, No. 7, February 15, 1914 |Various 

Gin I had a bonny wee dog I'd gie 'im ma ain brose, an' cuddle 'im, an' he couldna gang awa'. Greyfriars Bobby |Eleanor Atkinson 

Dear Love did cuddle it warm in her hands, and her husband did make the piece of egg into little divides for me to give to it. The Story of Opal |Opal Whiteley 

Maybe it was a little one that did have longings to cuddle in among the raindrops that do come together in the pond. The Story of Opal |Opal Whiteley