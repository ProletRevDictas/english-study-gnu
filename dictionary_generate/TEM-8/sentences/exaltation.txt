Rather than freeze a particular moment, Worrell seeks to capture the exaltation such an instant can spark. In the galleries: Building on an artwork expressed via different media |Mark Jenkins |April 23, 2021 |Washington Post 

Let’s call it a sigh of relief building to a cheer of exaltation. Inauguration Musical Performances Are Tricky. But Lady Gaga, Jennifer Lopez and Garth Brooks Did Exactly What We Needed Them to Do |Stephanie Zacharek |January 20, 2021 |Time 

Clearly a call to charity, this is also an exaltation of parenthood. Dealing with Awful Loss |Justin Green |February 25, 2013 |DAILY BEAST 

This exaltation of suffering may be difficult for many non-Catholics to understand. Contrasting Benedict and John Paul II |Christopher Dickey |February 11, 2013 |DAILY BEAST 

What made more sense than to sing a niggun, a tune of longing and exaltation? A Year On: Occupy L.A. And God |Aryeh Cohen |September 28, 2012 |DAILY BEAST 

Many people think of religion in personal terms, of the solace or insight or exaltation it can provide. The Real Reason for Christmas |Nicholas Wade |December 14, 2009 |DAILY BEAST 

One old woman, called Judy, came near having the power, as they call a kind of fit of spiritual exaltation. The Cromptons |Mary J. Holmes 

The sight of her aroused in me feelings which bore, I think, a close resemblance to religious exaltation. Marguerite |Anatole France 

She was now in Gemini, and therefore halfway from her exaltation to her depression. Chaucer's Works, Volume 1 (of 7) -- Romaunt of the Rose; Minor Poems |Geoffrey Chaucer 

But to Charity, in the reaction from her mood of passionate exaltation, there was something disquieting in his silence. Summer |Edith Wharton 

Yet these pains in her body, this alternate exaltation and depression, this pitiful weakness! When Valmond Came to Pontiac, Complete |Gilbert Parker