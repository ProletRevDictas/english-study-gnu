I’m happy I’m not a freshman right now because I think that my dorm experience is something I never would have wanted to give up. Even the most cautious schools are seeing outbreaks |Sy Mukherjee |September 17, 2020 |Fortune 

In fact, I was the kid who, if I got called up in front of the class, was not happy to publicly speak. Tom Colicchio Hopes (and Fears) COVID-19 Will Change the Restaurant Industry |Pallabi Munsi |September 16, 2020 |Ozy 

It’s a way to keep the ice-cream makers happy while saving the rainforest, and it can be scaled up now. The Environmental Headache in Your Shampoo - Issue 90: Something Green |Anastasia Bendebury & Michael Shilo DeLay |September 16, 2020 |Nautilus 

Zoom happy hours and small dinner parties are the pandemic’s new social scenes and cocktails should be part of this home experience. Bar Rescue: Pandemic Edition |jakemeth |September 15, 2020 |Fortune 

Rodríguez, who now lives in Arizona, in a message she sent to the Blade said she was very happy when Moreno called her and told her she had won her case. Lesbian woman from Cuba granted asylum in U.S. |Yariel Valdés González |September 15, 2020 |Washington Blade 

So, as far as Mexican officials like Peña Nieto are concerned, the goal is to keep their countrymen here — and keep them happy. Why Mexicans Are Enraged by Obama’s Big Tuesday Meeting |Ruben Navarrette Jr. |January 6, 2015 |DAILY BEAST 

How do you celebrate when happy occasions are colored by loss and absence? Everyone at This Dinner Party Has Lost Someone |Samantha Levine |January 6, 2015 |DAILY BEAST 

He seemed by all appearances perfectly happy to let the Republicans control the state senate. Mario Cuomo: An OK Governor, but a Far Better Person |Michael Tomasky |January 2, 2015 |DAILY BEAST 

The church was not happy with his views, and there was talk of excommunication. Mario Cuomo, a Frustrating Hero to Democrats, Is Dead at 82 |Eleanor Clift |January 2, 2015 |DAILY BEAST 

“We wish each and every one of you a happy and safe new year,” Giorgio said at the very end. Cop Families Boo De Blasio at NYPD Graduation |Michael Daly |December 30, 2014 |DAILY BEAST 

And that was that if he and his wife were to ever live together again and be happy, the family were to be kept out of it. The Homesteader |Oscar Micheaux 

Cousin George's position is such a happy one, that conversation is to him a thing superfluous. Physiology of The Opera |John H. Swaby (AKA "Scrici") 

Liszt gazed at "his Hans," as he calls him, with the fondest pride, and seemed perfectly happy over his arrival. Music-Study in Germany |Amy Fay 

M'Bongo and his whole court are now clothed, I am happy to say, at least to a certain extent. The Pit Town Coronet, Volume I (of 3) |Charles James Wills 

He was aware that his act by this time, had helped nobody, had made no one happy or satisfied—not even himself. The Homesteader |Oscar Micheaux