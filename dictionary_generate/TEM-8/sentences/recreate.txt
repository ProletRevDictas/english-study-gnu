Meanwhile, a growing selection of new nonalcoholic ingredients offer unprecedented freedom for those who want to be able to craft a complex, thoughtful drink or recreate the sensation of a classic cocktail sans the liquor. Keep dry January going all year with these cutting-edge non-alcoholic cocktails |By Dan Q. Dao/Saveur |February 2, 2021 |Popular-Science 

She found that those distances almost perfectly recreated the IAT’s results. An AI saw a cropped photo of AOC. It autocompleted her wearing a bikini. |Karen Hao |January 29, 2021 |MIT Technology Review 

Backcountry Safety Pledge It's as simple as knowing before you go, recreating responsibly, and caring for the places you explore. How to Safely Explore Colorado's Backcountry This Year |Outside Editors |January 28, 2021 |Outside Online 

The difficulty instead came from painstakingly recreating the exact positions of hallways and rooms. I recreated ‘Phasmophobia’ in ‘The Sims 4’ with the game’s new paranormal pack |Elise Favis |January 26, 2021 |Washington Post 

The researchers decided to try and recreate this capability by using similar high-level concepts learned by an AI to help it quickly learn previously unseen categories of images. How Mirroring the Architecture of the Human Brain Is Speeding Up AI Learning |Edd Gent |January 18, 2021 |Singularity Hub 

Ultimately, Gow says, the brothers failed in their efforts to recreate the Auroch. ‘Nazi Cows’ Tried to Kill British Farmer |Tom Sykes |January 6, 2015 |DAILY BEAST 

As she discussed her understanding of the voting rights campaign and how she planned to recreate it, I grew more relieved. Dr. King Goes to Hollywood: The Flawed History of ‘Selma’ |Gary May |January 2, 2015 |DAILY BEAST 

“I wanted to recreate the sense of all of these things happening simultaneously on all of these fronts,” Smith said. This Republican Loved Taxes & Modern Art |Scott Porch |November 19, 2014 |DAILY BEAST 

Game online if you want to live—or if you want to recreate scenes from your favorite movies. Viral Video of the Day: Terminator 2, Grand Theft Auto V Style |Alex Chancey |June 6, 2014 |DAILY BEAST 

And I really like the challenge of having to sustain and recreate the story each night. Toni Collette on ‘The Realistic Joneses,’‘Lucky Them,’ and Crying in ‘The Sixth Sense’ |Melissa Leon |June 1, 2014 |DAILY BEAST 

He had scarcely known what play was, and he did not know how to recreate himself. Captains of Industry |James Parton 

He could not travel abroad; he could not recreate his mind by pleasure. Captains of Industry |James Parton 

There is no particular need of our trying to recreate the picture of it as it was before the war began. Those Times And These |Irvin S. Cobb 

The only method he took to unbend and recreate himself, was to go from one work to another. The Life of the Truly Eminent and Learned Hugo Grotius |Jean Lvesque de Burigny 

Think of a book that can lift up our drooping spirits, and recreate us in Gods image! Pleasure &amp; Profit in Bible Study |Dwight Moody