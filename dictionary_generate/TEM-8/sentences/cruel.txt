That might sound a little cruel, but given his behavior, it’s his just rewards, But on the other hand, I think that it might work to his favor. From Wuhan Virus to Washington Virus |Daniel Malloy |October 4, 2020 |Ozy 

Beyond these two especially high-profile cases, this term gives the Court an opportunity to reshape the law governing cruel and unusual punishments — and to potentially weaken existing safeguards against such punishments considerably. 7 big cases the Supreme Court will hear in its new term, explained |Ian Millhiser |October 1, 2020 |Vox 

The Eighth Amendment forbids “cruel and unusual punishments,” and the Supreme Court has held that this amendment prohibits states from imposing certain punishments on certain individuals. 7 big cases the Supreme Court will hear in its new term, explained |Ian Millhiser |October 1, 2020 |Vox 

A clinician who had grown up in the town reached out to Anna Mueller for help breaking the cruel cycle. This year’s SN 10 scientists aim to solve some of science’s biggest challenges |Science News Staff |September 30, 2020 |Science News 

That's the cruel way someone anonymously replied to a family asking for help for their sick child. Family Told to Let Their Baby Die Instead of Raising Money For His Rare Disease |Health |November 27, 2019 |Health 

His later books drew heavily from experiences and people he encountered at the bar, including the cruel captain in The Sea-Wolf. The Bars That Made America Great |Nina Strochlic |December 28, 2014 |DAILY BEAST 

Not to those in power, not to the cruel and inhumane, not to the wealthy. Jesus Wasn’t Born Rich. Think About It. |Gene Robinson |December 25, 2014 |DAILY BEAST 

Hatuey replied that he would rather burn and be sent to hell than ever again encounter people as cruel as the Spanish. The Life and Hard Times Of The Family A Cuban Defector Left Behind |Brin-Jonathan Butler |December 19, 2014 |DAILY BEAST 

The New Jersey governor vetoed a ban on a rarely used cruel practice for pregnant pigs. Christie Bows to Iowa’s Pork Kings on Gestation Crates |Olivia Nuzzi |November 29, 2014 |DAILY BEAST 

The story of Alstory Simon has all the scope and scale, the cruel reversals, and pointless waste of proper tragedy. Wrongly Imprisoned for 15 Years Thanks to an Innocence Project |Jacob Siegel |November 13, 2014 |DAILY BEAST 

And then Jolly Robin would feel ashamed that he had even thought of being so cruel to an infant bird, even if he was a Cowbird. The Tale of Grandfather Mole |Arthur Scott Bailey 

Through what ages has that declaration, not to be denied, ascended to cold and cruel skies? Solomon and Solomonic Literature |Moncure Daniel Conway 

His silence had frightened her: what if he should resent on her the cruel words spoken by Dr. Ashton? Elster's Folly |Mrs. Henry Wood 

There had been cruel misunderstanding on his part somewhere; that misunderstanding must be burned away. The Wave |Algernon Blackwood 

In a thousand trials the cruel witness of Moses has sent innocent women to a painful death. God and my Neighbour |Robert Blatchford