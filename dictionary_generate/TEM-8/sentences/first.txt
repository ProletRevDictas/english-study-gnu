Fluoride first entered an American water supply through a rather inelegant technocratic scheme. Anti-Fluoriders Are The OG Anti-Vaxxers |Michael Schulson |July 27, 2016 |DAILY BEAST 

In the first episode, an officer is shown video of himself shooting and killing a man. 'Babylon' Review: The Dumb Lives of Trigger-Happy Cops |Melissa Leon |January 9, 2015 |DAILY BEAST 

But since those rosy scenarios were first floated, the California political scene has grown more crowded. The Golden State Preps for the ‘Red Wedding’ of Senate Races |David Freedlander |January 9, 2015 |DAILY BEAST 

Eric Garcetti succeeded Villaraigosa and has received high marks in his first year and a half on the job. The Golden State Preps for the ‘Red Wedding’ of Senate Races |David Freedlander |January 9, 2015 |DAILY BEAST 

He sees himself as the first Muslim president of all Europe. Houellebecq’s Incendiary Novel Imagines France With a Muslim President |Pierre Assouline |January 9, 2015 |DAILY BEAST 

This is the first and principal point at which we can stanch the wastage of teaching energy that now goes on. The Salvaging Of Civilisation |H. G. (Herbert George) Wells 

He was converted and baptized, and was the first Hebrew instructor at Harvard college. The Every Day Book of History and Chronology |Joel Munsell 

And I have not had the first morsel of food prepared from this grain offered me since I reached the shores of Europe. Glances at Europe |Horace Greeley 

Now first we shall want our pupil to understand, speak, read and write the mother tongue well. The Salvaging Of Civilisation |H. G. (Herbert George) Wells 

In treble, second and fourth, the first change is a dodge behind; and the second time the treble leads, there's a double Bob. Tintinnalogia, or, the Art of Ringing |Richard Duckworth and Fabian Stedman