Over the past couple years, the Zimmerman family has added the bungalows in the back and now a luxury high-rise. Houston for Tourists? If You Think That’s a Joke, the Joke’s on You |William O’Connor |July 30, 2021 |The Daily Beast 

Outsite is now experimenting with private apartments or bungalows in locations like Costa Rica where members get access to community activities and the co-working space. Work-life boundaries continue to blur: How the pandemic has changed co-living |Jessica Davies |July 20, 2021 |Digiday 

This pet-friendly bungalow was extensively renovated after sustaining damage from Hurricane Michael in 2018. Beachfront Airbnbs for Under $300 a Night |Megan Michelson |June 15, 2021 |Outside Online 

Up Chicago Avenue sits a pristine bungalow with a new “for sale” sign. The Intersection Where George Floyd Died Has Become a Strange, Sacred Place. Will Its Legacy Endure? |Janell Ross/Minneapolis |April 16, 2021 |Time 

Real estate here is small cottage and bungalow types that work perfectly for the LGBTQ community. Berkeley: A City That Fights for the Rights of All |LGBTQ-Editor |November 20, 2020 |No Straight News 

This bungalow has two levels, a screening room, a dining room, many offices, an art department, and cutting rooms. Alfred Hitchcock’s Fade to Black: The Great Director’s Final Days |David Freeman |December 13, 2014 |DAILY BEAST 

I'm to be at his Universal bungalow at twelve-thirty for lunch, to meet him for the first time, going to see a man about a job. Alfred Hitchcock’s Fade to Black: The Great Director’s Final Days |David Freeman |December 13, 2014 |DAILY BEAST 

I arrive at the bungalow and find his staff standing about stunned, some of them in tears. Alfred Hitchcock’s Fade to Black: The Great Director’s Final Days |David Freeman |December 13, 2014 |DAILY BEAST 

He'd kept the few offices at the front of the bungalow, now oddly barren. Alfred Hitchcock’s Fade to Black: The Great Director’s Final Days |David Freeman |December 13, 2014 |DAILY BEAST 

It's a studio bungalow the way Newport summer houses are “cottages.” Alfred Hitchcock’s Fade to Black: The Great Director’s Final Days |David Freeman |December 13, 2014 |DAILY BEAST 

Passing a bungalow that was blazing furiously, he saw in the compound the corpses of two women. The Red Year |Louis Tracy 

One evening in the month of April, a slim, straight-backed girl stood in the veranda of a bungalow at Meerut. The Red Year |Louis Tracy 

For he saw her looking up into his eyes as once before on the lawn of her English bungalow four months ago. The Wave |Algernon Blackwood 

The body of a young woman was found in the compound outside my bungalow, done to death in precisely the same way. Uncanny Tales |Various 

I found here the handsomest bungalow I had seen during the whole journey from Benares to Bombay. A Woman's Journey Round the World |Ida Pfeiffer