For the most part, these mountain lions are staying elusive and sticking to eating their natural prey. Four wild animals that are thriving in cities |By Ryan Chelius/Outdoor Life |February 9, 2021 |Popular-Science 

If seed rounds are becoming more elusive, maybe skip on that last hire, extend the runway, and try to gain some revenues. PSA: Most aggregate VC trend data is garbage |Danny Crichton |February 9, 2021 |TechCrunch 

This allows us to dramatically increase our digital inventory and efficiently offer this incredibly elusive audience to a whole new set of advertisers. Splitting the atom: Decoupling audience from inventory unleashes power of pubs |Trevor Grigoruk |February 9, 2021 |Digiday 

They must make assumptions not only about the virus’s biology, which remains far from fully understood, but about human behavior, which can be even more slippery and elusive. The Hard Lessons of Modeling the Coronavirus Pandemic |Jordana Cepelewicz |January 28, 2021 |Quanta Magazine 

However, establishing a causal relation between approaches to phonics instruction and gains in real reading has been more elusive. Is there really a ‘science of reading’ that tells us exactly how to teach kids to read? |Valerie Strauss |January 26, 2021 |Washington Post 

Huckabee will also need to establish a reliable fundraising base, something that up until now has proved to be elusive. Can Huckabee Convert the GOP’s Moneymen? |Lloyd Green |January 4, 2015 |DAILY BEAST 

As far as finally being acknowledged herself with that elusive Academy gold, well, Moore says she would not take it for granted. Julianne Moore Is Oscar Gold in ‘Still Alice’ |Marlow Stern |December 24, 2014 |DAILY BEAST 

There will be a lot of talk about “sustainable” development, but so far that has been elusive. Goodbye, Bahamas. Hello, Havana! |Clive Irving |December 18, 2014 |DAILY BEAST 

But the deliverance and liberation that has been longed for by Jews and Christians has proven to be an elusive thing. During Advent, Lots of Waiting, But Not Enough Hope |Gene Robinson |December 7, 2014 |DAILY BEAST 

They are an elusive bunch, in motion or in the thrall of another time. The Real-Life Raiders of the Lost Ark |Alex Belth |November 14, 2014 |DAILY BEAST 

That suddenly altered tone had awakened an elusive memory, but neither of them could succeed in identifying it. Dope |Sax Rohmer 

Its elusive light lay upon the slope, but ledge and stone seemed less distinct than their shadows, which were black as ebony. The Gold Trail |Harold Bindloss 

A faintly embarrassing situation this, even for an ancestor of the elusive Pimpernel. Punch, or the London Charivari, Vol. 147, November 4, 1914 |Various 

The look in her face was quizzical, yet there was a strange, elusive gravity in her eyes, an almost pathetic appealing. You Never Know Your Luck, Complete |Gilbert Parker 

Yet the real difficulty is still before an author: it is to decide what stamp to put upon such elusive matter as ideas. English: Composition and Literature |W. F. (William Franklin) Webster