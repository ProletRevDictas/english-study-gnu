In addition to the heat, shifts that should have had five or six employees had two or three, and one of Flores’s former supervisors was “very argumentative,” Flores told the “Today” show. Burger King workers announce resignation with a sign outside restaurant: ‘We all quit’ |Marisa Iati |July 13, 2021 |Washington Post 

This argumentative stance became a hallmark of his public persona. Prince Philip, royal consort to Queen Elizabeth II, dies at 99 |Adrian Higgins |April 9, 2021 |Washington Post 

The passengers were “non-mask compliant, rowdy, argumentative” and harassed crew members, the airline said. FAA warns of jail time, fines as airports and airlines prep for unruly passengers ahead of the inauguration |Ian Duncan, Lori Aratani |January 11, 2021 |Washington Post 

A “why” question, always dangerous for various reasons, is particularly objectionable when argumentative. Pistorius’s Cross-Examination Could Have Been Grounds for a Mistrial in a U.S. Court |James D. Zirin |May 5, 2014 |DAILY BEAST 

“Debate is competitive argumentation so debaters tend to be competitive and argumentative,” Lubetsky said. Ted Cruz at Princeton: Creepy, Sometimes Well Liked, and Exactly the Same |Patricia Murphy |August 19, 2013 |DAILY BEAST 

This deeply engaged, argumentative monologue is an exercise in reaching, again and again beyond the limits of unbelieving. This Week’s Hot Reads: April 1, 2013 |Mythili Rao, Jimmy So |April 1, 2013 |DAILY BEAST 

And being too argumentative can cost you part of your social circle. Unfriending Over Politics |Howard Kurtz |March 12, 2012 |DAILY BEAST 

Some have speculated the argumentative Hampton could be a new addition to the Atlanta Housewives cast. Phaedra Parks: The Real Housewives of Atlanta’s Scene Stealer |Geoff Berkshire |March 6, 2012 |DAILY BEAST 

Mr. Shiel followed, in a speech which was more personal than argumentative. The History of England in Three Volumes, Vol.III. |E. Farr and E. H. Nolan 

The argumentative force of the passage being admitted, its doctrinal import deserves attention. Expositor's Bible: The Second Epistle to the Corinthians |James Denney 

He simply took his model and abridged it, by throwing out all argumentative, illustrative and amplificatory material. A History of Mediaeval Jewish Philosophy |Isaac Husik 

There was something that caught his attention in the note of this—a longing half hopeless, half argumentative to be believed in. The Tragic Muse |Henry James 

Besides, it was not to be denied that even the elderly and argumentative found themselves listening to his discourses. In Connection with the De Willoughby Claim |Frances Hodgson Burnett