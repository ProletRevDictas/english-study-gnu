Favorite books can be displayed vertically stacked in between, which makes this a great and thoughtful choice for avid readers. Wine bottle holders and racks that make sophisticated gifts |PopSci Commerce Team |September 29, 2020 |Popular-Science 

Previously, Johnson, an avid rock climber, had been vice president of technical outdoor for the company, leading groups that built products for everything from trail running to snowboarding. Patagonia Just Announced a New CEO |Christopher Solomon |September 23, 2020 |Outside Online 

Gellert, 48, is an avid climber and backcountry snowboarder who has ridden all over the world. Patagonia Just Announced a New CEO |Christopher Solomon |September 23, 2020 |Outside Online 

Political scientist Salma Mousa of Stanford University, an avid soccer fan who grew up in the Middle East, wondered if the popular sport could bring those communities together. Interfaith soccer teams eased Muslim-Christian tensions — to a point |Sujata Gupta |August 13, 2020 |Science News 

The Sunday in late April that the avid runner became ill started like any other and included a seven-mile run. Why COVID-19 is both startlingly unique and painfully familiar |Aimee Cunningham |July 2, 2020 |Science News 

He grew up both a computer geek in the early days of video games and an avid record collector. DJ Spooky Wants You To Question Everything You Know About Music, Technology, and Philosophy |Oliver Jones |December 27, 2014 |DAILY BEAST 

Now, just as avid an art collector, Jay Z spoke about the meeting of cultural worlds (and rapped about them) in “Picasso Baby”. William, Kate, and Jay Z’s Favorite Art Star: Alexander Gilkes' World of Rock Stars and Royalty |Tim Teeman |December 10, 2014 |DAILY BEAST 

All this time, even back when he was studying at Purdue, Pragnell was an avid home-brewer. House of the Witch: The Renegade Craft Brewers of Panama |Jeff Campagna |November 30, 2014 |DAILY BEAST 

Describing by biographers as an avid writer of letters, little of his correspondence appears available to public view. The True Story of ‘The Elephant Man’ |Russell Saunders |November 3, 2014 |DAILY BEAST 

Fryberg was an avid hunter, according to his Facebook posts. The Homecoming Prince Who Tweeted His Killing Spree |Brandy Zadrozny |October 24, 2014 |DAILY BEAST 

The shout of approval showed how avid they were for some direct expression of their accumulated resentment. Mountain |Clement Wood 

The uneventful lives they led year after year made men and women alike avid for anything of the nature of news or incident. In Connection with the De Willoughby Claim |Frances Hodgson Burnett 

The funny thing is that Wagner never renounced anything: to the end he was greedy, avid of life. Richard Wagner |John F. Runciman 

What a break-off, leaving the gasping reader in a state of choking suspense, of avid, ungratified curiosity! Ptomaine Street |Carolyn Wells 

There was a meagre, passionless dulness about the aspect, though at times it quickened into a kind of avid acuteness. Lucretia, Complete |Edward Bulwer-Lytton