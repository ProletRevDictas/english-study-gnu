His mother took him and his two siblings to live with their uncle, John Mercer, a wealthy lawyer who owned one of the largest libraries in Virginia. Thank This Forgotten Founding Father for Your ‘Pursuit of Happiness’ |Nick Fouriezos |September 27, 2020 |Ozy 

Moreno publicly criticized Mariela Castro after she evoked her to defend the 1959 Cuban revolution that brought her uncle, Fidel Castro, to power. Cuban authorities threaten to arrest LGBTQ activist, journalist |Michael K. Lavers |September 17, 2020 |Washington Blade 

According to Thurmond’s testimony, Ellis and Montgomery’s uncle, James Timothy Norman, met at a club where she danced and were previously intimate. ‘Sweetie Pie’s’ Murder-For-Hire Possibly Caused After $200K Stolen From Robbie Montgomery’s Home |Hope Wright |September 11, 2020 |Essence.com 

It was her uncle, Alfredo, who sold me our place nearly 20 years ago. Buffett’s big bet on Japan sends global stocks higher |Bernhard Warner |August 31, 2020 |Fortune 

The ruse was up when the family moved back to Nigeria, and Oyelowo saw that many of his uncles had the same marks. David Oyelowo’s History of Dangerous Love |Eromo Egbejule |August 11, 2020 |Ozy 

Same with my uncle and cousins when their planes landed from Vietnam. A Veteran’s View: NYC Cold War Between Cops and City Hall |Matt Gallagher |December 29, 2014 |DAILY BEAST 

The young Jordanian pilot comes from a well-known military family in the kingdom and his uncle is a retired major general. Did ISIS Shoot Down a Fighter Jet? |Jamie Dettmer, Christopher Dickey |December 24, 2014 |DAILY BEAST 

It reminds me of an uncle of mine who said the London Blitz was irritating. Why Can’t Movies Capture Genius? |Clive Irving |December 14, 2014 |DAILY BEAST 

The uncle told RTL radio Hauchard called his grandmother, ostensibly from Syria, on Nov. 2, for her birthday. Showing the Faces of Its Murderers, ISIS Shows Its Global Reach |Tracy McNicoll |November 18, 2014 |DAILY BEAST 

Kim Jong Un, who assumed power on the death of his father, had given uncle Jang nearly free rein to handle relations with Beijing. Why North Korea Released Two Americans |Gordon G. Chang |November 9, 2014 |DAILY BEAST 

The strains of the syren at last woke her uncle, and brought back Miss Hood, who suggested that it was late. The Pit Town Coronet, Volume I (of 3) |Charles James Wills 

“It means, my dear, that the Dragoons and the 60th will have to teach these impudent rebels a much-needed lesson,” said her uncle. The Red Year |Louis Tracy 

He could not bear to open his dreadful situation to his Uncle David, nor to kill himself, nor to defy the vengeance of Longcluse. Checkmate |Joseph Sheridan Le Fanu 

Out gets Uncle David, looking brown and healthy after his northern excursion. Checkmate |Joseph Sheridan Le Fanu 

Uncle David nodded, and waved his hand, as on entering the door he gave them a farewell smile over his shoulder. Checkmate |Joseph Sheridan Le Fanu