At the same time, though, he argued that businesses shouldn’t be the only ones subject to enforcement because business owners could build fines into their overall cost of operation. Despite Crackdown Announcement, Not Much COVID-19 Enforcement Is Happening |Jesse Marx |February 11, 2021 |Voice of San Diego 

How to make a martini your guests will love — or at least love to argue aboutChocolate Lava Cakes for Two. This Valentine’s Day, we couldn’t help but wonder, which ‘Sex and the City’ character will you most eat like? |Kari Sonde |February 11, 2021 |Washington Post 

While officials argued that Mori’s presence at the helm of the organizing committee was needed to ensure the Games went ahead, it became apparent that his continued presence risked sinking the ship. Japan Olympics chief who said women talk too much will resign over remarks, reports say |Simon Denyer, Julia Mio Inuma |February 11, 2021 |Washington Post 

Officials argue some of the payments were not yet due, some were tied up in appeals and some were in the process of being gathered. Utility Companies Owe Millions to This State Regulatory Agency. The Problem? The Agency Can’t Track What It’s Owed. |by Scott Morris, Bay City News Foundation |February 10, 2021 |ProPublica 

Many of these telecom giants argue through their primary lobbying arm, the trade group USTelecom, that Congress should finance phone and broadband benefits for low-income Americans on its own. Lacking a Lifeline: How a federal effort to help low-income Americans pay their phone bills failed amid the pandemic |Tony Romm |February 9, 2021 |Washington Post 

In the book, Tavris and Aronson argue that the same ability to overlook minor flaws in a marriage leads to overlooking major ones. Why Didn’t Camille Dump Bill Cosby? |Amanda Marcotte |December 17, 2014 |DAILY BEAST 

Mailer would argue, for example, that timidity does more harm to the novelist than donning a mask of extreme self-confidence. Mailer’s Letters Pack a Punch and a Surprising Degree of Sweetness |Ronald K. Fried |December 14, 2014 |DAILY BEAST 

One could argue that this was never exactly hidden from her readers. Meet Zoella—The Newbie Author Whose Book Sales Topped J.K. Rowling |Lucy Scholes |December 11, 2014 |DAILY BEAST 

“Women go to the bathroom together and gossip, talk and argue all the time,” Vithi Cuc told The National. Middle East Murder Mystery: Who Killed an American Teacher in Abu Dhabi? |Chris Allbritton |December 3, 2014 |DAILY BEAST 

Starting with the idea of androgyny, you argue that there is a woman in every man, and vice versa. Living Black & Gay in the ’50s |Alain Mabanckou |December 3, 2014 |DAILY BEAST 

"But I can't stop to argue about it now;" and, saying this, he turned into a side path, and disappeared in the wood. Davy and The Goblin |Charles E. Carryl 

This seemed entirely unnecessary to mine host, and he wanted to argue the point. The Soldier of the Valley |Nelson Lloyd 

When people argue in this strain, I immediately assume the offensive. Tobacco; Its History, Varieties, Culture, Manufacture and Commerce |E. R. Billings. 

But she knew it was useless to argue with Henry, so she hastily groped in the bag for the matches and handed them to her brother. The Box-Car Children |Gertrude Chandler Warner 

It would argue too much literary conceit on my part were I anxious to restore it to the light of day. Marguerite |Anatole France