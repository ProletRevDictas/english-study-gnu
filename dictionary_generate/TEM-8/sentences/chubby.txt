His friends have previously said that Andrew has “chubby fingers” that do not match those in the photo. Defiant Prince Andrew to Stop Hiding and Fight Virginia Giuffre’s Claims |Tom Sykes |September 22, 2021 |The Daily Beast 

A short, chubby bush with many branches—a “shrubbery,” to get Pythonesque. Humans Are One Mixed-Up Ape - Facts So Romantic |Phil Jaekl |July 9, 2021 |Nautilus 

If the conditions are right, they’re just skinny enough to make carved turns on groomed, packed snow fun for the entire day, but they’re still chubby enough to float in all but the biggest in-bounds powder days. Long-Term Review: The Best Skis, Period |Heather Schultz and Marc Peruzzi |March 4, 2021 |Outside Online 

Through photos and a live camera located in Brooks Falls, where these bears hunt for salmon, people can vote for who they think is the chubbiest of them all. How scientists try to weigh some of the fattest bears on Earth |María Paula Rubiano A. |October 2, 2020 |Popular-Science 

“It often results in them having these really chubby arms,” Keefe says. These buff frogs never skip arm day |Sara Kiley Watson |September 25, 2020 |Popular-Science 

The scandal drove the company from the street business, but Chubby acquired a truck of his own. How Brooklyn’s First Ice Cream Girl Fought City Hall–and Won |Michael Daly |October 13, 2014 |DAILY BEAST 

As Maria set out to begin another day in the ice cream truck, Chubby would roll alongside her for part of the route. How Brooklyn’s First Ice Cream Girl Fought City Hall–and Won |Michael Daly |October 13, 2014 |DAILY BEAST 

The speakers included a man who had been struck by a hit-and-run driver who had then been chased down by Chubby. How Brooklyn’s First Ice Cream Girl Fought City Hall–and Won |Michael Daly |October 13, 2014 |DAILY BEAST 

At the same time, Chubby experienced growing heart troubles, finally becoming too disabled to work in 2004. How Brooklyn’s First Ice Cream Girl Fought City Hall–and Won |Michael Daly |October 13, 2014 |DAILY BEAST 

But in 2006, as he was turning 80, Chubby acquired a motorized wheelchair. How Brooklyn’s First Ice Cream Girl Fought City Hall–and Won |Michael Daly |October 13, 2014 |DAILY BEAST 

Here Letty drew the coverlet from the face of the sleeping babe, and displayed his chubby proportions with maternal pride. The World Before Them |Susanna Moodie 

Mr. Peck raised his hands, palms upward, and then dropped them to his chubby knees with a sharp slap. Hooded Detective, Volume III No. 2, January, 1942 |Various 

I found her sleeping sweetly, with one chubby hand under her rounded cheek. The Mayor's Wife |Anna Katherine Green 

Sydney is here, and is getting fat and chubby, a delightful little boy, and keeps us all very cheery. Letters of Lt.-Col. George Brenton Laurie |George Brenton Laurie 

Fina clapped her chubby hands together and climbed up into Leam's lap. Lippincott's Magazine of Popular Literature and Science |Various