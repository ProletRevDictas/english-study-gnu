The national creation narrative is obliged to contend with a host of destabilizing forces. Four presidents who put Virginia’s stamp on early America |Andrew Burstein |October 30, 2020 |Washington Post 

The third contribution of human rights is that they oblige governments to develop a holistic, integrated response to the pandemic. Why Human Rights Should Guide Responses To The Global Pandemic |LGBTQ-Editor |October 7, 2020 |No Straight News 

He obliged, naming specific dates on which he said he had abused Stewart, all while under the influence of alcohol and cocaine. Her Stepfather Admitted to Sexually Abusing Her. That Wasn’t Enough to Keep Her Safe. |by Nadia Sussman |September 18, 2020 |ProPublica 

There is also the “blank check” syndrome whereby the host city is legally obliged to cover cost overruns, while the IOC takes on no such liability. Want to Host the Olympics? Plan to Go Over Budget |Fiona Zublin |September 14, 2020 |Ozy 

He obliged the vacation request but was appalled to later find a worker’s compensation form in his file filled out by Brady citing stress and burnout, the investigation says. Accusations Flew, Then National School District Official Got Paid to Resign |Ashly McGlone |July 20, 2020 |Voice of San Diego 

The would-be pope killer loves to be in front of the cameras, and the press in Italy is happy to oblige. Pope-Shooter Ali Agca’s Very Weird Vatican Visit |Barbie Latza Nadeau |December 29, 2014 |DAILY BEAST 

The zoo is blessed with multiple wallabies and was happy to oblige. Ebola's Roots Are 50 Times Older Than Mankind. And That Could Be the Key to Stopping It. |Michael Daly |October 20, 2014 |DAILY BEAST 

To know the Egyptian military is to realize it will not oblige. Leslie H. Gelb on the Democracy-Elections Trap in Egypt |Leslie H. Gelb |July 22, 2013 |DAILY BEAST 

I know some magazines contractually oblige their staff writers to produce six features a year, for example. Lawrence Wright: How I Write |Noah Charney |May 22, 2013 |DAILY BEAST 

If Republicans wanted to talk books, Elleithee said, the McAuliffe campaign should be happy to oblige. In Virginia, Terry McAuliffe’s Memoir Comes Back to Haunt Him |David Freedlander |May 7, 2013 |DAILY BEAST 

And was it not possible that the E. of N. might oblige his old Friends in the same manner? An Account of the Growth of Deism in England |William Stephens 

Jonas: La baleine fut la fin oblige de le vomir tant un Prophte est un morceau difficile digrer. Baron d'Holbach |Max Pearson Cushing 

And to increase the horrid scene, they would oblige the husband to be a spectator before suffered himself. Fox's Book of Martyrs |John Foxe 

Señor Rhodes will be pleased to unfasten those heavy chains to oblige the lady. The Treasure Trail |Marah Ellis Ryan 

Few were the dances in which I did not take a part, sinking so low as occasionally to oblige with a hornpipe. In the Wrong Paradise |Andrew Lang