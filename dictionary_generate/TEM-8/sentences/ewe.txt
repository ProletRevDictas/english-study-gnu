As for a more humane socialism emerging, Aksyonov kebabs that ewe-lamb too. This 1979 Novel Predicted Putin’s Invasion Of Crimea |Michael Weiss |May 18, 2014 |DAILY BEAST 

He brought not just Ewe to English, but a tribal embrace to the angst-ridden homeland of Billy Joel. Kofi Awoonor, the Ghanaian Poet Killed in Westgate Mall Attack |Michael Daly |September 24, 2013 |DAILY BEAST 

Her name, The New York Times noted, means “the human being is more precious than gold” in Ewe. Kofi Awoonor, the Ghanaian Poet Killed in Westgate Mall Attack |Michael Daly |September 24, 2013 |DAILY BEAST 

A vigorous ewe may bring her first lamb at two years old, but it is better that it be deferred till three. Domestic Animals |Richard L. Allen 

The ewe should possess these characteristics generally, with such modifications as are suited to the sex. Domestic Animals |Richard L. Allen 

The old ewe had come quite close to the man, and one of the lambs was nibbling at his trousers' leg. The Shepherd of the Hills |Harold Bell Wright 

They made their own negotiations, drew their savings from the bank and started into business with four ewe lambs. The Red Cow and Her Friends |Peter McArthur 

Here comes the wonderful one-hoss shay, Drawn by a rat-tailed, ewe-necked bay, "Huddup!" The Book of Humorous Verse |Various