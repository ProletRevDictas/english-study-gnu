Accordingly, Stepien has reportedly been working to tighten the campaign’s belt — which has included cutting back on TV ads in several key states. Trump Has Lost His Edge In TV Advertising |Nathaniel Rakich (nathaniel.rakich@fivethirtyeight.com) |September 16, 2020 |FiveThirtyEight 

Take its Intelligent Tracking Prevention feature in Safari, which it has continued to tighten. ‘It’s the first time they’re listening’: Apple is striking a more conciliatory tone with the ad industry |Lara O'Reilly |September 8, 2020 |Digiday 

Even as revenue streams tightened, this year’s Resilience Awards finalists refused to panic — or to think selfishly. Shopify, Chewy and Ally are Resilience Awards finalists |Digiday Awards |September 1, 2020 |Digiday 

Health care is also a highly regulated business, and restrictions on the use of telehealth, some of which were relaxed during the pandemic, could be tightened. Buy or pass? The pros and cons of investing in 9 upcoming tech IPOs from Palantir to Asana |Aaron Pressman |August 29, 2020 |Fortune 

She thinks having a fingerprint verification might tighten security of the ballots even more. As states mull expanding vote by mail, they’re turning to Oregon for advice |Lee Clifford |August 24, 2020 |Fortune 

The grasp on the sabre would tighten; the quiet eyes would flash. Stonewall Jackson, VMI’s Most Embattled Professor |S. C. Gwynne |November 29, 2014 |DAILY BEAST 

I tried to relax too, but I felt my stomach tighten and I began to sweat. I Shot Bin Laden |Elliot Ackerman |November 16, 2014 |DAILY BEAST 

Some media reports say Turkey has begun recently to tighten some controls in the border region. Is NATO Ally Turkey Tacitly Fueling the ISIS War Machine? |Thomas Seibert |September 8, 2014 |DAILY BEAST 

Democrats want to tighten disclosure laws (as with the proposed DISCLOSE Act) but have blocked raising contribution limits. The Answer to the McCutcheon Decision Is More Big Money in Politics |Jonathan Rauch |April 3, 2014 |DAILY BEAST 

Congress passed the Helms–Burton Act to tighten the sanctions regime and write it into the law. Obama Should End America’s Stupidest Foreign Policy: Isolating Cuba |Robert Shrum |February 26, 2014 |DAILY BEAST 

In that instant I felt fingers tighten on my arm, tighten till they bit into the flesh, and I was pulled back into safety. Uncanny Tales |Various 

I felt her grasp tighten about my neck, and her firm body crowd against me as we both sank down for an instant. The Way of a Man |Emerson Hough 

But the touch only seemed to make the great living knot tighten, and after a try Morgan ceased. Mass' George |George Manville Fenn 

The cap should have a square nut for a wrench to tighten or unscrew the cap. Elements of Plumbing |Samuel Dibble 

The doctor, peering after him with smoldering eyes, felt his finger tighten on the trigger. Cursed |George Allan England