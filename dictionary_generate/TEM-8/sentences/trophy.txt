As for Tampa Bay, the Bolts are in danger of coughing away yet another golden opportunity to add to their trophy case. The Dallas Stars Are Putting Their Regular-Season Shortcomings Behind Them |Terrence Doyle |September 21, 2020 |FiveThirtyEight 

“Succession” star Jeremy Strong won the drama actor trophy for his role as an aspiring heir. ‘Succession’ wins best drama, while ‘Schitt’s Creek’ sweeps the Emmy Awards |radmarya |September 21, 2020 |Fortune 

Khadija Siddique come around to her son’s choice of careers, as Ash now makes enough money to support his family, which, he says, is a far bigger award than any trophy. An Unlikely Esports Star Emerges From Pakistan |Daniel Malloy |August 21, 2020 |Ozy 

So when domestic rival Liverpool was eliminated in early March, it appeared as though the only thing that stood between City and its first ever Champions League trophy was Bayern. After Manchester City’s Big Loss, Who’s The Favorite To Win The Champions League? |Terrence Doyle |August 18, 2020 |FiveThirtyEight 

The Bavarian club is now the heavy favorite to lift the Champions League trophy, and for good reason. After Manchester City’s Big Loss, Who’s The Favorite To Win The Champions League? |Terrence Doyle |August 18, 2020 |FiveThirtyEight 

In straight relationships with an age gap, words like ‘gold-digger’ and ‘trophy wife’ get thrown around. Freaking Out About Age Gaps in Gay Relationships Is Homophobic |Samantha Allen |January 9, 2015 |DAILY BEAST 

Get a thrill, get off a lucky shot, take home a trophy, put it up in a secret chamber of our heart. McConaughey’s ‘Stand’—And Ours |James Poulos |December 5, 2014 |DAILY BEAST 

“Management” in this situation means issuing permits for trophy grizzly hunts. What It Takes to Kill a Grizzly Bear |Doug Peacock |November 23, 2014 |DAILY BEAST 

The charity gets its name from the symbolic date when the America's Cup trophy left the U.K. The Disappearance of Kate Middleton |Tom Sykes |October 15, 2014 |DAILY BEAST 

Its horrific title aside, Trophy Wife was the closest thing in style, tone, and humor to Modern Family that ABC had ever produced. ‘black-ish’ Is the New ‘Modern Family’ |Kevin Fallon |October 1, 2014 |DAILY BEAST 

Sometimes Yung Pak would be the winner, and then he would march home with great glee and show the trophy to his father. Our Little Korean Cousin |H. Lee M. Pike 

Some of the soldiers insisted on skinning the beast and taking the skin along as a trophy. The Courier of the Ozarks |Byron A. Dunn 

The dæmon of anarchy has here raised a superb trophy on a monument of ruins. Travels through the South of France and the Interior of Provinces of Provence and Languedoc in the Years 1807 and 1808 |Lt-Col. Pinkney 

Then she started back for the caves taking the slat of wood with her as a trophy. The Beach of Dreams |H. De Vere Stacpoole 

I presented my trophy and treasure-trove to the fairy-like Miss Wee-wee. Baboo Jabberjee, B.A. |F. Anstey