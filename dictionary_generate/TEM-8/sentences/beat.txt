With some limitations on the usual nonconference games that help poll voters gauge teams, Gonzaga managed to get wins over ranked teams Virginia, West Virginia and Iowa, plus Kansas and Auburn, while Baylor beat No. College basketball’s kingdoms have gone haywire — in case you just started paying attention |Chuck Culpepper |February 12, 2021 |Washington Post 

Many of the competitions were spectacles, drawing large crowds with elaborate lights and window-rattling beats, but the sport was driven by independent event promoters without any movement trained on the Olympics. How break dancing made the leap from ’80s pop culture to the Olympic stage |Rick Maese |February 9, 2021 |Washington Post 

They played a heck of a game defensively and offensively to beat us. That really was one of the least enjoyable Super Bowls of all time |Neil Greenberg |February 9, 2021 |Washington Post 

For freshwater paddling, it’s hard to beat the scenery at the mile-long Jordan Pond. The Ultimate Acadia National Park Travel Guide |Virginia M. Wright |February 8, 2021 |Outside Online 

“It’s the worse I’ve been beaten in a long time,” Mahomes said. Super Bowl highlights: Bucs celebrate championship, Tom Brady wins MVP |Des Bieler, Mark Maske, Chuck Culpepper |February 8, 2021 |Washington Post 

He beat his illness twice, wrote about his battles with the disease, and continued broadcasting even as his health was failing. Remembering ESPN’s Sly, Cocky, and Cool Anchor Stuart Scott |Stereo Williams |January 4, 2015 |DAILY BEAST 

It went into remission, but it would resurface in 2011; and Scott was able to beat it once again. Remembering ESPN’s Sly, Cocky, and Cool Anchor Stuart Scott |Stereo Williams |January 4, 2015 |DAILY BEAST 

But underground classes have Persians getting with the beat. Iran’s Becoming a Footloose Nation as Dance Lessons Spread |IranWire |January 2, 2015 |DAILY BEAST 

It may have been the reason why Goldwater beat Rockefeller by three points, and effectively sewed up the GOP nomination. The World’s Toughest Political Quiz |Jeff Greenfield |December 31, 2014 |DAILY BEAST 

Despite the scandal, Grimm beat his Democratic opponent by 18 points in November. 2014 Was a Delectably Good Year for Sleaze |Patricia Murphy |December 30, 2014 |DAILY BEAST 

Even if poverty were gone, the flail could still beat hard enough upon the grain and chaff of humanity. The Unsolved Riddle of Social Justice |Stephen Leacock 

His face flushed with annoyance, and taking off his soft hat he began to beat it impatiently against his leg as he walked. The Awakening and Selected Short Stories |Kate Chopin 

The pulse in Louis's temples beat hard; yet he was determined not to anticipate, but make Wharton explain himself. The Pastor's Fire-side Vol. 3 of 4 |Jane Porter 

To be sure, he hadn't seen Mrs. Robin go, but he had heard the beat of her wings as she began her flight. The Tale of Grandfather Mole |Arthur Scott Bailey 

We should easily beat this in America with anything like equal facilities, and without charging the British price—£4 7s. Glances at Europe |Horace Greeley