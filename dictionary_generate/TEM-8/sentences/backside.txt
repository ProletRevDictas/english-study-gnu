On the backside of a marvelous career, however, Özil is not expecting to make that kind of money anymore, no matter where he is employed. Mesut Özil playing for D.C. United seems unlikely. But it’s not impossible. |Steven Goff |January 14, 2021 |Washington Post 

Of course, Smith soon roamed again, taking a short pass through the middle and over to the right sideline for a 34-yard touchdown, with the only drawback his hard fall into the end zone upon his backside. Alabama advances to the national title game with an artful offensive performance |Chuck Culpepper, Des Bieler |January 2, 2021 |Washington Post 

We correctly forecast when the changeover from snow to mixed precipitation would occur and when cold air and frozen precipitation would return on the storm’s backside. Here’s how much snow and ice fell in D.C. region from Wednesday’s winter storm |Jason Samenow |December 17, 2020 |Washington Post 

For instance, if there’s one corner matched up with one receiver on the backside of a formation, that corner’s playing man no matter what fancy name you give the coverage. In effective pass coverage, what you see isn’t always what you get |Richard Johnson |November 11, 2020 |Washington Post 

She knows you can’t see her face, but you can see her entire backside. ‘Revenge porn’ was already commonplace. The pandemic has made things even worse. |Jessica M. Goldstein |October 29, 2020 |Washington Post 

I was drawn to The Class for different reasons—chiefly, the pipe dream of achieving a tighter and tauter backside. How Taryn Toomey’s ‘The Class’ Became New York’s Latest Fitness Craze |Lizzie Crocker |January 9, 2015 |DAILY BEAST 

San Francisco police officer Chris Kohrs is hotter than the devil's backside on an August day in Georgia. Castro Street’s Hot Cop Is the Batman to Sexy Mug Shot Guy’s Joker |Itay Hod |July 9, 2014 |DAILY BEAST 

He said he still gets some pain “in my backside” and a couple of ibuprofen take care of that. Rick Perry, Humbled by His ‘Oops,’ May Be Ready This Time |Eleanor Clift |June 23, 2014 |DAILY BEAST 

While slips can hide VPL and your backside on a windy day, Spanx can help hide a lot more, like cellulite or a protruding tummy. Kate Middleton’s “Bottomgate” Shows Why Women Still Need Slips |Keli Goff |May 28, 2014 |DAILY BEAST 

As I have found out, recognition has its upside, its downside and—you may say—its backside. Kanye West Is a Modern Michelangelo; Pippa Middleton Jokes About Her Bridesmaid Dress |The Fashion Beast Team |February 26, 2014 |DAILY BEAST 

A squad of civilian youngsters was chasing Major Dampfer down the street, pelting the huge target of his backside with snowballs. The Great Potlatch Riots |Allen Kim Lang 

In fact, the boy, from a backside view at a little distance, seemed to be wearing a long-tailed coat. Watch Yourself Go By |Al. G. Field 

So—just the very backside of truth,—but lying is a figure in speech that interlards the greatest part of my conversation. Love for Love |William Congreve 

Just as I'm about to go sitting on a bare board, your worship would have me score my backside! The History of Don Quixote, Volume II., Complete |Miguel de Cervantes 

Many a younger fellow would sit on his backside for sheer astonishment. Pelle the Conqueror, Complete |Martin Anderson Nexo