When it comes to mating, the banded mongoose (Mungos mungo) likes to keep things in the family. Mongooses, Meerkats, and Ants, Oh My! Why Some Animals Keep Mating All in the Family |Helen Thompson |December 29, 2014 |DAILY BEAST 

A male and female who do most of the mating dominate packs, and younger subordinates only breed occasionally. Mongooses, Meerkats, and Ants, Oh My! Why Some Animals Keep Mating All in the Family |Helen Thompson |December 29, 2014 |DAILY BEAST 

Mating with a cousin or brother is safer than risking life and limb to mate with an outsider. Mongooses, Meerkats, and Ants, Oh My! Why Some Animals Keep Mating All in the Family |Helen Thompson |December 29, 2014 |DAILY BEAST 

In any case, there is a growing number of people evolving mating rituals not based on getting wasted. All the Grown-Up Hipsters Playing Kids’ Games |Daniel Genis |June 29, 2014 |DAILY BEAST 

Given that their mating sessions can last up to 70 hours, their bedroom—well, cave—performances seem pretty darn impressive. Not So Fast on the ‘Female Penis’ |Charlotte Lytton |April 20, 2014 |DAILY BEAST 

I don't doubt you will find more than one affinity if you are awakening; that is merely the mating instinct. Ancestors |Gertrude Atherton 

When the owls beat their wings and gave the mating call and hoot, it was like a foam of noise rising over a river of silence. Kari the Elephant |Dhan Gopal Mukerji 

Mating determined by unconscious psychological motives instead of eugenic considerations. Taboo and Genetics |Melvin Moses Knight, Iva Lowther Peters, and Phyllis Mary Blanchard 

Therefore, she can have no other motive for marrying a man than that of mating herself to a true companion. A California Girl |Edward Eldridge 

Many insects vulgarly supposed to be different species are but males and females of one race seeking each other for mating. The Natural Philosophy of Love |Remy de Gourmont