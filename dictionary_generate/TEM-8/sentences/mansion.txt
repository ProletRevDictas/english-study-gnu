If Faulconer rides a recall to the governor’s mansion, it will put an appropriate bow on his political career to date. Morning Report: Growing the County’s Carbon-Cutting Efforts |Voice of San Diego |February 3, 2021 |Voice of San Diego 

She’s a hair braider stuck in a doomed marriage, which is made all the worse when her husband Adan’s burglary of a nearby beach mansion does not go as planned. Here Are the 14 New Books You Should Read in February |Annabel Gutterman |February 2, 2021 |Time 

On the southern coast, there are barriers along Kahala Beach, where the shore has increasingly disappeared amid sprawling mansions. Officials Let Hawaii’s Waterfront Homeowners Damage Public Beaches Again and Again |by Sophie Cocke, Honolulu Star-Advertiser |December 31, 2020 |ProPublica 

The crisis runs rampant in cities and towns, on farms and in mansions, far away and next door. Doctors and patients face a painkiller crisis, even as they fight COVID |matthewheimer |December 16, 2020 |Fortune 

Gilman died before construction was complete and the mansion changed hands several times until it was gifted to the Baltimore Parks and Recreation Department. Escape (safely) to Baltimore’s luxurious Ivy Hotel |Kevin Naff |December 4, 2020 |Washington Blade 

Expect the couple to find another mansion in a safe Democratic district where an aging representative is expected to retire. The Rise and Fall of Chris Hughes and Sean Eldridge, America’s Worst Gay Power Couple |James Kirchick |December 9, 2014 |DAILY BEAST 

Goodman retreated to house arrest at his luxury mansion on $4 million bail. Money, Murder, and Adoption: The Wild Trial of the Polo King |Jacqui Goddard |October 28, 2014 |DAILY BEAST 

We ended up in one room in her mansion and never furnished it. Tom Sizemore’s Revenge: On Tom Cruise’s Scientology Recruitment, Drugs, and Craving a Comeback |Marlow Stern |September 26, 2014 |DAILY BEAST 

The ex-chef, accused by Maureen McDonnell of “embezzling” food from the mansion, went to the FBI, triggering its investigation. Tough-Guy Pols Let Wives Take the Fall, Maureen McDonnell Edition |Eleanor Clift |August 26, 2014 |DAILY BEAST 

Bogie and Bacall purchased a $160,000 mansion in Holmby Hills, a posh enclave in Los Angeles, and played house. Bogie & Bacall: A Hollywood Romance for the Ages |Marlow Stern |August 13, 2014 |DAILY BEAST 

Within were the park and the deer, and the mansion rearing its brilliant columns amidst the redundant groves of a Spanish autumn. The Pastor's Fire-side Vol. 3 of 4 |Jane Porter 

It was a splendid mansion, with its countless rooms and gorgeous appointments. Blackwood's Edinburgh Magazine, No. CCCXXXIX. January, 1844. Vol. LV. |Various 

He went home from the planter's mansion very happy, for now he should have an errand there every day during the next few weeks. Alila, Our Little Philippine Cousin |Mary Hazelton Wade 

Americans burnt the mansion house on Noddles island, and carried off the cattle. The Every Day Book of History and Chronology |Joel Munsell 

It was a great event, and Jane was dressed with the utmost care to visit the aristocratic mansion. Madame Roland, Makers of History |John S. C. Abbott