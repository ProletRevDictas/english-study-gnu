The team at Mount Rainier was supposed to look for these snow caves at the end of the spring, after the animals have left but before the snow all melts. Mount Rainier’s first wolverine mama in a century is a sign of the species’ comeback |Hannah Seo |August 28, 2020 |Popular-Science 

Other broken-off cave growths had been wielded as digging tools. Underwater caves once hosted the Americas’ oldest known ochre mines |Bruce Bower |July 3, 2020 |Science News 

The newly found items include stone tools and pendants made from cave bear teeth. This cave hosted the oldest known human remains in Europe |Bruce Bower |June 12, 2020 |Science News For Students 

His team spends up to 12 hours each time it ventures into these deep caves. The challenge of dinosaur hunting in deep caves |John Pickrell |May 19, 2020 |Science News For Students 

In response, the angry Amaterasu retreated into a dark cave, thus snatching away her divine light from the world, while the ever-boisterous Susanoo went away from heaven. 12 Major Japanese Gods and Goddesses You Should Know About |Dattatreya Mandal |May 6, 2020 |Realm of History 

Cast Angelina Jolie in that role with Brad Pitt as the cave hubbie, and maybe we have a blockbuster in the making. Can Tarzan of the Apes Survive in a Post-Colonial World? |Ted Gioia |November 23, 2014 |DAILY BEAST 

The existence of the images—which resemble the styles and themes found in European cave art—has been known for some time. The Oldest Cave Art May Not Be in Europe |Justin Jones |October 9, 2014 |DAILY BEAST 

It surpasses the paintings of horses and rhinoceros from the Chauvet Cave in France by 400 years. The Oldest Cave Art May Not Be in Europe |Justin Jones |October 9, 2014 |DAILY BEAST 

Scientists have long thought that the oldest cave art was in Europe. The Oldest Cave Art May Not Be in Europe |Justin Jones |October 9, 2014 |DAILY BEAST 

It was dark, dank, the walls charcoal-colored, the feeling of a cave. Fighting Back With Faith: Inside the Yezidis’ Iraqi Temple |Michael Luongo |August 21, 2014 |DAILY BEAST 

More soldiers crowded into the cave and Professor-Commander Krafft came in behind them. Sense of Obligation |Henry Maxwell Dempsey (AKA Harry Harrison) 

No young Cave Swallows were taken and gonads of adults were in various stages of reproductive activity. Summer Birds From the Yucatan Peninsula |Erwin E. Klaas 

Profiting by this, Benjy quietly moved away round a colossal buttress of the berg, and took refuge in an ice-cave. The Giant of the North |R.M. Ballantyne 

It was a dangerous journey into this wonderful cave, but sometime Alila must go there, his father said. Alila, Our Little Philippine Cousin |Mary Hazelton Wade 

If there be any whose appearance denotes a more than common birth, treat him with due respect, and conduct him to my cave. The Battle of Hexham; |George Colman