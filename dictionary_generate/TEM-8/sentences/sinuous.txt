To our surprise, the All-Purpose setting is so capable even in aggressively sporting driving on sinuous mountain highways, that the harsher ride of the Sport mode seems like an unnecessary trade-off. The Rivian R1T breaks the electric-pickup game wide open |Dan Carney |September 28, 2021 |Popular-Science 

Like some sinuous sax, we first hear the thoughts of Obersturmfuhrer Angelus (Golo) Thomsen. How Hitch & Amis Discovered Evil In My House |Peter Foges |September 28, 2014 |DAILY BEAST 

I love this German team, which is sinuous and brilliant and fluid and youthful. Why We Should Root for Germany |Tunku Varadarajan |July 7, 2010 |DAILY BEAST 

It is at once confining and infinitely sinuous, so at Biennale-time it abounds with situations I call Bonjour, Monsieur Courbet! My Biennale Favorites |Anthony Haden-Guest |June 8, 2009 |DAILY BEAST 

Whether this aptitude was combined with the sinuous cunning that is essentially Oriental Nigel did not know. Bella Donna |Robert Hichens 

The natural result of this billiard-ball movement of the waters is that the path of the stream is sinuous. Outlines of the Earth's History |Nathaniel Southgate Shaler 

Her hands were sinuous as serpents, the fingers tapering, the nails very long like the Chinese. Valley of the Croen |Lee Tarbell 

The river below Wroxham is very narrow and very sinuous; its banks lined with groves of trees which intercept the wind. Yachting Vol. 2 |Various. 

What a sinuous underground plot the superficial incidents of this journey covered! Overland |John William De Forest