Law professor sues George Mason University, challenging covid vaccine mandatePervading everything is a weariness with the virus, and a longing for more stable, certain times. As college campuses reopen, many faculty worry about covid |Susan Svrluga |August 24, 2021 |Washington Post 

It was an unrealistic burden to place on statistics, but the longing for a mathematical seal of approval burned hot. How the strange idea of ‘statistical significance’ was born |Bruce Bower |August 12, 2021 |Science News 

We come to understand that the alternating flashes of love, rage, longing and frustration Fanny experiences in the course of her obsession with Linda are echoes of what she feels for the mother who so remorselessly abandoned her. Amazon’s Divine Period Romance The Pursuit of Love Gives Classic Social Satire a Modern Twist |Judy Berman |July 30, 2021 |Time 

I preferred not to try so hard so that the longing did not dampen my spirits too much and not to end up with my soul rotten by depression and my eyes lost in memories. Locked up in the Land of Liberty: Part III |Yariel Valdés González |July 21, 2021 |Washington Blade 

Each conversation was a slam to my longings, a slap that plunged me deeper into a sea of ​​despair. Locked up in the Land of Liberty: Part II |Michael K. Lavers |July 14, 2021 |Washington Blade 

In that light, some Cubans might find themselves longing for the good old, bad old days. Will Hyman Roth Return to Havana With Normalized Relations? |John L. Smith |December 18, 2014 |DAILY BEAST 

That is to say, there is much longing in the hearts of Americans. During Advent, Lots of Waiting, But Not Enough Hope |Gene Robinson |December 7, 2014 |DAILY BEAST 

It was about the hope and longing for redemption and reconciliation that lies somewhere within each of us. During Advent, Lots of Waiting, But Not Enough Hope |Gene Robinson |December 7, 2014 |DAILY BEAST 

The convent, obviously, but also the court—and even her unrequited longing for the elusive lady of her sonnets. Sor Juana: Mexico’s Most Erotic Poet and Its Most Dangerous Nun |Katie Baker |November 8, 2014 |DAILY BEAST 

It requires him to let go of Lori and his longing for the past. The Walking Dead’s Luke Skywalker: Rick Grimes Is the Perfect Modern-Day Mythical Hero |Regina Lizik |October 28, 2014 |DAILY BEAST 

Feeling secure regarding their happiness and welfare, she did not miss them except with an occasional intense longing. The Awakening and Selected Short Stories |Kate Chopin 

But at intervals I still felt an inexpressible longing to see or hear from my mother. The Boarded-Up House |Augusta Huiell Seaman 

There was a longing light in his eyes and a look of appeal whenever our glances met. The Soldier of the Valley |Nelson Lloyd 

The little glimpse of domestic harmony which had been offered her, gave her no regret, no longing. The Awakening and Selected Short Stories |Kate Chopin 

The longing desire to hear the whole of the letter, might be read in every feature of her expressive face. The World Before Them |Susanna Moodie