There’s even another subset of businesses, such as San Francisco startup Elroy Air, that are designing new, massive vertical takeoff and landing drones that can lift far heavier cargos and carry them much further. Cessna makes history by taking off and landing with no one aboard. Here’s how |Jeremy Kahn |August 26, 2020 |Fortune 

The sales team was also responsible for selling the advertiser, in this case Cheerios, on the add-ons they could have in the vertical. ‘Not a simple adjacency strategy’: How Group Nine is selling advertisers on bigger and longer editorial deals |Kayleigh Barber |August 21, 2020 |Digiday 

It’s also developing an urban vertical farming project purported to be the region’s first. A Pandemic Tourism Pivot From Cool to Wellness |Dan Peleschuk |August 11, 2020 |Ozy 

To better understand these signals, we studied the correlation of 18 important ranking factors across 200 searches in the sports ticketing vertical. 1000 Ranking factors: How Google finds signals through the noise |Manick Bhan |July 22, 2020 |Search Engine Watch 

We’ve established a new knowledge vertical — effectively taking out subject matter expertise and packaging it together for insiders. South China Morning Post CEO Gary Liu on navigating a perilous time for Hong Kong |Pierre Bienaimé |July 14, 2020 |Digiday 

Two police assigned to the apartment on a detail were doing a “vertical patrol” up the stairs when the door opened. New York's Next Killer-Cop Grand Jury |Jacob Siegel |December 6, 2014 |DAILY BEAST 

They structured themselves not in vertical hierarchies but in networks, each member responding to conditions on the ground. How the NSA Became a Killing Machine |Shane Harris |November 9, 2014 |DAILY BEAST 

Abramson is also open, “in theory,” to the idea of interviewing high-profile figures for the new vertical. How Funny or Die Plans to Cover ISIS, Ebola and Elections |Asawin Suebsaeng |October 10, 2014 |DAILY BEAST 

There was one very large and easily identifiable piece of debris floating, the vertical stabilizer. MH370 Debris Is Lost Forever, Can the Plane Be Found Without It? |Clive Irving |September 7, 2014 |DAILY BEAST 

The hybrid aircraft have a vertical takeoff and landing capability. U.S. Diplomats and Marines Close Embassy and Flee Libya Fighting |Jamie Dettmer |July 27, 2014 |DAILY BEAST 

This takes at first the crude device of a couple of vertical lines attached to the head (see Fig. 4). Children's Ways |James Sully 

It is of the vertical kind, and stands on a shallow square tank, which forms the hot well. Life of Richard Trevithick, Volume II (of 2) |Francis Trevithick 

However, in a month, when everything was about five degrees off the vertical, notice began to be taken. Old Friends Are the Best |Jack Sharkey 

He can not make a record of what he sees as long as the element of horizontal and vertical distance is not clearly in mind. Outlines of the Earth's History |Nathaniel Southgate Shaler 

But the singular fact exists that in the human trunk the valves occur in the horizontal and are absent from the vertical veins. Man And His Ancestor |Charles Morris