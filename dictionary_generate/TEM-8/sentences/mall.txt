A late-August Morning Consult survey found that only 36% of consumers were willing to visit a shopping mall in the next 2 – 3 months. A Corona Xmas: Why physical stores will power online shopping this holiday season |Greg Sterling |September 4, 2020 |Search Engine Land 

In February, Macy’s said it would close its underperforming stores in the weakest US malls and explore ways to expand beyond malls. After decades of anchoring malls, Macy’s wants out |Marc Bain |September 2, 2020 |Quartz 

Simon bought fast-fashion purveyor Forever 21 out of bankruptcy earlier this year along with Authentic Brands and another large mall owner, Brookfield Property Partners. America’s Largest Shopping Mall Owner Gets a New Tenant: Itself |Daniel Malloy |August 20, 2020 |Ozy 

Suddenly Hangzhou’s 10 million residents were all required to show a green code to take the subway, shop for groceries, or enter a mall. Inside China’s unexpected quest to protect data privacy |Tate Ryan-Mosley |August 19, 2020 |MIT Technology Review 

Almost all the public spaces, starting with the malls and pharmacies, still require masks. How Mongolia has kept the coronavirus at bay |Tate Ryan-Mosley |August 18, 2020 |MIT Technology Review 

Your general reaction runs along the lines of: “When will these geezers give it up and go for a mall walk or something?” The World’s Toughest Political Quiz |Jeff Greenfield |December 31, 2014 |DAILY BEAST 

They had to go to the bazaar -- as the mall was then called -- and buy them. Keep Christmas Commercialized! |P. J. O’Rourke |December 6, 2014 |DAILY BEAST 

The figure enters the elevator and is then seen quickly leaving the mall, black cloth flapping behind it. Middle East Murder Mystery: Who Killed an American Teacher in Abu Dhabi? |Chris Allbritton |December 3, 2014 |DAILY BEAST 

Someone dressed as an Emirati woman killed an American teacher in a mall bathroom. Middle East Murder Mystery: Who Killed an American Teacher in Abu Dhabi? |Chris Allbritton |December 3, 2014 |DAILY BEAST 

Contrary to what you may assume about me, I actually enjoy the occasional trip to the mall. It’s Always Black Friday for Clerks |Michael Tomasky |November 28, 2014 |DAILY BEAST 

The first wounded mall who appealed for help was sitting with his back against a dead comrade. A Connecticut Yankee in King Arthur's Court, Complete |Mark Twain (Samuel Clemens) 

The shrill notes of the trumpets sounded louder and louder, and a brilliant cavalcade appeared at the end of the mall. The Seven Cardinal Sins: Envy and Indolence |Eugne Sue 

A promenade called the mall, shaded by lofty trees, bounds Pont Brillant on the south. The Seven Cardinal Sins: Envy and Indolence |Eugne Sue 

An old servant woman ushered them into the parlour, which was on the second floor, with windows overlooking the mall. The Seven Cardinal Sins: Envy and Indolence |Eugne Sue 

When at last she drew up the glass and her chair was borne away down the Mall, he sauntered idly in the opposite direction. The Rake's Progress |Marjorie Bowen