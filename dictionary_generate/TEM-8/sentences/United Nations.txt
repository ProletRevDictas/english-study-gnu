Nations' formal emissions-cutting pledges are collectively way too weak to put the world on track to meet the Paris climate deal's temperature-limiting target, a United Nations tally shows. UN says Paris carbon-cutting plans fall far short |Ben Geman |February 26, 2021 |Axios 

A United Nations report published six years later found that Shell had not followed its own procedures regarding the maintenance of oilfield infrastructure. Lawyers Are Working to Put 'Ecocide' on a Par with War Crimes. Could an International Law Hold Major Polluters to Account? |Mélissa Godin |February 19, 2021 |Time 

According to a United Nations report, the top 1 percent of income earners in the world account for 15 percent of emissions. Can Billionaires Really Save Us from Climate Disaster? |Heather Hansman |February 12, 2021 |Outside Online 

After learning about Nobel Peace Prize winner Malala Yousafzai, who stood up for girls’ education in Pakistan, Gorman made up her mind to participate in an annual meeting on women’s rights at the United Nations headquarters in New York. Amanda Gorman learned the power of poetry early on |Los Angeles Times |February 1, 2021 |Washington Post 

A United Nations agency called the Carbon Offsetting and Reduction Scheme for International Aviation, or CORSIA, wants airlines to stabilize CO2 emissions at 2020 levels. United Airlines aims to suck carbon dioxide from the friendly skies |Steven Mufson |January 12, 2021 |Washington Post 

It would became one of the first great mysteries in the United States of America, as it was only then 23 years old. New York’s Most Tragic Ghost Loves Minimalist Swedish Fashion |Nina Strochlic |January 8, 2015 |DAILY BEAST 

There is a particular focus in the magazine on attacking the United States, which al Qaeda calls a top target. U.S. Spies See Al Qaeda Fingerprints on Paris Massacre |Shane Harris, Nancy A. Youssef |January 8, 2015 |DAILY BEAST 

The United States government might not release that information for years, if ever. Was Sony Hit With a Second Hack? |Shane Harris |January 8, 2015 |DAILY BEAST 

While this deferred action is controversial in the United States, in Mexico, what Obama did is universally popular. Why Mexicans Are Enraged by Obama’s Big Tuesday Meeting |Ruben Navarrette Jr. |January 6, 2015 |DAILY BEAST 

Most coup members “lived in the diaspora in the United States and Germany,” Faal said. The Shadowy U.S. Veteran Who Tried to Overthrow a Country |Jacob Siegel |January 6, 2015 |DAILY BEAST 

Nations shall declare his wisdom, and the church shall shew forth his praise. The Bible, Douay-Rheims Version |Various 

Understandingthe best way to live,United for Serviceour Country to give. Boys and Girls Bookshelf; a Practical Plan of Character Building, Volume I (of 17) |Various 

Renounce the good law of the worshippers of Mazda, and thou shalt gain such a boon as the Murderer gained, the ruler of nations. Solomon and Solomonic Literature |Moncure Daniel Conway 

And as a flood hath watered the earth; so shall his wrath inherit the nations, that have not sought after him. The Bible, Douay-Rheims Version |Various 

Its record is largely that of battles and sieges, of the brave adventure of discovery and the vexed slaughter of the nations. The Unsolved Riddle of Social Justice |Stephen Leacock