Doug Williams, the Ring of Fame quarterback and longtime member of the Washington Football Team’s front office, has been appointed senior adviser to team president Jason Wright, the team announced Thursday. Doug Williams takes on new role with Washington Football Team |Nicki Jhabvala |February 4, 2021 |Washington Post 

There’s been plenty of uproar in Oceanside recently over Kori Jensen, a Realtor and the city’s newest councilwoman, and whether she actually lives in the district she was appointed to represent. North County Report: Appointment of New Oceanside Leader Making Waves |Kayla Jimenez |February 4, 2021 |Voice of San Diego 

In her January veto letter, Bowser said the new watchdog would create a “duplicative, overly broad and costly administrative structure” — and possibly violate the Home Rule Act by creating an executive office with a director appointed by the council. D.C. Council expected to override two Bowser vetoes on Tuesday |Michael Brice-Saddler |February 2, 2021 |Washington Post 

It also mandates that the Commander-in-Chief appoints key ministerial positions, and allows the military to take over in times of emergency. Myanmar’s Military Didn’t Just Overthrow Aung San Suu Kyi's Government. It’s Cracking Down on All Forms of Dissent |Kyaw Hsan Hlaing / Yangon, Myanmar |February 2, 2021 |Time 

When appointed principal, he was the first lay person to head a Catholic school in the Washington archdiocese. John Moylan, who led DeMatha High School for decades, dies at 88 |Bart Barnes |February 2, 2021 |Washington Post 

I quickly appoint myself bartender and make the man a drink. Alfred Hitchcock’s Fade to Black: The Great Director’s Final Days |David Freeman |December 13, 2014 |DAILY BEAST 

And he was said to be urging Obama to appoint her as his successor. For Next AG, Obama Picks a Quiet Fighter With a Heavy Punch |Michael Daly |November 8, 2014 |DAILY BEAST 

If I resign any time this year, he could not successfully appoint anyone I would like to see in the court. Ruth Bader Ginsburg Levels With Us on Why She’s Not Retiring |Jeff Greenfield |September 25, 2014 |DAILY BEAST 

Baquet and his evolving leadership team (he has yet to appoint a managing editor) face daunting challenges. Dean Baquet, the NYT’s Executive Editor, on Jill Abramson, Race, Surviving Cancer—and TMZ Envy |Lloyd Grove |September 16, 2014 |DAILY BEAST 

The online recruiters say IS can appoint a guardian for them to provide permission. The ISIS Online Campaign Luring Western Girls to Jihad |Jamie Dettmer |August 6, 2014 |DAILY BEAST 

The board will appoint a settling agent who shall keep the necessary records and accounts. Readings in Money and Banking |Chester Arthur Phillips 

Before I set about it I wish to see you and Mr. Fox, and will call any day you may appoint. Life of Richard Trevithick, Volume II (of 2) |Francis Trevithick 

If these gentlemen had thought to avoid slippery ground, they should have elected to appoint the meeting elsewhere. St. Martin's Summer |Rafael Sabatini 

I intend to sail for that place in about a month or six weeks, but shall appoint agents in England to erect these engines. Life of Richard Trevithick, Volume II (of 2) |Francis Trevithick 

So I was obliged to appoint somebody whose rank and lofty position under the government would protect him. A Connecticut Yankee in King Arthur's Court, Complete |Mark Twain (Samuel Clemens)