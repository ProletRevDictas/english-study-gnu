She found that even after considering geographic and demographic factors, the “Rush Limbaugh effect” persists. Political rhetoric may impact your hurricane preparedness |Ula Chrobak |September 11, 2020 |Popular-Science 

Ultimately they discovered that the turbulent patch persisted as long as the barrage kept coming. An Unexpected Twist Lights Up the Secrets of Turbulence |David H. Freedman |September 3, 2020 |Quanta Magazine 

A blunted stress response persisted into middle childhood, even after an average of seven to eight years in a household with healthy caregiving. Puberty can repair the brain’s stress responses after hardship early in life |Esther Landhuis |August 28, 2020 |Science News 

This allows stable patterns of useful behavior to emerge and persist. No, Animals Do Not Have Genders - Facts So Romantic |Cailin O'Connor |August 26, 2020 |Nautilus 

That’s the paperwork used to report interest income — and serves as a sign that low interest rates could persist. The U.S. economy is shedding over 1 million jobs per week. They won’t come back for years, the IRS says |Bernhard Warner |August 21, 2020 |Fortune 

While violent offenses are dramatically down in Bed Stuy, pockets of violence persist here. Anger at The Cop Killer - And The Police |M.L. Nestel |December 21, 2014 |DAILY BEAST 

In Sierra Leone, the WHO report reads, “steep increases persist.” Jail Threats for Sierra Leone Ebola Victims’ Families |Abby Haglage |December 10, 2014 |DAILY BEAST 

“That said, underneath the surface differences in threat perception and how to deal with Russia persist,” he says. Shocked by Ukraine Violence, NATO Prepares to Face Down Putin |Leo Cendrowicz |October 12, 2014 |DAILY BEAST 

And yet the real battle might be an even harder one: against the attitudes that allow all this to persist in 21st-Century India. Kailash Satyarthi, Malala's Nobel Peace Prize Co-Winner, Is Fighting India's Child Slavery Epidemic |Dilip D’Souza |October 11, 2014 |DAILY BEAST 

He went on to predict that Hong Kong will see “economic chaos” should the protests persist. Hong Kong Between Calm and Chaos |Ben Leung |October 3, 2014 |DAILY BEAST 

This fact worried him considerably, and made him persist in his own mind that the company would accept it. The Homesteader |Oscar Micheaux 

Typhoid bacilli have been known to persist for months and even years after the attack. A Manual of Clinical Diagnosis |James Campbell Todd 

This habit of scribble may persist after a child attempts a linear description of the parts of an object. Children's Ways |James Sully 

Doubtless Schopenhauer was right: it is merely the furious determination of the race to persist. Ancestors |Gertrude Atherton 

I suppose you will repent, and seek forgiveness for your sins, Worse; or will you persist in putting it off? Skipper Worse |Alexander Lange Kielland