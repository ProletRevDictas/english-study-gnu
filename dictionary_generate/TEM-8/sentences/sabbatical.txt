I was also teaching my courses at UC-Berkeley much of that time, though I had time off in the summers and through a sabbatical. How Richard Pryor Beat Bill Cosby and Transformed America |David Yaffe, Scott Saul |December 10, 2014 |DAILY BEAST 

After a four-year sabbatical, today LeBron James decided to go back home. LeBron James: The Yoko Ono of The Heatles |Robert Silverman |July 12, 2014 |DAILY BEAST 

The key to a truly disconnected holiday is to take an email sabbatical, says Danah Boyd, senior researcher at Microsoft. Seven Things to Do on National Day of Unplugging |Sam Schlinkert |March 1, 2013 |DAILY BEAST 

Her recent medical episode underscores her need for a sabbatical. Hillary Clinton 2016? Women Look Ahead To ‘History In the Making’ |Paul Alexander |January 16, 2013 |DAILY BEAST 

However, the rigor of the Jewish Sabbatical laws was by no means followed. Speeches, Addresses, and Occasional Sermons, Volume 2 (of 3) |Theodore Parker 

There are not less spirits drank on amount of a sabbatical gloom; for harmless chearfulness is rather a preservative of innocence. Letter To Sir Samuel Shepherd |Anonymous 

A sabbatical calm results from the contemplation of his labours. Res Judicat |Augustine Birrell 

Mrs. Morran all forenoon was in a state of un-Sabbatical disquiet. Huntingtower |John Buchan 

The seventh is the Sabbatical year, when bondsmen were to be released and debts go free. Bible Studies |Joseph M. Wheeler