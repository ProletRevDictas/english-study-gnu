Some areas from the District south are seeing a mix of snow, sleet and rain while, to the north, it’s more snow. Snow and wintry mix continue overnight, especially north of District |Jason Samenow, Wes Junker, Andrew Freedman |February 11, 2021 |Washington Post 

Rechargeable hand warmers are susceptible to water damage, so it’s probably best to keep them away from snow and sleet. Best hand warmers: Block the chill during your favorite winter activities |PopSci Commerce Team |February 10, 2021 |Popular-Science 

Some sleet could start mixing in esp south and southeast of town. After 2 to 4 inches of snow, an icy night ahead in D.C. area. Snow showers possible Monday. |Jason Samenow, Dan Stillman, Andrew Freedman |February 1, 2021 |Washington Post 

Monday night’s wintry mix generally produced a coating to an inch of snow and sleet from the District northward. Washington receives first measurable snow in 372 days, with another threat looming Sunday |Jason Samenow, Wes Junker |January 26, 2021 |Washington Post 

In fact, sleet has been reported as far south as Woodbridge, Waldorf and Southern Maryland. Updated forecast: After coating of snow and sleet, period of freezing rain possible overnight |Jason Samenow, Dan Stillman |January 26, 2021 |Washington Post 

Sleet is rain mixed with snow; there are pellet like snowflakes that fall between warming and cooling fronts. How to Survive a Southern Ice Storm |Nicholas Isabella |February 13, 2014 |DAILY BEAST 

Many people do not understand the difference between sleet and freezing rain. How to Survive a Southern Ice Storm |Nicholas Isabella |February 13, 2014 |DAILY BEAST 

In the past 63 years—through rain, sleet, snow, and bone-chilling cold—there have been 15 inaugural parades. Charlie Brotman, Announcer of Presidential Inaugurals Since Truman’s |Sandra McElwaine |January 17, 2013 |DAILY BEAST 

“Rain, sleet, snow, shark, alien invasions, whatever,” Dobles says. Send This Hurricane Sandy Victim a Card, Please |Michael Daly |December 9, 2012 |DAILY BEAST 

Later, men soaked in sleet walked along the Garden Ring, oblivious to the weather and glowing with happy smiles. Russia Cracks Down on North Caucasus Wedding Gunfire |Anna Nemtsova |October 28, 2012 |DAILY BEAST 

The weather was cold and the night dark, and there were peppery little showers of sleet. A Little Union Scout |Joel Chandler Harris 

All winter the cabin in Lonesome Cove slept through rain and sleet and snow, and no foot passed its threshold. The Trail of the Lonesome Pine |John Fox, Jr. 

Then, after standing until almost night in the snow, which had now turned to sleet, the column was headed homeward. The County Regiment |Dudley Landon Vaill 

After our one perfect day the weather changed again, and for the next three days we descended the Kama Valley in sleet and snow. Mount Everest the Reconnaissance, 1921 |Charles Kenneth Howard-Bury 

The rain and sleet beat through his clothes, and struck his skin with a sharp chilling touch that set him trembling. Gallegher and Other Stories |Richard Harding Davis