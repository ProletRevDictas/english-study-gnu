When cords snapped and the piano fell, those same keys could still be played, but they were now out of tune. ‘Little Nightmares II’ made me dread every moment. And I loved it. |Elise Favis |February 9, 2021 |Washington Post 

Rogers gave piano lessons to beginners as well as to more experienced musicians. Joanne Rogers, memory-keeper of ‘Mister Rogers’ Neighborhood,’ dies at 92 |Emily Langer |January 14, 2021 |Washington Post 

He described himself as “making revolution all over the place, falling in and out of love all the time, drinking and playing bridge” until he settled into serious study of the piano. Fou Ts’ong, Chinese pianist who bridged East and West, dies at 86 of covid-19 |Emily Langer |January 7, 2021 |Washington Post 

In October, New Yorker food writer Helen Rosner tweeted about a McDonald’s commercial that’s been lodged in her brain since childhood, this one featuring a girl at a music recital who’s thinking about McDonald’s instead of the piano. The McDonald’s Commercials That Live in Our Minds, Rent Free |MM Carrigan |December 18, 2020 |Eater 

They’re also saving $1,500 a month on child care and several hundred dollars a month on piano lessons and taekwondo classes for their older children. A time to splurge or a time to scrape by: Holidays expose widening disparity among U.S. families |Abha Bhattarai |December 17, 2020 |Washington Post 

I sat down at the living room piano and started playing this. Yes, I Like Christmas Music. Stop Laughing. |Michael Tomasky |December 24, 2014 |DAILY BEAST 

On piano was legendary session man Billy Preston, who co-wrote and recorded the original version one year before Cocker's. Joe Cocker's Deep Live Cuts |Asawin Suebsaeng |December 22, 2014 |DAILY BEAST 

Seth, Joe, and [Anthony] Mackie reenacting the scene from BIG on the floor piano at FAO Schwartz with KANYE WEST. Exclusive: Sony Emails Reveal Destiny’s Child and Kanye West Movies, and Spidey Cameo in Capt. 3 |William Boot |December 14, 2014 |DAILY BEAST 

A department store piano melody plays in the background while he admires everything he can see. Kirk Cameron Saves Christmas from Abominable Killjoys (Other Christians) |Brandy Zadrozny |November 14, 2014 |DAILY BEAST 

When you left the piano for the Fender Rhodes and various synthesizers and came back to it, did it affect the way you play piano? Herbie Hancock Holds Forth |David Yaffe |November 8, 2014 |DAILY BEAST 

He really seems to care almost nothing for his piano-playing or for his piano compositions. Music-Study in Germany |Amy Fay 

A splendid grand piano stands in one window (he receives a new one every year). Music-Study in Germany |Amy Fay 

At an early hour in the evening the Farival twins were prevailed upon to play the piano. The Awakening and Selected Short Stories |Kate Chopin 

The very first chords which Mademoiselle Reisz struck upon the piano sent a keen tremor down Mrs. Pontellier's spinal column. The Awakening and Selected Short Stories |Kate Chopin 

She sat perfectly still before the piano, not touching the keys, while Robert carried her message to Edna at the window. The Awakening and Selected Short Stories |Kate Chopin