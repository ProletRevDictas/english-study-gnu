The word inflammation was an essential one to early accounts of the virus coming out China a year ago. The virus caused more than a pandemic. It set us all ablaze. |Philip Kennicott |February 5, 2021 |Washington Post 

In a 2019 study that inspired the randomized trial, mice that lacked sigma-1 receptor died from systemic inflammation known as sepsis, whereas fluvoxamine treatment protected animals from deterioration and death. The antidepressant fluvoxamine could keep mild COVID-19 from worsening |Esther Landhuis |February 1, 2021 |Science News 

Obesity in some situations appears to be associated with chronic inflammation, which we think can be a driver of cancer. How to Fix the Incentives in Cancer Research (Ep. 449) |Stephen J. Dubner |January 28, 2021 |Freakonomics 

When stress is reduced, inflammation is reduced, and the immune system is better equipped to fight off infection and illness. The best humidifier: Fight dry air (and a dry nose) all winter long |PopSci Commerce Team |January 15, 2021 |Popular-Science 

In experiments on pairs of mice, one mouse received an injection that caused arthritis-like inflammation in one hind paw while the other mouse was unharmed. Mice may ‘catch’ each other’s pain — and pain relief |Carolyn Wilke |January 12, 2021 |Science News 

You get vaccinated in the arm, you shouldn't have inflammation in the joint. Uh Oh: Ebola Vaccine Trials Stop |Leigh Cowart |December 19, 2014 |DAILY BEAST 

The truth is that any exercise releases cortisol and results in a certain level of inflammation—and this is a good thing! Dispelling the ‘Chronic Cardio’ Myth |Jason Fitzgerald |April 23, 2014 |DAILY BEAST 

Their goals are not weight reduction or reducing inflammation, but rather, to race as fast as possible. Dispelling the ‘Chronic Cardio’ Myth |Jason Fitzgerald |April 23, 2014 |DAILY BEAST 

Animal studies showed a correlation between inflammation and cancer growth. How Big Pharma Holds Back in the War on Cancer |ProPublica |April 23, 2014 |DAILY BEAST 

Over-training is the result of systemic inflammation and under-recovery—not aerobic exercise. Dispelling the ‘Chronic Cardio’ Myth |Jason Fitzgerald |April 23, 2014 |DAILY BEAST 

He was threatened with erysipelas, and there was a rather critical inflammation of the left eye. Checkmate |Joseph Sheridan Le Fanu 

Larger amounts, not well mixed with fecal matter, indicate inflammation of the large intestine. A Manual of Clinical Diagnosis |James Campbell Todd 

Vincent's angina is a chronic pseudomembranous and ulcerative inflammation of pharynx and tonsils. A Manual of Clinical Diagnosis |James Campbell Todd 

The inflammation then hastily disappeared without producing the most distant mark of affection of the system. An Inquiry into the Causes and Effects of the Variolae Vaccinae |Edward Jenner 

On the 4th day, the inflammation was evidently subsiding, and on the 6th it was scarcely perceptible. An Inquiry into the Causes and Effects of the Variolae Vaccinae |Edward Jenner