Still, if there’s any connection at all, then it should ease concerns about inflation. After $20 trillion in pandemic relief spending, there’s still no sign of inflation. What happened? |Bernhard Warner |August 25, 2020 |Fortune 

After adjusting for inflation over two decades, the deal with Cisterra would end up costing about $127 million, according to the city’s independent budget analyst. How the City Came to Lease a Lemon |Lisa Halverstadt and Jesse Marx |August 10, 2020 |Voice of San Diego 

They analyzed how a collapsing universe would change its own structure, and they ultimately discovered that contraction can beat inflation at its own game. Big Bounce Simulations Challenge the Big Bang |Charlie Wood |August 4, 2020 |Quanta Magazine 

Its supporters believe a currency’s value should be connected with the amount of gold in reserves as a way to prevent inflation. Senate committee gave key approval to Fed nominee’s unorthodox economic views |Karen Ho |July 21, 2020 |Quartz 

California recently capped the amount rent could be raised across the state at 5 percent plus inflation. Why Rent Control Doesn’t Work (Ep. 373 Rebroadcast) |Stephen J. Dubner |March 12, 2020 |Freakonomics 

It raises inflation, since you need more rubles to buy imports. Putin Can’t Bully or Bomb a Recession |Daniel Gross |December 16, 2014 |DAILY BEAST 

The penalties are set in stone through 2016, but after that they will remain at $695 per year, plus inflation. Think You’re Invincible? Here’s Why Open Enrollment Matters |DailyBurn |November 16, 2014 |DAILY BEAST 

The median household income in inflation-adjusted dollars back in 1987 was… well, what do you think? The Real Reason Democrats Lost |Michael Tomasky |November 6, 2014 |DAILY BEAST 

A predictable, inflation-adjusted minimum wage would make business planning easier. To Make Their Victory Durable, the GOP Must Fix the Minimum Wage |Dmitri Mehlhorn |November 6, 2014 |DAILY BEAST 

With a total of $289 million (adjusted for inflation), it is the highest-grossing silent film of all time. How to Save Silent Movies: Inside New Jersey’s Cinema Paradiso |Rich Goldstein |October 2, 2014 |DAILY BEAST 

I candidly answered that I could not see the end of the inflation. Four Years in Rebel Capitals |T. C. DeLeon 

Then the results of the inflation came with relentless and rapid pace. Four Years in Rebel Capitals |T. C. DeLeon 

The diagnostic characters were the length and breadth of the rostrum and the relatively great inflation of the auditory bullae. Speciation in the Kangaroo Rat, Dipodomys ordii |Henry W. Setzer 

Under the stress of war, this process of credit inflation has been growing like the genii let out of the bottle. The Book of Life: Vol. I Mind and Body; Vol. II Love and Society |Upton Sinclair 

And do not let anybody fool you for a single second by talking about "fiat money" and "inflation of the currency." The Book of Life: Vol. I Mind and Body; Vol. II Love and Society |Upton Sinclair