Katy Perry was flanked by dancers wearing shark costumes during her 2015 performance. The Weeknd’s Super Bowl performance sparked a bounty of memes, as the halftime show usually does |Travis Andrews |February 8, 2021 |Washington Post 

You aunt has hit a new low in attempts to act as costume director at a wedding. Miss Manners: Declining help from the unmasked |Judith Martin, Nicholas Martin, Jacobina Martin |February 8, 2021 |Washington Post 

I don’t know what furries did 50 years ago beyond imagining or surreptitiously donning a wolf costume. The country is being buffeted by groups that couldn’t exist 30 years ago |Philip Bump |January 27, 2021 |Washington Post 

We added a place to hang Halloween costumes, party dresses, ski clothes and “just-in-case” fashion. The pandemic gave me the time to finally clean out my shameful attic. Here’s what I learned. |Jura Koncius |January 21, 2021 |Washington Post 

Officials at Kaiser Permanente San Jose Medical Center said a staff member wearing an inflatable Christmas tree costume to spread holiday cheer likely spread coronavirus-laden droplets instead. Another coronavirus variant linked to growing share of cases, several large outbreaks, in California |Fenit Nirappil |January 18, 2021 |Washington Post 

Her daughter, Elaina, 24, a trained costume designer and makeup artist, helps out by sewing clothes. Inside A Finishing School for Transwomen |Sharon Adarlo |December 27, 2014 |DAILY BEAST 

The brand logo turned out to feature a graceful archer on horseback, in a Tatar national costume, poised to shoot his arrow. Rebranding The Land of Mongol Warriors & Ivan The Terrible |Anna Nemtsova |December 25, 2014 |DAILY BEAST 

He then dons a Spider-Man costume and savagely starts attacking criminals. Exclusive: Sony Hack Reveals Studio's Detailed Plans For Another ‘Spider-Man’ Reboot |William Boot |December 13, 2014 |DAILY BEAST 

What makes this story resonate with fans is that it proves it takes more than just the costume to become “Spider-Man.” Exclusive: Sony Hack Reveals Studio's Detailed Plans For Another ‘Spider-Man’ Reboot |William Boot |December 13, 2014 |DAILY BEAST 

The Metropolitan Museum of Art Costume Institute has staged some truly fantastic shows over the past few years. The Daily Beast’s 2014 Holiday Gift Guide: For the Carrie Bradshaw in Your Life |Allison McNearney |November 29, 2014 |DAILY BEAST 

I had no idea who they were, as the Grand Duke was in morning costume, and had no star or decoration to distinguish him. Music-Study in Germany |Amy Fay 

Movement to know that she was attired in appropriate costume—short frock, biped continuations and a mannish oil-skin hat. Glances at Europe |Horace Greeley 

If she is so distingue in rather less than ordinary dress, what would she be in a Parisian costume? Music-Study in Germany |Amy Fay 

It was a corso blanc, and everyone wore white—chiefly modifications of Pierrot costume—and everyone was masked. The Joyous Adventures of Aristide Pujol |William J. Locke 

In this portrait of tiny dimensions the Prince is represented in fancy costume, after the manner of Holbein. Bastien Lepage |Fr. Crastre