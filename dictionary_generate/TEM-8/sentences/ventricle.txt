My heart felt like someone had grabbed it inside my rib cage and squeezed so hard the ventricles were about to burst. The Hidden Truth About Parenting Isn't That It's Really Hard |Lucy Huber |February 17, 2022 |Time 

Neither Kelly nor Lecomte had any change in the left ventricle ejection fraction, or the amount of blood pumped out with each contraction. Space Can Take a Nasty Toll On An Astronaut's Heart, Study Finds |Jeffrey Kluger |April 2, 2021 |Time 

If San Diego’s urban core were a human heart, then the 53rd Congressional District would be, geographically, its left ventricle. Environment Report: Gómez and Jacobs on the Green New Deal, Climate Injustice and More |MacKenzie Elmer |October 12, 2020 |Voice of San Diego 

When she pulled her fingers away, she could see the thrust of his ventricle against his skin. 'Are You Also With Fever?' |Dr. Abraham Verghese |February 11, 2009 |DAILY BEAST 

Persistent glycosuria has been noted in brain injuries involving the floor of the fourth ventricle. A Manual of Clinical Diagnosis |James Campbell Todd 

The little concavity between the false vocal bands above and the true vocal bands below is termed the ventricle of the larynx. Voice Production in Singing and Speaking |Wesley Mills 

The blood is thus made to pass into the arteries upon the contraction of the ventricle walls. A Civic Biology |George William Hunter 

Large ones are far from rare, and the ventricle is frequently enormously distended. A Treatise on Sheep: |Ambrose Blacklock 

In pregnant women, a corrupt matter is generated which, flowing to the ventricle, spoils the appetite and causes sickness. The Works of Aristotle the Famous Philosopher |Anonymous