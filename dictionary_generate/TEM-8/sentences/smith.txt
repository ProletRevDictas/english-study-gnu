“In the end, people don’t want to hear about dogs and babies and feeling your way into a phrase, or eating your heart out — people want to hear about you as you appear in these songs,” Smith writes as Billie reflecting on Billie. Tragedy was part of Billie Holiday’s life. It doesn’t have to define it. |Bethonie Butler |February 26, 2021 |Washington Post 

Gregory Monahan, who will return to his post as assistant chief when Smith takes the chief’s position on Sunday. Pamela Smith named chief of U.S. Park Police |Tom Jackman |February 25, 2021 |Washington Post 

“This bill accomplishes all three of those things,” Smith said. Push for police reform creates rift in Maryland’s Democratic caucus |Ovetta Wiggins |February 25, 2021 |Washington Post 

In Mook’s absence, Smith will be the House Majority PAC’s main money guy, shoring up donors ahead of a campaign season in which more than a dozen Democratic incumbents are vulnerable. Pelosi’s top money guy to head up fundraising for House Democrats’ main super PAC |Colby Itkowitz |February 25, 2021 |Washington Post 

Past studies have explored how well bacteria might survive on Mars, but we know less about fungi, Smith says. Some of Earth’s tiniest living things could survive on Mars |Ellie Shechet |February 24, 2021 |Popular-Science 

“Barbarism,” said retired NYPD Officer Jim Smith on Thursday. The Muslim Cop Killed by Terrorists |Michael Daly |January 9, 2015 |DAILY BEAST 

“Please, please do not permit this to happen here in Florida,” wrote Cris K. Smith of East Polk County. Jeb Bush’s Unseen Anti-Gay Marriage Emails |Jackie Kucinich |January 9, 2015 |DAILY BEAST 

Smith attended both funerals as a cop and as the husband of Police Officer Moira Smith, who died on 9/11. The Muslim Cop Killed by Terrorists |Michael Daly |January 9, 2015 |DAILY BEAST 

“Angry Birds is a small fun game plus a lot of pointless garbage,” Smith tells me. Lost For Thousands of Strokes: 'Desert Golfing' Is 'Angry Birds' as Modern Art |Alec Kubas-Meyer |January 2, 2015 |DAILY BEAST 

Neither Smith nor Marx can carry us far into the guts of globalized financial capitalism. American Democracy Under Threat for 250 Years |Jedediah Purdy |December 28, 2014 |DAILY BEAST 

Miss Smith immediately rises from the table, puts up her dear little mouth to her papa to be kissed. Physiology of The Opera |John H. Swaby (AKA "Scrici") 

Smith's method usually gives good results, as does also the more simple method of Hiss (p. 263). A Manual of Clinical Diagnosis |James Campbell Todd 

It wasn't far, so we kept on, and presently it developed that we had accidentally come upon old Piegan Smith. Raw Gold |Bertrand W. Sinclair 

If old Piegan Smith hadn't been sampling the contents of that keg so industriously he would never have made a break. Raw Gold |Bertrand W. Sinclair 

It appears in nearly every book on economic theory from Adam Smith and Ricardo till to-day. The Unsolved Riddle of Social Justice |Stephen Leacock