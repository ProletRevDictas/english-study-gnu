He mentioned in particular that aid should focus on assistance for small businesses, among other things. Pelosi abruptly shifts course, restarts relief push amid signs economy is straining |Erica Werner, Rachael Bade |September 24, 2020 |Washington Post 

Forty percent claim they can’t last another six months without government aid, and it will be an arduous road to recovery for the rest. This restaurant duo want a zero-carbon food system. Can it happen? |Bobbie Johnson |September 24, 2020 |MIT Technology Review 

However, that aid has to be distributed at a sustainable pace and I think some of the programs that were thought up for Afghanistan were too ambitious. When Your Safety Becomes My Danger (Ep. 432) |Stephen J. Dubner |September 24, 2020 |Freakonomics 

Republican leaders have suggesting trying to pass another PPP as a standalone bill while Democratic leaders believe aid to states is vital to stave off mass public sector layoffs. Supreme Court fight could derail chances for second stimulus check and extended unemployment benefits |Lance Lambert |September 21, 2020 |Fortune 

An ingenious invention originally meant to digitize bookkeeping, the software has enabled researchers and businesspeople to input infinite rows and columns of disparate data and then analyze the information with the aid of a computer. How to make A.I. smarter |jonathanvanian2015 |September 21, 2020 |Fortune 

So working with the militants in order to deliver aid “becomes a requirement,” she said. ISIS Fight Has a Spy Shortage, Intel Chair Says |Kimberly Dozier |January 2, 2015 |DAILY BEAST 

Think of it as a frequent buyer program for personal karma, or a spiritual band-aid. The Buddhist Business of Poaching Animals for Good Karma |Brendon Hong |December 28, 2014 |DAILY BEAST 

In the Senate, the Democrats passed the budget 56-40 with the generous aid of 24 Republicans. Congress’ Gift That Keeps on Giving |P. J. O’Rourke |December 20, 2014 |DAILY BEAST 

After all, smaller developing nations like Cameroon often depend on trade with and aid from the West. The Straight Hero of Cameroon’s Gays |Jay Michaelson |December 10, 2014 |DAILY BEAST 

Followers had traveled many miles to mourn the loss, and aid in the ritual washing, dressing, and honoring of the body. Jail Threats for Sierra Leone Ebola Victims’ Families |Abby Haglage |December 10, 2014 |DAILY BEAST 

Groping to the chimney-place with the aid of his matches, Mr. Collingwood found the candle and lit it. The Boarded-Up House |Augusta Huiell Seaman 

England gladly seized the opportunity of injuring her enemy and sent aid to the people of Spain. Napoleon's Marshals |R. P. Dunn-Pattison 

But once Austria was disposed of, Prussia and Russia met their punishment for having given her secret or open aid. Napoleon's Marshals |R. P. Dunn-Pattison 

This province, having taken depositions in regard to it, with the aid of the said letter, adjudged Japon accordingly. The Philippine Islands, 1493-1898, Volume XX, 1621-1624 |Various 

Therefore they charge the governor with concealing it; and all that is without the aid of authority to make investigation. The Philippine Islands, 1493-1898, Volume XX, 1621-1624 |Various