Note, that these bins are not very large, but that being said at 29 by 25 by 13 inches could fit in most smaller bathrooms. Attractive laundry hampers that make your dirty clothes look a little better |PopSci Commerce Team |September 16, 2020 |Popular-Science 

You’d be right to be angry if you found a friend had set up secret cameras in their bathrooms, but they could do so even if it was sort of hard to explain why in court. Making Poly Less of a Pain in the Ass |Eugene Robinson |September 14, 2020 |Ozy 

It either got into the house from the ground floor, climbed the steps, made a beeline to the bathroom, and somehow climbed up the sheer tile wall to get to the window sill where it could startle my wife. Why last week’s great tech sell-off should make investors wary |Bernhard Warner |September 8, 2020 |Fortune 

They’d look handsome as hand towels in a bathroom, or hanging on the oven door in the kitchen. Soft, lightweight Turkish towels for bathrooms and beach trips |PopSci Commerce Team |September 4, 2020 |Popular-Science 

Bring the salon experience to your own bathroom with this professional-grade, cult-favorite scalp scrub. Scalp scrubs that banish scaly patches and build-up |PopSci Commerce Team |September 4, 2020 |Popular-Science 

I knew there would be good times and bad, sickness and health, broken dishwashers and giant cockroaches in the bathroom. You’re Never ‘Cured’ of an Eating Disorder |Carrie Arnold |December 20, 2014 |DAILY BEAST 

After cleaning up the copious amount of blood on my body in a bathroom, I found my clothing and got dressed. I Was Gang Raped at a UVA Frat 30 Years Ago, and No One Did Anything |Liz Seccuro |December 16, 2014 |DAILY BEAST 

There was one bathroom on the ship, and there were no showers or beds. Inside the Smuggling Networks Flooding Europe with Refugees |Barbie Latza Nadeau |December 15, 2014 |DAILY BEAST 

In his City of God, Augustine was quite clear that in the hereafter humans would no longer need bathroom breaks. Sorry, Internet: Pope Francis Didn't Open Paradise to Pets |Candida Moss |December 14, 2014 |DAILY BEAST 

He kept it in a brown paper bag stashed in the bathroom of his office. Alfred Hitchcock’s Fade to Black: The Great Director’s Final Days |David Freeman |December 13, 2014 |DAILY BEAST 

Evidently, the lady was somewhat concerned about her pale complexion, for there was a sun lamp in the bathroom. Hooded Detective, Volume III No. 2, January, 1942 |Various 

Behind one was the bathroom, so-called as a compliment to the presence of a wash-basin and running water. The Woman Gives |Owen Johnson 

There were to be four radiators downstairs and three upstairs, one in the bathroom, one in the hall, and one in a chamber. The Idyl of Twin Fires |Walter Prichard Eaton 

To-day I went almost wild with fury—I rushed into the bathroom and locked the door and flung myself upon the floor. Love's Pilgrimage |Upton Sinclair 

There never was any veiled figure,—you saw a man hunting around here, while you were hidden in the bathroom. In the Onyx Lobby |Carolyn Wells