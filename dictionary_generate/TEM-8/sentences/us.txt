Analysts interpreted it as an immediate ripple effect of the newly established US-Cuban détente. Venezuela Says Goodbye to Its Lil Friend, While the Rest of the Continent Cheers |Catalina Lobo-Guererro |December 20, 2014 |DAILY BEAST 

Frankly, I have never heard anyone else speak with such insight into Afghan affairs, post-US surge. Heart of Darkness: Into Afghanistan’s Taliban Valley |Matt Trevithick, Daniel Seckman |November 15, 2014 |DAILY BEAST 

This entire ordeal reeks of bureaucratic overreach being bandied about in the name of “let-us-save-the-children” politics. The University Of New Orleans’ Cigarette Ban Is Total BS |Chloé Valdary |October 21, 2014 |DAILY BEAST 

“The war being waged by the US/UK & co is a war against Islam & Muslims,” he wrote. Britain’s Counter-Terror Raids: The End of Londonistan? |Nico Hines |September 25, 2014 |DAILY BEAST 

Perfect for wildlife watching, hiking trails snake through canyons beside the swirling Rio Grande in the US-Mexico borderlands. The U.S. Road Trips You Should Really Take |Lonely Planet |April 26, 2014 |DAILY BEAST 

Let's us-all go see ef we can't find the place where our flag still floats.' Those Times And These |Irvin S. Cobb 

We hurried back for them, forgetting that we had promised ourselves a long just-us talk to bridge the months of separation. Paris Vistas |Helen Davenport Gibbons 

Amorous, am′or-us, adj. easily inspired with love: fondly in love (with of): relating to love. Chambers's Twentieth Century Dictionary (part 1 of 4: A-D) |Various 

Animus, an′im-us, n. intention: actuating spirit: prejudice against. Chambers's Twentieth Century Dictionary (part 1 of 4: A-D) |Various 

Arduous, rd′ū-us, adj. deep, difficult to climb: difficult to accomplish: laborious. Chambers's Twentieth Century Dictionary (part 1 of 4: A-D) |Various