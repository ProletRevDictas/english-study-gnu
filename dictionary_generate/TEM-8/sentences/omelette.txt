His Sunday-morning ritual was cutting them into little pieces and frying them crisp and then folding them into an omelette. Reichl’s Favorite Food Books |The Browser |August 11, 2011 |DAILY BEAST 

Fanchon is deeply interested in the bacon omelette as she watches it browning and sputtering over the fire. Child Life In Town And Country |Anatole France 

Fanchon sits on the settle, her chin on a level with the table, to eat the steaming omelette and drink the sparkling cider. Child Life In Town And Country |Anatole France 

Breakfasted on the banks of the Elbe (omelette aux confitures) and returned to Dresden by boat. The Life &amp; Letters of Peter Ilich Tchaikovsky |Modeste Tchaikovsky 

When some friend spoke to her of her rival's salon, she exclaimed, "Voil bien du bruit pour une omelette au lard." Private Letters of Edward Gibbon (1753-1794) Volume 1 (of 2) |Edward Gibbon 

She brings breakfast of coffee without milk and an omelette, but we always have our ration of bacon as well. Letters of Lt.-Col. George Brenton Laurie |George Brenton Laurie