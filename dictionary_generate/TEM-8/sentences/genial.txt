A genial but shrewd interviewer, Taberski paints a sometimes inspiring, often critical portrait of a society redefining its identity in the wake of tragedy. The 10 Best Podcasts of 2021 |Eliana Dockterman |December 2, 2021 |Time 

For fans of the hit CBS sitcom How I Met Your Mother, Cobie Smulders will always be synonymous with Robin Scherbatsky, the genial journalist formerly known as “Robin Sparkles” and titular aunt of Ted Mosby’s kids. Cobie Smulders on Her Stunning Transformation Into Devious Ann Coulter |Marlow Stern |September 10, 2021 |The Daily Beast 

Video of the stop, recorded on multiple body cameras, shows a trooper and Lara having a genial conversation, with Lara agreeing to be searched. A former Marine was pulled over for following a truck too closely. Police took nearly $87,000 of his cash. |Matt Zapotosky |September 1, 2021 |Washington Post 

By this point, you should be imagining me as genial and easygoing, a fellow reader rather than a pontificating critic, someone who clearly knows a lot about books but also a guy who doesn’t make a big deal of it. The greatest prison escape ever? ‘The Confidence Men’ tells a sensational true story. |Michael Dirda |June 16, 2021 |Washington Post 

The genial gentleman at the register at first wasn’t sure what I was ordering, but after I pointed to its alluring picture on the light-up menu, and he confirmed that I, in fact, wanted “the new thing,” we were able to seal the deal. Burger King’s new spicy chicken sandwich is a worthy competitor, but it won’t dethrone Popeyes |Emily Heil |June 4, 2021 |Washington Post 

And finally, they meet Un (Randall Park), who appears to be nothing but genial and fun-loving—especially to Dave. Sony’s ‘The Interview’: A Glorious, Patriotic, and Katy Perry-Filled Mess |Marlow Stern |December 24, 2014 |DAILY BEAST 

All was jolly and genial between the king of late night and the pretender to the throne. Stephen Colbert’s Groveling ‘Late Show’ Debut |Tom Shales |April 23, 2014 |DAILY BEAST 

Some wandered in a genial trance wearing the faraway, slightly shell-shocked look of the recently colonically irrigated. My Week At An Austrian Fat Camp |Owen Matthews |October 27, 2013 |DAILY BEAST 

Hiddleston, a genial fellow, looks mighty dapper in a bespoke three-piece suit. Tom Hiddleston On His Rocker-Vampire in ‘Only Lovers Left Alive,’ ‘Thor 2,’ and ‘Avengers 2’ |Marlow Stern |September 7, 2013 |DAILY BEAST 

It was a culture nibbling on the genial jingoism of Norman Vincent Peale and being made somewhat uncomfortable by Adlai Stevenson. Why Elvis Presley Never Really Died |Larry Durstin |August 16, 2013 |DAILY BEAST 

Big Reginald took their lives at pool, and pocketed their half-crowns in an easy genial way, which almost made losing a pleasure. The Pit Town Coronet, Volume I (of 3) |Charles James Wills 

Society likes their genial companionship, and they are favourites with, and favoured alike by young and old. The World Before Them |Susanna Moodie 

Mr. Wadsworth spoke in his genial voice: Its a beautiful thing, daughters, to help a good man live a good life. Tessa Wadsworth's Discipline |Jennie M. Drinkwater 

Even the gossip of the vicar's wife, or the genial chat of the vicar himself, failed to interest her. The Pit Town Coronet, Volume II (of 3) |Charles James Wills 

Accustomed to the Scotch mists, this rain seemed a genial shower, and Sir James was enjoying it accordingly. Elster's Folly |Mrs. Henry Wood