It’s not something the group is determined to pursue but the idea has now advanced far enough to reportedly irk some of its allies. Politics Report: BIA Talks New Tax |Scott Lewis and Andrew Keatts |August 21, 2021 |Voice of San Diego 

In fact, Gen Zers are more likely to create their own product and brand to replace the brand that has irked them. CMO Summit Recap: How marketers are adjusting to the delayed phase-out of third-party cookies |Sara Jerde |July 27, 2021 |Digiday 

They are a beloved part of our lifestyle, but when untrained, they terrorize wildlife, chase bikers, irk passersby, and cause trouble on trails. I Was a Bad Dog Owner. Don’t Be Like Me. |Kate Siber |May 11, 2021 |Outside Online 

None, however, irks insiders in a presidential operation like the 100-Day contest. 100 Days, the Global Pandemic Edition |Philip Elliott |April 30, 2021 |Time 

Elrich irked smart-growth advocates in 2019 for questioning housing targets set by the Metropolitan Washington Council of Governments. After setting ambitious climate goals, a liberal Md. suburb struggles to take action |Rebecca Tan |April 12, 2021 |Washington Post 

But sometimes he veers into territory that could irk staunch patriots. Ron Paul Book’s Juiciest Red Meat |David A. Graham |July 2, 2011 |DAILY BEAST 

He was like a fly condemned to spend his life in the irk-some society of the spider. American Sketches |Charles Whibley 

The restrictions of the house began to irk her, and she was afraid of the garden. Poppy |Cynthia Stockley 

The school drew revenues from the mills on the Irk in the days when that stream ran in limpid purity into the Irwell. The Rivers of Great Britain; Rivers of the South and West Coasts |Various 

But even an influence as kindly as this gentle, indulgent old man's may irk. The Transgression of Andrew Vane |Guy Wetmore Carryl 

When he had apparently made all ready, he stooped down again and smoothed out a ruck, lest its discomfort should irk the dead. Murder Point |Coningsby Dawson