A Taliban spokesman explained their plans to Islamicize the country in their image, while Taliban military gains led a commander to boast, “When we arrive in Kabul, we will arrive as conquerors.” "My Future Is Now." An Afghan Woman from a Threatened Minority Wrestles with What Happens When the U.S. Withdraws |Knox Thames |June 28, 2021 |Time 

A tree standing alone in full sun in a clear-cut forest is not a triumphant conqueror, commanding all resources, but a solitary individual, vulnerable to blight and drought. A scientist’s career in communion with trees |Kate Brown |May 21, 2021 |Washington Post 

This point has typically been framed as a victory of our species, a vision in which we are the successful explorers or conquerors, but maybe it was the opposite. Much of What We Thought About Neanderthals Was Wrong. Here’s Why That Matters |Rebecca Wragg Sykes |October 27, 2020 |Time 

Each castle had its own strategic value for a would-be conqueror. Can You Reach The Summit First? |Zach Wissner-Gross |September 11, 2020 |FiveThirtyEight 

Then, in 2009, the BBC sent the Australian native to ride more than 2,200 miles from Spain to Italy for a documentary following in the footsteps of the conqueror Hannibal. 27 Epic Trips to Start Planning Now |The Editors |September 8, 2020 |Outside Online 

But even if the great conqueror lies elsewhere, the Kasta bones might well be those of his wife. Is This Alexander the Great’s Tomb—or His Wife’s? |James Romm |December 12, 2014 |DAILY BEAST 

The higher power has many names -- daylight, conqueror, monarch. Leaps of Faith: Wrestling With Uncertainty and Finding Grace Through the Music of Hiss Golden Messenger |Howard Wolfson |March 2, 2014 |DAILY BEAST 

Here's a long excerpt, with my emphasis: Would we name our children Warrior, Conqueror, Sword, or Holy War? Islamophobe With Militarist Name Attacks Muslims For Militarist Names |Ali Gharib |April 12, 2013 |DAILY BEAST 

William Rufus, son of William the Conqueror was gay, as was James I, his son Charles I and Richard I (Richard the Lionheart). Oral History: The Sex Lives of the Kings and Queens of England |Tom Sykes |July 10, 2012 |DAILY BEAST 

Aegon the Conqueror, who arrived in Westeros from ancient Valyria, used his three dragons to conquer the entire continent. Game of Thrones for Dummies |Jace Lacob |April 13, 2011 |DAILY BEAST 

He married Maud, daughter of William the conqueror, and is characterized as a mild and popular king. The Every Day Book of History and Chronology |Joel Munsell 

The chief ambition of the great conqueror and legislator was to be a good boatswain and a good ship's carpenter. The History of England from the Accession of James II. |Thomas Babington Macaulay 

If he was a conqueror, he doubtless cast his eyes on the fine country of Assyria. A Philosophical Dictionary, Volume 1 (of 10) |Franois-Marie Arouet (AKA Voltaire) 

A voice from heaven hails the invincible conqueror, and his soul in the form of a dove ascends to the skies. The Catacombs of Rome |William Henry Withrow 

Nothing was to be thought of but a frame for this—olive, bay, laurel, everything appropriate to the conqueror. The Daisy Chain |Charlotte Yonge