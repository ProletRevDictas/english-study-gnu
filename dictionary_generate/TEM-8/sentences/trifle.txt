Once you get past that, if you get past it, Stories do start to feel like what they’re intended to be–a well-made tech trifle. Ray-Ban Stories Smart Sunglasses Review: All-Seeing Eyes |Mike Epstein |October 5, 2021 |Popular-Science 

I know how to make her cassata cake and trifle, but the artichokes seemed so simple I never really dug in deep on this one. How to prep, cook and enjoy stuffed artichokes |Ann Maloney |March 11, 2021 |Washington Post 

If you read the obituaries of Richard Nixon, you would have thought Watergate a mere trifle. Joe Paterno’s Death Shouldn’t Turn Him Into Sandusky Case’s Martyr |Buzz Bissinger |January 22, 2012 |DAILY BEAST 

He also has hit clutch shots down the stretch (never mind that little trifle I wrote about pressure). The Vindication of LeBron James |Buzz Bissinger |June 1, 2011 |DAILY BEAST 

This list will also likely include petit fours, mini éclairs, trifle, and chocolate and lemon mousse. Royal Wedding Plans So Far for Prince William |Barbie Latza Nadeau, Jacqueline Williams |February 22, 2011 |DAILY BEAST 

It should be soft and light enough to spread easily over the top of the trifle in a not-too-thick layer. Ham, Green Bean Casserole, Easy Trifle |The Daily Beast |December 23, 2008 |DAILY BEAST 

His nose was hooked and rather large, his eyes were blue, bright as steel, and set a trifle wide. St. Martin's Summer |Rafael Sabatini 

The tiny frown reappeared between her eyes, lingered a trifle longer than before, and vanished. The Wave |Algernon Blackwood 

The lady in black, creeping behind them, looked a trifle paler and more jaded than usual. The Awakening and Selected Short Stories |Kate Chopin 

Everywhere cattle were being sold for a trifle, as there was no grass upon which they could feed. The Homesteader |Oscar Micheaux 

Edna was a trifle embarrassed at being thus signaled out for the imperious little woman's favor. The Awakening and Selected Short Stories |Kate Chopin