Some are afraid their homes are at risk if a fire escapes its bounds, or that the smoke will harm their health. How we can burn our way to a better future |Ula Chrobak |October 2, 2020 |Popular-Science 

What’s more, our awareness of exactly what’s lurking out there has advanced by leaps and bounds—especially when it comes to the largest, most dangerous objects in our neighborhood. The World’s Space Agencies Are on a Quest to Deflect a (Harmless) Asteroid |Jason Dorrier |September 27, 2020 |Singularity Hub 

This means all the energy available to support life was historically bounded by the supply provided by photosynthetic organisms. Dawn of the Heliocene - Issue 90: Something Green |Summer Praetorius |September 16, 2020 |Nautilus 

Though Deep Blue was meticulously programmed top-to-bottom to play chess, the approach was too labor-intensive, too dependent on clear rules and bounded possibilities to succeed at more complex games, let alone in the real world. DeepMind’s Newest AI Programs Itself to Make All the Right Decisions |Jason Dorrier |July 26, 2020 |Singularity Hub 

The neighborhood is bounded by University Boulevard, Cheery Creek, Downing Street, and 8th street. Denver: Where the Queer Community is a Mile High |LGBTQ-Editor |July 14, 2020 |No Straight News 

That act forever sealed his feeling for the Chief, bound it up with the war, with violence, with the gun. The Story Behind Lee Marvin’s Liberty Valance Smile |Robert Ward |January 3, 2015 |DAILY BEAST 

They were born in 51 countries and speak 59 foreign languages, but they seemed bound by a single purpose and resolve. Cop Families Boo De Blasio at NYPD Graduation |Michael Daly |December 30, 2014 |DAILY BEAST 

Bound together by mutual distrust, both sides end up lashing themselves to the mast of rigid law. Red Tape Is Strangling Good Samaritans |Philip K. Howard |December 27, 2014 |DAILY BEAST 

From 2012 to 2013, 31 men left Aarhus bound for combat in Syria. What the U.S. Can Learn from Europe About Dealing with Terrorists |Scott Beauchamp |December 15, 2014 |DAILY BEAST 

That is bound to put a dent in public confidence in the police. Idris Elba on Eric Garner, ‘Mi Mandela,’ and Selling Weed to Dave Chappelle |Marlow Stern |December 6, 2014 |DAILY BEAST 

On the thirteenth of the same month they bound to the stake, in order to burn alive, a man who had two religious in his house. The Philippine Islands, 1493-1898, Volume XX, 1621-1624 |Various 

A small book, bound in full purple calf, lay half hidden in a nest of fine tissue paper on the dressing-table. Hilda Lessways |Arnold Bennett 

A good many children seem to be like savages in distinguishing those to whom one is bound to speak the truth. Children's Ways |James Sully 

These officers are bound to maintayne themselves and families with food and rayment by their owne and their servant's industrie. Tobacco; Its History, Varieties, Culture, Manufacture and Commerce |E. R. Billings. 

A flock of weary sheep pattered along the road, barnward bound, heavy eyed and bleating softly. The Soldier of the Valley |Nelson Lloyd