Wade netted nearly $200 million in earnings during his playing career in addition to his off-court sponsorship agreements. Dwyane Wade purchases ownership stake in Utah Jazz |Ben Golliver |April 16, 2021 |Washington Post 

There are some purists who are going to claim that Wade just made up a word, that there’s no such thing as “un-turnover-able.” What Each WNBA Team Needs From This Year’s Draft |Howard Megdal |April 14, 2021 |FiveThirtyEight 

Wade likes Moleskine’s products for classic and bullet journaling. What We Use to Unplug and Reset |The Editors |February 22, 2021 |Outside Online 

The doctors were subsequently convicted of illegal distribution of opioids, with Wade sentenced to 10 years in prison and Diamond to 20 years. The Justice Department Sues Walmart, Accusing It of Illegally Dispensing Opioids |by Jesse Eisinger and James Bandler |December 23, 2020 |ProPublica 

The Big Three in Miami was a terror, but it’s easy to forget how out of sync James and Wade were offensively early on,15 as Bosh took time to settle into a tertiary role after being a No. LeBron And AD Are The Heroes. But The Sum Of This Lakers Club Was More Than Its Superstar Parts. |Chris Herring (chris.herring@fivethirtyeight.com) |October 12, 2020 |FiveThirtyEight 

Media outlets “crowd sourced” the project, asking readers to help wade through the 24,000 pages of Palin emails. The IRS Email Double Standard |Matt Lewis |November 25, 2014 |DAILY BEAST 

There are no emails for us to wade through—even if we were champing at the bits. The IRS Email Double Standard |Matt Lewis |November 25, 2014 |DAILY BEAST 

They were busily implementing these in cases like Roe v. Wade when a right-wing insurgency took them by surprise. A Reminder: Our Justices are Politicians in Robes |Jedediah Purdy |November 13, 2014 |DAILY BEAST 

To be sure, a more activist Supreme Court could still have decided to wade into the waters and decide this issue once and for all. Supreme Court to Gay-Marriage Foes: Get Lost |Jay Michaelson |October 6, 2014 |DAILY BEAST 

In the eloquent words of colonial preacher John Winthrop, “When a man is to wade through deep water, there is required tallness.” For Short Men in 2014, The News Is Surprisingly Good |Kevin Bleyer |September 13, 2014 |DAILY BEAST 

We had now at one moment to wade through plains of sand, and the next to clamber over the rocks by wretched paths. A Woman's Journey Round the World |Ida Pfeiffer 

"And my geldin' kin travel that same road spryer 'n Green's hoss—for a hunderd dollars," said Wade, eagerly. Scattergood Baines |Clarence Budington Kelland 

Streams which a boy could wade last March would now give an elephant a tussle. Gold-Seeking on the Dalton Trail |Arthur R. Thompson 

She was obliged to wade through, but escaped a serious wetting by walking on her heels. Gold-Seeking on the Dalton Trail |Arthur R. Thompson 

They were able to wade out unto the islet & thereon hid they themselves among the reeds. The Sagas of Olaf Tryggvason and of Harald The Tyrant (Harald Haardraade) |Snorri Sturluson