Just a few years ago, improperly stored smallpox vials were found in a lab in the US. Smallpox used to kill millions of people every year. Here’s how humans beat it. |Kelsey Piper |February 5, 2021 |Vox 

One potential challenge, though, is that if every vial is assumed to have six doses, then Pfizer can complete their contract with the US government by shipping fewer vials. There’s a new COVID-19 vaccine in the running—but variants could pose a problem |Sara Chodosh |January 28, 2021 |Popular-Science 

The department had free vaccine doses but needed help getting them out of vials and into arms. Meet the book club that’s helping to quickly vaccinate its town |Nora Krug |January 26, 2021 |Washington Post 

It’s hard to track how many vials of vaccines are swimming around San Diego County. Frequently Asked (Yet Largely Unanswerable) Questions on San Diego’s Vaccine Situation |MacKenzie Elmer |January 18, 2021 |Voice of San Diego 

Starting with the second shipment, BioNTech will send vaccine materials to Fosun to “fill and finish,” meaning Fosun will fill the vials and package the vaccines for distribution. Pfizer and BioNTech’s overlooked third partner will distribute the vaccine in China—and it’s facing some resistance |Grady McGregor |January 9, 2021 |Fortune 

Vial distinguishes between two types of images—the innocent and the showoff, in which the performers “play” for her. A Backstage Love Affair With Cirque du Soleil |Allison McNearney |December 1, 2014 |DAILY BEAST 

But no matter where her career has taken her, or how big the Cirque has become, Vial keeps coming back. A Backstage Love Affair With Cirque du Soleil |Allison McNearney |December 1, 2014 |DAILY BEAST 

Vial, on the other hand, has become a well-known photographer whose clients include celebrities and major advertisers. A Backstage Love Affair With Cirque du Soleil |Allison McNearney |December 1, 2014 |DAILY BEAST 

Twenty-eight years ago, Veronique Vial was asked to photograph Cirque du Soleil. A Backstage Love Affair With Cirque du Soleil |Allison McNearney |December 1, 2014 |DAILY BEAST 

When Vial got that first assignment, she was just beginning her photography career, and Cirque du Soleil was only a few years old. A Backstage Love Affair With Cirque du Soleil |Allison McNearney |December 1, 2014 |DAILY BEAST 

Meanwhile, the cardinal had passed behind the altar to put on his pontifical robes; soon he reappeared with the holy vial. Chicot the Jester |Alexandre Dumas, Pere 

But Iftikhar drew from his bosom a crystal vial, in which glowed a liquor red as vermilion. God Wills It! |William Stearns Davis 

But Iftikhar first knelt by Morgiana's side, drew forth the little red vial, and laid the magic, fiery drops upon her tongue. God Wills It! |William Stearns Davis 

You must uncork that vial and fling the contents into his face. Frank Merriwell's Pursuit |Burt L. Standish 

He filled the hypodermic from a little vial that glittered in the light of the lamp. Hunters Out of Space |Joseph Everidge Kelleam