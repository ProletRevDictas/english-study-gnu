Cane burning also produced an estimated 89% of the county’s carbonyls, a category of gaseous pollutants. “A Complete Failure of the State”: Authorities Didn’t Heed Researchers’ Calls to Study Health Effects of Burning Sugar Cane |by Lulu Ramadan, The Palm Beach Post |August 19, 2021 |ProPublica 

In the past, astronomers believed Saturn had a rocky core hidden at the center of its mostly gaseous mass. Saturn has a slushy core and rings that wiggle |Sara Chodosh |August 17, 2021 |Popular-Science 

The result is a material that, according to its creators, is almost as bad at conducting heat as gaseous air. The world’s worst conductor could be a game changer in the climate crisis |Claire Maldarelli |July 23, 2021 |Popular-Science 

A high-pressure onboard tank stores the hydrogen in gaseous form and dispenses it to the vehicle’s fuel cell. Land Rover’s next Defender will run on hydrogen |Rob Verger |June 19, 2021 |Popular-Science 

Plants have caused a dramatic change in the composition of the atmosphere by releasing gaseous oxygen as a side effect of converting the sun’s light into chemical energy via photosynthesis. Our Little Life Is Rounded with Possibility - Issue 102: Hidden Truths |Chiara Marletto |June 9, 2021 |Nautilus 

Was Obama so gaseous in the classroom when he taught constitutional law? Obama’s War on Journalism: ‘An Unconstitutional Act’ |Nick Gillespie |May 22, 2013 |DAILY BEAST 

And let's put that Gaseous Diffusion Plant in...well, on second thought, keep it, Kentucky. No Cash for You, Kentucky |Paul Begala |February 15, 2011 |DAILY BEAST 

As it expands into the gaseous state it absorbs heat, and a temperature of -20 C. has thus been produced. Yachting Vol. 2 |Various. 

We may ask, whether this theory cannot be used to explain the connection between osmotic and gaseous pressure. The Elements of Qualitative Chemical Analysis, vol. 1, parts 1 and 2. |Julius Stieglitz 

The constants for gaseous elements represent the constants of the gases under atmospheric pressure. The Elements of Qualitative Chemical Analysis, vol. 1, parts 1 and 2. |Julius Stieglitz 

The potentials given for the gaseous elements represent the potentials of the gases under 760 mm. The Elements of Qualitative Chemical Analysis, vol. 1, parts 1 and 2. |Julius Stieglitz 

The error was, non-observation of an important portion of the actual residue, namely, the gaseous products of combustion. A System of Logic: Ratiocinative and Inductive |John Stuart Mill