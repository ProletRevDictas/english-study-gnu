It seemed every time Mahomes tried to work his magic, eluding defenders, making his signature sidearm throws on the run, KC receivers would drop the ball. The Pandemic Super Bowl, Naturally, Was Rough To Watch |Sean Gregory |February 8, 2021 |Time 

One mutation, called E484K, which emerged independently in the variants seen in South Africa and Brazil, has shown signs of eluding antibodies produced either through the natural immune system or therapeutic drugs. Coronavirus mutations add urgency to vaccination effort as experts warn of long battle ahead |Joel Achenbach, Ariana Eunjung Cha |January 30, 2021 |Washington Post 

Another danger can arise if a mutation helps the virus elude the body’s immune response. How coronavirus variants may pose challenges for COVID-19 vaccines |Erin Garcia de Jesus |January 27, 2021 |Science News 

Despite their best efforts, translating this prowess into a digital, artificial computer “nose” has eluded scientists. Scientists Made a Biohybrid Nose Using Cells From Mosquitoes |Shelly Fan |January 26, 2021 |Singularity Hub 

Personal excellence continues to elude me, but I have voluntarily subjected myself to many torturous workouts over the years, in the irrational belief that getting marginally faster will provide some measure of redemption for my myriad failures. Tracksmith Made Running Culture Something You Can Buy |Martin Fritz Huber |January 11, 2021 |Outside Online 

Elsewhere, she tells her inamorata, “It does not matter if you elude my arms/my dear, when thought alone can imprison you.” Sor Juana: Mexico’s Most Erotic Poet and Its Most Dangerous Nun |Katie Baker |November 8, 2014 |DAILY BEAST 

But success stories elude many other former football players. College Football Fattens Players Up and Then Abandons Them |Evin Demirel |October 4, 2014 |DAILY BEAST 

Beautiful, daring and smart, Sophie managed to elude arrest on many occasions. Meet 'The Queen of Thieves' Marm Mandelbaum, New York City's First Mob Boss |J. North Conway |September 7, 2014 |DAILY BEAST 

But Shailene Woodley, both onscreen and off, seems to elude quick characterization. Shailene Woodley Is Hollywood’s It Girl Next Door |Teo Bugbee |June 5, 2014 |DAILY BEAST 

They would have assumed that they needed to get far out over an ocean to elude that. Malaysia’s Sinister Timeline for Flight 370 Unravels |Clive Irving |March 18, 2014 |DAILY BEAST 

For a second Marius considered whether he might not attempt to elude Garnache by a wild and sudden dash towards his men. St. Martin's Summer |Rafael Sabatini 

There seems to be one oath of this description which bids fair to elude all guess-work as to its origin or meaning. A Cursory History of Swearing |Julian Sharman 

If only she could reach the corridor above with its intricate windings, she could elude pursuit in some dark corner. They Looked and Loved |Mrs. Alex McVeigh Miller 

The hounds, who well knew where the ocelot had gone to, were chasing it from tree to tree; but still it continued to elude them. In the Wilds of Florida |W.H.G. Kingston 

The next afternoon Jenny, managing to elude the watchful eyes of her mother and governess, came over to the poor-house. The English Orphans |Mary Jane Holmes