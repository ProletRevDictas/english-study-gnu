Agency executives said that they would be relying on NBCU to evaluate its own inventory’s contribution to an advertiser’s sales. NBCUniversal tests new measurement program to prove it can push product sales for advertisers |Tim Peterson |September 17, 2020 |Digiday 

By the end of March, it had announced a new editorial series, Business Insider Spotlight, which featured reporters interviewing prominent executives about major developments in their respective industries. ‘Eager to explore more’: Business Insider’s virtual events strategy lifts volume and plants evergreen on-demand feature |Max Willens |September 16, 2020 |Digiday 

Jake Hoffman, president and chief executive of the Phoenix-based digital marketing firm, confirmed the online workers were classified as contractors but declined to comment further on “private employment matters.” Pro-Trump youth group enlists teens in secretive campaign likened to a ‘troll farm,’ prompting rebuke by Facebook and Twitter |Isaac Stanley-Becker |September 15, 2020 |Washington Post 

Beyond Quinn, it’s not clear whether any executive branch officials have participated in von Spakovsky’s remote briefings. No Democrats Allowed: A Conservative Lawyer Holds Secret Voter Fraud Meetings With State Election Officials |by Mike Spies, Jake Pearson and Jessica Huseman |September 15, 2020 |ProPublica 

We had all the free content, and then we had these very expensive, $15,000 executive conferences, and we had nothing in between. Fortune Connect is bringing its conference business to a larger audience, with a higher price tag |Kayleigh Barber |September 15, 2020 |Digiday 

“Having been a legislator and a mayor, I particularly enjoy being a chief executive,” he said. The Golden State Preps for the ‘Red Wedding’ of Senate Races |David Freedlander |January 9, 2015 |DAILY BEAST 

Reached for comment, one high-level industry executive refused to say a word. Pyongyang Shuffle: Hollywood In Dead Panic Over Sony Hack |James Poulos |December 19, 2014 |DAILY BEAST 

Colfer adapted the later into a 2012 film, which he also executive produced and starred in. Chris Colfer on Writing, Acting, and the Pain of Being A Pop Culture Trailblazer |Oliver Jones |December 15, 2014 |DAILY BEAST 

All those bloodthirsty tweets and arcane exhortations and now we find out you were an advertising executive—an ad exec! The Scared Widdle Kitty of ISIS |Jacob Siegel |December 12, 2014 |DAILY BEAST 

After the show, Executive Chef Michael Franey explained the process by which the theater selects its menu. Dinner at Nitehawk Cinema: ‘Christmas Vacation’ and a Beer in a Pear Tree |Rich Goldstein |December 12, 2014 |DAILY BEAST 

To Harrison and his wife there was no distinction between the executive and judicial branches of the law. The Bondboy |George W. (George Washington) Ogden 

Polavieja, as everybody knew, was the chosen executive of the friars, whose only care was to secure their own position. The Philippine Islands |John Foreman 

Its resolution will be put into practice with all fidelity by the executive power in its character of responsible government. The Philippine Islands |John Foreman 

Up to that date the civil executive authority in the organized provinces was vested in the military governor. The Philippine Islands |John Foreman 

But he was a man of marked executive ability, and when occasion demanded he wielded a facile and ready pen. The Courier of the Ozarks |Byron A. Dunn