From person to person, immune responses to an infection develop along a spectrum, with some people mounting robust, protective responses and others being left with weaker responses. Another COVID-19 reinfection: This time second infection was more severe |Beth Mole |August 28, 2020 |Ars Technica 

Vaccines, however, might trigger a more robust immune response. A Hong Kong man got the new coronavirus twice |Erin Garcia de Jesus |August 26, 2020 |Science News For Students 

Pirofski says it’s easier to feel confident about the answers to these questions when there are robust clinical trials with placebo controls. COVID-19 plasma treatments may be safe, but we don’t know if they work |Tina Hesman Saey |August 25, 2020 |Science News 

Vaccines, however, might trigger a more robust immune response and help protect populations by providing herd immunity. A man in Hong Kong is the first confirmed case of coronavirus reinfection |Erin Garcia de Jesus |August 24, 2020 |Science News 

Rebekah Kondrat, founder of consultancy Kondrat Retail said that in her experience, while all retailers have some form of de-escalation training, DTC startups opening stores for the first time often don’t have robust enough de-escalation training. By being too customer-obsessed, DTC startups are failing their retail employees |Anna Hensel |August 21, 2020 |Digiday 

Robust work in this area is coming from Jeffrey Gordon lab at Washington University in St. Louis. ‘Good Poop’ Diet Is the Next Big Thing |Daniela Drake |October 7, 2014 |DAILY BEAST 

But the malpractice system is not robust in China, and patients feel powerless. Will US Health Care Follow in China’s Bloody Footsteps? |Daniela Drake |September 21, 2014 |DAILY BEAST 

Where Simmons is jocular in a kind of clever fratboy way, Lund is more refined in his language and more robust in his indignation. Forget the Wife Beating—Are You Ready for Some Football? |Steve Almond |September 11, 2014 |DAILY BEAST 

Her normally flagging energy at that hour was surprisingly robust. Gaza, You're No Good For My Marriage |Josh Robin |August 9, 2014 |DAILY BEAST 

To this end, a robust Special Operations Forces presence beyond simply a modest advisory effort should be sent to Iraq. Lessons From Fallujah, Then and Now |Dr. Daniel R. Green |July 26, 2014 |DAILY BEAST 

The Englishman of the days of road-travel was a much more robust person than the Englishman of railway times. The Portsmouth Road and Its Tributaries |Charles G. Harper 

Jet black ringlets—then in vogue—clustered round an exceedingly fair face, on which there dwelt the hue of robust health. The Floating Light of the Goodwin Sands |R.M. Ballantyne 

At present the patient is a robust, healthy-looking woman, of fair intelligence and good spirits. A Statistical Inquiry Into the Nature and Treatment of Epilepsy |Alexander Hughes Bennett 

His strength is robust, so that he does his ordinary work as a pianoforte maker. A Statistical Inquiry Into the Nature and Treatment of Epilepsy |Alexander Hughes Bennett 

She is robust and active, and takes her full share in domestic work. A Statistical Inquiry Into the Nature and Treatment of Epilepsy |Alexander Hughes Bennett