“Follow the pills and you’ll find the overdose deaths,” Eric Eyre’s two-part investigation begins. These local newspapers say Facebook and Google are killing them. Now they’re fighting back. |Margaret Sullivan |February 4, 2021 |Washington Post 

The preliminary data found that more than 81,000 people died of an overdose in the 12-month period that ended this May—the largest number ever recorded. The pandemic has had a dramatic effect on drug overdoses |Kat Eschner |December 22, 2020 |Popular-Science 

Before lawmakers’ questions, the panel aired video remarks by parents whose children died of overdoses. Members of family that led maker of OxyContin deny responsibility for opioid crisis in congressional hearing |Meryl Kornfield |December 17, 2020 |Washington Post 

A 2018 study published in the journal Science found that overdose death rates have increased exponentially and “along a remarkably smooth trajectory” in the past 40 years. The war on drugs didn’t work. Oregon’s plan might. |Kat Eschner |December 17, 2020 |Popular-Science 

For some, the virtual meetings are a lifeline that has helped them maintain their sobriety during a time when mental health issues, alcohol consumption and overdoses are on the rise. What was lost when covid forced addiction support groups online — and what was gained |Allyson Chiu |November 23, 2020 |Washington Post 

That star would all too famously implode with her tragic death from a heroin overdose at a mere 27 years of age. Janis Joplin’s Kozmic Blues |William O’Connor |November 8, 2014 |DAILY BEAST 

In 1978, Scott Newman, his 28-year-old son, died of an accidental drug and alcohol overdose. The Stacks: The Eyes of Winter: Paul Newman at 70 |Peter Richmond |October 11, 2014 |DAILY BEAST 

Heroin overdose deaths from 2010 to 2012 increased in every single subgroup examined in the CDC report. Heroin Overdoses Double in Two Years |Brandy Zadrozny |October 2, 2014 |DAILY BEAST 

Deaths from both make up 57 percent of total drug overdose deaths. Heroin Overdoses Double in Two Years |Brandy Zadrozny |October 2, 2014 |DAILY BEAST 

Those suffering from addiction in the streets risk disease, overdose, and death. World Leaders' Proposal for Winning the War on Drugs: Legalize It! |Abby Haglage |September 9, 2014 |DAILY BEAST 

Sickness,—not her own, but that of dear ones, and an overdose of wealth. The Opened Shutters |Clara Louise Burnham 

Verdict was "That the deceased died from inadvertently taking an overdose of opium." The International Monthly, Volume 5, No. 3, March, 1852 |Various 

An overdose of "cold step" may, indeed, partially account for the bronchitis which riddles the ranks of the children of the poor. Highways and Byways in London |Mrs. E. T. Cook. 

(b) Failure of respiration from an overdose of chloroform or other ansthetic. A System of Operative Surgery, Volume IV (of 4) |Various 

Too much freedom at one time is as bad as an overdose of anything else. Letty and the Twins |Helen Sherman Griffith