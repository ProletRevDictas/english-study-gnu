Unproductive frivolity—joy, pleasure—might need no evolutionary explanation. The Whale Who Will Come Soon - Issue 105: Whale Songs |Rebecca Giggs |September 8, 2021 |Nautilus 

It’s not insignificant that “The Bold Type” took seriously the fantasies of teenage girls and young women, groups historically derided for the supposed frivolity of their wants. How ‘The Bold Type’ went from an irresistible feminist fantasy to an out-of-touch disappointment |Inkoo Kang |July 2, 2021 |Washington Post 

This is not lost on their commander, Rama (Shani Klein), an aspiring military careerist who looks down on frivolity in wartime. ‘Zero Motivation’: the Funny Side of the IDF |Melissa Leon |December 8, 2014 |DAILY BEAST 

When Ben Stiller showed up in full blue Navi makeup in 2010 to mock Avatar, the winking frivolity of it all was hysterical. Breaking: The Oscars Might Not Suck This Year |Kevin Fallon |February 27, 2014 |DAILY BEAST 

No putdowns, no jokes, no frivolity whatever—he was most solemn and his eyes focused somewhere far beyond the back of my head. What It Was Like to Watch the Beatles Become the Beatles—Nik Cohn Remembers |Nik Cohn |February 9, 2014 |DAILY BEAST 

Brown Dog has a code of ethics that separates him from the convention of his culture, and the partisan frivolity of politics. The Legend of Brown Dog: A Great American Hero Gets His Due |David Masciotra |December 7, 2013 |DAILY BEAST 

States should rarely amend their constitutions, and such frivolity is certainly not worthy, especially from conservatives. The Right to Hunt |Justin Green |December 21, 2012 |DAILY BEAST 

All this I admit to be the fever of the mind—a waking dream—an illusion to which mesmerism or magic is but a frivolity. Blackwood's Edinburgh Magazine, No. CCCXXXIX. January, 1844. Vol. LV. |Various 

Disgusted with the frivolity of the living, she sought solace for her wounded feelings in companionship with the illustrious dead. Madame Roland, Makers of History |John S. C. Abbott 

Frivolity enveloped the company as with a silken veil, and yet everything moved as politely and as sedately as a minuet. Skipper Worse |Alexander Lange Kielland 

I am a parent, so I instructed my wife to write a letter saying how much I was pained by William's frivolity. Punch, or the London Charivari, Vol. 147, November 4, 1914 |Various 

Hence he gives the impression of insincerity, of trifling with grave subjects and of using mysticism as a mask for frivolity. Encyclopaedia Britannica, 11th Edition, Volume 5, Slice 2 |Various