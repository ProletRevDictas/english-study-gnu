If a passenger is indicated, they will also be required to take a selfie. Electric moped startup Revel returns to New York with helmet selfie, other in-app safety features |Kirsten Korosec |August 27, 2020 |TechCrunch 

Pyrenean ibexes, dodo birds, passenger pigeons, and Tasmanian tigers are just a few of the many species of animals that humans have over hunted and wiped out. Climate change probably contributed to the woolly rhino’s rapid demise |Sara Kiley Watson |August 25, 2020 |Popular-Science 

It’s the same with ships out at sea with passengers who’ve tested positive for the virus. Is Maritime Piracy Back from the Dead? |Eromo Egbejule |August 25, 2020 |Ozy 

That is particularly worrying in the case of airliners, in which many passengers are confined in a small space. American Airlines touts a new tool to combat COVID. But does it really make flying safer? |dzanemorris |August 24, 2020 |Fortune 

In its final form, Starship is expected to be about 400 feet tall and 30 feet wide, able to take more than 100 tons worth of cargo and passengers to deep space destinations like the moon and Mars. SpaceX flew a prototype of its Starship vehicle for the first time |Neel Patel |August 5, 2020 |MIT Technology Review 

A click sends a user to a statement, a list of passenger nationalities, emergency call-center numbers, and other information. The Presumed Crash of AirAsia Flight QZ8501 Is Nothing Like MH370 |Lennox Samuels |December 29, 2014 |DAILY BEAST 

The Italian navy tweeted regular updates of the saved-to-stranded passenger ratio. ‘We’re Going to Die’: Survivors Recount Greek Ferry Fire Horror |Barbie Latza Nadeau |December 29, 2014 |DAILY BEAST 

Alexander and Adorno were doing what they could to save the officer on the passenger side, Liu. 'Please Don't Die!': The Frantic Battle to Save Murdered Cops |Michael Daly |December 22, 2014 |DAILY BEAST 

Brinsley stepped up to the passenger side of the patrol car, raised a silver Taurus semi-automatic pistol and began firing. 'Please Don't Die!': The Frantic Battle to Save Murdered Cops |Michael Daly |December 22, 2014 |DAILY BEAST 

Terrorists against Cuba who had once shot down passenger jets later found safe haven in Miami. The Life and Hard Times Of The Family A Cuban Defector Left Behind |Brin-Jonathan Butler |December 19, 2014 |DAILY BEAST 

We were about nine hours of fair daylight traversing 160 miles of level or descending grade, with a light passenger train. Glances at Europe |Horace Greeley 

There were only seventeen stations on the whole line, over which the first passenger train ran on Sept. 17. Showell's Dictionary of Birmingham |Thomas T. Harman and Walter Showell 

Neutral passenger-steamers were allowed to take away refugees other than Spanish subjects. The Philippine Islands |John Foreman 

It possesses, however, one advantage; it warns the foot passenger, and affords him time to get out of the way. A Woman's Journey Round the World |Ida Pfeiffer 

A passenger who stands on a platform or on the steps of a street car, when there is room inside, assumes all the risks himself. Putnam's Handy Law Book for the Layman |Albert Sidney Bolles