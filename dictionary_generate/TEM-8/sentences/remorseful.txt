I am deeply remorseful and I’m trying to be remorseful without being ashamed of myself. 'My Behavior Was Unacceptable.' Will Smith Apologizes to Chris Rock in a New Video |Anisha Kohli |July 29, 2022 |Time 

He is remorseful, and he does feel compelled now to do what he can to help his victims. Why Trust What Bernie Madoff Says About JP Morgan Now? |Burt Ross |February 21, 2014 |DAILY BEAST 

Has he publicly stated that he is remorseful about his crimes? My Professor, the Killer: Why Dr. James St. James Should Stay |Joelle Charbonneau |August 8, 2013 |DAILY BEAST 

Now he claims he is remorseful and wants to help young people avoid repeating his mistakes. Utah Man Awaits Firing Squad |The Daily Beast |June 17, 2010 |DAILY BEAST 

But the remorseful young men who are willing to appear in front of the Western cameras are not the whole story. Rehab the Terrorists...With Love |Robert Lacey |May 21, 2009 |DAILY BEAST 

Shame for her unjust accusations, and remorseful gratitude pierced Ida's bosom. Alone |Marion Harland 

I felt as remorseful as if every tear he was hiding was a drop of blood. Ernest Linwood |Caroline Lee Hentz 

If he was remorseful, so was she; she never wanted to see his sanctimonious face again. Robert Annys: Poor Priest |Annie Nathan Meyer 

But the captain's eyes were dull, and he walked his cabin, sunk in a gloomy, remorseful trance. Derelict |Alan Edward Nourse 

The quarryman proceeded on his way, and Pierston, deeply remorseful, knocked at the door of the minute freehold. The Well-Beloved |Thomas Hardy