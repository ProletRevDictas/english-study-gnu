Under that new system, teams with the misfortune of ranking outside the top four seeds in their conference were forced to play an extra round to even qualify for the regular 16-team bracket. Can The Pittsburgh Penguins Save Their Season? |Neil Paine (neil.paine@fivethirtyeight.com) |February 11, 2021 |FiveThirtyEight 

If the bracket were seeded today based on the BracketMatrix aggregate, Loyola would be the second-best double-digit seed of the past 20 seasons, trailing only — of course — Wichita State. Remember Loyola? This Team Is Even Better Than The One That Made The Final Four. |Jake Lourim |February 10, 2021 |FiveThirtyEight 

When companies signed the Presidency Reemployment Agreement in 1933, they agreed to wage brackets and price caps. One solution to pandemic unemployment? Working less |Samanth Subramanian |February 5, 2021 |Quartz 

The engine had been removed, “but its mounting brackets, as well as the fuel and oxidizer tanks, were still in place,” recalled Finer. Lunik: Inside the CIA’s audacious plot to steal a Soviet satellite |Bobbie Johnson |January 28, 2021 |MIT Technology Review 

Using the seedings from that consensus bracket, we can simulate the outcome by estimating the number of possessions for each team and, using the team’s adjusted offensive rating found at Ken Pomeroy’s site, project a scoring margin for each matchup. Gonzaga has a good chance to go undefeated and win the national championship |Neil Greenberg |January 15, 2021 |Washington Post 

Even at the age bracket where men and women appear closest in frequency, there is nothing remotely close to masturbation parity. C’mon, Ladies, Masturbation Isn’t Just for Bad Girls |Emily Shire |June 19, 2014 |DAILY BEAST 

Do you fill out one bracket or different brackets for each pool? ESPN’s Bracket Champion Shares His March Madness Secrets |Ben Teitelbaum |March 18, 2014 |DAILY BEAST 

I just go with that same bracket and I usually enter that into multiple pools. ESPN’s Bracket Champion Shares His March Madness Secrets |Ben Teitelbaum |March 18, 2014 |DAILY BEAST 

Are you cool with your bracket getting ruined if UCLA wins the national title? ESPN’s Bracket Champion Shares His March Madness Secrets |Ben Teitelbaum |March 18, 2014 |DAILY BEAST 

People ask me this: Would you trade UCLA winning the national championship for not winning the bracket challenge? ESPN’s Bracket Champion Shares His March Madness Secrets |Ben Teitelbaum |March 18, 2014 |DAILY BEAST 

A lovely bracket of carved wood fixed to the wall held a cheap cuckoo-clock from Switzerland. Bella Donna |Robert Hichens 

Rather hard on politicians this, to bracket their patriotic endeavours with pitch-and-toss and alcoholic indulgence! Punch, or the London Charivari, Vol. 109, July 27, 1895 |Various 

Beside the font is a very quaint iron bracket-stand, painted blue and gold, "constructed to carry" two candles. Notes and Queries, Number 196, July 30, 1853 |Various 

And please do not think when I thus seem to bracket myself with you, that I am wholly blinded with vanity. The Works of Robert Louis Stevenson - Swanston Edition Vol. 25 (of 25) |Robert Louis Stevenson 

He turned into a side street, at the corner of which was a broken lamp bracket used for hanging a man not a week ago. The Light That Lures |Percy Brebner