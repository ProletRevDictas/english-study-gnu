Against this backdrop, Duterte and his henchmen have moved against longstanding political enemies. Rodrigo Duterte Is Using One of the World's Longest COVID-19 Lockdowns to Strengthen His Grip on the Philippines |Aie Balagtas See / Manila |March 15, 2021 |Time 

He’s seen as a corporate henchman and traitor to progressive causes. Why Larry Summers Still Triggers Washington. (It Isn’t His Economics.) |Philip Elliott |February 8, 2021 |Time 

Mr. Duffy, it happened, was Big Bill Duffy, a jolly henchman of Owney Madden, the racketeer. Portrait of the Consummate Con Man |John Lardner |May 17, 2014 |DAILY BEAST 

That has the Journal newsroom worrying they could end up working for a controversial Rupert henchman. Succession Drama at WSJ |Nick Summers |July 16, 2011 |DAILY BEAST 

En route, Tron encounters the MCP's henchman, Sark, and the two battle it out cyberstyle. Catch Up on Tron: Watch 6 Key Moments |Alex Berg |December 16, 2010 |DAILY BEAST 

“He was always in the political equation,” Nixon henchman Chuck Colson once told me. How Kennedy Brought Down Nixon |Chris Matthews |September 13, 2009 |DAILY BEAST 

Finally, there were Coronado and his terrible henchman, Texas Smith, with their rifles and revolvers. Overland |John William De Forest 

He does so, and kind Tim "fixes it up," gaining thereby another loyal henchman. The Old World in the New |Edward Alsworth Ross 

His name was Nevil Ormm, nobody was quite sure whence he had come, and he was Dunnan's henchman and constant companion. Space Viking |Henry Beam Piper 

Evidently Duke Angus had dropped whatever he was doing as soon as he heard what his henchman had to tell him. Space Viking |Henry Beam Piper 

As reinforcements came up, McCane and his henchman backed against a pile of timber. The Boss of Wind River |David Goodger (goodger@python.org)