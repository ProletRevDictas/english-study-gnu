Our Will Huntsberry obtained a memo the diocese sent to school pastors and principals earlier this week that spelled out the diocese’s conclusion that any mandate added by the governor must include a medical exemption and a personal belief exemption. Morning Report: Catholic Schools Will Allow Personal Belief Exemptions to COVID Jab |Voice of San Diego |November 5, 2021 |Voice of San Diego 

Police are working with the diocese to determine whether there are more possible victims. Virginia religious youth leader charged with sexual assault of juveniles |Jasmine Hilton |November 4, 2021 |Washington Post 

We hope that this course of action by the diocese balances the need to protect the health of our students, teachers and staffs with the rights of parents to decide issues vital to their children. Local Catholic Diocese: Students Can Evade COVID-19 Vaccine With Personal Belief Exemption |Will Huntsberry |November 4, 2021 |Voice of San Diego 

Auxiliary Bishop John Dolan has dubbed the vaccine a “moral obligation” and the diocese has also issued statements supporting vaccination. Morning Report: Hospital Stays Locked Homeless Residents Out of Hotel Rooms |Voice of San Diego |March 5, 2021 |Voice of San Diego 

The court-appointed receiver has filed multiple lawsuits accusing Prospect and the diocese of “omissions and half-truths actionable as fraud,” demanding that they help make the pension whole. Investors Extracted $400 Million From a Hospital Chain That Sometimes Couldn’t Pay for Medical Supplies or Gas for Ambulances |by Peter Elkind with Doris Burke |September 30, 2020 |ProPublica 

Take the case of Herx v. Diocese of Fort Wayne, an employment discrimination suit in the Seventh Circuit. Catholic Church: Religious Freedom Trumps Civil Rights |Jay Michaelson |November 23, 2014 |DAILY BEAST 

The local diocese said assistance would be given to the woman and her baby for a few weeks until she decided on her future. Nun Names Surprise Baby After Pope Francis |The Telegraph |January 21, 2014 |DAILY BEAST 

Father Taraborelli is a trained exorcist for the Rome diocese, and his work schedule is very busy. Vatican and Pope Francis Seek New Demon Exorcists |Barbie Latza Nadeau |January 8, 2014 |DAILY BEAST 

His predecessor had just banned three priests in his diocese from public ministry. Donald Wuerl: America’s Candidate for Pope? |Christopher Dickey |March 10, 2013 |DAILY BEAST 

More than $600,000 has been paid in lawsuits to victims in the Los Angeles diocese. Should Sex-Abuse-Scandal Cardinals Be Allowed to Vote for New Pope? |Barbie Latza Nadeau |February 21, 2013 |DAILY BEAST 

The archbishop of Manila sends to the king (July 30, 1621) an account of ecclesiastical and some other affairs in his diocese. The Philippine Islands, 1493-1898, Volume XX, 1621-1624 |Various 

In 1848 there were only seven priests in Birmingham, and but seventy in the whole diocese. Showell's Dictionary of Birmingham |Thomas T. Harman and Walter Showell 

Sangley missions of the diocese of the archbishopric of Manila, and the number of souls directed in them. The Philippine Islands, 1493-1898, Volume XX, 1621-1624 |Various 

Then he returned to his province, entered the seminary, and became a sub-deacon of the diocese of Nueva Segovia. The Philippine Islands |John Foreman 

It is a great country church of very unusual architecture, elevated to the head of a diocese in 1888. British Highways And Byways From A Motor Car |Thomas D. Murphy