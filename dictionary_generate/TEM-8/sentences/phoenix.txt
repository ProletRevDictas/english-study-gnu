The Phoenix I uses a leveling system that automatically adjusts its center of gravity, making it more stable and comfortable to maneuver. Start-ups seek to breathe new life into stagnant wheelchair industry |Dalvin Brown |February 19, 2021 |Washington Post 

Gomez died unexpectedly at his Phoenix home Sunday afternoon, according to the statement. Pedro Gomez, ESPN baseball reporter, dies at 58 |Ben Strauss |February 8, 2021 |Washington Post 

It was 2002, and Korina Adkins was moving to Phoenix from Washington for a new job. A thrift shop photo album is full of mystery photos of a District family |John Kelly |January 27, 2021 |Washington Post 

That’s the Phoenix I’m into, the one that’s carrying all the damage with it. 'People Expect a Woman to Grieve a Certain Way.' What One Mom Learned About Trauma and Strength After Losing Her Young Son |Belinda Luscombe |January 20, 2021 |Time 

The place where this happened was Hacienda HealthCare in Phoenix. A Woman With Developmental Disabilities Was Abused in Arizona. The State Promised Changes. It Has Not Made Them Yet. |by Amy Silverman for Arizona Daily Star |January 20, 2021 |ProPublica 

Three on-the-record stories from a family: a mother and her daughters who came from Phoenix. I Tried to Warn You About Sleazy Billionaire Jeffrey Epstein in 2003 |Vicky Ward |January 7, 2015 |DAILY BEAST 

BEST ACTOR Joaquin Phoenix, Inherent Vice There is no better actor—right now—than Joaquin Phoenix. Oscars 2015: The Daily Beast’s Picks, From Scarlett Johansson to ‘Boyhood’ |Marlow Stern |January 6, 2015 |DAILY BEAST 

He came to Phoenix once and we went up to see him, and they got so crazy that I ended up trying to hitchhike home. The Story Behind Lee Marvin’s Liberty Valance Smile |Robert Ward |January 3, 2015 |DAILY BEAST 

But you know, I had only one other hero in my life acting and that was River [Phoenix]. Coffee Talk with Ethan Hawke: On ‘Boyhood,’ Jennifer Lawrence, and Bill Clinton’s Urinal Exchange |Marlow Stern |December 27, 2014 |DAILY BEAST 

When a client needs to move something by air, Phoenix gets it done. The American Ebola Rescue Plan Hinges on One Company. Meet Phoenix. |Abby Haglage |November 22, 2014 |DAILY BEAST 

The bridge was constructed in 1888 by the Phoenix Bridge Company, and was erected on staging. Encyclopaedia Britannica, 11th Edition, Volume 4, Part 3 |Various 

He wrongd you shrewdly,He toyld to climbe vp to the Phoenix nest,And in his prints leaues your ascent more easie. The Fatal Dowry |Philip Massinger 

"Open the door, Phoenix," mumbled Calavius, as he rocked and swayed. The Lion's Brood |Duffield Osborne 

The story of just how Chicago proved herself a veritable Phoenix is a very interesting one. Ocean to Ocean on Horseback |Willard Glazier 

The savage had before this event been merely "a legendary and heraldic animal like the griffin and the phoenix." The Myths of the North American Indians |Lewis Spence