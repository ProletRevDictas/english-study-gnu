Looking for advice on how to pair your gray Club Monaco shawl-collar cardigan with your Gant Rugger oxford shirt? Menswear Dog Shares Classic Couture With Man’s Best Friend |Jean Trinh |January 30, 2013 |DAILY BEAST 

"I wasn't playin' Rugger with the boots, Father," he said, patiently. More William |Richmal Crompton 

What good do you imagine you men will be in the trenches, if you can't last out a short game of rugger like this? The Loom of Youth |Alec Waugh 

And the other funny thing about it is that it was told to me by a huge Rugger Blue called Eric. Punch, or the London Charivari, Vol. 159, December 22, 1920 |Various 

He was a Rugger Blue (I told you) and a three-quarter at that, so he went fairly fast. Punch, or the London Charivari, Vol. 159, December 22, 1920 |Various 

"In the distance he don't look a lot older than Gore," Pete Rugger declared to his neighbor. Mariquita |John Ayscough