What is clear is that in one of the reports Prude’s name was written in the space to label the “Victim.” Officials Worked To Deliberately Delay Release Of Daniel Prude Body Cam Footage |mharris |September 17, 2020 |Essence.com 

It can make it hard to say what president would be better for space. What Joe Biden could mean for US space policy |Tim Fernholz |September 17, 2020 |Quartz 

To survive, coastal wetlands need space to shift, Narayan explains. Soggy coastal soils? Here’s why ecologists love them |Alison Pearce Stevens |September 17, 2020 |Science News For Students 

India’s space agency is debating the 2023 launch of a Venus orbiter called Shukrayaan-1 to study atmospheric chemistry. We need to go to Venus as soon as possible |Neel Patel |September 16, 2020 |MIT Technology Review 

Watch this space for more to come from OZY on a thrilling new podcast, our election night plans and more. Sunday Magazine: The Deciders |Daniel Malloy |September 13, 2020 |Ozy 

Space Invaders had just been introduced in America, and Atari was looking for a game that would do it one better. ‘Asteroids’ & The Dawn of the Gamer Age |David Owen |November 29, 2014 |DAILY BEAST 

Her very first sculpture, a metallic chrome unicorn aptly titled “Space Oracle,” sits on a pedestal directly in front. The Tiniest Jackson Pollock |Justin Jones |November 5, 2014 |DAILY BEAST 

An unmanned rocket intended to deliver supplies to the International Space Station exploded on Tuesday. Harry Potter Raps, The Catcalls Heard ‘Round the World and More Viral Videos |Alex Chancey |November 2, 2014 |DAILY BEAST 

These companies include SpaceX, Orbital Sciences, Sierra Nevada Space Systems, and more. Luxembourg and China Team Up on Private Mission to the Moon |Matthew R. Francis |October 26, 2014 |DAILY BEAST 

It should also be noted that Space Jam did not make the cut, so you can rest easy. Basketball Bounces Through Classic Movies |Alex Chancey, The Daily Beast Video |October 15, 2014 |DAILY BEAST 

And we may proceed to the remaining two of the great classes into which facts have been divided; Resemblance, and Order in Space. A System of Logic: Ratiocinative and Inductive |John Stuart Mill 

A suspense story of Tomorrow and a crisis in the advance into Space. Space Prison |Tom Godwin 

"We were going to talk about the Space Guard," Lyla said in an emotionless tone. --And Devious the Line of Duty |Tom Godwin 

I understand you wanted to talk to me about the Space Guard? --And Devious the Line of Duty |Tom Godwin 

Space will not permit going over these connections in detail. Elements of Plumbing |Samuel Dibble