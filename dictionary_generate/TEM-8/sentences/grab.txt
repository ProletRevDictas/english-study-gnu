The post The streaming wars have escalated over turf grabs appeared first on Digiday. The streaming wars have escalated over turf grabs |Tim Peterson |August 26, 2020 |Digiday 

Now, two years after Ethiopia first teased the world with plans that as much of half of the country’s telecommunications industry was up for grabs, there is still no clear timetable for its implementation. Ethiopia is quietly walking back plans to open up Africa’s last big telecom sector to foreign players |Zecharias Zelalem |August 19, 2020 |Quartz 

Yet that hasn’t stopped him from using the crisis to justify even more aggressive data grabs. Brazil is sliding into techno-authoritarianism |Tate Ryan-Mosley |August 19, 2020 |MIT Technology Review 

Kayla Jimenez compiled a list of the seats that are up for grabs and the major initiatives that have already qualified in this week’s North County Report. Morning Report: The Rise of Private, Non-School Schooling Options |Voice of San Diego |August 6, 2020 |Voice of San Diego 

There are two City Council seats — District 2 and District 4 — up for grabs. North County Report: What We Know About the November Ballot So Far |Kayla Jimenez |August 5, 2020 |Voice of San Diego 

The officers approached Garner and tried to grab hold of him, but he swatted their hands away. Before Eric Garner, There Was Michael Stewart: The Tragic Story of the Real-Life Radio Raheem |Marlow Stern |December 4, 2014 |DAILY BEAST 

Johnson would tell the grand jury that he never saw Brown stick his hand inside the car and grab the gun. 90 Seconds of Fury in Ferguson Are the Key to Making Peace in America |Michael Daly |November 26, 2014 |DAILY BEAST 

In fact, as Ihrig points out, Mussolini called himself “the Mustafa Kemal of a Milanese Ankara” as he began his own power-grab. The 20th-Century Dictator Most Idolized by Hitler |William O’Connor |November 24, 2014 |DAILY BEAST 

The brazen land grab of Crimea was planned while Putin was enjoying the limelight of the Sochi Winter Olympics. Putin Is Lying on Ukraine—and the West Can’t Stop Him |Jamie Dettmer |November 14, 2014 |DAILY BEAST 

With every grab, the idea that law and politics are separate becomes harder for anyone to believe. A Reminder: Our Justices are Politicians in Robes |Jedediah Purdy |November 13, 2014 |DAILY BEAST 

His motto was, "Grab a dollar to-day—but don't meddle with it if it interferes with a thousand dollars in ten years." Scattergood Baines |Clarence Budington Kelland 

Joe tried to grab him with the boathook, but it was useless, and the unhappy poltroon's body was whirled away. The Chequers |James Runciman 

Sure maybe you could grab that dough by blasting your way with the heaters plenty. Hooded Detective, Volume III No. 2, January, 1942 |Various 

Jehosophat kicked at him with his wet feet, and tried to grab the fat red nose that hung down over the turkey's beak. Seven O'Clock Stories |Robert Gordon Anderson 

It might be nothing more than the ordinary “grab racket” with which a feast commonly concludes; it might be something worse. The Works of Robert Louis Stevenson - Swanston Edition Vol. 25 (of 25) |Robert Louis Stevenson