Additionally, the acceleration of cross-buying by existing SoFi members has created a virtuous cycle of compounding growth, diversified revenue and high profitability. Online lender SoFi to go public with a $8.7 billion valuation |Verne Kopytoff |January 7, 2021 |Fortune 

Bing struggled to get this virtuous cycle started, and never really got to the scale that Google enjoys with its search offering. Could Apple build a search engine that competes with Google? |By Hamza Mudassir/The Conversation |December 15, 2020 |Popular-Science 

“We have gone from the most affected country to one of the virtuous countries in the management of the pandemic thanks to the clarity of the rules from the very beginning, and the willingness of everyone to respect them,” Lorini says. The Unlikely Latest COVID Poster Child: Italy |Charu Kasturi |September 25, 2020 |Ozy 

It’s both beloved and iconic, and Mulan, who is as virtuous as she is strong and brave, is an essential heroine. The history of Mulan, from a 6th-century ballad to the live-action Disney movie |Constance Grady |September 4, 2020 |Vox 

As a political party ward leader, she was not daunted on her virtuous missions, which, to me, seemed to be defined by failing candidates or losing causes. Departure from convention—mom, baseball, the postal worker, and patriotism |jakemeth |August 19, 2020 |Fortune 

In other words, the free speech exhibited by the folks at Charlie Hebdo was not virtuous—until there was a body count. Politicians Only Love Journalists When They're Dead |Luke O’Neil |January 8, 2015 |DAILY BEAST 

A May 2014 Slate article by Sam Kean details the tragic changes he suffered “from a virtuous foreman to a sociopathic drifter.” Understanding Tracy Morgan’s Traumatic Brain Injury |Jean Kim |November 20, 2014 |DAILY BEAST 

Some look at the Aspen museum and wonder whether Ban will be able to continue creating his humanitarian “virtuous” architecture. Shigeru Ban: Triumph From Disaster |Nate Berg |August 31, 2014 |DAILY BEAST 

At times it can seem too proud of its virtuous noncommerciality; its slowness can seem shallow, its artiness willful. ‘The Leftovers’ Review: A Fever Dream You Can’t Wake Up From |Andrew Romano |June 29, 2014 |DAILY BEAST 

Guy-guitar-genius music isn't inherently evil; mass-produced pop isn't inherently virtuous. Is Jack White the Last True Rock Star? |Andrew Romano |June 13, 2014 |DAILY BEAST 

She was also supposed to be the original or model of “the Virtuous Woman” therein portrayed! Solomon and Solomonic Literature |Moncure Daniel Conway 

The virtuous statesman advanced to meet him, while his countenance proclaimed that he knew all, and sympathized with its victim. The Pastor's Fire-side Vol. 3 of 4 |Jane Porter 

Both of these readings appeal to the Solomonic portrait of the virtuous woman, in Proverbs xxxi. Solomon and Solomonic Literature |Moncure Daniel Conway 

A stone mason was employed to engrave the following epitaph on a tradesman's wife: "A virtuous woman is a crown to her husband." The Book of Anecdotes and Budget of Fun; |Various 

For twenty years you hold an innocent and virtuous woman under an infamous suspicion. The Joyous Adventures of Aristide Pujol |William J. Locke