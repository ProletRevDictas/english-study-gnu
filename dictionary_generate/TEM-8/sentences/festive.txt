We think about how our cards were displayed because they were pretty and festive before, and how now, that displayability is more a metaphor for someone in their life. How Hallmark is handling pandemic Valentine’s Day |Luke Winkie |February 12, 2021 |Vox 

Bright and festive, with some bubbles, this cocktail is tart and tasty. This Valentine’s Day, we couldn’t help but wonder, which ‘Sex and the City’ character will you most eat like? |Kari Sonde |February 11, 2021 |Washington Post 

Although most of us had smaller versions of the festive events that we’ve become used to, that doesn’t mean that the day was any less magical. See How Your Favorite Stars Are Bringing In The New Year! |Brooklyn White |January 1, 2021 |Essence.com 

As much as family gatherings for festive dinners and opening gifts are annual Christmas rites, so is feeding and giving gifts to the poor, with churches and charitable organizations offering care to masses of needy strangers. On a covid Christmas, a new routine for feeding the homeless |Paul Schwartzman |December 25, 2020 |Washington Post 

The snow flurries today were festive and in the holiday spirit. Updated D.C.-area forecast: Very cold through Saturday with bitter wind chills |A. Camden Walker, Jason Samenow |December 25, 2020 |Washington Post 

Many love Christmas and all the festive decorations that come with it. Star Wars Christmas Lights, Unedited Footage of a Bear, and More Viral Videos |The Daily Beast Video |December 21, 2014 |DAILY BEAST 

Creepy thing to wrap up in festive paper and a bow and give to a newborn baby, yeah? Keep Christmas Commercialized! |P. J. O’Rourke |December 6, 2014 |DAILY BEAST 

The best part of having a drink is sharing the experience with friends, especially during festive occasions. National Liquor Days Are a Joke |Kayleigh Kulp |September 27, 2014 |DAILY BEAST 

And the entire Bon Temps gang has gathered for a festive Thanksgiving dinner. 'True Blood' Ends With a Whimper: The Sexy HBO Vampire Series Is (Finally) Over |Marlow Stern |August 25, 2014 |DAILY BEAST 

When the host is in a festive mood, entering customers are given strings of Mardi Gras beads. The Ultimate Southern Cheeseburger Created in South Carolina |Jane & Michael Stern |August 10, 2014 |DAILY BEAST 

John is called out at a festive gathering, and springs to his feet really intending to be clever. Glances at Europe |Horace Greeley 

They are ranged around a crescent-shaped table formed of cushions, and wear festive crowns upon their heads. The Catacombs of Rome |William Henry Withrow 

However, my thoughts will be with you, and you have my best wishes for a really festive day. A Thin Ghost and Others |M. R. (Montague Rhodes) James 

But two swallows don't make a summer, nor two gentlemen in evening dress a festive party. Punch, or the London Charivari, Vol. 108, April 6, 1895 |Various 

Jones stood close at my elbow, with a face as festive now as it was ruthful not long before. Blackwood's Edinburgh Magazine, Volume 67, Number 414, April, 1850 |Various