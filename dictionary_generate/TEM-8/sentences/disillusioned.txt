Founders Tom McGillycuddy and Matt Latham spent 8 years working in investment management but say they became disillusioned by the jargon, high fees and indifference to causes such as the environment. Tickr, which allows retail investors to back Impact companies, secures $3.4M from Ada Ventures |Mike Butcher |February 5, 2021 |TechCrunch 

He left the priesthood after he became disillusioned with the group he was working with, his wife said, and he realized he wanted to get married and have a family. Frank Anderson, 87, cared passionately about feeding the homeless in D.C. |Dana Hedgpeth |February 5, 2021 |Washington Post 

Another mother said she could no longer recognize her child, because he had grown so angry and disillusioned with the world during distance learning. Virginia’s largest school systems vow to reopen classrooms for all by March |Hannah Natanson |February 3, 2021 |Washington Post 

Watson is disillusioned and adamant that something better exists in the NFL. Deshaun Watson is taking a stand against disingenuous NFL owners. It could change the league. |Jerry Brewer |February 1, 2021 |Washington Post 

It’s clear from online posts that these groups believe disillusioned Q followers are ripe for radicalization, and they may be right. What Comes Next For QAnon Followers |Kaleigh Rogers |January 29, 2021 |FiveThirtyEight 

“I had started to become disillusioned with the norms of how people put together social structures,” recalls Miller. DJ Spooky Wants You To Question Everything You Know About Music, Technology, and Philosophy |Oliver Jones |December 27, 2014 |DAILY BEAST 

Of the ones that do survive, some will be too disillusioned to carry out an attack. What the U.S. Can Learn from Europe About Dealing with Terrorists |Scott Beauchamp |December 15, 2014 |DAILY BEAST 

Many former Eme stated that is why they left after feeling disillusioned. The Mexican Mafia Is the Daddy of All Street Gangs |Seth Ferranti |December 11, 2014 |DAILY BEAST 

A new survey finds Millennials disillusioned by Obama and the Democrats. New Harvard Study Finds the Door to Millennial Vote Is Open to GOP |Kristen Soltis Anderson |October 30, 2014 |DAILY BEAST 

Reeling from regional developments and disillusioned with the West, some Iraqi Christians are looking to Russia for support. Iraq’s Christians See Putin As Savior |Peter Schwartzstein |June 29, 2014 |DAILY BEAST 

Practically penniless and absolutely disillusioned, the amazing man was radiantly happy. The Joyous Adventures of Aristide Pujol |William J. Locke 

She rose, abruptly disillusioned, and looked at him as though she would spring at his throat. The Woman Gives |Owen Johnson 

The music of a concertina rose and fell, like the sighing of some disillusioned spirit. Villa Rubein and Other Stories |John Galsworthy 

The weather concerns the old, the satisfied and disillusioned of life, the folk from whom the romance of being has departed. The Cruise of the Shining Light |Norman Duncan 

The sceptic, disillusioned, is stated to have failed to appreciate the joke! The Story of the Cambrian |C. P. Gasquoine