He likes small windows and greatly dislikes the sweeping areas of glass and metal that characterise the work of Richard Rogers. Imagining Prince Charles as King Makes All of Britain Wish They Could Leave Like Scotland |Clive Irving |September 17, 2014 |DAILY BEAST 

The tumult and license which usually characterise a general election were more than ordinarily rampant and intolerant. The History of England in Three Volumes, Vol.III. |E. Farr and E. H. Nolan 

Very little of that unctuous spasmodic shouting, which used to characterise Wesleyanism, is heard in Lune-street Chapel. Our Churches and Chapels |Atticus 

Good sound sense, neatly adjusted argument, newness of thought, and clear illustration characterise his expressions. Our Churches and Chapels |Atticus 

For the rest, it is what we have attempted to characterise as poetical rant—imagination grown raving and delirious. Blackwood's Edinburgh Magazine, Volume 67, Number 414, April, 1850 |Various 

What epithet ought one to apply to Timaeus, and what word will properly characterise him? The Histories of Polybius, Vol. II (of 2) |Polybius