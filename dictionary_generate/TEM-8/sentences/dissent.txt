So, we instituted something we call “farming for dissent,” where on these big decisions everybody has to write down in public in a shared document, how they feel about the idea and their judgment about it. What if Your Company Had No Rules? (Bonus Episode) |Maria Konnikova |September 12, 2020 |Freakonomics 

The law, aimed at silencing dissent, shattered the notion that Hong Kong’s legislature is independent from Beijing. Hong Kong’s citywide COVID-19 testing has become a barometer of public trust |eamonbarrett |September 9, 2020 |Fortune 

He said Ikhrata was trying to remove a voice of dissent on the SANDAG board. One Race Could Make or Break Plans to Overhaul the Region’s Transportation System |Jesse Marx |August 13, 2020 |Voice of San Diego 

Given Lukashenko’s control over the system — he won’t allow independent election observers — it remains unlikely that Tikhanovskaya will win outright, but the past couple of weeks have fueled the fire of a growing dissent. Could She Upset Belarus’ Dictator? |Pallabi Munsi |August 5, 2020 |Ozy 

Justice Alito filed a separate dissent, joined by Justice Thomas. The Supreme Court Decision To Grant Protections To LGBT Workers Is An Important Expansion Of The Civil Rights Act |LGBTQ-Editor |June 18, 2020 |No Straight News 

In other words, fluoride is a broad-spectrum, bipartisan, long-lasting magnet for dissent. Anti-Fluoriders Are The OG Anti-Vaxxers |Michael Schulson |July 27, 2016 |DAILY BEAST 

Priests often preach support for the regime to their congregations, many of whom loudly dissent. In One Corner of Syria, Christmas Spirit Somehow Manages to Survive |Peter Schwartzstein |December 25, 2014 |DAILY BEAST 

As noted by Judge Martha Craig Daughtrey in dissent, this is an outrageous position. All The Wrong Reasons to Ban Gay Unions |Jay Michaelson |November 7, 2014 |DAILY BEAST 

Others also suspect civil groups with funding coming from Mainland China are sowing dissent. Hong Kong’s Triads Attack Protestors |Ben Leung |October 4, 2014 |DAILY BEAST 

Racial mistrust, military tactics against citizens, dissent quashed. Ferguson Shows a Nation at War With Itself |Roland S. Martin |August 16, 2014 |DAILY BEAST 

What justifies the disruption requires a dissent from the civil power, as a power not of God. The Ordinance of Covenanting |John Cunningham 

The first reading was carried without a division, the Duke of Richmond being the only peer who expressed dissent. The History of England in Three Volumes, Vol.III. |E. Farr and E. H. Nolan 

It requires more power and strength of mind and decision of character to dissent from the Church of the State than to support it. East Anglia |J. Ewing Ritchie 

For from these three Chapels came not only the impulse of the spiritual life of Llanyglo, but its local politics of dissent also. Mushroom Town |Oliver Onions 

He was re-nominated the next year without dissent or opposition, but declined a re-election on account of ill health. The Bay State Monthly, Vol. II, No. 6, March, 1885 |Various