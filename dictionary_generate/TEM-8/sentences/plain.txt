These microbes had been living beneath a vast, flat, sediment-covered plain. Some deep-seafloor microbes still alive after 100 million years! |Carolyn Gramling |September 1, 2020 |Science News For Students 

If I’m rolling into a Starbucks, in plain sight of lots of foot traffic, and the rack’s right out front, my odds are decent this is enough deterrent to ward off the baddies. The most secure ways to lock up your bike |By Michael Frank/Cycle Volta |August 26, 2020 |Popular-Science 

One of the main challenges in curing cancer is that unlike foreign invaders, tumor cells are part of the body and so able to hide in plain sight. Scientists Used Protein Switches to Turn T-Cells Into Cancer-Fighting Guided Missiles |Edd Gent |August 24, 2020 |Singularity Hub 

In the US, for instance, the Food and Drug Administration approved chloroquine for treating covid-19, only to reverse itself weeks later after it became plain the drug didn’t work. Every country wants a covid-19 vaccine. Who will get it first? |Katie McLean |August 13, 2020 |MIT Technology Review 

Researchers, led by microbiologist Yuki Morono of the Japan Agency for Marine-Earth Science and Technology in Kochi, examined sediments collected in 2010 from part of the abyssal plain beneath the South Pacific Gyre. These ancient seafloor microbes woke up after over 100 million years |Carolyn Gramling |July 28, 2020 |Science News 

Yet, much like the fate that fell the first season, ratings just plain weren't good. ‘The Comeback’ Finale: Give Lisa Kudrow All of the Awards |Kevin Fallon |December 29, 2014 |DAILY BEAST 

Thanks to the Atlanta case, they can now see another in plain sight. A Gift to the Jihadis: The Unseen Airport Security Threat |Clive Irving |December 27, 2014 |DAILY BEAST 

Because holy hell was that bland, unfunny, uncomfortable, and just plain confusing. The Biggest Bombs of 2014: ‘Sex Tape,’ Mariah Carey’s Vocals, ‘How I Met Your Mother’ and More |Kevin Fallon |December 19, 2014 |DAILY BEAST 

It is already well known that there are oilrigs disguised in plain sight all over the city. The Fiery Underground Oil Pit Eating L.A. |Geoff Manaugh |December 6, 2014 |DAILY BEAST 

A report by the Cleveland Plain Dealer describes balloons being released into the night sky. The Cleveland Cops Who Fired 137 Shots and Cried Victim |Michael Daly |December 2, 2014 |DAILY BEAST 

But what a magnificent plain is this we are entering upon: it is of immense extent. Blackwood's Edinburgh Magazine, Volume 60, No. 372, October 1846 |Various 

A far-off volley rumbled over the plain, and a few birds stirred uneasily among the trees. The Red Year |Louis Tracy 

There are many more good dwellings on this plain than in the rural portion of Lower Italy. Glances at Europe |Horace Greeley 

The word of the law shall be fulfilled without a lie, and wisdom shall be made plain in the mouth of the faithful. The Bible, Douay-Rheims Version |Various 

Here there is no question of emergency, or enemy pressure, or of haste; so much we see plain enough with our own eyes. Gallipoli Diary, Volume I |Ian Hamilton