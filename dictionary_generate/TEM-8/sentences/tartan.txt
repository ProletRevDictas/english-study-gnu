In her tartan bouclé jacket, a Chanel bag slung over her shoulder, she’s anything but regular folk—yet she acts surprised when the locals recognize her patented charm routine. Kristen Stewart and Pablo Larraín Do Princess Diana Wrong in Spencer |Stephanie Zacharek |September 3, 2021 |Time 

They stood at attention in their tartan kilts, white leggings and bearskin hats as a Marine band struck up “Hail to the Chief.” Michael Daly: My Last Day With JFK |Michael Daly |November 11, 2013 |DAILY BEAST 

I can barely imagine what tartan might have done to my psyche. The Seneca Tartan? |Blake Gopnik |September 14, 2012 |DAILY BEAST 

But the tartan insists that its tone shall be invariable, and sharply defined by contrasts of dark and light. Ceres' Runaway |Alice Meynell 

On a bundle of clothing tied in a tartan kerchief for a pillow, he lay very still and breathing heavily. Greyfriars Bobby |Eleanor Atkinson 

One was a British officer, in the scarlet jacket and tartan trews of a Highland regiment. Ponce de Leon |William Pilling 

The fore-part of his head is white: his back, tail, and wings green; and his breast and belly tartan. Wanderings in South America |Charles Waterton 

But—what on earth—a green tartan frock, and a toque with a white feather—she herself! Dry Fish and Wet |Anthon Bernhard Elias Nilsen