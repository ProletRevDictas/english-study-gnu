He circled back later to the idea that he might build a website to help more people find appointments. My mother and her friends couldn’t get coronavirus vaccine appointments, so they turned to a stranger for help. He’s 13. |Greg Harris |February 12, 2021 |Washington Post 

Patients could register for a slot—either online or with someone who comes to their home—and then wait to be picked for an appointment. Chicago thinks Zocdoc can help solve its vaccine chaos |Lindsay Muscato |February 12, 2021 |MIT Technology Review 

In the District, Nesbitt said, Giant pharmacies will be receiving some doses and will schedule appointments through the city’s website. Leaders in Washington region ask FEMA for help in vaccinating federal workers |Julie Zauzmer, Rachel Chason, Rebecca Tan |February 11, 2021 |Washington Post 

Holding clinics on church grounds can make getting the vaccine easier for people who do not have the ability to drive to a mass vaccination clinic or who have trouble getting online to check and recheck websites, hoping for an appointment. Churches pair up with clinics to deliver coronavirus vaccine to those who need it most |Jenna Portnoy |February 10, 2021 |Washington Post 

Her voice cracked as she described waiting in a 400-person phone queue to sign up for a shot, only to be told all appointments were taken and she couldn’t even leave her name. Confusion and chaos: Inside the vaccine rollout in D.C., Maryland and Virginia |Julie Zauzmer, Gregory S. Schneider, Erin Cox |February 9, 2021 |Washington Post 

He said the news of his appointment was not true, that it was disinformation spread by “some intelligence agency and my rivals.” ISIS Targets Afghanistan Just as the U.S. Quits |Sami Yousafzai, Christopher Dickey |December 19, 2014 |DAILY BEAST 

Jessen was named a Mormon bishop, but the appointment was met with vocal protests. The Luxury Homes That Torture and Your Tax Dollars Built |Michael Daly |December 12, 2014 |DAILY BEAST 

Sharpton noted that otherwise some of their mutual detractors might suggest that he had played a role in the appointment. Is Al Sharpton Running New York City Hall From the White House? |Michael Daly |November 7, 2014 |DAILY BEAST 

The appointment of Klain to lead the effort is giving the public what it wants—but not what it needs. Ron Klain Will Be the Best Ebola Czar Yet |Tim Mak, Abby Haglage |October 17, 2014 |DAILY BEAST 

The appointment of the new Ebola czar comes after Republicans began demanding a White House point person on the threat. Ron Klain Will Be the Best Ebola Czar Yet |Tim Mak, Abby Haglage |October 17, 2014 |DAILY BEAST 

She was helpless, because she had said nothing all day of her appointment, and because Janet had not mentioned it either. Hilda Lessways |Arnold Bennett 

Having seen no service, he owed his appointment largely to his conceit and good looks. Napoleon's Marshals |R. P. Dunn-Pattison 

As an M.P. you are duly qualified to accept any appointment under the Crown when the Government ask you. Punch, or the London Charivari, Volume 107, November 3, 1894 |Various 

A test examination would follow of a perfunctory character, and an intimation of your appointment would be the sequel. Punch, or the London Charivari, Volume 107, November 3, 1894 |Various 

This alone could hinder the execution of his appointment, for in other things he has excellent qualifications for the dignity. The Philippine Islands, 1493-1898, Volume XX, 1621-1624 |Various