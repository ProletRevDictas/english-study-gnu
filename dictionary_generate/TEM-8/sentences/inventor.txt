Indeed, many of history’s most revered artists and inventors had daily routines that incorporated periods of mental rest. It Pays to Be a Space Case - Facts So Romantic |Alice Fleerackers |October 7, 2020 |Nautilus 

Interestingly, the Club’s inventor was said to have been inspired by his service in the Korean War, when he used a chain to lock the steering wheel of military vehicles. When Your Safety Becomes My Danger (Ep. 432) |Stephen J. Dubner |September 24, 2020 |Freakonomics 

While prosthetic arms and legs remained quite primitive by modern standards, inventors emphasized improvements in comfort and modest gains in functionality. Times of strife can lead to medical innovation—when governments are willing |By Jeffrey Clemens/The Conversation |September 9, 2020 |Popular-Science 

At times she wanted to be a mathematician, an inventor, a scientist and an architect. Before working on spacecraft, this engineer overcame self-doubt |Carolyn Wilke |April 7, 2020 |Science News For Students 

Von Hippel went on to become a professional inventor, at least for a little while. Honey, I Grew the Economy (Ep. 399) |Stephen J. Dubner |December 5, 2019 |Freakonomics 

At his best, he was an inventor of part of the modern cinema's grammar. Alfred Hitchcock’s Fade to Black: The Great Director’s Final Days |David Freeman |December 13, 2014 |DAILY BEAST 

As described by its inventor, there is an Honest team and a Mafia team that compete against each other. Where Chechens Go to Escape Their Surreal Past—and Risky Present |Anna Nemtsova |December 9, 2014 |DAILY BEAST 

Here, I thought for years my father had been a cook, but apparently he was an inventor. Do Palestinians Really Exist? |Dean Obeidallah |July 31, 2014 |DAILY BEAST 

Yet this brilliant inventor, who revolutionized modern technological society, refused the recommended surgical procedure. Why Smart People Are Dumb Patients |Jean Kim |July 14, 2014 |DAILY BEAST 

I just read an interview with Roger Linn, the inventor of the Linn Drum. Giorgio Moroder, Dance Music Legend, on Remixing Coldplay’s ‘Midnight’ and ‘Crazy’ Lana Del Rey |Douglas Wolk |April 30, 2014 |DAILY BEAST 

He was also the inventor of "Poikilorgue," an expressive organ, which was the origin of the harmonium. The Recent Revolution in Organ Building |George Laing Miller 

Probably even to-day the majority would name Walter Map as the populariser, if not the inventor, of the Grail legend. The Three Days' Tournament |Jessie L. Weston 

The inventor claimed that the "American Transplanter" could do the work of several men and do it equally well. Tobacco; Its History, Varieties, Culture, Manufacture and Commerce |E. R. Billings. 

He was the inventor of Exchequer Bills; and they were popularly called Montague's notes. The History of England from the Accession of James II. |Thomas Babington Macaulay 

One who merely utilizes the ideas of others is not an original inventor and is not entitled to a patent. Putnam's Handy Law Book for the Layman |Albert Sidney Bolles