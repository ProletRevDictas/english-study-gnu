The Cuyahoga first caught on fire in 1868 and would burn 11 more times until the blaze on June 22, 1969. 51 Years Later, the Cuyahoga River Burns Again |Wes Siler |August 28, 2020 |Outside Online 

Here’s how these aerial operations work, and what it’s like battling blazes from the air. How aerial firefighters battle blazes from the skies |Rob Verger |August 27, 2020 |Popular-Science 

The blazes quickly ripped through hundreds of thousands of acres, forcing thousands to evacuate, filling the skies with smoke, and raining down ash across much of the region. Yes, climate change is almost certainly fueling California’s massive fires |James Temple |August 20, 2020 |MIT Technology Review 

A tornado of fire blazed before settling into a spinning blue flame several centimeters tall. Four types of flames join forces to make this eerie ‘blue whirl’ |Emily Conover |August 12, 2020 |Science News 

We’re blazing our own path, and creating opportunities that weren’t there before. How Would You Describe Millennials In One Word? |Candice Bradley |July 21, 2020 |Everything After Z 

On Christmas Day, sometime after dark, a hideous fire overtook the venue: 100 firefighters, 33 fire trucks, a four-alarm blaze. The Fiery Death of Sotto Sotto, Toronto’s Celebrity Hotspot |Shinan Govani |December 30, 2014 |DAILY BEAST 

A Molotov cocktail tumbled in an arc overhead and erupted briefly in a blaze. Mexican Protesters Look to Start a New Revolution |Jason McGahan |November 21, 2014 |DAILY BEAST 

The blaze was deemed suspicious enough to warrant an investigation. The Loser Who Wanted to Be the ISIS Agent Next Door |Michael Daly |September 18, 2014 |DAILY BEAST 

They began assisting whomever they could and made plans to fight this blaze on high. The Flying New York Fireman Who Shined on 9/11 |Michael Daly |September 11, 2014 |DAILY BEAST 

Pasto is almost 8,300 feet up in the mountains, so it was cold and crisp, with a blaze of stars across the sky. Spirit Tripping With Colombian Shamans |Chris Allbritton |August 24, 2014 |DAILY BEAST 

She got up and stood in front of the fire, having her hand on the chimney-piece and looking down at the blaze. Confidence |Henry James 

She waited for the material pictures which she thought would gather and blaze before her imagination. The Awakening and Selected Short Stories |Kate Chopin 

Through these flues were forced currents of hot air from a blaze in a large fireplace at one end of the house. Our Little Korean Cousin |H. Lee M. Pike 

Hard up as we are for shell he thinks it best to blaze it away freely before closing and to trust our bayonets when we get in. Gallipoli Diary, Volume I |Ian Hamilton 

There was a fire burning in the general-room of the hostelry, and Garnache went to warm him at its cheerful blaze. St. Martin's Summer |Rafael Sabatini