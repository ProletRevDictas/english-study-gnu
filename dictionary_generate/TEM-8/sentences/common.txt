Students would have a fully equipped laptop to call their own as well as one that didn’t lack key functionalities, which is common among donated devices. The coronavirus pandemic is expanding California’s digital divide |Walter Thompson |July 9, 2020 |TechCrunch 

The company’s financial history is occluded — common with private companies — and a bit uneven. Coinbase reported to consider late 2020, early 2021 public debut |Alex Wilhelm |July 9, 2020 |TechCrunch 

Residents often live four to a room, share a bathroom, and congregate in crowded common spaces. Canada’s “national shame”: Covid-19 in nursing homes |Sigal Samuel |July 7, 2020 |Vox 

The system can’t be easily reverse-engineered to determine what it learned to pay attention to during training — a common problem for researchers trying to use AI to do science. Why Is Glass Rigid? Signs of Its Secret Structure Emerge. |John Pavlus |July 7, 2020 |Quanta Magazine 

Accounts on these platforms were all registered using a handful of common email addresses and phone numbers. FBI nabs Nigerian business scammer who allegedly cost victims millions |Timothy B. Lee |July 6, 2020 |Ars Technica 

The email appears to have been a relatively common attempt to gain personal information from a wide range of unwitting victims. Was Sony Hit With a Second Hack? |Shane Harris |January 8, 2015 |DAILY BEAST 

The vaccine is delivered through a “carrier virus” that causes a common cold in chimpanzees but does not affect humans. The Race for the Ebola Vaccine |Abby Haglage |January 7, 2015 |DAILY BEAST 

Another read: “We need leaders who will stand against Common Core.” Rand Paul’s Passive-Aggressive Trolling Campaign |Olivia Nuzzi |January 6, 2015 |DAILY BEAST 

Finding the common bonds that help us realize that we have far more in common than that which separates us. In 2015, Let’s Try for More Compassion |Gene Robinson |January 4, 2015 |DAILY BEAST 

At the time, screen quotas were far more common among film producing industries. Propaganda, Protest, and Poisonous Vipers: The Cinema War in Korea |Rich Goldstein |December 30, 2014 |DAILY BEAST 

The Smooth Naked Horsetail is a common plant, specially by the sides of streams and pools. How to Know the Ferns |S. Leonard Bastin 

I would ask you to imagine it translated into every language, a common material of understanding throughout all the world. The Salvaging Of Civilisation |H. G. (Herbert George) Wells 

Our social life is aimless without it, we are a crowd without a common understanding. The Salvaging Of Civilisation |H. G. (Herbert George) Wells 

Diplococci without capsules are common in the sputum, but have no special significance. A Manual of Clinical Diagnosis |James Campbell Todd 

He had discovered that the all-glorious boast of Spain was not exempt from the infirmities of common men. The Pastor's Fire-side Vol. 3 of 4 |Jane Porter