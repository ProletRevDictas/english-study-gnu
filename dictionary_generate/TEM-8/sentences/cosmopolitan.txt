During what would turn out to be a 12-year stint in office, Ferré transformed Miami from a one-trick tourist destination into a thriving cosmopolitan city by fostering high-rise construction and opening the city’s ports to international business. Florida Like You’ve Never Seen It |Stephen Starr |August 27, 2021 |Ozy 

Adams carried most of the city’s neighborhoods outside its cosmopolitan core. How Eric Adams Won The New York City Mayoral Primary |Nathaniel Rakich (nathaniel.rakich@fivethirtyeight.com) |August 25, 2021 |FiveThirtyEight 

Its cosmopolitan hippies and surfers and acid burnouts still exist, but they share a state with millions of people who are more rural, more conservative, more rugged. The ballad of the Chowchilla bus kidnapping |Kaleb Horton |July 23, 2021 |Vox 

At the same time, though, where Bush was incurious and parochial, Obama was intellectual and cosmopolitan. Presidents Used To Be The Faces Of Their Political Parties. Is That No Longer The Case? |Julia Azari |July 13, 2021 |FiveThirtyEight 

Compared with those settlements, Salisbury was cosmopolitan. A Magical Realm of Crabs and Chickens |jversteegh |July 8, 2021 |Outside Online 

Cosmopolitan has asked readers, “Are You Dating a Lumbersexual?” How Straight World Stole ‘Gay’: The Last Gasp of the ‘Lumbersexual’ |Tim Teeman |November 12, 2014 |DAILY BEAST 

The Levant is already a far cry from the cosmopolitan melting pot it once was. Beirut Letter: In Lebanon, Fighting ISIS With Culture and Satire |Kim Ghattas |September 22, 2014 |DAILY BEAST 

He is deeply rooted in a heartfelt and learned cosmopolitan spiritualism. Meet The Democrats’ Secret Savior Against Cuomo Corporatism |James Poulos |September 14, 2014 |DAILY BEAST 

The British editor of Cosmopolitan says she loves talking about (and having) sex. Joanna Coles: Why Cosmopolitan Does Sexy and Serious So Well |Lloyd Grove |August 22, 2014 |DAILY BEAST 

Cosmopolitan is bringing its hilariously bad sex tips to lesbians now. I Tried Cosmo’s Lesbian Sex Tips and They Were Terrible |Samantha Allen |July 30, 2014 |DAILY BEAST 

He was mentally so cosmopolitan, so much at ease in the world, that here in London he readily found himself at home indeed. The Eve of the Revolution |Carl Becker 

As he spoke he gesticulated slightly, and no second glance was needed to realise that he was a thorough-going cosmopolitan. The Doctor of Pimlico |William Le Queux 

In certain moods he possessed that dash and devil-may-care air which pleases most women, providing the man is a cosmopolitan. The Doctor of Pimlico |William Le Queux 

He was a marvellously alert man, an unusually good linguist, and a cosmopolitan to his finger-tips. The Doctor of Pimlico |William Le Queux 

No second glance at Fetherston was needed to ascertain that he was a most thorough-going cosmopolitan. The Doctor of Pimlico |William Le Queux