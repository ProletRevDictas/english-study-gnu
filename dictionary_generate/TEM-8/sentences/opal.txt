The rebels then turned their guns on the family huddled in the Opal. ‘Argo’ in the Congo: The Ghosts of the Stanleyville Hostage Crisis |Nina Strochlic |November 23, 2014 |DAILY BEAST 

One of the more positive parts of your personal life is your marriage to Opal Stone Perlman, which has lasted over three decades. Ron Perlman's Secret Suicide Attempt |William O’Connor |October 28, 2014 |DAILY BEAST 

That clock has four faces made of opal, estimated at a value of $10-20 million. Grand Central Terminal: 100 Years, 100 Facts |Sarah Begley |February 1, 2013 |DAILY BEAST 

In that case, at least, Little, Brown responded by pulling Opal from the bookshelves. Another Memoir Meltdown |Howard Kurtz |April 18, 2011 |DAILY BEAST 

He owned a 54-ton yacht named the Opal, and attributed the wonderful health he enjoyed to his numerous sea voyages. The Recent Revolution in Organ Building |George Laing Miller 

If the collodion is good, the film is semi-transparent, of a bluish opal-like appearance. Notes and Queries, Number 178, March 26, 1853 |Various 

He fell asleep, and soon and slowly and ever so dimly the opal light of the prairie dawn crept shyly over the landscape. You Never Know Your Luck, Complete |Gilbert Parker 

The moon, ringed by a halo, shone like an opal in the milk-white sky. The Child of Pleasure |Gabriele D'Annunzio 

He was drinking a dose of sal-volatile, and admiring its opal tint. The Napoleon of Notting Hill |Gilbert K. Chesterton