It’s the first time astronomers had identified an exploding magnetar in another galaxy. Exploding neutron star proves to be energy standout of the cosmos |Lisa Grossman |February 12, 2021 |Science News For Students 

Each year, astronomers discover nova explosions in the Milky Way that cause dim stars to flare up and emit far more light than the sun before they fade again. The number of Milky Way nova explosions per year has been pinned down |Ken Croswell |February 12, 2021 |Science News 

Confirming the planet’s existence should not be too hard—astronomers simply have to observe the object again and see if its new position corresponds with an orbit. There’s a tantalizing sign of a habitable-zone planet in Alpha Centauri |Neel Patel |February 10, 2021 |MIT Technology Review 

A highlight of the five-day event is the Cadillac Mountain Star Party, with rangers and astronomers acting as travel guides to the sky. The Ultimate Acadia National Park Travel Guide |Virginia M. Wright |February 8, 2021 |Outside Online 

Both buck the trend astronomers expected from theories of how planetary systems form. Two exoplanet families redefine what planetary systems can look like |Lisa Grossman |February 5, 2021 |Science News 

“A lot of the critical details in the plot were a mishmash of ideas that made no sense,” astronomer Phil Plait wrote at Slate. Meet Kip Thorne, the Man Who Crafted the Artful Science of ‘Interstellar’ |Asawin Suebsaeng |November 14, 2014 |DAILY BEAST 

The new Research Institute set features an all-female cast: a paleontologist, astronomer, and chemist. Why It Took LEGO So Long to Get the Memo: Girls Like Science, Too |Samantha Allen |August 6, 2014 |DAILY BEAST 

In 1859, astronomer Richard Carrington observed a strong solar flare that was directed at the Earth. About That ‘World-Ending’ Solar Storm... |Nicole Gugliucci |July 28, 2014 |DAILY BEAST 

Calvin astronomer Howard Van Till was for years the leading evangelical champion of the Big Bang Theory. The Christian Reformed Church Still Won’t Stand Up For Science |Karl W. Giberson |June 29, 2014 |DAILY BEAST 

Edward Hubble, the greatest astronomer of the 20th century, who discovered the expanding universe, he was inspired by Jules Verne. Is Paris Hilton Killing Science? |Nash Landesman |April 15, 2009 |DAILY BEAST 

The crest-fallen astronomer plodded on his weary way, another example of a fool and his money soon parted. The Book of Anecdotes and Budget of Fun; |Various 

His birth might also be remembered as occurring in the same year as that of the great astronomer Galileo. Assimilative Memory |Marcus Dwight Larrowe (AKA Prof. A. Loisette) 

Let us suppose that the Astronomer Royal claimed infallibility, not only in matters astronomical, but also in politics and morals. God and my Neighbour |Robert Blatchford 

The modern astronomer needs to know much of chemistry, or else he can not understand many of his observations on the sun. Outlines of the Earth's History |Nathaniel Southgate Shaler 

On the way homewards, we visited the observatory of the famous astronomer, Dey Singh. A Woman's Journey Round the World |Ida Pfeiffer