For some people, it means getting the toothpaste off the bathroom mirror. How to find ‘green’ cleaning products that get the job done |Laura Daily |August 24, 2021 |Washington Post 

We also have to buy all of our essential items such as soap, shampoo, underwear, socks or toothpaste. Locked up in the Land of Liberty: Part II |Michael K. Lavers |July 14, 2021 |Washington Blade 

Agencies in our view can’t and shouldn’t try to put that toothpaste back in the tube. Federal government will maintain expansive work-from-home policies after the pandemic |Lisa Rein, Eric Yoder |June 10, 2021 |Washington Post 

For elephant toothpaste, you might measure the height of the foam using video recordings. Level up your demonstration: Make it an experiment |Bethany Brookshire |April 27, 2021 |Science News For Students 

The company just launched a clinical trial of the toothpaste that will enroll 32 adults who are allergic to peanuts. Could a toothpaste help treat peanut allergy? |Esther Landhuis |April 13, 2021 |Science News For Students 

Plus, Procter & Gamble has already removed triclosan from its Crest toothpaste. Antibacterial Soap’s Deadly Secret |Kent Sepkowitz |May 21, 2014 |DAILY BEAST 

Taken aback, toothpaste dribbling down my chin, I stood there befuddled, not quite sure what to do next. Can a Jew Get Down With Hot Jesus from 'Son of God?' |Sara Lieberman |March 9, 2014 |DAILY BEAST 

Toothpaste bombs, wiretapped toothbrushes, tastebud disasters. Up to a Point: P.J. O’Rourke on Valentine’s Day and Oral Hygiene |P. J. O’Rourke |February 14, 2014 |DAILY BEAST 

Procter & Gamble introducing Crest toothpaste line with nontraditional flavors—chocolate, vanilla, lime. Up to a Point: P.J. O’Rourke on Valentine’s Day and Oral Hygiene |P. J. O’Rourke |February 14, 2014 |DAILY BEAST 

P&G announcement came shortly before warnings by Russian security forces about dangers of toothpaste bombs in Sochi. Up to a Point: P.J. O’Rourke on Valentine’s Day and Oral Hygiene |P. J. O’Rourke |February 14, 2014 |DAILY BEAST 

They tried frantically to remedy the situation by the use of this toothpaste and that, and this deodorant and the other. Operation: Outer Space |William Fitzgerald Jenkins 

Stand clear and order off your thugs or I'll squeeze you till your guts squirt out your nose like toothpaste from a tube. Master of the Moondog |Stanley Mullen 

"I'll be dog-goned," swore Old Chauncey with toothpaste foam dribbling down his chin. Here Lies |H.W. Guernsey 

He missed the bobbypins on the floor, the nylons drying across the shower rack, the toothpaste tubes squeezed from the top. At the Post |Horace Leonard Gold 

The newspapers interviewed her, society women copied her, toothpaste and perfume manufacturers solicited her testimonials. The Easiest Way |Eugene Walter and Arthur Hornblow