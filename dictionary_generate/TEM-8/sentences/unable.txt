Early last year, health officials did not urge their use because of concerns health workers would be unable to get them. Masks should fit better or be doubled up to protect against coronavirus variants, CDC says |Lena H. Sun, Fenit Nirappil |February 11, 2021 |Washington Post 

Meanwhile, athletes are left to wait, unable to compete and their ability to hold formal practices or access training facilities varying by school. Ivy League sports were shut down quickly by coronavirus. A restart is proving much slower. |Glynn A. Hill |February 10, 2021 |Washington Post 

The Somali and Oromo callers were also unable to listen to the meeting, since it wasn’t translated for them. African Communities Warn Language Issues Could Shut Them Out of Redistricting |Maya Srikrishnan |February 10, 2021 |Voice of San Diego 

I feared she’d been drenched and unable to withstand the chill. How a sickly squirrel offered me unexpected comfort |Pam Spritzer |February 8, 2021 |Washington Post 

Because if we don’t — if we don’t, then conservatives all across this country will be unable to speak, they’ll be unable to do business, they’ll be unable to be heard. Republicans claim they are being silenced as they speak to millions of viewers |JM Rieger |February 4, 2021 |Washington Post 

That man was Xavier Cortada, a gay man who wrote of his frustration that he and his partner of eight years were unable to marry. Jeb Bush’s Unseen Anti-Gay Marriage Emails |Jackie Kucinich |January 9, 2015 |DAILY BEAST 

Those opposing same-sex marriage are on their heels, and increasingly unwilling or unable to make a stand against it. ‘Only God’ Can Stop Gay Marriage |Tim Mak |January 6, 2015 |DAILY BEAST 

But at this stage, he is either afraid or unable to get carried away by his thoughts. The Lost Novel of Nobel-Winner José Saramago |Charles Shafaieh |January 5, 2015 |DAILY BEAST 

Lee would stay up late, unable to sleep from the pains he had in his back. The Story Behind Lee Marvin’s Liberty Valance Smile |Robert Ward |January 3, 2015 |DAILY BEAST 

In the name of protecting passengers, however, tourists in Las Vegas are unable to take advantage of this service. Why Do ‘Progressives’ Want to Ban Uber and AirBnB? |Adam Thierer, Christopher Koopman |December 30, 2014 |DAILY BEAST 

As men fixed in the grip of nightmare, we were powerless—unable to do anything but wait. Gallipoli Diary, Volume I |Ian Hamilton 

Your sacrifice shall be the agony of agonies, the death of deaths, and yet you'll find yourself unable to resist. Checkmate |Joseph Sheridan Le Fanu 

The exertions of the city authorities, who had notice of the meditated riot, were unable to prevent or quell it. The Every Day Book of History and Chronology |Joel Munsell 

We stood staring after the fugitives in perfect bewilderment, totally unable to explain their apparently causeless panic. Blackwood's Edinburgh Magazine, No. CCCXXXIX. January, 1844. Vol. LV. |Various 

The young woman was unable to answer; she pressed the hand of the pianist convulsively. The Awakening and Selected Short Stories |Kate Chopin