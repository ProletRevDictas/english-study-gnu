Taste, and season with additional salt and lime juice, if desired. Caribbean-inspired seafood stew brings warm island vibes to your table |Ellie Krieger |March 4, 2021 |Washington Post 

Fried and dusted with chili lime or nacho spice, they don’t taste much different from say, corn nuts or extra crispy shrimp. They're Healthy. They're Sustainable. So Why Don't Humans Eat More Bugs? |Aryn Baker |February 26, 2021 |Time 

Our fanciest roller features “pressure points,” lime green ribs, and costs around $35. This Is the Foam Roller My Family Fights Over |Abigail Wise |January 27, 2021 |Outside Online 

Prior to serving, add the lime juice, then season with salt to taste. 7 Wellness Strategies to Build Resilience |Brad Stulberg |January 22, 2021 |Outside Online 

In a small bowl, combine the chicken stock, wine or water with the orange and lime juices. Aromatic roasted chicken with homemade adobo is an updated take on a classic meal |Christian Vazquez |January 15, 2021 |Washington Post 

Think wearing yellow lipstick, lime-green nails, and a SpongeBob SquarePants jumper with no trousers. The Improbable Rise of Rita Ora: A Guide for the Modern-Day Celebrity |Emma Gannon |May 5, 2014 |DAILY BEAST 

Some were injected with chemicals or dosed in lime for better preservation. Palermo Has an Underground City Filled With Its Mummified Dead |Nina Strochlic |May 1, 2014 |DAILY BEAST 

At a taco truck in New York I asked how their lime stock was faring. Limepocalypse! Inside the Great Lime Shortage of 2014 |Kara Cutruzzula |April 30, 2014 |DAILY BEAST 

Maybe all this Sturm und Drang will give birth to a new lime-lite generation of cocktails. Limepocalypse! Inside the Great Lime Shortage of 2014 |Kara Cutruzzula |April 30, 2014 |DAILY BEAST 

The verjus gives it that hit of sour acid, which is what we generally use lime for in cocktails. Limepocalypse! Inside the Great Lime Shortage of 2014 |Kara Cutruzzula |April 30, 2014 |DAILY BEAST 

Why not have sought out the pure white lime-rocks of the flat country, or the grey granite of the hills? Blackwood's Edinburgh Magazine, Volume 60, No. 372, October 1846 |Various 

For instance, the Limestone Polypody is not happy unless there is a certain amount of lime present in the soil. How to Know the Ferns |S. Leonard Bastin 

But, before they can be used for this purpose, these leaves are coated with lime made from oyster shells and then folded up. Alila, Our Little Philippine Cousin |Mary Hazelton Wade 

The acid is extracted from the juice of the citron, the lime, and the lemon, fruits grown in Sicily and the West Indies. Showell's Dictionary of Birmingham |Thomas T. Harman and Walter Showell 

Chloride of Lime … bad smell … bad egg … white of egg … fowl … grain … flour … flour and water … milk fluid … milk. Assimilative Memory |Marcus Dwight Larrowe (AKA Prof. A. Loisette)