It includes further features we had suggested, such as making it easier to unplug before bed and limit notifications. Big Tech’s attention economy can be reformed. Here’s how. |Gideon Lichfield |January 10, 2021 |MIT Technology Review 

Sometimes that means unplugging completely and tramping in the woods, like the weekend I spent camping along Devil’s Path in the Catskills last month. Kicking off the new year with a screen cleanse |rhhackettfortune |January 5, 2021 |Fortune 

Heather, waging her own battle with covid-19, the disease caused by the coronavirus, said goodbye to her younger sister on a live stream, tears running down her face as she watched doctors unplug the machine that had helped her sister breathe. ‘I said goodbye to my sister through a computer screen’ |Holly Bailey |January 2, 2021 |Washington Post 

You went straight to your room, unplugged your phone from the charger. Dark spaces on the map |Katie McLean |December 18, 2020 |MIT Technology Review 

According to their legal complaint, Drumwright had permission to use a generator for his PA system, and he approached a deputy to ask why the generator was being unplugged. Pistols, a Hearse and Trucks Playing Chicken: Why Some Voters Felt Harassed and Intimidated at the Polls |by Adriana Gallardo, Maryam Jameel and Ryan McCarthy |December 4, 2020 |ProPublica 

No cars, no medicine, no microwaves, no phones—just unplug from everything science has given you and leave the rest of us alone. Southern Baptist Convention: Trans People Don’t Exist |Jay Michaelson |June 12, 2014 |DAILY BEAST 

But to say we need to unplug … stopping progress is a bad, bad, bad idea. Relax, Robots Won’t Take Every Job: ‘The Second Machine Age’ |Scott Timberg |March 14, 2014 |DAILY BEAST 

He rarely suggests that we develop the fortitude to unplug our brains from the news-generated matrix that subsumes us. What is the News? Whatever Alain de Botton Thinks It Is |Robert Herritt |February 20, 2014 |DAILY BEAST 

One obvious (and healthy) way to unplug is to go exercise—ideally doing something that requires you use both hands. Seven Things to Do on National Day of Unplugging |Sam Schlinkert |March 1, 2013 |DAILY BEAST 

“I never unplug,” says Edith Zimmerman, editor of The Hairpin. Seven Things to Do on National Day of Unplugging |Sam Schlinkert |March 1, 2013 |DAILY BEAST 

His partner moved quickly and efficiently around the bed, eventually figuring out how to unplug the horn. Makers |Cory Doctorow 

To unplug a lamp you should grasp (cord) (plug) firmly and pull. Electricity for the 4-H Scientist |Eric B. Wilson