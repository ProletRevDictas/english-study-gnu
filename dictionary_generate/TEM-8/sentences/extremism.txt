The civil rights carve-out, for example, may be well intentioned but could lead to a worsening situation as those who support political extremism try to turn their beliefs into protected speech. How a Democratic plan to reform Section 230 could backfire |Bobbie Johnson |February 8, 2021 |MIT Technology Review 

“The entire infrastructure of that website from the beginning was about incitement,” said Rita Katz, executive director of SITE Intelligence Group, which monitors online extremism. TheDonald’s owner speaks out on why he finally pulled plug on hate-filled site |Craig Timberg, Drew Harwell |February 5, 2021 |Washington Post 

Often staffed in part by the formerly radicalized, they are on the front lines of the fight against right-wing extremism, a growing threat that is in the spotlight but which experts argue has long been neglected. After Capitol riots, desperate families turn to groups that ‘deprogram’ extremists |Paulina Villegas, Hannah Knowles |February 5, 2021 |Washington Post 

The vast majority joined the military for the right reasons, but even as extremism rises in society broadly, it will rise in the military. The U.S. Military Needs to Fight Extremism in Its Own Ranks. Here's How |James Stavridis |February 5, 2021 |Time 

In other words, we have evidence that a member of Congress promoted the kind of extremism and even bloodlust that led to an attempted insurrection at the Capitol. The GOP’s Marjorie Taylor Greene problem is spinning out of control |Aaron Blake |January 27, 2021 |Washington Post 

Moscow-based Editor in Chief Galina Timchenko was fired for ‘extremism’ after running an article on Ukraine. Russia’s Freest Website Now Lives in Latvia |Anna Nemtsova |November 29, 2014 |DAILY BEAST 

The defeat of Islamic extremism requires both hard and soft power responses. Why’s Al Qaeda So Strong? Washington Has (Literally) No idea |Bruce Riedel |November 9, 2014 |DAILY BEAST 

What these men do have is a muscular hold on popular disgust with religious extremism. Karen Armstrong’s New Rule: Religion Isn’t Responsible for Violence |Patricia Pearson |October 29, 2014 |DAILY BEAST 

As Assaf put it, “this is one way to reject extremism and make it so the people are not afraid.” Middle East Goes Monty Python on ISIS |Dean Obeidallah |October 29, 2014 |DAILY BEAST 

According to Pew, more than three in five Americans are very concerned about the rise of Islamic extremism. Iowa Frontrunner Mike Huckabee Talks to The Daily Beast |Lloyd Green |September 22, 2014 |DAILY BEAST 

Because of this extremism, his voice was doomed to remain that of one crying in the wilderness. Prophets of Dissent |Otto Heller 

We have a diplomatic strategy that is rallying the world to join in the fight against extremism. State of the Union Addresses of George W. Bush |George W. Bush 

We're also standing against the forces of extremism embodied by the regime in Tehran. State of the Union Addresses of George W. Bush |George W. Bush 

We're also standing against the forces of extremism in the Holy Land, where we have new cause for hope. State of the Union Addresses of George W. Bush |George W. Bush 

It is from this enthusiasm and extremism that there sounds one of the key-notes of woman's nature--her loyalty. Greek Women |Mitchell Carroll