In August, a statement by National Counterintelligence and Security Center Director William Evanina equated Russia’s interference efforts with those of China’s and Iran’s, and even put the China section first. What to make of the DHS whistleblower’s shocking complaint |Alex Ward |September 11, 2020 |Vox 

The stereotypical nonprofit fundraiser equates to old-fashioned and expensive, with a seat at a table costing hundreds or even thousands of dollars to swig Champagne with the barons of society. How nonprofits are catering to millennials and rethinking the charity gala for younger generations |Rachel King |September 7, 2020 |Fortune 

So if a utility produces a million units of energy and emits 500,000 metric tons of carbon emissions in the process at the social cost of $50 per ton, that equates to $25 million more in fees on the power company. Environment Report: One Way to Force Companies to Emit Less Carbon |MacKenzie Elmer |August 10, 2020 |Voice of San Diego 

Unfortunately, many business owners only equate their brand to its visual identity, its tagline, and its logo. How to plan your social media strategy for any business |Sumeet Anand |June 24, 2020 |Search Engine Watch 

Even Vivek Murthy cautions against equating aloneness with loneliness. Is There Really a “Loneliness Epidemic”? (Ep. 407) |Stephen J. Dubner |February 27, 2020 |Freakonomics 

There are those, however, who don't equate sangfroid and good manners with maturity. Will the Real Jim Palmer Please Stand Up |Tom Boswell |September 27, 2014 |DAILY BEAST 

Does enjoying parties directly equate to being an expert in hosting one? Living Like ‘The Hills’: Kristin Cavallari, Lauren Conrad, and the Phenomenon of Our Current ‘Tastemakers’ |Erin Cunningham |March 27, 2014 |DAILY BEAST 

In the marketing, or something as simple as a poster or a DVD cover or artwork, we even equate some of it with being on tour. ‘Portlandia’ Duo Fred Armisen and Carrie Brownstein Discuss the Secrets to the Cult Show’s Success |Marlow Stern |February 27, 2014 |DAILY BEAST 

Producers often tend to equate harder-hitting crime stories with a city setting – from Cracker and Prime Suspect to Luther. British Crime Dramas Explore the Dark Side of Small Town Life |Soraya Roberts |September 13, 2013 |DAILY BEAST 

It may be hard to equate John Kerry now with the same man in 2004 and 1971. Kerry vs. Kerry? It’s Not Simply Partisan Hypocrisy on Syria |Jamelle Bouie |September 6, 2013 |DAILY BEAST 

All at once every symbol was constant, static and livid upon the screen, enhanced by the words equate—complete—equate—complete. We're Friends, Now |Henry Hasse 

It is a more serious difficulty that Paul knows of no Longobardic king with a name which we can equate with Sceaf. Beowulf |R. W. Chambers 

Casembe sat before his hut on a equate seat placed on lion and leopard skins. The Last Journals of David Livingstone, in Central Africa, from 1865 to His Death, Volume I (of 2), 1866-1868 |David Livingstone 

Thousands of differences perplex the attempt to equate the measure of moral desert to men. Theoretical Ethics |Milton Valentine 

Plato had the ideal of an education which should equate individual realization and social coherency and stability. Democracy and Education |John Dewey