When internet was working its way into society, Brazil set up a body known as the Internet Steering Committee, whose job was to smooth the transition of the internet in society and kind of improve its development. Podcast: COVID-19 is helping turn Brazil into a surveillance state |Anthony Green |September 16, 2020 |MIT Technology Review 

Life transitions are moments when behavior change is abnormally easy. How a vacation—or a pandemic—can help you adopt better habits now |matthewheimer |September 12, 2020 |Fortune 

As my colleague Maria Aspan reported yesterday, the appointment was an expected transition. New Citi CEO Jane Fraser gives Wall Street banks a fresh perspective |Claire Zillman, reporter |September 11, 2020 |Fortune 

Anticipating this, Uber says it has earmarked $800 million to help its drivers transition to electric vehicles. Uber Wants to Go All-Electric by 2030. It Won’t Be Easy |Vanessa Bates Ramirez |September 10, 2020 |Singularity Hub 

We need, at least for the transition period, natural gas to ensure a secure energy supply in Germany. Trump has long wanted to kill a Russia-Germany natural gas pipeline. Navalny’s poisoning could do it for him |David Meyer |September 8, 2020 |Fortune 

A second document was titled: “Gambia Reborn: A Charter for Transition from Dictatorship to Democracy and Development.” The Shadowy U.S. Veteran Who Tried to Overthrow a Country |Jacob Siegel |January 6, 2015 |DAILY BEAST 

Exactly when the transition to modern domestic creature took place, for a bird that is wild to this day, is controversial. The History of the Chicken: How This Humble Bird Saved Humanity |William O’Connor |December 27, 2014 |DAILY BEAST 

How did you make the transition from pro surfer to modeling? Anastasia Ashley, Surfer-Cum-Model, Rides The Viral Internet Wave |James Joiner |December 23, 2014 |DAILY BEAST 

He held intrigue for journalists converging for the transition of papal power. How Pope Francis Became the World’s BFF |Jason Berry |December 21, 2014 |DAILY BEAST 

Will the transition of power from one Kim to another become drenched in even more blood? Kim Jong Un’s Kid Gloves Are Now Off |Gordon G. Chang |December 17, 2014 |DAILY BEAST 

With children of finer perception the transition to a correct profile view may be carried much further. Children's Ways |James Sully 

From this condition to the state where one of the stars is so nearly dark as to be invisible, the transition is but slight. Outlines of the Earth's History |Nathaniel Southgate Shaler 

It is merely the transition of matter into new forms—into combinations which are subject to new processes. Gospel Philosophy |J. H. Ward 

I consider you are at present in a transition period; in a state of fermentation; and no one knows what you are capable of doing. The Life &amp; Letters of Peter Ilich Tchaikovsky |Modeste Tchaikovsky 

So much for swearing when in grim earnest; how are we to account for it in its transition to sport and play? A Cursory History of Swearing |Julian Sharman