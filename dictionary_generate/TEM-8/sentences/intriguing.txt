Popping out of the blackness at lower left are two faint, intriguing specks of light. These Images Expose the Dark Side of the Solar System - Issue 89: The Dark Side |Corey S. Powell |August 26, 2020 |Nautilus 

One intriguing material is neopentylglycol, a type of plastic crystal. The Shape-Shifting Squeeze Coolers |Marcus Woo |August 24, 2020 |Quanta Magazine 

Some of these what-if scenarios are intriguing, like one where Edison admits he misjudged Tesla and suggests they become an electrical engineering dynamic duo. Ethan Hawke stars in ‘Tesla,’ a quirky biopic about the iconic inventor |Maria Temming |August 19, 2020 |Science News 

It’s this specific fandom, more so than the actual product itself, that makes Triller such an intriguing prospect for marketers now. WTF is Triller? |Seb Joseph |August 14, 2020 |Digiday 

That fact raises some intriguing possibilities about what we might discover beyond the bounds of Earth. How Life Could Continue to Evolve - Issue 88: Love & Sex |Caleb Scharf |August 12, 2020 |Nautilus 

Radcliffe remains one of the most intriguing young stars around. Daniel Radcliffe: I’m Richer Than One Direction |Tim Teeman |October 24, 2014 |DAILY BEAST 

Most intriguing, it is the male bodies that are shot at their most exposed. Prince Fielder’s Demi Moore Moment: World Loses It Over Athlete Without Six-Pack |Tim Teeman |July 10, 2014 |DAILY BEAST 

But those farewells immediately sent his story careening off in intriguing new directions. Best ‘Game of Thrones’ Season Yet |Andrew Romano |June 16, 2014 |DAILY BEAST 

All of which raises an intriguing question: Is the sudden rise of movies (and shows) that criticize drones a mere coincidence? Hollywood’s War on Drones |Andrew Romano |May 23, 2014 |DAILY BEAST 

Not since Gwyneth Paltrow's “conscious uncoupling” has one term been so intriguing, and so enigmatic. Avril Lavigne’s Dumb ‘Hello Kitty’ Video Is Rife with Cultural Appropriation |Amy Zimmerman |April 25, 2014 |DAILY BEAST 

The further he delved into the mystery of the whispering criminal known as the Eye, the more intriguing it became. Hooded Detective, Volume III No. 2, January, 1942 |Various 

The same intriguing sycophant who had encouraged the Papist in one fatal error was now encouraging the soldier in another. The History of England from the Accession of James II. |Thomas Babington Macaulay 

This does not look like intriguing for the viceroyalty, of which Fitzwilliam evidently suspected him. Ireland Under the Tudors, Vol. II (of 3) |Richard Bagwell 

On the other hand, it was alleged that the English government was intriguing for a prince of the house of Cobourg. The History of England in Three Volumes, Vol.III. |E. Farr and E. H. Nolan 

Hastings found that a French agent was intriguing with them, and took prompt measures against them. The Political History of England - Vol. X. |William Hunt