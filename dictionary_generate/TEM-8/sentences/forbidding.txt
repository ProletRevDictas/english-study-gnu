The consensus leans toward forbidding it, though some people of knowledge think it permissible. ISIS Jihadis Get ‘Slavery for Dummies’ |Jamie Dettmer |December 9, 2014 |DAILY BEAST 

Graterford is a forbidding, shabby, woebegone facility built in 1929. Here’s a Reform Even the Koch Brothers and George Soros Can Agree On |Tina Brown |November 10, 2014 |DAILY BEAST 

As forbidding as this terrain is, there is another force at work on the ocean surface – the Antarctic Circumpolar Current. MH370 Debris Is Lost Forever, Can the Plane Be Found Without It? |Clive Irving |September 7, 2014 |DAILY BEAST 

“Mujahid pledging allegiance to Abu Bakr al-Baghdadi and Islamic State commanding good and forbidding evil,” it reads. The Mystery of Donald Ray Morgan, the 44-Year-Old American Who Loved ISIS |Michael Daly |August 12, 2014 |DAILY BEAST 

Last year Russia passed a law forbidding “propaganda of non-traditional sexual relations,” allegedly to protect kids. Ukraine’s Fighting Words |Igor Kossov |May 13, 2014 |DAILY BEAST 

Some peculiar lines between these contracted brows gave a character of ferocity to this forbidding and sensual face. Checkmate |Joseph Sheridan Le Fanu 

She fixed her imploring eyes on the Virgin's face and on the saints; but all seemed to her to wear a forbidding look. Ramona |Helen Hunt Jackson 

But at Orleans he received an angry message from Bonaparte forbidding him to return to Paris. Napoleon's Marshals |R. P. Dunn-Pattison 

In the commands forbidding Israel to enter into covenant with the Canaanites, or their gods, the phrase is used. The Ordinance of Covenanting |John Cunningham 

On July 19 a proclamation was issued forbidding the possession of firearms without licence. The Philippine Islands |John Foreman