Then I sort of forgot about them, until roughly 2015, when a diabolically ingenious album called FFS—a collaboration between the Scottish band Franz Ferdinand and, once again, Sparks—caught my attention. The Sparks Brothers, a Sundance Delight, Tells the Grand Story of This Enduringly Enigmatic Art-Pop Duo |Stephanie Zacharek |February 3, 2021 |Time 

An ingenious little invention, this kitchen appliance can make a huge difference when it comes to quick snacks, full meals, and delicious leftovers. The best microwaves: This way for buttery popcorn and tempting leftovers |PopSci Commerce Team |January 22, 2021 |Popular-Science 

However, the game’s art direction and ingenious puzzles give “Call of the Sea” undeniable momentum. ‘Call of the Sea’: Strictly for hardcore puzzlers |Christopher Byrd |January 15, 2021 |Washington Post 

Once on the ground an extremely ingenious system allows the flight-pod “to open up like a flower and release the dog in less than 10 seconds after landing.” This new harness lets military dogs parachute safely and with style |Christina Mackenzie |January 4, 2021 |Popular-Science 

Pointy uses an ingenious and simple method to get local store inventory online, which can then be presented in organic search results or paid search ads. Even as more people shop online, searches for ‘available near me’ up over 100% vs. last year |Greg Sterling |December 10, 2020 |Search Engine Land 

Projects like this offer smart, sometimes ingenious ideas about how we can and should live in buildings and cities. Why Architects Dream Big—and Crazy |Nate Berg |August 23, 2014 |DAILY BEAST 

In ways large and small, devious and immature, ingenious and inspiring, she struggled to escape. The Day the Fairytale Died |Marilyn Johnson |July 12, 2014 |DAILY BEAST 

Mimicry is an ingenious survival technique, albeit one that is of little use against bulldozers and chainsaws. Exploring the Amazon, While We Still Can |Darrell Hartman |May 15, 2014 |DAILY BEAST 

We came up with an ingenious plan that would light a fire in the belly of the digital revolution. Sandi Thom On How To Make It As A Female Rock Star |Sandi Thom |April 9, 2014 |DAILY BEAST 

They have talent and Lennon/McCartney are the most inventive, wide-ranging and melodically ingenious writers pop has produced. What It Was Like to Watch the Beatles Become the Beatles—Nik Cohn Remembers |Nik Cohn |February 9, 2014 |DAILY BEAST 

Just try it once, and you'll see how ingenious it is—only one must be careful not to throw out the elbow in turning out the wrist. Music-Study in Germany |Amy Fay 

He uses the red pipe-stone and other materials in the production of his pipes, which are ingenious specimens of sculpture. Tobacco; Its History, Varieties, Culture, Manufacture and Commerce |E. R. Billings. 

Metal buttons or pistons located on the toe piece of the pedal-board were introduced by the ingenious Casavant of Canada. The Recent Revolution in Organ Building |George Laing Miller 

From these ingenious "conceits" we turn to a few thoughts on the present condition and history of the plant. Tobacco; Its History, Varieties, Culture, Manufacture and Commerce |E. R. Billings. 

The pipes are alternated and in this ingenious way sympathy is largely avoided. The Recent Revolution in Organ Building |George Laing Miller