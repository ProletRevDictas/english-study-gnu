They uncovered evidence that he had dipped into his clients’ insurance premiums for his own uses and borrowed money to keep his real estate business afloat. The Murder Chicago Didn’t Want to Solve |by Mick Dumke |February 25, 2021 |ProPublica 

This is not to single you out, “Navigating,” just borrowing you to make a larger point that’s been bugging me a lot lately. Carolyn Hax: What if your daughter and her BFF were really just BFs? |Carolyn Hax |February 25, 2021 |Washington Post 

This task was borrowed from an old experiment that asked lab subjects to turn little wooden pegs. Am I Boring You? (Ep. 225 Rebroadcast) |Stephen J. Dubner |February 25, 2021 |Freakonomics 

Mobile lending apps have become an easy source of credit for Kenyans who don’t have accounts with banks and other traditional financial institutions, or the regular income needed to borrow from such establishments. Kenya is preparing to crack down on a flood of high-interest loan apps |Carlos Mureithi |February 22, 2021 |Quartz 

Then again, the glory of American cuisine is the way it borrows from other cultures and comes up with something all its own. Fresh reasons to return to 1789, one of D.C.’s oldest restaurants |Tom Sietsema |February 19, 2021 |Washington Post 

To borrow an old right-wing talking point, these people are angry no matter what we do. Harry Shearer on The Dangerous Business of Satire |Lloyd Grove |January 8, 2015 |DAILY BEAST 

The rapid rise of the sharing economy is changing the way people around the world commute, shop, vacation, and borrow. Why Do ‘Progressives’ Want to Ban Uber and AirBnB? |Adam Thierer, Christopher Koopman |December 30, 2014 |DAILY BEAST 

“I have coordinated with our foreign minister so we will borrow from other countries which have offered,” he said. The Presumed Crash of AirAsia Flight QZ8501 Is Nothing Like MH370 |Lennox Samuels |December 29, 2014 |DAILY BEAST 

Much of what passes for political coverage these days is (to borrow a phrase) “bad Chucky.” Wanted: Less Terrible Political Coverage on TV |Matt Lewis |November 19, 2014 |DAILY BEAST 

These marriages are “facts on the ground,” to borrow a phrase from the conflict in the Middle East. Gay Marriage Chaos Begins |Jay Michaelson |November 11, 2014 |DAILY BEAST 

At the reserve bank they may borrow as a standing right and not as a favor which may be cut off. Readings in Money and Banking |Chester Arthur Phillips 

Germany invests money abroad, but she seems to borrow as much, and more, in the discount markets of London and Paris. Readings in Money and Banking |Chester Arthur Phillips 

"I can't borrow money—I can't—I don't know how to do it," said Brammel peevishly. Blackwood's Edinburgh Magazine, No. CCCXXXIX. January, 1844. Vol. LV. |Various 

The human species,” Charles Lamb says, “is composed of two distinct races, the men who borrow and the men who lend. Fifty Years of Railway Life in England, Scotland and Ireland |Joseph Tatlow 

I may record here that each of my assistants has since, to borrow an Americanism, “made good.” Fifty Years of Railway Life in England, Scotland and Ireland |Joseph Tatlow