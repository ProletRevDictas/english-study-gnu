We are learning so much this week — not only about a shameful attack on our very government, but the shamelessness of the people who made it inevitable. I was skeptical of Democrats pursuing a second impeachment. I was wrong. |Karen Tumulty |February 11, 2021 |Washington Post 

It seemed every time the Wizards mounted an attack, the Raptors responded with the same type of play — a far-too-easy three that bumped the score back into comfortable territory and sapped Washington’s energy at the same time. The Wizards’ defense again lets them down in loss at home to Raptors |Ava Wallace |February 11, 2021 |Washington Post 

Under pressure, Hogan launches effort to pay five exonerated prisoners Maryland exoneree suffers heart attack before testifying on compensation bill Maryland moves toward clear plan for paying people who were wrongly convicted |Ovetta Wiggins |February 10, 2021 |Washington Post 

As more and more processes and facilities gain remote and automated capabilities, this kind of attack becomes more of a concern. Hackers accessed a Florida water treatment plant’s system and tried to make a dangerous change |Stan Horaczek |February 9, 2021 |Popular-Science 

Roy thinks it’s an attack on the right to note that a Republican member of Congress was one of hundreds of thousands of people with underlying conditions to die after contracting the virus. What you’re saying when you say that covering the coronavirus is partisan |Philip Bump |February 9, 2021 |Washington Post 

“The institution of marraige [sic] is under attack in our society and it needs to be strengthened,” Bush wrote. Jeb Bush’s Unseen Anti-Gay Marriage Emails |Jackie Kucinich |January 9, 2015 |DAILY BEAST 

As soon as this attack [happened], Paris citizens came together to show were are not afraid, we are Charlie Hebdo. France Kills Charlie Hebdo Murderers |Nico Hines |January 9, 2015 |DAILY BEAST 

It is grandstanding for a right rarely protected unless under immediate attack. Politicians Only Love Journalists When They're Dead |Luke O’Neil |January 8, 2015 |DAILY BEAST 

The comedian responded to the deadly attack on a French satirical magazine by renewing his recent criticisms of the Islamic faith. Bill Maher: Hundreds of Millions of Muslims Support Attack on ‘Charlie Hebdo’ |Lloyd Grove |January 8, 2015 |DAILY BEAST 

It was also an attack on our freedom of expression and way of life. Politicians Only Love Journalists When They're Dead |Luke O’Neil |January 8, 2015 |DAILY BEAST 

In their shelter, Brion and Ulv crouched low and wondered why the attack didn't come. Sense of Obligation |Henry Maxwell Dempsey (AKA Harry Harrison) 

On to Gaba Tepe just in time to see the opening, the climax and the end of the dreaded Turkish counter attack. Gallipoli Diary, Volume I |Ian Hamilton 

Then the enemy's howitzers and field guns had it all their own way, forcing attack to yield a lot of ground. Gallipoli Diary, Volume I |Ian Hamilton 

But the strength of his arm, and the bravery of his heart could not have defended him long against their determined attack. The Pastor's Fire-side Vol. 3 of 4 |Jane Porter 

Uric acid is decreased before an attack of gout and increased afterward, but its etiologic relation is still uncertain. A Manual of Clinical Diagnosis |James Campbell Todd