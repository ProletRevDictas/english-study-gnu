Being able to do so is an important but early step to help an animal regain movement in an injured or paralyzed limb. Elon Musk shows off Neuralink brain implant technology in a living pig |jonathanvanian2015 |August 29, 2020 |Fortune 

Men and women who become infertile as a result of chemotherapy could also regain their reproductive capabilities. Men Will Someday Have Kids Without Women |Samantha Allen |January 3, 2015 |DAILY BEAST 

To regain their relevancy, Democrats need to go back to their evolutionary roots. Time to Bring Back the Truman Democrats |Joel Kotkin |December 21, 2014 |DAILY BEAST 

Muse was looking to regain custody of her four children—Justin, Sarah, Patrick and Rachel. Beaten By His Church for Being Gay |Justin Jones |December 16, 2014 |DAILY BEAST 

President Obama pledged an estimated $175 million and sent more than 4,000 troops to help the region regain its footing. Jail Threats for Sierra Leone Ebola Victims’ Families |Abby Haglage |December 10, 2014 |DAILY BEAST 

There was some small sliver of hope that Democrats could regain some lost ground in 2016 with Hillary Clinton on the ballot. Southern Dems Won’t Rise Again |Ben Jacobs |December 5, 2014 |DAILY BEAST 

A certain amount of his ill-humour vented, Tressan made an effort to regain his self-control. St. Martin's Summer |Rafael Sabatini 

While endeavoring to regain their feet, some were violently thrown upon the wooden platform. A Lost Hero |Elizabeth Stuart Phelps Ward and Herbert D. Ward 

This made it necessary to put on a spurt to regain lost distance, but on such ground the speed was dangerous. Hunting the Lions |R.M. Ballantyne 

He shoved Coulter as far off as possible and at the same time struck out to regain the surface of the lake. The Mystery at Putnam Hall |Arthur M. Winfield 

She half turned so that Mazaroff could not see the expression of her face; she wanted time to regain control over her features. The Weight of the Crown |Fred M. White