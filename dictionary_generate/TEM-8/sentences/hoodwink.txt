Critics from patriotic bloggers to academics go as far as claiming China’s leaders have been hoodwinked by Western counterparts trying to hold China back. China is key to saving the planet from climate change. But it can’t quit coal. |Lily Kuo, Steven Mufson |September 21, 2021 |Washington Post 

Initially hoodwinked by Lysenko, over time, as he looked into Lysenko’s claims, Vavilov became suspicious, and he asked a student of his to see if Lysenko’s results could be replicated. The Botanist Who Defied Stalin - Issue 99: Universality |Lee Alan Dugatkin |April 21, 2021 |Nautilus 

Or when he said the Clinton camp was trying to “bamboozle” or “hoodwink” voters? Hillary Clinton and Barack Obama’s Lovefest on ‘60 Minutes’ |Lauren Ashburn |January 28, 2013 |DAILY BEAST 

From blueberry-free blueberry muffins to nutty cereals with no nuts, how foodmakers hoodwink their customers. Foods That Fudge the Facts |Kate Dailey |January 26, 2011 |DAILY BEAST 

Madame de la Baudraye would have to hoodwink her mother, her husband, her maid, and her mother's maid; that is too much to do. Parisians in the Country |Honore de Balzac 

His whole policy in fact was but a miserable attempt to hoodwink the Spanish people. The War Upon Religion |Rev. Francis A. Cunningham 

Nothing, of course, and so the all-important point was to hoodwink the British commander. Hero Stories from American History |Albert F. Blaisdell 

The assertion that slavery did not exist in the Transvaal is only made to hoodwink the English public. The Last Boer War |H. Rider Haggard 

It was as though he had detected them in a sort of childs play by which they had hoped to hoodwink him. The Young Continentals at Bunker Hill |John T. McIntyre