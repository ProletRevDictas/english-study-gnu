Denied, againThe day after the presidential inauguration, Walston was on her laptop at a friend’s house when her email pinged. The stimulus relieved short-term pain, but eviction’s impact is a long haul |Kyle Swenson |February 8, 2021 |Washington Post 

A worker of Quarters uses her laptop in the community living area of Quarters Co-Living. The Surprising Boom in Pandemic Co-Living |Charu Kasturi |February 4, 2021 |Ozy 

Before his 190-square-foot apartment in San Francisco’s Tenderloin district was connected to the internet, Marvis Phillips depended on a friend with a laptop for his prolific letter-writing campaigns. Getting vaccinated is hard. It’s even harder without the internet. |Eileen Guo |February 3, 2021 |MIT Technology Review 

Its connection ports allow you to hook up to laptops, game consoles, and even a TV Stick, Chromecast, PC, or IOS or Android device—giving you ample options for streaming the live game. Backyard party essentials: Outdoor Super Bowl party ideas for 2021 |PopSci Commerce Team |January 30, 2021 |Popular-Science 

If you’re using a laptop, make sure it’s charging so that there’s no chance of a dead battery interrupting the reset process. How to reset your devices without losing everything |David Nield |January 28, 2021 |Popular-Science 

The hard drive on a BBC laptop was erased and memory cards were wiped clean. Violent Assaults on Reporters in Russia Go Unpunished |Anna Nemtsova |September 18, 2014 |DAILY BEAST 

I have forgotten my laptop on a park bench numerous times, and each time someone brought it back to me. Vilified Bitcoin Tycoon After Losing $500 Million: My Life Is at Risk |Nathalie-Kyoko Stucky |September 17, 2014 |DAILY BEAST 

We can hang out with our friends from the comfort of our laptop. Porn Fights For Your Right to Surf: Pornhub, YouPorn, and Redtube Lead Charge For Net Neutrality |Aurora Snow |September 13, 2014 |DAILY BEAST 

I often sit on the floor and work cross-legged with my laptop. Lumo Lift Vibrates You Into Better Posture |Gregory Ferenstein |August 26, 2014 |DAILY BEAST 

Rawashda is holding his laptop as he points out every nook and cranny of his studio apartment. Gay Palestinians In Israel: The 'Invisible Men' |Itay Hod |August 13, 2014 |DAILY BEAST 

They wouldn't get anything out of my mail -- it got cleared off the server and stored on my laptop at home. Little Brother |Cory Doctorow 

I downloaded it all and kept it on my laptop at home, which downloaded and deleted my mail from the server every sixty seconds. Little Brother |Cory Doctorow 

I powered up my laptop and punched a bunch of pillows into place behind me at the top of the bed. Little Brother |Cory Doctorow 

I figured that building a laptop would be the best way to get the power I wanted at the price I could afford. Little Brother |Cory Doctorow 

A minute later, I had the case open and the keyboard removed and I was staring at the guts of my laptop. Little Brother |Cory Doctorow