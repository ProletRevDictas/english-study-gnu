You can see him in the video, jamming his headphones into the pocket of his black trenchcoat. The Baptism of Michael Brown Sr. and Ferguson’s Baptism by Fire |Justin Glawe |November 27, 2014 |DAILY BEAST 

The crowd is an assortment of reporters, posse members, film students, and very skittish men sporting huge AOL headphones. James and the Giant Internet Company: Franco and AOL Get Kissy Faces |Amy Zimmerman |September 18, 2014 |DAILY BEAST 

I used to sneak the DVD up to my room at night and watch it with headphones in the dark. A Love Letter to ‘The Notebook,’ a Melodrama That Commits to Its Sentimentality |Teo Bugbee |June 26, 2014 |DAILY BEAST 

To keep the child quiet, the adults slap a pair of headphones on the young Wookie as the boy watches a Boba Fett cartoon. The Cult of Boba Fett: The ‘Star Wars’ Bounty Hunter’s Spin-Off |Rich Goldstein |June 5, 2014 |DAILY BEAST 

“Some people bring their kids to the London ones, with sound-proof headphones,” she said. The Drug-Free Breakfast Rave Is New York’s Latest Exercise Trend |Jessica Burdon |May 8, 2014 |DAILY BEAST 

As his watch told him that Tom must be nearing the bottom he seated himself by the switchboard, headphones clamped over his ears. Tom Swift and His Giant Telescope |Victor Appleton 

He reached for the com key and a second later tore the headphones from his appalled ears. Plague Ship |Andre Norton 

They had brought the two-step amplifier and proposed to use that for most of their listening in, rather than the headphones. The Campfire Girls on Station Island |Margaret Penrose 

He took off his headphones and clamped them on to the phonograph that stood on a table near by. The Radio Boys Trailing a Voice |Allen Chapman 

Inside his flying helmet, the doctor wore a pair of headphones which were connected to a box on the floor before him. Astounding Stories, August, 1931 |Various