Outside awarded MountainFlow a Gear of the Show award at last year’s Outdoor Retailer Winter Market, where we look at hundreds of products pitched to us at lightning speed over the course of a few days. A Green Ski-Wax Company Gets Funded on 'Shark Tank' |Joe Jackson |February 9, 2021 |Outside Online 

The video captures a thread of electric current, or lightning leader, zipping down from a thundercloud to meet another leader reaching up from the ground. The birth of a lightning bolt was caught on video |Maria Temming |February 8, 2021 |Science News 

It took a violent mob of insurrectionists and a lightning bolt moment in this very room. Ocasio-Cortez, other Democrats recount on House floor what they experienced during Capitol siege |Amy B Wang, Colby Itkowitz |February 5, 2021 |Washington Post 

Scientists have finally gotten a clear view of the spark that sets off a weird type of lightning called a blue jet. Space station sensors saw how weird ‘blue jet’ lightning forms |Maria Temming |February 2, 2021 |Science News For Students 

The first is like estimating the distance to a lightning strike by timing the delayed arrival of the thunderclap. Rumbles on Mars Raise Hopes of Underground Magma Flows |Robin George Andrews |February 1, 2021 |Quanta Magazine 

Should lightning strike and Hillary Clinton forgoes a presidential run, Democrats have a nominee in waiting. Sen. Warren’s Main Street Crusade to Pressure Clinton |Eleanor Clift |January 8, 2015 |DAILY BEAST 

Second, Michelle served as a lightning rod in the sense of drawing attacks away from other reform groups. A Letter of Thanks to Michelle Rhee |Dmitri Mehlhorn |August 16, 2014 |DAILY BEAST 

He made whatever was going on his own, and with such lightning speed you stopped concentrating. How Mork Melted the Fonz: Henry Winkler Recalls Robin Williams’s Storming ‘Happy Days’ Debut |Tim Teeman |August 12, 2014 |DAILY BEAST 

There were flashes of lightning outside and the rumble of thunder. The Gentle Giant Cut Down by Cops |Michael Daly |July 24, 2014 |DAILY BEAST 

Improvements in lightning tracking help scientists know where to send aircraft to look for fires. Fighting Wildfire With Satellites, Lasers, and Drones |Elizabeth Lopatto |July 9, 2014 |DAILY BEAST 

The left heel followed like lightning, and the right paw also slipped, letting the bear again fall heavily on the ice below. The Giant of the North |R.M. Ballantyne 

Spite, however, of punishments and prohibitions the use of tobacco spread with the rapidity of lightning. Tobacco; Its History, Varieties, Culture, Manufacture and Commerce |E. R. Billings. 

Like lightning he turned and seized by the wrist a man who had already opened the bag and laid hold of some of its contents. The Garret and the Garden |R.M. Ballantyne 

His only worry at the time lay in the dark sky above and the blue-white stabs of lightning that promised an electrical storm. Hooded Detective, Volume III No. 2, January, 1942 |Various 

Another lightning flash blinded the girls and the thunder following fairly deafened them for the moment. The Campfire Girls of Roselawn |Margaret Penrose