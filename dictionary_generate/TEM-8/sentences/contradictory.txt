The challenge with that is it’s completely contradictory to the agency model, which has been built around people, so there’s been a reluctance to build out AI and machine learning. Media Buying Briefing: Why AI is steadily finding its place in agency land |jim cooper |February 15, 2021 |Digiday 

On Conflicting Beliefs and Seeing People as MonstersAccording to Smith, the typical advocate of dehumanizing rhetoric has two contradictory beliefs. Believing in Monsters: David Livingstone Smith on the Subhuman - Facts So Romantic |Eric Schwitzgebel |September 11, 2020 |Nautilus 

As I was reading On Inhumanity, I was wondering how much Smith’s commitment to contradictory beliefs matters. Believing in Monsters: David Livingstone Smith on the Subhuman - Facts So Romantic |Eric Schwitzgebel |September 11, 2020 |Nautilus 

On this approach to belief, contradictory belief is impossible. Believing in Monsters: David Livingstone Smith on the Subhuman - Facts So Romantic |Eric Schwitzgebel |September 11, 2020 |Nautilus 

Data on how much infectious virus is present in the noses of asymptomatic people are contradictory, Kilpatrick says. How often do asymptomatic people spread the coronavirus? It’s unclear |Erin Garcia de Jesus |June 9, 2020 |Science News 

America presents two contradictory narratives that it struggles to reconcile. Will Texas Stay Texan? |David Fontana |December 29, 2014 |DAILY BEAST 

My optimism springs from the contradictory statements of the conservative justices of the court. The Supreme Court Is Weighing Corporate Power Yet Again |Zephyr Teachout |October 17, 2014 |DAILY BEAST 

His teammates go further, doubting whether anybody fathoms the consistently contradictory Palmer--least of all Palmer. Will the Real Jim Palmer Please Stand Up |Tom Boswell |September 27, 2014 |DAILY BEAST 

Consolmango writes that the Bible actually contains several contradictory Creation theories. Pope Francis Asked ‘Would You Baptize an Alien?’ Here’s the Answer. |Barbie Latza Nadeau |September 26, 2014 |DAILY BEAST 

The upshot of these contradictory findings is that most people have trouble believing the figures produced by either side. Scots Must Choose Heart or Head |Nico Hines |September 18, 2014 |DAILY BEAST 

By men the laws of a nation may be altered without being made contradictory to one another, or to oppose the law of God. The Ordinance of Covenanting |John Cunningham 

He was preserved from the fantastic by another gift which seems contradictory to the first. Repertory Of The Comedie Humaine, Complete, A -- Z |Anatole Cerfberr and Jules Franois Christophe 

A word on the contradictory education which we bestow upon our daughters. A Philosophical Dictionary, Volume 1 (of 10) |Franois-Marie Arouet (AKA Voltaire) 

But how are we to reconcile improbable facts related in a contradictory manner? A Philosophical Dictionary, Volume 1 (of 10) |Franois-Marie Arouet (AKA Voltaire) 

How, said they, could they assist by their presence at public prayers which were utterly contradictory to their private ones? The English Church in the Eighteenth Century |Charles J. Abbey and John H. Overton