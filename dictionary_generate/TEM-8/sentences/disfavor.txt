Egeblad is experimenting with a once-promising treatment that had fallen into disfavor because it also involved dangerous bacterial pathogens. Triggering the Body’s Defenses to Fight Cancer - Issue 108: Change |Lina Zeldovich |November 3, 2021 |Nautilus 

There are people with religious beliefs that disfavor all those groups. Right-Wing Activists: Yep, ‘Religious Freedom’ Protects Discrimination Against Jews |Jay Michaelson |June 14, 2014 |DAILY BEAST 

The Israelis also disfavor negotiations at this point, particularly if they include Iran. Why Now Is the Time For Syria Diplomacy |Trita Parsi |May 16, 2013 |DAILY BEAST 

But at this early juncture, with the Republican Party in disarray and disfavor, Hillary looks like the one to beat. Hillary Clinton in 2016: Be Afraid, Republicans |Lloyd Green |February 3, 2013 |DAILY BEAST 

The von Fleischl has long been the standard instrument, but has lately fallen into some disfavor. A Manual of Clinical Diagnosis |James Campbell Todd 

Another consideration explains the historical and popular disfavor in which Catherine is held. Catherine de' Medici |Honore de Balzac 

Finkenstein and Kalkstein were always covertly rather of the Queen's party, and now stand reprimanded, and in marked disfavor. History Of Friedrich II. of Prussia, Vol. VII. (of XXI.) |Thomas Carlyle 

Mr. Bangs's fish eyes regarded him with glittering disfavor. Quin |Alice Hegan Rice 

"So ignominious," said Sylvia, looking over her person with disfavor. The Opened Shutters |Clara Louise Burnham