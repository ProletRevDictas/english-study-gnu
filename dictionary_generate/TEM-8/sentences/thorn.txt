Since iAd’s closure, Apple has become a thorn in the side of the online ad industry. ‘It’s the first time they’re listening’: Apple is striking a more conciliatory tone with the ad industry |Lara O'Reilly |September 8, 2020 |Digiday 

The Tampa Bay Rays gave New York a good fight early last season and were also a thorn in the Houston Astros’ side during the postseason. Baseball Will Be Weird This Year. But The Astros And Yankees Are Favorites In The American League … Again. |Neil Paine (neil.paine@fivethirtyeight.com) |July 21, 2020 |FiveThirtyEight 

Some fish may be eating only the crown-of-thorns’ tiny, squishy larvae. Fish poop exposes what eats the destructive crown-of-thorns starfish |Jake Buehler |June 8, 2020 |Science News 

Adorned with spikes and toxins, crown-of-thorns starfish aren’t an easy meal. Fish poop exposes what eats the destructive crown-of-thorns starfish |Jake Buehler |June 8, 2020 |Science News 

Yet occasional starfish population booms suggest something is normally eating live, healthy crown-of-thorns and keeping their numbers in check. Fish poop exposes what eats the destructive crown-of-thorns starfish |Jake Buehler |June 8, 2020 |Science News 

Thorn also posted a video on his personal YouTube page wherein he desk-dances to Taylor Swift. Jimmy Kimmel Pranks Kids (Again), Taylor Swift’s 1989 Aerobics, and More Viral Videos |The Daily Beast Video |November 9, 2014 |DAILY BEAST 

But the system that Battle represents is a constant thorn in their side. Meet the Beer Bottle Dictator |Tim Mak |August 12, 2014 |DAILY BEAST 

That freedom has been a thorn in the side of many cardinals who feel the sisters should be more conservative. American Nuns Hope For Sister-Friendly New Pope |Barbie Latza Nadeau |February 13, 2013 |DAILY BEAST 

A tabletop bronze of a boy pulling a thorn from his foot, made around 1500 by the Renaissance sculptor known as Antico. A Roman Boy-Toy Gets a Dye Job |Blake Gopnik |January 10, 2013 |DAILY BEAST 

There are just as many covetable skirts and delicate silk tap shorts as there are thorn-cupped bras and barely-there g-strings. We Want: Fleur Du Mal |Misty White Sidell |December 4, 2012 |DAILY BEAST 

Taylor alludes to several made from the well known Glastonbury thorn. Tobacco; Its History, Varieties, Culture, Manufacture and Commerce |E. R. Billings. 

I feel as if he were a little child crying with a thorn in his finger, and he had no mother to take it out. Tessa Wadsworth's Discipline |Jennie M. Drinkwater 

He lay motionless in her lap, until the thorn suddenly let go and lay in Jess' hand. The Box-Car Children |Gertrude Chandler Warner 

Then she held the soft paw firmly with her left hand, and pulled steadily on the thorn with her right hand. The Box-Car Children |Gertrude Chandler Warner 

She received the infection on a part of the hand which had been previously in a slight degree injured by a scratch from a thorn. An Inquiry into the Causes and Effects of the Variolae Vaccinae |Edward Jenner