Protecting its most valued informant may well explain why it took decades to bring the Sixteenth Street Church bombers to justice. 40 years for Justice: Did the FBI Cover for the Birmingham Bombers? |Gary May |September 15, 2013 |DAILY BEAST 

Was Rowe involved in the bombing of the Sixteenth Street Baptist Church? 40 years for Justice: Did the FBI Cover for the Birmingham Bombers? |Gary May |September 15, 2013 |DAILY BEAST 

In the French Mustel reed organ the first touch is operated by depressing the keys about a sixteenth part of an inch. The Recent Revolution in Organ Building |George Laing Miller 

Virginia now ranks sixteenth in population among the fifty states of the United States. Hallowed Heritage: The Life of Virginia |Dorothy M. Torpey 

We will now take the reader to the primitive tobacco plantations of America about the middle of the Sixteenth Century. Tobacco; Its History, Varieties, Culture, Manufacture and Commerce |E. R. Billings. 

In a special alcove was a large number of priceless Fourteenth and Sixteenth Century editions. Ancestors |Gertrude Atherton 

Ducking back through the firedoor, he ran quickly up to the sixteenth floor, up past the fifteenth. Hooded Detective, Volume III No. 2, January, 1942 |Various