All would attest to the manifest goodness that inspired the perfect nickname for the boy who would become a perfect cop. In The Shadow of Murdered Cops |Michael Daly |December 26, 2014 |DAILY BEAST 

Survivors of Ebola, as thousands in West Africa can attest to, can—and often do—make a full recovery. The Sham, Scaremongering Guide to Ebola |Abby Haglage |November 20, 2014 |DAILY BEAST 

Her father can attest to that, having seen her in action in a Brooklyn courtroom. For Next AG, Obama Picks a Quiet Fighter With a Heavy Punch |Michael Daly |November 8, 2014 |DAILY BEAST 

The Yelp reviews attest to the diverse crowd that frequents Orchard Corset Center. New York's Sexiest Kosher Corsets |Emily Shire |August 20, 2014 |DAILY BEAST 

Anyone familiar with the events of four years ago can attest to that. The Decision 2: LeBron James Trolls the World… Again |Marlow Stern |July 11, 2014 |DAILY BEAST 

Just corporeal enough to attest humanity, yet sufficiently transparent to let the celestial origin shine through. Pearls of Thought |Maturin M. Ballou 

Tausig, in my opinion, did possess exceptional genius in composition, though he left but few works behind him to attest it. Music-Study in Germany |Amy Fay 

The Gentleman's Magazine contains a long list of the bridges and churches which attest his reputation and skill. The Every Day Book of History and Chronology |Joel Munsell 

That he was an autocratic chairman, his brother directors, were they now living, would I am sure attest. Fifty Years of Railway Life in England, Scotland and Ireland |Joseph Tatlow 

If the letter was preserved the seal was kept attached to it in order to attest its authenticity. The Private Life of the Romans |Harold Whetstone Johnston