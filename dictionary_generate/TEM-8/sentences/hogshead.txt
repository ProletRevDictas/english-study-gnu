“If BMW is ‘the ultimate driving machine,’ your Anthem is the ultimate differentiator,” writes Hogshead. Can Self-Help Books Really Make a New You? |Lizzie Crocker |December 29, 2014 |DAILY BEAST 

A blob of blue smoke curled out of a hole the size of a hogshead in a steep bank overhung with alders. Cabin Fever |B. M. Bower 

He first tried his machine with only two pounds of powder on a hogshead loaded with stones. Stories of Our Naval Heroes |Various 

The powder was set on fire, and up went the stones and the boards of the hogshead and a body of water, many feet into the air. Stories of Our Naval Heroes |Various 

Behind him, the Priests at the golden hogshead, now set free to taste the wine themselves, had lost no time. Pagan Passions |Gordon Randall Garrett 

The jar here means a big affair about half the size of a hogshead: I bathed in one this morning. Where Half The World Is Waking Up |Clarence Poe