Limbro was excellent, and I kind of loved how the show made no attempt to justify its obvious artifice. Two Feminists Who Love Reality TV Wrap Their Minds Around the FBoy Island Finale |Eliana Dockterman |August 13, 2021 |Time 

Her large-scale photographs spike tired conventions of documentary portraiture with freewheeling artifice and invention. Dawoud Bey, Jasper Johns and ‘Automania’ are among the many compelling reasons to visit museums this summer |Sebastian Smee |June 11, 2021 |Washington Post 

Like all of the show’s greatest set pieces, it was a stunning fusion of high artifice and deeply felt emotion. Pose Was More Than Just a TV Show. It Changed the Culture, Onscreen and Off |Judy Berman |June 3, 2021 |Time 

It might seem out of place on our busy modern calendars — a day shorn, like Rogers’ Hallmark tree, of artifice and pomp. We Need More Kindness in Our Lives: Let's Make 143 Day a National Holiday to Honor Mr. Rogers' Legacy |Gregg Behr and Ryan Rydzewski |May 21, 2021 |Time 

Liza’s authentic warmth is such a refreshing antidote to Halston’s chilly artifice that a full-on dual portrait might have worked better to deepen both characters. Netflix's Lush, Disappointing Halston Struggles to Say Something New About the Legendary Fashion Designer |Judy Berman |May 14, 2021 |Time 

There is usually something transparent about the artifice required by famous artists trying to remain current. The Many Lives of Artist David Hockney |William O’Connor |November 23, 2014 |DAILY BEAST 

Bratis, who trained in Athens, creates pieces of “femininity and pure elegance without artifice.” Is This the End of CR Fashion Book?; Armani Names New Protégé |The Fashion Beast Team |August 1, 2014 |DAILY BEAST 

Point of view is inconsistent and Berg employs enough obvious filmic artifice to keep reminding us we are watching dramatization. The Myth of Reality in ‘Lone Survivor’ |Benjamin Busch |January 19, 2014 |DAILY BEAST 

“Faye always tried to advance his own idea of the institution by judicial artifice,” Lecourt told Le Monde. One Last Derrida Attack |David Sessions |July 19, 2013 |DAILY BEAST 

The collection is by turn bizarre, hilarious, unpredictable, all of it without a single note of artifice. Khaled Hosseini’s Favorite Story Collections |Khaled Hosseini |May 21, 2013 |DAILY BEAST 

He used every artifice to prevent a collision between the French and Neapolitan troops. Napoleon's Marshals |R. P. Dunn-Pattison 

This way of owning Guilt in a wrong Place, is a common Artifice to hide it in a right one. A Letter from Mr. Cibber to Mr. Pope |Colley Cibber 

Artifice is always strictly subordinated, and the poet seems to sing spontaneously. Frdric Mistral |Charles Alfred Downer 

Then you would recommend rank rebellion, either by force or artifice, according as circumstances might require? The Daughters of Danaus |Mona Caird 

He listened, fascinated, incredulous, asking himself if human artifice could invent such a history. The Woman Gives |Owen Johnson