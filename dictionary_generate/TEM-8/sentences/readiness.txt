Ninety percent of Fairfax staff members have either requested or scheduled vaccination appointments, he said, and many have indicated their readiness to return to teaching in-person in the meantime. Virginia’s largest school systems vow to reopen classrooms for all by March |Hannah Natanson |February 3, 2021 |Washington Post 

To test the jam’s readiness, spoon a bit of jam onto the chilled plate, and run your finger through it. This twist on the classic Linzer cookie adds gentle sweetness and texture with corn flour |Roxana Jullapat |December 2, 2020 |Washington Post 

The government’s vaccine program, Operation Warp Speed, has projected optimism about its readiness to distribute the vaccine. Most States Aren’t Ready to Distribute the Leading COVID-19 Vaccine |by Isaac Arnsdorf, Ryan Gabrielson and Caroline Chen |November 10, 2020 |ProPublica 

The idea went nowhere amid questions about the screenings’ effectiveness and the agency’s readiness to take on a significant public health responsibility. An Iowa airport has a plan to screen passengers for the coronavirus. It’s being held up by the FAA. |Ian Duncan |November 4, 2020 |Washington Post 

This federal-level hub could monitor viral trends and track the readiness of supply chains for therapies and protective equipment. We already know how to keep the next pandemic from catching us off guard |Rob Verger |October 8, 2020 |Popular-Science 

There is a rough floor of about six aircraft that a unit is supposed to have even at low readiness levels. Navy Grounds Top Guns |Dave Majumdar |October 17, 2014 |DAILY BEAST 

The lesson we learned is to increase our readiness and responsiveness. Shocked by Ukraine Violence, NATO Prepares to Face Down Putin |Leo Cendrowicz |October 12, 2014 |DAILY BEAST 

The one thing everyone seems to agree on is readiness of the Internet. YouTube, Netflix, and the Death of Television |Rich Goldstein |November 14, 2013 |DAILY BEAST 

In this “new” Arab world, there is growing curiosity and readiness to challenge the conventions of the old regimes. 5 Arab Spring Opportunities For Israel |Nimrod Goren, Elie Podeh |June 20, 2013 |DAILY BEAST 

"Netanyahu signals readiness to consider 2002 Arab peace plan" read the Reuters headline. Why Bibi Won't Really 'Give Peace A Chance' |Elisheva Goldberg |June 5, 2013 |DAILY BEAST 

And he stood up in the shameful fall of the people: in the goodness and readiness of his soul he appeased God for Israel. The Bible, Douay-Rheims Version |Various 

In the street the men of his escort sat their horses, having mounted at his bidding in readiness for the journey. St. Martin's Summer |Rafael Sabatini 

She is always in fear of an earthquake, and feels safer to have a light burning in readiness all night long. Alila, Our Little Philippine Cousin |Mary Hazelton Wade 

The brilliant ceremony over, the Prince set out for Carlisle, where his army was ordered to be in readiness on July 8. King Robert the Bruce |A. F. Murison 

I wish you to write me if I am to give him notice also to hold himself in readiness for town. Life of Richard Trevithick, Volume II (of 2) |Francis Trevithick