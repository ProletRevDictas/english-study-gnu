The infamous Cutter pharmaceutical disaster of 1955 refers to the tragedy that occurred when companies rushed to produce the polio vaccines developed by Jonas Salk. On COVID-19 vaccines, Big Pharma knows to just say ‘no’ |matthewheimer |September 11, 2020 |Fortune 

Though we tried to remember to take it out of the water and secure it to the top of the boat during the big rapids, it may or may not have taken a ride down the infamous Crystal while clipped to the outside of the rig. The Gear You Need to Bring on a 225-Mile River Trip |Mitch Breton |September 6, 2020 |Outside Online 

It also mentions various cryptocurrency Ponzi schemes, such as the infamous OneCoin. ‘Colonialism’ and crypto claims: Why the .io domain name extension faces an uncertain future |David Meyer |August 31, 2020 |Fortune 

I think the most infamous example of this is in 2018 when Mark Zuckerberg testified to the Senate after the Cambridge Analytica scandal. Podcast: Want consumer privacy? Try China |Michael Reilly |August 19, 2020 |MIT Technology Review 

One particularly infamous hostile mob is a creeper, a dark green, frowning humanoid with a passion for blowing up. Words From Minecraft: What Are Your Kids Saying? |Minrose Straussman |July 8, 2020 |Everything After Z 

The main effort in the attack was carried out by the infamous Waffen SS 6th Panzer Army. Hitler’s Hail Mary |James A. Warren |December 20, 2014 |DAILY BEAST 

Do they not recall the appalling ramifications of the infamous moment when Michelle Obama put her arm around the Queen in 2009? LeBron's Touchy Feely Protocol Breach |Tom Sykes |December 9, 2014 |DAILY BEAST 

A notably large Irish contingent took part in the infamous draft riots because they did not want to compete for jobs with blacks. This Week's Riots Are Part of America's Long History of Racial Rage |Sharon Adarlo |November 29, 2014 |DAILY BEAST 

The most infamous case was that of Kanae Kijima, a onetime housekeeper who became a highly paid mistress. Beware of Japan’s “Black Widows” |Jake Adelstein |November 20, 2014 |DAILY BEAST 

The reality star bared her infamous behind on the cover of Paper magazine, and the web went wild. Kim Kardashian Bares Her Shiny, Bounteous Butt, Breaks the Internet |Marlow Stern |November 12, 2014 |DAILY BEAST 

For twenty years you hold an innocent and virtuous woman under an infamous suspicion. The Joyous Adventures of Aristide Pujol |William J. Locke 

The Jacobins now made a direct and infamous attempt to turn the rage of the populace against Madame Roland. Madame Roland, Makers of History |John S. C. Abbott 

Those letters incriminate you to the full in this infamous matter here at Condillac. St. Martin's Summer |Rafael Sabatini 

He condoned the infamous conduct of the police officer Contenson. Repertory Of The Comedie Humaine, Complete, A -- Z |Anatole Cerfberr and Jules Franois Christophe 

Voltaire exerted himself as warmly against this infamous act, as he had against the execution of Calas. The Every Day Book of History and Chronology |Joel Munsell