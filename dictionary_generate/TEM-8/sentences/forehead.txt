The hearse passed by, and made its way toward Arlington, where TJ Kolwicz, a Fairfax County police officer, watched with a hand on his forehead. Officer Brian Sicknick remembered as hero who died defending the U.S. Capitol |Meagan Flynn, Emily Davies, Paul M. Duggan |February 3, 2021 |Washington Post 

He had been raised by a grandmother who attacked him when she drank and left him with a V-shaped scar on his forehead. To stay or to go? |Hannah Dreier |December 26, 2020 |Washington Post 

There are only three ways to pay—whether it’s on a card, a phone, a fingerprint, or you and I looking at each other and touching our foreheads in the future. Why Mastercard isn’t a credit card company, according to its outgoing CEO Ajay Banga |cleaf2013 |December 3, 2020 |Fortune 

He hit him on the forehead in the second round, knocking him down. Nate Robinson’s boxing debut ends with second-round knockout by Jake Paul |Cindy Boren |November 29, 2020 |Washington Post 

At the same time, the “3-and-D” label that’s been written across these players’ foreheads over the past couple of months undersells everything else they can already do, along with the skills they might still develop. These 3 NBA Draft Prospects Are Perfect Fits For The Modern League |Michael Pina |November 17, 2020 |FiveThirtyEight 

His sons kissed his forehead as the judge announced the verdict. Mubarak’s Acquittal Signals Complete Triumph of Military Over Arab Spring |Jamie Dettmer |November 29, 2014 |DAILY BEAST 

I fired again, and as he fell the second round hit him in the forehead, just above the left eye. I Shot Bin Laden |Elliot Ackerman |November 16, 2014 |DAILY BEAST 

Whispering to himself and bowing, he touches his forehead to the ground. Heart of Darkness: Into Afghanistan’s Taliban Valley |Matt Trevithick, Daniel Seckman |November 15, 2014 |DAILY BEAST 

Somehow she survived, even though one of the bullets struck her in the forehead. Malala Yousafzai Is the Youngest Nobel Peace Prize Winner in History |Nico Hines |October 10, 2014 |DAILY BEAST 

He stood barefoot, his neck covered with interlocking black tattooed swirls, the word HELL inked into his forehead. Inside a Hospital for the Criminally Insane |Caitlin Dickson |September 15, 2014 |DAILY BEAST 

His hat was pushed back from his forehead, the collar of his blue flannel shirt was open. The Bondboy |George W. (George Washington) Ogden 

He rose and kissed her lightly on the forehead, experience teaching him to avoid a stray hair from the carefully built coiffure. Ancestors |Gertrude Atherton 

The Empress's hand was on her beating forehead, but she turned, even fiercely to his question. The Pastor's Fire-side Vol. 3 of 4 |Jane Porter 

The voice died out in a broken whisper, and two hot tears fell on Black Sheep's forehead. Kipling Stories and Poems Every Child Should Know, Book II |Rudyard Kipling 

Beads of perspiration gathered on my forehead as I slowly rose to my feet and faced Giles. Uncanny Tales |Various