The commissioner had heard screaming, looked outside and saw a father pushing a baby in a stroller accompanied by another toddler moving away from a person the witness described as a vagrant, who was following them with a brick, Krepp said. Father and child attacked with brick on Capitol Hill, officials say |Justin George |December 19, 2021 |Washington Post 

Among songbirds, Dufour says, vagrants are always young birds. Some songbirds now migrate east to west. Climate change may play a role |Jake Buehler |November 9, 2021 |Science News 

Occasionally, “vagrant” birds get lost and show up far from this range, including in Europe. Some songbirds now migrate east to west. Climate change may play a role |Jake Buehler |November 9, 2021 |Science News 

I’d gotten wind via the eBird Rare Bird Alert that a vagrant woodcock had been spotted along the Rio Grande near Albuquerque, New Mexico, just 15 minutes from my house. How eBird Changed Birding Forever |Jessie Williamson |December 4, 2020 |Outside Online 

In an interview, Liang said, “Air should be the most valueless commodity, free to breathe for any vagrant or beggar.” The Chinese Can’t Catch Their Breath |Brendon Hong |May 5, 2014 |DAILY BEAST 

Yet the word vagrant is a misnomer in this city, where economy has reached a finesse that is marvelous. The Real Latin Quarter |F. Berkeley Smith 

For the first time Bud had a vagrant suspicion that Foster had not told quite all there was to tell about this trip. Cabin Fever |B. M. Bower 

Orders were issued to the boards of management of the newly created vagrant districts, telling them that they need not meet. English Poor Law Policy |Sidney Webb 

Yet even for the professional vagrant the promiscuous London casual ward of 1864 was not to be extended. English Poor Law Policy |Sidney Webb 

Says one of the characters, referring to the importunities of a tipsy vagrant, “Give him half-a-crown!” A Cursory History of Swearing |Julian Sharman