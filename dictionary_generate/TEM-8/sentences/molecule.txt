Perhaps certain gene networks and signaling molecules act as individuals at the level of the cell, while others are spread out between cells. What Is an Individual? Biology Seeks Clues in Information Theory. |Jordana Cepelewicz |July 16, 2020 |Quanta Magazine 

Strikingly, if the flies are fed antioxidants that neutralize these molecules, it does not matter if they never sleep again. Why Sleep Deprivation Kills |Veronique Greenwood |June 4, 2020 |Quanta Magazine 

They might perfume the air by releasing small molecules that smell like a whiff of strawberries, perhaps. This ‘living’ concrete slurps up a greenhouse gas |Carolyn Wilke |May 6, 2020 |Science News For Students 

As water heats up, individual water molecules move faster and faster. Rock Candy Science 2: No such thing as too much sugar |Bethany Brookshire |April 30, 2020 |Science News For Students 

Ions — molecules that have a charge — move between these electrodes in a material called an electrolyte. Batteries should not burst into flames |Carolyn Wilke |April 16, 2020 |Science News For Students 

Each type of atom and molecule has its own unique spectrum, according to the rules of quantum mechanics. SAMI Is Like Google Earth for the Universe |Matthew R. Francis |July 27, 2014 |DAILY BEAST 

Specifically, it targets a molecule called VEGF that sends signals that promote the growth of blood vessels. Jobs’s Unorthodox Treatment |Sharon Begley |October 6, 2011 |DAILY BEAST 

And those examples are a molecule in the atom bomb of the Internet. Pro Sports’ Hero Deficit |Buzz Bissinger |July 6, 2011 |DAILY BEAST 

Every fructose molecule in sucrose, in contrast, is bound to a glucose. The Fight Over High-Fructose Corn Syrup |Sharon Begley |February 28, 2011 |DAILY BEAST 

A molecule of carbon dioxide traps heat, radiant heat, the long wave end of the spectrum. Hanging Out with Ian McEwan: Full Transcript |The Daily Beast Video |April 14, 2010 |DAILY BEAST 

Pete, after spewing the last hateful molecule away, reversed his tiny fibre engines, and began to draw in. Old Friends Are the Best |Jack Sharkey 

Does not the soul which animates us endure by the same right as each molecule of oxygen or nitrogen or iron? Urania |Camille Flammarion 

The foreign protein is changed by splitting its molecule into its simplest parts and then recombining them in the desired form. The Treatment of Hay Fever |George Frederick Laidlaw 

Each molecule of silver chromate forms two silver ions when it is ionized. The Elements of Qualitative Chemical Analysis, vol. 1, parts 1 and 2. |Julius Stieglitz 

This brings selenophene more akin to pyrrole than thiophene, but the group -NH- in the molecule of pyrrole is an auxochrome. Synthesis of 2-methyl-4-selenoquinazolone, 2-phenylbenzoselenazole, and its derivatives |Y-Gwan Chen