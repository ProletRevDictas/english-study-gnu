Made in collaboration with New Hampshire artisan Linny Kenney, these hand-dyed vegetable-tanned leather gloves are meant to age beautifully while protecting your hands from hot surfaces. From the Cut: 33 Valentine’s Day Gifts for the Foodie in Your Life |The Cut Staff |February 8, 2021 |Eater 

The bar sourced tools by local artisans like the Parra family in Michoacan, who have been working copper for generations. Someone Please Buy Me This Gorgeous Cocktail Set |Nick Mancall-Bitel |December 30, 2020 |Eater 

Many of the artisans have intellectual and other disabilities, and about half are nonverbal. Need a last-minute gift? How about a camel? A different kind of year has groups hoping shoppers will turn to a different kind of giving. |Theresa Vargas |December 16, 2020 |Washington Post 

Although the Kimdio has the look of an artisan coffee mug, it’s actually made of high quality ceramic that is dishwasher and microwave-save, and non-toxic. Coffee (or tea!) mugs that make for easy gifts |PopSci Commerce Team |September 29, 2020 |Popular-Science 

On Trade Days, my cousins would sell their pottery to tourists, while other artisans would sell paintings, woodworking, and other objets d’art. A California mountain community loses its heart |Mike Silverstein |September 10, 2020 |Washington Blade 

The relationship between artist and artisan is therefore very symbiotic, says Hetflaisz. Damien Hirst’s Army of Geppettos |Tim Teeman |December 2, 2014 |DAILY BEAST 

Szymon Oltarzewski is a Polish-born artisan who is also a sculptor in his own right. Damien Hirst’s Army of Geppettos |Tim Teeman |December 2, 2014 |DAILY BEAST 

When the artist themselves is often continents away, those artistic decisions are made by the artisan. Damien Hirst’s Army of Geppettos |Tim Teeman |December 2, 2014 |DAILY BEAST 

But from the anguish of soulless industrial lagers rises the emancipation of artisan brewing. House of the Witch: The Renegade Craft Brewers of Panama |Jeff Campagna |November 30, 2014 |DAILY BEAST 

But big corporations are taking shortcuts and pricing the artisan products out of the bar...and out of business. Grab A Shot Glass: Craft Tequila Needs Your Help |Kayleigh Kulp |September 7, 2014 |DAILY BEAST 

Here and there exceptional industry or extraordinary capacity raised the artisan to wealth and turned the "man" into the "master." The Unsolved Riddle of Social Justice |Stephen Leacock 

These Orchestras are chiefly selected from the ranks of the people, of whom the artisan is the chief contributor. Violins and Violin Makers |Joseph Pearce 

If an artisan has taken a son to bring up, and has caused him to learn his handicraft, no one has any claim. The Oldest Code of Laws in the World |Hammurabi, King of Babylon 

I am a mere artisan in composition, but you will be an artist, in the fullest sense of the word. The Life &amp; Letters of Peter Ilich Tchaikovsky |Modeste Tchaikovsky 

She was the child of an artisan who had been killed by the Burgundians in the woods of Boulogne-la-Grande. The Merrie Tales Of Jacques Tournebroche |Anatole France