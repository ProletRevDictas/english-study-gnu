According to NBA Advanced Stats, Curry’s efficiency fluctuates heavily depending on Green’s presence. Draymond Green Isn’t Scoring, But He’s Doing Everything Else For The Warriors |James L. Jackson |February 4, 2021 |FiveThirtyEight 

Add the potatoes and stir to coat them with the curry-oil mixture. Trinidad-style aloo and channa infuses an Indian classic with Caribbean flavor |Brigid Washington |January 22, 2021 |Washington Post 

In an email to Voice of San Diego, Curry called the disagreement a misunderstanding. Charter School Backs Off Push to Reopen After Ultimatum to Teachers |Kayla Jimenez |January 11, 2021 |Voice of San Diego 

In April, when my pandemic despair was especially keen and hopeless, I threw myself into unusual kitchen projects, and none am I more grateful for than having made Sonoko Sakai’s curry bricks with her curry brick kit. 20 Days of Turkey |Meghan McCarron |December 23, 2020 |Eater 

We should note here, too, that the Mavericks — who got Josh Richardson in exchange for Curry — also seemed to get a great deal. The Upsides And Downsides Of The NBA’s Five Biggest Trades So Far |Chris Herring (chris.herring@fivethirtyeight.com) |November 19, 2020 |FiveThirtyEight 

But behind them are ISIS fighters and sympathizers and locals eager to curry favor by selling out their neighbors. Darkness at Noon Prayers: Inside the Islamic Police State |Jamie Dettmer |November 5, 2014 |DAILY BEAST 

Curry was intrigued and, unlike the other filmmakers VanDyke consulted, he had time to take on a major project. ‘Point and Shoot’ Captures the Birth of a Warrior on Film |Lloyd Grove |October 31, 2014 |DAILY BEAST 

Stanley then went on to praise Curry after slamming her, a technique she would later use in her Rhimes piece. A Sexism Problem at The New York Times |Keli Goff |September 23, 2014 |DAILY BEAST 

"It's rubbed with curry and five-spice powder," he explains as waiters take notes. A Magical Meal at Louie’s Backyard in the Conch Republic |Jane & Michael Stern |July 13, 2014 |DAILY BEAST 

But Irons actually faced competition from Tim Curry and Malcolm McDowell, who were both considered for the part. ‘The Lion King’ Turns 20: Every Crazy, Weird Fact About the Disney Classic |Kevin Fallon |June 24, 2014 |DAILY BEAST 

A-course, Mrs. Bridger got a nice little pile of money fer it, and paid Curry the balance she owed him. Alec Lloyd, Cowpuncher |Eleanor Gates 

All the dishes, with the exception of the curry, are prepared after the English fashion, although the cooks are Chinese. A Woman's Journey Round the World |Ida Pfeiffer 

They all had immense plates of rice, and little bowls full of curry; a few pieces of dried fish supplied the place of bread. A Woman's Journey Round the World |Ida Pfeiffer 

He wished to capture the Greek girl, that he might curry favour with the Pasha Ibrahim by presenting her to him. Jack Harkaway's Boy Tinker Among The Turks |Bracebridge Hemyng 

He is stomach-plagued and old, And his curry soups will make thy cheek the colour of his gold. The Book of Humorous Verse |Various