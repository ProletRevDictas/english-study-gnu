In order to be covered by the moratorium, renters have to submit paperwork demonstrating they make less than $99,000 per earner and that homelessness or living with others in close proximity would be their only options if evicted. In 2021, a new wave of evictions could also fuel a rise in COVID cases |Kat Eschner |January 4, 2021 |Popular-Science 

Landlords can still evict tenants for unpaid rent if the tenant refuses to apply for or cooperate with rent relief. Trying to enjoy the holidays with eviction looming in the new year |Kyle Swenson |December 27, 2020 |Washington Post 

He said evicting people would lead some to double- or triple-up with family or friends, which could exacerbate the spread of the virus. Struggling renters face avalanche of evictions without federal aid |Jonathan O'Connell, Anu Narayanswamy |December 25, 2020 |Washington Post 

In September, after months of underpaying rent, Long was evicted from her $500-a month apartment. A time to splurge or a time to scrape by: Holidays expose widening disparity among U.S. families |Abha Bhattarai |December 17, 2020 |Washington Post 

Five landlords eventually challenged the legality of the moratorium on filing an eviction and a second moratorium on evicting tenants during the course of a state of emergency. D.C.’s eviction filing moratorium doesn’t pass ‘constitutional muster,’ judge rules |Kyle Swenson |December 16, 2020 |Washington Post 

In June, Pakistan launched an all-out military offensive in the region, ostensibly to evict all the militants from the area. Obama’s Deadly Informants: The Drone Spotters of Pakistan |Umar Farooq, Syed Fakhar Kakakhel |November 12, 2014 |DAILY BEAST 

A California family is struggling to evict their now-fired live-in nanny—and tenancy laws are on her side. When Mrs. Doubtfire Won’t Leave |Keli Goff |June 28, 2014 |DAILY BEAST 

Police came to evict Mark Kulis—and found a home wired to explode. When Sovereign Citizens Snap |Caitlin Dickson |January 17, 2014 |DAILY BEAST 

Tried to evict a guy who hadnt paid his mortgage in a few years. 13 Revelations From Reddit’s ‘Dirty Little Secret’ Thread Exposing Alleged Industry Secrets |Nina Strochlic |August 23, 2013 |DAILY BEAST 

The Israeli army even formally announced it wouldn't evict the Bab al-Shams until Obama left for Jordan on Friday. Obama's Presence Shields Palestinian "Anti-Settlement," For Now |Ali Gharib |March 21, 2013 |DAILY BEAST 

Certain it is that in the putting in force of the right to evict a tenant the landlord is very long-suffering. The Law and the Poor |Edward Abbott Parry 

However, after the battle the badgers ceased to try and evict him. Lives of the Fur Folk |M. D. Haviland 

He said he liked the place and meant to stay there, and if possible evict the ghost. Stranger Than Fiction |Mary L. Lewes 

Buy them out or evict them, and then the two furlongs will consist of nothing but demesne land and glebe. The Agrarian Problem in the Sixteenth Century |Richard Henry Tawney 

He never evicts a tenant, nor even threatens to evict those who vote against him. The Land-War In Ireland (1870) |James Godkin