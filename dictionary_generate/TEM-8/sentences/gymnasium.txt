In larger spaces, such as cafeterias or gymnasiums, children might sit 10 feet apart, he said. Arlington, Alexandria move forward with school reopening plans |Hannah Natanson |February 19, 2021 |Washington Post 

He asked a man mopping the gymnasium floor where he could find the principal. John Moylan, who led DeMatha High School for decades, dies at 88 |Bart Barnes |February 2, 2021 |Washington Post 

Wielding batons and shields, officers marched the prisoners into a gymnasium and conducted a series of strip-searches, according to a lawsuit the women filed in federal court. The Way Prisoners Flag Guard Abuse, Inadequate Health Care and Unsanitary Conditions Is Broken |by Shannon Heffernan, WBEZ |December 2, 2020 |ProPublica 

He lives alone in a Madison apartment, studies mostly online, works out at a gymnasium, and shops and cooks for himself. College students hit the road after an eerie pandemic semester. Will the virus go home with them? |Nick Anderson, Susan Svrluga |November 22, 2020 |Washington Post 

Another video showed a life-size whale splashing through a school gymnasium. Magic Leap tried to create an alternate reality. Its founder was already in one |Verne Kopytoff |September 26, 2020 |Fortune 

They stormed the gymnasium by land one sunny spring day in 1904. The GOP’s Last Identity Crisis Remade U.S. Politics |Michael Wolraich |July 24, 2014 |DAILY BEAST 

Two parallel fences tipped with barbed wire formed a narrow corridor into the gymnasium. The GOP’s Last Identity Crisis Remade U.S. Politics |Michael Wolraich |July 24, 2014 |DAILY BEAST 

The dining hall, it seemed, had been put to more use than the gymnasium. Pablo Escobar’s Private Prison Is Now Run by Monks for Senior Citizens |Jeff Campagna |June 7, 2014 |DAILY BEAST 

Emergency rooms were quickly overwhelmed and the city morgue had to set up a makeshift mortuary in a municipal gymnasium. Nightclub Inferno Kills Hundreds in Brazil |Mac Margolis |January 27, 2013 |DAILY BEAST 

“This is not a gymnasium or spectator sport,” Ingram warned. Despite Intimidation, Alleged Victim Testifies Against Accused Rapist |Allison Yarrow |December 3, 2012 |DAILY BEAST 

They entered a large room which combined the characteristics of a library with those of a military gymnasium. Dope |Sax Rohmer 

He said he didn't take the watch and chain, that he found them in the gymnasium near the lockers. The Mystery at Putnam Hall |Arthur M. Winfield 

Special classes have been opened at the gymnasium for the religious instruction of Jewish pupils. Prison Memoirs of an Anarchist |Alexander Berkman 

Soldiers were already stacking up the chairs ready for the clearance of the gymnasium for the morrow. The Doctor of Pimlico |William Le Queux 

Smyrna therefore was situated near the present gymnasium, at the back of the present city, but between Tracheia and Lepre Acta. The Geography of Strabo, Volume III (of 3) |Strabo