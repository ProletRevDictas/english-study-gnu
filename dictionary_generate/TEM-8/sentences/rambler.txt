No girl,” said my friend Al Bartz, “is willing to be seen dead in a Nash Rambler. P.J. O’Rourke on Grabbing the Keys to Happiness |P. J. O’Rourke |January 24, 2014 |DAILY BEAST 

Amount paid to American Rambler Productions LLC by the Romney campaign for campaign messaging and advertising. What They Could Have Bought: Better Uses for Campaign Spending |Paula Froelich |November 11, 2012 |DAILY BEAST 

“Johnsonians” come from all over the world to attend regular meetings and subscribe to their own publication, The New Rambler. The Will Self Book Club: How the Writer Became a God |Henry Krempels |May 24, 2012 |DAILY BEAST 

Johnson is religious through and through, but there are passages in the Rambler and Idler dark as starless, moonless midnight. More Pages from a Journal |Mark Rutherford 

I remember now the exact pattern of the wallpaper, a design of pale blue trellis-work with crimson rambler roses. The Sign of Silence |William Le Queux 

Ever mindful of the wisdom of the "Rambler," I have accustomed myself to mark the small peculiarities of character. Boswell's Correspondence with the Honourable Andrew Erskine, and His Journal of a Tour to Corsica |James Boswell 

They are rose pink and really as big as small rambler roses, each head hanging down on its long stalk. A Journal from Japan |Marie Carmichael Stopes 

He reached up and pulled a red rambler rose from a cluster which blocked the window. The Forsyte Saga, Volume III. |John Galsworthy