It is part of a growing amount of research, some of it funded by the aviation industry, that airlines hope will convince the public that air travel is safe as long as proper precautions are taken. Airports have taken steps to reduce coronavirus transmission but risks still remain, study says |Lori Aratani |February 12, 2021 |Washington Post 

Recent announcements by United Airlines and American Airlines come as aviation unions have begun to push for a second extension of the Payroll Support Program that has kept many workers on the job. Airlines warn employees they could be furloughed at the end of March |Lori Aratani |February 5, 2021 |Washington Post 

United Airlines is investing in a company that has nothing to do with aviation, per se. Climate-friendly farming and Science Moms |Lyndsey Layton |February 2, 2021 |Washington Post 

After the flight, the company hired an outside aviation expert, Dennis O’Donoghue, to conduct a safety review of the program, and he spent weeks interviewing company officials and poring over records, according to the book. Virgin Galactic ordered safety probe after wing of spacecraft was damaged during 2019 flight, book says |Christian Davenport |February 1, 2021 |Washington Post 

Still, experts say, breaking up the firms will do nothing unless fundamental safeguards are put in place to limit their business models in a way similar to how strict regulations exist for food safety and aviation, for example. Big Tech's Business Model Is a Threat to Democracy. Here's How to Build a Fairer Digital Future |Billy Perrigo |January 22, 2021 |Time 

There were no deaths on scheduled commercial aviation flights in 2014, in a system that operates 68,000 flights a day. Annoying Airport Delays Might Prevent You From Becoming the Next AirAsia 8501 |Clive Irving |January 6, 2015 |DAILY BEAST 

The accident rate in Asia has marred what was in 2014 a banner year for aviation safety. Annoying Airport Delays Might Prevent You From Becoming the Next AirAsia 8501 |Clive Irving |January 6, 2015 |DAILY BEAST 

Aviation experts across the world experienced severe jaw dropping at this news. Annoying Airport Delays Might Prevent You From Becoming the Next AirAsia 8501 |Clive Irving |January 6, 2015 |DAILY BEAST 

The latest disappearance is another huge aviation blow for Malaysia, where both Malaysia Airlines and AirAsia are based. The Presumed Crash of AirAsia Flight QZ8501 Is Nothing Like MH370 |Lennox Samuels |December 29, 2014 |DAILY BEAST 

This was very blunt and surprising to hear from any official in charge of an aviation disaster. Did Bad Weather Bring Down AirAsia 8501? |Clive Irving |December 29, 2014 |DAILY BEAST 

Only in his field of aviation medicine did he feel competent, secure. Warning from the Stars |Ron Cocking 

In the evening we were invited to the aviation camp in the suburbs of Paris. A Journey Through France in War Time |Joseph G. Butler, Jr. 

I don't know where he's stationed; and I'm going with the aviation—if it's ever ready! Ramsey Milholland |Booth Tarkington 

The Wright brothers directed their whole attention to aviation in 1899. Aviation in Peace and War |Sir Frederick Hugh Sykes 

The conclusion has been reached that we cannot dispense with aviation, even if we would. Aviation in Peace and War |Sir Frederick Hugh Sykes