Our hermit—we’ll later learn his name is Rob—trades his truffles for necessary goods. In Pig, Nicolas Cage Plays a Grouchy, Meditative Hermit—and Gives His Best Performance in Years |Stephanie Zacharek |July 15, 2021 |Time 

I’d grown used to hermit life, and the prospect of driving across the bridge to sing in a bar with strangers seemed risky, both health-wise, with reports of new variants around the world, and socially. Karaoke Was Always an Escape Before the Pandemic. Now It's More Than That |Aimee Phan |July 12, 2021 |Time 

A hermit, asked how he’s doing, answers, “Oh, same old, same old.” The action-packed saga ‘Monkey King: Journey to the West’ gets a modern take |Michael Dirda |March 3, 2021 |Washington Post 

During a summer internship for a local newspaper, she covered a hermit crab beauty pageant. Date Lab: She had many fun stories to share. He kept asking about work. |Marin Cogan |February 18, 2021 |Washington Post 

A trip to North Korea introduced Monisha Rajesh to Barbara Demick’s Nothing to Envy, which she describes as “a gripping examination of the so-called hermit kingdom through the voices of six defectors.” The Best Adventure Travel Books of 2020 |Heather Greenwood Davis |December 15, 2020 |Outside Online 

The FBI and the President may claim that the Hermit Kingdom is to blame for the most high-profile network breach in forever. No, North Korea Didn’t Hack Sony |Marc Rogers |December 24, 2014 |DAILY BEAST 

How ironic that the Hermit Kingdom is taking the blame for our first real look inside a clique that not even Vice dares penetrate. Pyongyang Shuffle: Hollywood In Dead Panic Over Sony Hack |James Poulos |December 19, 2014 |DAILY BEAST 

Someone called him a hermit crab lurking in the halls of the United Nations. The Man Who Invented the Word ‘Genocide’ |Nina Strochlic |November 19, 2014 |DAILY BEAST 

The Hermit Kingdom has opened its embassy doors for an exhibit highlighting the work of six artists from its state-run studio. North Korea’s Propaganda Art Exhibit in London |Nico Hines |November 6, 2014 |DAILY BEAST 

Considering the grand tradition of un-predictability in the Hermit Kingdom, there are countless other possibilities. Gout or Out: North Korea’s No-Show Leader Keeps ‘Em Guessing |Kevin Bleyer |October 11, 2014 |DAILY BEAST 

It was the Hermit's vast store of scientific knowledge that brought the half-dead cop back to health. Hooded Detective, Volume III No. 2, January, 1942 |Various 

He flattened out the slip of paper and placed it on the table in front of the Hermit. Hooded Detective, Volume III No. 2, January, 1942 |Various 

In the Ozarks he made his living by hunting and fishing, and for some years lived almost the life of a hermit. The Courier of the Ozarks |Byron A. Dunn 

He took an apartment in the Temple, turned his back on his friends, and became an inaccessible hermit. Friend Mac Donald |Max O'Rell 

"But an ordinary hermit wouldn't be able to play like a virtuoso," objected Amy. The Outdoor Girls in the Saddle |Laura Lee Hope