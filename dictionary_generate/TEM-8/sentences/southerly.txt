The techniques and timing for covered gardening differ in hotter, more southerly regions, but the concept remains the same. Fresh vegetables in the middle of winter? It’s possible, even in colder climes. |Adrian Higgins |January 20, 2021 |Washington Post 

With only a light southerly wind, it’s rather pleasant out there if you’re eager to get those lights down. D.C.-area forecast: Trending milder before another burst of cold midweek |Brian Jackson |December 27, 2020 |Washington Post 

We’ll enjoy temperatures rising into the lower 50s during the afternoon, with an occasionally breezy southerly wind. D.C.-area forecast: Trending milder before another burst of cold midweek |Brian Jackson |December 27, 2020 |Washington Post 

At least it’s warmer, in the mid-40s to around 50 degrees, thanks to a continued light southerly breeze. D.C.-area forecast: A couple more chilly days, before a slow warm up |A. Camden Walker |December 18, 2020 |Washington Post 

We can thank a southerly breeze around 5-10 mph for this warming assistance. D.C.-area forecast: A couple more chilly days, before a slow warm up |A. Camden Walker |December 18, 2020 |Washington Post 

The ship lifted, took off alone in a southerly direction, flying higher and higher and out of sight. Valley of the Croen |Lee Tarbell 

In a southerly direction, lying between the forest line and cliff brink, were dotted small huts at long intervals. Menotah |Ernest G. Henham 

Southerly and south-easterly gales, known as 'southerly busters,' often last three days, and bring cold rain and dirty weather. Yachting Vol. 2 |Various. 

A darkening of the water some miles distant showed that a southerly breeze was coming in. The Rival Campers Afloat |Ruel Perley Smith 

A brisk southerly wind prevailed, that rendered the atmosphere less oppressive than usual. Early Western Travels 1748-1846, Volume XVI |Various