I’m sure there are fans or media personnel who can’t wait to go back to covering Luka Dončić triple-doubles and James Harden highlights and all that. What Happened In The NBA This Week? |Sara Ziegler (sara.ziegler@fivethirtyeight.com) |August 28, 2020 |FiveThirtyEight 

With the season having started at the start of August, those subscribers also get to watch 15-minute highlights of matches 24 hours after they’ve aired on broadcaster Sky Sports before they get to watch the full match a week after it has aired. “It’s been a source of some much-needed income’: Sports clubs are building subscription businesses on Facebook |Seb Joseph |August 18, 2020 |Digiday 

We detailed our process for constructing those polling averages when we released them, so I’ll just review the highlights here. How FiveThirtyEight’s 2020 Presidential Forecast Works — And What’s Different Because Of COVID-19 |Nate Silver (nrsilver@fivethirtyeight.com) |August 12, 2020 |FiveThirtyEight 

To recreate the event feeling it streamed old FA Cup games from the 1990s and 2000s once a week, supplemented with several recaps and story-focused highlights of specific players or teams. ‘The audience is still there’: How U.K. sports outlets pivoted to branded content, social video to keep fans engaged as seasons were delayed |Lucinda Southern |July 29, 2020 |Digiday 

Read on for highlights of what we learned from each of our panelists. Watch: Quartz’s workshop on how to collaborate remotely |Heather Landy |July 23, 2020 |Quartz 

Their confrontation at dinner was, without a doubt, the highlight of the episode. ‘Downton Abbey’ Review: A Fire, Some Sex, and Sad, Sad Edith |Kevin Fallon |January 5, 2015 |DAILY BEAST 

He is my favorite Woodstock artist, the highlight of the entire festival. The Greatest Rock Voice of All Time Belonged to Joe Cocker |Ted Gioia |December 23, 2014 |DAILY BEAST 

Art is something that has been a highlight at The Center since its first group show—“The Center Show”—in 1989. The LGBT Center That Changed Our Lives |Justin Jones |December 22, 2014 |DAILY BEAST 

It can be really good if somebody is an amazing talent right off the bat or it can highlight your flaws. Inside ‘The Sex Factor’: Where 16 Men and Women Vie For Porn Immortality |Aurora Snow |November 22, 2014 |DAILY BEAST 

The documents also highlight the apparent complicity by secular law enforcement in keeping some of these offenders out of jail. How Sicko Priests Got Away With It |Barbie Latza Nadeau |November 16, 2014 |DAILY BEAST 

There has been a highlight added along the bottom of the gun just forward of the trigger guard and just below Oswald's left hand. Warren Commission (7 of 26): Hearings Vol. VII (of 15) |The President's Commission on the Assassination of President Kennedy 

There is a highlight along the top of the rifle between Oswald's left hand and the point where the rifle passes his left shoulder. Warren Commission (7 of 26): Hearings Vol. VII (of 15) |The President's Commission on the Assassination of President Kennedy 

On Shaneyfelt Exhibit No. 1, the highlight does not denote the top of the weapon. Warren Commission (7 of 26): Hearings Vol. VII (of 15) |The President's Commission on the Assassination of President Kennedy 

There is detail present that shows other areas of the gun, the breech, above the highlight. Warren Commission (7 of 26): Hearings Vol. VII (of 15) |The President's Commission on the Assassination of President Kennedy 

Yes; I mentioned that the highlight along the top from the butt to the bolt is generally similar in that it is in a straight line. Warren Commission (7 of 26): Hearings Vol. VII (of 15) |The President's Commission on the Assassination of President Kennedy