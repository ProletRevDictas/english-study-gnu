During the country’s dry season in 2019, nearly 4 million acres of peatland and tropical forest were burned. What wildfires in Brazil, Siberia, and the US West have in common |Lili Pike |September 17, 2020 |Vox 

It can also mean using saws and machines to cut and thin the forests. Suppressing fires has failed. Here’s what California needs to do instead. |James Temple |September 17, 2020 |MIT Technology Review 

Then, over the past few decades, the forests of Sarawak faced threats unlike any before. The Environmental Headache in Your Shampoo - Issue 90: Something Green |Anastasia Bendebury & Michael Shilo DeLay |September 16, 2020 |Nautilus 

Severe wildfires across the American West may already be altering the future of forests there. Severe wildfires in the West may already be altering the future of forests |Alison Snyder |September 10, 2020 |Axios 

Once it’s in wild boar in the forests there, it can be very difficult to control. Europe is on high alert after a deadly swine virus emerges in Germany |Bernhard Warner |September 10, 2020 |Fortune 

He first rose to prominence as a lawyer in Queens, who settled a boiling racial dispute over public housing in Forest Hills. Mario Cuomo: An OK Governor, but a Far Better Person |Michael Tomasky |January 2, 2015 |DAILY BEAST 

“It fundamentally changes the architecture of forest canopies,” says Watson. Mistletoe is the Vampire of Plants |Helen Thompson |December 21, 2014 |DAILY BEAST 

The birds poop all over the forest, and thanks to the viscin, the mistletoe seeds in said poop stick to branches. Mistletoe is the Vampire of Plants |Helen Thompson |December 21, 2014 |DAILY BEAST 

Instead, most of the suffering species ate insects on the forest floor. Mistletoe is the Vampire of Plants |Helen Thompson |December 21, 2014 |DAILY BEAST 

From the looks of it, mistletoe is a keystone species and plays a crucial role in that forest ecosystem. Mistletoe is the Vampire of Plants |Helen Thompson |December 21, 2014 |DAILY BEAST 

However, they were not seen to venture far into the surrounding deciduous forest. Summer Birds From the Yucatan Peninsula |Erwin E. Klaas 

He swims every day in the river; he fishes from his bamboo raft; he hunts in the forest with his father. Alila, Our Little Philippine Cousin |Mary Hazelton Wade 

And the covering of Juda shall be discovered, and thou shalt see in that day the armoury of the house of the forest. The Bible, Douay-Rheims Version |Various 

After this it wound along on ridges and in ravines till it reached the heart of a great pine forest, where stood a saw-mill. Ramona |Helen Hunt Jackson 

They started in the early morning and rode out over the plains till they came to the edge of a large forest. Alila, Our Little Philippine Cousin |Mary Hazelton Wade