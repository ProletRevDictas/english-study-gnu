For all the power, grace and technical skill demanded of elite gymnastics, the true measure of greatness is determined by fractions of a point awarded by a panel of judges. For competition-starved U.S. gymnasts, Winter Cup will measure Olympic readiness |Liz Clarke |February 26, 2021 |Washington Post 

In the opening of “Smoke,” Isaiah has put his little house in East Long Beach on the market, bid a painful farewell to the love of his life, an artist named Grace, and hit the road. Joe Ide’s IQ series continues with the idiosyncratic marvel ‘Smoke’ |Maureen Corrigan |February 26, 2021 |Washington Post 

In the last chat, Grace said, “Porto isn’t very good this year.” This Champions League Round Has Not Been Kind To Barcelona |Chadwick Matlin (chadwick.matlin@fivethirtyeight.com) |February 25, 2021 |FiveThirtyEight 

Hutchings nonetheless took a moment to ask for grace from frustrated parents, teachers and students. Arlington, Alexandria move forward with school reopening plans |Hannah Natanson |February 19, 2021 |Washington Post 

He spread his grace on everything and every one he came in contact with. Hank Aaron left an indelible mark on MLB players: 'He was not going to let anything distract him’ |Matt Bonesteel |January 22, 2021 |Washington Post 

But there is a big twist in this story that has left both Grace Castro and Lozoya frustrated and grasping for more answers. An Informant, a Missing American, and Juarez’s House of Death: Inside the 12-Year Cold Case of David Castro |Bill Conroy |January 6, 2015 |DAILY BEAST 

That is a reality that still eats at Grace Castro and Yvonne Lozoya. An Informant, a Missing American, and Juarez’s House of Death: Inside the 12-Year Cold Case of David Castro |Bill Conroy |January 6, 2015 |DAILY BEAST 

Twin girls, Greta and Grace, run around the floor in circles, wearing pink playsuits with tiny pink wings attached. A Sunni-Shia Love Story Imperiled by al Qaeda |Ruth Michaelson |December 26, 2014 |DAILY BEAST 

They made quiet plans together, saying that when they had a child together, they wanted a girl called Grace. A Sunni-Shia Love Story Imperiled by al Qaeda |Ruth Michaelson |December 26, 2014 |DAILY BEAST 

“Light trumps darkness, hope beats despair, grace wins over sin, love defeats hate, life conquers death,” the cardinal said. 'Please Don't Die!': The Frantic Battle to Save Murdered Cops |Michael Daly |December 22, 2014 |DAILY BEAST 

Many of them were delicious in the role; one of them was the embodiment of every womanly grace and charm. The Awakening and Selected Short Stories |Kate Chopin 

They ranged from moving trunks to cleaning cisterns, and, by grace of all of them, Sim was doing very well. The Bondboy |George W. (George Washington) Ogden 

She was growing a little stout, but it did not seem to detract an iota from the grace of every step, pose, gesture. The Awakening and Selected Short Stories |Kate Chopin 

May looked along at the dimpled grace, And then at the saint-like, fair old face, “How funny!” Boys and Girls Bookshelf; a Practical Plan of Character Building, Volume I (of 17) |Various 

See the ease and grace of the lady in the sacque, who sits on the bank there, under the myrtles, with the guitar on her lap! Checkmate |Joseph Sheridan Le Fanu