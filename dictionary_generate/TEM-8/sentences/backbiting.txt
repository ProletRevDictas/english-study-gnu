Trouble seemed to arise not from big issues like race but from petty gossip and backbiting. Tupac Shakur’s Race-Killer Prison Pal Talks |Michael Daly |July 29, 2014 |DAILY BEAST 

Never mind recent reports of backbiting and fingerpointing within the Perry camp. Brownback: Rick Perry’s Just Like John McCain in 2008 |Lloyd Grove |January 4, 2012 |DAILY BEAST 

The Koran warns against “backbiting,” and so does the Torah, which calls it “evil tongue.” When Gossip Is Good, from Scandals to Relationships |Allison Yarrow |November 22, 2011 |DAILY BEAST 

Matt Latimer on the petty backbiting that has plagued his tenure from the start. Michael Steele's GOP Enemies |Matt Latimer |December 12, 2010 |DAILY BEAST 

Of course, now that the reorganization is official, the speculation stops and the backbiting from disenchanted executives begins. 7 Winners, 9 Losers at the New NBC |Peter Lauria |November 17, 2010 |DAILY BEAST 

He hated any species of backbiting, and he had heard of Burleigh as an adept in the art, and a man to be feared. Warrior Gap |Charles King 

But I cannot bear that a slanderous backbiting tongue should make you think that I have seen no service. Can You Forgive Her? |Anthony Trollope 

To do so is to engage my daughter to sorrow, and hope deferred, and miserable backbiting! Was It Right to Forgive? |Amelia Edith Huddleston Barr 

For those were happy times when backbiting among artists took the more manly form of poisoning. An Artist's Letters From Japan |John La Farge 

They were much given to gluttony and drinking; and there was an unthinkable amount of scandal and backbiting and jealousy. Samuel the Seeker |Upton Sinclair