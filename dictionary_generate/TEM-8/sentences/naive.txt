Meanwhile, some pandemic experts say that presuming a return to normal public life, critical to Disney, would be naive anytime in the near future. Disney took a hit during the pandemic, but you wouldn’t know it from its stock price |Steven Zeitchik |February 19, 2021 |Washington Post 

We’re not naive to think that a business deal can’t blow up. The Baltimore Sun was spared a hedge-fund owner. Now it will try to survive as a nonprofit. |Paul Farhi, Elahe Izadi |February 17, 2021 |Washington Post 

It would be naive to think a robust sports schedule would have prevented the Capitol riot. This odd Super Bowl will bring us together for a day. Let’s not take that for granted. |Jerry Brewer |February 5, 2021 |Washington Post 

It was always optimistic, boarding on the naive, to think a new year would immediately wash away the problems of 2020. ‘More comfortable with the uncomfortable’: 2021 promises an uneasy sense of déjà vu for advertisers |Seb Joseph |February 1, 2021 |Digiday 

In many ways and for many years, Viking scholars have been naive and simplistic about their acknowledgement and recognition of gender variation in the later Iron Age. What Stereotypes About Viking Masculinity Get Wrong |Neil Price |August 25, 2020 |Time 

I was naive enough to assume that he would, at most, rob me. How I Stopped My Rapist |Natasha Alexenko |November 24, 2014 |DAILY BEAST 

Artists now consider the Ideal Palace a piece of “naive” or “outsider” art. The Postman Who Built a Palace in France…by Hand |Nina Strochlic |November 20, 2014 |DAILY BEAST 

She tackles weighty subjects with a naive sensibility and faux-innocence, but skillfully avoids dumbing them down. The Singular Artist of New Yorkistan |Lizzie Crocker |November 14, 2014 |DAILY BEAST 

I was definitely naive, I think the main similarity between me and Hal is that we were naive. Shakespeare Comes to Hulu with ‘Complete Works’ |Marina Watts |June 11, 2014 |DAILY BEAST 

Maybe you can call it naive but that's the way Shae simply is. Game of Thrones’ Sibel Kekilli Discusses Shae’s Treachery at the Trial of Tyrion Lannister |Marlow Stern |May 13, 2014 |DAILY BEAST 

And Jansoulet felt the delight of a child, a plebeian joy, compounded of ignorance and naive vanity. The Nabob |Alphonse Daudet 

There was a naturalness in his enjoyment which was almost boylike; a naive sort of exultation possessed him. When Valmond Came to Pontiac, Complete |Gilbert Parker 

Buzonniere, Rochfort and Fangouse are milder and more naive in their demonstrations and their works are of no weight or interest. Baron d'Holbach |Max Pearson Cushing 

A remark which Mendelssohn once made in his peculiar naive manner is very characteristic of him and his opinion of Chopin. Frederick Chopin as a Man and Musician |Frederick Niecks 

But he got the impression that she was almost fantastically naive. A World Called Crimson |Darius John Granger