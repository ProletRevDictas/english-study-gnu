At night, the team knocked dozens of species from roadside hedgerows or swept up larvae from grasses, catching nearly 2,500 caterpillars. Streetlights, especially super bright LEDs, may harm insect populations |Jonathan Lambert |August 31, 2021 |Science News 

This might include planting hedgerows or bushes to create more varied landscapes. Around the world, birds are in crisis |Alison Pearce Stevens |December 3, 2020 |Science News For Students 

This gave the Germans time to stabilize and dig in on the “hedgerow front” before St. Lô. Blood in the Sand: When James Jones Wrote a Grunt’s View of D-Day |James Jones |November 15, 2014 |DAILY BEAST 

He must soon, he thought, be getting near the opening at the Stone-pits: he should find it out by the break in the hedgerow. English: Composition and Literature |W. F. (William Franklin) Webster 

Then searching in and out the hedgerow for favourite seeds, and singing, singing all the while, verily a 'song without an end.' The Hills and the Vale |Richard Jefferies 

The pond by the hedgerow was sealed with ice, and he suffered much from the lack of his customary food. Creatures of the Night |Alfred W. Rees 

Soon the haze lifted, leaving the dew thick on the grass by the ditch, and on the moss and the ivy in the hedgerow bank. Creatures of the Night |Alfred W. Rees 

Disappointed, the fox turned towards the uplands and crossed the hedgerow into the nearest stubble. Creatures of the Night |Alfred W. Rees