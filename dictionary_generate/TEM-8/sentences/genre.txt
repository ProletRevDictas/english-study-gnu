In recent years, “lo-fi chill” and other forms of “focus music” have become so popular that there are now multiple YouTube channels devoted to the genre. People who really miss the office are listening to its sounds at home |Tanya Basu |September 10, 2020 |MIT Technology Review 

Throughout the process, Thio worked with lo-fi producers to curate bass lines, drum lines, and background ambience that exemplify the genre and sound good. Create your own moody quarantine music with Google’s AI |Karen Hao |September 4, 2020 |MIT Technology Review 

Stell is a rapidly rising artist in the electronic and dance scene, though his songs don’t fit neatly in the genre. Why Big Wild's Songs Feel Like Adventures |Outside Editors |September 3, 2020 |Outside Online 

Studios with a very focused portfolio around a specific genre — Viker’s core focus is social casino-type games — may benefit from advertisers who shift to more broad-based, contextual targeting, he added. ‘Speeding up market consolidation’: Apple’s privacy changes expected to spark wave of gaming and ad tech M&A |Lara O'Reilly |September 3, 2020 |Digiday 

The genre-bender is well-cast and suspenseful, and the cliffhanger ending suggests there’s more to come. Stream or Skip? A Synthetic Biologist’s Review of ‘Biohackers’ on Netflix |Elsa Sotiriadis |September 2, 2020 |Singularity Hub 

People watch night soaps because the genre allows them to believe in a world where people just react off their baser instincts. ‘Empire’ Review: Hip-Hop Musical Chairs with an Insane Soap Opera Twist |Judnick Mayard |January 8, 2015 |DAILY BEAST 

A lot of the culture around movies in the sci-fi/fantasy genre is about deconstructing them ad nauseam. Patton Oswalt on Fighting Conservatives With Satire |William O’Connor |January 6, 2015 |DAILY BEAST 

I came [to personal essays] through the route of, if you want to call it intellection or a kind of interpretive [genre]. Daphne Merkin on Lena Dunham, Book Criticism, and Self-Examination |Mindy Farabee |December 26, 2014 |DAILY BEAST 

You had a great line in your piece on Geoffrey Beene about the “genre” of evening wear. Daphne Merkin on Lena Dunham, Book Criticism, and Self-Examination |Mindy Farabee |December 26, 2014 |DAILY BEAST 

Phonetic, made-up lyrics are another venerable tradition of folk music, and “pa-rum-pa-pa-pum” is iconic of the genre. Yes, I Like Christmas Music. Stop Laughing. |Michael Tomasky |December 24, 2014 |DAILY BEAST 

Mrs. Woodbury paints in oils and water-colors; the latter are genre scenes, and among them are several Dutch subjects. Women in the fine arts, from the Seventh Century B.C. to the Twentieth Century A.D. |Clara Erskine Clement 

They live in Stockholm, where she paints portraits and genre subjects. Women in the fine arts, from the Seventh Century B.C. to the Twentieth Century A.D. |Clara Erskine Clement 

Occasionally this artist has painted genre subjects, but her real success has not been in this direction. Women in the fine arts, from the Seventh Century B.C. to the Twentieth Century A.D. |Clara Erskine Clement 

Unfortunately, the art of genre painting did not exist in those days, and that of engraving was in its infancy. Catherine de' Medici |Honore de Balzac 

Tout le monde sait que l'Allemagne possede en ce genre des trsors qui ont t jusqu'ici comme enfouis pour la France. Baron d'Holbach |Max Pearson Cushing