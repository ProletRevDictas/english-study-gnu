As two officers then try and put the girl—who is now in handcuffs—in the car, she repeatedly screams, “I want my Dad.” Police Investigation Underway After 9-Year-Old Girl Pepper Sprayed During Arrest in Rochester, New York |Josiah Bates |February 2, 2021 |Time 

Others wore tactical gear or brought zip ties, which can be used as handcuffs, officials said. Rioters wanted to ‘capture and assassinate’ lawmakers, prosecutors say. A note left by the ‘QAnon Shaman’ is evidence. |Teo Armus |January 15, 2021 |Washington Post 

He wore a Taser holstered to his hip and gripped a bunch of zip-tie handcuffs in one hand. Trumpist masculinity reaches its high water mark |Monica Hesse |January 12, 2021 |Washington Post 

After deputies removed Boney in handcuffs, the judge withdrew the contempt charge but said the publisher still had to be escorted from the building. A North Carolina judge is blocking journalists from his courtroom. One objected — and got handcuffed. |Elahe Izadi, Mark Berman |December 11, 2020 |Washington Post 

Member Margaret Lorber said she simply cannot accept the idea of guns and handcuffs in schools. Alexandria City Public Schools approves revisions to contract with police |Hannah Natanson |October 30, 2020 |Washington Post 

Tihen had insisted he was in her lap as she struggled to handcuff him. From Ferguson Cop Embroiled in a Brutality Suit to City Councilwoman |Michael Daly |August 20, 2014 |DAILY BEAST 

But she surprised these cops just as she had the officers back in December, when she managed to wiggle free of a handcuff. In Months Before Wild Capitol Scene, Miriam Carey Battled Psychosis |Michael Daly |October 5, 2013 |DAILY BEAST 

There are psychological chains and restraints that are so much stronger than any steel chain or handcuff. For the Cleveland Kidnapping Victims, a Tragic Sisterhood |Christine Pelisek |May 10, 2013 |DAILY BEAST 

Police say that Thomas resisted arrested, so they had to handcuff her and forced her to the ground. In Los Angeles, Questions of Police Brutality Dog LAPD |Christine Pelisek |November 24, 2012 |DAILY BEAST 

Court documents say she was nude with a handcuff still attached to her wrist. He Wanted to Kill 50 Women |Barbie Latza Nadeau |August 18, 2010 |DAILY BEAST 

And keep your eyes on me; if I act in the least peculiar, handcuff me—but don't knock me out. The Penal Cluster |Ivar Jorgensen (AKA Randall Garrett) 

Bill said that he thought that bruin was about to make up his mind to let us take off that handcuff. Fifty Years a Hunter and Trapper |Eldred Nathaniel Woodcock 

He unwound the rope which he had brought along and secured one circle of a double handcuff to his left wrist. The Incendiary |W. A. (William Augustine) Leahy 

A white mark, which slowly changed to red, showed where Delaney had clamped the handcuff down to its last notch. Whispering Wires |Henry Leverage 

"Why, he threatened to handcuff me and take me to jail if I didn't tell him all about Mr. Whitmore's death," complained Collins. The Substitute Prisoner |Max Marcin