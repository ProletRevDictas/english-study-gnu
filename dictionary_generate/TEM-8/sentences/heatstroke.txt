Where other animals would have to stop, to avoid heatstroke, we could keep going, stalking prey during the heat of the day. A Self-Professed Sweater Explores the Science Behind Stink |klindsey |July 28, 2021 |Outside Online 

Several people died of heatstroke in Snohomish County, while others in the western part of the state were found dead in their apartments because of heat-related stress, officials said. Historic heat wave in Pacific Northwest has killed hundreds in U.S. and Canada over the past week |Timothy Bella |July 1, 2021 |Washington Post 

In the throes of heatstroke, your internal temperature may spike above 105°F, but if you’re alone—victims often are—you’ll have gone cold by the time someone finds you. Heat is the silent killer we should all be worried about |Rachel Feltman |June 29, 2021 |Popular-Science 

Around 26% of all heatstroke deaths of children in vehicles occur when a child gains access on their own, and gets trapped. PM Update: Wonderful weather continues through Wednesday |Ian Livingston |June 15, 2021 |Washington Post 

McNair, a 19-year-old football player, died in June 2018 after the Maryland medical staff failed to treat him properly after he suffered exertional heatstroke during a team workout. Maryland approves $3.5 million settlement over Jordan McNair’s death |Emily Giambalvo, Rick Maese |January 27, 2021 |Washington Post 

In hot weather, dizziness is a first warning sign of heat exhaustion, which can lead to heatstroke. When You Don't Know Where to Turn |Steven J. Bartlett 

When a person falls with sunstroke (or heatstroke) he should at once be carried to a cool, shady place. Alcohol: A Dangerous and Unnecessary Medicine, How and Why |Martha M. Allen