If critics still want to clash swords and hold people to higher standards, it will be to demand the same authenticity from the institutions that once determined so much of our cultural life. Transformed by crisis, arts criticism may never be the same. And that’s a good thing. |Philip Kennicott |November 29, 2020 |Washington Post 

Of course, some buyers see this latter fact as somewhat of a double-edged sword – true, the interest rates might be low – but often, the costs of homes are high. High housing prices and low mortgage rates |Jeff Hammerberg |November 22, 2020 |Washington Blade 

Her works reflect that women’s agency is a double-edged sword. Why It’s Important To See Women As Capable … Of Terrible Atrocities |LGBTQ-Editor |November 21, 2020 |No Straight News 

Sure, you’re used to the rumble of the controller as your character falls down a rocky hill or gets struck by an enemy’s sword. PlayStation 5 review: PS5 is a sensory game-changer |Elise Favis |November 6, 2020 |Washington Post 

It comes at the film’s conclusion, after Mulan has returned home to her family and received her father’s blessing for running away with his sword and armor. The history of Mulan, from a 6th-century ballad to the live-action Disney movie |Constance Grady |September 4, 2020 |Vox 

But then the sword is miraculously returned to him, and he girds for battle once again. Meet Moses the Swashbuckling Israelite |James Romm |December 14, 2014 |DAILY BEAST 

Jonathan Gruber, the economist who helped design Romneycare and the Affordable Care Act, falls on his sword before Congress. Obamacare Architect: I Wanted to Sound Smart |Ben Jacobs |December 9, 2014 |DAILY BEAST 

Unlike all the trailers and screen shots for the movie, in the Bible Moses never holds a sword or wears armor. Christian Bale: One Man's Moses Is Another Man's Terrorist |Candida Moss, Joel Baden |December 7, 2014 |DAILY BEAST 

Joshua puts to the sword women, infants, and animals at Jericho. Christian Bale: One Man's Moses Is Another Man's Terrorist |Candida Moss, Joel Baden |December 7, 2014 |DAILY BEAST 

At Towton Field, on 29th March, 1461, 33,000 men perished by the sword and were buried there. Blood in the Sand: When James Jones Wrote a Grunt’s View of D-Day |James Jones |November 15, 2014 |DAILY BEAST 

The men, whose poniards his sword parried, had recourse to fire-arms, and two pistols were fired at him. The Pastor's Fire-side Vol. 3 of 4 |Jane Porter 

Whatever you do, don't write a word to that Carr friend of yours; he's as sharp as a two-edged sword. Elster's Folly |Mrs. Henry Wood 

He stabbed her from beneath, and passed half of his sword through her body, and at that the poor lady fell. The Philippine Islands, 1493-1898, Volume XX, 1621-1624 |Various 

Since words have different meanings, we may sometimes find that a pair of words exemplify all three Laws, as plough and sword. Assimilative Memory |Marcus Dwight Larrowe (AKA Prof. A. Loisette) 

The teeth of beasts, and scorpions, and serpents, and the sword taking vengeance upon the ungodly unto destruction. The Bible, Douay-Rheims Version |Various