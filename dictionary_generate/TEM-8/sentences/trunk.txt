The company, founded as a trunk maker in 1854 and known today for its high-end handbags, has created a pro model skateboarding sneaker. Louis Vuitton—yes, that Louis Vuitton—has created a pro skate shoe |Marc Bain |August 25, 2020 |Quartz 

On the surf trip, I put shoes, flip flops, and a rain jacket in the big clamshell compartment, and used cubes for underwear and socks, swim trunks, and sun shirts and rash guards. The Matador SEG42 Offers Unrivaled Gear Organization |Graham Averill |August 25, 2020 |Outside Online 

Cheetahs sink their claws into a tree trunk to climb, she notes. Shape-shifting cuts give shoes a better grip |Carolyn Wilke |July 14, 2020 |Science News For Students 

Finding and hitting a slim stem or trunk with a tiny patch clearly poses a challenge. Silk-based microneedles may help treat diseased plants |Kathryn Hulick |June 18, 2020 |Science News For Students 

As the pudgy, short-legged bear climbs, it presses its head briefly against the tree trunk again and again. Pandas use their heads as a kind of extra limb for climbing |Susan Milius |March 13, 2020 |Science News For Students 

In December, he did his first trunk shows in Paris and New York. The Photographer Who Gave Up Manhattan for Marrakech |Liza Foreman |January 6, 2015 |DAILY BEAST 

A declaration of candidacy signed by Cuomo was in the trunk of his car. Mario Cuomo, a Frustrating Hero to Democrats, Is Dead at 82 |Eleanor Clift |January 2, 2015 |DAILY BEAST 

“I was a little nervous every time I took the camera out [of the trunk] that it would take me over,” he says. From Fashion Player to Photographer Monk |Nina Strochlic |December 3, 2014 |DAILY BEAST 

The brainchild of a company called Sologic, the eTree looks like a basic tree with a wooden trunk. Parks and Regeneration |The Daily Beast |November 3, 2014 |DAILY BEAST 

The Queen sent her first email at an RAF base in 1976 and she made the first direct-dial trunk call from the UK in 1958. One Has Tweeted! |Tom Sykes |October 24, 2014 |DAILY BEAST 

A few moments afterward he was seen dragging his own trunk ashore, while Mr. Hitchcock finished his story on the boiler deck. The Book of Anecdotes and Budget of Fun; |Various 

Robert assisted her into the hammock which swung from the post before her door out to the trunk of a tree. The Awakening and Selected Short Stories |Kate Chopin 

Vunce I seen a feller–I hat some snakes here in algohol–unt dat feller he trunk de algohol. Alec Lloyd, Cowpuncher |Eleanor Gates 

Indeed a child will sometimes complete the drawing by adding feet and hands before he troubles to bring in the trunk (see Fig. 8). Children's Ways |James Sully 

The arms, too, not uncommonly are spread out from the two sides of the trunk just as in the front view. Children's Ways |James Sully