It also depicts her friendship with Roxon, a fellow Australian living stateside and the writer of the groundbreaking Lillian Roxon’s Rock Encyclopedia, which was published in 1969. Inside ‘I Am Woman’: A new biopic tells the story of Helen Reddy and her famous song |radmarya |September 10, 2020 |Fortune 

They also argue that a notorious photo apparently depicting a police officer patting a suspected assailant on the shoulder actually shows the officer “pushing” the suspect away. Hong Kong police are rewriting the history of an infamous thug attack on civilians |Mary Hui |August 27, 2020 |Quartz 

Investigators had recorded phone calls and emails showing the couple worked with Singer to get their daughters into USC with fake athletic profiles depicting them as star rowers. Lori Loughlin gets two months in prison after judge accepts plea deal in college bribery scandal |radmarya |August 21, 2020 |Fortune 

The scientists then drafted local artists to help illustrate the research they wanted to depict. The superheroes in these comics were inspired by real scientists |Kyle Plantz |July 28, 2020 |Science News For Students 

The low numbers do not depict the real situation, according to experts. Why South Asia’s COVID-19 Numbers Are So Low (For Now) |Puja Changoiwala |June 23, 2020 |Quanta Magazine 

On the opposite end of the spectrum are two other standout works, which depict Mary as a loving, nurturing mother. The Virgin Mary Lookbook |William O’Connor |December 7, 2014 |DAILY BEAST 

It is certainly not correct to exclusively depict her as an entirely innocent victim. Brecht's Mercenary Mother Courage Turns 75 |Katie Baker |September 10, 2014 |DAILY BEAST 

The resultant pop culture is as morbid and contagious as the epidemics they depict. Ebola Rages in West Africa, Reigniting Humanity’s Oldest Fear: The Plague |Scott Bixby |August 4, 2014 |DAILY BEAST 

As a reward for her attempt to depict the consequences of online commenters, Gwyneth Paltrow has become their latest victim. Gwyneth Was Right: America Turned Communication Into a Weaponized Battlefield |James Poulos |June 1, 2014 |DAILY BEAST 

The show tried to depict consensual sex between Jaime and Cersei Lannister—but messed up big time. Why We Should Pretend the ‘Game of Thrones’ Rape Scene Never Happened |Andrew Romano |May 4, 2014 |DAILY BEAST 

She paled beside Valerie Marneffe, though, to be sure, Daudet knew better than to attempt to depict any such queen of vice. The Nabob |Alphonse Daudet 

All historians who have written of Champlain attribute to him the qualities which we have endeavoured to depict in these pages. The Makers of Canada: Champlain |N. E. Dionne 

It has in all ages been a pastime of noble minds to try to depict a perfect state of society. Tolstoy on Shakespeare |Leo Tolstoy 

Reynolds loved to depict his sitters in mythological or historical settings. The History of Modern Painting, Volume 1 (of 4) |Richard Muther 

Why should we linger on a scene which each heart can depict for itself? Evenings at Donaldson Manor |Maria J. McIntosh