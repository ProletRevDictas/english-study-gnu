One-quarter of respondents don’t see getting back to normal operations until Q1 2021 or beyond, underscoring that this is a highly uneven recovery. Hurricane Laura and the Fed pack a one-two punch, pushing global stocks lower |Bernhard Warner |August 27, 2020 |Fortune 

The uneven recovery in spending highlights what is already a growing income gap. Haves and Have-Nots: Pandemic Recovery Explodes China’s Wealth Gap |Daniel Malloy |August 19, 2020 |Ozy 

Still, experts say that the gains from having arts-related facilities, while uneven, are real. Can the Arts Save Rural America From the Recession? |Charu Kasturi |August 16, 2020 |Ozy 

It also doesn’t deal with the uneven distribution of the technology that is supposed to catapult us into this new age. New Report Predicts Tech Could Fuel an Age of Freedom—or Make Civilization Collapse |Edd Gent |June 29, 2020 |Singularity Hub 

I do think, to Michael’s earlier point, that some of the problematic nature of the equipment they’ve shipped — their uneven reputation on the quality of their pharma products and so forth — will cause people to want to test it pretty heavily first. Will Covid-19 Spark a Cold War (or Worse) With China? (Ep. 414) |Stephen J. Dubner |April 23, 2020 |Freakonomics 

Data are much better for Europe and Latin America, and frustratingly uneven for Africa and much of Asia. It Gets Better—but Mostly if You Live in a Rich, Democratic Country |Jay Michaelson |November 11, 2014 |DAILY BEAST 

Their uneven threads showed potential for backing out or breaking, he said. Patients Screwed in Spine Surgery ‘Scam’ |The Center for Investigative Reporting |November 3, 2014 |DAILY BEAST 

The power delivered by the rocket motor was uneven and tricky to control. Virgin Galactic’s Flight Path to Disaster: A Clash of High Risk and Hyperbole |Clive Irving |November 1, 2014 |DAILY BEAST 

So the dependency story is strange and uneven, and especially nasty when it comes to women. Here’s Why You Helpless Tramps Don’t Vote Republican |Elizabeth Stoker Bruenig |August 31, 2014 |DAILY BEAST 

This stirring moment arrives in a chapter that spotlights the highs and lows of a persistently uneven book. The Twisted History of the Noose |Kevin Canfield |August 27, 2014 |DAILY BEAST 

Or were they merely orthodox through a more uneven balancing of their qualities, the animal in abeyance? Ancestors |Gertrude Atherton 

The shell is thick, and is surrounded by an uneven gelatinous envelop which is often stained with bile. A Manual of Clinical Diagnosis |James Campbell Todd 

The sweat of death was already on his brow as he reeled sideways, plunging blindly across the uneven tufts of grass. Uncanny Tales |Various 

The fields present a much more even appearance than similar fields in France, where the tobacco grown is small and uneven. Tobacco; Its History, Varieties, Culture, Manufacture and Commerce |E. R. Billings. 

But finally they were obliged to cut off only the uneven bits of cloth which hung around the edges. The Box-Car Children |Gertrude Chandler Warner