That system weakened as members of all three branches hewed increasingly to the platforms of the two political parties. How CEOs became the 4th branch of government |Felix Salmon |January 12, 2021 |Axios 

The xEMU has bearings that are lighter and hew closer to the joints than ones used for previous EMUs. Current spacesuits won’t cut it on the moon. So NASA made new ones. |Neel Patel |December 29, 2020 |MIT Technology Review 

The pandemic has been the ultimate stress test on us and on our nation, and it doesn’t hew to human timelines. The covid-era workplace: Hard questions, no easy answers |Karla Miller |December 17, 2020 |Washington Post 

All other Senate races hewed quite close to presidential results — and because this year’s Senate races took place largely on Republican turf, that was a big blow to Democrats’ efforts to flip the chamber. There Wasn’t That Much Split-Ticket Voting In 2020 |Nathaniel Rakich (nathaniel.rakich@fivethirtyeight.com) |December 2, 2020 |FiveThirtyEight 

Instead of hewing to the usual limit, we created five separate lists this year. Fortune’s 40 Under 40 honorees in tech defy the pandemic |rhhackettfortune |September 2, 2020 |Fortune 

That may make them more likely to, collectively, hew to a more moderate path when giving odds on the election. Intrade: An Ohio Story |Matthew Zeitlin |November 5, 2012 |DAILY BEAST 

Designers hew to their legacies for fall lines, while Armani argues against his reputation for elegance. Milan Fashion Week’s Big Finale: Versace and Dolce & Gabbana Wow, While Armani Falls Short |Robin Givhan |February 27, 2012 |DAILY BEAST 

After Jobs left, Scully says, the company tried to hew to his design philosophy. 7 Best Reads on Steve Jobs's Life |Josh Dzieza |October 6, 2011 |DAILY BEAST 

Behind all the finger-wagging is the idea that movies about history need to hew to facts. Heil, Tarantino! |Caryn James |August 20, 2009 |DAILY BEAST 

His object was to reach Allahabad that night—not to hew his way through opposing hordes and risk being cut down in the process. The Red Year |Louis Tracy 

Draw the belt tighter, my son, and hew me out this tree that is fallen across the road, for our campground is not here. The First Christmas Tree |Henry Van Dyke 

Then bade the King his men hew the ice and release his ships into the lake, and so went the men and set to work to hew the ice. The Sagas of Olaf Tryggvason and of Harald The Tyrant (Harald Haardraade) |Snorri Sturluson 

Look at the swagger of the vagabond who commands his braves, would you not think he was about to hew down everything in sight? The Pilgrim's Shell or Fergan the Quarryman |Eugne Sue 

The other man was negligible—a bovine lump of flesh without personality—born to hew wood and draw water for men of enterprise. The Hidden Places |Bertrand W. Sinclair