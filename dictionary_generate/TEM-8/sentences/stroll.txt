Along this 220-mile route, learn about Iowa’s connections to the Underground Railroad at the Todd House, prance through flower fields at the Loess Hills Lavender Farm, and go for a stroll in Waubonsie State Park. The Best Scenic Drive in Every State |eriley |August 26, 2021 |Outside Online 

The caterpillars that did crawl under streetlights were also heavier—not enough that you’d notice it while out on a stroll, but enough to be clear on a laboratory scale. Streetlights are making caterpillars grow up faster—and that’s a bad thing |Philip Kiefer |August 25, 2021 |Popular-Science 

This combo of breathability and warmth changes the way the world dresses for any chilly endeavor, from cycling to skiing, running to urban strolls. 11 Times Polartec Revolutionized How We Dress |bsmith |August 7, 2021 |Outside Online 

Jeremy King thought he’d probably be deprived of the simple pleasure of taking his child for a stroll. A teacher was pregnant. Students built a device so her spouse, who uses a wheelchair, can stroll with the baby. |Sydney Page |July 22, 2021 |Washington Post 

A recent one of mine took me on what was supposed to be a lovely evening stroll through New York City after a long work day of eye-frying screen time. The Best TV Shows of 2021 (So Far): From ‘Hacks’ to ‘WandaVision’ |Kevin Fallon |July 2, 2021 |The Daily Beast 

Williams said he went to the lake to take a stroll “because of his heart.” Exposed: The Gay-Bashing Pastor’s Same-Sex Assault |M.L. Nestel |December 21, 2014 |DAILY BEAST 

After the rally is over, Malloy joins Padilla for a stroll through Fair Haven. Dan Malloy Is Progressives’ Dream Governor. So Why Isn’t He Winning? |David Freedlander |October 30, 2014 |DAILY BEAST 

I hope I have time to stroll along the beautiful River Walk – it really is the heart and soul of the city! Women in the World Texas Sneak Peek |Cynthia Allum |October 20, 2014 |DAILY BEAST 

As we stroll through SoHo, we spot a tall, thin, pale man dressed in all black with bottle-blond hair. Chloe Sevigny on ‘The Cosmopolitans,’ New York’s Frat Boy Takeover, and ‘Asshole’ Michael Alig |Marlow Stern |August 24, 2014 |DAILY BEAST 

Tired of the classical sculptures, impressionist paintings, and chipped ancient relics you stroll past at your local art museum? Beware: Connecticut’s Museum of the Occult May Kill You |Nina Strochlic |July 3, 2014 |DAILY BEAST 

And when he took an underground stroll he was almost sure to find a few angleworms, which furnished most of his meals. The Tale of Grandfather Mole |Arthur Scott Bailey 

"I think I'll stroll down to the tavern and see this stranger," I replied carelessly. The Soldier of the Valley |Nelson Lloyd 

When the waltz is over they stroll out with them into the garden, and order wine, and talk of changing their steamer date. The Real Latin Quarter |F. Berkeley Smith 

To-dau I went for a stroll by the river in whose blue waters are mirrored the willows and the houses that befringe its banks. Marguerite |Anatole France 

And now let us take a stroll, or rather let us walk, for a stroll implies pleasure, and I certainly cannot promise you that. Friend Mac Donald |Max O'Rell