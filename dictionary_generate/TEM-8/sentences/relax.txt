Things like sitting in a pile of puppies, or trying new foods, or watching films, or even just relaxing with your partner’s arm around your shoulders. How to Live Big with a Chronic Illness |Blair Braverman |October 2, 2020 |Outside Online 

Because the injected muscle can no longer contract, the wrinkles soften and relax. Can you get too much Botox? |By Matthew J. Lin/The Conversation |October 1, 2020 |Popular-Science 

It’s the perfect setting for relaxing after a day spent exploring the adventure capital of Duluth. Our Favorite Hipcamp in Every State |Alison Van Houten |October 1, 2020 |Outside Online 

Coffee or lemongrass candles aid alertness, whereas lavender and chamomile candles help relax you. Looking for a small gift idea? Try one of these scented candles |PopSci Commerce Team |September 29, 2020 |Popular-Science 

Hold your arms straight along your body, just off the floor, with your shoulders relaxed and down. Time-Crunched? Try This Effective 10-Minute Workout |Hayden Carpenter |September 29, 2020 |Outside Online 

Young Living traffics in essential oils designed to help relax and rejuvenate. Honey Boo Boo, Snake Oil, and Ebola: The Weird World of Young Living Essential Oils |Kent Sepkowitz |December 5, 2014 |DAILY BEAST 

I tried to relax too, but I felt my stomach tighten and I began to sweat. I Shot Bin Laden |Elliot Ackerman |November 16, 2014 |DAILY BEAST 

Libkin found old beds to put in the wild-looking garden around Dacha, so couples in love could relax on them after the meal. In War-Torn Ukraine, Savva Libkin's Delicious Recipes for Survival |Anna Nemtsova |August 12, 2014 |DAILY BEAST 

Why won't anyone let Justin Bieber relax and have a good time?! An Unlikely Hero Blooms in Ibiza: Orlando Bloom Sort of Punches Justin Bieber |Amy Zimmerman |July 30, 2014 |DAILY BEAST 

But as the opening crawl assures, “none of this is canon, so just relax.” ‘Phineas and Ferb’ Pilot Disney’s Premier Voyage into ‘Star Wars’ |Jason Lynch |July 25, 2014 |DAILY BEAST 

You need but will, and it is done; but if you relax your efforts, you will be ruined; for ruin and recovery are both from within. Pearls of Thought |Maturin M. Ballou 

The women at once rose and began to shake out their draperies and relax their muscles. The Awakening and Selected Short Stories |Kate Chopin 

He braced himself unconsciously, and after Zeal's next words did not relax his body, although his lips turned white and stiff. Ancestors |Gertrude Atherton 

After that, one thing led to another, with the result that I offered to find somewhere else to relax. Fee of the Frontier |Horace Brown Fyfe 

He repairs to it with eagerness, and clings to it with a tenacity that time cannot relax, nor all the agonies of death dissolve. The Ordinance of Covenanting |John Cunningham