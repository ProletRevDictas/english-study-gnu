Her “ethics of care,” as she called it, involved encouraging the boys to take music lessons, read books and even meditate when she could persuade them to join her. The Climate Crisis Is Worse Than You Can Imagine. Here’s What Happens If You Try. |by Elizabeth Weil |January 25, 2021 |ProPublica 

Somehow, he finds time every day to exercise for an hour and to meditate. Advice for keeping your brain at its best |Katie Hafner |January 15, 2021 |Washington Post 

You can become someone who exercises and meditates every day and always drinks eight glasses of water. Can habit-tracking apps help bring some routine back to our quarantine lives? |Nisha Chittal |January 1, 2021 |Vox 

For the best results, consider turning off all push notifications at least while you’re meditating, and perhaps even for longer stretches of time surrounding your practice. How to make the most of meditation with science |Jessica Boddy |January 1, 2021 |Popular-Science 

The most bare-bones way to meditate is to focus on your breathing. How to make the most of meditation with science |Jessica Boddy |January 1, 2021 |Popular-Science 

Von Furstenberg generally refuses to meditate on the meaning of life. Diane von Furstenberg: How I Learned to Love My Wrap Dress |Lizzie Crocker |October 27, 2014 |DAILY BEAST 

As a Christian, I try to meditate or pray at least once a day, however briefly. Can We Lose the Violent Muslim Cliché? |Jay Parini |October 12, 2014 |DAILY BEAST 

There are yogis who actually meditate, pray, and are deeply concerned with the survival and well-being of our planet. When Gary Wright Met George Harrison: Dream Weaver, John and Yoko, and More |Gary Wright |September 29, 2014 |DAILY BEAST 

But the more you meditate on it, the deeper its wisdom becomes. Why Kids Are Making Us Crazy |James Poulos |March 30, 2014 |DAILY BEAST 

After I meditate, I shoot a strong espresso and go to the desk. How I Write: Paul Lynch |Noah Charney |December 18, 2013 |DAILY BEAST 

Much less would it help you to meditate upon the pure and holy things of God. The value of a praying mother |Isabel C. Byrum 

If it suits your evolutions, aunt Kitty and myself meditate a Sussex journey next week. Private Letters of Edward Gibbon (1753-1794) Volume 1 (of 2) |Edward Gibbon 

But some mornings I make observations through the tent flap that I cannot stay in bed to meditate on. The Red Cow and Her Friends |Peter McArthur 

Again he began to meditate an attempt to escape, and on a certain evening, set off from the convent. Fox's Book of Martyrs |John Foxe 

They then reviled him, and spurned him away from their sight, and began to meditate measures of violence against him. Fox's Book of Martyrs |John Foxe