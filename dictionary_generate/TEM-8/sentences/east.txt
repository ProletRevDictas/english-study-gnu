It’s moved to nearly every state east of the Rocky Mountains, and is likely to cause the virtual extinction of ash trees. Does a cold winter mean fewer bugs in the summer? |Philip Kiefer |February 25, 2021 |Popular-Science 

After zigzagging through the plains of north India for some 1,500 miles, it drains into the Bay of Bengal in India’s east. When a Body Meets a Boatman on the Ganges |Eugene Robinson |February 23, 2021 |Ozy 

Designed and constructed by master treehouse builder Pete Nelson, TreeHouse Point is a family-owned retreat in the Snoqualmie Valley, 30 minutes east of Seattle. 10 Adventure Lodges You Can Have to Yourself |Megan Michelson |February 23, 2021 |Outside Online 

You can also get there from State Route 140, which passes northeast through Merced, or east on State Route 120, which coasts through Groveland before connecting with the park’s Big Oak Flat Road. The Ultimate Yosemite National Park Travel Guide |Shawnté Salabert |February 22, 2021 |Outside Online 

Satellite images show that, between 1976 and 2000, there have been dramatic changes to the strips of land between Mount Kilimanjaro and Mount Meru, which is located less than 100 km to the east. What increased land use means for Mount Kilimanjaro’s already isolated ecosystem |Claudia Hemp |February 20, 2021 |Quartz 

According to Pew, 14 of the 20 countries in the Middle East and North Africa have blasphemy laws. In Defense of Blasphemy |Michael Tomasky |January 9, 2015 |DAILY BEAST 

“Please, please do not permit this to happen here in Florida,” wrote Cris K. Smith of East Polk County. Jeb Bush’s Unseen Anti-Gay Marriage Emails |Jackie Kucinich |January 9, 2015 |DAILY BEAST 

They took cover inside a print works to the north east of Paris, where they held a member of staff as a hostage. France Kills Charlie Hebdo Murderers |Nico Hines |January 9, 2015 |DAILY BEAST 

Sales are best in Asia, London, the Middle East, and Russia. Handbags: The More You Pay, The Smaller They Shrink |Elizabeth Landers |December 29, 2014 |DAILY BEAST 

Eventually, DeCrow and Seidenberg filed suit against the East Village mainstay. The Bars That Made America Great |Nina Strochlic |December 28, 2014 |DAILY BEAST 

Descending the Alps to the east or south into Piedmont, a new world lies around and before you. Glances at Europe |Horace Greeley 

The east window in this church has been classed as the A1 of modern painted windows. Showell's Dictionary of Birmingham |Thomas T. Harman and Walter Showell 

The cantonment was split into two sections by an irregular ravine, or nullah, running east and west. The Red Year |Louis Tracy 

Of course, my first row was a long one, quite through the city from west to east, including innumerable turnings and windings. Glances at Europe |Horace Greeley 

Turn we our backs to the cold gloomy north, to the wet windy west, to the dry parching east—on to the south! Blackwood's Edinburgh Magazine, Volume 60, No. 372, October 1846 |Various