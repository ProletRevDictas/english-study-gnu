While they can afford to spend influential sums on, say, trying to get a Democrat elected president, we might have only $10 or $100 to spend. Want to fight climate change effectively? Here’s where to donate your money. |Sigal Samuel |September 17, 2020 |Vox 

DeFazio, an Oregon Democrat, hasn’t yet unveiled his legislation. Boeing crashes were the “horrific culmination” of multiple mistakes, House report says |kdunn6 |September 16, 2020 |Fortune 

Few could have guessed that the Keystone State would eventually become the “keystone” of the Electoral College,34 since going into 2016, Pennsylvania had voted for the Democrat in six straight presidential elections. Why Pennsylvania Could Decide The 2020 Election |Nathaniel Rakich (nathaniel.rakich@fivethirtyeight.com) |September 15, 2020 |FiveThirtyEight 

Several Republicans and one Democrat said clearly that they supported calling one. Sacramento Report: Bipartisan Support for a Special Session |Sara Libby |September 11, 2020 |Voice of San Diego 

The Senate's top Democrat, Minority Leader Chuck Schumer, is throwing his weight behind an economic message that ties climate to goals around racial justice, income inequality, labor rights and a lot more. Democrats tie climate to racial justice and inequality agenda |Ben Geman |September 11, 2020 |Axios 

The New York governor was the foremost Democrat to stand athwart the Reagan Revolution. Mario Cuomo, a Frustrating Hero to Democrats, Is Dead at 82 |Eleanor Clift |January 2, 2015 |DAILY BEAST 

Such was the importance of showing the country that he was a “different kind of Democrat.” President Cuomo Would’ve Been a Lion |Jonathan Alter |January 2, 2015 |DAILY BEAST 

I never hear a Democrat talk about these goods, which are, in the literal sense, indivisible—for us all. The Democrats’ Black Hole—and What They Can Do About It |Michael Tomasky |December 31, 2014 |DAILY BEAST 

(Not one Democrat supported it on the procedural vote earlier Thursday afternoon). Nancy Pelosi Plays Hardball On Cromnibus |Ben Jacobs |December 11, 2014 |DAILY BEAST 

For example, 51 percent of North Carolinians voted that year for a Democrat to represent them in Congress. Seriously, Democrats: You’re Done in Dixie |Michael Tomasky |December 10, 2014 |DAILY BEAST 

It was in writing on these subjects that English writers borrowed the words aristocrat and democrat from the French writers. Stories That Words Tell Us |Elizabeth O'Neill 

He failed to see the editor of the "Fraser County Democrat" and peremptorily closed the incident. A Hoosier Chronicle |Meredith Nicholson 

When I came to this country, like the mass from beyond the sea, I was a Democrat; there was a charm in the name. Speech of John Hossack, Convicted of a Violation of the Fugitive Slave Law |John Hossack 

I could not tell from his face whether he were Democrat or Whig, for it changed not a whit. The Rose of Old St. Louis |Mary Dillon 

The boy is a better democrat when he leaves college than he will be later, if he goes into business. A Traveler from Altruria: Romance |William Dean Howells