She ought to find me supremely foolish, and her silence was not even that of rancour; it was contempt. Camille (La Dame aux Camilias) |Alexandre Dumas, fils 

The tiger, on the contrary, though glutted with carnage, has still an insatiate thirst for blood; his rancour has no intervals. Buffon's Natural History. Volume VII (of 10) |Georges Louis Leclerc de Buffon 

She was in that state that she could not have endured sharpness or rancour. Mary Gray |Katharine Tynan 

If there have been conflicts, they have left no rancour, no bitterness. American Sketches |Charles Whibley 

Bitterness invaded him; rancour, anger, scorn, and desires accumulated in his mind—as with lovers. The conquest of Rome |Matilde Serao