This is how the power of divinity was imagined, this kind of omniscience. Europe Is Saving Democracy From Big Tech, Says the Author of Surveillance Capitalism |Karl Vick |May 11, 2022 |Time 

That’s when he shared his recipes for divinity fudge and pralines with Nettie, who carried the secret to these sticky sweet nuggets — a mix of pecans, brown sugar, cream and butter — with her when she later relocated to Michigan. The Curious Warmth of Grandma’s Kitchen |Sohini Das Gupta |September 17, 2021 |Ozy 

“I thought if I could meld my sociology and city planning with divinity, I could get some things done in Haiti,” said Fisher, who serves as bishop of the Full Gospel Church of the Lord’s Missions International in the District. She has devoted her life to helping Haiti. She doesn’t plan to stop now. |Courtland Milloy |August 31, 2021 |Washington Post 

One thread running through all of this is the idea of getting into divinity. How Gideon the Ninth author Tamsyn Muir queers the space opera |Constance Grady |February 5, 2021 |Vox 

He holds a Master of Divinity from Southeastern Baptist Theological Seminary and a Master of Theology from Emory University. Conservative Christians Selectively Apply Biblical Teachings in the Same-Sex Marriage Debate |Kirsten Powers, Jonathan Merritt |February 23, 2014 |DAILY BEAST 

Data eclipsed God in 1973, and its continuing ascendance suggests a culture that treats it as a surrogate divinity. Why Big Data Doesn’t Live up to the Hype |Nick Romeo |January 4, 2014 |DAILY BEAST 

No one in the industry has been doing more to push the image of pop divinity than Lana. Lana Del Rey’s New Short Film ‘Tropico’ Is So Bad It Might Be Good |Jimmy So |December 5, 2013 |DAILY BEAST 

He considers the beauty of athleticism and wonders if it is proof of the existence of some divinity. David Foster Wallace, Traditionalist? Considering ‘Both Flesh and Not: Essays’ |David Masciotra |November 2, 2012 |DAILY BEAST 

A show called “Richteriana,” at Postmasters gallery in New York, subjects his divinity to some modest doubt. A Deity, Doubted |Blake Gopnik |June 4, 2012 |DAILY BEAST 

You may imagine the effect this missive produced upon the proud, high-minded doctor of divinity. Elster's Folly |Mrs. Henry Wood 

Philip Limborch died; a Dutch professor of divinity, and author of a history of the inquisition. The Every Day Book of History and Chronology |Joel Munsell 

Herbert Marsh, professor of divinity in the university of Cambridge, England, died. The Every Day Book of History and Chronology |Joel Munsell 

Frederick Spanheim died; a noted divinity professor at Leyden, and a voluminous writer. The Every Day Book of History and Chronology |Joel Munsell 

John Laurence Berti, a learned monk of Tuscany, died; author of about 20 quarto volumes of divinity. The Every Day Book of History and Chronology |Joel Munsell