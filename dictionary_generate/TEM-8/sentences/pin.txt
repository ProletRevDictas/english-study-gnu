It was a massive hit, and the safety-pin dress a brilliant cartoonish cherry on top of it. Happy 20th Birthday, Liz Hurley’s Safety-Pin Dress |Tim Teeman |December 12, 2014 |DAILY BEAST 

The online community of tattooed pin-up models turned 13 this year. Masters of Alt Sex: SuicideGirls Hits Puberty and Wants to Invade Your TV Set |Marlow Stern |December 9, 2014 |DAILY BEAST 

In other news, black and white pin-up shots are now officially less valuable than life-size Tiger Beat tear outs. Justin Bieber Isn’t Even 21, Yet Makes More Money Than Meryl Streep |Amy Zimmerman |November 25, 2014 |DAILY BEAST 

She had low-grade blood poisoning in her ear from the pin she used to pierce it. ‘My Crazy Love’ Reveals the Craziest Lies People Tell for Love |Kevin Fallon |November 18, 2014 |DAILY BEAST 

Lorna planted several pin flags, then we continued cruising. Knowing Where the Bodies Are Buried: An Excerpt From 'Lives in Ruins' |Marilyn Johnson |November 14, 2014 |DAILY BEAST 

Though, as everybody well knew, the doctor had forbidden her to lift so much as a pin! The Awakening and Selected Short Stories |Kate Chopin 

He held the pin delicately between finger and thumb, and controlled her with his roguish eyes. The Joyous Adventures of Aristide Pujol |William J. Locke 

The scolex is about the size of a pin-head, and is surrounded by four sucking discs, but has no hooklets (Fig. 96). A Manual of Clinical Diagnosis |James Campbell Todd 

A grain—requiring to be picked out with a pin and microscope—of truth, with a bushel of bunkum or cant. Tobacco; Its History, Varieties, Culture, Manufacture and Commerce |E. R. Billings. 

Welcome jerked himself away from the book, whirled around on his wooden pin and pointed his knife at the book-shelves. Motor Matt's "Century" Run |Stanley R. Matthews