Further, the appeals court remands the case back to Rosenberg for review to deliver a new ruling consistent with guidance on the First Amendment. Court strikes down bans on conversion therapy as violations of free speech |Chris Johnson |November 20, 2020 |Washington Blade 

We deliver them to every department within our hospital so that each and every person working has the opportunity to get the cookies and know the community is there supporting them and thinking of them. It started as a macho bake-off between dads. Months later, they’ve delivered 15,000 cookies to essential workers. |Sydney Page |November 20, 2020 |Washington Post 

If Hayes is going to deliver on that potential, he needed a landing spot that would give him a wide berth and a long runway. NBA draft winners and losers: A win for LaMelo Ball, woe for the Warriors |Ben Golliver |November 19, 2020 |Washington Post 

She calls them “mis hijitos,” or “my little sons,” and takes her own children to the weekend games or on the visits she makes to the complex to deliver groceries from the local food pantry. Inside the Lives of Immigrant Teens Working Dangerous Night Shifts in Suburban Factories |by Melissa Sanchez |November 19, 2020 |ProPublica 

Insulated water bottles are significantly heavier but deliver the magic of ice-cold liquids when the weather is hot and vice versa. How to Choose the Right Water Bottle for You |Joe Jackson |November 17, 2020 |Outside Online 

Compliments would earn their deliverer a stream of invective, while an insult or dirty joke “would earn his respect.” Paddy Chayefsky: The Dark Prophet of ‘Network’ News |Tim Teeman |February 16, 2014 |DAILY BEAST 

In the Dutch Walewein alone, so far as I know, is his rôle definitely that of the deliverer. The Three Days' Tournament |Jessie L. Weston 

The final parting between the tyrant and the future deliverer was not an event to be forgotten by any of the race of Nassau. The History of England from the Accession of James II. |Thomas Babington Macaulay 

The deliverer shot across their company one lightning glance—majesty, supremacy, scorn. God Wills It! |William Stearns Davis 

The Man rescues the Snake from beneath a rock, whereupon the Snake announces her intention of biting her deliverer. Nights With Uncle Remus |Joel Chandler Harris 

He felt that he had quite enough trouble without addition of records and secrecy for acts of the Deliverer. The Treasure Trail |Marah Ellis Ryan