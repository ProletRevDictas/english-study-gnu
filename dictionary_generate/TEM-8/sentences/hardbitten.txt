Just the hard-on before you shoot unarmed members of the public. 'Babylon' Review: The Dumb Lives of Trigger-Happy Cops |Melissa Leon |January 9, 2015 |DAILY BEAST 

But so-called jungle primaries are notoriously hard to predict or poll. The Golden State Preps for the ‘Red Wedding’ of Senate Races |David Freedlander |January 9, 2015 |DAILY BEAST 

My body used for his hard pleasure; a stone god gripping me in his hands. ‘A Gronking to Remember’ Speed Read: 8 Naughtiest Bits |Emily Shire |January 7, 2015 |DAILY BEAST 

All of my stories are about people trying hard not to grow up. Here’s the Lost Judd Apatow ‘Simpsons’ Episode, Penned by Judd Apatow |Asawin Suebsaeng |January 6, 2015 |DAILY BEAST 

We also have a growing body of biological research showing that fathers, like mothers, are hard-wired to care for children. How Good Dads Can Change the World |Gary Barker, PhD, Michael Kaufman |January 6, 2015 |DAILY BEAST 

He thought they were now in touch with our troops at "X" but that they had been through some hard fighting to get there. Gallipoli Diary, Volume I |Ian Hamilton 

However this be, it is hard to say that these fibs have that clear intention to deceive which constitutes a complete lie. Children's Ways |James Sully 

And it would be hard indeed, if so remote a prince's notions of virtue and vice were to be offered as a standard for all mankind. Gulliver's Travels |Jonathan Swift 

Even if poverty were gone, the flail could still beat hard enough upon the grain and chaff of humanity. The Unsolved Riddle of Social Justice |Stephen Leacock 

"I congratulate you on your engagement," he said at last, looking up with a face that seemed to Bernard hard and unnatural. Confidence |Henry James