Chlorine from the salt can inhibit fish from spawning and reduce dissolved oxygen levels in the water, which harms fish and other aquatic life. Fish blood could hold the answer to safer de-icing solutions during snowstorms |By Monika Bleszynski/The Conversation |February 1, 2021 |Popular-Science 

The aquatic blues of two Williams pictures that hang near “Wave Relief” complement its darker shades. In the galleries: Immersive exhibit explores a wonderland in blue |Mark Jenkins |January 22, 2021 |Washington Post 

To monitor aquatic animals and their underwater world, Deng often uses networks of such beacons. Bye-bye batteries? Power a phone with fabric or a beacon with sound |Kathryn Hulick |January 6, 2021 |Science News For Students 

These cells detect chemicals produced by many aquatic animals, explains Nicholas Bellono. Touching allows octopuses to pre-taste their food |Jonathan Lambert |January 4, 2021 |Science News For Students 

The photos depict nature, mostly in the form of aquatic birds, accommodating itself to AlexRenew’s water-treatment lagoons and the outfall pipes where effluent can enter the river. In the galleries: Up to his elbows in watery works and lustrous prints |Mark Jenkins |December 18, 2020 |Washington Post 

Hippopotamuses eat aquatic vegetation, like water hyacinths—loads of it, Irwin learned. Lake Bacon: The Story of The Man Who Wanted Us to Eat Mississippi Hippos |Jon Mooallem |August 10, 2014 |DAILY BEAST 

Description: A “swamp cabbage” is a semi-aquatic tropical plant. Craziest SXSW Band Names: Perfect Pussy, Death By Unga Bunga, and More |Marlow Stern |March 8, 2014 |DAILY BEAST 

Swimmers now enjoy its 12,000-square-meter aquatic theme park. Architectural White Elephants: Beijing, London, and the Post-Olympics Curse |Melinda Liu |August 14, 2012 |DAILY BEAST 

Paper presented at the 2008 World Aquatic Conference in Colorado Springs, Colo. 12. 15 Shocking Exercise Facts |Anneli Rufus |August 20, 2011 |DAILY BEAST 

They said that in any case the incident only highlights their worry that the Gulf is an aquatic tinderbox. Shocking Gulf Firefight Caught on Tape |Michael Adler |July 5, 2011 |DAILY BEAST 

The ponds were half covered with the white water-lily, and some other aquatic plants of the country. Journal of a Voyage to Brazil |Maria Graham 

Then happened one of those aquatic incidents which lend an atmosphere all their own to amphibious war. Gallipoli Diary, Volume 2 |Ian Hamilton 

At the foot of the hill lay a deep morass, covered with the nelumbo and other aquatic plants. Early Western Travels 1748-1846, Volume XVI |Various 

The first aquatic creatures were succeeded by the amphibia, the reptiles. Astronomy for Amateurs |Camille Flammarion 

A little more complicated than that of the fish, this method is probably older, and seems possible only for aquatic animals. The Natural Philosophy of Love |Remy de Gourmont