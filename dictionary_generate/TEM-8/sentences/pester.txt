Bauer has pestered women who challenge him on Twitter, a habit made more noticeable by the fact that he has not handled male critics on the platform so pointedly. Trevor Bauer, unorthodox star with an unorthodox deal, gets an unorthodox Dodgers intro |Chelsea Janes |February 12, 2021 |Washington Post 

Given how quickly I was wolfing books down, I didn’t want to pester my parents to drive me there every other day. Five Scientists on the Heroes Who Changed Their Lives - Issue 93: Forerunners |Alan Lightman, Hope Jahren, Robert Sapolsky, |December 2, 2020 |Nautilus 

We just understood this as another tool to pester with our living situation. Podcast: Facial recognition is quietly being used to control access to housing and social services |Tate Ryan-Mosley |December 2, 2020 |MIT Technology Review 

“The question of what determines biological diversity has pestered evolutionary biologists ever since Wallace and Darwin came up with their theory of evolution,” Salzburger said. New Fish Data Reveal How Evolutionary Bursts Create Species |Elena Renken |December 1, 2020 |Quanta Magazine 

This acceleration in bee-pestered plants is not entirely a surprise. Bumblebees may bite leaves to spur plant blooming |Susan Milius |July 2, 2020 |Science News For Students 

And resist the urge to probe or pester for reassuring answers. Zodiac Beast: May 1-7 |Starsky + Cox |April 30, 2011 |DAILY BEAST 

Do not continually pester either your companion or the conductor with questions, such as "Where are we now?" The Ladies' Book of Etiquette, and Manual of Politeness |Florence Hartley 

Do you want me to pester every office in the government with new inquiries? Paul Patoff |F. Marion Crawford 

The Landhofmeisterin continued to pester the Duke to convey her to Frankfort. A German Pompadour |Marie Hay 

"No fear, with my old woman to pester me," answered Richard, with a grim relaxing of his features. For John's Sake |Annie Frances Perram 

The next year he became editor-in-chief to the "Pester Lloyd," raising that paper to a high level of excellence. Some Jewish Witnesses For Christ |Rev. A. Bernstein, B.D.