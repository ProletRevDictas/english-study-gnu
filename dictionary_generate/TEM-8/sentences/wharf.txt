There were no trees in sight at the wharf to offer any shade. Greek air-conditioning limits test country’s resolve to support Ukraine |Michael Birnbaum, Elinda Labropoulou |August 5, 2022 |Washington Post 

With more money, the port could have expanded channels, fortified wharves and improved road and rail links, he said. Inside America’s Broken Supply Chain |David J. Lynch |September 30, 2021 |Washington Post 

Chiefs of the Mi’kmaq Grand Council join members of the Sipekne’katik First Nation on the wharf in Saulnierville, Nova Scotia, to bless the fleet before it launches a self-regulated fishery. The Lobster Attacks Striking at Canada’s Friendly Image |Charu Kasturi |October 29, 2020 |Ozy 

Then bed down in the seaside town of Mystic, Connecticut, with views of the wharf from your private room at the Steamboat Inn. The U.S. Road Trips You Should Really Take |Lonely Planet |April 26, 2014 |DAILY BEAST 

Sex is no less grotesque; during the act he hears “the wave-against-a-wharf smack of rubber on flesh.” American Dreams, 1933: Miss Lonelyhearts by Nathanael West |Nathaniel Rich |April 29, 2013 |DAILY BEAST 

Afterward, stumble out into North Beach and walk it off on a stroll down to the Wharf. The Easygoing Flair of San Francisco |Jolie Hunt |April 10, 2010 |DAILY BEAST 

And when I say East London, I mean Shoreditch—those of you expecting sterile Canary Wharf, think again. Gal With a Suitcase |Jolie Hunt |November 21, 2009 |DAILY BEAST 

When there was a big concert, the whole wharf and Main Street became transformed. The Great Summer Read Is Here |Jane Ciabattari |February 19, 2009 |DAILY BEAST 

She had just left the wharf at Cincinnati for Louisville, with 225 passengers on board, of whom but 124 were saved. The Every Day Book of History and Chronology |Joel Munsell 

Dick was at the wharf, one day last week, when one of the up river boats arrived. The Book of Anecdotes and Budget of Fun; |Various 

Robert uttered a shrill, piercing whistle which might have been heard back at the wharf. The Awakening and Selected Short Stories |Kate Chopin 

The lovers, who had laid their plans the night before, were already strolling toward the wharf. The Awakening and Selected Short Stories |Kate Chopin 

In both cases the riparian owner, so-called, may erect a wharf extending from his land subject to public control. Putnam's Handy Law Book for the Layman |Albert Sidney Bolles