“I had to have tricks, like moving through the set with my eyes downcast, so that when I opened my eyes I’d be experiencing everything only as Cora, because otherwise it would be too much for Thuso to take in,” Mbedu says. Filming ‘The Underground Railroad’ was grueling. But the cast grasped ‘the weight of what we were doing.’ |Stuart Miller |May 13, 2021 |Washington Post 

Others were more downcast in acknowledging the loss, which was far closer than pollsters predicted. ‘Today, We Celebrate. Tomorrow, We Work’ |Nick Fouriezos |November 8, 2020 |Ozy 

The soaked one squats on the bridge, eyes furious and downcast. Whatever You Do Someone Will Die. A Short Story About Impossible Choices in Iraq |Nathan Bradley Bethea |August 31, 2014 |DAILY BEAST 

As he was taken down from the dock to be driven to prison he was downcast, as anyone would be who was publicly sacrificed. Murdoch on the Rocks: How a Lone Reporter Revealed the Mogul's Tabloid Terror Machine |Clive Irving |August 25, 2014 |DAILY BEAST 

If we must confess it, they are quite offended and downcast when the cartoons stop. Churchill Would Be Famous Today on the Strength of His Writing Alone |Anthony Paletta |June 16, 2014 |DAILY BEAST 

He is downcast, and a senior member of his party comforts him in these words. The Case for Trollope |David Frum |February 5, 2013 |DAILY BEAST 

Despite the fuzzy pictures, the Daily Mail was quick to point out the star looked "miserable" and “downcast.” 8 Reported Facts About the Cruise-Holmes Divorce |Nina Strochlic |July 1, 2012 |DAILY BEAST 

The old man stood, with eyes downcast and hands clasped before him, a picture of humility. The Soldier of the Valley |Nelson Lloyd 

Her agitation was plain to him, and it puzzled him, as did the downcast glance of eyes usually so bold and insolent in their gaze. St. Martin's Summer |Rafael Sabatini 

Pale as usual, and with downcast eyes, she entered the room, whither her mother called her. Skipper Worse |Alexander Lange Kielland 

The fire in the stove had burned lower, and its downcast glow revealed less mercilessly the dirty condition of the floor. Dope |Sax Rohmer 

Henri found her there, at something before nine, rather downcast and worried, and debating about going up to bed. The Amazing Interlude |Mary Roberts Rinehart