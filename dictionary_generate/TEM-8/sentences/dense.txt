All seven worlds are almost identical to each other but slightly less dense than Earth, the team reports in the February Planetary Science Journal. Two exoplanet families redefine what planetary systems can look like |Lisa Grossman |February 5, 2021 |Science News 

The third planet is nearly the wispiest of the whole system, less dense than Jupiter. These 6 exoplanets somehow orbit their star in perfect rhythm |Charlie Wood |January 27, 2021 |Popular-Science 

The holes mean the mattress is less dense, though, and might not last as long. Best memory foam mattress: Sleep better on one of these picks |PopSci Commerce Team |January 25, 2021 |Popular-Science 

In the rush to convince people that electric vehicles are a practical alternative to gas-guzzlers, most of the focus has been on boosting their range by creating ever more energy-dense batteries. New Fast-Charging, Low-Cost Batteries Could Be a Game-Changer for Electric Cars |Edd Gent |January 25, 2021 |Singularity Hub 

If you want to eat healthfully, a better approach is to prioritize nutrient-dense foods—fruits, vegetables, whole grains, nuts, seeds, legumes, healthy oils, and lean proteins—without vowing to only eat these foods. There Are No Rules for Healthy Eating |Christine Byrne |January 22, 2021 |Outside Online 

He had a special knife designed to cut the dense loaf, and a ceremony to precede cutting the cake. One Cake to Rule Them All: How Stollen Stole Our Hearts |Molly Hannon |December 24, 2014 |DAILY BEAST 

The narrowest piece of land was at Panama, but it was covered in dense, mountainous jungle. China’s Nicaragua Canal Could Spark a New Central America Revolution |Nina Lakhani |November 30, 2014 |DAILY BEAST 

For those in the dense forests, beaches, and towns of West Africa, it is a real threat. The Sham, Scaremongering Guide to Ebola |Abby Haglage |November 20, 2014 |DAILY BEAST 

These days they are occasional meteorological irruptions, white river mists, not dense and toxic industrial pea-soupers. Sherlock Holmes Vs. Jack the Ripper |Clive Irving |November 16, 2014 |DAILY BEAST 

In the dense atmosphere of tobacco and conspiracy, one hot topic has been the death penalty. Ukraine Rebels Love Russia, Hate Gays, Threaten Executions |Anna Nemtsova |October 25, 2014 |DAILY BEAST 

No trail was so obtuse, no thicket so dense that members of that regiment would not track them to their lair. The Courier of the Ozarks |Byron A. Dunn 

The road on which the Federals were marching was narrow and on each side lined with dense underbrush. The Courier of the Ozarks |Byron A. Dunn 

The advance had to be carefully made, for the country was rough, wooded, and covered with a dense undergrowth of bushes. The Courier of the Ozarks |Byron A. Dunn 

Not having completed the loading of his gun, Tom hastily rode behind a dense bush, and concealed himself as well as he could. Hunting the Lions |R.M. Ballantyne 

Tom jumped behind a bush, and as they passed tried to fire, but the foliage was so dense that he failed to get a good aim. Hunting the Lions |R.M. Ballantyne