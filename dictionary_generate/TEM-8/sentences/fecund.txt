Virgin Galactic was one of a crop of private companies to sprout, mushroom-like, from the fecund decay of American spaceflight in the early 2000s. What’s driving the pilots that will fly paying customers into space? |Amelia Urry |June 11, 2021 |Washington Post 

Monica West’s “Revival Season” is an emotionally fecund and spellbinding debut novel. The spellbinding ‘Revival Season’ makes Monica West an author to watch |Naomi Jackson |May 28, 2021 |Washington Post 

Best to go with the fecund middle period, three novels sometimes referred to as “The London Trilogy.” Remedial Reader: The Essential Martin Amis |Ronald K. Fried |August 24, 2012 |DAILY BEAST 

That openness made the early Google a chaotically fecund operation. The Google Religion |Josh Dzieza |July 15, 2011 |DAILY BEAST 

Whether he was writing about sex, golf, or life in a small town, the fecund mind who gave the world Rabbit was never at rest. John Updike's Final Chapter |Daphne Merkin |January 29, 2009 |DAILY BEAST 

The more religion appeals to the senses, the more fecund has been the vocabulary of oaths. A Cursory History of Swearing |Julian Sharman 

They are conceived of fecund nods and looks, of the germination of writing and initials and signatures and contract-stamps. Mushroom Town |Oliver Onions 

A new sense came to her, not altogether depressing, of life's fecund possibility for unhappiness. The Open Question |Elizabeth Robins 

And this sea also pleases me by the treasures of fecund life which I know to abound in its dark depths. The Sea |Jules Michelet 

Besides, a science made solely in view of applications is impossible; truths are fecund only if bound together. The Foundations of Science: Science and Hypothesis, The Value of Science, Science and Method |Henri Poincar