The Cornhuskers, who returned to play Saturday, have relied on quick turnarounds between matchups in the hopes of making up their lost games. In schedule shuffle, Maryland will host Nebraska on back-to-back days next week |Emily Giambalvo |February 12, 2021 |Washington Post 

His name was lost among the dozens of teenagers chasing the dream of playing abroad, kids contracted by first-tier clubs and toiling in the developmental flights. Matthew Hoppe was a little-known American soccer player — until he reached the Bundesliga |Steven Goff |February 11, 2021 |Washington Post 

With their NCAA tournament hopes flickering, the Terrapins lost, 73-65, at Xfinity Center after allowing the Buckeyes to control the game in the second half. Maryland misses a chance to boost its NCAA tournament hopes with a loss to No. 4 Ohio State |Emily Giambalvo |February 9, 2021 |Washington Post 

For the most part, as soon as one team started losing, players on that team would begin to quit, with AI players taking their position. Keep the football mode in ‘Rocket League,’ you cowards |Mikhail Klimentov |February 8, 2021 |Washington Post 

TNT, which broadcasts the All-Star Game, would lose big in this scenario, and Silver might face criticism for bowing to the stars’ complaints. Four options for an NBA All-Star Game that the star players don’t seem to want |Ben Golliver |February 8, 2021 |Washington Post 

A lot of people ring in the New Year with vows to lose weight and exercise. How Skinny Is Too Skinny? Israel Bans ‘Underweight’ Models |Carrie Arnold |January 8, 2015 |DAILY BEAST 

Specifically, the pilots got themselves into a high altitude stall, where the wings lose the capacity to provide lift. Flight 8501 Poses Question: Are Modern Jets Too Automated to Fly? |Clive Irving |January 4, 2015 |DAILY BEAST 

The problem, says UC Davis physiologist and nutritionist Linda Bacon, is that very few people can lose weight and keep it off. Why Your New Year’s Diet Will Fail |Carrie Arnold |December 30, 2014 |DAILY BEAST 

“I guess it was their first incident where they lose a plane,” said Dobersberger, the travel agent. The Presumed Crash of AirAsia Flight QZ8501 Is Nothing Like MH370 |Lennox Samuels |December 29, 2014 |DAILY BEAST 

If anything, it would lose money gently, elegantly, hopefully not very much at one time. The Bookstore That Bewitched Mick Jagger, John Lennon, and Greta Garbo |Felice Picano |December 16, 2014 |DAILY BEAST 

There was not a moment to lose, for one well-directed shot might exterminate half of us. Blackwood's Edinburgh Magazine, No. CCCXXXIX. January, 1844. Vol. LV. |Various 

That he might lose his head and 'introduce an element of sex' was conscience confessing that it had been already introduced. The Wave |Algernon Blackwood 

Speaking with a certain dignity and using the language of the court, he said that they had not a moment to lose. The Red Year |Louis Tracy 

Fearing to lose his way, he bawls over the banister, and through the corridors, “Is any one there?” Checkmate |Joseph Sheridan Le Fanu 

When Tim hesitates he loses his temper as a sensible man should lose it—he buries it, and his indomitable good humor wins. The Soldier of the Valley |Nelson Lloyd