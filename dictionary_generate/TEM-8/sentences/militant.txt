It was suspected he had joined the Hizbul Mujahideen militant outfit. Four Sons, One War: All Dead |Eugene Robinson |September 25, 2020 |Ozy 

One of my professors asked me to write an article about young men who come from well-off families and are mostly well-educated, but who leave their studies and join the militants. Four Sons, One War: All Dead |Eugene Robinson |September 25, 2020 |Ozy 

It turned out that security personnel had shot dead two suspected militants in downtown Srinagar, and the government had turned off all connectivity to prevent the news from circulating and protesters from gathering. How India became the world’s leader in internet shutdowns |Katie McLean |August 19, 2020 |MIT Technology Review 

At the end of a 20-hour operation in Chewa Ullar village in south Kashmir on June 26, three militants lay dead. Is Kashmir’s Militancy Dead? |Charu Kasturi |August 6, 2020 |Ozy 

Cases were concentrated in the North Kivu and Ituri Provinces, and health officials struggled against militant groups and misinformation to contain the virus. The second-worst Ebola outbreak ever is officially over |Helen Thompson |June 25, 2020 |Science News 

But the enemy of the new emirs is neither the Jew nor the Christian, it is the godless militant defending secularism. Houellebecq’s Incendiary Novel Imagines France With a Muslim President |Pierre Assouline |January 9, 2015 |DAILY BEAST 

At least one child in CAR has been killed or gravely injured per day, and 10,000 have been recruited into militant groups. The Year’s Most Forgotten Humanitarian Crisis |Nina Strochlic |January 1, 2015 |DAILY BEAST 

This led to the formation of a Christian militant group to counter the rebels, and all-out sectarian violence exploded. The Year’s Most Forgotten Humanitarian Crisis |Nina Strochlic |January 1, 2015 |DAILY BEAST 

Among those claimed dead is Abu Muslim al-Turkmani, the most senior militant leader slain this year. Iraqi Kurds Get Their Groove Back, End Siege of Mount Sinjar |Jamie Dettmer |December 20, 2014 |DAILY BEAST 

Better, she says, to use local Muslim leaders and others in civil society to criticize militant arguments. ISIS Has a Message. Do We? |Jamie Dettmer |December 8, 2014 |DAILY BEAST 

Daphne was still erect, self-confident, militant; whereas Madeleine knew herself vanquished—vanquished both in body and soul. Marriage la mode |Mrs. Humphry Ward 

His policy, formerly in the main a conciliatory one, now became militant. The Two Great Republics: Rome and the United States |James Hamilton Lewis 

Nothing could have been more militant than her denunciation of militancy. Ramsey Milholland |Booth Tarkington 

Gus gazed calmly at the five militant youths in front of him. Radio Boys Loyalty |Wayne Whipple 

They will always remember that thou wert their faithful mother, their robust nurse, and their church militant. Continental Monthly, Vol. 4, No 3, September 1863 |Various