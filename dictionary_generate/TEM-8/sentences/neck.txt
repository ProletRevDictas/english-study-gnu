There are numerous ways in which a poorly adjusted chair can do damage to your body, from causing poor circulation to straining your neck. Best desk chair for any home office |PopSci Commerce Team |February 11, 2021 |Popular-Science 

When you hit the couch and turn on the television after a long day of work, the last thing you want is to crane and cramp your neck attempting to get a clear and comfortable view. The best wall mounts to optimize TV viewing |PopSci Commerce Team |February 5, 2021 |Popular-Science 

In the ’70s or ’80s, you’d have a cross or Star of David around your neck. Peloton makes toning your glutes feel spiritual. But should Jesus be part of the experience? |Michelle Boorstein |February 5, 2021 |Washington Post 

The snake then stretches its neck up and repeats the process. Newfound technique allows some tree snakes to climb wide trees |Maria Temming |February 1, 2021 |Science News For Students 

Feed one a photo of a man cropped right below his neck, and 43% of the time, it will autocomplete him wearing a suit. An AI saw a cropped photo of AOC. It autocompleted her wearing a bikini. |Karen Hao |January 29, 2021 |MIT Technology Review 

But his fingers moved through her silky strands of hair, and then down her neck. Powerful Congressman Writes About ‘Fleshy Breasts’ |Asawin Suebsaeng |January 7, 2015 |DAILY BEAST 

His chin rested on the thick plastic collar buckled around his neck. Dungeons and Genital Clamps: Inside a Legendary BDSM Chateau |Ian Frisch |December 20, 2014 |DAILY BEAST 

I received many bruises on my collarbones, neck, chest, and shoulders. Beaten By His Church for Being Gay |Justin Jones |December 16, 2014 |DAILY BEAST 

Police Officer Daniel Pantaleo then sought to bring the hulking Garner down by yoking him around the neck. Eric Garner Was Just a Number to Them |Michael Daly |December 5, 2014 |DAILY BEAST 

The 21-year-old was shot three times—twice in the back and once in the back of his neck. Chicago’s Cops Don’t Even Get Investigated for Shooting People in the Back |Justin Glawe |December 5, 2014 |DAILY BEAST 

One would not have wanted her white neck a mite less full or her beautiful arms more slender. The Awakening and Selected Short Stories |Kate Chopin 

For of sadness cometh death, and it overwhelmeth the strength, and the sorrow of the heart boweth down the neck. The Bible, Douay-Rheims Version |Various 

As the window dropped, Ripperda saw the wounded postilion fall on the neck of his horse. The Pastor's Fire-side Vol. 3 of 4 |Jane Porter 

About her neck was hung a covered basket and a door-key; and Davy at once concluded that she was Sindbad's house-keeper. Davy and The Goblin |Charles E. Carryl 

The governor attacked him, and on the way down stabbed him in the neck, with such force that he tripped and fell down. The Philippine Islands, 1493-1898, Volume XX, 1621-1624 |Various