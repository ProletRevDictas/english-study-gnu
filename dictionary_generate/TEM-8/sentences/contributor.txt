Next year, the organization should name Kenny Washington as one of the important players and contributors to the game. Kenny Washington’s time has come. NFL needs to recognize the man who broke the color barrier. |Fred Bowen |February 10, 2021 |Washington Post 

Wide receiver Antonio Brown was a major contributor with two catches for 21 yards after missing the NFC title game with a knee injury. Super Bowl highlights: Bucs celebrate championship, Tom Brady wins MVP |Des Bieler, Mark Maske, Chuck Culpepper |February 8, 2021 |Washington Post 

She is a senior contributor for Forbes and High Country columnist for the Aspen Times with other work appearing in the Denver Post, Modern Luxury, Curbed, Thrillist, and more. 24 Exciting Ways to Eat in Park City Right Now |Katie Shapiro |February 4, 2021 |Eater 

They added key defensive contributors such as safety Tyrann Mathieu and defensive end Frank Clark, and they subsequently improved from 30th to 13th in schedule-adjusted EPA on that side of the ball. There’s More Than One Way To Build A Super Bowl Team |Neil Paine (neil.paine@fivethirtyeight.com) |February 2, 2021 |FiveThirtyEight 

Kathi Wolfe, a writer and poet, is a regular contributor to the Blade. A gov’t that represents all Americans gives us hope |Kathi Wolfe |January 29, 2021 |Washington Blade 

The omission of contributor information on future reports should not be assumed to be an oversight. Mystery Man Buys Kentucky for the GOP |Center for Public Integrity |October 29, 2014 |DAILY BEAST 

Daily Beast contributor Dr. Kent Sepkowitz joined The Colbert Report last night to talk Ebola and whether we're all going to die. Dr. Kent Goes Toe-to-Toe With Colbert On Ebola |Kent Sepkowitz |October 3, 2014 |DAILY BEAST 

A Fox News contributor went so far as to showcase the many brands of boot available to our troops. Dumpster Politicians, Jeter Tributes, and More Viral Videos |Jack Holmes |September 21, 2014 |DAILY BEAST 

To date no conclusion can be made as to the contributor to the incident and it would be sub judice to say so. Who Gagged the Search for MH370? |Clive Irving |June 22, 2014 |DAILY BEAST 

“That is a great idea,” fellow roundtable contributor Carson Daly stated. Miranda Kerr Debuts Son In ‘Vogue’ Australia; Diane Kruger Designs for Jason Wu |The Fashion Beast Team |June 13, 2014 |DAILY BEAST 

He was a bookseller, but better known as a translator of the German contributor to the Gentleman's Magazine, &c. The Every Day Book of History and Chronology |Joel Munsell 

These Orchestras are chiefly selected from the ranks of the people, of whom the artisan is the chief contributor. Violins and Violin Makers |Joseph Pearce 

To the Railway News he was a valued contributor, and in railway polemics a master. Fifty Years of Railway Life in England, Scotland and Ireland |Joseph Tatlow 

The Felibrean festivities continue, the numerous publications in the Provençal tongue still have in him a constant contributor. Frdric Mistral |Charles Alfred Downer 

John Powell of Richmond is considered Virginia's greatest single contributor to the musical composition field. Hallowed Heritage: The Life of Virginia |Dorothy M. Torpey