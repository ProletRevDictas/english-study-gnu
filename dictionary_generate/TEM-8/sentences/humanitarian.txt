Israel must approve all imports to Palestinian land, including medical supplies, even those donated by humanitarian organizations. Israel leads the world in vaccination rates, but a key group is missing from the data |By Yara M. Asi/The Conversation |February 3, 2021 |Popular-Science 

I hope that world leaders will come together to address the climate crisis with the urgency and ambition needed to avert such a global humanitarian issue. The Paris climate pact is 5 years old. 5 youth activists share their hopes for what’s next. |Jariel Arvin |December 11, 2020 |Vox 

A government-imposed communications blackout cut off the internet and phone lines, limiting access for journalists and humanitarian groups. Ethiopia’s unfolding humanitarian crisis, explained by a top aid official |Jen Kirby |December 3, 2020 |Vox 

“Home Work,” by Julie Andrews with Emma Walton HamiltonThe Oscar-winning actress looks back on her life in Hollywood — filming “The Sound of Music” and “Mary Poppins,” among other hits — and her humanitarian work in Vietnam. October paperback releases: 14 books to read now |Stephanie Merry |October 30, 2020 |Washington Post 

Numerous countries are opening their doors to these migrants, both for humanitarian reasons as well as the opportunity to attract high-skilled workers. Countries are competing for skilled migrants fleeing crackdowns in Hong Kong and Belarus |Mary Hui |October 16, 2020 |Quartz 

Paula Kweskin is an attorney specializing in international humanitarian and human rights law. Promoting Girls’ Education Isn’t Enough: Malala Can Do More |Paula Kweskin |December 9, 2014 |DAILY BEAST 

With the harsh Middle Eastern winter approaching fast, what people in Syria and Iraq need most, in fact, is humanitarian support. Dutch Biker Gangs Vs. ISIS |Nadette De Visser, Christopher Dickey |December 9, 2014 |DAILY BEAST 

By 27 September, there were 1,400 people in the grounds of the embassy, creating a small humanitarian crisis. How Havel Inspired the Velvet Revolution |Michael Zantovsky |December 6, 2014 |DAILY BEAST 

Humanitarian organizations had already pulled out, and French troops rushed in to extract the 15 foreigners left in the city. ‘Argo’ in the Congo: The Ghosts of the Stanleyville Hostage Crisis |Nina Strochlic |November 23, 2014 |DAILY BEAST 

The precious cargo: two American humanitarian workers with Ebola. The American Ebola Rescue Plan Hinges on One Company. Meet Phoenix. |Abby Haglage |November 22, 2014 |DAILY BEAST 

How literally true it is that in this special form of social and humanitarian work we are seeking to save that which is lost! Fighting the Traffic in Young Girls |Various 

Humanitarian movements,—the abolition of the slave trade, the cause of Greece and Italy,—were European. The Life of Mazzini |Bolton King 

The idealism of the eighteenth century was not reformative and humanistic, but revolutionary and humanitarian. An Epitome of the History of Medicine |Roswell Park 

There was something inhuman even in their humanitarian zeal. The Outline of History: Being a Plain History of Life and Mankind |Herbert George Wells 

For hospitals are not engaged in a gainful pursuit, regardless of all humanitarian considerations. Making Both Ends Meet |Sue Ainslie Clark and Edith Wyatt