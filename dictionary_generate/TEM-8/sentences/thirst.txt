Agreeing to those terms appears nearly impossible for Schumer, given the growing thirst in his base for a filibuster-free Senate. The case against Democrats nuking the filibuster |Aaron Blake |January 22, 2021 |Washington Post 

Like pain or thirst, boredom is an emotional state that tells us we need to change our behavior. There’s a right and wrong way to be bored |Sara Chodosh |January 22, 2021 |Popular-Science 

Search marketers who have a thirst and curiosity to continually learn and grow are the ones that achieve excellence. Five ways to become a ‘Search Sherpa’ for your own organization |Lisa Little |December 7, 2020 |Search Engine Land 

Harassment and probable hospitalization of Luis Manuel Otero Alcántara or Maykel Osorbo, the two group members who also began a thirst strike, was expected. A hunger strike in San Isidro, the protest that does not let Havana sleep |Maykel González Vivero |November 30, 2020 |Washington Blade 

Because I think they often reflect a kind of thirst for knowledge, which I think is actually healthy. 5 Psychology Terms You’re Probably Misusing (Ep. 334 Rebroadcast) |Stephen J. Dubner |January 9, 2020 |Freakonomics 

Petty, shade, and thirst are my favorite human “virtues” and the trifecta of any good series of “stories.” ‘Empire’ Review: Hip-Hop Musical Chairs with an Insane Soap Opera Twist |Judnick Mayard |January 8, 2015 |DAILY BEAST 

This spatial displacement reveals your thirst for freedom, your desire for openness and to break with the protest novel. Living Black & Gay in the ’50s |Alain Mabanckou |December 3, 2014 |DAILY BEAST 

The hunger usually subsides quickly, but thirst sometimes causes serious pain. The Nurse Coaching People Through Death by Starvation |Nick Tabor |November 17, 2014 |DAILY BEAST 

In North Carolina, they let a 54-year-old untreated schizophrenic die of thirst after 35 days in solitary confinement. Here’s a Reform Even the Koch Brothers and George Soros Can Agree On |Tina Brown |November 10, 2014 |DAILY BEAST 

In response, voters thought voting for Madison was inconsistent with their thirst for free booze. Founding Fathers Loved Drunk Voters |Kevin Bleyer |November 1, 2014 |DAILY BEAST 

It was he who first said, If thine enemy hunger give him food, if he thirst give him drink. Solomon and Solomonic Literature |Moncure Daniel Conway 

He just got a good holt–a shore enough diamond hitch–on that thirst-parlour dawg, and chawed. Alec Lloyd, Cowpuncher |Eleanor Gates 

The needy and the poor seek for waters, and there are none: their tongue hath been dry with thirst. The Bible, Douay-Rheims Version |Various 

Some were already dying of thirst; others were too sick and weak to help in the care of the ship. Alila, Our Little Philippine Cousin |Mary Hazelton Wade 

But this was but a passing phase, and soon the thirst for glory called the young soldier to sterner things. Napoleon's Marshals |R. P. Dunn-Pattison