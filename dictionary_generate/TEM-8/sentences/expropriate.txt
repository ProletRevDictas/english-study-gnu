Its rule of law codified difference, curtailed freedoms, expropriated land and property, and ensured a steady stream of labor for the mines and plantations, the proceeds from which helped fuel Britain’s economy. Britain Can No Longer Hide Behind the Myth That Its Empire Was Benign |Caroline Elkins |April 2, 2022 |Time 

Perhaps most controversially, HKND is authorized to expropriate land wherever it wants. China’s Nicaragua Canal Could Spark a New Central America Revolution |Nina Lakhani |November 30, 2014 |DAILY BEAST 

That way, if Maduro o un revergo de esos comes to expropriate me, they can take the farm. Venezuelan Socialism in Practice |David Frum |March 18, 2013 |DAILY BEAST 

Iraq is now a sovereign state and its power to expropriate Camp Ashraf, after paying appropriate compensation, cannot be doubted. Iraq's Looming Massacre of Iranian MEK Refugees |Geoffrey Robertson |December 9, 2011 |DAILY BEAST 

Barrs would not come to expropriate his cauliflowers and early potatoes. A Tatter of Scarlet |S. R. Crockett 

The old theory was that the state would expropriate this industry and become the employer of all engaged in it. Twentieth Century Socialism |Edmond Kelly 

In taking over the waterways the Realm acquires the right to expropriate, to fix rates, and to administer the river police system. The New Germany |George Young 

Our watchword must be: to arm the proletariat so that it may defeat, expropriate, and disarm the bourgeoisie. Bolshevism |John Spargo 

The State makes him pay taxes; it ventures to expropriate him for the public good. Anarchism and Socialism |George Plechanoff