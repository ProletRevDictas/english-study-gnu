It has been a momentous year for tackling issues of social justice in the workplace—one that has seen many who are not directly impacted by racial and gender inequality look to step up to the plate as allies to the cause. Corporate leaders strive to make allyship a real thing at work |reymashayekhi |September 24, 2020 |Fortune 

To me, the most important thing is just to be eager to learn, find good allies, find good mentors. Women in search: Why allies and networking are critical |Ginny Marvin |September 22, 2020 |Search Engine Land 

The deal that Perry and his allies pursued for three years while he was in Washington didn’t die when he stepped down and returned to Texas. Rick Perry’s Ukrainian Dream |by Simon Shuster, TIME, and Ilya Marritz, WNYC |September 10, 2020 |ProPublica 

China sees Africa as an instrumental ally that will support its ambition to dominate the global stage and shift the focus of world leadership away from the US. What China has to gain from Africa |Chika Dunga |September 1, 2020 |Quartz 

The future of our businesses, our economy, and our nation relies on us today to be allies for our neighbors. We won’t have a true economic recovery until we tackle the racial wealth gap |matthewheimer |September 1, 2020 |Fortune 

But Ally knows better and dresses tastefully for her age and body type now. Inside A Finishing School for Transwomen |Sharon Adarlo |December 27, 2014 |DAILY BEAST 

We are not "equal" and you are not an ally if this is the childish base of your notions. The Unbearable Whiteness of Protesting |Rawiya Kameir, Judnick Mayard |December 10, 2014 |DAILY BEAST 

In many ways, the behavior of the male “ally” demonstrates how far we have to go in the tech industry. Tech’s Male ‘Feminists’ Aren’t Helping |Cate Huston, Karen Catlin |December 8, 2014 |DAILY BEAST 

But on closer inspection, Wadhwa is still early in his journey to becoming a real ally. Tech’s Male ‘Feminists’ Aren’t Helping |Cate Huston, Karen Catlin |December 8, 2014 |DAILY BEAST 

Whilst Whitacre never defined himself as an “ally,” this remains a cautionary tale of what not to do. Tech’s Male ‘Feminists’ Aren’t Helping |Cate Huston, Karen Catlin |December 8, 2014 |DAILY BEAST 

It was of course obvious that France, the traditional ally of Sweden, dominated Europe. Napoleon's Marshals |R. P. Dunn-Pattison 

At last the lascar rose and floundered through the mud toward the village, but he was careful to leave an ally to watch the boats. Kipling Stories and Poems Every Child Should Know, Book II |Rudyard Kipling 

Thenceforth he was always a reliable ally of the Spaniards against Moro incursions. The Philippine Islands |John Foreman 

In Nicholas Rubinstein he found a powerful friend and ally, who supported his enterprise for twenty years with unfailing energy. The Life &amp; Letters of Peter Ilich Tchaikovsky |Modeste Tchaikovsky 

To be called Thomas was gratifying, but the Mr. was quite overpowering and made Tom her ally at once. The Cromptons |Mary J. Holmes