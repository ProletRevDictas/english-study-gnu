So she logged in again and found an opening that afternoon at the local surgical hospital. What went wrong with America’s $44 million vaccine data system? |Cat Ferguson |January 30, 2021 |MIT Technology Review 

A new trend is to also double up on two effective multi-layer masks, such as wearing a surgical mask underneath a cloth mask or even a certified N95 mask underneath a cloth mask. What you can do right now to protect yourself from the new COVID-19 variants |Claire Maldarelli |January 29, 2021 |Popular-Science 

In Hong Kong, for instance, surgical masks and gloves litter hiking trails and wash up on beaches. Unmasking the pandemic’s pollution problem |Stephen Ornes |January 28, 2021 |Science News For Students 

Without a medical-grade face covering, Marr said, people can get the best, simplest protection by wearing a cloth mask tightly on top of a surgical mask. Time to double or upgrade masks as coronavirus variants emerge, experts say |Fenit Nirappil |January 27, 2021 |Washington Post 

A tightly woven cloth mask might get you to 60 or 70 percent, and a blue surgical mask can get you to 70 or 80 percent. Everyone should be wearing N95 masks now |Joseph G. Allen |January 26, 2021 |Washington Post 

Add in additional demand, as with a surgical procedure, and the body is pushed to its very limits. Justice Ruth Bader Ginsburg’s Risky Heart Surgery |Dr. Anand Veeravagu, MD |November 26, 2014 |DAILY BEAST 

When the same abnormality was detected in a subsequent pregnancy at 18 weeks, she was able to have a surgical abortion. Women Share Their Secret Abortion Stories For 1 in 3 Campaign |Brandy Zadrozny |November 20, 2014 |DAILY BEAST 

For some, 72 hours will mark the difference between having a medical vs. a surgical procedure. Abortion in Missouri Is the Wait of a Lifetime |Justin Glawe |November 12, 2014 |DAILY BEAST 

While some forms of DSD require careful medical attention, most cases do not require violent surgical intervention. Intersexuality and God Through the Ages |Candida Moss |November 9, 2014 |DAILY BEAST 

Two years ago, however, Nancy became ill with a post-surgical infection that caused her immense pain. U.K. Courts Grant Mother Right to End Her 12-Year-Old Disabled Daughter’s Life |Elizabeth Picciuto |November 4, 2014 |DAILY BEAST 

Strictures of the male urethra from chronic gonorrhoeal inflammation often require major surgical operations for relief. Essays In Pastoral Medicine |Austin Malley 

A—surgical operation is, he says, the only—only thing that can possibly save her life, and—he hopes it will. Elsie's Vacation and After Events |Martha Finley 

At his death, Benson became the possessor of his few books, his few surgical instruments and some curious preparations. Papers from Overlook-House |Casper Almore 

He also wrote on diseases of the urinary organs, and on local nervous affections of a surgical character. Encyclopaedia Britannica, 11th Edition, Volume 4, Part 3 |Various 

It was up to me to find the boss as quickly as I could and have the three-cornered surgical operation over with. The Wreckers |Francis Lynde