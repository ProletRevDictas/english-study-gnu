Google is testing this feature in these cities and you’ll soon be able to see how to avoid the crowd and remain punctual at the same time. Google Maps made some key changes. Use them to improve your commute. |Sandra Gutierrez |July 22, 2021 |Popular-Science 

Imani likes being punctual, but after picking up food from her friend’s restaurant, Reveler’s Hour in Adams Morgan, and an unexpected Uber issue, she ended up logging in about 10 minutes late. Date Lab: Can an ‘extreme extrovert’ and introvert hit it off? |Damona Hoffman |April 22, 2021 |Washington Post 

Deen is not only punctual, but also professionally courteous to his former co-stars. My Bizarre Night With James Deen, Libertarian Porn Star |Emily Shire |November 12, 2014 |DAILY BEAST 

On the day of the shoot, the crew and I arrived early—Mrs. Cleaver would have wanted us to be punctual! Farewell to Mrs. Cleaver |Jeff Bliss |October 17, 2010 |DAILY BEAST 

He was punctual to his appointment on the following morning; and so was Mr Bellamy. Blackwood's Edinburgh Magazine, No. CCCXXXIX. January, 1844. Vol. LV. |Various 

A discount allowed by the company for the punctual payment of premiums belongs not to the agent, but to the insured. Putnam's Handy Law Book for the Layman |Albert Sidney Bolles 

"I always make it a point to be punctual," Lamb dawdling in the background, overheard him say. Hooded Detective, Volume III No. 2, January, 1942 |Various 

The arrangement being thus fully made, the commissioner, promising to be punctual, bowed and retired. Rollo in Holland |Jacob Abbott 

He was a very punctual man—in other words, he always paced leisurely along, some time or another. Stories about Animals: with Pictures to Match |Francis C. Woodworth