Orient Beach State Park, a 363-acre spit of land jutting out into Gardiner’s Bay, is one of the few North Fork beaches that doesn’t require a residential parking permit. The 25 Best Fall Trips in the World |jversteegh |August 9, 2021 |Outside Online 

He bought an aquarium, an electric motor, a heating element, a metal spit rod and a few other spare parts and began to tinker. Ron Popeil, inventor, pitchman and TV infomercial star, dies at 86 |Matt Schudel |July 29, 2021 |Washington Post 

Pitchers put spit or other substances on the ball to make their pitches dip and swerve so they would be harder to hit. ‘Sticky stuff,’ spitballs, steroids and other ways MLB players have broken the rules to win |Fred Bowen |June 23, 2021 |Washington Post 

I bring a lot of masks in my backpack because I don’t like when my mask gets wet from spit. 'A Year Full of Emotions.' What Kids Learned From the COVID-19 Pandemic |Allison Singer |June 12, 2021 |Time 

They help filter or block spit or mucus droplets that carry infectious particles. Copper ‘foam’ could be used as filters for COVID-19 masks |Sid Perkins |May 11, 2021 |Science News For Students 

Qatar is just a little spit of land that looks like a polyp on edge of Saudi Arabia. U.S. Ally Qatar Shelters Jihadi Moneymen |Jamie Dettmer |December 10, 2014 |DAILY BEAST 

So he and his partner, Zack Simpson, did what they do so well: started spit-balling some game theory around politics. How to Fight Corruption With Game Theory |Mark McKinnon |November 29, 2014 |DAILY BEAST 

I breathed sloppily through my mouth, hung my head between my legs, and spit every so often. I Shot Bin Laden |Elliot Ackerman |November 16, 2014 |DAILY BEAST 

We still have a long way to go, but it has improved dramatically since the post Vietnam syndrome when Vets were spit on. McCain’s 13 Favorite Soldiers |Sandra McElwaine |November 11, 2014 |DAILY BEAST 

Forgive my candor, though such is my wont, but much like that moose on a spit, Bernie is dead. How the Lame Democrats Blew It |Goldie Taylor |November 5, 2014 |DAILY BEAST 

Many gallants 'took' their tobacco in the lords room over the stage, and went out to (Saint) Paul's to spit there privately. Tobacco; Its History, Varieties, Culture, Manufacture and Commerce |E. R. Billings. 

When used by gentlemen it was common to carry a silver basin to spit in. Tobacco; Its History, Varieties, Culture, Manufacture and Commerce |E. R. Billings. 

They are permitted to throw the leavings of their food and spit where they like, sit how they like and smoke everywhere. Third class in Indian railways |Mahatma Gandhi 

Fergan, as well as his companions, curious to know the purpose of the stake and spit, followed the priest. The Pilgrim's Shell or Fergan the Quarryman |Eugne Sue 

Sirs, this is but a temporary dispensation; this is but a puff of wind, this is but a spit of rain and by with it. The Works of Robert Louis Stevenson, Volume XXI |Robert Louis Stevenson