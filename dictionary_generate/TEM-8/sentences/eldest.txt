“I thought about throwing myself down a flight of stairs or have my eldest daughter pounce on top of me,” she said. Women Share Their Secret Abortion Stories For 1 in 3 Campaign |Brandy Zadrozny |November 20, 2014 |DAILY BEAST 

The film traces the legacy of the Abiola family, particularly Hafsat Abiola, the eldest daughter of Kudirat and MKO. The Nigerian Women Who Fight for Democracy |Nina Strochlic |October 1, 2014 |DAILY BEAST 

As the eldest sister she has played a huge role raising the other girls and her 12-year-old brother. Hanifa's Story: Her Five Sisters Taken by ISIS to Be Sold or Worse |Christine van den Toorn |August 19, 2014 |DAILY BEAST 

His father turned to the person he trusted most: his eldest son. When the Son of Hamas Spied for Israel |Nina Strochlic |August 5, 2014 |DAILY BEAST 

Her eldest son, an 18-year-old, is being held separately as an adult in a detention center. ‘We Cannot Return to Guatemala’: An Immigrant Mother’s Plea |Sarah Moga |July 24, 2014 |DAILY BEAST 

Her eldest daughter married in America, and was well known as a modeller in wax in New York. Women in the fine arts, from the Seventh Century B.C. to the Twentieth Century A.D. |Clara Erskine Clement 

She is very highly educated, and is preparing her eldest son for the university herself. Music-Study in Germany |Amy Fay 

The fourth Robert de Brus of Annandale, eldest son of the preceding lord, was born in 1210. King Robert the Bruce |A. F. Murison 

But no, it didn't matter about the eldest son—never mind about him; put him in the City, anywhere as long as he's out of the way. First Plays |A. A. Milne 

Scott's tomb, that of his wife and their eldest son are in one of the chapels whose vaulted roof still remains in position. British Highways And Byways From A Motor Car |Thomas D. Murphy