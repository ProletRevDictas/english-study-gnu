The television program proposed the atom not merely as a “friend,” to be juxtaposed with the warlike atomic bomb, but also as a solution to a serious problem confronting the world—the limitations of nature. The Disneyfication of Atomic Power - Issue 107: The Edge |Jacob Darwin Hamblin |October 6, 2021 |Nautilus 

Over time, however, Nixon’s rhetoric changed, and efforts became decidedly more warlike, with increased focus on using jail time to curb use. The war on drugs didn’t work. Oregon’s plan might. |Kat Eschner |December 17, 2020 |Popular-Science 

So sure, running primaries against people like this can be called warlike acts. No Country for Old Moderates |Michael Tomasky |October 24, 2013 |DAILY BEAST 

Sending anything more warlike than a progressively increasing amount of American military aid and advisers was not in the cards. When America Said No to War |Marc Wortman |September 10, 2013 |DAILY BEAST 

One education professor said the song taught millions of children that "the only real patriotism is warlike activity." Star-Spangled Confederates: How Southern Sympathizers Decided Our National Anthem |Jefferson Morley |July 4, 2013 |DAILY BEAST 

The president's Oval Office address on the oil spill sounded more warlike than his speeches on Afghanistan. Obama Declares Another War |Peter Beinart |June 16, 2010 |DAILY BEAST 

They are “peaceful” because they are not warlike now—again, another ambiguity perpetrated by the regime. Ahmadinejad Is No Hitler |Nazee Moinian |June 18, 2009 |DAILY BEAST 

In a warlike age this peacefulness of a monarch was the great and supernatural phenomenon. Solomon and Solomonic Literature |Moncure Daniel Conway 

What the armor-bearer was for the warlike races of old, such is the tchbukdi for their degenerate descendants. Tobacco; Its History, Varieties, Culture, Manufacture and Commerce |E. R. Billings. 

There had been a round year of peace negotiations and futile truces, with warlike preparation in the background. King Robert the Bruce |A. F. Murison 

It was quite expected that his progress would be challenged, hence the warlike preparations. The Philippine Islands |John Foreman 

At the same time, Robert must have heard of Edward's warlike preparations by land and sea. King Robert the Bruce |A. F. Murison