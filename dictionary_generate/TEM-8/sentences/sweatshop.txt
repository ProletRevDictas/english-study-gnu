She also worked as a child laborer in a sweatshop, putting zippers together. Exploring the nanoworld |David Triana |February 23, 2022 |MIT Technology Review 

When I spoke to her last year, she had compared Coupang’s warehouses to the infamous sweatshops in 1970s South Korea. This company delivers packages faster than Amazon, but workers pay the price |Max S. Kim |June 9, 2021 |MIT Technology Review 

Following the tragedy, American Apparel CEO Dov Charney spoke out against overseas, sweatshop production. American Apparel Stirs Up Controversy…Again |Erin Cunningham |March 6, 2014 |DAILY BEAST 

The dreary couch gag depicts a hellish sweatshop dedicated to manufacturing Simpsons merchandise. ‘Blame It on Lisa’? The Most Controversial ‘Simpsons’ Episodes (Video) |Shannon Donnelly |February 19, 2012 |DAILY BEAST 

A man works in a sweatshop, and has only a little time for self-improvement, and will I tell him what books he ought to read? The Book of Life: Vol. I Mind and Body; Vol. II Love and Society |Upton Sinclair 

The sweatshop, child-labor, excessive hours for women, were attacked with considerable effect. Consumers and Wage-Earners |J. Elliot Ross 

After a disheartening period of working in the sweatshop he saw an opportunity to start in business for himself. The Book of Courage |John Thomson Faris 

Says his father's dead and mother earns seventeen a week in a sweatshop and sends him to school. By Advice of Counsel |Arthur Train 

As long as women buy the cheap kind made at the sacrifice of human life, this sweatshop system will continue. Clothing and Health |Helen Kinne