In its Q3 update, Klarna wrote that it saw 43 percent growth in gross merchandise volume during the first nine months of the year. As BNPL startups raise, a look at Klarna, Affirm and Afterpay earnings |Alex Wilhelm |February 26, 2021 |TechCrunch 

Besides Spirited Away, Princess Mononoke and Howl’s Moving Castle—both from Studio Ghibli—as well as Makoto Shinkai’s 2016 hit project Your Name have ranked in Japan’s top 10 highest-grossing films. Everything to Know About Demon Slayer: The Manga, TV Series and Record-Breaking Film |Kat Moon |February 24, 2021 |Time 

Giants like Apple, Alphabet, Amazon, Facebook, and Microsoft collectively hold more than $570 billion in gross cash. How behemoth companies quash innovation |Katie McLean |February 24, 2021 |MIT Technology Review 

House Bill 6187 would impose a flat 10% tax on gross revenues from digital ad sales inside the state by companies with annual global revenues exceeding $10 billion. Media Briefing: Publishers’ and platforms’ businesses settle into the new normal |Tim Peterson |February 18, 2021 |Digiday 

Much of that was due to Detective Chinatown 3, a heist comedy made in China, which grossed $424 million on its own. China no longer needs Hollywood movies |Adam Epstein |February 16, 2021 |Quartz 

World GDP (including North Pole toyshop gross output) is $84.97 trillion. Santa Fails One More Time |P. J. O’Rourke |December 27, 2014 |DAILY BEAST 

In its opening weekend the movie Heaven Is For Real (budget: $12 million) doubled its gross. The Biggest Bombs of 2014: ‘Sex Tape,’ Mariah Carey’s Vocals, ‘How I Met Your Mother’ and More |Kevin Fallon |December 19, 2014 |DAILY BEAST 

The sex workers I spoke with rightly call it “vile,” “gross,” “terrifying,” and “exploitative.” To Catch a Sex Worker: A&E’s Awful, Exploitative Ambush Show |Samantha Allen |December 19, 2014 |DAILY BEAST 

The film was made with a reported $90 million but imploded with a $39 million domestic gross. The Biggest Bombs of 2014: ‘Sex Tape,’ Mariah Carey’s Vocals, ‘How I Met Your Mother’ and More |Kevin Fallon |December 19, 2014 |DAILY BEAST 

Insult to injury, its $43 million gross was less than one-fifth of what Ted took in. The Biggest Bombs of 2014: ‘Sex Tape,’ Mariah Carey’s Vocals, ‘How I Met Your Mother’ and More |Kevin Fallon |December 19, 2014 |DAILY BEAST 

Life is represented as struggling to free herself from the gross earthly forms that cling to her. Women in the fine arts, from the Seventh Century B.C. to the Twentieth Century A.D. |Clara Erskine Clement 

Does a friend come and add to the gross character of such a man the unknown trait of disgusting gluttony? The Portsmouth Road and Its Tributaries |Charles G. Harper 

All these gross absurdities show, that the real spirit has nothing whatever to do with such absurd doctrines or productions. Second Edition of A Discovery Concerning Ghosts |George Cruikshank 

When the hern or bitron flies low, the air is gross, and thickening into showers. Notes and Queries, Number 194, July 16, 1853 |Various 

To abandon any part of the inheritance of primitive times would be gross heresy, a fatal dereliction of Christian duty. The English Church in the Eighteenth Century |Charles J. Abbey and John H. Overton