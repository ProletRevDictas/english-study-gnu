Uscinski, for his part, thinks the risk of a social desirability bias with QAnon is minimal, given the unabashed zeal with which proponents seem to demonstrate their support. Why It’s So Hard To Gauge Support For QAnon |Kaleigh Rogers |June 11, 2021 |FiveThirtyEight 

He brings the same intuitive scrutiny to Tolontan and his staff, as they chase down new angles of their corruption story with a zeal that ends up endangering them. Oscar-Nominated Documentary Collective Is a Gripping Story of Investigative Journalism |Stephanie Zacharek |March 24, 2021 |Time 

At least that’s what you’d conclude when you think about New Year’s resolutions and the zeal with which so many people make them. Are You Ready for a Fresh Start? (Ep. 455) |Stephen J. Dubner |March 18, 2021 |Freakonomics 

If Carlson runs in 2024, he would be an instant frontrunner given his anti-establishment zeal and platform on the most popular cable news show by far. Fork in the Road: Where Will the GOP Turn? |Nick Fouriezos |January 10, 2021 |Ozy 

That is, the communal zeal these days is not love for party but hate for elites. Fork in the Road: Where Will the GOP Turn? |Nick Fouriezos |January 10, 2021 |Ozy 

It was his business acumen, his own unflagging zeal for the creative business solution, that had freed Sam to do this. How Martin Luther King Jr. Influenced Sam Cooke’s ‘A Change Is Gonna Come’ |Peter Guralnick |December 28, 2014 |DAILY BEAST 

Thereafter, the 1960s swelled with political zeal and social unrest. A History of Paris in 150 Photographs |Sarah Moroz |December 14, 2014 |DAILY BEAST 

He implored me to do so with the zeal of someone who had just found God, emphatically praising the article. War Is Hell and Such Good Fun |Don Gomez |November 11, 2014 |DAILY BEAST 

Even as the ranks of culture warriors on the right diminish, their zeal seems to intensify. The Right Wing Screams for the Wambulance Over Gay Marriage Ruling |Walter Olson |October 13, 2014 |DAILY BEAST 

One of those preachers admitted to The Daily Beast that he was taken aback by her zeal. The UK’s New Iron Lady in Kitten Heels |Nico Hines, Tom Sykes |October 4, 2014 |DAILY BEAST 

His zeal led him among foreigners as a missionary; after visiting Bohemia, he went among the Poles, by whom he was killed. The Every Day Book of History and Chronology |Joel Munsell 

They have fought countless bloody wars and have committed countless horrible atrocities in their zeal for Him. God and my Neighbour |Robert Blatchford 

I have dared to relate this to your Majesty because of my zeal as a loyal vassal, and as one who looks at things dispassionately. The Philippine Islands, 1493-1898, Volume XX, 1621-1624 |Various 

Zeal, who was sitting stiffly forward, his hands gripping the arms of his chair, laughed dryly. Ancestors |Gertrude Atherton 

He braced himself unconsciously, and after Zeal's next words did not relax his body, although his lips turned white and stiff. Ancestors |Gertrude Atherton