As your product evolves, you can enable different modules from the plugin marketplace. Datadog to acquire application security management platform Sqreen |Romain Dillet |February 12, 2021 |TechCrunch 

Vaccine tourism also has the potential to exacerbate the socioeconomic and racial inequalities that have persisted during the pandemic, enabling the rich and privileged to gain access to life-saving vaccines ahead of everyone else. Don’t hate the vaccine tourists, hate the vaccine game |Jen Kirby |February 12, 2021 |Vox 

They want to enable us to perfectly save and relive our favorite memories. New technology can get inside your head. Are you ready? |Laura Sanders |February 11, 2021 |Science News For Students 

It enabled them to decipher contracts, build businesses, engage in politics and influence social issues that affected their lives. My great-grandmother Ida B. Wells left a legacy of activism in education. We need that now. |Michelle Duster |February 11, 2021 |Washington Post 

Access to the internet, having an internet-enabled device and understanding how to use both have been necessary to sign up for the vaccine. One big hiccup in US efforts to distribute COVID-19 vaccines? Poor internet access. |By Tamra Burns Loeb, Et Al./The Conversation |February 10, 2021 |Popular-Science 

“There was great reception,” Drew jokes, adding that perhaps he and his girlfriend, a fellow workaholic, enable each other. How the Property Brothers Became Your Mom’s Favorite TV Stars |Kevin Fallon |November 25, 2014 |DAILY BEAST 

Of course, beyond tasting good, these chips make great platforms—the deep ridges enable them to hold dips effectively. The Latest in High-Tech Chips | |September 18, 2014 |DAILY BEAST 

Nonviolent subjects were easier to rule and more likely to provide the revenue and manpower that would enable further conquest. War! What Is It Good For? A Lot |Nick Romeo |August 13, 2014 |DAILY BEAST 

Even if their oxygen masks had deployed, there is a microphone in the masks to enable them to send a distress call. Passenger Flights Must Stop Carrying Lithium-Ion Batteries as Cargo |Clive Irving |May 5, 2014 |DAILY BEAST 

And do we not yearn to do as they did: enable America to “find its ‘greatness’ again”? Embodying Franklin Roosevelt’s Four Freedoms Remains a Vital Challenge |Harvey J. Kaye |April 6, 2014 |DAILY BEAST 

As a rule, however, even in the case of extreme varieties, a careful examination of the specimen will enable it to be identified. How to Know the Ferns |S. Leonard Bastin 

He had, however, recovered sufficiently to enable him to act with promptitude and discretion. The Giant of the North |R.M. Ballantyne 

Relieved when she entered, she was again struck with fear when Phœbe Chiffinch had come near enough to enable her to see her face. Checkmate |Joseph Sheridan Le Fanu 

The great abilities which enable a man to win and hold such a position as his fired my fancy. Fifty Years of Railway Life in England, Scotland and Ireland |Joseph Tatlow 

At length he thought of “Cattle” as a figure word to enable him to remember the number. Assimilative Memory |Marcus Dwight Larrowe (AKA Prof. A. Loisette)