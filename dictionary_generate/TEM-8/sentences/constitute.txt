Once-tight siblings I know now feel distant and guarded, their worldviews suddenly so opposed that they can’t even agree on what constitutes a fact. Politics and conspiracy theories are fracturing relationships. Here’s how to grieve those broken bonds. |Jeff Schrum |February 11, 2021 |Washington Post 

Besides, it’s not really on you to determine what constitutes damage in this case. How to Apologize to Your Friend |Blair Braverman |February 8, 2021 |Outside Online 

Rodriguez-Kennedy and Schumacher said they thought soliciting those desired appointments could constitute a Brown Act violation. Politics Report: About That SANDAG Seat |Scott Lewis and Andrew Keatts |January 30, 2021 |Voice of San Diego 

There may be Republicans who’d like to vote to convict if the vote were conducted in private, but it is almost certainly not a group that constitutes 31 of the 50 Republicans in the Senate. Why a secret impeachment vote isn’t going to happen |Philip Bump |January 26, 2021 |Washington Post 

A group of parents sued the West Ada teachers union, claiming that the walk-out constituted an illegal strike and that it caused undue emotional and financial stress on families. The Challenges Posed By COVID-19 Pushed Many Workers to Strike. Will the Labor Movement See Sustained Interest? |Abigail Abrams |January 17, 2021 |Time 

No longer does it constitute a reliable, middle class-based alternative to the corporatist mindset of the Republicans. Time to Bring Back the Truman Democrats |Joel Kotkin |December 21, 2014 |DAILY BEAST 

Kim Jung-un clearly recognizes that Hollywood and American popular culture in general constitute a dire threat. Inside the ‘Surprisingly Great’ North Korean Hacker Hotel |Michael Daly |December 20, 2014 |DAILY BEAST 

The reviews in themselves constitute a demonstration of why the regime restricts the Internet. Inside the ‘Surprisingly Great’ North Korean Hacker Hotel |Michael Daly |December 20, 2014 |DAILY BEAST 

Under that definition, forced kissing can certainly constitute as a form of sexual assault. Fact-Checking the Sunday Shows: Dec. 7 |PunditFact.com |December 7, 2014 |DAILY BEAST 

This will constitute a major victory for the forces of light, one very much worth marking and thinking back over. Who Are the Judicial Activists Now? |Michael Tomasky |October 7, 2014 |DAILY BEAST 

We can thus disregard the first 16 and consider only the last two figures which constitute the fraction of a century. Assimilative Memory |Marcus Dwight Larrowe (AKA Prof. A. Loisette) 

Cherry-tree stems, under the name of agriots, constitute a specialty of Austrian manufacture. Tobacco; Its History, Varieties, Culture, Manufacture and Commerce |E. R. Billings. 

In myelogenous leukemia myelocytes usually constitute more than 20 per cent. A Manual of Clinical Diagnosis |James Campbell Todd 

The blessings of time and eternity constitute the part of the promise offered to believers, through Christ. The Ordinance of Covenanting |John Cunningham 

In the preceding chapter we have endeavoured to solve the question what are the qualities that constitute good tone. Violins and Violin Makers |Joseph Pearce