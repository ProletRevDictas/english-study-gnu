The 46th president spoke of school reopenings in his inaugural address and later said he would like to see most K-8 schools reopen during his first 100 days in office. Virginia governor calls on all schools in state to reopen by March 15 |Hannah Natanson |February 5, 2021 |Washington Post 

Out of the 60 women that applied for exclusive mentorship programs, three inaugural winners were chosen to receive a Cantu-sponsored campaign valued at $160,000. Cantu Beauty Announces Winners of Elevate Workshop Series |Nandi Howard |February 5, 2021 |Essence.com 

The company is targeting this year for New Glenn’s inaugural launch. Blue Origin could definitely use more Jeff Bezos in the next decade |Neel Patel |February 4, 2021 |MIT Technology Review 

The funding actually marked BTV’s first investment in a cohort member of its inaugural accelerator program. TrustLayer raises $6M seed to become the ‘Carta for insurance’ |Mary Ann Azevedo |February 2, 2021 |TechCrunch 

Keren Taylor sobbed when she saw Gorman leave the inaugural stage. Amanda Gorman learned the power of poetry early on |Los Angeles Times |February 1, 2021 |Washington Post 

On June 18, 1971, the caucus threw its inaugural dinner at the Dunbar Hotel. When Bill Cosby N-Bombed the Congressional Black Caucus |Asawin Suebsaeng |December 2, 2014 |DAILY BEAST 

“We are all Republicans, we are all Federalists” said Thomas Jefferson in his first inaugural. Didn't Obama Hear Oregon’s Warning Shot on Immigration? |Doug McIntyre |November 14, 2014 |DAILY BEAST 

Thomas Jefferson also warned in his first inaugural that not “every difference of opinion is a difference of principle.” Didn't Obama Hear Oregon’s Warning Shot on Immigration? |Doug McIntyre |November 14, 2014 |DAILY BEAST 

Which brings us to FDR's first inaugural speech assertion that "we have nothing to fear but fear itself." Ebola, ISIS, the Border: So Much to Fear, So Little Time! |Gene Robinson |November 2, 2014 |DAILY BEAST 

Richard Blanco made waves in 2013 when he was the first immigrant, Latino, and openly gay man to be the inaugural poet. Richard Blanco’s Gay Latino Poet Survival Kit |William O’Connor |October 8, 2014 |DAILY BEAST 

(p. 362) Two years passed, and Abraham Lincoln gave utterance to other sentiments in his second inaugural address to the people. The Boys of '61 |Charles Carleton Coffin. 

One sentence in his inaugural address provoked derision: "We are at peace with all the world and the rest of mankind." A History of the Nineteenth Century, Year by Year |Edwin Emerson 

No possible argument, however, can reconcile these inaugural principles with the Kentucky resolutions. Nullification, Secession Webster's Argument and the Kentucky and Virginia Resolutions |Caleb William Loring 

The "penitentiarying" of Newt himself had been only the inaugural of more sweeping and hateful innovations. The Code of the Mountains |Charles Neville Buck 

Life is movement, cried Alois von Brinz, in his magnificent inaugural address. Criminal Psychology |Hans Gross