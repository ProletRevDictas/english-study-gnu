My preferred brand, David, comes coated in dill pickle, ranch, and barbecue powder, but in my opinion, sunflower seeds shouldn’t be any particular flavor. Sunflower Seeds Are the Best Snack for the Anxious Mind |Emma Alpern |September 17, 2020 |Eater 

Usually, most of the big summer blockbusters have already come out, people are squeezing in their last beach trips and barbecues, and movie theaters are looking ahead to the hefty fall season. This is the most important movie weekend of the year |Alissa Wilkinson |September 4, 2020 |Vox 

I wouldn’t want to cook a complex meal on it, but it’s perfect for burgers, hot dogs, and other basic barbecue foods. 5 Portable Grills for All Your Outdoor Cooking Needs |Amy Marturana Winderl |September 2, 2020 |Outside Online 

The rally included a community cleanup, food drive, music, barbecue, a bouncy house, healing circle and voter registration booth, reports Kenosha News. Our nation turns its lonely eyes to Kenosha |Ellen McGirt |September 1, 2020 |Fortune 

That means no backyard barbecues or parties on Fourth of July. Morning Report: After Protests, SDPD Turned to Streetlight Cameras |Voice of San Diego |June 30, 2020 |Voice of San Diego 

So, the Southside Smokehouse is more than a barbecue pit, a burger shack, or a Cajun kitchen. The Ultimate Southern Cheeseburger Created in South Carolina |Jane & Michael Stern |August 10, 2014 |DAILY BEAST 

We remember that time a flight attendant told us at a barbecue that if the bell tones four times, things are serious. The Malaysian Air Tragedy Reawakens a Primal Fear |Kelly Williams Brown |July 19, 2014 |DAILY BEAST 

He went on to open a dry cleaning business, barbecue restaurant, and liquor store. Donald Sterling and Five Other Members of the Lifetime Ban Club |Nina Strochlic |April 30, 2014 |DAILY BEAST 

The word primitive does not do justice to the elemental nature of dining in one of the great barbecue parlors of this region. The Texas Church of Beef |Jane & Michael Stern |April 27, 2014 |DAILY BEAST 

Then again, is there really ever a bad time for slow-cooked barbecue, hot jazz and cool blues music? The U.S. Road Trips You Should Really Take |Lonely Planet |April 26, 2014 |DAILY BEAST 

James Murray, mayor of Alexandria, La., was killed while attempting to suppress a disturbance at a barbecue. The Every Day Book of History and Chronology |Joel Munsell 

Now he plans to hold a big barbecue en send out invitations. David Lannarck, Midget |George S. Harney 

Later the Cherokee Indians changed the council fire into a barbecue, where they roasted whole beefs in pits of glowing coals. The Book of Camp-Lore and Woodcraft |Dan Beard 

A fire of hard-wood had been burning in it for hours, the preliminary to a gigantic barbecue of fat oxen. The Doomsman |Van Tassel Sutphen 

They were having a barbecue; that is, they were roasting oxen whole on great spits; and a horse race was to be run. Hero Stories from American History |Albert F. Blaisdell