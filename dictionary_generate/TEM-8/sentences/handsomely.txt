Large supermarket chains have profited handsomely during the pandemic. As Cities Mandate Hazard Pay for Grocery Workers, Groceries Sue |Brendan Seibel |February 26, 2021 |Eater 

In exchange, their work to end Covid-19 has rewarded pharmaceutical companies rather handsomely. Big Pharma wants to help poor countries fight Covid-19—but they’d better pay |Annalisa Merelli |February 12, 2021 |Quartz 

Apple improved dramatically on each concept and profited handsomely. Amazon the innovator or Amazon the violator? |Adam Lashinsky |October 13, 2020 |Fortune 

So do other developers, who can pay themselves handsomely while they pursue ideas that may never make it. It’s His Land. Now a Canadian Company Gets to Take It. |by Lee van der Voo for ProPublica |October 1, 2020 |ProPublica 

In the decade since Leonard Green & Partners, a private equity firm based in Los Angeles, bought control of a hospital company named Prospect Medical Holdings for $205 million, the owners have done handsomely. Investors Extracted $400 Million From a Hospital Chain That Sometimes Couldn’t Pay for Medical Supplies or Gas for Ambulances |by Peter Elkind with Doris Burke |September 30, 2020 |ProPublica 

He spent most of his vacation as the guest of our printers in Philadelphia, and they entertained him handsomely. The Stacks: H.L. Mencken on the 1904 Baltimore Fire |H.L. Mencken |October 4, 2014 |DAILY BEAST 

Given how infrequently new copies of the map appeared on the market, collectors would bid handsomely for the artifact. The Million-Dollar Map Thief |Nick Romeo |July 30, 2014 |DAILY BEAST 

As a boy, he once said, he acquired the throwing skill that served him handsomely later by killing quail with rocks. Portrait of the Consummate Con Man |John Lardner |May 17, 2014 |DAILY BEAST 

At its current trading value, it could be a gamble that pays off handsomely. Should Baby Boomers Invest in Bitcoin? |Sarah Kunst |January 9, 2014 |DAILY BEAST 

Jeremy Piven was the holdout, but he has been convinced (and will be paid handsomely for it). Eminem and Rihanna Release 'Monster,' New 'X-Men' Trailer Hits the Web | |October 29, 2013 |DAILY BEAST 

The dinner was handsomely attended, and the ball more handsomely still. Blackwood's Edinburgh Magazine, No. CCCXXXIX. January, 1844. Vol. LV. |Various 

The boy took off his hat, and very handsomely addressed the cow, with "Your servant, madam." The Book of Anecdotes and Budget of Fun; |Various 

“There she goes, handsomely,” cried the men, as the engine again resumed work at reasonable speed. The Floating Light of the Goodwin Sands |R.M. Ballantyne 

The family were at their country-house at Botafogo; and a most excellent house it is, very handsomely built and richly furnished. Journal of a Voyage to Brazil |Maria Graham 

A handsomely ornamented boat awaited us at the bank of the river, and on the other side a palanquin. A Woman's Journey Round the World |Ida Pfeiffer