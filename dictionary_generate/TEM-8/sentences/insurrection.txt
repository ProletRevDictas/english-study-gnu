Before the insurrection, people’s sense of security in the peaceful transfer of power and respect for election outcomes was so strong that the chipping away at the legitimacy of the results was maybe not viewed as seriously as it should have been. How Has The Nation Changed Since The Insurrection At The Capitol? |Micah Cohen (micah.cohen@fivethirtyeight.com) |February 11, 2021 |FiveThirtyEight 

Alexandra Ocasio-Cortez’s Instagram Live post on the insurrection shows why we can’t just move on. “I thought I was going to die”: AOC’s harrowing account of the Capitol Hill attack |Zack Beauchamp |February 2, 2021 |Vox 

The short-lived insurrection was planned and executed in public. The danger of right-wing mobs is real. Fencing at the U.S. Capitol won’t help. |Philip Kennicott |February 1, 2021 |Washington Post 

It’s a wonder any of us were able to tear ourselves away from the news for long enough to watch anything else this month, which took America from insurrection to impeachment to the inauguration of our 46th President in the space of just two weeks. The 5 Best New Shows Our TV Critic Watched in January 2021 |Judy Berman |January 29, 2021 |Time 

More than 100 of those involved in the act of insurrection have also been arrested, itself a form of moderation. The Capitol Attack, Impeachment and GameStop Make it Clear: 2021 Is Shaping Up to Be the Year of the Moderator |Alex Fitzpatrick |January 29, 2021 |Time 

In the early 1960s Cambridge University was a hotbed of cultural and social insurrection. Why Can’t Movies Capture Genius? |Clive Irving |December 14, 2014 |DAILY BEAST 

There are fears of a major new Islamist insurrection, possibly inspired by the so-called Islamic State in Iraq and Syria. Where Chechens Go to Escape Their Surreal Past—and Risky Present |Anna Nemtsova |December 9, 2014 |DAILY BEAST 

He was with James Meredith during the violent insurrection that followed the integration of the University of Mississippi in 1962. Honoring The Late John Doar, A Nearly Forgotten Hero Of The Civil Rights Era |Gary May |November 15, 2014 |DAILY BEAST 

Of course, without American logistical aid, the insurrection would have ended in tragedy. Who Liberated Paris in August 1944? | |August 24, 2014 |DAILY BEAST 

In a rare moment of insurrection, Rivera threatened to stay behind, even without permission. Speed Read: Highlights From Mariano Rivera’s Memoir, ‘The Closer’ |Ben Teitelbaum |May 15, 2014 |DAILY BEAST 

A serious insurrection occurred at Dresden, in Saxony, but was in a few days put down. The Every Day Book of History and Chronology |Joel Munsell 

During his mild régime the insurrection increased rapidly, and in one encounter he himself was very near falling a prisoner. The Philippine Islands |John Foreman 

The flame of organized insurrection was almost extinguished, but there still remained some dangerous embers. The Philippine Islands |John Foreman 

Meantime the discontents in the northern provinces had broken out into open insurrection, in the captaincy of Pernambuco. Journal of a Voyage to Brazil |Maria Graham 

Abramko had allowed himself to be compromised in the Polish insurrection and Magus was interested in saving him. Repertory Of The Comedie Humaine, Complete, A -- Z |Anatole Cerfberr and Jules Franois Christophe