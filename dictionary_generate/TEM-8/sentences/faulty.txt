While they debated, public health labs with the faulty kits couldn’t process samples, and the FDA still hadn’t authorized any tests made by commercial labs. Inside the Fall of the CDC |by James Bandler, Patricia Callahan, Sebastian Rotella and Kirsten Berg |October 15, 2020 |ProPublica 

When companies build technology around faulty business practices, the platform fails and vise-versa. Technology & real estate: How COVID propelled industry forward |Nick Ron |September 26, 2020 |Washington Blade 

A 404 can be generated when a user types in a faulty address, and this may result in an error being generated on their browser that may look bad, but you can definitely address the situation. Guide to using interactive 404s to boost your SEO |Amanda Jerelyn |September 24, 2020 |Search Engine Watch 

In the latest Environment Report, MacKenzie Elmer dives into the state’s water market and one plan in particular by a utility that serves Imperial Beach and Coronado, among others, that a state watchdog recently said hinged on faulty forecasting. Morning Report: San Diego Is Ignoring an Untapped Water Source |Voice of San Diego |September 15, 2020 |Voice of San Diego 

Those include fears over the United States Post Office’s ability to keep pace with such a huge influx of mail and the possibility of mail-in ballots getting thrown out for having an allegedly faulty signature or arriving late. Mandatory mail-in voting hurts neither Democratic nor Republican candidates |Sujata Gupta |August 26, 2020 |Science News 

Identifying and excising faulty accounts takes up more and more of their time as the country splinters again. ISIS Fighters Are Killing Faster than Statisticians Can Count |Peter Schwartzstein |December 5, 2014 |DAILY BEAST 

A senior Labour Party MP scoffed at what he suggested was faulty logic. Britain’s Let-Em-All-Die Policy |Nico Hines, Barbie Latza Nadeau |November 1, 2014 |DAILY BEAST 

Hongkongers aren't asking to secede from China, but Beijing's faulty calculus is only alienating the city. Is Hong Kong Tiananmen 2.0? |Brendon Hong |September 29, 2014 |DAILY BEAST 

But the fact remains that the accident would never have occurred in the first place were it not for the faulty switch. The Cops Who Found Out the Truth About GM's Deadly Cars—in 2006 |Michael Daly |July 17, 2014 |DAILY BEAST 

The view is faulty, both in it engineering claims and its economic conclusions. The FCC Must Ignore the Silly ‘Net Neutrality’ Advocates |Nick Gillespie |May 19, 2014 |DAILY BEAST 

The myth of "Boreas and Orithyia," though faulty perhaps in technique, is good in conception and arrangement. Women in the fine arts, from the Seventh Century B.C. to the Twentieth Century A.D. |Clara Erskine Clement 

If the ice or snow has accumulated by reason of a defective roof, then the landlord is liable because of its faulty construction. Putnam's Handy Law Book for the Layman |Albert Sidney Bolles 

If it cannot be accounted for by the causes at work in the story, the construction is faulty. English: Composition and Literature |W. F. (William Franklin) Webster 

At this point, we may see how faulty, and yet how constantly improving, has been the administration of the criminal law. Thoughts on Educational Topics and Institutions |George S. Boutwell 

The arches near the tower have been partly crushed owing to the shifting of the tower piers caused by faulty foundations. Bell's Cathedrals: The Cathedral Church of Carlisle |C. King Eley