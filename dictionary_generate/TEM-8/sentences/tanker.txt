Montgomery County Fire Department spokesman Pete Piringer said a tanker truck and several vehicles were involved in the crash and that three people were transported to the hospital with injuries that were not life-threatening. I-270 southbound reopens after tanker truck crash in Md. |Justin Wm. Moyer |February 4, 2021 |Washington Post 

With one typical method, a human operator lowers a long boom from the tanker to the top of the receiving plane. The 100 greatest innovations of 2020 |Popular Science Staff |December 2, 2020 |Popular-Science 

That essentially left the tanker to rot, fueling fears the oil on board might eventually spill into the waters below. A stranded oil tanker at risk of spilling in the Caribbean looks to be safe — for now |Jariel Arvin |October 22, 2020 |Vox 

SilverLiner, a tanker truck manufacturing company, also has hired 13 employees, Mura said. The Elk, the Tourists and the Missing Coal Country Jobs |by R.G. Dunlop, Kentucky Center for Investigative Reporting |October 22, 2020 |ProPublica 

For the most part, tankers like the one Hopf flies drop a line of retardant not on the fire itself, but in a place that will help steer or contain the blaze. How aerial firefighters battle blazes from the skies |Rob Verger |August 27, 2020 |Popular-Science 

Around 60 mangled and scorched tanker cars still remain there. Inside the Brutal Clean-up Efforts in Lac-Megantic |Christine Pelisek |July 16, 2013 |DAILY BEAST 

They pump their haul of diluted bitumen into tanker cars in the terminal's loading yard, thick with the smell of petroleum. Pipelines, Not Rail, Ctd. |Justin Green |April 10, 2013 |DAILY BEAST 

A giant barge has made its way to the front of the wreckage near an enormous oil tanker that arrived over the weekend. Costa Concordia Mess Widens With Salvage Team, Criminal Probe |Barbie Latza Nadeau |January 24, 2012 |DAILY BEAST 

The year before a gasoline tanker truck exploded near a building in which he was holding a meeting. Why Ahmed Wali Karzai Was a Target |Sami Yousafzai, Ron Moreau |July 12, 2011 |DAILY BEAST 

There was a massive explosion and the men in the fuel tanker were instantly incinerated. America's Secret Nuclear Test Revealed in Area 51 |Annie Jacobsen |May 13, 2011 |DAILY BEAST 

Twenty minutes later, Clay and the carrier driver had the new part installed and the tanker was on his way once again. Code Three |Rick Raphael 

Usually it is only the drunks who come over the side of an oil-tanker singing, but this was no drunk. Wide Courses |James Brendan Connolly 

One of the two Oman ships had been converted into a fuel-tanker and its yawning holds were being filled first. Masters of Space |Edward Elmer Smith 

All too soon, however, the heavily laden tanker appeared in the sky over Ardane. Masters of Space |Edward Elmer Smith 

There is a ship out yonder, but its a tanker or a freighter. Dorothy Dixon and the Mystery Plane |Dorothy Wayne