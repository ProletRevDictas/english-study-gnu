Hike the dunes along five designated trails, or continue your road trip along the Dunes Drive, an eight-mile roadway that takes you into the heart of the dunes. 4 Awesome Winter Road Trips to National Parks |Megan Michelson |February 11, 2021 |Outside Online 

It’s the slimmest of margins, but an experienced driver can take you into the air for a few wild seconds where you can see the top of the next dune and look down at the trough below. The Strange Allure of a Flight to Nowhere (and Other Places We're Desperate to Go) |Susanna Schrobsdorff |February 7, 2021 |Time 

At the time, the 35-year-old West Virginian was freelancing for “Sunday Morning,” and the CBS show aired a handful of his “On the Trail” segments from the field — and petrified forest, sand dunes, active volcano, etc. Tips for visiting U.S. national parks from someone who visited them all |Andrea Sachs |December 17, 2020 |Washington Post 

In Gearhart, James trudged out to Strawberry Knoll, walked across the dunes and onto the beach. To Find Hope in American Cooking, James Beard Looked to the West Coast |John Birdsall |October 2, 2020 |Eater 

Located in the middle of Great Sand Dunes National Park, Star Dune is the tallest dune in North America. I Tried to Climb the Largest Sand Dune in North America |Emily Pennington |September 22, 2020 |Outside Online 

We lugged the beach stuff onto the beach, avoiding anything that resembled a dune. Why I Hate The Beach |P. J. O’Rourke |July 27, 2014 |DAILY BEAST 

He even says in the documentary that he never read Dune; someone told him it was fantastic. ‘Jodorowsky’s Dune’ and the Allure of the Unmade Masterpiece |Caryn James |March 19, 2014 |DAILY BEAST 

The idea was more fashionable in 1975, when Alejandro Jodorowsky was planning his version of the sci-fi novel Dune. ‘Jodorowsky’s Dune’ and the Allure of the Unmade Masterpiece |Caryn James |March 19, 2014 |DAILY BEAST 

The whole dune area in Wainscott is washed away and the water came up to Main Street. Hamptons Residents Reel as Superstorm Sandy Recedes |Emily J. Weitz |October 31, 2012 |DAILY BEAST 

They climbed another dune, and came upon the great gray sea at low tide. Kipling Stories and Poems Every Child Should Know, Book II |Rudyard Kipling 

And another five minutes, perhaps another ten, had slipped by before Tony's head reappeared above a neighbouring dune. The Wave |Algernon Blackwood 

The dune rose in a Wave of glittering yellow sand, drowning them from head to foot. The Wave |Algernon Blackwood 

We were unco gleg to win hame when a' this was dune, an' after steekin' the door, to sit an' birsle oor taes at the bit blaze. Penelope's Experiences in Scotland |Kate Douglas Wiggin 

As this joyless impossibility flitted across my mind, I rounded a bleak sand-dune. In Search of the Unknown |Robert W. Chambers