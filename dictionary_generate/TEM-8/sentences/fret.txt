There is however a not-paranoid or market-driven reason to fret, albeit a VERY small one. Ding Dong, You Have Lice |Kent Sepkowitz |October 31, 2013 |DAILY BEAST 

Even assuming Wyoming is safe, however, Republicans are right to fret. Lean In, Liz Cheney, but Please Don’t Win That Senate Seat |Michelle Cottle |July 18, 2013 |DAILY BEAST 

In recent weeks, it has been fashionable (and even rational) to fret about the U.S. industrial economy. Data Show U.S. Industry Shrugs off Sandy Effects |Daniel Gross |December 14, 2012 |DAILY BEAST 

Not to fret—The Daily Beast breaks down the talking points that will keep things civil while eating your turkey. Thanksgiving Talking Points Guide: Guide to Hot-Button Topics, and Ones to Avoid |Nina Strochlic |November 21, 2012 |DAILY BEAST 

The “all clear” for many of the 10,000 possibly exposed campers will not be given till early October—a long time to fret. Yosemite Hanta Virus: Nature Strikes Back |Kent Sepkowitz |September 3, 2012 |DAILY BEAST 

Rose, l. 4705, And through the fret full, read A trouthe fret full.' Chaucer's Works, Volume 1 (of 7) -- Romaunt of the Rose; Minor Poems |Geoffrey Chaucer 

Hence were, in the next line, must mean to wear away, to fret; cf. note to 4712. Chaucer's Works, Volume 1 (of 7) -- Romaunt of the Rose; Minor Poems |Geoffrey Chaucer 

Strange to say, to the astonishment of all but Lucy, young Mrs. Haggard continued to "fret." The Pit Town Coronet, Volume II (of 3) |Charles James Wills 

Davie would be well away, for he would fret about his grandmother, and that would do neither of them any good. David Fleming's Forgiveness |Margaret Murray Robertson 

And the woman laughed, and said, Truly, thou doest ill to fret thyself for such a matter. The Cradle of Mankind |W.A. Wigram