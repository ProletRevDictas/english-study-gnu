While the more than $10 per ounce price tag makes this steak beyond decadent, I’ve tested Holy Grail’s premium Wagyu and don’t doubt that this will be the best steak my father has ever eaten. What Our Gear Guy Is Gifting for Christmas |Joe Jackson |December 20, 2020 |Outside Online 

Buttery and chewy, rich and decadent is how I describe this classic Philippine dessert. These chewy, date- and walnut-packed bars are called ‘food for the gods’ with good reason |Isa Fabro |December 2, 2020 |Washington Post 

This Hanukkah, we will skip the decadent accoutrements, as they feel wrong against the backdrop of such a difficult year. This crisp, classic potato latke recipe delivers a satisfying, celebratory crunch |Olga Massov |November 30, 2020 |Washington Post 

Hip-hop and R&B stars like Lil’ Kim, Missy Elliott and SWV singer Coko often wore decadent nails in their music videos. The Unlikely History of Acrylic Nails |Joshua Eferighe |September 30, 2020 |Ozy 

With more control over your oil use, sprayers are not just a decadent tool—they can help you stick to a healthier diet, and make your food taste better. Best oil sprayers and misters for home chefs |PopSci Commerce Team |September 11, 2020 |Popular-Science 

The grandson of legendary fashion editor Diana Vreeland, Nicholas Vreeland was poised for a decadent life in high-society. From Fashion Player to Photographer Monk |Nina Strochlic |December 3, 2014 |DAILY BEAST 

Since it could now survive travel over longer distances, lobster became a decadent treat for the American upper class. My Big, Buttery Lobster Roll Rumble: We Came, We Clawed, We Conquered |Scott Bixby |June 7, 2014 |DAILY BEAST 

I remember going to a rehearsal dinner that had lobster tail on the buffet and thinking that was decadent. Which of Kim Kardashian’s Weddings Was More Ridiculous? |Kevin Fallon |May 27, 2014 |DAILY BEAST 

Decadent, venal, ineffective, stratified, anxiety-ridden, stumbling from one declared crisis to the next—who wants that? The Real Clash of Civilizations |James Poulos |March 1, 2014 |DAILY BEAST 

Amongst the characters performances are decadent costumes, over-the-top wigs, and too much leather, fur, and slinky cuts to count. The ‘American Hustle’ Style Guide |Erin Cunningham |February 14, 2014 |DAILY BEAST 

He was that rare thing in a new land, a decadent, a connoisseur in vice, a lover of opiates and of liquor. You Never Know Your Luck, Complete |Gilbert Parker 

You must read your Latin authors well, for, since you must be decadent, it is better to decay from a good source. Sinister Street, vol. 1 |Compton Mackenzie 

We still seem to detect the influence of a decadent, late Magdalenian style of ornament. The New Stone Age in Northern Europe |John M. Tyler 

Here he became a friend of Grard de Nerval, who was of such influence on the later decadent school. Contes Franais |Douglas Labaree Buffum 

This prince of the seventeenth century was the beau-ideal decadent that many modern novelists have delighted to depict. Court Beauties of Old Whitehall |W. R. H. Trowbridge