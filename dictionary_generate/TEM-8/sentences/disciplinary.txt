Petyo knew about Webster’s disciplinary history in Dover but did not give that information to the Maryland Police Training and Standards Commission, as required by law. The first state to pass a law protecting police accused of misconduct may also be the first to repeal it. |Ovetta Wiggins |February 9, 2021 |Washington Post 

Investigators want to examine years of disciplinary records related to students accused of honor code violations. VMI resists letting investigators interview cadets, faculty without its lawyers present, report says |Ian Shapira |February 9, 2021 |Washington Post 

The following month, Lee was placed on a performance improvement plan, the only disciplinary action she had received in more than five years with the employer, according to her lawsuit. She reported sexual harassment by a former supervisor — and was fired soon after |Samantha Schmidt |February 8, 2021 |Washington Post 

ProPublica and the Asbury Park Press scoured 245 police union agreements to find sample clauses that experts say could waste taxpayer money or impede the disciplinary process. How the Police Bank Millions Through Their Union Contracts |by Andrew Ford, Asbury Park Press, and Agnes Chang, Jeff Kao and Agnel Philip, ProPublica |February 8, 2021 |ProPublica 

The NFL generally waits for the legal process to play out before imposing potential disciplinary measures. Chiefs assistant coach Britt Reid involved in car accident that injured two children |Mark Maske |February 8, 2021 |Washington Post 

For them, the trauma of assault can be compounded by a lack of institutional support, and even disciplinary action. Jameis Winston Cleared of Rape Like Every Other College Sports Star |Robert Silverman |December 22, 2014 |DAILY BEAST 

But the ambiguity of  “appropriate disciplinary action” is what is so frightening about the smoking ban. The University Of New Orleans’ Cigarette Ban Is Total BS |Chloé Valdary |October 21, 2014 |DAILY BEAST 

The Newport News, Virginia, school added, “Federal law prohibits us from reporting on university disciplinary actions.” The Math That Keeps Helping College Rapists |Michael Daly |October 3, 2014 |DAILY BEAST 

The purpose of the mid-June meeting was to discuss disciplinary action by the NFL. Fact-Checking the Sunday Shows: Sept 14 |PunditFact.com |September 14, 2014 |DAILY BEAST 

In the 13 cases that did result in disciplinary action for agents, this most often meant counseling. A Shooting on a Tribal Land Uncovers Feds Running Wild |Caitlin Dickson |August 26, 2014 |DAILY BEAST 

No trait is better marked in the normal child than the impulse to subject others to his own disciplinary system. Children's Ways |James Sully 

A strict, disciplinary master required to teach English at a preparatory school for the Army. The Sexual Life of the Child |Albert Moll 

It is the method by which one fills a Lenten mitebox—it is disciplinary, that is, it is meant to hurt a little, and it does. The Library of Work and Play: Housekeeping |Elizabeth Hale Gilman 

Suffering is not always punitive; it is sometimes disciplinary, designed to wean the good man from his sin. Encyclopaedia Britannica, 11th Edition, Volume 15, Slice 4 |Various 

They are rarely over-bearing in manner, but mostly use a tone which is persuasive rather than disciplinary. Chapters on Jewish Literature |Israel Abrahams