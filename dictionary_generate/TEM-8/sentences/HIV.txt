Egypt has a comparatively low number of HIV cases compared to the rest of Africa, with just 11,000 infected people nationwide. Sisi Is Persecuting, Prosecuting, and Publicly Shaming Egypt’s Gays |Bel Trew |December 30, 2014 |DAILY BEAST 

The following year, he developed pneumocystis pneumonia—a serious infection associated with HIV and AIDS. The LGBT Center That Changed Our Lives |Justin Jones |December 22, 2014 |DAILY BEAST 

Some medicines, like HIV drugs, are very expensive, as most them are under brand names. No More Paper Prescriptions: Docs Fight Fraud by Going Electronic |Dale Eisinger |December 18, 2014 |DAILY BEAST 

Some organizations, like amfAR, provide vital funding for scientists who bring innovative ideas to the HIV/AIDS research field. How You Can Help Make a More LGBT-Friendly World | |December 12, 2014 |DAILY BEAST 

He is a lawyer for AIDS-Free World, which pushes for more active and gay-inclusive policies to combat HIV. How Maurice Tomlinson Was Outed in Jamaica—and Forced Into Exile |Jay Michaelson |December 9, 2014 |DAILY BEAST 

An hiv I reely tae dae this wi a ma beesties gin the Germans come? Anecdotes of the Great War |Carleton Britton Case 

Hiv I no' a richt to tak' the breadth o' the street if I want it? Erchie |(AKA Hugh Foulis) Neil Munro 

Th' girruls wur foriver getting shtuck on yez, an' Oi dunno what ye hiv been doin' since l'avin' Fardale. Frank Merriwell Down South |Burt L. Standish 

Nivver a bit would it do for us both to go in there, fer th' craythers moight hiv us in a thrap. Frank Merriwell Down South |Burt L. Standish 

From there he started again with his people and went upstream to Kapotake-hiv'auve. Seven Mohave Myths |A. L. Kroeber