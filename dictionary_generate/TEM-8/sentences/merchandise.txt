A lot of companies also work with social influencers to promote their merchandise. Networking 101: Why Working Together Creates More Opportunity Than Working Apart |Shantel Holder |September 4, 2020 |Essence.com 

You can also join in the weirdness in our Facebook group and bedeck yourself in Weirdo merchandise from our Threadless shop. Your car is probably full of spiders |PopSci Staff |September 2, 2020 |Popular-Science 

Increasing shipping costs, a lack of live concerts at which to sell merchandise, and a slow down in new vinyl requests from musicians had record production and sales declining. Vinyl sales rock on in spite of Covid-19 |Dan Kopf |August 21, 2020 |Quartz 

It is also hoping to win new supporters for its anchor team, Manchester City, in emerging markets, like India and China—opening up new opportunities for sponsorships and merchandise sales. Why Manchestery City owner CFG’s play for France’s Troyes is shaking up professional soccer |Jeremy Kahn |August 20, 2020 |Fortune 

Meanwhile, it will also bar people from using terms like QAnon to sell merchandise. Will Facebook’s QAnon crackdown succeed? What people are saying so far |Jeff |August 20, 2020 |Fortune 

Manufacturing merchandise, publicity (a radio ad in SF, Facebook ads, venue specific advertising), supplies, shipping. How Much Money Does a Band Really Make on Tour? |Jack Conte |December 8, 2014 |DAILY BEAST 

CEO of armored car company tests his merchandise from the inside. Occupied SUV Fired On With AK-47 |Jack Holmes, The Daily Beast Video |November 12, 2014 |DAILY BEAST 

And they make, I believe, a reasonably good living being there and teaching and selling merchandise. Nigel Lythgoe on How to Save Reality TV, ‘On the Town,’ and ‘Brokeback Ballroom’ |Kevin Fallon |October 22, 2014 |DAILY BEAST 

Business Insider calculated that only about eight percent of the money spent on pink merchandise went to breast cancer charities. The Misogynistic Companies Jumping On The Breast Cancer Bandwagon |Emily Shire |October 16, 2014 |DAILY BEAST 

Also, FX created a bunch of ISIS merchandise that they now need to figure out what to do with. ‘Archer’ Drops ISIS: The FX Series Dumps the Spy Org’s Name in Light of Recent Events |Marlow Stern |October 11, 2014 |DAILY BEAST 

A few days after, three galliots arrived from Macan, laden with a rich cargo of silks and other merchandise. The Philippine Islands, 1493-1898, Volume XX, 1621-1624 |Various 

The ships from China do not come, and it is with their merchandise that our ships must go to Nueva Spaña. The Philippine Islands, 1493-1898, Volume XX, 1621-1624 |Various 

Merchandise brokers, unless factors, negotiate for the sale of merchandise without having possession or control of it. Putnam's Handy Law Book for the Layman |Albert Sidney Bolles 

Every article of merchandise has here, as at Canton, if not its own peculiar street, at least its own side of the street. A Woman's Journey Round the World |Ida Pfeiffer 

Instead of being a destroyer of merchandise, this new craft was an unarmed carrier of merchandise. The Wonder Book of Knowledge |Various