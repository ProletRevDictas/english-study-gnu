In that sense, comparing 2000 to 2020 isn’t “apples to apples,” he says. 3 ways tech stocks resemble the 2000 bubble—and one way they don’t |Anne Sraders |September 16, 2020 |Fortune 

Many flowers, from apples to zinnias, sport both male and female parts. ‘Vampire’ parasite challenges the definition of a plant |Susan Milius |September 16, 2020 |Science News For Students 

This scrub also boasts a string of household ingredients such as pink Himalayan sea salt and apple cider vinegar to remove even the most stubborn dry patches. Scalp scrubs that banish scaly patches and build-up |PopSci Commerce Team |September 4, 2020 |Popular-Science 

While the farm does sell some apples to wholesale distributors, Mofenson says, “Agritourism is really our primary focus.” Pick Your Poison |Nick Mancall-Bitel |September 3, 2020 |Eater 

The worst thing that can happen to them is to serve with a bad apple. Full Transcript: Tomi Lahren on ‘The Carlos Watson Show’ |Daniel Malloy |August 31, 2020 |Ozy 

Leapolitan responded by saying, “hopefully youll [sic] bite into a poison apple.” Grindr’s Trans Dating Problem |David Levesley |January 9, 2015 |DAILY BEAST 

Apple customers, on the other hand, are used to paying premium for perceived quality. Lost For Thousands of Strokes: 'Desert Golfing' Is 'Angry Birds' as Modern Art |Alec Kubas-Meyer |January 2, 2015 |DAILY BEAST 

The process of co-opting black music and selling it back to the adoring public in whiteface is as American as apple pie. The Cultural Crimes of Iggy Azalea |Amy Zimmerman |December 29, 2014 |DAILY BEAST 

Companies like Delta, Apple, and Nike flex their political muscle on behalf of gay rights. Corporations Are No Longer Silent on LGBT Issues | |December 24, 2014 |DAILY BEAST 

Apple, PetSmart, Wells Fargo, Marriott, and Delta also spoke out. Corporations Are No Longer Silent on LGBT Issues | |December 24, 2014 |DAILY BEAST 

This gives to the second volume something of the smell of an apple store-room. Punch, or the London Charivari, Volume 107, November 3, 1894 |Various 

Twenty acres of apple trees all in a orchard together, and twenty acres of strawberries set out betwixt and between the rows! The Bondboy |George W. (George Washington) Ogden 

All this while Squinty was chewing on the apple which he had picked up from the ground after he had jumped over the rope. Squinty the Comical Pig |Richard Barnum 

And it did not take Squinty long to learn to jump the rope when there was no apple on the other side. Squinty the Comical Pig |Richard Barnum 

Then Squinty would toss the apple up in the air, off his nose, and catch it as it came down. Squinty the Comical Pig |Richard Barnum