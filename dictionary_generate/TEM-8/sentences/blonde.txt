Typical Bama Rush TikTok videos share common characteristics, including a bevy of blondes with Southern accents, hashtags of the school’s call, “Roll Tide,” and a widespread affinity for brands like Michael Kors, Shein, Steve Madden and Kendra Scott. RushTok Is a Mesmerizing Viral Trend. It Also Amplifies Sororities’ Problems With Racism |Cady Lang |August 19, 2021 |Time 

The models on the boxes and bottles are all light-skinned, many with blue eyes and blonde hair. The skin lightening business is booming in Kenya—though no one will admit it |Kang-Chun Cheng |August 11, 2021 |Quartz 

That alone proves an upgrade for He-Man and company, even if the blonde do-gooder himself is a bit, shall we say, steroidal. By the Power of Grayskull, ‘He-Man’ Is Back—Beefier and Better Than Ever |Nick Schager |July 23, 2021 |The Daily Beast 

Come up with a way to jam Game of Thrones and Harry Potter into your movie that’s more creative than throwing a blonde wig on Fog Horn Leg Horn to make him look like Khaleesi and a Hufflepuff scarf on LeBron. Debating How Space Jam: A New Legacy Stacks Up Against the Original |Cady Lang |July 19, 2021 |Time 

I was turning into a Raymond Chandler character, only in a novel where no other characters, no hard-luck blondes or double-crossing cops, ever showed up at the door. How to garnish cocktails, now that you’re clinking glasses with friends again |M. Carrie Allan |May 21, 2021 |Washington Post 

One guy hams it up as Juliet, blonde wig and all, as a crowd gathers, delighted by the impromptu performance. Biking With the Bard |Kara Cutruzzula |December 28, 2014 |DAILY BEAST 

Blonde kids named Kyle and Zack cheered on Los Doyers while wearing jerseys with “Valenzuela” on the back. The Liberal Case Against Illegal Immigration |Doug McIntyre |November 25, 2014 |DAILY BEAST 

Thousands of platinum-blonde manes brush against bare, perma-tanned backs moist with snow. Russia’s Gold Digger Academy |Peter Pomerantsev |November 11, 2014 |DAILY BEAST 

For me at least, the singer and the songs on Blonde on Blonde and Highway 61 Revisited are indivisible. Digging the Gold in Dylan’s ‘Basement’ |Malcolm Jones |November 5, 2014 |DAILY BEAST 

A few were rock songs of roughly the same vintage as Highway 61 or Blonde on Blonde. Digging the Gold in Dylan’s ‘Basement’ |Malcolm Jones |November 5, 2014 |DAILY BEAST 

Whereat, he stepped to one side, and led upon the stage, a charming blonde who was greeted profusely. The Homesteader |Oscar Micheaux 

He was a manly young fellow, a sportsman and renowned at cricket, and she was amiable and pretty, a little blonde beauty. Fifty Years of Railway Life in England, Scotland and Ireland |Joseph Tatlow 

At the invitation of the blonde stenographer at the desk, he squatted on a chair and lighted a cigarette. Hooded Detective, Volume III No. 2, January, 1942 |Various 

An indolent blonde, fond of dancing, but a nonentity from both the moral and the intellectual standpoints. Repertory Of The Comedie Humaine, Complete, A -- Z |Anatole Cerfberr and Jules Franois Christophe 

A stout woman with a muddy blonde complexion splotched with freckles. Repertory Of The Comedie Humaine, Complete, A -- Z |Anatole Cerfberr and Jules Franois Christophe