Each globule of our blood is a world (and we have five millions per cubic millimetre). Urania |Camille Flammarion 

The experiments made in beating gold-leaf show that ten thousand leaves are contained in the thickness of a millimetre. Urania |Camille Flammarion 

It is white, cylindrical, straight and about four millimetres long by one millimetre thick. More Hunting Wasps |J. Henri Fabre 

It is a tiny, dull-white cylinder, about three millimetres long by half a millimetre wide. More Hunting Wasps |J. Henri Fabre 

The sting, by straying less than a millimetre, would leave the Scolia without progeny. More Hunting Wasps |J. Henri Fabre