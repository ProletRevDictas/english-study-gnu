It also sheds light on which patterns are linked to more indolent forms of the disease, where intervening with invasive surgery or toxic radiation or chemotherapy might do more harm than good. Changing Cancer Care, So Patients No Longer Feel Like a Number |Alice Park |November 3, 2021 |Time 

Rather, there are indolent cancers, similar to those in the prostate, that grow more slowly, or hardly at all. Bringing WISDOM to Breast Cancer Care |Alice Park |October 22, 2021 |Time 

I mean, who else could possibly be as indolent as a teachers' union member? The Dumbest Journalism Passage of All Time |Michael Tomasky |June 18, 2012 |DAILY BEAST 

This kind of cancer can be so indolent that patients often die with it than from it. Jobs’s Unorthodox Treatment |Sharon Begley |October 6, 2011 |DAILY BEAST 

In part, that is because neuroendocrine cancers tend to be quite slow growing, or indolent. Jobs’s Unorthodox Treatment |Sharon Begley |October 6, 2011 |DAILY BEAST 

Salon wrote: “Hilton is the one everyone has come to see, and her indolent, dull coolness does not disappoint.” Paris Hilton: End of an Era? |Tricia Romano |August 26, 2011 |DAILY BEAST 

[Rushdie] cut to a passage that imagined the most indolent couple imaginable, Linda Evangelista and Goncharov's Ilya Oblomov. Celebrating World Literature |Emily Stokes |April 30, 2010 |DAILY BEAST 

An indolent blonde, fond of dancing, but a nonentity from both the moral and the intellectual standpoints. Repertory Of The Comedie Humaine, Complete, A -- Z |Anatole Cerfberr and Jules Franois Christophe 

To this indolent, pleasure-loving son, nothing could be in greater contrast than the father. Those Dale Girls |Frank Weston Carruth 

The Portuguese are a people that require rousing; they are indolent, lazy, and generally helpless. Spanish Life in Town and Country |L. Higgin and Eugne E. Street 

They are equally indolent and cowardly, when glutted with prey; and they seldom attack men unless they find them asleep. Buffon's Natural History. Volume VII (of 10) |Georges Louis Leclerc de Buffon 

Michel, who was so indolent that he would not pay the slightest attention to his own business affairs, in years gone by! The Seven Cardinal Sins: Envy and Indolence |Eugne Sue