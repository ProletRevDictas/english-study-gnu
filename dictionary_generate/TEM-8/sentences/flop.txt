That doesn’t mean the new platforms are a flop — or even that they aren’t great. Review: Sony’s PlayStation 5 is here, but next-generation gaming is still on its way |Devin Coldewey |November 6, 2020 |TechCrunch 

It was an expensive flop, plagued by delays and quality issues, and the company ended up in bankruptcy. Electric vehicle stocks come unplugged |Aaron Pressman |November 2, 2020 |Fortune 

Whether ill-judged or simply ahead of its time, the transition was a resounding flop. BP is laying out its vision for a low-carbon future. Investors are skeptical |kdunn6 |September 28, 2020 |Fortune 

The company has been rumored for sometime to be working on a less expensive follow-up to its flop smart speaker, the HomePod. Apple’s ‘Time Flies’ event: 5 things to look for |Aaron Pressman |September 15, 2020 |Fortune 

If it’s a flop, we probably will never learn how many people took advantage. This is the most important movie weekend of the year |Alissa Wilkinson |September 4, 2020 |Vox 

When Fernandez got control of AirAsia it had been a limping, government-subsidized flop. Malaysia Airlines Is Going Down |Clive Irving |August 1, 2014 |DAILY BEAST 

Earlier, a two-headed dragon in the Ron Howard flop Willow was known, at least around the set, as the “Ebersisk.” My Friend, Roger Ebert: Pulitzer Prize Winner Tom Shales on the Moving Documentary ‘Life Itself’ |Tom Shales |July 6, 2014 |DAILY BEAST 

Everyone dressed appropriately and even impeccably—not a flip-flop in sight. The Exciting-but-Depressing Obama-Tumblr Student-Loan Summit |Kelly Williams Brown |June 11, 2014 |DAILY BEAST 

In case the film was transposed to 2014, the director believes it would be a guaranteed flop. Punks, UFOs, and Heroin: How ‘Liquid Sky’ Became a Cult Movie |Daniel Genis |June 2, 2014 |DAILY BEAST 

Not surprisingly, the per diem proposal has been a flop since Moran floated it a few weeks ago. These Guys Need a Raise? |Patricia Murphy |April 10, 2014 |DAILY BEAST 

Bud turned his hotcakes with a vicious flop that spattered more batter on the stove. Cabin Fever |B. M. Bower 

Never a flop of tail to indicate gratitude for blandishments, never the faintest symptom of canine appreciation. In Search of the Unknown |Robert W. Chambers 

One of these days Dan will take a flop and land clean over in the Thatcher camp. A Hoosier Chronicle |Meredith Nicholson 

She says to tell you it looks at last like our old eagle bird will have a chance to flop its wings in France. The Treasure Trail |Marah Ellis Ryan 

Margaret knelt in a soft flop of scented lingerie beside the indignant young thing. The Butterfly House |Mary E. Wilkins Freeman