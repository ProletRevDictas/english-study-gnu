Plastic rubbish in the oceans also ensnares birds, turtles and other wildlife. Let’s learn about plastic pollution |Maria Temming |August 17, 2021 |Science News For Students 

Science, by comparison, is “full of things that sound like complete rubbish” but turn out to work remarkably well—for example, neural nets, he says. Geoffrey Hinton has a hunch about what’s next for AI |Siobhan Roberts |April 16, 2021 |MIT Technology Review 

Publishers print stacks of rubbish – “Beach Reading” – for your sake. Our Doomed Love Affair with Summer |P. J. O’Rourke |August 30, 2014 |DAILY BEAST 

The actor has since denied involvement in the projects, calling rumors to the contrary “rubbish.” The Cult of Boba Fett: The ‘Star Wars’ Bounty Hunter’s Spin-Off |Rich Goldstein |June 5, 2014 |DAILY BEAST 

As he advanced his, eyebrows contracted, and his lips seemed to form the word “rubbish.” Read ‘The King in Yellow,’ the ‘True Detective’ Reference That’s the Key to the Show |Robert W. Chambers |February 20, 2014 |DAILY BEAST 

People ought to know that if they stuff themselves silly with high-calorie, rubbish foods they will get fat. Britain’s Weighty Issue |Emma Woolf |January 20, 2014 |DAILY BEAST 

It's complete rubbish from the outside, and on a day like this it's going to look even worse. The Resurgence of British Cuisine |Condé Nast Traveler |January 17, 2014 |DAILY BEAST 

Presently, one of the foremen or overlookers saw it, and wanted to know what all that rubbish had been put there for. Asbestos |Robert H. Jones 

And now, Monsieur Pujol,” said he impudently, “I am willing to sell you this rubbish for the cheque. The Joyous Adventures of Aristide Pujol |William J. Locke 

She hugged him, wheel and all, and began turning over the rubbish with great delight. The Box-Car Children |Gertrude Chandler Warner 

To tell the truth, Henry had found a few things in the rubbish which he had stored in his own pocket. The Box-Car Children |Gertrude Chandler Warner 

And more than one broken flask on its way to the rubbish heap was carefully carried up the hill to the hidden family. The Box-Car Children |Gertrude Chandler Warner