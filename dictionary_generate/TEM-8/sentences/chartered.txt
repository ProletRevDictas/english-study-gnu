The TVA, a federally owned and chartered electric power provider, is a New Deal legacy just like Social Security. Steve Scalise Shows There’s a Fine Line Between Confederate & Southern |Lloyd Green |January 2, 2015 |DAILY BEAST 

As for Snyderman and her three colleagues, whose names were also withheld, they will be flown home on a chartered aircraft. NBC Cameraman Gets Ebola, News Crew to Be Quarantined |Lloyd Grove |October 3, 2014 |DAILY BEAST 

So Sinatra simply chartered Dolly her own Learjet for the twenty-minute flight to Las Vegas. Frank Sinatra and the Birth of the Jet Set |William Stadiem |August 2, 2014 |DAILY BEAST 

For the long hauls and the concert tours, he still chartered the big 707s. Frank Sinatra and the Birth of the Jet Set |William Stadiem |August 2, 2014 |DAILY BEAST 

If Katz was using a chartered jet, “maybe that explains it,” Rendell said. Ed Rendell: I Could Have Been on Inquirer Owner Lewis Katz’s Crashed Jet |Lloyd Grove |June 1, 2014 |DAILY BEAST 

He chartered an outside car, t'other day, at Island Bridge Barrack, and drove to the post-office. The Book of Anecdotes and Budget of Fun; |Various 

The notion was simply that of a chartered bank established upon a novel basis and financing upon an original principle. A Cursory History of Swearing |Julian Sharman 

To my surprise, they told me that our Embassy in Berlin had chartered a special train and they were to be off in the morning. Ways of War and Peace |Delia Austrian 

Mackinnon's association, whose object A chartered company formed. Encyclopaedia Britannica, 11th Edition, Volume 4, Part 3 |Various 

The whole efforts of their members were concentrated on the vain endeavour to restrict trade to the chartered towns. The Influence and Development of English Gilds |Francis Aiden Hibbert