The midday breeze is a bit gusty from the west as temperatures hold in the mid- to upper 40s under partly cloudy skies. D.C.-area forecast: Still mild with sunshine today, gusty showers and storms tomorrow |Brian Jackson |November 29, 2020 |Washington Post 

However, the actual data on remdesivir paint a somewhat cloudy picture. The WHO and FDA are at odds over remdesivir |Rachael Zisk |November 23, 2020 |Popular-Science 

Onshore flow keeps us mostly cloudy today, before a cold front sweeps through with some showers tonight. D.C.-area forecast: Cloudy and cooler today with showers likely tonight; brighter with a gusty breeze tomorrow |Brian Jackson |November 22, 2020 |Washington Post 

Friday night should be partly cloudy with cooler weather as temperatures dip back into the 40s to around 50 right in the city. D.C.-area forecast: Another day of nice, warm weather before rain returns |Matt Rogers |November 10, 2020 |Washington Post 

These hypersaline environments, pockets of brine from which the salt crystallized, are what cause the crystals to be cloudy. Preserving a Sense of Wonder in DNA - Issue 92: Frontiers |Virat Markandeya |October 28, 2020 |Nautilus 

Imagine driving through the Scottish countryside, rolling through a vast landscape of green hills and cloudy skies. Ester Elchies, The Estate Built By Whiskey | |December 10, 2014 |DAILY BEAST 

“We can blame Carrie Bradshaw for this,” says Shaunaq Arora, half-joking; his sigh tinged with the cloudy breath of his Gauloises. How Brooklyn Invaded Paris—Next Stop, the World |Brandon Presser |October 23, 2014 |DAILY BEAST 

Mostly cloudy Monday through Wednesday, cloudy Thursday, overcast Friday, and considerable cloudiness over the weekend. It’s Always Sunny In England |The Daily Beast |September 17, 2014 |DAILY BEAST 

It was one of those rare winter days when it was cloudy in Palm Springs. Frank Sinatra and the Birth of the Jet Set |William Stadiem |August 2, 2014 |DAILY BEAST 

It was sunny, it was cloudy, it was raining, it was completely covered in snow, and then by the end of the day it was gone. ‘Sharknado 2’ in Winter: Has the Franchise Jumped the Shark? |Jason Lynch |July 28, 2014 |DAILY BEAST 

It is small in cloudy swelling from toxins and drugs, and variable in renal tuberculosis and neoplasms. A Manual of Clinical Diagnosis |James Campbell Todd 

It was a cloudy, stormy evening: high wind was blowing, and the branches of the trees groaned and creaked above our heads. Blackwood's Edinburgh Magazine, No. CCCXXXIX. January, 1844. Vol. LV. |Various 

A sharp rain, too, was beating against the window-panes, and the sky looked black and cloudy. Oliver Twist, Vol. II (of 3) |Charles Dickens 

He was still able to think consecutively, although his thoughts were cloudy and but dimly realised. The Everlasting Arms |Joseph Hocking 

The cloudy yellow twilight disclosed that a man little way off was a man and not a horse but did hardly more. Hilda |Sarah Jeanette Duncan