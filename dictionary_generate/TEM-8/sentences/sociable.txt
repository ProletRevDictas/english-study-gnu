For others, the thought of shared office devices, a lack of social distancing and an expectation to be, well, sociable again, fills them with dread. ‘We’re seeing a wide spectrum of feelings’: Reopening of society and offices is causing unexpected anxiety |Jessica Davies |May 17, 2021 |Digiday 

Sabrine was the outgoing, sociable type, and had many friends, while Ziad was shy and a little more introverted. A Sunni-Shia Love Story Imperiled by al Qaeda |Ruth Michaelson |December 26, 2014 |DAILY BEAST 

“Sociable” and “puckish” is how a Toledo Blade headline described them in 1957. Lovable ‘Madagascar’ Penguins Are Known to Rape and Torture in Real Life |Asawin Suebsaeng |November 26, 2014 |DAILY BEAST 

The goal of the present research is to help create the programming for a robot that is “a sociable partner.” Japan's Robots Are Reading Your Emotions |Angela Erika Kubo, Jake Adelstein |August 6, 2014 |DAILY BEAST 

He was gregarious and sociable, enjoying the company of entourages whenever he went to Cannes or some other film festival. My Friend, Roger Ebert: Pulitzer Prize Winner Tom Shales on the Moving Documentary ‘Life Itself’ |Tom Shales |July 6, 2014 |DAILY BEAST 

Edmund is now 4, and is a giggly, sociable, nosy, occasionally impertinent boy. The Cost of Raising a Special Needs Son |Elizabeth Picciuto |June 11, 2014 |DAILY BEAST 

He'd rather see me doing my duty than having a sociable pipe with him and hearing about the war. The Soldier of the Valley |Nelson Lloyd 

He had traveled over Europe, and parts of the East, and possessed great colloquial powers when inclined to be sociable. The Every Day Book of History and Chronology |Joel Munsell 

A man who is swayed by his feelings is more sociable and agreeable to converse with than one who is swayed by his intelligence. The 'Characters' of Jean de La Bruyre |Jean de La Bruyre 

But she was not exactly a sociable old lady, and few of the Thetford people knew her. Robin Redbreast |Mary Louisa Molesworth 

Almost the first thing with which I became sociable was a book which, at my first sight of it, had a fascination for me. You Never Know Your Luck, Complete |Gilbert Parker