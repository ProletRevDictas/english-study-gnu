It declined to disclose details about the volunteer’s illness. AstraZeneca’s COVID-19 vaccine is back on track after getting the green light to resume trials in the U.K. |Bernhard Warner |September 13, 2020 |Fortune 

Asked if he would have asked Mallott to resign if the lieutenant governor had not volunteered to do so, Walker said he doesn’t know. The Woman Propositioned by Alaska’s Former Lieutenant Governor Tells Her Story for the First Time |by Kyle Hopkins and Michelle Theriault Boots, Anchorage Daily News |September 10, 2020 |ProPublica 

Shortly after arriving in the United States, he volunteered to join the Army and went back and fought in Europe. My Dad served in WWII — he was a hero, not a loser |Peter Rosenstein |September 10, 2020 |Washington Blade 

All that is known officially is that one of the study volunteers went to the hospital after having neurological problems. Here’s what pausing the AstraZeneca-Oxford coronavirus vaccine trial really means |Aimee Cunningham |September 9, 2020 |Science News 

Depending on infection rates for the disease, a phase three vaccine trial may involve thousands to tens of thousands of volunteers. Oxford Scientists: These Are Final Steps We’re Taking to Get Our Coronavirus Vaccine Approved |Rebecca Ashfield |September 9, 2020 |Singularity Hub 

He then went back to his volunteer corps, which had formed when they did not yet have an ambulance. 'Please Don't Die!': The Frantic Battle to Save Murdered Cops |Michael Daly |December 22, 2014 |DAILY BEAST 

There is a distinct smell of apples, which are handed out by volunteer workers. Inside the Smuggling Networks Flooding Europe with Refugees |Barbie Latza Nadeau |December 15, 2014 |DAILY BEAST 

Women do volunteer and women are victims—you have to think in nuanced kind of way. The New Face of Boko Haram’s Terror: Teen Girls |Nina Strochlic |December 13, 2014 |DAILY BEAST 

You can read more about the civilian-volunteer position here. LAPD Foundation: Mark Wahlberg Would Make a Good Reserve Cop |Asawin Suebsaeng |December 9, 2014 |DAILY BEAST 

However, the Los Angeles Police Memorial Foundation seems to be behind the idea of making their famous supporter a volunteer cop. LAPD Foundation: Mark Wahlberg Would Make a Good Reserve Cop |Asawin Suebsaeng |December 9, 2014 |DAILY BEAST 

Yet there never was lacking a volunteer, either man or woman, to go to that well and obtain the precious water. The Red Year |Louis Tracy 

There were no more sleepless nights, fearing an attack from the dreaded rebel or the volunteer. The Philippine Islands |John Foreman 

His father, a man of means, was prominent as one of the pioneers in organizing the volunteer army of Great Britain. The Recent Revolution in Organ Building |George Laing Miller 

On reaching the front the volunteer captain soon found scope for his pencil. Napoleon's Marshals |R. P. Dunn-Pattison 

Captain Brasyer brought 130 loyal Sikhs to the column: there were six small guns, and eighteen volunteer cavalry. The Red Year |Louis Tracy