The report further argues that “Google has also actively demoted certain rivals through imposing algorithmic penalties.” Congressional report blasts Google, Apple, Amazon and Facebook as monopolistic ‘gatekeepers’ of the digital economy |Greg Sterling |October 7, 2020 |Search Engine Land 

According to Wikipedia “At age 21, she worked for the Social Security Administration office in Oklahoma, where she was demoted after becoming pregnant with her first child.” A hero passes; may RBG rest in peace |Peter Rosenstein |September 24, 2020 |Washington Blade 

Murphy fiercely denies those allegations, but shortly after the reports were published, he was demoted from his position and reassigned to an administrative support role. What to make of the DHS whistleblower’s shocking complaint |Alex Ward |September 11, 2020 |Vox 

For example, Bing might demote name-calling and offensive statements. A deeper dive into more of the Bing Search ranking factors |Barry Schwartz |August 25, 2020 |Search Engine Land 

The Narendra Modi government also bifurcated the state and demoted it to two union territories of Jammu & Kashmir and Leh. One year in, Modi’s “new Kashmir” has pauperised its people |Manavi Kapur |August 5, 2020 |Quartz 

Obama or Congress could demote or impeach Jaczko within the next year; either move would be highly unprecedented. Nuclear Regulatory Commission Chairman Greg Jaczko's Controversial Reign |Daniel Stone |December 14, 2011 |DAILY BEAST 

Unless he has a good excuse I shall demote him, by making you first mate. The Launch Boys' Adventures in Northern Waters |Edward S. Ellis 

Billy Kasker wandered off for a few moments and I had to demote him. Be It Ever Thus |Robert Moore Williams