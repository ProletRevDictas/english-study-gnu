However, the smart folks brought along these river-specific sleeping pads. The Gear You Need to Bring on a 225-Mile River Trip |Mitch Breton |September 6, 2020 |Outside Online 

Rocket Lab says it has monthly launches scheduled for the rest of 2020, including the company's first flight from a new pad at Wallops Island, Virginia. Rocket Report: Musk updates Super Heavy plan, China to launch spaceplane? |Eric Berger |September 4, 2020 |Ars Technica 

You’ll feel better, be back on your couch bingeing Selling Sunset before you know it, and you won’t be stinking up your pad. Working out at home? Here’s how to keep your house from smelling like a gym. |Harry Guinness |September 3, 2020 |Popular-Science 

This keyboard is a speed machine with a comfortable wrist pad for sustained comfort. Serious upgrades for your computer keyboard |PopSci Commerce Team |September 2, 2020 |Popular-Science 

Marry that with the adventures that nature offers, and the pandemic could serve as a launch pad to teach kids outdoor life skills that will come in handy. Is School Out Forever? |Daniel Malloy |August 9, 2020 |Ozy 

Instead, they saw music videos as a launch pad for a whole new artistic movement: virality. OK Go Is Helping Redefine the Music Video For the Internet Age |Lauren Schwartzberg |December 15, 2014 |DAILY BEAST 

He described in painful detail the composition of the bars and the heavy shackles on the pad locks. I Shot Bin Laden |Elliot Ackerman |November 16, 2014 |DAILY BEAST 

He has underpinned his future program by winning from NASA a 20-year lease on the legendary launch pad 39A at Cape Canaveral. Tycoons in Space: One in Orbit and One Still Grounded |Clive Irving |October 5, 2014 |DAILY BEAST 

Would you like to come over and see my new pad this weekend? When Gary Wright Met George Harrison: Dream Weaver, John and Yoko, and More |Gary Wright |September 29, 2014 |DAILY BEAST 

If ESPN is a sleek bachelor pad, ESPNW is the cottage next door filled with Activia and ultra-soft toilet paper. Women's Sports Are Getting Less Airtime |Evelyn Shoop |August 23, 2014 |DAILY BEAST 

Her mother, wearing an ink-stained jacket, was busy at her desk, the pen scratching on the big sheets of pad paper. The Girls of Central High on the Stage |Gertrude W. Morrison 

Bobby said it should have been written on yellow paper with an asbestos pad under it to save scorching Miss Pendletons desk. The Girls of Central High on the Stage |Gertrude W. Morrison 

Margaret Halley stared reflectively at the blotting-pad for a moment, and then described a typical seance at Kazmah's. Dope |Sax Rohmer 

As she sat by, crushing the juice from the berries with a stick, Jess planned the ink pad. The Box-Car Children |Gertrude Chandler Warner 

These they used for stuffing for the pad, and covered them with a pocket which Violet carefully ripped from her apron. The Box-Car Children |Gertrude Chandler Warner