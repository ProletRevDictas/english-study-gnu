Capital Plus Financial hasn’t yet filed any responses, but told ProPublica that the sole named plaintiff had provided an “illegible” tax return that wasn’t signed, which is why the company decided to revoke his loan. They Promised Quick and Easy PPP Loans. Often, They Only Delivered Hassle and Heartache. |by Lydia DePillis and Derek Willis |January 14, 2022 |ProPublica 

Hyperreal individualism is where the original references are largely illegible or incoherent, but the individual wishes to define themselves and create an identity around their own disparate tastes and styles anyway. The great American cool |Safy-Hallan Farah |July 14, 2021 |Vox 

Say one of our story producers has an image that looks great in a desktop web browser but is illegible when it gets squished down into a mobile screen. A New Article Page Design at ProPublica |by ProPublica’s Design and Platform Team |March 23, 2021 |ProPublica 

Illegible handwriting and typos lead to errors and duplicate entries. A Bipartisan Path to Fixing America’s Broken Elections |Michael Waldman |January 24, 2014 |DAILY BEAST 

They once would have seemed full of attitude, couched in almost illegible forms. Transmuting Lead into Thoughts |Blake Gopnik |June 10, 2013 |DAILY BEAST 

She continued, “If I saw a signature and it looked like that, illegible, I would say: Beware.” Bernie's John Hancock |Allan Dodds Frank |March 25, 2009 |DAILY BEAST 

They grew more shaky and more illegible towards the end, but they were sufficient to make the truth absolutely clear. The Doctor of Pimlico |William Le Queux 

The inscription, which is in Gothic letters, is rendered illegible by time. Travels through the South of France and the Interior of Provinces of Provence and Languedoc in the Years 1807 and 1808 |Lt-Col. Pinkney 

It is now illegible, and some of its lines appear to have been carefully erased—by some High Church chisel, probably. East Anglia |J. Ewing Ritchie 

They are, however, almost illegible, and I have made no attempt to reproduce them in the picture. In Search Of Gravestones Old And Curious |W.T. (William Thomas) Vincent 

There is a black-letter inscription upon the front of the structure, but it is unfortunately quite illegible. Bell's Cathedrals: The Cathedral Church of Ripon |Cecil Walter Charles Hallett