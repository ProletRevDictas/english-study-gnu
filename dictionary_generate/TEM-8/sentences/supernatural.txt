Fox felt that her performance in that movie, as well as the supernatural western “Jonah Hex” in 2010, didn’t deserve the harsh judgment it received. The Megan Fox renaissance is here. It’s long overdue. |Ilana Kaplan |July 2, 2021 |Washington Post 

You can also expect an added, “Pirates”-like element of the supernatural. 12 summer movies that just might get you off the couch and into the theater |Michael O'Sullivan |June 3, 2021 |Washington Post 

The plot centers on a family who moves to a farm in Oklahoma, and, after experiencing some supernatural happenings, discover their ties to the original Ghostbusters. The 20 Most Anticipated Movies of Summer 2021 |Marlow Stern |May 31, 2021 |The Daily Beast 

That’s why, even with the supernatural element in play, it feels emotionally true that Neil, a basically normal kid, would do something so awful it could leave a girl dead. How Gold Diggers finds the horror in petty high school betrayal |Constance Grady |May 14, 2021 |Vox 

He expressed a scientific attitude renouncing the supernatural more clearly than his predecessors. 2,500 years ago, the philosopher Anaxagoras brought science’s spirit to Athens |Tom Siegfried |May 4, 2021 |Science News 

Although Korra looks at PTSD and assault with supernatural grandiosity, fans were quick to pick up on it in some forums. Science-Fiction TV Finds a New Muse: Feminism |David Levesley |November 29, 2014 |DAILY BEAST 

On TripAdvisor, visitors gush about the tour and the supernatural experiences encountered during their stay. Would You Stay in Lizzie Borden’s Ax-Murder House? |Nina Strochlic |October 30, 2014 |DAILY BEAST 

Thrust into a world of seemingly supernatural monsters, his adventure begins. The Walking Dead’s Luke Skywalker: Rick Grimes Is the Perfect Modern-Day Mythical Hero |Regina Lizik |October 28, 2014 |DAILY BEAST 

It is clear Hitchens had no use for the supernatural or spiritual, but his alternative to religion was not merely scientific. Cornel West’s Disappointing Decline |David Masciotra |October 23, 2014 |DAILY BEAST 

It was identified as an ancient exorcism technique that indicated she had been suspected of supernatural powers. Bulgaria’s Vampire Graveyards |Nina Strochlic |October 15, 2014 |DAILY BEAST 

In a warlike age this peacefulness of a monarch was the great and supernatural phenomenon. Solomon and Solomonic Literature |Moncure Daniel Conway 

The resurrection of a supernatural man is not quite sufficient for people not supernatural. Solomon and Solomonic Literature |Moncure Daniel Conway 

It seems that there must have been some supernatural power of support to have sustained children under so awful an ordeal. Madame Roland, Makers of History |John S. C. Abbott 

One of the strongest opponents to the supernatural theory was a young man of perhaps twenty-seven years of age. The Everlasting Arms |Joseph Hocking 

Although the thought of the supernatural had left him, his experience of a few minutes before doubtless coloured his mind. The Everlasting Arms |Joseph Hocking