One thing the data showed was that the league’s low positivity rate wasn’t just accomplished with deep pockets and clout, though it used both of those. The NFL’s pandemic response was a striking success — and a genuine public service |Sally Jenkins |February 10, 2021 |Washington Post 

Amazon has repeatedly denied it uses its market clout to compete unfairly. Amazon CEO Jeff Bezos‘s successor will inherit his challenges |Jay Greene, Cat Zakrzewski |February 4, 2021 |Washington Post 

Last summer, Bezos was summoned to Capitol Hill to testify virtually alongside the CEOs of Apple, Google and Facebook before a House antitrust subcommittee investigation on the clout of the tech behemoths. Jeff Bezos stepping down as Amazon CEO, transitioning to executive chair role |Jay Greene, Tony Romm |February 4, 2021 |Washington Post 

As much as I appreciate high-caliber content, I shudder at the prospect of a world where a16z carries more media clout than the Times or the Wall Street Journal. Tech and crypto funder Andreessen Horowitz wants to replace the media. Is that bad news? |Jeff Roberts |January 20, 2021 |Fortune 

Tuesday’s Georgia runoff elections could hand Democrats an effective majority in the Senate, providing them somewhat more clout to set a health-care agenda. Pair of Georgia runoff races are razor close with U.S. Senate control at stake |Felicia Sonmez, Colby Itkowitz, John Wagner, Paulina Firozi, Amy B Wang |January 6, 2021 |Washington Post 

The great migration to the North through World War II had given black people at least some clout as they began to vote Democratic. How Rock and Roll Killed Jim Crow |Dennis McNally |October 26, 2014 |DAILY BEAST 

But the two also could abstain from caucusing with either party and possibly have even more clout. The Independents Who Could Tip the Senate in November |Linda Killian |October 13, 2014 |DAILY BEAST 

Military victory always gives a field commander added clout with his civilian overseers. George Washington, the First Vaxxer |Tom Shachtman |October 5, 2014 |DAILY BEAST 

CrowdMed is also a system where Ivy League diplomas and prestigious residencies carry no clout. Strangers Diagnose Your Illness and Get Cash in Return |Kevin Zawacki |August 15, 2014 |DAILY BEAST 

If my side had had real clout, there would have been no Iraq War. Awkward: This Democratic Judicial Candidate's Husband Is a White Supremacist |Gideon Resnick |August 11, 2014 |DAILY BEAST 

Skelton mentions rochets 'of fyne Raynes'; Colin Clout, 316. Chaucer's Works, Volume 1 (of 7) -- Romaunt of the Rose; Minor Poems |Geoffrey Chaucer 

Joe thought the fellow was loafing, so he hit him a clout on the head, and made very uncomplimentary remarks. The Chequers |James Runciman 

The Nubian wore black tights and shirt, black slippers and a white skull cap and breech-clout. Fifty Contemporary One-Act Plays |Various 

What, he that weares a clout about his necke,His cuffes ins pocket, and his heart ins mouth? The Fatal Dowry |Philip Massinger 

There are instances, however, of clout in the sense of a plate of iron fastened on the sole of a shoe. Milton's Comus |John Milton