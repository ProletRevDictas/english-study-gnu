The faucet monitors your water usage and can detect leaks, too. These pieces of kitchen gear make excellent gifts |PopSci Commerce Team |October 6, 2020 |Popular-Science 

For a 1% annual fee based on the purchase price, Pacaso will handle all the annoying details of managing a property from afar, from paying the utilities to finding a plumber to come fix a leaky faucet. Want a second home during COVID? These Zillow vets have invented a radically less expensive way to buy one |Lee Clifford |October 4, 2020 |Fortune 

Not sure whether it’s a focus thing, or simply that the Heat change defenses a lot midgame, which might turn the faucet on or off for the opponent. The Keys To Winning The NBA Finals For The Lakers And Heat |Chris Herring (chris.herring@fivethirtyeight.com) |September 30, 2020 |FiveThirtyEight 

All else being equal, this occasional leaky faucet of carbon would have created a slight deficit in the energy available to life in the past. Dawn of the Heliocene - Issue 90: Something Green |Summer Praetorius |September 16, 2020 |Nautilus 

Sometimes you just need a hand with dishes, but don’t have the space to set up with the kitchen faucet. Small dishwashers that fit in almost any kitchen |PopSci Commerce Team |August 25, 2020 |Popular-Science 

We learn about his various phobias—his fear of scary TV shows or the sound the bathroom faucet makes. Fear And Self-Loathing In Scandinavia: The Fiction Of Karl Ove Knausgaard |Ted Gioia |May 28, 2014 |DAILY BEAST 

The Iranian threat, of course, is real, but its immediacy gets turned on and off by the Prime Minister like a faucet. "When Netanyahu Speaks, The World Listens" |Don Futterman |January 14, 2013 |DAILY BEAST 

The chief drew the tumbler full twice from the faucet and gulped down the water. The Winning Clue |James Hay, Jr. 

The hose coupling makes it easy to connect the motor directly to the water faucet. The Boy Mechanic, Book 2 |Various 

For racking the wine, we should have: 1st a large brass faucet. The Cultivation of The Native Grape, and Manufacture of American Wines |George Husmann 

When the faucet is closed, the gas supply is shut off and the burners are put out. Elements of Plumbing |Samuel Dibble 

There shouldn't be an inch of water-pipe, nor a single faucet, that didn't have his critical inspection—and bill according! Dorothy |Evelyn Raymond