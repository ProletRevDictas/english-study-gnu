They grow hungry and decide to split an apple, but half an apple feels meager. Banach-Tarski and the Paradox of Infinite Cloning |Max G. Levy |August 26, 2021 |Quanta Magazine 

In a world inundated with self-prescribed hacks, quick fixes, and countless other silver bullets—the majority of which are plentiful on promises yet meager on results—it’s easy to forget the importance of hard work. Two Simple Rules for Progressing at Anything |cobrien |July 21, 2021 |Outside Online 

Despite meager supply and swift home-price growth, the purchase market remains strong. Mortgage rates are driven down to their lowest levels since winter |Kathy Orton |July 8, 2021 |Washington Post 

Instead, the returns have been meager, as the Office of Lottery and Gaming’s interim executive director Ridgely Bennett acknowledged in a council hearing last month. Auditor: D.C.’s sports betting company is far behind on requirement to hire locally |Julie Zauzmer |July 8, 2021 |Washington Post 

The series takes a group of strangers from different walks of life and strands them in the wilderness with only the clothes on their backs and some meager supplies. The 10 Best Outdoor Reality Shows of All Time |Mike Bloom |July 1, 2021 |Outside Online 

Especially in a country where the minimum wage is a meager $3.00 an hour. House of the Witch: The Renegade Craft Brewers of Panama |Jeff Campagna |November 30, 2014 |DAILY BEAST 

Rising to retrieve it, I offer her what meager reassurance I can muster. The Stacks: Grateful Dead I Have Known |Ed McClanahan |August 30, 2014 |DAILY BEAST 

He called on his parents not to accept any “meager compensation” for his death. Medieval Cruelty in Modern Times: ISIS Thugs Behead American Journalist |Christopher Dickey |August 19, 2014 |DAILY BEAST 

Look through that PDF I just linked to and feast on the meager amounts earmarked for democracy and assistance programs. Is It Just Me or Is the World Exploding? So Why Isn’t Obama Doing More? |Michael Tomasky |July 28, 2014 |DAILY BEAST 

Personal consumption expenditures—people buying stuff—grew at a meager one percent rate in the first quarter. How Obamacare Helped Crash the Economy |Daniel Gross |June 25, 2014 |DAILY BEAST 

This report, however, was very meager and lacking in any profusion of mechanical detail. The Wonder Book of Knowledge |Various 

I have no memory of either parent and my information concerning them is meager and second hand. David Lannarck, Midget |George S. Harney 

The surname Thin has the same meaning as Meagre, from which the common name Meager comes. Stories That Words Tell Us |Elizabeth O'Neill 

Old Lewson, the man who sat under the apple tree, gave his meager property to his children. A Yankee from the West |Opie Read 

A human lamprey, sticking himself always at the thin and meager board of the poor, a vile parasite, but holy! The Adventures of Kathlyn |Harold MacGrath