He emphasized that the company has a robust compliance program and that neither it nor any of its executives have been indicted for any crime. Pentagon awarded massive contract to Virginia company a U.S. senator called ‘fraudulent’ |Aaron Gregg |February 18, 2021 |Washington Post 

Republicans initially floated the idea of trying to give Traficant their own slots on committees, but by May of that year he was indeed indicted. Marjorie Taylor Greene would be in rare company if she is kicked off her committees |Aaron Blake |February 3, 2021 |Washington Post 

There was also an expectation that he would soon be indicted, given several of his associates had been convicted in a long-running corruption investigation. Marjorie Taylor Greene would be in rare company if she is kicked off her committees |Aaron Blake |February 3, 2021 |Washington Post 

He was indicted on 17 federal weapons charges after police allegedly found materials to produce Molotov cocktails and five illegal firearms — including an AR-15 — in his pickup truck. The Radicalization of Kevin Greeson |by Connor Sheets, AL.com |January 15, 2021 |ProPublica 

When two members of the Louisiana returning board were indicted in June 1877 for altering election returns, Democrats took it as proof that Hayes’s election had indeed been fraudulent. Ted Cruz’s proposed election commission can only hurt the country |Stuart MacKay |January 6, 2021 |Washington Post 

We see a system that will indict a 20-year-old for selling crack but not a police officer for choking the life out of a citizen. Bobby Shmurda and Rap’s Ultimate Hoop Dream |Rawiya Kameir |December 23, 2014 |DAILY BEAST 

Both were killed by police officers, but grand juries failed to indict in either case. Alleged Cop Killer Ismaaiyl Brinsley Had a Death Wish |M.L. Nestel |December 22, 2014 |DAILY BEAST 

I looked in the news and watched the news last night after the grand jury decided not to indict him. Even Grade School Kids Are Protesting the Garner Killing Now |Caitlin Dickson |December 6, 2014 |DAILY BEAST 

Even though a grand jury chose not to indict the cop who killed Eric Garner, the video is damning of police. ‘I Can’t Breathe!’ ‘I Can’t Breathe!’ A Moral Indictment of Cop Culture |Michael Daly |December 4, 2014 |DAILY BEAST 

Today, a grand jury announced that it would not indict the officer, Daniel Pantaleo. First Mike Brown, Then Eric Garner: Prosecutors Can’t Be Trusted to Try Cops |Sally Kohn |December 3, 2014 |DAILY BEAST 

Further clauses indict the inferior ministers occupied about the cess. Ireland Under the Tudors, Vol. II (of 3) |Richard Bagwell 

The curate, properly managed, may depose to the contrary; and then we will indict them all for forgery and conspiracy. Night and Morning, Complete |Edward Bulwer-Lytton 

People have a genius for remorse as for other emotions, and Forbes was of those who can mercilessly indict their own souls. What Will People Say? |Rupert Hughes 

The grand jury refused to indict the Mayor, and indicted his accusers. A History of the City of Brooklyn and Kings County Volume II |Stephen M. Ostrander 

If we believe the Duke himself, he was forced to move at last by efforts to indict him as a traitor in Ireland itself. History of the English People, Volume III (of 8) |John Richard Green