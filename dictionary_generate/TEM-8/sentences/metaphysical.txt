During this time, normal activities are disrupted, so an indivdual’s thoughts become more attuned to the metaphysical. Fasting May Have Become a Health Fad, But Religious Communities Have Been Doing It For Millennia |LGBTQ-Editor |July 30, 2021 |No Straight News 

There is a profound and metaphysical difference between the two. Want art you can’t look away from? These 5 visual illusions will entrance your mind |Corinne Iozzio |July 21, 2021 |Popular-Science 

Moreover, there may well be no independent “metaphysical” substance constituting our reality that underlies this web. Is Reality a Game of Quantum Mirrors? A New Theory Suggests It Might Be |Peter Evans |June 30, 2021 |Singularity Hub 

Springsteen closed the show with a new song off Letter to You, “I’ll See You in My Dreams,” a metaphysical invitation that replaces the earlier closer, “Born to Run.” The Reopening of Springsteen on Broadway Brought Broadway Out of Hibernation—and One Packed Theater Into a Brighter Future |Stephanie Zacharek |June 28, 2021 |Time 

Bechdel references these metaphysical moments, or “aerobic inebriation,” throughout her book. Memoirist Alison Bechdel Is No Longer Trying to Outrun Death |Eliana Dockterman |May 3, 2021 |Time 

He left the U.S., seeking a spiritual and metaphysical connection for his work elsewhere. A Gay American Artist in Kaiser’s Berlin |Sarah Bay Williams |August 10, 2014 |DAILY BEAST 

I do not say a creation … Any preexistence of the universe has a metaphysical character. Evangelicals Still Don’t Know What to Do With the Big Bang |Karl W. Giberson |March 23, 2014 |DAILY BEAST 

Things get metaphysical in one of the most masterful hours of television since ‘Breaking Bad.’ ‘True Detective’ Episode 5 Review: ‘The Secret Fate of All Life’ is the Best Episode Yet |Andrew Romano |February 17, 2014 |DAILY BEAST 

So in episode five—not to spoil anything—Cohle gives one of his metaphysical addresses. Inside the Obsessive, Strange Mind of True Detective’s Nic Pizzolatto |Andrew Romano |February 4, 2014 |DAILY BEAST 

Caleb makes clear that those plaguing him are honest-to-god metaphysical beings and not traumatic figments. Possessed by PTSD, A Veteran Uses Exorcisms to Cast Out His Demons |Brian Van Reet |February 2, 2014 |DAILY BEAST 

Metaphysical terms, taken in their proper sense, have sometimes determined the opinion of twenty nations. A Philosophical Dictionary, Volume 1 (of 10) |Franois-Marie Arouet (AKA Voltaire) 

In his metaphysical conversations, Baudelaire spoke much of his ideas, little of his sentiments, and never of his actions. Charles Baudelaire, His Life |Thophile Gautier 

Metaphysical subtlety, in pursuit of an impracticable theory, could alone have devised one that is calculated to destroy it. Key-Notes of American Liberty |Various 

Dr. Whewell equally misunderstands M. Comte's doctrine respecting the second, or metaphysical stage of speculation. A System of Logic: Ratiocinative and Inductive |John Stuart Mill 

A metaphysical explanation can be of use only when there is a doctrine of life which it serves to make manifest. My Religion |Leo Tolstoy