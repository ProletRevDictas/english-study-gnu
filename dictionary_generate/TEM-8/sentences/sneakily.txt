A sneaky winter storm that was predicted to dump several inches of snow left only a thin but hazardous glaze of ice across the region Thursday. Icy storm glazes region, making roads a mess |Fredrick Kunkle |February 19, 2021 |Washington Post 

For all their simplicity, viruses are sneaky little life forces. A Language AI Is Accurately Predicting Covid-19 ‘Escape’ Mutations |Shelly Fan |January 19, 2021 |Singularity Hub 

The pantheon of Sediuk pranks ranges from sneakily clever to blatantly rude. An Analysis of Vitalii Sediuk’s Pranks (He’s the Guy Who Touched Brad Pitt) |Amy Zimmerman |May 29, 2014 |DAILY BEAST 

Stallworth got his revenge, sneakily getting a photo taken of him with his harms around Duke. Speed Read: A Black Cop Infiltrates The KKK |William O’Connor |May 27, 2014 |DAILY BEAST 

Letts looked sneakily at the hut window where hung the remnants of a ragged curtain—all was quiet. Tess of the Storm Country |Grace Miller White 

Having pulled the excited mare into control and dismounted, I looked round, sneakily sideways, for Monty. Tell England |Ernest Raymond