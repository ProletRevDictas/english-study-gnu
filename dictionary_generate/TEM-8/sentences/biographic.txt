There are two key advantages of biometric over biographic checks according to Verdery. Wanna Fly? Give Us a Fingerprint. |Jacob Siegel |March 11, 2014 |DAILY BEAST 

To all which questions, not unessential in a Biographic work, mere Conjecture must for most part return answer. Sartor Resartus |Thomas Carlyle 

A biographic sketch of Emma Goldman's interesting career, with splendid portrait, is included in the book. Marriage and Love |Emma Goldman 

I remember once remarking to him upon the value of patois within certain limits—not only in imaginative but in biographic art. Lavengro |George Borrow 

See our Essay for the biographic interest of this poem, and also Notes at its close. The Complete Works of Richard Crashaw, Volume I (of 2) |Richard Crashaw 

To this period belong the Biographic Sketches which she contributed to a London newspaper. Critical Miscellanies (Vol. 3 of 3) |John Morley