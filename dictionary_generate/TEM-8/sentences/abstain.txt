As part of this practice, Muslims abstain from food, water, smoking, sex and all sensory pleasures from dawn to dusk during Ramadan, the ninth month of the Islamic lunar calendar. Fasting May Have Become a Health Fad, But Religious Communities Have Been Doing It For Millennia |LGBTQ-Editor |July 30, 2021 |No Straight News 

Metro board member Michael Goldman, who represents Maryland, abstained from the vote, saying the transit agency should switch completely to electric-bus purchases starting next year. Metro is phasing out diesel-powered buses, with plans to transform its fleet to electric by 2045 |Justin George |June 24, 2021 |Washington Post 

Soon-Shiong, who had told The Post as recently as midweek that he hadn’t yet decided how he was going to vote on the offer, ended up abstaining from voting. Tribune shareholders vote to sell legendary chain of newspapers to a hedge fund |Elahe Izadi, Sarah Ellison |May 21, 2021 |Washington Post 

He would drink and take drugs to “feel less of what I was feeling,” estimating that, even if he abstained during the week, he would drink a week’s worth in one day on a Friday or Saturday night. Prince Harry to Oprah: Prince Charles and the Royal Family ‘Bullied’ Me |Kevin Fallon |May 21, 2021 |The Daily Beast 

Maybe they’re abstaining because they had severe alcohol-use disorders in the past, and this has led to health conditions. Let’s Be Blunt: Marijuana Is a Boon for Older Workers (Ep. 459) |Stephen J. Dubner |April 22, 2021 |Freakonomics 

In fact, I publicly vowed to abstain from The Ball in 2012, but professional responsibilities and curiosity got the better of me. The Craziest Date Night for Single Jews, Where Mistletoe Is Ditched for Shots |Emily Shire |December 26, 2014 |DAILY BEAST 

But the two also could abstain from caucusing with either party and possibly have even more clout. The Independents Who Could Tip the Senate in November |Linda Killian |October 13, 2014 |DAILY BEAST 

So is everyone around you, even if you find a way to abstain. Keep Our Wilderness Off Of Wi-Fi |Michael Schulson |September 3, 2014 |DAILY BEAST 

Pollsters have been predicting disillusioned leftist voters are particularly likely to abstain. Sarkozy’s Surveillance Scandal |Tracy McNicoll |March 22, 2014 |DAILY BEAST 

Users then have the option to vote for or against each law, or simply abstain. Argentina’s Drag & Drop Democracy |Jeff Campagna |March 12, 2014 |DAILY BEAST 

It is not the right of any one, according to his pleasure, to abstain from entering into Covenant with God. The Ordinance of Covenanting |John Cunningham 

Let it be put forth in leading to abstain from countenancing an evil constitution, and to raise above the fear of consequences. The Ordinance of Covenanting |John Cunningham 

So the campers obtained fresh meat, and all were very glad to abstain awhile from bacon. Gold-Seeking on the Dalton Trail |Arthur R. Thompson 

It is therefore obligatory upon us to abstain as far as possible from referring to expressions that are calculated to alarm. A Cursory History of Swearing |Julian Sharman 

In concluding this notice of Roe, I cannot refrain from expressing a hope that gentlemen will abstain from the use of it. The Teesdale Angler |R Lakeland