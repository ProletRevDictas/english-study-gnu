Their statement follows an investigation by Byline Investigates into how the Sun tabloid hired an American private investigator, who says he unlawfully handed over personal details about the duchess when she first started dating Harry. Private investigator says he skirted laws to get info on Meghan Markle, sell it to the Sun tabloid |Karla Adam |March 19, 2021 |Washington Post 

The duchess is investing in Clevr Blends, a startup that makes instant oat-milk lattes. Exclusive: Meghan, The Duchess of Sussex, makes her debut as a startup investor |ehinchliffe |December 14, 2020 |Fortune 

A representative for the duchess declined to disclose the size of her investment, which wasn’t part of any kind of larger funding round. Exclusive: Meghan, The Duchess of Sussex, makes her debut as a startup investor |ehinchliffe |December 14, 2020 |Fortune 

The duchess joined the Fortune Most Powerful Women Summit and MPW Next Gen Summit this fall to discuss these issues. Exclusive: Meghan, The Duchess of Sussex, makes her debut as a startup investor |ehinchliffe |December 14, 2020 |Fortune 

Over the past four years, the actor turned duchess turned activist has been the subject of fascination—and extreme scrutiny—across the Internet. Meghan, The Duchess of Sussex: ‘If you listen to what I actually say, it’s not controversial’ |ehinchliffe |September 29, 2020 |Fortune 

The documentary also follows the fortunes of Consuelo Yznaga, later Duchess of Manchester. The Real-Life ‘Downton’ Millionairesses Who Changed Britain |Tim Teeman |December 31, 2014 |DAILY BEAST 

He called her The Duchess, and if The Duchess didn't like something, then it was of no value. Alfred Hitchcock’s Fade to Black: The Great Director’s Final Days |David Freeman |December 13, 2014 |DAILY BEAST 

The Barclays Center where the Duke and Duchess will be seated would have stood in thick of where the pivotal action transpired. The British Royals Reinvade Brooklyn: William and Kate Come Watch Basketball on Historic Battle Site |Justin Jones |December 6, 2014 |DAILY BEAST 

In an interview with Vanity Fair, Diez said he married the duchess for love, not money. Adiós to the Diva Duchess |Barbie Latza Nadeau |November 20, 2014 |DAILY BEAST 

A flamboyant, multi-titled, multiply married royal to remember, the Duchess of Alba died Thursday at the age of 88. Adiós to the Diva Duchess |Barbie Latza Nadeau |November 20, 2014 |DAILY BEAST 

The Duchess had also a tent for their sick men; so that we had a small town of our own here, and every body employed. The Life and Most Surprising Adventures of Robinson Crusoe, of York, Mariner (1801) |Daniel Defoe 

Aristide uncovered his head, as though he were about to address a duchess, and smiled at her engagingly. The Joyous Adventures of Aristide Pujol |William J. Locke 

A view of the duchess's ball-room, or of the dining-table of the earl, will supersede all occasion for lengthy fiddle-faddle. Blackwood's Edinburgh Magazine, No. CCCXXXIX. January, 1844. Vol. LV. |Various 

They then remembered what the Duchess had told them of these beautiful and dangerous waters where the nixies dwell. Honey-Bee |Anatole France 

That morning, it was the first Sunday after Easter, the Duchess rode out of the castle on her great sorrel horse, while on? Honey-Bee |Anatole France