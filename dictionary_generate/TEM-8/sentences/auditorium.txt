About an hour later, inside an auditorium underneath the Capitol, Republicans lined up behind microphones to air their views on Cheney — kicking off a meeting that would drag on for hours. McCarthy moves to keep splintering GOP intact, with protection for both Cheney and Greene |Mike DeBonis, Paul Kane |February 4, 2021 |Washington Post 

Dean watched workers load the cargo from the Soviet boat onto a train, and asked his colleague if they could somehow grab it during its journey to the auditorium. Lunik: Inside the CIA’s audacious plot to steal a Soviet satellite |Bobbie Johnson |January 28, 2021 |MIT Technology Review 

The next day, anti-government activist Ammon Bundy and two others were arrested when they refused to leave an auditorium in the Statehouse and another man was arrested when he refused to leave a press area. “Sense of Entitlement”: Rioters Faced Few Consequences Invading State Capitols. No Wonder They Turned to the U.S. Capitol Next. |by Jeremy Kohler |January 19, 2021 |ProPublica 

The crowd numbered about 200, filling only a quarter of the auditorium. Remembering the most scandalous film ever (not) shown in Washington |John Kelly |January 10, 2021 |Washington Post 

I’m not sure which book he was reading from when I first met Barry in 1978, as we walked together out of the auditorium at the University of Missouri, where I was in grad school. Remembering My Friend Barry Lopez |Bob Shacochis |January 5, 2021 |Outside Online 

Your first big splash was in Cape Fear, and that auditorium scene between you and De Niro is beyond creepy to this day. Juliette Lewis on Hollywood, Why the MSM Hates Scientology, and Masturbating to George Clooney |Marlow Stern |September 19, 2014 |DAILY BEAST 

A local Dad joked sotto voce to his wife “And afterwards there will be a reception who will be in the auditorium!” The Problem With Weird Al’s ‘Word Crimes’ Video |John McWhorter |July 23, 2014 |DAILY BEAST 

Being a true fan in a convention center, a stadium, an auditorium—that takes dedication, time and money. How ‘Battlestar Galactica,’ ‘Game of Thrones,’ and FanFiction Conquered Pop Culture |Arthur Chu |May 6, 2014 |DAILY BEAST 

There will be 15 auditorium style classrooms, each with a capacity for 700 students. Italy Plans a Disneyland for Foodies |Barbie Latza Nadeau |March 16, 2014 |DAILY BEAST 

Yet at my own institute every science program we put on fills up a 3,000-person auditorium with paying members of the public. The New 'Cosmos' Reboot Marks a Promising New Era for Science |Lawrence M. Krauss |March 10, 2014 |DAILY BEAST 

The fanfare sounds again, the buzz of conversation is stilled, the lights turned down, and darkness reigns in the auditorium. The Life &amp; Letters of Peter Ilich Tchaikovsky |Modeste Tchaikovsky 

In 1883 the interior was entirely remodeled and stained windows put in, thus making a handsome auditorium. The Bay State Monthly, Vol. II, No. 6, March, 1885 |Various 

The stage is of good size and well-appointed and the auditorium neat and attractive. The Bay State Monthly, Vol. II, No. 6, March, 1885 |Various 

And they were delayed in realizing it by a diversion from the other side of the auditorium. Sense from Thought Divide |Mark Irvin Clifton 

The point between us was whether Miss Euclid's speeches ought to be clearly audible in the auditorium. The Regent |E. Arnold Bennett