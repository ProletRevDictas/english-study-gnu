Users say they just want to be able to continue creating and watching video clips featuring users lip synching, dancing, and doing short comedy skits. What’s Oracle? TikTok users react to proposed Oracle deal |Danielle Abril |September 15, 2020 |Fortune 

Comedy Central’s The Daily Show, in creating comedy content remotely. How the coronavirus outbreak is roiling the film and entertainment industries |Alissa Wilkinson |September 11, 2020 |Vox 

It’s been a mission and an adventure, it’s almost like a comedy. ‘It can take on a panopticon effect’: Slack’s presenteeism problem grows with no end in sight for remote work |Lucinda Southern |August 28, 2020 |Digiday 

It was sort of a comedy of errors, a series of mishaps, just poor medical advice, and obviously I accept responsibility. Could Grant Hill Have Been Better Than Michael Jordan? |Joshua Eferighe |August 20, 2020 |Ozy 

The neighborhood is bordered by tree-lined Halsted Street on the north side of the city and is home to comedy clubs, theaters, and a young and vibrant nightlife. Chicago: A Midwestern Jewel for the LGBTQ Community |LGBTQ-Editor |July 11, 2020 |No Straight News 

This is comedy based on a cold humor, detached, euphemistic, devoid of any generosity. Houellebecq’s Incendiary Novel Imagines France With a Muslim President |Pierre Assouline |January 9, 2015 |DAILY BEAST 

Was that a transitional moment for you from music to comedy? Coffee Talk with Fred Armisen: On ‘Portlandia,’ Meeting Obama, and Taylor Swift’s Greatness |Marlow Stern |January 7, 2015 |DAILY BEAST 

You write a lot about how you were a jerk or a snob when it came to comedy or film. Patton Oswalt on Fighting Conservatives With Satire |William O’Connor |January 6, 2015 |DAILY BEAST 

Decades ago, the writer-director wrote an episode of the animated comedy that never was. Here’s the Lost Judd Apatow ‘Simpsons’ Episode, Penned by Judd Apatow |Asawin Suebsaeng |January 6, 2015 |DAILY BEAST 

The memoir follows Oswalt from 1995 to 1999 as he was starting out on his comedy career in Los Angeles. Patton Oswalt on Fighting Conservatives With Satire |William O’Connor |January 6, 2015 |DAILY BEAST 

Did he at all intrench upon your Sovereignty in Verse, because he had now and then written a Comedy that succeeded? A Letter from Mr. Cibber to Mr. Pope |Colley Cibber 

The play may be pure comedy, comedy-drama, tragedyeven farceor melodrama. The Girls of Central High on the Stage |Gertrude W. Morrison 

She laughed at the comedy and wept—she and the gaudy woman next to her wept over the tragedy. The Awakening and Selected Short Stories |Kate Chopin 

It was a comedy on both sides, but it remained a comedy so long as those papers were not forthcoming. The Weight of the Crown |Fred M. White 

I could not avoid meeting you, and I felt I could not play a comedy of lies as to my reason for not being able to go to Kamenka. The Life &amp; Letters of Peter Ilich Tchaikovsky |Modeste Tchaikovsky