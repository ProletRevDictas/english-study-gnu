I disliked his laugh because I knew it was forced, but I nodded gaily and asked him where he was going. Read ‘The King in Yellow,’ the ‘True Detective’ Reference That’s the Key to the Show |Robert W. Chambers |February 20, 2014 |DAILY BEAST 

At a meeting with Netanyahu, Rubio gaily mocked himself by toasting the Israeli leader with one of his famous water bottles. Marco Rubio Really Loves Israel and Has Pictures to Prove It |Eli Lake |February 21, 2013 |DAILY BEAST 

On another wall was hung a gaily colored quilt made by Harris. Scarsdale Doctor Killer Jean Harris Found Redemption in Prison |Michael Daly |December 31, 2012 |DAILY BEAST 

And at Easter, Americans dunk them in dye and buy pounds of their chocolate likenesses wrapped in gaily colored foil. Cracking the Code on All Things Egg |Jessica Konopa |September 15, 2009 |DAILY BEAST 

There was a time when Aristide Pujol, in sole charge of an automobile, went gaily scuttering over the roads of France. The Joyous Adventures of Aristide Pujol |William J. Locke 

And, Pedro preceding them, the young men gaily descended the stairs. Blackwood's Edinburgh Magazine, Volume 60, No. 372, October 1846 |Various 

The pair disappeared among the glittering and gaily-dressed crowd that thronged the portico. The Pit Town Coronet, Volume I (of 3) |Charles James Wills 

He had been momentarily forgetting care; was speaking gaily to his wife as they entered. Elster's Folly |Mrs. Henry Wood 

His hair relieved this somewhat, for it was white and always stood gaily on end, defying brush and comb. The Soldier of the Valley |Nelson Lloyd