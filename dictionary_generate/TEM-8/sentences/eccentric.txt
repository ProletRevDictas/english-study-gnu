He’s been around long enough to know that life is much more eccentric than he is. When Dead Beer Walks: A Dane in Vietnam Turns Strange Brew Into Craft Gin |Eugene Robinson |February 12, 2021 |Ozy 

Boston Dynamics, the company best known for eccentric videos of animal-like machines, has upgraded its classic robotic dog Spot. Boston Dynamics adds an ‘arm’ to its robotic dog Spot |Dalvin Brown |February 4, 2021 |Washington Post 

They bonded in their love of the Bills and of the eccentric. ‘Bills Mafia’ waited a generation for a team like this. It’s had to embrace it from afar. |Adam Kilgore |January 7, 2021 |Washington Post 

While a planet on a circular orbit moves at a constant speed around the star, planets on eccentric orbits move faster when they are closer to the star. We’re the Cosmic 1 Percent But Our Solar System Isn’t a Complete Weirdo - Facts So Romantic |Sean Raymond |January 5, 2021 |Nautilus 

Soon after, the eccentric and apparently sleepless billionaire tweeted a series of other messages about Bitcoin. Elon Musk trolls Bitcoin, causes novelty Dogecoin to soar |Jeff |December 21, 2020 |Fortune 

Sometimes I wear my silk pyjamas when I am going for a walk in the mornings, does that make me eccentric? The Death of the English Eccentric |Tom Sykes |November 25, 2014 |DAILY BEAST 

She was one of the wealthiest women in the world and certainly the most eccentric noble of her time. Adiós to the Diva Duchess |Barbie Latza Nadeau |November 20, 2014 |DAILY BEAST 

One of my most important mentors was a brilliant and eccentric rabbi from Bethesda, Maryland. Fixing a Dysfunctional Family: Congress |Gene Robinson |November 9, 2014 |DAILY BEAST 

To his peers, he's an all-star eccentric who is pitied or clucked over protectively as often as he is envied. Will the Real Jim Palmer Please Stand Up |Tom Boswell |September 27, 2014 |DAILY BEAST 

The women Peterson photographed were offbeat, eccentric, irreverent, and not conventionally pretty. Gosta Peterson's Bohemian Rhapsody: Unpacking a Photographer's '60s Secrets |Lizzie Crocker |September 10, 2014 |DAILY BEAST 

Lorenzo Dow is still remembered by some of the "old fogies" as one of the most eccentric men that ever lived. The Book of Anecdotes and Budget of Fun; |Various 

From all the accounts that have come down to us, he seems to have been a man of irregular habits and eccentric genius. Violins and Violin Makers |Joseph Pearce 

John N. Maffit, the well known and eccentric methodist preacher, died at Mobile. The Every Day Book of History and Chronology |Joel Munsell 

He is simply an eccentric Scot, who does not see why he should pay for crossing a river that he can cross for nothing. Friend Mac Donald |Max O'Rell 

But the man inside these voluminous clothes is even still more eccentric. The Real Latin Quarter |F. Berkeley Smith