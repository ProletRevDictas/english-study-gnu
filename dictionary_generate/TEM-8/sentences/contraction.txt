The economy’s deep contraction was heavily driven by services. A country that escaped a recession in 2008 is officially in one now |Claire Zillman, reporter |September 2, 2020 |Fortune 

When it comes to visualizing expansion and contraction, people often focus on a balloonlike universe whose change in size is described by a “scale factor.” Big Bounce Simulations Challenge the Big Bang |Charlie Wood |August 4, 2020 |Quanta Magazine 

In the cyclic universe, however, the smoothing happens during a period of contraction. Big Bounce Simulations Challenge the Big Bang |Charlie Wood |August 4, 2020 |Quanta Magazine 

The varying rates of contraction will be most extreme in countries like Thailand, Japan and Spain along with 20 others, where declines could see their populations halved by 2100, a new Lancet report on fertility and population growth scenarios shows. Africa’s population will triple by the end of the century even as the rest of the world shrinks |Yomi Kazeem |July 16, 2020 |Quartz 

The expected population contraction will be due to dropping fertility rates with death rates being either at par with or faster than birth rates in several countries. Africa’s population will triple by the end of the century even as the rest of the world shrinks |Yomi Kazeem |July 16, 2020 |Quartz 

Side effects may include recession, job contraction, 401(k) bruising, recurrent Dow fluctuation, and IRA bleeding. Up to a Point: PJ O’Rourke on Sochi and Senate Slackers |P. J. O’Rourke |February 7, 2014 |DAILY BEAST 

“AOL had a history of turmoil—rapid expansion and then rapid contraction,” Bewkes says. Is Tim Armstrong the Lazarus of Aol.? |Lloyd Grove |December 23, 2013 |DAILY BEAST 

That would place the country in recession, typically defined as two consecutive quarters of economic contraction... The British Economy Appears to be Tanking (Again) |Ilana Glazer |April 24, 2013 |DAILY BEAST 

The resulting credit contraction would be terrible news for the Italian economy. Euro Crisis: Reheated |Megan McArdle |February 27, 2013 |DAILY BEAST 

Darwin considered that this protective contraction “was a fundamental element in several of our most important expressions.” Why Do We Cry? |Michael Trimble |January 10, 2013 |DAILY BEAST 

In fact, incredibly faster, after his once-a-century contraction of short years before. Old Friends Are the Best |Jack Sharkey 

I take iowell (with a bar through the ll) to be the usual (Northern) contraction for Iowellis, jewels; F. text, joiau, pl. Chaucer's Works, Volume 1 (of 7) -- Romaunt of the Rose; Minor Poems |Geoffrey Chaucer 

The nick-name of Gigonnet was applied to Bidault on account of a feverish, involuntary contraction of a leg muscle. Repertory Of The Comedie Humaine, Complete, A -- Z |Anatole Cerfberr and Jules Franois Christophe 

Expansion and contraction broke the high arch and the connexions between the arches. Encyclopaedia Britannica, 11th Edition, Volume 4, Part 3 |Various 

Her lowered eyelids had that vague contraction which suggests a tear checked in its course, or a thought suppressed. Toilers of the Sea |Victor Hugo