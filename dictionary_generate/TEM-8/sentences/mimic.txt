Like remdesivir, favipiravir works by mimicking a building block of the virus’s genetic material, RNA. New treatments aim to treat COVID-19 early, before it gets serious |Tina Hesman Saey |August 24, 2020 |Science News 

It also focuses more on content about staycations, or recipes one can cook to mimic being someplace new. ”Pivot” has been the word’: How travel publishers are navigating the coronavirus pandemic |Max Willens |August 20, 2020 |Digiday 

That observing setup mimicked the way astronomers plan to probe the atmospheres of Earthlike exoplanets as they pass in front of their stars, filtering out some starlight. Hubble watched a lunar eclipse to see Earth from an alien’s perspective |Maria Temming |August 17, 2020 |Science News 

Next, clinicians should look for other explanations, conditions that could mimic brain death but are actually reversible. New guidance on brain death could ease debate over when life ends |Laura Sanders |August 10, 2020 |Science News 

The multimedia brand mimics the experience of Wolfe Pereira and his co-founders, all of whom grew up in multicultural households. Your Kids Need a Dose of Bilingual Culture? He Can Help |Nick Fouriezos |August 9, 2020 |Ozy 

When my hair gets long enough I kid myself I can mimic the glorious tumbling fringe of “the Rachel” sometimes. Angelina Jolie and Brad Pitt Got Married and We’re Worried About Jennifer Aniston |Kevin Fallon, Tim Teeman |August 28, 2014 |DAILY BEAST 

But under what moral principle must a nation mimic both the madness and the misdirection of its enemy? Numbers Don’t Tell the Mideast Story |Thane Rosenbaum |July 10, 2014 |DAILY BEAST 

The results: Even moderate MDMA doses in conditions that mimic hot, crowded, social settings could be lethal to rats. Why Molly Is Especially Deadly at Summer Music Festivals |Abby Haglage |June 7, 2014 |DAILY BEAST 

The team designed over 40 themed soundscapes that mimic environments, all of which are free to download. New Study Shows Dream App Helps People Craft Dreams and Wake Up Happier |Mihir Patkar |April 6, 2014 |DAILY BEAST 

He slowed down the action at times for effect; he jolted the camera to mimic the jittery imperfection of a documentary. WWII Lies of Hollywood's Greats |Caryn James |February 22, 2014 |DAILY BEAST 

For others life is but a foolish leisure with mock activities and mimic avocations to mask its uselessness. The Unsolved Riddle of Social Justice |Stephen Leacock 

Very often the little ones mimic it in fun, and children's games, most times, are copies of their elders' workaday doings. Child Life In Town And Country |Anatole France 

Samuel cried at the loss of his pretty kite, and Charles Duran was mean enough to mimic the boy whom he had thus injured. Charles Duran |The Author of The Waldos 

That monarch, easily the first comedian of his time, allowed no rivals on the mimic stage, and it languished during his reign. The Stones of Paris in History and Letters, Volume I (of 2) |Benjamin Ellis Martin 

It was almost as if, for a brief interval, the mimic was the scholar, though always with the drop of ridicule or mischief added. Adventures and Enthusiasms |E. V. Lucas