For one thing, “computer proofs may not be as alien as we think,” DeDeo said. How Close Are Computers to Automating Mathematical Reasoning? |Stephen Ornes |August 27, 2020 |Quanta Magazine 

Astrobiologists now consider it one of the most likely places to search for alien life in the solar system. These Images Expose the Dark Side of the Solar System - Issue 89: The Dark Side |Corey S. Powell |August 26, 2020 |Nautilus 

“We basically pretend we’re alien observers looking at our planet,” says Giada Arney, a planetary scientist at NASA’s Goddard Space Flight Center in Greenbelt, Md. Hubble watched a lunar eclipse to see Earth from an alien’s perspective |Maria Temming |August 17, 2020 |Science News 

If the aliens ever show up at Earth they won’t ask to be taken to our leader, they’ll ask for a cheek swab and a consent form. How Life Could Continue to Evolve - Issue 88: Love & Sex |Caleb Scharf |August 12, 2020 |Nautilus 

In interplanetary texts, humans of all nations, races, and genders have to come together as one people in the face of alien invasions. Science Fiction Explores the Interconnectedness Revealed by the Coronavirus Pandemic |Mayurika Chakravorty |August 9, 2020 |Singularity Hub 

Capaldi said the nature of the character—a time-travelling alien—meant his successor could take any form. Doctor Who: It’s Time For a Black, Asian, or Woman Doctor |Nico Hines |December 11, 2014 |DAILY BEAST 

The idea that I might simply want to express my independent thoughts was alien to them. What It’s Like to Be an Atheist in Palestine |Waleed al-Husseini, Movements.Org |December 8, 2014 |DAILY BEAST 

A limited number of medical patients will be able to purchase “Chunky Diesel” or “Alien Dawg” for just $100. Colorado Weed Dispensaries Celebrate ‘Green Friday’ |Abby Haglage |November 28, 2014 |DAILY BEAST 

“They treated me like an alien,” she says, explaining that North Koreans are viciously stereotyped in South Korea. How ‘Titanic ’Helped This Brave Young Woman Escape North Korea’s Totalitarian State |Lizzie Crocker |October 31, 2014 |DAILY BEAST 

Last but not least, the Inhumans are a genetically advanced “inhuman race” that was formed via experimentation by the alien Kree. Inside Marvel’s Phase 3: How ‘The Avengers’ Cross Paths with Black Panther and the New Superheroes |Marlow Stern |October 30, 2014 |DAILY BEAST 

The member banks should look upon the reserve bank not as an alien but as their own institution. Readings in Money and Banking |Chester Arthur Phillips 

An American woman therefore who marries an alien takes the nationality of her husband. Putnam's Handy Law Book for the Layman |Albert Sidney Bolles 

In 1855 Congress passed an act conferring citizenship on alien women who should marry American citizens. Putnam's Handy Law Book for the Layman |Albert Sidney Bolles 

A deep melancholy settled down upon the mind of the wounded girl, and she felt that she was desolate and an alien in her own home. Madame Roland, Makers of History |John S. C. Abbott 

A dependent who is an alien living in a foreign country is not debarred from receiving compensation. Putnam's Handy Law Book for the Layman |Albert Sidney Bolles