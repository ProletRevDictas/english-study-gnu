The Streambar includes all necessary cables as well as a dedicated Roku remote control, and an optional dedicated subwoofer bundle is also available for those who want to push their sound even further. The best, most practical Valentine’s Day gifts for any kind of partner |PopSci Commerce Team |February 8, 2021 |Popular-Science 

Each hut accommodates 12 riders and has a grill, an e-bike charging station, and optional beer delivery. Everything You Need to Know About Travel in 2021 |The Editors |February 1, 2021 |Outside Online 

In December, though, Borden told KPBS the union would negotiate to keep teachers who declined a vaccine employed, and she this month signaled her support for keeping them optional during a hearing before a state Senate education hearing. Morning Report: How Vaccines and New Rules Impact School Reopenings |Voice of San Diego |January 27, 2021 |Voice of San Diego 

However, as masks are optional, you may not want to accept anyway. Miss Manners: Bossy invitation offends guests |Judith Martin, Nicholas Martin, Jacobina Martin |January 20, 2021 |Washington Post 

Even before the pandemic, the subject tests and the optional essay were losing influence. College Board is scrapping SAT’s optional essay and subject tests |Nick Anderson |January 19, 2021 |Washington Post 

Sure, taking sexy pictures—like having sex itself—is, technically speaking, optional. In Defense of Nude Selfies: Don’t Shame ‘The Fappening’ Victims |Amanda Marcotte |September 23, 2014 |DAILY BEAST 

Van Zant was echoing a growing sentiment of paranoia and fear sparked by the optional public education standards program. Fringe Factor: The Common Core Will Turn Your Kids as Gay as Possible |Caitlin Dickson |May 25, 2014 |DAILY BEAST 

When I ask why she wears a veil in a society where it's optional, Sakdiyah explains how it frees her. Young Muslim Comic Takes On Fundamentalists |Moral Courage |May 19, 2014 |DAILY BEAST 

Preparing for retirement is “the why of politics, not merely optional dirty work.” Marco Rubio’s Climate Change Doublespeak |Eleanor Clift |May 14, 2014 |DAILY BEAST 

Then you may be delighted to learn that the essay portion of the SAT is now optional. Insufferable Elitism of the SATs |James Poulos |March 8, 2014 |DAILY BEAST 

On nearly all railways retirement is optional at sixty and compulsory at sixty-five. Fifty Years of Railway Life in England, Scotland and Ireland |Joseph Tatlow 

Local school and capitation taxes were optional with each county and public school district. Hallowed Heritage: The Life of Virginia |Dorothy M. Torpey 

After they have a school certificate, entrance to the works is optional. A Journey Through France in War Time |Joseph G. Butler, Jr. 

It is entirely optional; you have only to take it quietly or go to jail. A Confederate Girl's Diary |Sarah Margan Dawson 

Civil engineering and political economy are the only optional studies with the women. The College, the Market, and the Court |Caroline H. Dall