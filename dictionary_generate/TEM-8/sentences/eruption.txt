Palladino, she said, was being used to address “bimbo eruptions,” and had been successful at keeping a number of the alleged scandals out of print. Jack Palladino, aggressive sleuth who worked for Bill Clinton campaign, dies at 76 |Harrison Smith |February 2, 2021 |Washington Post 

The heat eventually escaped through radiation and volcanic eruptions. Rumbles on Mars Raise Hopes of Underground Magma Flows |Robin George Andrews |February 1, 2021 |Quanta Magazine 

Those eruptions release carbon dioxide to the atmosphere and produce fresh new rock for weathering. How the Earth-shaking theory of plate tectonics was born |Carolyn Gramling |January 13, 2021 |Science News 

Steamboat also seemed to show a seasonal eruption cycle, bursting forth more often during the summer than in the winter. Reawakened Yellowstone geyser isn’t a sign of imminent explosion |Carolyn Gramling |January 11, 2021 |Science News 

In terms of sheer number of aerosols sent into the stratosphere, the Australian plumes were on par with the strongest volcanic eruptions in the last 25 years. Towering fire-fueled thunderclouds can spew as many aerosols as volcanic eruptions |Carolyn Gramling |December 15, 2020 |Science News 

The rage that Marvin has embodied, a man on the edge of eruption, is always a badly wounded man. The Story Behind Lee Marvin’s Liberty Valance Smile |Robert Ward |January 3, 2015 |DAILY BEAST 

The animals are supposedly evacuating themselves in anticipation of an eruption at the park, which sits on a huge volcanic system. Turns Out, a Video of Bison Purportedly Fleeing Yellowstone Is a Hoax |Timothy Lesle |April 8, 2014 |DAILY BEAST 

That video of bison fleeing Yellowstone in anticipation of a volcanic eruption? Turns Out, a Video of Bison Purportedly Fleeing Yellowstone Is a Hoax |Timothy Lesle |April 8, 2014 |DAILY BEAST 

His controversial tweet marks yet another controversial eruption by a Republican congressman around an Obama speech to Congress. GOP Congressman Calls Obama 'Kommandant' in Chief |Ben Jacobs |January 29, 2014 |DAILY BEAST 

His views are not shared by Vivien Magdy, a 25-year-old who lost her fiancé during another notorious eruption of violence. Did Egypt's Arab Spring Martyrs Die in Vain? |Alastair Beach |January 26, 2014 |DAILY BEAST 

An extraordinary eruption of mount Vesuvius commenced, which in ten days had advanced ten miles from its original source. The Every Day Book of History and Chronology |Joel Munsell 

An eruption of mount tna, which extended its ravages four leagues around, and buried several persons alive. The Every Day Book of History and Chronology |Joel Munsell 

A long, long time ago, the boy's father cannot tell how many years have passed, there was a terrible eruption. Alila, Our Little Philippine Cousin |Mary Hazelton Wade 

Great eruption of the Scaptar Jokul, in Iceland, commenced, and continued several days (see 18th). The Every Day Book of History and Chronology |Joel Munsell 

No eruptive mountains, nor any traces of recent volcanic eruption, have yet been observed in any part of Australia. Narrative of a Survey of the Intertropical and Western Coasts of Australia] [Volume 2 of 2] |Phillip Parker King