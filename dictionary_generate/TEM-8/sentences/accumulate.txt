A lot of restaurants are accumulating debt via loans or depleting whatever funds they have left, the hole is going to be too big to get out of which will result in many more restaurant doors closing. Is the Government Just Going to Watch the Restaurant Industry Die? |Elazar Sontag |August 28, 2020 |Eater 

Some sought thousands of dollars in unpaid rent that had accumulated over months. She Was Sued Over Rent She Didn’t Owe. It Took Seven Court Dates to Prove She Was Right. |by Danielle Ohl, Capital Gazette, and Talia Buford and Beena Raghavendran, ProPublica |August 25, 2020 |ProPublica 

To learn how much ice is accumulated and how much of it slides off the continent, scientists set up special survey stakes…. 50 years ago, scientists clocked the speed of Antarctic ice |Maria Temming |August 20, 2020 |Science News 

Officers who accumulate such complaints should be watched closely, re-trained or encouraged to seek other employment. An Officer’s Word Shouldn’t Be the Last Word |Andrew Taylor |August 19, 2020 |Voice of San Diego 

Amazon and Roku have each accumulated user bases of more than 40 million households, and Roku has built up its platform business to the point that it makes more money from selling ads and subscriptions than from selling devices. Shut out of Fire TV and Roku, Peacock is the latest example of the arrival power moves to streaming |Tim Peterson |July 15, 2020 |Digiday 

Real understanding and actual truth accumulate more insidiously. Alfred Hitchcock’s Fade to Black: The Great Director’s Final Days |David Freeman |December 13, 2014 |DAILY BEAST 

Bolstered by the momentum of Savage, Masters continued to accumulate up-and-coming conservative talent. The Godfather of Right-Wing Radio |Caitlin Dickson |November 23, 2014 |DAILY BEAST 

This means that their gene pools stagnate and accumulate increasingly harmful mutations. Our Taste for Cheap Palm Oil Is Killing Chimpanzees |Carrie Arnold |July 11, 2014 |DAILY BEAST 

To accumulate wealth so fast and on such a scale, it is necessary to eliminate independent law enforcement. Ukraine’s Revolutionary Lesson for Russia |David Satter |March 2, 2014 |DAILY BEAST 

Until early November it was very easy to accumulate a large amount of money by selling, and re-selling highly modified cars. My Secret Life in Los Santos |Eli Lake |November 29, 2013 |DAILY BEAST 

These were frequently buried beneath deposits of stalagmite and other materials that must have taken a long time to accumulate. Man And His Ancestor |Charles Morris 

Legends accumulate here around the persons of Arthur and his knights. The Towns of Roman Britain |James Oliver Bevan 

The weather has turned to rain again, and the country is losing the snow, whilst the trenches accumulate the rain and mud badly. Letters of Lt.-Col. George Brenton Laurie |George Brenton Laurie 

What a vocabulary one could accumulate, if from six to eighteen he added only two words a day! English: Composition and Literature |W. F. (William Franklin) Webster 

To accumulate as quickly as possible the amount of money needed to enable us to lead an idle life. The Seven Cardinal Sins: Envy and Indolence |Eugne Sue