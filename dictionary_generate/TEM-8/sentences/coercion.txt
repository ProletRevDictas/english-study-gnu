Food in Nigeria has long been weaponized, withheld, and used as a means of corruption and coercion by those in power. Feeding a New Nigeria |Nelson C.J. |January 22, 2021 |Eater 

The complaint alleges that law enforcement deprived Alamance County voters “of their fundamental right to vote free from intimidation, harassment, threats, or other forms of coercion.” Pistols, a Hearse and Trucks Playing Chicken: Why Some Voters Felt Harassed and Intimidated at the Polls |by Adriana Gallardo, Maryam Jameel and Ryan McCarthy |December 4, 2020 |ProPublica 

I started hearing about rampant sexual harassment, coercion and abuse of undocumented immigrant women in low-wage, temporary factory jobs. Temp Workers Fight Back Against Alleged Sexual Harassment and Say They Face Retaliation for Doing So |by Melissa Sanchez |August 28, 2020 |ProPublica 

In the broadest sense, at home and internationally, the US is moving towards coercion and the exercise of hard power, and away from its previous strategies based on soft power and international leadership. George Floyd Protests Show How The US Has Retreated From Its Position As A World Leader |LGBTQ-Editor |June 9, 2020 |No Straight News 

In my work on wrongful conviction cases in Philadelphia, I regularly encounter patterns of police misconduct including witness intimidation, evidence tampering and coercion. Police Officers Accused Of Brutal Violence Often Have A History Of Complaints By Citizens |LGBTQ-Editor |June 1, 2020 |No Straight News 

Many out athletes found their voices silenced by coercion contracts many of their home countries gave them. ‘To Russia With Love’: Can Johnny Weir Save Russia’s Gays? |Kevin Fallon |October 29, 2014 |DAILY BEAST 

That kind of government coercion of speech and action seems a-OK to the conservative liberty crowd. Refusing to Marry Same-Sex Couples Isn’t Religious Freedom, It’s Just Discrimination |Sally Kohn |October 23, 2014 |DAILY BEAST 

Help with onerous conditions is not help so much as benevolent coercion. America’s Meddlers Are Our Worst Enemies |Stefan Beck |October 3, 2014 |DAILY BEAST 

We are watching an invasion using subversion, coercion, and somewhat limited military action. Is the U.S. Enabling Putin's Invasion? |Christopher Dickey |August 29, 2014 |DAILY BEAST 

In hindsight, this was probably a mistake because it opened the door to legitimate charges of federal coercion. The Incredibly Stupid War on the Common Core |Charles Upton Sahm |April 21, 2014 |DAILY BEAST 

When together the law presumes she acted from his coercion, he therefore must be the sufferer, while she escapes. Putnam's Handy Law Book for the Layman |Albert Sidney Bolles 

Neither did Virginia believe in the national policy of coercion of a state to return to the Union. Hallowed Heritage: The Life of Virginia |Dorothy M. Torpey 

The discussions on the coercion act had produced many personal conflicts in debate between Mr. O'Connell and the Irish secretary. The History of England in Three Volumes, Vol.III. |E. Farr and E. H. Nolan 

He argued that coercion was necessary; that crime could not be put down in Ireland but by the strong arm of the law. The History of England in Three Volumes, Vol.III. |E. Farr and E. H. Nolan 

Next session the government brought in a Coercion Bill, which Mr. Parnell opposed vigorously. Great Men and Famous Women. Vol. 4 of 8 |Various