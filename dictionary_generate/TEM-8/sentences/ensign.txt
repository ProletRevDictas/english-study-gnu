As a result, throughout the nation men returned to campuses, and universities and colleges began to grind out ensigns and lieutenants in the thousands. This isn’t the first time Army played Navy in a moment of crisis |Randy Roberts |December 11, 2020 |Washington Post 

Hello, Sen. David Vitter, John Ensign, Larry Craig, and so on. Marianne Gingrich Interview Casts Doubts on Newt’s New Image |Margaret Carlson |January 20, 2012 |DAILY BEAST 

South Gov. Mark Sanford, Sen. John Ensign, Rep. Anthony Weiner—all did their pressers of shame without their better halves. Why Gloria Cain Is Silent |Michelle Cottle |November 4, 2011 |DAILY BEAST 

Ensign resigned from the Senate the day before he was going to testify under oath about the scandal. Don’t Lionize Coburn Yet |Katherine Spillar |August 6, 2011 |DAILY BEAST 

ALLEGED OFFENDERS: John Edwards, John Ensign, James McGreevey. Eight Secrets to Avoiding a Sex Scandal |David A. Graham |May 27, 2011 |DAILY BEAST 

ALLEGED OFFENDERS: Bill Clinton, former Sen. John Ensign, California Lt. Gov. Gavin Newsom, former Sen. Larry Craig. Eight Secrets to Avoiding a Sex Scandal |David A. Graham |May 27, 2011 |DAILY BEAST 

A golden eagle, the armorial ensign of the Ripperda family, crested the centre arch. The Pastor's Fire-side Vol. 3 of 4 |Jane Porter 

He took part in a number of campaigns and received his first commission, that of Ensign, in 1890. A Brief History of the U. S. S. Imperator, one of the two Largest Ships in the U. S. Navy. |Anonymous 

The Hope lay safely moored, with her ensign at the peak, and flying the distinguished flag of the firm. Skipper Worse |Alexander Lange Kielland 

The flag of the club is the blue ensign of Her Majesty's fleet, with a gold crown and Maltese cross. Yachting Vol. 2 |Various. 

Our vessels paid him the more modest compliment of one royal salute, and hoisting the French imperial ensign. The British Expedition to the Crimea |William Howard Russell