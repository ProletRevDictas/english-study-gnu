“This is a political verdict,” the father of nineteen-year-old Ahmed Khaleefa, who was killed in 2011, told Reuters. Mubarak’s Acquittal Signals Complete Triumph of Military Over Arab Spring |Jamie Dettmer |November 29, 2014 |DAILY BEAST 

"My wife and I have been married for nineteen years," says Palmer, mulling the stress-fracture in his family life. Will the Real Jim Palmer Please Stand Up |Tom Boswell |September 27, 2014 |DAILY BEAST 

Here I am, played here for nineteen years, I'm the one who stayed. Will the Real Jim Palmer Please Stand Up |Tom Boswell |September 27, 2014 |DAILY BEAST 

He is getting ready to watch Jack Morris, the Tigers ace, go for win number nineteen against the Toronto Blue Jays. Elmore Leonard’s Rocky Road to Fame and Fortune |Mike Lupica |September 13, 2014 |DAILY BEAST 

He had been decorated for bravery nineteen times in his twenty-one-year career. The Resilient City: New York After 9/11 |John Avlon |September 11, 2014 |DAILY BEAST 

No historian claims that any God has been seen on earth for nearly nineteen centuries. God and my Neighbour |Robert Blatchford 

He began work at the early age of thirteen, had grown up on the railway and at nineteen was a station master. Fifty Years of Railway Life in England, Scotland and Ireland |Joseph Tatlow 

How can we rely upon such evidence after nineteen hundred years, and upon a statement of facts so important and so marvellous? God and my Neighbour |Robert Blatchford 

Out of three hundred and fifty white soldiers in the column he had lost one hundred and nineteen. The Red Year |Louis Tracy 

He was a dark-browed, good-looking youngster of nineteen, greatly resembling his mother, but with ten times her impetuosity. The Awakening and Selected Short Stories |Kate Chopin