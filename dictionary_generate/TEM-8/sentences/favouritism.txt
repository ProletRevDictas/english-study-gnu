Save in war time merit gained little reward; promotion came neither from the east nor the west, but from court favouritism. Napoleon's Marshals |R. P. Dunn-Pattison 

This sort of thing is destructive of all discipline, and proves that everything is to go by favouritism. A Roving Commission |G. A. Henty 

Is our own service entirely free from this sort of favouritism? The Two Admirals |J. Fenimore Cooper 

A plan of Favouritism for our executory Government is essentially at variance with the plan of our Legislature. Thoughts on the Present Discontents |Edmund Burke 

No favouritism can sustain a ministry which has become disgustful to the nation. Blackwood's Edinburgh Magazine, No. CCCXLII. Vol. LV. April, 1844 |Various