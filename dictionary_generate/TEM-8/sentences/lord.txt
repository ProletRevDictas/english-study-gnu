Boxer Will Mondrich can be a 50 percent partner with a scandalous lord. How ‘Bridgerton’ flipped the script on ‘The Duke and I’ |Vanessa Riley |January 12, 2021 |Washington Post 

I looked forward to the UPS man like he was the lord and savior himself with packages of books and food my mother shipped, and soon we became friends. How pregnancy bed rest helped prepare me for life in a pandemic |Aileen Weintraub |January 7, 2021 |Washington Post 

As the composer of 20 musicals — with a 21st, a new version of “Cinderella,” being prepped for the West End — and proprietor of six London theaters through his company, Really Useful Group, this lord is unique in the business. Andrew Lloyd Webber believes in the imminent return of Broadway and the West End. That’s all he asks of you. |Peter Marks |December 4, 2020 |Washington Post 

This time, you know for sure that one of them is a lord and one is a warrior. Can you expose the truth in these two riddles? |Claire Maldarelli |August 26, 2020 |Popular-Science 

The crucial thing you need to know about them is that warriors always tell the truth, and lords always lie. Can you expose the truth in these two riddles? |Claire Maldarelli |August 26, 2020 |Popular-Science 

His latest target has been Hajji Hassan, a Baluch drug lord who fled Iran and settled in Turbat in 2000. The Dangerous Drug-Funded Secret War Between Iran and Pakistan |Umar Farooq |December 29, 2014 |DAILY BEAST 

The good Lord has saved a special place in Hell for all those responsible for this wrongdoing. Wrongly Imprisoned for 15 Years Thanks to an Innocence Project |Jacob Siegel |November 13, 2014 |DAILY BEAST 

Gov. Calzada made it seem as though the drug lord were captured alone. Trading Dime Bags for Salvador Dali |Jason McGahan |October 19, 2014 |DAILY BEAST 

Nothing was quite made for the stage like the hilarious story of a teacher with cancer who turns into a meth-dealing drug lord. One Man 'Breaking Bad' Kills on Stage |Nico Hines |August 27, 2014 |DAILY BEAST 

Guardians centers on Peter Quill/Star-Lord (Chris Pratt), an intergalactic smuggler who swipes an orb. ‘Guardians of the Galaxy’ Filmmaker James Gunn on His Glorious Space Opera and Rise to the A-List |Marlow Stern |August 3, 2014 |DAILY BEAST 

But with all her advantages Miss Solomonson failed with the old lord, and she abuses him to this day. The Pit Town Coronet, Volume I (of 3) |Charles James Wills 

Ever since his majority Lord Hetton had annually entered a colt in the great race. The Pit Town Coronet, Volume I (of 3) |Charles James Wills 

He passed through all the honors of the law, and in 1836 became lord chancellor. The Every Day Book of History and Chronology |Joel Munsell 

But we must not class in this unclean category Lord Spunyarn and his friend Haggard, who were both playing at the big table. The Pit Town Coronet, Volume I (of 3) |Charles James Wills 

That they may know thee, as we also have known thee, that there is no God beside thee, O Lord. The Bible, Douay-Rheims Version |Various