Most of the existing sects designate themselves as "churches" with a distinctive forename to each. The Vitality of Mormonism--Brief Essays | James E. Talmage 

The name-token should follow close on the forename for the sake of clearness. An Outline of English Speech-craft | William Barnes 

Being given in the course of especially important religious ceremonies, this forename has a sacred character. The Elementary Forms of the Religious Life | Emile Durkheim British Dictionary definitions for forename forename / ( ˈfɔːˌneɪm ) / noun a first or Christian name Collins English Dictionary - Complete & Unabridged 2012 Digital Edition © William Collins Sons & Co. Ltd. 1979, 1986 © HarperCollins Publishers 1998, 2000, 2003, 2005, 2006, 2007, 2009, 2012