Kate Moss's Self Tanner: Kate Moss is now shilling self-tanner. Phillip Lim is Target's Next Guest Designer; Kate Moss Shills Self Tanner |The Fashion Beast Team |May 9, 2013 |DAILY BEAST 

For him, "elitism" effectively means putting intellectual honesty before partisan shilling. It's Not You, Mitt, It's Your Message, ctd. |Justin Green |September 26, 2012 |DAILY BEAST 

The hootenanny on political websites about the contest being up for grabs is shilling for advertising dollars. Newt Gingrich, Comic Diva: His New Reign of Terror |John Batchelor |January 23, 2012 |DAILY BEAST 

Shilling, a native of Arkansas, pleaded guilty to a pair of wire-fraud counts last July. The Granny Faked Funeral Case |Christine Pelisek |January 18, 2011 |DAILY BEAST 

According to court papers, Williams met Shilling at an Inglewood restaurant on July 10, 2006. The Granny Faked Funeral Case |Christine Pelisek |January 18, 2011 |DAILY BEAST 

The King now increased it to the enormous sum of two shilling and ten pence. Tobacco; Its History, Varieties, Culture, Manufacture and Commerce |E. R. Billings. 

He would haggle in a bargain for a shilling, and economize in things beneath a wise man's notice or consideration. Blackwood's Edinburgh Magazine, No. CCCXXXIX. January, 1844. Vol. LV. |Various 

Many a fine meerschaum keeps up its cheerful fire on a shilling a-week. Tobacco; Its History, Varieties, Culture, Manufacture and Commerce |E. R. Billings. 

The penny at that time was equal to a shilling of the present day, and would, relatively, purchase as much. Showell's Dictionary of Birmingham |Thomas T. Harman and Walter Showell 

It ran from Leicester to Loughborough and back at a fare of one shilling, and carried 570 passengers. Fifty Years of Railway Life in England, Scotland and Ireland |Joseph Tatlow