This is an elegant, graceful transitional episode that honors the past while also setting the stage for Rory’s departure to bigger and better things in the finale. Every episode of Gilmore Girls, ranked |Constance Grady |October 6, 2020 |Vox 

With his graceful long legs gobbling up the track, Farah’s feet flashed Nike yellow as he ran the final 400 meters in about 53 seconds. Inside a secret running program at Nike and a win-at-all-costs corporate culture |Rachel King |October 6, 2020 |Fortune 

Tall, graceful, and deep green, this artificial ficus tree is offered in various sizes depending upon the needs of your space from 36 inches up to an impressive 96 inches high. The best faux plants to bring a little green to your home or office |PopSci Commerce Team |October 5, 2020 |Popular-Science 

Lucius enjoyed the life of a magnifico in the nabob splendor of the Comstock Lode, among the graceful wooden neo-Renaissance mansions, peeling in the searing Nevada sun, built by nineteenth-century silver barons. To Find Hope in American Cooking, James Beard Looked to the West Coast |John Birdsall |October 2, 2020 |Eater 

Later she would become known for being soft-spoken, but as a baby she kicked so much that her family called her “Kiki,” a fitting early nickname for a woman whose power of graceful dissent eventually earned her the moniker “Notorious RBG.” The Glorious RBG |Tracy Moran |September 20, 2020 |Ozy 

Its graceful hotels and beautiful restaurants are totally dependent on the tourist trade. Is Putin Turning to Terrorism in Ukraine? |Anna Nemtsova |January 6, 2015 |DAILY BEAST 

“Wait…” Suddenly a huge, graceful black marlin leaps out of the water, sending a shower of water ten feet high. The Story Behind Lee Marvin’s Liberty Valance Smile |Robert Ward |January 3, 2015 |DAILY BEAST 

The brand logo turned out to feature a graceful archer on horseback, in a Tatar national costume, poised to shoot his arrow. Rebranding The Land of Mongol Warriors & Ivan The Terrible |Anna Nemtsova |December 25, 2014 |DAILY BEAST 

She sent me an unexpected, funny and graceful note after the interview was published. I Was There: Inside Joan Rivers’ Funeral |Tim Teeman |September 8, 2014 |DAILY BEAST 

Nicholas II, the last Romanov, built the graceful Livadia Palace on top of a hill there. Putin's Crimea Is a Big Anti-Gay Casino |Anna Nemtsova |September 8, 2014 |DAILY BEAST 

Arches more graceful in form, or better fitted to defy the assaults of time, I have never seen. Glances at Europe |Horace Greeley 

The wave-like movement of these animals is particularly graceful and cleverly done. Women in the fine arts, from the Seventh Century B.C. to the Twentieth Century A.D. |Clara Erskine Clement 

Some one had gathered orange and lemon branches, and with these fashioned graceful festoons between. The Awakening and Selected Short Stories |Kate Chopin 

Going back, Liszt indulged in a little graceful badinage apropos of the concerto. Music-Study in Germany |Amy Fay 

A mixed type of the present day Negro, she was slightly tall, and somewhat slender, with a figure straight and graceful. The Homesteader |Oscar Micheaux