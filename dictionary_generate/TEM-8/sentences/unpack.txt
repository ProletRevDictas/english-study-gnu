There’s a lot to unpack, so let’s dive into one of the wonkier features of the state’s water market. Environment Report: State Throws Cold Water on Pricing Scheme |MacKenzie Elmer |September 14, 2020 |Voice of San Diego 

Given a crisis on this scale, it’s hard to unpack what exactly might happen, but it’s very likely things would spiral out of control, and the resulting uncertainty could spark unrest and protests that could very well lead to violence. What If Trump Loses And Won’t Leave? |Geoffrey Skelley (geoffrey.skelley@abc.com) |September 14, 2020 |FiveThirtyEight 

His new book, No Rules Rules, which he wrote with business school professor Erin Meyer, unpacks why Netflix is the successful, global content machine and streaming powerhouse it is. What if Your Company Had No Rules? (Bonus Episode) |Maria Konnikova |September 12, 2020 |Freakonomics 

Welcome back to Human Capital, where we unpack the latest in tech labor and diversity and inclusion. Human Capital: The battle over the fate of gig workers continues |Megan Rose Dickey |September 11, 2020 |TechCrunch 

You’ve got all these different formats for audio, so there’s a large amount of number-crunching the TV has to do to unpack it all. How to fix that annoying audio delay on your soundbar |Whitson Gordon |August 25, 2020 |Popular-Science 

Nonetheless, the pop provocateur that soap fans know by the mononym “Franco” has decided to unpack it. James Franco and Scott Haze on 'The Sound and the Fury' and Gawker 'Outing' Them As A 'Couple' |Marlow Stern |September 6, 2014 |DAILY BEAST 

Someone needs to strap a jet pack on Dr. Drew and send him up to unpack these daddy issues, ASAP. You Can't Unsee Billy Ray Cyrus’s Pseudo Hip Hop ‘Achy Breaky 2’ |Amy Zimmerman |February 12, 2014 |DAILY BEAST 

A little while ago I asked a Texas conservative I know to unpack the antipathy aroused by Cruz. Why Does Ted Cruz Inspire Such Animosity? |David Frum |May 3, 2013 |DAILY BEAST 

Her contemplative films (never videos) unpack the world the way Cezanne and Chardin do. A Cezanne for Our Times |Blake Gopnik |May 8, 2012 |DAILY BEAST 

Let's unpack their verbal clash and see what it tells us about each man and his strategy. Romney Puts Perry on Defensive |Howard Kurtz |September 13, 2011 |DAILY BEAST 

June's hands so trembled when she found herself alone with her treasures that she could hardly unpack them. The Trail of the Lonesome Pine |John Fox, Jr. 

Never unpack them and leave their roots exposed to the air for any length of time. Amateur Gardencraft |Eben E. Rexford 

An hour had sufficed him to register and unpack his bag and trunk in the room assigned to him in Torrence Hall. Left Tackle Thayer |Ralph Henry Barbour 

Meanwhile, Hannam continued to unpack and mount the instruments forming the wireless plants. The Home of the Blizzard |Douglas Mawson 

These are your little beds; and Anderson, the schoolroom maid, will unpack your trunks presently. Betty Vivian |L. T. Meade