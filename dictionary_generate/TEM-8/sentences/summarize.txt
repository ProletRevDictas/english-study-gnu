He and his collaborators are working on a paper summarizing what they’ve discovered so far, but they’re nearly out of ideas and will likely have to give up on Collatz soon — for now, at least. Computer Scientists Attempt to Corner the Collatz Conjecture |Kevin Hartnett |August 26, 2020 |Quanta Magazine 

Accurately summarizing long documents is difficult for humans too. Can A.I. understand poetry? |Jeremy Kahn |August 18, 2020 |Fortune 

To summarize, it was only possible to arrange students so that no two were in the correct position relative to each other when N was odd. Can You Reach The Beach? |Zach Wissner-Gross |August 7, 2020 |FiveThirtyEight 

Mathematicians collate the representations of a given group into a table — called a character table — that summarizes information about the group. The ‘Useless’ Perspective That Transformed Mathematics |Kevin Hartnett |June 9, 2020 |Quanta Magazine 

The software can summarize data faster than people can, and from any number of games. Why sports are becoming all about numbers — lots and lots of numbers |Silke Schmidt |May 21, 2020 |Science News For Students 

We can summarize the three and a half hours of Oscars content for shafted viewers. ABC Promised to Livestream the Oscars and Totally Failed |Amy Zimmerman |March 3, 2014 |DAILY BEAST 

The plot is easy to summarize because there isn't much of it. ‘A Field in England’ Is a Psychedelic Cinematic Trip |Andrew Romano |February 9, 2014 |DAILY BEAST 

This rant is pretty difficult to summarize coherently, so probably best to just watch for yourself. Victoria Jackson’s Greatest Hits |Caitlin Dickson |February 5, 2014 |DAILY BEAST 

Rob Ford has a history of scandals too long to summarize in any one place. The Mayor McCrack Show |Jonathan Goldsbie |November 1, 2013 |DAILY BEAST 

Netanyahu's meandering and uninspired drivel left many confused but I will attempt to summarize it. Netanyahu’s Iran Soliloquy at the U.N. |Maysoon Zayid |October 2, 2013 |DAILY BEAST 

As Mr. Harwood is to appear frequently in this chronicle, it may be well to summarize briefly the facts of his history. A Hoosier Chronicle |Meredith Nicholson 

We need do little more than summarize then—adding a touch here and there, perhaps, from another point of view. Mark Twain, A Biography, 1835-1910, Complete |Albert Bigelow Paine 

Therefore, I shall do no more here and now than briefly summarize the Socialist answer to them. The Common Sense of Socialism |John Spargo 

To summarize: The intelligence of primitive man is indeed restricted to a narrow sphere of activity. Elements of Folk Psychology |Wilhelm Wundt 

And now that I come to summarize these lessons I find a single note running through all--from beginning to end. Where Half The World Is Waking Up |Clarence Poe