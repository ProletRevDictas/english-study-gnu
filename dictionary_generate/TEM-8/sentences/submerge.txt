Then, in 2018, it launched a new phase of the project by submerging a data center onto the seabed off Scotland’s Orkney Islands. Microsoft hails success of its undersea data center experiment—and says it could have implications on dry land, too |David Meyer |September 15, 2020 |Fortune 

When I released them next to a stream, I was astonished—they swam away like fish, disappearing into submerged vegetation on the opposite side. How to hunt for star-nosed moles (and their holes) |Kenneth Catania |September 15, 2020 |Popular-Science 

In all, Hauer projects that 13 million Americans will be forced to move away from submerged coastlines. Climate Change Will Force a New American Migration |by Abrahm Lustgarten, photography by Meridith Kohut |September 15, 2020 |ProPublica 

Toy boats bobbed along the bottom of the hovering liquid because, like boats floating right-side up along the top, the toys were partially submerged. Toy boats float upside down underneath a layer of levitated liquid |Maria Temming |September 2, 2020 |Science News 

Any object submerged in a liquid experiences a skyward, buoyant force, whose strength depends on the amount of space the object takes up in the liquid. Toy boats float upside down underneath a layer of levitated liquid |Maria Temming |September 2, 2020 |Science News 

Chives, herbs and leafy greens are grown in hydroponic systems that completely submerge plant roots in water without using soil. Vertical Indoor Farms Are Growing in the U.S. |Miranda Green |May 7, 2013 |DAILY BEAST 

From Midtown to the Brooklyn Battery Tunnel, watch Hurricane Sandy submerge the city. Seven Shocking New York City Flooding Videos |Ben Teitelbaum |October 30, 2012 |DAILY BEAST 

Submerge the apple slices in the syrup and allow them to “rest” for a few hours or, even better, overnight. Fresh Picks |Alex Guarnaschelli |September 15, 2011 |DAILY BEAST 

They're not willing to submerge their anger for the sake of winning elections. The GOP Is Blowing It |Peter Beinart |June 9, 2010 |DAILY BEAST 

Multiply this as many times as necessary to fully submerge the turkey. Simple Roast Turkey, Shiitake Gravy |The Daily Beast |November 25, 2008 |DAILY BEAST 

She tried to thrust out the idea, to submerge it beneath a wave of derision; but to no avail. Warrior of the Dawn |Howard Carleton Browne 

If we was to submerge the Grampus, I'd have to give Cassidy his orders by means of the periscope. Motor Matt's Peril, or, Cast Away in the Bahamas |Stanley R. Matthews 

And then his success began to submerge him: he gasped under the thickening shower of letters. Tales Of Men And Ghosts |Edith Wharton 

Without waiting for his master, he slipped into the water, to discover it deep enough almost to submerge him. The Hero of Panama |F. S. Brereton 

Now intense curiosity was born in her and seemed for the moment to submerge her uneasiness and fear. December Love |Robert Hichens