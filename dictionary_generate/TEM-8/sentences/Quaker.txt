The replaced woman, by the way, was Elizabeth Fry, a Quaker philanthropist who successfully campaigned for prison reform and better conditions in mental asylums in 19th-century England. Check the Data: It’s a Man’s World (The Freakonomics Radio Book Club Ep. 10) |Maria Konnikova |September 25, 2021 |Freakonomics 

Humility, however, is one Quaker attribute that Pearson never exhibited — in his career or his personal life. A top columnist who exposed corruption — and sometimes betrayed his principles |Matthew Pressman |July 9, 2021 |Washington Post 

Edward Hicks included the arch in one of more than five dozen paintings depicting his Quaker fantasy of an Edenic “Peaceable Kingdom” on Earth. Rekindling the wonder of Natural Bridge, once a testament to American grandeur |Philip Kennicott |June 17, 2021 |Washington Post 

Smith was said to have stressed the Quaker values of simplicity, plain-speaking and self-effacement. Robert L. Smith, who led Sidwell Friends School, dies at 96 |Bart Barnes |May 30, 2021 |Washington Post 

Other producers, including the Quaker Mill Company, jumped on board, with several mills merging into the American Cereal Company in 1891, retaining the smiling mascot of a man in Quaker garb. Get to know your oats, and all the types and ways to eat them |Becky Krystal |April 12, 2021 |Washington Post 

One was a Quaker school, whose name he can no longer recall, in upstate New York. The Story Behind Lee Marvin’s Liberty Valance Smile |Robert Ward |January 3, 2015 |DAILY BEAST 

The Quaker Chewy Dipps Chocolate Chip granola bar is more than 40 percent sugar by weight. Your Health Food’s Hidden Sugar Bomb |Michael Schulson |July 8, 2014 |DAILY BEAST 

A single packet of Quaker Maple and Brown Sugar instant oatmeal, though, contains a full tablespoon of sugar. Your Health Food’s Hidden Sugar Bomb |Michael Schulson |July 8, 2014 |DAILY BEAST 

Quaker did not return a request for comment at the time of publishing. Your Health Food’s Hidden Sugar Bomb |Michael Schulson |July 8, 2014 |DAILY BEAST 

A Modern Orthodox Jew, a Buddhist and a Quaker walk into…the Capitol? 10 Religious Surprises in the US Congress |Joshua DuBois |March 9, 2014 |DAILY BEAST 

They certainly were attractive specimens of their race, and the Quaker miller who offered them had a most benignant countenance. Dorothy at Skyrie |Evelyn Raymond 

I know a good farmer wouldn't let even a well-trained Quaker cow into his best meadow; even I know that! Dorothy at Skyrie |Evelyn Raymond 

Exchange bows, of course, if a Quaker will bow; but I'm too happy to-day to be disturbed by talk with him. Dorothy at Skyrie |Evelyn Raymond 

That splendid old Quaker gentleman has just left here, and has made me such a generous offer. Dorothy at Skyrie |Evelyn Raymond 

We learned this in conversation with a sweet-faced, quiet-mannered lady who had all the Quaker characteristics. British Highways And Byways From A Motor Car |Thomas D. Murphy