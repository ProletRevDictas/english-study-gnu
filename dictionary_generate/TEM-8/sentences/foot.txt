A kicked football will not reverse in midair and return to the kicker’s foot. How special relativity can help AI predict the future |Will Heaven |August 28, 2020 |MIT Technology Review 

It’s important to us that we put our best foot forward always, and that’s been hard to reconcile with the shipping delays. Slowed mail delivery is the last thing indie bookstores need right now |Rachel King |August 19, 2020 |Fortune 

There’s different muck under your boggy feet in different parts of the country, at different times. Every Decision Is A Risk. Every Risk Is A Decision. |Maggie Koerth (maggie.koerth-baker@fivethirtyeight.com) |July 21, 2020 |FiveThirtyEight 

The median sales price of homes in Cheesman Park is $799,000 with an average of $457 per square foot. Denver: Where the Queer Community is a Mile High |LGBTQ-Editor |July 14, 2020 |No Straight News 

If your influencer isn’t credible enough, your audience might not get swept off their feet. How to get more leads on Instagram: 10 Highly effective tactics |Bhavik Soni |July 7, 2020 |Search Engine Watch 

Together, they crossed over the International Bridges on foot into Juarez to conduct some business. An Informant, a Missing American, and Juarez’s House of Death: Inside the 12-Year Cold Case of David Castro |Bill Conroy |January 6, 2015 |DAILY BEAST 

In the classic skillset of piloting, mental acuity, and its coordination with hand and foot movements, is equally vital. Flight 8501 Poses Question: Are Modern Jets Too Automated to Fly? |Clive Irving |January 4, 2015 |DAILY BEAST 

Wearing the right foot of a chicken was considered good luck. The History of the Chicken: How This Humble Bird Saved Humanity |William O’Connor |December 27, 2014 |DAILY BEAST 

It made sense with so many suspects at hand, less so with the tower entrance separated from them by a forty foot wall. A Million Ways to Die in Prison |Daniel Genis |December 8, 2014 |DAILY BEAST 

Marabella, now licking her lollipop and tapping her foot, appears unfazed. Even Grade School Kids Are Protesting the Garner Killing Now |Caitlin Dickson |December 6, 2014 |DAILY BEAST 

The bride elect rushes up to him, and so they both step down to the foot-lights. Physiology of The Opera |John H. Swaby (AKA "Scrici") 

I find myself chained to the foot of a woman, my noble Cornelia would despise! The Pastor's Fire-side Vol. 3 of 4 |Jane Porter 

We had now approached closely to the foot of the mountain-ranges, and their lofty summits were high above us in mid-air. Blackwood's Edinburgh Magazine, Volume 60, No. 372, October 1846 |Various 

We see the whole land, even if but at a distance, instead of being limited merely to the spot where our foot treads. Music-Study in Germany |Amy Fay 

But there is a pinnacle of human success and of human opinion, on which human foot was never yet permitted to rest. The Pastor's Fire-side Vol. 3 of 4 |Jane Porter