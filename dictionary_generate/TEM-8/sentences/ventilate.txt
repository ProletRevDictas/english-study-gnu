The city had a $3 billion backlog of facilities repairs on its aging school buildings, WBEZ reported in 2018, and teachers were concerned that poorly ventilated classrooms could encourage the coronavirus to circulate. Chicago reaches deal with teachers to reopen school buildings |Moriah Balingit |February 10, 2021 |Washington Post 

The Centers for Disease Control and Prevention recommends that vaccinated people continue to wear masks, socially distance, avoid poorly ventilated spaces and wash their hands frequently to prevent the spread of the virus. Two lawmakers test positive for coronavirus, one after receiving both doses of vaccine |Amy B Wang, Sarah Kaplan |January 30, 2021 |Washington Post 

It had to be comfortable, ventilated, have its legs protected and be secure. This new harness lets military dogs parachute safely and with style |Christina Mackenzie |January 4, 2021 |Popular-Science 

Prison facilities are often overcrowded and poorly ventilated. Coronavirus is hitting prisons and jails hard—1 in 5 inmates has had COVID, and 1,700 have died |Bernhard Warner |December 18, 2020 |Fortune 

If you are inside with other people, make sure it’s as ventilated as it possibly can be and that the air is being cleaned as well as it can be. ‘We’re Backed Into a Corner’: Two Experts on the Best Path to Reopening |Megan Wood |December 16, 2020 |Voice of San Diego 

My dear, the object of a meeting is to ventilate the subject. Mrs. Dorriman, Volume 3 of 3 |Julie Bosville Chetwynd 

With impunity might the tenderfoot ventilate his "stovepipe" or his theories of culture. Roads of Destiny |O. Henry 

"I don't know as I wants to ventilate yu; we mostly poisons coyotes up my way," he added. Hopalong Cassidy's Rustler Round-Up |Clarence Edward Mulford 

There are some wide cracks in the siding, but they help to ventilate, and make it healthier for the cattle. The Elements of Agriculture |George E. Waring 

The object of this invention is to ventilate and cool railway cars used in the transportation of perishable articles. Scientific American, Vol. XXXIX.No. 6. [New Series.], August 10, 1878 |Various