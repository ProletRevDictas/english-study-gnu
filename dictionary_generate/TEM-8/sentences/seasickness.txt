The last two major categories were seasickness, which affected less than half of the rowers, and gastrointestinal problems, which affected about a quarter of the subjects. What It Takes to Row Across an Ocean |mmirhashem |February 9, 2022 |Outside Online 

Noise, although an auditory phenomenon, is strangely related to seasickness, a result of the odd conjoining of the auditory and vestibular systems. Your Brain Is Like Beethoven - Issue 107: The Edge |Jonathan Berger |October 27, 2021 |Nautilus 

It can get mighty windy out here, which creates chop, so anyone prone to seasickness might consider popping Dramamine before boarding. The Ultimate Channel Islands National Park Travel Guide |Shawnté Salabert |June 23, 2021 |Outside Online 

Nor is seasickness an issue, because of the wonder-drug Meclizine, sold as Sea Calm. In Defense of Cruises: Ignore the Carnival Sewage Disaster |Andrew Roberts |February 15, 2013 |DAILY BEAST 

But everything I read said women like me, who got seasickness, altitude sickness, motion sickness, were more likely to suffer HG. Prue Clarke on Her Battles With Kate Middleton’s Illness, Hyperemesis Gravidarum |Prue Clarke |December 5, 2012 |DAILY BEAST 

And there, to be frank, she forgot her fright in as bitter a tribute of seasickness as even the channel has ever exacted. The Amazing Interlude |Mary Roberts Rinehart 

The voyager embarks, and is in all probability confined to his cabin, suffering under the dreadful protraction of seasickness. Newton Forster |Captain Frederick Marryat 

I do not know whether it was the result of seasickness, or what it was, but everything in Hati looked crooked. Gardens of the Caribbees, v. 1/2 |Ida May Hill Starr 

Neither would suffer further from seasickness, they felt sure. The Iron Boys on the Ore Boats |James R. Mears 

He was weak and wretched with long seasickness and loss of sleep, and staggered as he walked along the wharf like a drunken man. Sevenoaks |J. G. Holland