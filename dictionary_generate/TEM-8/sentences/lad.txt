We are meant to see Coach Lad as a gentle shepherd, full of lessons about humility and brotherhood. Two New Films Preach Our Nation’s Corrosive Gridiron Gospel |Steve Almond |September 20, 2014 |DAILY BEAST 

He was a biggish lad, with a boyish, slightly mischievous grin, and thoughtfulness and consideration were seamed in his character. The James Foley I Knew in the ISIS War Zone |Jamie Dettmer |August 20, 2014 |DAILY BEAST 

To release this feminist anthem through what is essentially a lad mag that guys read at barbershops? Jenny Lewis on 'The Voyager,' the End of Rilo Kiley, and High School Classmate Angelina Jolie |Marlow Stern |August 17, 2014 |DAILY BEAST 

Unlike his conservative colleagues Michele Bachmann, Ron Paul, and perhaps his lad Rand, Gingrey endorsed mandatory vaccination. D.C. Moron Phil Gingrey Spread Ebola Fever Over Immigrants |Kent Sepkowitz |July 15, 2014 |DAILY BEAST 

She ties with a forward-thinking lad named John, who refuses to marry any woman against her will. ‘Free to Be…You and Me’ Did Not Emasculate Men |Emily Shire |March 11, 2014 |DAILY BEAST 

Egypt was once a land of mystery; now, every lad, on leaving Eton, yachts it to the pyramids. Blackwood's Edinburgh Magazine, No. CCCXXXIX. January, 1844. Vol. LV. |Various 

The soldiers killed a young lad who tried to pass, or wounded him so severely that it is said that he died. The Philippine Islands, 1493-1898, Volume XX, 1621-1624 |Various 

Again the unknown power smote the lad to the earth, which had become a raging sea. A Lost Hero |Elizabeth Stuart Phelps Ward and Herbert D. Ward 

Through a narrow door about three feet high the lad and his tutor entered their room. Our Little Korean Cousin |H. Lee M. Pike 

Even now, one volume lay on the window ledge, where the happy lad had risen to study it as soon as daylight came. Dorothy at Skyrie |Evelyn Raymond