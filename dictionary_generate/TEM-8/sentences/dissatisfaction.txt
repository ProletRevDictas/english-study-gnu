New research suggests that 1 in 4 teachers are thinking about quitting due to higher stress levels and general dissatisfaction with their job. Back to School, Ready or Not |Eromo Egbejule |August 27, 2021 |Ozy 

Board members’ dissatisfaction with the secrecy of the contracts – some which were entered into under a rule that allows the general manager to execute contracts below $150,000 without board approval – boiled over in a board meeting. Water Authority’s Confidential Consultant Contracts Surprised Board |MacKenzie Elmer |August 4, 2021 |Voice of San Diego 

While both men and women recognized the situation was imbalanced, it only led to relationship dissatisfaction among the women, unless the men were doing a lot of childcare. What We Learned About Relationships During the Pandemic |Belinda Luscombe |July 6, 2021 |Time 

Faculty from the College of Business Administration have also expressed dissatisfaction with the university’s decision-making and that they didn’t know their own colleague had harassed students in their program. CSU San Marcos Ordeal Brings Difficult Transparency Questions to Light |Kayla Jimenez |June 21, 2021 |Voice of San Diego 

Bolsonaro’s mishandling of the pandemic has created ripple effects in other areas, including the economy and public health care system, all of it increasing the public’s frustration and dissatisfaction. Jair Bolsonaro is facing a political reckoning in Brazil. How far will it go? |Jen Kirby |June 4, 2021 |Vox 

Time and again, the author confuses chattering class dissatisfaction with an honest assessment of accomplishment. Chuck Todd’s Lousy Obama Takedown |William O’Connor |November 14, 2014 |DAILY BEAST 

But amid this widespread and sustained dissatisfaction, 2014 was a terrible year for third-party candidates. How to Run a Statewide Campaign on $38 |Michael Ames |November 12, 2014 |DAILY BEAST 

Dempsey has twice made public statements that seemed to reveal his dissatisfaction with the White House policy. Military Hates White House ‘Micromanagement’ of ISIS War |Josh Rogin, Eli Lake |October 31, 2014 |DAILY BEAST 

“The best hope [for getting something done] is the growing job dissatisfaction that many members feel,” he said. There’s a Senate Civil War Coming, No Matter Who Wins in November |David Freedlander |October 14, 2014 |DAILY BEAST 

The incredible public dissatisfaction with both political parties has been building for some time. The Independents Who Could Tip the Senate in November |Linda Killian |October 13, 2014 |DAILY BEAST 

But the principal characters would furnish their own costumes, and that is where Lily Pendleton began to lose her dissatisfaction. The Girls of Central High on the Stage |Gertrude W. Morrison 

For a short time his dissatisfaction with The Oprichnik filled him with such doubt of his powers that his spirits flagged. The Life &amp; Letters of Peter Ilich Tchaikovsky |Modeste Tchaikovsky 

The officer in charge has repeatedly expressed dissatisfaction with my slow progress in the work. Prison Memoirs of an Anarchist |Alexander Berkman 

It may be argued again that dissatisfaction with our life's endeavour springs in some degree from dulness. The Pocket R.L.S. |Robert Louis Stevenson 

Dissatisfaction is expressed at the “little reasonable ordinances” of the Gilds but not against the companies themselves. The Influence and Development of English Gilds |Francis Aiden Hibbert