Politico has reported that some Republican donors see Scalise as damaged goods. The Price of Steve Scalise’s Silence |Jason Berry |January 7, 2015 |DAILY BEAST 

In fact, Mexico buys and sells more US goods than any other country on the planet except for Canada. Why Mexicans Are Enraged by Obama’s Big Tuesday Meeting |Ruben Navarrette Jr. |January 6, 2015 |DAILY BEAST 

I never hear a Democrat talk about these goods, which are, in the literal sense, indivisible—for us all. The Democrats’ Black Hole—and What They Can Do About It |Michael Tomasky |December 31, 2014 |DAILY BEAST 

As more come online, they will actively seek better selling prices elsewhere and also source their goods internationally. Silicon Valley Sets Its Sights on Africa |Christian Borys |December 22, 2014 |DAILY BEAST 

On Thursday, Russian bloggers published pictures of empty shelves in stores that once sold electric goods. After His Disastrous Annual Press Conference, Putin Needs A Hug |Anna Nemtsova |December 18, 2014 |DAILY BEAST 

Sweden excluded British goods, conformably to the continental system established by Bonaparte. The Every Day Book of History and Chronology |Joel Munsell 

The carrying of these heavy government debts is a question of the future production of goods, of commerce, and of saving. Readings in Money and Banking |Chester Arthur Phillips 

The restoration of stolen goods was probably dwarfed in his mind by the importance of capturing the stealers. Raw Gold |Bertrand W. Sinclair 

He even felt a certain enjoyment in the discomfiture of the self-constituted posse of searchers for stolen goods. Ramona |Helen Hunt Jackson 

He and his household are going with their goods in the galliots which are now leaving this city for Yndia. The Philippine Islands, 1493-1898, Volume XX, 1621-1624 |Various