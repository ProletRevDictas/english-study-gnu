Until then, Metro officials said customers can use shuttle buses, which will ferry riders between stations. Two Blue Line Metro stations to close through late May for platform work |Justin George |February 11, 2021 |Washington Post 

There is a shuttle bus taking passengers to L’Enfant plaza metro. Metro service restored after track problem brings suspension in D.C. |Justin George |February 4, 2021 |Washington Post 

“The Zoo pays FONZ to perform in-park services, such as operating guest shuttles, renting strollers, and even emptying the trash, that FONZ performed more efficiently than either the Zoo or for-profit vendors could,” she said in an email. National Zoo to split with longtime partner FONZ |Michael Ruane |February 4, 2021 |Washington Post 

After three decades, on July 21, 2011, NASA’s space shuttle program completed its 135th, and final, mission, when the shuttle Atlantis landed at Kennedy Space Center in Florida. Ralphie from ‘A Christmas Story’ could have been the first child in space. Then disaster struck. |Erik Olsen |December 23, 2020 |Popular-Science 

From there you can take a shuttle 80 miles north to Grand Canyon Village. Your New Adventure Travel Bucket List |The Editors |December 21, 2020 |Outside Online 

The first shuttle flights with two crew members used ejection seats and full pressure suits. Can Anyone Make Space Safe for Civilians? |Clive Irving |November 4, 2014 |DAILY BEAST 

It could have had a burn-through like Challenger Shuttle, but I would have thought that would take longer. Clues From SpaceShipTwo’s Wreckage: Did the Crew Compartment Fail? |Clive Irving |November 2, 2014 |DAILY BEAST 

Once a day, she says, a shuttle bus took them into town, dropping them not so subtly near the train station. Italy's Latest Export Is Refugees, and the Rest of Europe Is Not Happy |Barbie Latza Nadeau |August 26, 2014 |DAILY BEAST 

He used the Lear—which seated only six and had no bar—mostly to shuttle his pals between Los Angeles, Palm Springs, and Las Vegas. Frank Sinatra and the Birth of the Jet Set |William Stadiem |August 2, 2014 |DAILY BEAST 

Similar to the Space Shuttle in appearance, the diminutive X-37B is about a quarter the size of the old shuttles. Will The Pentagon’s Secret Space Plane Ever Return to Earth? |Kyle Mizokami |April 7, 2014 |DAILY BEAST 

He got on the shuttle and over to the West side and up to 96th and across the street from where Louis lived. Hooded Detective, Volume III No. 2, January, 1942 |Various 

Bent low over the machine, he seems absorbed in the work, his hands deftly manipulating the shuttle, his foot on the treadle. Prison Memoirs of an Anarchist |Alexander Berkman 

Last night, when Iftikhar spoke to you soft and low, I could see your eye following his as a weaver's the shuttle. God Wills It! |William Stearns Davis 

But while I admired, I wondered what had called forth in a lad so shuttle-witted this enduring sense of duty. The Works of Robert Louis Stevenson, Volume XXI |Robert Louis Stevenson 

Vaguely, uneasiness grew in his mind as he entered the shuttle station. The Link |Alan Edward Nourse