Mistress Maria Kent was sitting on the porch at her pretty little home, the picture of an old-time schoolmistress. Maid Sally |Harriet A. Cheever 

And this was the correct schoolmistress, the preceptress and example of all the virtues! Two Men of Sandy Bar |Bret Harte 

The child, having conceived a great fondness for her schoolmistress, wished to please the latter by attention to her lessons. The Sexual Life of the Child |Albert Moll 

"Come home, Henrietta," said Periwinkle, and she marched Henrietta out the door under the very eyes of the schoolmistress. Duffels |Edward Eggleston 

It was, perhaps, characteristic of the little old schoolmistress to show no interest whatever. The Isle of Unrest |Henry Seton Merriman