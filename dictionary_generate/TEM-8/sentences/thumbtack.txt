A gram of some fynbos plants’ root tissue – about the mass of a thumbtack – can stretch over 15 football field lengths of territory. Africa’s fynbos plants hold their ground with the world’s thinnest roots |Jake Buehler |March 1, 2022 |Science News 

The chart’s dense web of arrows and connections bore an unmistakable resemblance to an obsessive’s cork board covered with red string and thumbtacks. They called it a conspiracy theory. But Alina Chan tweeted life into the idea that the virus came from a lab. |Antonio Regalado |June 25, 2021 |MIT Technology Review 

A thumbtack is inserted in the standard near the top to prevent the box from being lifted entirely from the base. The Boy Mechanic, Book 2 |Various 

Lay the stencil pattern in position and thumbtack it down to the cheese cloth and blotting paper. The Library of Work and Play: Needlecraft |Effie Archer Archer 

If knives are used to cut out the design, thumbtack the leather before commencing. The Library of Work and Play: Needlecraft |Effie Archer Archer 

The latter she suspended by a thumbtack beside the mirror of her bureau. Out of the Air |Inez Haynes Irwin 

She took the miniature, thumbtack and all, from the wall, 183 and put it in her wrist bag. Out of the Air |Inez Haynes Irwin