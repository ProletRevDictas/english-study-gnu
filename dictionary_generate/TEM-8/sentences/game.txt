Still, the game of chicken between Pelosi and moderate Democrats is striking. Trump moves closer to Pelosi in economic aid talks, and House speaker must decide next move |Rachael Bade, Erica Werner |September 17, 2020 |Washington Post 

The output of a restaurant cannot be defined in numbers or data like a game. Christian Puglisi Is Closing His Influential Copenhagen Restaurants. COVID Is Only Partly to Blame |Rafael Tonon |September 17, 2020 |Eater 

“We’re using this as an opportunity to really understand our game at a higher level,” McCutchen said. Don’t Blame The Refs For All Of These Replay Reviews |Jared Dubin |September 17, 2020 |FiveThirtyEight 

Hahn’s comments — and his later apology — affirmed the idea that politics was seeping into the work of health officials who are supposed to be above playing that game. The Trump administration’s politicization of coronavirus comes to a head |Aaron Blake |September 16, 2020 |Washington Post 

When you write him off as being past his prime, he might just respond with a game like Sunday’s vintage performance — 364 yards, 73 percent of passes completed, four touchdowns and zero interceptions — against the Minnesota Vikings. Aaron Rodgers Is Playing Like Aaron Rodgers Again |Neil Paine (neil.paine@fivethirtyeight.com) |September 16, 2020 |FiveThirtyEight 

This is going to be the Game of Thrones of U.S. Senate races. The Golden State Preps for the ‘Red Wedding’ of Senate Races |David Freedlander |January 9, 2015 |DAILY BEAST 

Dora is seen getting dressed as a mermaid by a cursor being manned by some omniscient game player. Sleazy Billionaire’s Double Life Featured Beach Parties With Stephen Hawking |M.L. Nestel |January 8, 2015 |DAILY BEAST 

Think of it as Game of Thrones—if you subtract the sex and violence and add drunken revelry and singing. ‘Galavant’: A Drunken, Horny Musical Fairy Tale |Melissa Leon |January 5, 2015 |DAILY BEAST 

Actually, the guessing game is over; the weddings have begun, as have weird attempts to circumvent our constitutional democracy. The Back Alley, Low Blow-Ridden Fight to Stop Gay Marriage in Florida Is Finally Over |Jay Michaelson |January 5, 2015 |DAILY BEAST 

When the game starts, there is only sand, a white ball, a flag indicating hole 1, and a “0” at the top of the screen. Lost For Thousands of Strokes: 'Desert Golfing' Is 'Angry Birds' as Modern Art |Alec Kubas-Meyer |January 2, 2015 |DAILY BEAST 

And to tell the truth, she couldn't help wishing he could see, so he could make the game livelier. The Tale of Grandfather Mole |Arthur Scott Bailey 

Jean clung to his English nurse, who played the fascinating game of pretending to eat his hand. The Joyous Adventures of Aristide Pujol |William J. Locke 

Two many-branched candelabra, holding wax lights, brilliantly illuminate the game. The Pit Town Coronet, Volume I (of 3) |Charles James Wills 

The unhappy applicant was naturally obliged to temporarily retire from the game, at all events for that night. The Pit Town Coronet, Volume I (of 3) |Charles James Wills 

His head fell back limp on MacRae's arm, and the rest of the message went with the game old Dutchman across the big divide. Raw Gold |Bertrand W. Sinclair