In response, SDG&E pointed to its own data showing that when it clears trees beyond the state minimum, its equipment had fewer contact with vegetation and a drop in fires. Environment Report: Real Estate Sellers Aren’t Required to Disclose Sea Level Rise Risk |MacKenzie Elmer |August 24, 2020 |Voice of San Diego 

It pointed to its own data showing that when it clears trees beyond the state minimum, its equipment had fewer contact with vegetation and a drop in fires. Watchdog Warns: SDG&E’s Tree-Trimming Plan Could Worsen Wildfires |MacKenzie Elmer |August 24, 2020 |Voice of San Diego 

Rilling said the easements are not necessary for the fuel modification zone, or the management of combustible vegetation. The Burning Question Surrounding the Latest Version of Lilac Hills Ranch |Maya Srikrishnan |June 24, 2020 |Voice of San Diego 

Fuel modification in this context means that flammable vegetation will be thinned and managed in such a way that it will disrupt a fire’s spread. The Burning Question Surrounding the Latest Version of Lilac Hills Ranch |Maya Srikrishnan |June 24, 2020 |Voice of San Diego 

From buried traces of vegetation, the scientists reconstructed what the climate must have been like back then. A rainforest once grew near the South Pole |Carolyn Gramling |May 11, 2020 |Science News For Students 

Hippopotamuses eat aquatic vegetation, like water hyacinths—loads of it, Irwin learned. Lake Bacon: The Story of The Man Who Wanted Us to Eat Mississippi Hippos |Jon Mooallem |August 10, 2014 |DAILY BEAST 

They are searching for abnormalities in the soil, replanted vegetation and other signs of a rogue grave. The Hunt for Madeleine McCann’s Grave |Barbie Latza Nadeau |May 12, 2014 |DAILY BEAST 

The path for our group of six is being carved through tangles of vines and vegetation one machete hack at a time. Can Gorillas Save the Democratic Republic of the Congo? |Nina Strochlic |April 28, 2014 |DAILY BEAST 

Women would first bathe their feet in a mixture of vinegar and natural vegetation. Corsets, Muslin Disease, and More of the Deadly Fashion Trends |The Fashion Beast Team |April 1, 2014 |DAILY BEAST 

Adam tended to the garden and he and Eve ate locally sourced, organically grown vegetation. Diet Like Jesus: What the Bible Says About How to Eat |Candida Moss |October 15, 2013 |DAILY BEAST 

The particular phenomena of vegetation also afford abundant evidence that humus cannot be the only source of carbon. Elements of Agricultural Chemistry |Thomas Anderson 

In the Brazils a conflagration of this kind never extends very far, as the vegetation is too green and offers too much opposition. A Woman's Journey Round the World |Ida Pfeiffer 

The immediate effect of lime on the vegetation of the land to which it is applied is very striking. Elements of Agricultural Chemistry |Thomas Anderson 

Here and there they distinguished red cliffs, and some signs of a scanty and burnt up vegetation. Narrative of a Survey of the Intertropical and Western Coasts of Australia] [Volume 2 of 2] |Phillip Parker King 

It is impossible to conceive any thing richer than the vegetation down to the very water's edge around the lake. Journal of a Voyage to Brazil |Maria Graham