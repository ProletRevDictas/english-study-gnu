He had a special knife designed to cut the dense loaf, and a ceremony to precede cutting the cake. One Cake to Rule Them All: How Stollen Stole Our Hearts |Molly Hannon |December 24, 2014 |DAILY BEAST 

Of course, there are delicate negotiations that precede such an event. Santorum's Anemic E-Mail Endorsement |Howard Kurtz |May 8, 2012 |DAILY BEAST 

We see it as an appetizer that could precede a glorious banquet. If Wall Street Repents, Can Main Street Forgive? |Jacki Zehner, Katharine Rhodes Henderson |November 21, 2009 |DAILY BEAST 

To the invitation to precede him she readily responded, and, with a bow to the Seneschal, she began to walk across the apartment. St. Martin's Summer |Rafael Sabatini 

He made way for her to precede him in the narrow wood path, and then silently followed her up the glen. Ruth |Elizabeth Cleghorn Gaskell 

As causes precede effects, the causal order and the time order generally coincide. English: Composition and Literature |W. F. (William Franklin) Webster 

There was something almost ghastly in this terrific silence which could only precede some unnatural tumult. Menotah |Ernest G. Henham 

It was only on the first landing that the servant stood aside with the tray to allow me to precede her. In Accordance with the Evidence |Oliver Onions