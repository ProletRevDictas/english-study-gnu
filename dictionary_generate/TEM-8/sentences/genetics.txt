If dialect stems from genetics, these outsiders should still sound like outsiders. Naked mole-rat colonies speak with unique dialects |Jonathan Lambert |January 28, 2021 |Science News 

Addressing disparities related to living conditions, locations, and genetics has always been a factor of disease spread and mortality, but it has never been tracked, measured, and analyzed on such a scale. Envisioning better health outcomes for all |Jason Sparapani |January 26, 2021 |MIT Technology Review 

In 2015, for example, two dozen of the world’s leading sports genetics researchers published a consensus statement in the British Journal of Sports Medicine affirming that “genetic tests have no role of play in talent identification.” Decoding the DNA of 5 Olympic Athletes |Alex Hutchinson |January 14, 2021 |Outside Online 

Factors of metabolic rateMetabolic rate and calorie requirements vary from person to person depending on factors such as genetics, gender, age, body composition and amount of exercise you do. Some people can eat anything and not gain a pound. How metabolism affects the calories you burn each day. |Terezie Tolar-Peterson |January 2, 2021 |Washington Post 

This seems remarkable on the face of it because there is no viable scientific opposition to evolution and it is widely accepted by biologists and other life scientists as being fundamental to understanding biology—from genetics to medicine. Vaccines Are the Safest Medical Procedure We Have. Make Your Wager Wisely - Facts So Romantic |Stuart Firestein |December 29, 2020 |Nautilus 

Genetics alone does not an eating disorder make, generally speaking, and Bulik points out that environment still plays a role. How Skinny Is Too Skinny? Israel Bans ‘Underweight’ Models |Carrie Arnold |January 8, 2015 |DAILY BEAST 

The at-home genetics testing company 23andme, established in 2006, helps people learn more about their “DNA relatives.” Nothing Says I Love You Like Data |The Daily Beast |December 8, 2014 |DAILY BEAST 

Nature and nurture, genetics and family background all come into play. Inside the Mind of an ISIS Jihadi |Jamie Dettmer |September 21, 2014 |DAILY BEAST 

“Keep in mind that our body shape is often determined by genetics,” says Dr. Ball. Waist Training: Can You Cinch Your Waist Thin? |DailyBurn |July 18, 2014 |DAILY BEAST 

Recent research has shown perfectionism to be an issue of genetics. Face It—We Rubes Will Never Live Like Gwyneth and Jennifer Aniston |Rachel Bertsche |July 2, 2014 |DAILY BEAST 

This behavior, as the study of Genetics shows, may be determined in lesser organisms by experiment. The Pivot of Civilization |Margaret Sanger 

Genetics contain medicines which control the uterine and sexual systems, which may all be reckoned among Neurotics. The Action of Medicines in the System |Frederick William Headland 

Scientific stock-breeding supplies valuable practical examples of applied Genetics, or the Science of Heredity. Feminism and Sex-Extinction |Arabella Kenealy 

Tuly, who knows more of psychology and genetics than I, convinced me of three things. Masters of Space |Edward Elmer Smith 

The civilization of illiteracy is one of sampling, a concept originating in genetics. The Civilization of Illiteracy |Mihai Nadin