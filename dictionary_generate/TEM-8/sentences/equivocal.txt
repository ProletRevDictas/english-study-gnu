In a tentative ruling filed earlier this week, Judge William Orrick said Wisk’s “evidence of misappropriation is too equivocal to warrant a preliminary injunction.” Judge denies Wisk Aero’s request for preliminary injunction against Archer Aviation |Aria Alamalhodaei |July 23, 2021 |TechCrunch 

The results are almost always negative, and when they are equivocal it rarely is an indication of actual illness. Predator Doctors Take Advantage of Patients With ‘Chronic Lyme’ Scam |Russell Saunders |September 19, 2014 |DAILY BEAST 

His answer was equivocal as Paul said his wife didn't want him to run. Rand Paul: My Wife Will Decide If I Run |Ben Jacobs |December 6, 2013 |DAILY BEAST 

But his wavering and equivocal history on the marriage issue more properly suggests a profile in Jell-O. Obama's Gay-Marriage Switch: Why Now? |Michael Medved |May 10, 2012 |DAILY BEAST 

As America celebrated the bin Laden killing, the response in the Middle East was more equivocal. Arab World's Mixed Reaction |Fadel Lamen |May 3, 2011 |DAILY BEAST 

There never was a more equivocal expression; and such as it was then it still is. A Philosophical Dictionary, Volume 1 (of 10) |Franois-Marie Arouet (AKA Voltaire) 

The whole public law of Europe had its origin in equivocal expressions, beginning with the Salique law. A Philosophical Dictionary, Volume 1 (of 10) |Franois-Marie Arouet (AKA Voltaire) 

The climate of these animals is not therefore, equivocal; but it is more difficult to determine the relative bulk of each species. Buffon's Natural History. Volume VII (of 10) |Georges Louis Leclerc de Buffon 

He was "sick at heart of the equivocal position," and determined to "go on in a clearer path." The Life of Mazzini |Bolton King 

Even at first their acts were equivocal, and they soon came to be as illegal as they were oppressive. The Loyalists of America and Their Times, Vol. 1 of 2 |Egerton Ryerson