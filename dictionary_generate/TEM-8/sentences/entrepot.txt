When the Mahommedans first invaded that region Ghazni was a wealthy entrepot of the Indian trade. Encyclopaedia Britannica, 11th Edition, Volume 11, Slice 8 |Various 

Northern Illinois filled rapidly with a thrifty farming population, and the town of Chicago became an entrepot. The Old Northwest |Frederic Austin Ogg 

Sacramento City had been laid out, lots were being rapidly sold, and the town was being built up as an entrepot to the mines. The Memoirs of General W. T. Sherman, Complete |William T. Sherman 

The physical resources of this region are of such a nature and variety as to make Mackinaw city the entrepot of a vast commerce. Old Mackinaw |W. P. Strickland. 

Chusan was occupied by the Japanese during the Ming dynasty, and served as an important commercial entrepot. Encyclopaedia Britannica, 11th Edition, Volume 6, Slice 3 |Various