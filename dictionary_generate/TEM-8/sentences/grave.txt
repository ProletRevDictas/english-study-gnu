“Big picture, the way I’m looking at it, I’m gonna go to my grave thinking I helped the city save millions and millions of dollars by helping the city secure control of two long-term assets,” Wood said. The Deal Before the 101 Ash St. Debacle Helps Explain How We Got Here |Lisa Halverstadt and Jesse Marx |August 24, 2020 |Voice of San Diego 

The architects are likely rolling in their graves at this use of their plans, but our attention to detail allowed the producers of the virtual commencement video to transition seamlessly from the animated dome to images of the real one. The Minecraft Institvte of Technology |Tate Ryan-Mosley |August 19, 2020 |MIT Technology Review 

We need to use it as something that makes the situation more grave, rather than the only reason to give them protection. Border Report: One More Way the Pandemic Is Hurting Asylum-Seekers |Maya Srikrishnan |August 3, 2020 |Voice of San Diego 

You’ll see it a lot on social media today in contexts that aren’t so grave and somber … like that chicken sandwich. What Does “R.I.P.” Stand For? |Rachel Leonard |July 20, 2020 |Everything After Z 

Cassidy and Bradley’s team studied DNA from 44 individuals buried in various Irish tombs and graves dating to between roughly 6,600 and 4,500 years ago. DNA from a 5,200-year-old Irish tomb hints at ancient royal incest |Bruce Bower |June 17, 2020 |Science News 

That distant whirring sound you hear is a long-dead Greek physician spinning in his grave. Why So Many Surgeons Are Psychos |Russell Saunders |December 17, 2014 |DAILY BEAST 

A “komitetchik par excellence,” a man of “outstanding mediocrity,” and “the grave digger of the revolution.” Kotkin Biography Reveals Stalin's Evil Pragmatism |William O’Connor |November 30, 2014 |DAILY BEAST 

“I read articles that say ‘here’s another white girl joining in on the dance party on the grave of hip hop,” she says. From Church of Christ to Pansexual Rapper |Tyler Gillespie |November 28, 2014 |DAILY BEAST 

“The amount of literal brainwork needed to do his job too such a toll on him that it sent him to an early grave,” Goode says. From ‘The Good Wife’ to ‘The Imitation Game’: Matthew Goode Wages His Charm Offensive |Kevin Fallon |November 24, 2014 |DAILY BEAST 

You have focused on individual events and ideas in your books about Lincoln rather than the cradle-to-grave biographical approach. What Lincoln Could Teach Fox News |Scott Porch |November 6, 2014 |DAILY BEAST 

The water suggested the fear that he must be nearing the open sea, and he became supernaturally grave. The Giant of the North |R.M. Ballantyne 

After his death crowds flocked to his grave to touch his holy monument, till the authorities caused the church yard to be shut. The Every Day Book of History and Chronology |Joel Munsell 

They carried the two bodies together on some litters, and buried them both in the same grave. The Philippine Islands, 1493-1898, Volume XX, 1621-1624 |Various 

He, therefore, did as he said; made no further observation, but conducted himself to his young friend with grave distance. The Pastor's Fire-side Vol. 3 of 4 |Jane Porter 

As soon as he had seen his mother, he would set off again, and never cease searching till he had found either Ramona or her grave. Ramona |Helen Hunt Jackson