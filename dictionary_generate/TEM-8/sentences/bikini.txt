Professional kiteboarder and surfer Sensi Graves is in the business of making bikinis for women who rip, and the Claire top is a prime example of her work. The Surf Gear I Rely on from Women-Owned Companies |Rebecca Parsons |January 30, 2021 |Outside Online 

Feed the same one a cropped photo of a woman, even a famous woman like US Representative Alexandria Ocasio-Cortez, and 53% of the time, it will autocomplete her wearing a low-cut top or bikini. An AI saw a cropped photo of AOC. It autocompleted her wearing a bikini. |Karen Hao |January 29, 2021 |MIT Technology Review 

The researchers also found that the “undressing” algorithm is starting to be applied to videos, such as footage of bikini models walking down a runway. A deepfake bot is being used to “undress” underage girls |Karen Hao |October 20, 2020 |MIT Technology Review 

Following the Bikini Atoll Marshall Islands test in 1946, Louis Réard, a former automobile engineer, debuted the bikini, named for the site of the tests conducted only a month earlier. The Beauty Pageants Inspired by the Atomic Bomb |Fiona Zublin |October 15, 2020 |Ozy 

Fox News countered that women-in-bikini stories make up less than 1% of its annual digital content. It’s 2020, and CNN and Fox News are still battling over Comscore numbers |Steven Perlberg |July 31, 2020 |Digiday 

Fleshy breasts taunted him from low bikini tops, and fleshy thighs sloped from bikini bottoms. Powerful Congressman Writes About ‘Fleshy Breasts’ |Asawin Suebsaeng |January 7, 2015 |DAILY BEAST 

The new bikini under Perfectly Fit does not appear to go higher than a large. Let’s Get Rid of ‘Plus-Size’ for Good |Emily Shire |November 12, 2014 |DAILY BEAST 

If you Google your name, a lot of headlines come up like “Jessica Alba Sizzles in Bikini” and stuff of that nature. Jessica Alba on 'Sin City,' Typecasting, and How Homophobia Pushed Her Away From the Church |Marlow Stern |August 18, 2014 |DAILY BEAST 

She says her posts have changed from flirty Maxim-style bikini shots to controlled images from the waist up. Porn Stars Want to Know: Why Did Facebook Delete Me? |Aurora Snow |August 2, 2014 |DAILY BEAST 

The whole point of being a pageant queen is to trot around in your bikini to be ogled at while feigning sexual naiveté. Miss America Hypocrisy: The Vanessa Williams Nude Photo Shaming |Amanda Marcotte |July 23, 2014 |DAILY BEAST 

Following the pattern set by the Bikini tests, only a select score of press and radio representatives were admitted. The Brain |Alexander Blade 

The first of these surveys was conducted near Eniwetok and Bikini. Atoms, Nature, and Man |Neal O. Hines 

Bikini in 1946 was the scene of the first peacetime tests of atomic weapons. Atoms, Nature, and Man |Neal O. Hines 

Teams of scientists examined Bikini annually from 1946 to 1950 and from 1954 to 1958. Atoms, Nature, and Man |Neal O. Hines 

Bikini Atoll, scientists believed, needed only clearing and cultivation to make it once again suitable for human habitation. Atoms, Nature, and Man |Neal O. Hines