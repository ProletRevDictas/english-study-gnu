Its earthiness balances this salad of sweet, fresh peas, butter and bright mint. 5 hearty grains and how to use them in bowls, salads and even cake |Kari Sonde |February 25, 2021 |Washington Post 

This task for an ingredient like peas is done by nature, but others will require a bit of knife work. Potatoes aren’t the only vegetables you should be mashing |Aaron Hutcherson |January 20, 2021 |Washington Post 

Reduce the heat to low and simmer, partially covered, stirring occasionally, until the peas have lost their shape and the soup looks creamy, about 1 hour and 15 minutes. Leeks and fresh dill take cozy split pea soup from drab to fab |Ellie Krieger |January 6, 2021 |Washington Post 

Swap in beans, peas, or eggs for meats, and consume plenty of fruits, veggies, and whole grains. In 2021, as ever, the best diets are simple |Sara Chodosh |January 6, 2021 |Popular-Science 

They tasted at the big four — Inglenook, Beaulieu, Charles Krug, and Louis Martini — and lunched with a winery publicist on ravioli, chicken with mushrooms, and small, sweet spring peas. To Find Hope in American Cooking, James Beard Looked to the West Coast |John Birdsall |October 2, 2020 |Eater 

These days they are occasional meteorological irruptions, white river mists, not dense and toxic industrial pea-soupers. Sherlock Holmes Vs. Jack the Ripper |Clive Irving |November 16, 2014 |DAILY BEAST 

When we meet, Yeonmi is impeccably dressed in a red pea coat and heels, her long hair pulled back in a ponytail. How ‘Titanic ’Helped This Brave Young Woman Escape North Korea’s Totalitarian State |Lizzie Crocker |October 31, 2014 |DAILY BEAST 

“This poor guy has a pea coat on,” he says, pointing to a well-dressed youngster in the front row. Dan Malloy Is Progressives’ Dream Governor. So Why Isn’t He Winning? |David Freedlander |October 30, 2014 |DAILY BEAST 

“Pea Tear Griffon” is singing what goes up must come down while reading Nathaniel Hawthorne. Most Creative ‘Net Neutrality’ Comments on the FCC Website |Abby Haglage |June 9, 2014 |DAILY BEAST 

A couple weeks ago, I found a pea-sized shard of shrapnel from a past attack in a parking lot. Dodging Rockets in Afghanistan as the Taliban’s Fighting Season Begins |Nick Willard |May 14, 2014 |DAILY BEAST 

With this company he had rendered valiant service in the campaign which ended with the battle of Pea Ridge. The Courier of the Ozarks |Byron A. Dunn 

He is a Confederate soldier who was cruelly wounded at Pea Ridge, and found his way here. The Courier of the Ozarks |Byron A. Dunn 

Most of this work I did alone, leaving Mike free for other tasks, and Joe free to cut the pea brush. The Idyl of Twin Fires |Walter Prichard Eaton 

I have never seen a pea-green monkey before, and it strikes me you are quite gorgeous. The Tin Woodman of Oz |L. Frank Baum 

Not all at once did the pea-sticks become builders' scaffold-poles, the lines of string the plotting-out of streets. Mushroom Town |Oliver Onions