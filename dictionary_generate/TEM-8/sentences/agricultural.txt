In smaller cities, manual industries, like mining and agriculture, tend to account for a bigger share of the economy. What it takes for a city to jump into the knowledge economy |Karen Ho |September 9, 2020 |Quartz 

The only silver lining in India’s worst-ever quarterly GDP contraction was the agriculture sector. How climate change will hurt India’s already wounded economy |Prathamesh Mulye |September 8, 2020 |Quartz 

The general trends could best be characterized as high-volume and standardized agriculture. How the Supermarket Helped America Win the Cold War (Ep. 386 Rebroadcast) |Stephen J. Dubner |August 6, 2020 |Freakonomics 

This technology would spread across the world, affecting not just the demand side, supermarkets, but the agriculture supply side. How the Supermarket Helped America Win the Cold War (Ep. 386 Rebroadcast) |Stephen J. Dubner |August 6, 2020 |Freakonomics 

We really— we have to make agriculture green which is a strange, strange thing to say. How the Supermarket Helped America Win the Cold War (Ep. 386 Rebroadcast) |Stephen J. Dubner |August 6, 2020 |Freakonomics 

A Minnesota based agricultural/industrial construction company. The 26 Next Hobby Lobbys |Abby Haglage |December 17, 2014 |DAILY BEAST 

These agricultural pests migrate in mid-summer to the Rocky Mountains from Kansas and Nebraska to beat the heat. What It Takes to Kill a Grizzly Bear |Doug Peacock |November 23, 2014 |DAILY BEAST 

Carver was an agricultural and industrial pioneer—in more ways than one. Growth Stocks |The Daily Beast |October 17, 2014 |DAILY BEAST 

And when he died, another agricultural pioneer was just starting to bring research to bear on food production. Growth Stocks |The Daily Beast |October 17, 2014 |DAILY BEAST 

We may see hemp again become a major American agricultural product. The Chronic Chronicles: A History of Pot |Roger Roffman |July 6, 2014 |DAILY BEAST 

Virginia leaf still continues to flourish, and to-day it is the great agricultural product of the State. Tobacco; Its History, Varieties, Culture, Manufacture and Commerce |E. R. Billings. 

"It's dogged as does it," is not only the maxim of agricultural labourers in remote country districts. The Pit Town Coronet, Volume I (of 3) |Charles James Wills 

The duke was agricultural above all things; he had a model estate bristling with scientific improvement. Ancestors |Gertrude Atherton 

The use of the high-pressure steam agricultural engine was not confined to Cornwall. Life of Richard Trevithick, Volume II (of 2) |Francis Trevithick 

I am so young and inexperienced, and so ignorant of agricultural matters, I should make a poor farmer. The World Before Them |Susanna Moodie