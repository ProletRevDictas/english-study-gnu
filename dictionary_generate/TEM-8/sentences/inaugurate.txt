He had the map framed and brought into the West Wing shortly after he was inaugurated. House to vote on removing GOP’s Marjorie Taylor Greene from her committees |Felicia Sonmez, John Wagner, Colby Itkowitz |February 4, 2021 |Washington Post 

Even if you didn’t vote for the president being inaugurated, it can feel like you’re watching history being made as the new president is sworn in. A gov’t that represents all Americans gives us hope |Kathi Wolfe |January 29, 2021 |Washington Blade 

The first president inaugurated in Washington was Thomas Jefferson in 1801. A presidential inauguration for the history books |Marylou Tousignant |January 20, 2021 |Washington Post 

Until the 1930s, incoming presidents weren’t inaugurated until March 4 of the next year, so Washington had an even longer lame-duck period than presidents do now. At the nation’s first presidential transfer of power, George Washington was ‘radiant’ |Gillian Brockell |January 19, 2021 |Washington Post 

A new president will be inaugurated Wednesday in a city with areas under military lockdown. After Capitol attack, social studies and civics teachers struggle with real-time history lessons |Joe Heim, Valerie Strauss |January 19, 2021 |Washington Post 

The hiring seemed to inaugurate a détente between Rand Paul and Mitch McConnell. Top Aide to Mitch McConnell Linked to Shady Deal |Ben Jacobs |August 29, 2014 |DAILY BEAST 

Sweden was the first European country to inaugurate a dedicated LGBT retirement facility, which was opened in Stockholm in 2013. Spain Is Getting Its First LGBT Retirement Center |Barbie Latza Nadeau |April 20, 2014 |DAILY BEAST 

Each time we gather to inaugurate a president, we bear witness to the enduring strength of our Constitution. Full Text of President Obama's Inaugural Address |Justin Green |January 21, 2013 |DAILY BEAST 

But the reality of the world is that the media rarely inaugurate such probes. Romney on Abortion, Then and Now |Michael Tomasky |October 10, 2012 |DAILY BEAST 

In line with this growth, the Peruvian author is the first Nobel laureate invited to inaugurate the fair. Welcome to the World Book Capital |Joey Rubin |April 25, 2011 |DAILY BEAST 

Under such auspices dawned the year 1861, destined to inaugurate a new epoch in the life of Tchaikovsky. The Life &amp; Letters of Peter Ilich Tchaikovsky |Modeste Tchaikovsky 

Make the work efficient, though it be limited to a small number, rather than inaugurate a magnificent failure. Thoughts on Educational Topics and Institutions |George S. Boutwell 

Anyway she knows that persecution will result, and she has persuaded Mrs. Endicott to inaugurate it. The Art of Disappearing |John Talbot Smith 

I should have been glad to inaugurate in Boston, during the last six years, several important industrial movements. The College, the Market, and the Court |Caroline H. Dall 

The consequences of this blow were momentous; it may be said to inaugurate the ghetto period. Encyclopaedia Britannica, 11th Edition, Volume 15, Slice 4 |Various