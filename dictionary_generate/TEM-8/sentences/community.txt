The community does not yet have adequate testing, contact tracing, or isolation. I’m an epidemiologist and a dad. Here’s why I think schools should reopen. |Benjamin P. Linas |July 9, 2020 |Vox 

Sky glow is a term that’s already in use in the light pollution community, so that’s not my favorite term. The Scientist Leading the World’s Aurora Hunters |Robin George Andrews |July 9, 2020 |Quanta Magazine 

She sought input from various community stakeholders, many of whom had been rankled by her appointment to lead the police division. There’s little evidence showing which police reforms work |Sujata Gupta |July 9, 2020 |Science News 

Organizations like his try to do outreach and help convert messaging into something that resonates with underrepresented communities, but they are stretched thin, especially with the coronavirus pandemic and recent racial justice movement. Few Latino Residents Are Applying to Take Part in Redistricting |Andrew Keatts and Maya Srikrishnan |July 8, 2020 |Voice of San Diego 

She last wrote for Eater about the rise of community fridges across the country. Who Will Save the Food Timeline? |Dayna Evans |July 8, 2020 |Eater 

We have thousands of users who identify themselves as transgendered and they are welcome members of the Grindr community. Grindr’s Trans Dating Problem |David Levesley |January 9, 2015 |DAILY BEAST 

Some gay apps, like the newer Mister, have not subscribed to the community/tribe model. Grindr’s Trans Dating Problem |David Levesley |January 9, 2015 |DAILY BEAST 

What matters is being honest, humble, and a faithful and loyal friend, father and member of your community. Abramoff’s Advice for Virginia’s New Jailhouse Guv |Tim Mak, Jackie Kucinich |January 7, 2015 |DAILY BEAST 

The need for increased community policing is more urgent than ever before. How to Solve the Policing Crisis |Keli Goff |January 5, 2015 |DAILY BEAST 

Marrying another Jew was not just a personal simcha (joy), but one for the community. My Week on Jewish Tinder |Emily Shire |January 5, 2015 |DAILY BEAST 

But hitherto, before these new ideas began to spread in our community, the mass of men and women definitely settled down. The Salvaging Of Civilisation |H. G. (Herbert George) Wells 

I doubt if the modern community can afford to continue it; it certainly cannot afford to extend it very widely. The Salvaging Of Civilisation |H. G. (Herbert George) Wells 

And could it not be extended from its present limited range until it reached practically the whole adolescent community? The Salvaging Of Civilisation |H. G. (Herbert George) Wells 

It was not, however, through any of these artificial means that real relief was brought to the community. Readings in Money and Banking |Chester Arthur Phillips 

In the community her father was the wealthiest man, having made his fortune in the growing of potatoes and fruit. The Homesteader |Oscar Micheaux