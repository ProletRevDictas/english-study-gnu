In fact, it's getting so bad that a number of OEMs, including Ford and General Motors, have had to go as far as idling shifts and even entire factories. A silicon chip shortage is causing automakers to idle their factories |Jonathan M. Gitlin |February 4, 2021 |Ars Technica 

Virginia officials said they are temporarily pausing deactivation of idle E-ZPass accounts amid the coronavirus pandemic. Virginia pauses deactivation of idle E-ZPass accounts amid pandemic |Justin Wm. Moyer |February 3, 2021 |Washington Post 

They have been both overstretched in some departments, and rendered idle in others, and although in some cases they have seen increases in patient volume due to coronavirus outbreaks, they have by and large suffered significant financial losses. Covid-19 could teach US hospitals some lessons, if they’re willing to listen |Annalisa Merelli |January 28, 2021 |Quartz 

Officials hope that makeshift system will free up ambulances now sitting idle outside hospitals because their patients can’t be admitted. Los Angeles is running out of oxygen for patients as covid hospitalizations hit record highs nationwide |Fenit Nirappil, William Wan |January 5, 2021 |Washington Post 

A rare exception came early in the pandemic in March, when an apparel returns warehouse was idled by order of Kentucky Governor Andy Beshear after an outbreak there. Amazon closes New Jersey warehouse after spike in COVID-19 cases |Rachel King |December 21, 2020 |Fortune 

And in the summer, when the lift is idle, it feeds juice into the local community. Solar Powered Ski Lift |The Daily Beast |November 24, 2014 |DAILY BEAST 

Middle-aged, out-of-shape Putin sat idle and silent as his dreams and hopes for the future were destroyed. How the Fall of the Berlin Wall Radicalized Putin |Masha Gessen |November 9, 2014 |DAILY BEAST 

Any upcoming release of a new Apple product guarantees a deafening cacophony of idle speculation from tech sites. Amateur Stuntmen, the iPhone 6, and More Viral Videos |Jack Holmes |August 30, 2014 |DAILY BEAST 

The rest of the police looked on, sitting idle on horses in front of Starbucks. New York Solidarity as Michael Brown Protests Go National |Gideon Resnick |August 15, 2014 |DAILY BEAST 

The idle rich had driven down from Biarritz with their uniformed chauffeurs. Is This Hemingway’s Pamplona or a Lot of Bull? |Clive Irving |July 13, 2014 |DAILY BEAST 

It's an idle question, I know; wise men and musty philosophers say that regrets are foolish. Raw Gold |Bertrand W. Sinclair 

He worketh under correction, and seeketh to rest: let his hands be idle, and he seeketh liberty. The Bible, Douay-Rheims Version |Various 

There was a deep silence throughout the whole bivouac; some were sleeping, and those who watched were in no humour for idle chat. Blackwood's Edinburgh Magazine, No. CCCXXXIX. January, 1844. Vol. LV. |Various 

While the fortress was undermining at home, they were not idle, who were preparing to storm it from abroad. The Pastor's Fire-side Vol. 3 of 4 |Jane Porter 

The lower class were idle and lazy, and willing to serve any sovereign who appealed to them by ostentation. Napoleon's Marshals |R. P. Dunn-Pattison