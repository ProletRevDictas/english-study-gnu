That’s because people who are overweight or obese have more body fat — which is more hospitable than other tissue to the coronavirus — and they suffer from reduced lung capacity. The Health 202: Obese Americans could be prioritized for coronavirus vaccine |Paige Winfield Cunningham |November 30, 2020 |Washington Post 

We also pointed out that many types of microbes are extremely hardy and can readily survive the vacuum of space by going dormant until they are in a hospitable environment. Readers ask about life on Venus and high-energy cosmic rays |Science News Staff |November 29, 2020 |Science News 

Part of the surge was expected, as colder weather pushed people indoors into environments more hospitable to spreading the virus. Coronavirus cases are soaring in the D.C. region. Experts say the worst is yet to come. |Erin Cox, Julie Zauzmer |November 22, 2020 |Washington Post 

Initially we were doing takeout and delivery that was extremely no contact and figuring ways to make the experience personal and hospitable within the framework. How to Bring This Portland Restaurant’s Colorful Outdoor Oasis to Your Home |Emma Orlow |October 30, 2020 |Eater 

Meaning, you create a hospitable environment, then pass the functioning within that environment to her. Carolyn Hax: To summarize, you want to exclude her because she’s disabled? |Carolyn Hax |October 29, 2020 |Washington Post 

In return we lent the hospitable Post our halftones, and they adorned its first city edition next morning. The Stacks: H.L. Mencken on the 1904 Baltimore Fire |H.L. Mencken |October 4, 2014 |DAILY BEAST 

To make the environment more hospitable, Abramson made sure they had plenty of snacks. Jill Abramson Talks Obama Secrecy and Her New York Times Firing |Eleanor Clift |July 10, 2014 |DAILY BEAST 

I spent a month doing AIDS work in Uganda among lovely, hospitable people, who had they known I was gay, would have driven me out. Chill Out About Firefox, Everybody |Gene Robinson |April 5, 2014 |DAILY BEAST 

But what those hospitable South Carolinians have not heard about is Jay Stamper. Prankster or Politician? Jay Stamper Says His S.C. Senate Bid Is Real |David Freedlander |October 29, 2013 |DAILY BEAST 

We have a culture that has lost engagement with the process of serving food to people in a hospitable way. If Cosi Wants to Make a Profit, It Needs to Increase Wages |Daniel Gross |August 22, 2013 |DAILY BEAST 

He was a very tall man, and he had a very tall and hospitable daughter, nearly as big as himself, who received us very cordially. Music-Study in Germany |Amy Fay 

Gwynne looked more than hospitable as he ran down the veranda steps to assist his guests out of the high buggy. Ancestors |Gertrude Atherton 

We spent a very happy day at the hospitable country house of Mr. Wardrope, and our cavalcade to the town at night was delightful. Journal of a Voyage to Brazil |Maria Graham 

Val played the hospitable host; but there was a shadow on his face that his wife did not fail to see. Elster's Folly |Mrs. Henry Wood 

The rulers of his country have always been hospitable and favourably inclined towards my family. The Double Four |E. Phillips Oppenheim