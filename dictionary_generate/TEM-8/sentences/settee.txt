An with that I laid down on the settee, an felt orful bad, an the more I tho't about it, the wus I felt. The Book of Anecdotes and Budget of Fun; |Various 

"It's good of you to drop in," he said, after he had fixed me in his own comfortable chair and drawn up the settee for himself. The Soldier of the Valley |Nelson Lloyd 

Drawing the settee closer to the light, he opened the great volume across his knees and adjusted his spectacles. The Soldier of the Valley |Nelson Lloyd 

I have forgotten a companionable cat who each morning takes her seat on the long leather settee beside me and shares my crescents. The Real Latin Quarter |F. Berkeley Smith 

She avoided his ardent gaze, but he moved to the settee beside her and looked into the bewitching face. Dope |Sax Rohmer