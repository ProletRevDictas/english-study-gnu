The week after Rebecca Grant took away her kids’ video games for a month, after a year of relaxed pandemic rules, her 10-year-old son was livid. After pandemic free-for-all, parents struggle to reinstate screen-time rules |Heather Kelly |June 24, 2021 |Washington Post 

Many faculty at UNC and others who admire the work of Hannah-Jones are livid at what they see as a failure to properly recognize an authoritative figure in the field of journalism, one with close ties to Chapel Hill. UNC faculty upset that prizewinning 1619 Project journalist won’t have tenure when she starts teaching at Chapel Hill |Nick Anderson |May 20, 2021 |Washington Post 

The fact that they did this without giving us any warning, we were livid. A huge explosion cracked house foundations in New Hampshire. An ‘extreme’ gender-reveal party was to blame. |Andrea Salcedo |April 23, 2021 |Washington Post 

Many who need medical care that depends on electricity are in dire conditions — and livid at how unprepared the state was for the ice and snow. Texas is frozen and in crisis. A furniture store owner has emerged as a hero — again. |Cathy Free |February 18, 2021 |Washington Post 

She thought about her research and was suddenly, absolutely livid. Our Radicalized Republic |Maggie Koerth (maggie.koerth-baker@fivethirtyeight.com) |January 25, 2021 |FiveThirtyEight 

But a group of livid fans—over 45,000 of them, actually—are still lobbying to “Bring Beth Back!” ‘The Walking Dead’ Fans Demand: Bring Back Beth! |Melissa Leon |December 11, 2014 |DAILY BEAST 

While this will be some comfort to the Queen, she will likely still be livid at the news. Queen's Horse Tests Positive For Morphine |Tom Sykes |July 22, 2014 |DAILY BEAST 

Opie is devastated, Anthony is unrepentant, and their fans are livid and seeking revenge. Fans Rage Over Opie Minus Anthony |Lloyd Grove |July 8, 2014 |DAILY BEAST 

He was “livid” because “I was better than most of the guys they were picking.” Peyton Manning Vs. Richard Sherman |Allen Barra |January 31, 2014 |DAILY BEAST 

Now, imagine a speech that had excited Democrats, that had had something surprising in it, something that made Republicans livid. Will Obama Ever Play Hardball? |Michael Tomasky |January 29, 2014 |DAILY BEAST 

She pointed hastily to some livid bruises upon her neck and arms, and continued with great rapidity. Oliver Twist, Vol. II (of 3) |Charles Dickens 

For a moment Colonel Jennison was too astonished to speak; then his face turned livid with passion. The Courier of the Ozarks |Byron A. Dunn 

She watched the colour fade from his cheeks, and the ugly, livid hue that spread in its room to his very lips. St. Martin's Summer |Rafael Sabatini 

It is pleasant to look at the smiling, cheerful old Beguine, and think no more of yonder livid face. Little Travels and Roadside Sketches |William Makepeace Thackeray 

Crushed by that bolt from the blue, Richard sat as if stunned, the flush receding from his face until his very lips were livid. Mistress Wilding |Rafael Sabatini