The announcement comes as the carrier successfully completed a test with Corning to deploy a private 5G network inside a laboratory. Verizon plans to offer indoor 5G networks by year-end |Aaron Pressman |September 16, 2020 |Fortune 

Though Verizon is the largest wireless carrier overall, with 116 million regular monthly subscribers, it has only 4 million prepaid customers. Verizon is buying TracFone Wireless for $6.25 billion |radmarya |September 14, 2020 |Fortune 

That would have meant widespread testing to identify those who had caught the virus, quarantining and tracing the contacts of both symptomatic and asymptomatic carriers who could spread the disease to the most vulnerable. America Is About to Lose Its 200,000th Life to Coronavirus. How Many More Have to Die? |by Stephen Engelberg |September 14, 2020 |ProPublica 

In more than a half-dozen cases, the carriers persuaded a judge to throw out the lawsuit, in part because the court agreed that a property insurance policy can’t be invoked if there is no property damage. Got interruption insurance? These companies found it’s useless in the age of COVID-19 |Bernhard Warner |September 12, 2020 |Fortune 

Your lender will need the name and contact information of your insurance carrier before completing your loan. Determining your insurance needs |Valerie Blake |September 12, 2020 |Washington Blade 

The vaccine is delivered through a “carrier virus” that causes a common cold in chimpanzees but does not affect humans. The Race for the Ebola Vaccine |Abby Haglage |January 7, 2015 |DAILY BEAST 

The airplane was owned by an Indonesian budget carrier, Lion Air. Who Will Get AsiaAir 8501’s Black Boxes? |Clive Irving |December 30, 2014 |DAILY BEAST 

Riffing off the slogan “Now Everyone Can Fly,” the carrier offered no-frills flights that were both cheap and plentiful. The Presumed Crash of AirAsia Flight QZ8501 Is Nothing Like MH370 |Lennox Samuels |December 29, 2014 |DAILY BEAST 

AirAsia, on the other hand, is a relatively new carrier, an upstart in the tradition of Southwest Airlines in the United States. The Presumed Crash of AirAsia Flight QZ8501 Is Nothing Like MH370 |Lennox Samuels |December 29, 2014 |DAILY BEAST 

The NYPD remained his ultimate goal as he went to work as a carrier for Airborne Express/DHL and then as a school safety officer. In The Shadow of Murdered Cops |Michael Daly |December 26, 2014 |DAILY BEAST 

She repeated the brief phrases, as well as she could recall them, to a Eurasian whom she found acting as a water-carrier. The Red Year |Louis Tracy 

And the same goes for any other common carrier—the railroads, bus service, and airlines. Hooded Detective, Volume III No. 2, January, 1942 |Various 

Louis the Goon Engel was a mere walk-on in the piece, a spear-carrier doomed to death. Hooded Detective, Volume III No. 2, January, 1942 |Various 

Instead of being a destroyer of merchandise, this new craft was an unarmed carrier of merchandise. The Wonder Book of Knowledge |Various 

But it's all right now—they'll throw the letters into the mail-carrier's bag—there'll be many of them—this is general letter day. Prison Memoirs of an Anarchist |Alexander Berkman