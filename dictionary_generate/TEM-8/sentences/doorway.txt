To do a door-frame row, which targets your arms and back, stand in front of an open doorway. Indoor exercises to prepare you for hiking the great outdoors |Pam Moore |January 27, 2021 |Washington Post 

This suggests that a dirty doorway keeps hornets from trespassing on the hive. Honeybees fend off deadly hornets by decorating hives with poop |Asher Jones |January 19, 2021 |Science News For Students 

Now they bring visions of congressional staffers huddling under a table, angry men tossing a journalist over a barrier and a police officer crying out in pain as a chaotic crowd crushes him in a doorway. The riot is over, but fear remains in the nation’s capital |Theresa Vargas |January 13, 2021 |Washington Post 

The photographer said he thought the image was taken in a doorway near the northwest corner of the Capitol. Several Capitol police officers suspended, more than a dozen under investigation over actions related to rally, riot |Aaron Davis, Rebecca Tan, Beth Reinhard |January 12, 2021 |Washington Post 

He couldn’t resist talking to a neighbor at his doorway when the building went off pandemic lockdown a few weeks ago. The Long Goodbye: How COVID-19 Took My Dad’s Life |Randy Dotinga |January 11, 2021 |Voice of San Diego 

Black and purple bunting went up over the doorway at the 84th Precinct stationhouse where Ramos and Liu had been assigned. 'Please Don't Die!': The Frantic Battle to Save Murdered Cops |Michael Daly |December 22, 2014 |DAILY BEAST 

The man pleaded with them while his wife stood in the doorway of the bus holding their baby. On the Bus: Ukraine’s Frontline Express Across the Battle Lines |Ted Phillips |September 8, 2014 |DAILY BEAST 

Looking for a place to get a salad, we pass a gift shop with a rack of dresses near the doorway. The Stacks: The Inimitable Albert Brooks Caught at the Dawn of His Movie Career |Paul Slansky |April 13, 2014 |DAILY BEAST 

He pushed past a female curate and raced towards the exit, but Father Andrew Cain got to the doorway first. Meet the Gay Priest Getting Married |Nico Hines |March 25, 2014 |DAILY BEAST 

He is standing in a doorway looking at his wife, who is sitting in front of a computer wearing a telephone headset. The Stacks: The True Greatness of Muhammad Ali |Peter Richmond |February 23, 2014 |DAILY BEAST 

He returned shortly, to meet his mother standing in the doorway, with pale, affrighted face. Ramona |Helen Hunt Jackson 

Father Gaspara was about to ask another question, when Ramona appeared in the doorway, flushed with running. Ramona |Helen Hunt Jackson 

Then a fat, untidy old man appeared in the doorway of a cubicle within the shop, and Edwin Clayhanger blushed. Hilda Lessways |Arnold Bennett 

Goodell paused in the doorway and emitted a whistle of surprise at sight of a horse in one of the stalls. Raw Gold |Bertrand W. Sinclair 

And saying a hasty good afternoon, he popped through his doorway and vanished at Billy Woodchuck's feet. The Tale of Grandfather Mole |Arthur Scott Bailey