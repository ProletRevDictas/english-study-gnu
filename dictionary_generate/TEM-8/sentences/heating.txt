With Ebola still raging in West Africa, the race to find a vaccine is heating up. The Race for the Ebola Vaccine |Abby Haglage |January 7, 2015 |DAILY BEAST 

We have no heating; there is no gas and it is very cold inside the prison. An American Marine in Iran’s Prisons Goes on Hunger Strike |IranWire |December 18, 2014 |DAILY BEAST 

It is provided with best conditions for water supply, heating and ventilation. North Korea's Top College: Brainwash U |Justin Rohrlich |December 13, 2014 |DAILY BEAST 

And, always, global warming could push the cutworm moths north, out of the park, by heating up the region. What It Takes to Kill a Grizzly Bear |Doug Peacock |November 23, 2014 |DAILY BEAST 

Winds can either compress other clouds, heating them up and making stars, or break those clouds up. The Supermassive Black Hole Smokescreen |Matthew R. Francis |June 22, 2014 |DAILY BEAST 

I gathered that he thought something of the boy, and was heating up to the door-smashing stage. Fee of the Frontier |Horace Brown Fyfe 

The greatest part of the waste steam is condensed in heating the water to fill the boiler; what escapes is a mere nothing. Life of Richard Trevithick, Volume II (of 2) |Francis Trevithick 

Now it appears that the heating of Binner Downs 300 surface feet gave a saving of 6000 lbs. Life of Richard Trevithick, Volume II (of 2) |Francis Trevithick 

In this engine the proportion of saving by the heating flues was the same as in the large engine. Life of Richard Trevithick, Volume II (of 2) |Francis Trevithick 

The steady heating of an oak branch on the porch roof told me it was blowing hard. The Soldier of the Valley |Nelson Lloyd