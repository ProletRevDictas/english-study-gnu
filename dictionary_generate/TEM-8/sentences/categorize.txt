It’s the largest asteroid in the solar system, so large that it’s categorized as a dwarf planet. The 5 best places to explore in the solar system—besides Mars |Neel Patel |August 17, 2020 |MIT Technology Review 

It’s been named and it’s been categorized, and they’re taking steps to prepare. Hurricanes have names. Some climate experts say heat waves should, too |Jack J. Lee |August 14, 2020 |Science News 

Human involvement is still crucial, of course, by rating categorizing the leads based on your criteria, you can train your data further. How AI can supercharge performance marketing |Tereza Litsa |August 12, 2020 |Search Engine Watch 

This way all your keywords are categorized based on the groupings that align with your site categories. How search data can inform larger online business decisions |Sebastian Compagnucci |August 5, 2020 |Search Engine Land 

Links that added value to content but couldn’t be vouched for were categorized as nofollow links. What Google says about nofollow, sponsored, and UGC links in 2020: Does it affect your SEO rankings? |Joseph Dyson |July 24, 2020 |Search Engine Watch 

I was never like, “I wonder how people are going to categorize this?” The Rise of Jack Antonoff, the Taylor Swift Whisperer |Kevin Fallon |November 14, 2014 |DAILY BEAST 

Is making art when you feel inspired to make it something that you need to categorize? The Rise of Jack Antonoff, the Taylor Swift Whisperer |Kevin Fallon |November 14, 2014 |DAILY BEAST 

Since there's a near infinite number of possible exercises, Amiigo doesn't bother trying to categorize them ahead of time. Amiigo Tracker Pairs Data With Deadlifts |Gregory Ferenstein |August 4, 2014 |DAILY BEAST 

After the workout, I can re-categorize "full body burst" as "deadlift." Amiigo Tracker Pairs Data With Deadlifts |Gregory Ferenstein |August 4, 2014 |DAILY BEAST 

Urologists categorize diphallia into three groups based on how much is duplicated. This Man Claims to Have Two Penises, Science Confirms It’s Possible |Kent Sepkowitz |January 3, 2014 |DAILY BEAST 

I think you mentioned before that he had wanted to put you in a category, categorize you. Warren Commission (2 of 26): Hearings Vol. II (of 15) |The President's Commission on the Assassination of President Kennedy 

Is he what you would categorize as polite in his answers or not? Warren Commission (7 of 26): Hearings Vol. VII (of 15) |The President's Commission on the Assassination of President Kennedy