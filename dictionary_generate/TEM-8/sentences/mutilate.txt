A military defector codenamed “Caesar,” and “Sami,” a relative who assisted him, smuggled thousands of photos out of Syria showing mutilated, tortured, and emaciated corpses strewn across the floors of Syrian government detention centers. I Helped Bring a Syrian War Criminal to Justice. But the Work Is Just Beginning |Mazen Darwish |February 17, 2022 |Time 

After the man’s body was recovered, his mutilated left leg probably detached and was placed on his chest when he was buried, the researchers say. A partial skeleton reveals the world’s oldest known shark attack |Bruce Bower |July 23, 2021 |Science News 

The thing with translators is that you have to cut off the oratory, mutilate the ideas to give them time to do their work. Locked up in the Land of Liberty: Part III |Yariel Valdés González |July 21, 2021 |Washington Blade 

False rumors swirled that Hughes had mutilated Pearl Farlow’s throat and breasts, sending the mob into a murderous rage. In Texas, a struggle to memorialize a brutal lynching as resistance grows to teaching historical racism |Sydney Trent |June 3, 2021 |Washington Post 

Their world, like Paul’s, was full of monsters determined to abuse, kidnap and mutilate kids. Rand Paul’s ignorant questioning of Rachel Levine showed why we need her in government |Monica Hesse |February 26, 2021 |Washington Post 

Prisoners there became severely depressed: some began to compulsively mutilate themselves; others attempted suicide. The One Photo a Prisoner Wants to See |Isabel Wilkinson |May 6, 2013 |DAILY BEAST 

With cynical cruelty, he set himself to insult, to undermine, to mutilate it. The Child of Pleasure |Gabriele D'Annunzio 

It would not mutilate and disfigure the body, for it is a sacred temple, to be made beautiful and attractive. Beacon Lights of History, Volume VII |John Lord 

You mutilate my brain with your clumsy pincers—you put your claws into my thoughts and tear them to pieces! Creditors; Pariah |August Strindberg 

Not only does the censorship mutilate literary works, but it often suffocates the inspiration of the author. Contemporary Russian Novelists |Serge Persky 

This dispatch, too, the House of Commons took care to mutilate before sending it to the press. The Digger Movement in the Days of the Commonwealth |Lewis H. Berens