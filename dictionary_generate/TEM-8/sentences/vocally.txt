Coates Ellis wants to “redefine” the role of mayor into one that is more vocal on social media about constituent services and marketing the city. In blue N.Va., Democrats eye a mayoral seat long held by the GOP |Antonio Olivo |October 28, 2020 |Washington Post 

One of those unsuccessful Democratic Council runs in 2012 was by Mat Kostrinsky, who worked for Bry’s campaign in the primary and who continues to be a vocal supporter. Voice Poll Reveals Tight Mayoral Race, Messy Post-GOP Future for Dems |Andrew Keatts |October 27, 2020 |Voice of San Diego 

She’s also been vocal about the need to reopen schools amid the pandemic. Sacramento Report: San Diego’s Two Most Competitive State Races |Sara Libby |October 23, 2020 |Voice of San Diego 

Democrats have also been vocal about the consequences of having a majority conservative Supreme Court since Barrett’s nomination happening just one week after the passing of Ruth Bader Ginsburg. Senate Judiciary Committee Decides To Move Forward With Amy Coney Barrett Nomination |Kirsten West Savali |October 22, 2020 |Essence.com 

While a vocal subset of its conservative critics in Congress might disagree, Facebook’s real problems are about what stays online — not what gets taken down. Facebook’s controversial Oversight Board starts reviewing content moderation cases |Taylor Hatmaker |October 22, 2020 |TechCrunch 

“I was terrifyingly aware of the fact that my father was vocally against that lifestyle when I came out,” Hayward tells me. Thank God! To the Church, This Transgender Woman Is Just a Skank |Emily Shire |October 22, 2014 |DAILY BEAST 

Until then, I will continue to do my part by vocally denouncing those who claim they are acting in the name of my faith. Why Muslims Hate Terrorism More |Dean Obeidallah |August 12, 2014 |DAILY BEAST 

But while lawmakers vocally opposed to the deal were scarce, it faced some criticism in the think tank world. It’s a Miracle! Congress Compromises on VA Reform Bill |Tim Mak |July 29, 2014 |DAILY BEAST 

But what made The Beatles and The Beach Boys so spectacular vocally was that they could vanish into each other with their voices. The Band’s ‘Rock of Ages’ Is the Greatest Live Album Ever |Andrew Romano |October 14, 2013 |DAILY BEAST 

Hamas has vocally opposed the return to negotiations and the fact they were not invited to the party. Palestinians 'Everywhere' to Vote on Any Deal with Israel |Maysoon Zayid |September 9, 2013 |DAILY BEAST 

Whatever argument can be employed to establish the propriety of engaging vocally in any religious service is here available. The Ordinance of Covenanting |John Cunningham 

They conversed by means of thought impulses, and were neither capable of making a sound vocally nor of hearing one uttered. The Jameson Satellite |Neil Ronald Jones 

To render vocally or even to understand Kipling requires some appreciation of the peculiarities of the monologue. Browning and the Dramatic Monologue |S. S. Curry 

And visit the house of each member, and exhort them to pray vocally and in secret, and attend to all family duties. History of the Church of Jesus Christ of Latter-Day Saints |Joseph Smith 

And visit the house of each member, exhorting them to pray vocally and in secret, and attend to all family duties. History of the Church of Jesus Christ of Latter-Day Saints |Joseph Smith