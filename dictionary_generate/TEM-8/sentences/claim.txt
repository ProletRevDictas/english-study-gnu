Before the 2016 election and, from a bigger platform, before the 2020 election, he amplified unfounded claims about the flaws in the electoral system to try to backstop his potential electoral loss. What happens to an entrenched two-party system when one party undermines the system? |Philip Bump |February 12, 2021 |Washington Post 

To be sure, Brooks recognizes the “otherization” at play when officers peremptorily reject claims that racial bias insinuates itself into policing decisions by reducing 7-D residents to “animals.” A Georgetown professor trades her classroom for a police beat |Ronald S. Sullivan Jr. |February 12, 2021 |Washington Post 

Those claims came into question on Thursday, when the Associated Press reported that leaders of the organization were notified in June of at least 10 harassment claims against Weaver, including two involving Lincoln Project employees. Lincoln Project tweeted a co-founder’s private messages after leaders promised to probe sexual harassment claims |Andrea Salcedo |February 12, 2021 |Washington Post 

A HUD official told The Washington Post that Thursday’s move expands the universe of people who can file a fair-housing complaint because individuals will no longer have to make a nonconformity allegation in discrimination claims. HUD expands fair housing protections for transgender people |Tracy Jan |February 11, 2021 |Washington Post 

If it stops you, maybe you should switch to DEET, sunscreen, or Johnson Wax, as the users of these products make the same claim. 20 ingenious uses for WD-40 |By Bill Heavey/Field & Stream |February 11, 2021 |Popular-Science 

Defenders of the status quo claim the old rules protect consumers. Why Do ‘Progressives’ Want to Ban Uber and AirBnB? |Adam Thierer, Christopher Koopman |December 30, 2014 |DAILY BEAST 

How the hell does somebody show up at a David Duke organized event in 2002 and claim ignorance? No. 3 Republican Admits Talking to White Supremacist Conference |Tim Mak |December 30, 2014 |DAILY BEAST 

The claim is one of a series of allegations made in a controversial documentary that the BBC has now pulled. Pulled Documentary Says William Felt ‘Used’ by Charles’ Push for Camilla |Tom Sykes |December 30, 2014 |DAILY BEAST 

They were called La Red Avispa (The Wasp Network) and claim to have successfully foiled a number of threats against the island. Of Cuban Spies, a Baby, and a Filmmaker: The Strange Tale of the Cuban Five |Nina Strochlic |December 28, 2014 |DAILY BEAST 

The FBI and the President may claim that the Hermit Kingdom is to blame for the most high-profile network breach in forever. No, North Korea Didn’t Hack Sony |Marc Rogers |December 24, 2014 |DAILY BEAST 

The purchasers found that this claim was not well founded, and sought to recover their money. Women in the fine arts, from the Seventh Century B.C. to the Twentieth Century A.D. |Clara Erskine Clement 

I claim that it contains many errors of fact, and the Higher Criticism supports the claim; as we shall see. God and my Neighbour |Robert Blatchford 

He took mental inventory of his possessions and what he could lay claim to, and he happened to think about his wife's homestead. The Homesteader |Oscar Micheaux 

Having thus enrolled himself as one of the Agency Indians, he had a claim on this the Agency doctor. Ramona |Helen Hunt Jackson 

Who was he, indeed, that he should claim the right to thwart another's happiness, hinder another's best self-realisation? The Wave |Algernon Blackwood