My cousin reciprocated by mailing me a fairly expensive gift card to a restaurant where I rarely dine. Miss Manners: Sorry, receptionists aren’t going to remember your name |Judith Martin, Nicholas Martin, Jacobina Martin |February 5, 2021 |Washington Post 

Iowa State has not traditionally been a college football program that reciprocates heightened expectations with on-field success. The Secret To Iowa State’s Improbable Season? Balance. |Josh Planos |December 15, 2020 |FiveThirtyEight 

Every chance she got, she would playfully swat at Luna with her front paws, and expect Luna to reciprocate. ‘The Mozart of fungi’: For ages, truffle hunting has been one of the most challenging pursuits on earth. Then the pandemic hit |Bernhard Warner |December 12, 2020 |Fortune 

There is a state compact where they will reciprocate and recognize parts of their license or education, but you still usually have to go through a process and it takes weeks even if they are part of that compact. A depleted workforce and no end in sight: An inside look at America’s ailing health care industry |Erika Fry |December 8, 2020 |Fortune 

China quickly reciprocated by sanctioning 11 prominent Americans. The world’s money transfer system is China’s Achilles heel in its sanctions battle against the US |Mary Hui |August 19, 2020 |Quartz 

“She was always aloof, quiet, and never put out any effort to reciprocate,” Wall said. Utah’s Murderer Mom Is a Monster but She’s Not the First |Steve Miller |April 16, 2014 |DAILY BEAST 

And world powers said they were ready to reciprocate, if Iran gave significant assurances. Western and Iranian Diplomats Upbeat after Swiss Talks on Security Issues |Ali Gharib |October 15, 2013 |DAILY BEAST 

The Israelis reciprocate with unique intelligence assistance. The Not So Special U.S.-Israel Relationship |Andrew Apostolou |October 29, 2012 |DAILY BEAST 

They treat the Yemenis with contempt, and the Yemenis reciprocate. Yemen on the Brink |Bruce Riedel |September 24, 2011 |DAILY BEAST 

When we meet in debates, Karl is cordial—even genial—and I do my best to reciprocate. Karl Rove’s Gift to the Dems |Paul Begala |June 30, 2011 |DAILY BEAST 

This dedication is very precious, as indicating your regard for me, and on my part I reciprocate your feeling. The Life &amp; Letters of Peter Ilich Tchaikovsky |Modeste Tchaikovsky 

I can assure you it would have been very pleasant and interesting, for I, too, warmly reciprocate your sympathy. The Life &amp; Letters of Peter Ilich Tchaikovsky |Modeste Tchaikovsky 

To-day we reciprocate by sending Western farmers with capital into the Canadian Northwest. The Old World in the New |Edward Alsworth Ross 

I must, however, allow that Mr. Henson is a courteous disputant, and I hope I shall reciprocate his good feeling. Flowers of Freethought |George W. Foote 

Hence, there was always some truth in the charge of his friends that he failed to reciprocate their devotion with his favors. Abraham Lincoln, Volume 2 (of 2) |William H. Herndon