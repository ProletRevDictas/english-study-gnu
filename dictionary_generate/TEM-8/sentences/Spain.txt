He also holds a BS in Computer Engineering from Virginia Tech and an MBA from The IE Business School in Spain, according to the Northzone website. Sources: Lightspeed is close to hiring a new London-based partner to put down further roots in Europe |Steve O'Hear |February 26, 2021 |TechCrunch 

The company surpassed Spain’s Inditex, owner of Zara, to become the world’s most valuable clothing retailer, Nikkei reported. How Uniqlo became the world’s most valuable clothing company |Marc Bain |February 22, 2021 |Quartz 

In 2014, Spain passed a law requiring Google to pay for aggregating news content in search results. Australia wants Facebook and Google to pay for news on their sites. Other countries think it’s a good idea too |Gerrit De Vynck |February 19, 2021 |Washington Post 

We found a clinic in Spain and to be honest it was a bit of a crazy time for us. Still fighting for parental rights in Ireland |Ranae von Meding |February 14, 2021 |Washington Blade 

Two of his most classic compositions, “Spain” and “La Fiesta,” would come out of that era and impulse. Remembering Chick Corea, An Endlessly Inquisitive Jazz Pioneer |Andrew R. Chow |February 12, 2021 |Time 

Meanwhile, the Netherlands and Spain opted for the "unedited" version. Sony Emails Show How the Studio Plans to Censor Kim Jong Un Assassination Comedy ‘The Interview’ |William Boot |December 15, 2014 |DAILY BEAST 

But there were two designations that seemed anachronisms to me: -- Spain and the United Kingdom among the beer swillers. Beer Countries vs. Wine Countries |Clive Irving |December 7, 2014 |DAILY BEAST 

Beer-swilling Britain and Spain now boast impressive varietals while America is challenging France with how much wine is consumed. Beer Countries vs. Wine Countries |Clive Irving |December 7, 2014 |DAILY BEAST 

The locale was not Perugia, but Seville, in Spain, a country I have lived in and love. Amanda Knox: A Mother’s Obsession |Nina Darnton |November 26, 2014 |DAILY BEAST 

The wine-producing countries of Spain, Italy, and France, suggest limits that are double and triple that of the U.S. Americans Drink Too Much, But We’re Not All Alcoholics |Gabrielle Glaser |November 25, 2014 |DAILY BEAST 

In Spain he was regarded as the right arm of the ultra-clericals and a possible supporter of Carlism. The Philippine Islands |John Foreman 

As Spain, however, has fallen from the high place she once held, her colonial system has also gone down. Tobacco; Its History, Varieties, Culture, Manufacture and Commerce |E. R. Billings. 

However, on reaching Spain, the magic of the Emperor's personality soon restored the vigour and prestige of the French arms. Napoleon's Marshals |R. P. Dunn-Pattison 

General Lachambre, as the hero of Cavite, followed to receive the applause which was everywhere showered upon him in Spain. The Philippine Islands |John Foreman 

His enemies persistently insinuated that he was really returning to Spain to support the clericals actively. The Philippine Islands |John Foreman