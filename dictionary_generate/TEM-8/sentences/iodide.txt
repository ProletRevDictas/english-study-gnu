Blasting UV light at a mix of PFAS, iodide and sulfite broke down nearly all the PFAS in a sample. Simple process destroys toxic and widespread ‘forever’ pollutants |Janet Raloff |July 14, 2022 |Science News For Students 

It’s surprising that no one had demonstrated the effectiveness of using sulfite with iodide to degrade PFAS before, McKay says. Just 3 ingredients can quickly destroy widely used PFAS ‘forever chemicals’ |Nikk Ogasa |June 3, 2022 |Science News 

It’s little wonder that people have been stocking up on potassium iodide pills, which temporarily stop iodine-131 from entering the thyroid—though do little to protect against exposure to any other nuclear material. Amazon Sellers Are Making Big Money on Nuclear War Survival Products |Chris Stokel-Walker |March 23, 2022 |Time 

But the shops usually do not carry potassium iodide in large quantities, and many have sold out and now have long waiting lists. California's Nuclear Freakout |Josh Dzieza, Tricia Romano |March 18, 2011 |DAILY BEAST 

They have been going into radiated areas without any protection or potassium iodide. Yakuza to the Rescue |Jake Adelstein |March 18, 2011 |DAILY BEAST 

CVS, meanwhile, carries no radiation-fighting iodide, according to a representative. California's Nuclear Freakout |Josh Dzieza, Tricia Romano |March 18, 2011 |DAILY BEAST 

This must have depended upon the free iodide it contained, and yet such collodion has worked most admirably. Notes and Queries, Number 218, December 31, 1853 |Various 

It has since been obtained, in a separate state, by the action of sodium upon iodide of allyl. Cooley's Cyclopdia of Practical Receipts and Collateral Information in the Arts, Manufactures, Professions, and Trades..., Sixth Edition, Volume I |Arnold Cooley 

By this time the iodide of silver looked like pure solid brimstone in the wet paper. Notes and Queries, Number 241, June 10, 1854 |Various 

HCl, cooled in ice, and diazotized with sodium nitrite solution, until starch iodide paper showed excess nitrous acid. Synthesis of 2-methyl-4-selenoquinazolone, 2-phenylbenzoselenazole, and its derivatives |Y-Gwan Chen 

Suspecting a specific origin for this malady, I put him on the use of iodide potassium, with increasing doses. Report on Surgery to the Santa Clara County Medical Society |Joseph Bradford Cox