The most likely scenario, according to the cold case team, was that Arnold van den Bergh, a Jewish notary, tipped off authorities to the Frank family’s hiding place in a secret annex of a building in Amsterdam. Researchers say they may have uncovered who betrayed Anne Frank’s family to Nazis |Adela Suliman |January 18, 2022 |Washington Post 

Months after the closure, an annex elsewhere in the county began offering immunizations, infectious disease testing, women’s health and other programs. COVID-19 Hit This County Hard. A Weakened Health Department Still Can’t Get People Vaccinated. |by Aliyya Swaby |December 6, 2021 |ProPublica 

Because hundreds of NBC Sports staffers have been left home at their Stamford headquarters due to capacity restrictions in Japan — and because that headquarters has its own social-distancing requirements — the company created an annex to house them. For NBC, the covid Olympics will be a feat of strange movements and remote mystery |Steven Zeitchik |July 23, 2021 |Washington Post 

Cocaine and cannabis were also passed around at the party, which moved from the annex house to the main chapter lodge. A freshman was ‘hazed to death,’ his family says. 15 former fraternity members now face charges. |Reis Thebault |June 4, 2021 |Washington Post 

A few hours later the Annex itself came under attack and two of the same brave GRS operatives were killed. This Sexy Thriller Is Just the Document the Benghazi Commission Needs |Christopher Dickey |September 15, 2014 |DAILY BEAST 

Annex Medical Inc Annex Medical and Sacred Heart Medical are companies that design, manufacture, and sell medical devices. After Hobby Lobby, These 82 Corporations Could Drop Birth Control Coverage |Abby Haglage |June 30, 2014 |DAILY BEAST 

She was the first but not last leader of Russia to annex Crimea. Russian History Is on Our Side: Putin Will Surely Screw Himself |P. J. O’Rourke |May 11, 2014 |DAILY BEAST 

“We are too small to be independent for long and would have to ask Russia to annex us,” says translator Alec. Putin’s People Stage Their Bogus Vote |Jamie Dettmer |May 11, 2014 |DAILY BEAST 

That team fought their way back to the CIA annex with other Americans and sustained a low-level firefight throughout the evening. General: We Didn't Even Try To Save American Lives In Benghazi |Eli Lake |May 1, 2014 |DAILY BEAST 

But, nevertheless, in this house and not in its secret annex of a Hundred Raptures he designed to spend the night. Dope |Sax Rohmer 

Th' ilivator in th' left annex fell thirteen stories Thursday, but no wan was injured. Mr. Dooley Says |Finley Dunne 

Electricity seems destined to annex the whole field, not merely of optics, but probably also of thermotics. Steam Steel and Electricity |James W. Steele 

Once more men began to value empire, to seek to annex new territory overseas, and to bind closer the existing possessions. The Canadian Dominion |Oscar D. Skelton 

Further, Hodgeman and he built an annex out of spare timber to connect the entrance veranda with the store. The Home of the Blizzard |Douglas Mawson