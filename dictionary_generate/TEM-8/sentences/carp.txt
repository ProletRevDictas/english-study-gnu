Grass carp are actually pretty territorial—I’ve stood for 45 minutes in a specific area because I was waiting for one particular fish that was really big. Don’t have a boat? Try bowfishing. |By Natalie Krebs/Outdoor Life |November 3, 2020 |Popular-Science 

They fed eight mallard ducks thousands of fish eggs from two species of carp. Pooping ducks can shed the live eggs of fish |Carolyn Wilke |July 17, 2020 |Science News For Students 

A single, large carp can release hundreds of thousands of eggs at a time, she notes. Pooping ducks can shed the live eggs of fish |Carolyn Wilke |July 17, 2020 |Science News For Students 

This includes some carp species that were introduced in the southern United States in the 1970s. Pooping ducks can shed the live eggs of fish |Carolyn Wilke |July 17, 2020 |Science News For Students 

A single carp can release hundreds of thousands of eggs at a time, Vincze says. Fish eggs can hatch after being eaten and pooped out by ducks |Carolyn Wilke |June 29, 2020 |Science News 

Bringing in more revenue than most African governments is hardly reason to carp. Google 2011 Results Show Growth, But Can Tech Giant Thrive in Long Run? |Zachary Karabell |January 20, 2012 |DAILY BEAST 

Made with roe from mullet, carp, cod, lobsters, or even crab, taramasalata—salty, creamy, bright, with little pops! What to Eat: Mediterranean Feast |Cookstr.com |October 6, 2009 |DAILY BEAST 

Instead, they gave Americans tax cuts, defense hikes, and middle class spending, leaving Democrats to carp about deficits. Why Obama Can Spend, Spend, Spend |Peter Beinart |December 24, 2008 |DAILY BEAST 

I'm not enamored of a straight meat diet as a rule, but that evening I was in no mood to carp at anything half-way eatable. Raw Gold |Bertrand W. Sinclair 

He, for one, did not carp at Aunt Rachel's declaration that they were intended to spend time and eternity together. At Last |Marion Harland 

A fountain tinkled and fat carp swam about in the fluted marble basin. The Adventures of Kathlyn |Harold MacGrath 

If it has humour, deplore its lack of thoughtfulness; if it is grave, carp at its lack of gaiety. How to Fail in Literature |Andrew Lang 

It would be idle to carp at any means when the end is so thoroughly good. Mystic London: |Charles Maurice Davies