Plans are already underway to hire another 26 over the coming weeks to cope with the volume of work. ‘We’re building the plane while flying’: AB InBev’s in-house team plans structural changes as it takes on more work |Seb Joseph |February 10, 2021 |Digiday 

“The Love Proof,” by Madeleine Henry A pair of Yalies fall in love until one of them — physics prodigy Sophie — is forced to cope with the unexpected. The best books to beat this year’s special brand of Valentine’s Day blues |Zibby Owens |February 5, 2021 |Washington Post 

You’ve talked about the need to find positives in the frontline experience in order to cope. Kansas City Chiefs Lineman Laurent Duvernay-Tardif Stayed On The COVID-19 Front Lines. Now He's Missing The Super Bowl. |Sean Gregory |February 4, 2021 |Time 

To cope, Outside staffers have been alternating between physical activities in the cold and cozying up at home. The Gear Our Editors Loved in January |The Editors |February 3, 2021 |Outside Online 

He knew what had been going on and really just wanted to find out he we all were coping with that. Friends and colleagues mourn the loss of Mel Antonen, longtime MLB reporter |Scott Allen |February 1, 2021 |Washington Post 

Stephanie Giorgio, a classical musician, credits The Class for helping her cope with anxiety, focus, fear, and self-doubt. How Taryn Toomey’s ‘The Class’ Became New York’s Latest Fitness Craze |Lizzie Crocker |January 9, 2015 |DAILY BEAST 

Fulkerson, the founder of the magazine who has hired March, is someone he can cope with. The Novel That Foretold the TNR Meltdown |Nicolaus Mills |December 22, 2014 |DAILY BEAST 

Eric told me about a case with which a fellow lobbyist had to cope. Up to a Point: In Defense of Lobbyists |P. J. O’Rourke |October 25, 2014 |DAILY BEAST 

Child workers, even when they are brought back into the classroom, are unable to cope without proper bridge courses. Stopping the Small Hands of Slavery |Meenakshi Ganguly |October 13, 2014 |DAILY BEAST 

In an effort to cope with the implications of this question, Americans have subtly but sweepingly shifted their ideals. How Young People Are Destroying Liberty |James Poulos |October 11, 2014 |DAILY BEAST 

And when ordinary fellows like you and me attempt to cope with their idiosyncrasies the result is bungling. The Awakening and Selected Short Stories |Kate Chopin 

Constabulary was insufficient to cope with the marauders, and regular troops had to be sent to these provinces. The Philippine Islands |John Foreman 

Louis the Lusty fled before an insurrection that he did not think himself equal to cope with. The Pilgrim's Shell or Fergan the Quarryman |Eugne Sue 

Chivey was not soft, but he was not competent to cope with such a keen spirit as this Spanish notary. Jack Harkaway's Boy Tinker Among The Turks |Bracebridge Hemyng 

Yamba acted as cook and waitress, but after a time the work was more than she could cope with unaided. The Adventures of Louis de Rougemont |Louis de Rougemont