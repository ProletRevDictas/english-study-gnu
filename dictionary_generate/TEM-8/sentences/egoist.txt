You might think that your renter is an egoist, because he or she is presumably only concerned with his or her own welfare. A Philosopher Answers Everyday Moral Dilemmas In A Time Of Coronavirus |LGBTQ-Editor |April 15, 2020 |No Straight News 

Was she merely an egoist—it ran in the family—or did it conceal much that she had no intention of revealing? Ancestors |Gertrude Atherton 

Ada's a good child, as good as a born egoist can be, but—well—we are not all made on the same plan. Ancestors |Gertrude Atherton 

You are an egoist to create all this excitement; don't you know that the maids are out in the hall crying? Ways of War and Peace |Delia Austrian 

It is an awful thing when a poseur ceases to pose, when an egoist becomes a human being. The Romance of His Life |Mary Cholmondeley 

My sufferings, my repugnance, my feelings, all my egoism—for I know that I am an egoist—ought to be sacrificed to the family. Honorine |Honore de Balzac