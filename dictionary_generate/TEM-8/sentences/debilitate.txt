A debilitating pandemic swept through the global economy, upending long-established ways of doing business. The Digiday Worklife Awards shortlist: Keeping collaboration and charity alive during an unprecedented year |Digiday Awards |October 20, 2020 |Digiday 

That leads to dementia, a debilitating condition that affects millions of Americans each year. Everyone should have a social life, especially older adults |Kat Eschner |October 20, 2020 |Popular-Science 

Chikungunya is a disease that can result in debilitating joint pain. A glowing zebrafish wins the 2020 Nikon Small World photography contest |Erin Garcia de Jesus |October 13, 2020 |Science News 

Extreme humidity from New Orleans to northern Wisconsin will make summers increasingly unbearable, turning otherwise seemingly survivable heat waves into debilitating health threats. Climate Change Will Force a New American Migration |by Abrahm Lustgarten, photography by Meridith Kohut |September 15, 2020 |ProPublica 

His story also doubles as the suffocating, debilitating nature of the closet itself. Toward a queer Disney canon |Emily VanDerWerff |September 4, 2020 |Vox 

Infernal, it can cause fires and explosions; toxic, it can debilitate, poison, and kill. The Fiery Underground Oil Pit Eating L.A. |Geoff Manaugh |December 6, 2014 |DAILY BEAST 

Taking hot food or drink, habitually, tends to debilitate all the organs thus needlessly excited. A Treatise on Domestic Economy |Catherine Esther Beecher 

Many suppose that a warm bath exposes a person more readily to take cold; and that it tends to debilitate the system. A Treatise on Domestic Economy |Catherine Esther Beecher 

He was one of those whom books cannot debilitate, nor a life of study incapacitate for the study of life. On the Sublime |Longinus 

Even tea and coffee, the common beverages of all classes of people, have a tendency to debilitate the digestive organs…. Smoking and Drinking |James Parton 

In such a case diarrhea will no longer serve a good end, but will on the contrary debilitate the system. Intestinal Ills |Alcinous Burton Jamison