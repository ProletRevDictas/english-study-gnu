If you heard the song at a crowded bar, you might mistake it for an airy faux-feminist ditty. What’s the Song of the Summer for 2021? Here Are Our Predictions |Kat Moon |June 23, 2021 |Time 

Legendary composer Stephen Sondheim loosely nodded toward the idea with a singsongy little ditty about beans called “The Witch’s Rap” in 1986’s Into the Woods. How In the Heights went from a student musical to one of the summer’s biggest movies |Constance Grady |June 11, 2021 |Vox 

He often composes ditties to play at the farewell parties of staff members. NIH Director Francis Collins Is Fighting This Coronavirus While Preparing for the Next One |Belinda Luscombe |February 4, 2021 |Time 

The addicting ditty “One of These Things” was used to help children learn to compare and discern differences. ‘Sesame Street’ Is Middle-Aged and Awesome |Emily Shire |November 10, 2014 |DAILY BEAST 

If she ever knew that I would never be the man I promised I would be,” he sings on the 1990s pop ditty “Opposite of Me. Robin Thicke’s ‘Paula’ Is What You Shouldn’t Do When You Get Dumped |Andrew Romano |June 26, 2014 |DAILY BEAST 

The kids get a record deal, and the bosses receive some mistaken intel that the song is a folk ditty, up for grabs copyright-wise. ‘The Lion Sleeps Tonight’: Rian Malan’s South Africa Reviewed |Katie Baker |November 28, 2012 |DAILY BEAST 

A high school vibe overtook the hall: it was a pep rally, complete with its own music video—“Kahana was right,” laments the ditty. 'Kahane For Kids' |Elisheva Goldberg |November 5, 2012 |DAILY BEAST 

“Miss Atomic Bomb,” a five-minute ditty that swells into a soaring rock anthem, is an early standout. The Killers Talk New Album ‘Battle Born,’ Mitt Romney, Mormonism & More |Marlow Stern |September 15, 2012 |DAILY BEAST 

Mea, however, fought passionately for her friend and never gave way till Kurt had promised not to go on with his ditty. Maezli |Johanna Spyri 

The musicians played energetically, switching now from the hymn to their unofficial little ditty. Pagan Passions |Gordon Randall Garrett 

None other than that sweet sentimental ditty, "Be kind to the loved ones at home." The Life of Thomas Wanless, Peasant |Alexander Johnstone Wilson 

We almost felt like having that bright little ditty 'In Dixie's Land' served up to us, we all felt so jubilant. The Blue and The Gray |A. R. White 

The knife daily pierces the neck of the swine, and the kitchen wench wrings off the head of the fowl while she hums a ditty. Wild Western Scenes |John Beauchamp Jones