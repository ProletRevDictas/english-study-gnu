Who was the second, who has attained such notoriety in connexion with Nelson's name; and when and where were they married? Notes and Queries, Number 177, March 19, 1853 |Various 

His reply was, “Just a thread of connexion with the United States to keep us from being the prey of other nations!” The Philippine Islands |John Foreman 

One feature of especial interest associated with this cemetery is its connexion with an adjacent arenarium, or sand pit. The Catacombs of Rome |William Henry Withrow 

In this connexion it may be said that a good railway system obviates many of the disadvantages attending the use of tents. Encyclopaedia Britannica, 11th Edition, Volume 5, Slice 2 |Various 

He maintained the connexion of church and state, and opposed triennial parliaments and the ballot. Encyclopaedia Britannica, 11th Edition, Volume 5, Slice 2 |Various