Waste carbon dioxide emitted at a refinery or factory is captured at the source and then stored in an aim to remove the potential harmful byproduct from the environment and mitigate climate change. Elon Musk is donating $100M to find the best carbon capture technology |Kirsten Korosec |January 22, 2021 |TechCrunch 

That makes them a key part of the Earth process of pulling climate-warming carbon dioxide out of the air. Can people protect as much space as nature needs? |Jonathan Lambert |January 21, 2021 |Science News For Students 

The subducting plate begins to melt, and volcanoes bloom on the overlying plate, belching carbon dioxide and other gases into the atmosphere. How the Earth-shaking theory of plate tectonics was born |Carolyn Gramling |January 13, 2021 |Science News 

Saudi Aramco is close to signing a $15 million deal with Global Thermostat to capture carbon dioxide, add hydrogen and make synthetic gasoline. United Airlines aims to suck carbon dioxide from the friendly skies |Steven Mufson |January 12, 2021 |Washington Post 

Respiration describes how oxygen moves into our cells and carbon dioxide moves out. Scientists Say: Respiration |Bethany Brookshire |January 11, 2021 |Science News For Students 

It reacts very readily with oxygen by burning smokelessly, with carbon dioxide and water as its byproducts. Methane on Mars: Life or Just Gas? |Matthew R. Francis |December 17, 2014 |DAILY BEAST 

Other psychiatrists attempted to treat schizophrenia with carbon dioxide gas and artificially-induced comas. Schizophrenia Isn’t One Disorder but Eight |Samantha Allen |September 16, 2014 |DAILY BEAST 

The higher levels of carbon dioxide will induce something of a feeding frenzy for plants, at least for a while. Blame Climate Change for Your Terrible Seasonal Allergies |Kent Sepkowitz |May 14, 2014 |DAILY BEAST 

Grosvenor Place, which runs alongside the palace, has almost four times the maximum permissible amount of nitrogen dioxide. Pollution At The Palace |Tom Sykes |February 24, 2014 |DAILY BEAST 

“We are short of carbon dioxide for the needs of the plants,” Anderson said. Fringe Factor: More Carbon Dioxide, Please |Caitlin Dickson |February 23, 2014 |DAILY BEAST 

The primary purpose of respiration in all animals is the same—namely, to furnish oxygen and remove carbon dioxide (carbonic acid). Voice Production in Singing and Speaking |Wesley Mills 

If we collect enough of these bubbles of gas to make a test, we find it to be carbon dioxide. A Civic Biology |George William Hunter 

In previous experiments we have proved that carbon dioxide is given off by any living thing when oxidation occurs in the body. A Civic Biology |George William Hunter 

The plants give off oxygen to the animals, and the animals give carbon dioxide to the plants. A Civic Biology |George William Hunter 

The organs of respiration: the organs in which the blood receives oxygen and gives up carbon dioxide. A Civic Biology |George William Hunter