After I had filled him in on my family, we would start talking hoops. Tom Konchalski made basketball better. The sport won’t be the same without him. |John Feinstein |February 8, 2021 |Washington Post 

Without someone creating and finishing near the hoop, the Raptors don’t make sense on offense — no matter how many triples they launch at the rim. The Toronto Raptors Can’t Seem To Finish Their Drives |Louis Zatzman |January 27, 2021 |FiveThirtyEight 

Everybody who plays basketball — from the pros to kids on the playground — loves putting the ball in the hoop. With Harden, Durant and Irving, could Brooklyn net an NBA title? |Fred Bowen |January 20, 2021 |Washington Post 

Sink hoops of wire or pipes into your growing bed every three to four feet, and cover them with the long, narrow blankets of row cover, a spun synthetic fiber that allows light and rainwater to reach the growing bed. Fresh vegetables in the middle of winter? It’s possible, even in colder climes. |Adrian Higgins |January 20, 2021 |Washington Post 

Little brother Landen is a freshman in high school with his own hoop dreams. Dynamic sophomores Ashley Owusu and Diamond Miller are powering an explosive Maryland offense |Kareem Copeland |January 13, 2021 |Washington Post 

Hoop skirts of the Civil War era relaxed into flowing, streamlined gowns. The Best-Dressed Way to Say Goodbye |Justin Jones |October 21, 2014 |DAILY BEAST 

Directed by fellow Chicagoan Steve James ( Hoop Dreams), Life Itself is a worthy tribute to the most popular film critic ever. My Friend, Roger Ebert: Pulitzer Prize Winner Tom Shales on the Moving Documentary ‘Life Itself’ |Tom Shales |July 6, 2014 |DAILY BEAST 

She sits polished in a hip yet age-appropriate (age-defying, really, for 76 ) leather jacket, and hoop earrings. Marlo Thomas Says Girls Should Feel Free to Be Like Hannah Horvath |Emily Shire |April 24, 2014 |DAILY BEAST 

Then the second hoop is the profession, then industry, then finally society at large. Following Tuberculosis From Death Sentence to Cure |Tessa Miller |April 16, 2014 |DAILY BEAST 

Roger Ebert and Gene Siskel first championed my film, Hoop Dreams, which was essential to its success. ‘Life Itself’: An Open Letter From Filmmaker Steve James About His Roger Ebert Documentary |Steve James |December 12, 2013 |DAILY BEAST 

This hoop must have been left by us last year at Careening Bay. Narrative of a Survey of the Intertropical and Western Coasts of Australia] [Volume 2 of 2] |Phillip Parker King 

The afternoon was drawing on; they all hurried out into the playground, having got hold of every hoop to be found. Digby Heathcote |W.H.G. Kingston 

Procure a piece of flat iron similar to an iron hoop, bend it, as shown in the sketch, to make a piece 3 in. The Boy Mechanic, Book 2 |Various 

Procure a good barrel with a bottom and cut off each alternate stave at both ends close up to the first hoop. The Boy Mechanic, Book 2 |Various 

Mappo was so surprised, as he felt himself fairly flying through the paper hoop, that he did not know exactly what was happening. Mappo, the Merry Monkey |Richard Barnum