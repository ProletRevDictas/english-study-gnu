Enjoy these with casual snacks or appetizers, such as olives, salami and other nibbles. This $14 Italian red is a gem that invites a pairing with a pot roast or pasta |Dave McIntyre |February 12, 2021 |Washington Post 

Inside a cabinet are more snacks, all complimentary, including a selection of savory and sweet nibbles, waters, and even Kombucha. Escape (safely) to Baltimore’s luxurious Ivy Hotel |Kevin Naff |December 4, 2020 |Washington Blade 

Will he nibble around the edges, or will he actually take on the oil companies? Clinton’s Environmental Failure |Bill McKibben |December 9, 2012 |DAILY BEAST 

Would he be able to nibble on foie gras, slurp fettuccine Alfredo, and sample chocolate mousse without putting on weight again? Frank Bruni Revealed |Nicki Gostin |August 18, 2009 |DAILY BEAST 

It stopped to nibble a few burrs, and when it was shooed on, it didn't stop to walk around the obstruction. The Red Cow and Her Friends |Peter McArthur 

The result was that the sheep came right up to the door to nibble the young and juicy grass. The Red Cow and Her Friends |Peter McArthur 

If they are young hawk-bills, they will nibble the seaweed, and soon go on to crabs and shell-fish, and even little fishes. The Animal Story Book |Various 

Beneath this the girl stopped a moment, and let Bumper nibble at the green grass. Bumper, The White Rabbit |George Ethelbert Walsh 

He had tried all day to nibble through it, and dig under it, but the wire had only hurt his teeth without giving way a particle. Bumper, The White Rabbit |George Ethelbert Walsh