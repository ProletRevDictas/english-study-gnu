Linking Scott’s legacy to a bird “is just adding to the erasure by putting another layer over it.” Racism lurks in names given to plants and animals. That’s starting to change |Jaime Chambers |August 25, 2021 |Science News 

The good news is that historians and journalists, as well as the women themselves, have been working hard to reverse this erasure and are having significant success. The voices of women in tech are still being erased |Mar Hicks |August 3, 2021 |MIT Technology Review 

More than an erasure of historical fact it is another example of the ongoing and dangerous practice of cherry-picking parts of our past to fit prepackaged national myths. Every American needs to take a history of Mexico class |Gabriela Laveaga |July 22, 2021 |Washington Post 

By exploiting the rules that neurons use to learn new associations, these next-generation electroceuticals may enable permanent disease erasure. How to Unlearn a Disease - Issue 103: Healthy Communication |Kelly Clancy |July 14, 2021 |Nautilus 

The sheet ended up being wounded by a playful machete, full of cut phrases, notes on the edges, reminders, arrows that redirect the reading and erasures in search of a better piece in each version. Locked up in the Land of Liberty: Part II |Michael K. Lavers |July 14, 2021 |Washington Blade 

In this way, inspiration becomes appropriation, which leads directly to theft and erasure. The Cultural Crimes of Iggy Azalea |Amy Zimmerman |December 29, 2014 |DAILY BEAST 

While many of these depictions play into bisexual erasure, others reinforce harmful bisexual stereotypes. It Ain't Easy Being Bisexual on TV |Amy Zimmerman |August 14, 2014 |DAILY BEAST 

The act of erasure through mis- or under-representation is an insidious one. It Ain't Easy Being Bisexual on TV |Amy Zimmerman |August 14, 2014 |DAILY BEAST 

The total erasure of former employees is so familiar it even has a nickname. Spies, Cash, and Fear: Inside Christian Money Guru Dave Ramsey’s Social Media Witch Hunt |Matthew Paul Turner |May 29, 2014 |DAILY BEAST 

I love the way erasure becomes a tool for depiction and emphasis, and failure becomes a heroic condition. Mike Kelley's Creative Erasure |Blake Gopnik |April 16, 2013 |DAILY BEAST 

The French attorney general demanded the erasure of his name from the list of magistrates, but this the court refused. Fox's Book of Martyrs |John Foxe 

For answer she bent over her typewriter and began to make an erasure. Tom Slade with the Colors |Percy K. Fitzhugh 

Bruslart (ubi supra, i. 136) denies that the erasure was actually made as Charles had commanded. History of the Rise of the Huguenots |Henry Baird 

This thin paste of wax was also spread on tablets of wood, that it might more easily admit of erasure. The Book of Curiosities |I. Platts 

Consequently the action of the iodine differs according to the extent of the erasure. Disputed Handwriting |Jerome B. Lavay