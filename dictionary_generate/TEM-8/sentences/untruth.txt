It’s perplexing for them to claim I’ve said things that are untrue about the investigation, without saying what those untruths are, when they haven’t even provided me or anyone with the findings of the investigation. ‘Expect a Legal Challenge.’ Ouster of Human Rights Campaign President Alphonso David Steeped in Controversy |Madeleine Carlisle |September 7, 2021 |Time 

Those outreach workers … would spend 20 minutes with one person trying to roll back untruths. D.C. won’t bring back masks to guard against delta variant, officials say |Julie Zauzmer |July 2, 2021 |Washington Post 

There’s nothing new about conspiracy theories, disinformation, and untruths in politics. Machine-learning project takes aim at disinformation |MIT Technology Review Insights |May 3, 2021 |MIT Technology Review 

Every day we tell many small untruths, lies uttered by mutual consent that keep societal interactions civil. We’re constantly fooling ourselves — and that’s (mostly) okay |Katie Hafner |March 26, 2021 |Washington Post 

Looking back, Lowery says, “The on-the-record statements they provided that evening were absolutely untrue and the only reason I was able to keep those untruths out of The Washington Post was because I was there,” says Lowery. Failed prosecution of Iowa reporter was an attack on journalism |Erik Wemple |March 11, 2021 |Washington Post 

But then people started defending--or minimizing to the point of untruth--the clearly indefensible. Why Hugo Chavez Was Bad for Venezuela |Megan McArdle |March 7, 2013 |DAILY BEAST 

Her words were not simply empty sentiment, or yet another playful untruth. Elsa Maxwell, the Kingmaker |Mark Braude |November 1, 2012 |DAILY BEAST 

He was going nuts with frustration in the face of what he considered to be phoniness and untruth. In Vice Presidential Debate, Joe Biden Perfects Art of the Smirk |Lee Siegel |October 13, 2012 |DAILY BEAST 

And then the very fact of celebrity bankrolls its own momentum, creates its own truth out of a card house of untruth. Trump Endorsed Romney Because It Was Like Calling to Like |Lee Siegel |February 3, 2012 |DAILY BEAST 

By remaining silent about this, it became a convenient untruth! The Man Suing Joan Rivers |Jacob Bernstein |June 25, 2010 |DAILY BEAST 

Yet if there is a measure of untruth in such pretty flatteries, one needs to be superhuman in order to condemn them harshly. Children's Ways |James Sully 

On the other hand it seems to be thought that there are people who are specially fitted to be the victims of untruth. Children's Ways |James Sully 

An untruth is passed over carelessly and the child allowed to cover up its sins without realizing their sinfulness. The value of a praying mother |Isabel C. Byrum 

And little better in results than telling an untruth is putting the child off till some future time. The value of a praying mother |Isabel C. Byrum 

You may say so without any risk of telling an untruth; for, on the word of a hunter, I never had such a tough job. The Border Rifles |Gustave Aimard