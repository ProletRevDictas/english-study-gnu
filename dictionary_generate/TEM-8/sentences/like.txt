They included brands like Lundberg Family Farms, Hain, and Chico-San. The Rise and Fall of the Rice Cake, America’s One-Time Favorite Health Snack |Brenna Houck |September 17, 2020 |Eater 

Apples, to me, can be diced like onions, and popcorn eaten one kernel at a time. Sunflower Seeds Are the Best Snack for the Anxious Mind |Emma Alpern |September 17, 2020 |Eater 

Snowflake on Wednesday went public in the largest software IPO of all time, and then kept running like the Energizer Bunny on speed. Breaking down why Snowflake's massive IPO stood out from the stock market froth |Dan Primack |September 17, 2020 |Axios 

Up until recently, CBD and marijuana brands could not advertise on Google or Facebook, and networks like Taboola and Outbrain wouldn’t sell them traffic either. ‘An educational stance’: Publishers mull CBD’s alluring – and complex – commerce opportunities |Max Willens |September 17, 2020 |Digiday 

I still like playing it down because I don’t want to create a panic. Ten days: After an early coronavirus warning, Trump is distracted as he downplays threat |Ashley Parker, Josh Dawsey, Yasmeen Abutaleb |September 17, 2020 |Washington Post 

So I just patted him kind-like on the shoulder and sat down. The Story Behind Lee Marvin’s Liberty Valance Smile |Robert Ward |January 3, 2015 |DAILY BEAST 

Where these laser-like missiles are falling out of the sky onto a city and you have to stop each of them from hitting the targets? Coffee Talk with Ethan Hawke: On ‘Boyhood,’ Jennifer Lawrence, and Bill Clinton’s Urinal Exchange |Marlow Stern |December 27, 2014 |DAILY BEAST 

It got so bad, that the school resorted to “Groupon-like services” to fill seats. Is Any College Football Coach Worth $60 Million? Jim Harbaugh Is |Jesse Lawrence |December 20, 2014 |DAILY BEAST 

Though not in production yet, the high-end heels will be sold through a Tesla-like model. The Coolest Fashion Innovations of 2014 |Raquel Laneri |December 18, 2014 |DAILY BEAST 

He was kept in a dorm-like building, which has also been reported in other allegations. Beaten By His Church for Being Gay |Justin Jones |December 16, 2014 |DAILY BEAST 

May looked along at the dimpled grace, And then at the saint-like, fair old face, “How funny!” Boys and Girls Bookshelf; a Practical Plan of Character Building, Volume I (of 17) |Various 

His strong legs and his broad, spade-like feet helped to make him a fine swimmer. The Tale of Grandfather Mole |Arthur Scott Bailey 

The wave-like movement of these animals is particularly graceful and cleverly done. Women in the fine arts, from the Seventh Century B.C. to the Twentieth Century A.D. |Clara Erskine Clement 

These dreamy, Madonna-like beauties are the result of the most severe and protracted study. Women in the fine arts, from the Seventh Century B.C. to the Twentieth Century A.D. |Clara Erskine Clement 

Rarely, sodium urate occurs in crystalline form—slender prisms, arranged in fan- or sheaf-like structures (Fig. 32). A Manual of Clinical Diagnosis |James Campbell Todd