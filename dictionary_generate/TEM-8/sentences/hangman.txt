Plus hanging with a specific hangman never stopped and still is used in New Hampshire and Washington State. The Death Penalty’s Gruesome Truth |Kent Sepkowitz |February 6, 2014 |DAILY BEAST 

Parliament of England ordered the Book of Sports to be burned by the common hangman. The Every Day Book of History and Chronology |Joel Munsell 

The solemn league and covenant burned by the common hangman at London, and afterwards throughout the country. The Every Day Book of History and Chronology |Joel Munsell 

But now the hangman smokes, and the criminal condemned to death smokes before being hanged. Tobacco; Its History, Varieties, Culture, Manufacture and Commerce |E. R. Billings. 

How refinedly brutal is this constant care lest the hangman be robbed of his prey! Prison Memoirs of an Anarchist |Alexander Berkman 

This day among other stories he told me how despicable a thing it is to be a hangman in Poland, although it be a place of credit. Diary of Samuel Pepys, Complete |Samuel Pepys