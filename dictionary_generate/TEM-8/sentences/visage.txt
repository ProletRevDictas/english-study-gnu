An app called Reface, for example, lets them switch visages with another person—a popular TikTok hobby. Want ethical AI? Hand the keys to middle schoolers. |Sarah Scoles |November 21, 2021 |Popular-Science 

Use sharp angles to keep your visage out of frame or shrouded in shadow, or shoot with a bright flash in a mirror, for example. A complete guide on how to safely take, send, and store nudes |Sandra Gutierrez G. |September 9, 2021 |Popular-Science 

The Kowalskis worked alongside Ross and his second wife, Jane, to build an artistic empire that includes paints and brushes bearing Ross’s name and visage. 4 things we learned from the Bob Ross documentary, including his hair secrets and an alleged affair |Bethonie Butler |August 27, 2021 |Washington Post 

Today, presidents’ faces stare out from this now-solidified magma, those ancient oceanic sediments sitting just below the visage of George Washington. A new book reveals stories of ancient life written in North America’s rocks |Alka Tripathy-Lang |August 3, 2021 |Science News 

If moving to the country for the sake of a wrinkle-free visage is not a viable option, a gentle cleanser twice a day is all you need. 7 things you can do to actually prevent wrinkles |Sandra Gutierrez |July 13, 2021 |Popular-Science 

The cans were printed with his smiling visage and the words “good person.” The Chinese Can’t Catch Their Breath |Brendon Hong |May 5, 2014 |DAILY BEAST 

Here, instead of verbalizing her emotions, Winslet oozes dread via her broken-down visage, and slight mannerisms. At Telluride, Jason Reitman’s ‘Labor Day’ Is Superbly Acted |Marlow Stern |August 30, 2013 |DAILY BEAST 

A stay-at-home California mom named Kira Davis was a smiling visage with a dead-serious question. Why Barack Obama and Joe Biden Are Gambling on Social Media |Howard Kurtz |February 21, 2013 |DAILY BEAST 

Krugman does raise the amusing question of whose visage should appear on such a coin, if minted. The Platinum Coin is a Weird Idea |Michael Tomasky |January 7, 2013 |DAILY BEAST 

LaPierre paused in his sermon and assumed the long-suffering visage of a saint. The NRA: From Awful to Even Worse |Michelle Cottle |December 22, 2012 |DAILY BEAST 

A threatening denunciation was in his visage, as he advanced with his staff of office towards his prisoner. The Pastor's Fire-side Vol. 3 of 4 |Jane Porter 

Un Vieillard.Jeune fille au riant visage,Que cherches-tu sous cet ombrage? A Selection from the Works of Frederick Locker |Frederick Locker 

As many have been astonished at thee, so shall his visage be inglorious among men, and his form among the sons of men. The Bible, Douay-Rheims Version |Various 

Gwynne made a wry face as he sat down before the dressing-table that he might reflect his visage while he brushed his hair. Ancestors |Gertrude Atherton 

Thou hast now knowen and ataynt the doutous or double visage of thilke blinde goddesse Fortune. Chaucer's Works, Volume 1 (of 7) -- Romaunt of the Rose; Minor Poems |Geoffrey Chaucer