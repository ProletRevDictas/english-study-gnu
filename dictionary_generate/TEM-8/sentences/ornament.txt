You might not have been considering an ornament from the Supreme Court Historical Society or butterfly-inspired jewelry from Brookside Gardens, but there’s a lot to explore. The best things to do — virtually and in person — while social distancing in the D.C. area |Fritz Hahn, Hau Chu |November 12, 2020 |Washington Post 

It also said the tree would be illuminated throughout December, and visitors are invited to the site to view it and trees representing each state and territory and the District, decorated with ornaments designed by students from across the country. No live audience for National Christmas Tree Lighting, park service says |Martin Weil |November 11, 2020 |Washington Post 

To commemorate surviving 2020—something that everyone should celebrate—we’ve picked out a few ornaments that seem quite fitting for these times. Gift Guide: Extremely 2020 holiday ornaments |Rachel Schallom |November 7, 2020 |Fortune 

Show them your father will not, will no longer, have license to treat you as ornaments for his own ego and needs. Carolyn Hax: How to tell Dad that you want to dispense with the unpleasantries |Carolyn Hax |November 6, 2020 |Washington Post 

This battery-free device dangles like an ornament from a tree and harvests all the energy it needs from the natural swaying of branches. Trees power this alarm system for remote forest fires |Stephen Ornes |October 16, 2020 |Science News For Students 

McConnell soon followed, beaming like an ornament atop a Christmas tree. Mitch’s Brotastic Victory Bash |Olivia Nuzzi |November 5, 2014 |DAILY BEAST 

It is your ornament, your grace, your seduction, your chant for courting. ‘Mirages’: Anaïs Nin’s Intimate, Unexpurgated Diaries |Lizzie Crocker |October 26, 2013 |DAILY BEAST 

The Daily Pic: Silversmith Sakurako Shimizu makes ornament from exclamations. Jewelry that Speaks for Itself |Blake Gopnik |April 8, 2013 |DAILY BEAST 

She inspired the designer to craft a brilliant silver minidress with a lightning-bolt head ornament. Armani Goes Gaga: Giorgio Armani Intoxicated by Lady Gaga |Robin Givhan |February 6, 2011 |DAILY BEAST 

She was examining the ornament on the back of which was carved a miniature bar of music, with three or four notes. The Cromptons |Mary J. Holmes 

Meantime the court receives the rents; the garden, the chief ornament of the town, is running wild, and the house is deserted. Journal of a Voyage to Brazil |Maria Graham 

The trees are sometimes prettily arranged in alleys, but are planted far less for ornament than for use. A Woman's Journey Round the World |Ida Pfeiffer 

She was dressed in a high-necked dress of black lace, and wore in her corsage a large circular ornament of diamonds and emeralds. The Doctor of Pimlico |William Le Queux 

Three clubs, which look from without to be very comfortable, ornament this square with their gas-lamps. Little Travels and Roadside Sketches |William Makepeace Thackeray