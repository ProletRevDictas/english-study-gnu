This is what happened in the 1980s, when a modern satanic panic surged in response to social and political transformations not all Americans agreed with. The revolutionary roots of satanic panic still invoked in American politics |Zara Anishanslin |August 5, 2022 |Washington Post 

Players assume the role of a possessed lamb and build a flock of deceptively cute woodland creatures to become the biggest, baddest satanic cult around. Gamescom: ‘Halo Infinite’ release date, new Marvel game from 2K and all the biggest announcements |Alyse Stanley, Shannon Liao |August 25, 2021 |Washington Post 

She had previously shared QAnon conspiracy videos, called Melinda Gates “satanic,” claimed that George Soros had paid racial-justice protesters and pushed disinformation about LGBTQ “conversion” therapy. QAnon Candidates Are Winning Local Elections. Can They Be Stopped? |Vera Bergengruen |April 16, 2021 |Time 

I only went a couple of times, due to attending a church that believed Halloween was satanic. Watching It’s the Great Pumpkin, Charlie Brown with a 5-year-old |Emily VanDerWerff |October 30, 2020 |Vox 

Its central claims were that Hillary Clinton’s campaign chair, John Podesta, was an occultist, and that a dinner hosted by a prominent performance artist was actually a secret satanic ritual. How the truth was murdered |Abby Ohlheiser |October 7, 2020 |MIT Technology Review 

The Satanic Temple wins these fights because they are small fights. In Florida, ’Tis The Season for Satan |Jay Michaelson |December 7, 2014 |DAILY BEAST 

The Satanic Temple won a battle to put a display in the Florida state capitol, but the religious right is fighting a bigger war. In Florida, ’Tis The Season for Satan |Jay Michaelson |December 7, 2014 |DAILY BEAST 

It started when I asked whether his seemingly Satanic serial killer was “a vehicle for a discussion of religion.” ‘True Detective’s’ Godless Universe: Is the HBO Show Anti-Christian? |Andrew Romano |March 6, 2014 |DAILY BEAST 

As an alternative religious player, says Greaves, the Satanic Temple provides yet another “counterbalance.” Satan Is Coming to Oklahoma |Michelle Cottle |December 10, 2013 |DAILY BEAST 

So practically speaking, the Satanic Temple is less a religion than a small band of guerilla activists. Satan Is Coming to Oklahoma |Michelle Cottle |December 10, 2013 |DAILY BEAST 

In English archaic writings the instances in which the mention of the Satanic power is thus utilised are not numerous. A Cursory History of Swearing |Julian Sharman 

Some beautiful legends have been preserved which tell of miraculous deliverance of Christian girls from this most Satanic cruelty. Fighting the Traffic in Young Girls |Various 

In a cross-road, at some distance from the Satanic hill, the animal which I rode cast a shoe. Lavengro |George Borrow 

But in spite of this appeal and of a pact signed with the blood of the writer, no Satanic apparitions were forthcoming. Secret Societies And Subversive Movements |Nesta H. Webster 

In his choice of a date his Satanic Majesty showed his respect for popular superstitions. Irish Witchcraft and Demonology |St. John D. (St. John Drelincourt) Seymour