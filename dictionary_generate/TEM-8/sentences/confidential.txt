Former Washington Football Team cheerleaders who appeared in lewd videos that team employees secretly produced from outtakes of 2008 and 2010 swimsuit calendar shoots have reached confidential settlements with the team. Former cheerleaders settle with Washington Football Team as program’s future is in doubt |Beth Reinhard |February 10, 2021 |Washington Post 

The first filings with the SEC are for its originally confidential Draft Registration Statement to go IPO in early 2020. PSA: Most aggregate VC trend data is garbage |Danny Crichton |February 9, 2021 |TechCrunch 

Wilkinson’s probe also includes information about a 2009 confidential settlement. Beth Wilkinson close to completing Washington Football Team investigation, Roger Goodell says |Mark Maske |February 5, 2021 |Washington Post 

Scott said she wrote the story with the understanding that it would be confidential until Sheehan’s death. Seeing the Pentagon Papers in a New Light |by Stephen Engelberg |February 3, 2021 |ProPublica 

Medical data, on the other hand, are highly confidential and usually not seen by the team at all — unless “there is something that can affect the team,” Lorang says. As biometrics boom, who owns athletes’ data? It depends on the sport. |Nick Busca |February 2, 2021 |Washington Post 

A year later, the suit ended in a settlement, the details of which remain confidential. The Godfather of Right-Wing Radio |Caitlin Dickson |November 23, 2014 |DAILY BEAST 

The provision of confidential details to the IG does not mean that they will be publicly released. The Peace Corps' Awful Secret |Tim Mak |August 16, 2014 |DAILY BEAST 

Now the Supreme Leader has thrown the confidential details out into the open and marked his line in the sand. Iran Supreme Leader Spills the Nuke Talk Secrets |IranWire |July 12, 2014 |DAILY BEAST 

In recent months Foreign Minister Mohammad Javad Zarif has insisted on keeping the negotiating details confidential. Iran Supreme Leader Spills the Nuke Talk Secrets |IranWire |July 12, 2014 |DAILY BEAST 

But L.A. Confidential was important for temporarily resuscitating film noir. The Great Character Actor: Guy Pearce on His Brilliant Career, From ‘Priscilla’ to ‘The Rover’ |Richard Porton |May 23, 2014 |DAILY BEAST 

Nine lusty-lunged adults in that one room prohibited confidential speech. Raw Gold |Bertrand W. Sinclair 

As soon as Michael made sure of the duel, he saw his confidential clerk. Blackwood's Edinburgh Magazine, No. CCCXXXIX. January, 1844. Vol. LV. |Various 

It emboldens me to ask another favor—that you will regard what I have told you of my plans as confidential. Ancestors |Gertrude Atherton 

He entreated her to bear in mind that the disclosures of the afternoon were strictly confidential. The Awakening and Selected Short Stories |Kate Chopin 

Amongst well-bred persons, every conversation is considered in a measure confidential. The Ladies' Book of Etiquette, and Manual of Politeness |Florence Hartley