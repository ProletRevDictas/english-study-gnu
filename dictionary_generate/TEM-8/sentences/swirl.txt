Another strong offering is “Wave Relief,” a wall piece whose incised, blue-tinted swirls are the show’s closest thing to representational imagery. In the galleries: Immersive exhibit explores a wonderland in blue |Mark Jenkins |January 22, 2021 |Washington Post 

The stuffed breast slices up with a swirl of cranberry throughout. Make it a one-pan Thanksgiving with stuffed turkey breast, sweet potatoes and green beans |Ellie Krieger |November 10, 2020 |Washington Post 

Because the sanding disk travels in a random elliptical motion, it won’t leave swirl patterns on surfaces and it isn’t as affected by the direction of the wood grain. Reliable random orbital sanders for your workshop |PopSci Commerce Team |October 9, 2020 |Popular-Science 

HSBC now risks being caught in deepening turmoil after a swirl of trouble over the past year amid political unrest and an economic slump in its biggest market, Hong Kong. 2 new reports push HSBC stock below its 2009 low |Claire Zillman, reporter |September 21, 2020 |Fortune 

Of course since 2018, there’s been a swirl of new ways to go public. Warby Parker gets a $3 billion valuation |Lucinda Shen |August 27, 2020 |Fortune 

Hana seeks refuge from the buzzing lights of Otome Road in a nearby café and makes another swirl with her straw. The Japanese Women Who Love Gay Anime |Brandon Presser |December 6, 2014 |DAILY BEAST 

Rumors swirl as Robert Downey Jr. confirms a return to the role that made the Marvel Universe. The Coming Civil War: Iron Man Vs. Captain America 3 |Rich Goldstein |October 19, 2014 |DAILY BEAST 

Tales still swirl about the strange forest ruins and mysterious happenings that have occurred around Gedi. Kenya Has Its Own Machu Picchu—the Lost Town of Gedi |Nina Strochlic |September 18, 2014 |DAILY BEAST 

Rumors swirl that pay equity or her management style are to blame. Jill Abramson Fired from the Times: Was It About Money and Sexism—Or Management Style? |Lloyd Grove |May 15, 2014 |DAILY BEAST 

Thousands of people are posting videos on YouTube while they gargle and swirl oil in their mouths. Oil Pulling: Miracle Cure or Oily Mess? |DailyBurn |March 28, 2014 |DAILY BEAST 

But here was one mighty wave that was always itself, and every fluted swirl of it, constant as the wreathing of a shell. English: Composition and Literature |W. F. (William Franklin) Webster 

He knew that Myra had been carried this way and that in the great, cruel, indifferent swirl that was life. The Hidden Places |Bertrand W. Sinclair 

What if the dust did swirl up in blinding sheets from the south? The Lion's Brood |Duffield Osborne 

The swirl of the current swept him into the shallower stream below. The Highgrader |William MacLeod Raine 

He was trapped in a blinding swirl of radiance, with darkness above it. The Status Civilization |Robert Sheckley