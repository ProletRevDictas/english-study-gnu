Whoever sends more soldiers to a given castle conquers that castle and wins its victory points. The Fifth Battle For Riddler Nation |Zach Wissner-Gross |September 4, 2020 |FiveThirtyEight 

To make the numbers, Tesla needs to conquer an additional one-fifth of the entire global luxury car market. Tesla has a business model problem: It can never justify its current stock price by simply making cars |Shawn Tully |August 29, 2020 |Fortune 

Many times, I’ve tried and failed to turn myself into one of those righteous dawn patrollers, who have conquered all of their demons and will inherit the Earth. The Sublime Agony of Hot-Weather Running |Martin Fritz Huber |August 27, 2020 |Outside Online 

Perhaps in Term Two the president can conquer the literal and figurative gridlock. Sunday Magazine: Go Inside Trump’s Second Term |Daniel Malloy |August 23, 2020 |Ozy 

NFL wide receiver Chris Hogan has now conquered the AFC East after signing on Sunday with the New York Jets, completing a doable but pretty uncommon feat across sports. The Summer Of Playoffs |Sarah Shachat |August 18, 2020 |FiveThirtyEight 

Do you feel like you can conquer anything on the film landscape after making these massive trilogies? ‘No Regrets’: Peter Jackson Says Goodbye to Middle-Earth |Alex Suskind |December 4, 2014 |DAILY BEAST 

"Give me a horse and a gun and an open plain, and we can conquer the world," he thundered before the assembled crowd. In Texas, Cruz, Perry Crow Over GOP Rout |Tim Mak |November 5, 2014 |DAILY BEAST 

He also told the paper that journalist James Foley deserved to die and that they will one day conquer the Vatican. Italy Steps Up Security Over Alleged ISIS Plot to Kill The Pope |Barbie Latza Nadeau |August 28, 2014 |DAILY BEAST 

If you hold to it, you will conquer Rome and own the world, if Allah wills. Pope Francis, ISIS, and the Last Crusade |Christopher Dickey |August 18, 2014 |DAILY BEAST 

Most recently, he has promised that followers who obey him “will conquer Rome and own the world.” Why the Caliphate Will Devour Its Children |Philip Jenkins |July 11, 2014 |DAILY BEAST 

To-day men of science are trying to conquer the horrors of cancer and smallpox, and rabies and consumption. God and my Neighbour |Robert Blatchford 

It was the spiritual way, whose method and secret lie in that subtle paradox: Yield to conquer. The Wave |Algernon Blackwood 

Their standards had nothing in common; in the one honour could conquer ambition, in the other ambition knew no rules of honour. Napoleon's Marshals |R. P. Dunn-Pattison 

Norman went slowly down, with failing knees, hardly able to conquer the shudder that came over him, as he passed those rooms. The Daisy Chain |Charlotte Yonge 

Thus it was that he himself created the morale which enabled him again and again to conquer against overwhelming odds. Napoleon's Marshals |R. P. Dunn-Pattison