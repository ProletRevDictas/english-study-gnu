But—and I forbear to lead up to it artistically—I dissever myself from your chariot wheels. Ancestors |Gertrude Atherton 

And thou shalt speak my words to them, if perhaps they will hear, and forbear: for they provoke me to anger. The Bible, Douay-Rheims Version |Various 

Georgie could not forbear a smile, while Lucy burst into inextinguishable peals of silvery laughter. The Pit Town Coronet, Volume II (of 3) |Charles James Wills 

Can they stand still when everything is in motion, when everything is stirring, and forbear running whither every one runs? The 'Characters' of Jean de La Bruyre |Jean de La Bruyre 

Nor can any right-minded man forbear his tribute to the good which Socialistic agitation has done. The Inhumanity of Socialism |Edward F. Adams