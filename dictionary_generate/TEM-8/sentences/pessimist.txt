“Some of us are optimists, some of us are pessimists, and some of us are in the middle,” Swaffer says. Dementia content gets billions of views on TikTok. Whose story does it tell? |Abby Ohlheiser |February 16, 2022 |MIT Technology Review 

It takes several episodes for sitcom writers to nail the chemistry in an ensemble cast, and I want more for Jesse than acting as the pessimist to Zoey’s optimist. How I Met Your Father and the Insufferable History of the Lovelorn Sitcom Dude |Eliana Dockterman |January 20, 2022 |Time 

There is tremendous inequality, polarization, populism, and I am not one of the pessimists about America. Is the U.S. Really Less Corrupt Than China? (Ep. 481) |Stephen J. Dubner |November 4, 2021 |Freakonomics 

Those who identify as optimists can be too quick to dismiss or downplay the problems of technology, while self-styled technology pessimists or progress skeptics can be too reluctant to believe in solutions. Why I’m a proud solutionist |Jason Crawford |July 13, 2021 |MIT Technology Review 

We should be fundamentally neither optimists nor pessimists, but solutionists. Why I’m a proud solutionist |Jason Crawford |July 13, 2021 |MIT Technology Review 

One such pessimist was the Mayor* of the town: A little while later, yielding to his vapors, he committed suicide. The Stacks: H.L. Mencken on the 1904 Baltimore Fire |H.L. Mencken |October 4, 2014 |DAILY BEAST 

Like Sendak, Miyazaki is somewhat cranky—a pessimist at odds with modernity. Hayao Miyazaki’s ‘The Wind Rises’: An Anime Icon Bows Out |Andrew Romano |November 15, 2013 |DAILY BEAST 

Pessimist: Which is a big reason why young people find it so difficult to get started in life. Post-Convention Hangover: America Isn't Working |David Frum |September 7, 2012 |DAILY BEAST 

Pessimist: But most of the new jobs being created pay much less than the jobs lost in 2008-2009. Post-Convention Hangover: America Isn't Working |David Frum |September 7, 2012 |DAILY BEAST 

Pessimist: I thought you said you were an optimist - not a fantasist. Post-Convention Hangover: America Isn't Working |David Frum |September 7, 2012 |DAILY BEAST 

If a fervent desire to help Man, instead of wasting time in prayer to "God," is pessimism, I am a pessimist. Tyranny of God |Joseph Lewis 

I ain't what they call a pessimist, but I thinks poorly of most things. The Adventures of Harry Revel |Sir Arthur Thomas Quiller-Couch 

Clemens was forty-eight, and becoming more and more the philosopher; also, in logic at least, a good deal of a pessimist. Mark Twain, A Biography, 1835-1910, Complete |Albert Bigelow Paine 

He was no pessimist, croaking out doleful prophecies and lamentations and bitter criticisms. When the Holy Ghost is Come |S. L. Brengle 

When the Semitic skin of Job is scratched, we find a modern pessimist beneath. Encyclopaedia Britannica, 11th Edition, Volume 15, Slice 4 |Various