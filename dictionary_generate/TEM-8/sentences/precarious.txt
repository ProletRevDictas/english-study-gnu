While places like Buca di Beppo are hardly independent restaurants, presumably Fieri’s restaurant concept keeps some members of the service industry employed at a time when restaurant work is precarious. Welcome to Flavortown, Population: Us |Jaya Saxena |February 11, 2021 |Eater 

The unique challenges some older people in the US face with digital access is making a precarious situation worse for them. A big hurdle for older Americans trying to get vaccinated: Using the internet |Rebecca Heilweil |January 27, 2021 |Vox 

As gender non-conforming persons, our natal homes can often render our wellbeing precarious. The Covid-19 pandemic hasn’t been a “great equaliser” for India’s queer population |Neel Sengupta |January 22, 2021 |Quartz 

An absolutely global union would have no such motivation, making it far more precarious. Why a Universal Society Is Unattainable - Issue 95: Escape |Mark W. Moffett |January 14, 2021 |Nautilus 

Indeed, after watching Bridgerton, I concluded in my review that “its most interesting ideas — like Simon’s precarious place in society as a duke of color or his mysterious time abroad — get almost no attention.” The debate over Bridgerton and race |Aja Romano |January 7, 2021 |Vox 

But he has somehow leapt to a higher plateau during the last few years—all the more amazing given his precarious health. The Best Albums of 2014 |Ted Gioia |December 13, 2014 |DAILY BEAST 

Somebody else suggests that the evidence is precarious, coming as it does from victims who might not make confident witnesses. The Castration of Alan Turing, Britain’s Code-Breaking WWII Hero |Clive Irving |November 29, 2014 |DAILY BEAST 

It was a precarious life, tented on the fairgrounds with all earnings in cash. Best Career Arc Ever: From Burlesque To Bartending |Anne Berry |September 13, 2014 |DAILY BEAST 

The incident highlights the precarious state of Afghanistan as U.S. troops prepare to withdraw from the country. Who Assassinated a U.S. General? |Eli Lake |August 6, 2014 |DAILY BEAST 

Their harrowing escape to Erbil has ended in a precarious and hardscrabble existence. Church Bells Fall Silent in Mosul as Iraq’s Christians Flee |Andrew Doran, Drew Bowling |June 29, 2014 |DAILY BEAST 

When riding along a good road his position was precarious enough, requiring all his best efforts to maintain his balance. Our Little Korean Cousin |H. Lee M. Pike 

To what sad, precarious, and miserable existence does he vow himself—he who takes up a literary career? Charles Baudelaire, His Life |Thophile Gautier 

She had felt that no really nice girl would travel so far on so precarious an errand, particularly when she was alone. The Amazing Interlude |Mary Roberts Rinehart 

Um could reach the highest shelf without standing on an inverted rice-pot, or the even more precarious fish-cleaning bench. The Dragon Painter |Mary McNeil Fenollosa 

The life of the forest and desert hunters is one of incessant activity, and their food supply is precarious. Man And His Ancestor |Charles Morris