If you’ve ever been enthralled by the idea of owning a sleek, easy-to-ride electric bike, you know how quickly that charm can fade once you see the price—they average between $2,000 and $3,000. Your new e-bike could already be sitting in your garage |William Elcock |February 7, 2022 |Popular-Science 

“Paystone is on a mission to help businesses grow, and we were enthralled by their commitment to that mission and their focus on service-oriented verticals,” said Léa Perge, investor at Crédit Mutuel Equity in Canada, via email. Payments company Paystone raises $23.8M to help service-based businesses engage with customers |Christine Hall |July 23, 2021 |TechCrunch 

This exciting, poignant book enthralled even me, whose school years were spent in ancient times. The best audiobooks for your summer drive, sorted by length — and who’s in the car |Katherine A. Powers |May 27, 2021 |Washington Post 

He was enthralled by the stories that Raquel’s father, a decorated Vietnam veteran, told about the war, and he’d always liked to help people. Caron Nazario saw Eric Garner, who he called ‘uncle,’ die in police hands. Then officers pepper-sprayed him six years later. |John Woodrow Cox, Michael Rosenwald |April 15, 2021 |Washington Post 

For years, the internet has been enthralled by videos of robots doing far more than walking and regaining their balance. This Robot Taught Itself to Walk in a Simulation—Then Went for a Stroll in Berkeley |Jason Dorrier |April 11, 2021 |Singularity Hub 

He defied the earth-magic, defied those sylvan deities who as he divined, sought to enthral him. The History of Sir Richard Calmady |Lucas Malet 

All night long he was sending messages directing the plan of battle the news of which was to enthral the civilized world. Drake, Nelson and Napoleon |Walter Runciman 

For my part, I think the Indians would be perfectly right to emancipate themselves from the galling chains which enthral them. Manco, the Peruvian Chief |W.H.G. Kingston 

Already her peace reigns o'er hill and hall, her rapturous awe the heart does enthral; allow then the light to fall! Tristan and Isolda |Richard Wagner 

London women are very hard to beat in the little things which captivate and enthral. John Brown |Captain R. W. Campbell