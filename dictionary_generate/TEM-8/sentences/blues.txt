My Playground Was a Ditch by the Train Tracks could be a nice blues album if you decide to keep doing work that doesn’t require your face. W. Kamau Bell on Searching for a United America |eriley |October 26, 2021 |Outside Online 

One of my favorite blues records was recorded in 1962 by Williams, a Louisianan guitarist who spent years in the Louisiana State Penitentiary before getting released. Cry Your Heart Out: The 11 Best Breakup Albums Ever Made |Peter Allen Clark |September 17, 2021 |Time 

I’ll need money and clothes, but not the gorgeous red blues guitar hanging on the wall. Why We New Orleanians Keep Coming Back After Every Storm |Maurice Carlos Ruffin |September 2, 2021 |Time 

After kicking his heroin habit temporarily for the first time, he soon emerged with a 10-minute, driving blues number that he reportedly jotted down on toilet paper during the session. Lee Morgan’s ‘Live at the Lighthouse’ was a masterpiece that turned out to be a farewell |Shannon J. Effinger |August 27, 2021 |Washington Post 

Hill was a seasoned blues musician by the time he joined ZZ Top, having played bass with performers including Freddie King and Lightnin’ Hopkins. Dusty Hill, ZZ Top bassist with a legendary beard, dies at 72 |Harrison Smith |July 29, 2021 |Washington Post 

Ragtime, blues, country, jazz, soul, and rock and roll were all pioneered or inspired by black artists. The Cultural Crimes of Iggy Azalea |Amy Zimmerman |December 29, 2014 |DAILY BEAST 

Artists like Mick Jagger and Van Morrison obsessively revered and imitated African-American blues and rock musicians. The Cultural Crimes of Iggy Azalea |Amy Zimmerman |December 29, 2014 |DAILY BEAST 

You spice it with blues and skiffle music, and pickle it in alcohol and tobacco smoke. The Greatest Rock Voice of All Time Belonged to Joe Cocker |Ted Gioia |December 23, 2014 |DAILY BEAST 

"Goin' Down Slow" Cocker and Elton John duetted on this blues cover on French music show Trafic Musique in 2005. Joe Cocker's Deep Live Cuts |Asawin Suebsaeng |December 22, 2014 |DAILY BEAST 

English blues and rock singer Joe Cocker has died at age 70. Joe Cocker's Deep Live Cuts |Asawin Suebsaeng |December 22, 2014 |DAILY BEAST 

She dressed in simple lines and quiet tones, dark blues and black, with only a broad lace collar and cuffs in neat relief. The Woman Gives |Owen Johnson 

The woods of the Undercliff sank softly to the blues and purple, the silver streaks and gorgeous shadows of the sea floor. Marriage la mode |Mrs. Humphry Ward 

I shall hear of his having the blues and nervous attacks next, I suppose. The Seven Cardinal Sins: Envy and Indolence |Eugne Sue 

She made a white-robed, dusky figure against the deep blues of my big window. The New Machiavelli |Herbert George Wells 

I like the Wall of Troy design you are using, and the blues and gray will be a good combination. Patchwork |Anna Balmer Myers