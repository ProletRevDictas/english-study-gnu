For instance, the Jazz were ousted in five games by the eventual champion San Antonio Spurs, while the Celtics lost an ugly six-game affair against the New Jersey Nets. The Miami Heat Act Like They’ve Been Here Before. They (Mostly) Haven’t. |Andres Waters |September 15, 2020 |FiveThirtyEight 

His contacts in Kyiv included close associates of Ukraine’s corrupt former President Viktor Yanukovych, who was ousted in a revolution in 2014. Rick Perry’s Ukrainian Dream |by Simon Shuster, TIME, and Ilya Marritz, WNYC |September 10, 2020 |ProPublica 

Viacom was actually worried about me being ousted and thought it was a dangerous move because I was the brand. When Did Gavin McInnes Lose His Mind? |Eugene Robinson |September 6, 2020 |Ozy 

The Food and Drug Administration on Friday ousted its top spokesperson, Emily Miller, after less than two weeks on the job, reports the New York Times. FDA removes top spokesperson after 11 days on the job |Marisa Fernandez |August 28, 2020 |Axios 

Following the passionate viewer enthusiasm for Barack Obama’s 2008 campaign and election, Klein was ousted from CNN in 2010 amid low prime-time ratings and, in his opinion, post-election viewer fatigue during the financial crisis. ‘There’s no antagonist’: News outlets mull the possible end of their editorial and business-side ‘Trump Bump’ bonanza |Steven Perlberg |August 10, 2020 |Digiday 

The broader goal was to oust Saddam in order to build a beautiful democracy in the Middle East and thereby transform the region. Can America Still Win Wars? |Michael Tomasky |October 4, 2014 |DAILY BEAST 

Two years later, Kansas helped oust Curtis—and Hoover—by voting for Franklin Roosevelt and re-electing McGill. A Loss by Pat Roberts in Kansas? Actually, Not So Bizarre |Jeff Greenfield |October 3, 2014 |DAILY BEAST 

In 1992 Dostum “defected” to the side of the mujahedin and joined in the battle to take Kabul and oust Najibullah. The Warlord Who Defines Afghanistan: An Excerpt From Bruce Riedel’s ’What We Won’ |Bruce Riedel |July 27, 2014 |DAILY BEAST 

The Ukrainian parliament was deciding whether to officially oust President Victor Yanukovych. How Ukraine’s Parliament Brought Down Yanukovych |Maxim Eristavi |March 2, 2014 |DAILY BEAST 

Just two weeks ago, Prayuth felt compelled to deny that senior figures in the country had pressured him to oust the government. Can Thailand’s Prime Minister Cling To Power? |Lennox Samuels |February 19, 2014 |DAILY BEAST 

Junot already held Portugal; it seemed as if it needed but a vigorous movement to oust the Bourbons from Madrid. Napoleon's Marshals |R. P. Dunn-Pattison 

The nativesʼ anxiety to oust the Spaniards was far stronger than their wish to be under American, or indeed any foreign, control. The Philippine Islands |John Foreman 

Who would there be who could effectively contest his claim, or oust him from his place? Tristram of Blent |Anthony Hope 

Of old time, golden wheat conquered and held possession, and now the grass threatens to oust the conqueror. The Hills and the Vale |Richard Jefferies 

However, the badger is nothing if not persevering, and Stubbs and Grunter decided to make one last effort to oust the invader. Lives of the Fur Folk |M. D. Haviland