I’ve never really paid much heed to the purported differences between millennials and Gen Xers or whatever else, but certainly what are referred to as Gen Z, new people coming into the workforce, have different expectations in life. America's Workplaces Are Seeing a Job Turnover 'Tsunami,' According to JLL's Work Dynamics CEO |Eben Shapiro |July 18, 2021 |Time 

Titled “India’s Covid-19 Emergency,” the editorial in one of the world’s oldest and best-known general medical journals blamed the government for not paying heed to superspreader events and not preventing the unprecedented surge of Covid-19 cases. India could see over 1 million Covid deaths by August due to Modi’s “self-inflicted national catastrophe” |Manavi Kapur |May 8, 2021 |Quartz 

Paying heed to the importance of visualizing data means following a few best practices. Unlocking the secrets of data storytelling in 2021 |Ronita Mohan |March 18, 2021 |Search Engine Watch 

He claimed to be worried about the children, but paid no heed to guidance of medical organizations — including the American Academy of Pediatrics — that recommend treating gender-diverse children by affirming their gender identities. Rand Paul’s ignorant questioning of Rachel Levine showed why we need her in government |Monica Hesse |February 26, 2021 |Washington Post 

The science of minutes management is fairly clear, and most teams have taken heed of it. How The Best NBA Teams Juggle Their Lineups |Jared Dubin |February 23, 2021 |FiveThirtyEight 

When it comes to educating our children, Congress should heed that message, not ignore it. The ‘No Child’ Rewrite Threatens Your Kids’ Future |Jonah Edelman |January 3, 2015 |DAILY BEAST 

But now, Obama should heed his call for a ‘service year’ and get on board. It’s Time for Obama to Heed McChrystal’s Call for the ‘Service Year’ |Jonathan Alter |June 23, 2014 |DAILY BEAST 

TMZ should show some decency and heed the plea Krizya Fuqua. TMZ Makes Tragedy Porn Out of Tracy Morgan’s Gruesome Car Accident |Dean Obeidallah |June 12, 2014 |DAILY BEAST 

Failing that, Hillary Clinton should heed his findings about wealth and inequality—and take on the crisis head on. Real Vs. Republican Populism: How to Win the War on Inequality |Michael Tomasky |April 16, 2014 |DAILY BEAST 

I texted him that I would heed his advice and schedule an appointment with the specialist. My Therapist Dumped Me |Lizzie Crocker |April 4, 2014 |DAILY BEAST 

The man that giveth heed to lying visions, is like to him that catcheth at a shadow, and followeth after the wind. The Bible, Douay-Rheims Version |Various 

But she was young enough and pretty enough to pay little heed to pose or background. The Red Year |Louis Tracy 

She waved a dissenting hand, and went on, paying no further heed to their renewed cries which sought to detain her. The Awakening and Selected Short Stories |Kate Chopin 

He gave little heed to the play; his thoughts were elsewhere, and, while they rambled, his eyes wandered round the house. Confidence |Henry James 

Do not heed the Governor-Generalʼs decree, calling you to arms, even though it cost you your lives. The Philippine Islands |John Foreman