At the heart of celluloid, however, was the natural substance cellulose. Materials of the last century shaped modern life, but at a price |Carolyn Wilke |January 28, 2022 |Science News 

Creative Cauldron’s producing director Laura Connors Hull brought them the obscure yet charming book that surprisingly had never before been reworked for stage or celluloid, and the pair got to work last spring. Meet the husbands and creative partners behind ‘Christmas Angel’ |Patrick Folliard |December 4, 2021 |Washington Blade 

People surviving or dying in ways at once shudderingly alien and hauntingly familiar, if only on celluloid. Sept. 11, 2001: An ordinary work day, then surreal scenes of dread and death |David Maraniss |September 10, 2021 |Washington Post 

Robinson’s film work was just one part of his meteoric career, and his choices were limited by industry practices of the times, but the problematic celluloid images stuck. ‘The Mayor of Harlem’ celebrates Bill Robinson as dance superstar, as well as social activist |Celia Wren |May 21, 2021 |Washington Post 

In the most crowd-pleasing section of the exhibition—dubbed Stage and Screen—hang his pictures of celluloid legends. How Horst Captured Dietrich, Rita Hayworth, and Vivien Leigh—and Changed Fashion Photography |Patrick Strudwick |September 8, 2014 |DAILY BEAST 

“Everyone could see their love right there on celluloid,” added their son, Stephen Bogart. Bogie & Bacall: A Hollywood Romance for the Ages |Marlow Stern |August 13, 2014 |DAILY BEAST 

Of course, a great literary work does not a great film make—if it did, Shakespeare would need merely to be slathered on celluloid. ‘The Trial’ & More Top Film Adaptations of Literary Classics (VIDEO) |Jimmy So |November 24, 2012 |DAILY BEAST 

On celluloid, similar fates have been met—sometimes even worse. Will ‘Avatar’ Actress Zoe Saldana Play Legendary Singer Nina Simone? |Karu F. Daniels |August 23, 2012 |DAILY BEAST 

In her hands, celluloid comes off as a medium that allows for old-fashioned rumination, with some of the slowness of oil paint. Tacita Dean’s ‘Five Americans’ Captures a Quiet Brilliance |Blake Gopnik |May 7, 2012 |DAILY BEAST 

Real celluloid ivory combs, fit for the President's wife, sure enough. Narcissa, or the Road to Rome |Laura E. Richards 

No high gloss to look like Celluloid or Paper Collars, but a nice medium finish that has all the appearance of new work. My Pet Recipes, Tried and True |Various 

The front of the box is provided with a handle and a celluloid label for the name of the contained medium. The Elements of Bacteriological Technique |John William Henry Eyre 

Then he sat down and pulled out his mothers celluloid memorandum tablets. Winona of the Camp Fire |Margaret Widdemer 

Just behind the nervous young man with the celluloid collar sat a stout individual with a bald head. Cap'n Eri |Joseph Crosby Lincoln