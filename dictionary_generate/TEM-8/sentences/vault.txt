Reading this book is like sticking a stethoscoped ear to the vault containing the cryptocurrency industry’s origins. The best books of 2020, according to Fortune staff |Rachel King |December 6, 2020 |Fortune 

VOSD resident historian Randy Dotinga — we truly don’t know where he gets these things — is back with another one from the vault in honor of Wednesday’s vice presidential debate. Morning Report: What Dems Say They’d Do With County Control |Voice of San Diego |October 8, 2020 |Voice of San Diego 

You can set how the software interacts with your browser, set up alerts for when your credentials are involved in a data breach, and set a default vault for new passwords. How to get started using a password manager |David Nield |September 8, 2020 |Popular-Science 

All the closing documents go into a digital vault that attorneys and lenders can access online. First he took energy trading and the NYSE electronic. Now Jeff Sprecher of ICE shares his plans to digitize your mortgage |Shawn Tully |September 2, 2020 |Fortune 

A gymnast launching off the vault will flip and twist multiple times within a single second. A robot referee can really keep its ‘eye’ on the ball |Kathryn Hulick |August 20, 2020 |Science News For Students 

It was fronted by what looked like a heavily reinforced bank vault door. Writing a Novel: Even Making It Up Requires Research |Ridley Pearson |July 16, 2014 |DAILY BEAST 

They are simply repositories of capital, like so many gold bars in the vault of a bank. London’s Oligarch Ghost Town |Tom Sykes |June 16, 2014 |DAILY BEAST 

It was, he writes, like “looking into the vault of the universe.” Exploring the Amazon, While We Still Can |Darrell Hartman |May 15, 2014 |DAILY BEAST 

For decades, these fascinating recordings languished in the Cash Family vault, unheard and unremembered. The Inside Story of Johnny Cash’s Legendary Lost LP, ‘Out Among the Stars’ |Andrew Romano |March 18, 2014 |DAILY BEAST 

Now, we're in a very small, very hot room with no decor—there is only an ominous-looking vault door. The Secret Speakeasies of Buenos Aires |Jeff Campagna |February 25, 2014 |DAILY BEAST 

(b) To open the Treasury vault, the presence of two persons designated by the Secretary of the Treasury is required. Readings in Money and Banking |Chester Arthur Phillips 

Each entry on the vault record book shall be signed by the persons having access to the safe. Readings in Money and Banking |Chester Arthur Phillips 

On the 18th fifty heavy shells, including 12-inch and 14-inch, dropped out of the blue vault of heaven on to the Anzacs. Gallipoli Diary, Volume I |Ian Hamilton 

Coloured shafts mapped the vault from horizon to zenith like the spokes of a prodigious wheel of fire. The Wave |Algernon Blackwood 

Cautiously they began to go up into the dark vault of the upper house, the boards creaking under their weight. The Empty House And Other Ghost Stories |Algernon Blackwood