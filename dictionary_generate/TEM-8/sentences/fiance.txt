My fiancé has a ten-year-old daughter who’s becoming interested in sports, and I encouraged her to add this to her reading list. Everything Our Editors Loved in December |The Editors |January 8, 2021 |Outside Online 

My fiancé said that his commitment to me is also a lifetime commitment to her. ‘Power’ Star Naturi Naughton Got Engaged For Christmas |Charli Penn |January 1, 2021 |Essence.com 

I have gotten many friends registered to vote, the hardest one being my fiancé, who has never taken an interest in politics prior to this year. A Front-Line Struggle: Getting Reluctant Voters to Vote |Eugene Robinson |November 3, 2020 |Ozy 

She and her fiancé have been very cautious throughout the pandemic, ordering in groceries and even skipping out on festivities for their child’s first birthday. Beauty businesses are struggling without us |Melinda Fakuade |October 30, 2020 |Vox 

So, although it’s odd to say your ex-fiancé, my ex fiancé is one of those people for me. Full Transcript: Tomi Lahren on ‘The Carlos Watson Show’ |Daniel Malloy |August 31, 2020 |Ozy 

And this is all on top of a 2005 incident, where a former fiance sought a restraining order against him. Yet Again, George Zimmerman Proves He’s Violent, Aggressive, and Confrontational |Jamelle Bouie |November 19, 2013 |DAILY BEAST 

Dear Blogger: My fiance and I are roughly-30-year-old software developers. First World Financial Problems |Megan McArdle |March 11, 2013 |DAILY BEAST 

I used to be into you and your fiance Blake Shelton's tweets, but neither of you have really been tweeting anymore. Miranda Lambert Dishes With Meghan McCain |Meghan McCain |February 12, 2011 |DAILY BEAST 

Katie Finneran's emotional acceptance speech dedicated to her fiance. The Tonys Bring Out the Stars |Jacob Bernstein |June 14, 2010 |DAILY BEAST 

He employed Corentin to clear up the dark side of the life of Clotilde's fiance. Repertory Of The Comedie Humaine, Complete, A -- Z |Anatole Cerfberr and Jules Franois Christophe 

But certainly I had not expected to see sitting on the page written in my fiance's hand, the face of another woman. The Way of a Man |Emerson Hough 

The father agrees with her; and he and her fiance, this gentleman here, have run away with her, to prevent her being locked up. The Cornet of Horse |G. A. Henty 

And then suppose after you came home you took your wife, or your mother, or your fiance, to see this play. Seeing Things at Night |Heywood Broun 

He merely returned home to bid his last farewell to his dying mother, and to intrust her to the care of his fiance. A Fantasy of Far Japan |Baron Kencho Suyematsu