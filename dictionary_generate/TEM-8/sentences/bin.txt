The laundry bins can be carried over to your washer and then slide back into the bamboo frame. Attractive laundry hampers that make your dirty clothes look a little better |PopSci Commerce Team |September 16, 2020 |Popular-Science 

You can buy collapsible buckets made for this task, or just bring some plastic bins from home. The Absolute Beginner's Guide to Camp Cooking |AC Shilton |September 5, 2020 |Outside Online 

Better yet, collect dirty water in a bin and pour it in a cathole when you’re done to avoid attracting wildlife. A Dirtbag's Guide to Sanitation During a Pandemic |Maren Larsen |August 31, 2020 |Outside Online 

Trash would overflow every bin and was stuck in every compartment. What I learned from 5 years of cleaning airplanes in the middle of the night |matthewheimer |August 30, 2020 |Fortune 

These days I’m into storage bins, label makers, and color-coordinated folders. The Matador SEG42 Offers Unrivaled Gear Organization |Graham Averill |August 25, 2020 |Outside Online 

You were basically the guy to do every dictator or crazy character, from Gaddafi and Ahmadinejad to Bin Laden. Coffee Talk with Fred Armisen: On ‘Portlandia,’ Meeting Obama, and Taylor Swift’s Greatness |Marlow Stern |January 7, 2015 |DAILY BEAST 

The 2001 grand jury indictment named 21 suspects as being involved in the U.S. embassy bombings, including Osama bin Laden. Final Chapter for Accused Africa Bomber |Jamie Dettmer |January 4, 2015 |DAILY BEAST 

Fellow SEAL Matt Bissonnette also claims to have shot bin Laden. Exclusive: Bin Laden ‘Shooter’ Under Investigation for Leaking Secrets |Shane Harris |December 23, 2014 |DAILY BEAST 

Yet I had serious trouble understanding how to cheer on the news of Bin Laden or anyone else dying. The Life and Hard Times Of The Family A Cuban Defector Left Behind |Brin-Jonathan Butler |December 19, 2014 |DAILY BEAST 

In the mid-1990s, some visitors did begin to show up due to the construction efforts of Osama Bin Laden. Egypt Ain’t The Only Pyramid Show In Town |Nina Strochlic |December 11, 2014 |DAILY BEAST 

He told Horner you'd bin shot resistin' arrest, an' wanted t' see her afore yuh cashed in. Raw Gold |Bertrand W. Sinclair 

Our picture shows a bin of crude up-river Para the finest rubber known. The Wonder Book of Knowledge |Various 

As soon as he gets Miss Holland they go up by leaps and bounds, and it's bin goin' steady ever since. The Creators |May Sinclair 

Uncle Bin never flung that accusation at women if they were merely implicated. In the Onyx Lobby |Carolyn Wells 

No, Sir, what you smell ain't incense—on'y the vaults after the damp weather we've bin 'aving. Punch, or the London Charivari, Vol. 108, April 6, 1895 |Various