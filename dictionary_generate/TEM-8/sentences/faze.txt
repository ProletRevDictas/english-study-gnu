Even a $5 billion fine imposed by the Federal Trade Commission after the Cambridge Analytica scandal didn’t really faze the company. Facebook’s strategy: Avert disaster, apologize and keep growing |Susan Benkelman |July 9, 2021 |Washington Post 

Although some leagues were fazed less than others, with the NFL somehow coming out as predictable as ever — because of course it did — many of the sports we track had some of their most chaotic seasons in recent memory. How Well Did Our Sports Predictions Hold Up During A Year Of Chaos? |Neil Paine (neil.paine@fivethirtyeight.com) |June 8, 2021 |FiveThirtyEight 

It doesn’t faze the giraffes, but a pressure like that would cause all sorts of problems for people, from heart failure to kidney failure to swollen ankles and legs. Heads up! The cardiovascular secrets of giraffes |Ars Staff |May 30, 2021 |Ars Technica 

However, when asked about his comfort level putting a rookie in net, he said it didn’t faze him. Capitals not naming their starting goaltender ahead of playoff opener |Samantha Pell |May 13, 2021 |Washington Post 

Viewers can’t look away, cringe from vicarious fear, or some combination of both — and are amazed that Thompson isn’t fazed at all. TikTok’s viral beekeeper is getting a lot of ... buzz |Travis Andrews |March 18, 2021 |Washington Post 

I said I already know about Ferguson, nothing new can faze me about Ferguson. The Day Ferguson Cops Were Caught in a Bloody Lie |Michael Daly |August 15, 2014 |DAILY BEAST 

None of this has seemed to faze the journalist, who, this fashion season, displayed her trademark ability to excite and infuriate. Fashion's Most Feared Critic |Jacob Bernstein |October 11, 2010 |DAILY BEAST 

First, the cheap shot threats that might have intimidated other victims and their lawyers don't faze me. Steven Seagal Under Siege |John Connolly |April 18, 2010 |DAILY BEAST 

J'Absolutely Refuse to let this afternoon's decision faze me. The Bag Lady Papers, Part IV |Alexandra Penney |January 14, 2009 |DAILY BEAST 

They may tell me we had a heavy storm in the middle of the night, but it didn't faze me one little bit. Chums of the Camp Fire |Lawrence J. Leslie 

Once or twice they came near having a spill in wind that didn't faze us a bit. The Outdoor Girls in a Winter Camp |Laura Lee Hope 

She got witch en' hoodoo charms, en' say ol' Nick en' all his imps cayn't faze 'er. The Cottage of Delight |Will N. Harben 

With only two of us aboard you know how easy she climbed; three passengers she could hoist, but four might faze her. The Aeroplane Boys Flight |John Luther Langworthy 

Seems as though his sense of proportion was out of gear; and you cant faze him, either. Weatherby's Inning |Ralph Henry Barbour