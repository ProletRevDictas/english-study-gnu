Men nudged their neighbours; some looked frightened and some grinned at the treasonable words. Mistress Wilding |Rafael Sabatini 

Complaint was immediately made to the Parliamentary Committee of Examination that it contained treasonable and scandalous matter. Witch, Warlock, and Magician |William Henry Davenport Adams 

In other words, he had cast her horoscope, a proceeding common enough in those days, and one which had no treasonable complexion. Witch, Warlock, and Magician |William Henry Davenport Adams 

The caballing for dissolution of the Union, why should that be treasonable? Blackwood's Edinburgh Magazine, Volume 54, No. 334, August 1843 |Various 

For those purposes a treasonable book against the King's right to the Crown was 'divulged.' Sir Walter Ralegh |William Stebbing