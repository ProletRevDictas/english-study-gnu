While Tide was a bit underwhelming, the Ecover did as well or better on dirt and odor tests than any fancy sports-specific stuff, on both synthetics and woolens. How to Clean Your Cycling Gear |agintzler |July 17, 2021 |Outside Online 

Learn to iron, eliminate horrible stains, wash woolens and other awkward-to-clean items, and see how to rescue yellowed linens and special-event clothing like a pro. Laundry is his love language |Terri Schlichenmeyer |May 4, 2021 |Washington Blade 

These woolen blankets are equally useful when you’re camping out under the stars or camped out on the sofa daydreaming of your next trip. 37 Gifts to Buy This Mother's Day for Every Type of Mom in Your Life |Melissa Locker |May 3, 2021 |Time 

Illustrators depicted them weighed down by heavy woolens, trudging through wintry woods, seemingly always on their way to church. How America Keeps Adapting the Story of the Pilgrims at Plymouth to Match the Story We Need to Tell |Peter C. Mancall |December 17, 2020 |Time 

Two men, dressed in winter coats and woolen hats to defend against the frigid night air, walk side by side through the crowded streets of downtown Seoul. Know when to fold ’em: How a company best known for playing games used A.I. to solve one of biology’s greatest mysteries |Jeremy Kahn |November 30, 2020 |Fortune 

His face is gory and pitted with deep shrapnel wounds and his injured hands drip blood on the rumpled woolen blankets. Hezbollah and Assad Plan Offensive On Jihadists In Yabrud |Jamie Dettmer |January 31, 2014 |DAILY BEAST 

Good hygiene, students are advised, is important for warding away bad breath and bad smells from that woolen suit. Perfect Your Ho-Ho-Ho’s at the Top Santa-Training School |Nina Strochlic |December 26, 2013 |DAILY BEAST 

Leaning back in his chair, a striped woolen scarf thrown cavalierly around his neck, Carr frowned. Earthquake at The New York Times |Nicole LaPorte |January 24, 2011 |DAILY BEAST 

As his student in the spring of 1992, I remember his woolen sweaters and an umbrella, and perhaps even a shepherd's hat. My Professor, the Spy |Tom Murray |June 9, 2009 |DAILY BEAST 

By spreading over us the heavy woolen blankets the Mounted Police use under their saddles, we slept in comfort. Raw Gold |Bertrand W. Sinclair 

Both dress and cloak should be made of a woolen material, (varying of course with the season,) which will shed water. The Ladies' Book of Etiquette, and Manual of Politeness |Florence Hartley 

A woolen skirt, made quite short, to clear the muddy streets, is the proper thing. The Ladies' Book of Etiquette, and Manual of Politeness |Florence Hartley 

If you are to pass the night in the cars, carry a warm woolen or silk hood, that you may take off your bonnet at night. The Ladies' Book of Etiquette, and Manual of Politeness |Florence Hartley 

He is an old, grizzled man dressed in dungaree pants, a sweater, and a woolen cap with ear flaps. Fifty Contemporary One-Act Plays |Various