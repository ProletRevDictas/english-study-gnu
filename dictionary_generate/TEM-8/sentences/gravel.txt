Gow went back to the family’s ranch house, low and brown and cut into the forest where the gravel road ends in a loop, the living room stuffed with prize saddles and trophy buckles, the walls stocked with photos of a family that ropes and races. It’s His Land. Now a Canadian Company Gets to Take It. |by Lee van der Voo for ProPublica |October 1, 2020 |ProPublica 

Ariella Gintzler Some magic happens when you swap out your skinny road tires with 40-millimeter treaded rubber on a gravel bike. 11 Great Microadventures You Can Do Now |The Editors |October 1, 2020 |Outside Online 

The Pathway offers a balance of everyday style, technical performance, and ample coverage that’s perfect for bikepacking and gravel adventures. New Gravel Bike Accessories for a Smoother Ride |Josh Patterson |September 28, 2020 |Outside Online 

Pereyra said without warning, Profitt pulled him to the ground, struck him in the back of the head and pushed his face into a gravel driveway. How Criminal Cops Often Avoid Jail |by Andrew Ford, Asbury Park Press |September 23, 2020 |ProPublica 

Walk into any bike shop and you’ll see road, mountain, gravel, casual, and commuter bikes, and e-versions of all of these, but likely nothing made for carrying more than the rider and a modest amount of stuff. Do You Want to Buy an E-Cargo Bike? Read This First. |Joe Lindsey |August 30, 2020 |Outside Online 

They dress in clothing from the flophouse lost-and-found and are groomed with a hacksaw and gravel rake. Up to a Point: They Made Me Write About Lena Dunham |P. J. O’Rourke |December 13, 2014 |DAILY BEAST 

A much larger gravel lot across the street also exhibits multiple sites of seepage, as if pixelating from below with black matter. The Fiery Underground Oil Pit Eating L.A. |Geoff Manaugh |December 6, 2014 |DAILY BEAST 

Down a one-lane gravel road sits the house Burton left seven years ago. Mrs. Manson, Hometown Antihero |Justin Glawe |November 24, 2014 |DAILY BEAST 

The truck pulls the guitar over railroad tracks, through rocks and gravel, whatever, and the guitar is still speaking. Greil Marcus Talks About Trying to Unlock Rock and Roll in 10 Songs |Allen Barra |November 17, 2014 |DAILY BEAST 

The majority of the slabs that constituted the wall were demolished and used for highway gravel. What Happened to the Berlin Wall? |James Kirchick |November 9, 2014 |DAILY BEAST 

Suddenly his quick eye lit on something in the gravel path and his heart gave a great leap. The Joyous Adventures of Aristide Pujol |William J. Locke 

Some chickens were clucking outside the windows, scratching for bits of gravel in the grass. The Awakening and Selected Short Stories |Kate Chopin 

The train stopped, and he stood on the grey gravel platform like a man in a dream. Three More John Silence Stories |Algernon Blackwood 

Thence were taken fifteen baskets of gravel and dirt, which has the color of coal, in order to assay it. The Philippine Islands, 1493-1898, Volume XX, 1621-1624 |Various 

As soon as he had reached the south gate he had ascertained this by a glance at the gravel shoulder of the road. Hooded Detective, Volume III No. 2, January, 1942 |Various