Several used loans to pay for additional visa processing and many customers turned to Stilt because other financial providers shut down or reduced their loan programs over concerns about repayment. Stilt, a financial services provider for immigrants, raises $100 million debt facility from Silicon Valley Bank |Catherine Shu |January 27, 2021 |TechCrunch 

With Hernán Losada, United’s new coach, awaiting his work visa in Belgium, technical director Stewart Mairs and assistant coach Chad Ashton joined Kasper in overseeing the draft. In first round of MLS draft, D.C. United doubles up on ACC talent |Steven Goff |January 21, 2021 |Washington Post 

That’s partly why popular tourist destinations like Barbados and Costa Rica have created special “digital nomad” visas that waive the requirement to file taxes at all for a while. Can These Creative Taxes Catch Up With a Changing World? |Nick Fouriezos |January 14, 2021 |Ozy 

The attack on immigration policies, everything from DACA to even temporary visas that were suspended or terminated, is a huge issue for San Diego. Border Report: Gloria Brings in a Cross-Border Heavy-Hitter |Maya Srikrishnan |January 11, 2021 |Voice of San Diego 

The team was supposed to begin investigations in the first week of January, but China reportedly failed to approve their visas. WHO team investigating the origin of COVID-19 will enter China after delays |Eamon Barrett |January 11, 2021 |Fortune 

Undeterred by the snub in November, and denied a visa to Italy, Agca made plans for clandestine travel to Vatican City. Pope-Shooter Ali Agca’s Very Weird Vatican Visit |Barbie Latza Nadeau |December 29, 2014 |DAILY BEAST 

The language school did not focus on providing instruction but instead was a visa mill. ISIS, Boko Haram, and the Growing Role of Human Trafficking in 21st Century Terrorism |Louise I. Shelley |December 26, 2014 |DAILY BEAST 

I eventually left the West Bank for Jordan with a visa I obtained from the French embassy. What It’s Like to Be an Atheist in Palestine |Waleed al-Husseini, Movements.Org |December 8, 2014 |DAILY BEAST 

His father went to the U.S. Embassy in Nigeria and said his kid was dangerous, and that they should revoke his U.S. travel visa. Laura Poitras on Snowden's Unrevealed Secrets |Marlow Stern |December 1, 2014 |DAILY BEAST 

Australia earlier cancelled Blanc's visa over claims his tactics promote sexual assault. ‘Pick-Up Artist’ to Be Banned from UK |Tom Sykes |November 19, 2014 |DAILY BEAST 

Chopin had been advised by Mr. Beyer to have London instead of Paris put as a visa in his passport. Frederick Chopin as a Man and Musician |Frederick Niecks 

At the hamlet of Abeele there is a Belgian custom-house (visa of "triptyque" or motor-car permit). Ypres and the Battles of Ypres |Unknown 

You've a passport, I suppose; you won't need a visa for France, and from there you can find means to slip over. Loyalties (Fifth Series Plays) |John Galsworthy 

Dane found he could not watch the visa plate now, Rip's hands about their task filled his whole range of sight. Plague Ship |Andre Norton 

Dane bit out a forceful word born of twinges of his own, and then snapped on the visa-plate. Plague Ship |Andre Norton