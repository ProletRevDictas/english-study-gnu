He’d even gone as far as installing protective gates on his own dime. Bee theft is almost a perfect crime—but there’s a new sheriff in town |Andrew Zaleski |February 9, 2021 |Popular-Science 

Add in a little negotiation, and you’ll ensure you never waste a dime. Find the perfect internet speed for you |Whitson Gordon |December 10, 2020 |Popular-Science 

Additionally, if the tax increase did lead to a significant increase in electric vehicle purchases, SANDAG would have done a great deal to reduce carbon emissions without spending a dime. A Sixth Big Move for SANDAG’s New Transportation Vision |Joe Bettles |November 30, 2020 |Voice of San Diego 

Unlike calling the SEC, however, that won’t earn them a dime. Corporate whistleblowers can now collect more reward money |Geoffrey Colvin |November 21, 2020 |Fortune 

The artist never sees a dime from that, relying instead on the value of future releases to pay dividends on the work. Crypto-driven marketplace Zora raises $2M to build a sustainable creator economy |Matthew Panzarino |October 16, 2020 |TechCrunch 

Antoine himself had recently been arrested on a six-year-old warrant for a dime bag of weed. Ground Zero of the NYPD Slowdown |Batya Ungar-Sargon |January 1, 2015 |DAILY BEAST 

In a country where talk is “cheap” and opinions are “a dime a dozen,” we give the facts special privileges and special status. On Torture, Chuck Johnson & Sondheim |James Poulos |December 13, 2014 |DAILY BEAST 

God help us, it all took place on our taxpayer dime, all in the name of defending the Land of the Free and the Home of the Brave. After Torture Report, Our Moral Authority As a Nation Is Gone |Nick Gillespie |December 11, 2014 |DAILY BEAST 

I pressed the dime-sized rubber button on my vest, which was linked to my radio. I Shot Bin Laden |Elliot Ackerman |November 16, 2014 |DAILY BEAST 

He spun three times, stopped on a dime, and flashed the familiar “jazz hands” pose before walking away. Inside a Hospital for the Criminally Insane |Caitlin Dickson |September 15, 2014 |DAILY BEAST 

They ran a dime apiece up into multi-millions without batting their eye-lashes. Hooded Detective, Volume III No. 2, January, 1942 |Various 

An itching red spot about the size of a dime was noticed in thirty-six hours, and it steadily increased in size. Some Constituents of the Poison Ivy Plant: (Rhus Toxicodendron) |William Anderson Syme 

Some day New York will find out that 'the finest police force in the world' is the biggest sham outside the dime museum. Average Jones |Samuel Hopkins Adams 

"Thank you," returned Average Jones, enormously entertained by the dime-novel setting which his host had provided for him. Average Jones |Samuel Hopkins Adams 

"Old Bishop Berkeley would give a nonexistent dime to your nonexistent presence," Anders said gaily. Warm |Robert Sheckley