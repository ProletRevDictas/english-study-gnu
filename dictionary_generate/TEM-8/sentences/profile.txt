He says he worries about the data of citizens who benefit from public services like welfare being used to build political profiles that can be targeted by the government. Podcast: COVID-19 is helping turn Brazil into a surveillance state |Anthony Green |September 16, 2020 |MIT Technology Review 

For example, Drew Ryn, a 23-year-old singer from Nashville who was once on Fox show The X Factor, said she uses TikTok to help boost her profile as a musician. What’s Oracle? TikTok users react to proposed Oracle deal |Danielle Abril |September 15, 2020 |Fortune 

His profile has risen lately after he announced the iPhone 11’s processor and was one of the showmen of Apple’s Mac processor transition. Apple’s leadership evolves ahead of a post-Tim Cook era |radmarya |September 12, 2020 |Fortune 

Create your custom profile, and the platform searches for and connects you with like-minded people. Last call for early-stage founders to exhibit at Disrupt 2020 |Marquise Foster |September 11, 2020 |TechCrunch 

Of those kits already tested, about 36 percent found a DNA profile for someone other than the victim, the DA’s office said. Morning Report: Downtown Jail Outbreak Goes From Bad to Worse |Voice of San Diego |September 11, 2020 |Voice of San Diego 

You get these high-profile people that go into prison, and the staff abuse their authority. How a ‘Real Housewife’ Survives Prison: ‘I Don’t See [Teresa Giudice] Having a Cakewalk Here’ |Michael Howard |January 6, 2015 |DAILY BEAST 

When they get someone high profile, like the governor [Bob McDonnell] or like Teresa, they will abuse their positions. How a ‘Real Housewife’ Survives Prison: ‘I Don’t See [Teresa Giudice] Having a Cakewalk Here’ |Michael Howard |January 6, 2015 |DAILY BEAST 

But while his public profile receded, his private life blossomed. Ed Brooke: The Senate's Civil Rights Pioneer and Prophet of a Post-Racial America |John Avlon |January 4, 2015 |DAILY BEAST 

The exposure and buzz from Short Term have raised her profile considerably. Brie Larson’s Hollywood Transformation |Marlow Stern |December 29, 2014 |DAILY BEAST 

The FBI and the President may claim that the Hermit Kingdom is to blame for the most high-profile network breach in forever. No, North Korea Didn’t Hack Sony |Marc Rogers |December 24, 2014 |DAILY BEAST 

She opened a large black fan and moved it slowly while looking intently at her son's bent profile. Ancestors |Gertrude Atherton 

She was barely five feet five, but she ranked with tall women, her height as unchallenged as the chiselling of her profile. Ancestors |Gertrude Atherton 

He indicated a fair beautiful creature with a determined profile and deep womanly figure. Ancestors |Gertrude Atherton 

New and highly curious characteristics begin to appear when he attempts to give the profile aspect. Children's Ways |James Sully 

With children of finer perception the transition to a correct profile view may be carried much further. Children's Ways |James Sully