Four smart little slices in the frame and foam near the cheekbones and brows—barely perceptible unless you look—let the goggle seat without pressure points. The Best Ski and Snowboard Goggles of 2022 |jversteegh |October 26, 2021 |Outside Online 

If your particular needs and setup benefit more from versatility and the hassle-free nature of wireless than they would for a perhaps-barely-perceptible-to-you increase in sound quality, wireless might be the way you want to go. Level-up your PC audio experience with the best desktop speakers |Tony Ware |August 19, 2021 |Popular-Science 

However, BIPOC working in the media industry recognize that not all companies’ DE&I shortcomings are perceptible from the outside. Media Briefing: How media companies’ DE&I efforts, office return statuses are affecting hiring |Tim Peterson |June 17, 2021 |Digiday 

In a side-by-side taste test comparing NotMilk with cow’s milk, the difference was perceptible. NotMilk says it has achieved a breakthrough: Plant-based milk that mimics dairy |Emily Codik |June 16, 2021 |Washington Post 

There has been a perceptible improvement in my own wellbeing since the state where we live granted us the right to marry. New Report Says Same-Sex Marriage Is Good for Public Health |Russell Saunders |April 15, 2014 |DAILY BEAST 

Sometimes the shift was barely perceptible, but in retrospect we can gauge its profound impact. Music Criticism Has Degenerated Into Lifestyle Reporting |Ted Gioia |March 18, 2014 |DAILY BEAST 

The fact of the handmaking of these pictures, which is such an important part of them, is hardly perceptible in their presence. Ad Reinhardt Hides His Tracks |Blake Gopnik |December 3, 2013 |DAILY BEAST 

There is no sound in American culture sadder than that barely perceptible rattle when a comic talent passes his moment of glory. ‘The Campaign’: Will Ferrell Phones It In |Richard Rushfield |August 12, 2012 |DAILY BEAST 

A 32-page kids-comic is a story told in words and pictures that has a very perceptible structure. New Yorker Covers You Weren’t Meant to See |Jorge Colombo |May 7, 2012 |DAILY BEAST 

The benefits of this change, however, can be but slowly realized, and are for the present hardly perceptible. Glances at Europe |Horace Greeley 

It was, therefore, more in the construction and workmanship then, that the sign manual was perceptible. Antonio Stradivari |Horace William Petherick 

A few words about the interior of Stradivari's instruments; one kind of work is perceptible in all of them. Antonio Stradivari |Horace William Petherick 

This applies also in many other cases—but every master has some distinct difference which is perceptible to the practised eye. Violins and Violin Makers |Joseph Pearce 

We recollect being greatly struck with the ominous calmness perceptible in the tone of this speech. Blackwood's Edinburgh Magazine, No. CCCXXXIX. January, 1844. Vol. LV. |Various