There will be some counterfactual scenario in which the putative ground of morality is absent or points us toward evil rather than the good. The Universe Knows Right from Wrong - Issue 89: The Dark Side |Philip Goff |September 9, 2020 |Nautilus 

The ground of morality is brought closer to us, but we still don’t have an account of how I come to know about its goal-directed nature through moral intuition. The Universe Knows Right from Wrong - Issue 89: The Dark Side |Philip Goff |September 9, 2020 |Nautilus 

She spent the last two years researching computational morality at Mila, the Quebec AI Institute. Why Mathematicians Should Stop Naming Things After Each Other - Issue 89: The Dark Side |Laura Ball |September 2, 2020 |Nautilus 

According to Kant, morality is ultimately about respect and love. In The Time Of The COVID-19 Pandemic, What Should You Say To Someone Who Refuses To Wear A Mask? A Philosopher Weighs In |LGBTQ-Editor |August 30, 2020 |No Straight News 

Many scientists and humanists misunderstand the connection between evolution and morality, grimly determined that evolutionary facts are dangerous because they justify human misbehavior. Just Because It’s Natural Doesn’t Mean It’s Good - Issue 89: The Dark Side |David P. Barash |August 19, 2020 |Nautilus 

In Ferguson, these values are seen as white values, the morality a white morality. Prosecutor Used Grand Jury to Let Darren Wilson Walk |Tom Nolan |November 28, 2014 |DAILY BEAST 

For us, the police embrace nothing if not working- and middle-class values and morality. Prosecutor Used Grand Jury to Let Darren Wilson Walk |Tom Nolan |November 28, 2014 |DAILY BEAST 

In a country with a constitution that values secularism, religion is still the prime indicator of morality and goodness. The Good Wife’s Religion Politics: Voters Have No Faith in Alicia's Atheism |Regina Lizik |November 24, 2014 |DAILY BEAST 

That the fate of marriage as an institution has little to do with morality—and a lot more to do with money. The Real Enemy of Marital Bliss Are Those Most Opposed to Marriage Equality |Jay Michaelson |October 25, 2014 |DAILY BEAST 

All of this comes just as the Iranian parliament has passed a law that gives further powers to morality patrols. Acid Attacks on Women Spread Terror in Iran |IranWire |October 18, 2014 |DAILY BEAST 

The ruse by which he and Lannes captured the bridge below Vienna was discreditable no doubt from the point of view of morality. Napoleon's Marshals |R. P. Dunn-Pattison 

A writer of the day said, “Mr. Hudson is neither better nor worse than the morality of his time.” Fifty Years of Railway Life in England, Scotland and Ireland |Joseph Tatlow 

Social betterment must depend at every stage on the force of public spirit and public morality that inspires it. The Unsolved Riddle of Social Justice |Stephen Leacock 

It may be admitted at once that the direct influence on morality was very small indeed. The Religion of Ancient Rome |Cyril Bailey 

If we knew more of it, we should see more clearly where religion and morality joined hands, but we know enough to give us a clue. The Religion of Ancient Rome |Cyril Bailey