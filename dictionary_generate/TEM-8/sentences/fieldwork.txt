Moreover, the biologists say, its protection from development has provided a rare chance to do fieldwork nine miles from downtown Washington. Biologists say a wider American Legion Bridge would destroy critical research site |Katherine Shaver |December 11, 2020 |Washington Post 

That potential for personal connections helps explain hitchhiking’s appeal, agreed Laviolette, whose own hitchhiking experiences became part of his fieldwork. Thumbs up for three new books that capture hitchhiking’s adventurous spirit |Jen Rose Smith |October 30, 2020 |Washington Post 

The guidelines still recommend replacing fieldwork with distanced alternatives whenever possible. Why bat scientists are socially distancing from their subjects |Jerimiah Oetting |October 23, 2020 |Science News 

The following summer she did fieldwork in fishing and agricultural villages in Bahia, Brazil. Bringing the margin to the center |Katie McLean |October 20, 2020 |MIT Technology Review 

I’ve observed that directly in my fieldwork, and have heard the same from long-term militia members who say it dates back to the beginnings of the movement in the early 1990s. Lessons From Embedding With The Michigan Militia – 5 Questions Answered About The Group Allegedly Plotting To Kidnap A Governor |LGBTQ-Editor |October 10, 2020 |No Straight News 

Limitations aside, the survey is the first attempt at collecting data on sexual harassment and assault in fieldwork. Two-Thirds of These Female Scientists Say They’ve Been Sexually Harassed |Brandy Zadrozny |July 16, 2014 |DAILY BEAST 

By fieldwork you mean attached to your field stations, of which I believe there are 65 in the United States? Warren Commission (5 of 26): Hearings Vol. V (of 15) |The President's Commission on the Assassination of President Kennedy 

Strong and healthy, neither wind nor rain interfered with her fieldwork in botany or paleontology. Woman in Science |John Augustine Zahm 

After a series has been taken at each station, the fieldwork is complete. The Boy Mechanic, Book 2 |Various 

Once the triangle has been laid out, the fieldwork is very simple. The Boy Mechanic, Book 2 |Various 

He now devoted himself to palaeontological studies, and to fieldwork in various parts of Germany, Italy and France. Encyclopaedia Britannica, 11th Edition, Volume 4, Part 3 |Various