The three people who recounted the president’s unsettled response spoke on the condition of anonymity to discuss private conversations. Trump and his campaign try to allay concerns about trailing Biden in television ads |Michael Scherer, Josh Dawsey |September 17, 2020 |Washington Post 

I also avoid the unsettled tummy I sometimes get from other post-workout shakes and meals. This Delicious Japanese Breakfast Costs Under $1 |Wes Siler |September 2, 2020 |Outside Online 

So after Lagarias and Shor, the only unsettled dimensions were seven, eight and nine. Computer Search Settles 90-Year-Old Math Problem |Kevin Hartnett |August 19, 2020 |Quanta Magazine 

That growing body of research can help explain the unrest people are now experiencing as beloved rituals go virtual or get punted to some unsettled future. Why do we miss the rituals put on hold by the COVID-19 pandemic? |Sujata Gupta |August 14, 2020 |Science News 

When India gained its independence in 1947, it inherited unsettled frontiers with several neighbours. China And India’s Deadly Himalayan Clash Is A Big Test For Modi. And A Big Concern For The World |LGBTQ-Editor |June 19, 2020 |No Straight News 

If these issues raise uncomfortable questions for Democrats, Clinton has reasons not to be too unsettled. Want President Hillary? Then Primary Her |Jeff Greenfield |November 24, 2014 |DAILY BEAST 

They were the women voters who turned to the GOP in the past when things got unsettled internationally. Dems Desperately Seeking the Gender Gap |Eleanor Clift |October 28, 2014 |DAILY BEAST 

“Things have been pretty unsettled,” said third-year student Joe Pittman. Person of Interest Identified in Disappearance of UVA Student Hannah Graham |Julia Horowitz |September 19, 2014 |DAILY BEAST 

The details of exactly what Obama would do and how many people stood to get legal status were unsettled. Why Obama Won't Act on Immigration |Ruben Navarrette Jr. |September 2, 2014 |DAILY BEAST 

Unsettled by the reality that the cops can't help them, Oakland residents are hiring private patrols. The Daily Beast’s Best Longreads, June 14, 2014 |The Daily Beast |June 14, 2014 |DAILY BEAST 

The ensuing day the weather was still squally and unsettled. Narrative of a Survey of the Intertropical and Western Coasts of Australia] [Volume 2 of 2] |Phillip Parker King 

There is no doubt the Government land unsettled and untouched in this province amounts to 90 per cent. The Philippine Islands |John Foreman 

The comtesse vanished at the end of six months, leaving a board bill unsettled. Repertory Of The Comedie Humaine, Complete, A -- Z |Anatole Cerfberr and Jules Franois Christophe 

These and other unsettled points the following inquiry attempts to make clear. A Statistical Inquiry Into the Nature and Treatment of Epilepsy |Alexander Hughes Bennett 

Last year the great question of the Spanish succession was unsettled, and there was serious danger of a general war. The History of England from the Accession of James II. |Thomas Babington Macaulay