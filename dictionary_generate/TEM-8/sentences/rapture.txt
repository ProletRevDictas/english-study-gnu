By the time the four-and-a-half-minute program was over, Brown was skating at a sprint and the arena was in raptures. Quads Are Nonnegotiable In Men’s Figure Skating. But Fans Of Jason Brown Don’t Care. |Dvora Meyers |February 7, 2022 |FiveThirtyEight 

It’s hard to go online these days without seeing someone yelling about the metaverse, whether in rapture or derision. Why TIME Is Launching a New Newsletter on the Metaverse |Andrew R. Chow |November 18, 2021 |Time 

It’s also a beginning, an open door to rapture, to finding your place in the world—and, sometimes, to creating new work that builds on the old. Three New Music Documentaries Celebrate the Cosmic Connection Between Artist and Audience |Stephanie Zacharek |July 2, 2021 |Time 

McDonald’s commercials were among our earliest images of love, humanity, sexuality, and even rapture. The McDonald’s Commercials That Live in Our Minds, Rent Free |MM Carrigan |December 18, 2020 |Eater 

However, a new set of neuroscience research findings suggests that losing track of time is also intimately bound up with creativity, beauty, and rapture. The Neurology of Flow States - Issue 91: The Amazing Brain |Heather Berlin |October 14, 2020 |Nautilus 

A second coming of Rapture-minded evangelicalism is always one catastrophe, book, revival, or Nicolas Cage movie away. The Rapture: The Theological Idea That Inspired ‘The Leftovers’ |Matthew Paul Turner |July 6, 2014 |DAILY BEAST 

The book was optioned to HBO in 2011, around the time evangelist Harold Camping claimed The Rapture would occur—on May 21, 2011. From ‘Lost’ to The Rapture: Creators Damon Lindelof and Tom Perrotta on HBO’s ‘The Leftovers’ |Marlow Stern |June 24, 2014 |DAILY BEAST 

We can feel his sad cadences and the rapture of language in the Gettysburg Address. Lincoln in Love |Jerome Charyn |February 14, 2014 |DAILY BEAST 

There is a phrase for the foreigners' rapture: mal d'afrique. Susan Minot on Africa, Joseph Kony, and the Limits of Writing About Love |Lea Carpenter |February 10, 2014 |DAILY BEAST 

Most recently, Harold Camping went bust predicting that the Rapture would take place on May 21, 2011. Sorry, Evangelicals, Syria Will Not Spur the Second Coming |Candida Moss |September 5, 2013 |DAILY BEAST 

Since that memorable night of mingled joy and despair, I thought not that such rapture awaited me again on earth. Blackwood's Edinburgh Magazine, Volume 60, No. 372, October 1846 |Various 

However, all seemed to do very well, and no one ever came into her room without some degree of rapture about Mr. Ernescliffe. The Daisy Chain |Charlotte Yonge 

Her face wore an expression of mystic rapture like that characterizing the features of some Chinese Buddhas. Dope |Sax Rohmer 

I longed to hear her and to see her always; I would have died in rapture at her side, but I was never fain to wed her. Marguerite |Anatole France 

It was the first time she had ever given him more than her hand to kiss, and the rapture repaid him for all. The Atlantic Monthly, Volume 17, No. 101, March, 1866 |Various