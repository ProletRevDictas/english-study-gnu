Miss Manners understands your mother-in-law’s not wanting to put friends and family to work, although she might have found a more tactful way to express it to your wife. Miss Manners: I feel robbed of my potluck wedding |Judith Martin, Nicholas Martin, Jacobina Martin |January 14, 2021 |Washington Post 

The response from the national Kappa KappaGamma organization took a very similar, albeit more tactful, approach. How Kappa Kappa Gamma Threw A UConn Sorority Sister Under The Bus |Emily Shire |May 15, 2014 |DAILY BEAST 

Various media outfits have already praised Beyoncé and Jay Z's tactful approach to their Anne Frank House experience. Should Beyoncé Have Instagrammed From the Anne Frank House? |Amy Zimmerman |March 21, 2014 |DAILY BEAST 

The tactful Hamad manages to patch up relations, and the two are photographed sitting together on a sofa later that day. Muammar Gaddafi's 25 Strangest Moments |David A. Graham |February 23, 2011 |DAILY BEAST 

Similarly, in pop culture, the man who was originally deemed inadequate is lifted to her level through tactful speech. Sex Advice from Cyrano |Ben Crair |July 23, 2009 |DAILY BEAST 

I think that is going to change, and the most vivid, tactful demonstration of change is the election of Obama. Carter's One Regret |Reza Aslan |January 29, 2009 |DAILY BEAST 

But she was a very tactful person and was about to drop the subject, when Isabel slowly turned her eyes. Ancestors |Gertrude Atherton 

He had tried to soften his reply, but not being politic or tactful had succeeded only in expressing himself more brusquely. Dorothy at Skyrie |Evelyn Raymond 

This, or else a tactful absenteeism, became their custom whenever licencing matters came up to be discussed. Mushroom Town |Oliver Onions 

He felt he had not been tactful, but he was very tired, and if he ventured an explanation might make things worse. The Girl From Keller's |Harold Bindloss 

No one knew how to flatter in so tactful a manner, particularly in portraits of ladies. The History of Modern Painting, Volume 1 (of 4) |Richard Muther