The channels are deeper and more complex than previously thought, and may be funneling warm ocean water all the way to the underside of the glacier, melting it from below, the researchers found. New maps show how warm water may reach Thwaites Glacier’s icy underbelly |Carolyn Gramling |September 9, 2020 |Science News 

From January to March 2019 researchers used a variety of airborne and ship-based methods — including radar, sonar and gravity measurements — to examine the seafloor around the glacier and two neighboring ice shelves. New maps show how warm water may reach Thwaites Glacier’s icy underbelly |Carolyn Gramling |September 9, 2020 |Science News 

They do, however, welcome visitors to their wild Arctic frontier, marked by 11,000-foot peaks, polar bears, and Jakobshavn, the planet’s fastest-moving glacier. 27 Epic Trips to Start Planning Now |The Editors |September 8, 2020 |Outside Online 

These are the shadows of mountains on Pluto—craggy peaks that are composed of deep-frozen water ice and capped with nitrogen glaciers. These Images Expose the Dark Side of the Solar System - Issue 89: The Dark Side |Corey S. Powell |August 26, 2020 |Nautilus 

Joe Levy, a geologist at Colgate University who wasn’t involved with the study, thinks the glacier research is thought provoking, but does point out it “struggles to pin down a single process that is responsible for forming each valley.” Mars may not have been the warm, wet planet we thought it was |Neel Patel |August 7, 2020 |MIT Technology Review 

After 50 years, members of the Huna Tlingit people can finally collect harvest sea gull eggs again in Glacier National Park. Nazis, Sunscreen, and Sea Gull Eggs: Congress in 2014 Was Hella Productive |Ben Jacobs |December 29, 2014 |DAILY BEAST 

Peacock served as an expert witness on grizzlies in federal court for Glacier National Park. What It Takes to Kill a Grizzly Bear |Doug Peacock |November 23, 2014 |DAILY BEAST 

“The first time I saw Glacier National Park, it was the magical fantasy land I had always been dreaming about,” says Chin. Adventure Photographer Jimmy Chin: Defying the Rational, Physically and Creatively |Oliver Jones |October 6, 2014 |DAILY BEAST 

The highest density of wolverines left south of Canada is in Glacier National Park. Who Will Save the Wolverine? Not the U.S. Fish and Wildlife Service |Doug Peacock |July 20, 2014 |DAILY BEAST 

A lieutenant commander in the Navy, he was knock-out handsome with a smile that would melt a glacier. Can Walking on the Moon Be Better Than Sex in Space? |Lily Koppel |July 6, 2013 |DAILY BEAST 

The effect of the intense ice action above noted is rapidly to wear away the rocks of the valley in which the glacier is situated. Outlines of the Earth's History |Nathaniel Southgate Shaler 

A trace of this colour is often visible even in the surface snow on the glacier, and sometimes also in our ordinary winter fields. Outlines of the Earth's History |Nathaniel Southgate Shaler 

Model a river valley whose upper part is filled with a glacier. The Later Cave-Men |Katharine Elizabeth Dopp 

He will foller ye right up the side of a glacier, but he ain't mentally constructed to take the lead. David Lannarck, Midget |George S. Harney 

The mighty glacier seemed like a great river frozen into ice, hemmed in by the steep rocks. Rudy and Babette |Hans Christian Andersen