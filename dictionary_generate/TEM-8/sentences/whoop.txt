I want to laugh, even let out a whoop, but instead I just keep flying. Chronic Illness Had Me Stuck in Grief. My E-Bike Helped Me Find Joy Again. |mmirhashem |October 11, 2021 |Outside Online 

This is food that prompts little whoops of pleasure and encourages discussion. Tosca returns to business downtown with a fresh look and new chef |Tom Sietsema |June 4, 2021 |Washington Post 

Their vocals ranged from yodels to yips, whoops to whispers. What Makes Music Universal - Issue 99: Universality |Kevin Berger |April 29, 2021 |Nautilus 

The only remaining question is where Sony found such a giant can of whoop-ass. PS 4 Beats Xbox One For Best New Video Game Console |Winston Ross |June 13, 2013 |DAILY BEAST 

That was some whoop-de-doo speech Barack Obama gave Tuesday in Florida. Michael Tomasky: Economic Fairness Isn’t Enough for Obama’s Game Plan |Michael Tomasky |April 12, 2012 |DAILY BEAST 

Finally I located and extracted the missing bauble, and with a triumphant whoop Elizabeth led me back into the waiting room. Where I Found Elizabeth Taylor's Diamond Earring |Sandra McElwaine |March 24, 2011 |DAILY BEAST 

Two-seventy happened and with it the apprehensive city at last let out a collective whoop of relief and jubilation. The Capital Goes Nuts |Simon Schama |November 6, 2008 |DAILY BEAST 

In a spontaneous act of call and response, we all whoop and start clapping back, united by euphoria. Subway Euphoria |Jessi Klein |November 5, 2008 |DAILY BEAST 

Giving a terrific whoop, he raised his gun and fired, the ball just missing Lawrence's head. The Courier of the Ozarks |Byron A. Dunn 

Well, I'll make the salve an' do the talkin'; Giz'll sort o' whoop things up a bit and Lut'er'll git cured. Fifty Contemporary One-Act Plays |Various 

Well, I seemed to be in the open river again by and by, but I couldn't hear no sign of a whoop nowheres. Adventures of Huckleberry Finn, Complete |Mark Twain (Samuel Clemens) 

Away down there somewheres I hears a small whoop, and up comes my spirits. Adventures of Huckleberry Finn, Complete |Mark Twain (Samuel Clemens) 

He stared for a moment wildly at the occupants of the Warren easy chairs, and the next moment let out a whoop of delight. The Rival Campers Afloat |Ruel Perley Smith