It’s a document of Gullah Geechee culinary history, as well as the story of a self-described “prodigal son” returning to the land that raised him. No-Recipe Recipes Aren’t a Fad; They’re as Old as Cooking Instruction Itself |Marian Bull |July 14, 2021 |Eater 

Abercrombie weaves the tale of Prince Yarvi in a tale part Captains Courageous, part Revenge of the Nerds, and part Prodigal Son. A Fantasy Titan Invades the YA Kingdom |William O’Connor |July 18, 2014 |DAILY BEAST 

No one knows, but on the 4th of July he began bellowing that the Prodigal Son would, in fact, return. LeBron James Returns to Cleveland: How 'The Decision 2.0' Happened |Robert Silverman |July 11, 2014 |DAILY BEAST 

Her "prodigal son" brother, Mehran (Reza Sixo Safari), a former classical musician, returns home from a stint in drug rehab. Iran’s Controversial New Lesbian Film |David Ansen |August 25, 2011 |DAILY BEAST 

Turns out, Nash's "prodigal roommate" Charles isn't real, but rather a personification of Nash's loss of youthful exuberance. Imaginary Friends in Movies |Marlow Stern |May 5, 2011 |DAILY BEAST 

In going to the Cleveland Cavaliers, he was the prodigal son playing in his homeland. King James Can't Handle the Pressure |Buzz Bissinger |March 11, 2011 |DAILY BEAST 

I doubt if the State itself has ever known the meaning of hospitality since the old ranch days, when, of course, it was prodigal. Ancestors |Gertrude Atherton 

Here is Christianity with its marvellous parable of the Prodigal Son to teach us indulgence and pardon. Camille (La Dame aux Camilias) |Alexandre Dumas, fils 

Sterile, dissipated and prodigal, she made her husband very unhappy, thus avenging the first Mme. Brunner. Repertory Of The Comedie Humaine, Complete, A -- Z |Anatole Cerfberr and Jules Franois Christophe 

The rooks were awake in Randolph Crescent; but the windows looked down, discreetly blinded, on the return of the prodigal. Tales and Fantasies |Robert Louis Stevenson 

In a pew on the left-hand side a little old man was holding forth as to the “prodigal son.” Our Churches and Chapels |Atticus