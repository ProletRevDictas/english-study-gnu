We can witness as great a shift in presidents as we have seen since Herbert Hoover passed the baton to Franklin Delano Roosevelt. Forget half-baked punditry. Watch a historic shift. |Jennifer Rubin |November 30, 2020 |Washington Post 

Her letter also referenced Anderson Arboleda, a 24-year-old Afro-Colombian man who died in May after a Colombian police officer allegedly struck him on the head with a baton. Reggaeton needed a racial reckoning. Afro-Latinos are leading it. |Bethonie Butler |November 23, 2020 |Washington Post 

We first saw that kind of transition back in 2009, when Xerox CEO Anne Mulcahy passed the baton to Ursula Burns. PG&E’s new CEO is the first woman to leap from top of one Fortune 500 company to another |kristenlbellstrom |November 19, 2020 |Fortune 

“It was the eight-year mark and the department is in a good position to pass the baton to the next generation,” Guglielmi said. Fairfax County police chief to step down in February |Justin Jouvenal |November 5, 2020 |Washington Post 

The baton then got passed again to another group, which used hundreds of thousands of medical billing data from people who either tested positive or were presumed positive for Covid-19, to verify those viral protein candidates. Can We Wipe Out All Coronaviruses for Good? Here’s What a Group of 200 Scientists Think |Shelly Fan |October 27, 2020 |Singularity Hub 

The Obama administration took up the baton in 2009 and has since become the most evidence-based administration in history. Can the U.S. Government Go Moneyball? |Peter Orszag, Jim Nussle |December 23, 2014 |DAILY BEAST 

But the most recent poll of the race, conducted for the Baton Rouge Fox affiliate, has Landrieu ahead of Cassidy 36 to 32 percent. How This Election Could Go to January |Michael Tomasky |October 24, 2014 |DAILY BEAST 

The trooper reached with her right hand for her expandable baton. The Muslim Convert Behind America’s First Workplace Beheading |Michael Daly |September 27, 2014 |DAILY BEAST 

You see, as far as passing the baton down, Michael used to look at Fred Astaire, Gene Kelly, and James Brown. Quincy Jones Talks Chicago’s Mean Streets, Why Kanye West Is No Michael Jackson, and Bieber |Marlow Stern |September 25, 2014 |DAILY BEAST 

I glimpse an alarming, finger-length aluminum baton in her bag. Best Career Arc Ever: From Burlesque To Bartending |Anne Berry |September 13, 2014 |DAILY BEAST 

On account of his bravery Fleetfoot was given a baton which showed that he might lead the men. The Later Cave-Men |Katharine Elizabeth Dopp 

But here the Greek, whose face had crimsoned, snatched a tiny baton beside a bronze gong. God Wills It! |William Stearns Davis 

The conductor is energetic and efficient, wields his baton in a lively manner, but hits nobody with it. Our Churches and Chapels |Atticus 

This proof of confidence—the object of much secret envy—is, to women, a field-marshal's baton. The Petty Troubles of Married Life, Complete |Honore de Balzac 

In his hand he carried a short staff, or baton, with gold knobs, and he wore a thin golden circlet in his hair. In the Wrong Paradise |Andrew Lang