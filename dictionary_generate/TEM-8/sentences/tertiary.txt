A resolution that leaves things better than they were before is, at best, a tertiary concern. YouTuber Valkyrae’s skincare fiasco proves online drama doesn’t produce accountability |Nathan Grayson |October 28, 2021 |Washington Post 

They also fold into semi-rigid structures before turning into more complicated tertiary models. Deep Learning Is Tackling Another Core Biology Mystery: RNA Structure |Shelly Fan |August 31, 2021 |Singularity Hub 

The study confirmed one death and at least 649 cases connected to the rally, including secondary and tertiary spread. Sturgis motorcycle rally linked to more than 100 coronavirus infections amid delta variant’s spread |Brittany Shammas, Hannah Knowles, Dan Keating |August 26, 2021 |Washington Post 

Antetokounmpo’s scoring was spectacular, but his tertiary skills alone place him alongside the greats. Giannis Antetokounmpo Has Gone Where Few Players Have Before |Louis Zatzman |July 22, 2021 |FiveThirtyEight 

The various three-dimensional, tertiary structures proteins reliably form are largely responsible for adaptive functions, like giving off color when exposed to certain wavelengths of light. Playing Go with Darwin - Issue 94: Evolving |David Krakauer |December 16, 2020 |Nautilus 

But forget about the tertiary side characters, the main players are also saddled with catastrophically absurd stories as well. NBC’s ‘Smash’: Weak Writing, Terrible Characters, and Painful Subplots |Jace Lacob |March 26, 2012 |DAILY BEAST 

My only consolation is the knowledge that the speaker is entirely secondary (or tertiary) to the proceedings. My Commencement Address |Christopher Buckley |May 17, 2009 |DAILY BEAST 

The tertiary stage commonly begins from three to four years after the primary infection. Essays In Pastoral Medicine |Austin Malley 

The lesions of the tertiary stage may cause great destruction of tissues and very grave consequences. Essays In Pastoral Medicine |Austin Malley 

The bones of the face are frequently attacked in the tertiary stage and they rot away. Essays In Pastoral Medicine |Austin Malley 

Many physicians hold that in the tertiary stage the disease is not transmissible, but that statement is not true. Essays In Pastoral Medicine |Austin Malley 

Tertiary Colours are three only, citrine, russet, and olive. Field's Chromatography |George Field