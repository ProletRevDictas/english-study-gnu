You would see this young, beautiful man and an orchestra, then you would see a jazz trio and a harp. Revisiting ‘The Visitors’: An oral history of Ragnar Kjartansson’s multimedia masterpiece |Sebastian Smee, Gabriel Florit, Joanne Lee |July 23, 2021 |Washington Post 

Lento The harp is having a moment right now, and I am very much here for it. Beyond Vivaldi, a classical playlist to bring on the spring |Michael Andor Brodeur |April 2, 2021 |Washington Post 

For other captions, such as “a snail made of harp,” the results are less good, with images that combine snails and harps in odd ways. This avocado armchair could be the future of AI |Will Heaven |January 5, 2021 |MIT Technology Review 

You’re going to want to hear his harp take on Notorious BIG’s “Big Poppa,” and read how the 26-year-old launched a classical response to George Floyd’s death. A New Vision for America |Daniel Malloy |November 29, 2020 |Ozy 

Others described it as sounding like a broken harp or lyre string. Imagine traveling to the moon only to realize you’re allergic to it. One astronaut did. |PopSci Staff |November 25, 2020 |Popular-Science 

They often harp on the indiscretion, such as an affair, and not the larger picture. Why Do Voters Stick With Hypocrites Like Scott DesJarlais? |Keli Goff |August 18, 2014 |DAILY BEAST 

“My job was to assess their fear and then harp on that fear, capitalize on that fear and get them to buy,” said Maddox, 33. ‘Degree Mills’ Are Exploiting Veterans and Making Millions Off the GI Bill |Aaron Glantz |June 28, 2014 |DAILY BEAST 

I've been harping on this theme for nearly a decade now, and now it's time to harp again. America's European Allies Drop the Ball |David Frum |May 2, 2013 |DAILY BEAST 

Later he started playing classical guitar and then the harp. Shooting the Stars With Fashion Photographers Markus and Indrani |Abigail Pesta |November 25, 2012 |DAILY BEAST 

Through March 2012, HARP was only permitted to refinance mortgages that were up to 125 percent of the value of a home. New Data Shows HARP Mortgage Refinance Program Is Finally Working |Matthew Zeitlin |October 4, 2012 |DAILY BEAST 

The mirth of timbrels hath ceased, the noise of them that rejoice is ended, the melody of the harp is silent. The Bible, Douay-Rheims Version |Various 

Wherefore my bowels shall sound like a harp for Moab, and my inward parts for the brick wall. The Bible, Douay-Rheims Version |Various 

Perhaps in the full fruition of your genius your music, like the warm western wind to the harp, may bring life to her soul. The Fifth String |John Philip Sousa 

There's Raphael singing, Gabriel accompanying him on the harp, and all the angels flapping their wings to express their joy. Friend Mac Donald |Max O'Rell 

The world was strung with them like a harp, and upon them the wind played a monotonous refrain. The Dragon Painter |Mary McNeil Fenollosa