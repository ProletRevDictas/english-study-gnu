Meny suggests the compact, dense variety named Rubicon, which has the bonus of turning an attractive plum-purple in cold winters. Winter wonders: Conifers revive the sleeping garden. But remember, less is more. |Adrian Higgins |December 2, 2020 |Washington Post 

In 1939, an “early New England Thanksgiving menu” included oyster soup, venison, cornbread and plum pudding with brandy sauce. 143 years of Thanksgiving coverage in The Post |Becky Krystal |November 9, 2020 |Washington Post 

North America’s various beach plums bear purple-blue, cherry-sized fruits that make for a beloved New England jelly. How passion, luck and sweat saved some of North America’s rarest plants |Susan Milius |November 5, 2020 |Science News 

The Sense also supports Fitbit Pay, which connects to your credit and debit cards and supports contactless payments—a plum feature to have during a pandemic for those on the go. Gift Guide: What you need to work(out) from home |Rachel King |November 2, 2020 |Fortune 

This unique style suits coach Bill Laimbeer — whose teams have often ranked near the bottom of the league in 3-point shooting — and a roster missing Plum’s pick-and-roll wizardry and Cambage’s dominant interior scoring. The Aces Don’t Need Threes To Win |Mike Prada |September 18, 2020 |FiveThirtyEight 

It was popularized as a holiday dessert in 16th-century England and also is known as Christmas pudding or plum pudding. The Most Confusing Christmas Music Lyrics Explained (VIDEO) |Kevin Fallon |December 24, 2014 |DAILY BEAST 

At least those parents whose kids landed the plum roles will be. Was Baby Jesus A Holy Terror? |Candida Moss |December 21, 2014 |DAILY BEAST 

Central won thirty-three consecutive games, and Suffridge became a plum for the college recruiters. Football Great Bob Suffridge Wanders Through the End Zone of Life |Paul Hemphill |September 6, 2014 |DAILY BEAST 

Now Yesh Atid will demand and receive additional plum ministries. The Knesset's New Faces |Don Futterman |January 23, 2013 |DAILY BEAST 

After realizing how difficult this plum post would be, “Biden” writes that he “wised up and settled on my current fallback plan.” Speed Read: 11 Best Bits from Joe Biden Satire ‘The President of Vice’ |Caroline Linton |January 19, 2013 |DAILY BEAST 

Spain is at war with North America, and now offers us this sugar-plum to draw us to her side to defend her against invasion. The Philippine Islands |John Foreman 

The system would be perfect for the mellowing of port or madeira, but when it is applied to plum and apple jam or, when 18 pr. Gallipoli Diary, Volume I |Ian Hamilton 

If the cake rises well in the oven, it is commonly said that it is "nice and plum;" and vice vers, that it is heavy. Notes and Queries, Number 194, July 16, 1853 |Various 

Cheese is now eaten with apple puddings and pies; but is there any nook in England where they still grate it over plum pudding? Notes and Queries, Number 178, March 26, 1853 |Various 

Kano pensively lifted a plum upon the point of a toothpick and began nibbling at its wrinkled skin. The Dragon Painter |Mary McNeil Fenollosa