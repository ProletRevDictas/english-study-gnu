When the shortage happened, we were looking into milk donors, but it just wasn’t something we were 100% onboard with because our babies’ tummies are super sensitive. 5 Parents on the Stress of Trying to Feed Their Babies Amid a Nationwide Formula Shortage |Eliana Dockterman |May 13, 2022 |Time 

Tell your parents you’ve got a tummy ache, and they’ll write you a note. Taking a mental health day is legit. Just ask the kids. |Petula Dvorak |June 3, 2021 |Washington Post 

Sometimes restaurant veggies are cooked with heavy sauces and spices that could upset a baby’s tummy. Hints From Heloise: Keep out cold air with a ‘snake in the draft’ |Heloise Heloise |January 7, 2021 |Washington Post 

Maybe it just tastes good, or in some cases, it helps them vomit if their tummy is upset. Hints From Heloise: Give your full attention on a first date |Heloise Heloise |November 9, 2020 |Washington Post 

The goal came off his tummy inside the Portuguese box, an apt way to score for a very gutsy player. Team USA 2, Portugal 2: Seconds Away From World Cup Glory |Tunku Varadarajan |June 23, 2014 |DAILY BEAST 

While slips can hide VPL and your backside on a windy day, Spanx can help hide a lot more, like cellulite or a protruding tummy. Kate Middleton’s “Bottomgate” Shows Why Women Still Need Slips |Keli Goff |May 28, 2014 |DAILY BEAST 

It was scheduled for last year but the Queen had to cancel with an upset tummy which saw her hospitalized. Pope's Gift To Prince George |Tom Sykes |April 3, 2014 |DAILY BEAST 

“I made a promise to my children when they were in my tummy that there is no way I could ever, ever, ever go back,” she had said. Jenna Jameson Breaks Her Vow |Aurora Snow |November 18, 2013 |DAILY BEAST 

I pooed on my skirt at work today Today was an 'upset tummy' day. Penis Beakers and Constipated Dolls: What Mothers REALLY Want To Know |Tom Sykes |October 11, 2013 |DAILY BEAST 

Happy's liable to go to bed with an empty tummy, if yuh don't ride out and warn him to approach easy. The Happy Family |Bertha Muzzy Bower 

I came in plastered from head to foot with lying in the rain on my tummy and peering over the top of a trench. Carry On |Coningsby Dawson 

I had to lie on my tummy in the mud, my nose just showing above the parapet, for the best part of twenty-four hours. Carry On |Coningsby Dawson 

"Come f-f-f-f-from the e-e-e-earth getting a t-t-t-t-tummy ache," sagely announced Ding-dong Bell. The Motor Rangers Through the Sierras |Marvin West 

You wont have any room in your tummy for your picnic, and Huldah has packed an awful big one. Letty and the Twins |Helen Sherman Griffith