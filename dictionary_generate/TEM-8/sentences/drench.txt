Even by day the Maruts create darkness with the water-bearing cloud, when they drench the earth. Sacred Books of the East |Various 

Drench is the causative of drink: here the nominative of the verb is ‘Iris’ and the object ‘beds.’ Milton's Comus |John Milton 

Sometimes she was so weary that she sank down by the roadside and let130 the night-dew drench her aching limbs. Stories of Old Greece and Rome |Emilie Kip Baker 

Epsom salt, in one ounce doses, given either as a gruel or a drench, will be found to answer the purpose well. A Treatise on Sheep: |Ambrose Blacklock 

You will drench yourself in the blood of the innocent, only that you may do it—while no effect shall follow.' Aurelian |William Ware