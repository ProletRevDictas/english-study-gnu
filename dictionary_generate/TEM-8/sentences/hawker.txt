Hi Quartz Africa members,Hi Quartz Africa members,Informal shops, open market stalls, and hawkers are examples of the informal operations that dominate the retail market in Africa. ✦ The informal retailers driving Africa’s economy |Martin Siele |July 13, 2022 |Quartz 

These stories are told by a diverse cast of chefs, home cooks, street hawkers, and restaurateurs, all people who make us excited to travel, cooking the foods that make us excited to eat. Where to Eat in 2022 |Eater Staff |January 18, 2022 |Eater 

There are fishmongers and queso fresco vendors and a newspaper hawker and kids playing hide-and-seek and other games, and, in the center of the commotion, a cow that’s parked itself near the church door. His collection of miniatures from around the world fills 16 rooms. And he’s not done yet. |Eddie Dean |October 18, 2021 |Washington Post 

The coastal city was unfamiliar, from the language, to the dusty streets packed with motorcycles, horse carts, and hawkers, but he immediately felt at home. African Americans view Senegal as an ancestral homeland—and business opportunity |Nellie Peyton |June 19, 2021 |Quartz 

Shuffling behind tour guides, gazing upward at the architecture or pausing abruptly to buy souvenirs from street hawkers, the visitors were often a nuisance to locals navigating the streets. Can Barcelona Fix Its Love-Hate Relationship With Tourists After the Pandemic? |Ciara Nugent |June 9, 2021 |Time 

A particularly vivid instance comes from an interview conducted by the journalist Paul Hawker in 2010. Knocking on Heaven's Door: True Stories of Unexplained, Uncanny Experiences at the Hour of Death |Patricia Pearson |August 11, 2014 |DAILY BEAST 

Could it have taken her any longer to break up with that hawker of newspaper scandal, Carlisle? Family Feud: Which ‘Downton Abbey’ Sister Is the Best? |Caitlin Dickson, Kevin Fallon, Abby Haglage |January 9, 2013 |DAILY BEAST 

In fact, the private jet—a Gulfstream, a Hawker-Beechcraft—occupies a central role in American corporate culture. ‘Abercrombie’ Lawsuit: Why CEOs Love Their Jets |Daniel Gross |October 19, 2012 |DAILY BEAST 

The flagon which contained the giants allowance of liquor is mentioned by Hawker in a private letter. Footprints of Former Men in Far Cornwall |Robert S. Hawker 

The grave-digger said he had been christened and married by Mr. Hawker: one of the best passons, he added, us ever had. Footprints of Former Men in Far Cornwall |Robert S. Hawker 

Hawker and Landor (it may be remarked by the way) were in many respects kindred spirits. Footprints of Former Men in Far Cornwall |Robert S. Hawker 

Possibly Hawker altered this in accordance with the superstition mentioned on p. 21. Footprints of Former Men in Far Cornwall |Robert S. Hawker 

This screen was constructed by Mr. Hawker from the rescued remnants of an older screen, pieced together with ironwork. Footprints of Former Men in Far Cornwall |Robert S. Hawker