As 1996’s Scream so expertly satirized, the slasher is a rich genre with established conventions and so-called “rules,” making it perfect fodder for an interactive story with branching paths. 5 scary games to play this Halloween |Will Fulton |October 15, 2021 |Popular-Science 

Much of the believability is due to Kleeman’s sharp eye for the weirdness of consumerism, and especially her ability to satirize a certain type of fancy person with laser precision and humor. ‘Something New Under the Sun’ Is a Climate-Change Mystery Set in Hollywood |smurguia |October 9, 2021 |Outside Online 

You don’t have to know what’s being satirized to love the satire. S.J. Perelman was a master of comedy. Nearly a century later, his work still delivers laughs. |Donald Liebenson |August 25, 2021 |Washington Post 

Lopez and Affleck attempted to satirize the flood of attention in November 2002, when Lopez released the video for “Jenny From the Block.” The Jennifer Lopez-Ben Affleck recoupling is the greatest gossip story we’ve had in years |Constance Grady |August 24, 2021 |Vox 

It will shift tones, sometimes satirizing video game tropes, and sometimes drawing on them for inspiration. ‘Nier Replicant’ is an experimental, heartbreaking pop album of a video game |Gene Park |April 22, 2021 |Washington Post 

At least they satirize entitlement instead of unwittingly enacting (and celebrating) it. Zach Braff’s Irritating Sense of Entitlement |Andrew Romano |July 18, 2014 |DAILY BEAST 

The genre was always a simple one, easy to satirize and dismiss, though immensely profitable. Goodbye to My Soap Star Life |Michael E. Knight |September 20, 2011 |DAILY BEAST 

These satirize the customs and social habits of the Jews of his day in a bright and powerful style. Chapters on Jewish Literature |Israel Abrahams 

We may satirize character and qualities in the abstract without injury to our moral nature, but persons hardly ever. George Eliot's Life, Vol. I (of 3) |George Eliot 

"Don't satirize it," she exclaimed, looking up at him with a start. The Adventures of a Widow |Edgar Fawcett 

It is true, also, that Thackeray approached "society" rather to satirize it than to set forth its agreeableness. From Chaucer to Tennyson |Henry A. Beers 

All that we used to satirize in former entertainments of this kind fails to exist in those I am describing. An Ambitious Woman |Edgar Fawcett