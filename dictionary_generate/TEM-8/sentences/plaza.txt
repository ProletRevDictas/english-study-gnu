The normally bustling plazas around the State House were empty. Maryland lawmakers return to Annapolis for start of unusual 90-day session |Ovetta Wiggins, Erin Cox |January 13, 2021 |Washington Post 

You would be reasonably close, to be able to protest and express your view, but nobody belongs on the Capitol plaza. Woman dies after shooting in U.S. Capitol; D.C. National Guard activated after mob breaches building |Washington Post Staff |January 7, 2021 |Washington Post 

“They’re not only fighting for major plazas and smuggling routes, but they’re also fighting for corners,” Calderón said. Border Report: What’s Behind Tijuana’s Violent Year |Maya Srikrishnan |December 15, 2020 |Voice of San Diego 

Like a public plaza playground-y, there are kids and people climbing on stuff — but shops, and llama signs. PLAYBACK (2015): Could the Next Brooklyn Be … Las Vegas?! (Ep. 205) |Stephen J. Dubner |December 6, 2020 |Freakonomics 

One Saturday, a man from Providence, Rhode Island, stopped her in the plaza and asked for help registering some seniors back in his state. How to fix America’s voter registration system so more people can vote |Jen Kirby |October 6, 2020 |Vox 

In her struggle to find her daughter, Esther becomes one of the founders of the Mothers of Plaza de Mayo. How Pope Francis Became the World’s BFF |Jason Berry |December 21, 2014 |DAILY BEAST 

The Plaza Hotel cooked a fancy-pants latke with red wine braised oxtail, horseradish sunchoke cream, and crispy kale. I Ate Potato Pancakes Til I Plotzed |Emily Shire |December 17, 2014 |DAILY BEAST 

A crate of the stuff arrived at the studio, compliments of the Plaza Athénée. Alfred Hitchcock’s Fade to Black: The Great Director’s Final Days |David Freeman |December 13, 2014 |DAILY BEAST 

When they do dine, the Hitchcocks sometimes use Limoges china marked “Plaza Athénée.” Alfred Hitchcock’s Fade to Black: The Great Director’s Final Days |David Freeman |December 13, 2014 |DAILY BEAST 

In August, Trump filed a lawsuit to have his name removed from the casino and from the nearby, since-closed Trump Plaza. I Watched a Casino Kill Itself: The Awful Last Nights of Atlantic City’s Taj Mahal |Olivia Nuzzi |December 8, 2014 |DAILY BEAST 

This rampart of dwellings was in the shape of a rectangle, and enclosed a large square or plaza containing a noble reservoir. Overland |John William De Forest 

Twenty men and twenty women advanced to the centre of the plaza in double file and faced each other. Overland |John William De Forest 

The Plaza was full of people, women talking under the stiff palms, and men sitting on wicker chairs on the hotel piazza opposite. The Belted Seas |Arthur Colton 

Meantime the Plaza is being filled with chairs—rocking-chairs—which seem to spring up out of nothing. Gardens of the Caribbees, v. 1/2 |Ida May Hill Starr 

The Plaza is still the parlor in Guadalajara and it's enchanting! Jane Journeys On |Ruth Comfort Mitchell