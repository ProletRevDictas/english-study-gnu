SPCA International has arranged foster care for Sully and Peggy, while efforts continue to reunite them with Weldon and Kurulgan. There's a Travel Ban on Dogs From More Than 100 Countries, and You Can Blame COVID-19 |Melissa Chan |November 10, 2021 |Time 

Weldon worries that Sully, who he raised from a ball of fur that could barely stand to a healthy dog prancing around his Marine base, might forget him. There's a Travel Ban on Dogs From More Than 100 Countries, and You Can Blame COVID-19 |Melissa Chan |November 10, 2021 |Time 

Sully told Keaney he “wanted to study something about basketball,” his favorite sport. Kids help elevate E.B. Henderson’s contributions to basketball |Fred Bowen |June 2, 2021 |Washington Post 

I begin to observe that it sounds as if Sully is in microcosm what Newman himself…but that is as far as I get. The Stacks: The Eyes of Winter: Paul Newman at 70 |Peter Richmond |October 11, 2014 |DAILY BEAST 

In giving Sully a life, he gave the character some of his own life. The Stacks: The Eyes of Winter: Paul Newman at 70 |Peter Richmond |October 11, 2014 |DAILY BEAST 

Sully decides to face the truth of what his negligence has sown. The Stacks: The Eyes of Winter: Paul Newman at 70 |Peter Richmond |October 11, 2014 |DAILY BEAST 

But he was also showing a gritty and sully city in a beautiful way. Bill Cunningham: Through the Lens of a Style King |Justin Jones |March 14, 2014 |DAILY BEAST 

The Sully people catalogue a variety of reactions to today's job numbers. It's a 'Could Be Worse' Economy, and We're Living in It |Justin Green |December 7, 2012 |DAILY BEAST 

All day long I sully sheet after sheet of paper and beguile the tedious hours with the half-faded recollections of my childhood. Marguerite |Anatole France 

He sent Sully over to cement the good understanding of the two States by arguments and gifts to the leading courtiers. Sir Walter Ralegh |William Stebbing 

Sully was represented at the Philadelphia Academy by one hundred and sixteen pictures. Revolutionary Reader |Sophie Lee Foster 

No pains were spared to sully his character, to ruin his fortunes, and to render him an object of public indignation. The Chronicles of Crime or The New Newgate Calendar. v. 1/2 |Camden Pelham 

There are, nevertheless, three or four female heads, of an ethereal beauty-portraits in the manner of Sully. The Works of Edgar Allan Poe |Edgar Allan Poe