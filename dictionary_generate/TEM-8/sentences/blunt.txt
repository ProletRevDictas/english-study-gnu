A 2018 survey by the International Monetary Fund, cited by the Wall Street Journal, found government bankers are experimenting with the technology as a way to lower costs and to blunt the rise of private cryptocurrencies like Bitcoin. Mastercard launches digital currency kit for central banks |Jeff |September 9, 2020 |Fortune 

Washington’s inability to strike a deal on a new stimulus spending plan, market observers agree, has blunted the economic recovery. Jittery investors eye today’s big jobs report as markets rebound from an epic sell-off |Bernhard Warner |September 4, 2020 |Fortune 

Ethically speaking, then, any respectful engagement with them calls for a recognition of that fact, not a blunt attempt at persuasion. In The Time Of The COVID-19 Pandemic, What Should You Say To Someone Who Refuses To Wear A Mask? A Philosopher Weighs In |LGBTQ-Editor |August 30, 2020 |No Straight News 

Galloway, known well for blunt and opinionated commentary on the tech industry, says he grew concerned that Robinhood had become “casino-like” and gamified, encouraging newer investors to trade when they had little knowledge of the markets. Public, a stock trading app, gets a seven-figure check from Scott Galloway |Lucinda Shen |August 25, 2020 |Fortune 

They either blunt other economic activity or have too small of an overall impact to even measure. Why are local governments paying Amazon to destroy Main Street? |jakemeth |August 23, 2020 |Fortune 

The pale, baby-faced, red-cheeked rapper is furiously puffing away at a hastily-made blunt crammed with low-grade weed. The Cult of Yung Lean: ‘I’m Building An Anarchistic Society From the Ground Up’ |Marlow Stern |January 4, 2015 |DAILY BEAST 

This was very blunt and surprising to hear from any official in charge of an aviation disaster. Did Bad Weather Bring Down AirAsia 8501? |Clive Irving |December 29, 2014 |DAILY BEAST 

The real reason Mister Ham was taking bids was, to be perfectly blunt, because he needed the money. The Stacks: Sell the Overcoat, Keep the Dignity |Paul Hemphill |December 22, 2014 |DAILY BEAST 

No matter how admirable or inspiring his message appears to be, it often hits you over the head like a blunt instrument. Catch Him If You Can: Reliving Banksy’s New York Invasion |Alex Suskind |November 14, 2014 |DAILY BEAST 

“It was like a curtain was beginning to be opened,” says Noor, who was surprised by the blunt request. Obama’s Deadly Informants: The Drone Spotters of Pakistan |Umar Farooq, Syed Fakhar Kakakhel |November 12, 2014 |DAILY BEAST 

Presently his blunt ungainly head rose within ten feet of them. Hunting the Lions |R.M. Ballantyne 

She shook her head—Gilbert was not at home, and her axe was so blunt that a body might ride to Rumford on it. The World Before Them |Susanna Moodie 

Jess worked hard over the head, pushing the padding well into the blunt nose. The Box-Car Children |Gertrude Chandler Warner 

Then she picked up a magazine and glanced through it, cutting the pages with a blunt edge of her knife. The Awakening and Selected Short Stories |Kate Chopin 

You can bet no trace will ever be found of that blunt instrument, and naturally he left no evidence coming or going. We're Friends, Now |Henry Hasse