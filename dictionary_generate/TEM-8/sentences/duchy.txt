The Pahonia, emblem of the Belarusian nation drawn from the insignia of the Grand Duchy of Lithuania, is banned under Lukashenko. Forget Kim Jong Un—China’s New Favorite Dictator Is Belarus’s Aleksandr Lukashenko. |Kapil Komireddi |January 28, 2014 |DAILY BEAST 

It relies on the Duchy to provide it with accurate information without carrying out its own independent checks. Report Published on Prince Charles's Tax Affairs |Tom Sykes |November 5, 2013 |DAILY BEAST 

It also criticised the lack of official scrutiny of the Duchy's account. Report Published on Prince Charles's Tax Affairs |Tom Sykes |November 5, 2013 |DAILY BEAST 

Well, the Prince last year received an annual income from the Duchy of £19m, on which he paid £4.4m in income tax and VAT. MP: Prince Charles Pays Less Tax Than His Servants |Tom Sykes |July 16, 2013 |DAILY BEAST 

He will also lose the massive income from the lands held by the Duchy of Cornwall, which currently provide some of their funding. Charles Charities Face Funding Crisis |Tom Sykes |June 5, 2013 |DAILY BEAST 

As Foreign Minister, by simply taking what he wanted, he added considerably to the extent of his duchy. Napoleon's Marshals |R. P. Dunn-Pattison 

On her death the duchy of Brabant passed, by a family arrangement, to the House of Burgundy. Belgium |George W. T. (George William Thomson) Omond 

You are right as to yourself, Albert; for they have given you the ancient title and duchy of de Retz. Catherine de' Medici |Honore de Balzac 

Saxony had recovered her independence, the peoples of Dantzic and the duchy of Warsaw their country and their rights. The Life of Napoleon Bonaparte |William Milligan Sloane 

In order to populate the new port, he proclaimed there a religious liberty he denied to his Duchy at large. Florence and Northern Tuscany with Genoa |Edward Hutton