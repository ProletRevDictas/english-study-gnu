For, in mitigating the symptoms of mental illness, we also work to mitigate the stigma against it. How colonialism and capitalism helped place a stigma on mental illness |Balaji Ravichandran |February 12, 2021 |Washington Post 

Albanese says, the most effective method for mitigating bird strikes was shooting birds that presented an immediate danger to aircraft. Four wild animals that are thriving in cities |By Ryan Chelius/Outdoor Life |February 9, 2021 |Popular-Science 

In short, using advanced data analytics we can better assess, price and significantly mitigate risk. Twinco Capital scores €3M for its supply chain finance solution |Steve O'Hear |February 5, 2021 |TechCrunch 

For example, it could be equipped with shock-mitigating seats, a hard top, a crash engine rail, lights, cameras, and so on. This fast French military boat can crawl from water to land without wheels |Christina Mackenzie |February 3, 2021 |Popular-Science 

With that information, the team can mitigate the risk of overtraining and can better help its athletes recover from injuries. As biometrics boom, who owns athletes’ data? It depends on the sport. |Nick Busca |February 2, 2021 |Washington Post 

But the military can mitigate the risks simply by virtue of its enormous logistical reach. The Military’s Mission to Fight Ebola Might Be Dangerous But it Won’t Be Black Hawk Down |Nathan Bradley Bethea |September 19, 2014 |DAILY BEAST 

Of course, cities can take steps right away to mitigate the damage done by militarizing law enforcement. What’s Next, Police With Tanks? |James Poulos |June 28, 2014 |DAILY BEAST 

The deafening klaxons can leave one feeling helpless, but there are still steps you can take to mitigate the damage. How to Mitigate the Damage of the Heartbleed Security Hole |Joshua Kopstein |April 11, 2014 |DAILY BEAST 

Yes, you can do a lot to mitigate this by providing mentors, training, college prep, and other services. The Flaw in My Brother’s Keeper |Jamelle Bouie |February 28, 2014 |DAILY BEAST 

There are lots of things, including changing the kind of inner dialog, that can mitigate anxiety. A Q&A with Scott Stossel, Author of ‘My Age of Anxiety: Fear, Hope, Dread, and the Search for Peace of Mind’ |Jesse Singal |February 20, 2014 |DAILY BEAST 

If she have a tongue that can cure, and likewise mitigate and shew mercy: her husband is not like other men. The Bible, Douay-Rheims Version |Various 

And there being constant danger of excess, the effort ought to be by force of public opinion to mitigate and assuage it. Key-Notes of American Liberty |Various 

No sense of her goodness, her injury and nobility, and the enormous generosity of her forgiveness, sufficed to mitigate that. The New Machiavelli |Herbert George Wells 

Rendered furious by this insolence, I forbade him my sight; and, without seeking to mitigate my anger, he departed for France. The Mysterious Wanderer, Vol. III |Sophia Reeve 

It remains the right term and your simplicity doesn't mitigate it. The Tragic Muse |Henry James