After testing, the researchers showed that the air’s humidity — water content — had been powering the setup. Will bacterial ‘wires’ one day power your phone? |Alison Pearce Stevens |September 2, 2020 |Science News For Students 

The company has developed an attachment for air-conditioning systems that uses these materials to reduce the humidity in the air before it goes into a standard unit. Air conditioning technology is the great missed opportunity in the fight against climate change |James Temple |September 1, 2020 |MIT Technology Review 

Six instruments distributed across the neck, body and interior will measure air temperature, air pressure, humidity, radiation and wind speed and direction. NASA’s Perseverance rover will seek signs of past life on Mars |Lisa Grossman |July 28, 2020 |Science News 

What’s more, he points out, “The challenge is not just to take up water from humid air, but to do it at low humidity, too.” Here’s one way to harvest water right out of the air |Sid Perkins |April 24, 2020 |Science News For Students 

They exposed their aluminum-based MOF to outdoor air with humidity as low as 10 percent. Here’s one way to harvest water right out of the air |Sid Perkins |April 24, 2020 |Science News For Students 

In the desert, an unbelievable humidity emerges when standing near water sources. Whatever You Do Someone Will Die. A Short Story About Impossible Choices in Iraq |Nathan Bradley Bethea |August 31, 2014 |DAILY BEAST 

Fall and early summer are the ideal times to visit, in-between the bitter winters and the humidity of the hot August nights. The Foodie Capital of Canada |Michele Willens |May 31, 2014 |DAILY BEAST 

The unit features three unlabeled controls, as well as a digital display of the current temperature and humidity. Are U.S. Kids Creative Enough? |Amanda Ripley |April 2, 2014 |DAILY BEAST 

Dehydration, in heat and humidity as well as dry winter weather, is a major headache trigger. Why Everything from Frigid Temperatures to Lightning Can Induce Migraines |Caitlin Dickson |January 8, 2014 |DAILY BEAST 

When Moore was little, the two would spend almost all of August at Disney World, braving the humidity and crowds. ‘Escape From Tomorrow’: Making Disney’s Worst Nightmare |Marlow Stern |October 9, 2013 |DAILY BEAST 

As all parts of this apparatus are of metal changes in humidity or temperature do not affect its regulation. The Recent Revolution in Organ Building |George Laing Miller 

The humidity of the earth had rusted the screws, and it was not without some difficulty that the coffin was opened. Camille (La Dame aux Camilias) |Alexandre Dumas, fils 

As camels live in the desert they must keep smelling the air to find out its humidity. Kari the Elephant |Dhan Gopal Mukerji 

The great humidity gives rise to many diseases, particularly fevers, and the alternations from heat to damp cause dysentery. Travels in Peru, on the Coast, in the Sierra, Across the Cordilleras and the Andes, into the Primeval Forests |J. J. von Tschudi 

In the cold season its weight increases by the absorption of humidity. Travels in Peru, on the Coast, in the Sierra, Across the Cordilleras and the Andes, into the Primeval Forests |J. J. von Tschudi