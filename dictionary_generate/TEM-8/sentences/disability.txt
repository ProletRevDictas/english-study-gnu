If they’re designed well, for example, AI-based learning tools have been shown to improve children’s critical-thinking and problem-solving skills, and they can be useful for kids with learning disabilities. Why kids need special protection from AI’s influence |Karen Hao |September 17, 2020 |MIT Technology Review 

Central Aroostook Association, a Presque Isle nonprofit that helps children with intellectual disabilities, switched to the co-op last year to save 20% on its health premiums, said administrator Tammi Easler. Only three of 26 Obamacare-era nonprofit health insurance co-ops will soon remain |lbelanger225 |September 6, 2020 |Fortune 

Users with disabilities might also want to compare their statistics to their non-wheelchair-using friends. Smart Watches Could Do More For Wheelchair Users |John Loeppky |September 4, 2020 |FiveThirtyEight 

Anyone from an underrepresented group — including people of color, women, LGBTQ people and people with disabilities — is welcome to apply to be a mentee. Hand-Picked Mentors and Networking: Apply for ProPublica’s 2020 Diversity Mentorship Program at ONA |by ProPublica |August 31, 2020 |ProPublica 

MTS changed its vetting process in March 2011 after the agency reported a spike in the number of seniors and people with disabilities with reduced-fare passes. MTS Frequently Overrules Doctors’ Orders on Reduced Fares for the Disabled |Lisa Halverstadt |August 31, 2020 |Voice of San Diego 

An IQ below 70 generally indicates someone with intellectual disability (ID). How the U.S. Justice System Screws Prisoners with Disabilities |Elizabeth Picciuto |December 16, 2014 |DAILY BEAST 

As a matter of dollars and cents, America in the short term may be able to afford disability and food stamps. Bush, Christie, Romney: Who’ll Be the GOP Class Warrior? |Lloyd Green |December 15, 2014 |DAILY BEAST 

Sen. Tom Harkin of Iowa, a longtime disability advocate, has made HCBS a priority, a Harkin aide told The Daily Beast. Medicaid Will Give You Money for At-Home Care, but You Might Wait Years |Elizabeth Picciuto |December 2, 2014 |DAILY BEAST 

Jason Kingsley, the son of one of the producers, would go on to appear 55 times on the show talking about his disability. ‘Sesame Street’ Is Middle-Aged and Awesome |Emily Shire |November 10, 2014 |DAILY BEAST 

What if the pain her mother and doctors observed had nothing to do with disability, but was in fact pain? U.K. Courts Grant Mother Right to End Her 12-Year-Old Disabled Daughter’s Life |Elizabeth Picciuto |November 4, 2014 |DAILY BEAST 

Their disability however has been largely removed by statutes in all the states, as we shall learn in another place. Putnam's Handy Law Book for the Layman |Albert Sidney Bolles 

But a disability arising after the statute has begun to run in his favor will not prevent it from running. Putnam's Handy Law Book for the Layman |Albert Sidney Bolles 

In the face of such motives, in the disability under which I labor of stopping the evil, I had to seek my own safety. The Sword of Honor, volumes 1 &amp; 2 |Eugne Sue 

On the 14th of October, 1862, Mr. Walter was honorably discharged from the service on account of disability. Lyman's History of old Walla Walla County, Vol. 2 (of 2) |William Denison Lyman 

(b) For total or partial disability for less than five years, 60 per cent. Proceedings, Third National Conference Workmen's Compensation for Industrial Accidents |Various