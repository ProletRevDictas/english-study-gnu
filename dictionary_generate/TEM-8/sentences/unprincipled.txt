In any event, the Court has previously drawn unprincipled lines that are difficult to square with legal texts and existing doctrines. The Supreme Court appears really eager to force taxpayers to fund religious education |Ian Millhiser |December 8, 2021 |Vox 

“Roe and Casey are unprincipled decisions that have damaged the democratic process, poisoned our national discourse, plagued the law — and, in doing so, harmed this Court,” the brief states. Mississippi asks Supreme Court to overturn Roe v. Wade in upcoming case |Robert Barnes |July 22, 2021 |Washington Post 

Refusing to do so on the basis of ethnic solidarity is an unprincipled copout. Three Cheers for a Settlement Boycott |Hussein Ibish |March 21, 2012 |DAILY BEAST 

In politics, impractical positions count as inherently … unprincipled. Stop Rewriting Thanksgiving—and the Rest of History |Michael Medved |November 25, 2011 |DAILY BEAST 

How did BP become an unprincipled bottom-line operator ready to cut any corners for profit? Why Is BP's Former Boss a U.K. Hero? |Clive Irving |June 10, 2010 |DAILY BEAST 

I also thought he was completely unprincipled—he could change his views like leaves change their colors. The 10 Biggest Scoops From Karl Rove's Memoir |The Daily Beast |March 8, 2010 |DAILY BEAST 

Anything more high-minded than an unprincipled grab for power voters might consider a sham. Our Gutless Turn-Tail Politics |Lee Siegel |January 9, 2010 |DAILY BEAST 

O Bessie, there are so many unprincipled men in the world who love to win and betray the confidence of young innocent girls. The value of a praying mother |Isabel C. Byrum 

That an unprincipled man should be followed by a majority of the House of Commons is no doubt an evil. The History of England from the Accession of James II. |Thomas Babington Macaulay 

Others muttered that the event which all good men lamented was to be ascribed to unprincipled ambition. The History of England from the Accession of James II. |Thomas Babington Macaulay 

Orford was covetous and unprincipled; but he had great professional skill and knowledge, great industry, and a strong will. The History of England from the Accession of James II. |Thomas Babington Macaulay 

Unprincipled men, alike in Church and State, made use of their position and power to gain their own ends and enslave the people. Hunted and Harried |R.M. Ballantyne