Not civic or selfless, but corrosive, destructive and illegal. The Capitol mob images shouldn’t surprise you. Open insurrection was always where we were headed. |Philip Kennicott |January 6, 2021 |Washington Post 

A “good” coach is selfless as a leader and puts the needs of others ahead of his own interests, while an “evil” coach is willing to actively harm others to get what he wants. Who’s The Most Chaotic Movie Football Coach? |Josh Hermsmeyer |December 4, 2020 |FiveThirtyEight 

He represents the selfless service so many of our armed service members embody, and it’s one of the many reasons he’s put himself in a position to win on Election Day. The Trailer: Is this the future of the GOP? |David Weigel |October 20, 2020 |Washington Post 

The selfless civic engagement we see this season is to be applauded, as fortifying democracy becomes the business of American business. Why America’s volunteer spirit could save the election |jakemeth |October 19, 2020 |Fortune 

Officer Leath will be deeply missed for how selfless and caring she was, and Mayor Joe Hogsett ordered the city’s flags to be flown at half-staff in her honor. 24-Year-Old Officer Fatally Shot While Trying To Save A Woman On A Domestic Violence Call |D. N. |April 15, 2020 |No Straight News 

Yet Lohse is confident that the reader will take his actions as the fruits of selfless moral courage. An Ivy League Frat Boy’s Shallow Repentance |Stefan Beck |November 24, 2014 |DAILY BEAST 

“These medical workers are amazing, such selfless people,” he says. The American Ebola Rescue Plan Hinges on One Company. Meet Phoenix. |Abby Haglage |November 22, 2014 |DAILY BEAST 

We celebrate military heroes and selfless individuals who sacrifice their own lives for the good of others. On Her Own Terms: Why Brittany Maynard Has Chosen to Die |Gene Robinson |October 12, 2014 |DAILY BEAST 

In fact, our devotion to those ideals has only been strengthened by the selfless heroism we have seen. The Resilient City: New York After 9/11 |John Avlon |September 11, 2014 |DAILY BEAST 

The letter closes with noble words of faith and hope from the hot zone where screw-ups are met each day with selfless courage. How Bureaucrats Let Ebola Spread to Nigeria |Michael Daly |August 14, 2014 |DAILY BEAST 

He should have that oil of joy for his work, for his high, selfless purposes. Jane Journeys On |Ruth Comfort Mitchell 

She appears as utterly selfless, as having devoted herself to the will of God as He shall manifest that will. Our Lady Saint Mary |J. G. H. Barry 

Thus our aims will have been pure and selfless; each one of us here will have risked all for the sake of an unknown. "Unto Caesar" |Baroness Emmuska Orczy 

There is in every country a number of people who are selfless followers of liberty and who desire to see every country free. Freedom Through Disobedience |C. R. (Chittaranjan) Das 

With my greater powers, poured through me for selfless ends by the God of All, I could use force and render him harmless. A Son of Perdition |Fergus Hume