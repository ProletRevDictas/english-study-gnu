The proposal also would raise ACA insurance subsidies for consumers already eligible for that help. Affordable Care Act subsidies likely to increase under congressional plan |Amy Goldstein |February 11, 2021 |Washington Post 

And, for those more reliant on phrase match, be sure to keep an eye on your keywords as the change occurs since additional queries will be eligible to match, and make use of negative keywords to ensure that your budget is being used effectively. Google might have to subsidize journalism, but not like this… |George Nguyen |February 10, 2021 |Search Engine Land 

If the players test negative, they could be eligible to face the Cavaliers, unless they are subject to punishment from UNC’s basketball program or university administrators. UNC-Miami postponed after two Tar Heels are shown celebrating Duke win without masks |Des Bieler |February 9, 2021 |Washington Post 

Anything over that would not be eligible, because they are the people who really are hurting right now and need the help the most. House Democrats reject plan to sharply curtail $1,400 stimulus payments in coronavirus relief package |Jeff Stein, Erica Werner |February 9, 2021 |Washington Post 

They have missed the past eight games but are eligible to play as early as Tuesday night when Washington hosts the Philadelphia Flyers. Capitals’ Evgeny Kuznetsov, Ilya Samsonov return to practice after bouts with the coronavirus |Samantha Pell |February 8, 2021 |Washington Post 

If you answered seven or more of these correctly, you are eligible for a lifetime supply of Metamucil. The World’s Toughest Political Quiz |Jeff Greenfield |December 31, 2014 |DAILY BEAST 

At-home caretakers are eligible for Medicaid waivers, which allow benefits regardless of income. Medicaid Will Give You Money for At-Home Care, but You Might Wait Years |Elizabeth Picciuto |December 2, 2014 |DAILY BEAST 

Jones ruled for Ray and he is now back in business, eligible to sign with any NFL club. The $44 Million Teflon Don of the NFL |Mike Barnicle |November 30, 2014 |DAILY BEAST 

Expect many of those who are eligible for relief under the new program to say, “Thanks but no thanks” and go on their way. Will 5 Million Undocumented Immigrants Take Obama's Tough Love Immigration Deal? |Ruben Navarrette Jr. |November 21, 2014 |DAILY BEAST 

They profoundly question whether these programs will be available to them when they become eligible. This Republican Loved Taxes & Modern Art |Scott Porch |November 19, 2014 |DAILY BEAST 

The Seine and Aulbe rivers render the situation of this domain as beautiful as it is strong and eligible for defense. The Jesuit Relations and Allied Documents, Vol. II: Acadia, 1612-1614 |Various 

Ever since her unexpected good fortune, Mr. Rushmere had secretly contemplated Miss Watling as a very eligible wife for his son. The World Before Them |Susanna Moodie 

It will be used in court proceedings, and no person will be eligible for Government service who does not know that language. The Philippine Islands |John Foreman 

Upon whom does the country look, as the most eligible of her favored sons? The Condition, Elevation, Emigration, and Destiny of the Colored People of the United States |Martin R. Delany 

I should have taken out my revenge by marrying the first eligible man that offered himself. Elster's Folly |Mrs. Henry Wood