Even those who have remained free and clear of the virus have had their lives fundamentally disrupted. The NBA’s week of controversies show how hard life is outside of the bubble |Ben Golliver |February 12, 2021 |Washington Post 

If sites contain words with a high volume, it’s clear that they’re going to get more traffic. A comprehensive guide on using Google Trends for keyword research |Aayush Gupta |February 12, 2021 |Search Engine Watch 

If Bauer pitches to the level his contract dictates, even for just one year, the Dodgers are clear favorites to repeat as World Series champions. Trevor Bauer, unorthodox star with an unorthodox deal, gets an unorthodox Dodgers intro |Chelsea Janes |February 12, 2021 |Washington Post 

A clear sign of the times, it was the first instance of the luxury powerhouse partnering with a celebrity, or a woman of color, to launch a brand from scratch. Why Rihanna’s luxury collaboration with LVMH failed |Marc Bain |February 10, 2021 |Quartz 

Rebecca Hull recently launched a clear mask giveaway through the Independence Center, a nonprofit that advocates for people with disabilities in Colorado Springs. Ford’s next pandemic mission: Clear N95 masks and low-cost air filters |Hannah Denham |February 9, 2021 |Washington Post 

The story of fluoridation reads like a postmodern fable, and the moral is clear: a scientific discovery might seem like a boon. Anti-Fluoriders Are The OG Anti-Vaxxers |Michael Schulson |July 27, 2016 |DAILY BEAST 

But the tide was turning on this issue, an email from another constituent made clear. Jeb Bush’s Unseen Anti-Gay Marriage Emails |Jackie Kucinich |January 9, 2015 |DAILY BEAST 

The use of slurs from both characters makes it clear just how “new” the idea of an openly gay son is even in this time. ‘Empire’ Review: Hip-Hop Musical Chairs with an Insane Soap Opera Twist |Judnick Mayard |January 8, 2015 |DAILY BEAST 

Instead, straighten your civic backbone and push back in clear conscience. Why We Stand With Charlie Hebdo—And You Should Too |John Avlon |January 8, 2015 |DAILY BEAST 

He made clear that he fully appreciated what the cops had done. Shot Down During the NYPD Slowdown |Michael Daly |January 7, 2015 |DAILY BEAST 

It separates into three layers upon standing—a brown deposit, a clear fluid, and a frothy layer. A Manual of Clinical Diagnosis |James Campbell Todd 

However this be, it is hard to say that these fibs have that clear intention to deceive which constitutes a complete lie. Children's Ways |James Sully 

Knowing by experience that he would soon be up to it, he used his pole with all his might, hoping to steer clear of it. The Giant of the North |R.M. Ballantyne 

I am pleading for a clear white light of education that shall go like the sun round the whole world. The Salvaging Of Civilisation |H. G. (Herbert George) Wells 

Is the Bible revelation so clear and explicit that no difference of opinion as to its meaning is possible? God and my Neighbour |Robert Blatchford