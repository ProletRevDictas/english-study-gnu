It is a woeful abuse of history to claim the filibuster protects the minority from the tyranny of the majority. The definitive case for ending the filibuster |Ezra Klein |October 1, 2020 |Vox 

These tactics jettison a different kind of tyranny — the tyranny of the click — and leverage the duration of readers’ engagement instead. With quality content, publishers can overcome the ‘tyranny of choice’ |Duration Media |October 1, 2020 |Digiday 

Memory releases our mental life from the tyranny of the present moment. You can’t completely trust your memories |David Linden |September 30, 2020 |Popular-Science 

We are here today to declare that we will never submit to tyranny. Election live updates: Trump returns to Wisconsin; Biden to face live audience at town hall |Colby Itkowitz, Felicia Sonmez, John Wagner |September 17, 2020 |Washington Post 

Lewis’s concept was that even that would be preferable to “omnipotent moral busybodies” being in charge, because he said that could lead to what “may be the most oppressive” form of tyranny. William Barr’s eyebrow-raising ‘robber barons’ metaphor for the Trump era |Aaron Blake |September 17, 2020 |Washington Post 

Applying the apartheid label is incorrect—and is also confusing because it obscures the tyranny which is in force. Don’t Accuse Israel of Apartheid |Benjamin Pogrund |July 17, 2014 |DAILY BEAST 

This platform can be a force multiplier for those struggling against tyranny. New Web Platform Crowdsources Human Rights |Josh Rogin |July 10, 2014 |DAILY BEAST 

The Bill of Rights, and especially the First Amendment, were intended to protect the powerless from the tyranny of the powerful. The Supreme Court Turns the First Amendment Into a Weapon for Corporations |Sally Kohn |July 8, 2014 |DAILY BEAST 

In one fell swoop, the Supreme Court has constrained government power, expanded corporate rights, and protected religious tyranny. Hobby Lobby: Sex, Lies, and Craft Supplies |Sally Kohn |July 2, 2014 |DAILY BEAST 

It also called for the establishment of laws and institutions that might protect minorities against the tyranny of the majority. How Iran and America Can Beat ISIS Together |Ben Van Heuvelen |June 21, 2014 |DAILY BEAST 

The object of these scarli is to manifest the popular exultation at the annihilation of feudal tyranny. The Pit Town Coronet, Volume I (of 3) |Charles James Wills 

Mr. Hutchinson was doubtless temperamentally less inclined to fear tyranny than anarchy. The Eve of the Revolution |Carl Becker 

Yet the Clarion opposes sweating and tyranny and hypocrisy, and does its best to defeat and to destroy them. God and my Neighbour |Robert Blatchford 

She thinks of politics, and of the tyranny of kings and nobles, and of the unjust inequalities of man. Madame Roland, Makers of History |John S. C. Abbott 

Amid the perpetration of much bloodshed and tyranny, Christianity has certainly achieved some good. God and my Neighbour |Robert Blatchford