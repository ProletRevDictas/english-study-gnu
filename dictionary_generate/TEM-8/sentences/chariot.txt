Interestingly enough, the Egyptians even went on to create state-funded stud farms and grazing grounds for horses that were needed for chariots. The Ancient Egyptian Soldiers of the New Kingdom |Dattatreya Mandal |July 6, 2022 |Realm of History 

He and all of the Jackass crew must sense the breeze of time’s winged chariot catching up with their naked butts. Jackass Forever Is an Idiotic Delight, Attuned to the Passing of Wind and Time |Stephanie Zacharek |February 3, 2022 |Time 

Phaethon is the son of the sun-god Helios, who drives his blazing chariot across the sky every day. What the Greek Myths Can Teach Us About Our Moment of Crisis |Charlotte Higgins |January 19, 2022 |Time 

It’s one thing to read about it but another to wonder whether the wide stones, so soft to the touch, were flattened by wheels of ancient chariots. In France, Narbonne takes a turn in the international spotlight |Sylvie Bigar |October 29, 2021 |Washington Post 

The blasted Trojan landscape — once-fertile farmland scarred by “chariot wheels and the tramp of marching feet,” a beach strewn with dead sea creatures — reminds us that the devastation we wreak on the Earth is by no means a modern phenomenon. Pat Barker’s ‘The Women of Troy’ continues her brilliant reassessment of the Trojan War |Wendy Smith |August 24, 2021 |Washington Post 

And a man who then demonstrated that a wheelchair can be a chariot. How Brooklyn’s First Ice Cream Girl Fought City Hall–and Won |Michael Daly |October 13, 2014 |DAILY BEAST 

Chestnut was last, carried on a yellow chariot through a sea of adoring fans. How to Stomach a Hot Dog Eating Contest |Robert Silverman |July 5, 2014 |DAILY BEAST 

"We'll have your triumphal chariot waiting outside to bring you back to your hotel tonight," the auctioneer joked. Leonardo DiCaprio’s Big Christie’s Auction Brings in $38.8 Million |Isabel Wilkinson |May 14, 2013 |DAILY BEAST 

The tattered outcast dozes on his bench while the chariot of the wealthy is drawn by. The Unsolved Riddle of Social Justice |Stephen Leacock 

Martini was on his mission to Vienna; but another valet was put into the chariot to support the Duke. The Pastor's Fire-side Vol. 3 of 4 |Jane Porter 

It was Ezechiel that saw the glorious vision, which was shewn him upon the chariot of cherubims. The Bible, Douay-Rheims Version |Various 

Pretty well for "a cross between an Astley's chariot, a flying machine and a treadmill." Glances at Europe |Horace Greeley 

And Elam took the quiver, the chariot of the horseman, and the shield was taken down from the wall. The Bible, Douay-Rheims Version |Various