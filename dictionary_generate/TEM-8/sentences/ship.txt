Greece responded by sending its own ships to trail Turkey’s vessels. Butterfly Effect: The Next U.S.-Russia Conflict Theater Could be the Mediterranean |Charu Kasturi |September 17, 2020 |Ozy 

Early in Quantum Reality, Baggott likens science to a ship that travels back and forth over the “Sea of Representation” from the rocky shores of Empirical Reality to the sandy beaches of Metaphysical Reality. Your Guide to the Many Meanings of Quantum Mechanics - Facts So Romantic |Sabine Hossenfelder |September 3, 2020 |Nautilus 

In the Americas, there has been a series of recent attacks against oil platforms and ships in Mexico’s Bay of Campeche. Is Maritime Piracy Back from the Dead? |Eromo Egbejule |August 25, 2020 |Ozy 

In the DTC world especially it seems like for whatever reason the media buyers are the ones running the ship and making a lot of decisions on strategy. ‘The dollar amount isn’t worth the mental toll’: Confessions of a media buyer on the pressure to keep performance up amid the pandemic |Kristina Monllos |August 25, 2020 |Digiday 

One module ferries the crew to the moon’s surface from a larger ship in orbit. Here’s what NASA’s next moon lander may look like |Aaron Pressman |August 21, 2020 |Fortune 

After the captain made the call to abandon ship, 150 people were able to escape on lifeboats lowered by electronic arms. ‘We’re Going to Die’: Survivors Recount Greek Ferry Fire Horror |Barbie Latza Nadeau |December 29, 2014 |DAILY BEAST 

Nerd Cruise By Adam Rogers, Wired What 800 Nerds on a Cruise Ship Taught Me About Life, the Universe, and Snorkeling. The Daily Beast’s Best Longreads, Dec 22-28, 2014 |William Boot |December 28, 2014 |DAILY BEAST 

There was one bathroom on the ship, and there were no showers or beds. Inside the Smuggling Networks Flooding Europe with Refugees |Barbie Latza Nadeau |December 15, 2014 |DAILY BEAST 

Two years into an Arctic expedition, they were forced to abandon ship a thousand miles north of Siberia. The Best Nonfiction Books of 2014 |William O’Connor |December 14, 2014 |DAILY BEAST 

The estimated ship date of the gadget is December 2014—perfect timing to say sayonara to smoking forever. Nothing Says I Love You Like Data |The Daily Beast |December 8, 2014 |DAILY BEAST 

A wise man hateth not the commandments and justices, and he shall not be dashed in pieces as a ship in a storm. The Bible, Douay-Rheims Version |Various 

The president sat in a chair which came over with the pilgrims in their ship, the Mayflower. Blackwood's Edinburgh Magazine, Volume 60, No. 372, October 1846 |Various 

It was more like the boarding of a ship than any land fight I had ever seen or imagined. Blackwood's Edinburgh Magazine, No. CCCXXXIX. January, 1844. Vol. LV. |Various 

My orders ought to have been taken before a single unwounded Officer or man was ferried back aboard ship. Gallipoli Diary, Volume I |Ian Hamilton 

Fancy that enormous shell dropping suddenly out of the blue on to a ship's deck swarming with troops! Gallipoli Diary, Volume I |Ian Hamilton