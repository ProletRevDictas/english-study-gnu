In addition, their eldest child, who has cerebral palsy and is in a wheelchair, needs special care. Worker advocates push to extend COVID-related emergency paid leave program |lbelanger225 |December 18, 2020 |Fortune 

More than 40 years ago, Arizona set up a revolutionary system to protect the safety of residents with developmental disabilities like Down syndrome, autism and cerebral palsy. They Made a Revolutionary System to Protect People With Developmental Disabilities. Now It’s Falling Apart. |by Amy Silverman for Arizona Daily Star |December 12, 2020 |ProPublica 

Puzzle master David Kwong is adding his cerebral talents to a burgeoning new performing arts franchise — participatory theater online — with a warmly brain-teasing show that is selling tickets as fast as they can be printed. With playhouses dark, interactive theater online is lighting things up |Peter Marks |October 29, 2020 |Washington Post 

The story is similar for other cerebral measures, which is why trying to explain supposed cognitive sex differences through brain anatomy has not been very fruitful. Brain Scientists Haven’t Been Able To Find Major Differences Between Women’s And Men’s Brains, Despite Over A Century Of Searching |LGBTQ-Editor |August 13, 2020 |No Straight News 

Within the cerebrum, no region has received more attention in both race and sex difference research than the corpus callosum, a thick band of nerve fibers that carries signals between the two cerebral hemispheres. Brain Scientists Haven’t Been Able To Find Major Differences Between Women’s And Men’s Brains, Despite Over A Century Of Searching |LGBTQ-Editor |August 13, 2020 |No Straight News 

The cerebral McLaughlin, who also served as acting director of Central Intelligence, was hardly reassuring on the “what now?” Indefensible but Indispensable America |Jamie Dettmer |December 12, 2014 |DAILY BEAST 

In fact, the estrogen that they employed did worse than castrate the subject—it could act as a cerebral depressant. The Castration of Alan Turing, Britain’s Code-Breaking WWII Hero |Clive Irving |November 29, 2014 |DAILY BEAST 

Ross has cerebral palsy, and the Pathways to Careers initiative of SourceAmerica helped him get his position. Hiring People With Disabilities Isn’t Just the Right Thing to Do—It’s Good for Business |Elizabeth Picciuto |October 27, 2014 |DAILY BEAST 

Its pleasures are undoubtedly visual, but also more cerebral than many of the other performing arts. How High Fashion Saved the Ballet |Raquel Laneri |October 13, 2014 |DAILY BEAST 

Honor called the move “ballsy” and his friend shot back, “Cerebral ballsy!” Craziest SXSW Band Names: Perfect Pussy, Death By Unga Bunga, and More |Marlow Stern |March 8, 2014 |DAILY BEAST 

Now, the intrusion of a definite, uncontorted memory was evidence of returning cerebral activity. Dope |Sax Rohmer 

The cerebral neurasthenic makes rash, impetuous changes in his mode of life. Essays In Pastoral Medicine |Austin Malley 

The rest of the subjects are still lodged within the cerebral cells of the author. Comrade Kropotkin |Victor Robinson 

Is it not possible that one of the imperceptible keys of the cerebral finger-board has been paralyzed in me? Masterpieces of Mystery, Vol. 1 (of 4) |Various 

Wherever there has been much cerebral disturbance, traces of congestion are usually discernible. Cooley's Cyclopdia of Practical Receipts and Collateral Information in the Arts, Manufactures, Professions, and Trades..., Sixth Edition, Volume I |Arnold Cooley