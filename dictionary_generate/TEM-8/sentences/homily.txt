His sermons, unfailingly lucid, penetrating and inspired, went beyond the homily of the day to encompass the news of the world. Desmond Tutu, Anti-Apartheid Campaigner Who Tried to Heal the World, Dies at 90 |Aryn Baker |December 26, 2021 |Time 

He figured on letting the gospel, specifically Matthew 1:28, guide his homily. 11 Children Shot in Milwaukee, One in Her Grandpa's Lap |Michael Daly |November 12, 2014 |DAILY BEAST 

Then on March 23, Romero delivered a truly impassioned homily. Why Pope Francis Wants to Declare Murdered Archbishop Romero a Saint |Christopher Dickey |August 24, 2014 |DAILY BEAST 

Pope Francis was uncharacteristically ceremonial, sticking largely to his scripted homily and dressed in the usual papal garb. Onscene as Pope Francis Makes Saints of John Paul II and John XXIII |Barbie Latza Nadeau |April 27, 2014 |DAILY BEAST 

The question she was asked by the priest during his homily: “What would you like for Christmas?” ‘Resurrection’ Is TV’s Silliest Show and Probably Dead on Arrival |Kevin Fallon |March 7, 2014 |DAILY BEAST 

Since Wright had no plans to take part in the beachside homily and ticket booths were shuttered, she was out of luck. Pope Francis, ‘the Maker of Traffic Jams’ |Mac Margolis |July 26, 2013 |DAILY BEAST 

He mounts his best ruffles and his finest tunic as he sits down to write his homily. A Cursory History of Swearing |Julian Sharman 

Such a homily, at such a time, must have made Mary feel like a person of a very ordinary sort indeed. The Life and Letters of Mary Wollstonecraft Shelley, Volume I (of 2) |Florence A. Thomas Marshall 

The story remains, the burden of the rude rhyme of the primer, a text for many a homily of old,—a topic for us now. The Hearth-Stone |Samuel Osgood 

Well, I did not expect, when you handed me out of my carriage to-day, that I was going to listen to a homily on prudence. Endymion |Benjamin Disraeli 

Smithson Junior (as the homily ends and the real business is about to start): "Please, sir, is it sterilized?" Mr. Punch's History of Modern England Vol. IV of IV. |Charles L. Graves