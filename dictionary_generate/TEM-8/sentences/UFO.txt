At that point, a sphere lit up, resembling the landing of the UFO in E.T., and the overheard lights descended on the stage. I'm Not Country or Pop. I'm Just Pure Garth Brooks. |David Masciotra |September 10, 2014 |DAILY BEAST 

Long speculated to be a UFO research center, Burlington has been the subject of rampant conspiracies for decades. Britain’s Nuke-Proof Underground City |Nina Strochlic |June 26, 2014 |DAILY BEAST 

There is a lot of elaborate make-up and costuming, and then a UFO arrives. Punks, UFOs, and Heroin: How ‘Liquid Sky’ Became a Cult Movie |Daniel Genis |June 2, 2014 |DAILY BEAST 

Ryder recently disclosed that he had seen a  giant UFO hovering 50 feet above him. Bez To Stand For Parliament |Tom Sykes |March 15, 2014 |DAILY BEAST 

I must have done a double-take, like the Navy pilot who sees a UFO outside his cockpit. A History of American Fun |Stefan Beck |February 9, 2014 |DAILY BEAST 

Persons wishing to report UFO sightings should be advised to contact local law enforcement agencies. USAF Fact Sheet 95-03 |United States Air Force