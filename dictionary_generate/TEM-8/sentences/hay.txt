We grew up bailing hay and chasing cows around and riding four wheelers and digging holes into the ground. Jake Paul’s Going to Save Combat Sports? Bank On It |Eugene Robinson |June 2, 2021 |Ozy 

Other politicians make hay out of their opposition to mandated mask-wearing, using those mandates — themselves a function of the density of people who otherwise refuse to wear one — as a way to trumpet their liberty bona fides. The gas shortage is another recent example of Americans making things worse for one another |Philip Bump |May 12, 2021 |Washington Post 

Some SSPs will struggle if they’re unable to afford proposed commercial terms and have to sit back and do nothing while their competitors make hay. BT is the latest advertiser to take a ‘fixed fee’ programmatic path |Seb Joseph |April 29, 2021 |Digiday 

It was refreshing but not perfumy, with notes of hay and earth that I’d never smelled in a shower product. This Face Wash Made Me Rethink My Skin-Care Routine |Luke Whelan |April 21, 2021 |Outside Online 

Yet it feels like the bulk of the hay is in the barn when it comes to the at-large field. NCAA tournament bracketology: Potential bid snatchers to watch on Friday |Patrick Stevens |March 12, 2021 |Washington Post 

But I sent him some hay and some information and he turned it around. Amy Sedaris Is Hollywood's Beloved Rabbit-Loving Comedian Crafter |Kevin Fallon |August 28, 2014 |DAILY BEAST 

The less scrupulous have made financial hay out of a diagnosis that promises easy access to stimulants. Adult Women Are the New Face of ADHD |Rae Jacobson |June 30, 2014 |DAILY BEAST 

“I think this is part of the inside game that junkies make hay out of,” he said. Prosecutors Allege ‘Criminal Scheme’ Involving Gov. Scott Walker |David Freedlander |June 20, 2014 |DAILY BEAST 

After that, who knows how many innocent straws of hay will start to look like needles under the gaze of unseen algorithms. The NSA Can ‘Collect-it-All,’ But What Will It Do With Our Data Next? |Joshua Kopstein |May 16, 2014 |DAILY BEAST 

The straw and hay piled around the tent only exacerbated the situation. Thrills and Too Many Spills: The Dangers of the Circus |Marina Watts |May 5, 2014 |DAILY BEAST 

The grass had a delightful fragrance, like new-mown hay, and was neatly wound around the tunnel, like the inside of a bird's-nest. Davy and The Goblin |Charles E. Carryl 

The challenge was accepted and the hay-wagon driven round and the trial commenced. The Book of Anecdotes and Budget of Fun; |Various 

As she left the wood she saw a big hay-stack, as firm and shapely of outline as a house, not a loose wisp anywhere. Ancestors |Gertrude Atherton 

Some of the half-made hay in the meadows looks as though it had been standing out to bleach for the last fortnight. Glances at Europe |Horace Greeley 

Mr. Rushmere had been called away to the town on business, and the lovers had been working all day in the hay-field. The World Before Them |Susanna Moodie