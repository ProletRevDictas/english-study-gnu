He offers to drop the price for the uranium from €100 million to €20 million in slangy, uneducated Russian. A New Nuclear Scare Rocks Eastern Europe |Owen Matthews, Anna Nemtsova |June 30, 2011 |DAILY BEAST 

The subjects themselves seem to call forth a cheap, slangy vocabulary and the vulgar phrases of sporting life. English: Composition and Literature |W. F. (William Franklin) Webster 

Bill, to use a terse but slangy term, proceeded to go up in the air. Radio Boys Loyalty |Wayne Whipple 

The language they use is not only ungrammatical but oftentimes both slangy and profane. The Mother and Her Child |William S. Sadler 

Akin to this is fashionably slangy conversation concerning the latest thing in books, magazine articles, trivial plays. A Girl's Student Days and After |Jeannette Marks 

She was direct of speech, frank, and often slangy when slang best expressed her meaning. The Boss of Wind River |David Goodger (goodger@python.org)