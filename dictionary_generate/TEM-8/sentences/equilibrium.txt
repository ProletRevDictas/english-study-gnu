Go back to the basic ground-school diagram of the four forces—lift, weight, thrust and drag—that must be in equilibrium. The science behind how an aircraft glides |By Peter Garrison/Flying Mag |September 3, 2020 |Popular-Science 

As the beads cooled, they weren’t in thermal equilibrium, meaning their locations in the potential energy landscape weren’t distributed in a manner that would allow a single temperature to describe them. A new experiment hints at how hot water can freeze faster than cold |Emily Conover |August 7, 2020 |Science News 

The first involves a basic blueprint strategy for the whole game, allowing it to reach a much faster equilibrium than its predecessor. The Deck Is Not Rigged: Poker and the Limits of AI |Maria Konnikova |August 7, 2020 |Singularity Hub 

They believe that markets work best when supply and demand are allowed to find a natural equilibrium, with price acting as the referee. Why Rent Control Doesn’t Work (Ep. 373 Rebroadcast) |Stephen J. Dubner |March 12, 2020 |Freakonomics 

So this sort of puts the onus on policymakers and funding agencies, and a sense of saying we need to change the equilibrium. Policymaking Is Not a Science (Yet) (Ep. 405) |Stephen J. Dubner |February 13, 2020 |Freakonomics 

A tense, dynamic equilibrium between the U.S. and China seems more likely than a clear displacement of the former by the latter. Why China Won’t Eclipse the United States |Ali Wyne |June 12, 2014 |DAILY BEAST 

But how many of us, thus sunk in despair, have not been vaulted back to equilibrium by another look at Groundhog Day? Harold Ramis’s ‘Groundhog Day’ Is About as Perfect as a Movie Gets |Malcolm Jones |February 25, 2014 |DAILY BEAST 

Since 1989, this arrangement has provided a workable degree of stability, but one based on an equilibrium of unstable elements. Is Syria Being 'Lebanized' or is Lebanon Being 'Syrianized'? |Hussein Ibish |August 29, 2013 |DAILY BEAST 

Lebanese politics for more than a decade have been characterized by an equilibrium of unstable elements. Is Syria Being 'Lebanized' or is Lebanon Being 'Syrianized'? |Hussein Ibish |August 29, 2013 |DAILY BEAST 

But by 2009, Aaron seemed to her to be regaining his equilibrium. Aaron Hernandez’s Terrifying Past |Michael Daly |July 22, 2013 |DAILY BEAST 

Industrial society is therefore mobile, elastic, standing at any moment in a temporary and unstable equilibrium. The Unsolved Riddle of Social Justice |Stephen Leacock 

But the balanced forces once displaced would be seen constantly to come to an equilibrium at a new point. The Unsolved Riddle of Social Justice |Stephen Leacock 

It is a very simple plan, and will be perfectly tight; it is by restoring an equilibrium on both sides of the piston. Life of Richard Trevithick, Volume II (of 2) |Francis Trevithick 

Michael, for sudden joy and excitement, was wellnigh thrown from his equilibrium. Blackwood's Edinburgh Magazine, No. CCCXXXIX. January, 1844. Vol. LV. |Various 

The equilibrium valve is unchanged, except that the rack is taken out and a link put in. Life of Richard Trevithick, Volume II (of 2) |Francis Trevithick