Punch gathered himself together and eyed the house with disfavour. Kipling Stories and Poems Every Child Should Know, Book II |Rudyard Kipling 

The Evangelical movement had done good even in quarters where it had been looked upon with disfavour. The English Church in the Eighteenth Century |Charles J. Abbey and John H. Overton 

Whatever the motive, however, rate-aided emigration remained in disfavour. English Poor Law Policy |Sidney Webb 

The disfavour with which, as we have noted, the Central Authority regarded apprenticeship, seems to have continued. English Poor Law Policy |Sidney Webb 

His reading of those prayers was interrupted by forced coughs and sneezings and other manifestations of disfavour. Letters of Lt.-Col. George Brenton Laurie |George Brenton Laurie