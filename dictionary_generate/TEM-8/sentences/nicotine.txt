While e-cigarettes come in many forms, they typically include a tiny battery and a cartridge of liquid nicotine. How Juul found success: Innovation and addiction |James Hirsch |July 23, 2021 |Washington Post 

E-cigarettes deliver nicotine but are generally considered less dangerous than traditional cigarettes, making them a potentially useful tool for adults trying to stop smoking. Juul Is Paying $40 Million to Rebuild Its Reputation. Will It Work? |Jamie Ducharme |June 29, 2021 |Time 

These give them a smaller dose of nicotine that might help make quitting for good a little easier. Scientists Say: Nicotine |Bethany Brookshire |June 7, 2021 |Science News For Students 

They claim Juul purposely designed its stylish, flash-drive-like devices and flavored nicotine e-liquids to appeal to teenagers. How Juul Got Vaporized |Jamie Ducharme |May 17, 2021 |Time 

From surviving to thriving as a hardware startupThe other challenge is how to lower nicotine content. Juul inventor’s Myst lands funding as institutional investors turn to China’s e-cigs |Rita Liao |May 7, 2021 |TechCrunch 

Ground glass is put in food to cause internal bleeding, and nicotine concentrated by boiling can cause a heart attack. A Million Ways to Die in Prison |Daniel Genis |December 8, 2014 |DAILY BEAST 

All of the employees were constantly blowing out steam, some of it without even nicotine in it, let alone THC. This Is Your E-Cigarette on Drugs |Daniel Genis |July 28, 2014 |DAILY BEAST 

A man named Herbert Gilbert patented one back in 1963 that heated a nicotine solution and produced steam. This Is Your E-Cigarette on Drugs |Daniel Genis |July 28, 2014 |DAILY BEAST 

If you plan to take in vapor in such amounts, you have to get juice with a low nicotine content to avoid poisoning yourself. This Is Your E-Cigarette on Drugs |Daniel Genis |July 28, 2014 |DAILY BEAST 

Some get juice without nicotine at all, just to enjoy the process esthetically. This Is Your E-Cigarette on Drugs |Daniel Genis |July 28, 2014 |DAILY BEAST 

The tobacco plant, as is well known, produces a virulent poison known as Nicotine. Tobacco; Its History, Varieties, Culture, Manufacture and Commerce |E. R. Billings. 

Chemistry has taught us that nicotine is only one among many principles which are contained in the plant. Tobacco; Its History, Varieties, Culture, Manufacture and Commerce |E. R. Billings. 

Now, the strength of tobacco comes from its nicotine, and if the specimens I sent contain no nicotine, whence the strength? Tobacco; Its History, Varieties, Culture, Manufacture and Commerce |E. R. Billings. 

I perceive that the amount of nicotine in a great measure depends on the extent to which the leaf is allowed to ripen. Tobacco; Its History, Varieties, Culture, Manufacture and Commerce |E. R. Billings. 

It is a dark maroon-colored leaf, and contains a large proportion of the nicotine oil. Tobacco; Its History, Varieties, Culture, Manufacture and Commerce |E. R. Billings.