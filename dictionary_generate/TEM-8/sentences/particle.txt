The butter coating the particles of flour prevents clumps from forming, compared with adding flour by itself. Sauce, gravy or stew too thin? We’ve got 3 ways to fix that. |Aaron Hutcherson |February 12, 2021 |Washington Post 

When just the receiving manikin wore the double mask, it was protected from 83 percent of particles. Making masks fit better can reduce coronavirus exposure by 96 percent |Tina Hesman Saey |February 12, 2021 |Science News 

While it’s clear that nowhere is safe from this cloud of microplastics, research into the human health impacts of the particles has been relatively scant. Microplastics are everywhere. Here’s what that means for our health. |Ula Chrobak |February 11, 2021 |Popular-Science 

Philadelphia had the lowest average particle concentration of any city, at 112 micrograms per cubic meter. Air pollution in US subway stations is disturbingly high |Kate Baggaley |February 11, 2021 |Popular-Science 

France has mandated its citizens wear masks that block more than 90 percent of airborne particles in public places. Masks should fit better or be doubled up to protect against coronavirus variants, CDC says |Lena H. Sun, Fenit Nirappil |February 11, 2021 |Washington Post 

Indeed, many cutting-edge physicians are manipulating the diet to lower particle number. The AHA’s Absurd Saturated Fat Obsession |Dr. Barbara H. Roberts |June 3, 2014 |DAILY BEAST 

When the Higgs particle was discovered, everywhere I went I heard people wondering about its significance. The New 'Cosmos' Reboot Marks a Promising New Era for Science |Lawrence M. Krauss |March 10, 2014 |DAILY BEAST 

After episodes of not-so-subtly mentioning the particle accelerator at S.T.A.R Labs, we finally get what we want. Arrow ‘Three Ghosts’ Recap: Here Comes The Flash! |Chancellor Agard |December 12, 2013 |DAILY BEAST 

After eight episodes of not-so-subtly mentioning the particle accelerator at S.T.A.R Labs, we finally get what we want. Arrow ‘Three Ghosts’ Recap: Here Comes The Flash! |Chancellor Agard |December 12, 2013 |DAILY BEAST 

For 50 years, scientists had predicted the existence of the particle we now know as Higgs boson, which gives mass to matter. Give It Up for the Other Nobel Prize Winners |Nina Strochlic |October 12, 2013 |DAILY BEAST 

This, as a piece of pure economics, does not interest the individual employer a particle. The Unsolved Riddle of Social Justice |Stephen Leacock 

I can tell you, my dear idealist—you have not changed a particle, by the way—that there is another side you have never seen. Ancestors |Gertrude Atherton 

Let us conceive a particle of air situated immediately over the earth's polar axis. Outlines of the Earth's History |Nathaniel Southgate Shaler 

Then let us imagine the particle moving toward the equator with the speed of an ordinary wind. Outlines of the Earth's History |Nathaniel Southgate Shaler 

It was a hair-raising problem, too, and called for every ounce of nerve and every particle of skill the boy possessed. Motor Matt's "Century" Run |Stanley R. Matthews