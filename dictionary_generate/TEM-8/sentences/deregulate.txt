With deregulated business and privatized public services and natural resources, the system helped make Chile a haven for foreign investors and one of the richest countries in South America. The Leftist Millennial Who Could Lead One of Latin America's Wealthiest and Most Unequal Countries |Ciara Nugent |November 19, 2021 |Time 

On the deregulated, idiosyncratic Texas power market, REPs generally purchase wholesale electricity from power generators and sell it to customers. Tesla wants to sell electricity in Texas |Aria Alamalhodaei |August 27, 2021 |TechCrunch 

This is the gamble at the heart of a deregulated electricity market, in which companies compete to produce and sell electricity, as opposed to a monopoly system with rates fixed by regulators. Who should pay to fix the electric grid? |Tim McDonnell |February 26, 2021 |Quartz 

Many energy experts say the very nature of the state’s deregulated electric market is perhaps most to blame for last week’s power crisis. “Power Companies Get Exactly What They Want”: How Texas Repeatedly Failed to Protect Its Power Grid Against Extreme Weather |by Jeremy Schwartz, Kiah Collier and Vianna Davila |February 22, 2021 |ProPublica 

In 1999, Texas joined more than a dozen other states in “deregulating” its electricity system, meaning that it switched from a monopoly power provider to a marketplace of competing power plants and retail utilities. Is this the end of Texas’s Wild West electricity market? |Tim McDonnell |February 20, 2021 |Quartz 

This former Goldman Sachs partner has frustrated many progressives by working hard to deregulate Wall Street. Congress Can Be Corrupt Without Corrupt People |Lawrence Lessig |December 13, 2013 |DAILY BEAST 

Their Reaganite Republican adversaries, by contrast, want to deregulate it radically. The Rise of the New New Left |Peter Beinart |September 12, 2013 |DAILY BEAST 

One, deregulate everything so that banks can start placing bets agaist their own securities. Waste, Fraud, and Abuse: The Eric Cantor Story |Michael Tomasky |June 21, 2013 |DAILY BEAST 

The former press secretary criticized GOP plans to increase tax cuts for the wealthy and deregulate banks and Wall Street. Sunday Talk’s 7 Best Moments: Mitch McConnell & More (Video) |The Daily Beast Video |July 8, 2012 |DAILY BEAST 

Should we fix the ratings agencies by regulating them more stringently, or do we need to deregulate and increase competition? Slow Down, Obama! |Matthew Yglesias |April 26, 2010 |DAILY BEAST 

We seek to fully deregulate natural gas to bring on new supplies and bring us closer to energy independence. Complete State of the Union Addresses from 1790 to 2006 |Various