In some of the balmier spots around the country, you still might be able to enjoy al fresco dining without getting frostbite. When it comes to COVID-19 risk, what counts as ‘outdoor’ dining? |Sara Kiley Watson |January 11, 2021 |Popular-Science 

On a balmy August morning in Emanuel County in eastern Georgia, hundreds of children bounded off freshly cleaned school buses and out of their parents’ cars. Two School Districts Had Different Mask Policies. Only One Had a Teacher on a Ventilator. |by Annie Waldman and Heather Vogell |November 23, 2020 |ProPublica 

The entire industry was benefiting from a balmy climate where relatively few car, credit card, or small-business loans were going into default, a benefit that countered the drag of low rates and brought one of the best runs in banking history. How JPMorgan Chase is proceeding with extreme caution—and still making plenty of money |Shawn Tully |October 14, 2020 |Fortune 

The planet has become a scorched and acidic wasteland today, but researchers suspect it hosted balmy oceans for billions of years in its early history—roughly ten times longer than Mars did. Three ways scientists could search for life on Venus |Charlie Wood |October 1, 2020 |Popular-Science 

High carbon dioxide in the atmosphere alone wouldn’t have been enough to keep the region balmy so close to the pole. A rainforest once grew near the South Pole |Carolyn Gramling |May 11, 2020 |Science News For Students 

I got into my car and just sat there in the balmy London night replaying the events of the day in my mind. When Gary Wright Met George Harrison: Dream Weaver, John and Yoko, and More |Gary Wright |September 29, 2014 |DAILY BEAST 

On a New York terrace, there is a dinner dance one balmy summer evening. Adam Hochschild on Keeping Company With His Dying Father |Adam Hochschild |June 14, 2014 |DAILY BEAST 

During a balmy summer, few things provide immediate enjoyment like a chilled glass of rosé. Summer in a Glass: Everything’s Coming Up Rosés |Jordan Salcito |June 7, 2014 |DAILY BEAST 

The surprisingly boring movie Contagion, has arrived, signaling the end of balmy youth for the field of infectious diseases. The Distorted Science of Contagion |Kent Sepkowitz |September 23, 2011 |DAILY BEAST 

Eleanor Roosevelt addressed the record crowd on a balmy Southern California afternoon. 'Tricky Dick' vs. the Pink Lady |Sally Denton |November 16, 2009 |DAILY BEAST 

The relation existing between the balmy plant and the commerce of the world is of the strongest kind. Tobacco; Its History, Varieties, Culture, Manufacture and Commerce |E. R. Billings. 

It seems hardly possible to draw a more graphic picture of the blessings diffused by the balmy plant, than that just given. Tobacco; Its History, Varieties, Culture, Manufacture and Commerce |E. R. Billings. 

It opened most propitiously and was one of those soft, balmy September days, more like early June than autumn. The Cromptons |Mary J. Holmes 

And it is not a bitter potion, such as Alfred ordered; no, it is balmy with the scent of wild flowers. Child Life In Town And Country |Anatole France 

Everything would be favorable; it was balmy and beautiful spring weather now, and Nature was all tailored out in her new clothes. A Connecticut Yankee in King Arthur's Court, Complete |Mark Twain (Samuel Clemens)