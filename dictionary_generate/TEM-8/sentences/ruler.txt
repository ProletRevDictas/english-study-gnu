It was a way to show that they were the proper kind of gentlemen to be your rulers. Not all presidents’ dance skills are created equal |Bonnie Berkowitz, Joanne Lee |January 21, 2021 |Washington Post 

It may be surprising just how many options you have when it comes to the simple ruler. The best rulers for home, office, and school |PopSci Commerce Team |January 8, 2021 |Popular-Science 

Whether you need a basic set for school or a more advanced set for professional designing - it is clear that rulers are an important tool that you don’t outgrow. The best rulers for home, office, and school |PopSci Commerce Team |January 8, 2021 |Popular-Science 

Trotsky contended that Stalin as ruler was simply a spokesman for the oppressive state bureaucracy. Did Stalin’s rise to power foretell the butchery that came next? |Robert Service |October 30, 2020 |Washington Post 

It features two ribbon placeholders, an expandable pocket, and a sturdy sewn-in pen holder and comes with a flat, 15-centimeter ruler and colorful tabs to bookmark important lists, notes, and more. These notebooks are excellent gifts |PopSci Commerce Team |September 28, 2020 |Popular-Science 

How will we know if Kim, when he eventually surfaces, is ruler or figurehead? Kim Jong Un: Erased? |Gordon G. Chang |October 10, 2014 |DAILY BEAST 

He wanted peace and harmony, and in this respect he was just another Roman ruler interested in imperial unity. Plotting Nicea III Could Be Pope Francis's Masterstroke |Candida Moss |June 8, 2014 |DAILY BEAST 

Anyone “who has authority of the sword, is the ruler of a city, or wears the purple” has to resign his job or leave. St. Hippolytus’ Careers Christians Should Never Have |Candida Moss |May 4, 2014 |DAILY BEAST 

The Bible makes this out to be a matter of popular acclaim, the people lining up to have David as their ruler. The King David You Never Knew |Joel Baden |October 27, 2013 |DAILY BEAST 

Like a paranoid ruler, it only trusts what it can directly control. Obamacare’s Rollout Is a Disaster That Didn’t Have to Happen |Gregory Ferenstein |October 20, 2013 |DAILY BEAST 

Renounce the good law of the worshippers of Mazda, and thou shalt gain such a boon as the Murderer gained, the ruler of nations. Solomon and Solomonic Literature |Moncure Daniel Conway 

Murat was in no hurry to commence his reign, and his subjects showed no great anxiety to see their new ruler. Napoleon's Marshals |R. P. Dunn-Pattison 

The deposed ruler plotted and planned all kinds of schemes whereby he might be restored to his old position of authority. Our Little Korean Cousin |H. Lee M. Pike 

“Confound it, no;” rejoined Mr. Simmery, stopping for an instant to smash a fly with the ruler. The Posthumous Papers of the Pickwick Club, v. 2(of 2) |Charles Dickens 

Gascony has ever been the mother of ambitious men, and many a ruler has she supplied to France. Napoleon's Marshals |R. P. Dunn-Pattison