He later led the US Geological Survey’s Volcano Hazards Program, before returning to UAF in 2012 to become dean of the graduate school. Drilling molten rock to learn its secrets |Katie McLean |February 24, 2021 |MIT Technology Review 

Eastman parted ways with Chapman University, where he served as dean of the law school, after speaking at the rally. Ginni Thomas apologizes to husband’s Supreme Court clerks after Capitol riot fallout |Robert Barnes |February 2, 2021 |Washington Post 

It’s still unclear if that’s the case, however, and clinical trials that could help figure that out are still ongoing, Dean says. What do COVID-19 vaccines mean for daily life in the months ahead? |Erin Garcia de Jesus |January 29, 2021 |Science News 

Thomas Rashad Easley, assistant dean of community and inclusion at the Yale School of Forestry and Environmental Studies, knows how narrow the routes in can be. How Environmentalism Can Center Racial Justice in 2021 |Heather Hansman |January 29, 2021 |Outside Online 

From their car, Silveti and Dean watched as the Soviets waved the truck into the station, unchecked. Lunik: Inside the CIA’s audacious plot to steal a Soviet satellite |Bobbie Johnson |January 28, 2021 |MIT Technology Review 

(It's worth noting that Dean himself has already endorsed Clinton). Elizabeth Warren 2016 Gets First Check From Liberals |Ben Jacobs |December 17, 2014 |DAILY BEAST 

Dean Todd arranged for me to sit behind a screen and talk about my rape for a group of student leaders and activists. I Was Gang Raped at a UVA Frat 30 Years Ago, and No One Did Anything |Liz Seccuro |December 16, 2014 |DAILY BEAST 

Dean Sybil Todd passed away from pancreatic cancer before she could testify. I Was Gang Raped at a UVA Frat 30 Years Ago, and No One Did Anything |Liz Seccuro |December 16, 2014 |DAILY BEAST 

Dean Todd remained my friend until I graduated in 1988, with my degree in English literature. I Was Gang Raped at a UVA Frat 30 Years Ago, and No One Did Anything |Liz Seccuro |December 16, 2014 |DAILY BEAST 

Canevari passed me off to Dean Sybil Todd, who accompanied me to the University Police Department. I Was Gang Raped at a UVA Frat 30 Years Ago, and No One Did Anything |Liz Seccuro |December 16, 2014 |DAILY BEAST 

Dean Swift was indeed a misanthrope by theory, however he may have made exception to private life. Gulliver's Travels |Jonathan Swift 

Several uneducated business men are said to have written to the Dean asking the Latin for what they think of the new Budget. Punch, or the London Charivari, Volume 158, April 28, 1920 |Various 

Away they hastened; and, being admitted to the Dean's study, stated the occasion of their visit. Tobacco; Its History, Varieties, Culture, Manufacture and Commerce |E. R. Billings. 

To all of this Mr Dean listened in perfect silence, patiently, and with a smile of universal benevolence. The Garret and the Garden |R.M. Ballantyne 

You see, up to that time he had thought himself rather a knowing fellow; but Mr Dean managed to remove the scales from his eyes. The Garret and the Garden |R.M. Ballantyne