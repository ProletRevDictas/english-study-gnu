The more salacious and outrageous the content, the more people want to watch, in spite of themselves. How a Democratic plan to reform Section 230 could backfire |Bobbie Johnson |February 8, 2021 |MIT Technology Review 

So outrageous are the claims in the Secret History that many historians have disregarded the text as pure political satire, intended for the pleasure and humor of an educated elite circle. What the QAnon of the 6th Century Teaches Us About Conspiracies |Roland Betancourt |February 3, 2021 |Time 

I’d heard that the wildlife sightings would be pretty outrageous amid this 70,448-acre expanse of rugged badlands cut through by the Little Missouri River. Seeing Big Vistas at Theodore Roosevelt National Park |Emily Pennington |January 21, 2021 |Outside Online 

That wisdom applies to America’s big-caps, and nothing illustrates their vulnerability better than Tesla’s outrageous run. Tesla is the proxy for a stock market gone mad |Shawn Tully |January 21, 2021 |Fortune 

This kind of hard luck is outrageous, even for them, but it won’t stop the NFL train. The Cleveland Browns were an NFL feel-good story. Then the coronavirus got jealous. |Jerry Brewer |January 6, 2021 |Washington Post 

None of this, however, is what makes Confessions so outrageous. An Ivy League Frat Boy’s Shallow Repentance |Stefan Beck |November 24, 2014 |DAILY BEAST 

As noted by Judge Martha Craig Daughtrey in dissent, this is an outrageous position. All The Wrong Reasons to Ban Gay Unions |Jay Michaelson |November 7, 2014 |DAILY BEAST 

They also passed an outrageous Farm Bill that subsidizes rich farmers and keeps domestic prices artificially high. Assuming GOP Does Take the Senate, Dems Have Nothing to Fear |Veronique de Rugy |November 1, 2014 |DAILY BEAST 

In their incongruity and outrageous character, they were more and more effective. Joseph Campbell on the Roots of Halloween |Joseph Campbell |October 31, 2014 |DAILY BEAST 

Campaigns often exchange outrageous attacks but to do so in the guise of a government mailer is quite unusual. Grimes Campaign “Exploring Legal Options” Against McConnell |Ben Jacobs |October 31, 2014 |DAILY BEAST 

Rushing into an opposite extreme, the most outrageous receptacles for the precious dust were devised. Tobacco; Its History, Varieties, Culture, Manufacture and Commerce |E. R. Billings. 

The Lords justices read this outrageous note with indignation, and sent it with all speed to Loo. The History of England from the Accession of James II. |Thomas Babington Macaulay 

Wrongs less wanton and outrageous precipitated the French Revolution. Portrait and Biography of Parson Brownlow, The Tennessee Patriot |William Gannaway Brownlow 

It soon appeared, indeed, that the list of outrageous offenders against the laws decreased throughout the country. The History of England in Three Volumes, Vol.III. |E. Farr and E. H. Nolan 

These travelled follows are outrageous bores, with their bushy moustachios and outlandish lingo. Alone |Marion Harland