Ahead of Father’s Day on Sunday, we dig into the DNA of dads at a time of daddy disruption. Discover the Science of Fatherhood |Sean Culligan |June 18, 2021 |Ozy 

My daddy makes movies for work, and we made a movie together. 'A Year Full of Emotions.' What Kids Learned From the COVID-19 Pandemic |Allison Singer |June 12, 2021 |Time 

My daddy was a teacher too, so of course I just learned from seeing him, but he was real serious about music. Let’s Talk About Wynton Marsalis |Eugene Robinson |May 18, 2021 |Ozy 

It was just Southern life, and my daddy’s trying to play jazz, nobody really want to hear that. Let’s Talk About Wynton Marsalis |Eugene Robinson |May 18, 2021 |Ozy 

So, I more learned to be serious about it, because I like to joke around a lot with my daddy. Let’s Talk About Wynton Marsalis |Eugene Robinson |May 18, 2021 |Ozy 

Between 25 and 30, you’re trying to decide how much longer before you start growing a beard and calling yourself ‘Daddy. Freaking Out About Age Gaps in Gay Relationships Is Homophobic |Samantha Allen |January 9, 2015 |DAILY BEAST 

A recent U.S. study found men get a “daddy bonus” —employers seem to like men who have children and their salaries show it. How Good Dads Can Change the World |Gary Barker, PhD, Michael Kaufman |January 6, 2015 |DAILY BEAST 

Of course, Kim Jong-Un takes an image hit as a Katy Perry-obsessed, margarita-drinking maniac with daddy issues. I Was Honeydicked Into Spending Christmas with ‘The Interview’ |Allison McNearney |December 26, 2014 |DAILY BEAST 

Too many people reverted to a childlike state, and they wanted a daddy-protector. The U.S. Will Torture Again—and We’re All to Blame |Michael Tomasky |December 12, 2014 |DAILY BEAST 

Alexis did not know to act differently than she might for any other picture taken of her by her daddy. ‘I Can’t Breathe!’ ‘I Can’t Breathe!’ A Moral Indictment of Cop Culture |Michael Daly |December 4, 2014 |DAILY BEAST 

I hoped Daddy would get my letter and come and take charge of the search himself. The Campfire Girls of Roselawn |Margaret Penrose 

“I am interested in her particularly because Daddy Norwood needs her,” admitted the older girl. The Campfire Girls of Roselawn |Margaret Penrose 

I dare say Daddy has heaps of extra things on his hands because of all the time he spent gadding with us in Europe. Those Dale Girls |Frank Weston Carruth 

Still no sign of daddy, think must be dead or stolen though nobody to steal same in country. Cabin Fever |B. M. Bower 

We have roamed around with Daddy too much to be quite like pattern society girls. Those Dale Girls |Frank Weston Carruth