In the winter of 1987, Lee and her family settled in Toronto. Hong Kong is about to see a Tiananmen-era wave of migration |Karen Ho |August 16, 2020 |Quartz 

This means one of the group’s other functions has become helping newcomers effectively lead a protest if they don’t know how, Lee said. The Art of Protest Promotion |Bella Ross |June 22, 2020 |Voice of San Diego 

By around 900, Mongolian women fought in wars and held political power, Lee says. Skeletons hint that ancient societies had women warriors |Bruce Bower |May 28, 2020 |Science News For Students 

So Lee has helped develop a new type of microneedle — one that can barely be felt. Micro-barbs could make shots less painful |Stephen Ornes |May 13, 2020 |Science News For Students 

So make a pact with your friends to put your screens away during certain times of the day, Lee suggests. How to cope as COVID-19 imposes social distancing |Sheila Mulrooney Eldred |March 23, 2020 |Science News For Students 

Lee and Coogan did briefly meet with the pope, with pictures to prove it, but no one at the Vatican officially screened the film. Pope Francis Has the Pleasure of Meeting Angelina Jolie for a Few Seconds |Barbie Latza Nadeau |January 8, 2015 |DAILY BEAST 

Twelve-year-old dance prodigy Maddie Ziegler has suffered the wrath of Dance Moms tyrant Abby Lee Miller. See Burly Shia LaBeouf Interpretive Cage Fight Lil Sia in the Singer’s Fantastic New Music Video |Marlow Stern |January 7, 2015 |DAILY BEAST 

What stuck in my mind were the two supporting actors, Gloria Grahame and Lee Marvin. The Story Behind Lee Marvin’s Liberty Valance Smile |Robert Ward |January 3, 2015 |DAILY BEAST 

In my opinion Lee was one of the greatest actors of all time. The Story Behind Lee Marvin’s Liberty Valance Smile |Robert Ward |January 3, 2015 |DAILY BEAST 

Little did I know that Lee had actually been born into a wealthy family. The Story Behind Lee Marvin’s Liberty Valance Smile |Robert Ward |January 3, 2015 |DAILY BEAST 

It was seen just in time to put the helm a-lee, or we should have run upon it. Narrative of a Survey of the Intertropical and Western Coasts of Australia] [Volume 2 of 2] |Phillip Parker King 

Lee's army is sweeping victoriously through Maryland; Harper's Ferry taken with ten thousand prisoners. The Courier of the Ozarks |Byron A. Dunn 

Afterward, when the news came that Lee had succeeded in getting his army safely across the Potomac, Mr. Middleton's hopes revived. The Courier of the Ozarks |Byron A. Dunn 

Consequently, the official voting was postponed for three weeks, but Lee's resolution was adopted by the Congress on July 2, 1776. Hallowed Heritage: The Life of Virginia |Dorothy M. Torpey 

Lee realized the wonderful honor for which he had been selected and was deeply appreciative. Hallowed Heritage: The Life of Virginia |Dorothy M. Torpey