In its own ironic way, Halloween has always offered a brief respite from real-world horrors. Stephen King on how to properly adapt his books and which project went ‘entirely off the rails’ |Travis Andrews |October 30, 2020 |Washington Post 

That’s quite ironic coming from the head of a company whose products are used in operations that contribute to serious human rights violations. As Palantir goes public, consider its troubling human rights record |jakemeth |September 30, 2020 |Fortune 

The collection, which includes 15 pieces, reflects Abloh’s signature “ironic” style. Ikea promises ‘democratic’ design. Has its Virgil Abloh collaboration lived up? |claychandler |August 25, 2020 |Fortune 

It is ironic that the integrity of the second law of thermodynamics, which is responsible for irreversibility in the world, is safeguarded at the micro level by this reversibility. Why Our Perpetual Energy Puzzle Fails |Pradeep Mutalik |May 22, 2020 |Quanta Magazine 

The title may sound ironic, but it wasn’t intended that way. Reasons to Be Cheerful (Ep. 417) |Stephen J. Dubner |May 7, 2020 |Freakonomics 

So it was ironic a couple of months later when the Tea Partiers were railing against it—it had already expired. To GOP Congress, as Usual, It’s Welfare on the Chopping Block |Monica Potts |December 25, 2014 |DAILY BEAST 

Ironic, since it was originally meant to suppress sugar lust. One Cake to Rule Them All: How Stollen Stole Our Hearts |Molly Hannon |December 24, 2014 |DAILY BEAST 

How ironic that the Hermit Kingdom is taking the blame for our first real look inside a clique that not even Vice dares penetrate. Pyongyang Shuffle: Hollywood In Dead Panic Over Sony Hack |James Poulos |December 19, 2014 |DAILY BEAST 

How ironic and unfortunate that the critics tend to focus on one “bad” class or the other. Walmart Lifts Black Friday’s Curse |James Poulos |November 26, 2014 |DAILY BEAST 

To them, they were being ironic and funny, and proving how ‘liberal’ they were. ‘Dear White People’: How An Ex-Publicist’s Twitter Became One of the Year’s Most Important Films |Marlow Stern |October 30, 2014 |DAILY BEAST 

So this was the curious and ironic fact; the nation had been saved—but only to be handed over to the money-changers! Love's Pilgrimage |Upton Sinclair 

As the Tassos reached for him, a last ironic thought drifted through Hendricks mind. Second Variety |Philip Kindred Dick 

Gray, silent in the shadows, laughed a bitter, ironic laugh. A World is Born |Leigh Douglass Brackett 

There was a suggestion of the ludicrous, a faint ironic aroma, in the phrase, which aroused angry passions. The Open Question |Elizabeth Robins 

My friend S— the elder passed me on the other side of the street with a wave of the hand and an ironic smile. 'Twixt Land &amp; Sea |Joseph Conrad