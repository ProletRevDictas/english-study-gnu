When I walk through a property with clients who are selling their home, I always recommend paying close attention to light fixtures in the entryway, dining room, pendant lighting in the kitchen, and bathrooms. Tips to properly prep your home to sell for the most |Justin Noble |June 24, 2021 |Washington Blade 

The purse contained a set of keys that had an El Salvador keychain, a blue turtle, a pendant spelling out the word “Love” and what appears to be a glitter-covered canister of protective spray. Partly dismembered body found by hikers in Maryland: ‘This person has a story to tell’ |Dan Morse |June 16, 2021 |Washington Post 

Appropriately enough, “Sybille Bedford” makes just the right pendant to Selina Hastings’s excellent earlier biographies of Somerset Maugham and Evelyn Waugh. ‘Sybille Bedford’ is a gossipy appreciation of an oft-overlooked literary great |Michael Dirda |February 17, 2021 |Washington Post 

His parents even accessorize him in a mini diamond-encrusted watch and a tiny chain with a pendant that reads “papa bear.” Nicki Minaj Shares The First Photos Of Her Son |Jasmine Grant |January 2, 2021 |Essence.com 

She is having a necklace made of her sister’s fingerprint, a teardrop pendant similar to the one the sisters had made after Kim’s death. ‘I said goodbye to my sister through a computer screen’ |Holly Bailey |January 2, 2021 |Washington Post 

Three years later, another father and son duo found a 500-year-old gold pendant worth $4 million in Essex. How To Strike Viking Gold |Lizzie Crocker |October 18, 2014 |DAILY BEAST 

But the beautiful porcelain pendant that Agatha (Saoirse Ronan) wears. The Look of ‘The Grand Budapest Hotel’ |Andrew Romano |March 7, 2014 |DAILY BEAST 

It was made by society jewelers Garrard, who refashioned the gems from a pendant she was given by her husband, George VI. Kate Dazzles in Vintage Tiara |Tom Sykes |December 4, 2013 |DAILY BEAST 

The pendant light is a flotilla of hot air balloons hand blown in glass. OMG I Want That ...Nursery (Royal Edition) |Tom Sykes |May 9, 2013 |DAILY BEAST 

It had a pendant that looked like a house, which to me signifies warmth and happiness. Susan Cain: How I Write |Noah Charney |May 8, 2013 |DAILY BEAST 

The dew moistened each leaf, or hung in glittering pendant drops upon the thorn of the prickly pears which lined the roads. Newton Forster |Captain Frederick Marryat 

Jimmy sat bolt upright, his black hat pendant between his knees. Blazed Trail Stories |Stewart Edward White 

Histoire de l'Europe pendant la Révolution Française, ii., 58-60. The Political History of England - Vol. X. |William Hunt 

A gold chain, with a number of pendant scaraboei, was found in a tomb in Vulci, transcending anything before seen by him. The Bay State Monthly, Vol. II, No. 6, March, 1885 |Various 

Each bore a slender wand of two triangles of reeds adorned at the corners with pendant plumes. The Mountain Chant, A Navajo Ceremony |Washington Matthews