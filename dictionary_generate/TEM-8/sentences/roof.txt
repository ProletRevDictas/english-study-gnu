They found that asphalt from roofs and roads may contribute as much secondary organic compounds as all the vehicles in the region on an annual basis. City pavement is a big source of air pollution |Ula Chrobak |September 3, 2020 |Popular-Science 

The major horizontal roof beam, oriented east-west, is called “the sun’s path.” An Ancient Site with Human Skulls on Display - Issue 89: The Dark Side |Jo Marchant |September 2, 2020 |Nautilus 

He had a cinder-block wall around the tank, so the heat wasn’t blowing toward his house, but the eaves of the roof caught on fire in the last five minutes before the tank stopped burning. How We Saved Our Neighborhood from a Wildfire |Dain Zaffke |September 2, 2020 |Outside Online 

Creating such “cool roofs” across 80% of the nation’s commercial buildings could cut annual energy use by more than 10 terawatt-hours and save more than $700 million, according to an earlier study by the Lawrence Berkeley National Lab. Air conditioning technology is the great missed opportunity in the fight against climate change |James Temple |September 1, 2020 |MIT Technology Review 

Stupid young people went back to trying to jump into pools from their roofs. Team Reopen: 2, Schools: 0 |Scott Lewis |August 31, 2020 |Voice of San Diego 

Which is why you should: “Clap along, if you feel like a room without a roof.” Forget the Resolutions; Try a Few Declarations |Kevin Bleyer |January 1, 2015 |DAILY BEAST 

We were on her roof talking and trying to come up with ideas, to think of alternatives to renting a studio. #Setinthestreet: Your Street Corner Is Their Art Project |James Joiner |December 24, 2014 |DAILY BEAST 

The two once lived together under the same roof, after Brooke asked Fenner to live with them. Beaten By His Church for Being Gay |Justin Jones |December 16, 2014 |DAILY BEAST 

Some wielding signs hit the roof, windshield, and body of the car I was traveling in. It’s Time to Hold Protesters Accountable |Ron Christie |December 4, 2014 |DAILY BEAST 

Yeah, I mean, as far as Maggie goes, her reducing a church to just “four walls and a roof” says a lot about the character. ‘Walking Dead’ Showrunner Scott Gimple Teases ‘Darker, Weirder’ Times Ahead |Melissa Leon |December 2, 2014 |DAILY BEAST 

The Pontellier and Ratignolle compartments adjoined one another under the same roof. The Awakening and Selected Short Stories |Kate Chopin 

Even as they gazed they saw its roof caught up, and whirled off as if it had been a scroll of paper. The Giant of the North |R.M. Ballantyne 

First the chimneys sank down through the roof, as if they were being lowered into the cellar. Davy and The Goblin |Charles E. Carryl 

The tower has four clock faces, pinnacles at the angles, and a steep slate roof and is 120 feet high. Showell's Dictionary of Birmingham |Thomas T. Harman and Walter Showell 

Then the roof itself, with its gables and dormer windows, softly folded itself flat down upon the top of the house, out of sight. Davy and The Goblin |Charles E. Carryl