We’ve seen the pandemic, the campaign and now, as you and I speak in the aftermath of the election, we’ve seen very clearly how the sinister role of propaganda has affected our national discourse. Dan Rather: Propaganda and injustice are a ‘very potent, toxic mix’ |Joe Heim |November 23, 2020 |Washington Post 

The game’s biggest draw was a storyline which unfolded over multiple play sessions, with diseases mutating and cities falling into chaos as a sinister conspiracy spread its tendrils across the world. One hell of a send-off: Pandemic Legacy: Season 0 wraps a stylish board game series |Ars Staff |November 21, 2020 |Ars Technica 

Some characters buy the gadgets out of simple curiosity or a desire for companionship, while others use the technology for more sinister ends. Everything Our Editors Loved in August |The Editors |September 10, 2020 |Outside Online 

Then, when the host plant flowers, so does the dodder, setting the stage for the sinister cycle to begin again. This parasitic plant eavesdrops on its host to know when to flower |Jonathan Lambert |September 4, 2020 |Science News 

There are plenty of reasons why duopolies exist, and they’re not necessarily all sinister. America’s Hidden Duopoly (Ep. 356 Rebroadcast) |Stephen J. Dubner |September 3, 2020 |Freakonomics 

Some have innocuous-seeming URLs like cardpool.com or giftcardgranny.com, which cloak the sinister operations. The Insane $11 Billion Scam at Retailers’ Return Desks |M.L. Nestel |December 19, 2014 |DAILY BEAST 

“i wanted to talk to him about sinister and jobs,” wrote Pascal. Exclusive: Sony Emails Slam Leonardo DiCaprio, Willow and Jaden Smith, Gush Over Ryan Gosling |William Boot |December 13, 2014 |DAILY BEAST 

The Kremlin likes to portray these as sinister Western conspiracies. Putin’s Health Care Disaster |Anna Nemtsova |November 30, 2014 |DAILY BEAST 

A raft of thrillers, sci-fi movies, and sinister dramas followed. Can Condon's Freak Show Win Broadway? |Tim Teeman |November 18, 2014 |DAILY BEAST 

As sinister and well-resourced as it is, it may be the weakest link in the chain. Egypt’s LGBTs Fight Grindr Crackdown |Jay Michaelson |October 18, 2014 |DAILY BEAST 

Nevertheless, this world of mankind to-day seems to me to be a very sinister and dreadful world. The Salvaging Of Civilisation |H. G. (Herbert George) Wells 

These are obtained easily, whence follow the sinister reports that they give your Majesty, to the harm of the public welfare. The Philippine Islands, 1493-1898, Volume XX, 1621-1624 |Various 

A chill, sinister feeling crept over me, but I kept my gaze fixed steadily in the same direction. Uncanny Tales |Various 

Then he turned suddenly, and Harris saw that his face had turned most oddly and disagreeably sinister. Three More John Silence Stories |Algernon Blackwood 

The countenances round him turned sinister, but not idly, negatively sinister: they grew dark with purpose. Three More John Silence Stories |Algernon Blackwood