Without these data, we cannot even evaluate whether any changes in policing reduces racial inequalities in interactions with police. Why some senior officers are making it harder for police departments to fight racism |matthewheimer |August 26, 2020 |Fortune 

One very useful tool for evaluating air purifiers is this directory of room air cleaners maintained by AHAM. Can an air purifier help protect you from COVID-19? |dzanemorris |August 22, 2020 |Fortune 

The large-scale stage of testing is intended to evaluate the vaccine’s effectiveness and safety. A Chinese company says its vaccine will be ready by December—but it won’t be cheap |Grady McGregor |August 22, 2020 |Fortune 

We are open to looking at and evaluate anything that we think is going to drive long-term shareholder value. Downtrodden GM stock gets a boost from electric-vehicle spinoff speculation |Aaron Pressman |August 18, 2020 |Fortune 

The Chicago-based aerospace giant has been evaluating its workforce as it completes the initial reduction announced earlier this year. Boeing is preparing additional layoffs with new buyouts |Rachel King |August 18, 2020 |Fortune 

In schools, this meant finding new ways to evaluate students—and hence their teachers. Your Local School Doesn’t Have to Suck |Michael S. Roth |December 17, 2014 |DAILY BEAST 

Because these ingredients are so new, we need new methodologies just to evaluate them. The Science of Ingredient Innovation | |December 15, 2014 |DAILY BEAST 

Now we can set up a scientifically well prepared study to evaluate the transfusions vs. improved care. Blood Is Ebola’s Weapon and Weakness |Abby Haglage |October 26, 2014 |DAILY BEAST 

DOJ and CDC numbers differ, and conviction rates are harder to evaluate. The Myth of the Central Park Five |Edward Conlon |October 19, 2014 |DAILY BEAST 

When asked to evaluate his own work, Leigh was a little more reticent. Mike Leigh Is the Master Filmmaker Who Hates Hollywood |Nico Hines |October 14, 2014 |DAILY BEAST 

Here again the Committee was not engaged on a fact-finding mission, but was seeking to evaluate the evidence in a broad way. Report of the Special Committee on Moral Delinquency in Children and Adolescents |Oswald Chettle Mazengarb et al. 

About the other's narrow hips was slung a belt from which hung pouches and tools the primitive colonist could not evaluate. Star Born |Andre Norton 

They will still have in common certain fundamental morphological features, but it will be difficult to know how to evaluate them. Language |Edward Sapir 

It is a question of judgment as to how you evaluate a given characteristic. Warren Commission (7 of 26): Hearings Vol. VII (of 15) |The President's Commission on the Assassination of President Kennedy 

There were several agent examiners available to evaluate this material. Warren Commission (7 of 26): Hearings Vol. VII (of 15) |The President's Commission on the Assassination of President Kennedy