They include signed photographs of Eisenhower and Field Marshal Montgomery, as well as paintings and a red leather despatch box. Churchill’s Secret Treasures for Sale: A British PM’s Life on the Auction Block |Tom Teodorczuk |December 8, 2014 |DAILY BEAST 

Send out your invitations by a servant, or man hired for the purpose; do not trust them to despatch or penny post. The Ladies' Book of Etiquette, and Manual of Politeness |Florence Hartley 

At about one o'clock the young lady approached Dr. Brownlow, and asked him to write, in her name, a despatch to Jeff. Portrait and Biography of Parson Brownlow, The Tennessee Patriot |William Gannaway Brownlow 

Eschar to be gone, I sent my letters by a porter to the posthouse in Southwark to be sent by despatch to the Downs. Diary of Samuel Pepys, Complete |Samuel Pepys 

Easily enough; we can get a hoondee on this place, and send it to him in a letter, or we can despatch a man with it. Confessions of a Thug |Philip Meadows Taylor 

The same weapon, a few minutes later, was used for the despatch of Egmont's friend, Count Horn. Belgium |George W. T. (George William Thomson) Omond