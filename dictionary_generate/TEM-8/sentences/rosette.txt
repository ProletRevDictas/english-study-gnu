In the accompanying illustration is shown a very simple method of tying a rosette in the corner of a couch cover. The Boy Mechanic, Book 2 |Various 

Ronny leaned forward and pinned a huge lace-paper rosette on the obliging Lookout. Marjorie Dean College Freshman |Pauline Lester 

The lower right rosette is a little too far also to the right, ordinarily at least. History of the Postage Stamps of the United States of America |John Kerr Tiffany 

The left line touches the rosette, and is very near the upper left block. History of the Postage Stamps of the United States of America |John Kerr Tiffany 

With the extra outside line from level of lips to the upper rosette. History of the Postage Stamps of the United States of America |John Kerr Tiffany