I wanted his calming presence when the doctor came in, brusque and aloof, and I tried to remember all the questions that kept me up at night. My Kids Can't Get Vaccinated Yet, and I'm Barely Keeping It Together |Katie Gutierrez |December 20, 2021 |Time 

It wasn’t easy to watch, especially because he seems to have a naturally brusque demeanor, which isn’t softened when the ever-present mask hides his wide smile. No vaccination card? No sitting inside this restaurant. |Petula Dvorak |July 22, 2021 |Washington Post 

Sifton writes in the brusque but encouraging tone of a neighborhood dad coaching a soccer game. No-Recipe Recipes Aren’t a Fad; They’re as Old as Cooking Instruction Itself |Marian Bull |July 14, 2021 |Eater 

They described him as an alpha male, brusque, sometimes rude — even to the queen — but one who worked hard to support her and give a modern gloss to the 1,000-year-old institution of the English monarchy. Prince Philip, royal consort to Queen Elizabeth II, dies at 99 |Adrian Higgins |April 9, 2021 |Washington Post 

The fact that he’s still in office is a testament to his brusque determination and general unconcern for the opinions of others. Andrew Cuomo is plummeting, and there’s no one left to catch him |David Von Drehle |March 26, 2021 |Washington Post 

He identifies as “brusque” like other New York City residents. Andrew Cuomo Ignores Rural New York |David Fontana |November 8, 2014 |DAILY BEAST 

Those who have interacted with him describe him as brusque, eccentric, clenched. Meet the Beer Bottle Dictator |Tim Mak |August 12, 2014 |DAILY BEAST 

And McCauley was surely friendlier that his brusque air of command indicated. The Strange and Mysterious Death of Mrs. Jerry Lee Lewis |Richard Ben Cramer |January 11, 2014 |DAILY BEAST 

Whatever shortcomings the sometimes brusque Abramson has as a manager, she just led the paper to four Pulitzers. A Hatchet Job on Jill Abramson |Howard Kurtz |April 24, 2013 |DAILY BEAST 

She is frequently described in the press with such adjectives as “brusque,” “aggressive,” and “undiplomatic in the extreme.” Susan Rice’s Personality 'Disorder' |Lloyd Grove |December 12, 2012 |DAILY BEAST 

"I thought you wouldn't like the bed," she said, with the brusque familiarity of an old servant and friend. A Butterfly on the Wheel |Cyril Arthur Edward Ranger Gull 

She assumed, however, a tone almost brusque, artificially airy and unimportant. Mrs. Falchion, Complete |Gilbert Parker 

The brusque and rather timid young officer is lionized in the drawing-room of Madame Tallien. Napoleon's Young Neighbor |Helen Leah Reed 

He spoke in brusque tones, and he looked at Mina as if he did not know what she might be doing there. Tristram of Blent |Anthony Hope 

In most cases he is deplorably curt of speech and brusque of deportment. The Arena |Various