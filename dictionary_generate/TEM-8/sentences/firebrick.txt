The furnace in which this operation is carried out is a tall, vertical cylinder of iron, lined with firebrick. The Romance of War Inventions |Thomas W. Corbin 

The back and sides of the fireplace should be constructed of firebrick only. Farmers' Bulletin 1230 - Chimneys &amp; Fireplaces |A. M. Daniels 

Few, if any, samples of firebrick will stand the heat of this blast, if the system is fully utilized. Scientific American Supplement, No. 481, March 21, 1885 |Various 

Coke dust or graphite is used for the same purpose in crucible making (see Firebrick). Encyclopaedia Britannica, 11th Edition, Volume 11, Slice 3 |Various 

Slacked-lime putty may be used in place of hydrated lime; firebrick is best laid in fire-clay. Farmers' Bulletin 1889 - Fireplaces and Chimneys |Arthur H. Senner