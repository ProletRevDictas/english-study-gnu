But, I mean, I’m part of a Facebook group for my class, and a lot of Wesleyan upperclassmen have been complaining about kids, like freshmen, going around asking for parties. Even the most cautious schools are seeing outbreaks |Sy Mukherjee |September 17, 2020 |Fortune 

As the city takes steps to repeal a 102-year-old law banning seditious language, new details about how police wielded the law paint a picture of random harassment and punishment for those who complained. Those Ticketed for Seditious Language Say Their Only Crime Was Talking Back |Kate Nucci |September 9, 2020 |Voice of San Diego 

Developers complain that titles waiting for App Store review sometimes take weeks to be cleared. Apple’s App Store draws scrutiny in yet another country |Verne Kopytoff |September 3, 2020 |Fortune 

It is pure metaphysics, he complains, that has lost contact with empirical reality. Your Guide to the Many Meanings of Quantum Mechanics - Facts So Romantic |Sabine Hossenfelder |September 3, 2020 |Nautilus 

The venerable Bill Thurston was known to complain about the perversity which, by the end of his career, had produced Thurston’s theorem, which says that Thurston maps are Thurston-equivalent to polynomials, unless they have Thurston obstructions. Why Mathematicians Should Stop Naming Things After Each Other - Issue 89: The Dark Side |Laura Ball |September 2, 2020 |Nautilus 

And it might be what Islamists complain about while sitting in their caves. Why We Stand With Charlie Hebdo—And You Should Too |John Avlon |January 8, 2015 |DAILY BEAST 

I could complain about how, two out of eight episodes in, Agent Carter is in no hurry to introduce its real villain. Marvel’s ‘Agent Carter’ Stomps on the Patriarchy |Melissa Leon |January 7, 2015 |DAILY BEAST 

On the one hand, residents of these neighborhoods complain of over-policing. Ground Zero of the NYPD Slowdown |Batya Ungar-Sargon |January 1, 2015 |DAILY BEAST 

When they complain that there is no future for them here,” she confides after a long pause, “I worry they are right. Lebanese Christians Gun Up Against ISIS |Susannah George |November 10, 2014 |DAILY BEAST 

“His wife went for the visit and suddenly started to complain about the cuts,” Guadalupe told The Daily Beast. Did Joran Van Der Sloot Fake His Prison Shanking? |Andrea Zarate, Barbie Latza Nadeau |November 5, 2014 |DAILY BEAST 

Only in the pursuit of agriculture can the black man not complain that he is discriminated against on account of his color. The Homesteader |Oscar Micheaux 

Teachers often complain that they can never induce some of their pupils to ask questions on their tasks. Assimilative Memory |Marcus Dwight Larrowe (AKA Prof. A. Loisette) 

He could not complain of the neglect of mankind, or of the ingratitude of those he served. Blackwood's Edinburgh Magazine, No. CCCXXXIX. January, 1844. Vol. LV. |Various 

After a few days of excessive nervousness the most timorous among the women were heard to complain of the monotony of existence! The Red Year |Louis Tracy 

Having at last regained the Emperor's favour, the Marshal had never again to complain of lack of employment. Napoleon's Marshals |R. P. Dunn-Pattison