When we hit our company goals, we celebrate the team’s hard work—whether that’s together under the misters on our one-of-a-kind office patio overlooking the iconic Boulder Flatirons or a special surprise event. The 50 Best Places to Work in 2021 |smurguia |November 9, 2021 |Outside Online 

On the west side, the Palm Springs side, are money-green golf courses, misters spraying from palm trees, wide, gorgeously paved roads, and a concert series called Splash House that features a poolside stage. Postcard From Thermal: Surviving the Climate Gap in Eastern Coachella Valley |by Elizabeth Weil and Mauricio Rodríguez Pons |August 17, 2021 |ProPublica 

Best of all, this fan comes with a built-in mister and 17-millimeter water reservoir, which can produce a steady stream of gentle mist for ten minutes at a time. Best portable fans: Stay chill on a hot day with a cooling personal fan |Irena Collaku |July 21, 2021 |Popular-Science 

These come with peel-and-stick labels so you don’t mix up what’s what, and they have both jet and mister spray settings. 7 gifts to take the stress out of stress-cleaning |Jessica Boddy |December 16, 2020 |Popular-Science 

Some gay apps, like the newer Mister, have not subscribed to the community/tribe model. Grindr’s Trans Dating Problem |David Levesley |January 9, 2015 |DAILY BEAST 

Carl Sandler, who founded Mister in 2012, previously founded Daddyhunt.com and worked at Gay.com. Grindr’s Trans Dating Problem |David Levesley |January 9, 2015 |DAILY BEAST 

Mister Ham in need of cash: That is something a lot of people will not believe. The Stacks: Sell the Overcoat, Keep the Dignity |Paul Hemphill |December 22, 2014 |DAILY BEAST 

And this week it was Mister Ham, General Delivery, United States. The Stacks: Sell the Overcoat, Keep the Dignity |Paul Hemphill |December 22, 2014 |DAILY BEAST 

“Maybe you need a good overcoat for Christmas,” Mister Ham was saying. The Stacks: Sell the Overcoat, Keep the Dignity |Paul Hemphill |December 22, 2014 |DAILY BEAST 

He forgot the great doctrine of humility, and declared that "Mister" Weston should have the volume that very night. The Soldier of the Valley |Nelson Lloyd 

Mister, I say, I don't suppose you don't know of nobody who don't want to hire nobody to do nothing, don't you? The Book of Anecdotes and Budget of Fun; |Various 

I sure would be disappointed, Mister Welborn, if you didn't have a lot of clean snow. David Lannarck, Midget |George S. Harney 

Cash my check for three hundred dollars and meet my podner, Mister Welborn. David Lannarck, Midget |George S. Harney 

All you cimarrons wipe yer hands real clean en shake with my friend Mister Lannarck. David Lannarck, Midget |George S. Harney