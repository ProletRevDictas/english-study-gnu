She subsequently apologized and voted for a Democratic resolution denouncing hatred, though Pelosi and other leaders did not heed GOP calls to remove her from the House Foreign Affairs Committee. House ejects Marjorie Taylor Greene from committees over extremist remarks |Mike DeBonis |February 5, 2021 |Washington Post 

Distancing himself from McCarthy’s approach, McConnell took the unusual step this week of publicly denouncing the “loony lies and conspiracy theories” of people such as Greene, whom he compared to a “cancer” in the party. Republicans worry their big tent will mean big problems in 2022 elections |Michael Scherer, Josh Dawsey |February 4, 2021 |Washington Post 

Then Georgia Republicans sent a QAnon supporter to Congress, which prompted McCarthy to falsely claim Marjorie Taylor Greene had denounced it herself. The GOP: the new know-nothing party |Aaron Blake |February 4, 2021 |Washington Post 

And, in similar fashion, the elites being challenged were caught off guard by the suddenness and intensity and responded by denouncing those forces as misguided, misled and destructive. How the GameStop Trading Surge Will Transform Wall Street |Zachary Karabell |January 29, 2021 |Time 

In recent days former allies, including his political mentors and major donors, have denounced him. Sen. Hawley has been condemned. His bad legal arguments should be stamped out, too. |Daniel Epps, Alan Trammell |January 20, 2021 |Washington Post 

Did he denounce the involvement of organized crime in the abduction and disappearance of 43 students in the nearby city of Iguala? Mexico’s Priests Are Marked for Murder |Jason McGahan |January 7, 2015 |DAILY BEAST 

I stood with a tape recorder, listening to men denounce the liberal media controlled by Jews. The Louisiana Racists Who Courted Steve Scalise |Jason Berry |January 3, 2015 |DAILY BEAST 

Today, former TNR writers and the rest of the media establishment are racing to denounce Hughes. The Rise and Fall of Chris Hughes and Sean Eldridge, America’s Worst Gay Power Couple |James Kirchick |December 9, 2014 |DAILY BEAST 

In the hours before his arrest, Choudary took to Twitter to denounce airstrikes against ISIS. Britain’s Counter-Terror Raids: The End of Londonistan? |Nico Hines |September 25, 2014 |DAILY BEAST 

Another faction says why should we denounce people who we have zero connection with? Why Muslims Hate Terrorism More |Dean Obeidallah |August 12, 2014 |DAILY BEAST 

On one occasion he took the liberty, while preaching, to denounce a rich man in the community, recently deceased. The Book of Anecdotes and Budget of Fun; |Various 

The philosophic Determinist would denounce the offender's conduct, but would not denounce the offender. God and my Neighbour |Robert Blatchford 

Many denounce the system of morning calls as silly, frivolous, and a waste of time. The Ladies' Book of Etiquette, and Manual of Politeness |Florence Hartley 

He could not denounce her without proclaiming his own shame, and the clever woman traded on that. The Weight of the Crown |Fred M. White 

Not exactly, except that I heard my stepfather denounce the doctor as an infernal cur and blackguard. The Doctor of Pimlico |William Le Queux