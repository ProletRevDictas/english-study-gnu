I think that’s what I … I have very little tolerance for people whose egos are inflated and who have lost their sense of curiosity about other people. The BBC’s Katty Kay Shuts Down Men Who ‘Tell Me to Shut Up’ |Eugene Robinson |January 23, 2021 |Ozy 

A remote expanse of badlands and prairie that humbles the ego instantly. Seeing Big Vistas at Theodore Roosevelt National Park |Emily Pennington |January 21, 2021 |Outside Online 

That is, stable in his penchant to destroy everything and everyone that doesn’t feed his ego. Joe Scarborough: ‘We made the same mistake that people made during Hitler’s rise’ |Erik Wemple |January 14, 2021 |Washington Post 

In service of his own ego, he is undermining the constitution and democracy, and may well hand a defeat to Senate Republicans in the process. Business backs democracy |Alan Murray |January 5, 2021 |Fortune 

The first rule is not to destroy your career, your life, just because of the ego. Everest Summits May Become Easier Due to Climate Change |Kyla Mandel |November 20, 2020 |Outside Online 

“You know, I never had a monstrous ego,” Mailer confides to a friend in l987. Mailer’s Letters Pack a Punch and a Surprising Degree of Sweetness |Ronald K. Fried |December 14, 2014 |DAILY BEAST 

All I had in those days was a monstrous lack of ego which therefore required huge injections of actorly ego and misled people. Mailer’s Letters Pack a Punch and a Surprising Degree of Sweetness |Ronald K. Fried |December 14, 2014 |DAILY BEAST 

He even allegedly had a nickname for his violent, aggressive alter ego: Rick. Creed Singer Scott Stapp’s Fall From Grace: From 40 Million Albums Sold to Living in a Holiday Inn |Marlow Stern |November 27, 2014 |DAILY BEAST 

In other words, Taylor Kitsch at his best plays against ego. Alright ‘True Detective,’ You Got Me: Taylor Kitsch Is a Woman’s Man |Teo Bugbee |November 1, 2014 |DAILY BEAST 

Given his alter ego, it may not come as a surprise that Goyeneche is into antiquities, especially of the pre-Colombian variety. Trading Dime Bags for Salvador Dali |Jason McGahan |October 19, 2014 |DAILY BEAST 

Increpaui ego, vt potui, per interpret paganicos hos mores in iam Christianis. The Jesuit Relations and Allied Documents, Vol. II: Acadia, 1612-1614 |Various 

Ego r improbaui, veritus scilicet, ne vel Galli, vel eti Gtiles hoc interpretartur in fidei nostr iniuri. The Jesuit Relations and Allied Documents, Vol. II: Acadia, 1612-1614 |Various 

From an active state of resistance the ego traversed a descending curve ending in absolute passivity. Dope |Sax Rohmer 

The final test of one's ability to project the personal ego over all else in the material world. Hooded Detective, Volume III No. 2, January, 1942 |Various 

Wilson Lamb's ego died a horrible death seventeen seconds before he did. Hooded Detective, Volume III No. 2, January, 1942 |Various