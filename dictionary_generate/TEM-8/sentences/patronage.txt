Rather, the substance of national partisan conflict largely had to do with competing tariff policy visions and how best to exploit political spoils and patronage. How Much Longer Can This Era Of Political Gridlock Last? |Lee Drutman (drutman@newamerica.org) |March 4, 2021 |FiveThirtyEight 

Meghan, a former TV actress, will also surrender her patronage of the National Theatre, bestowed to her by the queen, who herself had held the honor for 45 years. Prince Harry and Meghan lose their patronages, won’t return as ‘working royals’ |William Booth, Karla Adam |February 19, 2021 |Washington Post 

The palace spun the yanking of their patronages as something that must be done in accord with tradition, but stressed that there are no hard feelings. Prince Harry and Meghan lose their patronages, won’t return as ‘working royals’ |William Booth, Karla Adam |February 19, 2021 |Washington Post 

While the spots in those guides still very much deserve patronage, it’s worth looking at the Honolulu restaurants that are surviving and even thriving during this trying year. 38 Exciting Ways to Eat in Honolulu Right Now |Eater Staff |December 2, 2020 |Eater 

Back at Glenaan Station in New Zealand, Allbirds’ place in the market relative to more well known brands takes a back seat to the fact that its patronage allows the shepherd who supplies its wool to make impressive capital improvements to his farm. Allbirds is stepping up for the planet—by treading lightly on it |sheilamarikar |September 21, 2020 |Fortune 

Barack Obama has shown America that crony corporatism, patronage politics, and limitless government know no party. America’s Slumbering Secession Obsession |James Poulos |September 23, 2014 |DAILY BEAST 

Kate's patronage of the High Street is undoubtedly partly to blame. Kate Middleton's History of Flesh-Flashing Wardrobe Malfunctions |Tom Sykes |May 29, 2014 |DAILY BEAST 

This is why Tocqueville puts such a stress on the perils of patronage. What’s At Stake In The Tocqueville/Piketty Debate |James Poulos |April 27, 2014 |DAILY BEAST 

Tocqueville is not most concerned that corporate “dynasties of wealth” will seize control of the government through patronage. What’s At Stake In The Tocqueville/Piketty Debate |James Poulos |April 27, 2014 |DAILY BEAST 

Lacking devoted patronage, there Telugu evolved into a spectacularly hideous argot. India’s Newest State Telangana Is Bosnia Redux |Kranti Rai |March 22, 2014 |DAILY BEAST 

He was a weaver in humble life till his self-acquired attainments attracted patronage. The Every Day Book of History and Chronology |Joel Munsell 

Coldriver did not know there was such a thing as inviting patronage by skillful display. Scattergood Baines |Clarence Budington Kelland 

It is now a city of fifty thousand and dates its rise from the patronage of royalty a century and a half ago. British Highways And Byways From A Motor Car |Thomas D. Murphy 

Mr. Nell, is an excellent man, and deserves the patronage of the public. The Condition, Elevation, Emigration, and Destiny of the Colored People of the United States |Martin R. Delany 

Notwithstanding her popularity and patronage, she died in France in great obscurity and penury. The Every Day Book of History and Chronology |Joel Munsell