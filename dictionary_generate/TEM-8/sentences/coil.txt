A wireless signal can be sent that creates a current in a coil surrounding the pin, affecting its magnetic field and dropping the gadget. These tiny sensors can hitch a ride on mothback |Devin Coldewey |October 8, 2020 |TechCrunch 

Internal air chambers simulate traditional coils, and a double-lock valve helps keep air from escaping, and the convenient two-pack makes for an economic option when outfitting a family. Air mattresses for people who love to entertain and travel |PopSci Commerce Team |September 30, 2020 |Popular-Science 

This mattress gets high marks for its double-height construction and the 40 internal air chambers in the top of the mattress that simulate the individual coils of a traditional mattress. Air mattresses for people who love to entertain and travel |PopSci Commerce Team |September 30, 2020 |Popular-Science 

Each notebook is bound with a spiral lock coil binding that helps prevent the coils from catching on other things in your backpack. Notable notebooks for writing and drawing |PopSci Commerce Team |September 17, 2020 |Popular-Science 

That change in the magnetic field around the coil generates an electric current that also can be used to power the device. A Game Boy look-alike runs on solar panels and button smashes |Maria Temming |September 15, 2020 |Science News 

ABC had effectively removed the launching coil on its Modern Family springboard. ‘black-ish’ Is the New ‘Modern Family’ |Kevin Fallon |October 1, 2014 |DAILY BEAST 

Dabbing wax on the coil or using hash oil on the wick also works. This Is Your E-Cigarette on Drugs |Daniel Genis |July 28, 2014 |DAILY BEAST 

With time I learned to disassemble the entire hotpot and mount the heating coil on a roast beef can with a whole punched in it. Tales of a Jailhouse Gourmet: How I learned to Cook in Prison |Daniel Genis |June 21, 2014 |DAILY BEAST 

On the other far end of the spectrum, admittedly, is the idea that our first president might have unsprung the mortal coil. Was George Washington Among the Walking Dead? |Kevin Bleyer |January 26, 2014 |DAILY BEAST 

The people of his auto-erotic novel Crash coil sex, death, and the motor car, a megadose of pleasure brew. The Best Hot Reads of 2013 |Nicholas Mancusi, Mythili Rao |December 30, 2013 |DAILY BEAST 

Over this spot he twisted all the remaining hair into a coil about four inches long, pointing slightly forward like a horn. Our Little Korean Cousin |H. Lee M. Pike 

No matter what Jessie did to the tuning coil she could not bring that strangely broadcasted message back to their ears. The Campfire Girls of Roselawn |Margaret Penrose 

Amy adjusted the earphones while her friend manipulated the slides on the tuning coil. The Campfire Girls of Roselawn |Margaret Penrose 

She adjusted it, sticking the hat pin through the heavy coil of hair with some deliberation. The Awakening and Selected Short Stories |Kate Chopin 

“No visitors allowed aboard,” replied Mr Welton sternly; catching up, nevertheless, a coil of rope. The Floating Light of the Goodwin Sands |R.M. Ballantyne