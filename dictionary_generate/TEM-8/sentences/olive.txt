For this recipe, I speed up the process by quick-roasting cherry or grape tomatoes whole with dashes of salt, pepper, olive oil and vinegar. How to make a wonderful BLT with out-of-season tomatoes |Daniela Galarza |February 25, 2021 |Washington Post 

This simple mash brightens the vegetable’s subtle taste with the addition of lemon zest and juice, and good extra-virgin olive oil adds both fat and flavor. Potatoes aren’t the only vegetables you should be mashing |Aaron Hutcherson |January 20, 2021 |Washington Post 

This simple mash brightens the vegetable's subtle taste with the addition of lemon zest and juice, and good extra-virgin olive oil adds both fat and flavor. Mashed celery root shines in this bright vegan side dish |Aaron Hutcherson |January 20, 2021 |Washington Post 

Usually, I start with a sheet pan of root vegetables tossed with olive oil, salt, pepper, and herbs. To Combat Cooking Burnout, Do It All at Once |Elazar Sontag |January 14, 2021 |Eater 

Fold the edges of the pastry around the filling and brush it lightly with the olive oil. Wrap leftover roasted root vegetables in phyllo for a quick, crispy galette |Ann Maloney |January 12, 2021 |Washington Post 

Rub the loin with olive oil, and season with salt and pepper. Make Carla Hall’s Roasted Pork Loin With Cranberries |Carla Hall |December 24, 2014 |DAILY BEAST 

Heat a large sauté pan over medium-high heat and add olive oil. Make Carla Hall’s Roasted Pork Loin With Cranberries |Carla Hall |December 24, 2014 |DAILY BEAST 

Add olive oil to the pan and toss in the garlic and chili flake. Make Carla Hall’s Roasted Pork Loin With Cranberries |Carla Hall |December 24, 2014 |DAILY BEAST 

The charismatic bearded revolucionario dressed in a dark olive uniform promised to restore order and hold elections. Cuba Is A Kleptocracy, Not Communist |Romina Ruiz-Goiriena |December 19, 2014 |DAILY BEAST 

Before “we” might offer “you” an olive branch, you must offer one to us. Do LGBTs Owe Christians an Olive Branch? Try The Other Way Around |Jay Michaelson |December 14, 2014 |DAILY BEAST 

It was deep twilight in the room, and rather cold, for the eucalyptus and olive logs in the fireplace still awaited the match. Rosemary in Search of a Father |C. N. Williamson 

It seems that Mr. Adams would have presented the sword boldly, keeping the olive branch carefully concealed behind his back. The Eve of the Revolution |Carl Becker 

Burn the male olive-branch and the pine twig and juniper, and let the blazing laurel crackle amid the hearth. The Religion of Ancient Rome |Cyril Bailey 

He wore an olive green coat, yellow waistcoat, and light grey trousers, strapped over his boots. Skipper Worse |Alexander Lange Kielland 

He was handsome, with the olive-tinted warmth of his southern homefairly tall, straight-limbed and lithea picture of poetic grace. The Fifth String |John Philip Sousa