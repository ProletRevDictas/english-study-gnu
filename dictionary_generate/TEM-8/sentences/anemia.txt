Economists believe high rates of iron-deficiency anemia also have a macroeconomic effect, reducing individual productivity by as much as 40% and reducing GDP by over 1%. One man’s crusade to end a global scourge with better salt |Katie McLean |December 18, 2020 |MIT Technology Review 

Inhibiting sickling would reduce cell lysis — where the cell’s outer membrane is broken — and improve anemia, possibly reducing episodes of pain. Science Might Finally Have a Fix for This Rare Blood Disorder |Nick Fouriezos |December 15, 2020 |Ozy 

Three days after celebrating his 60th birthday this month, he was admitted to a clinic in La Plata with anemia and dehydration. Police search home and office of Maradona’s doctor in death investigation |Ruby Mellen, Ana Herrero |November 29, 2020 |Washington Post 

Maradona, who turned 60 on Friday, was initially hospitalized Monday with symptoms of anemia, dehydration and depression, per reports. Diego Maradona recovering from surgery to remove blood clot in his brain |Des Bieler, Cindy Boren |November 4, 2020 |Washington Post 

For example, CRISPR Therapeutics has ongoing clinical trials employing CRISPR-Cas9 to fix sickle cell anemia, as well as another blood condition known as beta thalassemia. Two women just won the Nobel Prize for their work on the gene-editing technique CRISPR |Claire Maldarelli |October 7, 2020 |Popular-Science 

When I was a medical student in my native Oklahoma, I treated a young woman with a calm smile and severe anemia. You Probably Shouldn’t Try to Lose 20 Pounds by Eating Clay |Kent Sepkowitz |June 24, 2014 |DAILY BEAST 

But no one could figure out the cause of her anemia until someone asked her more carefully about her diet. You Probably Shouldn’t Try to Lose 20 Pounds by Eating Clay |Kent Sepkowitz |June 24, 2014 |DAILY BEAST 

Its relationship to anemia, however, is more complex than the cause-and-effect sequence I learned in medical school. You Probably Shouldn’t Try to Lose 20 Pounds by Eating Clay |Kent Sepkowitz |June 24, 2014 |DAILY BEAST 

MDS is a relatively rare condition that can lead to a depletion of red or white blood cells, anemia, heavy bleeding. How Robin Roberts’ Breast Cancer Treatment Could Cause More Cancer |Casey Schwartz |June 12, 2012 |DAILY BEAST 

The three anti-anemia drugs have cost the government a reported $60 billion since 1989. Medicare's Blood Drugs |Eve Conant |November 8, 2011 |DAILY BEAST 

It occurs in well-marked cases of pernicious anemia and leukemia, and, much less commonly, in very severe symptomatic anemias. A Manual of Clinical Diagnosis |James Campbell Todd 

In pernicious anemia they are always greatly diminished, and an increase should exclude the diagnosis of this disease. A Manual of Clinical Diagnosis |James Campbell Todd 

(b) In secondary anemia plaques are generally increased, although sometimes decreased. A Manual of Clinical Diagnosis |James Campbell Todd 

Megaloblasts are found in pernicious anemia, and with extreme rarity in any other condition. A Manual of Clinical Diagnosis |James Campbell Todd 

Pathologically, normoblasts occur in severe symptomatic anemia, leukemia, and pernicious anemia. A Manual of Clinical Diagnosis |James Campbell Todd