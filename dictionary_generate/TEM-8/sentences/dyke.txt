She told a story of a fellow employee who identifies as a butch dyke (a lesbian who takes on a more masculine identity). The LGBT Center That Changed Our Lives |Justin Jones |December 22, 2014 |DAILY BEAST 

It would be fun to talk to the Carl Reiners of the world, going back to Dick Van Dyke. Jim Rash on ‘The Writers’ Room’ and the Future of ‘Community’ |Kevin Fallon |April 18, 2014 |DAILY BEAST 

As the car passes the Super All Drugs, Butch Trucks cranes around to stare at a flamboyant leather dyke. Stacks: Hitting the Note with the Allman Brothers Band |Grover Lewis |March 15, 2014 |DAILY BEAST 

When my friend told the cis dyke that she was in fact a trans woman, the cis dyke seemed to immediately lose interest. The Struggle To Find Trans Love |Julia Serano |January 14, 2014 |DAILY BEAST 

But Van Dyke has also been the target of blistering criticism from those who see him as less freedom fighter and more war tourist. War Tourists Flock to Syria’s Front Lines |Ben Taub |November 2, 2013 |DAILY BEAST 

The great dyke which kept out arbitrary power had been broken. The History of England from the Accession of James II. |Thomas Babington Macaulay 

Dyke Acland's mark is an anchor on the near side of each of his large herd of ponies, on Exmoor. Notes and Queries, Number 194, July 16, 1853 |Various 

As we came on board we saw our ambassador, Mr. Van Dyke, tell some of his friends goodbye and wish them Godspeed. Ways of War and Peace |Delia Austrian 

Mr. Van Dyke and his clerks, assisted by boy scouts, were working overtime to gratify all these demands. Ways of War and Peace |Delia Austrian 

A wherry sweeping down the dyke with peak lowered leaves us but scant room to pass as we sail back to Heigham Sounds. Yachting Vol. 2 |Various.