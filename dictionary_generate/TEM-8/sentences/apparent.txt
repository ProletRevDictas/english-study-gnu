Both he and Coach Scott Brooks mentioned this week the apparent signs of fatigue up and down the roster. Bradley Beal will miss Friday’s game against the Knicks for rest |Ava Wallace |February 11, 2021 |Washington Post 

The following day, he apologized but showed no apparent remorse and said he had no intention of resigning. Japan Olympics chief who said women talk too much will resign over remarks, reports say |Simon Denyer, Julia Mio Inuma |February 11, 2021 |Washington Post 

But, halfway into last season, the Titans parted ways with the former RunAway roster after an apparent falling out between the team and the organization. The OWL’s London Spitfire are hoping to go big by going home |Teddy Amenabar |February 11, 2021 |Washington Post 

That it is so readily apparent now is testament to the power of hard work and activism. ‘The Lady and the Dale’ explores transphobia in 1970s America |John Paul King |February 10, 2021 |Washington Blade 

It wasn’t until the winter, for example, that South Dakota had the most deaths in its long-term-care facilities, months after the need to protect nursing-home residents was apparent. What to make of New York’s revised nursing-home coronavirus numbers |Philip Bump |February 9, 2021 |Washington Post 

Residents of the neighborhoods where cops are needed the most are mixed on the impact of the apparent slowdown. Ground Zero of the NYPD Slowdown |Batya Ungar-Sargon |January 1, 2015 |DAILY BEAST 

Neither is unnerved by her apparent anger, nor do they see her as threatening. What Would Happen if I Got in White Cop’s Face? |Goldie Taylor |December 30, 2014 |DAILY BEAST 

Yet according to Hamilton, “it was quickly apparent that other than pecuniary consolation would be acceptable.” Should the U.S. Really Pay a Kim’s Ransom? |Kevin Bleyer |December 21, 2014 |DAILY BEAST 

Despite her omnipresence, The Pinkprint makes it apparent how little we actually know about Nicki Minaj. Nicki Minaj Bares Her Own Vulnerability on ‘The Pinkprint’ |Rawiya Kameir |December 16, 2014 |DAILY BEAST 

Nowhere is this new family dynamic more apparent than around the holidays. Binge Watching is the New Bonding Time |The Daily Beast |December 10, 2014 |DAILY BEAST 

He heard himself saying lightly, though with apparent lack of interest: 'How curious, Lettice, how very odd! The Wave |Algernon Blackwood 

The heir apparent and his brothers were cowering in fear, afraid to strike, yet hoping that others would strike for them. The Red Year |Louis Tracy 

The result of the restoration of trade, banking, and credit to earlier and more normal conditions has been steadily apparent. Readings in Money and Banking |Chester Arthur Phillips 

The apparent slightness of these English changes reveals their deliberate subtlety. Solomon and Solomonic Literature |Moncure Daniel Conway 

Increase of hemoglobin, or hyperchromemia, is uncommon, and is probably more apparent than real. A Manual of Clinical Diagnosis |James Campbell Todd