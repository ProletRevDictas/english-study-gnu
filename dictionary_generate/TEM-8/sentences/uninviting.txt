Over the next two decades, people ate, drank, and smoked under those uninviting gateways. How These Rothkos Were Restored Without Touching the Canvas |Adam Rogers |May 30, 2021 |The Daily Beast 

Second,tourism industries worldwide never advertise the uninviting or unpalatable side to their tourist destinations. A Trojan Horse in CUNY’s Center for Lesbian and Gay Studies? |Andrea Weiss |July 19, 2012 |DAILY BEAST 

The banks on the American side, where we knew we had safe passage, looked uninviting—covered with a wall of forbidding plants. My Night on the Border |Bryan Curtis |May 25, 2010 |DAILY BEAST 

The bed was so untidy and uninviting that she would not lie down on it, so she made them bring in an old armchair. The Seven Cardinal Sins: Envy and Indolence |Eugne Sue 

Last section of East arte should go; but rocks up to the shoulder are uninviting. Mount Everest the Reconnaissance, 1921 |Charles Kenneth Howard-Bury 

Every window reflected back the same blank uninviting gloom. The New Tenant |E. Phillips Oppenheim 

On the left the country varied between flat and upland, but was hardly less uninviting. The Yeoman Adventurer |George W. Gough 

The extremes of climate, and our uninviting roads, discourage open air exercise, and comparatively few have much time to go out. The Education of American Girls |Anna Callender Brackett