Now in terms of equipment, ancient writers and pictorial evidence rather paint a vague picture of the renowned foot companions. The Mighty Ancient Macedonian Army of Alexander the Great |Dattatreya Mandal |August 4, 2022 |Realm of History 

Meanwhile, composers became increasingly interested in pictorial descriptions of environmental and human noises such as thunder, wind, and the din of battle. Your Brain Is Like Beethoven - Issue 107: The Edge |Jonathan Berger |October 27, 2021 |Nautilus 

Those images helped the researchers match some pictorial accounts of quakes, including one in 1507, to later descriptions of the events. This pictogram is one of the oldest known accounts of earthquakes in the Americas |Carolyn Gramling |September 7, 2021 |Science News 

The Telleriano-Remensis uses a pictorial representation of a 52-year cycle to roughly date the quakes. This pictogram is one of the oldest known accounts of earthquakes in the Americas |Carolyn Gramling |September 7, 2021 |Science News 

Both artists play with print and bookmaking formats, using internal frames to boost pictorial drama. In the galleries: Posters as a medium for serious but jubilant communication |Mark Jenkins |July 9, 2021 |Washington Post 

A helpful pictorial index provides photographs of the actual objects. The Best Coffee Table Books of 2014 |Robert Birnbaum |December 13, 2014 |DAILY BEAST 

They are never portraits of specific individuals who might interest us, even visually, apart from their pictorial roles. The Stacks: Edward Hopper’s X-Ray Vision |Hilton Kramer |October 25, 2014 |DAILY BEAST 

He declared them to be “a pictorial representation of England as a free society and the liberator of other peoples.” Virgin Sacrifice and the Meaning of the Parthenon |Nick Romeo |February 12, 2014 |DAILY BEAST 

One little girl attempted to smell at the trees in a drawing and pretended to feed some pictorial dogs. Children's Ways |James Sully 

With less intelligent children traces of this tendency to take pictorial representation for reality may appear as late as four. Children's Ways |James Sully 

Calmly we seated ourselves in the "arm chair," and continued our labors upon our magnificent Pictorial. The Book of Anecdotes and Budget of Fun; |Various 

The use of pictorial representations appears often to have been a matter of necessity. The Catacombs of Rome |William Henry Withrow 

Another slab bears the outline of a little pig, the pictorial translation of the somewhat singular name Porcella. The Catacombs of Rome |William Henry Withrow