Three and half days after his birth/death, I undressed my son, changed his nappy, bathed him, and dressed him in an outfit. Daily Beast Readers React to YouTube Stillborn Baby Memorials |Brandy Zadrozny |November 12, 2013 |DAILY BEAST 

Prince Harry showed he is still game for a laugh today when he attempted to change the nappy of a baby (doll) with just one hand. Practice Makes Perfect: Uncle Harry Changes A Baby's Diaper |Tom Sykes |April 25, 2013 |DAILY BEAST 

A nappy change prompted high security alert on a recent United Airlines flight from DC to San Francisco. The National-Security Diaper Scramble |Philip Shishkin |April 25, 2013 |DAILY BEAST 

Care, mad to see a man sae happy, E'en drown'd himsel' amang the nappy! The Book of Humorous Verse |Various 

No nest box or pan is really needed, but many pigeon keepers use a nest bowl, called a nappy, of earthenware or wood fiber. Our Domestic Birds |John H. Robinson 

"Miss 'Bell needn't strut so big; she got short nappy har's well's I," said Nell, with a broad grin that showed her teeth. Clotelle |William Wells Brown 

She let her fingers wander up and down his cheek and across his shoulders and into his uneven nappy hair. Just Around the Corner |Fannie Hurst 

They had a pair of plates that for ugliness and price knocked the "ginuwine Hall nappy" higher 'n the main truck. Cape Cod Stories |Joseph C. Lincoln