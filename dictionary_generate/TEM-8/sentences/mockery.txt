Rodimer entered just hours before the filing deadline, drawing some mockery from fellow Republicans. The Trailer: Is election reform headed for passage, courts, or nowhere? |David Weigel |March 4, 2021 |Washington Post 

Football coaches at all levels have made a mockery of mask-wearing, with the NFL imposing hefty fines on coaches who expose their noses and mouths and some college conferences threatening to dock schools up to $1 million. A college football coach’s season at war with the coronavirus — and his own school |Kent Babb |January 19, 2021 |Washington Post 

It makes a mockery of those rules to say that misallocating funds to decorate your office, for example, is punishable, but seeking to undo an election and inciting rioters are not. Seditious Republicans must be held accountable |Jennifer Rubin |January 7, 2021 |Washington Post 

In its lawsuit, the Justice Department argues that Facebook's hiring practices made a mockery of these requirements. Feds say Facebook broke US law offering permanent jobs to H-1B workers |Timothy B. Lee |December 3, 2020 |Ars Technica 

Revel in mirth as Borat makes a mockery of a pair of rubes with a Don’t Tread on Me flag who were kind enough to take him in in the midst of the covid-19 epidemic! You should side with Borat’s victims, not mock them |Sonny Bunch |October 30, 2020 |Washington Post 

The mockery comes from a place unburdened by history and untouched by the present. What Would Happen if I Got in White Cop’s Face? |Goldie Taylor |December 30, 2014 |DAILY BEAST 

Our mockery of celebworld helps us evade the soul-crushing decadence concealed within. Pyongyang Shuffle: Hollywood In Dead Panic Over Sony Hack |James Poulos |December 19, 2014 |DAILY BEAST 

Martyrdom, in this context, being defined as “mockery, slander, ostracism.” ‘Persecuted’ Is the Christian Right’s Paranoid Wet Dream |Candida Moss |July 22, 2014 |DAILY BEAST 

But this delightful book has much more than mockery on its mind. Newsweek Takedown From Beyond the Grave: Michael Hastings’s Fiction Tells the Truth |Christopher Dickey |June 18, 2014 |DAILY BEAST 

I detected some mockery, the mockery of infidels, but I did not care. Why Americans Should Love the World Cup |Sean Wilsey |June 12, 2014 |DAILY BEAST 

He drew himself up, twisted his moustache, and met her eyes—they were rather sad and tired—with the roguish mockery of his own. The Joyous Adventures of Aristide Pujol |William J. Locke 

A mockery of a government—a disgrace to the office pretended to be held—a parody on the position assumed. The Condition, Elevation, Emigration, and Destiny of the Colored People of the United States |Martin R. Delany 

He did not like his cynical way of looking at things, nor understand his mockery of current morality. The Everlasting Arms |Joseph Hocking 

He could not go further, for it seemed to him like mockery to suggest by way of comfort that fourteen years would come to an end. The Floating Light of the Goodwin Sands |R.M. Ballantyne 

For all your sneers and your mockery you've always known I loved you the way a man loves a decent woman. Summer |Edith Wharton