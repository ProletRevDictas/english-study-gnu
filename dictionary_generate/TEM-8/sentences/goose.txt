Sandpipers, owls, loons, and geese all go through smaller migrations. A peregrine falcon’s power to migrate may lie in its DNA |Philip Kiefer |March 4, 2021 |Popular-Science 

Under a giant oak tree, I conducted a phone interview without interruption from my office mates — shrieking kids and nasal-y geese. Was a three-week trip to New Orleans for work or vacation? Both. |Andrea Sachs |February 19, 2021 |Washington Post 

It’s common to see dozens upon dozens of geese nesting on the side of parkways on Long Island. Four wild animals that are thriving in cities |By Ryan Chelius/Outdoor Life |February 9, 2021 |Popular-Science 

Fact is, their goose was cooked by going for 2 at the end of the third quarter to turn down 5 into down 3. The Packers Kicked A Field Goal, Josh Allen Had A Bad Day, And The GOAT And Baby GOAT Are Super Bowl-Bound |Sara Ziegler (sara.ziegler@fivethirtyeight.com) |January 25, 2021 |FiveThirtyEight 

Every year, millions of bar-headed geese migrate over the Himalayas and have been doing so for millions of years. Why Birds Can Fly Over Mount Everest - Issue 94: Evolving |Walter Murch |December 30, 2020 |Nautilus 

We had a little goose that was laying golden eggs, and they told us to snip its head off. David Lynch on Transcendental Meditation, ‘Twin Peaks,’ and Collaborating With Kanye West |Marlow Stern |October 6, 2014 |DAILY BEAST 

I wandered around aimlessly for a while, then gave the goose to an acquiescent hippy on a barge. The Life and Art of Radical Provocateur—and Commune Leader—Otto Muehl |Anthony Haden-Guest |September 22, 2014 |DAILY BEAST 

One of the men was carrying a carving knife and a live goose. The Life and Art of Radical Provocateur—and Commune Leader—Otto Muehl |Anthony Haden-Guest |September 22, 2014 |DAILY BEAST 

Finally, Cleese goose-steps out of the dining room as the hapless Germans cringe and sob. Life Under Air Strikes: Children Under Fire Will Never Forget — or Forgive |Clive Irving |August 3, 2014 |DAILY BEAST 

Then, the big goose egg that the administration is going to get from Iran will more obviously be a zip. Putin Poised to Retaliate Against Obama by Trashing Iran Deal |Josh Rogin |July 19, 2014 |DAILY BEAST 

To hear the creature talk about it makes my mouth as a brick kiln and my flesh as that of a goose. The Joyous Adventures of Aristide Pujol |William J. Locke 

The Kutchin make pretty pipe-stems out of goose-quills wound about with porcupine-quills. Tobacco; Its History, Varieties, Culture, Manufacture and Commerce |E. R. Billings. 

The universal desire was for mamma to see him, and when the time came, she owned that papa's swan had not turned out a goose. The Daisy Chain |Charlotte Yonge 

The declination axis is short, and is supported by a massive goose-neck bolted to the upper end of the polar axis. Photographs of Nebul and Clusters |James Edward Keeler 

Of waking dog, nor gaggling goose more waker then the hound.' Chaucer's Works, Volume 1 (of 7) -- Romaunt of the Rose; Minor Poems |Geoffrey Chaucer