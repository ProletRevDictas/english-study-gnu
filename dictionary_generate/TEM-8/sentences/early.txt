Working two guided trips a summer could earn early guides enough to buy a few more cows and raise the quality of life of an entire extended family. It’s Time to Embrace Ski Guiding |Marc Peruzzi |February 26, 2021 |Outside Online 

They pointed to an earlier message McCoy posted prior to his apology giving a different reason for deleting his post about obese people. FOX 5’s McCoy suspended over offensive Tweet |Lou Chibbaro Jr. |February 24, 2021 |Washington Blade 

The earlier proposal called for a 25,599-square-foot building. Rehoboth theater supporters hopeful for approval of new buildings |Lou Chibbaro Jr. |February 23, 2021 |Washington Blade 

To find the case assignments that went to ineligible attorneys, we compared the assignment date to the earliest date that an attorney appeared on a list in any court in the state. Lawyers Who Were Ineligible to Handle Serious Criminal Charges Were Given Thousands of These Cases Anyway |by Samantha Hogan, The Maine Monitor, and Agnel Philip, ProPublica |February 23, 2021 |ProPublica 

In the earlier cold snap, the grid was pushed to the limit and rolling blackouts swept the state, spurring an angry Legislature to order a study of what went wrong. “Power Companies Get Exactly What They Want”: How Texas Repeatedly Failed to Protect Its Power Grid Against Extreme Weather |by Jeremy Schwartz, Kiah Collier and Vianna Davila |February 22, 2021 |ProPublica 

When cities started adding chlorine to their water supplies, in the early 1900s, it set off public outcry. Anti-Fluoriders Are The OG Anti-Vaxxers |Michael Schulson |July 27, 2016 |DAILY BEAST 

It’s cool because Trenchmouth opened for Green Day in the early ‘90s in Wisconsin. Coffee Talk with Fred Armisen: On ‘Portlandia,’ Meeting Obama, and Taylor Swift’s Greatness |Marlow Stern |January 7, 2015 |DAILY BEAST 

At first—it was the early stages of reporting—I was amused at having been so crassly underestimated. I Tried to Warn You About Sleazy Billionaire Jeffrey Epstein in 2003 |Vicky Ward |January 7, 2015 |DAILY BEAST 

Early on, the sexual protagonist complains that her Molson-drinking husband is pretty much an incompetent Neanderthal. ‘A Gronking to Remember’ Speed Read: 8 Naughtiest Bits |Emily Shire |January 7, 2015 |DAILY BEAST 

Detectives with a fugitive task force caught up with Polanco and a friend on a Bronx street in the early afternoon. Shot Down During the NYPD Slowdown |Michael Daly |January 7, 2015 |DAILY BEAST 

The "new world" was really found in the wonder-years of the eighteenth and early nineteenth centuries. The Unsolved Riddle of Social Justice |Stephen Leacock 

When his lordship retired early, as was his custom, the other men adjourned once more to the billiard-room. The Pit Town Coronet, Volume I (of 3) |Charles James Wills 

In the early stages of chronic nephritis, when diagnosis is difficult, it is usually normal. A Manual of Clinical Diagnosis |James Campbell Todd 

He was in early life a shipcarpenter, and subsequently American consul at Antwerp. The Every Day Book of History and Chronology |Joel Munsell 

Hamo in alluding to the early cultivation of tobacco by the colony, says, that John Rolfe was the pioneer tobacco planter. Tobacco; Its History, Varieties, Culture, Manufacture and Commerce |E. R. Billings.