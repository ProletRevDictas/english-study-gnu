Yard debris like dry leaves, branches, and unkempt trees are a fuel for wildfires to spread quickly and ferociously. The high cost of landscaping is a problem for wildfire defense |Clarisa Diaz |August 19, 2021 |Quartz 

As Rob, he’s bearded and unkempt but still radiant, a messy movie Jesus. In Pig, Nicolas Cage Plays a Grouchy, Meditative Hermit—and Gives His Best Performance in Years |Stephanie Zacharek |July 15, 2021 |Time 

If you’re planning an audio-only podcast, then feel free to show up unshowered in your favorite pair of unkempt, soft pants. Best podcast microphone: Find the right gear to be your voice’s best friend |Billy Cadden |June 16, 2021 |Popular-Science 

More men are asking how they can donate their wild, unkempt hair once they can finally get haircuts, Chimera said. Our long pandemic hair is finally getting cut — and hair donation charities are overwhelmed |Ashley Fetters |April 22, 2021 |Washington Post 

Half-naked crew members jammed themselves into lifeboats and rowed away … We were close enough to see their unkempt beards and the tattoos on their arms. Wreckage of long-lost WW II ship, sunken with its Native American skipper and half its crew, identified |Michael Ruane |April 2, 2021 |Washington Post 

Lemkin hung around the proceedings, disheveled and unkempt, but determined. The Man Who Invented the Word ‘Genocide’ |Nina Strochlic |November 19, 2014 |DAILY BEAST 

The typically fresh-faced, photogenic 31-year-old actor is sporting a bushy, unkempt, downright Biblical-looking beard. Andrew Garfield on the Evils of Capitalism, the Hacking Scandal, and Criticism of ‘Spider-Man 2’ |Marlow Stern |September 10, 2014 |DAILY BEAST 

Al Pacino comes dressed in black and gray, wearing multiple bracelets and an unkempt tuft of hair poking up from his scalp. Al Pacino Does What He Wants to Do: 'The Humbling,' Scorsese, and That 'Scarface' Remake |Alex Suskind |September 9, 2014 |DAILY BEAST 

Strands of unkempt white hair wave above his head like an anti-surrender flag. Can This Ornery Socialist Spoil the Clinton Coronation? |David Freedlander |July 2, 2014 |DAILY BEAST 

It was a hot Friday morning and Brad Zaun was juggling a baby in a slightly unkempt living room of suburban Des Moines. The Bizarro World Of Iowa’s GOP Convention |Ben Jacobs |June 23, 2014 |DAILY BEAST 

These unkempt-looking Father Times and Methuselahs prowl about the staircases of the different ateliers daily. The Real Latin Quarter |F. Berkeley Smith 

George jerked his peaked cap from his head, revealing a tangle of unkempt red hair. Dope |Sax Rohmer 

They were resting on their cumbrous belongings, strange groups, unkempt and half dressed. Ancestors |Gertrude Atherton 

They were standing now in front of a tall, gloomy house, unkempt, with broken gate—a large but miserable-looking abode. The Double Four |E. Phillips Oppenheim 

And at the trail's end the unkempt, ribald crew swarmed their dark and dirty camp as a band of pirates a galleon. Blazed Trail Stories |Stewart Edward White