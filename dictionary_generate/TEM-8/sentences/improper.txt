Nothing could be alleged as illegal or improper or unethical. Jack Palladino, aggressive sleuth who worked for Bill Clinton campaign, dies at 76 |Harrison Smith |February 2, 2021 |Washington Post 

McSweeney argues the action was improper because it amounts to “special legislation” — meaning a law targeting one specific case, which is unconstitutional, instead of setting out broad policy. Residents ask high court to block removal of Richmond’s Robert E. Lee statue as Virginia prepares to act |Gregory S. Schneider |January 26, 2021 |Washington Post 

Later, he was busted from sergeant to private for “improper dancing” with the daughter of a Spanish instructor, according to author Jean Edward Smith in “Eisenhower in War and Peace” — although that speaks more to judgment than ability. Not all presidents’ dance skills are created equal |Bonnie Berkowitz, Joanne Lee |January 21, 2021 |Washington Post 

Those familiar with the disciplinary system say data at the time showed that punishment for internal infractions — such as an improper log book or refusal to follow an order — had often been more severe than for misconduct against civilians. Still Can’t Breathe |by Topher Sanders, ProPublica, and Yoav Gonen, THE CITY, video by Lucas Waldron, ProPublica |January 21, 2021 |ProPublica 

The group claims thousands of members who assert the right to defy government orders they deem improper. Self-styled militia members planned on storming the U.S. Capitol days in advance of Jan. 6 attack, court documents say |Spencer Hsu, Tom Jackman, Devlin Barrett |January 20, 2021 |Washington Post 

Farrell issued a ticket to an 18-year-old shipyard worker for speeding and an improper exhaust mechanism, according to the TP. The Louisiana Racists Who Courted Steve Scalise |Jason Berry |January 3, 2015 |DAILY BEAST 

And if any police were warning bars against serving the Santas, “that would be improper behavior by a government official.” Before the Bros, SantaCon Was as an Anti-Corporate Protest |David Freedlander |December 12, 2014 |DAILY BEAST 

I feel sorry because (patients) got the surgery with improper devices, so they might suffer from it. Patients Screwed in Spine Surgery ‘Scam’ |The Center for Investigative Reporting |November 3, 2014 |DAILY BEAST 

It could be that those downstream abnormalities in cell development were due to improper signaling from the cerebellum. Early Brain Injury Might Be the Root of Autism |Russell Saunders |September 7, 2014 |DAILY BEAST 

Improper burial, Dove says, could mean that harmful bacteria are leeching into the waterways. Aporkalypse Now: Pig-Killing Virus Could Mean the End of Bacon |Carrie Arnold |August 20, 2014 |DAILY BEAST 

Yet the business may become a nuisance when conducted in some localities, or in an improper manner. Putnam's Handy Law Book for the Layman |Albert Sidney Bolles 

Are we to abandon any one means of doing good, because the improper use of it would do injury? The Ordinance of Covenanting |John Cunningham 

I often heard Europeans remark that they considered the procession of the nuptial couch extremely improper. A Woman's Journey Round the World |Ida Pfeiffer 

This improper use of a parent's home has also occurred in other districts. Report of the Special Committee on Moral Delinquency in Children and Adolescents |Oswald Chettle Mazengarb et al. 

What was a generation ago considered improper is now generally accepted as a subject for display. Report of the Special Committee on Moral Delinquency in Children and Adolescents |Oswald Chettle Mazengarb et al.