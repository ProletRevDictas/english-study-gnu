Companies like Uber pay their drivers low wages, require workers to report to unpredictable systems run by algorithms, and have very little legal recourse and access to their own data if they have a problem, Mozilla’s report says. The Internet needs a Richard Simmons |Danielle Abril |January 28, 2021 |Fortune 

In a fresh respite for professionals and businesses who would until now take recourse to Instagram to publicize their business, LinkedIn stories have arrived to help professionals share their on-the-fly moments. Five strategies to promote your business using LinkedIn Stories |Aayush |December 31, 2020 |Search Engine Watch 

Your only recourse then is to beat the tightness into submission. Gifts for the most stressed out people you know |Rachel Feltman |December 15, 2020 |Popular-Science 

Nikola responded the following day, saying the report contained “false and misleading statements” and that it had hired counsel to “evaluate potential legal recourse.” Little Big Shorts: How tiny ‘activist’ firms became sheriffs in the stock market’s Wild West |Bernhard Warner |December 3, 2020 |Fortune 

Before the rule change, federal law didn't address the issue of emotional support animals, so airlines had little recourse but to accommodate them. Airlines will no longer be required to transport emotional support animals |Lori Aratani |December 2, 2020 |Washington Post 

What recourse would they have to prove that they should be eligible for release? Are Mandatory Ebola Quarantines Legal? |Tim Mak |October 28, 2014 |DAILY BEAST 

I know that it might get nowhere, but this is my only recourse. Iran Says Take Off the Veil—and Be Raped |IranWire |June 9, 2014 |DAILY BEAST 

Had the board decided to give Eich a few weeks to prove himself, those who disagreed would have had no recourse. Chill Out About Firefox, Everybody |Gene Robinson |April 5, 2014 |DAILY BEAST 

They, according to one juror, who spoke to Nightline, believed Dunn had no recourse but to shoot. Michael Dunn, Jordan Davis, and America's Racist Heritage |Jamelle Bouie |February 19, 2014 |DAILY BEAST 

However well-intentioned, we are not sure that this bill would be the most effective means of recourse. The Movement to Boycott the American Studies Association for Boycotting Israel |David Freedlander |February 15, 2014 |DAILY BEAST 

The men, whose poniards his sword parried, had recourse to fire-arms, and two pistols were fired at him. The Pastor's Fire-side Vol. 3 of 4 |Jane Porter 

To such persons does he open the doors to pay them, while they are shut on the wretched owners without recourse. The Philippine Islands, 1493-1898, Volume XX, 1621-1624 |Various 

Then we blotted out the fire, and, stretching ourselves on the ground, had recourse to the solace of tobacco. Raw Gold |Bertrand W. Sinclair 

No other remedy remained but the application of force, and convinced of this, it had recourse to revolution. The Philippine Islands |John Foreman 

When we wish to regulate the admission of light to our rooms we have recourse to very clumsy contrivances. Gospel Philosophy |J. H. Ward