So the virus may have spread when the rodents groomed, rubbed their noses or moved around. Dust can infect animals with flu, raising coronavirus concerns |Erin Garcia de Jesus |August 24, 2020 |Science News For Students 

Like the regions in the brain, each stage seeks different types of general pictorial elements like those the brain finds, rather than seeking the eyes, nose, and so on. The Bias in the Machine - Issue 89: The Dark Side |Sidney Perkowitz |August 19, 2020 |Nautilus 

The main stumbling block is that we’re still not quite sure how individual smells activate the entire neural highway from nose to brain. A Highway to Smell: How Scientists Used Light to Incept Smell in Mice |Shelly Fan |July 1, 2020 |Singularity Hub 

So he convinced his supervisors to let him bring home some equipment so he could evaluate whether cloth masks cut down on the number of potentially virus-laden particles that spew from people’s mouths and noses when they talk, cough or breathe. Why scientists say wearing masks shouldn’t be controversial |Tina Hesman Saey |June 26, 2020 |Science News 

There, clusters of nerve endings called glomeruli organize the smell signals picked up in the nose. How to make a mouse smell a smell that doesn’t actually exist |Laura Sanders |June 18, 2020 |Science News 

I will turn my nose up when you offer me the rest of some delicious pastry that you nibbled on. Why My Norovirus Panic Makes Me Sick |Lizzie Crocker |January 5, 2015 |DAILY BEAST 

They should have pointed the nose of the Airbus down and applied more power. Flight 8501 Poses Question: Are Modern Jets Too Automated to Fly? |Clive Irving |January 4, 2015 |DAILY BEAST 

A spandex mask stretched over his face, covering his eyes and nose. Dungeons and Genital Clamps: Inside a Legendary BDSM Chateau |Ian Frisch |December 20, 2014 |DAILY BEAST 

The struggle continues and Chan is punched, suffering a broken nose. Protesters Slimed This Good Samaritan Cop |Michael Daly |December 16, 2014 |DAILY BEAST 

Also, your nose is in front of your face and the sun will come up tomorrow. The Gun Battle Since Newtown |Cliff Schecter |December 14, 2014 |DAILY BEAST 

His nose was hooked and rather large, his eyes were blue, bright as steel, and set a trifle wide. St. Martin's Summer |Rafael Sabatini 

Sympathising with its desires, Benjy changed his posture, and managed just to touch the nose of his enemy. The Giant of the North |R.M. Ballantyne 

He walked first to one side, and then the other, rooting in the dirt with his funny, rubbery nose. Squinty the Comical Pig |Richard Barnum 

He shut his fist and hit Butterface a weak but well intended right-hander on the nose. The Giant of the North |R.M. Ballantyne 

The girl began to hum, as she powdered her nose with a white glove, lying in a powder box. Rosemary in Search of a Father |C. N. Williamson