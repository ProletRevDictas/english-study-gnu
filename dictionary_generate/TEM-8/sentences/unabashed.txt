Surveillance video, photos and media interviews captured Barnett in Pelosi’s office for six minutes and his unabashed boasts later that he broke in and took mail from the office, according to authorities. Capitol riot suspect pictured at Pelosi’s desk screams ‘It’s not fair’ in courtroom tantrum |Meryl Kornfield |March 5, 2021 |Washington Post 

He stood out even in a helmet — his trademark blond dreadlocks swung as he blew up plays — and he exhibited the unabashed joy of an elementary-schooler at recess. Chase Young’s upbringing made him a ‘crazy unusual’ leader, and Washington is already following |Sam Fortier |January 1, 2021 |Washington Post 

It is a feel-good, unabashed spectacle that controls well, looks great and has a hyper-efficient story line that never tries to overdeliver. ‘Marvel’s Spider-Man: Miles Morales’: Like its movie counterparts, a confident and entertaining spectacle |Christopher Byrd |November 6, 2020 |Washington Post 

PayPal CEO Dan Schulman is one of the most passionate proponents of stakeholder capitalism that I know, and is unabashed in his advocacy. Why PayPal’s Dan Schulman gave workers pay increases, without the market requiring it |Alan Murray |September 9, 2020 |Fortune 

She was feisty, unabashed, loud, almost deaf, and mentally ill. Learning to parent your parent |Terri Schlichenmeyer |August 21, 2020 |Washington Blade 

Unabashed feminist author Catlin Moran lampooned women who did not identify as feminists in her book, How To Be a Woman. You Don’t Hate Feminism. You Just Don’t Understand It. |Emily Shire |July 24, 2014 |DAILY BEAST 

But despite its popularity, Reddit manages to retain a glorious, dark, unabashed weirdness that positively thrives there. Five Subreddits You May Have Missed, and Probably Still Should Give a Miss |Kelly Williams Brown |April 5, 2014 |DAILY BEAST 

Anna (Kristen Bell), aside from being funny, eager, and a little awkward, is unabashed about her dream of finding true love. Disney’s Sublimely Subversive ‘Frozen’ Isn’t Your Typical Princess Movie |Melissa Leon |November 29, 2013 |DAILY BEAST 

Amy Zimmerman on the ridiculous plot, over-the-top tone, and unabashed silliness viewers seem to love. ‘Sleepy Hollow’ Is TV’s Craziest, Most Over-the-Top New Show ... And You Should Watch It |Amy Zimmerman |October 8, 2013 |DAILY BEAST 

In the end, his egotistical personality and his unabashed ambition to join the highest ranks did him in. The Eerie Echoes Between the Trials of Bo Xilai and His Father |Wenguang Huang, Pin Ho |August 27, 2013 |DAILY BEAST 

"I was on'y a-openin' it, sar, ready for de company," exclaimed the unabashed Tinker. Jack Harkaway's Boy Tinker Among The Turks |Bracebridge Hemyng 

In the eighties any writer who dealt unabashed with death was regarded as an unpleasant person. The Letters of Ambrose Bierce |Ambrose Bierce 

"We must be awfully popular here," Paddy remarked cheerfully, and he looked unabashed into the scowling faces of the natives. The Argus Pheasant |John Charles Beecham 

"We come for our day's hire, your reverence," said the foreman, unabashed. My New Curate |P.A. Sheehan 

As to the others, whenever their trials and tribulations abate for an instant, they relapse into a state of unabashed contentment. Humanly Speaking |Samuel McChord Crothers