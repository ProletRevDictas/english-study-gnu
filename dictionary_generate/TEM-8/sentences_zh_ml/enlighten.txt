本指南旨在启发您寻找最好的夜视镜。 最佳夜视镜:当太阳下山时如何看到银河系Irena Collaku | 2021年8月15日|大众科学 
 在经历了大流行暴露出来的现实之后，许多公司开始接受全新的肯定，并承诺会有更开明、更温和、更公平的工作场所。 如何经营一家女权主义公司|Lila MacLellan | 2021年6月10日|Quartz 
 渐渐地，陌陌开始意识到机器人的压迫，她把自己小时候的一次手术和儿时最好朋友的失踪联系起来。 政治和大流行改变了我们对城市的想象Joanne McNeil, 2021年4月28日，麻省理工科技评论 
 如果你对纽约有那么一点点的关心——可能你并不关心——这本书将会给你启迪，很可能会让你激动。 纽约是如何不断重塑自我的? (奖金)库尔特·安德森，2021年3月21日 
 这些年轻女性在19世纪90年代所遭受的耻辱和悲伤，与我们这个被认为是开明的时代，那些试图怀孕或结束怀孕的女性所承受的痛苦并没有太大区别。 在安娜·诺斯引人入胜的《非法》中，没有什么比一个没有孩子的女人更危险的了 
 也许你们这些从亲爱的格劳恩跟着我过来的英国人能给点启发。 是的，我喜欢圣诞音乐。 停止笑。 Michael Tomasky | 2014年12月24日|DAILY BEAST 
 创作者罗德·瑟林(Rod Serling)被“不仅仅是娱乐，而且是启迪”的需求所驱使。 一个厌战的老兵是如何创造“模糊地带”|Rich Goldstein | 2014年11月13日|每日野兽 
 有了辛克莱作为我们的非评判指南，我们看到，这些启示不是为了震惊我们，而是，也许，启发我们。 “高维护”，就像一个好的高潮，有趣，有时令人不安|凯特琳·迪克森| 2014年11月11日|每日野兽 
 他们应该给观众带来希望和启发，同时促进积极的情绪。 《利维坦》内部:俄罗斯电影人安德烈·萨金采夫获奖反普京戛纳电影|理查德·波顿| 2014年5月27日|每日野兽 
 谁能给我点启发:女生应该是认真的吗? 为什么“女孩”对女人不好|艾玛·伍尔夫| 2014年3月31日|每日野兽 
 我们当然打算公平地处理利比里亚问题，并向读者提供可能使他们有所启发的每一项信息。 《美国有色人种的状况、地位、移民和命运》马丁·r·德拉尼 
 杰克想起查尔斯·梅森牧师画的那个家的照片，不禁打了个寒颤，但他就是不给她开脱。 克朗普顿|玛丽·j·霍姆斯 
 胡安娜太晚才知道我们的法律、习惯和行政习惯，没有及时给她丈夫开化。 胡安娜，奥诺雷·德·巴尔扎克 
 理智平和的人自我启蒙; 他们的光芒逐渐扩散，最终到达人们的身边。 古今迷信(1732)|让·梅斯耶 
 今天人类的启蒙如此之少，只是因为我们有预防措施，或有运气，一点一点地启蒙了他。 古今迷信(1732)|让·梅斯耶