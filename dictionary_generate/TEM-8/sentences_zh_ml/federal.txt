2018年，几家公共机构联合起来起诉联邦政府的不作为。 晨报:空缺税Déjà Vu |圣地亚哥之声| 2021年2月11日|圣地亚哥之声 
 当地官员说，联邦政府已经为国会议员和军方等部分雇员使用了自己的疫苗供应。 华盛顿地区领导人请求联邦应急管理局帮助为联邦工作人员接种疫苗|Julie Zauzmer, Rachel Chason, Rebecca Tan | 2021年2月11日|华盛顿邮报 
 另一项是提高联邦政府对COBRA的补贴，这往往是昂贵的。 根据国会计划，《平价医疗法案》补贴可能会增加|艾米·戈德斯坦| 2021年2月11日|华盛顿邮报 
 该机构一直依靠国会今年4月通过的联邦《关爱法案》(medicare Act)资金生存，但7.67亿美元将于今年年初用完。 Metro寻求发行债券为资本项目筹集3.6亿美元Justin George 2021年2月10日华盛顿邮报 
 2004年，奥马茨被任命为该报的调查小组成员，在那里她帮助揭露了联邦政府在迈阿密的救灾项目中的欺诈行为。 Megan O 'Matz加入ProPublica中西部新闻编辑室|by ProPublica | 2021年2月10日| 
 “这是一项联邦命令，给全国各地的学校带来了一些真正的问题，”克莱恩在7月对哥伦比亚广播公司下属机构说。 共和党对甘蓝的战争帕特丽夏·墨菲| 2015年1月7日|每日野兽 
 至于联邦当局，他们已经提供了帮助，但神职人员没有要求特别保护。 墨西哥的牧师被标记为谋杀|杰森·麦加汉| 2015年1月7日|每日野兽 
 拉洛说，他向海关和海关执法局的工作人员报告了绑架事件，一名熟悉案情的前联邦特工证实了这一点。 一个线人，一个失踪的美国人，和华雷斯的死亡之屋:大卫·卡斯特罗12年的悬案内幕|比尔·康罗伊|月6日|每日野兽 
 据一位为即将入狱的人提供咨询的人说，在联邦教养机构待15个月会是什么样子。 一个“真正的家庭主妇”如何在监狱里生存:“我看不到[Teresa Giudice]在这里有一个简单的行走”|迈克尔·霍华德| 2015年1月6日|每日野兽 
 这意味着，当州和联邦预算紧张时，它们也属于最有可能被砍掉的项目。 如何解决警务危机|Keli Goff | 2015年1月5日|DAILY BEAST 
 选举来自纽约的代表，审议联邦宪法的举行。 《每日历史与年表》|乔尔·蒙塞尔 
 历史对他们的关注很少，联邦政府也没有给予他们应有的奖励。 《欧扎克的信使》拜伦·a·邓恩 
 客户将以联邦储备系统的名义做他们以前从未做过的事情。 《货币银行学》|Chester Arthur Phillips 
 这似乎违背了该法案的精神和意图，该法案的主要目的是集中联邦储备银行的准备金。 《货币银行学》|Chester Arthur Phillips 
 《联邦储备法案》(Federal Reserve Act)从未打算让成员银行继续维持这些储备账户。 《货币银行学》|Chester Arthur Phillips