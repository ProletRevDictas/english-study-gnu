他举起一块拳头大小的棕色沙质岩石，岩石上嵌着一个黑色的长方形脊状物体，显然是植物或贝壳碎片。 你现在可以做的11个伟大的微冒险|编辑| 2020年10月1日|户外在线 
 我们人类都有一种天生的倾向，从记忆片段中创造出一个看似合理的故事。 你不能完全相信你的记忆|大卫·林登| 2020年9月30日|大众科学 
 我仍然会不时流鼻血，而且在我的余生中都会如此——一块没有完全愈合的骨头碎片漂浮在那里。 意外谋杀未遂|尤金·罗宾逊| 2020年9月2日|Ozy 
 就连流媒体也在被Instagram、Quibi和TikTok等媒体粉碎成越来越小的碎片。 《日落大道》，破碎的梦想|David Ehrenstein | 2020年8月20日|Washington Blade 
 有时，这些漂浮在外太空的岩石碎片进入地球大气层，受到地心引力的吸引。 来自火星的陨石包含有关红色星球地质的线索|艾莉亚·乌德里| 2020年6月17日|奇点枢纽 
 这段文字是用科普特语写的，而不是希腊语，也并非如标题所示，实际上出自《圣经》。 《拆解历史:古代文献的黑幕在线交易》|Candida Moss | 2014年11月23日|DAILY BEAST 
 重达250吨的飞机残骸竟然一个也没有发现，真是令人吃惊。 MH370残骸永远消失，没有它能找到飞机吗? 克里夫·欧文，2014年9月7日，每日野兽 
 如果它分崩离析，那么“哈里发国”几乎肯定也会分崩离析。 ISIS哈里发恐怖的第一个月|查理温特| 2014年7月29日|每日野兽 
 一块碎片穿透了她的肩膀，漏了一英寸的大动脉。 以色列的应用程序红色警报拯救了生命——但它可能会让你发疯| 2014年7月15日|每日野兽 
 搜寻MH370航班的第50天来了又去，没有发现这架波音777飞机的一块碎片。 马来西亚为何隐瞒MH370报告? 克莱夫·欧文2014年4月28日 
 如果我们把商业活动的任何一小部分单独看作一个片段，这无疑是正确的。 《社会正义未解之谜》|史蒂芬·利科克 
 有点奇怪的是，在《克里斯塔贝尔》出版之前，已经出现了对这一精彩片段的结论。 注释与查询，177号，1853年3月19日|各种各样 
 一块明显来自柱状块状石头的碎片，介于碎石和致密长石之间。 澳大利亚热带和西部海岸调查叙述[卷二中]|菲利普·帕克·金 
 这段片断的作者用一种似乎属于原始时代的风格写作。 《哲学词典》第一卷(10卷)|弗朗索瓦-玛丽·阿鲁埃(伏尔泰) 
 独立的莫基人是新墨西哥古老统治种族的一部分。 陆上|约翰威廉德森林