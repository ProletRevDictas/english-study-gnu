语音并不会影响总分，但这些功能都非常强调鼓励用户根据人工智能驱动的建议来塑造自己的行为。 亚马逊的新健身追踪器可以通过你的声音来判断你的心情斯坦·霍拉泽克2020年9月2日 
 莱维恩告诉Axios，公司的战略“总体上是有效的”，“任何改变都是一个重点问题。” “‘不可阻挡的创新者’:梅雷迪思·科皮特·列文的迅速崛起，纽约时报的下一任首席执行官史蒂文·珀尔伯格，2020年8月19日 
 卡尔森说，虽然野生动物贸易是溢出效应的一个来源，但人们对它的重视往往超过了证据。 为了防止下一次大流行，我们可能需要减少砍伐树木。乔纳森·兰伯特2020年7月23日科学新闻 
 因此，现有的营销计划被搁置，为强调同理心而非商业主义的新冠病毒意识内容战略让路。 封锁的教训:用户真正参与的四种内容类型|Edward Coram James | 2020年7月20日|搜索引擎观察 
 对媒体来说，这种强调警示新闻的做法是一种商业决策。 快乐的理由(第417集)|Stephen J. Dubner | 2020年5月7日|魔鬼经济学 
 尤德尔在竞选的最后几周将重点转移到经济上，但为时已晚。 一颗共和党之星在科罗拉多州崛起，击败尤德尔，埃莉诺·克里夫特，2014年11月5日，每日野兽 
 当音乐进入讨论时，重点总是放在摇滚乐的声音和演奏者上。 The Stacks: John Coltrane的Mighty Musical Quest |Nat Hentoff | 2014年10月18日|DAILY BEAST 
 对偏执者的批评者应该开始把重点放在他的言行上，而不是他的言论和感受上。 为什么顽固顽固|Stephen Eric Bronner | 2014年9月28日|DAILY BEAST 
 他对中产阶级担忧的强调是可以理解的。 爱荷华州领跑者麦克·哈克比接受《每日野兽》采访，劳埃德·格林2014年9月22日 
 虽然人们总是强调下一个伟大的明星，但李娜已经打开了这扇门。 网球明星李娜告别球场，并质疑这项运动在亚洲的崛起 
 这是难以形容的——有点严厉，有点狂野，隐约有一种透过它窥视的野蛮人的强调。 阿尔杰农·布莱克伍德 
 有四种一般的强调形式，它们是表达特征的标志。 表达声音文化|杰西·埃尔德里奇·索斯威克 
 它是由前者，通过添加前缀，强调的意思，派生出来的。 契约条例|约翰·坎宁安 
 “沐浴在梦中情人的心血里。”他一本正经地强调说。 三个约翰·沉默的故事|阿尔杰农·布莱克伍德 
 让你的发音简单，清晰，重音正确，语调和重点与你的话语相适应。 《女士礼仪手册和礼貌手册》|弗洛伦斯·哈特利