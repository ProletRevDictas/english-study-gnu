如今，曾经私人的、孤独的长跑追求越来越成为一种公开的运动，我们比以往任何时候都更有动力为期待已久的读者记录我们的成功和失败。 Strava如何塑造我们的跑步故事|mmirhashem | 2021年10月22日|Outside Online 
 在这里，这架飞机再次成为一艘孤独的船只，机上的人面临着他们独特的命运，但不知何故，他们已经与更大的戏剧联系在一起，再次通过手机联系在一起。 2001年9月11日:一个普通的工作日，然后是恐怖和死亡的超现实场景|大卫·马拉尼斯| 2021年9月10日|华盛顿邮报 
 这条来回的小路两端都有停车场，并不费劲——你可以花时间思考，独自一人享受郁郁葱葱的徒步旅行。 10个理想的独自冒险者徒步旅行|syadron | 2021年7月30日|户外在线 
 在这种孤独的环境中，他们被期待着规划毕业后的未来。 对大多数学生来说，申请大学从来不是件容易的事。 大流行使这几乎不可能|凯蒂·赖利| 2021年3月31日|时间 
 四分之一个世纪后，杜瓦尔与寂寞鸽子的编剧威廉·d·维特里夫重新合作，拍摄了《老墨西哥之夜》。 罗伯特·杜瓦尔谈他传奇的职业生涯，他的新电影，以及他为什么要抛弃共和党|马洛·斯特恩| 2014年3月13日|每日野兽 
 许多人认为杜瓦尔主演的迷你剧《寂寞的鸽子》是有史以来最伟大的西部片之一。 罗伯特·杜瓦尔谈他传奇的职业生涯，他的新电影，以及他为什么要抛弃共和党|马洛·斯特恩| 2014年3月13日|每日野兽 
 这是一个寻找孤独的、带鼻音的音乐的博客，这些音乐听起来很古老，但其实不然。 最佳音乐博客|Howard Wolfson | 2014年1月26日|DAILY BEAST 
 您的许多作品，从《寂寞的鸽子》到《骑士》、《路过》，都被搬上了银幕。 拉里·麦克默里:我如何写作|诺亚·查尼| 2013年4月24日|每日野兽 
 它只影响了我后来选择写《寂寞的鸽子》四部曲中的其他三本书。 拉里·麦克默里:我如何写作|诺亚·查尼| 2013年4月24日|每日野兽 
 可是食槽里还没有什么吃的，箱子里似乎很寂寞，因为独眼斜眼孤伶仃的。 滑稽猪斜眼|理查德·巴纳姆 
 这只滑稽的小猪在斯利科离开它后感到相当孤独，但多亏了橡子，它不再饿了。 滑稽猪斜眼|理查德·巴纳姆 
 那幽静的地方，一棵梧桐树下的一个座位，一盏孤寂的圆弧灯完全照在上面，已经有人了。 阿里斯蒂德·普约尔的欢乐冒险|威廉·j·洛克 
 我不愿意住在这样一个寂寞的地方，而老拉什米尔，一个晚上唯一在这里的人。 他们面前的世界|苏珊娜·穆迪 
 但过了一会儿，一切似乎都静止了，弗利特福特开始感到寂寞。 后来的穴居人|凯瑟琳·伊丽莎白·多普