然而，尽管我们知道这只是一种错觉，我们都把自己的童年理想化和偶像化了。 埃里克，或者点点滴滴弗雷德里克·w·法勒 
 我所有的智慧和生命都变成了精神，去崇拜你。 朱丽叶·杜洛埃的《给维克多·雨果的情书》|路易·吉姆波特 
 孩子们崇拜他，整个街坊都崇拜他。 《匹克威克俱乐部的遗书》诉查尔斯·狄更斯(2 / 2) 
 弗雷德结婚十个月了，似乎很崇拜他的妻子。 原版便士读物|乔治·曼维尔·芬恩 
 在天气变得太热之前，他们带我出去看兵营和一块他们似乎很崇拜的摇摇欲坠的旧田地。 东方遭遇|马默杜克·皮克索尔