排盖和类似的东西可以让园丁延长两端的生长季节，保护幼嫩的新幼苗和移植物，排除昆虫和更大的害虫——包括鹿——而不用化学药品。 大冬天吃新鲜的蔬菜? 这是可能的，即使在寒冷的气候。 阿德里安·希金斯2021年1月20日|华盛顿邮报 
 如果雨季来得更早，满是幼苗或幼苗的田地可能会被淹没。 令人惊讶的是，长期的灰尘和焦油正在融化高山冰川Sid Perkins, 2020年11月17日，学生科学新闻 
 这种颗粒状的土壤会使本地植物和树苗难以生长。 跳跃的“蛇虫”正在入侵美国的森林|Megan Sever | 2020年11月9日|学生科学新闻 
 这反过来又会减少或移除森林下层植被，为生活在那里的生物或幼苗生长提供更少的营养或保护。 入侵性跳虫破坏美国土壤，威胁森林|Megan Sever | 2020年9月29日|科学新闻 
 基本上，农民提供土地和劳动力，而Komaza提供种苗、支持和收获服务。 一家专注于非洲的“微型林业”初创公司筹集了2800万美元，计划种植10亿棵树 
 “他是安比·苗德尔，就是那个有时到泰波塞斯来帮忙的小伙子，"她冷淡地解释道。 德伯家的苔丝，托马斯·哈代 
 在苗床中间套一株好壮的骨髓苗，根往下。 《潘趣》，或《伦敦查里瓦里》，第156卷，1919年4月9日 
 在圣约翰河畔的苗圃里可以买到生长三年的幼苗。 棕榈叶|哈里特·比彻·斯托 
 插进苗床的插枝比幼苗结实得快。 全年每周的室内园艺|威廉·基恩 
 受人爱戴的杰奎米诺将军的一个苗子，也是最好的苗子之一。 玫瑰上的帕森斯|塞缪尔·布朗·帕森斯