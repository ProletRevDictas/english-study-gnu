然而，倡导人士说，面对语言和扫盲障碍、移民恐惧，或者缺乏地方和州政府的推广，一些最脆弱的社区已经成为疫苗错误信息的沃土。 在拉丁裔移民中，虚假疫苗声明的传播速度就像病毒一样快 
 马里兰州议会(Maryland General Assembly)准备创建一项由州授权的制度，以补偿被错误定罪和监禁的人，消除数十年来被无罪释放者获得报酬的障碍。 马里兰州朝着明确的计划迈进，向被误判的人支付报酬，Ovetta Wiggins, 2021年2月10日，华盛顿邮报 
 使用的行话和首字母缩写甚至会让大多数政治观察家感到困惑，更不用说那些有语言障碍的人了。 早间报道:重新划分选区的问题可能会在翻译中丢失|圣地亚哥之声，2021年2月10日，圣地亚哥之声 
 我们的立场和框架是，委员会应该制定计划，既要满足规模较大的群体的需求，也要满足正在组织参加活动的较小语言群体的需求，但语言将是一个障碍。 非洲社区警告说，语言问题可能将他们排除在重划选区之外 
 相反，缺乏可获得的或可替代的资源造成了障碍。 福特的下一个大流行任务:清洁N95口罩和低成本空气过滤器|汉娜·德纳姆| 2021年2月9日|华盛顿邮报 
 我出去了，可能在大堡礁钓黑马林鱼。 Lee Marvin的Liberty Valance微笑背后的故事|Robert Ward | 2015年1月3日|DAILY BEAST 
 就像在他之前的其他打破障碍者一样，科尔弗也受到了质疑。 克里斯·科尔弗谈写作、表演和成为流行文化开拓者的痛苦|奥利弗·琼斯| 2014年12月15日|每日野兽 
 语言不是障碍; 这个星球上几乎每个人的舌头都在喋喋不休地说着什么，沉浸在邪教精心设计的神秘之中。 福尔摩斯大战开膛手杰克|克莱夫·欧文| 2014年11月16日|每日野兽 
 它被称为，相当引人注目的，打破音障。 维珍银河的灾难之路:高风险与夸张的冲突克莱夫·欧文| 2014年11月1日|每日野兽 
 他看到了一个用PVC管道覆盖的链条屏障，吉普车显然是撞坏了这个屏障，然后被卡住了。 追捕痛恨警察的宾夕法尼亚“生存主义者”迈克尔·戴利| 2014年9月17日|每日野兽 
 当然，他遭到了印度兵的射击，但马和人都安然无恙地逃了出来，他们毫不费力地跳过了那道低矮的屏障。 红年|路易斯·特雷西 
 但是轻蔑是火山而不是冰川，是性和判断之间的一个很差的屏障。 祖先|格特鲁德阿瑟顿 
 他们之间的隔阂明显又降低了，汤姆感到他失去的信心在一瞬间恢复了。 阿尔杰农·布莱克伍德 
 他们之间的谈话很少触及现实，仿佛有一道屏障使他们的声音都哑了。 阿尔杰农·布莱克伍德 
 老头儿老实人，三言两语，就把我和玛丽之间的隔阂一扫而空。 《山谷战士》纳尔逊·劳埃德