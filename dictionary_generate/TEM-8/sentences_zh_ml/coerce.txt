其中两人被指控犯有联邦恐怖主义罪，涉及破坏政府财产以恐吓或胁迫政府。 还有五人被指控与堪萨斯城的骄傲男孩们合作，他们是斯宾塞·许，蕾切尔·韦纳 
 怀孕的人历来被认为是“弱势”人群——这个称号也适用于那些可能被强迫参与研究的群体，如儿童和被监禁者。 在几乎没有数据指导的情况下，怀孕的医护人员正在加紧接种COVID-19疫苗杰米·杜charme 
 一旦你找到幽灵最喜欢的房间，你可能需要强迫他们出来，因为有些人很害羞。 你是“相位恐惧症”专家吗? 这里有一些保持恐惧新鲜感的替代规则。 Elise Favis | 2021年1月11日|华盛顿邮报 
 任何觉得被迫支付赎金的组织至少都应该分析制裁的潜在风险，特别是如果比特币支付最终被恐怖组织发现的话。 黑客与医院的“停火”已经结束，这应该会让我们感到害怕 
 她带着她的三个孩子搬到了一个新的州，并与一家非营利组织建立了联系，该组织支持被胁迫债务和家庭暴力的幸存者。 即将到来的对隐藏算法的战争，让人们陷入贫困Karen Hao bb1, 2020年12月4日，MIT技术评论 
 一位法官同事也指控麦卡弗里试图胁迫他反对卡斯提尔。 法官行为不端:伟大的美国传统|Asawin Suebsaeng | 2014年10月30日|DAILY BEAST 
 俄罗斯再次挥舞着切断天然气供应的威胁，以挤压基辅，胁迫欧洲。 2014年9月23日，乌克兰，冬天来了|安娜·涅姆佐夫娃| 
 阿利托通过质疑“反常”的Abood判例打开了大门，该判例允许各州强制工会成员缴纳会费。 哈里斯诉奎因案判决后的工会保守派案例James Poulos | 2014年7月2日 
 在其他情况下，中情局的招聘者使用几乎不加掩饰的威胁来迫使他们合作。 中央情报局试图从关塔那摩基地组织囚犯中招募间谍丹尼尔·克莱德曼2013年11月28日每日野兽 
 杰斐逊认为，宗教自由剥夺了多数人胁迫持不同意见的少数人的任何权利，即使是对宗教怀有敌意的少数人。 托马斯·杰斐逊的《古兰经》:伊斯兰教如何塑造开国元勋|R.B. 伯恩斯坦2013年9月29日 
 本宪法并不企图以其政治能力胁迫主权机构、国家。 丹尼尔·韦伯斯特演讲选集|丹尼尔·韦伯斯特 
 它引导，它不必强迫或迫使，尽管它可能。 《当代评论》，1883年1月 
 强迫他们不情愿地自我否定，这对他的愿望和希望都是不可能的。 释者圣经:哥林多人第二书信|詹姆斯丹尼 
 在空旷的沼泽和森林中继续寻找，在这寂静的荒野中四处摸索，在丛林中强求，都是无望的。 人行道尽头在哪里约翰·罗素 
 尽管我们发现休谟谴责富兰克林的行为，但他反对任何胁迫美国的企图。 大卫·休谟的生活和通信，卷二(2)|约翰·希尔·伯顿