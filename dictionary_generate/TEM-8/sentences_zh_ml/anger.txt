eBay前首席执行长维尼格(Devin Wenig)去年给一位同事发短信，对这种负面报道表示担忧和愤怒。维尼格没有受到指控。 4名前ebay员工承认网络跟踪阴谋，2020年9月23日《财富》杂志 
 过去十年中最成功的涓滴运动不是由恐惧或愤怒激发的，而是由兴奋激发的。 气候行动:恐惧并没有激励人们，所以让我们让他们兴奋起来|马修wheimer | 2020年9月22日|财富 
 马丁·路德·金为民权斗争的动机，既是出于爱，也是出于对不公正的愤怒。 哲学和心理学一致认为-对不戴口罩的人大喊大叫是行不通的lgbtq编辑| 2020年9月21日|没有直接新闻 
 此外，如果你的家庭缺乏特权，就有必要讨论一下如何处理歧视带来的愤怒。 如何和孩子们谈论冠状病毒和其他坏消息|莎拉·凯利·沃森| 2020年8月6日|科普 
 撤资运动的一个愤怒来源是，福克纳提议增加SDPD的支出，尽管该市的预算因疫情而缩减。 政治报道:度假租房? 就像过去一样! 斯科特·刘易斯和安德鲁·济慈| 2020年7月4日|圣地亚哥之声 
 如果克里斯蒂不是一个有愤怒管理问题的总统竞选人，这段插曲甚至可能不会上榜。 2014年对卑劣的人来说是令人愉快的好年|帕特里夏·墨菲| 2014年12月30日|每日野兽 
 他们都没有被她明显的愤怒所吓倒，也不认为她具有威胁性。 如果我打了白人警察的脸会怎么样? Goldie Taylor 2014年12月30日|DAILY BEAST 
 作家兼心理学家安德里亚·勃兰特写道，我们大多数人都与愤怒有着不健康的关系。 自助书籍真的能重塑你吗? Lizzie Crocker 2014年12月29日|DAILY BEAST 
 但她并没有通过公关人员悄悄发布一份声明，而是将自己的愤怒传播到很远的地方。 詹妮弗·劳伦斯的正义之怒说出了我们想说的一切|凯文·奥基夫| 2014年12月29日|每日野兽 
 愤怒通常表现为克制者的另一种自我毁灭的、但更容易被社会接受的感觉或行为，比如焦虑。 自助书籍真的能重塑你吗? Lizzie Crocker 2014年12月29日|DAILY BEAST 
 本能地，他试图隐藏痛苦和愤怒——这只会增加已经存在的距离。 阿尔杰农·布莱克伍德 
 她就止住怒气。 她以前对他的傲慢和轻蔑，现在也给抹掉了。 圣马丁之夏，拉斐尔·萨巴蒂尼 
 如果他继续忘恩负义和残忍，就说我的愤怒没有止境——我的心都碎了——会把我压死的。 布莱克伍德爱丁堡杂志，No。 CCCXXXIX。 1844年1月。 卷,LV。 |各种 
 所有这些脾气和愤怒的表现都是我在许多其他信件中向陛下指出的结果。 菲律宾群岛，1493-1898，卷XX, 1621-1624 
 大卫叔叔一时气得要命，我想他几乎要打他了。 约瑟夫·谢里丹·勒法努将军