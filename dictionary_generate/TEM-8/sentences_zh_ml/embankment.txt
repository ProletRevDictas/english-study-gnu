警官们没有意识到卡里略就在他们的上方，他在距离他们40英尺远的地方，以一个隐蔽的隐蔽位置，站在陡峭的堤坝上，瞄准着检察官说他在奥克兰用过的“幽灵”武器。 《我觉得恨超过一切》:一名现役飞行员如何试图发动内战》|作者:吉塞拉Pérez de Acha，凯瑟琳·赫德和艾丽·莱特福德，伯克利新闻调查报道项目| 2021年4月13日|ProPublica 
 这次霍歇尔避免了在路堤上滑倒，但他没能像周六那样保住标准杆。 自信和冷静，松山英树结束了日本对大师赛冠军的漫长等待查克·卡尔佩珀，斯科特·艾伦，德斯·比勒 
 我们在小溪上方陡峭的河堤上的五六处遗址中找到了一个地方，我们和这里的另外两个人(一个男人和两个女人)之间有足够的空间。 一对父子在大峡谷的背包旅行是对冒险的介绍约翰·布里利2021年4月9日华盛顿邮报 
 我爬过原木和沟渠，发现这群人正蜷缩在小溪边的河堤上。 来见见痴迷蘑菇的文艺复兴男子吧，道格·比尔德，2021年3月10日，户外在线 
 然而，开车向北行驶的伍兹穿过了中间和相反的车道，然后在路堤上休息。 罗岭山庄(Rolling Hills Estates)的居民是如何从老虎伍兹(Tiger Woods)的车祸中醒来的:哦，不，又是一起车祸。 本·戈利弗，2021年2月26日，华盛顿邮报 
 这幅画中间的透视是一个旧铁路路基的凸起。 Rackstraw Downes的艺术和散文是同一个天才的两面|比尔·莫里斯| 2014年6月4日|每日野兽 
 火车载着我离开了……我们经过一排又一排与路堤成直角的灰色贫民窟小房子。 奥威尔的谎言:他的日记揭示了真相的问题|Jimmy So | 2012年8月19日|DAILY BEAST 
 飞机降落在距离跑道约400码的一条五车道高速公路的路堤上。 特拉维斯·巴克的情感漫游|马洛·斯特恩| 2011年9月26日|每日野兽 
 “它爬上路堤，滑进泥里，气疯了，把我咬了半个小时，”克朗克痛苦地说。 戴安·戴蒙德，2011年7月1日，每日野兽 
 他写道:“今天早上，在以高速公路的速度撞上6英尺高的土堤后，我的丰田雅力士滚动了三次。” 5必读博客文章|每日野兽| 2008年10月26日|每日野兽 
 路堤或路基是由巨大的打桩开始的，非常宽阔和坚固。 看一眼欧洲|贺拉斯格里利 
 巨大而模糊的身影沿着堤岸冲来，发出奇异的声音。 遗失的英雄|伊丽莎白·斯图尔特·菲尔普斯·沃德和赫伯特·d·沃德 
 你必须振作起来，否则你会彻底疯掉的，然后你可能会从河堤上跳下去。 离奇的故事|各种 
 汽车冲过堤坝，冲毁了好几码的石墙，停在几百英尺下的山谷里。 《从一辆汽车看英国的公路和小道》|托马斯·d·墨菲 
 不一会儿，我听到了与我平起平坐的声音——对面河堤的山脊顶上，一百多英尺远。 《亚瑟王宫廷中的康涅狄格美国佬》，完成|马克·吐温(塞缪尔·克莱门斯)