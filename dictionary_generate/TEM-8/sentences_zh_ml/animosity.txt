其中最有害的一种是种族仇恨——怨恨和愤怒形成于一种信念，即其他种族的人与你不同，不能被信任，不配得到你应得的。 我们激进的共和国|玛吉·柯尔特(maggie.koerth-baker@fivethirtyeight.com) | 2021年1月25日|FiveThirtyEight 
 惠特塞尔写道，民主党人推动弹劾只是因为他们对总统的敌意，以及该党损害批评者的“长期计划”。 早间报道:格洛里亚的大计划和优先事项|圣地亚哥之声| 2021年1月15日|圣地亚哥之声| 
 一项刑事调查发现，eBay内部一直对博主怀有敌意，这些博主有时在报道中批评eBay。 2020年最大的商业丑闻|李克利福德| 2020年12月27日|财富 
 长期以来，竞争、仇恨和自我都是鸟类世界的特征。 eBird如何永远改变观鸟|Jessie Williamson | 2020年12月4日|Outside Online 
 经过数周的暴力比赛和日益增长的敌意，体育界最伟大的体育精神和尊重的展示之一到来了。 丹尼·迈耶想在一场激烈的选举后把所有人拉到谈判桌前。 Twitter说:太快了。 2020年11月9日，华盛顿邮报 
 但社区和执法部门之间的敌意并不是什么新鲜事。 弗格森展示了一个国家在与自己作战|罗兰·s·马丁| 2014年8月16日|每日野兽 
 现在看到美国和俄罗斯之间的敌意是不是很奇怪? Archer创作者Adam Reed在《Vice》第六季的“Unreboot”中，和新角色Marlow Stern | 2014年8月5日|DAILY BEAST 
 结果造成了相当多的“敌意和敌意”。 Eric Cantor是如何自残|Ben Jacobs, Tim Mak | 2014年6月11日|DAILY BEAST 
 影片中出现的正是这种由贪婪和仇恨驱动的无脑暴行。 大屠杀恐怖萦绕在电影《艾达》和《德国医生》|Jack Schwartz | 2014年5月12日|每日野兽 
 但仇恨始于20世纪20年代，当时犹太人和阿拉伯人发生冲突。 雅法:两个国度的故事|Lauren Gelfond Feldinger | 2014年2月16日|DAILY BEAST 
 希尔达急不可耐地转过头去; 他们的目光相遇了片刻，带着怀疑、挑战和敌意。 希尔达·莱斯韦，阿诺德·贝内特 
 对荷兰人的仇恨与对常备军的仇恨以及对王室拨款的仇恨交织在一起。 从詹姆斯二世即位开始的英国历史。 |托马斯·宾顿麦考利 
 即使在反复无常的时候，仇恨也是坚决的; 它没有什么伪装的能力，也没有什么假设的能力。 粗口史|朱利安·沙曼 
 因此，在他们的斗争中，并没有宗教问题所可能带来的那种敌意。 凯瑟琳·德·美第奇，奥诺雷·德·巴尔扎克 
 弗雷德里克的敌意在那时达到了顶峰，我们现在知道了疾病的全部程度。 《七宗罪:嫉妒和懒惰》欧热尼·苏