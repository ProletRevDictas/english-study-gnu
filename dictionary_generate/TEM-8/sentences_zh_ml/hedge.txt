2018年，花旗成为维权对冲基金ValueAct的目标，该基金曾推动其他公司进行重组。 花旗首席执行官简·弗雷泽的最大挑战，在一个图表上|约翰·德特里克斯| 2020年9月10日|Quartz 
 2019年1月，他正式成立了一家名为Next Alpha的对冲基金，据他说，该基金目前管理着约3000万美元。 人工智能对冲基金能跑赢市场吗? 2020年8月25日|财富 
 这促成了许多宏观对冲基金的复兴，其中一些获得了自上世纪90年代全盛时期以来从未有过的收益。 对冲基金“海盗”再次启航|丹尼尔·马洛伊| 2020年8月21日|Ozy 
 对冲基金投资公司Aurum fund Management的数据显示，今年全球宏观基金的平均表现持平。 对冲基金“海盗”再次启航|丹尼尔·马洛伊| 2020年8月21日|Ozy 
 这家对冲基金发布了它认为误导投资者的公司的报告，同时押注股价下跌。 世界上最著名的做空者表示，特斯拉交易员正在吸食睾丸激素 
 所有削减公共教育开支的钱都可以用于对冲基金的税收减免。 饥饿游戏来到纽约州公立学校|西风教育| 2014年11月26日|每日野兽 
 第三，破坏:这些对冲基金经理希望消除对特许学校的所有限制和监督。 饥饿游戏来到纽约州公立学校|西风教育| 2014年11月26日|每日野兽 
 中间偏右的对冲基金集团“第三条路”(Third Way)，以及与之相关的“蓝狗”(Blue Dogs)和跟风者。 中期选举中很少被注意到的大输家之一:全国步枪协会，克里夫·谢克特，2014年11月10日，每日野兽 
 邓纳姆开自己的玩笑，只是为了让自己能够规避风险，接受一个权威的角色。 《是时候长大了》，莉娜·邓汉姆|艾米丽·夏尔| 2014年10月10日|每日野兽 
 1998年，当对冲基金长期资本管理公司(Long Term Capital Management)破产时，纽约联邦储备银行(New York Fed)帮助组织了36.5亿美元的救助计划。 美联储对高盛令人难以置信的“懦弱”——录于录像带丹尼尔·格罗斯| 2014年9月26日|每日野兽 
 没有篱笆，家产必被抢夺。没有妻子，缺少的人必悲哀。 《圣经》，Douay-Rheims版本| 
 我必除掉他的篱笆，他就荒废。我必拆毁他的墙垣，他必被人践踏。 《圣经》，Douay-Rheims版本| 
 他夹在我们中间，我们穿过后面的田野，直得像乌鸦飞过树篱和壕沟。 《雾都孤儿》第二卷(共3卷)查尔斯·狄更斯 
 撤退的联邦军聚集在房子里、花园的每一道篱笆和树篱后面。 《欧扎克的信使》拜伦·a·邓恩 
 他们向爱德华·布鲁斯爵士的师团猛攻，后者像“茂密的树篱”或“树林”一样接待了他们。 罗伯特·布鲁斯国王| f . Murison