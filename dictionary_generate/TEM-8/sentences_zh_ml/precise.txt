我们根据风险、回报和我们身体想要感受的精确方式，把经历分类为是、不是和可能。 一个新的播客探索户外性爱|Heather Hansman | 2021年2月12日|Outside Online 
 这增加了战斗的紧张感，因为你必须非常精确。 《小噩梦2》:一个催眠的黑暗童话故事，克里斯托弗·伯德，2021年2月9日，华盛顿邮报 
 约瑟夫·科尔蒂纳的画极具绘画感，却又十分精确，显示出他对艺术的极大投入。 画廊:探索实体艺术和数字艺术之间的紧张关系|马克·詹金斯| 2021年2月5日|华盛顿邮报 
 现在，阿戈尔、多恩和同事们已经对TRAPPIST-1的质量进行了最精确的测量。 两个系外行星家族重新定义了行星系统的样子，Lisa Grossman, 2021年2月5日，科学新闻 
 亚马逊表示，政府对用户数据的要求在2020年飙升了800% Zack Whittaker, 2021年2月1日，TechCrunch 
 目标是对上个世纪——或者更准确地说，从1900年到2014年——进行文学剖析。 2014年度小说|Nathaniel Rich | 2014年12月29日|DAILY BEAST 
 我花了半个小时测量总统周围的所有尺寸，得到27个精确的尺寸，我需要制作一件真正的定制西装。 从奥斯维辛到白宫:一个裁缝的美国故事|马丁·格林菲尔德| 2014年12月5日|每日野兽 
 我觉得他非常聪明，非常温柔，说话轻声细语，一丝不苟。 万众瞩目:安杰莉卡·休斯顿:《爱，虐待》和杰克·尼克尔森的传奇女演员Alex Suskind | 2014年11月10日|每日野兽 
 医生们对阴阳人的精确定义也缺乏一致意见。 中间性与上帝千古流传|坎迪达·莫斯| 2014年11月9日 
 很难得到确切的数字，但有报道称，袭击造成数百人死亡，其中包括儿童。 像卡戴珊一样的人在恶搞阿萨德|Noah Shachtman, Michael Kennedy | 2014年10月17日|DAILY BEAST 
 这也是不准确的，因为没有涵盖这一时期的确切人口数字。 《社会正义未解之谜》|史蒂芬·利科克 
 假如总有事情可做，那对千百万不知道如何确切地做这件事的人又有什么用呢? 看一眼欧洲|贺拉斯格里利 
 在整个解剖学中，没有比学习背部肌肉的精确连接更困难的任务了。 同化记忆|马库斯·德怀特·拉罗(又名A. Loisette教授) 
 接着组织起来，国家-神和国家仪式的一般概念就更加明确和精确了。 古罗马宗教|西里尔·贝利 
 现代科学的准确性密切依赖于这些仪器来进行所有精确的检查。 地球历史概述|纳撒尼尔·索斯盖特·谢勒