但我一见到她，就觉得一开始就脱口而出这样的演讲很可笑。 查尔斯·赫伯·克拉克 
 我应该脱口而出，然后像那个叫什么来着的使者一样逃离耶户。 阿维尔·罗萨·努切特·凯里 
 一个男人会脱口而出，然后我就知道我在哪里了。 模仿者和其他故事|玛丽·e·威尔金斯·弗里曼 
 士气低落，情绪失控，他们会脱口而出真相，从而使自己有罪。 《第三程度》|查尔斯·克莱因和亚瑟·霍恩布罗 
 有时他想，如果把真相脱口而出，然后被放逐，那就更能忍受了。 火焰的阴影|爱美丽河