lsd的所有奇怪之处——幻觉、迷幻和其他一切——都可能是血清素释放的生化封锁的直接后果。 做梦就像服用LSD -第95期:逃脱安东尼奥·扎德拉和罗伯特·斯蒂克戈德| 2021年1月14日|诺第留斯 
 在克利福德·霍华德吞下三颗仙人掌后大约一个小时，幻觉开始出现。 《普里蒂》是好莱坞最具争议的电影之一。 见见它的华盛顿撰稿人。 约翰·凯利2021年1月11日|华盛顿邮报 
 没有执行的视觉是幻觉，但没有视觉的执行就像仓鼠转轮。 西蒙·斯涅克说:向前走，做个了不起的人! 尤金·罗宾逊2020年10月9日|Ozy 
 这些症状不像所谓的阳性症状那么明显，阳性症状表明存在一些额外的东西，比如幻觉。 机器可以发现心理健康问题——如果你交出你的个人数据 
 他们不会明确提到这些幻听，但他们确实会在日常对话中更多地使用相关词汇——“声音”、“听到”、“吟诵”、“大声”。 机器可以发现心理健康问题——如果你交出你的个人数据 
 我们如何对他们的经历进行分类，并将其与幻觉或精神崩溃区分开来? Eben Alexander有天堂的GPS |Patricia Pearson | 2014年10月8日|每日野兽 
 这种幻觉在视觉上是不连贯的，要么是文本的粗略近似，要么是字母的随机组合。 敲天堂的门:死亡时刻无法解释的离奇经历的真实故事|帕特里夏·皮尔森| 2014年8月11日|每日野兽 
 有时会让人困惑; 什么是真实，什么是幻觉并不总是那么清楚。 Lady Gaga, Avril Lavigne &更多本周最佳音乐录影带(视频)|Victoria Kezra | 2013年8月25日|DAILY BEAST 
 但这是另一种幻觉，因为，事实上，它们并没有。 大卫梦游仙境|侯赛因·伊比什| 2013年3月20日|每日野兽 
 整部电影完全是直来直去的，没有任何迹象表明它是梦境、幻觉或脱离现实。 12个最荒谬的“30岁摇滚”时刻(视频)|凯文·法伦| 2013年1月31日|每日野兽 
 小女仆叫他德拉克，认出了他手里的花，就活在这个幻觉里。 查尔斯·阿尔弗雷德·唐纳 
 但是，与我眼前的奇异幻觉相比，即使是这个伟大的奇迹也显得很自然。 凯瑟琳·德·美第奇，奥诺雷·德·巴尔扎克 
 我比以往任何时候都更充分地询问了他，以便掌握他产生幻觉的事实。 吸血鬼| Bram Stoker 
 她有一种感情上的幻觉，就像其他人有生理上的幻觉一样。 《快乐之子》|加布里埃尔·达农奇奥 
 这个年轻人似乎是由于发烧而产生了幻觉。 猎虎者梅恩·里德