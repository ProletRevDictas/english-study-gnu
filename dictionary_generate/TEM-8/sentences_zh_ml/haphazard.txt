随着时间的推移，毛茸茸的小动物可能会在这个随意的庇护所下居住，把你的院子变成猫头鹰来访的可靠食物来源。 把你的院子变成猫头鹰王国|约翰肯尼迪| 2021年7月29日|大众科学 
 虽然数据还没有公布，但与那些由制造商和服务主导的、相当随意的充电建筑相比，这是一个大型的基础设施实验。 印第安纳州想要无线给电动汽车充电的磁性高速公路 
 然而，谷歌的执行可能是随意的，这让他们感到担忧。 谷歌的三振出局广告政策不是问题所在，让广告商担心的是政策应用 
 相比之下，影子诉讼案件往往在短短几天内就能得到裁决——这意味着如果法官不够谨慎，它们往往会导致草率的决策。 在刚刚结束的最高法院任期中，有3个赢家和3个输家 
 所有这一切都将产生后果，因为各国现在正争先恐后地以一种毫无章法的方式决定自己的政治前途。 推迟的人口普查如何影响选区重划|杰弗里·斯凯利(geoffrey.skelley@abc.com) | 2021年6月28日|FiveThirtyEight 
 尽管2000年就实施了严格的环境控制措施，但执行起来一直很随意。 再见,巴哈马群岛。 你好,哈瓦那! Clive Irving | 2014年12月18日|DAILY BEAST 
 指挥和控制的缺失意味着地面上的作战协调只是随意的。 奥巴马在叙利亚问题上的时间不多了|杰米·德特默| 2014年10月30日|每日野兽 
 我漫无目的地走了一小段路后，遇到了一个坐着雪橇的农民。 书包:如果不太可能的旅行书|肖恩威尔西| 2014年9月4日|每日野兽 
 然后，当然，你的整个营被转移到随意的伊拉克陆军基地。 无论你做什么都会有人死。 关于伊拉克不可能的选择的一个小故事|Nathan Bradley Bethea | 2014年8月31日|DAILY BEAST 
 邻近的城邦雅典在杀婴问题上则更为随意。 与残疾生活在黑暗时代|Elizabeth Picciuto | 2014年7月22日|每日野兽 
 如果一个人一开始就随遇而安，他就会花很长时间找到自己的方向，浪费很多宝贵的时间。 《从一辆汽车看英国的公路和小道》|托马斯·d·墨菲 
 他们都笑了，但克罗泽知道，这个善于观察的赌农并不是在随便说话。 You Never Know Your Luck .完成|Gilbert Parker 
 “你的声音真奇怪，”他说，没有注意到她的问题，用他那随意跳跃的方式。 欧文·约翰逊 
 他以一种随意的、无拘无束的方式来到新奥尔良，前往墨西哥。 安布罗斯·比尔斯书信|安布罗斯·比尔斯 
 第一次引起人们注意的是一本随意翻开的《圣经》上的一段文字，它被认为是预示未来的。 女巫、术士和魔术师|威廉·亨利·达文波特·亚当斯