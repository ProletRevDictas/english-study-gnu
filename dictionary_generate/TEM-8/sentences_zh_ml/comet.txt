一个世纪后，当一颗携带疾病和社会动荡的彗星颠覆世界时，非洲未来主义可能比以往任何时候都更有意义。 这些是2020年网络上我们最喜欢的科技故事 
 流星雨发生时，飞过的彗星会留下灰尘痕迹，所以当地球穿过这些痕迹时，灰尘撞击大气层，燃烧起来，我们就看到了流星。 《天空观察》:1月天空中发生了什么|布莱恩·p·弗雷德兰德小| 2020年12月26日|华盛顿邮报 
 星子的外层盘——基本上就是一大堆剩余的彗星——几乎被完全清除了。 木星和土星在一起的景象是一件美丽的事情-如此浪漫的事实Sean Raymond & Sebastiaan Krijt | 2020年12月21日|鹦鹉鹦鹉 
 当彗星的冰在阳光下蒸发时，尘埃颗粒也会飞离彗星。 12月令人惊叹的双子座流星雨诞生于一颗不起眼的小行星|Ken Croswell | 2020年12月2日|科学新闻 
 科学家们在100多年前就知道彗星会引发流星雨，但直到1983年，研究人员才掌握了将双子座和一颗小行星连接起来的技术，这颗小行星被称为法厄同，因为它的摆动距离太阳非常近。 2020年11月29日，科学新闻 
 他们发现彗星67P上的水中大约有1900个氢原子对应一个氘原子。 彗星是地球海洋的起源吗? |Matthew R. Francis | 2014年12月14日|DAILY BEAST 
 探测器似乎位于彗星上的“悬崖”底部，但除此之外就很难判断了。 地球人，我们登上了一颗彗星|马修·r·弗朗西斯| 2014年11月12日|每日野兽 
 基本的方法按照计划进行:从罗塞塔轨道器分离后，“菲莱”缓慢地滑行到彗星表面。 地球人，我们登上了一颗彗星|马修·r·弗朗西斯| 2014年11月12日|每日野兽 
 登陆任何其他星球都是困难的，但彗星67P尤其具有挑战性，即使不考虑它的低重力。 地球人，我们登上了一颗彗星|马修·r·弗朗西斯| 2014年11月12日|每日野兽 
 这颗彗星的形状基本上像一只橡胶鸭子，但表面要粗糙得多。 地球人，我们登上了一颗彗星|马修·r·弗朗西斯| 2014年11月12日|每日野兽 
 “彗星”号是第一艘在阿肯色河上航行的汽船。 《每日历史与年表》|乔尔·蒙塞尔 
 当他到达桥上时，汽缸发出了轻柔的呜呜声，彗星号像一辆特快列车一样行驶着。 汽车马特的“世纪”跑|斯坦利r马修斯 
 在这里，马特带着卡普一起来，即使是在彗星方面速度有所下降的情况下，他的智慧也得到了体现。 汽车马特的“世纪”跑|斯坦利r马修斯 
 杰姆，那辆敞篷跑车的司机，就在彗星号开始加速的时候，撞进了灌木丛。 汽车马特的“世纪”跑|斯坦利r马修斯 
 由于所有的备用动力都投入到机器中，彗星号在山上艰难地工作着。 汽车马特的“世纪”跑|斯坦利r马修斯