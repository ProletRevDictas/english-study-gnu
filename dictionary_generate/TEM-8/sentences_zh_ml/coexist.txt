根据环境的不同，这些特征通常是共存或变化的。 本周的TikTok: TikTok是如何知道我患有多动症的? 2021年2月9日|Vox 
 最终，酸奶被重新整合，两者在糊状的平衡中共存。 用一个泔水冻糕开始你的一天Elazar Sontag | 2021年2月4日|食客 
 自从有女性向我汇报工作以来，我就一直鼓励每周工作四天，必要时在休息时间上班，这样工作和家庭生活就可以共存。 由于COVID-19，女性失去的工作数量不成比例。 以下是我们如何开始修复一个破碎的系统|邦妮·汉默| 2021年1月25日|时间 
 新的Alexa定制助手产品于周五发布，可以与Alexa助手共存和合作。 亚马逊的最新产品允许公司为汽车、应用程序和视频游戏构建自己的Alexa助手 
 要使多个模式和谐共存，有一些基本规则。 如何在你的家里融入7种流行的图案，根据专家Marissa Hermanson 2021年1月12日华盛顿邮报 
 毕竟，在我的车尾贴上“共存”贴纸，只有一个可靠的信息可以传递出去。 共存的傻瓜保险杠贴纸政治|迈克尔·舒尔森| 2014年12月21日|每日野兽 
 奇怪的是，共存贴纸本身很好地说明了这一点。 共存的傻瓜保险杠贴纸政治|迈克尔·舒尔森| 2014年12月21日|每日野兽 
 在“共存”阵营的心态中，那些抽象的信仰变成了扭曲的东西，包裹着仇恨。 共存的傻瓜保险杠贴纸政治|迈克尔·舒尔森| 2014年12月21日|每日野兽 
 换句话说，共存贴纸可能暗示着对全球爱的渴望。 共存的傻瓜保险杠贴纸政治|迈克尔·舒尔森| 2014年12月21日|每日野兽 
 这对两人来说是一种相当大的恭维，他们可以真诚地喜欢对方，也真诚地不喜欢对方——但却能共存。 真正的吉姆·帕尔默请站起来|汤姆·Boswell | 2014年9月27日|每日野兽 
 月经过多和出血通常有相同的原因，它们经常共存。 畜牧医学论文集|奥斯汀·马利 
 所有的细节都必须在我们的记忆中共存，就像一幅画的各个部分在我们眼前共存一样。 英语:写作与文学|W。 韦伯斯特 
 这些只能与自由共存; 因为民主制度比贵族制度更有利于广大市民的集会。 历史的灯塔之光，第一卷|约翰·洛德 
 同一原因的协同效应自然地相互共存。 逻辑系统:推理与归纳|约翰·斯图亚特·密尔 
 在这里，就像在其他文明民族的神圣传说中一样，不同层次的神话和宗教思想并存。 神话、仪式与宗教，第一卷|