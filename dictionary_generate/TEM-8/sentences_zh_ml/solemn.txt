这件不那么庄严的纪念品是Target画廊18位艺术家的“一年”的特色，这是一个由南希·戴利(Nancy Daly)策划的2020-2021年时间胶囊。 画廊:一系列的照片创造了图形的连续性|马克·詹金斯| 2021年8月27日|华盛顿邮报 
 法诺内说，警察们被告知中场休息时将举行一个仪式——一个庄严的荣誉和敬意的游行，就像我们在美国创造英雄的那种仪式。 Mike Fanone不能忘记的事|Molly Ball | 2021年8月5日|时间 
 作为当选的议员，我们有庄严的责任和义务来制定和颁布政策，永久地结束无家可归的危机。 我住在车里，现在我进了国会。 我们需要解决美国的住房危机。 Cori Bush, 2021年7月30日 
 这是承认美国种族不平等的一个即兴而庄严的姿态。 美国人如何看待体育赛事中的国歌? 这取决于你问的是哪个美国人。 迈克尔·李，斯科特·克莱门特，艾米丽·古斯金，2021年5月21日，华盛顿邮报 
 例如，在庄重的场合或专业的工作场所，狗狗穿的衣服有一种固有的愚蠢，可能会显得不尊重人。 给你的狗狗穿上滑稽的衣服|布莱尔·布雷弗曼| 2021年4月11日|户外在线 
 他们站成一排，肃穆地肃立在一起，而刘家则留在屋里。 葬礼抗议对纽约警察局老板来说太过分了|迈克尔·戴利| 2015年1月5日|每日野兽 
 斯基德莫尔有些腼腆地承认，“理查德将以壮观的方式打破这个庄严的誓言。” 三个迪克:切尼，尼克松，理查德三世和声誉恢复的艺术克莱夫·欧文| 2014年7月27日 
 游行是庄严的，伴随着虔诚的音乐和两个唱诗班的对应式歌唱。 第一批庆祝独立日的美国人是摩拉维亚和平主义者琳达·c·布林森| 2014年7月4日|每日野兽 
 没有贬损，没有开玩笑，没有轻浮——他非常严肃，他的眼睛注视着远远超出我后脑勺的某个地方。 看披头士成为披头士是什么感觉-尼克·科恩记得|尼克·科恩| 2014年2月9日|每日野兽 
 第三，他开始写印度风格的歌曲，都是咖喱粉和泰姬陵的纪念品，非常庄严。 看披头士成为披头士是什么感觉-尼克·科恩记得|尼克·科恩| 2014年2月9日|每日野兽 
 乔微笑着望着她，他的脸虽然年轻，眼睛后面也闪烁着新燃起的希望之火，但仍然庄严而严肃。 乔治·w·(乔治·华盛顿)奥格登 
 因此，他坚决宣布她将因作伪证而面临牢狱之苦。 乔治·w·(乔治·华盛顿)奥格登 
 他们正准备在他最后的告别仪式上庆祝tabagie，或一场庄严的宴会。 耶稣会的关系和联合文件，卷二:阿卡迪亚，1612-1614 |各种 
 请尽快通知投降的消息，以便对这一事件进行应有的、郑重的宣传。 菲律宾群岛，约翰·福尔曼 
 那只举起的手，那有力的沉默，那一百双眼睛的庄严注视，使老人难以忍受。 《山谷战士》纳尔逊·劳埃德