剪线绳的人并不满意，因为他们不知道凯恩的最后一句话为什么是“玫瑰花蕾”，他们派了一名记者去采访凯恩的前朋友和同事，想弄清楚这个问题。 大卫·芬奇的《曼克》背后错综复杂的历史——以及它与公民凯恩的联系——解释了艾丽莎·威尔金森2020年12月4日 
 我整理了一个集锦集，记录了我生命中最珍视的时刻。 每个感恩节，我的心理健康策略都是回忆难忘的个人时刻|鲍勃·布罗迪| 2020年11月23日|华盛顿邮报 
 就像早期流行的热门电影《最后的舞蹈》(讲述迈克尔·乔丹与芝加哥公牛队的比赛)一样，《崛起的凤凰城》是一部永不停歇的精彩影片。 今年秋天将播放3部新纪录片|艾琳·伯格| 2020年9月21日|Outside Online 
 如果你是钓鱼的新手，你可以找到便宜的纺丝棒组合，它有杆和卷轴，对初学者来说很容易使用。 今年秋天尝试三个适合家庭的冒险|户外编辑|020年9月17日|户外在线 
 和TikTok的“给你的页面”一样，Reels的重点是发现新内容，用户——包括那些企业拥有的用户——可以选择与更广泛的Instagram观众分享他们的reel。 “首先建立一个社区”:Instagram Reels目前还没有为广告商提供什么——2020年8月6日 
 《少年时代》就像在看一部12季的电视剧被压缩成3小时的集锦。 黑色的“少年时代”总是黑人优先，男孩后|Teo Bugbee | 2014年8月30日|DAILY BEAST 
 在拍摄过程中没有人受伤，但从最后的花絮卷轴来看，有很多的重生。 每日病毒视频:终结者2，侠盗猎车手V Style |Alex Chancey | 2014年6月6日|DAILY BEAST 
 3月26日，比伯推出了他的丑闻系列中最热门的一首歌曲。 贾斯汀·比伯的糟糕之年:涉嫌卖淫，鲁莽驾驶，以及更多|艾米·齐默尔曼| 2013年12月13日|每日野兽 
 另一方面，这是一部精彩的对话卷。 《衰姐们》第三季预告片首发。 它是最能引起共鸣的吗? 凯文·法伦2013年11月22日每日野兽 
 Reel带着最初驱使du Chaillu的好奇心和冒险精神回顾了他的生活和工作。 本周热读:2013年3月11日|Mythili Rao | 2013年3月11日|DAILY BEAST 
 我对他们总有一种不好的感觉; 我不想靠近他们，也不想让他们靠近我。 雷蒙娜，海伦·亨特·杰克逊 
 大屠杀是可怕的，冲锋的纵队停了下来，摇摇晃晃，然后开始后退。 《欧扎克的信使》拜伦·a·邓恩 
 一个醉汉会从一边滚到另一边，直到他掉进地窖的活板门，掉进阴沟，或者掉进海里。 更糟糕的是亚历山大·兰格·基兰德 
 劳伦斯看到他的一个手下趔趄了一下，然后向前倒了下去，紧紧抓住了他的马脖子。 《欧扎克的信使》拜伦·a·邓恩 
 成品被送到皮带驱动的卷绕卷盘上。 知识的神奇之书|各种各样