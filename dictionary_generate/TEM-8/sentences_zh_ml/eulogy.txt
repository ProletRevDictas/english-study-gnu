1986年邓家先去世时，杨德昌为这位为中国的核防御事业奉献了一生的朋友写了一篇感人的悼词。 几个世纪以来，中国的现代化之路都经过了我的家乡阳阳城 
 虽然曼钦还没有准备好为拖延演讲悼词，但你可以在这位西弗吉尼亚州议员身上看到对华盛顿的失望程度，这与他在2018年寻求连任的决定受到的威胁是一样的。 为什么克里斯滕·西内马可以控制Filibuster的未来|菲利普·埃利奥特| 2021年6月4日 
 乔丹经常被邀请发表演讲、祝酒词、悼词——他从未让人失望。 弗农·乔丹是如何成为独一无二的华盛顿人物|罗克珊·罗伯茨| 2021年3月3日|华盛顿邮报 
 麦克伦顿本人是一名浸信会牧师，他鼓足了勇气，为儿子的悼词布道。 联邦快递没有解决已知的危险，一名临时工在工作中死亡。 罚款:7000美元。 |作者:邓迪·c·托马斯，MLK50:通过新闻实现正义| 2020年12月23日|ProPublica 
 数以百万计的人正在死去，但手机是一种工具，让这些人更真实，利用这些空间来创作悼词，记录和拍照。 我们表达对陌生人的悲痛的方式正在改变谭雅·巴苏，2020年12月3日，麻省理工科技评论 
 在对罗莎·帕克斯的悼词中，杰西·杰克逊给美国人民以及当时的总统乔治·w·布什上了一堂历史课。 大卫·马斯乔特拉| 2014年8月23日|每日野兽 
 他说:“在我生命中的任何时刻，除了致悼词，我都将努力让人们发笑。” Pol的“恶搞广告”瞄准lolz和参议院席位|Olivia Nuzzi |014年4月10日b| DAILY BEAST 
 两周后，这篇悼词在阿灵顿国家公墓发表。 一个值得相信的人:海军军士长亚伦·托里安的悼词艾略特·阿克曼| 2014年3月5日|每日野兽 
 她的儿子托马斯(Thomas)会在悼词中说，“她的任何一个有执照的十几岁孩子都是可以招募的对象。” 纪念马洛瑞斯，10个孩子的母亲克里斯蒂诽谤赢得他的第一次选举|迈克尔戴利| 2014年1月23日|每日野兽 
 悼词还没开始就结束了，萨克雷几乎没有被提及，更不用说受到尊敬了。 约翰·萨瑟兰的有趣的小文学史|马尔科姆·福布斯| 2013年11月29日|每日野兽 
 当她舒舒服服地坐在他那把最深的椅子上时，她听见那少女般的浅嗓音开始赞美风景。 祖先|格特鲁德阿瑟顿 
 这大大激起了我的竞争，我成功地找到了一些她忘记的颂词的理由。 查尔斯·波德莱尔《他的一生 
 我急忙用一种更受欢迎的腔调，粗鄙地赞美我对手的好品味。 新马基雅维利|赫伯特·乔治·威尔斯 
 但对死者的悼词是这所学校创始人的伙伴和朋友的适当职责。 教育话题与制度的思考乔治·s·布特维尔 
 布拉德福德在书面悼词中称他为“一个真正牧师的温柔的爱和虔诚的关怀”。 普利茅斯的威廉·布拉德福德和阿尔伯特·黑尔·帕朗柏