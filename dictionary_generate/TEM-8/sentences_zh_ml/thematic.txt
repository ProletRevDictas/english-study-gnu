当你考虑到目前有多少电视剧的主题超出了他们的理解范围时，这是一种令人耳目一新的感觉。 闪亮的Starz喜剧《奔跑世界》为30多岁的人单身生活|朱迪·伯曼| 2021年5月16日|时间 
 虽然游戏中有一些奖励性的支线任务可以加快这一过程，但除了延长游戏时间外，购买游戏中数十种武器中的每一种似乎并不能达到任何叙事或主题目的。 《尼尔复制人》是一张实验性的、令人心碎的流行音乐专辑，是关于一个电子游戏的 
 这是一个充满戏剧性和主题潜力的设置。 雄心勃勃、参差不齐的卢瑟福瀑布能像“好地方”之于哲学一样，为美国历史做出贡献吗? 朱迪·伯曼，2021年4月22日 
 大约两个月前我收到的一封邮件缺乏其他邮件的乐观主题。 我的生活被一次癌症诊断颠覆了35年。 医生刚告诉我我被误诊了。 Jeff Henigson 2021年3月26日|华盛顿邮报 
 我认为就我们如何看待标识符而言，它最终会变得更加主题化。 媒体简报:出版商正在改变他们的付费墙|Tim Peterson | 2021年3月25日|Digiday 
 当他第一次向我推荐《黑暗骑士》的主题时，我喜欢这个想法，但我觉得它可能有点牵强。 克里斯托弗·诺兰·Uncut:《星际穿越》，本·阿弗莱克的蝙蝠侠，以及人类的未来| 
 展览探索了四条主题道路，第一个是我们最早的祖先的路线。 当沙特阿拉伯统治世界|Emily Wilson | 2014年10月31日|DAILY BEAST 
 例如，家庭暴力为几代作家和电影制作人提供了主题素材。 土耳其猖獗的家庭暴力问题|艾米丽·费尔德曼| 2014年7月18日|每日野兽 
 你觉得《守望尘世》和《迷失》在主题上有相似之处吗? 从“迷失”到“被提”:主创Damon Lindelof和Tom Perrotta在HBO的《守望尘世》| 
 《Do It Again》的凝聚力是音乐、音波还是主题? Robyn和Royksopp的夏日配乐“Do It Again”|安德鲁·罗马诺| 2014年5月27日|每日野兽 
 主题材料几乎完全是衍生和模仿，一种无与伦比的平庸和压抑。 音乐肖像|保罗罗森菲尔德 
 有些完整的段落仅仅是为了服从某些学术上对主题倒置和变形的要求而存在。 音乐肖像|保罗罗森菲尔德 
 她的作品并非全部出版，但都表现出良好的主题材料和不同寻常的音乐形式感。 女性在音乐中的工作|阿瑟·埃尔森 
 主题的阐述只有暗示，除了在最后一个精心设计的中间乐章。 莫扎特的一生，卷二(三)|奥托·雅恩 
 这在主题阐述占主导地位的部分表现得最为明显，这些部分比迄今为止更加丰富和自由。 莫扎特的一生，卷二(三)|奥托·雅恩