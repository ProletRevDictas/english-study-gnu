Talk Hiring根据面试者的回答给他们打分，考虑他们的回答是否包含问题、行动和结果，以及他们说话的节奏和音量等因素。 找工作吗? 下面是如何编写AI会喜欢的résumé。 Sheridan Wall, Hilke Schellmann, 2021年8月4日，MIT科技评论 
 许多经常使用麦克风的主播、广播员、采访对象、音乐家和其他人可能需要从他们熟悉的位置调整来适应这一点。 Samson Q9U评论:一个值得工作室使用的移动友好麦克风|Tony Ware | 2021年7月20日| 
 史蒂夫·马丁(Steve Martin)和马丁·肖特(Martin Short)通过嘲讽主持人颠覆了脱口秀嘉宾的剧本，而扎克·加利菲亚纳基斯(Zach Galifianakis)作为热门访谈者每次出场都搞砸了，他要么使劲拉舞台的窗帘，要么从奥布莱恩(O 'Brien)的书桌上冲过去。 柯南·奥布莱恩的深夜秀恰到好处地以他作为受人喜爱的笑柄结束了，笑柄是Inkoo Kang, 2021年6月25日，华盛顿邮报 
 大多数受访者将这些体验归因于冥想，但值得注意的是，研究作者没有排除其他因素，也没有确定冥想导致了这些体验。 冥想并不总是能让人平静下来。 对少数人来说，它可能会导致精神病。 Claire Maldarelli, 2021年6月21日| 
 90后已婚受访者已经适应了独生子女文化，对生二胎的可能性采取了“观望”的态度。 中国的三胎政策不太可能受到职业女性的欢迎| lgbtq编辑| 2021年6月7日|没有直人新闻 
 作为一个和蔼可亲的受访者，他非常愿意谈论表演技巧，他广泛的银幕角色，以及更多。 最伟大的性格演员:盖伊·皮尔斯谈他辉煌的职业生涯，从《普莉西拉》到《漫游者 
 《我为喜剧狂》后来的一个小品中，瑞秋·德拉奇(Rachel Dratch)饰演的“沃尔特斯”(Walters)面试官鼓励她的面试者“glerg”。 别记得芭芭拉·沃尔特斯的《观点》|蒂姆·蒂曼| 2014年4月8日|每日野兽 
 一旦这个问题被解决，一旦被采访者离开绿色房间，它就会被擦干净。 克莱夫·欧文，|，2013年9月1日|，每日野兽 
 用一位受访者的话来说:“我相信美国和奥巴马总统会信守诺言。” 不只是莎拉·西尔弗曼支持奥巴马|Emily L. Hauser | 2012年10月22日|DAILY BEAST 
 作为记者，我们小心翼翼地与被采访者保持着距离。 划清界限:以色列如何抹杀自己|Gershom Gorenberg | 2012年3月26日|每日野兽 
 不管怎样，面试官和被面试者都尽可能地避免这个难听的词。 《奴隶叙事:美国奴隶制的民间历史，摘自对前奴隶的采访:第二卷，阿肯色州叙事，第二部分|工作计划管理》