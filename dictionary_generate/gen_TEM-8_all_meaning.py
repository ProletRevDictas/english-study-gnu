f=open('TEM-8 tables.txt','r');l=f.read().replace('*','').split('\n');f.close()
t={}
for a in l:
    if not a:continue
    b=[c.strip().split('(')[0].split(')')[0].split('/')[0].split('ˈ')[0].strip().split('，')for c in a.split('[')[0].split(',')]
    try:d=a.split('||')[0].strip().split(']')[1].strip()
    except:
        if a.split(' ')[0]in['add-on']:d=a.split(' ')[1].strip()
        elif a.split(' ')[0]in['arbitrary','astound','casino','categorize','Christian','continual','courtesy','deafen','draft','emergent','humanity','presumption']:d=a.split('/')[1].strip()
        elif a.split(' ')[0]=='aversion':d=a.split('/ əˈvɜːʃən ')[1].strip()
        elif a.split(' ')[0]=='chaotic':d=a.split('/ keɪˈɒtɪk ')[1].strip()
        elif a.split(' ')[0]=='correspondence':d=a.split('/ˌkɒrɪˈspɒndəns ')[1].strip()
        elif a.split(' ')[0]=='deadlock':d=a.split('/ ˈdedlɒk ')[1].strip()
        elif a.split(' ')[0]=='disagreement':d=a.split('/ˌdɪsəˈɡriːmənt ')[1].strip()
        elif a.split(' ')[0]=='distaste':d=a.split('/ dɪsˈteɪst ')[1].strip()
        elif a.split(' ')[0]=='elevation':d=a.split('/ˌelɪˈveɪʃən ')[1].strip()
        elif a.split(' ')[0]=='false':d=a.split('/ fɔːls ')[1].strip()
        elif a.split(' ')[0]=='metal':d=a.split('/ ˈmetəl ')[1].strip()
        elif a.split(' ')[0]=='painter':d=a.split('/ ˈpeɪntə ')[1].strip()
        elif a.split(' ')[0]=='plateau':d=a.split('/ ˈplætəʊ ')[1].strip()
        elif a.split(' ')[0]=='rival':d=a.split('/ˈraɪvəl ')[1].strip()
        elif a.split(' ')[0]=='rusty':d=a.split('/ˈrʌstɪ')[1].strip()
        elif a.split(' ')[0]=='sadness':d=a.split('/ˈsædnrs')[1].strip()
        elif a.split(' ')[0]=='sandal':d=a.split('/ ˈsændəl ')[1].strip()
        elif a.split(' ')[0]=='sod':d=a.split('/sɒdn. ')[1].strip()
        elif a.split(' ')[0]=='theologian':d=a.split('/ ˌθɪəˈləʊdʒən ')[1].strip()
        elif a.split(' ')[0]=='uninviting':d=a.split('/ ˌʌnɪnˈvaɪtɪŋ ')[1].strip()
        elif a.split('(')[0]in['CAD','e. g. ','HIV ','HRH','lb ']:d=a.split(')')[1].strip()
        else:print(a);quit()
    for c in b:
        for v in c:
            t[v]=d
l=[]
for a in t.keys():l.append('::'.join([a,t[a]]))
l.sort(key=lambda x:x[0])
t='\n'.join(l)
f=open('TEM-8 meaning.txt','w+');f.write(t);f.close()
