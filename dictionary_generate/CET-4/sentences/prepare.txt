The timing of Fraser’s promotion may have been helped along by federal regulators, who are preparing to reprimand Citigroup over its risk-management systems, the Wall Street Journal reported Tuesday. ‘It has to have an impact’: What Citi’s new CEO means for other women on Wall Street |Maria Aspan |September 16, 2020 |Fortune 

The departure of Jacques, 48, who joined Rio in 2011, comes amid wider upheaval in the top ranks of the mining industry, as the sector prepares for a longer-term slowdown in demand from China and navigates moves to decarbonize the global economy. The CEO of mining giant Rio Tinto is forced out following the destruction of an ancient archeological site |Bernhard Warner |September 11, 2020 |Fortune 

This calendar includes 18 months, so prepare to use it for a while. Desk calendars to organize your life |PopSci Commerce Team |September 10, 2020 |Popular-Science 

It’s easy to prepare and consume in a few minutes and you can eat more of what you want for your actual meal. BCAA supplements can enhance your workout, but should you take them? |Amy Schellenbaum |September 10, 2020 |Popular-Science 

For much of that summer, his staff had been preparing to sign an international energy agreement during a summit in Warsaw. Rick Perry’s Ukrainian Dream |by Simon Shuster, TIME, and Ilya Marritz, WNYC |September 10, 2020 |ProPublica 

Adults prepare food and drink dark sweet tea on the doorsteps of their homes as they watch their children playing. The Brothers Who Ambushed ISIS |Mohammed A. Salih |December 27, 2014 |DAILY BEAST 

Prepare a large bowl with water and ice along with a strainer. Make Carla Hall’s Crispy Shallot Green Bean Casserole |Carla Hall |December 27, 2014 |DAILY BEAST 

Hitchcock's going on about English pork butchers and how best to prepare pork cracklings. Alfred Hitchcock’s Fade to Black: The Great Director’s Final Days |David Freeman |December 13, 2014 |DAILY BEAST 

One morning I arrive about nine to prepare for our morning meeting. Alfred Hitchcock’s Fade to Black: The Great Director’s Final Days |David Freeman |December 13, 2014 |DAILY BEAST 

Prepare for takeoff, because quality vacation time will certainly boost your mood. 9 Ways to Cope With Seasonal Affective Disorder |DailyBurn |December 5, 2014 |DAILY BEAST 

She had left her chair, meaning to go indoors and prepare for supper before Tony actually arrived. The Wave |Algernon Blackwood 

Prepare the table, behold in the watchtower them that eat and drink: arise, ye princes, take up the shield. The Bible, Douay-Rheims Version |Various 

That will give us time to turn about us, and to prepare ourselves against similar unpleasant casualties. Blackwood's Edinburgh Magazine, No. CCCXXXIX. January, 1844. Vol. LV. |Various 

For three days Black Sheep was shut in his own bedroom—to prepare his heart. Kipling Stories and Poems Every Child Should Know, Book II |Rudyard Kipling 

Lawrence, when the day was lost, rode back to prepare the hapless Europeans in the city for the hazard that now threatened. The Red Year |Louis Tracy