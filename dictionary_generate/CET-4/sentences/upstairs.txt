On the night Al Capone died, she remembers her mother leading her upstairs to his room and her father lifting her onto the bed. Mobster Al Capone’s granddaughters are selling his possessions, partly out of fear of wildfires |Marisa Iati |August 25, 2021 |Washington Post 

My main focus is to balance and step IN BETWEEN lines, never on one … Of course going downstairs you step in between every line but upstairs you skip every other bar. Our brains exist in a state of “controlled hallucination” |Matthew Hutson |August 25, 2021 |MIT Technology Review 

This allowed for two bedrooms upstairs and a bathroom where only short people could stand up to take care of business. Buying your first home |Valerie Blake |August 20, 2021 |Washington Blade 

A cordless vacuum cleaner lets you clean around furniture and move upstairs without tripping over a cord. Stick it to dirt and dust with the best vacuum on the market |Florie Korani |July 28, 2021 |Popular-Science 

I found the mode especially useful at times when I suspected my wife was about to call me upstairs to hold the baby. Jaybird Vista 2 review: Customizable sound for all athletes |Billy Cadden |June 21, 2021 |Popular-Science 

Upstairs, in the living room, splintered logs of hemlock cackled and spat from inside the wood stove. Dungeons and Genital Clamps: Inside a Legendary BDSM Chateau |Ian Frisch |December 20, 2014 |DAILY BEAST 

A man who lived upstairs opened his door and witnessed the attack. The Myth of the Central Park Five |Edward Conlon |October 19, 2014 |DAILY BEAST 

He was taken back to the cell upstairs, where his time in detention would span 20 days. Abducted, Tortured, Indoctrinated: The Tale of a Teen Who Escaped ISIS |Yusuf Sayman |August 4, 2014 |DAILY BEAST 

He was told to address the management upstairs with that question. I Heard About the Latest Crazed Shooter While I Watched the World Cup with Guys He Almost Killed |Daniel Genis |July 1, 2014 |DAILY BEAST 

I went upstairs and stared out the window for a little while. The Shocking ‘Fargo’ Finale: Creator Noah Hawley Breaks Down the Epic Bloodbath |Kevin Fallon |June 18, 2014 |DAILY BEAST 

The governor went upstairs and found Juan de Messa in the hall. The Philippine Islands, 1493-1898, Volume XX, 1621-1624 |Various 

The Professor put down his cards without a word, and left the room, going straight upstairs. Uncanny Tales |Various 

When he was gone, Isaacson returned to his sitting-room upstairs and lit a nargeeleh pipe. Bella Donna |Robert Hichens 

Having reduced Punch to a second agony of tears Harry departed upstairs with the news that Punch was still rebellious. Kipling Stories and Poems Every Child Should Know, Book II |Rudyard Kipling 

"Give us a table upstairs alone," said Nigel to the head-steward, putting something into his hand. Bella Donna |Robert Hichens