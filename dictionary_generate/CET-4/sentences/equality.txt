Kaepernick wound up out of football and the issue of racial equality wound up back on the margins. Why Can’t Schools Get What the N.F.L. Has? (Ep. 431) |Stephen J. Dubner |September 10, 2020 |Freakonomics 

We have to be as committed to equality with the standard sense of urgency before the decision of marriage equality. Ritchie Torres, set to be first out Afro-Latino in Congress, seeks big changes amid COVID |Chris Johnson |September 9, 2020 |Washington Blade 

We’ll also examine the forces of change altering the course and speed of innovation, including security, diversity, and equality. Our world has hit an inflection point |Caroline da Cunha |September 2, 2020 |MIT Technology Review 

It’s important that we highlight again for equality voters the reality of this administration’s record, that this is a horrifically anti-LGBTQ administration. Facing Trump’s LGBTQ outreach, advocates hold firm on plan to show his record |Chris Johnson |September 2, 2020 |Washington Blade 

Sparked by a series of tragedies, people in cities across America and around the world have taken to the streets and social media to push for justice and racial equality. Introducing Fortune’s all-new 40 Under 40 list—and how it’s different this year |Rachel Schallom |September 2, 2020 |Fortune 

So, how do we achieve such equality in the U.S. and other parts of the world? How Good Dads Can Change the World |Gary Barker, PhD, Michael Kaufman |January 6, 2015 |DAILY BEAST 

In the 21st century women are earning their equality every step of the way… including the bedroom. Career-Minded Women Turn to Male Escorts For No-Strings Fun and (Maybe) Sex |Aurora Snow |January 3, 2015 |DAILY BEAST 

He goes into some detail into what it took to persuade voters to pass marriage equality at the ballot box in four states in 2012. The Real Story Behind the Fight for Marriage Equality |E.J. Graff |December 30, 2014 |DAILY BEAST 

Finally, he takes us behind the scenes into how Obama came out in favor of marriage equality that same year. The Real Story Behind the Fight for Marriage Equality |E.J. Graff |December 30, 2014 |DAILY BEAST 

By 2012, the marriage equality movement had won in courts and legislatures—but not at the ballot box. The Real Story Behind the Fight for Marriage Equality |E.J. Graff |December 30, 2014 |DAILY BEAST 

He did this to signalise his disapprobation of royalty, and his preference for democratic equality. Assimilative Memory |Marcus Dwight Larrowe (AKA Prof. A. Loisette) 

Moreover, he had seen on service how little the preachers of the equality of man carried out their doctrine in practice. Napoleon's Marshals |R. P. Dunn-Pattison 

I am prepared to recognise the principle established by law—that there shall be equality in civil privileges. Blackwood's Edinburgh Magazine, No. CCCXXXIX. January, 1844. Vol. LV. |Various 

I am prepared to respect the franchise, to give substantially, although not nominally, equality. Blackwood's Edinburgh Magazine, No. CCCXXXIX. January, 1844. Vol. LV. |Various 

What is necessary to be done, in order to attain an equality, is to change the condition, and the person is at once changed. The Condition, Elevation, Emigration, and Destiny of the Colored People of the United States |Martin R. Delany