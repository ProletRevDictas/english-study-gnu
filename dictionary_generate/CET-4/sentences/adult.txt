Lundberg also touted recent innovations like the chocolate-covered Chocolate Thin Snackers and Organic Rice Cake Minis, a bite-sized version of the original product geared toward adults and kids crunching on the go. The Rise and Fall of the Rice Cake, America’s One-Time Favorite Health Snack |Brenna Houck |September 17, 2020 |Eater 

Those impressions were made by herds of juvenile and adult animals, the scientists say. Seven footprints may be the oldest evidence of humans on the Arabian Peninsula |Bruce Bower |September 17, 2020 |Science News 

A rear bench seat fits two adults or children, with three-point seat belts and rated LATCH child-seat attachments. Can’t Afford a Sprinter? Get a Tiny Van Instead. |Emily Pennington |September 16, 2020 |Outside Online 

That means a family could link watches for a child or older adult to a parent’s iPhone. Two new Apple Watches announced at Apple’s ‘Time Flies’ event |Aaron Pressman |September 15, 2020 |Fortune 

Among adults age 18 to 24, 62% oppose the executive order to force a sale of TikTok. Could a TikTok shutdown cost Trump at the polls? |Lance Lambert |September 12, 2020 |Fortune 

Jones is a veteran of another beloved-yet-controversial animated series on Adult Swim, The Boondocks. ‘Black Dynamite’ Presents Police Brutality: The Musical |Stereo Williams |January 9, 2015 |DAILY BEAST 

As of 2013, Jews make up 1.8 to 2.2 percent of the adult U.S. population. Why Was Bess Myerson the First and Last Jewish Miss America? |Emily Shire |January 7, 2015 |DAILY BEAST 

Another side of Spider-Man that might be interesting to explore in a reboot is seeing him as an adult. Exclusive: Sony Hack Reveals Studio's Detailed Plans For Another ‘Spider-Man’ Reboot |William Boot |December 13, 2014 |DAILY BEAST 

I was under the impression that “girls” is a demeaning term for adult women. Up to a Point: They Made Me Write About Lena Dunham |P. J. O’Rourke |December 13, 2014 |DAILY BEAST 

It might feel fresh to see Peter Parker juggling with adult issues. Exclusive: Sony Hack Reveals Studio's Detailed Plans For Another ‘Spider-Man’ Reboot |William Boot |December 13, 2014 |DAILY BEAST 

Few persons can attain to adult life without being profoundly impressed by the appalling inequalities of our human lot. The Unsolved Riddle of Social Justice |Stephen Leacock 

But between the phase of schooling and the phase of adult learning there is an intermediate stage. The Salvaging Of Civilisation |H. G. (Herbert George) Wells 

And this college course I have sketched should, in the modern state, pass insensibly into adult mental activities. The Salvaging Of Civilisation |H. G. (Herbert George) Wells 

It stands very much in the way of that universal adult education which is our present concern. The Salvaging Of Civilisation |H. G. (Herbert George) Wells 

Not more than one adult worker in ten—so at least it might with confidence be estimated—is employed on necessary things. The Unsolved Riddle of Social Justice |Stephen Leacock