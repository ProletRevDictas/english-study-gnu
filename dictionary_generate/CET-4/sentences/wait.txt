Nesbitt said she worried that residents who had heard about long phone waits in the early days of vaccine registration were too discouraged to try calling in recent days. South Africa and U.K. coronavirus variants detected in D.C.; Maryland to open third mass vaccination site |Erin Cox, Julie Zauzmer, Rachel Chason |February 11, 2021 |Washington Post 

In Virginia, for example, the wait list for vaccinations in Fairfax County reached 186,602 earlier this week. Maryland Democrats call for ‘course correction’ amid regionwide frustration over vaccine rollout |Erin Cox, Rebecca Tan, Antonio Olivo |February 3, 2021 |Washington Post 

The Zinners have fundraised so they could hire an immigration attorney for Stephanie and her son, but they anticipate a long wait for any resolution. A Maryland couple opened their home to a Honduran mother and son. They ended up sharing more than space. |Stephanie García |January 30, 2021 |Washington Post 

After two failed appointments and a 45-minute wait at the hospital, her third time in the system was a success. What went wrong with America’s $44 million vaccine data system? |Cat Ferguson |January 30, 2021 |MIT Technology Review 

Maps could even help community residents compare wait times among different facilities to guide their choices and offer the best possible experiences. Envisioning better health outcomes for all |Jason Sparapani |January 26, 2021 |MIT Technology Review 

So far, all the players seemed to be willing to wait their turn. The Golden State Preps for the ‘Red Wedding’ of Senate Races |David Freedlander |January 9, 2015 |DAILY BEAST 

And they might not have to wait that long to show their political heft. Asian-Americans Are The New Florida |Tim Mak |January 8, 2015 |DAILY BEAST 

And as he adjusted to this change in circumstances, he screamed at himself a second time: Wait! Powerful Congressman Writes About ‘Fleshy Breasts’ |Asawin Suebsaeng |January 7, 2015 |DAILY BEAST 

Some refugees wait for days on the ships before setting sail. Ghost Ships of the Mediterranean |Barbie Latza Nadeau |January 6, 2015 |DAILY BEAST 

“Wait…” Suddenly a huge, graceful black marlin leaps out of the water, sending a shower of water ten feet high. The Story Behind Lee Marvin’s Liberty Valance Smile |Robert Ward |January 3, 2015 |DAILY BEAST 

As men fixed in the grip of nightmare, we were powerless—unable to do anything but wait. Gallipoli Diary, Volume I |Ian Hamilton 

He didn't need to wait—as the birds did—until an angleworm stuck his head above ground. The Tale of Grandfather Mole |Arthur Scott Bailey 

The pig family did not know when Squinty would be taken away from them, and all they could do was to wait. Squinty the Comical Pig |Richard Barnum 

Janet might have said before leaving: "Tea had better not wait too long--Hilda has to be down at Clayhanger's at half-past six." Hilda Lessways |Arnold Bennett 

And Jimmy thought that if he must wait for him again he would wait in a pleasant place. The Tale of Grandfather Mole |Arthur Scott Bailey