He defied the atheism of communism and the empty religious practices of Putinism. Remembering the Russian Priest Who Fought the Orthodox Church |Cathy Young |December 28, 2014 |DAILY BEAST 

Otherwise, we will be but celebrating an empty holiday, missing its true meaning altogether. Jesus Wasn’t Born Rich. Think About It. |Gene Robinson |December 25, 2014 |DAILY BEAST 

Later schools empty out children, who race over to play games in the shade. The Life and Hard Times Of The Family A Cuban Defector Left Behind |Brin-Jonathan Butler |December 19, 2014 |DAILY BEAST 

On Thursday, Russian bloggers published pictures of empty shelves in stores that once sold electric goods. After His Disastrous Annual Press Conference, Putin Needs A Hug |Anna Nemtsova |December 18, 2014 |DAILY BEAST 

If they are in fact linked to North Korea, the threat may not be as empty as people think. Kim Jong Un’s Kid Gloves Are Now Off |Gordon G. Chang |December 17, 2014 |DAILY BEAST 

Herein he found an empty stall that was dark enough not to be seen, and still afforded sufficient light to read in. The Homesteader |Oscar Micheaux 

When the funeral was over, and they returned to their desolate home, at the sight of the empty cradle Ramona broke down. Ramona |Helen Hunt Jackson 

A quite young child will, for example, pretend to do something, as to take an empty cup and carry out the semblance of drinking. Children's Ways |James Sully 

For his heart seemed alternately full and empty; all the life he had was centred there. The Wave |Algernon Blackwood 

In Flanders, he says, they would never attack with empty limbers behind them; they would wait till they were full up. Gallipoli Diary, Volume I |Ian Hamilton