Experts suggested a well-fitting, three-layer mask, with an outer layer that’s tightly woven or even waterproof and with either a built-in filter or a pocket for replaceable filters as the middle layer. Is It Time to Wear a Better Mask for COVID-19? We Asked the Experts |Tara Law |February 8, 2021 |Time 

Many types of bacteria live close to the outer layers of a sea star’s skin. Choked by bacteria, some starfish are turning to goo |Erin Garcia de Jesus |February 8, 2021 |Science News For Students 

We love the large the mesh outer pocket, which helps air out soaked clothes. Outside's Valentine's Day Gift Guide |The Editors |February 6, 2021 |Outside Online 

The outer edge of the cakes should have a bit of spring, though the middle will look slightly wobbly and dipped in the center. If loving a piping hot chocolate lava cake is wrong, I don’t want to be right |Becky Krystal |February 4, 2021 |Washington Post 

Dotted along the outer rim of the bubbles are proteins, called olfactory receptors. Scientists Made a Biohybrid Nose Using Cells From Mosquitoes |Shelly Fan |January 26, 2021 |Singularity Hub 

The twins themselves, played beautifully by Erin Davie and Emily Padgett, are our inner voice and outer voice, says Condon. Can Condon's Freak Show Win Broadway? |Tim Teeman |November 18, 2014 |DAILY BEAST 

Indeed there is a pre-Hispanic ceremonial site located a scant 400 meters from the outer edge of the Otomí Lake & Villas property. Trading Dime Bags for Salvador Dali |Jason McGahan |October 19, 2014 |DAILY BEAST 

An agent on the outer perimeter radios in that the motorcade is in sight. Behind the Scenes With a ‘Site Agent’: The Secret Service’s Hardest Job |Marc Ambinder |October 2, 2014 |DAILY BEAST 

Finding good wifi in the Outer Hebrides is about as common as finding someone between the ages of 18 and 30. Scotland’s ‘Yes’ Campaign and the Myth of Scottish Equality |Noah Caldwell |September 18, 2014 |DAILY BEAST 

The play, which won several Tony Awards when it first premiered in 1979, deals with human dignity and inner and outer beauty. Fall Broadway Preview: 'This Is Our Youth,' Bradley Cooper as ‘The Elephant Man,' and More |Janice Kaplan |September 11, 2014 |DAILY BEAST 

The menace of a thunder-cloud approached as in his childhood's dream; disaster lurked behind the quiet outer show. The Wave |Algernon Blackwood 

The nine barricaded the outer gates and placed in the best positions guns loaded with grape. The Red Year |Louis Tracy 

The others stood silent till they heard the outer door of the apartment close behind him. Confidence |Henry James 

Dobson, mouth agape, struck a little bell on the desk and the orderly stepped in from the outer room. Raw Gold |Bertrand W. Sinclair 

The gentleman bows very ceremoniously, and Monsieur St. Ange walks slowly out, and takes a glass of curaoa in the outer room. Checkmate |Joseph Sheridan Le Fanu