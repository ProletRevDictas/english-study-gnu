We need to give the option to postpone military service to those whose career peaks in their 20s. South Korean boy band BTS built an army. Now, they might get to defer military service |kdunn6 |September 4, 2020 |Fortune 

The event, which was to be held this year, has been postponed until 2021 because of the coronavirus—and may not be held at all. The legacy Shinzo Abe, Japan’s longest-serving Prime Minister, will leave after resigning |claychandler |August 28, 2020 |Fortune 

Not only has the tool allowed those execs on the account to make faster and more informed decisions about when to replace, postpone, cancel and renegotiate ad dollars with media owners, it’s also spread those jobs across more of the team. Marketers ‘looking to feel confident’: the coronavirus causing irreversible changes to the advertiser-agency dynamic |Seb Joseph |August 20, 2020 |Digiday 

Protests by members of the Kumeyaay Nation have been postponing construction of a portion of the border wall in East County since the end of June. Border Report: Kumeyaay Band Sues to Stop Border Wall Construction |Maya Srikrishnan |August 17, 2020 |Voice of San Diego 

Back in March, at the beginning of enforced lockdown, publishers, advertisers and conference companies assumed the fall would be stacked with postponed in-person events. ‘Lots of halo effects’: The Financial Times’ virtual lifestyle festival pivots focus to U.S., global audience |Lucinda Southern |August 14, 2020 |Digiday 

His first prescription when I saw him was to have the CAT scan test that I had been forced to postpone for a month and a half. My Insurance Company Killed Me, Despite Obamacare |Malcolm MacDougall |November 24, 2014 |DAILY BEAST 

At this point, the photographer says, he decided that too many people knew about his trip and so he decided to postpone it. Was U.S. Journalist Steven Sotloff a Marked Man? |Ben Taub |September 2, 2014 |DAILY BEAST 

Others postpone motherhood until they are financially stable. The Crumbling Post-35 Pregnancy Myth |Jean Twenge |June 29, 2014 |DAILY BEAST 

The prerequisite for this is for the U.S. to postpone current efforts for some kind of political transition among Syrians. Face the Assad Reality In Syria |Frank G. Wisner, Leslie H. Gelb |January 26, 2014 |DAILY BEAST 

We still need to see if that will happen, or if we need to postpone it. ‘Hocus Pocus’ Turns 20: Meet the Voice Behind Binx the Talking Cat |Kevin Fallon |October 31, 2013 |DAILY BEAST 

I may be tempted to postpone my retirement, and for a while longer to continue to gather the golden harvest that ripens round me. Checkmate |Joseph Sheridan Le Fanu 

Better postpone your solace to more fitting time and place—the close of day and your own veranda. Tobacco; Its History, Varieties, Culture, Manufacture and Commerce |E. R. Billings. 

If you love tea, postpone pipe till after it; no man can enjoy fine tea who has smoked. Tobacco; Its History, Varieties, Culture, Manufacture and Commerce |E. R. Billings. 

The length of several of the communications in our present Number compels us to postpone this week our Notes on Books, &c. Notes and Queries, Number 177, March 19, 1853 |Various 

If Arsenio had a mind to reform, let him postpone that reformation until Garnache should have done with him. St. Martin's Summer |Rafael Sabatini