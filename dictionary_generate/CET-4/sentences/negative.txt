Attitudes toward China have turned negative in the past few years. Poll: Sharp partisan differences now exist on foreign policy, views of American exceptionalism |Dan Balz, Scott Clement |September 17, 2020 |Washington Post 

I think when we get into April, in the warmer weather, that has a very negative effect on that and that type of a virus. Timeline: The 124 times Trump has downplayed the coronavirus threat |Aaron Blake, JM Rieger |September 17, 2020 |Washington Post 

This was a clue that the polio virus used in the vaccines might not have been fully inactivated as was intended even though the samples otherwise tested negative for live polio virus. On COVID-19 vaccines, Big Pharma knows to just say ‘no’ |matthewheimer |September 11, 2020 |Fortune 

In an election year, and after dealing with many negative blows to its image, Facebook is trying to reestablish itself as the original social-media company, which in the case of Campus means turning back to its roots. Facebook just invented … Facebook |Tanya Basu |September 10, 2020 |MIT Technology Review 

More than 70% of the posts were negative, while only 13% of the comments were positive. Disney’s ‘Mulan’ is set for a disappointing debut in the market it’s courted most |Claire Zillman, reporter |September 10, 2020 |Fortune 

Myerson herself appears to have bought into that stigma, offering mixed to negative views on the Miss America pageant. Why Was Bess Myerson the First and Last Jewish Miss America? |Emily Shire |January 7, 2015 |DAILY BEAST 

I think part of being in the public eye is getting recognized, and dealing with positive and negative scrutiny. Tim Howard’s Wall of Intensity |William O’Connor |December 22, 2014 |DAILY BEAST 

In addition to the negative on-field attention, off-the-field problems were just as bad this year. Is Any College Football Coach Worth $60 Million? Jim Harbaugh Is |Jesse Lawrence |December 20, 2014 |DAILY BEAST 

The negative headlines and accusations of sexual assault continue. Team Cosby’s Mission Impossible |Lloyd Grove |December 18, 2014 |DAILY BEAST 

He says he has yet to experience any negative feedback from the galaxy of Whovians. Doctor Who: It’s Time For a Black, Asian, or Woman Doctor |Nico Hines |December 11, 2014 |DAILY BEAST 

Before Louis could answer in the negative, he heard voices in the adjoining garden. The Pastor's Fire-side Vol. 3 of 4 |Jane Porter 

The great majority belong to the colon bacillus group, and are negative to Gram's method of staining. A Manual of Clinical Diagnosis |James Campbell Todd 

This is a minute, slender rod, which lies within and between the pus-corpuscles (Fig. 125), and is negative to Gram's stain. A Manual of Clinical Diagnosis |James Campbell Todd 

The organism is a short, thick diplobacillus, is frequently intracellular, and is Gram-negative (Fig. 126). A Manual of Clinical Diagnosis |James Campbell Todd 

The press censorship is a negative evil in London; in Cairo there is no doubt it is positive. Gallipoli Diary, Volume I |Ian Hamilton