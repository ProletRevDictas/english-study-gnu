Fossils eroding out of footprint-bearing sediment included remains of elephants and large gazelles called oryxes, but not humans. Seven footprints may be the oldest evidence of humans on the Arabian Peninsula |Bruce Bower |September 17, 2020 |Science News 

When fires burn up vegetation, the charred remains become hydrophobic—meaning they repel away any water. California wildfires may give way to massive mudslides |Ula Chrobak |September 17, 2020 |Popular-Science 

Until now, the oldest remains of an ancient gibbon species consisted of a small number of teeth found in China, which date from around 7 million to 9 million years ago. A stray molar is the oldest known fossil from an ancient gibbon |Bruce Bower |September 8, 2020 |Science News 

She’s a bioarchaeologist — someone who studies human history through research on human remains. Women like Mulan didn’t need to go to war in disguise |Bethany Brookshire |September 4, 2020 |Science News For Students 

Its remains will then be smeared out into a system of rings, like the rings of Saturn but smaller and darker. These Images Expose the Dark Side of the Solar System - Issue 89: The Dark Side |Corey S. Powell |August 26, 2020 |Nautilus 

Any plans to grow her exercise movement must, she insists, remain “completely organic.” How Taryn Toomey’s ‘The Class’ Became New York’s Latest Fitness Craze |Lizzie Crocker |January 9, 2015 |DAILY BEAST 

To those who agreed with him, Bush pledged that the law against same-sex marriage would remain intact. Jeb Bush’s Unseen Anti-Gay Marriage Emails |Jackie Kucinich |January 9, 2015 |DAILY BEAST 

The people who are involved in the violence, they figure out ways to remain here at all costs and continue causing trouble. Mexico’s Priests Are Marked for Murder |Jason McGahan |January 7, 2015 |DAILY BEAST 

But the qualities Mario Cuomo brought to public life—compassion, integrity, commitment to principle—remain in short supply today. President Cuomo Would’ve Been a Lion |Jonathan Alter |January 2, 2015 |DAILY BEAST 

Instead, black models are required to remain meekly, silently off stage, waiting for a turn that may never come. One Vogue Cover Doesn’t Solve Fashion’s Big Race Problem |Danielle Belton |January 2, 2015 |DAILY BEAST 

As long as may be necessary, Sam,” replied Mr. Pickwick, “you have my full permission to remain. The Posthumous Papers of the Pickwick Club, v. 2(of 2) |Charles Dickens 

I had it put in order whilst you were in London; it was a shame to let a sacred place remain in such a state. Elster's Folly |Mrs. Henry Wood 

The observer might well remain perplexed at the pathetic discord between human work and human wants. The Unsolved Riddle of Social Justice |Stephen Leacock 

But the quiet old town, with its musical name and its great orchestra, will long remain in my memory. Music-Study in Germany |Amy Fay 

And having nothing in their minds which seeks utterance, they remain quiet. Assimilative Memory |Marcus Dwight Larrowe (AKA Prof. A. Loisette)