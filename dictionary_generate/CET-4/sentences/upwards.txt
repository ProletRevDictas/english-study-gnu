Using standard methods, the cost of printing DNA could run upwards of a billion dollars or more, depending on the strand. Design Your Own Dinosaur: The Era of Custom DNA |Justin Jones |January 8, 2015 |DAILY BEAST 

Not surprisingly, rates for recovery vary enormously, from as low as three percent to upwards of 75 percent. You’re Never ‘Cured’ of an Eating Disorder |Carrie Arnold |December 20, 2014 |DAILY BEAST 

Yet the undocumented population remains upwards eleven million. Legal but Still Poor: The Economic Consequences of Amnesty |Joel Kotkin |November 21, 2014 |DAILY BEAST 

At its height in the 1920s, Terry noted, the Klan wielded real political influence, boasting a membership upwards of four million. The Klan’s Call to Violence in Ferguson Blows the Lid Off Its Hypocritical Rebrand |Caitlin Dickson |November 14, 2014 |DAILY BEAST 

Currently, elaborate sex toys, including life-like love dolls, can cost upwards of $6,000. Welcome to Oculus XXX: In-Your-Face 3D is the Future of Porn |Aurora Snow |October 18, 2014 |DAILY BEAST 

The entire city was burnt down, and upwards of 200,000 of the inhabitants perished in the flames. The Every Day Book of History and Chronology |Joel Munsell 

The last thing—against the skyline—a little column of French soldiers of the line charging back upwards towards the lost redoubt. Gallipoli Diary, Volume I |Ian Hamilton 

A Naval Officer who has seen her says she is lying in shallow water—6 fathoms—bottom upwards looking like a stranded whale. Gallipoli Diary, Volume I |Ian Hamilton 

The next sale on Thursday is upwards of 1100 tons, and we expect a little better standard. Life of Richard Trevithick, Volume II (of 2) |Francis Trevithick 

The invested funds of the society to-day amount to upwards of a million, and in 1897 they were £476,000. Fifty Years of Railway Life in England, Scotland and Ireland |Joseph Tatlow