Doomsday messaging may be accurate with regards to the gravity of the problem at hand, but it holds little sway over a public wracked by empathy burnout. What ‘Schitt’s Creek’ can teach us about climate action |matthewheimer |December 6, 2020 |Fortune 

I stayed at their Catskills East location and woke up each morning to the sway of century-old trees. After Eight Weeks Cramped Inside An Apartment, I Escaped To The Catskills |Charli Penn |October 15, 2020 |Essence.com 

The hearing gave the clearest indication yet of both parties' best arguments in the matter and of which positions seem most likely to hold sway with Rogers as the case heads toward a full trial. Apple v. Epic hearing previews a long, hard-fought trial to come |Kyle Orland |September 28, 2020 |Ars Technica 

These kinds of rules are in place to protect the governing process from improper political influence and elections from the sway of powerful actors. The GOP Convention Violated Plenty Of Norms, But Did It Undermine Democratic Values? |Julia Azari |September 1, 2020 |FiveThirtyEight 

Physicists have never proposed that the law of gravity, the increase in entropy, or the various electromagnetic “rules” that hold sway among subatomic particles should be consulted as a source of ethical good. Just Because It’s Natural Doesn’t Mean It’s Good - Issue 89: The Dark Side |David P. Barash |August 19, 2020 |Nautilus 

There were no obvious leaders; no single ideology or organization held sway over the crowd. Eric Garner Protests: ‘It’s Like Vietnam’ |Abby Haglage, Caitlin Dickson, Jacob Siegel, Chris Allbritton |December 5, 2014 |DAILY BEAST 

Many millions have been spent on television ads in North Carolina, as groups on the right and left try to sway the electorate. Why Voters Are So Totally Checked Out |Eleanor Clift |October 22, 2014 |DAILY BEAST 

Those groups were eager to get their hands on anyone who could be used for ransom or political sway. Was U.S. Journalist Steven Sotloff a Marked Man? |Ben Taub |September 2, 2014 |DAILY BEAST 

None of these studies, campaigns, or assertions should be enough to sway public opinion towards or against pot. Another Hazy Week For Weed |Abby Haglage |September 1, 2014 |DAILY BEAST 

You have to sway from one foot to another to keep them from staking their claim. Whatever You Do Someone Will Die. A Short Story About Impossible Choices in Iraq |Nathan Bradley Bethea |August 31, 2014 |DAILY BEAST 

Vicars' wives had come and gone, but all had submitted, some after a brief struggle, to old Mrs. Wurzel's sway. The Pit Town Coronet, Volume I (of 3) |Charles James Wills 

He rules with a gentler sway than many who are accustomed to other methods of command would believe possible. Fifty Years of Railway Life in England, Scotland and Ireland |Joseph Tatlow 

His brain—the part where human reasoning holds normal sway—was dominated by the purely primitive instinct of flight. Uncanny Tales |Various 

Shakespeare tells us that mercy 'is mightiest in the mightiest,' and is 'above this sceptred sway'; Merch. Chaucer's Works, Volume 1 (of 7) -- Romaunt of the Rose; Minor Poems |Geoffrey Chaucer 

The mob now ruled with undisputed sway in both legislative and executive halls. Madame Roland, Makers of History |John S. C. Abbott