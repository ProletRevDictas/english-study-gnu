It seemed the plan was to slowly capture market share with a gradual introduction of new features across its platform. The future of Google and what it means for search |Pete Eckersley |February 5, 2021 |Search Engine Watch 

Its gradual eradication meant ending the needless suffering and death of millions and millions of people every year. Smallpox used to kill millions of people every year. Here’s how humans beat it. |Kelsey Piper |February 5, 2021 |Vox 

By Wednesday, we start clearing out and begin a gradual warming trend until another shot of cold air by Saturday. Updates: An icy morning with scattered snow showers developing into the afternoon |Jason Samenow |February 1, 2021 |Washington Post 

In October 2019, five weeks after finishing the AT, a doctor rechecked an X-ray of my left foot and cleared me for a gradual return to running. Did Thru-Hiking the Appalachian Trail Ruin My Body? |Grayson Haver Currin |January 25, 2021 |Outside Online 

This has been a gradual, maybe too gradual, transition for our industry. By clamping down on DC rioters, Airbnb is finally acting like it owns the place |Rani Molla |January 15, 2021 |Vox 

Utilizing fear, or taking a reasoned approach to gradual, incremental change? Jon Stewart Talks ‘Rosewater’ and the ‘Chickensh-t’ Democrats’ Midterm Massacre |Marlow Stern |November 9, 2014 |DAILY BEAST 

Now 88 senators are urging the Obama administration to take a very different approach to the group: gradual regime change. Senators: Take Gaza Away From Hamas |Eli Lake |September 22, 2014 |DAILY BEAST 

Tshering is in favor of growth, he says, but a “gradual one and spread throughout the year.” Can Traditional Bhutan Survive Tourism? |Esha Chhabra |August 17, 2014 |DAILY BEAST 

It also traces his days as a juvenile delinquent, and gradual rise up the R&B charts. ‘Get On Up’ Star Chadwick Boseman on Becoming James Brown—With A Little Help From Mick Jagger |Marlow Stern |August 4, 2014 |DAILY BEAST 

That sounds small, but the buildup of Hezbollah forces in Syria was gradual, too. Hezbollah’s Widening War Spreads to Iraq |Jamie Dettmer |August 1, 2014 |DAILY BEAST 

The occupants of the room had been too absorbed with their own affairs to notice the gradual dimming of the illumination. The Boarded-Up House |Augusta Huiell Seaman 

His departure in autumn had been so gradual, that it was difficult to say when night began to overcome the day. The Giant of the North |R.M. Ballantyne 

John Baptiste Robinet taught the gradual development of all forms of existence from a single creative cause. Assimilative Memory |Marcus Dwight Larrowe (AKA Prof. A. Loisette) 

It is not exactly so, but is still very different to the gradual swell on the other Cremona instruments. Violins and Violin Makers |Joseph Pearce 

That at some period of Man's gradual evolution from the brute, God found Man guilty of some sin, and cursed him. God and my Neighbour |Robert Blatchford