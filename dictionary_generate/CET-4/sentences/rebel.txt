Russia has also entrenched itself along Libya’s coast through its support for the rebel leader Khalifa Haftar, who controls the region. Butterfly Effect: The Next U.S.-Russia Conflict Theater Could be the Mediterranean |Charu Kasturi |September 17, 2020 |Ozy 

The GRU chief is also a central figure in Russia’s attempts to bring rebel Libyan leader Khalifa Haftar to power in Tripoli. Spy Wars: The Hidden Foe America Must Defeat to Save Its Democracy |Charu Kasturi |September 13, 2020 |Ozy 

Chef Nomi, as the rebel-bear called himself, attracted a following with promises of a “fair launch.” Is the SushiSwap saga a preview of a new wave of crypto chaos? |dzanemorris |September 9, 2020 |Fortune 

Among them was Muhammad Qasim Shah, the last known militant from Tral, a township that had given birth in 2015 to a fresh wave of armed rebels against India following years of steady decline in the number of militants. Is Kashmir’s Militancy Dead? |Charu Kasturi |August 6, 2020 |Ozy 

In fact, voters rebelled in November when patriotic hysteria went too far. ‘Keep Your Mouth Shut’: Why San Diego Banned ‘Seditious’ Talk in 1918 |Randy Dotinga |August 4, 2020 |Voice of San Diego 

Malakhov says there are criminals who have joined the rebel ranks and are exerting influence with their new positions. The Corrupt Cops of Rebel-Held East Ukraine |Kristina Jovanovski |December 11, 2014 |DAILY BEAST 

Excerpted from Rebel Yell: The Violence, Passion, and Redemption of Stonewall Jackson by S.C. Gwynne. Stonewall Jackson, VMI’s Most Embattled Professor |S. C. Gwynne |November 29, 2014 |DAILY BEAST 

Despite its recent gains on the battlefield, the fight against rebel brigades has taken a significant toll on the government army. Local Truces Are Syria’s Sad Little Pieces of Peace |Joshua Hersh |November 18, 2014 |DAILY BEAST 

But the calmness in rebel-held Donetsk on Sunday night suggested no big push is planned in the next few hours, at least. Eastern Ukraine Braces for ‘Full-Scale War’ |Jamie Dettmer |November 17, 2014 |DAILY BEAST 

Other more moderate rebel groups have long claimed that was the case. Digital Doublethink: Playing Truth or Dare with Putin, Assad and ISIS |Christopher Dickey, Anna Nemtsova |November 16, 2014 |DAILY BEAST 

The rebel Planner had fallen from his allegiance, and was making his terms with the enemy. Blackwood's Edinburgh Magazine, No. CCCXXXIX. January, 1844. Vol. LV. |Various 

The rebel general opportunely arrived in Singapore at or about the time of the outbreak of American-Spanish hostilities. The Philippine Islands |John Foreman 

De Valor resumed it, when he raised the rebel standard on the Alpuxara mountains. The Pastor's Fire-side Vol. 3 of 4 |Jane Porter 

Lastly, there was Aguinaldoʼs old rebel party, which rallied to the one cry “Independence.” The Philippine Islands |John Foreman 

It was one of these rebel detachments that passed the four fugitives from Cawnpore on the outskirts of Bunnee. The Red Year |Louis Tracy