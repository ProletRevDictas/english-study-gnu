Spotify, for instance, is offering educational classes to train artists to better promote their music to global audiences. The Biggest Challenge for Apple and Spotify in North Africa: YouTube |Eromo Egbejule |September 17, 2020 |Ozy 

If you’re a writer or artist, you can use your talents to convey a message that will resonate with people. Want to fight climate change effectively? Here’s where to donate your money. |Sigal Samuel |September 17, 2020 |Vox 

The worst thing a chef can be told by a businessperson or investor is that you’re an artist. Momofuku’s David Chang on the big changes the restaurant industry needs to make to survive |Beth Kowitt |September 14, 2020 |Fortune 

Each show is hosted by or features an artist, designer or influencer that is associated with the brand or product. ‘Our goal is to become a massive marketplace’: NTWRK is bringing livestream commerce to a younger generation |Kayleigh Barber |September 14, 2020 |Digiday 

Your Friends In New York’ is predicted to bridge the gap between brands, artists and the community together in different forms including the next evolution of Pyer Moss fashion show. Kerby Jean-Raymond Launch ‘Your Friends In New York’ |Nandi Howard |September 11, 2020 |Essence.com 

No artist had ever done anything like that at an awards show. Bow Down, Bitches: How Beyoncé Turned an Elevator Brawl Into a Perfect Year |Kevin Fallon |December 31, 2014 |DAILY BEAST 

To be fair, no artist had ever been asked to, or could have pulled it off if they had. Bow Down, Bitches: How Beyoncé Turned an Elevator Brawl Into a Perfect Year |Kevin Fallon |December 31, 2014 |DAILY BEAST 

The most exciting and thrillingly unique artist to surface in 2014. The 10 Best Albums of 2014: Taylor Swift, Sia, Run the Jewels, and More |Marlow Stern |December 28, 2014 |DAILY BEAST 

A 59-year-old retired subway train driver, who gave his name only as Artist, admitted that he had family members in the NYPD. Protesters Demand Justice For Gurley As Gap Grows Between Cops and NYC |M.L. Nestel |December 28, 2014 |DAILY BEAST 

A lot of actors are good, but Phil was a fully developed artist. Coffee Talk with Ethan Hawke: On ‘Boyhood,’ Jennifer Lawrence, and Bill Clinton’s Urinal Exchange |Marlow Stern |December 27, 2014 |DAILY BEAST 

This attracted much attention, and the London journals praised the artist. Women in the fine arts, from the Seventh Century B.C. to the Twentieth Century A.D. |Clara Erskine Clement 

Her attachment to impressionism leads this artist to many experiments in color—or, as one critic wrote, "to play with color." Women in the fine arts, from the Seventh Century B.C. to the Twentieth Century A.D. |Clara Erskine Clement 

The pictures of flowers which this artist paints prove her to be a devoted lover of nature. Women in the fine arts, from the Seventh Century B.C. to the Twentieth Century A.D. |Clara Erskine Clement 

This artist is, sui generis, a daughter of the people, of unconventional tastes and habits. Women in the fine arts, from the Seventh Century B.C. to the Twentieth Century A.D. |Clara Erskine Clement 

Other pictures by this artist remind one of the works of Botticelli. Women in the fine arts, from the Seventh Century B.C. to the Twentieth Century A.D. |Clara Erskine Clement