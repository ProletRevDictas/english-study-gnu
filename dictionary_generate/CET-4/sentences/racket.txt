A neighbor, apparently annoyed by the racket, started filming the scene and caught Wallen using the racial slur. Country star Morgan Wallen suspended by record label, dropped by hundreds of radio stations after using racial slur |Emily Yahr |February 3, 2021 |Washington Post 

As you might’ve guessed, Warrior is determined to cut through the racket. One of tech’s most powerful women wants to modernize your book club |Claire Zillman |January 15, 2021 |Fortune 

If I’m on a tennis site, I might be served an ad for a tennis racket. How A.I. can make digital advertising less creepy |jakemeth |December 17, 2020 |Fortune 

Yes, there are consumer products that are upsold based on their use of graphene—headphones, tennis rackets, shoes—but “success is having hundreds to thousands of tons of your material being sold,” he says. Graphene gets real: Meet the entrepreneurs bringing the wonder substance to market |David Meyer |December 13, 2020 |Fortune 

As a vendor, it’s tempting to make claims that will get your company noticed and help your product stand out in the racket—but don’t. The email security market is littered with false claims. How to fix it |jakemeth |November 27, 2020 |Fortune 

For decades, these two industrial brewers have basked in a sort of shared-monopoly over the Panamanian beer racket. House of the Witch: The Renegade Craft Brewers of Panama |Jeff Campagna |November 30, 2014 |DAILY BEAST 

According to police, Kory then attacked the victim with an aluminum tennis racket. Meet Your New ‘Hot Mugshot Guy’: Sean Kory, Fox News’ Public Enemy No. 1 |Marlow Stern |November 3, 2014 |DAILY BEAST 

For all who do believe this, the very existence of Israel is a sort of fraud or a racket. No Drama Obama's Israel Ambivalence |James Poulos |July 26, 2014 |DAILY BEAST 

This past Monday afternoon, I headed off for my regular tennis game with my racket strapped to my back and my wife in her whites. I Heard About the Latest Crazed Shooter While I Watched the World Cup with Guys He Almost Killed |Daniel Genis |July 1, 2014 |DAILY BEAST 

Across the street, in a chinaberry tree, a gang of sparrows are making a racket. Stanley Booth on the Life and Hard Times of Blues Genius Furry Lewis |Stanley Booth |June 7, 2014 |DAILY BEAST 

This is simpler than having to cram and then stand the racket of a competitive examination. Punch, or the London Charivari, Volume 107, November 3, 1894 |Various 

Surely no one inside the Weedham plant could have heard the gun fire above the racket the machines were making. Hooded Detective, Volume III No. 2, January, 1942 |Various 

Below the latest war communiques was a small column-head about a threatened gang war in the numbers racket. Hooded Detective, Volume III No. 2, January, 1942 |Various 

Midway down the page was more about the threatened strife in the numbers racket. Hooded Detective, Volume III No. 2, January, 1942 |Various 

Girra was a powerful figure in the metropolitan pin-ball game syndicate and had a piece of the number policy racket too. Hooded Detective, Volume III No. 2, January, 1942 |Various