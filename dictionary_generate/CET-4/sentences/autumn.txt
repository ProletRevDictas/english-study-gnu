Typically, winter precipitation starts to set in and cap fire seasons in the autumn. Colorado is fighting its largest wildfire in history. Other massive blazes are close behind. |Umair Irfan |October 22, 2020 |Vox 

Despite the pandemic that had driven most everyone from their usual routines and traditions, Midwesterners were getting pumped for chillier autumn weather. A Looming Menace for Restaurants: Winter Is Coming |Brenna Houck |October 15, 2020 |Eater 

Italy has hardly dodged the latest autumn wave of contagion hitting Europe, but it’s holding up better than neighboring countries. As COVID cases spike in Europe, Italy stands out—but this time for doing things right |Bernhard Warner |October 15, 2020 |Fortune 

The inclement weather may have kept the birds on the western front through the autumn in 1917 and 1918, close to both military and civilian populations, providing an excellent opportunity for the virus to cross between species and mutate. A climate anomaly may have worsened the 1918 pandemic and WWI |Kate Baggaley |September 25, 2020 |Popular-Science 

Based on those findings, an action plan will be drawn up for the autumn. ‘It’s all been plan, plan, plan mode:’ Agencies have big ideas for greater diversity, but more action is needed |Seb Joseph |September 15, 2020 |Digiday 

One afternoon we were watching Ingmar Bergman's Autumn Sonata. Alfred Hitchcock’s Fade to Black: The Great Director’s Final Days |David Freeman |December 13, 2014 |DAILY BEAST 

From the first shots of Autumn Sonata it's clear that this is going to be slow going. Alfred Hitchcock’s Fade to Black: The Great Director’s Final Days |David Freeman |December 13, 2014 |DAILY BEAST 

Standing in the chill breeze of autumn, I knew something had passed between us. What It Takes to Kill a Grizzly Bear |Doug Peacock |November 23, 2014 |DAILY BEAST 

The summer heat is fleeting, and the crisp golden brown of autumn lingers just a little bit longer than it should. Jason Schwartzman Is the Nicest Jerk You’ll Ever Meet in ‘Listen Up Philip’ |Emma Myers |October 17, 2014 |DAILY BEAST 

Another summer has passed, and with its passing the rites of autumn have begun. The Doctor’s Note Must Die! |Russell Saunders |September 16, 2014 |DAILY BEAST 

The afternoon was a lovely one—the day was a perfect example of the mellowest mood of autumn. Confidence |Henry James 

The plant as a whole remains green until late in the autumn. How to Know the Ferns |S. Leonard Bastin 

In a general way the fronds are best collected during the summer and autumn, when they will, of course, be well developed. How to Know the Ferns |S. Leonard Bastin 

Through the beautiful, windy autumn days, he labored at his difficult task, the task of telling a story. The Homesteader |Oscar Micheaux 

His departure in autumn had been so gradual, that it was difficult to say when night began to overcome the day. The Giant of the North |R.M. Ballantyne