After Backstrom’s tally, things got heated, with Wilson being called for a two-minute minor for interference after his late shoulder-to-chest hit on Mark Jankowski. Tom Wilson gets the Capitals back on track with a win over the Penguins |Samantha Pell |February 26, 2021 |Washington Post 

A simpler approach, he believes, will get him back to the mechanics he thrived with in the minors. Carter Kieboom, unbothered by the spotlight, is confident he can keep his third base job |Jesse Dougherty |February 25, 2021 |Washington Post 

Toss in a small roof rack, some extra fuel, and minor interior modifications, and Stuart was left with a truck that, when I directed him to a vehicle scale, measured out at just about 5,600 pounds—without him or his gear in it. How to Modify Your Tacoma the Right Way |Wes Siler |February 25, 2021 |Outside Online 

In her first appearance after recovering from a minor injury, O’Hara logged a preplanned 30-plus minutes before making way for Washington Spirit teammate Emily Sonnett. USWNT flexes muscle, routs Argentina to win SheBelieves Cup, 6-0 |Steven Goff |February 25, 2021 |Washington Post 

Perhaps police should be even more worried if someone flees when police confront him over a minor infraction, Roberts said. Supreme Court considers giving police greater powers when pursuing suspects |Robert Barnes |February 24, 2021 |Washington Post 

Many of those who have become cops in New York seem to have ceased to address such minor offenses over the past few days. Shot Down During the NYPD Slowdown |Michael Daly |January 7, 2015 |DAILY BEAST 

A couple of people were treated for minor injuries but no major incidents occurred. Slow Motion Tiger Jump, a Tornado at the Rose Bowl and More Viral Videos |The Daily Beast Video |January 4, 2015 |DAILY BEAST 

The numbers reinforce another article in the Post, in which cops confessed to “turning a blind eye” to minor crimes. Ground Zero of the NYPD Slowdown |Batya Ungar-Sargon |January 1, 2015 |DAILY BEAST 

It starts off like any other Lana tune, replete with minor chords and humming, distorted vocals. The 14 Best Songs of 2014: Bobby Shmurda, Future Islands, Drake, and More |Marlow Stern |December 31, 2014 |DAILY BEAST 

A few minor notes, born of reflection: Traditionally, the best columns are dominated by politics—its most popular topic. The Best Columns of 2014 |John Avlon, Errol Louis |December 31, 2014 |DAILY BEAST 

Three days later he was in Switzerland, and a few days later again he was on the summit of a minor but still difficult peak. Uncanny Tales |Various 

The 'whole' of him that now dealt with Lettice was far above all minor and partial means of knowing. The Wave |Algernon Blackwood 

Barton Booth died; a celebrated tragedian in the reign of queen Anne, author of some songs and minor pieces. The Every Day Book of History and Chronology |Joel Munsell 

Even a minor dislocation breaks down a certain part of the machinery of society. The Unsolved Riddle of Social Justice |Stephen Leacock 

When she struck the chord of G minor, it was the right preparation, and brought you immediately into the mood for what followed. Music-Study in Germany |Amy Fay