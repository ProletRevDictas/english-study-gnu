The growth stems, in large part, from China’s lack of access to clean tap water. A blockbuster IPO briefly made a bottled water entrepreneur China’s richest man |Grady McGregor |September 8, 2020 |Fortune 

Smith makes a saline solution with sparkling water instead of tap water—he prefers San Pellegrino for its crisp minerality. Why you should be adding salt to your cocktails |By Céline Bossart/Saveur |September 4, 2020 |Popular-Science 

Heavy rain and strong winds are already on tap for Cuba, Jamaica, and the Cayman Islands, according to meteorologists. Tropical Storm Laura, not yet a hurricane, threatens coastal Texas and Louisiana |Andrew Nusca |August 24, 2020 |Fortune 

After having trouble cleaning the jar, he filled it with tap water and left it to soak. Scientists stumbled across the first known manganese-fueled bacteria |Carolyn Beans |July 21, 2020 |Science News 

Leadbetter and Yu first identified about 70 bacterial species in the jar, which likely came from the tap water. Scientists stumbled across the first known manganese-fueled bacteria |Carolyn Beans |July 21, 2020 |Science News 

Obama is widely believed to tap an ex-physicist who cuts military waste like a laser to become the next secretary of defense. Ashton Carter, the Wonk Who Would Lead the Pentagon |Shane Harris, Tim Mak |December 2, 2014 |DAILY BEAST 

It should be noted that LA tap water, rather than NYC's famed brand, was chosen for the test. The Bottled Water Taste Test |The Daily Beast Video |December 1, 2014 |DAILY BEAST 

Cervecerías Barú resolved problems with their draft system that prohibited them from selling on tap for almost seven years. House of the Witch: The Renegade Craft Brewers of Panama |Jeff Campagna |November 30, 2014 |DAILY BEAST 

Ricky was very influenced by This Is Spinal Tap, and I was always a fan of Woody Allen movies. Stephen Merchant Talks ‘Hello Ladies’ movie, the Nicole Kidman Cameo, and Legacy of ‘The Office’ |Marlow Stern |November 22, 2014 |DAILY BEAST 

The NSA had already built the infrastructure to tap into communications networks. How the NSA Became a Killing Machine |Shane Harris |November 9, 2014 |DAILY BEAST 

By the time I had done my toilette there was a tap at the door, and in another minute I was in the salle--manger. Blackwood's Edinburgh Magazine, Volume 60, No. 372, October 1846 |Various 

"Come in," said a Cockney voice shrill with youth, in answer to her tap; and the child obeyed. Rosemary in Search of a Father |C. N. Williamson 

While thus occupied, there came a soft tap to the outer door—as was sure to be the case, the clerk being absent—and Val opened it. Elster's Folly |Mrs. Henry Wood 

Almost immediately there came a gentle tap at the door, and General Maxgregor entered. The Weight of the Crown |Fred M. White 

Thus, the magnetic telegraph was expected for quite three hundred years before its first tap of the keys announced its presence. Gospel Philosophy |J. H. Ward