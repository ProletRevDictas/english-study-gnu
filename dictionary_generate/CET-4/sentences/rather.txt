Fluoride first entered an American water supply through a rather inelegant technocratic scheme. Anti-Fluoriders Are The OG Anti-Vaxxers |Michael Schulson |July 27, 2016 |DAILY BEAST 

To put it rather uncharitably, the USPHS practiced a major dental experiment on a city full of unconsenting subjects. Anti-Fluoriders Are The OG Anti-Vaxxers |Michael Schulson |July 27, 2016 |DAILY BEAST 

What they found was that most people preferred to work with the lovable fool rather than the competent jerk. The Case Against In-Your-Face Atheism |Steve Neumann |January 4, 2015 |DAILY BEAST 

Which is impossible unless people talk publicly rather than letting each crime be its own isolated incident. Cover-Ups and Concern Trolls: Actually, It's About Ethics in Suicide Journalism |Arthur Chu |January 3, 2015 |DAILY BEAST 

Lady Rose is also rather subdued in the premiere, which is a pity. What Downton’s Fashion Really Means |Katie Baker |January 2, 2015 |DAILY BEAST 

Truth is a torch, but one of enormous size; so that we slink past it in rather a blinking fashion for fear it should burn us. Pearls of Thought |Maturin M. Ballou 

It ended on a complaint that she was 'tired rather and spending my time at full length on a deck-chair in the garden.' The Wave |Algernon Blackwood 

I was rather awed by his imposing appearance, and advanced timidly to the doors, which were of glass, and pulled the bell. Music-Study in Germany |Amy Fay 

Were you ever arrested, having in your custody another man's cash, and would rather go to gaol, than break it? The Life and Most Surprising Adventures of Robinson Crusoe, of York, Mariner (1801) |Daniel Defoe 

The streets here are rather wide for an Italian city but would be deemed intolerably narrow in America. Glances at Europe |Horace Greeley