Some only make about a cup of frothed milk, but others may make a quart or more and require the counter and storage space for that capacity. Best milk frother for delicious drinks at home |Billy Cadden |July 29, 2021 |Popular-Science 

They can be either square or round and come in sizes ranging from 1 to 22 quarts, with the larger sizes being particularly useful for storing dry goods such as flour, sugar, rice and beans. Why restaurant supply stores are the best places to buy kitchen tools |Aaron Hutcherson |July 23, 2021 |Washington Post 

Since the pandemic started, Woltereck said, Woodard has donated bottles to the museum’s collection, including a Coca-Cola bottle stamped with the city’s name, a glass quart bottle from Cloverland Farms Dairy and a Royal Crown RC Cola bottle. Amateur Baltimore historian unearths new passion for archaeology |Angela Roberts |January 21, 2021 |Washington Post 

Most air fryers have capacities that are measured in quarts and generally range anywhere from two quarts—ideal for two to three servings of food—up to six quarts, which can prepare batches large enough for entertaining and cooking for groups. Best air fryer: Five things to consider |PopSci Commerce Team |December 4, 2020 |Popular-Science 

His court-appointed lawyer was drinking a quart of liquor per day. How the U.S. Justice System Screws Prisoners with Disabilities |Elizabeth Picciuto |December 16, 2014 |DAILY BEAST 

On it are balanced a plate of eggs and toast, an open quart jar of grape jelly, and a beer mug full to the brim with orange juice. The Ridiculousness of Father's Day |P. J. O’Rourke |June 15, 2014 |DAILY BEAST 

In 1603 it was ordered that one quart of best ale, or two of small, should be sold for one penny. Showell's Dictionary of Birmingham |Thomas T. Harman and Walter Showell 

The berries were so thick she did not have to change her position before the towel held over a quart. The Box-Car Children |Gertrude Chandler Warner 

We had a quart of excellent champagne, a pint of decent port and a good cigar, and we felt that the gods were good. Jaffery |William J. Locke 

With his usual predilection for fruit, Phil went off and picked a quart of marsh-berries. Gold-Seeking on the Dalton Trail |Arthur R. Thompson 

Father said it wus ten cents a quart, dat is de whiskey made outen corn, and de brandy wus cheap too. Slave Narratives: a Folk History of Slavery in the United States |Various