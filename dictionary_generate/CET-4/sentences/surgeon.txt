That is Diane Simeone, a pancreatic-cancer surgeon who also runs a research lab at NYU-Langone in New York. How to Fix the Incentives in Cancer Research (Ep. 449) |Stephen J. Dubner |January 28, 2021 |Freakonomics 

In yet others, it wore either the type of mask worn by surgeons or a more protective N95 mask. What did you say? Fabric masks can really muffle voices |Sid Perkins |January 20, 2021 |Science News For Students 

Of course, surgeons like Hagler have specific training and experience that guide them, as all these people with extreme jobs do. Stay calm under pressure with lessons learned in the world’s most stressful careers |Rob Verger |January 8, 2021 |Popular-Science 

“But if he has to show his face, he would be much younger, and I would probably need a good plastic surgeon!” Jeremy Bulloch, Boba Fett in first Star Wars trilogy, dies at 75 |Andrew Dalton |December 18, 2020 |Washington Post 

If you’re a surgeon and you botched the surgery or if you’re a physician who prescribes the wrong medication, there is no amount of compassion that’s going to undo that. How Do You Cure a Compassion Crisis? (Ep. 444) |Stephen J. Dubner |December 17, 2020 |Freakonomics 

“As far as we were aware, and as far as the surgeon was aware, the surgery was a go,” Shaheen sighs. The Insurance Company Promised a Gender Reassignment. Then They Made a Mistake. |James Joiner |December 29, 2014 |DAILY BEAST 

Althea is now re-scheduled with her surgeon for this spring. The Insurance Company Promised a Gender Reassignment. Then They Made a Mistake. |James Joiner |December 29, 2014 |DAILY BEAST 

The new surgeon general certainly has his work cut out for him. The NRA’s Twisted List for Santa |John Feinblatt |December 23, 2014 |DAILY BEAST 

My surgeon told me my bones were so soft he could barely install the screws. You’re Never ‘Cured’ of an Eating Disorder |Carrie Arnold |December 20, 2014 |DAILY BEAST 

Strange was a surgeon who lost the use of his hands in an automobile accident. The Flying Sorcery of Dr. Strange: Benedict Cumberbatch Is Marvel's Most Bizarre Magician |Rich Goldstein |December 8, 2014 |DAILY BEAST 

One party shall provide a surgeon, the other a pair of ordinary cavalry sabres. The Pit Town Coronet, Volume I (of 3) |Charles James Wills 

He said he didn't want his family fidgeting him, and the surgeon said he would be all right in a few days. Ancestors |Gertrude Atherton 

They were followed by a little dried-up Italian army surgeon, who carried under his arm an ominous-looking black case. The Pit Town Coronet, Volume I (of 3) |Charles James Wills 

Walking forth, he strolled down the road towards Calne, intending to ask a question or two of the surgeon. Elster's Folly |Mrs. Henry Wood 

"Only cut deep enough to make it bleed freely," said the surgeon, as he dressed Harry's arm. The Courier of the Ozarks |Byron A. Dunn