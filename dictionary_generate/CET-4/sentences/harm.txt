The global AI ethics efforts under way today—of which there are dozens—aim to help everyone benefit from this technology, and to prevent it from causing harm. AI ethics groups are repeating one of society’s classic mistakes |Amy Nordrum |September 14, 2020 |MIT Technology Review 

These can include factors like transportation and shelter access, and those without the necessary money and resources have the hardest time getting out of harm’s way. Political rhetoric may impact your hurricane preparedness |Ula Chrobak |September 11, 2020 |Popular-Science 

Twitter said it will evaluate a tweet’s potential to cause harm when determining whether it will be removed. How Google, Facebook, and Twitter plan to handle misinformation surrounding 2020 presidential election results |Danielle Abril |September 10, 2020 |Fortune 

And, it can also make it easier for users to locate the information they’re looking for and help you get rid of underperforming content that may be doing more harm than good. How content consolidation can help boost your rankings |George Nguyen |September 10, 2020 |Search Engine Land 

Only then can the industry produce solutions that reduce harm. What’s missing from corporate statements on racial injustice? The real cause of racism. |Amy Nordrum |September 5, 2020 |MIT Technology Review 

I thought about the mother, her fear of the dark, of the harm she feared might come to her daughters. I Tried to Warn You About Sleazy Billionaire Jeffrey Epstein in 2003 |Vicky Ward |January 7, 2015 |DAILY BEAST 

I meant no harm by it, but I remembered how this person talked, and I did it for my Mom and she was not into it. Coffee Talk with Fred Armisen: On ‘Portlandia,’ Meeting Obama, and Taylor Swift’s Greatness |Marlow Stern |January 7, 2015 |DAILY BEAST 

The official spoke on condition of anonymity so as not to harm future access to those embattled communities. ISIS Fight Has a Spy Shortage, Intel Chair Says |Kimberly Dozier |January 2, 2015 |DAILY BEAST 

Is there something wrong with trans people that drives us to self-harm? Dear Leelah, We Will Fight On For You: A Letter to a Dead Trans Teen |Parker Molloy |January 1, 2015 |DAILY BEAST 

Whether or not Hippocrates ever actually said “First, do no harm,” the axiom is central to medical ethics. Why So Many Surgeons Are Psychos |Russell Saunders |December 17, 2014 |DAILY BEAST 

Fajardo seconds the demand of the citizens of Manila that the Audiencia be suppressed, alleging that it does more harm than good. The Philippine Islands, 1493-1898, Volume XX, 1621-1624 |Various 

And if he was worried about Farmer Green's cat, why didn't he dig a hole for himself at once, and get out of harm's way? The Tale of Grandfather Mole |Arthur Scott Bailey 

Calkilate we should do ourselves more harm than him by shooting down his people. Blackwood's Edinburgh Magazine, No. CCCXXXIX. January, 1844. Vol. LV. |Various 

These are obtained easily, whence follow the sinister reports that they give your Majesty, to the harm of the public welfare. The Philippine Islands, 1493-1898, Volume XX, 1621-1624 |Various 

"I wouldn't do him any harm for the world," said Mrs. Kaye, casting down her eyes and looking very young and innocent. Ancestors |Gertrude Atherton