We forge ahead with the same passion for the Institute’s mission, the same distinctive practical optimism, the same gaze toward the future. Sustaining our mission, shaping the conversation |Katie McLean |December 18, 2020 |MIT Technology Review 

Maybe now we needed dozens or hundreds of narrower gazes, using the Transit Technique as the principal method of discovery. My satellite would fit in a small suitcase. |Katie McLean |December 18, 2020 |MIT Technology Review 

From start to finish, keep your gaze locked on the kettlebell. Why (and How) You Should Master the Turkish Get-Up |Hayden Carpenter |November 21, 2020 |Outside Online 

Then slowly move your gaze down to the floor and try to look at your belly button. Pocket This Anti-Stress Routine for Difficult Days |Lauren Bedosky |October 20, 2020 |Outside Online 

Keeping your shoulders relaxed and your chest forward, slowly look up to the ceiling, letting your head follow your gaze. Pocket This Anti-Stress Routine for Difficult Days |Lauren Bedosky |October 20, 2020 |Outside Online 

Even good, arresting visual art is transformed by the gaze of a potential consumer. Sneer and Clothing in Miami: Inside The $3 Billion Woodstock of Contemporary Art |Jay Michaelson |December 6, 2014 |DAILY BEAST 

The show, Bell Hooks argued in Black Looks: Race and Representation, “represents wom[e]n as the object of a phallocentric gaze.” Science-Fiction TV Finds a New Muse: Feminism |David Levesley |November 29, 2014 |DAILY BEAST 

Click on it, gaze over it, and think about which of those states in the two shades of light blue might rush to buy into Obamacare. Will GOP Govs Really Rescue Obamacare? |Michael Tomasky |November 12, 2014 |DAILY BEAST 

Sensitive subjects are met with a short burst of laughter, and serious answers are sandwiched between a piercing gaze. A Belgian Prince, Gorillas, Guerrillas & the Future of the Congo |Nina Strochlic |November 6, 2014 |DAILY BEAST 

I try to catch the eye of this third boy, but he plops down onto a stool and avoids my gaze. Magical Gardens for the Blind, Deaf, and Disabled |Elizabeth Picciuto |October 22, 2014 |DAILY BEAST 

Louis stood firm, though pale and respectful, before the resentful gaze of Elizabeth. The Pastor's Fire-side Vol. 3 of 4 |Jane Porter 

Gaze not upon another man's wife, and be not inquisitive after his handmaid, and approach not her bed. The Bible, Douay-Rheims Version |Various 

The patch of soft green that I knew for the cottonwoods Rutter had spoken of drew my roving gaze whether I would or no. Raw Gold |Bertrand W. Sinclair 

She stopped, and turned to face him, an incredible shyness seeming to cause her to avoid his gaze. St. Martin's Summer |Rafael Sabatini 

Thus enjoined, she took the letter; for a second her eyes met Garnache's glittering gaze, and she shivered. St. Martin's Summer |Rafael Sabatini