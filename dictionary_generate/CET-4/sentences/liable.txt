Then, in July, Faber issued a second ruling that found Bluestone liable for the selenium violations. This Billionaire Governor’s Coal Company Might Get a Big Break From His Own Regulators |by Ken Ward Jr. |September 17, 2020 |ProPublica 

They don’t want to be liable if someone has an adverse reaction. ‘An educational stance’: Publishers mull CBD’s alluring – and complex – commerce opportunities |Max Willens |September 17, 2020 |Digiday 

It meant that a parent corporation like McDonald’s, one of the companies embroiled in litigation over the rule, could be held liable for a franchise owner’s wrongdoing, such as retaliating against workers for trying to unionize. “Cover Up”: House Democrats Subpoena Documents That NLRB Refused to Share in Ethics Investigation |by Ian MacDougall |September 15, 2020 |ProPublica 

If the employee did so, the agency would have terminated a full-time contract with the employee and worked on a contract basis instead leaving the employee liable for paying for their own healthcare and other benefits can . ‘Necessary to attract talent’: How agencies are managing employees’ requests to move to different states |Kristina Monllos |August 26, 2020 |Digiday 

Yesterday a California appeals court ruled Amazon is liable for products sold by third parties on its site. Amazon’s days of dodging liability for its marketplace could be numbered |Marc Bain |August 14, 2020 |Quartz 

Clients who are wary of online transactions are liable to see escorts with print ads as less likely to cheat or scam  them. The Importance of Adult Classifieds |Hazlitt |September 6, 2014 |DAILY BEAST 

And while eBay makes a direct profit from sales, it is generally not liable unless it had knowledge of a suspicious seller. Why eBay Is an Art Forger’s Paradise |Lizzie Crocker |August 19, 2014 |DAILY BEAST 

In fact, the suit tries to somehow find MLB liable for a still picture of Rector posted on a website called NotSportcenter. Sleeping Yankees Fan Files Worst Lawsuit Ever |Ben Jacobs |July 9, 2014 |DAILY BEAST 

When he says that,” Lefty Wilson, the trainer, said, “he's liable to get three goals. Gordie Howe Hockey’s Greatest War Horse |W.C. Heinz |May 31, 2014 |DAILY BEAST 

This was the first time an oil refinery had been held criminally liable under the Act. Pollution Is a Violent Crime—Prosecute It as Such |Jedediah Purdy |May 8, 2014 |DAILY BEAST 

Births, marriages, and deaths were also made liable to duties by the same Act. Showell's Dictionary of Birmingham |Thomas T. Harman and Walter Showell 

Another way of rendering a principal liable for the act of his agent is by ratifying it. Putnam's Handy Law Book for the Layman |Albert Sidney Bolles 

His principal surely would not be liable, though the conductor doubtless would be. Putnam's Handy Law Book for the Layman |Albert Sidney Bolles 

A principal is liable for the statements and representations of his agent that have been expressly authorized. Putnam's Handy Law Book for the Layman |Albert Sidney Bolles 

Distilled water may be used for dilution, but is more liable to cause error. A Manual of Clinical Diagnosis |James Campbell Todd