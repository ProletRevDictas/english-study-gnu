Rodríguez and Maykel González Vivero, director of Tremenda Nota, the Blade’s media partner in Cuba, are among those who the Cuban government has prevented from leaving the country. Cuban authorities threaten to arrest LGBTQ activist, journalist |Michael K. Lavers |September 17, 2020 |Washington Blade 

Because of the large number of votes coming in by mail, “we may have to prepare for election week or even election month,” wrote Elaine Kamarck, director of the Center for Effective Public Management at the Brookings Institution. ProPublica’s Pandemic Guide to Making Sure Your Vote Counts |by Susie Armitage |September 16, 2020 |ProPublica 

The OPM is being headed on an acting basis by its deputy director, Michael Rigas, who has been nominated to become the deputy director for management at the Office of Management and Budget. Committee delays vote on former political commentator to head Office of Personnel Management |Eric Yoder |September 16, 2020 |Washington Post 

Terry Schilling, executive director of the American Principles Project, decried the decision by Facebook in a statement. Facebook slaps fact check on ads stoking fears about transgender kids in girls sports |Chris Johnson |September 15, 2020 |Washington Blade 

Both nonprofits are required only to disclose the salaries of directors, officers and key employees, said Marc Owens, a tax attorney with Loeb & Loeb. Pro-Trump youth group enlists teens in secretive campaign likened to a ‘troll farm,’ prompting rebuke by Facebook and Twitter |Isaac Stanley-Becker |September 15, 2020 |Washington Post 

Toomey lives here with her husband, Mark, a managing director at Goldman Sachs, and their two daughters. How Taryn Toomey’s ‘The Class’ Became New York’s Latest Fitness Craze |Lizzie Crocker |January 9, 2015 |DAILY BEAST 

Decades ago, the writer-director wrote an episode of the animated comedy that never was. Here’s the Lost Judd Apatow ‘Simpsons’ Episode, Penned by Judd Apatow |Asawin Suebsaeng |January 6, 2015 |DAILY BEAST 

This is a Hollywood director at the height of his powers creating original, wildly ambitious epics. Oscars 2015: The Daily Beast’s Picks, From Scarlett Johansson to ‘Boyhood’ |Marlow Stern |January 6, 2015 |DAILY BEAST 

This is not the first time the director has fallen for Russian propaganda. Oliver Stone’s Latest Dictator Suckup |James Kirchick |January 5, 2015 |DAILY BEAST 

His surprise marriage to theater director Sophie Hunter may have broken hearts, but the squeals of delight were even louder. All Your Internet Boyfriends Are Taken: Gosling, Cumberbatch, and now Joseph Gordon-Levitt |Melissa Leon |January 3, 2015 |DAILY BEAST 

He then received the honour of knighthood but had retired from active service and become a director of his company. Fifty Years of Railway Life in England, Scotland and Ireland |Joseph Tatlow 

The Briton, holding the documents in a pudgy hand, looked at the swift-gestured director with portentous solemnity. The Joyous Adventures of Aristide Pujol |William J. Locke 

Mr. Jump, a director on the mine, pointed out a balance-beam that Mr. Trevithick had put up thirty years before. Life of Richard Trevithick, Volume II (of 2) |Francis Trevithick 

A director who performs a different service, serves as an attorney, for example, may receive compensation for it. Putnam's Handy Law Book for the Layman |Albert Sidney Bolles 

Suppose each director of a bank wished to obtain a loan of money from it. Putnam's Handy Law Book for the Layman |Albert Sidney Bolles