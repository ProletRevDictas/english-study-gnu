When you get a fever, your heart rate tends to increase, even before you notice other symptoms, so it can act as an early warning system. The new Fitbit knows when you’re stressed—and how to help you chill |Stan Horaczek |August 26, 2020 |Popular-Science 

Yellow fever, a mosquito-borne hemorrhagic disease that is common in South America and sub-Saharan Africa, annually infects about 200,000 people and causes an estimated 30,000 deaths. New antibodies in just months |Katie McLean |August 19, 2020 |MIT Technology Review 

The two cats that died were among four that had developed severe symptoms, such as jaundice and high fever. How two coronavirus drugs for cats might help humans fight COVID-19 |Erin Garcia de Jesus |August 11, 2020 |Science News 

A high dose of the vaccine produced severe side effects such as fever in 9 percent of volunteers, but a lower dose produced a severe side effect in only 1 percent of people. COVID-19 vaccines by Oxford, CanSino and Pfizer all trigger immune responses |Tina Hesman Saey |July 21, 2020 |Science News 

It can scan people coming into an area to see if they have a fever. Tackling the novel coronavirus calls for novel ideas |Kathiann Kowalski |May 7, 2020 |Science News For Students 

Besides, victory fever had spread like wildfire throughout the Allied armies. Hitler’s Hail Mary |James A. Warren |December 20, 2014 |DAILY BEAST 

The sets—which, really, were a feat of design and direction—appeared to be remnants of a Lewis Carroll fever dream. ‘Peter Pan Live!’ Review: No Amount of Clapping Brings It to Life |Kevin Fallon |December 5, 2014 |DAILY BEAST 

Future lives, careers and attitudes were being determined in this lightly regulated fever. The Castration of Alan Turing, Britain’s Code-Breaking WWII Hero |Clive Irving |November 29, 2014 |DAILY BEAST 

Spirits in Stanleyville were high, and a local 19-year-old was emboldened by independence fever. ‘Argo’ in the Congo: The Ghosts of the Stanleyville Hostage Crisis |Nina Strochlic |November 23, 2014 |DAILY BEAST 

Take Too Many Cooks: a fever dream of a segment that aired at 4:00am earlier this week. Jimmy Kimmel Pranks Kids (Again), Taylor Swift’s 1989 Aerobics, and More Viral Videos |The Daily Beast Video |November 9, 2014 |DAILY BEAST 

Day by day these fretting anxieties and perplexities wasted her strength, and her fever grew higher and higher. Ramona |Helen Hunt Jackson 

Père Bracasse was ill, suffering from rheumatism, bronchitis, fever and corns. The Joyous Adventures of Aristide Pujol |William J. Locke 

When Stanhope entered to him, he found his guest lying on a sofa, in a high state of fever, both from his wounds and agitation. The Pastor's Fire-side Vol. 3 of 4 |Jane Porter 

If the fever ends by crisis, the crisis is accompanied by a rapid and striking increase. A Manual of Clinical Diagnosis |James Campbell Todd 

The spirillum of relapsing fever can be identified by the method for the malarial parasite in fresh blood. A Manual of Clinical Diagnosis |James Campbell Todd