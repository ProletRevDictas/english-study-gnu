After all, even if democratizing access to skills is the first step in a bigger race, it’s not an easy one. Forage, formerly InsideSherpa, raises $9.3 million Series A for virtual work experiences |Natasha Mascarenhas |September 17, 2020 |TechCrunch 

In the 2018 Senate races that led to Republican gains, most votes were cast for Democrats. America needs a democratic revolution |Matthew Yglesias |September 17, 2020 |Vox 

The scientific race for a coronavirus vaccine is moving at record-shattering speed. The risks of moving too fast on a coronavirus vaccine |Sam Baker |September 17, 2020 |Axios 

Polling in each state has moved within a fairly narrow range, as has the race overall. The key shifts in Minnesota and Wisconsin that have improved Biden’s chances of victory |Philip Bump |September 16, 2020 |Washington Post 

Warren, who was one of more than two dozen candidates to run in the Democratic primary, dropped out of the race in March. Trump, in town hall, says he wouldn’t have done anything differently on pandemic |Colby Itkowitz, Josh Dawsey, Felicia Sonmez, John Wagner |September 16, 2020 |Washington Post 

On Thursday, Garcetti ruled himself out of the race to succeed Boxer. The Golden State Preps for the ‘Red Wedding’ of Senate Races |David Freedlander |January 9, 2015 |DAILY BEAST 

Think back to the Bush-Kerry race of 2004, the Thrilla in Vanilla. Jeb Bush’s Unseen Anti-Gay Marriage Emails |Jackie Kucinich |January 9, 2015 |DAILY BEAST 

How far has Congress really evolved on race when in 50 years it has gone from one black senator to two? The Unbearable Whiteness of Congress |Dean Obeidallah |January 8, 2015 |DAILY BEAST 

If Congress accurately reflected our nation on the basis of race, about 63 percent would be white, not 80 percent. The Unbearable Whiteness of Congress |Dean Obeidallah |January 8, 2015 |DAILY BEAST 

Each individual race involves an unusual collaboration between researchers, manufacturers, and public-health entities. The Race for the Ebola Vaccine |Abby Haglage |January 7, 2015 |DAILY BEAST 

His hero, Gulliver, discovers race after race of beings who typify the genera in his classification of mankind. Gulliver's Travels |Jonathan Swift 

Ever since his majority Lord Hetton had annually entered a colt in the great race. The Pit Town Coronet, Volume I (of 3) |Charles James Wills 

Decide about it, ye that are learned in the ethnographic distinctions of our race—but heaven defend us from the Bourbonnaises! Blackwood's Edinburgh Magazine, Volume 60, No. 372, October 1846 |Various 

His unbounded generosity won for him the admiration of all his race, who graciously recognized him as their Maguinoó. The Philippine Islands |John Foreman 

One of the lower and mixed forms of artistic activity, in the case of the child and of the race alike, is personal adornment. Children's Ways |James Sully