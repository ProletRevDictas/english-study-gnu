The block highlighted in red was the one above which the blocks were no longer balanced. How Many Ways Can You Build A Staircase? |Zach Wissner-Gross |February 26, 2021 |FiveThirtyEight 

Some will guard from their cars, while others will pace around the block, flashlights in hand. An Asian American family was being harassed. Neighbors now stand guard each night at their home. |Sydney Page |February 26, 2021 |Washington Post 

Since larger blocks of ice take longer to melt, this system can safely be used over long periods during entertaining without added refrigeration. Best ice pick: A versatile winter tool for camping and more |PopSci Commerce Team |February 25, 2021 |Popular-Science 

Whetstones are essentially blocks of sandpaper in different textures. The best knife sharpener to keep your blades safe and effective |Edmund Torr |February 25, 2021 |Popular-Science 

Some volunteers drive more than an hour to walk these blocks — largely deserted by a combination of fear and pandemic lockdown — to hand out bilingual fliers that explain how to report a crime to police. ‘Nobody came, nobody helped’: Fears of anti-Asian violence rattle the community |Marian Liu, Rachel Hatzipanagos |February 25, 2021 |Washington Post 

Church bells pealed from St. Catherine of Siena parish one block away. The Louisiana Racists Who Courted Steve Scalise |Jason Berry |January 3, 2015 |DAILY BEAST 

During his trek, Brinsley twice passed within a block of a police stationhouse and he almost certainly saw cops along the way. Exclusive: Inside a Cop-Killer’s Final Hours |Michael Daly |December 31, 2014 |DAILY BEAST 

Block 3F is slated for release in 2019, but who knows how much that will slip? New U.S. Stealth Jet Can’t Fire Its Gun Until 2019 |Dave Majumdar |December 31, 2014 |DAILY BEAST 

After walking block after block holding that container, he had suddenly discarded it and was now clutching a gun. Exclusive: Inside a Cop-Killer’s Final Hours |Michael Daly |December 31, 2014 |DAILY BEAST 

If someone wants to ensure a direct and secure connection, no entity, whether a hotel or otherwise, should be able to block it. How ‘Ethical’ Hotel Chain Marriott Gouges Guests in the Name of Wi-Fi Security |Kyle Chayka |December 31, 2014 |DAILY BEAST 

The Spanish troops did not care to venture past a block of buildings in which were the offices and stores of a British firm. The Philippine Islands |John Foreman 

The upper block was left a little thicker, the junction or root of the neck necessitating this. Antonio Stradivari |Horace William Petherick 

The long unbroken block had as many and as various stores as are generally spread over the entire area of a town. Ancestors |Gertrude Atherton 

It had been very scanty information and late in its arrival—too late to enable the master manhunter to block the plan. Hooded Detective, Volume III No. 2, January, 1942 |Various 

How I do wish sometimes to give Ritchie a jog, when there is some stumbling-block that he sticks fast at. The Daisy Chain |Charlotte Yonge