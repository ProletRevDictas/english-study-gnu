It will be Biden’s first prime-time town hall since accepting the Democratic nomination. Election live updates: Trump returns to Wisconsin; Biden to face live audience at town hall |Colby Itkowitz, Felicia Sonmez, John Wagner |September 17, 2020 |Washington Post 

So if the pathogen returns, the cell is already primed to respond faster. ‘Trained Immunity’ Offers Hope in Fight Against Coronavirus |Esther Landhuis |September 14, 2020 |Quanta Magazine 

The overnight nationwide lockdown announced by prime minister Narendra Modi on March 24 forced over 10 million migrant labourers to return to their home states. India’s harsh Covid-19 lockdown displaced at least 10 million migrants |Niharika Sharma |September 14, 2020 |Quartz 

Birgitte’s prime ministership is certainly historic, but when it comes time for her to actually accomplish anything of note, she struggles to get things done. One Good Thing: A Danish drama perfect for political devotees, now on Netflix |Emily VanDerWerff |September 11, 2020 |Vox 

The most common digital encryption technique, RSA, which was invented in 1977, is based on multiplying two large prime numbers. Quantum computers threaten to end digital security. Here’s what’s being done about it |Jeremy Kahn |September 11, 2020 |Fortune 

Domestically, the prime minister maintains the dubious line that he is the only man who can keep the still-fragile peace. Cambodia’s Smoke-and-Mirrors Democracy |David Shaftel |January 9, 2015 |DAILY BEAST 

Hamish Marshall himself is a former staffer of Prime Minister Harper. How Canadian Oilmen Pinkwash the Keystone Pipeline |Jay Michaelson |December 28, 2014 |DAILY BEAST 

Castro actually flew up to Montreal to be a pallbearer at the 2000 funeral of a beloved Canadian Prime Minister, Pierre Trudeau. Canada ♥ Cuba Just Got Complicated |Shinan Govani |December 22, 2014 |DAILY BEAST 

A prime example: your “real name” policy, which unfairly targeted the LGBTQ community. 10 Things That Made Us Want to Turn Off the Internet Forever in 2014 |The Daily Beast |December 15, 2014 |DAILY BEAST 

When they were working together, 35 years ago, she was in her prime and one of the most beautiful women in the world. Alfred Hitchcock’s Fade to Black: The Great Director’s Final Days |David Freeman |December 13, 2014 |DAILY BEAST 

But,” said the prime minister of Flatland, starting a difficulty, “who is to be greatest chief? The Giant of the North |R.M. Ballantyne 

The Minister pointed out to them the attendant difficulties, and referred them to the Prime Minister. The Philippine Islands |John Foreman 

Even then the Prime Minister was with difficulty prevented from bowling during forbidden hours. Punch, or the London Charivari, Volume 107, November 3, 1894 |Various 

Thimbletoes doesn't fancy that, you know, because the Prime Minister has all the honey he wants, by way of a salary. Davy and The Goblin |Charles E. Carryl 

Spencer Perceval, prime minister of Great Britain, shot in the lobby of the house of commons. The Every Day Book of History and Chronology |Joel Munsell