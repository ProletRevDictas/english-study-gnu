Unrated during the pandemicIn the olden days — you know, before 2020 — chefs were judged primarily on what they put on a plate. Now that’s room service: What it’s like to check into a hotel just for dinner |Tom Sietsema |February 12, 2021 |Washington Post 

Maybe it’s because the wealthier wards have higher vaccination rates than the lower-income wards, whose residents are primarily people of color. The mid-pandemic return to school is totally weird for kids. And possibly lonely, too. |Petula Dvorak |February 11, 2021 |Washington Post 

The liability is primarily due to officers who were hired before the 2010 law passed. How the Police Bank Millions Through Their Union Contracts |by Andrew Ford, Asbury Park Press, and Agnes Chang, Jeff Kao and Agnel Philip, ProPublica |February 8, 2021 |ProPublica 

Further, more than 100,000 Palestinians enter Israel every day to work, primarily in construction, most continuing to do so even during the pandemic. Israel leads the world in vaccination rates, but a key group is missing from the data |By Yara M. Asi/The Conversation |February 3, 2021 |Popular-Science 

If you will be primarily wearing your gloves for outdoor winter sports like skiing, you will want to find warm, waterproof gloves that allow for moisture-wicking as well as a secure fit at the wrist to keep snow and cold out. Best winter gloves: Our picks for touch screen gloves, ski gloves, and more |PopSci Commerce Team |February 2, 2021 |Popular-Science 

Saudi Arabia, on the other hand, primarily produces petroleum. Fact-Checking the Sunday Shows: Jan. 4 |PunditFact.com |January 5, 2015 |DAILY BEAST 

Human trafficking was once a crime associated primarily with a range of small to large crime groups. ISIS, Boko Haram, and the Growing Role of Human Trafficking in 21st Century Terrorism |Louise I. Shelley |December 26, 2014 |DAILY BEAST 

The caller mentioned my work, which focused primarily on consumer products, mobile apps, emerging start-ups, and web trends. A Female Writer’s New Milestone: Her First Death Threat |Annie Gaus |December 1, 2014 |DAILY BEAST 

This has long stopped being primarily about the death of an unarmed young black man in St. Louis. Ferguson, Immigration, and ‘Us Vs. Them’ |Gene Robinson |November 27, 2014 |DAILY BEAST 

Primarily because the way most of us in the west and Anglophile world view the history of the Great War is from the winning side. How WWI Produced the Holocaust |J.P. O’Malley |November 21, 2014 |DAILY BEAST 

This seems to be contrary to the spirit and intent of the act, which is primarily to centralize reserves in Federal Reserve Banks. Readings in Money and Banking |Chester Arthur Phillips 

But it may be primarily laid hold upon in some instances in the formal performance of that exercise. The Ordinance of Covenanting |John Cunningham 

The power to expel members is incident to every society or association unless organized primarily for gain. Putnam's Handy Law Book for the Layman |Albert Sidney Bolles 

It is not usual to find gonococci when many other bacteria are present, even though the pus is primarily of gonorrheal origin. A Manual of Clinical Diagnosis |James Campbell Todd 

The Appalachian Ridge consists primarily of a narrow strip of land thirty-five to one hundred miles wide. Hallowed Heritage: The Life of Virginia |Dorothy M. Torpey