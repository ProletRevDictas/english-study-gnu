Or has the see and hear and speak-no-evil stance of the Republican House persuaded him that he is in the clear? The Price of Steve Scalise’s Silence |Jason Berry |January 7, 2015 |DAILY BEAST 

These days, plenty of women are turning to online sites for no-frills male companionship. Career-Minded Women Turn to Male Escorts For No-Strings Fun and (Maybe) Sex |Aurora Snow |January 3, 2015 |DAILY BEAST 

Riffing off the slogan “Now Everyone Can Fly,” the carrier offered no-frills flights that were both cheap and plentiful. The Presumed Crash of AirAsia Flight QZ8501 Is Nothing Like MH370 |Lennox Samuels |December 29, 2014 |DAILY BEAST 

And why would its ostensible spokesperson refuse to answer yes/no questions about them? How Canadian Oilmen Pinkwash the Keystone Pipeline |Jay Michaelson |December 28, 2014 |DAILY BEAST 

Because Wright was a no-show in criminal court to face the loud music and pot bust he already had an outstanding warrant. The Navy ‘Hero’ Who Pimped an HIV-Positive Teen |M.L. Nestel |December 11, 2014 |DAILY BEAST 

My No. 4239 addressed to Maxwell and repeated to you was sent before receiving your telegram under reply. Gallipoli Diary, Volume I |Ian Hamilton 

He was a new breed, that parson, a genuwine no-two-alike, come-one-in-a-box kind. Alec Lloyd, Cowpuncher |Eleanor Gates 

A pupil had loaned money to a horse-dealer who lived at No. 715 of a certain street. Assimilative Memory |Marcus Dwight Larrowe (AKA Prof. A. Loisette) 

Papa, can't I go to the zoologerical rooms to see the camomile fight the rhy-no-sir-ee-hoss? The Book of Anecdotes and Budget of Fun; |Various 

On her hand was one large pustulous sore, which resembled that delinated in Plate No. 1. An Inquiry into the Causes and Effects of the Variolae Vaccinae |Edward Jenner