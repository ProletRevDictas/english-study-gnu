Unlike government funding, donations come with minimal accountability and no guaranteed public oversight. Can Billionaires Really Save Us from Climate Disaster? |Heather Hansman |February 12, 2021 |Outside Online 

Canopy Growth, a Canadian cannabis company, said it is accelerating its US growth strategy based on expectations of “significant reform” led by the Democratic party now that it controls two branches of government. Canada’s largest cannabis company is turning its attention to the US |Karen Ho |February 11, 2021 |Quartz 

Coastal peasants and Afromestizos farmers continued to fight for independence from the central government. The problem of environmental racism in Mexico today is rooted in history |Jayson Porter |February 11, 2021 |Washington Post 

In an economy where governments are printing money hand over fist, people want a more secure place to put their assets. Bitcoin’s Blowing Up, and That’s Good News for Human Rights. Here’s Why |Vanessa Bates Ramirez |February 10, 2021 |Singularity Hub 

The good part about it, if you would call it that, is that it is impossible to imagine how the government would be able to block a lot of this harmful speech. Twenty-Six Words Created the Internet. What Will It Take to Save It? |Stephen Engelberg |February 9, 2021 |ProPublica 

A few years back, designer John Galliano was fined by the government for sharing just such anti-semitic sentiments in public. Politicians Only Love Journalists When They're Dead |Luke O’Neil |January 8, 2015 |DAILY BEAST 

Not actual CIA agents, but U.S. government personnel who have worked very closely with the CIA, and who are fans of the show. ‘Archer’ Creator Adam Reed Spills Season 6 Secrets, From Surreal Plotlines to Life Post-ISIS |Marlow Stern |January 8, 2015 |DAILY BEAST 

I think the response of the French government so far has been pretty appropriate in that regard. Harry Shearer on The Dangerous Business of Satire |Lloyd Grove |January 8, 2015 |DAILY BEAST 

The United States government might not release that information for years, if ever. Was Sony Hit With a Second Hack? |Shane Harris |January 8, 2015 |DAILY BEAST 

But most likely it was linked to the way priests identify with the poor in the face of government and criminal abuses. Mexico’s Priests Are Marked for Murder |Jason McGahan |January 7, 2015 |DAILY BEAST 

Dockier, a prominent leader of the Levelers, in the times of the English commonwealth, was shot by order of the government. The Every Day Book of History and Chronology |Joel Munsell 

The patriarchal decree of the government was a good deal of a joke on the plains, anyway—except when you were caught defying it! Raw Gold |Bertrand W. Sinclair 

History gives them scant notice, and the Federal government has failed to reward them as they deserve. The Courier of the Ozarks |Byron A. Dunn 

A royal decree (December 31, 1622) orders the Dominicans in the Philippines not to meddle in affairs of government. The Philippine Islands, 1493-1898, Volume XX, 1621-1624 |Various 

For the first time in his experience the Corsican had to meet the forces of a nation and not of a government. Napoleon's Marshals |R. P. Dunn-Pattison