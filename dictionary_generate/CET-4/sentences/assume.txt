Bishop Garrison, assuming a newly created role, will directly advise Defense Secretary Lloyd Austin on diversity and inclusion issues. The Pentagon is taking a major step to deal with its diversity problems |Alex Ward |February 12, 2021 |Vox 

Covington, who began disparaging Usman months before their fight, assumed a more vitriolic tone than Woodley, provoking several confrontations including one in the Palms Casino Resort buffet line the day after Usman defeated Woodley. As Kamaru Usman edges toward UFC greatness, his former teammate wants to stop him |Glynn A. Hill |February 12, 2021 |Washington Post 

Since you assume your benefits are on hold, and you know nothing about the scammer's lies and schemes to steal your identity, you're unaware that your payments are being illegally sent to the scammers. Hints From Heloise: New scam tied to coronavirus pandemic |Heloise Heloise |February 12, 2021 |Washington Post 

According to the authors, it's generally been assumed that whichever creature inflicts the most damage wins the fight. Caged heat: Mesquite bugs battle in a plastic cup—for science! |Jennifer Ouellette |February 11, 2021 |Ars Technica 

A parent will always assume that it is their child’s friend who suggested doing the prohibited thing, not their beloved progeny. Half of Republicans say that the Capitol violence was mostly antifa’s fault |Philip Bump |February 11, 2021 |Washington Post 

When our elected representatives assume their respective offices, they take an oath to “protect and defend the Constitution.” Are Police Stealing People’s Property? |Joan Blades, Matt Kibbe |January 2, 2015 |DAILY BEAST 

Nor should we ever assume that weather alone, however extreme, should be fatal to a commercial flight. Did Bad Weather Bring Down AirAsia 8501? |Clive Irving |December 29, 2014 |DAILY BEAST 

Campaigns like opechatesgays.com assume that LGBT people are an interest group with only one interest: their own. How Canadian Oilmen Pinkwash the Keystone Pipeline |Jay Michaelson |December 28, 2014 |DAILY BEAST 

It occurs to me that Mount must assume that Hitchcock has read it--after all, it came from him. Alfred Hitchcock’s Fade to Black: The Great Director’s Final Days |David Freeman |December 13, 2014 |DAILY BEAST 

Now that I am free, I have Medicaid and doctors no longer assume I am malingering. A Million Ways to Die in Prison |Daniel Genis |December 8, 2014 |DAILY BEAST 

Many adults assume that a child can look at a landscape as they look at it, taking in the whole picturesque effect. Children's Ways |James Sully 

It will be found that as a whole they assume a flat position, and are very easily handled. How to Know the Ferns |S. Leonard Bastin 

He was told that a son must not play in his father's presence, nor assume free or easy posture before him. Our Little Korean Cousin |H. Lee M. Pike 

When people argue in this strain, I immediately assume the offensive. Tobacco; Its History, Varieties, Culture, Manufacture and Commerce |E. R. Billings. 

We may fairly assume the presence here of one or two, if not more, assistants, besides a pupil or improver. Antonio Stradivari |Horace William Petherick