Testing is crucial both for passenger confidence and for the confidence of border authorities, and—given the volume of passengers passing through large airports—speed is of the essence. ‘We know the dogs work’: Coronavirus-sniffing dogs go into action at Helsinki Airport |David Meyer |September 23, 2020 |Fortune 

At one point the airport itself questioned the legality of the fee and moved to join the litigation, but later backed away from the challenge. Port Settlements Will Send Refunds to Airport Rental Car Customers |Ashly McGlone |September 22, 2020 |Voice of San Diego 

Humanitarian permits can be granted for medical treatment and some businessmen get more expansive permits that allow them to use the airport and pass freely at checkpoints. Palestinians Are Breaking Lockdown to Go to the Beach |Fiona Zublin |September 18, 2020 |Ozy 

Limiting touchpoints and unnecessary face-to-face interactions will change the way airports operate. Spirit Airlines starts testing biometric check-ins |Frederic Lardinois |September 3, 2020 |TechCrunch 

The opening will give the UAE’s biggest carriers—Emirates and Etihad—an opportunity to feed Israeli passengers through their airport hubs in Dubai and Abu Dhabi, respectively, connecting to destinations further east and west. Israel’s El Al Airlines makes historic flight through Saudi airspace en route to UAE |Bernhard Warner |August 31, 2020 |Fortune 

It opens with Huckabee's dramatic recollection of going through security at the airport. Huckabee 2016: Bend Over and Take It Like a Prisoner! |Olivia Nuzzi |January 8, 2015 |DAILY BEAST 

That apparently includes some members of the management of the airport itself and some air traffic controllers. Annoying Airport Delays Might Prevent You From Becoming the Next AirAsia 8501 |Clive Irving |January 6, 2015 |DAILY BEAST 

Her travel clique has been known to arrive at an airport, bags packed, passport-in-hand, within hours of spotting a deal. ‘We Out Here’: Inside the New Black Travel Movement |Charlise Ferguson |January 4, 2015 |DAILY BEAST 

Tim Russert and I are driving back to the Albany airport after taking our kids to the baseball Hall of Fame in Cooperstown. Mario Cuomo, Always Moving Us Toward the Light |Mike Barnicle |January 4, 2015 |DAILY BEAST 

Antoine, a 40-year-old DJ who works at the airport, says he fears for his children if the police stop doing their jobs. Ground Zero of the NYPD Slowdown |Batya Ungar-Sargon |January 1, 2015 |DAILY BEAST 

Every wheel in the District motor pool was on the highway from the airport, shuttling in the wedding-party. The Great Potlatch Riots |Allen Kim Lang 

The boys waited until Ricardo Montoya had breakfasted, then rode with him to the airport. The Flaming Mountain |Harold Leland Goodwin 

By agreement, the countdown was to be broadcast to all aircraft over one of the airport frequencies. The Flaming Mountain |Harold Leland Goodwin 

I hailed a cab to the Logan Airport, changed my reservations to an earlier plane, and returned to Washington. The Professional Approach |Charles Leonard Harness 

As he flew along toward the Newark airport, a shadow fell athwart the wing and he looked up. The Onslaught from Rigel |Fletcher Pratt