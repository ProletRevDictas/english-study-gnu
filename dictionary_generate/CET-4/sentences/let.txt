It’s always a let down when a movie doesn’t live up to your expectations. ‘Evan Hansen’ is better than you think – and that’s too bad |John Paul King |September 30, 2021 |Washington Blade 

So as long as I can convince physicians that they should prescribe this drug, then I’m going to invest in this, rather than investing in let’s say something extraordinarily risky, which has enormous potential upside from a public health standpoint. How Pharma’s Lucrative Patent System Is Complicating The Pandemic |Anna Rothschild |May 24, 2021 |FiveThirtyEight 

So we have built an algorithm in our economy, which is clearly wrong, just like Facebook’s focus on let’s show people things that are more engaging, turned out to be wrong. Building a better data economy |Jason Sparapani |March 11, 2021 |MIT Technology Review 

This entire ordeal reeks of bureaucratic overreach being bandied about in the name of “let-us-save-the-children” politics. The University Of New Orleans’ Cigarette Ban Is Total BS |Chloé Valdary |October 21, 2014 |DAILY BEAST 

Uh, what part of last-team-to-let-Tim-Tebow-go are you not understanding, non-believers? The Tithe Is High: End the GOP Civil War! |Bill Schulz |January 11, 2014 |DAILY BEAST 

The White House just had to scratch it out with a real strategy and a never-let-go attitude. Memo: The Aaron Sorkin Model of Political Discourse Doesn't Actually Work |Megan McArdle |April 23, 2013 |DAILY BEAST 

The jukebox blasts “Aquarius/Let the Sunshine In” by the 5th Dimension. Dennis Hopper and the Cult of Nonreaders |Tom Folsom |March 8, 2013 |DAILY BEAST 

Beyond that, Wallace spoke openly about his struggles with depression well before we entered the let-it-all-hang-out culture. Howard Kurtz Remembers Mike Wallace, Legendary CBS Newsman, Dead at 93 |Howard Kurtz |April 8, 2012 |DAILY BEAST 

Further, why not sub-let the flat to any of your own friends who can afford to give you a few guineas a week for it? Punch, or the London Charivari, Vol. 152, May 16, 1917. |Various 

Mr. O'Connell was, in fact, "a middle man;" he rented extensive lands, and sub-let at a very large profit. The History of England in Three Volumes, Vol.III. |E. Farr and E. H. Nolan 

Burmans love it, and no feast is complete without it, indeed a packet of let-pet is an invitation to something festive. Round the Wonderful World |G. E. Mitton 

At one place it is let-pet, or pickled tea, though the plant from which the stuff is made is not really a tea-plant. Round the Wonderful World |G. E. Mitton 

All was serene and lovely on the surface, however, with many won't-you-let-me's and please-do-now's on both sides. The "Genius" |Theodore Dreiser