While there are lags in sales, the tendency is to continually check in with people. ‘It can take on a panopticon effect’: Slack’s presenteeism problem grows with no end in sight for remote work |Lucinda Southern |August 28, 2020 |Digiday 

This right-of-sale dynamic appears to be particularly acute in the free, ad-supported streaming TV market. The streaming wars have escalated over turf grabs |Tim Peterson |August 26, 2020 |Digiday 

Business publisher Quartz had to close its London office space after ad sales halved in May, cutting 40% of its global workforce. ‘Not enough money to go around’: US digital-media publishers curb international expansion |Lucinda Southern |August 26, 2020 |Digiday 

Facebook Shops is an e-commerce feature that lets businesses list products for sale across its apps via virtual storefronts. As online shopping intensifies, e-commerce marketers are becoming increasingly reliant on Facebook’s ads |Seb Joseph |August 25, 2020 |Digiday 

Our online and direct-to-consumer sales are up 700% since January, but our in-store retail sales are down 75%. OneRepublic’s Ryan Tedder on launching a hemp-infused sparkling water brand |Rachel King |August 24, 2020 |Fortune 

Tickets go on sale to the public January 15; check back then for a link and an early peek at the inspiring lineup of speakers. Save the Date: Women in the World 2015 | |December 23, 2014 |DAILY BEAST 

In October, news broke that Regal hired Morgan Stanley to explore a possible sale. The Right-Wing Billionaire Who Bowed to North Korea over ‘The Interview’ |Asawin Suebsaeng |December 19, 2014 |DAILY BEAST 

Dee Dee candles, rosaries, shirts and prints are offered for sale near the gallery's door as a kind of consolation. ‘All Good Cretins Go to Heaven’: Dee Dee Ramone’s Twisted Punk Paintings |Melissa Leon |December 15, 2014 |DAILY BEAST 

Its first sale was a Dan Colen work, which sold for “something like $120,000 or $150,000.” William, Kate, and Jay Z’s Favorite Art Star: Alexander Gilkes' World of Rock Stars and Royalty |Tim Teeman |December 10, 2014 |DAILY BEAST 

But perhaps the most spectacular lot in the sale is a silver jug, a birthday present to Churchill from his War Cabinet in 1942. Churchill’s Secret Treasures for Sale: A British PM’s Life on the Auction Block |Tom Teodorczuk |December 8, 2014 |DAILY BEAST 

He conceived an idea of securing agents among the colored people, and in that way effect a good sale. The Homesteader |Oscar Micheaux 

Fruit-trees are clearly too scarce, though Cherries in abundance were offered for sale as we passed. Glances at Europe |Horace Greeley 

He continued its sale, however, as a kingly monopoly, allowing only those to engage in it who paid him for the privilege. Tobacco; Its History, Varieties, Culture, Manufacture and Commerce |E. R. Billings. 

The duty on importation had been only twopence per pound, a moderate sum in view of the prices realized by the sale of it. Tobacco; Its History, Varieties, Culture, Manufacture and Commerce |E. R. Billings. 

We are going to send our butler to the sale to-morrow, to pick up some of that sixty-four. The Posthumous Papers of the Pickwick Club, v. 2(of 2) |Charles Dickens