Life can be so pleasant in San Diego that we sometimes tend to forget or gloss over that we’re not in heaven, not yet. Politics Report: A Poll and a Court Ruling in Key Council Race |Scott Lewis |August 22, 2020 |Voice of San Diego 

June, August, and September are often considered to be some of the most pleasant months of the year in Cleveland. Cleveland – The Rock and Roll Capital of the World |LGBTQ-Editor |August 21, 2020 |No Straight News 

Design solutions must not only effectively contain the spread of the virus, but also make going back to work a pleasant, even beautiful experience. How to design an office that employees will want to return to |Anne Quito |August 9, 2020 |Quartz 

However, summers are amazing, and autumns are mild and pleasant. Chicago: A Midwestern Jewel for the LGBTQ Community |LGBTQ-Editor |July 11, 2020 |No Straight News 

Good stories and user-generated content will always leave a pleasant aftertaste and increase interest in your brand. Hottest user-centric video advertising trends of 2020: CTV, vertical, and social formats |Ivan Guzenko |June 12, 2020 |Search Engine Watch 

When we had that meeting in the Caribbean, Jeffrey was holding his own and not only was he a pleasant host, he was pleasant guy. Sleazy Billionaire’s Double Life Featured Beach Parties With Stephen Hawking |M.L. Nestel |January 8, 2015 |DAILY BEAST 

However, in reality, this should be the norm, not a “pleasant surprise.” Muslims & Jews Unite vs. Abercrombie & Fitch |Dean Obeidallah |December 16, 2014 |DAILY BEAST 

The Internet is like booze—a little bit gives you a pleasant buzz. 10 Things That Made Us Want to Turn Off the Internet Forever in 2014 |The Daily Beast |December 15, 2014 |DAILY BEAST 

That the Globes did this year, rewarding both Rodriguez and the series in Best Comedy, is a pleasant surprise. 15 Enraging Golden Globe TV Snubs and Surprises: Amy Poehler, 'Mad Men' & More |Kevin Fallon |December 11, 2014 |DAILY BEAST 

Office workers have pleasant picnic lunches on the empty roads. Chinese Tourists Are Taking Hong Kong Protest Selfies |Brendon Hong |October 23, 2014 |DAILY BEAST 

The flute and the psaltery make a sweet melody, but a pleasant tongue is above them both. The Bible, Douay-Rheims Version |Various 

And Jimmy thought that if he must wait for him again he would wait in a pleasant place. The Tale of Grandfather Mole |Arthur Scott Bailey 

Now this morning I saw it put down for to-day Very pleasant, and I knew for sartin it would rain before night. The Book of Anecdotes and Budget of Fun; |Various 

He was a tall, active young fellow with a pleasant face and a pair of fine black eyes. St. Martin's Summer |Rafael Sabatini 

He couldn't know now whether she was serious or merely joking; but notwithstanding it sounded pleasant to his ears. The Homesteader |Oscar Micheaux