They are specific to whatever product or service you’re selling. A comprehensive guide on using Google Trends for keyword research |Aayush Gupta |February 12, 2021 |Search Engine Watch 

Previously, there was no specific requirement of what type of mask should be worn. NHL adds game-day rapid testing to coronavirus protocols |Samantha Pell |February 12, 2021 |Washington Post 

He now associates different areas with specific types of trash. The ‘garbage guy’ walks 12 miles a day around D.C. picking up trash: ‘I’ll pick up pretty much anything.’ |Sydney Page |February 11, 2021 |Washington Post 

The specifics of this call were unique, but the theme was a common refrain. The loneliness of an interrupted adolescence |Ellen McCarthy |February 11, 2021 |Washington Post 

Oh, Naaaaaaancy is a very specific scene from a horror movie. Capitol rioters searched for Nancy Pelosi in a way that should make every woman’s skin crawl |Monica Hesse |February 11, 2021 |Washington Post 

One specific kind of emergency is at the heart of this, such as when an airplane suffers a loss of stability at night. Flight 8501 Poses Question: Are Modern Jets Too Automated to Fly? |Clive Irving |January 4, 2015 |DAILY BEAST 

So too with a vaccine that provokes a specific immune response aimed at a specific RNA sequence. When You Get the Flu This Winter, You Can Blame Anti-Vaxxers |Kent Sepkowitz |January 1, 2015 |DAILY BEAST 

Mistletoes infections can kill individual trees and stands of trees, and most mistletoe species attack specific tree species. Mistletoe is the Vampire of Plants |Helen Thompson |December 21, 2014 |DAILY BEAST 

“Protocols have specific meanings behind them, rather than do what I say because I say so,” she added. Dungeons and Genital Clamps: Inside a Legendary BDSM Chateau |Ian Frisch |December 20, 2014 |DAILY BEAST 

“Very few agencies offer police any specific guidance or training on how to question people with ID,” said Garrett. How the U.S. Justice System Screws Prisoners with Disabilities |Elizabeth Picciuto |December 16, 2014 |DAILY BEAST 

The specific gravity is most conveniently estimated by means of the urinometer—Squibb's is preferable (Fig. 14). A Manual of Clinical Diagnosis |James Campbell Todd 

One frequently wishes to ascertain the specific gravity of quantities of fluid too small to float an urinometer. A Manual of Clinical Diagnosis |James Campbell Todd 

The specific gravity method is very useful when special instruments are not at hand. A Manual of Clinical Diagnosis |James Campbell Todd 

The Specific Gravity is the relative weight of a body compared to an equal bulk of some other body taken as a standard. Assimilative Memory |Marcus Dwight Larrowe (AKA Prof. A. Loisette) 

A special agent is authorized to do a specific thing, to sell a home, buy a horse, or effect some particular end or purpose. Putnam's Handy Law Book for the Layman |Albert Sidney Bolles