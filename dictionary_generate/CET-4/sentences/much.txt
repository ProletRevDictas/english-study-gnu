However much we gossip about heterosexual couples with large age gaps, we at least refrain from calling them sex offenders. Freaking Out About Age Gaps in Gay Relationships Is Homophobic |Samantha Allen |January 9, 2015 |DAILY BEAST 

Between 25 and 30, you’re trying to decide how much longer before you start growing a beard and calling yourself ‘Daddy. Freaking Out About Age Gaps in Gay Relationships Is Homophobic |Samantha Allen |January 9, 2015 |DAILY BEAST 

Her style, much like her diminutive nickname, is best described as “Hamptons twee”—preppy and peppy. How Taryn Toomey’s ‘The Class’ Became New York’s Latest Fitness Craze |Lizzie Crocker |January 9, 2015 |DAILY BEAST 

As far as I can tell, this magazine spent as much time making fun of French politicians as it did of Muslims or Islam. Harry Shearer on The Dangerous Business of Satire |Lloyd Grove |January 8, 2015 |DAILY BEAST 

Much of the media coverage around eating disorders surrounds celebrities and models. How Skinny Is Too Skinny? Israel Bans ‘Underweight’ Models |Carrie Arnold |January 8, 2015 |DAILY BEAST 

You would not think it too much to set the whole province in flames so that you could have your way with this wretched child. St. Martin's Summer |Rafael Sabatini 

Edna did not reveal so much as all this to Madame Ratignolle that summer day when they sat with faces turned to the sea. The Awakening and Selected Short Stories |Kate Chopin 

He was too drowsy to hold the thought more than a moment in his mind, much less to reflect upon it. The Wave |Algernon Blackwood 

In the drawing-room things went on much as they always do in country drawing-rooms in the hot weather. The Pit Town Coronet, Volume I (of 3) |Charles James Wills 

I hate to be long at my toilette at any time; but to delay much in such a matter while travelling is folly. Blackwood's Edinburgh Magazine, Volume 60, No. 372, October 1846 |Various