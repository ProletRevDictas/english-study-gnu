Vladimir Putin endorses plan to tweet about Barneys models from bathroom of his/her choice. Up to a Point: PJ O’Rourke on Sochi and Senate Slackers |P. J. O’Rourke |February 7, 2014 |DAILY BEAST 

No, not because of that whole selling-his-soul-to-Satan business. The Cheneys’ Gay Marriage War |Michelle Cottle |November 18, 2013 |DAILY BEAST 

The social media was quick to pick up the word and redefine it as “someone who fights for his/her individual rights.” Smiling Under a Cloud of Tear Gas: Elif Shafak on Istanbul’s Streets |Elif Shafak |June 11, 2013 |DAILY BEAST 

The story showcased him as a down-on-his-luck and homeless father struggling to survive with his son. The Not-So-Fresh Prince: Will Smith Falls Flat |Allison Samuels |June 4, 2013 |DAILY BEAST 

Or of dinners where the wealthiest friend picks up the check while everyone else looks at his/her sneakers. Income Inequality Within Families is Emerging as a Major Issue |Janna Malamud Smith |January 19, 2013 |DAILY BEAST 

I've been a-turnin' the bis'ness over in my mind, and he may make his-self easy, Sammy. The Pickwick Papers |Charles Dickens 

“Follows-his-own-fancy,” she repeated, as one repeats a strange phrase, the meaning of which is obscure. Unveiling a Parallel |Alice Ilgenfritz Jones and Ella Marchant 

The crowd hurried to meet it, and Litvinov followed it, dragging his-207- feet like a condemned man. Smoke |Turgenev Ivan Sergeevich 

Of medium height, he gave the impression of being tall-his head thrown up, and like a lion's, rather large for his body. Mark Twain, A Biography, 1835-1910, Complete |Albert Bigelow Paine 

Loathsome prigs, stiff conventions, editor of cheap magazines ladled in Sir Wots-his-name. Instigations |Ezra Pound