So, not in the immediate term, but if you think about how much money we’re spending, it’s completely appropriate. Remembrance of Economic Crises Past (Ep. 425) |Stephen J. Dubner |July 9, 2020 |Freakonomics 

We need to have a really frank public discussion about what we think of appropriate trade-offs. All kinds of outbreaks, from COVID-19 to violence, share the same principles |Cassandra Willyard |July 7, 2020 |Science News 

He warned that hospitalizations and deaths are an indicator that tends to lag an increase in new cases, and argued that pointing to those numbers to justify keeping businesses opened isn’t appropriate. Morning Report: What’s in Faulconer’s ‘Complete Communities’ Plan |Voice of San Diego |July 7, 2020 |Voice of San Diego 

Establish and follow appropriate data retention and deletion policies for each type of sensitive data. Beyond the AI hype cycle: Trust and the future of AI |Jason Sparapani |July 6, 2020 |MIT Technology Review 

At the start of the pandemic, businesses within cities were focused on getting their employees working remotely and securely, with access to the appropriate tools. Cities still have a place in the post-pandemic world—but they have to be different. Here’s how |jakemeth |July 4, 2020 |Fortune 

Is that a utilitarian approach—that you need to understand how institutions have changed to understand the way they are? Thank Congress, Not LBJ for Great Society |Julian Zelizer, Scott Porch |January 4, 2015 |DAILY BEAST 

Like any good marketer, Silverman says he has sales figures proving his approach is working. The Case Against In-Your-Face Atheism |Steve Neumann |January 4, 2015 |DAILY BEAST 

But we were attempting a deliberate naiveté, a decision to approach these books as if they might have something to teach us. American Democracy Under Threat for 250 Years |Jedediah Purdy |December 28, 2014 |DAILY BEAST 

This is kind of an accidental career for me—which is why I approach it with irreverence and playfulness. DJ Spooky Wants You To Question Everything You Know About Music, Technology, and Philosophy |Oliver Jones |December 27, 2014 |DAILY BEAST 

But this approach can be troublesome for a variety of reasons. The Craziest Date Night for Single Jews, Where Mistletoe Is Ditched for Shots |Emily Shire |December 26, 2014 |DAILY BEAST 

In truth, it was so intently engaged with a sleeping seal that it had not observed the approach of the sledge. The Giant of the North |R.M. Ballantyne 

Gaze not upon another man's wife, and be not inquisitive after his handmaid, and approach not her bed. The Bible, Douay-Rheims Version |Various 

There seemed the flavour of some strange authority in her that baffled all approach to the former intimacy. The Wave |Algernon Blackwood 

He that adoreth God with joy, shall be accepted, and his prayer shall approach even to the clouds. The Bible, Douay-Rheims Version |Various 

Perhaps the nearest approach to a pure æsthetic enjoyment in these early days is the love of flowers. Children's Ways |James Sully