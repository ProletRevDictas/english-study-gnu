The question now is if more countries — like Oman and Sudan — will follow suit. Trump announces that Bahrain will normalize relations with Israel |Alex Ward |September 11, 2020 |Vox 

If that effort fails, the agency can then bring suit against the company. The U.S. Equal Employment Opportunity Commission Confirms a Pattern of Age Discrimination at IBM |by Peter Gosselin, special to ProPublica |September 11, 2020 |ProPublica 

Epic has filed a separate suit with similar claims against Google. Apple countersues Epic over ‘unlawful’ Fortnite payment system |Verne Kopytoff |September 8, 2020 |Fortune 

WNBA players have been leading on social justice issues for a while now, and once the NBA players decided not to play, it made sense that WNBA players would follow suit. What Happened In The NBA This Week? |Sara Ziegler (sara.ziegler@fivethirtyeight.com) |August 28, 2020 |FiveThirtyEight 

Three state attorneys general have filed suits related to recent changes in the postal service. USPS delays threaten women’s access to birth control |ehinchliffe |August 27, 2020 |Fortune 

San Francisco was the first city to pass one in 2006; since then, 14 other cities and three states have followed suit. Christie Blames Parents for Bad Economy |Monica Potts |January 3, 2015 |DAILY BEAST 

Eventually, DeCrow and Seidenberg filed suit against the East Village mainstay. The Bars That Made America Great |Nina Strochlic |December 28, 2014 |DAILY BEAST 

The pieces are near-identical, excepting the signature buttons on the Chanel suit and a few small tailoring details. The Big Business of Fashion Counterfeits |Lizzie Crocker |December 24, 2014 |DAILY BEAST 

We meet in his study, where Hitchcock sits waiting, dressed in his black suit. Alfred Hitchcock’s Fade to Black: The Great Director’s Final Days |David Freeman |December 13, 2014 |DAILY BEAST 

He had a tailor who ran up dozens of the same suit in different sizes to account for slight variations in his weight. Alfred Hitchcock’s Fade to Black: The Great Director’s Final Days |David Freeman |December 13, 2014 |DAILY BEAST 

Behold a dumpy, comfortable British paterfamilias in a light flannel suit and a faded sun hat. God and my Neighbour |Robert Blatchford 

He is what the bill wishes to make for us, a regular root doctor, and will suit the place exactly. The Book of Anecdotes and Budget of Fun; |Various 

We had three long tables which Liszt arranged to suit himself, his own place being in the middle. Music-Study in Germany |Amy Fay 

Not a dollar did he possess—not even did he have a suit of clothes any more, and wore every day his corduroys. The Homesteader |Oscar Micheaux 

When Dan put his tobacco back unbitten, it was always an infallible sign that something had gone in a way that did not suit him. The Courier of the Ozarks |Byron A. Dunn