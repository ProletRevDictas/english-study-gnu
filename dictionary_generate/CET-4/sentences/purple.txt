However, the observations in purple exceeded four inches and launched into our boom scenario. After Sunday’s slush fest, another winter storm threatens by Wednesday night |Jason Samenow, Wes Junker |February 8, 2021 |Washington Post 

Schools that opened while they were in the red tier can actually open in the purple tier. The Learning Curve: Is It Vaccines for All or No School? |Will Huntsberry |February 4, 2021 |Voice of San Diego 

Schools that had not offered on-campus schooling for at least one entire grade level at least part of the week during the red tier cannot expand reopenings while in the purple tier – which San Diego County is in now. State: School Districts Like San Diego Unified Cannot Reopen Now |Ashly McGlone |January 26, 2021 |Voice of San Diego 

On the ceiling of a small chamber in another Sulawesi cave, the researchers found a large pig painting — like the others, executed in red or dark red and purple mineral pigments — that dates to between 32,000 and 73,400 years ago. One of the oldest known cave paintings has been found in Indonesia |Bruce Bower |January 13, 2021 |Science News 

Spearheaded by Stacey Abrams and her organization Fair Fight, together with many activist and grassroots organizations, Georgia turned a brilliant deep shade of blue, very close to purple, but blue nonetheless. Georgia Is Just the Beginning. It's Time to Tell a New Story About the South |Brittney Cooper |January 8, 2021 |Time 

Black and purple bunting went up over the doorway at the 84th Precinct stationhouse where Ramos and Liu had been assigned. 'Please Don't Die!': The Frantic Battle to Save Murdered Cops |Michael Daly |December 22, 2014 |DAILY BEAST 

She says that every film she makes, she has to hit someone—The Color Purple, The Butler, and Selma. Ava DuVernay on ‘Selma,’ the Racist Sony Emails, and Making Golden Globes History |Marlow Stern |December 15, 2014 |DAILY BEAST 

Kansas, Nebraska, Wyoming, Idaho, and Utah will never come anywhere close to being purple. Dems, It’s Time to Dump Dixie |Michael Tomasky |December 8, 2014 |DAILY BEAST 

Here and there, sparingly, one of the dolls might be purple or green: “Rainbow Piets,” they call them. Dutch Try to Save Santa’s Slave |Nadette De Visser |December 2, 2014 |DAILY BEAST 

Worse, when Richman woke up the next morning, her entire ear was purple. ‘My Crazy Love’ Reveals the Craziest Lies People Tell for Love |Kevin Fallon |November 18, 2014 |DAILY BEAST 

A small book, bound in full purple calf, lay half hidden in a nest of fine tissue paper on the dressing-table. Hilda Lessways |Arnold Bennett 

Its pages are filled with the purple gowns of kings and the scarlet trappings of the warrior. The Unsolved Riddle of Social Justice |Stephen Leacock 

She was in a soiled dressing gown of purple flannel, with several of the buttons off. Rosemary in Search of a Father |C. N. Williamson 

Wright's stain gives the nucleus a deep purple color and the cytoplasm a pale robin's-egg blue in typical cells. A Manual of Clinical Diagnosis |James Campbell Todd 

Malarial parasites stain characteristically: the cytoplasm, sky-blue; the chromatin, reddish-purple. A Manual of Clinical Diagnosis |James Campbell Todd