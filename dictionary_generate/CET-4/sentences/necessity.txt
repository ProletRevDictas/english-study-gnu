When I talked to lawyers about how to delay filing a Form D, all gave the standard perfunctory response about the necessity of filing — until we went off-the-record. PSA: Most aggregate VC trend data is garbage |Danny Crichton |February 9, 2021 |TechCrunch 

The best Valentine’s Day gifts to give and receive are those given based upon both feeling and necessity. The best, most practical Valentine’s Day gifts for any kind of partner |PopSci Commerce Team |February 8, 2021 |Popular-Science 

Economists see such savings as a sign the family did not need the money for basic necessities. Here’s the new Democratic plan for $1,400 stimulus checks |Heather Long, Jeff Stein |February 5, 2021 |Washington Post 

We believe a far greater number of people can’t afford internet, but are sacrificing other necessities. Getting vaccinated is hard. It’s even harder without the internet. |Eileen Guo |February 3, 2021 |MIT Technology Review 

Every play during a pandemic is, by necessity, experimental. A careful return to live theater is coming down to costume design |Anne Quito |February 2, 2021 |Quartz 

I had to create plays out of necessity, because African women deserved a voice and a place on the stage. Walking Dead’s Danai Gurira Vs. Boko Haram |Kristi York Wooten |November 30, 2014 |DAILY BEAST 

All of these changes to college financing occurred at exactly the time when college education became a necessity. The Student Loan Crisis That Isn’t About Kids at Harvard |Monica Potts |November 30, 2014 |DAILY BEAST 

Nazi texts proclaimed that the annihilation or expulsion of the Armenians was a “compelling necessity.” The 20th-Century Dictator Most Idolized by Hitler |William O’Connor |November 24, 2014 |DAILY BEAST 

The heartbreaking death of MTV reality TV personality Diem Brown proves the emotional necessity and value of the reality TV genre. MTV’s Diem Brown Dies: When Reality TV Starts Getting Real |Kevin Fallon |November 14, 2014 |DAILY BEAST 

After all, the Prince of necessity had to focus on defeating his external enemies. Valerie Jarrett, Obama Consigliere—and Democracy Killer |James Poulos |November 12, 2014 |DAILY BEAST 

Sometimes necessity makes an honest man a knave: and a rich man a honest man, because he has no occasion to be a knave. The Life and Most Surprising Adventures of Robinson Crusoe, of York, Mariner (1801) |Daniel Defoe 

There was a tremendous necessity for an example of the resurrection of an ordinary man. Solomon and Solomonic Literature |Moncure Daniel Conway 

Ha—assure you we quite understand; no necessity to say another word about it. Punch, or the London Charivari, Volume 107, November 3, 1894 |Various 

The proudest man in Christendom has found no friend in his extremest necessity, but you his bitterest enemy! The Pastor's Fire-side Vol. 3 of 4 |Jane Porter 

Quiet and good natured, when necessity arose he never failed to assert his authority. Fifty Years of Railway Life in England, Scotland and Ireland |Joseph Tatlow