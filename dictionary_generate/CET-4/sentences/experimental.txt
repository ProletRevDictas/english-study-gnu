Einstein was doubted for two decades, and cosmologists are still searching for experimental proofs of relativity. How Pseudoscientists Get Away With It - Facts So Romantic |Stuart Firestein |August 28, 2020 |Nautilus 

Instead, Utah and Washington both had different counties adopt the system at different times, providing a shifting set of experimental and control conditions. Vote by mail doesn’t really change much, election-wise |John Timmer |August 27, 2020 |Ars Technica 

The team searched for experimental issues that could explain the result, but came up empty. A measurement of positronium’s energy levels confounds scientists |Emily Conover |August 24, 2020 |Science News 

The country has offered experimental vaccines to officials at state enterprises in China. Russia says it has a covid vaccine called “Sputnik-V” |Antonio Regalado |August 11, 2020 |MIT Technology Review 

If evolution was a scientist, then centenarians, and the rest of us, are two experimental groups in action. The Secret to a Long, Healthy Life Is in the Genes of the Oldest Humans Alive |Shelly Fan |August 10, 2020 |Singularity Hub 

The course Jackson taught at VMI, “Natural and Experimental Philosophy,” was brutally difficult. Stonewall Jackson, VMI’s Most Embattled Professor |S. C. Gwynne |November 29, 2014 |DAILY BEAST 

I made experimental commercials in the experimental division of a production house, Film X, that made commercials for ad agencies. The Renegade: Robert Downey Sr. on His Classic Films, Son’s Battle with Drugs, and Bill Cosby |Marlow Stern |November 26, 2014 |DAILY BEAST 

All the Virgin Galactic test flying was done under a special experimental permit issued by the Federal Aviation Administration. Virgin Galactic’s Flight Path to Disaster: A Clash of High Risk and Hyperbole |Clive Irving |November 1, 2014 |DAILY BEAST 

“Rather than the finalized models, we display the experimental work-in-progress ones,” Migarou says. Frank Gehry Is Architecture’s Mad Genius |Sarah Moroz |October 27, 2014 |DAILY BEAST 

But owing to another experimental vaccine he received, its impossible to say whether the blood is what saved him. Blood Is Ebola’s Weapon and Weakness |Abby Haglage |October 26, 2014 |DAILY BEAST 

Several Americans, we are informed, have gallantly offered themselves for experimental purposes. Punch, or the London Charivari, Volume 158, April 28, 1920 |Various 

Neither I nor my companion would have had the courage to have gone in her on her experimental trip. A Woman's Journey Round the World |Ida Pfeiffer 

The work of this department is experimental and investigative, with a view to the improvement of agriculture in all its branches. The Philippine Islands |John Foreman 

He was the advocate of experimental rather than theoretical systems of medicine, and early adopted the practice of inoculation. The Every Day Book of History and Chronology |Joel Munsell 

He appeared as cool and steady as if he were commanding on an experimental test instead of making his first rescue in the air. Astounding Stories, May, 1931 |Various