Instead, they should hit you with a raw burst of salt, enough to burn your mouth like sour candy. Sunflower Seeds Are the Best Snack for the Anxious Mind |Emma Alpern |September 17, 2020 |Eater 

Consumers will need sufficient information to decide which vaccine to receive, and they should get that information from their physicians, not pharmaceutical ads on television or word of mouth. 6 questions that must be answered in the race for a vaccine |jakemeth |September 15, 2020 |Fortune 

Intubation is the process of inserting a tube through a patient’s mouth into their airway. How the Best Workplaces in Manufacturing have risen to the COVID-19 challenge |lbelanger225 |September 10, 2020 |Fortune 

I think you’ll see some players using a neck gaiter out on the field that they pull up over nose and mouth. Why Can’t Schools Get What the N.F.L. Has? (Ep. 431) |Stephen J. Dubner |September 10, 2020 |Freakonomics 

Asking loyal customers to write or record reviews about your company is the best way to grow your company through word of mouth. Networking 101: Why Working Together Creates More Opportunity Than Working Apart |Shantel Holder |September 4, 2020 |Essence.com 

And of course, Rod, being Rod, goes for it a hundred percent; his mouth drops open and he says, ‘What?’ The Story Behind Lee Marvin’s Liberty Valance Smile |Robert Ward |January 3, 2015 |DAILY BEAST 

But news of the classes is spread mainly by word of mouth, and participants bring along their friends and families. Iran’s Becoming a Footloose Nation as Dance Lessons Spread |IranWire |January 2, 2015 |DAILY BEAST 

She has had clients from all over the world, including Ireland and India, who are drawn to her via word of mouth and her website. Inside A Finishing School for Transwomen |Sharon Adarlo |December 27, 2014 |DAILY BEAST 

The “new civility” promoted by Antoine Courtin expected the mouth to be kept shut when smiling. The French Court’s Royal Ban on Smiles |William O’Connor |December 14, 2014 |DAILY BEAST 

During one session, detainee Abu Zubaydah became “completely unresponsive with bubbles rising through his open full mouth.” The Most Gruesome Moments in the CIA ‘Torture Report’ |Shane Harris, Tim Mak |December 9, 2014 |DAILY BEAST 

Aristide washed and powdered Jean himself, the landlord lounging by, pipe in mouth, administering suggestions. The Joyous Adventures of Aristide Pujol |William J. Locke 

Sol got up, slowly; took a backward step into the yard; filled his lungs, opened his mouth, made his eyes round. The Bondboy |George W. (George Washington) Ogden 

But such a thing had, nevertheless, come quite glibly out of her mouth, and she knew not why. Hilda Lessways |Arnold Bennett 

Miss Smith immediately rises from the table, puts up her dear little mouth to her papa to be kissed. Physiology of The Opera |John H. Swaby (AKA "Scrici") 

The word of the law shall be fulfilled without a lie, and wisdom shall be made plain in the mouth of the faithful. The Bible, Douay-Rheims Version |Various