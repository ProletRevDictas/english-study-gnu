I want them to do well for their sake, their communities’ sake and for my community’s sake. Like GameStop, Bed Bath & Beyond has been reduced to a meme stock. But 42,000 people work there. |Allan Sloan |February 8, 2021 |Washington Post 

For the sake of hand hygiene, the entire event will be cash-free and hand sanitizer will be stationed throughout the stadium. Even at half capacity, the Super Bowl could cause COVID-19 outbreaks |Kate Baggaley |February 5, 2021 |Popular-Science 

The biggest mistake entrepreneurs make is they write product descriptions for the sake of describing their product features only. Eight simple steps to write epic product descriptions that boost conversions |Ricky Hayes |January 29, 2021 |Search Engine Watch 

If not, we won’t rush them back just for the sake of saying we played. The Howard Bison’s hopes for a dream basketball season have turned into a nightmare |Tramel Raggs |January 27, 2021 |Washington Post 

Our public discourse is ruled by a handful of people for the sake of their profits. 'We Need a Fundamental Reset.' Shoshana Zuboff on Building an Internet That Lets Democracy Flourish |Billy Perrigo |January 22, 2021 |Time 

So, he decided to give the church a chance, if not just for the sake of mending his relationship with his mother. Beaten By His Church for Being Gay |Justin Jones |December 16, 2014 |DAILY BEAST 

He gave his soul for the sake of the people of Israel, The Torah, and the Land. Inside Hebron, Israel’s Heart of Darkness |Michael Tomasky |November 21, 2014 |DAILY BEAST 

But now it is time for them to put their interests in the forefront for the sake of the nation. What Brazil’s Dilma Rousseff Can Teach Hillary Clinton |Heather Arnet |October 29, 2014 |DAILY BEAST 

Locals were upset by the change—they like their traditions, even if it is just for the sake of being Sark. The Crazy Medieval Island of Sark |Liza Foreman |October 4, 2014 |DAILY BEAST 

Again and again, the band sacrifices the simple joy of a pop hook for the sake of a dense, meditative ambiance. U2 Generously Gives Us a Lousy Album, Sucks at the Corporate Teat |Hampton Stevens |September 13, 2014 |DAILY BEAST 

There is a companion who condoleth with his friend for his belly's sake, and he will take up a shield against the enemy. The Bible, Douay-Rheims Version |Various 

These Eskimos were very fond of kite-flying, for its own sake, without reference to utility! The Giant of the North |R.M. Ballantyne 

For God's sake write and persuade him to go to Davos at once—and picture the delights of a pretty and devoted nurse. Ancestors |Gertrude Atherton 

I supply look, for the sake of sense and metre; read—But good swet' hert-, look that ye. Chaucer's Works, Volume 1 (of 7) -- Romaunt of the Rose; Minor Poems |Geoffrey Chaucer 

Once on the ground, I began to think we were in no immediate danger of getting our throats cut for the sake of the treasure. Raw Gold |Bertrand W. Sinclair