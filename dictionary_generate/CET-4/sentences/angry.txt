Fishers like them because the worms wriggle and thrash like angry snakes, which lures fish, says Henshue. Invasive jumping worms damage U.S. soil and threaten forests |Megan Sever |September 29, 2020 |Science News 

A recent Facebook post garnered more than 1,500 angry comments supporting him. Veteran, War Hero, Defendant, Troll |by Jeremy Schwartz and Perla Trevizo |September 29, 2020 |ProPublica 

I don’t need angry lab directors emailing me about broken centrifuges. Can You Save Some Cold Pizza? |Zach Wissner-Gross |September 25, 2020 |FiveThirtyEight 

When presented with images on a screen, we perceive angry faces as lasting longer than neutral ones, spiders as lasting longer than butterflies, and the color red as lasting longer than blue. Reasons Revealed for the Brain’s Elastic Sense of Time |Jordana Cepelewicz |September 24, 2020 |Quanta Magazine 

While there’s no suggestion in court papers that Wenig knew of the plot, prosecutors say his angry emails and texts triggered the scheme, and they play a central role in the narrative spelled out by the government. Four ex-eBay employees to admit guilt in cyberstalking plot |Verne Kopytoff |September 23, 2020 |Fortune 

To borrow an old right-wing talking point, these people are angry no matter what we do. Harry Shearer on The Dangerous Business of Satire |Lloyd Grove |January 8, 2015 |DAILY BEAST 

Desert Golfing is the distillation of Angry Birds into its purest essence. Lost For Thousands of Strokes: 'Desert Golfing' Is 'Angry Birds' as Modern Art |Alec Kubas-Meyer |January 2, 2015 |DAILY BEAST 

But since that explosion of popularity, Angry Birds has become about everything else. Lost For Thousands of Strokes: 'Desert Golfing' Is 'Angry Birds' as Modern Art |Alec Kubas-Meyer |January 2, 2015 |DAILY BEAST 

And in this way, it follows not what Angry Birds became, but how it began. Lost For Thousands of Strokes: 'Desert Golfing' Is 'Angry Birds' as Modern Art |Alec Kubas-Meyer |January 2, 2015 |DAILY BEAST 

Angry Birds at its simplest was the same way, though you wanted to watch things collapse and explode. Lost For Thousands of Strokes: 'Desert Golfing' Is 'Angry Birds' as Modern Art |Alec Kubas-Meyer |January 2, 2015 |DAILY BEAST 

I am an easiful old pagan, and I am not angry with you at all—you funny, little champion of the Most High. God and my Neighbour |Robert Blatchford 

Isabel lifted her head still higher, annoyed at the angry blood that leaped to her face. Ancestors |Gertrude Atherton 

He was given no reply save a muttered curse, a command to hold his tongue, and an angry tug at his tied arms. The Red Year |Louis Tracy 

Smoking, the angry and fuming king protests, had made our manners as rude as those of the fish-wives of Dieppe. Tobacco; Its History, Varieties, Culture, Manufacture and Commerce |E. R. Billings. 

When the man turned bad on his hands, Jahweh was angry, and cursed him and his seed for thousands of years. God and my Neighbour |Robert Blatchford