This year, electric vehicle startups have gone public in droves on those terms, with multibillion-dollar valuations doled out to innovators like QuantumScape, a battery developer, and Hyliion, which makes electrified powertrains for freight trucks. The electric car frenzy is helping even troubled companies raise big money |dzanemorris |October 23, 2020 |Fortune 

Diesel engines are the workhorses of freight transportation and agriculture — and by extension keep the economy fed and well supplied. ClearFlame Engine Technologies takes aim at cleaning up diesel engines |Kirsten Korosec |September 17, 2020 |TechCrunch 

Accordingly, Horowitz encouraged Tristan to abandon a few of his earliest ideas—one to revolutionize freight, another tackling childhood obesity with play—and instead to pursue this thing he was uniquely experienced to execute. ‘How I Built This’ host Guy Raz on insights from some of the world’s most famous entrepreneurs |Rachel King |September 15, 2020 |Fortune 

Rose, who is Reliable’s chief executive officer, says the company’s business plan is to equip a fleet of Cessna 208 Caravans with its self-flying systems and use them for air freight deliveries. Cessna makes history by taking off and landing with no one aboard. Here’s how |Jeremy Kahn |August 26, 2020 |Fortune 

A similar subscription model is planned by Nikola, but Nikola is focused largely on commercial electric freight trucks and is expected to require more commitment from customers. Electric-vehicle startup Canoo to go public, joining the wave of companies chasing Tesla’s success |dzanemorris |August 18, 2020 |Fortune 

Up in the tower, Bucca was joined by Battalion Chief Orio Palmer, who had managed to get a freight elevator to bring him part way. The Flying New York Fireman Who Shined on 9/11 |Michael Daly |September 11, 2014 |DAILY BEAST 

Toledo is a tough city, a factory town, a freight train junction, a lake steamer port. Toledo: The Town Too Tough for Toxic Water |P. J. O’Rourke |August 4, 2014 |DAILY BEAST 

But a year later he was back home in New Orleans and back on the docks, handling freight on ships bound for the Gulf of Mexico. The Stacks: The Neville Brothers Stake Their Claim as Bards of the Bayou |John Ed Bradley |April 27, 2014 |DAILY BEAST 

Bette would “have no choice” but to pay the extra, socialistic freight. Health Care's Resistors and Adapters |Michael Tomasky |March 11, 2014 |DAILY BEAST 

“She seemed to indicate she had worked at Stapleton Air Freight,” he said. Lost and Found in Tijuana: Behind an Amazing American Rescue |Christine Pelisek |September 26, 2013 |DAILY BEAST 

We made the freight camp, however, just as the storm cut loose in deadly earnest. Raw Gold |Bertrand W. Sinclair 

You know that if there was anything they wanted they weren't taking any risk by going to any freight camp. Raw Gold |Bertrand W. Sinclair 

Faintly outlined among the trees, Jess saw an old freight or box car. The Box-Car Children |Gertrude Chandler Warner 

By sheer good fortune a big tree stump stood under the door of the freight car, or the children never could have opened it. The Box-Car Children |Gertrude Chandler Warner 

They could see nothing at all, for the freight car was tightly made, and all outside was nearly as black as night. The Box-Car Children |Gertrude Chandler Warner