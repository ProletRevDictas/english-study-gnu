Regenerative agriculture dates back at least to biblical times, when the ancients practiced rudimentary crop rotation by letting their fields lie fallow in order to keep the soil rich with nutrients. Allbirds is stepping up for the planet—by treading lightly on it |sheilamarikar |September 21, 2020 |Fortune 

George Poinar, an entomologist and paleontologist at Oregon State University in Corvallis who described the ancient cricket, stands by his conclusions. A tiny crustacean fossil contains roughly 100-million-year-old giant sperm |Curtis Segarra |September 21, 2020 |Science News 

The evidence of ancient cricket sperm is less clear, he says. A tiny crustacean fossil contains roughly 100-million-year-old giant sperm |Curtis Segarra |September 21, 2020 |Science News 

Microraptor, which lived some 120 m­illion years ago alongside ancient birds, is distantly related to Aves. Readers ask about Microraptors, an X-ray map of the sky and more |Science News Staff |September 20, 2020 |Science News 

Those developing embryos were reverting into ants that looked more like their ancient endosymbiont-free ancestors. How Two Became One: Origins of a Mysterious Symbiosis Found |Viviane Callier |September 9, 2020 |Quanta Magazine 

But the last national figure to wield ancient personal authority in an explicitly religious way was Robert F. Kennedy. No Gods, No Cops, No Masters |James Poulos |January 1, 2015 |DAILY BEAST 

Historical justifications for most modern celebrations can be found in the ancient world. New Year’s Eve, Babylon Style |Candida Moss |December 31, 2014 |DAILY BEAST 

Ancient Romans exchanged gifts of figs and honey and would make sure to work part of the day as a good omen for the coming year. New Year’s Eve, Babylon Style |Candida Moss |December 31, 2014 |DAILY BEAST 

The ancient Egyptian festival of Wepet Renpet (“opening of the year”) was not just a time of rebirth—it was dedicated to drinking. New Year’s Eve, Babylon Style |Candida Moss |December 31, 2014 |DAILY BEAST 

It can be hard to wrap your head around the problems facing the continent because they might seem ancient to us. Silicon Valley Sets Its Sights on Africa |Christian Borys |December 22, 2014 |DAILY BEAST 

I believe that these are ideal characters constructed from still more ancient legends and traditions. God and my Neighbour |Robert Blatchford 

Elyon is the name of an ancient Phœnician god, slain by his son El, no doubt the “first-born of death” in Job xviii. Solomon and Solomonic Literature |Moncure Daniel Conway 

Something remote and ancient stirred in her, something that was not of herself To-day, something half primitive, half barbaric. The Wave |Algernon Blackwood 

When Cortez made conquest of Mexico in 1519 smoking seemed to be a common as well as an ancient custom among the natives. Tobacco; Its History, Varieties, Culture, Manufacture and Commerce |E. R. Billings. 

Long before that, however, the sun had come back to gladden the Polar regions, and break up the reign of ancient night. The Giant of the North |R.M. Ballantyne