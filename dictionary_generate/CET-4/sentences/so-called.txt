And yes, someone has already called Spencer a “Small Fry,” har har. Freaking Out About Age Gaps in Gay Relationships Is Homophobic |Samantha Allen |January 9, 2015 |DAILY BEAST 

But so-called jungle primaries are notoriously hard to predict or poll. The Golden State Preps for the ‘Red Wedding’ of Senate Races |David Freedlander |January 9, 2015 |DAILY BEAST 

The governor of Punjab province, a Muslim man, called publicly for leniency for her. In Defense of Blasphemy |Michael Tomasky |January 9, 2015 |DAILY BEAST 

Grindr introduced the feature themselves in October the same year and called it ‘tribes.’ Grindr’s Trans Dating Problem |David Levesley |January 9, 2015 |DAILY BEAST 

We just saw an edit of one called, “Doug Becomes A Feminist,” and I just really enjoyed watching it. Coffee Talk with Fred Armisen: On ‘Portlandia,’ Meeting Obama, and Taylor Swift’s Greatness |Marlow Stern |January 7, 2015 |DAILY BEAST 

That which is called nasality is caused by the failure of the tone to reach freely the anterior cavities of the nares. Expressive Voice Culture |Jessie Eldridge Southwick 

I called out several times, as loud as I could raise my voice, but all to no purpose. Gulliver's Travels |Jonathan Swift 

Many so-called "humming tones" are given for practice, but in accepting them observe whether the foregoing principle is obeyed. Expressive Voice Culture |Jessie Eldridge Southwick 

I called it a spinet, because it somewhat resembled that instrument, and was played upon in the same manner. Gulliver's Travels |Jonathan Swift 

He wanted to tell her that if she called her father, it would mean the end of everything for them, but he withheld this. The Homesteader |Oscar Micheaux