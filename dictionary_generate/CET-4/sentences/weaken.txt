It has tightened internet censorship and developed a “social credit” system, which punishes behaviors the authorities say weaken social stability. Inside China’s unexpected quest to protect data privacy |Tate Ryan-Mosley |August 19, 2020 |MIT Technology Review 

Two days after that, they measured whether the drug cleared the weakened viruses that make up the vaccine. New antibodies in just months |Katie McLean |August 19, 2020 |MIT Technology Review 

The absence of this immune signaling protein causes a severe susceptibility to all forms of mycobacteria, not only the highly virulent strains that cause tuberculosis but even weakened strains like the one in the BCG vaccine. Our Genes May Explain Severity of COVID-19 and Other Infections |Monique Brouillette |July 27, 2020 |Quanta Magazine 

That decision itself turned on whether the city measure sufficiently weakened or reversed the old measure. Politics Report: City’s Big Recycled Water Project Wins in Court |Andrew Keatts |July 25, 2020 |Voice of San Diego 

This situation could weaken recovery efforts over the long term. Crisis looms for 50 million parents as US states decide how to handle the school year |Karen Ho |July 15, 2020 |Quartz 

Democrats split over moves to weaken Wall Street reforms, and Republicans pouted over lost leverage. ‘Cromnibus’ Passes, But Did Anyone Win? |Ben Jacobs |December 12, 2014 |DAILY BEAST 

But his voice never seems to crack or weaken, and he's always in motion—jiggling, aerobic walking, jumping, dancing. The Stacks: Pauline Kael's Talking Heads Obsession |Pauline Kael |November 22, 2014 |DAILY BEAST 

But the attacks could also weaken the most potent opponents to the dictator Bashar al-Assad. Al Qaeda Plotters in Syria ‘Went Dark,’ U.S. Spies Say |Eli Lake |September 24, 2014 |DAILY BEAST 

Assassinations of community leaders both weaken local coordination against ISIS and deter potential informants. Who the U.S. Should Really Hit in ISIS |Daniel Trombly, Yasir Abbas |September 23, 2014 |DAILY BEAST 

Assad has been accused of knowingly leaving room for ISIS to grow, the better to weaken the less radical rebels. Beirut Letter: In Lebanon, Fighting ISIS With Culture and Satire |Kim Ghattas |September 22, 2014 |DAILY BEAST 

Cornstalk was too wise a warrior to weaken his forces for a score of scalps when a general engagement was pending. A Virginia Scout |Hugh Pendexter 

Wait,” said the latter one evening, “and let him develop his attack; we should only weaken ourselves by going out to meet him. The Bag of Diamonds |George Manville Fenn 

As the sun dropped out of the sky, the bull felt his knees begin to weaken. Stories the Iroquois Tell Their Children |Mabel Powers 

They "supped full of horrors," and listened greedily to tales of death, which served to weaken and terrify. The British Expedition to the Crimea |William Howard Russell 

The softness of the implication she swept aside, as if she hardly dared regard it lest it weaken her resolve. Country Neighbors |Alice Brown