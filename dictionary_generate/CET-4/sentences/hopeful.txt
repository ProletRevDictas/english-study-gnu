She’s hopeful the project will help us better understand why we don’t talk about sex and sexual health publicly—and, ideally, start to walk back puritanical norms about bodies and appropriateness. A New Podcast Explores Sex in the Great Outdoors |Heather Hansman |February 12, 2021 |Outside Online 

I believe that we will see a time — I’m hopeful in my daughter’s lifetime and I pray that I’m around to see it — that women are protected in the Constitution. Actress, Mother, Activist Alyssa Milano on Life as a Triple Threat |Esabelle Lee |February 11, 2021 |Ozy 

Asked to share their approach to an uncertain future, four media executives said they remain hopeful for a partial return this year, but that policies will emphasize patience, caution and flexibility to employees’ varying needs and comfort levels. Publishers look to allay employee fears over a return to the office by offering additional flexibility |Jessica Davies |February 10, 2021 |Digiday 

The engineer working on Google Search Console was great, and heard me out, but I didn’t leave that conversation feeling we would see an API any time soon… although I was hopeful we might see one in the future. Quest for more coverage: Making a case for larger exports from Google Search Console’s Coverage reporting |Glenn Gabe |February 9, 2021 |Search Engine Land 

We’re hopeful Johnson & Johnson will recognize they are making this vaccine here in Baltimore and it would be a shame if Baltimoreans didn’t directly benefit from it. Officials in D.C. region try to combat vaccine hesitancy while seeking more doses |Jenna Portnoy, Rebecca Tan, Michael Brice-Saddler |February 8, 2021 |Washington Post 

Is there any chance the potential 2016 hopeful will stand up to the right and embrace paid sick leave? Christie Blames Parents for Bad Economy |Monica Potts |January 3, 2015 |DAILY BEAST 

He remained as hopeful as ever that he would himself join the NYPD, whatever the danger. In The Shadow of Murdered Cops |Michael Daly |December 26, 2014 |DAILY BEAST 

Foxx is hopeful that young people are evolving past the point of being preoccupied with race. Jamie Foxx: Get Over the Black ‘Annie’ |Stereo Williams |December 20, 2014 |DAILY BEAST 

But assuming things were ever that hopeful, heaven was short-lived, and trouble followed. The Sydney Astrologer Turned Islamic Radical |Jacob Siegel |December 16, 2014 |DAILY BEAST 

In our screenings, he always sits in the same corner chair and always looks hopeful, no matter what the movie. Alfred Hitchcock’s Fade to Black: The Great Director’s Final Days |David Freeman |December 13, 2014 |DAILY BEAST 

The conclusion is reached that, despite these drawbacks, the Jesuit mission in Canada has made a hopeful beginning. The Jesuit Relations and Allied Documents, Vol. II: Acadia, 1612-1614 |Various 

“A hopeful family yours, Mr. Trotter,” said Perker, sealing a letter which he had just finished writing. The Posthumous Papers of the Pickwick Club, v. 2(of 2) |Charles Dickens 

The feeling is hopeful if only we had more men and especially drafts to fill up our weakened battalions. Gallipoli Diary, Volume I |Ian Hamilton 

She reminded him as she stood there then, of a serious young literary woman, and he was made hopeful by her visit. The Homesteader |Oscar Micheaux 

He had a hopeful, sunny nature, and never looked upon the dark side of things if he could help it. Dorothy at Skyrie |Evelyn Raymond