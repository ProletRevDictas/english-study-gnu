It should serve as a reminder of those parents still experiencing that agony. For a mother forced to give up her child, decades of grief, shame and secrets |Ellen McCarthy |February 5, 2021 |Washington Post 

If your muscles feel so tight that the gentlest prod causes agony, a heated pad can be a great ally. A step-by-step guide to giving yourself a massage |Sandra Gutierrez G. |January 14, 2021 |Popular-Science 

Through Nadir’s journey, Joukhadar gives us a raw and powerful portrayal of his character’s agony and loneliness as he deals with living in a body that make him look female. Zeyn Joukhadar’s ‘The Thirty Names of Night’ is a poetic portrait of a trans man’s search for a rare bird — and his own identity |Carol Memmott |November 23, 2020 |Washington Post 

I once imagined the agony of illness being the greatest challenge I’d face as a doctor. Thank You for the 7 PM Clapping, But Camaraderie Is Needed More Than Ever - Facts So Romantic |Ayala Danzig |October 5, 2020 |Nautilus 

Twice, Raymond Jefferson’s service to his country left him in pain, agony and facing years of recovery. He was forced to resign after a government report criticized him. Eight years later, the government took it back. |Joe Davidson |September 24, 2020 |Washington Post 

Truth be told, there is no one better at capturing the agony and alarm of a woman in the throes of a nervous breakdown than Moore. Julianne Moore Is Oscar Gold in ‘Still Alice’ |Marlow Stern |December 24, 2014 |DAILY BEAST 

The agony of being so close to our goal but failing gnaws at our insides while we replay the events over and over in our heads. Heart of Darkness: Into Afghanistan’s Taliban Valley |Matt Trevithick, Daniel Seckman |November 15, 2014 |DAILY BEAST 

Animals in agony or danger are used by Martin Wittfooth, often to hint at the future of the human condition. Trading Dime Bags for Salvador Dali |Jason McGahan |October 19, 2014 |DAILY BEAST 

Murray lost 6-1 7-6, 6-2 as Kate and William grimaced and groaned in agony with the rest of the nation. Murray Crashes Out Of Wimbledon As Kate And William Watch |Tom Sykes |July 2, 2014 |DAILY BEAST 

But if he positioned himself a certain way, he found he could avoid excruciating agony. The Crash and Churn of Lawrence O’Donnell |Lloyd Grove |June 20, 2014 |DAILY BEAST 

In the year of misery, of agony and suffering in general he had endured, he had settled upon one theory. The Homesteader |Oscar Micheaux 

Your sacrifice shall be the agony of agonies, the death of deaths, and yet you'll find yourself unable to resist. Checkmate |Joseph Sheridan Le Fanu 

Each sentence came as if torn piecemeal from his unwilling tongue; short, jerky phrases, conceived in pain and delivered in agony. Raw Gold |Bertrand W. Sinclair 

Having reduced Punch to a second agony of tears Harry departed upstairs with the news that Punch was still rebellious. Kipling Stories and Poems Every Child Should Know, Book II |Rudyard Kipling 

How little did she realize the long drawn-out agony that was even then beginning for her sisters in that ill-fated entrenchment! The Red Year |Louis Tracy