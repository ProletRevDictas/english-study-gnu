I share McDonald’s CEO Chris Kempczinski’s belief that the company’s franchisee ranks should more closely reflect the diverse composition of America and the world. Why McDonald’s sets the standard for equitable business models |jakemeth |September 16, 2020 |Fortune 

Different wine growing regions had distinct microbial communities in both the soil and the must, which appeared to influence the unique compositions of metabolites in the finished wine. How does a crop’s environment shape a food’s smell and taste? |Carolyn Beans |September 10, 2020 |Science News 

Physicist Inho Hong from the Center for Humans & Machines at the Max Planck Institute for Human Development in Germany looked at the industry composition of 350 US metro areas between 1998 and 2013. What it takes for a city to jump into the knowledge economy |Karen Ho |September 9, 2020 |Quartz 

Treatments that modulate the composition and functionality of the gut microbiome should be explored, according to Ng. COVID-19 infection lingers in the gut, even after it clears the respiratory system, researchers say |Claire Zillman, reporter |September 8, 2020 |Fortune 

We’ve changed the composition of the team, taken down the number of traditional sales roles by 25% but added more programmatic specialists, content creation specialists. ‘It’s less dire than it seemed to be’: How The Wall Street Journal’s digital ads business has weathered the downturn |Lucinda Southern |August 20, 2020 |Digiday 

John Luther Adams lives up to the title of his composition, capturing an oceanic torrent of sound in an awe-inspiring performance. The Best Albums of 2014 |Ted Gioia |December 13, 2014 |DAILY BEAST 

Flaubert, for instance, hated the works of Dickens: “What defective composition!” The Birth of the Novel |Nick Romeo |November 27, 2014 |DAILY BEAST 

He described in painful detail the composition of the bars and the heavy shackles on the pad locks. I Shot Bin Laden |Elliot Ackerman |November 16, 2014 |DAILY BEAST 

Plus, did you know that meditation can change your brain composition — just like exercise can change your body? Meditation Tips for the Easily Distracted |DailyBurn |October 30, 2014 |DAILY BEAST 

He approaches the composition of a painting rather as a theatrical director might set the scene of a play. The Stacks: Edward Hopper’s X-Ray Vision |Hilton Kramer |October 25, 2014 |DAILY BEAST 

He was the most distinguished representative of the English school of composition, and was knighted in 1842. The Every Day Book of History and Chronology |Joel Munsell 

When he plays a sonata it is as if the composition rose from the dead and stood transfigured before you. Music-Study in Germany |Amy Fay 

Of writing he knew little and the art of composition appeared very difficult. The Homesteader |Oscar Micheaux 

The countess-dowager was not very adroit at spelling and composition, whether French or English, as you observe. Elster's Folly |Mrs. Henry Wood 

Tausig, in my opinion, did possess exceptional genius in composition, though he left but few works behind him to attest it. Music-Study in Germany |Amy Fay