Then add in all bored people, as well as people whose job it is to report on celebrities. Sia and Shia LaBeouf’s Pedophilia Nontroversy Over ‘Elastic Heart’ |Marlow Stern |January 9, 2015 |DAILY BEAST 

Seeing what they were doing, I was inspired to add my vision to their technique. The Photographer Who Gave Up Manhattan for Marrakech |Liza Foreman |January 6, 2015 |DAILY BEAST 

Think of it as Game of Thrones—if you subtract the sex and violence and add drunken revelry and singing. ‘Galavant’: A Drunken, Horny Musical Fairy Tale |Melissa Leon |January 5, 2015 |DAILY BEAST 

Her new comments will only add to ongoing speculation that the Yorks plan, one day, to remarry. Fergie Dives Into Prince Andrew’s Sex Scandal |Tom Sykes |January 5, 2015 |DAILY BEAST 

The economy has begun to add jobs, but the quality of those jobs is an increasing concern. Christie Blames Parents for Bad Economy |Monica Potts |January 3, 2015 |DAILY BEAST 

Add to this, if you please, the great difficulty of obtaining from them even the words that they have. The Jesuit Relations and Allied Documents, Vol. II: Acadia, 1612-1614 |Various 

To add point to this success, he knew that the victor of Montebello was straining every nerve to gain this very prize. Napoleon's Marshals |R. P. Dunn-Pattison 

It is painful to add, that the latter years of his life were passed in prison, where he was confined for debt. The Every Day Book of History and Chronology |Joel Munsell 

Must I add, that your good money paid this second loan—and yet a third—a fourth—a fifth? Blackwood's Edinburgh Magazine, No. CCCXXXIX. January, 1844. Vol. LV. |Various 

The Federal Reserve Board reserves the right to add to, alter, or amend these regulations. Readings in Money and Banking |Chester Arthur Phillips