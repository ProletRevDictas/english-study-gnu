Among the rise of green hills dotted with cows, at the farm and creamery of Louis Bononci, James had his first taste of Teleme, a washed-rind cheese with a subtly elastic texture and milky tang. To Find Hope in American Cooking, James Beard Looked to the West Coast |John Birdsall |October 2, 2020 |Eater 

The tools and threads are all held in place with elastic straps, so the supplies stay organized even if bounced around in a bag. Sewing kits perfect for home, travel, and gift-giving |PopSci Commerce Team |October 1, 2020 |Popular-Science 

The remolded electronic structure won’t stay in place of its own accord, just as a piece of elastic won’t stay stretched if you don’t keep pulling. Alchemy Arrives in a Burst of Light |Philip Ball |September 30, 2020 |Quanta Magazine 

It’s comfortable and it’s got a nice elastic spring that adheres nicely to movement. You can now return Apple’s Solo Loop for a new size, without sending back the Watch |Brian Heater |September 24, 2020 |TechCrunch 

The lid has an elastic opening that allows for quick access while hiding the garbage inside. Car trash cans to keep your auto spotless |PopSci Commerce Team |September 23, 2020 |Popular-Science 

The second was that the demand for seduction schooling was elastic. The Secret World of Pickup Artist Julien Blanc |Brandy Zadrozny |December 1, 2014 |DAILY BEAST 

He was wearing a black sweatshirt and light grey sweatpants with an elastic waist. Gripping His Koran, Anas al-Liby Has His Day in Court |Michael Daly |October 16, 2013 |DAILY BEAST 

Jenny Packham polka-dotted frock with a high-waisted elastic waistband and wedge heels. Kate Middleton Looked Goddam Fabulous Today |Isabel Wilkinson |July 23, 2013 |DAILY BEAST 

But I think powerful, long friendships often are elastic enough to incorporate envy into them, and not destroy the friendship. Endless Summer: Meg Wolitzer Talks About “The Interestings” |Jane Ciabattari |April 16, 2013 |DAILY BEAST 

Elastic bracelets—with brads to place just so in an acupressure spot on the inner wrist purported to reduce nausea—are popular. Hyperemesis Gravidarum: What’s Ailing Kate Middleton |Kent Sepkowitz |December 4, 2012 |DAILY BEAST 

Accordingly, the question "How far does the note issue under the new system seem likely to prove an elastic one?" Readings in Money and Banking |Chester Arthur Phillips 

The sputum of more advanced cases resembles that of chronic bronchitis, with the addition of tubercle bacilli and elastic fibers. A Manual of Clinical Diagnosis |James Campbell Todd 

Industrial society is therefore mobile, elastic, standing at any moment in a temporary and unstable equilibrium. The Unsolved Riddle of Social Justice |Stephen Leacock 

The non-elastic character of water made it unsuitable for a machine requiring a fly-wheel. Life of Richard Trevithick, Volume II (of 2) |Francis Trevithick 

I met him on Kingstown promenade the other day walking with an elastic step and with the brightness of youth in his eye. Fifty Years of Railway Life in England, Scotland and Ireland |Joseph Tatlow