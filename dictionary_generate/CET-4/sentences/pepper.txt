My introduction to jerk chicken — its skin soaked in the flavor of sweet smoke, of Scotch bonnet peppers, allspice berries, ginger, and green onion — was during my first year of college, across the Hudson river from a New York town called Kingston. Until I Can Go Back to My Favorite Restaurant, This Jerk Paste Is the Next Best Thing |Elazar Sontag |September 25, 2020 |Eater 

Others will also layer on a hint of ground chile or pepper to add a subtle kick. Turn a cheap chicken dinner into a Turkish street-food getaway |SAVEUR Editors |September 25, 2020 |Popular-Science 

The indictment accused him of using pepper spray on a handcuffed woman, filing a false report about the encounter and encouraging other officers to fake their reports, too. How Criminal Cops Often Avoid Jail |by Andrew Ford, Asbury Park Press |September 23, 2020 |ProPublica 

How law enforcement cleared protesters In the same June 2 statement, Park Police said officers used “smoke canisters and pepper balls” to clear protesters but “did not use tear gas or OC Skat Shells.” How the Trump administration has tried to explain clearing protesters from Lafayette Square |JM Rieger |September 17, 2020 |Washington Post 

Rivera calls adobo and sazón the salt and pepper of Puerto Rican cooking, and he wants his customers to use the products exactly like that. The Redemption of the Spice Blend |Jaya Saxena |September 10, 2020 |Eater 

Whisk in the half and half and season to taste with salt and pepper. Make Carla Hall’s Crispy Shallot Green Bean Casserole |Carla Hall |December 27, 2014 |DAILY BEAST 

Rub the loin with olive oil, and season with salt and pepper. Make Carla Hall’s Roasted Pork Loin With Cranberries |Carla Hall |December 24, 2014 |DAILY BEAST 

Sage and sausage patty came next, served between cumin scented Buttermilk biscuits and smothered in a black pepper country gravy. Dinner at Nitehawk Cinema: ‘Christmas Vacation’ and a Beer in a Pear Tree |Rich Goldstein |December 12, 2014 |DAILY BEAST 

Long gone are the days of the Young British Artists movement: now a salt-and-pepper-haired filmmaker is the artist of the moment. Has the Turner Prize Gone Soft? |David Levesley |December 2, 2014 |DAILY BEAST 

Brush the pastry with egg wash and sprinkle with fleur de sel and pepper. Make These Barefoot Contessa Chicken Pot Pies |Ina Garten |November 29, 2014 |DAILY BEAST 

One whist table only is at work; General Pepper and three old hands of the same kidney are hard at it. The Pit Town Coronet, Volume I (of 3) |Charles James Wills 

As, during the whole pepper-harvest, they feed wholly on this stimulant, they become exceedingly irritable. The Book of Anecdotes and Budget of Fun; |Various 

"I think at least one of us ought to stay here until Mr. Ford comes," said Pepper. The Mystery at Putnam Hall |Arthur M. Winfield 

"Let us go down the shore a bit," suggested Jack to Pepper and Andy, and the three joined hands for the spin. The Mystery at Putnam Hall |Arthur M. Winfield 

Jack and Pepper watched Ritter closely and then looked questioningly at each other. The Mystery at Putnam Hall |Arthur M. Winfield