In the intervening period, as we weighed the cost of our overall portfolio and strategic focus, we made the decision not to relaunch the service. Mozilla shutters Firefox Send and Notes |Frederic Lardinois |September 17, 2020 |TechCrunch 

Several startups, including MJ Platform and BioTrack, are building similar platforms for this market, but Canix says the company’s focus on improving data entry makes it stand apart. Canix aims to ease cannabis cultivators’ regulatory bookkeeping |Matt Burns |September 17, 2020 |TechCrunch 

West Virginia environmental regulators are proposing to reduce the fines that a coal company owned by the state’s governor could pay for water pollution violations that are the focus of a federal court case. This Billionaire Governor’s Coal Company Might Get a Big Break From His Own Regulators |by Ken Ward Jr. |September 17, 2020 |ProPublica 

Initially there may be a limited supply of vaccines available, and the focus will be on protecting health workers, other essential employees, and people in vulnerable groups. U.S. outlines sweeping plan to provide free COVID-19 vaccines |Rachel Schallom |September 16, 2020 |Fortune 

However, different aspects vary based on the agency’s focus. How would an SEO agency be built today? Part 2: Current business model(s) |Sponsored Content: SEOmonitor |September 16, 2020 |Search Engine Land 

Back in New York, the slow pace and inward focus of her yoga practice was less fulfilling. How Taryn Toomey’s ‘The Class’ Became New York’s Latest Fitness Craze |Lizzie Crocker |January 9, 2015 |DAILY BEAST 

Stephanie Giorgio, a classical musician, credits The Class for helping her cope with anxiety, focus, fear, and self-doubt. How Taryn Toomey’s ‘The Class’ Became New York’s Latest Fitness Craze |Lizzie Crocker |January 9, 2015 |DAILY BEAST 

There is a particular focus in the magazine on attacking the United States, which al Qaeda calls a top target. U.S. Spies See Al Qaeda Fingerprints on Paris Massacre |Shane Harris, Nancy A. Youssef |January 8, 2015 |DAILY BEAST 

And too much of a focus on numbers can obscure strategic truths. Pentagon Doesn’t Know How Many People It’s Killed in the ISIS War |Nancy A. Youssef |January 7, 2015 |DAILY BEAST 

His wife passed away and they had kids, and he wanted to focus on being a dad so he just stopped to raise his kids. Coffee Talk with Fred Armisen: On ‘Portlandia,’ Meeting Obama, and Taylor Swift’s Greatness |Marlow Stern |January 7, 2015 |DAILY BEAST 

Lessard's high-handed squelching of MacRae had thrown everything out of focus. Raw Gold |Bertrand W. Sinclair 

William Weedham brought scowling eyes to focus upon Kip Burland. Hooded Detective, Volume III No. 2, January, 1942 |Various 

It is doubtful if any woman had done as much to entice them to a common focus as the surmounting Mrs. Hofer. Ancestors |Gertrude Atherton 

Why the focus of the telescope should change during a long exposure is not quite clear. Photographs of Nebul and Clusters |James Edward Keeler 

Before beginning an exposure the focus is adjusted by means of a high-power positive eyepiece. Photographs of Nebul and Clusters |James Edward Keeler