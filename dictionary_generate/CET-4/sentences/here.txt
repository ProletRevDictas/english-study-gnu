The best comparison here for an American audience is, well, Internet stuff. Trolls and Martyrdom: Je Ne Suis Pas Charlie |Arthur Chu |January 9, 2015 |DAILY BEAST 

“Please, please do not permit this to happen here in Florida,” wrote Cris K. Smith of East Polk County. Jeb Bush’s Unseen Anti-Gay Marriage Emails |Jackie Kucinich |January 9, 2015 |DAILY BEAST 

Toomey lives here with her husband, Mark, a managing director at Goldman Sachs, and their two daughters. How Taryn Toomey’s ‘The Class’ Became New York’s Latest Fitness Craze |Lizzie Crocker |January 9, 2015 |DAILY BEAST 

So here I am in my requisite Lululemon pants, grunting along to an old hip-hop song at a most ungodly hour. How Taryn Toomey’s ‘The Class’ Became New York’s Latest Fitness Craze |Lizzie Crocker |January 9, 2015 |DAILY BEAST 

“I love my job and I love my city and I am committed to the work here,” he said in a statement. The Golden State Preps for the ‘Red Wedding’ of Senate Races |David Freedlander |January 9, 2015 |DAILY BEAST 

The Duchess had also a tent for their sick men; so that we had a small town of our own here, and every body employed. The Life and Most Surprising Adventures of Robinson Crusoe, of York, Mariner (1801) |Daniel Defoe 

Here began indeed, in the drab surroundings of the workshop, in the silent mystery of the laboratory, the magic of the new age. The Unsolved Riddle of Social Justice |Stephen Leacock 

The people here retained the same paganism and barbarity, only they were not so dangerous, being conquered by the Muscovites. The Life and Most Surprising Adventures of Robinson Crusoe, of York, Mariner (1801) |Daniel Defoe 

Genoa has but recently and partially felt the new impulse, yet even here the march of improvement is visible. Glances at Europe |Horace Greeley 

One of the simplest of these childish tricks is the invention of an excuse for not instantly obeying a command, as "Come here!" Children's Ways |James Sully