Other species would be able to think about and solve problems in ways that are outside our space of comprehension. Talking Is Throwing Fictional Worlds at One Another - Issue 89: The Dark Side |Kevin Berger |September 9, 2020 |Nautilus 

The TI-84 Plus CE makes comprehension and mastery of math and science topics quicker, easier, lighter, and brighter with color graphing and a slim design. The best graphing calculators for students |PopSci Commerce Team |September 4, 2020 |Popular-Science 

Even today, chiaroscuro remains a powerful tool of discovery and comprehension. These Images Expose the Dark Side of the Solar System - Issue 89: The Dark Side |Corey S. Powell |August 26, 2020 |Nautilus 

Below are some illustrations of its lack of comprehension—all, as we will see later, prefigured in an earlier critique that one of us wrote about GPT-3’s predecessor. GPT-3, Bloviator: OpenAI’s language generator has no idea what it’s talking about |Amy Nordrum |August 22, 2020 |MIT Technology Review 

It certainly won’t help identify life beyond our own solar system, which could be completely beyond human comprehension, like the sentient ocean of plasma that the writer Stanisław Lem imagined in his 1961 novel Solaris. What Is an Individual? Biology Seeks Clues in Information Theory. |Jordana Cepelewicz |July 16, 2020 |Quanta Magazine 

It was a small step in learning to stick to my guns, but a leap in my comprehension of phonetics. ‘Sesame Street’ Is Middle-Aged and Awesome |Emily Shire |November 10, 2014 |DAILY BEAST 

To be clear, what Peterson (allegedly) did is abhorrent and almost beyond comprehension. Adrian Peterson’s ‘Whooping’ and Ray Rice’s Knockout Are Both Domestic Violence |Robert Silverman |September 13, 2014 |DAILY BEAST 

To do so in a Salvadoran prison defies comprehension and inspires respect for their grit and determination. Out and Proud in El Salvador’s Murderous Gangland |Gene Robinson |July 13, 2014 |DAILY BEAST 

Nothing defeats my powers of comprehension like the deliberate harm of a child. When Table Salt Becomes Poison |Russell Saunders |June 20, 2014 |DAILY BEAST 

This is nostalgia as a recording exercise rather than a lesson in empathy or comprehension. Memory Porn: America’s Obscene Anniversary Obsession |Tim Teeman |June 17, 2014 |DAILY BEAST 

She has embodied in her work a modern comprehension of old legends. Women in the fine arts, from the Seventh Century B.C. to the Twentieth Century A.D. |Clara Erskine Clement 

It is beyond the comprehension of any man not blinded by superstition, not warped by prejudice and old-time convention. God and my Neighbour |Robert Blatchford 

This is given in the next few pages, and it will be found to be easy of comprehension and interesting to a surprising degree. Assimilative Memory |Marcus Dwight Larrowe (AKA Prof. A. Loisette) 

The mysterious letter was handed round our circle in succession, and seemed equally beyond comprehension to us all. Blackwood's Edinburgh Magazine, No. CCCXXXIX. January, 1844. Vol. LV. |Various 

To Madame Ratignolle he said the music dispensed at her soirees was too "heavy," too far beyond his untrained comprehension. The Awakening and Selected Short Stories |Kate Chopin