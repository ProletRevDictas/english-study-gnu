According to Van Eenennaam, the number of dairy cows in the United States decreased from around 25 million in the 1940s to around 9 million in 2007, while milk production has increased by nearly 60 percent. Biotechnology Could Change the Cattle Industry. Will It Succeed? |Dyllan Furness |August 16, 2020 |Singularity Hub 

Last week, the oat milk company raised $200mm at a $2bn valuation. Disruption, served one thread at a time: The weird world of DTC thoughtleader Twitter (1/23) |Anna Hensel |August 7, 2020 |Digiday 

Of course, these milk samples might feel different in the mouth. Can we taste fat? The brain thinks so |Bethany Brookshire |July 24, 2020 |Science News For Students 

For producers, that creates a milk glut — at least until the supply chain can be reconfigured. What Happens When Everyone Stays Home to Eat? (Ep. 412) |Stephen J. Dubner |April 9, 2020 |Freakonomics 

Without lactase, their bodies can’t properly digest the sugar found in milk. Scientists Say: Enzyme |Carolyn Wilke |April 6, 2020 |Science News For Students 

And, with Coca-Cola announcing the launch of a new milk product, the beverage could be back in our hands before we know it. History's Craziest Hangover Cures |Justin Jones |December 30, 2014 |DAILY BEAST 

He would shake a chilled Coke, and then spray the soda into a cold glass of milk. History's Craziest Hangover Cures |Justin Jones |December 30, 2014 |DAILY BEAST 

Warm milk mixed with a spoonful of fireplace ashes seemed to also be popular among 19th century England. History's Craziest Hangover Cures |Justin Jones |December 30, 2014 |DAILY BEAST 

Tosi has been using cereal milk as a flavor ever since 2007, and she says it taps into a universal “memory sensor.” Cereal Cafe’s Big Bowl of Hate |David Levesley |December 14, 2014 |DAILY BEAST 

You can even buy containers of their Cereal Milk in select stores. Cereal Cafe’s Big Bowl of Hate |David Levesley |December 14, 2014 |DAILY BEAST 

And right after that, some nice sour milk would come splashing down into the trough of the pen. Squinty the Comical Pig |Richard Barnum 

As soon as he was in it Squinty ran over to the trough, hoping there would be some sour milk in it. Squinty the Comical Pig |Richard Barnum 

He watched the man put some bread and milk in a tin pan, and set it down on the floor of the basket. Squinty the Comical Pig |Richard Barnum 

"Getting back home," answered Squinty, as he took a big drink of sour milk. Squinty the Comical Pig |Richard Barnum 

And Squinty thought acorns were just the best things he had ever tasted, except apples, and potatoes or perhaps sour milk. Squinty the Comical Pig |Richard Barnum