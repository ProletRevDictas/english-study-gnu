You know, when I was younger, I used to make problems for myself, like it was too easy. The Story Behind Lee Marvin’s Liberty Valance Smile |Robert Ward |January 3, 2015 |DAILY BEAST 

I was going to do it myself, but was waiting for the new year. The Insurance Company Promised a Gender Reassignment. Then They Made a Mistake. |James Joiner |December 29, 2014 |DAILY BEAST 

I push down on the pedal with my right leg and instead of propelling myself forward, I topple over sideways. You’re Never ‘Cured’ of an Eating Disorder |Carrie Arnold |December 20, 2014 |DAILY BEAST 

Walking along the Prado, every time, for better or worse, I pass different versions of myself and of Havana. The Life and Hard Times Of The Family A Cuban Defector Left Behind |Brin-Jonathan Butler |December 19, 2014 |DAILY BEAST 

Still, I found myself agreeing with the older gentleman who saw the room as a sea of gentiles. I Ate Potato Pancakes Til I Plotzed |Emily Shire |December 17, 2014 |DAILY BEAST 

I find myself chained to the foot of a woman, my noble Cornelia would despise! The Pastor's Fire-side Vol. 3 of 4 |Jane Porter 

There were three young men and four young ladies, of whom three, including myself, were Americans. Music-Study in Germany |Amy Fay 

We had half a dozen passengers to Ferrara; for the rest of the way, I had this extensive traveling establishment to myself. Glances at Europe |Horace Greeley 

Madame and myself had just been regretting that we should have to pass the evening in this miserable hole of a town. Blackwood's Edinburgh Magazine, Volume 60, No. 372, October 1846 |Various 

When I come home from the lessons I fling myself on the sofa, and feel as if I never wanted to get up again. Music-Study in Germany |Amy Fay