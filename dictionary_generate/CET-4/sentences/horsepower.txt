Beyond connectivity, this model’s quiet belt drive offers one- and-a-quarter horsepower, making it powerful enough for larger doors. Best garage door opener for your home |Irena Collaku |August 23, 2021 |Popular-Science 

Another even more specific way of defining them is as vehicles whose horsepower is 500 and up that cost around $250,000 or more. Anyone can drive a supercar, but truly tapping its potential is another matter |Rob Verger |July 28, 2021 |Popular-Science 

Size, weight, horsepower, and fuel source are just a few of the things to look for when shopping for a zero-turn mower. The best zero-turn mower: Tame your lawn with precision |Irena Collaku |July 22, 2021 |Popular-Science 

At times, Chrome was apparently limited to the Cortex-A55 cores only, ignoring most of the phone's computing horsepower, which is present in the bigger A78 and X1 cores. OnePlus admits to throttling 300 popular apps with recent update |Ron Amadeo |July 7, 2021 |Ars Technica 

There was a built-in audience with the kind of gearheads who love to pore over horsepower, steering and any other little cog that makes a car go vroom. The Fast and Furious franchise ranked, from worst to best |Hau Chu |June 24, 2021 |Washington Post 

In the race to the future, horsepower was losing to gas power, and the city was changing. ‘The Harness Maker’s Dream:’ The Unlikely Ranch King of Texas |Nick Kotz |September 20, 2014 |DAILY BEAST 

But the real leverage between the U.S. and China comes down to economic horsepower. Time to Get Tough With China? |Leslie H. Gelb |December 8, 2013 |DAILY BEAST 

They used a 30 horsepower refrigeration unit to keep the wall frozen and spent about $15 a day to power it. Fukushima N-Plant Will Be Surrounded by a Wall of Ice |Josh Dzieza |September 4, 2013 |DAILY BEAST 

It cruises at 43 miles per hour and has about a 10 horsepower engine, no larger than the Wright brothers' original vessel. Solar Plane That Can Fly Day and Night Takes Off |Miranda Green |May 3, 2013 |DAILY BEAST 

Unveiled at the EICMA motor show in Milan, the 1199 Panigale is the new king of the motorcycle horsepower race. Ducati’s Panigale and History’s Most Innovative Motorcycles |Chris Hunter |November 13, 2011 |DAILY BEAST 

The horsepower figures do not fully represent the extent of actual commercial control. Proceedings of the Second National Conservation Congress |Various 

If you had five-horsepower work to perform, how foolish it would be to install a two-hundred-and-fifty-pound engine! Think |Col. Wm. C. Hunter 

Zeppelin's first airship had two cars, with a motor in each, giving about 30 horsepower. The Romance of Aircraft |Lawrence Yard Smith 

Practice has shown that the amount of heating surface practically required by a boiler is 12 to 15 square feet per horsepower. Farm Engines and How to Run Them |James H. Stephenson 

In selecting an engine, the higher the horsepower for the given dimensions, the more economical of both fuel and water. Farm Engines and How to Run Them |James H. Stephenson