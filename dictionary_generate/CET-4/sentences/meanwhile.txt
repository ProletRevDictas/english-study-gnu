Meanwhile, in Florida, Bush was flooded with questions about whether gay marriage could possibly come to the Sunshine State. Jeb Bush’s Unseen Anti-Gay Marriage Emails |Jackie Kucinich |January 9, 2015 |DAILY BEAST 

Meanwhile, almost exactly 30 years after the trial, the judge left his home to board a steamboat and was never heard from again. New York’s Most Tragic Ghost Loves Minimalist Swedish Fashion |Nina Strochlic |January 8, 2015 |DAILY BEAST 

Meanwhile two kids were taken from their mother when she flew back to the UK from Turkey. Britain May Spy on Preschoolers Searching for Potential Jihadis |Nico Hines |January 7, 2015 |DAILY BEAST 

The woman in question, meanwhile, has business of her own to take care of—she is reported to be shopping a memoir. From Playboy Prince to Dirty Old Man? |Tom Sykes |January 5, 2015 |DAILY BEAST 

Meanwhile, the Anti-Defamation League issued a statement deploring the state GOP for its failure to censure Duke. The Louisiana Racists Who Courted Steve Scalise |Jason Berry |January 3, 2015 |DAILY BEAST 

Elizabeth, meanwhile, was filled with alarms respecting her daughter's unhappy infatuation. The Pastor's Fire-side Vol. 3 of 4 |Jane Porter 

Meanwhile, he had been selected as aide-de-camp by General d'Ure de Molans. Napoleon's Marshals |R. P. Dunn-Pattison 

Meanwhile, the Australian submarine has got up through the Narrows and has torpedoed a gunboat at Chunuk. Gallipoli Diary, Volume I |Ian Hamilton 

None of them came; but meanwhile a very extraordinary thing happened, for the house itself began to go. Davy and The Goblin |Charles E. Carryl 

Meanwhile, another form of imitation is developing, the fashioning of lasting semblances. Children's Ways |James Sully