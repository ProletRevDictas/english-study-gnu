As a means of preventing tooth decay in those cities that do fluoridate, the practice certainly looks like a success. Anti-Fluoriders Are The OG Anti-Vaxxers |Michael Schulson |July 27, 2016 |DAILY BEAST 

That would truly be a milestone to celebrate—until you see what that record “diversity” actually means. The Unbearable Whiteness of Congress |Dean Obeidallah |January 8, 2015 |DAILY BEAST 

And in order for them to realize their vision, they are willing to use any means. Ayaan Hirsi Ali: Our Duty Is to Keep Charlie Hebdo Alive |Ayaan Hirsi Ali |January 8, 2015 |DAILY BEAST 

With that, there is no means to consistently measure progress. Pentagon Doesn’t Know How Many People It’s Killed in the ISIS War |Nancy A. Youssef |January 7, 2015 |DAILY BEAST 

Part of the problem is the mandate of the war and the means with which the U.S. is fighting it do not match up. Pentagon Doesn’t Know How Many People It’s Killed in the ISIS War |Nancy A. Youssef |January 7, 2015 |DAILY BEAST 

She skilfully manages the side-lights, and by this means produces strong effects. Women in the fine arts, from the Seventh Century B.C. to the Twentieth Century A.D. |Clara Erskine Clement 

This is a feature by means of which it is always possible to distinguish the Great Horsetail from any other species. How to Know the Ferns |S. Leonard Bastin 

The specific gravity is most conveniently estimated by means of the urinometer—Squibb's is preferable (Fig. 14). A Manual of Clinical Diagnosis |James Campbell Todd 

There is cause for alarm when they bring one hundred and ten ships into these seas without any means of resistance on our part. The Philippine Islands, 1493-1898, Volume XX, 1621-1624 |Various 

“It means, my dear, that the Dragoons and the 60th will have to teach these impudent rebels a much-needed lesson,” said her uncle. The Red Year |Louis Tracy