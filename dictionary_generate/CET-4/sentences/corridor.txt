Colorado-based Four Points Funding invests largely in projects in the state’s rural corridors, which are often left out of the national conversation about economic development and equity. Opportunity Zones haven’t fully reached their potential, but don’t write them off yet |jakemeth |September 16, 2020 |Fortune 

The grand historic buildings along its retail corridors fell into disrepair. The Big Corporate Rescue and the America That’s Too Small to Save |by Lydia DePillis, Justin Elliott and Paul Kiel |September 12, 2020 |ProPublica 

The industrial corridor that stretches alongside the Mississippi River from Baton Rouge to New Orleans is nicknamed “Cancer Alley” because of the perceived health risks associated with local chemical emissions. New Research Shows Disproportionate Rate of Coronavirus Deaths in Polluted Areas |by Lylla Younes, ProPublica, and Sara Sneath |September 11, 2020 |ProPublica 

The country’s fourth national park, Gishwati Mukura, is due to open to tourists this spring between Volcanoes and Nyungwe National Parks to create a thriving wildlife corridor. 27 Epic Trips to Start Planning Now |The Editors |September 8, 2020 |Outside Online 

The federal decision to suspend those so-called risk corridor payments — designed to help health plans recover some of their losses — was one of the factors that caused many of the co-ops to fail, Corlette said. Only three of 26 Obamacare-era nonprofit health insurance co-ops will soon remain |lbelanger225 |September 6, 2020 |Fortune 

After the scanning takes place, KSM is led down a long corridor flanked by chain-link fences. 9/11 Mastermind Is Afraid of the Ladies |Tim Mak |December 16, 2014 |DAILY BEAST 

Controlling the corridor was essential to supporting deep operations elsewhere in eastern Afghanistan. Heart of Darkness: Into Afghanistan’s Taliban Valley |Matt Trevithick, Daniel Seckman |November 15, 2014 |DAILY BEAST 

“The southern border is a well known corridor for illegal entry into the United States,” he said. Republicans Fear-Mongering Over ISIS and Mexico Silent on Canadian Border |Ben Jacobs, Asawin Suebsaeng |October 23, 2014 |DAILY BEAST 

From doorways that lined a long corridor, people emerged at a run and began searching frantically. Inside a Hospital for the Criminally Insane |Caitlin Dickson |September 15, 2014 |DAILY BEAST 

He backed away and walked down the corridor as a file of terrified patients pressed themselves against the walls. Inside a Hospital for the Criminally Insane |Caitlin Dickson |September 15, 2014 |DAILY BEAST 

They were walking down a corridor, and Miss Thangue was peering through her lorgnette at the cards on the doors. Ancestors |Gertrude Atherton 

When we got to the house we entered an obscure corridor and began to find our way up a dark and narrow staircase. Music-Study in Germany |Amy Fay 

Her last words floated back from the depths of the corridor; a clock was striking and she had pattered off hastily. Ancestors |Gertrude Atherton 

They made their way to the corridor and strolled slowly up and down, passing and repassing others who were discussing the music. Bella Donna |Robert Hichens 

She opened a door at the back of the central hall and found herself in a pillared corridor with a door at either end. Ancestors |Gertrude Atherton