For instance, if you search Facebook today for vaccines, Facebook will gladly point you to several large groups that tell you not to get one. Facebook tries to clean up Groups with new policies |Sarah Perez |September 17, 2020 |TechCrunch 

According to Schwartz, there have been instances in which police officers on prescription medications like Xanax have been impaired on the job. The policing reforms in the Breonna Taylor settlement, explained |Fabiola Cineas |September 17, 2020 |Vox 

It depends on what your definition of “worth it” is, but coach’s challenges have certainly resulted in more instances of overturned calls than have requests. Don’t Blame The Refs For All Of These Replay Reviews |Jared Dubin |September 17, 2020 |FiveThirtyEight 

Milwaukee’s Fiserv Forum and Atlanta’s State Farm Arena, for instance, will serve as early voting locations. Trump contradicts CDC director on vaccine; Biden says Americans shouldn’t trust Trump |Colby Itkowitz, Felicia Sonmez, John Wagner |September 16, 2020 |Washington Post 

The first-round defeat against the Jazz in 2017, for instance, came after just a 2-1 edge. When It Comes To Playoff Disappointment, The Clippers Are In A League Of Their Own |Chris Herring (chris.herring@fivethirtyeight.com) |September 16, 2020 |FiveThirtyEight 

They just reflect the range of breeds that were used to create the Heck cattle in the first instance. ‘Nazi Cows’ Tried to Kill British Farmer |Tom Sykes |January 6, 2015 |DAILY BEAST 

For instance, Best Buy has over 40 million members in its customer loyalty program, Reward Zone. Best Buy Punches Back at Amazon |William O’Connor |December 27, 2014 |DAILY BEAST 

For instance, how do you balance honesty with any protective urge? Daphne Merkin on Lena Dunham, Book Criticism, and Self-Examination |Mindy Farabee |December 26, 2014 |DAILY BEAST 

El Bulli, for instance, previously named the best restaurant in the world, shuttered its doors after only a few decades. Inside The World’s 10 Oldest Restaurants |Justin Jones |December 20, 2014 |DAILY BEAST 

This happens, for instance, in one out of five vaccinations against rubella. Uh Oh: Ebola Vaccine Trials Stop |Leigh Cowart |December 19, 2014 |DAILY BEAST 

For instance, the Limestone Polypody is not happy unless there is a certain amount of lime present in the soil. How to Know the Ferns |S. Leonard Bastin 

A delightful instance of this fell under my own observation, as I was walking on Hampstead Heath. Children's Ways |James Sully 

Because the universe is governed by laws, and there is no credible instance on record of those laws being suspended. God and my Neighbour |Robert Blatchford 

Many of his bird neighbors,p. 31 for instance, liked the same things to eat that he did. The Tale of Grandfather Mole |Arthur Scott Bailey 

For instance, few workmen will take a holiday; they prefer a "day's out" or "play." Showell's Dictionary of Birmingham |Thomas T. Harman and Walter Showell