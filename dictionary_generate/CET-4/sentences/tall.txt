Today, drivers want tall wagons that we call “crossover SUVs.” Ford’s electric Mustang Mach-E is an important leap into the future |Dan Carney |February 12, 2021 |Popular-Science 

Having recently studied mountains — the American Rockies and the Himalayas — in my seventh-grade geology class, I take issue with the Alleghenies being mountains, even though Liberty’s 1,190-foot summit is the tallest I’ve ever seen. Small ski areas, big impact |Dina Mishev |February 5, 2021 |Washington Post 

Xerox’s new printer is 9 feet wide, 7 feet tall, and reaches an internal temperature of more than 1,500 degrees Fahrenheit. This huge Xerox printer can create metal parts for the US Navy |Rob Verger |February 4, 2021 |Popular-Science 

Our tallest building, One America Plaza at the foot of Broadway, reaches right to the maximum height. San Diego’s Sky-High Dreams Keep Getting Quashed |Randy Dotinga |February 2, 2021 |Voice of San Diego 

An earlier version of this article said the Helix will be 22 stories tall, but it will not have traditional stories. Amazon unveils Helix building as heart of campus in Arlington |Fredrick Kunkle |February 2, 2021 |Washington Post 

Also, she was tall and thin, too, further adding to the ways she met the physical beauty conventions. Why Was Bess Myerson the First and Last Jewish Miss America? |Emily Shire |January 7, 2015 |DAILY BEAST 

But when she called back, Brinsley was determined to tall her about his minted screenwriter status. Alleged Cop Killer’s Blood-Soaked Screenplay |M.L. Nestel |December 24, 2014 |DAILY BEAST 

It had a wide brim and a tall crown, which created an insulated pocket of air and could also be used to carry water. My Love Letter to the Stetson |Mark McKinnon |December 24, 2014 |DAILY BEAST 

A tugboat improbably sits high on the bank, obscured by tall grass, a broken oil rig hangs over the water nearby. The Congo's Forgotten Colonial Getaway |Nina Strochlic |December 18, 2014 |DAILY BEAST 

It was headquartered in Stanleyville, in a tall corner building that still stands in the decrepit, yet lively, downtown. The Congo's Forgotten Colonial Getaway |Nina Strochlic |December 18, 2014 |DAILY BEAST 

A tall phantom in livery appeared, as if by magic, and signed to me to ascend the grand staircase. Music-Study in Germany |Amy Fay 

He is rather tall and narrow, and wears a long abb's coat reaching nearly down to his feet. Music-Study in Germany |Amy Fay 

He was tall and of familiar figure, and the firelight was playing in the tossed curls of his short, fair hair. The Bondboy |George W. (George Washington) Ogden 

She threw out her arms as if swimming when she walked, beating the tall grass as one strikes out in the water. The Awakening and Selected Short Stories |Kate Chopin 

The way was under a double row of tall trees, which met at the top and formed a green arch over our heads. Music-Study in Germany |Amy Fay