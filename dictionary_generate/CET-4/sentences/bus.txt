A county spokesperson in Texas said that only 15 to 20 people were being placed on buses rather than the usual 50, and there were concerns about having large numbers of people sheltering in one place. Hurricane Laura is the strongest storm to hit Louisiana in more than a century |Sara Chodosh |August 28, 2020 |Popular-Science 

A school bus driver, Gladden was out of work for the summer. She Was Sued Over Rent She Didn’t Owe. It Took Seven Court Dates to Prove She Was Right. |by Danielle Ohl, Capital Gazette, and Talia Buford and Beena Raghavendran, ProPublica |August 25, 2020 |ProPublica 

Officers employed by the Metropolitan Transit System, which runs many of the region’s buses and trolleys, cited a man last year for failing to comply with their orders. Morning Report: 3 Body Cameras, No Footage |Voice of San Diego |July 22, 2020 |Voice of San Diego 

Last week, the agency responsible in San Diego for the buses and trolleys decided to pull back on a program to catch more people who ride without paying. Morning Report: Police Reformers Are Coming for MTS |Voice of San Diego |June 25, 2020 |Voice of San Diego 

Together they form teams that patrol stations, buses and trolleys. The Police Reform Push Comes for MTS |Lisa Halverstadt and Jesse Marx |June 25, 2020 |Voice of San Diego 

Occasionally a pamphlet for a salsa class might be tossed on a doorstop or stuck on a pole near a bus stop. Iran’s Becoming a Footloose Nation as Dance Lessons Spread |IranWire |January 2, 2015 |DAILY BEAST 

The detectives learned early on that Brinsley had arrived by bus in Manhattan. Exclusive: Inside a Cop-Killer’s Final Hours |Michael Daly |December 31, 2014 |DAILY BEAST 

At the music studio, Brinsley would arrive by train or bus to break into the music scene. Alleged Cop Killer’s Blood-Soaked Screenplay |M.L. Nestel |December 24, 2014 |DAILY BEAST 

In the video, the bus is getting searched by a cop with a German shepherd. Alleged Cop Killer Ismaaiyl Brinsley Had a Death Wish |M.L. Nestel |December 22, 2014 |DAILY BEAST 

A few children, settler children, congregate near what appears to have been the bus station. Inside Hebron, Israel’s Heart of Darkness |Michael Tomasky |November 21, 2014 |DAILY BEAST 

Passengers are requested not to stand on top of the Bus back seats for smoking. Punch, or the London Charivari, Volume 158, April 28, 1920 |Various 

And the same goes for any other common carrier—the railroads, bus service, and airlines. Hooded Detective, Volume III No. 2, January, 1942 |Various 

Buying an evening paper—wearing a waistcoat again—running after a bus—anything—it's all holiday. First Plays |A. A. Milne 

Henri crawled under the bus, though the policeman was extremely anxious to keep him out. The Amazing Interlude |Mary Roberts Rinehart 

Coming up she had sat on top of the bus and watched with wide curious, eyes the strange traffic of London. The Amazing Interlude |Mary Roberts Rinehart