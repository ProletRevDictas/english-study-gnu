Officials countered the claim, saying they tended to Taylor as soon as they could, though they didn’t initially know she was injured since everything happened in the dark. The policing reforms in the Breonna Taylor settlement, explained |Fabiola Cineas |September 17, 2020 |Vox 

There are, of course, counter-arguments to what Robinson is saying here. Does Anyone Really Know What Socialism Is? (Ep. 408 Rebroadcast) |Stephen J. Dubner |September 17, 2020 |Freakonomics 

Ernie Herrman, TJX chief Retailers are also planning online promotions early in the season in an attempt to counter Amazon’s Prime Day, which is usually held in July but has been pushed back until the fourth quarter this year. Retail Jingle Bells to Ring Longer Than Ever Before |Charu Kasturi |September 9, 2020 |Ozy 

What we have, I learn, is a late-season, last-minute, over-the-counter, nonresident, archery-only antlered-deer tag on public land. Instagram's Most Fascinating Subculture? Women Hunters. |Rachel Levin |September 8, 2020 |Outside Online 

Reinoehl had described himself in a social media posts as "100% ANTIFA" and suggested the tactics of counter-protesters amounted to "warfare," per the AP. Portland shooting suspect Michael Forest Reinoehl killed by officers |Axios |September 4, 2020 |Axios 

When Chérif got out of prison, he worked at the fish counter of a supermarket. France Mourns—and Hunts |Nico Hines, Christopher Dickey |January 8, 2015 |DAILY BEAST 

This led to the formation of a Christian militant group to counter the rebels, and all-out sectarian violence exploded. The Year’s Most Forgotten Humanitarian Crisis |Nina Strochlic |January 1, 2015 |DAILY BEAST 

It seemed gratuitous and counter-intuitive in a story that had already inflicted more than enough suffering. How Dickens and Scrooge Saved Christmas |Clive Irving |December 22, 2014 |DAILY BEAST 

Then the sun went down and the anger came back as a “Thank You NYPD” rally traded insults with counter-protestors. NYC’s Garner Protesters vs. Pro-Cop Protesters |Jacob Siegel |December 20, 2014 |DAILY BEAST 

If you need to store the bottle in the fridge, let it warm up for a few minutes on the counter before serving. Champagne: You’re Drinking It All Wrong |Kayleigh Kulp |December 20, 2014 |DAILY BEAST 

On to Gaba Tepe just in time to see the opening, the climax and the end of the dreaded Turkish counter attack. Gallipoli Diary, Volume I |Ian Hamilton 

Our beloved Queen had drawn the teeth of the Turkish counter-attack on our extreme left. Gallipoli Diary, Volume I |Ian Hamilton 

Thereupon, I gave the word for a general counter-attack and our line began to advance. Gallipoli Diary, Volume I |Ian Hamilton 

The handkerchief glimmered on the counter, more white than anything else in that grey dusk. Hilda Lessways |Arnold Bennett 

She preceded him along a passage and then, taking a door on the left, found herself surprisingly in the shop, behind a counter. Hilda Lessways |Arnold Bennett