It sat in my coat pocket and warmed my hand as I boarded Amtrak to go back to Penn Station. Until I Can Go Back to My Favorite Restaurant, This Jerk Paste Is the Next Best Thing |Elazar Sontag |September 25, 2020 |Eater 

At a height of 62 inches, it’s also roomy enough to hold long garments like coats and dresses without dragging them on the ground. Garment racks for maximizing space in every type of room |PopSci Commerce Team |September 9, 2020 |Popular-Science 

Since they’re made of rigid material, bugs also have a hard time getting in, making this a great choice for wool blankets and coats. The best under-bed storage solutions |PopSci Commerce Team |September 9, 2020 |Popular-Science 

Male prisoners would usually rip the lining and seams of the coat to shreds, keeping only the outer shell intact. Auschwitz: Women Used Different Survival And Sabotage Strategies Than Men At Nazi Death Camp |LGBTQ-Editor |April 19, 2020 |No Straight News 

Train was perfect for this job because his thick coat protected him from plants with spines and prickles. Conservation is going to the dogs |Alison Pearce Stevens |April 2, 2020 |Science News For Students 

He wore white gloves, a dignified long black coat, and matching pants and vest, and he carried a dark walking stick. The Black Man Who Replaced Jefferson Davis in the Senate |Philip Dray |January 7, 2015 |DAILY BEAST 

Place the thinly sliced shallots in a medium bowl and pour buttermilk over to coat. Make Carla Hall’s Crispy Shallot Green Bean Casserole |Carla Hall |December 27, 2014 |DAILY BEAST 

The man finally manages to break free with the help of the others, slipping out of his coat. Protesters Slimed This Good Samaritan Cop |Michael Daly |December 16, 2014 |DAILY BEAST 

Micah is 10 years old and he had a coat geared to the season, a Patagonia winter jacket with a hood. The Wildly Peaceful, Human, Almost Boring, Ultimately Great New York City Protests for Eric Garner |Mike Barnicle |December 8, 2014 |DAILY BEAST 

He tore a piece of meat off the breast and stroked her coat while she ate. The Stacks: A Chicken Dinner That Mends Your Heart |Pete Dexter |December 7, 2014 |DAILY BEAST 

He is rather tall and narrow, and wears a long abb's coat reaching nearly down to his feet. Music-Study in Germany |Amy Fay 

You don't mind staying here in the sunshine, I hope, while my coat dries? The Tale of Grandfather Mole |Arthur Scott Bailey 

Presently he began to shiver so, with some sort of a chill, that I took off my coat and wrapped it round him. The Boarded-Up House |Augusta Huiell Seaman 

He scratched his head in vexation, sat down, and as he did so, saw that his coat hung also upon the chair. The Homesteader |Oscar Micheaux 

I queried; and as I asked the question I noticed for the first time the gilt bars on his coat sleeve. Raw Gold |Bertrand W. Sinclair