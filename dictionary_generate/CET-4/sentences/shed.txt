I found some paper and sketched out a crude design—shed style, a simple two-by-four frame, with a top layer of corrugated tin. I Missed Bars. So I Built One in My Own Backyard. |Nick Heil |October 16, 2020 |Outside Online 

Outdoor tool sheds can be a real game changer for the busy homeowner. The best outdoor tool sheds for all of your storage needs |PopSci Commerce Team |October 14, 2020 |Popular-Science 

Homes there might be unstable sheds made of sheet-metal and plywood. A dirty and growing problem: Too few toilets |Stephanie Parker |September 24, 2020 |Science News For Students 

Headquarters is a cluster of large hangar-like sheds, surrounded by woods, several kilometers off the beaten track. The Fungal Evangelist Who Would Save the Bees - Issue 90: Something Green |Merlin Sheldrake |September 23, 2020 |Nautilus 

From a spare bedroom and a nook under the stairs, to a backyard shed or a repurposed dining table, the millions around the world who are new to working remotely are having to reassess their living quarters to make room for a viable work surface. A peek inside home offices around the world |Anne Quito |September 20, 2020 |Quartz 

“I sense that mobile games are starting to shed their skin, getting rid of all the dead things they carry around,” he says. Lost For Thousands of Strokes: 'Desert Golfing' Is 'Angry Birds' as Modern Art |Alec Kubas-Meyer |January 2, 2015 |DAILY BEAST 

When I first arrived at Duke, hooking up with a stranger seemed like a way to shed my inhibitions. Random Hook-Ups or Dry Spells: Why Millennials Flunk College Dating |Ellie Schaack |January 1, 2015 |DAILY BEAST 

Overnight people shed their fears, their protective camouflage and their restraints. How Havel Inspired the Velvet Revolution |Michael Zantovsky |December 6, 2014 |DAILY BEAST 

Both priceless papyri that could shed light on early Christianity and forgeries are openly trafficked online. Dismembering History: The Shady Online Trade in Ancient Texts |Candida Moss |November 23, 2014 |DAILY BEAST 

While the bats are infected, they shed large quantities of virus that can infect other animals. Bats’ Link to Ebola Finally Solved |Carrie Arnold |November 12, 2014 |DAILY BEAST 

A little corral for the sheep, and a rough shed for the pony, and the home was complete: far the prettiest home they had ever had. Ramona |Helen Hunt Jackson 

Above, great standard electric lamps shed their white glare upon the eddying throng casting a myriad of grotesque shadows. The Joyous Adventures of Aristide Pujol |William J. Locke 

Then waves of grief broke over her, and she sobbed convulsively; but still she shed no tears. Ramona |Helen Hunt Jackson 

Even the Grass-land is often ridged so as to shed the water quickly, while deep ditches or drains do duty for fences. Glances at Europe |Horace Greeley 

Robert was out there under the shed, reclining in the shade against the sloping keel of the overturned boat. The Awakening and Selected Short Stories |Kate Chopin