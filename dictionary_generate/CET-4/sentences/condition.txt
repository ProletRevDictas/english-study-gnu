Most of the time, you’re better off running a modern studless winter tire, like these Nokians, and staying home if conditions reach the point where those might not be enough. Don't Run Tire Chains This Winter |Nathan Norby |February 12, 2021 |Outside Online 

As you might imagine, we have several duplicates of kitchen gadgets, all of which are in very good condition. Hints From Heloise: New scam tied to coronavirus pandemic |Heloise Heloise |February 12, 2021 |Washington Post 

None of it was easy, but it taught her a lot about the human condition. The loneliness of an interrupted adolescence |Ellen McCarthy |February 11, 2021 |Washington Post 

Police say she’s listed in critical condition at a hospital. As police continue probe of Britt Reid crash, 5-year-old remains in critical condition |Rick Maese |February 9, 2021 |Washington Post 

She alleged that they were responsible for the crash because Zobayan had not properly checked the weather before taking off and had flown into unsafe conditions. Helicopter pilot flying Kobe Bryant didn’t follow his training when flying into disorienting clouds, federal investigators say |Ian Duncan |February 9, 2021 |Washington Post 

At St. Barnabas Hospital, Pellerano was listed in stable condition with wounds to his chest and arm. Shot Down During the NYPD Slowdown |Michael Daly |January 7, 2015 |DAILY BEAST 

Dossi initially was listed in critical condition with wounds to his arm and lower back. Shot Down During the NYPD Slowdown |Michael Daly |January 7, 2015 |DAILY BEAST 

When we meet them, their lives are unfulfilled, and at no point are we convinced their condition will change. The Lost Novel of Nobel-Winner José Saramago |Charles Shafaieh |January 5, 2015 |DAILY BEAST 

So, what happens if nothing in his training has replicated such a dire condition? Flight 8501 Poses Question: Are Modern Jets Too Automated to Fly? |Clive Irving |January 4, 2015 |DAILY BEAST 

The official spoke on condition of anonymity so as not to harm future access to those embattled communities. ISIS Fight Has a Spy Shortage, Intel Chair Says |Kimberly Dozier |January 2, 2015 |DAILY BEAST 

There he gave orders for the car to be put into running condition for the following morning, and returned to the hotel. The Joyous Adventures of Aristide Pujol |William J. Locke 

Soon after that, I wrote you in regard to the condition in which we found this infant Church and Colony. The Jesuit Relations and Allied Documents, Vol. II: Acadia, 1612-1614 |Various 

The men arrived in very bad condition, and many of them blinded with the salt water which had dashed into their eyes. The Philippine Islands, 1493-1898, Volume XX, 1621-1624 |Various 

Mlle. Mayer had been for some time in a depressed condition, and her friends had been anxious about her. Women in the fine arts, from the Seventh Century B.C. to the Twentieth Century A.D. |Clara Erskine Clement 

Condition of the archbishopric of Manila in regard to the affairs of ecclesiastical and secular government. The Philippine Islands, 1493-1898, Volume XX, 1621-1624 |Various