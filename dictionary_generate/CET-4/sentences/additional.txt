In fact, on some nights the Secret Service appears to have been charged for even more rooms than usual — not just the Sarazen Cottage, but one or two additional rooms as well. Trump’s businesses charged Secret Service more than $1.1 million, including for rooms in club shuttered for pandemic |David Fahrenthold, Josh Dawsey |September 17, 2020 |Washington Post 

The event will be hosted by Felicia Curry and Naomi Jacobson and will include additional award announcements for outstanding productions, ensembles, and the John Aniello Award for Outstanding Emerging Theatre Company. D.C. fall calendar filled with virtual events |Steph Purifoy |September 16, 2020 |Washington Blade 

House Democrats argue those efforts raise additional ethical concerns. “Cover Up”: House Democrats Subpoena Documents That NLRB Refused to Share in Ethics Investigation |by Ian MacDougall |September 15, 2020 |ProPublica 

A division spokeswoman confirmed Quinn was not yet an employee in the division at the time she participated in the meeting but declined to answer additional questions. No Democrats Allowed: A Conservative Lawyer Holds Secret Voter Fraud Meetings With State Election Officials |by Mike Spies, Jake Pearson and Jessica Huseman |September 15, 2020 |ProPublica 

In these places, heat alone will cause as many as 80 additional deaths per 100,000 people — the nation’s opioid crisis, by comparison, produces 15 additional deaths per 100,000. Climate Change Will Force a New American Migration |by Abrahm Lustgarten, photography by Meridith Kohut |September 15, 2020 |ProPublica 

An additional 12,000 took to the streets in other German towns. Europe’s Islam Haters Say We Told You So |Barbie Latza Nadeau |January 8, 2015 |DAILY BEAST 

Should capability delivery experience additional changes, this estimate will be revised appropriately. Pentagon Misfires in Stealth Jet Scandal |Dave Majumdar |January 8, 2015 |DAILY BEAST 

As it happened, the coup members found the State House “fortified with additional soldiers.” The Shadowy U.S. Veteran Who Tried to Overthrow a Country |Jacob Siegel |January 6, 2015 |DAILY BEAST 

Once transferred to Karaj Prison, he spent an additional 15 days in solitary confinement. A Daughter’s Plea: Free My Father from Prison in Iran |Mitra Pourshajari, Movements.Org, Advancing Human Rights |December 26, 2014 |DAILY BEAST 

In November 2014, that agreement was extended by four months, with some additional restrictions on Iran. Fact-Checking the Sunday Shows: Dec 21 |PunditFact.com |December 21, 2014 |DAILY BEAST 

The case may be kept in a light position, and when once under way it will rarely need any additional water. How to Know the Ferns |S. Leonard Bastin 

These additional languages can be acquired easily if they are learnt in the right way. The Salvaging Of Civilisation |H. G. (Herbert George) Wells 

Nevertheless, when once issued, they made unnecessary any resort to additional Bank of England notes. Readings in Money and Banking |Chester Arthur Phillips 

“Doctrine”—the Monroe doctrine declared that no foreign power should acquire additional dominion in America. Assimilative Memory |Marcus Dwight Larrowe (AKA Prof. A. Loisette) 

Some peasants had brought the news to the chateau, with the additional information that they were all to be shot within two days. Blackwood's Edinburgh Magazine, Volume 60, No. 372, October 1846 |Various