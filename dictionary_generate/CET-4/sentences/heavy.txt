On heavier, more powerful machines, good tires are even more important, as they let you access more of the ATV’s power. Six ways to make your ATV even more rugged |By Tyler Freel/Outdoor Life |September 17, 2020 |Popular-Science 

Please note this is a heavy item at approximately 47 pounds. Great filing cabinets for your home office |PopSci Commerce Team |September 17, 2020 |Popular-Science 

Smooth binding and rounded corners keep this notebook sturdy after heavy use. Notable notebooks for writing and drawing |PopSci Commerce Team |September 17, 2020 |Popular-Science 

Tech always carries a heavy weight, but that increases markedly when investors bid shares up. 3 ways tech stocks resemble the 2000 bubble—and one way they don’t |Anne Sraders |September 16, 2020 |Fortune 

She endorsed Medicare-for-all and a Green New Deal, suggesting that a suburb-heavy state that has trended toward Democrats could have more liberal representation in Washington. Trump, in town hall, says he wouldn’t have done anything differently on pandemic |Colby Itkowitz, Josh Dawsey, Felicia Sonmez, John Wagner |September 16, 2020 |Washington Post 

The clichés about football-obsessed husbands and frustrated wives are pretty heavy-handed. ‘A Gronking to Remember’ Speed Read: 8 Naughtiest Bits |Emily Shire |January 7, 2015 |DAILY BEAST 

“There is a heavy security presence but nothing has changed,” agrees Father Javier. Mexico’s Priests Are Marked for Murder |Jason McGahan |January 7, 2015 |DAILY BEAST 

The running machines are a gloomy chorus of heavy-footed stomping. How to Survive the New Year ‘Gympocalypse’ |Tim Teeman |January 6, 2015 |DAILY BEAST 

“JSwipe is currently under heavy load,” flashed across the screen, one night as a friend and I looked at it. My Week on Jewish Tinder |Emily Shire |January 5, 2015 |DAILY BEAST 

Up till then I was just a dog-assed heavy, one of the posse. The Story Behind Lee Marvin’s Liberty Valance Smile |Robert Ward |January 3, 2015 |DAILY BEAST 

The policemen looked dull and heavy, as if never again would any one be criminal, and as if they had come to know it. Bella Donna |Robert Hichens 

Drone: the largest tube of a bag-pipe, giving forth a dull heavy tone. Gulliver's Travels |Jonathan Swift 

Hunter-Weston despite his heavy losses will be advancing to-morrow which should divert pressure from you. Gallipoli Diary, Volume I |Ian Hamilton 

Heavy firing continued all that afternoon, inflicting great loss on the rebels, whilst the Spaniards lost one soldier. The Philippine Islands |John Foreman 

The keen resentment had faded from his face, but an immense reproach was there—a heavy, helpless, appealing reproach. Confidence |Henry James