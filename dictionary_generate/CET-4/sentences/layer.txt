I just wanted to add layers to it and not create a scary villain-type character but really give him some intricacies. LaKeith Stanfield Says Portraying William O’Neil In ‘Judas’ Changed His Perception Of The FBI Informant |Brande Victorian |February 12, 2021 |Essence.com 

Dating of sediment layers and burned wood fragments provided an age estimate for the site. Stonehenge may have had roots in a Welsh stone circle |Bruce Bower |February 12, 2021 |Science News 

By watching how the vibrations' paths shift as they encounter different materials, we can get a picture of where different rock layers meet, where rock becomes molten, and more. Using whale songs to image beneath the ocean’s floor |John Timmer |February 11, 2021 |Ars Technica 

It’s the layer I throw on when I’m topping out on a windy ridge or skinning up the mountain. 5 Pieces of Gear That Help Me Stay Active During Winter |Jakob Schiller |February 11, 2021 |Outside Online 

Heated vests can be a critical extra layer that keep your core warm and your muscles relaxed throughout your sweat session. Best heated vest: Beat the cold weather with the right winter gear |PopSci Commerce Team |February 9, 2021 |Popular-Science 

Just who is crazy enough to go swimming when the pond across the street has a layer of ice across the top? Diving Into 2015 With Polar Bear Plunge Extremists |James Joiner |January 1, 2015 |DAILY BEAST 

When summer comes, adult beetles attack and larva feed in the cambium layer, girdling the trees and sealing their doom. What It Takes to Kill a Grizzly Bear |Doug Peacock |November 23, 2014 |DAILY BEAST 

After maybe an hour, the firefighters had cut through the last layer of a 4- by 8-foot area. Rescue at One World Trade Center |Michael Daly |November 13, 2014 |DAILY BEAST 

These words take on a dark layer of irony given the charges facing her. Colombian Beauty Queen Arrested for Running Child Prostitution Ring |Jason Batansky |October 17, 2014 |DAILY BEAST 

With each successive layer of networks, the U.S. became more potent. Electricity Superhighway | |October 16, 2014 |DAILY BEAST 

It separates into three layers upon standing—a brown deposit, a clear fluid, and a frothy layer. A Manual of Clinical Diagnosis |James Campbell Todd 

The upper surfaces of the boilers were covered with a layer of ashes for the same purpose. Life of Richard Trevithick, Volume II (of 2) |Francis Trevithick 

By the repetition of this process, a layer of several feet in thickness, of an excellent soil, is accumulated on the surface. Elements of Agricultural Chemistry |Thomas Anderson 

He was shot by a man of the 32d, and his body formed the lowermost layer of a causeway of corpses that soon choked the ditch. The Red Year |Louis Tracy 

Laura drew out the third drawerjust glancing at the top layer of papersand then the fourth and last. The Girls of Central High on the Stage |Gertrude W. Morrison