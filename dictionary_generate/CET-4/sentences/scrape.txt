A case with a raised edge around the screen can provide a handy bumper that surrounds the display and protects it from small bumps, scrapes, and accidental trips to the floor. The best Apple Watch case to protect the computer on your wrist |Stan Horaczek |August 23, 2021 |Popular-Science 

After spending weeks on a Conservation Corps in the Arizona desert last fall, my work pants reeked of urine and my legs suffered dozens of scrapes from trying to find a secluded spot to squat in thorny Devils Claw. Tested: Pants You Can Pee In |Claire Barber |June 24, 2021 |Outside Online 

There might some scrapes on it, but unless it gets run over by a car, it really shouldn’t crack or anything. A college pitcher is trying to turn the theft of his prosthetic arm into a charity drive |Cindy Boren |May 7, 2021 |Washington Post 

Compean, who sustained just a few minor cuts and scrapes, yelled repeatedly to no one in particular. A hiker was lost and desperate. A stranger with an unusual hobby saved him. |Sydney Page |April 22, 2021 |Washington Post 

You remember it when you park or whenever another car gets a little too close in traffic, given the pricey consequences of a scratch or scrape. The Rolls-Royce Ghost: A magic carpet ride that costs as much as a house |Jonathan M. Gitlin |April 12, 2021 |Ars Technica