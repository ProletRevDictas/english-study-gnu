A final call came into the Westmoreland County 911 Center in Pennsylvania from a man who said he was locked in the lavatory. Sept. 11, 2001: An ordinary work day, then surreal scenes of dread and death |David Maraniss |September 10, 2021 |Washington Post 

Those who need to go to the bathroom are escorted by an officer to the plane’s tiny lavatory. Locked up in the Land of Liberty: Part I |Yariel Valdés González |July 7, 2021 |Washington Blade 

Pictures taken by Tarai show the flight attendant standing by the lavatory, feet away from Quinonez, as she waited for police to escort the passenger from the plane after they landed. Video shows a woman punching a Southwest flight attendant in the face, knocking out teeth: ‘It was all bad’ |Andrea Salcedo |May 28, 2021 |Washington Post 

Some cities, including Portland, Oregon, and Montreal, Canada, are rising to the challenge with stand-alone lavatories that anybody can use, free of charge. How to build freestanding public restrooms that are clean and safe |Andrew Giambrone |April 28, 2021 |MIT Technology Review 

He jiggled the faceted glass door knob to let the person inside know he was hogging the lavatory. Exclusive Excerpt: MLK's Haunting Final Hours |Hampton Sides |April 24, 2010 |DAILY BEAST 

The impromptu Bombastes excited universal applause, and just at the end Wright ran in through the lavatory. Eric, or Little by Little |Frederic W. Farrar 

The consequence was, that I was severely hurt, and might have been seriously injured in entering the lavatory. Eric, or Little by Little |Frederic W. Farrar 

There was a little lavatory off the third room of the suite, and Mr. Norcross went in and washed his face and hands. The Wreckers |Francis Lynde 

On the other, through a curtained alcove, he could see a tiny lavatory. The Status Civilization |Robert Sheckley 

A study of the illustrations will show clearly how the lavatory has been improved. Elements of Plumbing |Samuel Dibble