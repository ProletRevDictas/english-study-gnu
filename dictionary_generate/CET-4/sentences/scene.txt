We don’t want to come on set and crowd an actress who’s just had a really difficult scene. ‘Antebellum’ tackles the past head on in an effort to ‘move forward’ |radmarya |September 17, 2020 |Fortune 

The 19-year-old from Los Angeles posts comedy videos, skits, and re-enacts scenes from Netflix shows. What’s Oracle? TikTok users react to proposed Oracle deal |Danielle Abril |September 15, 2020 |Fortune 

These 39 people — some of whom you’ll recognize, while others operate behind the scenes — will shape what could be a rocky time from November 3 to January 20. Sunday Magazine: The Deciders |Daniel Malloy |September 13, 2020 |Ozy 

I’ve had moments where I’m literally on set, waiting to do a scene, my lines are ready, I’m geeked up, ready to go. Sitcom Queen Tichina Arnold Got Funny to Avoid Spankings |Pallabi Munsi |September 10, 2020 |Ozy 

Behind the scenes, people were talking about Potts’ trip to the hospital. The Woman Propositioned by Alaska’s Former Lieutenant Governor Tells Her Story for the First Time |by Kyle Hopkins and Michelle Theriault Boots, Anchorage Daily News |September 10, 2020 |ProPublica 

Hovering above the scene, commandos in helicopters were poised with automatic rifles. France Kills Charlie Hebdo Murderers |Nico Hines |January 9, 2015 |DAILY BEAST 

But since those rosy scenarios were first floated, the California political scene has grown more crowded. The Golden State Preps for the ‘Red Wedding’ of Senate Races |David Freedlander |January 9, 2015 |DAILY BEAST 

The scene was heavily cordoned off to traffic and anyone not with the police, press, or residents. Police Hunt for Paris Massacre Suspects |Tracy McNicoll, Christopher Dickey |January 7, 2015 |DAILY BEAST 

The following page details a tribute gag the Simpsons team inserted into the background of a scene. Here’s the Lost Judd Apatow ‘Simpsons’ Episode, Penned by Judd Apatow |Asawin Suebsaeng |January 6, 2015 |DAILY BEAST 

“The play contains one five minute scene about James Hewitt,” Conway says. Harry’s Daddy, and Diana’s ‘Murder’: Royal Rumors In a New Play |Tom Sykes |January 4, 2015 |DAILY BEAST 

Then Paterno adroitly brought matters to a crisis in a bold peroration which changed the whole scene. The Philippine Islands |John Foreman 

The scene is the covenant made between the two first persons of the Trinity on Mount Moriah. Blackwood's Edinburgh Magazine, Volume 60, No. 372, October 1846 |Various 

Impressed by the lugubrious scene, Aguinaldo yielded, and the next day peace negotiations were opened. The Philippine Islands |John Foreman 

The scene and field of that learning hitherto has been, in our Western communities, the University. The Salvaging Of Civilisation |H. G. (Herbert George) Wells 

It was not until later days that Malcolm knew the real nature of the scene through which he rode. The Red Year |Louis Tracy