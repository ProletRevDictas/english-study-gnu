The executive wondered whether his employer could prevent him from taking the trip, or if he’d have to avoid returning to the office afterward. 33 states have instituted post-travel COVID quarantines—posing a dilemma for executives |Jeff |July 23, 2020 |Fortune 

I then made a line of small piles of ricotta stuffing, before folding over the pasta and squeezing out air pockets to avoid the raviolis bursting in the boiling water. With food tourism in crisis, virtual pasta and paella courses take off |Bernhard Warner |July 18, 2020 |Fortune 

Because of coronavirus fears, many people have switched to using credit cards and mobile payments to avoid handling money. Why is there a coin shortage in the U.S.? |Danielle Abril |July 18, 2020 |Fortune 

The state has released about 10,000 inmates early to avoid crowding because of the pandemic, and another 8,000 could be released by August. California is facing a wildfire fighter shortage because prisoners are getting sick with COVID |Nicole Goodkind |July 15, 2020 |Fortune 

For example, it can allow households to avoid home foreclosure, evictions and car repossession. Bankruptcy Courts Ill-Prepared For Tsunami of People Going Broke From Coronavirus Shutdown |LGBTQ-Editor |July 12, 2020 |No Straight News 

There are reasons that European countries tend to avoid fluoride. Anti-Fluoriders Are The OG Anti-Vaxxers |Michael Schulson |July 27, 2016 |DAILY BEAST 

It is also important to avoid using the pope as part of a marketing strategy. Pope Francis Has the Pleasure of Meeting Angelina Jolie for a Few Seconds |Barbie Latza Nadeau |January 8, 2015 |DAILY BEAST 

We try to avoid going away for too long, so we can check back in. Belle & Sebastian Aren’t So Shy Anymore |James Joiner |January 7, 2015 |DAILY BEAST 

Such statements are rare, as the Guards routinely avoid going public with news about the demise of one of their commanders. What an Iranian Funeral Tells Us About the Wars in Iraq |IranWire |January 6, 2015 |DAILY BEAST 

The pilot asked air-traffic control for permission to climb from 32,000 to 38,000 feet to avoid the bad weather. Wreckage, Bodies of AirAsia Crash Found |Lennox Samuels |December 30, 2014 |DAILY BEAST 

He had not estimated that if Jean Baptiste sought his wife secretly, it must have been because he wished to avoid him. The Homesteader |Oscar Micheaux 

But I always suspected it was a stratagem on his part to avoid playing, and that nothing really ailed him. Music-Study in Germany |Amy Fay 

But it was my only chance then; or rather I had seen enough of business to avoid making mistakes when I could. Ancestors |Gertrude Atherton 

She stopped, and turned to face him, an incredible shyness seeming to cause her to avoid his gaze. St. Martin's Summer |Rafael Sabatini 

He ought not to be in London now—it is stifling—went up for some business meeting or other—seemed to wish to avoid details. Ancestors |Gertrude Atherton