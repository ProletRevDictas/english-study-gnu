Other hobbies include reading, playing chess, working out, and pretending I can cook. Meet D.C.’s Most Eligible LGBTQ Singles |Staff reports |February 11, 2021 |Washington Blade 

You have to play that chess match with the defensive coordinator and the defensive players out there on the field on how they want to defend you. Travis Kelce is the best tight end in football. Just ask any NFL player. |Adam Kilgore |February 4, 2021 |Washington Post 

A virtual robot arm has learned to solve a wide range of different puzzles—stacking blocks, setting the table, arranging chess pieces—without having to be retrained for each task. These virtual robot arms get smarter by training each other |Will Heaven |January 22, 2021 |MIT Technology Review 

If he had only played that one game, he would be famous in chess. Lubomir Kavalek, international chess grandmaster, dies at 77 |Emily Langer |January 20, 2021 |Washington Post 

This approach has led to algorithms that have independently learned to play chess at a superhuman level and prove mathematical theorems without any human guidance. How Explainable Artificial Intelligence Can Help Humans Innovate |Forest Agostinelli |January 13, 2021 |Singularity Hub 

In late April or early May 1955, Chuck approached Muddy Waters about recording, and Muddy sent him to Leonard Chess. How Rock and Roll Killed Jim Crow |Dennis McNally |October 26, 2014 |DAILY BEAST 

We thought this season it would be more fun to just start the chess game right away. ‘The Good Wife’ Creators on the Premiere’s Big Cary Twist, Will’s Death, and More |Kevin Fallon |September 22, 2014 |DAILY BEAST 

One of his most entertaining books is about Chess Records, the legendary Blues record label. The Stacks: How Leonard Chess Helped Make Muddy Waters |Alex Belth |August 2, 2014 |DAILY BEAST 

“I never during all those Chess years looked upon my father or my uncle or myself as artists,” Marshall Chess told me. The Stacks: How Leonard Chess Helped Make Muddy Waters |Alex Belth |August 2, 2014 |DAILY BEAST 

Argentina and Belgium, earlier in the day, had fought out a fascinating duel, not unlike chess on turf. Costa Rica vs. the Netherlands: A Tale of Two Goalies |Tunku Varadarajan |July 5, 2014 |DAILY BEAST 

This other by what seems a congenial activity, fascinating as a game of chess, acquires uncounted millions. The Unsolved Riddle of Social Justice |Stephen Leacock 

I dont know that I care for chess; I can not concentrate my attention as I could a year ago. Tessa Wadsworth's Discipline |Jennie M. Drinkwater 

She came to the table, set the candlestick down and looked over the chess-board. Tessa Wadsworth's Discipline |Jennie M. Drinkwater 

Then he goes in to dine and play chess with the parson, and leaves me here to watch and wait. The Nursery, August 1873, Vol. XIV. No. 2 |Various 

She felt like a chess player who, by the clever handling of his pieces, sees the game taking the course intended. The Awakening and Selected Short Stories |Kate Chopin