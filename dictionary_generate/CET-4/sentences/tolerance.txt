Dark Triad people have no tolerance for anything, or acceptance. Are You Yoda or Darth Vader? - Issue 89: The Dark Side |Brian Gallagher |August 26, 2020 |Nautilus 

Desmond said the growing number of positive cases was a good thing because healthy people would build up a tolerance to the virus and protect the vulnerable. Supervisor by Day, But a COVID-19 Skeptic on the Airwaves |Katy Stegall |August 20, 2020 |Voice of San Diego 

That comity and tolerance seems to be vanishing, with that generation of Americans replaced with a generation driven by the anger of extremists. Departure from convention—mom, baseball, the postal worker, and patriotism |jakemeth |August 19, 2020 |Fortune 

With funding from the USDA’s National Institute of Food and Agriculture, the researchers now plan to identify specific genetic markers that correlate with tolerance to tropical conditions. Biotechnology Could Change the Cattle Industry. Will It Succeed? |Dyllan Furness |August 16, 2020 |Singularity Hub 

Those genes may be behind the reptiles’ tolerance of cool temperatures, the researchers say. How tuatara live so long and can withstand cool weather |Jake Buehler |August 5, 2020 |Science News 

And yes, our values include tolerance of those who wish to make fun of religion. Ayaan Hirsi Ali: Our Duty Is to Keep Charlie Hebdo Alive |Ayaan Hirsi Ali |January 8, 2015 |DAILY BEAST 

And what do we really mean when we say “religious tolerance”? Santa Fails One More Time |P. J. O’Rourke |December 27, 2014 |DAILY BEAST 

World peace, religious tolerance, and an end to global poverty, hunger, and disease. Santa Fails One More Time |P. J. O’Rourke |December 27, 2014 |DAILY BEAST 

But religious tolerance would be a wholesome goodie for every boy and girl. Santa Fails One More Time |P. J. O’Rourke |December 27, 2014 |DAILY BEAST 

I asked for world peace, religious tolerance and an end to poverty. Santa Fails One More Time |P. J. O’Rourke |December 27, 2014 |DAILY BEAST 

Ample tolerance of all religions and sects, but abolition and expulsion of all monastic Orders. The Philippine Islands |John Foreman 

In the same session, James Madison actively participated in a discussion concerning religious liberty and tolerance. Hallowed Heritage: The Life of Virginia |Dorothy M. Torpey 

Though it fell far short of what would now be understood by tolerance, it was fully up to the level of the times. The English Church in the Eighteenth Century |Charles J. Abbey and John H. Overton 

Two wide-reaching limitations of the principle of tolerance intervened to close the gate against other Nonconformists than these. The English Church in the Eighteenth Century |Charles J. Abbey and John H. Overton 

The Brodricks seemed to tolerate their brother-in-law; and he seemed, more sublimely, to tolerate their tolerance. The Creators |May Sinclair