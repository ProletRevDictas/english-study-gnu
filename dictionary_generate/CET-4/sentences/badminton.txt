At least three Myanmar athletes—in badminton, judo and shooting—have qualified to compete in Tokyo. Myanmar's Top Swimmer Boycotts the Olympics to Protest the Military Coup—and Says the IOC Must Stop Hiding Behind Neutrality |Aria Chen |July 13, 2021 |Time 

Richard recalled playing badminton at the Boys Club in Southeast Washington. There’s a word for that. It may just not be the word you learned growing up. |John Kelly |May 5, 2021 |Washington Post 

Players’ minds wander from basketball while they take up Wiffle ball, badminton, corn hole, soccer and football, with varying degrees of skill and seriousness. Men’s Sweet 16 teams might be stuck in Indy with idle time, but they don’t want to leave. |Emily Giambalvo |March 26, 2021 |Washington Post 

Even without specific training, the self-talk patterns of runners and badminton players are consistent with the distinction between motivational and instructional self-talk. What Marathoners (and Badminton Players) Think About |Alex Hutchinson |February 26, 2021 |Outside Online 

The researchers wanted to figure out whether a computer could use machine learning to tell the difference between runners and badminton players based only on the content of their self-talk. What Marathoners (and Badminton Players) Think About |Alex Hutchinson |February 26, 2021 |Outside Online 

His exposé of the sinister badminton scandal was an instant classic. John Avlon’s Picks for 12 Best Opinion Columns of 2012 |John Avlon |December 31, 2012 |DAILY BEAST 

I play badminton with a bunch of dachshunds yipping at my heels. Gary Shteyngart: How I Write |Noah Charney |December 26, 2012 |DAILY BEAST 

As to why badminton is largely ignored, or even laughed, at in the US, Jiang threw the question right back. Olympics Badminton Scandal Rocks China |Dan Levin |August 2, 2012 |DAILY BEAST 

But this Olympics, badminton is making headlines for all the wrong reasons. Olympics Badminton Scandal Rocks China |Dan Levin |August 2, 2012 |DAILY BEAST 

Over the last decade, the two countries have engaged in “wrestling diplomacy,” “soccer diplomacy,” even “badminton diplomacy.” Ahmadinejad Tangles With Hollywood |Reza Aslan |March 2, 2009 |DAILY BEAST 

His household at Badminton was regulated after the fashion of an earlier generation. The History of England from the Accession of James II. |Thomas Babington Macaulay 

The same may be said of badminton, another favorite Lenten game, played somewhat after the manner of tennis. The Complete Bachelor |Walter Germain 

The “Badminton Library,” an English series of books on sport, is at a huge premium already, when on “large paper.” Letters on Literature |Andrew Lang 

At first he fell into profound slumber: it was the inevitable result of the Badminton and the late hour. Lothair |Benjamin Disraeli 

The directions for playing them may be found in Billiards (Badminton Library series). Encyclopaedia Britannica, 11th Edition, Volume 3, Slice 7 |Various