For example, if you’re skilled in Photoshop and you meet someone who is a genius with video editing, you all could agree to use your talents to help the other. Networking 101: Why Working Together Creates More Opportunity Than Working Apart |Shantel Holder |September 4, 2020 |Essence.com 

He wasn’t some sort of genius, but he was good at reading people in terms of who was vulnerable, what their vulnerabilities were. “People want to believe”: How Love Fraud builds an absorbing docuseries around a romantic con man |Alissa Wilkinson |September 4, 2020 |Vox 

A large part of the impetus to start Camelback was just this feeling that we have a genius in our communities of color and we’re not necessarily leveraging those for social change. How to Confront Racism in Philanthropy |Joshua Eferighe |August 24, 2020 |Ozy 

We have a genius in our communities of color and we’re not necessarily leveraging those for social change. How to Confront Racism in Philanthropy |Joshua Eferighe |August 24, 2020 |Ozy 

We’re not all geniuses with Einstein-like hair that are antisocial and just have Eureka moments all the time. Real-life scientists inspire these comic book superheroes |Kyle Plantz |June 14, 2020 |Science News 

Bonauto, now an official MacArthur genius, is rightly known as the Thurgood Marshall of the marriage movement. The Real Story Behind the Fight for Marriage Equality |E.J. Graff |December 30, 2014 |DAILY BEAST 

And this song is just absolute genius and totally universal. Yes, I Like Christmas Music. Stop Laughing. |Michael Tomasky |December 24, 2014 |DAILY BEAST 

But the comedic genius was wrong; success in most dimensions of the human enterprise is showing up at the right time. Why We Should Delay The Israel-Palestinian Peace Process |Aaron David Miller |December 19, 2014 |DAILY BEAST 

The problem is, how do you find a movie narrative that can explain genius, British or otherwise? Why Can’t Movies Capture Genius? |Clive Irving |December 14, 2014 |DAILY BEAST 

In that case the device was to put the genius in opposition to a majority of established cultural tastes and codes. Why Can’t Movies Capture Genius? |Clive Irving |December 14, 2014 |DAILY BEAST 

God has placed the genius of women in their hearts; because the works of this genius are always works of love. Pearls of Thought |Maturin M. Ballou 

When Yima pressed the earth with this ring, the genius of the Earth, Aramaîti, responded to his wish and order. Solomon and Solomonic Literature |Moncure Daniel Conway 

Even genius, however, needs direction and adjustment to secure the most perfect and reliable results. Expressive Voice Culture |Jessie Eldridge Southwick 

The universal ignorance of the working class broke down the aspiring force of genius. The Unsolved Riddle of Social Justice |Stephen Leacock 

However, I have felt some comfort in knowing that it is not Liszt's genius alone that makes him such a player. Music-Study in Germany |Amy Fay