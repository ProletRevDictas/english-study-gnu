We could have looped back, but instead followed a separate trail to Pounds Hollow Lake. A day’s drive from Chicago, exploring a very different Illinois |Carson Vaughan |February 12, 2021 |Washington Post 

Here again, that only serves to further separate the haves from the have-nots. Don’t hate the vaccine tourists, hate the vaccine game |Jen Kirby |February 12, 2021 |Vox 

The two were separated, and Ujiri eventually joined the team and did a TV interview. Raptors president, sheriff’s deputy drop lawsuits over shoving incident at NBA Finals |Cindy Boren |February 12, 2021 |Washington Post 

At a time when we need whatever unity we can find and sports might be one place to find it, the anthem could be a two-minute span when we agree that we’re all Americans, that we should be together rather than separate. The pregame national anthem — in all its roiling contradictions — still has something to offer |Barry Svrluga |February 11, 2021 |Washington Post 

The result was the Ignite, a separate team of elite prospects, surrounded by handpicked veterans, that has no affiliation with an NBA franchise. An NBA experiment lets draft prospects skip college, stay home and get paid to play |Michael Lee |February 11, 2021 |Washington Post 

There is, however, a separate wing of AQAP designed to inspire their followers to conduct attacks against the West. U.S. Spies See Al Qaeda Fingerprints on Paris Massacre |Shane Harris, Nancy A. Youssef |January 8, 2015 |DAILY BEAST 

My younger, straighter-than-an-arrow son was stopped and arrested in two separate jurisdictions a few years ago. What Would Happen if I Got in White Cop’s Face? |Goldie Taylor |December 30, 2014 |DAILY BEAST 

We separate the search for justice from the search for truth at our peril. Bill de Blasio’s Tea Party Problem |Will Cain |December 30, 2014 |DAILY BEAST 

“I never felt that culture and the arts were separate from politics,” he says. DJ Spooky Wants You To Question Everything You Know About Music, Technology, and Philosophy |Oliver Jones |December 27, 2014 |DAILY BEAST 

“She was hot-headed, had her own way of doing things,” Gill said—and so, he left to form a separate militia group. Open-Carry Militia Mom ‘Murders’ Family |Brandy Zadrozny |December 16, 2014 |DAILY BEAST 

It was an error not to separate borrowing entirely from monetary issues. Readings in Money and Banking |Chester Arthur Phillips 

He devoured it whole with a kind of visual gulp—a flash; the entire meaning first, then lines, then separate words. The Wave |Algernon Blackwood 

By a device resorted to in each separate case to help make a more vivid First Impression. Assimilative Memory |Marcus Dwight Larrowe (AKA Prof. A. Loisette) 

It has one separate room where poor Spanish women are treated, which generally has from twelve to twenty women. The Philippine Islands, 1493-1898, Volume XX, 1621-1624 |Various 

Therefore, every piece had its own separate voice in exact proportion to the amount of trouble spent upon it. Kipling Stories and Poems Every Child Should Know, Book II |Rudyard Kipling