The only sound was from the workmen above handling the ropes and yelling “shweya” — slowly. In the tombs of Saqqara, new discoveries are rewriting ancient Egypt’s history |Sudarsan Raghavan |April 22, 2021 |Washington Post 

Around it some barefoot workmen were operating power tools without any protective gear. Netflix v Modi and the battle for Indian cinema’s soul |Konstantin Kakaes |March 24, 2021 |MIT Technology Review 

In the special, Workman plays the old man who, as a cabin boy, watched the pirates bury their treasure. Garfield Television: The Cat Who Saved Primetime Cartoons |Rich Goldstein |November 5, 2014 |DAILY BEAST 

One of the first, Fanny Bullock Workman, was the daughter of a Massachusetts governor. Breaking Mount Everest’s Glass Ceiling |Amanda Padoan, Peter Zuckerman |March 30, 2014 |DAILY BEAST 

For almost a century, women like Workman continued to establish themselves in the sport, but Everest eluded them. Breaking Mount Everest’s Glass Ceiling |Amanda Padoan, Peter Zuckerman |March 30, 2014 |DAILY BEAST 

Representative Workman might also consider that moral conduct guides many laws. Dwarf Tossing Should Be Illegal |Bill Klein |October 23, 2011 |DAILY BEAST 

If Representative Workman wanted to serve his constituency well, he would look to create jobs for the masses. Dwarf Tossing Should Be Illegal |Bill Klein |October 23, 2011 |DAILY BEAST 

Now for the tempering of the Gudgeons, I leave it to the judgment of the Workman; but a word or two of the polishing of it. Tintinnalogia, or, the Art of Ringing |Richard Duckworth and Fabian Stedman 

Is a Tailor, that can make a new Coat well, the worse Workman, because he can mend an old one? A Letter from Mr. Cibber to Mr. Pope |Colley Cibber 

He was, as has been said, an unusually good workman, consequently his employers had no wish to part with him. Asbestos |Robert H. Jones 

He hath chosen strong wood, and that will not rot: the skilful workman seeketh how he may set up an idol that may not be moved. The Bible, Douay-Rheims Version |Various 

He was slight, wore a workman's overall suit, and he had a lunch box under his arm. Hooded Detective, Volume III No. 2, January, 1942 |Various