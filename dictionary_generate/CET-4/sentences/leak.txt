Filipino YouTuber Tech Buff brings us an exclusive leak of the device, which has some pixels where there are not normally pixels. New Xiaomi smartphone has an extra screen in… uh, the camera bump? |Ron Amadeo |February 12, 2021 |Ars Technica 

Good filtration removes as many particles as possible, and a good fit means that there are no leaks around the sides of your mask, where air — and viruses — can leak through. Making masks fit better can reduce coronavirus exposure by 96 percent |Tina Hesman Saey |February 12, 2021 |Science News 

If someone was eating cauliflower near a gas leak, people would die before anyone realized they were in danger. This cauliflower hater likes the vegetable one way only: Pickled |Jim Webster |February 3, 2021 |Washington Post 

We've discussed this canceled Xbox 360 game at length over the years, with updates coming mostly in the form of increasingly detailed leaks—and this week's is easily the biggest yet. Goldeneye 007’s lost Xbox 360 remaster has leaked—as a full-game speedrun |Sam Machkovech |February 1, 2021 |Ars Technica 

In the decade since, there have been no large-scale leaks about the residence. The Inside Story of How Alexey Navalny Uncovered Putin's $1.3 Billion Palace |Madeline Roache |January 29, 2021 |Time 

Targeting pods can bulge out a bit, and leak out unwanted signals. Newest U.S. Stealth Fighter ‘10 Years Behind’ Older Jets |Dave Majumdar |December 26, 2014 |DAILY BEAST 

The leak suggests that Mr. Obama remains blind to the principal cause of his foreign policy woes. Before Ditching His Top Aides, Obama Should Look in the Mirror |Leslie H. Gelb |November 2, 2014 |DAILY BEAST 

They simply would not leak this shocking story about big lineup changes on their own accord. Before Ditching His Top Aides, Obama Should Look in the Mirror |Leslie H. Gelb |November 2, 2014 |DAILY BEAST 

At its core, the tale revealed by the leak of what may be more than a million classified documents is a complicated one. ‘Citizenfour’ Is Mesmerizing (If You Don’t Mind the Omissions) |Michael Cohen |October 20, 2014 |DAILY BEAST 

Local mechanics pitched in to help mend the craft, but weeks into setting off the repairs wore thin and the vessel sprung a leak. Victor Mooney’s Epic Adventure for His Dead Brother |Justin Jones |October 19, 2014 |DAILY BEAST 

Their sin began on Holy Thursday, with so little secrecy and so bad an example, that the affair was beginning to leak out. The Philippine Islands, 1493-1898, Volume XX, 1621-1624 |Various 

Your composing-room door is locked, and the present item of news destined for your readers is not likely to leak out. The Weight of the Crown |Fred M. White 

De slave houses wus warm and really dey wus good houses, an' didn't leak neither. Slave Narratives: a Folk History of Slavery in the United States |Various 

Ash pits near a house carry moisture to walls, Cesspools leak through the soil. Essays In Pastoral Medicine |Austin Malley 

Here sit up to the chin for twenty minutes, shivering at thought of what would happen supposing bath sprang a leak. Punch, or the London Charivari, Vol. 93, September 24, 1887 |Various