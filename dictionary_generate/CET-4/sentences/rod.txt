Adjust the resistance of these ankle weights by adding or removing the iron rods, which total between three to five pounds. The best ankle weights for a sculpted lower body |PopSci Commerce Team |September 1, 2020 |Popular-Science 

One of the darkest incidents of last year’s protests took place on July 21, when dozens of thugs armed with sticks and metal rods launched an indiscriminate attack on civilians at the suburban subway station of Yuen Long. Hong Kong police are rewriting the history of an infamous thug attack on civilians |Mary Hui |August 27, 2020 |Quartz 

Since then, Shapery has cited his partnership with Doug Manchester, a political lightning rod and donor to the mayor, as a major obstacle. The Deal Before the 101 Ash St. Debacle Helps Explain How We Got Here |Lisa Halverstadt and Jesse Marx |August 24, 2020 |Voice of San Diego 

The retina’s rod cells aren’t part of the cone coloring system. Explainer: How our eyes make sense of light |Tina Hesman Saey |July 16, 2020 |Science News For Students 

They are so sensitive that a rod cell can detect a single photon of light — the smallest possible particle. Explainer: How our eyes make sense of light |Tina Hesman Saey |July 16, 2020 |Science News For Students 

So I asked the driver to honk the horn, which he does, and Rod looks over. The Story Behind Lee Marvin’s Liberty Valance Smile |Robert Ward |January 3, 2015 |DAILY BEAST 

And of course, Rod, being Rod, goes for it a hundred percent; his mouth drops open and he says, ‘What?’ The Story Behind Lee Marvin’s Liberty Valance Smile |Robert Ward |January 3, 2015 |DAILY BEAST 

Rod Stewart and Diane Sawyer This is just highly entertaining. The Most WTF Covers of ‘Baby, It’s Cold Outside,’ Everyone’s Favorite Date-Rape Holiday Classic |Kevin Fallon |November 19, 2014 |DAILY BEAST 

Creator Rod Serling was compelled by the need “not to just entertain but to enlighten.” How a War-Weary Vet Created ‘The Twilight Zone’ |Rich Goldstein |November 13, 2014 |DAILY BEAST 

The painting is of a human heart set inside a wind-up music box that has a metal rod poking out of the pulmonary artery. Trading Dime Bags for Salvador Dali |Jason McGahan |October 19, 2014 |DAILY BEAST 

And there shall come forth a rod out of the root of Jesse, and a flower shall rise up out of his root. The Bible, Douay-Rheims Version |Various 

A connecting rod worked a balance-beam, which worked the air-pump, feed-pump, and plug-rod for moving the valves. Life of Richard Trevithick, Volume II (of 2) |Francis Trevithick 

A piece of iron sticking out from the cross-head carried the plug-rod for working the gear-handles. Life of Richard Trevithick, Volume II (of 2) |Francis Trevithick 

Thereafter we were buffeted like chips in the swirling maw of a whirlpool; we fought our way rod by rod. Raw Gold |Bertrand W. Sinclair 

This is a minute, slender rod, which lies within and between the pus-corpuscles (Fig. 125), and is negative to Gram's stain. A Manual of Clinical Diagnosis |James Campbell Todd