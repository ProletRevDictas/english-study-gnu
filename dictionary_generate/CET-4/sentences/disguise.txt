He mentioned on Football Night in America that losing Dak might be a “blessing in disguise.” NFL Week 5 Upsets Were Overshadowed By Dak Prescott’s Awful Injury |Sara Ziegler (sara.ziegler@fivethirtyeight.com) |October 12, 2020 |FiveThirtyEight 

The historian Kim Phillips-Fein thinks this may be at least a small blessing in disguise. Is New York City Over? (Ep. 434) |Stephen J. Dubner |October 8, 2020 |Freakonomics 

A new normal of a mere 100 guests, to begin with, was not something many couples could have imagined, but is a change that they see as a blessing in disguise. Coronavirus has made it easier for millennials to scale back the big fat Indian wedding |Manavi Kapur |September 18, 2020 |Quartz 

In August, Logically helped alert Utah officials to the fact that an anti-sex-trafficking event was actually a QAnon march in disguise, leading to its permit being revoked. American Fringes: How the Media Makes it Worse |Nick Fouriezos |September 7, 2020 |Ozy 

In that sense, the reduced supply of khat — even without a formal ban — has been a blessing in disguise, says Somali anti-khat campaigner Abukar Awale, who is backing a petition to Somalia’s government that seeks to make the drug illegal. How the Pandemic Is Saving Lives in the Horn of Africa |Eromo Egbejule |September 3, 2020 |Ozy 

And Pope Alexander VI had the painter Pinturicchio disguise his mistress as the Virgin Mary in one fresco. Great Renaissance Art Thrived Amid Filth |Nick Romeo |December 3, 2014 |DAILY BEAST 

Tumid and unstoppable, there is little that new wallpaper or re-poured driveways can do to disguise it. Silicon Valley Mansions, Swallowed Alive |Geoff Manaugh |November 8, 2014 |DAILY BEAST 

Maybe, then, the Hathahate phenomenon is a blessing in disguise. Do We Still Hate Anne Hathaway? |Kevin Fallon |November 5, 2014 |DAILY BEAST 

Roberts and the Republicans are trying to portray the independent as a Barack Obama supporter who is just a Democrat in disguise. The Independents Who Could Tip the Senate in November |Linda Killian |October 13, 2014 |DAILY BEAST 

Rivers, it had emerged, had told them she was Ruth Madoff in disguise, and not to speak to her or approach her when she walked in. I Was There: Inside Joan Rivers’ Funeral |Tim Teeman |September 8, 2014 |DAILY BEAST 

Napoleon landed at Elba at an early hour in disguise, with a sergeant's company of marines. The Every Day Book of History and Chronology |Joel Munsell 

It was a habit with him to disguise himself in ordinary clothing and then to go out and mingle with the common people. Our Little Korean Cousin |H. Lee M. Pike 

In short, I shall begin life all over again—as if I were a criminal in disguise instead of the sport of circumstances. Ancestors |Gertrude Atherton 

But then who is there that can bear so total a disguise as filth and untidiness spread over a woman? Journal of a Voyage to Brazil |Maria Graham 

Isn't that Squid Murphy over there in the corner, trying to disguise himself as a corner of that safe? Hooded Detective, Volume III No. 2, January, 1942 |Various