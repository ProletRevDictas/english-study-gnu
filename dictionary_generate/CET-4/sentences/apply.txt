My daughter uses the disposable gloves when she applies oily hand cream to her own hands. Hints From Heloise: New scam tied to coronavirus pandemic |Heloise Heloise |February 12, 2021 |Washington Post 

So there’s Wilson, stirring up drama this week in an interview with Dan Patrick, detailing the nearly 400 sacks he has taken in his first nine seasons and applying pressure on the Seahawks in an uncharacteristic manner. The NFL’s top QBs are waking up to their power, following Tom Brady and LeBron James |Jerry Brewer |February 11, 2021 |Washington Post 

This can be accomplished by the same pauses applied on a larger scale — namely, calls less frequently returned and finding that you have less time to spend with her. Miss Manners: Time to cut ties with longtime friend? |Judith Martin, Nicholas Martin, Jacobina Martin |February 11, 2021 |Washington Post 

The Cavaliers continued to apply pressure on both ends of the court. Virginia pulls away from Georgia Tech, tightens grip on first place in ACC |Gene Wang |February 11, 2021 |Washington Post 

Bidding strategies are also available at a portfolio level, allowing you to apply machine learning to optimize all of your campaigns. Smart Bidding: Five ways machine learning improves PPC advertising |Gabrielle Sadeh |February 10, 2021 |Search Engine Watch 

She began teaching herself how to apply makeup through books and videos. Inside A Finishing School for Transwomen |Sharon Adarlo |December 27, 2014 |DAILY BEAST 

And if he is re-elected, the House advisory rules prohibiting him from voting no longer apply. The Felon Who Wouldn’t Leave Congress |Ben Jacobs, David Freedlander |December 23, 2014 |DAILY BEAST 

His words apply not only to the Roman Curia at the Vatican but to the entire Church throughout the world. Pope Francis Denounces the Vatican Elite’s 'Spiritual Alzheimer’s' |Barbie Latza Nadeau |December 23, 2014 |DAILY BEAST 

The idea was to identify what they are and apply them to different snacks, beverages, and foods. The Science of Ingredient Innovation | |December 15, 2014 |DAILY BEAST 

Based on our conversation, he decided to do more research and apply to at least one small selective college. Forget the Kids Who Can’t Get In; What About Those Who Don’t Even Apply? |Jonah Edelman |December 9, 2014 |DAILY BEAST 

This, however, did not apply to the waters lying directly around the Poloe and Flatland groups. The Giant of the North |R.M. Ballantyne 

We may apply to it with advantage the spectacles of social reform, but what the socialist offers us is total blindness. The Unsolved Riddle of Social Justice |Stephen Leacock 

But the hours are made longer or shorter according to whether too many or too few young people apply to come in. The Unsolved Riddle of Social Justice |Stephen Leacock 

Let dry, apply a cover-glass, and run glacial acetic acid underneath it. A Manual of Clinical Diagnosis |James Campbell Todd 

This rule however does not apply to travelers walking along a rural highway. Putnam's Handy Law Book for the Layman |Albert Sidney Bolles