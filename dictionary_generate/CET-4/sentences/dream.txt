His duet with Egyptian singer Mohammed Ramadan, released four years on, racked up 100 million views in a month — numbers that American superstars dream of. The Biggest Challenge for Apple and Spotify in North Africa: YouTube |Eromo Egbejule |September 17, 2020 |Ozy 

We all know times are incredibly tough, and everyone’s working overtime on steroids to keep their startup dreams alive. TechCrunch still brings the fun to Disrupt 2020 |Alexandra Ames |September 11, 2020 |TechCrunch 

This book is so meaningful to me, and I recommend everyone read it—especially young women who may be unsure of decisions you are facing and how they may impact your ability to reach your dreams. Book recommendations from Fortune’s 40 under 40 in government and policy |Rachel King |September 10, 2020 |Fortune 

It’s not clear how much he’s paying for the trip, but one has to assume it’s more than most of us could ever dream of making in our lifetimes. When will we see ordinary people going into space? |Neel Patel |September 9, 2020 |MIT Technology Review 

Many dreamed that instant access to information would improve our ability to discern truth from lies and good from bad. The race for a COVID-19 vaccine shows the power of ‘community intelligence’ |matthewheimer |September 9, 2020 |Fortune 

I was drawn to The Class for different reasons—chiefly, the pipe dream of achieving a tighter and tauter backside. How Taryn Toomey’s ‘The Class’ Became New York’s Latest Fitness Craze |Lizzie Crocker |January 9, 2015 |DAILY BEAST 

The Eighty-ninth Congress was potentially more fertile ground for the broad range of controversial programs on his dream agenda. Thank Congress, Not LBJ for Great Society |Julian Zelizer, Scott Porch |January 4, 2015 |DAILY BEAST 

I fall back into a dream and then suddenly there is a tapping on the window just above my bed. The Story Behind Lee Marvin’s Liberty Valance Smile |Robert Ward |January 3, 2015 |DAILY BEAST 

So where does this leave the millions of Palestinians—like my relatives—who dream of self-determination and a sovereign state? In the Middle East, the Two-State Solution Is Dead |Dean Obeidallah |January 2, 2015 |DAILY BEAST 

And for those on the Palestinian right who still dream of driving the Jews into the sea, they too can forget it. In the Middle East, the Two-State Solution Is Dead |Dean Obeidallah |January 2, 2015 |DAILY BEAST 

She would never forget it; but realizing its gravity, she decided thereupon never to tell it—the dream—to anybody. The Homesteader |Oscar Micheaux 

Little did Tressan dream to what a cask of gunpowder he was applying the match of his smug pertness. St. Martin's Summer |Rafael Sabatini 

She was in a dream of oily odours and monstrous iron constructions, dominated by the grand foreman: and Edwin was in the dream. Hilda Lessways |Arnold Bennett 

As the devil never wanted insinuators, I shall observe, that I learned a way how to make a man dream of what I pleased. The Life and Most Surprising Adventures of Robinson Crusoe, of York, Mariner (1801) |Daniel Defoe 

Alice Arden, you little dream of the man and the route by which, possibly, deliverance is speeding to you. Checkmate |Joseph Sheridan Le Fanu