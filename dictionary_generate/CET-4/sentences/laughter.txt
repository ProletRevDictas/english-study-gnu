“We continue to need art and connection and laughter, so we have an imperative to try to think safely about it,” said Liz Fehrenbach, program director at Joseph’s House. Parents are going the extra mile to save Halloween for their kids |Emily Davies |October 29, 2020 |Washington Post 

In my study of dog play, I shadowed dogs with a video camera rolling, and controlled my own delighted laughter at their fun long enough to record bouts of play, from a few seconds to many minutes long. Forget Everything You Know About Your Dog (Ep. 436) |Stephen J. Dubner |October 22, 2020 |Freakonomics 

His laughter punctuates the conversation from behind a closed door. She Was Afraid of Her Lawyer. Then the Text Messages Started. |by Samantha Hogan, The Maine Monitor |October 8, 2020 |ProPublica 

The shock — both at the crash and at the sudden cut — drives our laughter. One Good Thing: The trick uniting the internet’s funniest videos |Emily VanDerWerff |September 25, 2020 |Vox 

She even resorted to removing her clothes, which led to amusement among the other gods who started roaring in joy and laughter. 12 Major Japanese Gods and Goddesses You Should Know About |Dattatreya Mandal |May 6, 2020 |Realm of History 

May their married life have laughter, and that they love one another forever after! All Your Internet Boyfriends Are Taken: Gosling, Cumberbatch, and now Joseph Gordon-Levitt |Melissa Leon |January 3, 2015 |DAILY BEAST 

If laughter is the best medicine, The Comeback made you feel enough pain to need a dose—and then it delivered in spades. ‘The Comeback’ Finale: Give Lisa Kudrow All of the Awards |Kevin Fallon |December 29, 2014 |DAILY BEAST 

Under the Sun King, such humor—and the laughter associated with it—was seen as more suitable for the masses. The French Court’s Royal Ban on Smiles |William O’Connor |December 14, 2014 |DAILY BEAST 

“Robin had us blubbing with laughter all the way through one dinner,” Gilkes recalls. William, Kate, and Jay Z’s Favorite Art Star: Alexander Gilkes' World of Rock Stars and Royalty |Tim Teeman |December 10, 2014 |DAILY BEAST 

Obama said, through laughter, according to an eyewitness report of the meeting in The Telegraph. When Your Royalty Met Ours: Kate Meets Bey Courtside |Tom Sykes |December 9, 2014 |DAILY BEAST 

His books were read in our homes, often aloud to the family circle by paterfamilias, and moved us to laughter or tears. Fifty Years of Railway Life in England, Scotland and Ireland |Joseph Tatlow 

This was said with a comical air of doubt, and a half smile, which sent a ripple of laughter over the charming face. The World Before Them |Susanna Moodie 

After supper, stories were told and the laughter, which was loud and long, lasted far into the night. Fifty Years of Railway Life in England, Scotland and Ireland |Joseph Tatlow 

“It would cost the blood and tears and laughter of the human race,” said Aristide. The Joyous Adventures of Aristide Pujol |William J. Locke 

Her chum leaned against the door jamb while peal after peal of laughter shook her. The Campfire Girls of Roselawn |Margaret Penrose