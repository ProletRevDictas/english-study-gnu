Stewart and Carpenter are locked in a contentious battle for the District 2 Senate seat in Aroostook County. Maine Hires Lawyers With Criminal Records to Defend Poor Residents. The Governor Wants Reform. |by Samantha Hogan, The Maine Monitor |October 14, 2020 |ProPublica 

Both cows and carpenter ants, for example, rely on bacterial partners in their digestive systems to help them get the most out of their food. How Two Became One: Origins of a Mysterious Symbiosis Found |Viviane Callier |September 9, 2020 |Quanta Magazine 

In adult carpenter ants, the bacteria produce essential amino acids and play important roles in immunity. How Two Became One: Origins of a Mysterious Symbiosis Found |Viviane Callier |September 9, 2020 |Quanta Magazine 

When the McGill researchers scrutinized the development of carpenter ant embryos, however, they saw to their surprise that Hox proteins were appearing during the first nuclear divisions — far ahead of schedule. How Two Became One: Origins of a Mysterious Symbiosis Found |Viviane Callier |September 9, 2020 |Quanta Magazine 

We had a few old-timers, guys in their seventies—a classic carpenter, an arborist, and a Vietnam vet—and these guys just wouldn’t back down. How We Saved Our Neighborhood from a Wildfire |Dain Zaffke |September 2, 2020 |Outside Online 

Sands was involved in a scandalous-for-the-time romance with the carpenter and there were rumors she was pregnant with his child. New York’s Most Tragic Ghost Loves Minimalist Swedish Fashion |Nina Strochlic |January 8, 2015 |DAILY BEAST 

The model: a carpenter who showed up to her house looking for work. Blessed or Cursed? Child Prodigies Reveal All |Justin Jones |November 17, 2014 |DAILY BEAST 

He had been a Union carpenter for 30 years before retiring and devoting himself to nature photography. He Faces Jail for Rescuing Baby Eagles |Michael Daly |November 2, 2014 |DAILY BEAST 

The singular author Don Carpenter took his own life in 1995. Don Carpenter Was a Novelist Both Lacerating and Forgiving |Louis B. Jones |July 14, 2014 |DAILY BEAST 

When a female candidate slips up, she should respond succinctly and then introduce third-party validators, said Carpenter. The Gubernatorial Glass Ceiling—and What It Means for Hillary Clinton |Eleanor Clift |June 5, 2014 |DAILY BEAST 

Carpenter were the leaders, and this is claimed to have been the origin of Mechanics' Institutes. Showell's Dictionary of Birmingham |Thomas T. Harman and Walter Showell 

American writers claim that the first pressed glass tumbler was made about 40 years back in that country, by a carpenter. Showell's Dictionary of Birmingham |Thomas T. Harman and Walter Showell 

Strachan departed highly elated, and repaired to a carpenter shop, where he ordered ten rough coffins made. The Courier of the Ozarks |Byron A. Dunn 

The chief ambition of the great conqueror and legislator was to be a good boatswain and a good ship's carpenter. The History of England from the Accession of James II. |Thomas Babington Macaulay 

The red comb on the top of his head has teeth like a carpenter's saw, and is so large it will not stand up straight. Seven O'Clock Stories |Robert Gordon Anderson