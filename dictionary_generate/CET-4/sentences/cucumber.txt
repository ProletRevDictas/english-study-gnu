In a large bowl, toss the watermelon and cucumber until combined. Watermelon and cucumber salad is as refreshing as it is easy to make |Ellie Krieger |August 26, 2021 |Washington Post 

Ruiz includes slices of cucumber in his tiritas, for extra crunch, and a bit of olive oil, which brings out the silkiness of the fish. Fresh fish tiritas are a faster way to ceviche |Daniela Galarza |August 26, 2021 |Washington Post 

It produces quarter-sized blue flowers that connoisseurs say taste a bit like cucumber. 7 edible flowers and how to use them |John Kennedy |July 20, 2021 |Popular-Science 

Then, pile on thinly sliced or matchstick-cut cucumbers, roasted peanuts, sliced scallions and lots of fresh cilantro. This take on bang bang chicken is spicy, sour and quick enough for a weeknight |Daniela Galarza |July 8, 2021 |Washington Post 

If you are making granita out of, say, watermelon or cucumber, neither of which is a particular fibrous specimen, you can puree them directly. This Summer, Learn to Make a Perfect Granita |Emma Wartzman |July 1, 2021 |Eater 

Try explaining what a cucumber tastes like to a 19th century Inuit. Eben Alexander Has a GPS for Heaven |Patricia Pearson |October 8, 2014 |DAILY BEAST 

Together, we decide that the drink should be called a Cucumber Superlative. The Absinthe-Minded Porteños of Buenos Aires |Jeff Campagna |March 10, 2014 |DAILY BEAST 

Allergies—Mix one part cucumber juice with one part beet root juice and three parts carrot juice. Use These 15 Home Remedies Based On Ayurveda To Cure Menstrual Cramps, Hangovers, and Indigestion |Ari Meisel |January 21, 2014 |DAILY BEAST 

“That scallop dish is just a raw scallop with a grilled cucumber and cucumber vinaigrette,” said McGarry. Meet Flynn McGarry: America’s Next Great Chef Is 14 Years Old |Jace Lacob |May 23, 2013 |DAILY BEAST 

Basil, cucumber, mangoes, the cooing of turtledoves on torrid afternoons, the screech of buses coming to a sudden halt. André Aciman: How I Write |Noah Charney |November 28, 2012 |DAILY BEAST 

Scarcely less inspiring were the cucumber-trees, or mountain magnolias, which here reached the perfection of growth. A Virginia Scout |Hugh Pendexter 

Ornament the top with chopped aspic and alternate slices of lemon and cucumber round. Dressed Game and Poultry la Mode |Harriet A. de Salis 

Arrange the pieces of rabbit in a circle, put the cucumber in the middle, and pour the sauce over the fillets. Dressed Game and Poultry la Mode |Harriet A. de Salis 

Make a sauce of white stock, and put the pieces of rabbit into it with the cucumber until it is quite done. Dressed Game and Poultry la Mode |Harriet A. de Salis 

Encouraged by his success, Cucumber fell to capering about in a squatting position, singing the refrain of: 'Shildi-budildi!' A Desperate Character and Other Stories |Ivan Turgenev