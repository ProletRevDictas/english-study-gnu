It isn’t fair to ask groups offended by these symbols to wait even longer for change. ‘End racism,’ the NFL implored. So what about that Chiefs’ name? |Liz Clarke |February 5, 2021 |Washington Post 

I would like to take back what I said, and apologize to those who were offended by my remarks. Tokyo Olympics chief says women talk too much at meetings, calls it ‘annoying’ |Matt Bonesteel |February 4, 2021 |Washington Post 

That begins with its worst-offending units, the gas-fired plants known as “peakers,” turned on only to give the electric grid a boost on hot, or “peak,” days. At New York City’s biggest power plant, a switch to clean energy will help a neighborhood breathe easier |Andrew Blum |February 2, 2021 |Popular-Science 

I feel like we’re in a climate where you don’t want to say things to offend people since we’ve not seen anything like this in our lifetime. After Capitol attack, social studies and civics teachers struggle with real-time history lessons |Joe Heim, Valerie Strauss |January 19, 2021 |Washington Post 

“I thought it was a lot and I thought it was unnecessary, but I think it’s important that I keep my opinions to myself because it’s not going to be pleasant,” she said, indicating that she was personally offended by Adefeso’s actions. Trina Braxton To David Adefeso: ‘When You Attack One, You Attack Us All’ |Hope Wright |September 17, 2020 |Essence.com 

And could the word "Russkiy," or 'Russian', offend ethnically non-Russian citizens around the country? Rebranding The Land of Mongol Warriors & Ivan The Terrible |Anna Nemtsova |December 25, 2014 |DAILY BEAST 

Worried this might turn people off from his performance, Khan said he didn't intend  to offend anyone. Defying Stereotypes, Young Muslim Writers Find Community Onstage |Julianne Chiaet |October 12, 2014 |DAILY BEAST 

But Alex Rubin cannot afford to offend any of the media willing to cover safely dead dissidents. From Moscow to Queens, Down Sergei Dovlatov Way |Daniel Genis |September 15, 2014 |DAILY BEAST 

Some steps would offend some Republicans, such as restarting a modern version of the Depression era Works Progress Administration. Class Issues, Not Race, Will Likely Seal the Next Election |Joel Kotkin |September 7, 2014 |DAILY BEAST 

“This show is not an exercise to offend people,” McGruder countered. ‘Black Jesus’ Resurrected: Racial Stereotypes or Subversive Comedy? |Rawiya Kameir |August 8, 2014 |DAILY BEAST 

You need not fear to offend by refusing to take wine with a gentleman, even your host. The Ladies' Book of Etiquette, and Manual of Politeness |Florence Hartley 

Francoeur was afraid of nothing, but he was old; his heart like his head was polished by age, and he disliked to offend people. Honey-Bee |Anatole France 

Some offend because they crave popularity or want to do what their friends are doing. Report of the Special Committee on Moral Delinquency in Children and Adolescents |Oswald Chettle Mazengarb et al. 

Ruth was neither impudent nor hardened; she was ignorant enough, and might offend from knowing no better. Ruth |Elizabeth Cleghorn Gaskell 

He therefore belaboured him till his sullen obstinacy gave way to a roar for mercy, and promises never so to offend again. Eric, or Little by Little |Frederic W. Farrar