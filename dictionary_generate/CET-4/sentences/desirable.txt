The reason an injunction is so desirable for law enforcement is that the police don’t have the resources to pursue gang members when they commit bona fide crimes. While We’re Rethinking Policing, It’s Time to End Gang Injunctions |Jamie Wilson |September 15, 2020 |Voice of San Diego 

“Legislation like this rarely takes into account the ripple effect on all workers of the impact on the employer, which can result in outcomes that are less desirable than intended,” Walshok said. City to Weigh Measure Giving Laid-Off Hotel Workers First Shot at Open Jobs |Maya Srikrishnan |September 8, 2020 |Voice of San Diego 

Many people, including scientists, are concerned that rogue scientists wouldn’t stop at editing out diseases and would create “designer babies” with enhanced athletic ability, intelligence or other desirable traits. Strict new guidelines lay out a path to heritable human gene editing |Tina Hesman Saey |September 3, 2020 |Science News 

A raised, sloped area—where the keys are broken up by the triangular shape—helps create a more desirable, comfortable position for your hands. Serious upgrades for your computer keyboard |PopSci Commerce Team |September 2, 2020 |Popular-Science 

The piece, which argues the pandemic has forever made cities like New York less desirable, got a ton of pushback. Are cities really on the edge of a mass exodus? |Lance Lambert |September 1, 2020 |Fortune 

The more expensive it becomes, the better it must be, which in turn makes it more desirable. The Cult of Pappy van Winkle |Eric Felten |December 3, 2014 |DAILY BEAST 

That seems to be the secret to creating a successful, desirable, and wearable device. Wearables Hit the Runway at Fashion Week |Raquel Laneri |September 11, 2014 |DAILY BEAST 

Is gambling culture more desirable than gay culture and counterculture? Putin's Crimea Is a Big Anti-Gay Casino |Anna Nemtsova |September 8, 2014 |DAILY BEAST 

Now, he's out to prove that redheads are just as desirable as their neutral-hued brethren. Redheads Are Sexy, Dammit! |Justin Jones |September 2, 2014 |DAILY BEAST 

As prejudices waned, it became easier and ultimately desirable for Jews to fully assimilate. The Ghost Hotels of the Catskills |Brandon Presser |August 25, 2014 |DAILY BEAST 

And all over the world each language would be taught with the same accent and quantities and idioms—a very desirable thing indeed. The Salvaging Of Civilisation |H. G. (Herbert George) Wells 

Some critics feel that, despite much that is desirable in her work, the soul is lacking in the women she paints. Women in the fine arts, from the Seventh Century B.C. to the Twentieth Century A.D. |Clara Erskine Clement 

To my friends ever since I have not failed to recommend the passage of the Butterley tunnel as a desirable pleasure excursion. Fifty Years of Railway Life in England, Scotland and Ireland |Joseph Tatlow 

And I will make thy bulwarks of jasper: and thy gates of graven stones, and all thy borders of desirable stones. The Bible, Douay-Rheims Version |Various 

To bring forth a violin of this desirable type, Stradivari directed his energies. Antonio Stradivari |Horace William Petherick