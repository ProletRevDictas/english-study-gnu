After a recent day at Busch Gardens Williamsburg in Virginia, I can assure them that a piece of fabric does not muffle shrieks. Theme parks aim to keep visitors safe — and screaming |Andrea Sachs |May 21, 2021 |Washington Post 

The night of the downpour we slept under a hardware-store tarp in a patch of skunkweed near the river, and I stayed awake listening to the wind in case it ramped up to that high shriek that means treetops are going to start snapping. Sebastian Junger on Walking America’s Railroads |Sebastian Junger |May 19, 2021 |Outside Online 

Students moan and growl and shriek and yawp, as if exorcising demons in a ritualistic ceremony. How Taryn Toomey’s ‘The Class’ Became New York’s Latest Fitness Craze |Lizzie Crocker |January 9, 2015 |DAILY BEAST 

A shriek of glee briefly broke out across the Web as inquiring minds tried to deduce who was the lucky lady. My Bizarre Night With James Deen, Libertarian Porn Star |Emily Shire |November 12, 2014 |DAILY BEAST 

Even her brother, Sheriff, who tried to pick her up to cuddle her, was pushed away with a firm “no” and a shriek. The Life of a Liberian Child with Ebola |Sarah Crowe |November 5, 2014 |DAILY BEAST 

For all its performance art and immersive theater foundation, the show also has its own shriek moments. Sex, Blood, and Screaming: Blackout’s Dark Frights |Tim Teeman |October 7, 2014 |DAILY BEAST 

One nurse pushed her hip alarm and the pulsing shriek rang out again. Inside a Hospital for the Criminally Insane |Caitlin Dickson |September 15, 2014 |DAILY BEAST 

As she peered into the face of Dr. Ashton, her own was scarlet and yellow, and her voice rose to a shriek. Elster's Folly |Mrs. Henry Wood 

He was hurrying towards the corner of the palace grounds when a shriek from Winifred set his teeth on edge. The Red Year |Louis Tracy 

Now under the ivy-laden branches of a tall old tree an owl startles them with its shriek. Checkmate |Joseph Sheridan Le Fanu 

Then a moan, then a howl and a shriek arose which reached from group to group, from house to house, from square to forest. A Lost Hero |Elizabeth Stuart Phelps Ward and Herbert D. Ward 

Above the uproar of the reeling earth the shriek of the train sounded in his deafened ears. A Lost Hero |Elizabeth Stuart Phelps Ward and Herbert D. Ward