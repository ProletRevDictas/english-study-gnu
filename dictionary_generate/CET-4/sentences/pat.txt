Pat Robertson finished second in the 1988 Iowa caucus, and it was all downhill from there. Can Huckabee Convert the GOP’s Moneymen? |Lloyd Green |January 4, 2015 |DAILY BEAST 

Pat Robertson wants to talk about the extinction of the gays. No Gods, No Cops, No Masters |James Poulos |January 1, 2015 |DAILY BEAST 

Divide the dough in half and very gently pat each half into a round 1-inch-thick disk. Make ‘The Chew’s’ Carla Hall’s Pumpkin Pecan Pie |Carla Hall |December 26, 2014 |DAILY BEAST 

Pat the chicken dry with paper towels, place on a sheet pan, brush with olive oil, and sprinkle with salt and pepper. Make These Barefoot Contessa Chicken Pot Pies |Ina Garten |November 29, 2014 |DAILY BEAST 

In Illinois, embattled Democratic incumbent Pat Quinn lost his bid for re-election against Bruce Rauner. GOP Shocks Democrats in Governor Races |Ben Jacobs |November 5, 2014 |DAILY BEAST 

Pat Malone, you are fined five dollars for assault and battery on Mike Sweeney. The Book of Anecdotes and Budget of Fun; |Various 

I'm somewhat puzzled to know why they didn't stand pat and make a clean job of us both. Raw Gold |Bertrand W. Sinclair 

Henry gave his younger sister a gentle pat, as she returned with her workbag and fished for the chalk. The Box-Car Children |Gertrude Chandler Warner 

“He was worth saving,” remarked Stanley, stooping to pat the meek head of the dog. The Floating Light of the Goodwin Sands |R.M. Ballantyne 

The branches seem to pat the house lovingly and to protect the children when the sun is too hot or the rain comes down too fast. Seven O'Clock Stories |Robert Gordon Anderson