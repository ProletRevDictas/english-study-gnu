Slamming the door or setting an alarm clock was a cardinal sin so wakeups were conducted by a junior sailor who gently coaxed you awake when it was time to stand watch. How a Nuclear Submarine Officer Learned to Live in Tight Quarters - Issue 94: Evolving |Steve Weiner |December 30, 2020 |Nautilus 

The 17-by-21-inch framed print from the San Francisco artist and co-owner of Lost Art Salon was inspired by stories of sailors using the sun, moon and stars to guide them. Shopping with the pros: Paloma Contreras’s favorite items from Serena & Lily |Mari-Jane Williams |December 17, 2020 |Washington Post 

Before the invention of GPS technology, sailors crossing the globe relied on a combination of complex mechanical instruments and accurate timepieces. How Sea Turtles Find Their Way - Issue 94: Evolving |Jason G. Goldman |December 16, 2020 |Nautilus 

Fred Fourie, marine robotics engineerMilitary spending is a key driver for this technology, since monitoring the world’s oceans without having to carry sailors could be a game-changing proposition for the Navy. Are Sailing Drones the Next Big Military Weapon? |Charu Kasturi |November 23, 2020 |Ozy 

Rum was served daily to the sailors as a long-standing tradition of the Navy. Luxury hotels are wooing guests with exclusive wines and spirits |Rachel King |September 5, 2020 |Fortune 

My dad was a sailor, and all through my childhood he was away half of the time at sea, and to an extent I have a similar job. Belle & Sebastian Aren’t So Shy Anymore |James Joiner |January 7, 2015 |DAILY BEAST 

Throughout the years it has also served as a sailor tavern and a high-end restaurant. Inside The World’s 10 Oldest Restaurants |Justin Jones |December 20, 2014 |DAILY BEAST 

Many Sailor Moon story arcs, in the comics and on television, end with the Sailor Senshi dying and being reborn. ‘Sailor Moon’ Is an Oasis for Superheroes Who Can Save the Universe in Heels |Rich Goldstein |November 26, 2014 |DAILY BEAST 

Sailor Moon Crystal is expected to wrap up its initial storylines by the end of the year. ‘Sailor Moon’ Is an Oasis for Superheroes Who Can Save the Universe in Heels |Rich Goldstein |November 26, 2014 |DAILY BEAST 

Still, Sailor Moon fans are always ravenous for new content, especially after such a long time away. ‘Sailor Moon’ Is an Oasis for Superheroes Who Can Save the Universe in Heels |Rich Goldstein |November 26, 2014 |DAILY BEAST 

The Goblin stared about him in a dazed manner for a moment, and then said, "Sindbad the Sailor's house." Davy and The Goblin |Charles E. Carryl 

In reality he was annoyed at having old Monsieur Farival, who considered himself the better sailor of the two. The Awakening and Selected Short Stories |Kate Chopin 

Sea-faring men seldom take snuff: a sailor with a snuff-box is as rarely to be met with as a sailor without a knife. Tobacco; Its History, Varieties, Culture, Manufacture and Commerce |E. R. Billings. 

The solo voice of the Nubian sailor was lost in the chorus of voices which came floating over the Nile. Bella Donna |Robert Hichens 

Marryat gives us in “Peter Simple” a vivid and convincing picture of the sailor going to Portsmouth to rejoin his ship. The Portsmouth Road and Its Tributaries |Charles G. Harper