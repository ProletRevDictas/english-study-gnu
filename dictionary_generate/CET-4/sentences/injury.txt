Last year, Neuralink showed a product designed to eventually treat those with traumatic brain and spinal cord injuries. Elon Musk’s secretive brain-machine venture, Neuralink, offers a glimpse |Lucinda Shen |August 28, 2020 |Fortune 

Having said all that, it’s the second problem—injury risk—that makes the new paper most interesting. Stop Counting Your Running Mileage |Alex Hutchinson |August 28, 2020 |Outside Online 

Deep brain stimulation, or treatment via electrodes implanted into the brain, is already used for traumatic brain injuries. Elon Musk’s brain company plans a big reveal on Friday. Here’s what we already know |Verne Kopytoff |August 27, 2020 |Fortune 

A study of 5,000 patients published June 11 in the Journal of Clinical Investigation found serious adverse events, such as allergic reactions or transfusion-related lung injury, occurred less than 1 percent of the time. COVID-19 plasma treatments may be safe, but we don’t know if they work |Tina Hesman Saey |August 25, 2020 |Science News 

Suárez is fully recovered from a late winter shoulder injury. The Dodgers Lead Our National League Predictions, But Don’t Count Out The Nats Or … Reds? |Travis Sawchik |July 22, 2020 |FiveThirtyEight 

It was like a constant assault, an almost stupefying catalogue of mindless racial insult and injury. How Martin Luther King Jr. Influenced Sam Cooke’s ‘A Change Is Gonna Come’ |Peter Guralnick |December 28, 2014 |DAILY BEAST 

And thus I end up at the bottom of the stairs, about one month after my injury and two months after my wedding. You’re Never ‘Cured’ of an Eating Disorder |Carrie Arnold |December 20, 2014 |DAILY BEAST 

Insult to injury, its $43 million gross was less than one-fifth of what Ted took in. The Biggest Bombs of 2014: ‘Sex Tape,’ Mariah Carey’s Vocals, ‘How I Met Your Mother’ and More |Kevin Fallon |December 19, 2014 |DAILY BEAST 

First he served 90 days in solitary for breaking the rule against self-injury. A Million Ways to Die in Prison |Daniel Genis |December 8, 2014 |DAILY BEAST 

A football player killed himself after saying he was ‘all f----- up’ from a head injury. Will the NCAA Let Ohio State’s Kosta Karageorge Die in Vain? |Robert Silverman |December 1, 2014 |DAILY BEAST 

It was in the reading-room at the time of the fire, but fortunately escaped injury. Showell's Dictionary of Birmingham |Thomas T. Harman and Walter Showell 

The law only gave you the right to proceed against him to recover money damages for the legal injury. Putnam's Handy Law Book for the Layman |Albert Sidney Bolles 

He is shining black, and as he tosses his head one can see the wicked horns, capable of doing such terrible injury. Alila, Our Little Philippine Cousin |Mary Hazelton Wade 

When the sailors are employed in shifting the sails, great care must be taken to avoid injury by the falling of any of the ropes. A Woman's Journey Round the World |Ida Pfeiffer 

An automobilist must exercise reasonable or ordinary care to avoid injury to other persons using the highway. Putnam's Handy Law Book for the Layman |Albert Sidney Bolles