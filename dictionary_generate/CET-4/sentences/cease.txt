At what point does a netizen's right to privacy and anonymity cease is the crux of the lawsuit brought forth against LiquidVPN. Film studios sue “no logs” VPN provider for $10 million |Ax Sharma |September 27, 2021 |Ars Technica 

According to a cease and desist letter Brown sent to Bishop Sycamore in February, YouthBuild informed the state attorney general and the police department of Bishop Sycamore’s “fraudulent” activities. Bishop Sycamore, IMG and the high school football game that duped ESPN |Ben Strauss |September 1, 2021 |Washington Post 

Despite this cease fire unless things change we will see violence break out every few years. Opinion | Netanyahu, Hamas must go to achieve any peace |Peter Rosenstein |May 27, 2021 |Washington Blade 

Egypt helped the United States with backroom diplomacy and brokered the most recent cease fire. Opinion | Netanyahu, Hamas must go to achieve any peace |Peter Rosenstein |May 27, 2021 |Washington Blade 

Slowly, slowly, dance classes may cease to be such secret and guilty pleasures in Iran. Iran’s Becoming a Footloose Nation as Dance Lessons Spread |IranWire |January 2, 2015 |DAILY BEAST 

While there are a couple of antibiotics that usually work, if they are overused they, too, may cease to be effective. Without Education, Antibiotic Resistance Will Be Our Greatest Health Crisis |Russell Saunders |December 19, 2014 |DAILY BEAST 

The liberated soul does not cease to act, to think, to create, to instigate revolutionary flows. On Torture, Chuck Johnson & Sondheim |James Poulos |December 13, 2014 |DAILY BEAST 

If America ceases to be good, America will cease to be great. After Torture Report, Our Moral Authority As a Nation Is Gone |Nick Gillespie |December 11, 2014 |DAILY BEAST 

The facts do not cease to matter merely because a white cop killed a black boy. Dear White People: Well-Meaning Paternalism Is Still Racist |Chloé Valdary |December 9, 2014 |DAILY BEAST 

The bear watched him narrowly with its wicked little eyes, though it did not see fit to cease its paw-licking. The Giant of the North |R.M. Ballantyne 

As soon as he had seen his mother, he would set off again, and never cease searching till he had found either Ramona or her grave. Ramona |Helen Hunt Jackson 

It was a life full of freedom, and I shall never cease to be grateful for it, but I must go home soon and look after my affairs. Ancestors |Gertrude Atherton 

He made him like the saints in glory, and magnified him in the fear of his enemies, and with his words he made prodigies to cease. The Bible, Douay-Rheims Version |Various 

Industrial society, they say, must be reorganized from top to bottom; private industry must cease. The Unsolved Riddle of Social Justice |Stephen Leacock