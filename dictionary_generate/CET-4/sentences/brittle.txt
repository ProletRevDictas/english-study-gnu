The old dam on New Hampshire’s Second Connecticut Lake is caked with brittle crusty ice when I arrive, and the surrounding hills shrouded in fresh snow. He escaped the cacophony by strapping on snowshoes and slipping into the Great North Woods |Miles Howard |January 15, 2021 |Washington Post 

Slight or seemingly random perturbations to a dataset—often undetectable by the human eye—can enormously alter the final output, something dubbed “brittle” for an algorithm. 2021 Could Be a Banner Year for AI—If We Solve These 4 Problems |Shelly Fan |January 5, 2021 |Singularity Hub 

More commonly, the root can be dug, washed, chopped, and roasted until brittle to create a steeping mixture that can be used like coffee grounds. 13 edible plants you can still find in the winter |By Tim MacWelch/Outdoor Life |December 1, 2020 |Popular-Science 

Even the denser rocks are much more porous and brittle than meteorites from similar asteroids that have been found on Earth. The asteroid Bennu’s brittle boulders may make grabbing a sample easier |Lisa Grossman |October 8, 2020 |Science News 

However during later times after the Industrial Age, craftsmen were able to achieve higher temperatures for impurity removal, and they further improved upon the process by mixing carbon that made the brittle iron stronger. 14 Exceptional Weapon Systems from History That Were Ahead of their Time |Dattatreya Mandal |March 26, 2020 |Realm of History 

Instead of being strong and resilient, bones become weak and brittle. You’re Never ‘Cured’ of an Eating Disorder |Carrie Arnold |December 20, 2014 |DAILY BEAST 

The way to fight being brittle—to keep the disease at bay—is to work at being limber. The Stacks: The True Greatness of Muhammad Ali |Peter Richmond |February 23, 2014 |DAILY BEAST 

Brittle egos are bolstered less by what they love about themselves than what they find contemptible in others. Why Boring Names Are Best |Justin Green |March 4, 2013 |DAILY BEAST 

But freedom is a beautifully brittle spirit balancing on a narrow strip of the intellectual spectrum. This Week’s Hot Reads: Dec. 17, 2012 |Jimmy So |December 18, 2012 |DAILY BEAST 

Her many style tics—stacked one atop the other—read as code for narcissism, self-indulgence, and brittle self-absorption. Newt Gingrich's Wife Callista's Prissy Style Problem |Robin Givhan |December 13, 2011 |DAILY BEAST 

More like the noise of powdering an iron bar on a nutmeg-grater, suggested Brittle. Oliver Twist, Vol. II (of 3) |Charles Dickens 

At this time the leaves are very brittle and unless the cutter is an experienced hand much injury may be done to the leaves. Tobacco; Its History, Varieties, Culture, Manufacture and Commerce |E. R. Billings. 

There was so little rain during the hot months that things became dry and brittle. The value of a praying mother |Isabel C. Byrum 

The rubber is much like tough, heavy dough—there is not much stretch to it and in a cold place it would become hard and brittle. The Wonder Book of Knowledge |Various 

Light round targets were brought them, and in the place of pointed lances, long brittle reeds. God Wills It! |William Stearns Davis