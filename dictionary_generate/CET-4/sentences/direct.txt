Players and their direct support teams and essential staff members not able to work from home will be the only players allowed to enter Melbourne Park. Novak Djokovic’s five-set battle at Australian Open started with fans and ended without them |Matt Bonesteel |February 12, 2021 |Washington Post 

Multiple assistant coaches said they have had to come to terms with the direct impact that the protocols can have on their team’s record, either by cutting down on practice time and in-person meetings or by sidelining players. The NBA’s week of controversies show how hard life is outside of the bubble |Ben Golliver |February 12, 2021 |Washington Post 

In June 2019, Housing Commissioners Stefanie Benvenuto and Ryan Clumpner directed the agency to study the viability of a vacancy fee. Vacancy Tax Study Is Giving City Officials Déjà Vu |Lisa Halverstadt and Andrew Keatts |February 10, 2021 |Voice of San Diego 

The more common direct-drive motor begins spinning the blades once the machine is turned on, while a clutch-drive motor engages when you move a handle or throw a switch. Wood chippers to keep your property looking great |PopSci Commerce Team |February 9, 2021 |Popular-Science 

Today, Shop Pay’s payment option is used by a number of top direct-to-consumer and newer brands, including Allbirds, Kith, Beyond Yoga, Kylie Cosmetics, Jonathan Adler, Loeffler Randall, Blueland and others. Shopify expands its payment option, Shop Pay, to its merchants on Facebook and Instagram |Sarah Perez |February 9, 2021 |TechCrunch 

In 2009, Lee Daniels announced that he would direct Selma and that Liam Neeson would play President Lyndon Johnson. Dr. King Goes to Hollywood: The Flawed History of ‘Selma’ |Gary May |January 2, 2015 |DAILY BEAST 

If someone wants to ensure a direct and secure connection, no entity, whether a hotel or otherwise, should be able to block it. How ‘Ethical’ Hotel Chain Marriott Gouges Guests in the Name of Wi-Fi Security |Kyle Chayka |December 31, 2014 |DAILY BEAST 

The twang we hear as emblematic of white country music is actually the direct descendant of black folk music banjo. The Cultural Crimes of Iggy Azalea |Amy Zimmerman |December 29, 2014 |DAILY BEAST 

Idiocies multiply in direct proportion to the accumulating legal rigidities. Red Tape Is Strangling Good Samaritans |Philip K. Howard |December 27, 2014 |DAILY BEAST 

Today they are more direct, especially in many conflict regions of the world. ISIS, Boko Haram, and the Growing Role of Human Trafficking in 21st Century Terrorism |Louise I. Shelley |December 26, 2014 |DAILY BEAST 

Each religion claims that its own Bible is the direct revelation of God, and is the only true Bible teaching the only true faith. God and my Neighbour |Robert Blatchford 

The steamboat of 1809 and the steam locomotive of 1830 were the direct result of what had gone before. The Unsolved Riddle of Social Justice |Stephen Leacock 

It was a direct lie to tell the Austrian commander that an armistice had been arranged and the bridge ceded to the French. Napoleon's Marshals |R. P. Dunn-Pattison 

I suppose that to most men such a warning would be a direct incitement to make the attempt. Uncanny Tales |Various 

Even the policeman who is paid to direct you, replies to your inquiry with the shortest and gruffest monosyllable that will do. Glances at Europe |Horace Greeley