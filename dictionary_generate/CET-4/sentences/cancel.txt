Polls suggest that a lot of voters currently don’t know what cancel culture is — and that’s true even among Republicans, despite the party’s elites talking about cancel culture nonstop. Why Attacking ‘Cancel Culture’ And ‘Woke’ People Is Becoming The GOP’s New Political Strategy |Perry Bacon Jr. (perry.bacon@fivethirtyeight.com) |March 17, 2021 |FiveThirtyEight 

What we now call cancel culture is the contentious nature of a free society wrestling with the respectable parameters of public speech. Anti-BDS laws an affront to free speech |Khelil Bouarrouj |February 19, 2021 |Washington Blade 

Did he participate in his own extortion and cancel his plans for a big Christmas premiere? Should the U.S. Really Pay a Kim’s Ransom? |Kevin Bleyer |December 21, 2014 |DAILY BEAST 

My family is ready to mount an intervention, and cancel my streaming accounts. The Best Albums of 2014 |Ted Gioia |December 13, 2014 |DAILY BEAST 

Should we cancel gatherings, reunions, excursions, or throw ourselves into them with even more gratitude for one another? The Media's Pro-Torture Cheerleaders |Jedediah Purdy |December 10, 2014 |DAILY BEAST 

Presumably, without those subsidies, most will just cancel their policies. The GOP Could Make Obama Kill Obamacare |Michael Tomasky |November 10, 2014 |DAILY BEAST 

In a statement, the MoD admitted that it had been forced to cancel the rest of the training program. Libyan Troops Go Wild in England |Nico Hines |November 4, 2014 |DAILY BEAST 

A lease made by a minor is not void, but he may avoid or cancel it by some positive act. Putnam's Handy Law Book for the Layman |Albert Sidney Bolles 

Does a debtor who turns over a note to his creditor in payment, thereby cancel the debt? Putnam's Handy Law Book for the Layman |Albert Sidney Bolles 

Either of the parties might cancel the bond, but only after a formal and public notice of his intentions. The Private Life of the Romans |Harold Whetstone Johnston 

Mendelssohn wanted to cancel the excommunication on the ground that the church has no rights in civil matters. Solomon Maimon: An Autobiography. |Solomon Maimon 

The Law does not cancel the promise, but faith in the promised Christ cancels the Law. Commentary on the Epistle to the Galatians |Martin Luther