It’s unclear how many students are actually doing the portions of PE that aren’t live-streamed. Kids are shooting hoops with rolled up socks, but pandemic physical education is not canceled |Kelly Field |February 12, 2021 |Washington Post 

The services include selling ad inventory in the form of pre-roll, mid-roll or post-roll ads around content while taking a portion of the earnings in a revenue share model. How TheSoul Publishing grew revenue via platforms with viral social media life hacks |Kayleigh Barber |February 12, 2021 |Digiday 

She also noted a decline in the portion of residents answering contact tracers’ phone calls, which has dropped below 70 percent for the first time since September. South Africa and U.K. coronavirus variants detected in D.C.; Maryland to open third mass vaccination site |Erin Cox, Julie Zauzmer, Rachel Chason |February 11, 2021 |Washington Post 

For example, RapidSOS stepped in to provide data when the Nashville bombing took out a portion of 911 infrastructure on Christmas Day, affecting 300 agencies. RapidSOS raises $85M for a big data platform aimed at emergency responders |Ingrid Lunden |February 9, 2021 |TechCrunch 

You can even ski on Park Loop Road, portions of which are unplowed. The Ultimate Acadia National Park Travel Guide |Virginia M. Wright |February 8, 2021 |Outside Online 

Counting encores, NBC estimated that over 44 million people had watched some portion of the production by the end of the month. The Cast of ‘Peter Pan Live!’ Knows You Hatewatched ‘The Sound of Music’ |Kevin Fallon |December 2, 2014 |DAILY BEAST 

In the financing portion, the numbers are particularly bleak. Millions Promised for Ebola Not Adding Up |Abby Haglage |November 25, 2014 |DAILY BEAST 

And its military destroyed a large portion of Syria's chemical weapons stockpile. Politics End In Halifax As Democratic and GOP Senators Seek Common Ground on National Security |Tim Mak |November 22, 2014 |DAILY BEAST 

In 2000, the Israeli government simply closed the portion of downtown Hebron under its control. Inside Hebron, Israel’s Heart of Darkness |Michael Tomasky |November 21, 2014 |DAILY BEAST 

But they can win back a portion of it, and in many states and congressional districts, a portion will be enough to change things. The Real Reason Democrats Lost |Michael Tomasky |November 6, 2014 |DAILY BEAST 

On the upper part of the stem the whorls are very close together, but they are more widely separated at the lower portion. How to Know the Ferns |S. Leonard Bastin 

In the old world, poverty seemed, and poverty was, the natural and inevitable lot of the greater portion of mankind. The Unsolved Riddle of Social Justice |Stephen Leacock 

There are many more good dwellings on this plain than in the rural portion of Lower Italy. Glances at Europe |Horace Greeley 

Turn not away thy face from thy neighbour, and of taking away a portion and not restoring. The Bible, Douay-Rheims Version |Various 

First of all, wrap a portion of damp newspaper round the roots, and then tie up with dry paper. How to Know the Ferns |S. Leonard Bastin