The strategy is to establish itself as the default companion app for CGMs, but to do so it will need to gain access to CGM-generated data. Undermyfork scores $400K seed for its diabetes tracking app and US launch |Steve O'Hear |August 28, 2020 |TechCrunch 

For the past couple of months, our pets have become our coworkers, confidants, and constant companions. How to help your pet with separation anxiety |Sara Kiley Watson |August 26, 2020 |Popular-Science 

When my companions and I passed an enticing swimming hole in the high desert of northeastern Nevada, I slipped into bikini bottoms in the parking lot and back out of them again afterward. In Praise of the Adventure Dress |Alison Van Houten |August 22, 2020 |Outside Online 

Players can work with online friends, as well as virtual running companions, toward shared goals—running a combined distance of 10 miles, for example, or collecting imaginary potions from different locations. A breath of fresh air in video games |Tate Ryan-Mosley |August 19, 2020 |MIT Technology Review 

Yet, the two brothers are each other’s most frequent opera and wine-tasting companion. If your spirits need a boost, watch ‘Frasier’ |Kathi Wolfe |August 13, 2020 |Washington Blade 

But she soldiered on to the end, a loving and faithful companion in victory and defeat. Boris Johnson’s Churchill Man Crush |Michael F. Bishop |November 22, 2014 |DAILY BEAST 

We continuously pause to pull them out while Zalwar Khan and his companion smirk at us and chew unbothered. Heart of Darkness: Into Afghanistan’s Taliban Valley |Matt Trevithick, Daniel Seckman |November 15, 2014 |DAILY BEAST 

He meets his current companion, Sandra Lee, at a party in the Hamptons. Andrew Cuomo Ignores Rural New York |David Fontana |November 8, 2014 |DAILY BEAST 

She claims to be bowled over by a genuine attraction to another older companion. When Eva Braun Met Anna Nicole Smith |Nico Hines |October 26, 2014 |DAILY BEAST 

The device is plugged into the wall and the companion smartphone app instructs the device when to start recording. The Tracker That Might Actually Help You Sleep Better |DailyBurn |October 17, 2014 |DAILY BEAST 

The bear laughed and joined his companion, and the torpedo thundered away. The Joyous Adventures of Aristide Pujol |William J. Locke 

She took the fan from Madame Ratignolle and began to fan both herself and her companion. The Awakening and Selected Short Stories |Kate Chopin 

A friend and companion meeting together in season, but above them both is a wife with her husband. The Bible, Douay-Rheims Version |Various 

I was thinking of any thing but the Virgin, or the window, or the light; I was thinking of my companion—so fair, and so devout. Blackwood's Edinburgh Magazine, Volume 60, No. 372, October 1846 |Various 

There is a companion who condoleth with his friend for his belly's sake, and he will take up a shield against the enemy. The Bible, Douay-Rheims Version |Various