In part, it’s just bad luck that the United States is being slammed with these events back-to-back-to-back. What’s behind August 2020’s extreme weather? Climate change and bad luck |Carolyn Gramling |August 27, 2020 |Science News 

The NBA draft lottery isn’t all about luck — teams tank for a reason, of course — but the bouncing pingpong balls do play a big role. The NBA’s Unluckiest Lottery Team Finally Got A Good Bounce |Neil Paine (neil.paine@fivethirtyeight.com) |August 21, 2020 |FiveThirtyEight 

The next point on the luck end of the spectrum was the stock market. The Economics of Sports Gambling (Ep. 388 Rebroadcast) |Stephen J. Dubner |August 20, 2020 |Freakonomics 

The most you can do is put yourself in the path of luck—but to think you can guess with certainty the actual outcome is a presumptuousness the true poker player foregoes. The Deck Is Not Rigged: Poker and the Limits of AI |Maria Konnikova |August 7, 2020 |Singularity Hub 

So stats achieved before they can stabilize are mostly the result of luck. The MLB Records* That Could Fall In A 60-Game Season |Michael Salfino |August 3, 2020 |FiveThirtyEight 

And good luck getting the song (and music video) to “Chandelier” out of your head. The 10 Best Albums of 2014: Taylor Swift, Sia, Run the Jewels, and More |Marlow Stern |December 28, 2014 |DAILY BEAST 

The Horse You Came in On Saloon, Baltimore Horse-themed bars must be bad luck for famous authors. The Bars That Made America Great |Nina Strochlic |December 28, 2014 |DAILY BEAST 

Wearing the right foot of a chicken was considered good luck. The History of the Chicken: How This Humble Bird Saved Humanity |William O’Connor |December 27, 2014 |DAILY BEAST 

But good luck convincing other countries that the case against North Korea is airtight. Cyberwar on North Korea Could Be Illegal |Shane Harris |December 23, 2014 |DAILY BEAST 

The story follows a down on his luck family man named Bill Scanlon (Wes Bentley), who takes to stealing after losing his job. After The Fall: Introducing The Anti-Villain |Rich Goldstein |December 21, 2014 |DAILY BEAST 

G was a gamester, who had but ill-luck; H was a Hunter, who hunted a buck. Boys and Girls Bookshelf; a Practical Plan of Character Building, Volume I (of 17) |Various 

I wouldn't go on if I were you, sir; the luck's dead against you to-night; I wouldn't go on, indeed I wouldn't. The Pit Town Coronet, Volume I (of 3) |Charles James Wills 

Then my luck changed and I found myself under one of the very greatest teachers of his time, Professor Huxley. The Salvaging Of Civilisation |H. G. (Herbert George) Wells 

By bad luck d'Amade was away, up in the front trenches, and I could not well deliver myself to des Coigns. Gallipoli Diary, Volume I |Ian Hamilton 

Well, we must try our luck with a regulation sabre; they can't well refuse it; ours is the stronger and bigger man. The Pit Town Coronet, Volume I (of 3) |Charles James Wills