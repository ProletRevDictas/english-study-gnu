Twinco Capital also has a debt facility with the Spanish investment bank EBN Banco de Negocios, which is common for any type of lending company. Twinco Capital scores €3M for its supply chain finance solution |Steve O'Hear |February 5, 2021 |TechCrunch 

Initially, she finds excuses – a visit to check in on her “neighbor” at the hospital, an offer to lend a hand with the various burdens of care – but when these begin to wear thin, her efforts escalate. Lesbian love story becomes thriller in ‘Two of Us’ |John Paul King |February 4, 2021 |Washington Blade 

Unlike mortgage originators, which lend money to the borrower, a mortgage servicer interfaces with the borrower for the duration of their loan – and that can be anywhere from 15 to 30 years. Valon closes on $50M a16z-led Series A to grow mobile-first mortgage servicing platform |Mary Ann Azevedo |February 2, 2021 |TechCrunch 

We've written a number of times about why Animal Crossing's chill, landscape-tending gameplay lends itself perfectly to our current stuck-inside-amid-a-pandemic moment. Why Animal Crossing: New Horizons’ 31 million sales are so incredible |Kyle Orland |February 1, 2021 |Ars Technica 

My experiences as a mother to my son, George, have also lent inspiration to my work, especially my founding of the Fabrics Matter Movement. The future of maternity workwear is all in the details |Rachel King |January 31, 2021 |Fortune 

In 2008, his monastery was in desperate need of funds and Vreeland decided to lend a hand with his first photography exhibition. From Fashion Player to Photographer Monk |Nina Strochlic |December 3, 2014 |DAILY BEAST 

His play The Hairy Ape, the agent noted, “could easily lend itself to radical propaganda.” Was the Unabomber a Eugene O’Neill Fan? |Robert M. Dowling |November 6, 2014 |DAILY BEAST 

After seeing the film, he also agreed to lend his synthesized voice to the latter portion. Eddie Redmayne’s Time Has Come: On His Heartrending Turn as Stephen Hawking and Benedict Bromance |Marlow Stern |November 3, 2014 |DAILY BEAST 

Is it that collectivist cultures such as those in Asia lend themselves to this nature of group sexual crime? The Psychology of Sex Slave Rings |Charlotte Lytton |August 31, 2014 |DAILY BEAST 

Lakeside in Texas, baked by the heat, Louganis described how Red Bull got him to lend his credibility to the competition. The World Series of Cliff Diving Takes Itself Very Seriously |Hampton Stevens |June 29, 2014 |DAILY BEAST 

Other orchestra leaders are always writing and begging him to lend them his copies of Oratorios, etc. Music-Study in Germany |Amy Fay 

Then he held down a hand to her, bade her set her foot on his, and called with an oath to Rabecque to lend her his assistance. St. Martin's Summer |Rafael Sabatini 

The human species,” Charles Lamb says, “is composed of two distinct races, the men who borrow and the men who lend. Fifty Years of Railway Life in England, Scotland and Ireland |Joseph Tatlow 

He took me to the house of a musical friend of his who was to lend me his grand piano, and there we tried our sonata. Music-Study in Germany |Amy Fay 

To this the great do not care to lend their ears, and the small have not wings strong enough to fly so far. The Jesuit Relations and Allied Documents, Vol. II: Acadia, 1612-1614 |Various