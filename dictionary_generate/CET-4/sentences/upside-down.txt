Clad in a blue, striped button-down, a silver watch adorning his left wrist, Huckabee beams on the cover. Huckabee 2016: Bend Over and Take It Like a Prisoner! |Olivia Nuzzi |January 8, 2015 |DAILY BEAST 

That article noted that the F-35 does not currently have the ability to down-link live video to ground troops,. Pentagon Misfires in Stealth Jet Scandal |Dave Majumdar |January 8, 2015 |DAILY BEAST 

A grand juror in the Ferguson case is suing to be able to explain exactly what went down in the courtroom. Politicians Only Love Journalists When They're Dead |Luke O’Neil |January 8, 2015 |DAILY BEAST 

The gunman then burst from the restaurant and fled down the street with the other man. Shot Down During the NYPD Slowdown |Michael Daly |January 7, 2015 |DAILY BEAST 

My doctor insisted that once I filed this piece I lie down on my bed and not get out. I Tried to Warn You About Sleazy Billionaire Jeffrey Epstein in 2003 |Vicky Ward |January 7, 2015 |DAILY BEAST 

Then there was Wee Wo,—he was a little Chinese chap, and we used to send him down the chimneys to open front doors for us. Davy and The Goblin |Charles E. Carryl 

The bride elect rushes up to him, and so they both step down to the foot-lights. Physiology of The Opera |John H. Swaby (AKA "Scrici") 

I take the Extream Bells, and set down the six Changes on them thus. Tintinnalogia, or, the Art of Ringing |Richard Duckworth and Fabian Stedman 

His wife stood smiling and waving, the boys shouting, as he disappeared in the old rockaway down the sandy road. The Awakening and Selected Short Stories |Kate Chopin 

So he bore down on the solemn declaration that she stood face to face with a prison term for perjury. The Bondboy |George W. (George Washington) Ogden