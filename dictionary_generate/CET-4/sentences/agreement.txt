Such an agreement currently exists for pandemic influenza, Phelan notes, but not for any other kind of disease or vaccine. A coronavirus vaccine will save more lives if we share it widely |Kat Eschner |September 17, 2020 |Popular-Science 

After all, he and his appointees have reversed or defanged dozens of other environmental rules, practices, and international agreements during the last four years. Trump’s rollbacks could add half an EU’s worth of climate pollution by 2035 |James Temple |September 17, 2020 |MIT Technology Review 

I agree with a lot of it…I heard Nancy Pelosi say she doesn’t want to leave until we have an agreement. Trump warms to ‘Problem Solvers’ stimulus bill—including $1,200 checks and $450 weekly unemployment benefits |Lance Lambert |September 17, 2020 |Fortune 

This decision went hand-in-hand with a bipartisan agreement to offer all registered voters the chance to vote by mail-in absentee ballot or by dropping one off early, according to the Louisville Courier Journal. Why you should vote as early as possible (and how to do it) |John Kennedy |September 17, 2020 |Popular-Science 

The good news is that California reached an agreement in August with the US Forest Service to boost these efforts, with a goal of treating a million acres per year for the next two decades. Suppressing fires has failed. Here’s what California needs to do instead. |James Temple |September 17, 2020 |MIT Technology Review 

Ronald Reagan approved the agreement and the USTR reviewed Korean practices through the end of his term. Propaganda, Protest, and Poisonous Vipers: The Cinema War in Korea |Rich Goldstein |December 30, 2014 |DAILY BEAST 

In November 2014, that agreement was extended by four months, with some additional restrictions on Iran. Fact-Checking the Sunday Shows: Dec 21 |PunditFact.com |December 21, 2014 |DAILY BEAST 

The agreement has three main points, all of which Iran has met, the IAEA says. Fact-Checking the Sunday Shows: Dec 21 |PunditFact.com |December 21, 2014 |DAILY BEAST 

“The CIA has since paid out more than $1 million pursuant to the agreement,” the report notes. The Luxury Homes That Torture and Your Tax Dollars Built |Michael Daly |December 12, 2014 |DAILY BEAST 

Schettino also tried to enter a plea bargain agreement, which ultimately was rejected by the Grosseto court. The Costa Concordia’s Randy Reckless Captain Takes the Stand |Barbie Latza Nadeau |December 2, 2014 |DAILY BEAST 

But no one in Spain and few in Manila as yet could foresee how the fulfilment of the Agreement would be bungled. The Philippine Islands |John Foreman 

The commander of this fleet was an Englishman, according to the agreement between them. The Philippine Islands, 1493-1898, Volume XX, 1621-1624 |Various 

By their agreement with the owner, the Company have the right of mining under an area of 185 acres, at a royalty of 6d. Showell's Dictionary of Birmingham |Thomas T. Harman and Walter Showell 

I do not know that the agreement matters much for a few days up or down. Life of Richard Trevithick, Volume II (of 2) |Francis Trevithick 

It therefore designates the act by which one enters into an agreement or a covenant with another. The Ordinance of Covenanting |John Cunningham