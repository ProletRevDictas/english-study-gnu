A small group of vanguard companies have proven that it’s possible for every employee to enjoy the fruits of ownership—for everyone at work to be a self-managing “micropreneur” blessed with autonomy and a shot at the brass ring. The biggest problem with capitalism? Not enough capitalists |jakemeth |November 21, 2020 |Fortune 

For the previous 28 years of the competition, finalists of Brandstorm have presented their pitches in person to members of the company’s top brass in Paris. ‘Focus on quality over quantity’: Why companies like L’Oreal are now aiming to use virtual events for recruiting |Kristina Monllos |November 9, 2020 |Digiday 

This sprinkler features 20 precision brass nozzles for gentle and even coverage of up to 3,600 square feet. Sprinklers to keep lawns fresh |PopSci Commerce Team |October 30, 2020 |Popular-Science 

For the theremin’s 100th anniversary, Moog unveils the gorgeous Claravox Centennial — With a walnut cabinet, brass antennas and a plethora of wonderful knobs and dials, the Claravox looks like it emerged from a prewar recording studio. Daily Crunch: Facebook Dating comes to Europe |Anthony Ha |October 22, 2020 |TechCrunch 

With a walnut cabinet, brass antennas and a plethora of wonderful knobs and dials, the Claravox looks like it emerged from a prewar recording studio, as indeed is the intention. For the theremin’s 100th anniversary, Moog unveils the gorgeous Claravox Centennial |Devin Coldewey |October 22, 2020 |TechCrunch 

But that project ran into serious hurdles with the network brass. Life After ‘Winter’s Bone’: Debra Granik on Finding J. Law and the Plight of the Female Director |Marlow Stern |October 24, 2014 |DAILY BEAST 

So maybe we should take a lesson from women with brass ovaries: comedy and feminism are longstanding bedfellows. Comedians and Feminism Getting Laughs |Agunda Okeyo |October 23, 2014 |DAILY BEAST 

But even as he was receiving awards, the military brass was processing his discharge—they had found out he was transgender. Yes to LGB, No to T: The Pentagon Still Has a Transgender Ban |Tim Mak |October 21, 2014 |DAILY BEAST 

Violation of gravitas is being taken ‘extremely seriously’ by army top brass, source says. Hunt To Identify Pirouetting 'Bearskin' Guardsman Who Shamed Army |Tom Sykes |September 3, 2014 |DAILY BEAST 

A source said that while the video may be entertaining, the incident is being taken ‘extremely seriously’ by army top brass. Hunt To Identify Pirouetting 'Bearskin' Guardsman Who Shamed Army |Tom Sykes |September 3, 2014 |DAILY BEAST 

They were provided with sails and twelve oars each, and a falconet, or small brass cannon. The Every Day Book of History and Chronology |Joel Munsell 

"I wonder if 'twas a brass drum, such as has 'Eblubust Unum' printed on't," said Mrs. Slocum. The Book of Anecdotes and Budget of Fun; |Various 

Picture frames, nicely moulded in brass, were made here in 1825, by a modeller named Maurice Garvey. Showell's Dictionary of Birmingham |Thomas T. Harman and Walter Showell 

The air was heavy with the perfume of frankincense which smouldered in a brass vessel set upon a tray. Dope |Sax Rohmer 

Latten, the term given to thin sheets of brass, was formerly applied to sheets of tinned iron. Showell's Dictionary of Birmingham |Thomas T. Harman and Walter Showell