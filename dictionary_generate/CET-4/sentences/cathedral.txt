For Allen, a Roman Catholic as passionate about the sport as she is about her faith, the baseball diamond has always been a cathedral. How Three Minor League Teams Saved Hometown Baseball |Nick Fouriezos |February 3, 2021 |Ozy 

Tickets to the virtual Q&A are free, though donations to the cathedral are welcome. The best things to do — virtually and in person — while social distancing in the D.C. area |Fritz Hahn, Hau Chu |November 12, 2020 |Washington Post 

Then came a Frankish church, a Merovingian basilica, and Carolingian and Romanesque cathedrals. Watching the flames burn Notre Dame, the spiritual heart of France |Elaine Sciolino |November 6, 2020 |Washington Post 

With fancy software, they can make a song sound like it was recorded in an echo-y cathedral. Here’s what bats ‘see’ when they explore the world with sound |Carolyn Wilke |October 29, 2020 |Science News For Students 

Also, it should be noted that some cathedrals collapsed as a result of short-sighted workmanship. Humanity is stuck in short-term thinking. Here’s how we escape. |Katie McLean |October 21, 2020 |MIT Technology Review 

In 1997, an earthquake in Assisi caused the collapse of the main cathedral and killed ten people. Florence Preps ‘David’ for the Big One |Barbie Latza Nadeau |December 25, 2014 |DAILY BEAST 

Here it is being performed by the Westminster Cathedral Choir. Yes, I Like Christmas Music. Stop Laughing. |Michael Tomasky |December 24, 2014 |DAILY BEAST 

He led the packed cathedral in applause for Ramos and Liu and asked Bratton to bring a message to the men and women of the NYPD. 'Please Don't Die!': The Frantic Battle to Save Murdered Cops |Michael Daly |December 22, 2014 |DAILY BEAST 

From deep within, looking up at the tropical sky is like staring through the dome of some kind of earthen cathedral. What Made Mexico’s Most Mysterious Beach? |Brandon Presser |October 14, 2014 |DAILY BEAST 

Can you believe someone could get into a cathedral and do a concert? Putin’s Hockey Pal Tells All: Slava Fetisov on ‘Red Army,’ Soviet Nostalgia, and What Drives Putin |Marlow Stern |October 9, 2014 |DAILY BEAST 

The cathedral is the only Spanish parochial church; it cares for two thousand four hundred souls. The Philippine Islands, 1493-1898, Volume XX, 1621-1624 |Various 

Father Fochel of the Cathedral had attempted to explain it; but he had not done so to her satisfaction. The Awakening and Selected Short Stories |Kate Chopin 

They have an old Cathedral here (now Presbyterian) of which the citizens seem quite proud, I can't perceive why. Glances at Europe |Horace Greeley 

Inside the walls of Manila there is only one Spanish parochial church, namely, the cathedral. The Philippine Islands, 1493-1898, Volume XX, 1621-1624 |Various 

There is a six-horse steam engine in use in Chester Cathedral (installed 1876). The Recent Revolution in Organ Building |George Laing Miller