I also like a slightly wider tire over the stock rubber, but I usually only get an inch larger diameter tire. Six ways to make your ATV even more rugged |By Tyler Freel/Outdoor Life |September 17, 2020 |Popular-Science 

For now, the team plans to set the diameter of the cones by hand. How special relativity can help AI predict the future |Will Heaven |August 28, 2020 |MIT Technology Review 

Both are tiny—Phobos, the larger, is 14 miles in diameter, scarcely bigger than Manhattan Island—but they also circle extremely close to the planet. These Images Expose the Dark Side of the Solar System - Issue 89: The Dark Side |Corey S. Powell |August 26, 2020 |Nautilus 

In contrast, a raindrop’s diameter can be 20 to 30 times that size. Explainer: Rainbows, fogbows and their eerie cousins |Matthew Cappucci |May 1, 2020 |Science News For Students 

Those deep sockets can increase the eye’s visual ability without increasing its diameter. This dinosaur was no bigger than a hummingbird |Carolyn Gramling |April 13, 2020 |Science News For Students 

Kepler-186f is about 11 percent larger than Earth in diameter, which means it has nearly 25% more surface area. What Does the Discovery of “Another Earth” Mean for Us? |Matthew R. Francis |April 18, 2014 |DAILY BEAST 

An array of whittled bamboo sticks, each four millimeters in diameter, makes up the two-room installation. The Royal Academy Wants You to Finish This Artwork |Chloë Ashby |January 24, 2014 |DAILY BEAST 

The doctors discovered the entry wound into the frontal lobe with a diameter about equal to that of a cigarette. The Little Syrian Girl With a Bullet in Her Head |Gregory Beals |November 29, 2013 |DAILY BEAST 

Using a 2-inch (5cm) diameter ring cutter, cut the scallops into straight cylinders; reserve the trim. Daniel Boulud Reveals His 4 Favorite Recipes From His New Cookbook |Daniel Boulud |October 15, 2013 |DAILY BEAST 

Pi is officially defined as the ratio of the circumference of a circle to its diameter. 17 Facts to Crack the Mystery of Pi |Abby Haglage |March 14, 2013 |DAILY BEAST 

In cross-section the burrows varied from round (three inches in diameter) to oval (three inches high and four inches wide). Summer Birds From the Yucatan Peninsula |Erwin E. Klaas 

Reckoning that Neptune is the outermost planet of the solar system, that system would have a diameter of 5,584 millions of miles. God and my Neighbour |Robert Blatchford 

I think 6½ feet diameter for the fly, and 9½ inches diameter for the small wheel, will give speed enough to the drum. Life of Richard Trevithick, Volume II (of 2) |Francis Trevithick 

The organism is an actively motile spiral thread, about four times the diameter of a red corpuscle in length. A Manual of Clinical Diagnosis |James Campbell Todd 

The care shown in rearing insures a perfect straightness of stem, and an equable diameter of about an inch or an inch and a half. Tobacco; Its History, Varieties, Culture, Manufacture and Commerce |E. R. Billings.