You want it to cover your shoulders and fall a bit below your toes for ideal weight distribution. Best weighted blanket: Sleep like a baby with our comfy bedding picks |PopSci Commerce Team |February 12, 2021 |Popular-Science 

The ideal chair should be about 16 to 21 inches above the ground, and your arms should rest comfortably while your feet remain flat. Best desk chair for any home office |PopSci Commerce Team |February 11, 2021 |Popular-Science 

On one hand, the year-long shutdown, with less rushing around, has provided ideal circumstances for serious chats. Watching edgier TV with your kids during the pandemic? You’re not alone. |Bonnie Miller Rubin |February 11, 2021 |Washington Post 

Makridis and Wu’s research underscores how the ongoing erosion of social trust in the United States created an ideal environment for a pandemic to flourish. Researchers identify social factors inoculating some communities against coronavirus |Christopher Ingraham |February 11, 2021 |Washington Post 

The climate in Death Valley is ideal during the winter months. 4 Awesome Winter Road Trips to National Parks |Megan Michelson |February 11, 2021 |Outside Online 

White and Crandall agree that low-intensity workouts are ideal. 5 Hangover Cures to Save You After a Few Too Many |DailyBurn |December 19, 2014 |DAILY BEAST 

The land at Easter Elchies was the ideal place for Reid to set up his business. Ester Elchies, The Estate Built By Whiskey | |December 10, 2014 |DAILY BEAST 

I reached out with heartfelt pleas, and spoke to people that I felt were ideal—and I was right in most cases. Idris Elba on Eric Garner, ‘Mi Mandela,’ and Selling Weed to Dave Chappelle |Marlow Stern |December 6, 2014 |DAILY BEAST 

However the American sexual ideal is intimately related to a certain idea of masculinity. Living Black & Gay in the ’50s |Alain Mabanckou |December 3, 2014 |DAILY BEAST 

The problem comes at this point because the liquid loses its potency within an hour even in ideal conditions. Powdered Measles Vaccine Could Be Huge for Developing World |Kent Sepkowitz |December 2, 2014 |DAILY BEAST 

I believe that these are ideal characters constructed from still more ancient legends and traditions. God and my Neighbour |Robert Blatchford 

That title of Castile might become the cherished ideal in the Philippines if it were valued as I desire. The Philippine Islands |John Foreman 

It makes one believe that fundamentally the country must be sound—that unswerving fidelity to an ideal. Ancestors |Gertrude Atherton 

The ideal is not a thing to be clutched at, or taken by force, but all of the conditions—every tittle—must be fulfilled. Solomon and Solomonic Literature |Moncure Daniel Conway 

Awe stole upon him; he felt himself included in the great ideal of this older day. The Wave |Algernon Blackwood