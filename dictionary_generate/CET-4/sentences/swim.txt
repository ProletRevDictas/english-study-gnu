They have an IP68 rating, fit comfortably under a swim cap, and can be submerged in 2 meters of water for up to two hours at a time. The best bone-conduction headphones for sound and safety |Tony Ware |August 23, 2021 |Popular-Science 

Also, consider making the jump to a saltwater above-ground pool for a more natural, soothing swim. The best above-ground pool: Have a splash in your backyard |Irena Collaku |August 12, 2021 |Popular-Science 

It’s prime mountain biking and trail-running terrain, with quiet beaches for a lonely swim all along the way. The 25 Best Fall Trips in the World |jversteegh |August 9, 2021 |Outside Online 

When you go for a swim in the ocean after applying sunscreen, or shower at the end of the day, some can wash off your body and end up in waterways. Almost Every Doctor Recommends Sunscreen. So Why Don't We Know More About Its Safety? |Jamie Ducharme |August 2, 2021 |Time 

Somehow she had to wash off the loss and reset for the mile-long swim to come. Katie Ledecky’s Incredible Olympic Legacy |Alice Park/Tokyo |July 31, 2021 |Time 

Jones is a veteran of another beloved-yet-controversial animated series on Adult Swim, The Boondocks. ‘Black Dynamite’ Presents Police Brutality: The Musical |Stereo Williams |January 9, 2015 |DAILY BEAST 

Adult Swim airs ‘In Search of Miracle Man,’ its follow up to ‘Too Many Cooks,’ the deranged late-night comedy clip gone viral. There Are More 'Too Many Cooks' Where That First Fever Dream Came From |Kevin Fallon |November 11, 2014 |DAILY BEAST 

He walked down to the beach anyway, in the rain, and went for a long swim. Colm Toibin Describes The Creation Of His Quiet Masterpiece ‘Nora Webster’ |Jennie Yabroff |November 3, 2014 |DAILY BEAST 

Her downfall came about, because for a second she forgot that to swim in the shark pool, you have to always act like a shark. ‘Housewife Tycoon’ Took On ‘Mad Men’ NYC Real Estate Market and Won |Vicky Ward |October 26, 2014 |DAILY BEAST 

My father used to swim in these fountains, to cool off from the heat and to make my mother laugh. Those Kansas City Blues: A Family History |Katie Baker |October 24, 2014 |DAILY BEAST 

But, do you know, I have a notion to go down to the beach and take a good wash and even a little swim, before dinner? The Awakening and Selected Short Stories |Kate Chopin 

She felt her sails flapping about her, but none the less was she determined to reach her goal if she had to get out and swim. Ancestors |Gertrude Atherton 

Swim to the edge of the moat and, clambering out, take to his legs was naturally the first impulse. St. Martin's Summer |Rafael Sabatini 

He abandoned his grip of the wall and began to swim gently toward the eastern angle. St. Martin's Summer |Rafael Sabatini 

So, when they saw the land quite near, what did they do but leap overboard, and swim towards it! The Nursery, January 1873, Vol. XIII. |Various