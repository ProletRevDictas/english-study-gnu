A childhood filled with hardship, negligence or abuse can skew the system that regulates how the body responds to stress. Puberty may reboot the brain and behaviors |Esther Landhuis |August 27, 2020 |Science News For Students 

That helps older teens regulate their emotions more like an adult. Explainer: What is puberty? |Esther Landhuis |August 27, 2020 |Science News For Students 

Brockovich outlines how, according to the EPA, Tyson was the second-biggest waterway polluter in the country from 2010 to 2014, just ahead of the Department of Defense, and many of the pollutants it releases aren’t regulated or measured. Erin Brockovich Is at It Again |Heather Hansman |August 27, 2020 |Outside Online 

In recent decades, anthropologists and psychologists have tested the idea that rituals regulate emotions. Why do we miss the rituals put on hold by the COVID-19 pandemic? |Sujata Gupta |August 14, 2020 |Science News 

Back in 2016, the Lagos state government first attempted to regulate ride-hailing startups by requiring taxi companies register each operator with the state at the cost of $320 per car. Uber and Bolt are set to face expensive ride-hailing regulation in Africa’s largest city |Yomi Kazeem |August 11, 2020 |Quartz 

A few weeks ago, Reid called a vote on a Constitutional amendment that would allow Congress to regulate money in politics. Time is Money: How to Fix Outrageous Political Spending |Jim Arkedis |November 3, 2014 |DAILY BEAST 

The two talked about reproductive freedom (with a dig at recent efforts in Texas to highly regulate abortion clinics). Live from San Antonio: Women in the World Texas! |Women in the World |October 23, 2014 |DAILY BEAST 

Second, the Nobel Prize for economics went to Jean Tirole, who studies how to regulate politically powerful companies. The Supreme Court Is Weighing Corporate Power Yet Again |Zephyr Teachout |October 17, 2014 |DAILY BEAST 

Laws vary from state to state, and banks are essentially left to self-regulate. The Real Problem With Sperm Banks |Keli Goff |October 7, 2014 |DAILY BEAST 

Do you favor giving the state legislature the constitutional authority to regulate abortions, or do you oppose this? Tennessee Voters Face a Loaded Abortion Question |Eleanor Clift |October 4, 2014 |DAILY BEAST 

In his youngest days, when his mother used to regulate his food, she would stuff him full of rice. Our Little Korean Cousin |H. Lee M. Pike 

As the business may be prohibited, a municipality or other power may regulate and control his business. Putnam's Handy Law Book for the Layman |Albert Sidney Bolles 

Place the sticks on tiers about ten inches apart, and regulate the plants on the sticks. Tobacco; Its History, Varieties, Culture, Manufacture and Commerce |E. R. Billings. 

The statutes in some states regulate his duty in this regard; it is one that he cannot safely omit. Putnam's Handy Law Book for the Layman |Albert Sidney Bolles 

However arbitrary, there are certain policies that regulate all well organized institutions and corporate bodies. The Condition, Elevation, Emigration, and Destiny of the Colored People of the United States |Martin R. Delany