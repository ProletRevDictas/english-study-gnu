In quantum electrodynamics, making predictions involves calculating to a certain level of precision, leaving out terms that are less significant and more difficult to calculate. A measurement of positronium’s energy levels confounds scientists |Emily Conover |August 24, 2020 |Science News 

This imposes a ceiling on predictive precision — and on how well physicists can understand what quantum theory says. The Mathematical Structure of Particle Collisions Comes Into View |Charlie Wood |August 20, 2020 |Quanta Magazine 

It’s a very small effect, but gravitational waves have been observed with such high precision that we can already say that the graviton mass must be less than 10−21 electron volts. The Physicist Who Slayed Gravity’s Ghosts |Thomas Lewton |August 18, 2020 |Quanta Magazine 

As technology tracks and analyzes our behaviors and lifestyles with ever more precision—sometimes with our knowledge and sometimes without—the opportunities for others to remotely monitor our mental state is growing fast. Machines can spot mental health issues—if you hand over your personal data |Bobbie Johnson |August 13, 2020 |MIT Technology Review 

The final proof is like a precision instrument, which has just the right combination of ideas to produce the outcome they wanted. New Geometric Perspective Cracks Old Problem About Rectangles |Kevin Hartnett |June 25, 2020 |Quanta Magazine 

The precision it took to craft such a cohesive, wholly compelling work over 12 years is nothing short of remarkable. Oscars 2015: The Daily Beast’s Picks, From Scarlett Johansson to ‘Boyhood’ |Marlow Stern |January 6, 2015 |DAILY BEAST 

And increasingly smart navigation aids in the cockpit brought far greater precision and efficiency to route planning. Flight 8501 Poses Question: Are Modern Jets Too Automated to Fly? |Clive Irving |January 4, 2015 |DAILY BEAST 

While not all 86 million maintain positions of governance or public service, the Party's machinery runs on watchmaker precision. China’s Internet Is Freer Than You Think |Brendon Hong |December 27, 2014 |DAILY BEAST 

It takes Sharp four hours to get into character: “I take joy in the mathematical, symmetrical precision and perfectness of Bach.” The Brit Who Stormed Broadway |Tim Teeman |December 7, 2014 |DAILY BEAST 

We do know with great precision what happened to the expelled student. Stonewall Jackson, VMI’s Most Embattled Professor |S. C. Gwynne |November 29, 2014 |DAILY BEAST 

He was engaged in the Encyclopedie, and his articles on grammar are drawn up with great precision, correctness and judgment. The Every Day Book of History and Chronology |Joel Munsell 

Then turning again to Doctor Kraus, he said slowly and clearly, enunciating each word with care and precision. Hooded Detective, Volume III No. 2, January, 1942 |Various 

They divide the day into ten thousand minutes, and know, with the greatest precision, what minute is favorable or otherwise. A Philosophical Dictionary, Volume 1 (of 10) |Franois-Marie Arouet (AKA Voltaire) 

The sight of those two hands, moving with their usual skill and precision, woke her out of her dream. Summer |Edith Wharton 

Um-ko lifted them, dusted the velvet thongs, and placed them with mathematical precision side by side upon the flat stone. The Dragon Painter |Mary McNeil Fenollosa