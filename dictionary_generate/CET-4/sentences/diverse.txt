By having a very broad and diverse ambassador and influencer network, it allows us to become a very inclusive brand. Fabletics’ Adam Goldenberg and Kevin Hart on what’s next for the activewear empire |Lucas Matney |September 17, 2020 |TechCrunch 

London Met’s diverse intake, which includes a high proportion of mature and postgraduate students, have helped it stay on track to meet its recruitment targets this year. UK Universities Predicted a COVID-19 Crash. They Got the Opposite |Fiona Zublin |September 17, 2020 |Ozy 

Of the 12 directors at McDonald’s, half are women or racially diverse, including the chairman of the board. Why McDonald’s sets the standard for equitable business models |jakemeth |September 16, 2020 |Fortune 

It’s encouraging to see international organizations trying to include more diverse perspectives in their discussions about AI. AI ethics groups are repeating one of society’s classic mistakes |Amy Nordrum |September 14, 2020 |MIT Technology Review 

By the 2000s, the neighborhood had lost nearly two thirds of its population, and was racially diverse but desperately poor. The Big Corporate Rescue and the America That’s Too Small to Save |by Lydia DePillis, Justin Elliott and Paul Kiel |September 12, 2020 |ProPublica 

Congress is now 92 percent Christian, resembling more to a papal enclave than our religiously diverse nation. The Unbearable Whiteness of Congress |Dean Obeidallah |January 8, 2015 |DAILY BEAST 

So, why no Jewess in the mix of more recent and diverse Miss Americas? Why Was Bess Myerson the First and Last Jewish Miss America? |Emily Shire |January 7, 2015 |DAILY BEAST 

Regal Entertainment Group is the biggest and most geographically diverse theater company in the country. The Right-Wing Billionaire Who Bowed to North Korea over ‘The Interview’ |Asawin Suebsaeng |December 19, 2014 |DAILY BEAST 

We are a huge, complex, diverse country still offering freedom, opportunity and hope. Dick Cheney vs. ‘Unbroken’ |Mike Barnicle |December 15, 2014 |DAILY BEAST 

The characters you play on the show are extremely diverse—ranging from a cocaine-rattled rich boy to an ornery Jewish grandpa. The Zany Shades of Nick Kroll |Abby Haglage |December 15, 2014 |DAILY BEAST 

On this point, I have the testimony of eye-witnesses of diverse sentiments and of unimpeachable character. Glances at Europe |Horace Greeley 

They rather adopted and purified it for Christian purposes, just as they did the diverse elements of ancient civilization. The Catacombs of Rome |William Henry Withrow 

It might be a temple; it might be a hall for the transaction of public business; such were the diverse guesses of the travellers. Overland |John William De Forest 

Thence to Westminster Hall, and there met with diverse people, it being terme time. Diary of Samuel Pepys, Complete |Samuel Pepys 

Two men of such diverse character could probably have never worked cordially together. The Life of Mazzini |Bolton King