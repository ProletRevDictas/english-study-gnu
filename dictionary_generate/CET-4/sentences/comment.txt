The former officials either did not respond to requests for comment or declined to discuss their views. Former Pence aide says she will vote for Biden because of Trump’s ‘flat out disregard for human life’ during pandemic |Josh Dawsey |September 17, 2020 |Washington Post 

The comments were later confirmed in part by The Washington Post and other news organizations. Trump, in town hall, says he wouldn’t have done anything differently on pandemic |Colby Itkowitz, Josh Dawsey, Felicia Sonmez, John Wagner |September 16, 2020 |Washington Post 

That office did not immediately return a call seeking comment. ICE Deported a Woman Who Accused Guards of Sexual Assault While the Feds Were Still Investigating the Incident |by Lomi Kriel |September 15, 2020 |ProPublica 

As ProPublica reported last year, contracting documents indicated the temp staff might be involved in substantively assessing the comments, which could violate federal contracting rules. “Cover Up”: House Democrats Subpoena Documents That NLRB Refused to Share in Ethics Investigation |by Ian MacDougall |September 15, 2020 |ProPublica 

The Chicken Council didn’t return calls or emails seeking comment. Emails Show the Meatpacking Industry Drafted an Executive Order to Keep Plants Open |by Michael Grabell and Bernice Yeung |September 14, 2020 |ProPublica 

It was hard not to take it as a sign, a personal comment on my own Jewish dating failings. My Week on Jewish Tinder |Emily Shire |January 5, 2015 |DAILY BEAST 

Maxwell was not available for comment but has described all claims against her as “untrue” and “obvious lies.” From Playboy Prince to Dirty Old Man? |Tom Sykes |January 5, 2015 |DAILY BEAST 

Maxwell was not available for comment describes all claims against her as “untrue” and “obvious lies.” Buckingham Palace Disputes Sex Allegations Against Prince ‘Randy Andy’ |Tom Sykes |January 4, 2015 |DAILY BEAST 

The Italian foreign ministry has declined to comment on the video. Jihadis Release New Year’s Eve Video of Italian Female Hostages |Jamie Dettmer, Barbie Latza Nadeau |January 2, 2015 |DAILY BEAST 

The FCC investigation recently closed its comment period on the Marriott case. How ‘Ethical’ Hotel Chain Marriott Gouges Guests in the Name of Wi-Fi Security |Kyle Chayka |December 31, 2014 |DAILY BEAST 

Any one may possess the portrait of a tragedian without exciting suspicion or comment. The Awakening and Selected Short Stories |Kate Chopin 

The facts have been stated very simply, plus one brief general comment. Gallipoli Diary, Volume I |Ian Hamilton 

Again he stopped, but I made no comment, only waited with breathless interest till he went on. Uncanny Tales |Various 

She placed it in Edna's hands, and without further comment arose and went to the piano. The Awakening and Selected Short Stories |Kate Chopin 

Besides,” Burd Alling said in comment on this, “for a good cause we are all ready and willing to be bunkoed a little. The Campfire Girls of Roselawn |Margaret Penrose