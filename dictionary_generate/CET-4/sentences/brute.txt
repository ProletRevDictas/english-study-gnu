Finally, Apple blocked hackers from trying “brute force” attacks over and over in rapid succession. Google says it’s too easy for hackers to find new security flaws |Bobbie Johnson |February 3, 2021 |MIT Technology Review 

That wasn’t a prohibitively large number, and so some solvers, like Siddhartha Srivastava of Patna, India, powered forth with computational brute force to find the unique solution. Can You Skillfully Ski The Slopes? |Zach Wissner-Gross |January 22, 2021 |FiveThirtyEight 

If you don’t own one or just want a bit more of a workout, then a wooden spoon and brute force will do the trick. Potatoes aren’t the only vegetables you should be mashing |Aaron Hutcherson |January 20, 2021 |Washington Post 

Sinovac’s inactivated vaccine relies on a brute-force technology that has been around for hundreds of years. A drugmaker’s COVID-19 vaccine is 78% effective—but can it escape its spotty corporate history? |Grady McGregor |January 8, 2021 |Fortune 

Only Cleveland’s Nick Chubb was better at extending plays with brute force. The ultimate playoff fantasy football rankings |Neil Greenberg |January 7, 2021 |Washington Post 

Brute is the story of Mac and Jesse, two disenfranchised teens who turn to robbing houses as a form of recreation and quick cash. Nitehawk Shorts Festival: ‘Brute,’ a Twisted Take on Playing in the Dark |Julia Grinberg |November 28, 2014 |DAILY BEAST 

They knew that as much as they tried to make up for it with brute force, their cultural power was nil. Of Gamers, Gates, and Disco Demolition: The Roots of Reactionary Rage |Arthur Chu |October 16, 2014 |DAILY BEAST 

Police ignored this advice and instead used brute force to remove Saylor. Police Brutality's Hidden Victims: The Disabled |Elizabeth Heideman |September 8, 2014 |DAILY BEAST 

“It was no brute whom Smiley was pursuing with such mastery, no unqualified fanatic after all, no automaton,” le Carré writes. Iran’s Top Spy Is the Modern-Day Karla, John Le Carré’s Villainous Mastermind |Michael Weiss |July 2, 2014 |DAILY BEAST 

And on Game of Thrones, it's never wise to bet on good intentions over brute force. Game of Thrones’ 'The Watchers on the Wall': The Battle of Castle Black Is One For the Ages |Andrew Romano |June 9, 2014 |DAILY BEAST 

He has become by brute force of circumstances a sort of collectivist, puzzled only as to how much of a collectivist to be. The Unsolved Riddle of Social Justice |Stephen Leacock 

The eyes of the huge brute opened instantly, and he had half risen before the loud report of the gun rang through the thicket. Hunting the Lions |R.M. Ballantyne 

Saying this he advanced towards the brute, but again the powerful hand of Mafuta seized him. Hunting the Lions |R.M. Ballantyne 

He recovered himself, however, in a moment, and fired—sending a ball into the brute which just touched the brain and stunned it. Hunting the Lions |R.M. Ballantyne 

It wasn't any scruple of mercy, for Hicks was as cold-blooded a brute as ever glanced down a gun-barrel. Raw Gold |Bertrand W. Sinclair