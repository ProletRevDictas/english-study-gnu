There are many different ways to make snowshoes, and the crudest and quickest method is to strap a bundle of evergreen boughs underneath each foot—though it’s a cumbersome task that does little to improve traction. How to build snowshoes on the fly—and 4 other tips for surviving deep snow |By Tim MacWelch/Outdoor Life |January 5, 2021 |Popular-Science 

To make a bough bed, you can use leaves, grass, evergreen boughs, or other plant material for insulation. Winter survival shelters you should know how to build |By Tim MacWelch/Outdoor Life |December 21, 2020 |Popular-Science 

Add a bough bedWhile this is not a shelter by itself, a thick bough bed makes an outstanding addition to any type of shelter large enough to accommodate it. Winter survival shelters you should know how to build |By Tim MacWelch/Outdoor Life |December 21, 2020 |Popular-Science 

Since a fire would melt the snow covered boughs overhead, your best bet for warmth is packing the well with insulating materials. Winter survival shelters you should know how to build |By Tim MacWelch/Outdoor Life |December 21, 2020 |Popular-Science 

In the event the snow won’t hold together to build solid building blocks or you are caught without tools, you can still dig out a trench and cover it with a flat roof of poles, tree boughs, and an insulating layer of snow. Winter survival shelters you should know how to build |By Tim MacWelch/Outdoor Life |December 21, 2020 |Popular-Science 

A couple of innocuous paragraphs are all that appeared in the privately printed book, The Fruitful Bough. Inside the Kennedy Death Threats |Adam Clymer |June 14, 2010 |DAILY BEAST 

No man should regard the subject of religion as decided for him until he has read The Golden Bough. God and my Neighbour |Robert Blatchford 

Outside Rome, showing the same ideas at work among neighbouring peoples, was the 'golden bough' in the grove of Diana at Aricia. The Religion of Ancient Rome |Cyril Bailey 

A few more shots put an end to its existence, and we then pulled up under the bough on which it was hanging. A Woman's Journey Round the World |Ida Pfeiffer 

Wandering from the parent bough,Little, trembling leaf,Whither goest thou? The Poems of Giacomo Leopardi |Giacomo Leopardi 

The birds were singing, black squirrels were jumping from bough to bough, and they could hear the tapping of the woodpecker. Mrs. Falchion, Complete |Gilbert Parker