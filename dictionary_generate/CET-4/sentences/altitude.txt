She cautions it cannot serve as an end-all solution for scanning debris of all sizes and altitudes—but should make for another useful tool in the debris tracking toolbelt. How to cast a wider net for tracking space junk |Neel Patel |August 5, 2020 |MIT Technology Review 

Surprisingly, another yellow-rumped leaf-eared mouse was found at sea level, indicating that this species has the broadest altitude distribution of any mammal, in addition to the altitude record. A South American mouse is the world’s highest-dwelling mammal |Jack J. Lee |July 29, 2020 |Science News 

Even at mile-high altitude, pitching will be key to any success for the Rockies, given that they project to have some of the poorest production in the majors at catcher, first base, left field and center field, according to FanGraphs. The Dodgers Lead Our National League Predictions, But Don’t Count Out The Nats Or … Reds? |Travis Sawchik |July 22, 2020 |FiveThirtyEight 

In the last few decades, turbine heights have more than tripled to go after greater gusts at higher altitudes, and they’ve sprouted rotors long as football fields to more efficiently catch all that energy. GE Will 3D Print the Bases of Wind Turbines Taller Than Seattle’s Space Needle |Jason Dorrier |June 21, 2020 |Singularity Hub 

One especially large, long-lasting smoke plume rose to a record altitude while spinning and wrapping itself in rotating winds. Smoke from Australian fires rose higher into the ozone layer than ever before |Maria Temming |June 15, 2020 |Science News 

Specifically, the pilots got themselves into a high altitude stall, where the wings lose the capacity to provide lift. Flight 8501 Poses Question: Are Modern Jets Too Automated to Fly? |Clive Irving |January 4, 2015 |DAILY BEAST 

Whatever happened overtook them both within a minute or so of that altitude change request, and they were never heard from again. Flight 8501 Poses Question: Are Modern Jets Too Automated to Fly? |Clive Irving |January 4, 2015 |DAILY BEAST 

These skills are particularly needed when, as in the case of the AirAsia flight, the airplane is at cruise altitude. Did Bad Weather Bring Down AirAsia 8501? |Clive Irving |December 29, 2014 |DAILY BEAST 

This high-altitude pine needs to be 50-80 years old before it even begins to produce cones. What It Takes to Kill a Grizzly Bear |Doug Peacock |November 23, 2014 |DAILY BEAST 

And now that you mention it, I also got seasick, and had altitude sickness, and had to be rescued a few times. The Real-Life Raiders of the Lost Ark |Alex Belth |November 14, 2014 |DAILY BEAST 

At this place he obtained an indifferent meridian altitude which placed it in 16 degrees 40 minutes 18 seconds South. Narrative of a Survey of the Intertropical and Western Coasts of Australia] [Volume 2 of 2] |Phillip Parker King 

Livingstone gives an account of a variety that attained an altitude much higher than the American plant. Tobacco; Its History, Varieties, Culture, Manufacture and Commerce |E. R. Billings. 

Those produced by the moon would have an altitude of about one foot, and those by the sun of about three inches. Outlines of the Earth's History |Nathaniel Southgate Shaler 

It is likely that in a general way the ocean surges which beat against the coast are of greater altitude. Outlines of the Earth's History |Nathaniel Southgate Shaler 

Not only should the altitude of the plant be taken into account, but also the size and texture of the leaf. Tobacco; Its History, Varieties, Culture, Manufacture and Commerce |E. R. Billings.