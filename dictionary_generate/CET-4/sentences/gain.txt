Because if I ask the question, there’s a chance I might blow up a personal relationship or ruin a work relationship, whereas the gains to be made are, in some cases at least not life-changing. Can I Ask You a Ridiculously Personal Question? (Ep. 451) |Stephen J. Dubner |February 11, 2021 |Freakonomics 

On average, participants in each of the program’s training groups saw similar gains. Want to Improve Your Running? Focus on Recovery. |Outside Editors |February 5, 2021 |Outside Online 

The stunning losses followed spectacular gains last month, which drew in many new investors chasing the hype and potential profits. Robinhood lifts GameStop restrictions as U.S. markets close in on best week since November |Hamza Shaban |February 5, 2021 |Washington Post 

Broad-based gains in tech, autos and pharma are pushing global equities ever closer to an all-time high. Here’s what’s driving global stocks to new all-time highs |Bernhard Warner |February 5, 2021 |Fortune 

Republicans now expect to make gains in upcoming redistricting that could exceed the current 10-seat margin of Democratic control. Republicans worry their big tent will mean big problems in 2022 elections |Michael Scherer, Josh Dawsey |February 4, 2021 |Washington Post 

The email appears to have been a relatively common attempt to gain personal information from a wide range of unwitting victims. Was Sony Hit With a Second Hack? |Shane Harris |January 8, 2015 |DAILY BEAST 

In an effort to gain early attention, he focused his attention on the Iowa precinct caucuses, which had never mattered much. The World’s Toughest Political Quiz |Jeff Greenfield |December 31, 2014 |DAILY BEAST 

How does it happen that citizens of modest means suffer as public sector unions gain? How Public Sector Unions Divide the Democrats |Daniel DiSalvo |December 29, 2014 |DAILY BEAST 

But Goff called it “insulting” to suggest that he might be running a super PAC for personal gain. ‘Ready for Romney’ Is Amateur Hour |Tim Mak |December 23, 2014 |DAILY BEAST 

Those with a slightly sleazier bent have dredged up reports of his weight gain, substance abuse, and arrest. D’Angelo’s ‘Black Messiah’ Was Worth Waiting 15 Years For |James Joiner |December 16, 2014 |DAILY BEAST 

Renounce the good law of the worshippers of Mazda, and thou shalt gain such a boon as the Murderer gained, the ruler of nations. Solomon and Solomonic Literature |Moncure Daniel Conway 

To add point to this success, he knew that the victor of Montebello was straining every nerve to gain this very prize. Napoleon's Marshals |R. P. Dunn-Pattison 

How great glory did he gain when he lifted up his hands, and stretched out swords against the cities? The Bible, Douay-Rheims Version |Various 

We have other things to engage us now, but I sometimes think all is not gain that the march of progress brings. Fifty Years of Railway Life in England, Scotland and Ireland |Joseph Tatlow 

The power to expel members is incident to every society or association unless organized primarily for gain. Putnam's Handy Law Book for the Layman |Albert Sidney Bolles