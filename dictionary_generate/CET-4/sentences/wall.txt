Mount them on the wall in your kiddo’s bedroom, or build a backyard climbing wall like this one. A 15-Week Exercise Plan for Kids and Families |Krista Langlois |September 12, 2020 |Outside Online 

Drone imagery alone can’t establish whether rituals occurred at the buried earthwork or if, perhaps, non-combatants hid behind walls along its borders when the site was attacked. Drones find signs of a Native American ‘Great Settlement’ beneath a Kansas pasture |Bruce Bower |September 10, 2020 |Science News 

This universal wall offers a lifetime’s worth of routes, from V4 to V14, on an eight-by-twelve-foot surface. The Most Futuristic Workout Gear of 2020 |Hayden Carpenter |September 5, 2020 |Outside Online 

Recently, the actor let down his walls a bit to tell the story of how he met his wife, Tomasina Tate, 20 years ago—a fate made possible by fellow actor Jamie Foxx. Larenz Tate Reveals How Jamie Foxx Helped Him Meet His Wife Tomasina |Hope Wright |September 4, 2020 |Essence.com 

Ocean Spray isn’t the only legacy brand looking to build out new brands within its walls. ‘We have to be open to failure’: Why Ocean Spray launched a brand incubator for the DTC era |Kristina Monllos |September 1, 2020 |Digiday 

A Wall Street person should not be allowed to help oversee the Dodd-Frank reforms. Antonio Weiss Is Not Part of the Problem |Steve Rattner |January 7, 2015 |DAILY BEAST 

It was a brick wall that we turned into the on-ramp of a highway. Coffee Talk with Fred Armisen: On ‘Portlandia,’ Meeting Obama, and Taylor Swift’s Greatness |Marlow Stern |January 7, 2015 |DAILY BEAST 

It reminded me a bit of an alternative take on The Wolf of Wall Street—through the Toni and Candace lens. Coffee Talk with Fred Armisen: On ‘Portlandia,’ Meeting Obama, and Taylor Swift’s Greatness |Marlow Stern |January 7, 2015 |DAILY BEAST 

Marvin hops over the edge of his retaining wall, which he built. The Story Behind Lee Marvin’s Liberty Valance Smile |Robert Ward |January 3, 2015 |DAILY BEAST 

He scrambled outside to find a 25-foot-wide crater just beyond the mud wall surrounding his family compound. The Dangerous Drug-Funded Secret War Between Iran and Pakistan |Umar Farooq |December 29, 2014 |DAILY BEAST 

A flash of surprise and pleasure lit the fine eyes of the haughty beauty perched up there on the palace wall. The Red Year |Louis Tracy 

Kind of a reception-room in there—guess I know a reception-room from a hole in the wall. Boys and Girls Bookshelf; a Practical Plan of Character Building, Volume I (of 17) |Various 

Distance, the uncertain light, and imagination, magnified it to a high wall; high as the wall of China. The Giant of the North |R.M. Ballantyne 

He leant against the wall of his refuge, notwithstanding this boast, and licked the ice to moisten his parched lips. The Giant of the North |R.M. Ballantyne 

A little boy aged two years and four months was deprived of a pencil from Thursday to Sunday for scribbling on the wall-paper. Children's Ways |James Sully