Ember just raised $13 million for its popular, temperature-controlled mugs Ember names former Dyson head as consumer CEO, as the startup looks beyond the smart mug |Brian Heater |February 12, 2021 |TechCrunch 

Discard the hot water in the mugs, ladle the mixture into your mugs and garnish as you like. Boozy hot chocolate recaptures the magic of a childhood snow day with a grown-up twist |M. Carrie Allan |February 5, 2021 |Washington Post 

As I poured it into a mug more appropriate for the indoors, it was still steaming. This Zojirushi Mug Makes My Winter Walks Tolerable |Monica Burton |January 29, 2021 |Eater 

I sat down at her kitchen table while she poured coffee into a mug in front of me. She’s 90 and Italian. I’m in my 30s and half Indian. Here’s why we’re the closest of friends. |Raj Tawney |January 27, 2021 |Washington Post 

The handle is extended for grip comfort and the uncrackable Duracoat coating keeps the mug dry in your hand. Stainless steel cups for any beverage |PopSci Commerce Team |January 14, 2021 |Popular-Science 

To this day, Bush media maven Roger Ailes adamantly denies that he or the campaign had any role in the Willie Horton mug shot ad. Want President Hillary? Then Primary Her |Jeff Greenfield |November 24, 2014 |DAILY BEAST 

He plants himself on an outdoor couch, stirs Nesquik into a mug, and leans forward. A Belgian Prince, Gorillas, Guerrillas & the Future of the Congo |Nina Strochlic |November 6, 2014 |DAILY BEAST 

Beside the mug shot of McCollum was one of a man named Wayne Laws. How the North Carolina GOP Made a Wrongfully Convicted Man a Death Row Scapegoat |Michael Daly |September 4, 2014 |DAILY BEAST 

You know, he dug up 32-year-old mug shots of me that I had never even seen before, that had never been posted. The Weirdest Story About a Conservative Obsession, a Convicted Bomber, and Taylor Swift You Have Ever Read |David Weigel |August 30, 2014 |DAILY BEAST 

When he turned himself in, he wore a smirk in his mug shot, and then he went out for ice cream with reporters in tow. Vote for the (Alleged) Crooks! How Rick Perry, Chris Christie, and Scott Walker are Running While Under Investigation |Olivia Nuzzi |August 29, 2014 |DAILY BEAST 

In Tiefurt we partook of a magnificent collation consisting of a mug of beer, brown bread and sausage! Music-Study in Germany |Amy Fay 

He put his hand to his belt, screwed up his mug, and said he felt plumb et up inside. Alec Lloyd, Cowpuncher |Eleanor Gates 

Ever see anything more fetching than those great Irish eyes in a regular little Dago mug? Ancestors |Gertrude Atherton 

The skipper of the smack invited Jim to go below, and handed him a steaming mug of tea. The Chequers |James Runciman 

He went into the room below, knocked the neck off a wine bottle and poured the contents into a mug and drank, smacking his lips. The Light That Lures |Percy Brebner