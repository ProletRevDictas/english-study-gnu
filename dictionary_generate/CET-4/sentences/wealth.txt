His proposal focuses on delivering equitable solutions to close racial gaps in health, education, environmental justice and wealth. Trending News For Our Community |Tanya Christian |August 28, 2020 |Essence.com 

Bloomberg reports that, in China, HSBC has ramped up investment in commercial banking, credit cards, investment banking, and wealth management, and now employs more than 8,000 in 170 outlets in the country. HSBC walks the tightrope between Beijing and the U.S. |claychandler |August 27, 2020 |Fortune 

Crypto—once reserved for the gamers, coders, and early tech millionaires—is now a place where more people have the opportunity to participate in an alternative system where they can have more control over their wealth. Why 2020 might be the year cryptocurrency goes mainstream |jakemeth |August 24, 2020 |Fortune 

Gorton scolded Giannulli for what he described as “breathtaking fraud” made possible by his wealth and privilege. Lori Loughlin gets two months in prison after judge accepts plea deal in college bribery scandal |radmarya |August 21, 2020 |Fortune 

A pandemic-spurred rise in e-learning also raises questions about whether pricey university educations are worth the cost, when there are a wealth of online options. Is School Out Forever? |Daniel Malloy |August 9, 2020 |Ozy 

The Perfect Storm writer talks combat brotherhood and the threat posed by growing wealth inequality. Sebastian Junger on War, Loss, and a Divided America |The Daily Beast Video |January 1, 2015 |DAILY BEAST 

The second, and perhaps more surprising, is the wealth of human capital already existent in the region. The Rustbelt Roars Back From the Dead |Joel Kotkin, Richey Piiparinen |December 7, 2014 |DAILY BEAST 

Here, black children are born into families with about 10 percent—one-tenth—the average wealth of white families. The Hunger Games Economy |Jedediah Purdy |November 29, 2014 |DAILY BEAST 

Why have educational outcomes so stubbornly flat-lined in the face of this wealth of educational resources? How a GOP Senate Can Help the Poor |Veronique de Rugy |November 23, 2014 |DAILY BEAST 

He was a scion of immense wealth, a civil rights activist, and an art collector and patron. This Republican Loved Taxes & Modern Art |Scott Porch |November 19, 2014 |DAILY BEAST 

The old earl's property, the source of his wealth, as from his title the reader will have shrewdly guessed, was in collieries. The Pit Town Coronet, Volume I (of 3) |Charles James Wills 

It is a lofty and richly-decorated pile of the fourteenth century; and tells of the labours and the wealth of a foreign land. Blackwood's Edinburgh Magazine, Volume 60, No. 372, October 1846 |Various 

He saw with evident pleasure the outward and visible signs of the old earl's immense wealth. The Pit Town Coronet, Volume I (of 3) |Charles James Wills 

If wealth were always thus employed, it were a pity that great fortunes are not more numerous. Glances at Europe |Horace Greeley 

Here and there exceptional industry or extraordinary capacity raised the artisan to wealth and turned the "man" into the "master." The Unsolved Riddle of Social Justice |Stephen Leacock