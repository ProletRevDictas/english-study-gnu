By March, regulators were desperately trying to stem the tide of price gouging flooding online retailers, especially Amazon's sprawling third-party Marketplace. Price gouging and defective products rampant on Amazon, reports find |Kate Cox |September 11, 2020 |Ars Technica 

The pause stemmed from a standard review of the company’s vaccine trials after one person developed an unexplained illness, AstraZeneca said in a statement. Some scientists downplay significance of AstraZeneca’s COVID-19 vaccine trial halt |Claire Zillman, reporter |September 9, 2020 |Fortune 

The commission recommends more research on using stem cells to produce eggs and sperm in lab dishes, which could then be used to create embryos that don’t carry genetic diseases. Strict new guidelines lay out a path to heritable human gene editing |Tina Hesman Saey |September 3, 2020 |Science News 

Julia Gaydina, co-founder of sustainable luxury clothing brand Infantium Victoria He spent the next 18 months trying to understand how to extract the calotropis gigantea fiber from its stem and pod without using any chemicals. How a Wasteland Shrub Is Becoming the Next Big Thing in Fashion |Daniel Malloy |August 28, 2020 |Ozy 

In 2019, approximately $919 million of craft distiller revenues stemmed from on-site sales. Craft distillers have lost out on more than $700 million in sales because of the pandemic |Rachel King |August 20, 2020 |Fortune 

Republican legislatures are looking for any way to stem the tide, and religious exemptions are one way to do that. RFRA Madness: What’s Next for Anti-Democratic ‘Religious Exemptions’ |Jay Michaelson |November 16, 2014 |DAILY BEAST 

The researchers first isolated a set of cells known as neural stem cells (NSCs) from the brains of rats. Fish Oil, Turmeric, and Ginseng, Oh My! Are ‘Brain Foods’ B.S.? |Dr. Anand Veeravagu, MD |October 10, 2014 |DAILY BEAST 

In part, it may stem from what looks like an increasingly “political” Court. Ruth Bader Ginsburg Levels With Us on Why She’s Not Retiring |Jeff Greenfield |September 25, 2014 |DAILY BEAST 

Seager writes about being threatened by a patient with a shank carved out of an eyeglass stem. Inside a Hospital for the Criminally Insane |Caitlin Dickson |September 15, 2014 |DAILY BEAST 

Anti-vaxxers and anti-stem cell-ers come together, thanks to a recent paper linking autism to vaccines that use stem cells. No, Stem Cells Don't Cause Autism |Kent Sepkowitz |September 11, 2014 |DAILY BEAST 

On the upper part of the stem the whorls are very close together, but they are more widely separated at the lower portion. How to Know the Ferns |S. Leonard Bastin 

The upper part of the stem is usually unbranched, but whorls of branches occur towards the base. How to Know the Ferns |S. Leonard Bastin 

As he spoke he wedged himself between Grandfather Mole and the stem of the toadstool umbrella. The Tale of Grandfather Mole |Arthur Scott Bailey 

A mushir (marshal) would find it derogatory to his dignity to smoke out of a stem less than two yards in length. Tobacco; Its History, Varieties, Culture, Manufacture and Commerce |E. R. Billings. 

The Chukchees use a pipe similar to those of the Eskimo, but with a much larger and shorter stem. Tobacco; Its History, Varieties, Culture, Manufacture and Commerce |E. R. Billings.