Without a direct avenue to customers, Apple has been at the mercy of third-party sellers. Apple’s iPhone sales have lagged in India for years. It’s only now unleashing its branding firepower |Grady McGregor |September 6, 2020 |Fortune 

When Mulan finally meets her, the witch says, “When they find out who you are, they will show you no mercy.” Women like Mulan didn’t need to go to war in disguise |Bethany Brookshire |September 4, 2020 |Science News For Students 

However, since their existing upfront deals did not provide pandemic-related escape clauses and the advertisers were at the mercy of the networks to excuse them. ‘Kind of squishy’: Advertisers lobby to add pandemic clauses to TV upfront deals |Tim Peterson |September 1, 2020 |Digiday 

The city was at the mercy of its own bureaucracy and individual family members who made up the trust had different views of what they should do with the property. The Deal Before the 101 Ash St. Debacle Helps Explain How We Got Here |Lisa Halverstadt and Jesse Marx |August 24, 2020 |Voice of San Diego 

So we will never again be at the mercy of China and other foreign countries in order to protect our own people. Biden faults Trump for mismanaging coronavirus in convention speech |Chris Johnson |August 21, 2020 |Washington Blade 

But give the Kingdom credit for its sense of mercy: The lashes will be administered only 50 at a time. In Defense of Blasphemy |Michael Tomasky |January 9, 2015 |DAILY BEAST 

But it never has been the site of equal mercy, and it never will be. No Gods, No Cops, No Masters |James Poulos |January 1, 2015 |DAILY BEAST 

If mercy is not preached by a national figure we take seriously, our battles over policy power will grow ever more merciless. No Gods, No Cops, No Masters |James Poulos |January 1, 2015 |DAILY BEAST 

Policy is about wielding power, while mercy is about transcending power by renouncing it. No Gods, No Cops, No Masters |James Poulos |January 1, 2015 |DAILY BEAST 

Canned drinks like Mercy contain up 5,000 percent of the daily value of certain vitamins. History's Craziest Hangover Cures |Justin Jones |December 30, 2014 |DAILY BEAST 

Brethren are a help in the time of trouble, but mercy shall deliver more than they. The Bible, Douay-Rheims Version |Various 

Have mercy on thy people, upon whom thy name is invoked: and upon Israel, whom thou hast raised up to be thy firstborn. The Bible, Douay-Rheims Version |Various 

If she have a tongue that can cure, and likewise mitigate and shew mercy: her husband is not like other men. The Bible, Douay-Rheims Version |Various 

Henceforth he must remember Winifred only when his sword was at the throat of some wretched mutineer appealing for mercy. The Red Year |Louis Tracy 

And thou hast delivered me, according to the multitude of the mercy of thy name, from them that did roar, prepared to devour. The Bible, Douay-Rheims Version |Various