I also noticed that on the sort of holidays and things like that, food was always really central. Tom Colicchio Hopes (and Fears) COVID-19 Will Change the Restaurant Industry |Pallabi Munsi |September 16, 2020 |Ozy 

To make the process faster, you should offer some sort of internal website search functionality. How to drive digital innovation necessary during the pandemic |Nick Chasinov |September 16, 2020 |Search Engine Watch 

The flipside, of course, is that this is James Harden — the sort of offensive talent that any coach would want the chance to build a system around if given the opportunity. Everything Should Be On The Table For The Houston Rockets. Even James Harden’s Future. |Chris Herring (chris.herring@fivethirtyeight.com) |September 14, 2020 |FiveThirtyEight 

To his surprise, he found quite a few reports describing this sort of immune cross-protection. ‘Trained Immunity’ Offers Hope in Fight Against Coronavirus |Esther Landhuis |September 14, 2020 |Quanta Magazine 

Stylistically, the Mystery Ship looks like a sort of dead end, but thematically, Craig Vetter knew exactly where motorcycles were headed. 22 of the weirdest concept motorcycles ever made |By John Burns/Cycle World |September 10, 2020 |Popular-Science 

Is it sort of evidence of the Gladwellian 10,000 hours theory? Coffee Talk with Fred Armisen: On ‘Portlandia,’ Meeting Obama, and Taylor Swift’s Greatness |Marlow Stern |January 7, 2015 |DAILY BEAST 

I had enough experiences around languages that it just sort of happened. Coffee Talk with Fred Armisen: On ‘Portlandia,’ Meeting Obama, and Taylor Swift’s Greatness |Marlow Stern |January 7, 2015 |DAILY BEAST 

When he was first incarcerated, he says some sort of paperwork snafu had him imprisoned under two different, but similar, names. His First Day Out Of Jail After 40 Years: Adjusting To Life Outside |Justin Rohrlich |January 3, 2015 |DAILY BEAST 

But I trusted Tony Robbins could sort me out on both fronts. Can Self-Help Books Really Make a New You? |Lizzie Crocker |December 29, 2014 |DAILY BEAST 

Luckily, Tor was prepared for this sort of assault, and has built-in defenses to protect against it. The Attack on the Hidden Internet |Marc Rogers |December 29, 2014 |DAILY BEAST 

This treacherous sort of calm, we thought, might forbode a storm, and we did not allow it to lull us into security. Blackwood's Edinburgh Magazine, No. CCCXXXIX. January, 1844. Vol. LV. |Various 

I want to see the sort of thing happening to schools that has already happened to many sorts of retail shops. The Salvaging Of Civilisation |H. G. (Herbert George) Wells 

It is no good settling down in a world that, on its part, refuses to do anything of the sort. The Salvaging Of Civilisation |H. G. (Herbert George) Wells 

Now in addition to that I had something else in my own college course—something of the same sort of thing but better. The Salvaging Of Civilisation |H. G. (Herbert George) Wells 

Presently he began to shiver so, with some sort of a chill, that I took off my coat and wrapped it round him. The Boarded-Up House |Augusta Huiell Seaman