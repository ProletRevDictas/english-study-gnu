In its first flexing of new muscle, the branch pored over the plans filed just a few years ago by private utilities, looking for inconsistencies or holes. Watchdog Warns: SDG&E’s Tree-Trimming Plan Could Worsen Wildfires |MacKenzie Elmer |August 24, 2020 |Voice of San Diego 

The alloy contracts like a muscle when heated, and extends once cool. Methanol fuel gives this tiny beetle bot the freedom to roam |Carmen Drahl |August 19, 2020 |Science News 

Hemp is a source of cannabidiol, also known as CBD, the cannabis-derived compound that consumers use for relief from muscle and joint pain, anxiety, and insomnia. The CBD-fueled hemp boom has gone bust |Jenni Avins |August 12, 2020 |Quartz 

It’s caused by mutations in the gene that makes dystrophin, a protein that serves to rebuild and strengthen muscle fibers in skeletal and cardiac muscles. A Year After Gene Therapy, Boys With Muscular Dystrophy Are Healthier and Stronger |Vanessa Bates Ramirez |July 30, 2020 |Singularity Hub 

“Working with children using our device, I’ve witnessed a physical moment where the brain “clicks” and starts moving the hand rather than focusing on moving the muscles,” LaChappelle said. This Startup Is 3D Printing Custom Prosthetics for a Fraction of the Standard Cost |Vanessa Bates Ramirez |July 22, 2020 |Singularity Hub 

Security guards have also been posted to add some muscle (but this has done little to deter vandals in past years). Sweden’s Burning Christmas Goat |Nina Strochlic |December 25, 2014 |DAILY BEAST 

Companies like Delta, Apple, and Nike flex their political muscle on behalf of gay rights. Corporations Are No Longer Silent on LGBT Issues | |December 24, 2014 |DAILY BEAST 

The bell tower bellows loudly when a little muscle power is put into it. The Congo's Forgotten Colonial Getaway |Nina Strochlic |December 18, 2014 |DAILY BEAST 

When it comes to tangible gifts, the sharing economy really starts to flex its holiday disrupting muscle. One of a Kind Gifts Are Only a Neighbor Away |Lawrence Ferber |December 8, 2014 |DAILY BEAST 

I can see the implant in there, and see where the muscle is snatching that implant up. Azealia Banks Opens Up About Her Journey from Stripping to Rap Stardom |Marlow Stern |November 17, 2014 |DAILY BEAST 

The strength of the lion is tremendous, owing to the immense mass of muscle around its jaws, shoulders, and forearms. Hunting the Lions |R.M. Ballantyne 

The man was accustomed to the French of Englishmen, and withdrew without moving a muscle of his face. Elster's Folly |Mrs. Henry Wood 

He was a man of gigantic muscle, and seizing the arm of Louis, called aloud to bar the egress. The Pastor's Fire-side Vol. 3 of 4 |Jane Porter 

In this country an unexplained marked eosinophilia warrants examination of a portion of muscle for Trichina spiralis (p. 255). A Manual of Clinical Diagnosis |James Campbell Todd 

The second pair show that the transverse processes, from the first to the third, are those into which the muscle is inserted. Assimilative Memory |Marcus Dwight Larrowe (AKA Prof. A. Loisette)