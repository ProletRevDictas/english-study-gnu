Ken Chapman flies a strange-looking helicopter for Erickson Incorporated called an S-64 Aircrane, and to get the wet stuff on board, all he has to do is submerge a snorkel about 18 inches deep in a body of water. How aerial firefighters battle blazes from the skies |Rob Verger |August 27, 2020 |Popular-Science 

While fire is a normal and necessary part of much of California’s forest and rangelands, the region’s trend toward heightened wet and dry extremes, coupled with overall warmer conditions, make the state especially primed for high-intensity burns. California and the Forest Service have a plan to prevent future catastrophic fires |Ula Chrobak |August 27, 2020 |Popular-Science 

The MJO travels eastward along the equator as winds push warm, wet air high into the atmosphere, where the air dries out, cools and descends back toward the surface. Improved three-week weather forecasts could save lives from disaster |Alexandra Witze |August 27, 2020 |Science News 

I took a hose to the pack for a few minutes, and some of my outer layers got wet through the zippers. The Matador SEG42 Offers Unrivaled Gear Organization |Graham Averill |August 25, 2020 |Outside Online 

So don’t think you’re totally safe because, if something failed, this area will get wet. Nobody’s Talking About the Sports Arena Flood Zone |MacKenzie Elmer |August 19, 2020 |Voice of San Diego 

Fidel jumped out and hopped into the ocean without getting wet. The Life and Hard Times Of The Family A Cuban Defector Left Behind |Brin-Jonathan Butler |December 19, 2014 |DAILY BEAST 

Have you noticed there are some people who would love to put a big wet blanket on all of this? Kirk Cameron Saves Christmas from Abominable Killjoys (Other Christians) |Brandy Zadrozny |November 14, 2014 |DAILY BEAST 

After she “got her feet wet,” Knox began writing under her own name. Amanda Knox, Cub Reporter: The Convicted Murderer Is Now Writing Theater Reviews for a Small Seattle Paper |Justin Miller |November 4, 2014 |DAILY BEAST 

If those dry counties get wet, those border stores could find their revenue drying up. Will Arkansas’ Prohibition Finally End? |Jack Holmes |November 1, 2014 |DAILY BEAST 

A September poll found 79 percent of likely voters “believe that counties should decide for themselves whether to be wet or dry.” Will Arkansas’ Prohibition Finally End? |Jack Holmes |November 1, 2014 |DAILY BEAST 

He turned to the gentle accents of his sweet Alice, breathed in a letter which had been wet with her grateful tears. The Pastor's Fire-side Vol. 3 of 4 |Jane Porter 

Turn we our backs to the cold gloomy north, to the wet windy west, to the dry parching east—on to the south! Blackwood's Edinburgh Magazine, Volume 60, No. 372, October 1846 |Various 

The farmer told him it was six miles; "but," he added, "you must ride sharp, or you will get a wet jacket before you reach it." The Book of Anecdotes and Budget of Fun; |Various 

No; they shall stay at home, and never learn anything, sooner than go and get wet. The Book of Anecdotes and Budget of Fun; |Various 

Her fat red cheeks would quiver with emotion, and be wet with briny tears, over the sorrows of Mr. Trollope's heroines. The Pit Town Coronet, Volume I (of 3) |Charles James Wills