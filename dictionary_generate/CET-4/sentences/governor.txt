Republicans are serving today as the governors of Massachusetts, Maryland, and Vermont while Democrats govern Kansas, Louisiana, and Kentucky. America needs a democratic revolution |Matthew Yglesias |September 17, 2020 |Vox 

A reporter asked whether the governor would consider special exceptions like San Diego’s seeking. Morning Report: Lincoln Abruptly Canceled AP Class |Voice of San Diego |September 17, 2020 |Voice of San Diego 

So, when the governor calls in the National Guard, it means that the state pays for the soldiers and the use of the equipment. Mobilizing the National Guard Doesn’t Mean Your State Is Under Martial Law. Usually. |by Logan Jaffe |September 17, 2020 |ProPublica 

Since then, though, Biden has been clearer that this would be something he would press governors to implement. Trump’s increasingly overt effort to pretend Biden is actually president |Aaron Blake |September 16, 2020 |Washington Post 

Stephanopoulos noted Biden called on governors to implement the mandate. Trump’s incoherent defense of his coronavirus response |Aaron Blake |September 16, 2020 |Washington Post 

Like many Americans—but few Republican presidential candidates—the former Florida governor has evolved on the issue. Jeb Bush’s Unseen Anti-Gay Marriage Emails |Jackie Kucinich |January 9, 2015 |DAILY BEAST 

The governor of Punjab province, a Muslim man, called publicly for leniency for her. In Defense of Blasphemy |Michael Tomasky |January 9, 2015 |DAILY BEAST 

And now, similarly, former Arkansas governor Mike Huckabee: "Bend over and take it like a prisoner!" Huckabee 2016: Bend Over and Take It Like a Prisoner! |Olivia Nuzzi |January 8, 2015 |DAILY BEAST 

Duke was a state representative whose neo-Nazi alliances were disgorged in media reports during his run for governor in 1991. The Price of Steve Scalise’s Silence |Jason Berry |January 7, 2015 |DAILY BEAST 

Abramoff said that the governor needed to remember to “be humble.” Abramoff’s Advice for Virginia’s New Jailhouse Guv |Tim Mak, Jackie Kucinich |January 7, 2015 |DAILY BEAST 

In particular the Governor of Adinskoy offered us a guard of fifty men to the next station, if we apprehended any danger. The Life and Most Surprising Adventures of Robinson Crusoe, of York, Mariner (1801) |Daniel Defoe 

The governor of the fortress was provided with a safe residence in Egypt, and an annual pension of 75,000 piasters. The Every Day Book of History and Chronology |Joel Munsell 

He recounts at much length the reasons for which he supposes the governor arrested him. The Philippine Islands, 1493-1898, Volume XX, 1621-1624 |Various 

He shall serve among great men, and appear before the governor. The Bible, Douay-Rheims Version |Various 

Messa urges the king to send a new governor, and gives his advice as to the character of him who should be sent. The Philippine Islands, 1493-1898, Volume XX, 1621-1624 |Various