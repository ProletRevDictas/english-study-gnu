It might be possible to boost oxygen levels in a water tank to make this gas more available to sea stars. Choked by bacteria, some starfish are turning to goo |Erin Garcia de Jesus |February 8, 2021 |Science News For Students 

Some techniques include increasing the oxygen levels in a water tank to make the gas more easily available to sea stars or getting rid of extra organic matter with ultraviolet light or water exchange. Some bacteria are suffocating sea stars, turning the animals to goo |Erin Garcia de Jesus |January 20, 2021 |Science News 

Keep in mind that budget models usually don’t include filters or fans, which adds further cleaning requirements on top of the frequent refilling faced with small tanks. The best humidifier: Fight dry air (and a dry nose) all winter long |PopSci Commerce Team |January 15, 2021 |Popular-Science 

If there’s still fuel in my tank after this hour or so of cooking, I’ll make a big jar of super-simple salad dressing. To Combat Cooking Burnout, Do It All at Once |Elazar Sontag |January 14, 2021 |Eater 

A fully-sealed HEPA filtration system means it’s great for anyone with allergies, and the easy-empty tank avoids mess. Best vacuum cleaner: How to tidy up fast |Charlotte Marcus |January 14, 2021 |Popular-Science 

“The events this year with Ukraine led to his ties with Cato being severed,” a source at the think tank told The Daily Beast. Vaclav Klaus, Libertarian Hero, Has His Wings Clipped by Cato Institute |James Kirchick |December 22, 2014 |DAILY BEAST 

Tank Battle Kim's death -- a clean version of Kim's Face shot (no head burning or head exploding). Sony Emails Show How the Studio Plans to Censor Kim Jong Un Assassination Comedy ‘The Interview’ |William Boot |December 15, 2014 |DAILY BEAST 

Tank Battle Jeep Guard Crush -- some editorial changes and the removal of all blood when the guards are crushed by the tank. Sony Emails Show How the Studio Plans to Censor Kim Jong Un Assassination Comedy ‘The Interview’ |William Boot |December 15, 2014 |DAILY BEAST 

And he would especially like American FGM-148 Javelins, man-portable anti-tank missiles to hit at Russian armor. Should the U.S. Arm Ukraine’s Militias? |Jamie Dettmer |November 24, 2014 |DAILY BEAST 

As they passed the runway, bullets shot up from the tall grass, puncturing a fuel tank. ‘Argo’ in the Congo: The Ghosts of the Stanleyville Hostage Crisis |Nina Strochlic |November 23, 2014 |DAILY BEAST 

It is of the vertical kind, and stands on a shallow square tank, which forms the hot well. Life of Richard Trevithick, Volume II (of 2) |Francis Trevithick 

On one side loomed a huge tank, to the brink of which a rickety wooden ladder invited the explorer to ascend. Dope |Sax Rohmer 

A single oar used rather as rudder than paddle guides the tank to the middle of the stream, where it floats to its destination. Journal of a Voyage to Brazil |Maria Graham 

No shooting and they would be miles away before they stopped rubbing their eyes in that one water-tank burg. Hooded Detective, Volume III No. 2, January, 1942 |Various 

The milky juice is emptied from the cups into a tank and lime juice is added and it is then allowed to stand. The Wonder Book of Knowledge |Various