These invisible fragments may be toxic on their own, and they also have the potential to harbor a layer of harmful microbes or chemicals. Microplastics are everywhere. Here’s what that means for our health. |Ula Chrobak |February 11, 2021 |Popular-Science 

That said, scientists haven’t fully nailed down how harmful subway particles may be to human health. Air pollution in US subway stations is disturbingly high |Kate Baggaley |February 11, 2021 |Popular-Science 

It’s not harmful in small amounts, but higher concentrations become dangerous. Hackers accessed a Florida water treatment plant’s system and tried to make a dangerous change |Stan Horaczek |February 9, 2021 |Popular-Science 

The work of organizers over the past eight months continues a long tradition of confronting unnecessary and harmful investments in institutions like the police. What ‘defund the police’ really means |Simon Balto |February 9, 2021 |Washington Post 

It’s also a place where people spread harmful misinformation—a problem that has proliferated on the platforms of its more mature rivals. TikTok takes on the mess that is misinformation |Danielle Abril |February 4, 2021 |Fortune 

Not long ago, the concept of diversity was viewed as anti-meritocratic—even harmful. The Republican Rainbow Coalition Is Real |Tim Mak |November 18, 2014 |DAILY BEAST 

Others fear that giving them the force of regulation could be more harmful because they would become outdated quickly. How Your Pacemaker Will Get Hacked |Kaiser Health News |November 17, 2014 |DAILY BEAST 

So why are we continually inundated with images and hate speech perpetuating this harmful lie? Can We Lose the Violent Muslim Cliché? |Jay Parini |October 12, 2014 |DAILY BEAST 

It will likely be harmful to the reputations of both the president and vice president. Exposed: The White House’s Professor-in-Chief |Jonathan Alter |October 8, 2014 |DAILY BEAST 

Another act, however, may be considered immoral not because it is harmful but because it evinces disloyalty. It’s Official: Religion Doesn’t Make You More Moral |Elizabeth Picciuto |September 23, 2014 |DAILY BEAST 

This line is not in the F. text; it seems to mean—'a wave, harmful in wearing away the shore.' Chaucer's Works, Volume 1 (of 7) -- Romaunt of the Rose; Minor Poems |Geoffrey Chaucer 

There is one great point in our favour, these people can do nothing very harmful so long as those papers are missing. The Weight of the Crown |Fred M. White 

In addition, this advertising may be harmful to those juveniles and adolescents with whom this Committee is primarily concerned. Report of the Special Committee on Moral Delinquency in Children and Adolescents |Oswald Chettle Mazengarb et al. 

It involves a continual struggle against harmful influences from outside. Report of the Special Committee on Moral Delinquency in Children and Adolescents |Oswald Chettle Mazengarb et al. 

Tell me,” said Hadria, “what are the qualities in a human being that make him most serviceable, or least harmful? The Daughters of Danaus |Mona Caird