The pandemic-specific cards came up quickly afterwards, because we heard people saying, “Gosh, birthdays aren’t what they used to be.” How Hallmark is handling pandemic Valentine’s Day |Luke Winkie |February 12, 2021 |Vox 

For a milestone birthday, she bought herself a designer purse, a matching wallet and some brand-name apparel. Miss Manners: She’s ‘best’ at giving gifts but stinks at receiving them |Judith Martin, Nicholas Martin, Jacobina Martin |February 4, 2021 |Washington Post 

Because of this success, CAMP will create new online shopping experiences for birthday parties, baby showers and holidays like Valentine’s Day. ‘A retail media company’: How CAMP wants its online content to eventually drive customers back to stores |Kayleigh Barber |February 3, 2021 |Digiday 

Like their liquor-based counterparts, booze-free drinks can serve to help us discover new flavors or celebrate a special occasion like an anniversary, a birthday, or a new job—or just making it through the work week at your current one. Keep dry January going all year with these cutting-edge non-alcoholic cocktails |By Dan Q. Dao/Saveur |February 2, 2021 |Popular-Science 

It’s what lets you guess why your friend is upset on her birthday. This Is Where Empathy Lives in the Brain, and How It Works |Shelly Fan |February 2, 2021 |Singularity Hub 

They had three years together before Ziad left to do his compulsory military service, leaving on her birthday, April 4, 2001. A Sunni-Shia Love Story Imperiled by al Qaeda |Ruth Michaelson |December 26, 2014 |DAILY BEAST 

As a result, while Newton was born on December 25, 1642 in England, his birthday was January 4, 1643 everywhere else. Neil deGrasse Tyson Trolls Christians on Christmas |Ben Jacobs |December 25, 2014 |DAILY BEAST 

A Spaniard by birth, Victor Serna left home shy of his 14th birthday and entered the monastery to become a Marist brother. Obama’s One Hand Clap With Castro |Doug McIntyre |December 24, 2014 |DAILY BEAST 

Recently a woman successfully crowdfunded to pay off a $360 trip taken on her 26th birthday. In Defense of Uber’s Awful Sydney Surge Pricing |Olivia Nuzzi |December 16, 2014 |DAILY BEAST 

So, happy 20th birthday to this proudly silly fashion classic. Happy 20th Birthday, Liz Hurley’s Safety-Pin Dress |Tim Teeman |December 12, 2014 |DAILY BEAST 

It will be no more monotonous than having one's seventh birthday or falling in love for the first time. The Salvaging Of Civilisation |H. G. (Herbert George) Wells 

He gave his cousin a silk dress, and he bought himself a set of books for his birthday; he was thirty-two. Tessa Wadsworth's Discipline |Jennie M. Drinkwater 

Musical festival in Westminster abbey, in commemoration of the birthday of Handel. The Every Day Book of History and Chronology |Joel Munsell 

A person becomes of age at the beginning of the day before his twenty-first birthday. Putnam's Handy Law Book for the Layman |Albert Sidney Bolles 

I had a good crying spell on my twenty-fifth birthday and Ive looked through clear eyes ever since. Tessa Wadsworth's Discipline |Jennie M. Drinkwater