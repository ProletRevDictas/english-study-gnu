One that views everything through the eyes of the badge and not the citizens we serve. Officials Worked To Deliberately Delay Release Of Daniel Prude Body Cam Footage |mharris |September 17, 2020 |Essence.com 

Just how much fires increase in the future, or whether they do at all despite the rise in risk, depends on how policymakers and citizens respond to the threat. What wildfires in Brazil, Siberia, and the US West have in common |Lili Pike |September 17, 2020 |Vox 

Federal law also allows citizens to file suit over Clean Water Act violations, both to seek fines, payable to the government, and to force measures to stop further violations. This Billionaire Governor’s Coal Company Might Get a Big Break From His Own Regulators |by Ken Ward Jr. |September 17, 2020 |ProPublica 

The core principle of democracy is political equality — the idea that all citizens are equal under the law and deserve to have their interests considered equally by the political system. America needs a democratic revolution |Matthew Yglesias |September 17, 2020 |Vox 

My administration will take all necessary steps to safeguard our citizens from this threat. Ten days: After an early coronavirus warning, Trump is distracted as he downplays threat |Ashley Parker, Josh Dawsey, Yasmeen Abutaleb |September 17, 2020 |Washington Post 

The Pentagon said Faal served in the Air Force for seven years, during which time he became a U.S. citizen. The Shadowy U.S. Veteran Who Tried to Overthrow a Country |Jacob Siegel |January 6, 2015 |DAILY BEAST 

The feisty airline is the brainchild of entrepreneur Tony Fernandes, a Malaysian of Indian descent who also is a British citizen. The Presumed Crash of AirAsia Flight QZ8501 Is Nothing Like MH370 |Lennox Samuels |December 29, 2014 |DAILY BEAST 

Congress unanimously passed a bill to make Bernardo de Galvez an honorary citizen of the United States in 2014. Nazis, Sunscreen, and Sea Gull Eggs: Congress in 2014 Was Hella Productive |Ben Jacobs |December 29, 2014 |DAILY BEAST 

Eventually Julio made the decision to become an American Citizen, and even voted for Barack Obama in the 2008 election. Cuban Hip-Hop Was Born in Alamar |Daniel Levin |December 26, 2014 |DAILY BEAST 

We see a system that will indict a 20-year-old for selling crack but not a police officer for choking the life out of a citizen. Bobby Shmurda and Rap’s Ultimate Hoop Dream |Rawiya Kameir |December 23, 2014 |DAILY BEAST 

Now what should college give the young citizen, male or female, upon the foundation of schooling we have already sketched out? The Salvaging Of Civilisation |H. G. (Herbert George) Wells 

It makes out of the savage raw material which is our basal mental stuff, a citizen. The Salvaging Of Civilisation |H. G. (Herbert George) Wells 

The average citizen of three generations ago was probably not aware that he was an extreme individualist. The Unsolved Riddle of Social Justice |Stephen Leacock 

The goods are all valued according to the amount of time used in their making and each citizen draws out the same total amount. The Unsolved Riddle of Social Justice |Stephen Leacock 

So speaks the ideal official dealing with the ideal citizen in the dream life among the angels. The Unsolved Riddle of Social Justice |Stephen Leacock