He said more analysis is needed to determine the extent that excessive speeding contributed to the increase in speed-related crashes, but last year’s data shows a troubling trend. Traffic counts fell during the coronavirus pandemic, but road fatalities still increased |Luz Lazo |February 12, 2021 |Washington Post 

In the ocean, organisms accumulate these pieces in their bodies over their lifetimes, sometimes to harmful or lethal extents, and in turn microplastic also shows up in the seafood we eat. Microplastics are everywhere. Here’s what that means for our health. |Ula Chrobak |February 11, 2021 |Popular-Science 

Most students probably will not see the changes until the fall, when the university has said it expects to reopen “to the fullest extent possible.” George Washington University commits to single-use-plastic ban |Lauren Lumpkin |February 11, 2021 |Washington Post 

Both sides are to a certain extent under the illusion if you got rid of Section 230, that would magically fix all of their problems. Twenty-Six Words Created the Internet. What Will It Take to Save It? |Stephen Engelberg |February 9, 2021 |ProPublica 

“We as a state must be able to assure, to the greatest extent possible, that we maintain an unbiased, balanced and fair taxation system,” said Brad Witt, a state representative who leads Oregon’s House Committee on Agriculture and Natural Resources. “We Have Counties in Deep Trouble”: Oregon Lawmakers Seek to Reverse Timber Tax Cuts That Cost Communities Billions |by Rob Davis, The Oregonian/OregonLive, and Tony Schick, Oregon Public Broadcasting |February 5, 2021 |ProPublica 

My dad was a sailor, and all through my childhood he was away half of the time at sea, and to an extent I have a similar job. Belle & Sebastian Aren’t So Shy Anymore |James Joiner |January 7, 2015 |DAILY BEAST 

And, as Gow adds wryly from his own personal experience, “To a huge extent they achieved that aim very well.” ‘Nazi Cows’ Tried to Kill British Farmer |Tom Sykes |January 6, 2015 |DAILY BEAST 

That was the extent of it during the peak of the flames, and the numbers that swooshed around in the press the next day. The Fiery Death of Sotto Sotto, Toronto’s Celebrity Hotspot |Shinan Govani |December 30, 2014 |DAILY BEAST 

I did not think that it would go to the extent it did, but our office was not shocked. The Baptism of Michael Brown Sr. and Ferguson’s Baptism by Fire |Justin Glawe |November 27, 2014 |DAILY BEAST 

To what extent was the testimony the grand jury heard corroborated or contradicted by forensic evidence? Ferguson’s Grand Jury Bought Darren Wilson’s Story |Paul Campos |November 25, 2014 |DAILY BEAST 

But what a magnificent plain is this we are entering upon: it is of immense extent. Blackwood's Edinburgh Magazine, Volume 60, No. 372, October 1846 |Various 

Adequate conception of the extent, the variety, the excellence of the works of Art here heaped together is impossible. Glances at Europe |Horace Greeley 

Tobacco is a strong growing plant resisting heat and drought to a far (p. 018) greater extent than most plants. Tobacco; Its History, Varieties, Culture, Manufacture and Commerce |E. R. Billings. 

M'Bongo and his whole court are now clothed, I am happy to say, at least to a certain extent. The Pit Town Coronet, Volume I (of 3) |Charles James Wills 

And it is too true that ages of subjugation have demoralized, to a fearful extent, the Italian People. Glances at Europe |Horace Greeley