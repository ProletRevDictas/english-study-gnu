Even in the spring, says Jeremy Kamil, a virologist at LSU Health Sciences-Shreveport and another senior author, it was clear that something was strange about Louisiana’s outbreak. Can Louisiana’s COVID surge trace back to one Mardi Gras reveler? |Philip Kiefer |February 11, 2021 |Popular-Science 

While that might sound a little strange, if your heated throw has a plug and runs on electricity—well, that’s an appliance. Best heated throw blanket: Bundle up with these electric blankets |PopSci Commerce Team |February 11, 2021 |Popular-Science 

This rebuttal proffers a strange theory of governance that American accounts are somehow bound by the lifetime of its generations. 'Uncomfortable truth’: The new push for a slavery reparations commission in Congress |DeNeen L. Brown |February 10, 2021 |Washington Post 

After months of eating alone, it sounded strange to my ears. Love, Loneliness, and the Chicken in My Freezer |Elazar Sontag |February 9, 2021 |Eater 

It’s no longer strange to see Tom Brady in a Tampa Bay Buccaneers uniform. Tom Brady, the one-man dynasty, was Tampa Bay’s perfect missing piece |Jerry Brewer |February 8, 2021 |Washington Post 

In front of this strange structure are two blank-faced, well-dressed models showing off the latest in European minimalism. New York’s Most Tragic Ghost Loves Minimalist Swedish Fashion |Nina Strochlic |January 8, 2015 |DAILY BEAST 

It was a bit strange for a while here with all the Newsweek stuff. Coffee Talk with Fred Armisen: On ‘Portlandia,’ Meeting Obama, and Taylor Swift’s Greatness |Marlow Stern |January 7, 2015 |DAILY BEAST 

The Strange Social History of Our Most Intimate Institution. The Real Story Behind the Fight for Marriage Equality |E.J. Graff |December 30, 2014 |DAILY BEAST 

In another year, stories about the strange new face of an A-list actress might draw chortles and cackles. Renée Zellweger Got a New Face—and Everyone Had An Opinion About It |Kevin O’Keeffe |December 29, 2014 |DAILY BEAST 

Like, OK, to be around them when we were away from work is great, but being at work was still kind of strange for me. How A Company’s Support of Gay Employees Helped One of Them To Come Out | |December 24, 2014 |DAILY BEAST 

It seems very strange that I shall actually know Liszt at last, after hearing of him so many years. Music-Study in Germany |Amy Fay 

He did believe you, more or less, and what you said fell in with his own impressions—strange impressions that they were, poor man! Confidence |Henry James 

The associations of place recall her strange interview with Mr. Longcluse but a few months before. Checkmate |Joseph Sheridan Le Fanu 

Almost as soon as she had finished building her nest she had discovered a strange-looking egg there. The Tale of Grandfather Mole |Arthur Scott Bailey 

Her feet felt rooted to the floor in the wonder and doubt of this strange occurrence. The Bondboy |George W. (George Washington) Ogden