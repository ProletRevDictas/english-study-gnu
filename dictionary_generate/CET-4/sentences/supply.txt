If you’re looking for a weatherproof, heavy-duty case with plenty of first aid supplies, this one’s for you. The best first aid kits for staying safe and prepared |PopSci Commerce Team |September 4, 2020 |Popular-Science 

It supplies Nreal with its Snapdragon processors, allowing the startup’s lightweight mixed reality glasses to easily plug into an Android phone. Qualcomm-powered Chinese XR startup Nreal raises $40 million |Rita Liao |September 4, 2020 |TechCrunch 

“Despite a high self-sufficiency ratio for grains, the balance between the food demand and supply has been quite tight in recent years,” Si says. Why Is China Cracking Down on Food Waste? |Daniel Malloy |September 3, 2020 |Ozy 

While the Covid-19 outbreak has hammered the global economy and disrupted supply chains, Apple is seeing strong demand for iPhones, iPads and Mac computers from people working and studying remotely. Apple is prepping 75 million 5G iPhones for later this year |radmarya |September 1, 2020 |Fortune 

Greening that supply chain is crucial to the consumer electronics giant’s goal, announced in July, to have a net zero carbon footprint by 2030. Apple is leading the charge to overhaul Taiwan’s renewable energy market |Tim McDonnell |August 31, 2020 |Quartz 

Fluoride first entered an American water supply through a rather inelegant technocratic scheme. Anti-Fluoriders Are The OG Anti-Vaxxers |Michael Schulson |July 27, 2016 |DAILY BEAST 

But the qualities Mario Cuomo brought to public life—compassion, integrity, commitment to principle—remain in short supply today. President Cuomo Would’ve Been a Lion |Jonathan Alter |January 2, 2015 |DAILY BEAST 

If you answered seven or more of these correctly, you are eligible for a lifetime supply of Metamucil. The World’s Toughest Political Quiz |Jeff Greenfield |December 31, 2014 |DAILY BEAST 

In Mosul, foreign fighters have left, the city is flooded with refugees and supply routes are cut off. Has the Kurdish Victory at Sinjar Turned the Tide of ISIS War? |Niqash |December 27, 2014 |DAILY BEAST 

It would seek to cut off the main Allied lines of supply and communication. Hitler’s Hail Mary |James A. Warren |December 20, 2014 |DAILY BEAST 

First, how about the expansibility needed to supply adequate funds for crop-moving? Readings in Money and Banking |Chester Arthur Phillips 

What course was taken to supply that assembly when any noble family became extinct? Gulliver's Travels |Jonathan Swift 

You see, I am the city undertaker, and the people are dying here so fast, that I can hardly supply the demand for coffins. The Book of Anecdotes and Budget of Fun; |Various 

Almost one-quarter of the total supply printed has been placed in circulation. Readings in Money and Banking |Chester Arthur Phillips 

The increased volume of the supply thus produced inevitably forces down the price till it sinks to the point of cost. The Unsolved Riddle of Social Justice |Stephen Leacock