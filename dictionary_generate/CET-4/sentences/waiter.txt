Though entrepreneurs can hire others to join their business—a restaurant owner can employ waiters, for example, or a taxi owner can let others drive their car—they cannot get legal recognition for their business as a company. Cuba Is Opening Up Its Economy. But Don't Call It a Shift to Capitalism Just Yet |Ciara Nugent |February 9, 2021 |Time 

While it’s difficult to get exact numbers of their contribution to the economy, these workers keep Jackson running by filling jobs in all sectors, from house cleaners and construction workers to cooks and waiters. What America's Richest Ski Town's Handling of COVID-19 Says About the Country |Lucas Isakowitz |February 1, 2021 |Time 

As digital ordering gains ground, robot waiters and chefs are likely not far behind. Is the Pandemic Spurring a Robot Revolution? |Marius Robles |November 30, 2020 |Singularity Hub 

The latest example is a new service from payment giant Square that reduces social interactions by eliminating many tasks performed by a waiter. Fewer waiters, no menus: Is Square’s new service the future of dining? |Jeff |September 29, 2020 |Fortune 

We were then surprised when a waiter came over, poured us water, and asked what we wanted to drink. People Are Dining Out, They Just Don’t Want Anyone to Know About It |Jaya Saxena |September 3, 2020 |Eater 

“If you are a waiter, you can make twice as much in Austin relative to Flint,” remarked Moretti. The Rustbelt Roars Back From the Dead |Joel Kotkin, Richey Piiparinen |December 7, 2014 |DAILY BEAST 

A waiter brings out some fresh pretzels and homemade pork and wine sausages. House of the Witch: The Renegade Craft Brewers of Panama |Jeff Campagna |November 30, 2014 |DAILY BEAST 

Here, breakfast begins with a crusty cornetto alla crema served by a formal, dinner jacket-wearing waiter. Palermo Is Not Just for the Mafia Anymore |Alia Akkam |August 18, 2014 |DAILY BEAST 

He was Russel Rebello, a 33-year-old ship waiter from India who deserves to be recovered so his family can have closure. The Racism of Disaster Coverage |Barbie Latza Nadeau |July 25, 2014 |DAILY BEAST 

In Chobits, hapless waiter Hideki ends up purchasing a “persocom,” an android called Chi, and slowly falls in love with her. Hollywood Sci-Fi Films Are Ripping Off Anime |David Levesley |April 18, 2014 |DAILY BEAST 

He was dressed like a waiter, and he looked like one—a regular City waiter, you know. The Pit Town Coronet, Volume I (of 3) |Charles James Wills 

“Let us have some of your best wine to-day, waiter,” said old Wardle, rubbing his hands. The Posthumous Papers of the Pickwick Club, v. 2(of 2) |Charles Dickens 

He sealed the letter, addressing it to Cannes; called a waiter, late as it was, and desired him to post it. Elster's Folly |Mrs. Henry Wood 

"Lettres et journal pour monsieur," interrupted a waiter, entering with two letters and the Times. Elster's Folly |Mrs. Henry Wood 

A pedantic fellow called for a bottle of hock at a tavern, which the waiter, not hearing distinctly, asked him to repeat. The Book of Anecdotes and Budget of Fun; |Various