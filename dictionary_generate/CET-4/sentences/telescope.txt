In the four centuries since humans first pointed their telescopes at Mars, no eruption has been seen, leading to the assumption that it’s volcanically dead. Rumbles on Mars Raise Hopes of Underground Magma Flows |Robin George Andrews |February 1, 2021 |Quanta Magazine 

The CHEOPS observations confirmed intermediate work from other telescopes, and eventually a collaboration of roughly 200 astronomers managed to piece together a more complete picture of the system. These 6 exoplanets somehow orbit their star in perfect rhythm |Charlie Wood |January 27, 2021 |Popular-Science 

Ordinarily, we point telescopes at some object we want to see in greater detail. How Many Galaxies Are in the Universe? A New Answer From the Darkest Sky Ever Observed |Jason Dorrier |January 15, 2021 |Singularity Hub 

Together, these telescopes will provide unprecedented views of the sun, helping to solve some of the most enduring mysteries of our star. The Parker Solar Probe will have company on its next pass by the sun |Lisa Grossman |January 15, 2021 |Science News 

They allowed me to get toys that were clever, even though we couldn’t afford them — mechanical toys and telescopes. A Prodigy Who Cracked Open the Cosmos |Claudia Dreifus |January 12, 2021 |Quanta Magazine 

Williams did it with me several times, for instance when I mentioned the Hubble Space Telescope. The Stacks: Robin Williams, More Than A Shtick Figure |Joe Morgenstern |August 16, 2014 |DAILY BEAST 

The instrument is attached to the Anglo-Australian Telescope at the Siding Spring Observatory, northwest of Sydney, Australia. SAMI Is Like Google Earth for the Universe |Matthew R. Francis |July 27, 2014 |DAILY BEAST 

The Swift telescope is designed to detect any amount of extra high-energy light, then turn to locate the source. The Gamma-Ray Burst That Wasn’t |Matthew R. Francis |June 1, 2014 |DAILY BEAST 

If you live in a place with a dark night sky, you might be able to see M31 without a telescope. The Gamma-Ray Burst That Wasn’t |Matthew R. Francis |June 1, 2014 |DAILY BEAST 

But the results coming out of BOSS are beautiful, even if the telescope is hideous. Using Black Holes to Measure Dark Energy, Like a BOSS |Matthew R. Francis |April 13, 2014 |DAILY BEAST 

Another crash, which nearly shut up his spine like a telescope, told him that there were no wings. The Giant of the North |R.M. Ballantyne 

To spy out the land with a naval telescope over a mile of sea means taking a lot on trust as we learned to our cost on April 25th. Gallipoli Diary, Volume I |Ian Hamilton 

These instruments were of a simple nature, for the magnifying glass was not yet contrived, and so the telescope was impossible. Outlines of the Earth's History |Nathaniel Southgate Shaler 

With the telescope somewhere near a hundred million are brought within the limits of vision. Outlines of the Earth's History |Nathaniel Southgate Shaler 

He put his canvas telescope down, and placed a heavy foot on it for safety. Scattergood Baines |Clarence Budington Kelland