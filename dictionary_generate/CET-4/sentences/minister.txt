Suga often has been mentioned as one of many possible successors to Abe, especially in recent months as the prime minister scaled back public appearances and faced a series of political scandals involving friends and family. How the son of strawberry pickers became Japan’s most likely choice for next prime minister |claychandler |September 3, 2020 |Fortune 

There was a large anti-government protest planned for December 19 at the Red Fort in Delhi, which is a historic monument from where the prime minister traditionally delivers a televised address to the nation on independence day. Podcast: How a 135-year-old law lets India shutdown the internet |Anthony Green |September 2, 2020 |MIT Technology Review 

The policy, which granted exemptions on medical grounds, was backed by his minister for industry, science, and technology. Australia won’t be the last country to wrestle with a mandatory coronavirus vaccine |Olivia Goldhill |August 20, 2020 |Quartz 

When the prime minister says, “No country would find 173 billion barrels of oil in the ground and just leave them there,” he’s not wrong. The oil sands triggered a heated global debate on our energy needs. Now, they could be a sign of what’s to come |kdunn6 |August 20, 2020 |Fortune 

This past July, May was replaced as prime minister by Boris Johnson, her former Foreign Minister and, before that, the Mayor of London. The Prime Minister Who Cried Brexit (Ep. 392) |Stephen J. Dubner |October 10, 2019 |Freakonomics 

Domestically, the prime minister maintains the dubious line that he is the only man who can keep the still-fragile peace. Cambodia’s Smoke-and-Mirrors Democracy |David Shaftel |January 9, 2015 |DAILY BEAST 

One of its top officials is the current minister of the interior in Baghad. What an Iranian Funeral Tells Us About the Wars in Iraq |IranWire |January 6, 2015 |DAILY BEAST 

Even those Christians who do want to minister amid the rancor of race and policing are missing the mark. No Gods, No Cops, No Masters |James Poulos |January 1, 2015 |DAILY BEAST 

“I have coordinated with our foreign minister so we will borrow from other countries which have offered,” he said. The Presumed Crash of AirAsia Flight QZ8501 Is Nothing Like MH370 |Lennox Samuels |December 29, 2014 |DAILY BEAST 

Hamish Marshall himself is a former staffer of Prime Minister Harper. How Canadian Oilmen Pinkwash the Keystone Pipeline |Jay Michaelson |December 28, 2014 |DAILY BEAST 

But,” said the prime minister of Flatland, starting a difficulty, “who is to be greatest chief? The Giant of the North |R.M. Ballantyne 

He professed both to abominate and despise all mystery, refinement, and intrigue, either in a prince or a minister. Gulliver's Travels |Jonathan Swift 

One of her humours was to unite the son of her minister, with a niece of the widowed Queen of Saint Germain's. The Pastor's Fire-side Vol. 3 of 4 |Jane Porter 

The minister's eye kept steady to one point; to raise the country he governed, to the utmost pinnacle of earthly grandeur. The Pastor's Fire-side Vol. 3 of 4 |Jane Porter 

All that was necessary was a slight knowledge of a Cabinet Minister, and a smattering of schooling. Punch, or the London Charivari, Volume 107, November 3, 1894 |Various