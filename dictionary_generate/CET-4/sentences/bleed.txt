He didn’t sleep on the bus, even as Saturday night bled into Sunday morning. A rare disease, a covid diagnosis, a painful decision: The death of basketball coach Lew Hill |Dave Sheinin |February 11, 2021 |Washington Post 

“I’d be lying if I didn’t tell you my heart bleeds for the people involved in it,” Andy Reid said after the game. Chiefs assistant coach Britt Reid involved in car accident that injured two children |Mark Maske |February 8, 2021 |Washington Post 

These were 3-, 4-, 5-year-old kids bleeding to death, bleeding out of their ears, eyes, nose, skin and bowels, bleeding internally, vomiting blood. Emil Freireich, a pioneer of chemotherapy and a ‘towering figure in oncology,’ dies at 93 |Emily Langer |February 4, 2021 |Washington Post 

A blanket of banana slices or pecans or nut butter between warm oatmeal and creamy-cold yogurt will protect the distinctly different soft foods from bleeding into one another as you sink your spoon through the many beautiful layers. Start Your Day With a Slop Parfait |Elazar Sontag |February 4, 2021 |Eater 

The lack of ego in that relationship is hopefully bleeding into this and we can have a similar situation here. Washington introduces new execs Martin Mayhew and Marty Hurney as focus shifts to quarterback |Nicki Jhabvala |February 3, 2021 |Washington Post 

This ever-so-slight heart-bleed for immigrant children branded him a party apostate, and he began to change course. Arkansas’s Blue Collar Social Conservatives Don’t Know What’s Coming |Monica Potts |November 10, 2014 |DAILY BEAST 

There was no reading of single lines whatsoever because the voices would bleed through on the other mics. Garfield Television: The Cat Who Saved Primetime Cartoons |Rich Goldstein |November 5, 2014 |DAILY BEAST 

Those prognosticators had reason to believe the 10,000 lakes could bleed a little red into Washington. What Al Franken’s Normcore Senate Race Can Teach Other Democrats |Ana Marie Cox |October 27, 2014 |DAILY BEAST 

For instance, when a couple is having trouble, the tension and hostility can bleed into BDSM scenes. Coming Out Kinky to Your Doctor, in Black and Blue |Heather Boerner |October 25, 2014 |DAILY BEAST 

Will the freedom you mentioned writing the novel bleed into your work writing your next screenplay? David Cronenberg: Why Frustrated Novelists Hate the Screenplay |Craig Hubert |October 13, 2014 |DAILY BEAST 

"Only cut deep enough to make it bleed freely," said the surgeon, as he dressed Harry's arm. The Courier of the Ozarks |Byron A. Dunn 

Undoubtedly the type who got sick to his stomach at the sight of blood even though it might be no more than a nose-bleed. Hooded Detective, Volume III No. 2, January, 1942 |Various 

I must not wound myself—I would bleed slowly—they might discover me still alive. Prison Memoirs of an Anarchist |Alexander Berkman 

Called in to bleed Mme. de Mortsauf, whose life was saved by this operation. Repertory Of The Comedie Humaine, Complete, A -- Z |Anatole Cerfberr and Jules Franois Christophe 

I suppose that something has been smashed up, so that it cannot bleed. Under Wellington's Command |G. A. Henty