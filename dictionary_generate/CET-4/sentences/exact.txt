He would not disclose the exact revenue earned or how much these partnerships contribute to the publisher’s overall revenue breakdown. How TheSoul Publishing grew revenue via platforms with viral social media life hacks |Kayleigh Barber |February 12, 2021 |Digiday 

It’s not clear what the exact nature of the Daily Caller story was other than Weaver in the aftermath of him taking medical leave. Lincoln Project’s avowed ignorance of Weaver texts undercut by leaked communications |Chris Johnson |February 9, 2021 |Washington Blade 

At that moment I realized that not everybody has the exact same perspective, not everybody has the exact same upbringing or the exact same privileged and fortunate life that I had. When Madison Cawthorn Felt ‘Invisible’ |Eugene Robinson |February 9, 2021 |Ozy 

At several points in the book, Ruffin interrupts the humor to acknowledge that many of her family’s experiences, when totaled up this way, exact a heavy burden. Late-night TV sensation Amber Ruffin and her sister co-wrote a book about racism. And, yes, it’s hilarious. |Hau Chu |February 8, 2021 |Washington Post 

Only three points, to be exact, rather than the seven or eight Kansas City surely would have preferred, but kicker Harrison Butker did well to connect from 52 yards out. Super Bowl highlights: Bucs celebrate championship, Tom Brady wins MVP |Des Bieler, Mark Maske, Chuck Culpepper |February 8, 2021 |Washington Post 

And by the time an airplane was in the water, its exact position would be known. Red Tape and Black Boxes: Why We Keep ‘Losing’ Airliners in 2014 |Clive Irving |December 29, 2014 |DAILY BEAST 

He will tell you why became a cop with the exact same words used by DePrimo and many of their fellow officers. Protesters Slimed This Good Samaritan Cop |Michael Daly |December 16, 2014 |DAILY BEAST 

Behind him stood a flock of fifth-grade boys—and two second-grade girls—all of them wearing the exact same yellow hat. Even Grade School Kids Are Protesting the Garner Killing Now |Caitlin Dickson |December 6, 2014 |DAILY BEAST 

Cooper had little Alexis pose for a picture on the exact spot there Garner was pinned. ‘I Can’t Breathe!’ ‘I Can’t Breathe!’ A Moral Indictment of Cop Culture |Michael Daly |December 4, 2014 |DAILY BEAST 

While difficult to estimate exact numbers, thousands of Americans die every year because of delayed or denied claims. My Insurance Company Killed Me, Despite Obamacare |Malcolm MacDougall |November 24, 2014 |DAILY BEAST 

As company after company appeared, we were able to form a pretty exact estimate of their numbers. Blackwood's Edinburgh Magazine, No. CCCXXXIX. January, 1844. Vol. LV. |Various 

A method of Vacuity pure and simple—the exact opposite of Mental Assimilation. Assimilative Memory |Marcus Dwight Larrowe (AKA Prof. A. Loisette) 

That was not the exact word that he used, but he expressed it by beating his tail against the table and giving a long howl. The Soldier of the Valley |Nelson Lloyd 

Those were not his exact words, but I saw his answer in his eyes, for he had climbed higher and they were close to mine. The Soldier of the Valley |Nelson Lloyd 

Carrying these suggestions to the text, they help fix the exact number of times the word “bells” occurs in each line. Assimilative Memory |Marcus Dwight Larrowe (AKA Prof. A. Loisette)