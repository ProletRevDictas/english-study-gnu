That cheerful presence also heightens the actual and perceived safety of the streets and residents’ sense of community. Even COVID can’t stop New York City’s sidewalk Christmas tree vendors |matthewheimer |December 24, 2020 |Fortune 

In the digital age, the artist is unfailingly genial, cheerful, relatable. Art has been brutalized by tech’s giants. How can it survive? |Konstantin Kakaes |December 23, 2020 |MIT Technology Review 

On the other hand, Paige's life, seen only as an indefatigable, wise and funny personal odyssey, is a cheerful tale. Satchel Paige was one of baseball’s best. It didn’t take an announcement to know that. |Thomas M. Boswell |December 18, 2020 |Washington Post 

“I was just at the window, being happy and cheerful, and just trying to be as positive as I can be during the pandemic,” said Eddie Aldrete, a 22-year-old Starbucks employee. It started as a spontaneous pep talk. Then this Starbucks barista became a lifeline for a boy with cancer. |Sydney Page |December 11, 2020 |Washington Post 

“It beats driving alone,” a cartoon skunk declares in a cheerful 1974 public service announcement encouraging Americans to carpool. Carpooling is poised to make a comeback this fall |Lila MacLellan |September 9, 2020 |Quartz 

They also love Christmas, and are determined to make it cheerful and to give gifts. One Cake to Rule Them All: How Stollen Stole Our Hearts |Molly Hannon |December 24, 2014 |DAILY BEAST 

A cheerful convict was found dead by his devoted caretaker one morning. A Million Ways to Die in Prison |Daniel Genis |December 8, 2014 |DAILY BEAST 

“Highly inquisitive, highly playful, always cheerful,” he recalls. For Next AG, Obama Picks a Quiet Fighter With a Heavy Punch |Michael Daly |November 8, 2014 |DAILY BEAST 

Eri Hayward is cheerful, even as she drops verbal bombs that demolish mainstream conceptions about being transgender. Thank God! To the Church, This Transgender Woman Is Just a Skank |Emily Shire |October 22, 2014 |DAILY BEAST 

And as with all cheerful, well-intentioned memes, the Challenge has provoked some good old-fashioned political trolling. Ice Bucket Challenge Leads to Political Trolling |Asawin Suebsaeng |August 21, 2014 |DAILY BEAST 

This gave the house a very cheerful appearance, as if it were constantly on a broad grin. Davy and The Goblin |Charles E. Carryl 

Your hangings must be of chintz, of pretty chintz, and you will put a cheerful carpet on the floor. The Pit Town Coronet, Volume I (of 3) |Charles James Wills 

But from the rather dismal salon they passed into a more cheerful room. The Pit Town Coronet, Volume I (of 3) |Charles James Wills 

The cheerful sound of music came from the deck of a big saloon steamer, bearing its crowd of noisy tourists. The Pit Town Coronet, Volume I (of 3) |Charles James Wills 

The cheerful hours of easy labor vary but do not destroy the pursuit of pleasure and of recreation. The Unsolved Riddle of Social Justice |Stephen Leacock