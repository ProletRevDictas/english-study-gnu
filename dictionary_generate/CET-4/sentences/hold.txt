Observations they had planned for the spring were put on hold by the coronavirus pandemic. Phosphine gas found in Venus’ atmosphere may be ‘a possible sign of life’ |Lisa Grossman |September 14, 2020 |Science News 

They are readily available, and I never have to wait on hold for an hour. Only three of 26 Obamacare-era nonprofit health insurance co-ops will soon remain |lbelanger225 |September 6, 2020 |Fortune 

Hartman said he was told the hold was due to unspecified operational issues. AstraZeneca COVID-19 vaccine enters final-stage trial in the U.S. |Claire Zillman, reporter |September 1, 2020 |Fortune 

His factory, which employs 14 people, was shut for more than two months, with most first-half orders put on hold. Can Italy’s Fashion Artisans Survive COVID? |Charu Kasturi |August 30, 2020 |Ozy 

In March, as the Coronavirus crisis was beginning to take hold, agencies conducted tests to see how viable remote work was before sending employees home for what many thought would be just a few weeks. ‘Necessary to attract talent’: How agencies are managing employees’ requests to move to different states |Kristina Monllos |August 26, 2020 |Digiday 

Blacks would hold about 13 percent of the seats and Latinos 17 percent. The Unbearable Whiteness of Congress |Dean Obeidallah |January 8, 2015 |DAILY BEAST 

Tomorrow they should hold placards of the cartoons Charlie Hebdo had printed. Ayaan Hirsi Ali: Our Duty Is to Keep Charlie Hebdo Alive |Ayaan Hirsi Ali |January 8, 2015 |DAILY BEAST 

Yeah, the “Giant man-puppy” that is Gronkowski won't hold a sexual candle to the blue-eyed dreamboat. ‘A Gronking to Remember’ Speed Read: 8 Naughtiest Bits |Emily Shire |January 7, 2015 |DAILY BEAST 

But how much they have regained or how durable their hold is remains unclear. Pentagon Doesn’t Know How Many People It’s Killed in the ISIS War |Nancy A. Youssef |January 7, 2015 |DAILY BEAST 

But that would now have to be put on hold because he had been shot in the Bronx. Shot Down During the NYPD Slowdown |Michael Daly |January 7, 2015 |DAILY BEAST 

Most of the men leaped up, caught hold of spears or knives, and rushed out. The Giant of the North |R.M. Ballantyne 

He was too drowsy to hold the thought more than a moment in his mind, much less to reflect upon it. The Wave |Algernon Blackwood 

One adorable smile she gave him, and before he could advance to hold the door for her, she had opened it and passed out. St. Martin's Summer |Rafael Sabatini 

Not being sufficiently numerous to hold out the town as well as the Alamo, they retreated into the latter. Blackwood's Edinburgh Magazine, No. CCCXXXIX. January, 1844. Vol. LV. |Various 

He was well set in the form of a man now, the months since his imprisonment having brought him much to fasten upon and hold. The Bondboy |George W. (George Washington) Ogden