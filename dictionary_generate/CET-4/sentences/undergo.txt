The Zydus Cadila and Bharat Biotech vaccine candidates are two of 37 potential coronavirus vaccines currently undergoing human trials around the world. More than manufacturing: India’s homegrown COVID vaccines could transform its pharma industry |Naomi Xu Elegant |September 6, 2020 |Fortune 

The Office of Personnel Management announces a massive security breach that eventually is shown to affect over 20 million people who had undergone background checks for federal jobs since 2000. A brief history of US-China espionage entanglements |Konstantin Kakaes |September 3, 2020 |MIT Technology Review 

Many patients have already undergone the procedure with promising results. Elon Musk’s brain company plans a big reveal on Friday. Here’s what we already know |Verne Kopytoff |August 27, 2020 |Fortune 

Given that the world is undergoing its second “once-in-a-generation” disruption in 12 years, a reappraisal is in order. COVID-19 and climate change expose the dangers of unstable supply chains |matthewheimer |August 27, 2020 |Fortune 

Public in recent months has put in so-called “safety labels,” which asks users to reconfirm, for example, if they want to buy a particular stock that is undergoing bankruptcy proceedings. As VC payday nears, Sequoia sits in the middle of the IPO deluge |Lucinda Shen |August 25, 2020 |Fortune 

A woman typically starts her life with millions of eggs but only 400 or so will ever undergo ovulation. Men Will Someday Have Kids Without Women |Samantha Allen |January 3, 2015 |DAILY BEAST 

There is no better way to redefine your image than to undergo a religious conversion. The Good Wife’s Religion Politics: Voters Have No Faith in Alicia's Atheism |Regina Lizik |November 24, 2014 |DAILY BEAST 

Many others who survived suffered severe frostbite and have had or soon will undergo amputations. Nepal’s Deadliest Avalanche Was Totally Avoidable |Dick Dorworth |October 20, 2014 |DAILY BEAST 

I would never encourage anyone to go abroad, I would never encourage anyone to undergo military training. Britain’s Counter-Terror Raids: The End of Londonistan? |Nico Hines |September 25, 2014 |DAILY BEAST 

The procedure they undergo to extract eggs is intense and invasive and there are no sexual kicks involved. Today’s Sperm Donor Isn’t a Broke 20-Something |Stephanie Fairyington |September 20, 2014 |DAILY BEAST 

They are more susceptible to the horrors and discomforts of what they were never brought up to undergo. Gallipoli Diary, Volume I |Ian Hamilton 

Edward sent him to London, 'fettered on a hackney,' to undergo the same barbarous death as his heroic brother. King Robert the Bruce |A. F. Murison 

Christianity was destined to undergo a final ordeal before it should ascend the throne of the Csars. The Catacombs of Rome |William Henry Withrow 

But as they that prepare a feast, and seek to satisfy the will of others: for the sake of many, we willingly undergo the labour. The Bible, Douay-Rheims Version |Various 

Soon after returning into school, Mr Tugman called him up to undergo the threatened examination. Digby Heathcote |W.H.G. Kingston