Amazon didn’t say when the service would be more widely available. Amazon jumps into smart wearables market with Halo activity tracking band |Aaron Pressman |August 27, 2020 |Fortune 

In the first months of the pandemic, South Korea’s test and trace methods were widely praised as exemplary, demonstrating how the technique can reduce transmission by isolating the infected and other potential carriers from the general populace. Hong Kong’s new mass COVID testing scheme is free and voluntary—and some citizens are suspicious |eamonbarrett |August 26, 2020 |Fortune 

Although Apple’s Siri is widely used on Apple devices, Amazon’s Alexa and Google’s digital assistant have made major inroads in helping people manage their lives, particularly in homes and offices. Apple CEO Tim Cook is fulfilling another Steve Jobs vision |Rachel Schallom |August 24, 2020 |Fortune 

Electrostatic disinfectant spraying is already widely used by airlines, including American competitors Southwest and Delta. American Airlines touts a new tool to combat COVID. But does it really make flying safer? |dzanemorris |August 24, 2020 |Fortune 

That can save hassle, but the effectiveness of filterless purifiers varies widely, and research on their effect on the coronavirus specifically is still limited. Can an air purifier help protect you from COVID-19? |dzanemorris |August 22, 2020 |Fortune 

This view is known as “theistic evolution” and is widely embraced by educated evangelicals. 2014: Revenge of the Creationists |Karl W. Giberson |December 27, 2014 |DAILY BEAST 

The source code for the original “Shamoon” malware is widely known to have leaked. No, North Korea Didn’t Hack Sony |Marc Rogers |December 24, 2014 |DAILY BEAST 

Abramson, biting her tongue, was widely portrayed in rival outlets as classily above the fray. The Bloodiest Media Coups of 2014 |Lloyd Grove |December 22, 2014 |DAILY BEAST 

They are fathers, describing to the rest of us what I thought was a widely acknowledged reality. The NY Police Union’s Vile War with Mayor De Blasio |Michael Tomasky |December 21, 2014 |DAILY BEAST 

Schiff, the Hollywood congressman, said that the movie should be promptly released and widely broadcast. Obama Could Hit China to Punish North Korea |Shane Harris, Tim Mak |December 20, 2014 |DAILY BEAST 

On the upper part of the stem the whorls are very close together, but they are more widely separated at the lower portion. How to Know the Ferns |S. Leonard Bastin 

You may, you probably will, differ very widely upon much that I have here put before you. The Salvaging Of Civilisation |H. G. (Herbert George) Wells 

Both Esbach's, which is very widely used, and the centrifugal method give fair results. A Manual of Clinical Diagnosis |James Campbell Todd 

I doubt if the modern community can afford to continue it; it certainly cannot afford to extend it very widely. The Salvaging Of Civilisation |H. G. (Herbert George) Wells 

Indeed, his success was widely known, and Jean Baptiste had been rather curious to know the family intimately. The Homesteader |Oscar Micheaux