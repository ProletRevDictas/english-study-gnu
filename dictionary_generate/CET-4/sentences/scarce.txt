The evidence is scarce, but since we don’t have more information, it’s better to be safe than sorry. How to prepare for getting the COVID-19 vaccine |Tara Santora |February 8, 2021 |Popular-Science 

The last year was the clearest example yet of what can happen when we allow stable housing options to become scarce. Covid-19 caused a recession. So why did the housing market boom? |Jerusalem Demsas |February 5, 2021 |Vox 

Larger stones are more scarce, but a larger stone is not necessarily more valuable. Hints From Heloise: Before buying a diamond, learn about the four C’s |Heloise Heloise |February 4, 2021 |Washington Post 

With vaccinations off to a rocky start globally, experts had been counting on a one-dose vaccine that would stretch scarce supplies and avoid the logistics nightmare of getting people to return for boosters. Johnson & Johnson’s single-dose COVID-19 vaccine is 66% effective, lagging rivals |Rachel Schallom |January 29, 2021 |Fortune 

Food is scarce, and the rodents vigorously attack intruders from other colonies. Naked mole-rat colonies speak with unique dialects |Jonathan Lambert |January 28, 2021 |Science News 

Now, visitors are scarce and the jungle is taking over, leaving some locals nostalgic. Six Must-Read Stories About the Sony Hacks, Congo’s Forgotten Colonial Getaway and Another Woman’s Story of U-VA |The Daily Beast |December 20, 2014 |DAILY BEAST 

We fight over their ownership and control, as if reality were a resource as scarce as the water and oil in Mad Max. On Torture, Chuck Johnson & Sondheim |James Poulos |December 13, 2014 |DAILY BEAST 

In a country where food was already scarce, slimmed-down portions could be the difference between life and death. Liberia’s Ebola Famine |Abby Haglage, Nina Strochlic |November 13, 2014 |DAILY BEAST 

After two decades of war, even the most basic infrastructure is scarce. A Belgian Prince, Gorillas, Guerrillas & the Future of the Congo |Nina Strochlic |November 6, 2014 |DAILY BEAST 

Food is becoming scarce, which has led to prices increasing beyond the reach of ordinary people. Fighting Ebola and Starvation in Sierra Leone |Abby Haglage |November 5, 2014 |DAILY BEAST 

And the girl, scarce believing her good fortune, departed with a speed that bordered on the ludicrous. St. Martin's Summer |Rafael Sabatini 

Fruit-trees are clearly too scarce, though Cherries in abundance were offered for sale as we passed. Glances at Europe |Horace Greeley 

Scarce a day passed without some engagement in which the King of Naples showed his audacity and his talent as a leader. Napoleon's Marshals |R. P. Dunn-Pattison 

When very scarce, they may sometimes be found, although their structure is not well shown, by the method of Ruge. A Manual of Clinical Diagnosis |James Campbell Todd 

But the way was toilsome, the heat intense, and the water scarce—more so than it had been on the outward journey. Hunting the Lions |R.M. Ballantyne