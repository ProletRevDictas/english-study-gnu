After struggling to play music while vacationing at a Lebanese ski resort in 2010 — iTunes was unavailable in the country then — founder Eddy Maroun raised funding to build a solution. The Biggest Challenge for Apple and Spotify in North Africa: YouTube |Eromo Egbejule |September 17, 2020 |Ozy 

Drivers of the Encore GX SUV can use the voice assistant to perform a number of tasks while they drive, from providing directions to playing music and ordering goods on the go. ‘Amazon is a brand play for us’: How Buick is building a long-term partnership around Amazon’s ad business |Seb Joseph |September 10, 2020 |Digiday 

Blum says that Facebook is best for her B2B relationships with other vendors, and Buonassissi uses YouTube to show videos of DJ sets and has a podcast about event music. Inside ‘Insta-Booking’: How couples are hiring wedding vendors via Instagram |Rachel King |September 6, 2020 |Fortune 

Search for “Katy Perry” and you will get a box next to the main search results telling you that Katy Perry is an American singer-songwriter with music available on YouTube, Spotify, and Deezer. This know-it-all AI learns by reading the entire web nonstop |Will Heaven |September 4, 2020 |MIT Technology Review 

The order is the result of the agencies cartel-like behavior in the past, and ensures they can’t arbitrarily withhold access to songs or gouge those who license the music. Trump and ‘Hallelujah’: Why it’s so hard to stop campaigns from playing songs, even when artists object |Jeff |August 31, 2020 |Fortune 

Music is a huge part of the tone of Black Dynamite overall—going back to the original 2009 movie on which the series is based. ‘Black Dynamite’ Presents Police Brutality: The Musical |Stereo Williams |January 9, 2015 |DAILY BEAST 

I gotta say—I think this past year was pretty bad for music. Coffee Talk with Fred Armisen: On ‘Portlandia,’ Meeting Obama, and Taylor Swift’s Greatness |Marlow Stern |January 7, 2015 |DAILY BEAST 

What an amazing thing to be able to listen to any music you want, a whole world of bands. Belle & Sebastian Aren’t So Shy Anymore |James Joiner |January 7, 2015 |DAILY BEAST 

Do you think that as we get older our thoughts shift to the more abstract, the music, than the definite, the lyrics? Belle & Sebastian Aren’t So Shy Anymore |James Joiner |January 7, 2015 |DAILY BEAST 

I remember all our music appeared on Spotify overnight, without anybody asking us. Belle & Sebastian Aren’t So Shy Anymore |James Joiner |January 7, 2015 |DAILY BEAST 

Henry Rowley Bishop, a noted English music composer, died, aged 68. The Every Day Book of History and Chronology |Joel Munsell 

In Manila particularly, amidst the pealing of bells and strains of music, unfeigned enthusiasm and joy were everywhere evident. The Philippine Islands |John Foreman 

He takes a turn up and down the room, looks at the music, and if the piece interests him, he will call upon you. Music-Study in Germany |Amy Fay 

A child outside the temple of art hears its music before he sees its veiled beauties. Children's Ways |James Sully 

Then, as he neared the room, a sound of music floated out to meet him— Tony was singing to his own accompaniment. The Wave |Algernon Blackwood