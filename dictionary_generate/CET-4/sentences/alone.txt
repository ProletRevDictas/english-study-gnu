Genetics alone does not an eating disorder make, generally speaking, and Bulik points out that environment still plays a role. How Skinny Is Too Skinny? Israel Bans ‘Underweight’ Models |Carrie Arnold |January 8, 2015 |DAILY BEAST 

I watch every episode alone on my couch and I just sit there and laugh, and laugh. Coffee Talk with Fred Armisen: On ‘Portlandia,’ Meeting Obama, and Taylor Swift’s Greatness |Marlow Stern |January 7, 2015 |DAILY BEAST 

By contrast, John McCain, the eventual GOP nominee, had raised approximately $12.7 million in the first quarter of 2007 alone. Can Huckabee Convert the GOP’s Moneymen? |Lloyd Green |January 4, 2015 |DAILY BEAST 

Nor should we ever assume that weather alone, however extreme, should be fatal to a commercial flight. Did Bad Weather Bring Down AirAsia 8501? |Clive Irving |December 29, 2014 |DAILY BEAST 

Women and children are disproportionately victims, but they are not alone. ISIS, Boko Haram, and the Growing Role of Human Trafficking in 21st Century Terrorism |Louise I. Shelley |December 26, 2014 |DAILY BEAST 

Ten minutes later, veiled and cloaked, she stepped out alone into the garden. Hilda Lessways |Arnold Bennett 

The two women had no intention of bathing; they had just strolled down to the beach for a walk and to be alone and near the water. The Awakening and Selected Short Stories |Kate Chopin 

When we were mounted Mac leaned over and muttered an admonitory word for Piegan's ear alone. Raw Gold |Bertrand W. Sinclair 

If Mac had been alone he would have made the post by sundown, for the Mounted Police rode picked horses, the best money could buy. Raw Gold |Bertrand W. Sinclair 

Alone Orlean lay trying vainly to forget something—something that stood like a spectre before her eyes. The Homesteader |Oscar Micheaux