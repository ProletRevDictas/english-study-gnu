As Yellowstone bears increasingly wander outside the sanctuary of the park, they run an ever-greater risk of getting shot. What It Takes to Kill a Grizzly Bear |Doug Peacock |November 23, 2014 |DAILY BEAST 

Some kids are prone to letting their minds wander and daydreaming. Daydreaming Is Not a Disorder |Russell Saunders |October 4, 2014 |DAILY BEAST 

Save for the clip of heels as visitors wander around the gallery, the venue is silent. The Dark Rock Star Fantasy of Saint Laurent’s Hedi Slimane |Liza Foreman |September 24, 2014 |DAILY BEAST 

Finally, I have to wander slightly off brief with my last two recommendations. Doctors Can Write More Than Prescriptions: The Best Books by Doctors |Gabriel Weston |August 14, 2014 |DAILY BEAST 

One can even rent out the villa here or wander down from the Arc de Triomphe and peek through the fence. Paris’s Secret Fashion Week Haunts |Liza Foreman |July 8, 2014 |DAILY BEAST 

He allowed his mind to wander back into the past—back many years to the time when he had gone into the country to take a meal. The Homesteader |Oscar Micheaux 

Of all the inhabitants of London chambers those are the most lonely who never wander away from London. The Doctor of Pimlico |William Le Queux 

Sometimes Jehosophat's father opens the gate in the fence and lets the geese wander down to the pond. Seven O'Clock Stories |Robert Gordon Anderson 

Literally and figuratively, their lives seemed to wander through flowery pleasure-paths. Ruth |Elizabeth Cleghorn Gaskell 

She appeared to wander in her mind, for sometimes her words were coherent, at other times she merely babbled. The Floating Light of the Goodwin Sands |R.M. Ballantyne