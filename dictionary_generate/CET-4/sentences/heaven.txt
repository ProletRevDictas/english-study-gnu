For the remaining usually visible planets in our heavens, February will be a little tough. Skywatch: What’s happening in the heavens in February |Blaine P. Friedlander Jr. |January 30, 2021 |Washington Post 

This is a meditative show for people who can’t stomach meditation apps—and thank heavens for that. The 5 Best New Shows Our TV Critic Watched in January 2021 |Judy Berman |January 29, 2021 |Time 

He says he is an engineering student at the University of Alberta, in Canada—“not much of a party person,” by his own admission, and happier playing Minecraft or scouring the heavens with his telescope. A Reddit user on what it’s like to be part of “the mother of all short squeezes” |Samanth Subramanian |January 28, 2021 |Quartz 

Instead, hold on to the patience you foster while putting one foot in front of the other for six months, since you might need it as you recover from putting your body and brain through both heaven and hell at once. Did Thru-Hiking the Appalachian Trail Ruin My Body? |Grayson Haver Currin |January 25, 2021 |Outside Online 

Our next-door planetary neighbor Mars is high in the southern heavens as evening falls in the new year. Skywatch: What’s happening in the heavens in January |Blaine P. Friedlander Jr. |December 26, 2020 |Washington Post 

“I´m now writing to you from goat heaven,” he lamented on the blog he maintains. Sweden’s Burning Christmas Goat |Nina Strochlic |December 25, 2014 |DAILY BEAST 

In its opening weekend the movie Heaven Is For Real (budget: $12 million) doubled its gross. The Biggest Bombs of 2014: ‘Sex Tape,’ Mariah Carey’s Vocals, ‘How I Met Your Mother’ and More |Kevin Fallon |December 19, 2014 |DAILY BEAST 

Hatuey asked the religious man holding the flame if indeed any Christians were in heaven. The Life and Hard Times Of The Family A Cuban Defector Left Behind |Brin-Jonathan Butler |December 19, 2014 |DAILY BEAST 

But assuming things were ever that hopeful, heaven was short-lived, and trouble followed. The Sydney Astrologer Turned Islamic Radical |Jacob Siegel |December 16, 2014 |DAILY BEAST 

After all, according to lyrics he once wrote, “all good cretins go to heaven.” ‘All Good Cretins Go to Heaven’: Dee Dee Ramone’s Twisted Punk Paintings |Melissa Leon |December 15, 2014 |DAILY BEAST 

Decide about it, ye that are learned in the ethnographic distinctions of our race—but heaven defend us from the Bourbonnaises! Blackwood's Edinburgh Magazine, Volume 60, No. 372, October 1846 |Various 

And if an earthly father would act thus wisely and thus kindly, "how much more your Father which is in Heaven?" God and my Neighbour |Robert Blatchford 

The expression of his features was calm and composed, and his eyes were raised to heaven with a look of hope and supplication. Blackwood's Edinburgh Magazine, No. CCCXXXIX. January, 1844. Vol. LV. |Various 

A burning crimson flushed over the cheek of Wharton, as Louis uttered this ardent appeal to friendship and to Heaven. The Pastor's Fire-side Vol. 3 of 4 |Jane Porter 

In the state Louis was in, between man's perfidy and woman's wiles, any refuge from the world, seemed a heaven to him. The Pastor's Fire-side Vol. 3 of 4 |Jane Porter