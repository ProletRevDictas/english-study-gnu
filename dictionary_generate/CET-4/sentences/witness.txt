Since 2011, legal scholars, political scientists and mathematicians conducting gerrymandering research have served as expert witnesses in more than 250 state and federal court cases regarding redistricting. How next-gen computer generated maps detect partisan gerrymandering |Sujata Gupta |September 7, 2020 |Science News 

New York Attorney General Letitia James said Monday that she’s suing to enforce seven subpoenas seeking thousands of documents and testimony from multiple witnesses. New York’s AG is investigating whether Trump’s company falsely reported asset valuations to get loans |Lee Clifford |August 24, 2020 |Fortune 

Though the review happened years ago, it’s newly relevant in light of a November ballot measure that would give the board more power to access witnesses and documents. VOSD Podcast: Cops and Cameras, Cameras and Cops |Nate John |July 24, 2020 |Voice of San Diego 

When the community review board – then known as the Citizens Review Board – investigated the shooting, it reviewed that security camera footage, as well as witness statements, police reports, photos from the scene, SDPD policies and other materials. Police Review Board Was Denied Docs, Interviews in 2015 Shooting Review |Sara Libby |July 22, 2020 |Voice of San Diego 

Our goal was to be the first confirmed humans to witness a total solar eclipse from a peak above 20,000 feet. My Crazy Bid to See a Solar Eclipse at 20,000 Feet |Mark Jenkins |July 8, 2020 |Outside Online 

One witness said the gunfire began after a traffic collision, which drew the attention of a nearby police officer. France Mourns—and Hunts |Nico Hines, Christopher Dickey |January 8, 2015 |DAILY BEAST 

The mother, Emily Kruse, was charged with obstructing justice and intimidating a witness. Judge: Rehoming Kids Is Trafficking |Tina Traster |December 30, 2014 |DAILY BEAST 

Jay, the main witness for the state, is leading a relatively normal life, though that probably has all changed with Serial. The Scoop on ‘Serial’: Making Sense of The Nisha Call, Asia's Letters, and Our Obsession |Emily Shire |December 11, 2014 |DAILY BEAST 

There is no perfect juror, just as there is no such thing as a perfect witness. The Post-Brown and Garner Question: Who ‘Deserves’ to Die? |Goldie Taylor |December 9, 2014 |DAILY BEAST 

“The senior people were too shocked to speak,” said a witness. Facebook Prince Purges The New Republic: Inside the Destruction of a 100-Year-Old Magazine |Lloyd Grove |December 5, 2014 |DAILY BEAST 

At his desk sat his secretary, who had been a witness of the interview, lost in wonder almost as great as the Seneschal's own. St. Martin's Summer |Rafael Sabatini 

And now everybody turned out with a feeling of intense relief to witness the rejoicings on the village green. The Pit Town Coronet, Volume I (of 3) |Charles James Wills 

The endless miles of railways, the vast apparatus of the factories, the soaring structures of the cities bear easy witness to it. The Unsolved Riddle of Social Justice |Stephen Leacock 

In a thousand trials the cruel witness of Moses has sent innocent women to a painful death. God and my Neighbour |Robert Blatchford 

Seven years from this present time will witness the Centenary of the railway system. Fifty Years of Railway Life in England, Scotland and Ireland |Joseph Tatlow