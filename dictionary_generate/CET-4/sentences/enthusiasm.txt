Now Tinder will find out if audiences in the rest of the world, where its competes with a large roster of other dating apps, will respond to “Swipe Night” with the same level of enthusiasm. Tinder’s interactive video event ‘Swipe Night’ will launch in international markets this month |Catherine Shu |September 4, 2020 |TechCrunch 

Hadiqa Bashir, on an attempted marriage when she was 11When we chat, Bashir’s voice is filled with a nonchalant and childish enthusiasm, and yet there is something omniscient about the way she speaks. The Teenager Breaking Up Child Marriages, Door to Door |Pallabi Munsi |September 2, 2020 |Ozy 

Despite pleas to stay away, the president said his visit to Kenosha would “increase enthusiasm” in Wisconsin—a key battleground state ahead of November’s presidential election. Despite pleas to stay away, Trump visits Kenosha |Aric Jenkins |September 1, 2020 |Fortune 

Sprokkreeff offered a company-wide mental health day at Webprofits this year, to much enthusiasm. How managers can recognize burnout remotely |Kristine Gill |August 28, 2020 |Fortune 

The upcoming four-for-one stock split, a move that has no effect on share price but often spurs investor enthusiasm, is one measure of Apple’s success under Cook. Apple CEO Tim Cook is fulfilling another Steve Jobs vision |Rachel Schallom |August 24, 2020 |Fortune 

And similar shards of enthusiasm-killing kryptonite are lodged in John Kasich, Mike Pence and Ted Cruz. Why This Liberal Hearts Huckabee |Sally Kohn |January 6, 2015 |DAILY BEAST 

Having finally seen Selma on November 17, I must report, sadly, that I do not share the enthusiasm the film has generated so far. Dr. King Goes to Hollywood: The Flawed History of ‘Selma’ |Gary May |January 2, 2015 |DAILY BEAST 

With the exception of New Hampshire, Paul has not demonstrated potential enthusiasm in the early primary states. GOP Won’t Forgive Rand for Cop Critique |Lloyd Green |December 23, 2014 |DAILY BEAST 

Yet for all his enthusiasm for the American film industry, he remained forever an expatriate. Alfred Hitchcock’s Fade to Black: The Great Director’s Final Days |David Freeman |December 13, 2014 |DAILY BEAST 

I so loved the fierce bodily contact of football that I suppose my enthusiasm made up somewhat for my lack of size. How His West Point Football Experience Inspired Eisenhower |Nicolaus Mills |November 11, 2014 |DAILY BEAST 

These practical demonstrations occurred usually in the opening enthusiasm of the term. The Salvaging Of Civilisation |H. G. (Herbert George) Wells 

In Manila particularly, amidst the pealing of bells and strains of music, unfeigned enthusiasm and joy were everywhere evident. The Philippine Islands |John Foreman 

Man's enthusiasm in praise of a fellow mortal, is soon damped by the original sin of his nature—rebellious pride! The Pastor's Fire-side Vol. 3 of 4 |Jane Porter 

Nigel's enthusiasm seemed almost visibly to exhale from the paper as Isaacson held the letter in his hands. Bella Donna |Robert Hichens 

To attempt to cut out Mrs. Kaye I should need a little genuine enthusiasm; and frankly, your beloved prodigy does not inspire it. Ancestors |Gertrude Atherton