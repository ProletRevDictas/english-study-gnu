Through Stafford and southern Charles counties about one to three inches fell. D.C.-area forecast: Raw today. Iciness threat grows Saturday into early Sunday. |A. Camden Walker |February 12, 2021 |Washington Post 

Still, the chance of picking up a quick inch somewhere along the line seems good. D.C.-area forecast: Snow and wintry mix taper off this morning, while ice concerns mount for Saturday |David Streit |February 11, 2021 |Washington Post 

Mittens that fit properly will extend about a quarter of an inch from your outstretched fingers and allow you to comfortably make a fist. Best mittens: Keep your hands cozy |PopSci Commerce Team |February 8, 2021 |Popular-Science 

In parts of Southern Maryland, however, where the snow was heavier, amounts were as high as two to four inches despite similar temperatures. After Sunday’s slush fest, another winter storm threatens by Wednesday night |Jason Samenow, Wes Junker |February 8, 2021 |Washington Post 

The lessons started with shooting and more shooting, and nature helped as Lee grew an inch a year. Lee for 3! Can This South Korean Break the NBA Barrier? |Daniel Malloy |February 5, 2021 |Ozy 

Neil Patrick Harris, Hedwig and the Angry Inch Neil Patrick Harris in fishnets, high heels, and glitter could be a great gag. Hedwig, Hugh & Michael Cera: 12 Powerhouse Theater Performances of 2014 |Janice Kaplan |December 31, 2014 |DAILY BEAST 

With a 1¾-inch ice cream scoop (or two spoons), scoop round balls of dough onto the prepared sheet pans. Make These Barefoot Contessa Salty Oatmeal Chocolate Chunk Cookies |Ina Garten |November 28, 2014 |DAILY BEAST 

Cut the phyllo in half crosswise to make two (7 × 8½-inch) rectangles. The Barefoot Contessa’s Tasty Trip to Paris |Ina Garten |November 27, 2014 |DAILY BEAST 

Anna Whiston-Donaldson is a popular blogger at An Inch of Gray. Book Bag: Reading Your Way Out Of Grief |Anna Whiston-Donaldson |October 16, 2014 |DAILY BEAST 

In the coming month, Maynard may play a crucial role in helping the GOP inch forward on this controversial issue. The Beautiful Newlywed Who Made the Right Change Its Mind on Physician-Assisted Death |Samantha Allen |October 10, 2014 |DAILY BEAST 

Clodd tells us that one cubic inch of rotten stone contains 41 thousand million vegetable skeletons of diatoms. God and my Neighbour |Robert Blatchford 

I believe you will find this an exceeding good duty for a 5½-inch cylinder engine. Life of Richard Trevithick, Volume II (of 2) |Francis Trevithick 

The care shown in rearing insures a perfect straightness of stem, and an equable diameter of about an inch or an inch and a half. Tobacco; Its History, Varieties, Culture, Manufacture and Commerce |E. R. Billings. 

He was absorbed in the agitated present, and dared not look an inch away from it. Blackwood's Edinburgh Magazine, No. CCCXXXIX. January, 1844. Vol. LV. |Various 

Search with a one-twelfth-inch objective, using very subdued light. A Manual of Clinical Diagnosis |James Campbell Todd