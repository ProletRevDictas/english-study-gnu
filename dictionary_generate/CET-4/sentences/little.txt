Mounting a winch can take a little time, and you’ll usually need a bracket specific to your ATV, but it’s not all that difficult. Six ways to make your ATV even more rugged |By Tyler Freel/Outdoor Life |September 17, 2020 |Popular-Science 

We have very little problem in this country at this moment — five. Timeline: The 124 times Trump has downplayed the coronavirus threat |Aaron Blake, JM Rieger |September 17, 2020 |Washington Post 

A little more than a month ago, the Big Ten became the first major conference to postpone the season. Trump contradicts CDC director on vaccine; Biden says Americans shouldn’t trust Trump |Colby Itkowitz, Felicia Sonmez, John Wagner |September 16, 2020 |Washington Post 

Later, a researcher from Yale and Pew Research Center conducted separate tests that also found little to no evidence in support of the claim. If voters are wary of stating support for Trump in polls, why does he outperform GOP Senate candidates? |Philip Bump |September 16, 2020 |Washington Post 

Separately, the CEO of TripAction says business travel has picked up a little recently, but is still down about 80% since February—compared to the 90% drop in March. How businesses are preparing for the U.S. election outcome |Alan Murray |September 16, 2020 |Fortune 

But Babylon asks us to do a little more: It wants us to empathize. 'Babylon' Review: The Dumb Lives of Trigger-Happy Cops |Melissa Leon |January 9, 2015 |DAILY BEAST 

Everybody is trapped in an elevator together and tempers run a little hot. ‘Archer’ Creator Adam Reed Spills Season 6 Secrets, From Surreal Plotlines to Life Post-ISIS |Marlow Stern |January 8, 2015 |DAILY BEAST 

How about baby drama with little Abijean and the Wee Baby Seamus? ‘Archer’ Creator Adam Reed Spills Season 6 Secrets, From Surreal Plotlines to Life Post-ISIS |Marlow Stern |January 8, 2015 |DAILY BEAST 

I was a little mystified at how benignly he responded to my questions about his business activities. I Tried to Warn You About Sleazy Billionaire Jeffrey Epstein in 2003 |Vicky Ward |January 7, 2015 |DAILY BEAST 

Scalise spoke briefly, adding little of substance, saying that the people back home know him best. The Price of Steve Scalise’s Silence |Jason Berry |January 7, 2015 |DAILY BEAST 

It is most peculiar, and when he plays that way, the most bewitching little expression comes over his face. Music-Study in Germany |Amy Fay 

In less than ten minutes, the bivouac was broken up, and our little army on the march. Blackwood's Edinburgh Magazine, No. CCCXXXIX. January, 1844. Vol. LV. |Various 

A little boy of four was moved to passionate grief at the sight of a dead dog taken from a pond. Children's Ways |James Sully 

Squinty could look out, but the slats were as close together as those in a chicken coop, and the little pig could not get out. Squinty the Comical Pig |Richard Barnum 

Then there was Wee Wo,—he was a little Chinese chap, and we used to send him down the chimneys to open front doors for us. Davy and The Goblin |Charles E. Carryl