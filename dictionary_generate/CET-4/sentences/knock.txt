Even though arthritis swells beneath both of my knock knees. How a Plus-Size Hiker Found Her Footing on the Trail |syadron |August 22, 2021 |Outside Online 

The “never voted” line is a knock at Carey, who has never held office. The Trailer: Who sponsored this message? Why you're seeing so many ads so far from the midterms. |David Weigel |July 29, 2021 |Washington Post 

They plan to spend a week of vacation wallowing in the glorious luxuries of a house they are not rich enough to own but are rich enough to aspire toward — but then, on their first night at the house, there’s a knock at the door. Leave the World Behind’s bougie apocalypse |Constance Grady |June 11, 2021 |Vox 

Perhaps the biggest knock on the play-in tournament is that it is somewhat difficult to explain. LeBron James wants the NBA’s play-in guy fired. The NBA’s play-in guy disagrees. |Ben Golliver |May 5, 2021 |Washington Post 

Mallory Talbott, who manages a barbecue restaurant, said parents knew to call the house phone if no one answered a knock. The Child Care Industry Was Collapsing. Mrs. Jackie Bet Everything on an Impossible Dream to Save It. |by Lizzie Presser |May 1, 2021 |ProPublica 

This courageous act earned him a late-night knock on the door with orders for Serna to vamos from Cuba. Obama’s One Hand Clap With Castro |Doug McIntyre |December 24, 2014 |DAILY BEAST 

In the back of my mind I was wondering how much time we had before there might be an ominous knock at the door. The Life and Hard Times Of The Family A Cuban Defector Left Behind |Brin-Jonathan Butler |December 19, 2014 |DAILY BEAST 

I was writing Lorrie Moore knock-off short stories before I switched to nonfiction. Meghan Daum On Tackling The Unspeakable Parts Of Life |David Yaffe |December 6, 2014 |DAILY BEAST 

Still, we had the 13 dwarves to deal with, but at least in this movie we get to knock a couple off, which is a relief. ‘No Regrets’: Peter Jackson Says Goodbye to Middle-Earth |Alex Suskind |December 4, 2014 |DAILY BEAST 

But hey, if you want to take on Cosby for telling you to stay in school, knock yourself out. When Your Comic Hero Is an Alleged Rapist |Doug McIntyre |November 18, 2014 |DAILY BEAST 

If the Turks get hold of a lot of fresh men and throw them upon us during the night,—perhaps they may knock us off into the sea. Gallipoli Diary, Volume I |Ian Hamilton 

There was no response to the knock, and Davy cautiously pushed open the door and went in. Davy and The Goblin |Charles E. Carryl 

Captain Lovelock got up as well; Bernard heard him knock over his little gilded chair. Confidence |Henry James 

“I think it is a knock at the door,” said Mr. Pickwick, as if there could be the smallest doubt of the fact! The Posthumous Papers of the Pickwick Club, v. 2(of 2) |Charles Dickens 

But a knock at the door interrupted them; the discreet Capt entered, bearing a telegram upon a salver. The Pit Town Coronet, Volume I (of 3) |Charles James Wills