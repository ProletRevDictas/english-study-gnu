Of course this was 40 years ago, which to a digital Millennial might as well make it 99 BC. My Time on the Set of 'Jaws,' or How to Get a Photo of a Frickin' Mechanical Shark |Tom Shales |August 17, 2014 |DAILY BEAST 

The first unambiguous evidence of fortification walls dates from around 4300 BC in what is now Turkey. War! What Is It Good For? A Lot |Nick Romeo |August 13, 2014 |DAILY BEAST 

For hundreds of years, around the first millennium BC, the house of worship and its home city were renowned as holy sites. Iraq’s Long-Lost Mythical Temple Has Been Found…and Is In Danger of Disappearing Again |Nina Strochlic |July 24, 2014 |DAILY BEAST 

Later, around 400 BC, they invented faloodeh: rice water, vermicelli, and ice mixed with saffron and/or fruit. An Investigation Into the Delicious Origins of Ice Cream |Andrew Romano |July 13, 2014 |DAILY BEAST 

Built circa 2700 BC, it is the oldest freestanding mud-brick building in the world. The Nile: Where Ancient and Modern Meet |William O’Connor |June 21, 2014 |DAILY BEAST 

Machine ab to bc, of the sole piece (fig. 2), open and press the seam. Needlework Economies |Various 

For all permanent purposes bc-fel, or book-skin, was used; either vellum or parchmyn smothe, whyte and scribable. Old English Libraries |Ernest Savage 

Measure the distance bc between the top of the mercury and the closed end of the tube. General Science |Bertha M. Clark 

If we supply the food unit by unit, the utility of the successive increments will decline along the curve BC. Essentials of Economic Theory |John Bates Clark 

It is, in like manner, the absolute utility of the successive increments supplied which declines along the curve BC. Essentials of Economic Theory |John Bates Clark