Perhaps it was perpetuated by the need to seek refuge whenever something about the world would confuse or frighten me. How I Escaped My Troubles Through Science - Issue 104: Harmony |Subodh Patil |August 25, 2021 |Nautilus 

As Barry recounts, at first Damji was frightened by all sounds, “because they were meaningless.” Our brains exist in a state of “controlled hallucination” |Matthew Hutson |August 25, 2021 |MIT Technology Review 

When you see its expansive, vast scale — how dynamic it is, how much it can change by the season — and you realize how critically important it is, the prospect of losing it does frighten me. Humanity’s greatest ally against climate change is the Earth itself |Sarah Kaplan |April 22, 2021 |Washington Post 

This disparity is not due to pandemic-frightened advertisers redirecting dollars away from publishers to put into platforms like Google, Facebook and Amazon. Media Briefing: Gimme data control, say publishers to identity tech firms |Tim Peterson |April 8, 2021 |Digiday 

Lupe Villagrana, the manager at Ridgepoint Apartments in Vista, recalled an incident last year in which one of her tenants accidentally broke into the wrong unit while intoxicated and frightened the neighbor. County’s ‘Crime-Free Housing’ Programs Walk Fine Line Between Safety, Surveillance |Jesse Marx |March 16, 2021 |Voice of San Diego 

Perhaps made doubly frightening because not only does the old man frighten Garfield and Odie, but he steals their candy as well. Garfield Television: The Cat Who Saved Primetime Cartoons |Rich Goldstein |November 5, 2014 |DAILY BEAST 

Indeed, his inept attempts to frighten off Stewie only make the situation worse. Tim Winton's Beautiful, Baffling 'Eyrie' |Wendy Smith |August 18, 2014 |DAILY BEAST 

Big scary Transformer-like robots with heads ablaze that frighten the kids back across the treacherous desert? The So-Called Immigration Border Crisis Is Neither |Sally Kohn |July 10, 2014 |DAILY BEAST 

Their goal is to frighten women who have objected to forced hijab. Iran Says Take Off the Veil—and Be Raped |IranWire |June 9, 2014 |DAILY BEAST 

This happens to be the perfect course of action, better to frighten you with the hellish episode of slavery. The ‘12 Years a Slave’ Book Shows Slavery As Even More Appalling Than In the Film |Jimmy So |October 18, 2013 |DAILY BEAST 

They foreclose without mercy, but that does not frighten their old patrons, who have the perennial optimism of the country. Ancestors |Gertrude Atherton 

They did at first endeavour with their weapons to frighten us, who, lying ashore, deterred them from one of their fishing-places. Narrative of a Survey of the Intertropical and Western Coasts of Australia] [Volume 2 of 2] |Phillip Parker King 

His hand was not in; I had nothing to frighten him with, which we always must have in the beginning, or we labour in vain. Oliver Twist, Vol. II (of 3) |Charles Dickens 

But instead of driving home their attack they thought to frighten them by a mere cannonade. Napoleon's Marshals |R. P. Dunn-Pattison 

Should Gorton turn up he is just the one to frighten a defenceless woman, and purchase his own silence. Elster's Folly |Mrs. Henry Wood