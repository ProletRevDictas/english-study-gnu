Similar feelings have doubtless been captured in recent photographs, as Americans lament the potential loss of their republic within an epistemological shift away from truth and evidence. At the Met, beautiful paintings that won’t love you back |Philip Kennicott |June 24, 2021 |Washington Post 

Pete Buttigieg, doubtless, welcomes this “first,” but it will not define him. Pete Buttigieg as Cabinet secretary is more than a ‘first’ |Charles Francis |December 19, 2020 |Washington Blade 

It was doubtless a warm reunion with his family, who are featured in The Cuban Wives. Of Cuban Spies, a Baby, and a Filmmaker: The Strange Tale of the Cuban Five |Nina Strochlic |December 28, 2014 |DAILY BEAST 

Doubtless John Kerry, too, would like to see another Israeli leader with whom he could dance a real peace process. The Inside Story of U.S. Meddling in Israel’s Elections |Aaron David Miller |December 4, 2014 |DAILY BEAST 

It is doubtless this sojourn that accounts for her ability to sing in French. Meet the Future Mrs. Benedict Cumberbatch |Tom Sykes |November 5, 2014 |DAILY BEAST 

And McCain had every reason to be happy that he had put Carney (and, by extension, he doubtless hoped, Obama) on the defensive. John McCain Roughs Up Jay Carney During His CNN Primetime Debut |Lloyd Grove |September 11, 2014 |DAILY BEAST 

Fans will doubtless flock to Austin to see that dress and two others that adorned Vivien Leigh. How 'Gone With the Wind' Got Made |Helen Anders |September 10, 2014 |DAILY BEAST 

As there are still many varieties of the plant grown in America, so there doubtless was when cultivated by the Indians. Tobacco; Its History, Varieties, Culture, Manufacture and Commerce |E. R. Billings. 

Judged from this point of view only, the elasticity provided by the new law is doubtless adequate. Readings in Money and Banking |Chester Arthur Phillips 

Doubtless the commentator habit is fixed in the nature of man; but it was pre-eminently mediaeval. The Mediaeval Mind (Volume II of II) |Henry Osborn Taylor 

So intelligent were her methods that she doubtless had great influence in making the memory of his art enduring. Women in the fine arts, from the Seventh Century B.C. to the Twentieth Century A.D. |Clara Erskine Clement 

After the Reserve Banks have been in operation long enough to be running smoothly, not a few branches will doubtless be organized. Readings in Money and Banking |Chester Arthur Phillips