The fan of feathers at the base of its tail would have resembled those typically observed in present-day birds such as pigeons or sparrows. This 120-million-year-old bird may have been one of the first to shake its tail feathers |Kate Baggaley |September 17, 2021 |Popular-Science 

In the new study, Rogalla and colleagues turned to sunbirds, nectar-feeding birds typically smaller than sparrows that are native to Africa, Asia and Australia. Sunbirds’ dazzling feathers are hot, in both senses of the word |Jake Buehler |August 17, 2021 |Science News 

Out my window, the sparrows were chirping excitedly, not ready to call it a day. An Existential Crisis in Neuroscience - Issue 94: Evolving |Grigori Guitchounts |December 30, 2020 |Nautilus 

Without all the background sound, common sparrows in San Francisco changed their tunes to have a broader vocal range and softer notes. 2020 isn’t all bad. Here are 13 science stories to be thankful for. |Rachel Feltman, Sara Chodosh |November 26, 2020 |Popular-Science 

“No eyes are on the sparrow, eyes are on the sparrow / He is singing anyway.” Is Bigger Better for St. Vincent? |David Yaffe |December 4, 2014 |DAILY BEAST 

Before she finished I began to paint, and she resumed the pose, smiling and chattering like a sparrow. Read ‘The King in Yellow,’ the ‘True Detective’ Reference That’s the Key to the Show |Robert W. Chambers |February 20, 2014 |DAILY BEAST 

A man “in traditional Alpine costume” addresses a group of children while holding a sparrow. The Kaffeehaus Canon |George Prochnik |December 31, 2010 |DAILY BEAST 

I can hear her now warbling her own rendition of "His Eye Is on the Sparrow." Dame Edna Mouths Off |Kevin Sessums |March 17, 2010 |DAILY BEAST 

But she is a girl, and she does not mind her jack-sparrow being a trifle headstrong, if only he has a kind heart. Child Life In Town And Country |Anatole France 

Over near Neighbour Brown's fence they were peeping through the green leaves at the song-sparrow's nest. Seven O'Clock Stories |Robert Gordon Anderson 

A song sparrow was singing out by the road, and the thin, sweet flutings of a Peabody came from the pasture. The Idyl of Twin Fires |Walter Prichard Eaton 

A magnificent view is had from Sparrow Hill; the ascent is made by a steep and tortuous road. Ways of War and Peace |Delia Austrian 

Was the girl going away any real part of the strength and beauty of the old Sparrow place? Growing Up |Jennie M. Drinkwater