“I’m hopeful the downward trend in positive cases means that we can start shifting our focus on reopening things and less on enforcement,” he said. Despite Crackdown Announcement, Not Much COVID-19 Enforcement Is Happening |Jesse Marx |February 11, 2021 |Voice of San Diego 

The longer we wait, the more difficult it will be to turn emissions downward. Countries must ramp up climate pledges by 80 percent to hit key Paris target, study finds |Brady Dennis |February 9, 2021 |Washington Post 

In theory, those policies should also help ease the burden to families of raising children, thus slowing the downward spiral in the nation’s birth rate. Head of Tokyo Olympic committee says it must limit women because they talk too much |Clay Chandler |February 4, 2021 |Fortune 

For a country whose economy has been on a downward slope since demonetisation in 2016, India’s management of the pandemic proved to be the final nail in the coffin. India hopes for a V-shaped recovery, but will it give the unemployed their careers back? |Aarefa Johari |February 3, 2021 |Quartz 

Be aware there is some visual distortion with these lenses when looking downward. Best ski goggles: What to look for in a pair you’ll love |Eric Alt |January 20, 2021 |Popular-Science 

The now-convicted felons will hear their sentences in January, but their story continues to spiral downward. 2014 Was a Delectably Good Year for Sleaze |Patricia Murphy |December 30, 2014 |DAILY BEAST 

This downward spiral involving local power politics was obvious to the Americans in the valley. Heart of Darkness: Into Afghanistan’s Taliban Valley |Matt Trevithick, Daniel Seckman |November 15, 2014 |DAILY BEAST 

To his fellow survivors and to the audience, this delusion indicates another slip on a downward spiral. The Walking Dead’s Luke Skywalker: Rick Grimes Is the Perfect Modern-Day Mythical Hero |Regina Lizik |October 28, 2014 |DAILY BEAST 

All of these rules grew organically from the community rather than being dictated downward by a cen­tral authority. You Can Look It Up: The Wikipedia Story |Walter Isaacson |October 19, 2014 |DAILY BEAST 

The number of families who struggle to put food on the table has barely inched downward, even though employment is up. Congress Unites to Screw the Hungry |Monica Potts |September 8, 2014 |DAILY BEAST 

Now here was a lover's meeting, not lacking the shy, downward glance of dark eyes as steel-blue eyes flashed frank admiration. Dope |Sax Rohmer 

Their path here separated, Mrs. Martin following the downward course of the sandy lane, and Dorothy climbing the hill. The World Before Them |Susanna Moodie 

One swift downward thrust Garnache made at the mass that wriggled under his cloak. St. Martin's Summer |Rafael Sabatini 

Rising to a point where it cools, the vapour gathers back on the rafts and tends again to weight the cloud downward. Outlines of the Earth's History |Nathaniel Southgate Shaler 

When they start downward they have, as observations show, a temperature not much above the freezing point of salt water. Outlines of the Earth's History |Nathaniel Southgate Shaler