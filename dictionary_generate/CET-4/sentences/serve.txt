Logan and I seemed to be good teammates—he was scratching the surface of what would later be a nasty serve, and I liked to relive my baseball-playing glory days by diving for a loose ball whenever possible. Spikeball Is the Perfect Pandemic Activity |smurguia |August 10, 2021 |Outside Online 

Nadal produced eight winners and drew 17 errors from Djokovic on the serve plus one. Rafael Nadal Dominates The Third Shot Of A Point |Amy Lundy |June 7, 2021 |FiveThirtyEight 

First, when serving, he’s winning the point on his first shot after the serve, a term that in recent years has come to be called “serve plus one.” Rafael Nadal Dominates The Third Shot Of A Point |Amy Lundy |June 7, 2021 |FiveThirtyEight 

Nadal’s opponents can’t seem to do the same thing to him with their serve plus one. Rafael Nadal Dominates The Third Shot Of A Point |Amy Lundy |June 7, 2021 |FiveThirtyEight 

When Djokovic was serving, he hit six winners and drew only a single error from Nadal using his serve plus one. Rafael Nadal Dominates The Third Shot Of A Point |Amy Lundy |June 7, 2021 |FiveThirtyEight 

Placed in drinking water, fluoride can serve people who otherwise have poor access to dental care. Anti-Fluoriders Are The OG Anti-Vaxxers |Michael Schulson |July 27, 2016 |DAILY BEAST 

Real Housewives of New Jersey star Teresa Giudice turned herself in to serve a 15-month sentence for bankruptcy fraud. How a ‘Real Housewife’ Survives Prison: ‘I Don’t See [Teresa Giudice] Having a Cakewalk Here’ |Michael Howard |January 6, 2015 |DAILY BEAST 

Serve with the warm sauce and your choice of ice cream, whipped cream, or yogurt. Make ‘The Chew’s’ Carla Hall’s Sticky Toffee Pudding |Carla Hall |December 28, 2014 |DAILY BEAST 

Krivov was sentenced to serve four years at a general regime penal colony for his fight for freedom and human rights. Behind Bars for the Holidays: 11 Political Prisoners We Want to See Free In 2015 |Movements.Org |December 25, 2014 |DAILY BEAST 

But as we are seeing all over the world, one can serve the other. France’s Wave of Crazy-Terror Christmas Attacks |Christopher Dickey |December 24, 2014 |DAILY BEAST 

These Rules (leaving out the Tenor) serves for five bells; and leaving out the fifth and Tenor, they serve for four bells. Tintinnalogia, or, the Art of Ringing |Richard Duckworth and Fabian Stedman 

She had carried the baby over to Juana's and left her there, that she might be free to serve the Father's supper. Ramona |Helen Hunt Jackson 

He shall serve among great men, and appear before the governor. The Bible, Douay-Rheims Version |Various 

There are four general forms of emphasis which serve as indications of the characteristics of expression. Expressive Voice Culture |Jessie Eldridge Southwick 

The lower class were idle and lazy, and willing to serve any sovereign who appealed to them by ostentation. Napoleon's Marshals |R. P. Dunn-Pattison