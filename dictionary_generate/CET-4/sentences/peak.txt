While many of these novice investors did make large sums of money — at its peak, the stock hit almost 30 times its value from the start of January — many lost out as well. What Americans Think About The GameStop Investors |Dhrumil Mehta (dhrumil.mehta@fivethirtyeight.com) |February 12, 2021 |FiveThirtyEight 

Precipitation should end by mid-morning on Thursday, but it will stay cold, with temperatures peaking in the low-to-mid 30s. Snow and wintry mix continue overnight, especially north of District |Jason Samenow, Wes Junker, Andrew Freedman |February 11, 2021 |Washington Post 

The state’s number of daily vaccinations has dipped slightly this week compared with the last week of January, when it hit a peak of nearly 30,000 vaccinations in one day. Leaders in Washington region ask FEMA for help in vaccinating federal workers |Julie Zauzmer, Rachel Chason, Rebecca Tan |February 11, 2021 |Washington Post 

Hustler, whose circulation peaked above 2 million in the late 1970s, thumbed its nose at sleeker skin publications such as Playboy and Penthouse. Larry Flynt, pornographer and self-styled First Amendment champion, dies at 78 |Paul W. Valentine |February 10, 2021 |Washington Post 

After peaking above 1,000 in 2016 and 2017, thefts dipped to just 300 in 2018, but rose again to 542 in 2019. Bee theft is almost a perfect crime—but there’s a new sheriff in town |Andrew Zaleski |February 9, 2021 |Popular-Science 

That was the extent of it during the peak of the flames, and the numbers that swooshed around in the press the next day. The Fiery Death of Sotto Sotto, Toronto’s Celebrity Hotspot |Shinan Govani |December 30, 2014 |DAILY BEAST 

At its peak, his business made as much as $30,000 a year—provided he worked the entire month of December. Kerry Bentivolio: The Congressman Who Believes in Santa Claus |Ben Jacobs |December 24, 2014 |DAILY BEAST 

At his year-end, pre-Hawaii press conference, we caught a rare glimpse of peak Obama. The Liberation of the Lame Duck: Obama Goes Full Bulworth |John Avlon |December 19, 2014 |DAILY BEAST 

The series came to life just as the era of “hatewatching” was at its peak. 'The Newsroom' Ended As It Began: Weird, Controversial, and Noble |Kevin Fallon |December 15, 2014 |DAILY BEAST 

Following a peak of 153 new cases a week in August, Lofa was down to just four new cases for the week ending Nov. 1. How Liberia (Might Have) Beat Ebola |Abby Haglage |November 17, 2014 |DAILY BEAST 

Three days later he was in Switzerland, and a few days later again he was on the summit of a minor but still difficult peak. Uncanny Tales |Various 

The Hope lay safely moored, with her ensign at the peak, and flying the distinguished flag of the firm. Skipper Worse |Alexander Lange Kielland 

We passed a heap of black ashes, which anywhere but at the base of the peak would be called a respectable mountain. Journal of a Voyage to Brazil |Maria Graham 

The Peak District in Derbyshire we omitted for the same reason—a previous visit. British Highways And Byways From A Motor Car |Thomas D. Murphy 

The ice is procured from a large cavern near the cone of the peak; it is almost full of the finest ice all the year round. Journal of a Voyage to Brazil |Maria Graham