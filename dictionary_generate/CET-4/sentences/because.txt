Because he's joined the colors—he's not dead!Because he's found his duty—he's not lost! With the Colors |Everard Jack Appleton 

I will drive you over-because it is rather a lonesome walk for you. The Copy-Cat and Other Stories |Mary E. Wilkins Freeman 

He was silent; and presently she said: "I—the reason of it—my crying—is b-b-because I don't wish you to be unhappy." The Younger Set |Robert W. Chambers 

Can you picture a room where the portires are all of different lengths?because the decorator had no sense of line value? Polly in New York |Lillian Elizabeth Roy 

At length she stammered: "I did not come b-because I simply couldn't stand it!" The Fighting Chance |Robert W. Chambers