Just because the Taliban are victorious today doesn’t mean that everything achieved over the past 20 years will be lost. I Returned to Afghanistan 20 Years Ago to Help My Country. I’m Not Leaving Now |Anonymous |August 19, 2021 |Time 

Jacobs finished 19th at the 2019 world championships but emerged victorious in Tokyo on a night the AP called “one of the most unusual the sport has ever seen.” The Fastest Men In The World Are Still Chasing Usain Bolt |Josh Planos |August 3, 2021 |FiveThirtyEight 

Unlike his brother, who becomes violent and unrecognizable, Beauchard emerges victorious. How to Unlearn a Disease - Issue 103: Healthy Communication |Kelly Clancy |July 14, 2021 |Nautilus 

In the battle between big tech and regulators, Ma had emerged victorious. Here's What the Crackdown on China's Big Tech Firms Is Really About |Charlie Campbell / Shanghai |July 13, 2021 |Time 

As you expand your empire, you can enlist the help of your friends and their clans, or battle them and see who emerges victorious—adding an extra layer of interest to what is already an engaging and entertaining mobile game. Great online games to play with friends, even when you’re apart |empire |June 18, 2021 |Popular-Science 

The West is packing up, victorious in battle but defeated in war. Heart of Darkness: Into Afghanistan’s Taliban Valley |Matt Trevithick, Daniel Seckman |November 15, 2014 |DAILY BEAST 

Victorious Republican Gov. Nathan Deal boasted of his progress in reducing the number of incarcerated black men in Georgia. How’d the GOP Win? By Running Left |Sally Kohn |November 6, 2014 |DAILY BEAST 

One hopes they will be nurtured and continue to grow, whichever candidate emerges victorious next Tuesday. The Leak of a Mysterious Video Could Change the Outcome of Newark’s Mayor’s Race |Charles Upton Sahm |May 5, 2014 |DAILY BEAST 

He was strategically sophisticated, ruthless, and always emerged victorious. ‘The Search for General Tso’: The Origins of America’s Favorite Chinese Dish, General Tso’s Chicken |Marlow Stern |April 19, 2014 |DAILY BEAST 

Founded in 1542, the town was the birthplace of the victorious Mexican War of Independence against Spain. The Second Life of San Miguel de Allende |Michele Willens |February 26, 2014 |DAILY BEAST 

Then with your victorious legions you can march south and help drive the Yankee invaders from the land. The Courier of the Ozarks |Byron A. Dunn 

Anthony, titular king of Portugal, died at Paris, a fugitive from the victorious arms of the Spaniards. The Every Day Book of History and Chronology |Joel Munsell 

The promoters went his security and put up the cash into the bargain, and he went back to the publishing house victorious. The Homesteader |Oscar Micheaux 

“Unwhipped”—Jackson always came off victorious in all his duels and military campaigns. Assimilative Memory |Marcus Dwight Larrowe (AKA Prof. A. Loisette) 

He was thereafter called upon to try to stem the victorious advance of the English into Spain. Napoleon's Marshals |R. P. Dunn-Pattison