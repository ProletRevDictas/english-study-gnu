Not only did they use them, but they could go to women setting up a new place to live. Hints From Heloise: New scam tied to coronavirus pandemic |Heloise Heloise |February 12, 2021 |Washington Post 

There should be room for both views — and a time and a place to air them. The pregame national anthem — in all its roiling contradictions — still has something to offer |Barry Svrluga |February 11, 2021 |Washington Post 

The amaro is an aromatic snapshot of a place I loved, and it has a powerful pull on my memory. Aromas can evoke beloved journeys — or voyages not yet taken |Jen Rose Smith |February 11, 2021 |Washington Post 

We wanted to make sure that we had something in place that more young players that thought this was right for them, they could take advantage of it. An NBA experiment lets draft prospects skip college, stay home and get paid to play |Michael Lee |February 11, 2021 |Washington Post 

I realized they were going places when Renee revealed to me that she and Willie were texting during our post-date interview. Date Lab: One of our setups is still going strong two years later. We caught up with Willie and Renee for an update. |Marin Cogan |February 11, 2021 |Washington Post 

The world that Black Dynamite lives in is not the most PC place to be in. ‘Black Dynamite’ Presents Police Brutality: The Musical |Stereo Williams |January 9, 2015 |DAILY BEAST 

“This is the only place in the souk you can buy safety pins,” he said. The Photographer Who Gave Up Manhattan for Marrakech |Liza Foreman |January 6, 2015 |DAILY BEAST 

He hasn't bothered to visit Iguala, the place where the students were abducted and killed. Why Mexicans Are Enraged by Obama’s Big Tuesday Meeting |Ruben Navarrette Jr. |January 6, 2015 |DAILY BEAST 

A place that has multiplied success for generation after generation of its children. Mario Cuomo, Always Moving Us Toward the Light |Mike Barnicle |January 4, 2015 |DAILY BEAST 

The Great Society is a place where every child can find knowledge to enrich his mind and to enlarge his talents. Thank Congress, Not LBJ for Great Society |Julian Zelizer, Scott Porch |January 4, 2015 |DAILY BEAST 

This is the place where the Muscovite criminals are banished to, if they are not put to death. The Life and Most Surprising Adventures of Robinson Crusoe, of York, Mariner (1801) |Daniel Defoe 

He looked strangely out of place in the dusty combat uniform. Sense of Obligation |Henry Maxwell Dempsey (AKA Harry Harrison) 

The associations of place recall her strange interview with Mr. Longcluse but a few months before. Checkmate |Joseph Sheridan Le Fanu 

As Spain, however, has fallen from the high place she once held, her colonial system has also gone down. Tobacco; Its History, Varieties, Culture, Manufacture and Commerce |E. R. Billings. 

Give a sweet savour, and a memorial of fine flour, and make a fat offering, and then give place to the physician. The Bible, Douay-Rheims Version |Various