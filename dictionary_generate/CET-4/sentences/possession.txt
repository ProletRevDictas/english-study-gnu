Barcelona has the highest possession and is the highest passing team left in the Champions League. The Best Round Of The Champions League Is Here |Chadwick Matlin (chadwick.matlin@fivethirtyeight.com) |February 12, 2021 |FiveThirtyEight 

So, when a brand acquires a negative association for some consumers, as is happening with MyPillow, consumers may feel tainted by their association with this possession. MyPillow boycott: How a product can spark an identity crisis |Elizabeth Chang |February 12, 2021 |Washington Post 

Once we cross half court from the possession, we forget about it, we’re on to the next one. College basketball’s kingdoms have gone haywire — in case you just started paying attention |Chuck Culpepper |February 12, 2021 |Washington Post 

When he took possession of the mic, he shouted out the defensive line. Tampa Bay Buccaneers celebrate Super Bowl LV victory in boat parade |Cindy Boren, Glynn A. Hill |February 10, 2021 |Washington Post 

The Missouri Valley has delivered some of the country’s best mid-majors over the years, and in terms of per-possession efficiency metrics, this Loyola team can play with all of them. Remember Loyola? This Team Is Even Better Than The One That Made The Final Four. |Jake Lourim |February 10, 2021 |FiveThirtyEight 

But Brinsley went away for two years in prison for weapons possession in August 2011. Alleged Cop Killer Ismaaiyl Brinsley Had a Death Wish |M.L. Nestel |December 22, 2014 |DAILY BEAST 

“We were finding people in possession of thousands of paper prescriptions,” he said. No More Paper Prescriptions: Docs Fight Fraud by Going Electronic |Dale Eisinger |December 18, 2014 |DAILY BEAST 

He was charged with criminal possession of a weapon and faced 15 years prison time. Rapper Bobby Shmurda Arrested at New York’s Notorious Quad Studios |M.L. Nestel |December 17, 2014 |DAILY BEAST 

The interval between possession and hell was short,” he says, “though I admit it was wonderful. Owning Up to Possession’s Downside |Samantha Harvey |December 14, 2014 |DAILY BEAST 

If we begin to see the other as our possession and commodity, our shoe, the shadow of our shadow, is there ever a happy outcome? Owning Up to Possession’s Downside |Samantha Harvey |December 14, 2014 |DAILY BEAST 

Within the past thirty years civilization has rapidly taken possession of this lovely region. Among the Sioux |R. J. Creswell 

The last-named building remained in the possession of the Unitarians until 1861, when it was sold to the Roman Catholics. Showell's Dictionary of Birmingham |Thomas T. Harman and Walter Showell 

The evening previous to his death he was walking about the farm, in the full possession of all his faculties of mind and body. The Every Day Book of History and Chronology |Joel Munsell 

Providence, interposing, made him a painter, and the gaiety of nations has been increased by the possession of some storks. Punch, or the London Charivari, Volume 107, November 3, 1894 |Various 

"Marco's" reply conclusively proved his possession of a Christian spirit. Punch, or the London Charivari, Volume 107, November 3, 1894 |Various