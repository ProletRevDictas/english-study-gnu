Like many gardeners in northern states and Canadian provinces, she has learned to extend the season by growing hardy veggies under covers. Fresh vegetables in the middle of winter? It’s possible, even in colder climes. |Adrian Higgins |January 20, 2021 |Washington Post 

It comes from a legacy brand with a reliable design that gardeners have tested and approved for decades. Plant pruners to shape up your saplings |PopSci Commerce Team |January 19, 2021 |Popular-Science 

Behind the ring of planters, there is an inner “catwalk” ring that gardeners can use for inspecting and caring for the plants. Changing climates can take cooling tips from warm regions |Sharon Oosthoek |October 8, 2020 |Science News For Students 

Their potting mix is no exception—it’s also a great choice for indoor or outdoor containers gardeners, and is chock full of helpful plant nutrients to help your plant babies blossom, and stay fertilized for three months. Indoor potting mixes for a thriving houseplant jungle |PopSci Commerce Team |September 24, 2020 |Popular-Science 

Desert plants hate the dampness that gardener’s call “wet feet,” because they’re used to living on such little water. Indoor potting mixes for a thriving houseplant jungle |PopSci Commerce Team |September 24, 2020 |Popular-Science 

The Constant Gardener was the first film I worked on, and it was during my first summer holiday when I went back to Kenya. Lupita Nyong’o On Her Magical Journey from Kenya to ‘12 Years A Slave’ and Possible Oscar Glory |Marlow Stern |February 22, 2014 |DAILY BEAST 

I read your first film work was behind-the-scenes, though, working as a PA on the sets of The Constant Gardener and The Namesake. Lupita Nyong’o On Her Magical Journey from Kenya to ‘12 Years A Slave’ and Possible Oscar Glory |Marlow Stern |February 22, 2014 |DAILY BEAST 

Mavis Batey—a codebreaker crucial to the success of D-Day, author of book about Jane Austen, and expert gardener—dead at 92. Week in Death: The Woman Who Cracked Hitler’s Codes |The Telegraph |November 17, 2013 |DAILY BEAST 

As Ward points out, there is some reason to think Lewis would actually have been thrilled—the word of his gardener Paxford. Three Great Men Died That Day: JFK, C.S. Lewis, and Aldous Huxley |John Garth |November 3, 2013 |DAILY BEAST 

At a pitch I could be a tolerable road-sweeper or an inefficient gardener or even a tenth-rate farm hand. Thatcher's Economic Legacy |Megan McArdle |April 8, 2013 |DAILY BEAST 

A little boy of two years and one month was once asked to give a lot of old toys to the children of the gardener. Children's Ways |James Sully 

"It was the gardener's dog," said Nigel, letting himself down into his chair with a sigh of satisfaction. Bella Donna |Robert Hichens 

I thanked the gardener again, putting a few coins into his hand, and made my way to the address he had given me. Camille (La Dame aux Camilias) |Alexandre Dumas, fils 

Meanwhile the holy women and the gardener tarried about the bleeding corse. The Merrie Tales Of Jacques Tournebroche |Anatole France 

Robin the gardener brought up the rear, his body all shaking with his infirmity, and showing the divine stigmata on his hands. The Merrie Tales Of Jacques Tournebroche |Anatole France