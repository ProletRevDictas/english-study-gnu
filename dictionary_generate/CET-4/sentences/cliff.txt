“People at my agency are hopeful, but are also on the edge of a cliff wondering whether or not leadership is going to take that leap into action,” said a senior planner at one of the larger media agency networks. ‘It’s all been plan, plan, plan mode:’ Agencies have big ideas for greater diversity, but more action is needed |Seb Joseph |September 15, 2020 |Digiday 

In Vega Baja, January’s hurricane cleared the vegetation covering once unnavigable mountain cliffs, allowing the outfitter 21 Climb and Tour to open the Roca Norte Outdoor Climbing Gym. 27 Epic Trips to Start Planning Now |The Editors |September 8, 2020 |Outside Online 

Amid the worst collapse in fossil fuel demand in a century, production has fallen off a cliff. The American West’s economy has a lot in common with Saudi Arabia |Michael J. Coren |August 1, 2020 |Quartz 

These cycles have operated throughout time, yielding the alternating layers of sediment you see in cliffs and road cuts. How Earth’s Climate Changes Naturally (and Why Things Are Different Now) |Howard Lee |July 21, 2020 |Quanta Magazine 

Despite winning four Gold Glove awards, his defensive metrics were surprisingly poor — thus hurting his JAWS ranking — and his performance fell off a cliff quickly as he got older. Bernie Williams Deserves More Credit For Making The Yankees A Dynasty |Neil Paine (neil.paine@fivethirtyeight.com) |July 14, 2020 |FiveThirtyEight 

This is where I think the argument against me falls off the cliff. Seriously, Democrats: You’re Done in Dixie |Michael Tomasky |December 10, 2014 |DAILY BEAST 

Would I like to tell half the people I work with to go jump off a cliff? The Hot Designer Who Hates Fashion: VK Nagrani Triumphs His Own Way |Tom Teodorczuk |December 1, 2014 |DAILY BEAST 

We first met Hajji Zalwar Khan over tea and lunch in the Pech Valley in a house clinging to a cliff high above the valley floor. Heart of Darkness: Into Afghanistan’s Taliban Valley |Matt Trevithick, Daniel Seckman |November 15, 2014 |DAILY BEAST 

The probe appears to be sitting at the bottom of a "cliff" on the comet, but beyond that it's hard to tell. Earthlings, We Landed on a Comet |Matthew R. Francis |November 12, 2014 |DAILY BEAST 

The young goslings' first major life event is to cliff dive down to their parents, as was captured here by BBC cameras. Barnacle Gosling’s Death-Defying Cliff Dive |Alex Chancey, The Daily Beast Video |October 28, 2014 |DAILY BEAST 

Only the petrol tins they took for water right and left of their pathway up the cliff; huge diamonds in the evening sun. Gallipoli Diary, Volume I |Ian Hamilton 

At dawn half a battalion of Turks tried to make the attack along the top of the cliff and were entirely wiped out. Gallipoli Diary, Volume I |Ian Hamilton 

One morning Straightshaft climbed the cliff and looked far up and down the valley. The Later Cave-Men |Katharine Elizabeth Dopp 

Then everybody climbed up a hill or a high cliff and watched the coming of the bison. The Later Cave-Men |Katharine Elizabeth Dopp 

There was Cliff Manning, you turned the cold shoulder to him because he couldnt talk grammar. Tessa Wadsworth's Discipline |Jennie M. Drinkwater