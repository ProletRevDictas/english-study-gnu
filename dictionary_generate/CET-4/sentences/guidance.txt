“The USMS’s detention facility oversight plan is inconsistent and does not ensure that all active facilities” comply with the latest Centers for Disease Control and Prevention guidance, says the watchdog’s report. U.S. Marshals Service’s lax covid-19 oversight of some inmates reflects a larger problem |Joe Davidson |February 12, 2021 |Washington Post 

Because of the pandemic, the IRS has issued guidance that gave 529-plan account holders a longer window to return refunded college expenses. Tax season 2021: A tornado is coming |Michelle Singletary |February 12, 2021 |Washington Post 

A guidance on hearing risks places danger at any level above 80 decibels and the loudest concerts as hitting roughly 120 decibels. Using whale songs to image beneath the ocean’s floor |John Timmer |February 11, 2021 |Ars Technica 

The guidance comes at a potentially perilous moment, as health officials race to vaccinate tens of millions of people to protect them from more transmissible and possibly more lethal variants but remain hampered by a limited vaccine supply. Masks should fit better or be doubled up to protect against coronavirus variants, CDC says |Lena H. Sun, Fenit Nirappil |February 11, 2021 |Washington Post 

Seeking guidance or good fortune, Dido would quickly mold one from the clay next to the field where she harvested wheat. What archaeologists got wrong about female statues, goddesses, and fertility |Annalee Newitz |February 10, 2021 |Popular-Science 

Jettison your lawyers as a source of prison-yard guidance, Abramoff said. Abramoff’s Advice for Virginia’s New Jailhouse Guv |Tim Mak, Jackie Kucinich |January 7, 2015 |DAILY BEAST 

“Very few agencies offer police any specific guidance or training on how to question people with ID,” said Garrett. How the U.S. Justice System Screws Prisoners with Disabilities |Elizabeth Picciuto |December 16, 2014 |DAILY BEAST 

“Some tribes” have “requested guidance on the enforcement of the Controlled Substances Act (CSA),” the opening reads. Tribes to U.S. Government: Take Your Weed and Shove It |Abby Haglage |December 13, 2014 |DAILY BEAST 

According to Wahlberg, his time in prison, as well as the guidance of a parish priest, helped him turn his life around. Mark Wahlberg’s Pardon Plea: A Look Back At His Troubling, Violent, and Racist Rap Sheet |Marlow Stern |December 7, 2014 |DAILY BEAST 

Mariame protests, and tells the guidance counselor that she does not understand. ‘Girlhood’: Coming of Age in France’s Projects |Molly Hannon |November 25, 2014 |DAILY BEAST 

And here let me point out for your future guidance the importance of having a private secretary thoroughly up to his work. Punch, or the London Charivari, Volume 107, November 3, 1894 |Various 

In a strict sense, of course, no child's drawing is absolutely spontaneous and independent of external stimulus and guidance. Children's Ways |James Sully 

For thousands of years—perhaps for millions of years—the generations of men prayed to God for help, for comfort, for guidance. God and my Neighbour |Robert Blatchford 

And efforts should be made, and supplications offered, to obtain guidance on this point into all truth. The Ordinance of Covenanting |John Cunningham 

Chumru and the ryot bestrode the third horse, and under the guidance of one who knew every path, they set out for the Ganges. The Red Year |Louis Tracy