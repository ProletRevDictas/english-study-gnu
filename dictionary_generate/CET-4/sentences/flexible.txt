To help the tech player navigate and manage its growth not only as a somewhat-newly-public company but also as an increasingly distributed one—even after the pandemic passes, Slack expects to have a much more flexible workforce. Slack hires former Live Nation exec as new chief people officer |Michal Lev-Ram, writer |September 16, 2020 |Fortune 

Successful re-opening, therefore, will require that schools be flexible and make some changes. Here’s how COVID-19 is changing classes this year |Bethany Brookshire |September 8, 2020 |Science News For Students 

However, over the last six months, the movement to reshape advertising to actually be agile, nimble and flexible has no longer been an abstract idea but rather a necessity. ‘We have the capability’: How the coronavirus crisis has accelerated advertising’s shift to agility |Kristina Monllos |September 7, 2020 |Digiday 

These perovskite layers are also being developed to manufacture flexible solar panels that can be processed to roll like newsprint, further reducing costs. How a New Solar and Lighting Technology Could Propel a Renewable Energy Transformation |Sam Stranks |September 3, 2020 |Singularity Hub 

Over time, mathematicians have developed an increasingly flexible view of what it means for two objects to be “the same.” Conducting the Mathematical Orchestra From the Middle |Rachel Crowell |September 2, 2020 |Quanta Magazine 

And lo, Snowballs—underpants which can hold a flexible gel pack that you store in the freezer—was born. Men, Ice Your Balls To Make Babies—and Other Male Fertility Fixes |Tom Sykes |December 22, 2014 |DAILY BEAST 

Of course, a more flexible interpretation is just as accurate. Justice Ginsburg Shouldn’t Quit Just Yet |Kevin Bleyer |December 1, 2014 |DAILY BEAST 

Have a plan but be flexible and adjust to emerging realities. Tony La Russa Explains How To Make It To The World Series |Dave Pottruck |October 4, 2014 |DAILY BEAST 

The US should be more flexible and honest regarding its policy of never paying ransom for hostages. One Former Hostage Says Negotiate With ISIS, And Pay Ransoms If You Must |Sarah Shourd |September 6, 2014 |DAILY BEAST 

For example, to build flexible career and promotional tracks which do not conflict with biology. Whither the Women’s Movement? |Judith Barnett |July 19, 2014 |DAILY BEAST 

All the ancient civilized peoples used ropes and cordage, made from such flexible materials as their countries afforded. The Wonder Book of Knowledge |Various 

She could admire their fine flexible play under the water; do what she would with them her hands at least were feminine. The Creators |May Sinclair 

This consists of a tarred rope, or a flexible whip-stalk, three-fourths of an inch in diameter, with a swab or bulbous end. Domestic Animals |Richard L. Allen 

The dry leaflets may be made flexible for this purpose by laying them on the grass in the night air. Philippine Mats |Hugo H. Miller 

Jimmy Hall, the sealer, laid his flexible rule over the face of each log. Blazed Trail Stories |Stewart Edward White