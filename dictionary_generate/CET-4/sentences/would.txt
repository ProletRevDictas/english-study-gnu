But on Thursday Boxer triggered a Golden State political earthquake, announcing that she would not seek a fifth term in 2016. The Golden State Preps for the ‘Red Wedding’ of Senate Races |David Freedlander |January 9, 2015 |DAILY BEAST 

To those who agreed with him, Bush pledged that the law against same-sex marriage would remain intact. Jeb Bush’s Unseen Anti-Gay Marriage Emails |Jackie Kucinich |January 9, 2015 |DAILY BEAST 

It would became one of the first great mysteries in the United States of America, as it was only then 23 years old. New York’s Most Tragic Ghost Loves Minimalist Swedish Fashion |Nina Strochlic |January 8, 2015 |DAILY BEAST 

Without it, they say, the disease would surely kill her within two years. Should Teens Have The Right To Die? |Brandy Zadrozny |January 8, 2015 |DAILY BEAST 

Almost all of the network and cable news channels said that they would not be showing the cartoons either. Politicians Only Love Journalists When They're Dead |Luke O’Neil |January 8, 2015 |DAILY BEAST 

"Capital, capital," his lordship would remark with great alacrity, when there was no other way of escape. The Pit Town Coronet, Volume I (of 3) |Charles James Wills 

Bessires was included because he would never win it at any later date, but his doglike devotion made him a priceless subordinate. Napoleon's Marshals |R. P. Dunn-Pattison 

You would not think it too much to set the whole province in flames so that you could have your way with this wretched child. St. Martin's Summer |Rafael Sabatini 

He was voluble in his declarations that they would “put the screws” to Ollie on the charge of perjury. The Bondboy |George W. (George Washington) Ogden 

And she would be wearing some of the jewels with the white dress—just a few, not many, of course. Rosemary in Search of a Father |C. N. Williamson