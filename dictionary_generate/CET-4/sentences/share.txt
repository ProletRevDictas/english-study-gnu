By 1984, Heinz swooped in to scoop up Chico-San’s market share. The Rise and Fall of the Rice Cake, America’s One-Time Favorite Health Snack |Brenna Houck |September 17, 2020 |Eater 

At the time, Mozilla described it as a file-sharing tool with a focus on privacy. Mozilla shutters Firefox Send and Notes |Frederic Lardinois |September 17, 2020 |TechCrunch 

Now, Amazon will allow users to share their favorite routines with others. Amazon makes Alexa Routines shareable |Sarah Perez |September 17, 2020 |TechCrunch 

Trina reported that her younger sister was doing “better” and would share more details about what she experienced when she was ready. Trina Braxton To David Adefeso: ‘When You Attack One, You Attack Us All’ |Hope Wright |September 17, 2020 |Essence.com 

Little wonder Square’s share price has climbed 150% this year. ‘Square is a beast’ |Jeff |September 16, 2020 |Fortune 

Cosby conspiracy theorists share a perspective born of a long, pained history of American racism. Phylicia Rashad and the Cult of Cosby Truthers |Stereo Williams |January 8, 2015 |DAILY BEAST 

We have to share those feelings of concern that the people are feeling. Mexico’s Priests Are Marked for Murder |Jason McGahan |January 7, 2015 |DAILY BEAST 

Mr. Bachner said it had been hard to introduce his work ethic and share his vision with the locals and his team. The Photographer Who Gave Up Manhattan for Marrakech |Liza Foreman |January 6, 2015 |DAILY BEAST 

Getting men to do their share of care and domestic work is a key overlooked strategy in reducing poverty. How Good Dads Can Change the World |Gary Barker, PhD, Michael Kaufman |January 6, 2015 |DAILY BEAST 

Perhaps it always seems that way at the time, but surely we face our fair share right now. In 2015, Let’s Try for More Compassion |Gene Robinson |January 4, 2015 |DAILY BEAST 

The foster-child remained behind to share the hut of the political exile. The Philippine Islands |John Foreman 

Rent, the share of the land-owner, offered to the classicist a rather peculiar case. The Unsolved Riddle of Social Justice |Stephen Leacock 

Murat, who had earnestly begged to be allowed to share the Austrian campaign of 1809, was delighted to serve in person. Napoleon's Marshals |R. P. Dunn-Pattison 

It is almost unnecessary to add, that the porter had his share well paid, and that the fisherman got the full value for his prize. The Book of Anecdotes and Budget of Fun; |Various 

But Yung Pak was not allowed to share the pleasures and the trials of the boys in the public school. Our Little Korean Cousin |H. Lee M. Pike