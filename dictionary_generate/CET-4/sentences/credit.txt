Issues around one wage, getting rid of the tip credit and paying waitstaff not sub-minimum-wage anymore, but with that comes tip sharing. Tom Colicchio Hopes (and Fears) COVID-19 Will Change the Restaurant Industry |Pallabi Munsi |September 16, 2020 |Ozy 

The first company Team8 Fintech is building will provide an engine to evaluate credit risk of small- and medium-sized enterprises in e-commerce. She was one of the world’s few female bank CEOs. Now she’s founding a fintech venture group |Claire Zillman, reporter |September 15, 2020 |Fortune 

Paycheck Protection Program funds are gone, and for most businesses, revenue hasn’t nearly recovered — but they have neither access to unlimited credit nor the means to pay it back. The Big Corporate Rescue and the America That’s Too Small to Save |by Lydia DePillis, Justin Elliott and Paul Kiel |September 12, 2020 |ProPublica 

I started my credit card processing company, Gravity Payments, 16 years ago to support these small businesses. How we can save small business from coronavirus-induced extinction |matthewheimer |September 10, 2020 |Fortune 

They often cite the trillions in fiscal spending and super-loose monetary policy that have deluged the economy with cheap credit. Global markets dip as investors again sour on tech stocks |Bernhard Warner |September 10, 2020 |Fortune 

But give the Kingdom credit for its sense of mercy: The lashes will be administered only 50 at a time. In Defense of Blasphemy |Michael Tomasky |January 9, 2015 |DAILY BEAST 

To his credit, Huckabee is conscious of the fact that he will need a cluster of deep-pocketed patrons and bundlers. Can Huckabee Convert the GOP’s Moneymen? |Lloyd Green |January 4, 2015 |DAILY BEAST 

To be sure, Jefferson did share the credit, but not in the way such a resolution might be interpreted. Forget the Resolutions; Try a Few Declarations |Kevin Bleyer |January 1, 2015 |DAILY BEAST 

That could include private financial or personal information—like the credit-card numbers you used to pay for the corrupted Wi-Fi. How ‘Ethical’ Hotel Chain Marriott Gouges Guests in the Name of Wi-Fi Security |Kyle Chayka |December 31, 2014 |DAILY BEAST 

And much of the credit to her transformation is owed to a finishing school that caters to women just like her. Inside A Finishing School for Transwomen |Sharon Adarlo |December 27, 2014 |DAILY BEAST 

After all, here was a babe equipped to face the exigencies of a censorious world; in looks and apparel a credit to any father. The Joyous Adventures of Aristide Pujol |William J. Locke 

The result of the restoration of trade, banking, and credit to earlier and more normal conditions has been steadily apparent. Readings in Money and Banking |Chester Arthur Phillips 

He went to a bank in the little town where he had other friends from whom he had never asked credit. The Homesteader |Oscar Micheaux 

I must make no mistake, and blunder into a national type of features, all wrong; if I make your mask, it must do us credit. Checkmate |Joseph Sheridan Le Fanu 

The so-called war credit banks are designed to serve this purpose. Readings in Money and Banking |Chester Arthur Phillips