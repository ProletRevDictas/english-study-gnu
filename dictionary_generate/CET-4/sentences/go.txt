Everywhere I go, ‘Hey Cartman, you must like Family Guy, right?’ Trolls and Martyrdom: Je Ne Suis Pas Charlie |Arthur Chu |January 9, 2015 |DAILY BEAST 

Luckily enough I have this dedicated flat that is just along from my house that I go to every day. Belle & Sebastian Aren’t So Shy Anymore |James Joiner |January 7, 2015 |DAILY BEAST 

The other songs go in to lesser percentages of “me” as you move along. Belle & Sebastian Aren’t So Shy Anymore |James Joiner |January 7, 2015 |DAILY BEAST 

At the moment, the only chance I get is when I go do Late Night with Seth Meyers. Coffee Talk with Fred Armisen: On ‘Portlandia,’ Meeting Obama, and Taylor Swift’s Greatness |Marlow Stern |January 7, 2015 |DAILY BEAST 

You just travel light with carry-on luggage, go to cities that you love, and get to hang out with all your friends. Coffee Talk with Fred Armisen: On ‘Portlandia,’ Meeting Obama, and Taylor Swift’s Greatness |Marlow Stern |January 7, 2015 |DAILY BEAST 

When the women came, he was preparing to go to the west side for his daily visit with Mrs. Pruitt. The Homesteader |Oscar Micheaux 

Were you ever arrested, having in your custody another man's cash, and would rather go to gaol, than break it? The Life and Most Surprising Adventures of Robinson Crusoe, of York, Mariner (1801) |Daniel Defoe 

He desired his secretary to go to the devil, but, thinking better of it, he recalled him as he reached the door. St. Martin's Summer |Rafael Sabatini 

All Weimar adores him, and people say that women still go perfectly crazy over him. Music-Study in Germany |Amy Fay 

To see a part of my scheme, from which I had hoped so much, go wrong before my eyes is maddening! Gallipoli Diary, Volume I |Ian Hamilton