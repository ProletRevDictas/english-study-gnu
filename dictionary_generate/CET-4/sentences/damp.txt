We entered the fruiting rooms, which were warm and damp, the air thick and cloying. The Fungal Evangelist Who Would Save the Bees - Issue 90: Something Green |Merlin Sheldrake |September 23, 2020 |Nautilus 

There was no one there to help her, put a damp cloth on her forehead or hold her hand to convey confidence. Cuban doctor contracts coronavirus in ICE custody |Yariel Valdés González |September 9, 2020 |Washington Blade 

A sleeved, moisture-wicking dress is the easiest thing to toss on over a damp suit. In Praise of the Adventure Dress |Alison Van Houten |August 22, 2020 |Outside Online 

If you’re still anxious about potential toxins in the adhesive, or simply don’t like the taste, moisten the glue strip with a damp sponge instead of your tongue. Science of ‘Seinfeld’ |LGBTQ-Editor |June 15, 2020 |No Straight News 

They had to wriggle through dark, damp and cramped spaces every time they visited. The challenge of dinosaur hunting in deep caves |John Pickrell |May 19, 2020 |Science News For Students 

Place the stack of phyllo dough sheets on a cutting board and cover it with a slightly damp towel. The Barefoot Contessa’s Tasty Trip to Paris |Ina Garten |November 27, 2014 |DAILY BEAST 

Instead, I stayed in a bed-and-breakfast kind of place near the sea, so near that everything in it felt damp all the time. My Time on the Set of 'Jaws,' or How to Get a Photo of a Frickin' Mechanical Shark |Tom Shales |August 17, 2014 |DAILY BEAST 

With me tagging along, they dove into a rudimentary, damp shelter they had dug in a wood nearby. Shakeup In the Ukraine Rebel High Command |Jamie Dettmer |August 15, 2014 |DAILY BEAST 

That same day, despite tissues still damp from the aftermath of the botched audition, my life changed in a matter of two hours. Uzo Aduba: My Road to ‘Orange Is the New Black’ |Uzoamaka Aduba |August 4, 2014 |DAILY BEAST 

It insists on efficiency standards for household appliances so that your towels come out of the dryer refreshingly cool and damp. The Federal Government Has Violated My Right to Chainsaw |P. J. O’Rourke |April 27, 2014 |DAILY BEAST 

The tears came so fast to Mrs. Pontellier's eyes that the damp sleeve of her peignoir no longer served to dry them. The Awakening and Selected Short Stories |Kate Chopin 

As for him, he much preferred the darkness of his cool, damp galleries under the ground. The Tale of Grandfather Mole |Arthur Scott Bailey 

First of all, wrap a portion of damp newspaper round the roots, and then tie up with dry paper. How to Know the Ferns |S. Leonard Bastin 

Dense fogs always prevail, and generally make the country very damp. The Philippine Islands, 1493-1898, Volume XX, 1621-1624 |Various 

A damp mist rose from the river and the marshy ground about, and spread itself over the dreary fields. Oliver Twist, Vol. II (of 3) |Charles Dickens