One of the big benefits that is touted for RSAs is time savings. RSAs: Are they living up to the promise? It depends |Ginny Marvin |September 9, 2020 |Search Engine Land 

We feel like 20% is the bare minimum you need to have savings, to not struggle, and to make ends meet at the end of the month…You can’t have passionate employees if every month they are worried about how they are going to make ends meet. Why PayPal’s Dan Schulman gave workers pay increases, without the market requiring it |Alan Murray |September 9, 2020 |Fortune 

Of course, cost savings also make the numbers look good, sources say. ‘Too big to ignore’: Future estimates profits of nearly $110 million this year |Lucinda Southern |September 8, 2020 |Digiday 

Currently, investors can make returns on cash starting at 3 percent—a relatively high figure at a time when even “high yield” savings accounts are paying under 1 percent. Crypto soars again as traders embrace ‘DeFi’ and ‘yield farming’—but some see echoes of the 2017 bubble |Jeff |August 25, 2020 |Fortune 

In these reviews, cost savings are still a crucial factor in those decisions, according to an exec who has managed some of these reviews. Marketers ‘looking to feel confident’: the coronavirus causing irreversible changes to the advertiser-agency dynamic |Seb Joseph |August 20, 2020 |Digiday 

Just another example of a guy in a John B. Stetson hat saving the day. My Love Letter to the Stetson |Mark McKinnon |December 24, 2014 |DAILY BEAST 

Can we provide better services to millions more Americans while actually saving billions of dollars? Can the U.S. Government Go Moneyball? |Peter Orszag, Jim Nussle |December 23, 2014 |DAILY BEAST 

Now, the goalkeeper is out with a memoir about his life until that point: The Keeper: A Life of Saving Goals and Achieving Them. Tim Howard’s Wall of Intensity |William O’Connor |December 22, 2014 |DAILY BEAST 

But it remains a moral crime to vilify good cops who have made the city safe, saving thousands of lives. Protesters Slimed This Good Samaritan Cop |Michael Daly |December 16, 2014 |DAILY BEAST 

We thanked them on stage for saving our asses and supporting indie music. How Much Money Does a Band Really Make on Tour? |Jack Conte |December 8, 2014 |DAILY BEAST 

The carrying of these heavy government debts is a question of the future production of goods, of commerce, and of saving. Readings in Money and Banking |Chester Arthur Phillips 

Steam machinery would accomplish more than nine-tenths of all the work, besides saving the expense of all the powder. Life of Richard Trevithick, Volume II (of 2) |Francis Trevithick 

I think it is better to make them ourselves, for if we do not, some others will, for there must be a saving of coal by condensing. Life of Richard Trevithick, Volume II (of 2) |Francis Trevithick 

Tom pitched forward heavily, saving himself and his animal from an ignominious accident just in the nick of time. The Wave |Algernon Blackwood 

So I put on my life-saving waistcoat and blew it out; clapped my new gas-mask on my head and entered. Gallipoli Diary, Volume I |Ian Hamilton