The publication reported that the sum included a €22 million fine, although Facebook has not confirmed this. Facebook coughs up $125 million to settle French tax dispute |David Meyer |August 24, 2020 |Fortune 

During those lean months, publications had to get creative to bring money in the door. ”Pivot” has been the word’: How travel publishers are navigating the coronavirus pandemic |Max Willens |August 20, 2020 |Digiday 

Participating publications reportedly include News Corp’s Wall Street Journal, Dow Jones and New York Post, BuzzFeed News and Business Insider, according to The Wall Street journal. Explained: Google’s tussle in Australia over paying publishers for news |Lara O'Reilly |August 19, 2020 |Digiday 

For example, Huge’s digital publication, Magenta, uses the platform. ‘Let’s put it out in the world’: Why Code and Theory is creating its own thought leadership publication, Decode |Kristina Monllos |August 6, 2020 |Digiday 

Since then, I have been writing regularly for various publications on subjects ranging from elections to features on travel and cinema. The Covid-19 survival kit for India’s freelance writers: online workshops, writing books, and more social media |Basav Biradar |August 3, 2020 |Quartz 

I do not believe we have a current count of fugitives for publication, but will inquire. Cuba Protects America’s Most Wanted |Michael Daly |December 18, 2014 |DAILY BEAST 

The CIA Publication Review Board has required that the following statement be included with this commentary. CIA Agents Assess: How Real Is ‘Homeland’? |Chuck Cogan, John MacGaffin |December 15, 2014 |DAILY BEAST 

As of Friday, just how the final publication would play out remained a mystery, like so many Christmas presents under the tree. CIA Won’t Defend Its One-Time Torturers |Shane Harris, Tim Mak |December 6, 2014 |DAILY BEAST 

The publication said the plot was first uncovered by American intelligence. U.S. Clams Up on Xmas Airline Bomb ‘Plot’ |Shane Harris |December 2, 2014 |DAILY BEAST 

The alleged plan was first reported in the British publication Express, citing anonymous security sources. U.S. Clams Up on Xmas Airline Bomb ‘Plot’ |Shane Harris |December 2, 2014 |DAILY BEAST 

My mother had this rectified in a later publication of the paper, but that, of course, I never saw. The Boarded-Up House |Augusta Huiell Seaman 

I wish I could write up Deppe's system for publication, but it is a very difficult thing to give any adequate idea of. Music-Study in Germany |Amy Fay 

A local publication gives full details of those who have looked—and perished. Uncanny Tales |Various 

It is somewhat curious that, previous to the publication of Christabel, there appeared a conclusion to that splendid fragment. Notes and Queries, Number 177, March 19, 1853 |Various 

Many southern states passed resolutions requesting the northern states to forbid the publication of abolitionist papers. Hallowed Heritage: The Life of Virginia |Dorothy M. Torpey