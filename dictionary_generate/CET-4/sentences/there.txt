Is there/will there be plans to have Jamaica compete in the Winter Olympics again? Jamaican Bobsledder Tal Stokes Takes to Reddit and Reveals ‘Cool Runnings’ Was Mostly Made Up | |October 17, 2013 |DAILY BEAST 

There are just as many covetable skirts and delicate silk tap shorts as there are thorn-cupped bras and barely-there g-strings. We Want: Fleur Du Mal |Misty White Sidell |December 4, 2012 |DAILY BEAST 

She tends to go toward the Lady Gaga deconstruction, Alexander McQueen type of just out-there kind of costume. RuPaul Picks Favorite All-Star Drag Race Looks |Maria Elena Fernandez |October 19, 2012 |DAILY BEAST 

Last Friday morning, reams of documents filled the room-there were more than 800 binders of them packed against one wall alone. Oligarch v. Oligarch: London's Courts Attract Litigious Tycoons |Mike Giglio |July 23, 2012 |DAILY BEAST 

The there-and-back-again mission was a crucial steppingstone for the company on its path to sending a manned craft into the ether. 11 Cool Facts About SpaceX: Dragon, Microsoft, Investments & More |Matthew DeLuca |May 23, 2012 |DAILY BEAST 

The door banged shut behind him and I heard him at the foot of the stairs roaring "Ho-ho-there-ho!" The Soldier of the Valley |Nelson Lloyd 

We are introduced successively to the Palestinian, the Assimilator, and the Neither-here-nor-there. The History of Yiddish Literature in the Nineteenth Century |Leo Wiener 

Emendations on the here-a-little-there-a-little plan, while they do no harm, do little good. The Atlantic Monthly, Volume 14, No. 85, November, 1864 |Various 

Swinburne dismisses him in two lines: Maximilian is a good-natured, neither here-nor-there kind of youth. The Life of Ludwig van Beethoven, Volume I (of 3) |Alexander Wheelock Thayer 

There-about many small coffee growers are settled, and several hundred thousand bags of the beans pass through annually. All About Coffee |William H. Ukers