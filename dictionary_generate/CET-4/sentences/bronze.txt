He laid flowers at the feet of the six-meter bronze statue of Deng that stands in Shenzhen’s Lianhuashan Park. Xi Jinping’s new economic strategy for China: ‘Dual circulation’ or doublespeak? |claychandler |October 15, 2020 |Fortune 

The large bronze wire metal storage baskets offer ample space to organize cans, fruit, vegetables, scarves, socks, office supplies, toys and all your essentials. The best bins and baskets to keep your pantry perfectly organized |PopSci Commerce Team |October 13, 2020 |Popular-Science 

An athlete representing the United States of America in the 2018 Winter Olympics, for instance, would have received $37,500 for a gold medal, $22,500 for silver, and $15,000 for bronze. Inside a secret running program at Nike and a win-at-all-costs corporate culture |Rachel King |October 6, 2020 |Fortune 

Singaporean athletes are paid the most for ascending the Olympic podium, receiving $1 million for a gold medal, $500,000 for silver, and $250,000 for bronze. Inside a secret running program at Nike and a win-at-all-costs corporate culture |Rachel King |October 6, 2020 |Fortune 

Made of heavy-duty, bronze-plated steel and able to hold 6 pairs of shoes on each of four racks, this conversation piece will allow you quick access to your favorite footwear. The top shoe organizers for a tidier hallway or closet |PopSci Commerce Team |September 24, 2020 |Popular-Science 

There was deep brown flesh, and bronze flesh, and pallid white flesh, and flesh turned red from the hot sun. Powerful Congressman Writes About ‘Fleshy Breasts’ |Asawin Suebsaeng |January 7, 2015 |DAILY BEAST 

The quote appears on the bronze plaque the players touch before they take the field for home games. A West Point MVP Who Never Played a Down |Nicolaus Mills |December 13, 2014 |DAILY BEAST 

A platinum plan pays 90 percent of costs; gold plans pay 80 percent; silver plans pay 70 percent; bronze pay 60 percent. Think You’re Invincible? Here’s Why Open Enrollment Matters |DailyBurn |November 16, 2014 |DAILY BEAST 

Once dried, a liquid, such as plaster, wax, or bronze, is poured in for a perfect representation of the face. The Ukrainian Face Collector Launches an Exhibition in Kiev |Nina Strochlic |August 21, 2014 |DAILY BEAST 

Beginning in 2005, Marf Zamua began to document Late Bronze Age and Iron Age sites that were revealed during a period of unrest. Iraq’s Long-Lost Mythical Temple Has Been Found…and Is In Danger of Disappearing Again |Nina Strochlic |July 24, 2014 |DAILY BEAST 

Among these are "Medusa," a bronze bust; and a "Mater Dolorosa," in terra-cotta. Women in the fine arts, from the Seventh Century B.C. to the Twentieth Century A.D. |Clara Erskine Clement 

A lurid spot on each cheek showed burning red through the bronze of his skin. Ramona |Helen Hunt Jackson 

And as bronze reflects the light, her mentality seemed to reflect all the cold lights in her nature. Bella Donna |Robert Hichens 

The little faces shone like polished bronze; they held their hands out, their bare feet pattered in the sand. The Wave |Algernon Blackwood 

Alas, its colour was other than the deep chestnut bronze of Georgie Haggard's. The Pit Town Coronet, Volume I (of 3) |Charles James Wills