And, by warming the oceans, climate change is also setting the stage for supercharged storms, scientists say. What’s behind August 2020’s extreme weather? Climate change and bad luck |Carolyn Gramling |August 27, 2020 |Science News 

So, while local materials may have delivered the bulk of Earth’s water, the oceans were likely topped off a bit later by collisions with remote space rocks. Earth’s building blocks may have had far more water than previously thought |Christopher Crockett |August 27, 2020 |Science News 

In particular, models have a hard time reproducing what happens to an MJO when it hits Southeast Asia’s mix of islands and ocean known as the Maritime Continent. Improved three-week weather forecasts could save lives from disaster |Alexandra Witze |August 27, 2020 |Science News 

Your house in the redwoods, by the creek and ocean, lasted nearly 19 years. We Lost Our Home to a Wildfire |Wallace J. Nichols |August 25, 2020 |Outside Online 

The regular eruption of volcanoes along the rift and new insights into the break up of continents adds to the belief that the continent may be splitting to form a new ocean. Scientists say a new ocean will form in Africa as the continent continues to split into two |Uwagbale Edward-Ekpu |August 13, 2020 |Quartz 

These brave souls took an icy dip in the ocean to ring in 2015 and raise money for charity. Diving Into 2015 With Polar Bear Plunge Extremists |James Joiner |January 1, 2015 |DAILY BEAST 

Miles of Soviet era housing projects sat along on the ocean. Cuban Hip-Hop Was Born in Alamar |Daniel Levin |December 26, 2014 |DAILY BEAST 

Fidel jumped out and hopped into the ocean without getting wet. The Life and Hard Times Of The Family A Cuban Defector Left Behind |Brin-Jonathan Butler |December 19, 2014 |DAILY BEAST 

Opposite is a red-brick monastery leaning like an ocean liner in the snow. Russia’s Gold Digger Academy |Peter Pomerantsev |November 11, 2014 |DAILY BEAST 

The real story of who killed bin Laden may have gone to the bottom of the ocean or been plowed back into the dirt in Abbottabad. Bin Laden ‘Shooter’ Story Is FUBAR, Special Ops Sources Say |Shane Harris |November 7, 2014 |DAILY BEAST 

They are so rich in harmony, so weird, so wild, that when you hear them you are like a sea-weed cast upon the bosom of the ocean. Music-Study in Germany |Amy Fay 

His soul was tossed on the billows of a tempestuous ocean, in the midst of which he saw his father perishing. The Pastor's Fire-side Vol. 3 of 4 |Jane Porter 

Monsieur Farival thought that Victor should have been taken out in mid-ocean in his earliest youth and drowned. The Awakening and Selected Short Stories |Kate Chopin 

But this port (to obviate misunderstanding) is not on the Ocean lying eastward, but on that gulf which I have called French bay. The Jesuit Relations and Allied Documents, Vol. II: Acadia, 1612-1614 |Various 

The common law is therefore always slowly changing like the ocean and is never at rest. Putnam's Handy Law Book for the Layman |Albert Sidney Bolles