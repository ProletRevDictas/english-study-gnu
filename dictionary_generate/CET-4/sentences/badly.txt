“If you ticket individuals with other sorts of mechanisms, then you could start to get it from the supply side and ensure if businesses are behaving badly it falls on everyone who participates,” he said. Despite Crackdown Announcement, Not Much COVID-19 Enforcement Is Happening |Jesse Marx |February 11, 2021 |Voice of San Diego 

The Chiefs hurt themselves badly with mistakes, including a key drop from tight end Travis Kelce on a third down. Super Bowl highlights: Bucs celebrate championship, Tom Brady wins MVP |Des Bieler, Mark Maske, Chuck Culpepper |February 8, 2021 |Washington Post 

All his life doing whatever he could to serve others gladly, to make their lives go less badly. Frank Anderson, 87, cared passionately about feeding the homeless in D.C. |Dana Hedgpeth |February 5, 2021 |Washington Post 

Meanwhile, Doncic hasn’t quite lived up to sky-high expectations, and his sidekick, Kristaps Porzingis, has struggled badly since he returned from October knee surgery. How Luka Doncic’s Mavericks lost their joyful swagger and how they can get it back |Ben Golliver |February 3, 2021 |Washington Post 

Fucso has gone back to the gym she missed badly and uses the treadmill and weights. Many who have received the coronavirus vaccine wonder: What can I safely do? |Laurie McGinley, Lenny Bernstein |February 1, 2021 |Washington Post 

The rage that Marvin has embodied, a man on the edge of eruption, is always a badly wounded man. The Story Behind Lee Marvin’s Liberty Valance Smile |Robert Ward |January 3, 2015 |DAILY BEAST 

All of us can readily conjure up horror scenarios by the isolated person acting badly. Red Tape Is Strangling Good Samaritans |Philip K. Howard |December 27, 2014 |DAILY BEAST 

He then saw two badly wounded uniformed officers in the front of a radio car. 'Please Don't Die!': The Frantic Battle to Save Murdered Cops |Michael Daly |December 22, 2014 |DAILY BEAST 

Kurnosova badly wants the change to take place democratically. Russians Plot Exiled Government in Kiev |Anna Nemtsova |December 16, 2014 |DAILY BEAST 

On her first entrance, Hitchcock says, “She looks old, they've shot her badly.” Alfred Hitchcock’s Fade to Black: The Great Director’s Final Days |David Freeman |December 13, 2014 |DAILY BEAST 

Still, gambling seemed to be made particularly fascinating here, and he wanted to be fascinated, wanted it badly. Rosemary in Search of a Father |C. N. Williamson 

The evening was cold and raw and so dark that it was almost impossible to distinguish people on the badly lighted little platform. Uncanny Tales |Various 

All badly insert pure (dissyllabic) before flat; but smothe has two syllables. Chaucer's Works, Volume 1 (of 7) -- Romaunt of the Rose; Minor Poems |Geoffrey Chaucer 

All were badly and insufficiently fed, as much from disorganized commissariat arrangements as from actual want of supplies. The Philippine Islands |John Foreman 

A country parish clerk, being asked how the inscriptions on the tombs in the church-yard were so badly spelled? The Book of Anecdotes and Budget of Fun; |Various