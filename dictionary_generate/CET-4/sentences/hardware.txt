Founded in 2012, the hardware startup most recently raised a $20 million Series D in early 2019, bringing its total funding up to just shy of $50 million. Ember names former Dyson head as consumer CEO, as the startup looks beyond the smart mug |Brian Heater |February 12, 2021 |TechCrunch 

By 1960, the company had more than doubled in size, to seven people, and they sold an average of 45 cases per day from the trunks of their cars to hardware and sporting-goods stores in the San Diego area. 20 ingenious uses for WD-40 |By Bill Heavey/Field & Stream |February 11, 2021 |Popular-Science 

The mechanisms that allow it to learn are directly embedded in its hardware structure—no extra AI software required. This ‘Quantum Brain’ Would Mimic Our Own to Speed Up AI |Shelly Fan |February 9, 2021 |Singularity Hub 

Swarm prices out its orbital IoT network’s hardware and services Swarm’s low-cost satellite data network is now available to commercial clients |Darrell Etherington |February 9, 2021 |TechCrunch 

Apple announced Ternus will replace Riccio as the head of hardware engineering overall. Apple hardware chief Dan Riccio stepped down to focus on AR/VR |Samuel Axon |February 8, 2021 |Ars Technica 

A Michigan-based company that supplies fasteners to the hardware store, home center, and industrial markets. The 26 Next Hobby Lobbys |Abby Haglage |December 17, 2014 |DAILY BEAST 

A plastic factory, a hardware supplier, and shipping–and-receiving giants like Fed-Ex and DHL are neighboring businesses. House of the Witch: The Renegade Craft Brewers of Panama |Jeff Campagna |November 30, 2014 |DAILY BEAST 

Williams sold hardware manufactured by a legitimate company, said Sargent, who took a 25 to 30 percent commission. Patients Screwed in Spine Surgery ‘Scam’ |The Center for Investigative Reporting |November 3, 2014 |DAILY BEAST 

Not only in terms of the politics on the ground, but in terms of material and hardware. ‘Homeland’ Creator Talks Season 4’s ‘Reboot,’ Life After Brody, and How ISIS Will Affect the Show |Marlow Stern |October 6, 2014 |DAILY BEAST 

Amiigo is a wrist and shoelace hardware combo, paired a smartphone app that crunches movement data in the cloud. Amiigo Tracker Pairs Data With Deadlifts |Gregory Ferenstein |August 4, 2014 |DAILY BEAST 

They did so, and Crane looked up at the fat hardware man with eyes that were not quite so contemptuous. Scattergood Baines |Clarence Budington Kelland 

Scattergood Baines sat on the porch of his hardware store and looked down Coldriver Valley. Scattergood Baines |Clarence Budington Kelland 

He went back to his hardware store and waited—waited for Crane and Keith to start their inevitable logging operations. Scattergood Baines |Clarence Budington Kelland 

It cost him something like half a dollar an acre, and Landers considered he had robbed the hardware merchant of a machine. Scattergood Baines |Clarence Budington Kelland 

He had simply gone along selling hardware in his own way—and selling a good deal of it. Scattergood Baines |Clarence Budington Kelland