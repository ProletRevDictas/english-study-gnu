As an example of good science-and-society policymaking, the history of fluoride may be more of a cautionary tale. Anti-Fluoriders Are The OG Anti-Vaxxers |Michael Schulson |July 27, 2016 |DAILY BEAST 

For more than a century, Americans have been fretting about these sorts of ghosts. Anti-Fluoriders Are The OG Anti-Vaxxers |Michael Schulson |July 27, 2016 |DAILY BEAST 

But if Democrats are faced with the reality of a glut of qualified candidates, Republicans are assembling more of a fantasy team. The Golden State Preps for the ‘Red Wedding’ of Senate Races |David Freedlander |January 9, 2015 |DAILY BEAST 

But since those rosy scenarios were first floated, the California political scene has grown more crowded. The Golden State Preps for the ‘Red Wedding’ of Senate Races |David Freedlander |January 9, 2015 |DAILY BEAST 

If anything the work the two cops and the maintenance guy were doing deserves more respect and probably helped a lot more people. Trolls and Martyrdom: Je Ne Suis Pas Charlie |Arthur Chu |January 9, 2015 |DAILY BEAST 

Madame Ratignolle, more careful of her complexion, had twined a gauze veil about her head. The Awakening and Selected Short Stories |Kate Chopin 

I waited three months more, in great impatience, then sent him back to the same post, to see if there might be a reply. The Boarded-Up House |Augusta Huiell Seaman 

Bernard stood there face to face with Mrs. Vivian, whose eyes seemed to plead with him more than ever. Confidence |Henry James 

He was too drowsy to hold the thought more than a moment in his mind, much less to reflect upon it. The Wave |Algernon Blackwood 

The vision—it had been an instantaneous flash after all and nothing more—had left his mind completely for the time. The Wave |Algernon Blackwood