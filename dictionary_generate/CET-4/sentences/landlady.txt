Three days later, the landlady finally called my mom to say, “I think something might be wrong with Lee.” My Father's Life Was Shaped by Racism. So Was His Death |Savala Nolan |July 16, 2021 |Time 

She had been sharing a ground-floor apartment with her 28-year-old son in Allerton, a working-class neighborhood in the Bronx, before her landlady pushed her out to make space for her grandchildren. “We Don’t Even Know Who Is Dead or Alive”: Trapped Inside an Assisted Living Facility During the Pandemic |by Ava Kofman |November 30, 2020 |ProPublica 

His landlady/sometime girlfriend sued him, saying that they had agreed to split the winnings. Mega Millions Winners Might Not Be So Lucky: Jack Whittaker and More Unlucky Lotto Winners | |March 31, 2012 |DAILY BEAST 

The landlady had related the tragic history of the dead mother and the invalid aunt. The Joyous Adventures of Aristide Pujol |William J. Locke 

He returned to the hotel, and, eluding a gossip-seeking landlady, went up to his room. The Joyous Adventures of Aristide Pujol |William J. Locke 

The landlady also came tripping towards us, and invited us, in a very friendly manner, to spend the next Sunday with them. A Woman's Journey Round the World |Ida Pfeiffer 

When we returned to the Castle Inn, we found the landlady all attention and she spared no effort to contribute to our comfort. British Highways And Byways From A Motor Car |Thomas D. Murphy 

The landlady, clad in a low-necked black dress with long sweeping train, was typical of many we saw in the old-country hotels. British Highways And Byways From A Motor Car |Thomas D. Murphy