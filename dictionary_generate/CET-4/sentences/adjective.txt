I don’t have any more adjectives other than what you guys have probably been using, but, yeah, he’s playing at an elite, elite level. Auston Matthews, Connor McDavid lead the NHL MVP race |Neil Greenberg |February 26, 2021 |Washington Post 

Emotions and adjectives ran the gamut but few observers appeared to come away thinking that the last remaining superpower could rise above its bitter partisan rancor as the election looms barely a month away. ‘America faces a dangerous several weeks’: The world reacts to the first presidential debate |John Buysse |September 30, 2020 |Fortune 

In fact, “plastic” can also be used as an adjective and mean that something can be formed or shaped easily. Scientists Say: Plastic |Bethany Brookshire |September 28, 2020 |Science News For Students 

Moving forward, the team plans to experiment more to improve the quality of the image generation and expand the model’s visual and linguistic vocabulary to include more topics, objects, and adjectives. These weird, unsettling photos show that AI is getting smarter |Karen Hao |September 25, 2020 |MIT Technology Review 

This highly-trafficked phrase is also a testament to just how critical it is to communicate the specific dangers posed by an incoming storm -- even a simple choice of adjectives can make a difference in how information is disseminated. NOAA is changing the way it talks about hurricanes |Greta Moran |September 22, 2020 |Popular-Science 

This time he decline to offer an adjective, said he would leave it to others to go through the tea leaves of the election. In Press Conference, Obama Turns Conciliatory—Mostly |Eleanor Clift |November 6, 2014 |DAILY BEAST 

How could I have written something so, so (fill in the negative adjective)? Kerouac Biographer Gets Back on the Road |Dennis McNally |October 2, 2014 |DAILY BEAST 

And so we are all supposed to denote something from “working mother” as a descriptive adjective. The New Right-Wing Idol: Working Moms |Tim Teeman |July 16, 2014 |DAILY BEAST 

You use an adjective, it better be a sixty-four-dollar adjective. Pete Dexter’s Indelible Portrait of Author Norman Maclean |Pete Dexter |March 23, 2014 |DAILY BEAST 

The French have an adjective that the English language lacks—“digest.” Yes, Women Can Make Great Wine |Jordan Salcito |March 22, 2014 |DAILY BEAST 

Pope-holy; properly an adjective, meaning 'holy as a pope,' hence, hypocritical. Chaucer's Works, Volume 1 (of 7) -- Romaunt of the Rose; Minor Poems |Geoffrey Chaucer 

When the exact sense was lost, the suffix -al seemed to be adjectival, and the word dismal became at last an adjective. Chaucer's Works, Volume 1 (of 7) -- Romaunt of the Rose; Minor Poems |Geoffrey Chaucer 

This is the termination of the present participle and verbal adjective derived from verbs in -a. Frdric Mistral |Charles Alfred Downer 

This is wrong, and probably due to the dropping of the final e in the definite adjective firste. Chaucer's Works, Volume 1 (of 7) -- Romaunt of the Rose; Minor Poems |Geoffrey Chaucer 

Once more there is no definition of the term able-bodied, which is used sometimes as an adjective and sometimes as a substantive. English Poor Law Policy |Sidney Webb