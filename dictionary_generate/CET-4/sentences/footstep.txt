Emily followed in her footsteps and had a remarkable career as a surgeon, institutional leader and head of a medical school for women. Determined to practice medicine, two sisters defied conventions |Janet Golden |February 5, 2021 |Washington Post 

Presumably, she has followed in the footsteps of her godmother, Captain Marvel, and become some sort of do-gooder. Everything You Need to Know Before Watching WandaVision |Eliana Dockterman |January 14, 2021 |Time 

I guess I had map-making in my blood, though I hadn’t planned to follow in my father’s footsteps. Marie Tharp’s groundbreaking maps brought the seafloor to the world |Betsy Mason |January 13, 2021 |Science News 

Her career as a doctor may be one of the few things that will make her think twice about a full-time political career in her mother’s footsteps. Politics Report: Shirley Weber’s Shoes to Fill |Scott Lewis |December 26, 2020 |Voice of San Diego 

Rogers initially signed with George Washington in 2015, intent on following in his father’s footsteps. At 5-foot-2, a college guard puts up numbers that overshadow his height |Rick Maese |December 26, 2020 |Washington Post 

For three years I had been listening to the voice of Hope, and for three years I had waited for a footstep on my threshold. Read ‘The King in Yellow,’ the ‘True Detective’ Reference That’s the Key to the Show |Robert W. Chambers |February 20, 2014 |DAILY BEAST 

You can hear every footstep, which detracts from the experience exponentially. Gal With a Suitcase |Jolie Hunt |February 20, 2011 |DAILY BEAST 

Soon, however, another footstep became audible on the stairs below, and this time Sarah heard it distinctly. Skipper Worse |Alexander Lange Kielland 

She tried to see her way through; she was thinking it out when the sound of a footstep behind caused her to look round. The Weight of the Crown |Fred M. White 

One step away he made, then his foot halted, as the whispering sound of a quick footstep came from behind through the bush. Menotah |Ernest G. Henham 

Away they went, with Gyp at their heels, and every footstep resounded through the old house until they reached the upper floor. St. Nicholas, Vol. 5, No. 5, March, 1878 |Various 

There was a slow, heavy footstep upon the stairs, and in a moment Seth had hidden the letter. The Light That Lures |Percy Brebner