A chrome-plated steel nib writes smoothly and holds its shape for years. No-fuss fountain pens that make your note taking and letter writing more elegant |PopSci Commerce Team |September 15, 2020 |Popular-Science 

We quickly realized that indexing problems come in all possible shapes and sizes. How to earn your place in Google’s index in 2020 |Bartosz Góralewicz |September 14, 2020 |Search Engine Land 

It is only connected to the collection of shapes and sounds that make up the word tree because we have all agreed that it is. The true love story in Elif Batuman’s The Idiot is a love affair with language |Constance Grady |September 11, 2020 |Vox 

One of Howell’s own studies, she and her team reported online in August in mSphere, suggests that fungal species in particular shape the metabolites — and thus aroma and flavor — in wine from different growing regions in Australia. How does a crop’s environment shape a food’s smell and taste? |Carolyn Beans |September 10, 2020 |Science News 

I think it helped us connect, not to compare struggles, they’re all different, but to connect with a lot of different people who are in that struggle for belonging in some way, shape or form. Pete Buttigieg Says Dems Did Not Coordinate to Seal Biden’s Primary Win |Nick Fouriezos |September 9, 2020 |Ozy 

I mean, physically, mentally, you know, in every way, shape, and form. I Tried to Warn You About Sleazy Billionaire Jeffrey Epstein in 2003 |Vicky Ward |January 7, 2015 |DAILY BEAST 

“Under Suleimani several military branches have taken shape [in Iraq] which are run by Iran and the Iranian military,” he said. What an Iranian Funeral Tells Us About the Wars in Iraq |IranWire |January 6, 2015 |DAILY BEAST 

“You were going to work your way into my marriage and you were going to call its new three-way shape holy” the narrator recounts. A Novel Nearly Impossible to Review |Nicholas Mancusi |December 28, 2014 |DAILY BEAST 

If you drink from a flute, do so from a tulip-shape one to concentrate the notes, Simonetti-Bryan says. Champagne: You’re Drinking It All Wrong |Kayleigh Kulp |December 20, 2014 |DAILY BEAST 

The Babadook is the shape of grief: all-enveloping, shape-shifting, black, here intensely, terrifying, then gone. Grief: The Real Monster in The Babadook |Tim Teeman |December 19, 2014 |DAILY BEAST 

They are ovoid in shape, and lie in pairs, end to end, often forming short chains. A Manual of Clinical Diagnosis |James Campbell Todd 

Leaves of a lanceolate form are the largest, and the shape of those found on most varieties of the American plant. Tobacco; Its History, Varieties, Culture, Manufacture and Commerce |E. R. Billings. 

Microscopically, they are yellow or reddish-brown crystals, which differ greatly in size and shape. A Manual of Clinical Diagnosis |James Campbell Todd 

Here the Goat, who evidently was not yet quite started, inquired, "Must all the halves be of the same shape?" Davy and The Goblin |Charles E. Carryl 

He was Honour's self, till he brought the serpent to his bosom, in the shape of his perfidious son. The Pastor's Fire-side Vol. 3 of 4 |Jane Porter