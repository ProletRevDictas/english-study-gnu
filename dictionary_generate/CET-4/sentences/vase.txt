An art history book is shown amid a stack of books, alongside a vase of fresh flowers and a ceramic figurine of a stout, reclining man who looks relaxed and content. Alibaba shares jump on Jack Ma’s first appearance in 3 months |Rita Liao |January 20, 2021 |TechCrunch 

The manufacturer suggests the shelf can be used for plants or vases of cut flowers to add to the decor of a home. Macrame wall hangings for every home |PopSci Commerce Team |January 8, 2021 |Popular-Science 

For vases and other glass items, stuff them with plenty of cushioning, both inside and out. Mailing gifts this year? Here are tips from the pros on how to box up your items properly. |Jura Koncius |December 3, 2020 |Washington Post 

Bezos sat in front of a large bookshelf, complete with vases. Facebook, Google, Twitter CEOs clash with Congress in pre-election showdown |Tony Romm, Rachel Lerman, Cat Zakrzewski, Heather Kelly, Elizabeth Dwoskin |October 28, 2020 |Washington Post 

Also included is twine rope to help hang each vase from planter hooks. These hanging planters bring life to your space |PopSci Commerce Team |October 8, 2020 |Popular-Science 

The modest eating area has more than a dozen tables, each punctuated by a small porcelain vase with a plastic flower poking out. The Secret Soviet Power Bunker—in Latvia, a Hiding Place for the Elite |Brandon Presser |September 25, 2014 |DAILY BEAST 

Other days we bring replacement flowers and a new stuffed animal, which my wife ties to the vase to keep it in place. How Losing My Daughter Changed My Faith |Kyle Cupp |June 15, 2014 |DAILY BEAST 

Never mind that the Tribune props him up like a Ming vase now. The Stacks: John Schulian’s Classic Profile of Newspaper Columnist Mike Royko |John Schulian |January 5, 2014 |DAILY BEAST 

I could see the vase on her dresser, one of the thin glass globes, I imagined, that come free with flower arrangements. Daddy, How Come You’re Always Broke? Benjamin Anastas’s ‘Too Good to Be True’ |Benjamin Anastas |October 15, 2012 |DAILY BEAST 

Throwing Louis C.K. out in the cold with Daniel Tosh is like tossing a Ming vase out with the old Tupperware. Louis C.K. on Daniel Tosh’s Rape Joke: Are Comedy and Feminism Enemies? |Jennifer L. Pozner |July 18, 2012 |DAILY BEAST 

In a sweeping passion she seized a glass vase from the table and flung it upon the tiles of the hearth. The Awakening and Selected Short Stories |Kate Chopin 

As she fills each vase she takes it to its place, steps back to consider the effect, and returns to fill the next one. First Plays |A. A. Milne 

In the centre of this table stood a tawdry Japanese vase, worth, perhaps, five or six shillings. Bella Donna |Robert Hichens 

We were shown how the bowl or vase was burned, shrinking to nearly half its size in the process. British Highways And Byways From A Motor Car |Thomas D. Murphy 

Henry knew it was exactly the same white puffball that he had noticed in Violet's vase that very morning. The Box-Car Children |Gertrude Chandler Warner