Within a decade or so, business schools had begun to teach that the purpose of business is to maximize “shareholder value,” a mantra that eventually rang out from practically every company boardroom. The ghost of Milton Friedman will haunt the markets until companies fix CEO pay |Judith Samuelson |September 16, 2020 |Quartz 

IBM is feeling confident enough in its abilities that it is unveiling its full quantum hardware roadmap for the decade ahead. IBM plans a huge leap in superfast quantum computing by 2023 |rhhackettfortune |September 15, 2020 |Fortune 

Indeed, with electric vehicles cutting into oil demand by the end of the decade, it may never fully recover. Big Oil’s hopes are pinned on plastics. It won’t end well. |David Roberts |September 4, 2020 |Vox 

After literal decades of looking up at Brady and the Patriots, their fellow AFC East teams can sense that a new era may be at hand. Newton Can Replace Brady, But Can The Pats Replace Half Of Their Defense? |Neil Paine (neil.paine@fivethirtyeight.com) |September 3, 2020 |FiveThirtyEight 

The more we looked over the past couple of decades, the more prevalent fresh produce is as a source of foodborne illness. The Salmonella Outbreaks Among Peaches and Onions, Explained |Jenny G. Zhang |August 28, 2020 |Eater 

Yet for a vivid decade or so, sleaze was, somewhat paradoxically, a force for literacy and empowerment. How Pulp Fiction Saved Literature |Wendy Smith |January 8, 2015 |DAILY BEAST 

Who are some younger popular historians that you think will be a lot better known a decade from now? Thank Congress, Not LBJ for Great Society |Julian Zelizer, Scott Porch |January 4, 2015 |DAILY BEAST 

Who knew that “we shall overcome” meant “we, the few, shall book covers every decade or so, maybe, sometimes, if we are in style.” One Vogue Cover Doesn’t Solve Fashion’s Big Race Problem |Danielle Belton |January 2, 2015 |DAILY BEAST 

And black fury toward cops today is fueled by historic economic disparities and by the economic disaster of the past decade plus. No Gods, No Cops, No Masters |James Poulos |January 1, 2015 |DAILY BEAST 

Beyoncé has, for close to a decade now, been a deity in entertainment: untouchable, successful, divine. Bow Down, Bitches: How Beyoncé Turned an Elevator Brawl Into a Perfect Year |Kevin Fallon |December 31, 2014 |DAILY BEAST 

During the past decade, the population of three cities has been materially increased through annexation. Hallowed Heritage: The Life of Virginia |Dorothy M. Torpey 

Approximately 216,900 persons became residents of Virginia during this decade. Hallowed Heritage: The Life of Virginia |Dorothy M. Torpey 

We do not know when Chrétien wrote the Erec, but it was almost certainly some time in the decade 1150-60. The Three Days' Tournament |Jessie L. Weston 

The advancing sand gradually crept into the hamlet, and in the course of a decade dispossessed the people by burying their houses. Outlines of the Earth's History |Nathaniel Southgate Shaler 

He lived indeed through its first decade, but his active life was over before it began. The English Church in the Eighteenth Century |Charles J. Abbey and John H. Overton