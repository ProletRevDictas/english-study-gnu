Some control cell division or root production or bud formation. Pig sex and celery have a surprising connection |PopSci Staff |September 30, 2020 |Popular-Science 

Givago Miranda, a farmer in the Tres Pontas municipality of Minas Gerais, said his trees are withering and turning yellow, with buds getting “cooked” before opening in some cases. Coffee prices could surge soon as La Nina heat scorches world’s largest crop supply |Rachel King |September 29, 2020 |Fortune 

The Epic headphones have deep, clear sound and a no-hassle fit, thanks to the five different ear tips and fins that help secure the buds. Gear to Take Your Office on the Road |Graham Averill |September 27, 2020 |Outside Online 

This pathogenic bacterium can infest plant leaves, stems, buds, and flowers, causing a nasty infection and a lot of sick plants. Junk Food Is Bad For Plants, Too - Issue 90: Something Green |Anne Bikl&#233; & David R. Montgomery |September 23, 2020 |Nautilus 

Learning how these cells sense and how information passes between them, the buds and the brain, he says, could someday allow people to engineer taste signals. Newly discovered cells in mice can sense four of the five tastes |Carolyn Wilke |August 13, 2020 |Science News 

Can you imagine Bud Powell or Charlie Parker writing a jingle? Herbie Hancock Holds Forth |David Yaffe |November 8, 2014 |DAILY BEAST 

At one point Mullaney returned to Coronado and served as a BUD/S instructor training junior SEALs. The Navy SEAL Who Swindled His Brothers |Jacob Siegel |September 12, 2014 |DAILY BEAST 

He owns a smallish vaporizer that still looks like it came from the future, and buys wax instead of bud. This Is Your E-Cigarette on Drugs |Daniel Genis |July 28, 2014 |DAILY BEAST 

Yeah,” he says finally, squelching his giggles by downing half a bottle of Bud, “that sounds like Ray. The Stacks: The Judas Priest Teen Suicide Trial |Ivan Solotaroff |June 28, 2014 |DAILY BEAST 

Lebanese security agencies have been quick to try to nip what could well be a new bombing spate in the bud. ISIS May Open a Third Front in Lebanon |Jamie Dettmer |June 25, 2014 |DAILY BEAST 

By a voice he saith: Hear me, ye divine offspring, and bud forth as the rose planted by the brooks of waters. The Bible, Douay-Rheims Version |Various 

When they shall rush in unto Jacob, Israel shall blossom and bud, and they shall fill the face of the world with seed. The Bible, Douay-Rheims Version |Various 

He had seen Mildred creep from babyhood into childhood, and bud from girlhood to womanhood. The Fifth String |John Philip Sousa 

It was not until the third trip that Bud thought her beautiful, and was secretly glad that he had not kissed that San Jose girl. Cabin Fever |B. M. Bower 

But Bud happened to be a simple-souled fellow, and there was something about Marie—He didn't know what it was. Cabin Fever |B. M. Bower