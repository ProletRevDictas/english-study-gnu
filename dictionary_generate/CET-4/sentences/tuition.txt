Unfortunately, her family couldn’t afford to pay her tuition and expenses. How the Best Workplaces in Manufacturing have risen to the COVID-19 challenge |lbelanger225 |September 10, 2020 |Fortune 

Guild works with companies to offer tuition reimbursement, online programs, and degrees to their employees. ‘Don’t wait to be perfect:’ 4 top startup tips from a unicorn’s founder and investor |Beth Kowitt |September 6, 2020 |Fortune 

Tuition resets — a one-time reduction in tuition usually accompanied by cuts in financial aid — are also on the table for some schools. The (Deferred) Class of 2020 |Sandya Kola |August 9, 2020 |Ozy 

Nor will she have to pay full tuition for a remote or hybrid education. With school plans unclear, affluent US parents are pursuing other options |Michelle Cheng |July 28, 2020 |Quartz 

In addition to paying tuition, these students help professors conduct research, teach undergraduates, and help retain top faculty. ICE’s ruling is a staggering blow to the US’s computer science departments |Karen Ho |July 9, 2020 |Quartz 

“Roughly a third” of the tuition goes to instructors, according to one former coach who asked not to be named. The Secret World of Pickup Artist Julien Blanc |Brandy Zadrozny |December 1, 2014 |DAILY BEAST 

Now, tuition accounts for an average of 36 percent of their operating budgets. The Student Loan Crisis That Isn’t About Kids at Harvard |Monica Potts |November 30, 2014 |DAILY BEAST 

And why has tuition risen so sharply at public universities? The Student Loan Crisis That Isn’t About Kids at Harvard |Monica Potts |November 30, 2014 |DAILY BEAST 

The Branson School holds an elite reputation in tony Marin County, charging around $40,000 a year for tuition. Headmasters Behaving Badly |Emily Shire |November 29, 2014 |DAILY BEAST 

It covers kindergarten through 8th grade and has $3,825 annual tuition, but fundraising allows many to get $1,500 in tuition aid. Freedom From Fear for Dreamer Kids |Mike Barnicle |November 24, 2014 |DAILY BEAST 

In short, Marcella had been too long under her tuition, to become a willing devotee to the monastic rites of the Romish Church. The Pastor's Fire-side Vol. 3 of 4 |Jane Porter 

He may, if he desires to assist himself, have recourse to literary labor, or to tuition. The Ladies' Book of Etiquette, and Manual of Politeness |Florence Hartley 

Of course, their tuition fees—one hundred and seventy-five dollars each—for the year had been already paid. Ruth Fielding At College |Alice B. Emerson 

There is no inducement to admit a pupil for the sake of the tuition fees, or for the purpose of adding to the number of scholars. Thoughts on Educational Topics and Institutions |George S. Boutwell 

They very soon became expert swimmers, by the way, under my tuition. The Adventures of Louis de Rougemont |Louis de Rougemont