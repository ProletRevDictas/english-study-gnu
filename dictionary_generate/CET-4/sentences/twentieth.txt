For example, a high-contrast signal will arrive a twentieth of a second faster than a low-contrast signal. Want art you can’t look away from? These 5 visual illusions will entrance your mind |Corinne Iozzio |July 21, 2021 |Popular-Science 

The second lesson is that no one writing before the twentieth century holds a key to our problems. American Democracy Under Threat for 250 Years |Jedediah Purdy |December 28, 2014 |DAILY BEAST 

Whatever the true figure, it was a massacre of twentieth-century proportions. In Threatening Baghdad, Militants Seek to Undo 800 Years of History |Justin Marozzi |August 16, 2014 |DAILY BEAST 

FoxFaith, established in 2006 by Twentieth Century-Fox, flew under the radar for awhile, and died a quick death. Bible Flicks Move Beyond the B-List |Lewis Beale |August 3, 2014 |DAILY BEAST 

Beside the fact that they are two of the greatest draughtsmen of the twentieth century. The Art World’s New Gang War |Anthony Haden-Guest |May 1, 2014 |DAILY BEAST 

In 1975, Rüdiger Heim, approaching his twentieth birthday, decided that he had to see his father again. Hunting Down Aribert Heim, Egypt’s Hidden Nazi |Nicholas Kulish, Souad Mekhennet |March 24, 2014 |DAILY BEAST 

Hereditary legislation in the twentieth century and the most civilized country in the world! Ancestors |Gertrude Atherton 

The payment for this drainage was to be one-twentieth part of the ore raised by the different mines. Life of Richard Trevithick, Volume II (of 2) |Francis Trevithick 

When the men landed upon the moon in the twentieth century, they did not find it at first. Old Friends Are the Best |Jack Sharkey 

It is an old, straggling place that seems to be little in harmony with the progress of the Twentieth Century. British Highways And Byways From A Motor Car |Thomas D. Murphy 

And in the four and twentieth day of the first month, I was by the great river, which is the Tigris. The Bible, Douay-Rheims Version |Various