With a foot in the door, it’s easy to look past unpleasant or unsafe behavior, especially when no one else is sounding an alarm. The Toxicity of Restaurant Kitchens Is Exactly Why I Never Reported Abuses |Lindsey Danis |February 4, 2021 |Eater 

It was a job requirement, particularly during his nearly 15-year career as a diplomat in Europe, but he clearly considered it an unpleasant obligation. Not all presidents’ dance skills are created equal |Bonnie Berkowitz, Joanne Lee |January 21, 2021 |Washington Post 

This is a very unpleasant experience, but I am able to see where errors were made. Olympic gold medal hurdler provisionally suspended in doping case |Glynn A. Hill |January 14, 2021 |Washington Post 

There may yet be more unpleasant surprises in store for millions of people around the world as the pandemic plays out. Technology can help us feed the world, if we look beyond profit |Katie McLean |December 18, 2020 |MIT Technology Review 

It is altogether unpleasant, and I shudder to think what new habit will come next. Miss Manners: Husband’s rude eating habits driving wife up the wall |Judith Martin, Nicholas Martin, Jacobina Martin |December 18, 2020 |Washington Post 

Both are stale and boring, and whichever one you end up having in the end is still unpleasant. Team Peeta or Team Gale: Why the ‘Hunger Games’ Love Triangle Ruins ‘Mockingjay – Part 1’ |Kevin Fallon |November 28, 2014 |DAILY BEAST 

That was an unpleasant reminder that for many people, science is still considered a boys club. Earthlings, We Landed on a Comet |Matthew R. Francis |November 12, 2014 |DAILY BEAST 

It had to be an unpleasant shock for the much-lauded Bratton. Bill Bratton Scolds Giggling Audience at American Justice Summit |Lloyd Grove |November 11, 2014 |DAILY BEAST 

It goes beyond just finding the music unpleasant, it invokes the rhetoric of legitimacy. Of Gamers, Gates, and Disco Demolition: The Roots of Reactionary Rage |Arthur Chu |October 16, 2014 |DAILY BEAST 

It was unpleasant and discombobulating: a simulation of hostage-taking, mental asylum and demented dreamscape all rolled into one. Sex, Blood, and Screaming: Blackout’s Dark Frights |Tim Teeman |October 7, 2014 |DAILY BEAST 

That will give us time to turn about us, and to prepare ourselves against similar unpleasant casualties. Blackwood's Edinburgh Magazine, No. CCCXXXIX. January, 1844. Vol. LV. |Various 

No one could appreciate better than ourselves the unpleasant possibilities that stared us in the face. Raw Gold |Bertrand W. Sinclair 

An attempt to impose an imitation on a practised judge is always productive of an unpleasant result. Violins and Violin Makers |Joseph Pearce 

Jess had been used to these unpleasant occasions ever since she was a very little girl. The Girls of Central High on the Stage |Gertrude W. Morrison 

Never repeat to a person with whom you converse, any unpleasant speech you may have heard concerning her. The Ladies' Book of Etiquette, and Manual of Politeness |Florence Hartley