People have been forced to go online, who might not have gone there as a first choice. Sophie Hill on the changing face of retail and surviving 2020 |Margaret Trainor |September 17, 2020 |TechCrunch 

The Prismacolor pencil set comes with an impressive variety of 150 colors, making this a great choice for art enthusiasts. Excellent pencils for school, work, and beyond |PopSci Commerce Team |September 16, 2020 |Popular-Science 

The economy ranks as the top issue in the state, with 27 percent of registered voters citing it as the single most important issue in their choice for president. Post-ABC Wisconsin poll shows Biden holding narrow edge over Trump |Dan Balz, Emily Guskin |September 16, 2020 |Washington Post 

Zero-rating is detrimental to consumers, it compromises their freedom of choice. The EU’s top court just closed a major loophole in Europe’s net-neutrality rules |David Meyer |September 15, 2020 |Fortune 

He said he wants to create a “workplace of choice,” with policies that allow anyone who desires to be successful within the company to find their path. Lowe’s teams up with Daymond John to diversify its suppliers with a virtual pitch competition |McKenna Moore |September 15, 2020 |Fortune 

The choice between freedom and fear is not difficult when seen with perspective. Why We Stand With Charlie Hebdo—And You Should Too |John Avlon |January 8, 2015 |DAILY BEAST 

Serve with the warm sauce and your choice of ice cream, whipped cream, or yogurt. Make ‘The Chew’s’ Carla Hall’s Sticky Toffee Pudding |Carla Hall |December 28, 2014 |DAILY BEAST 

Removing choice is bullying and seems a horrid basis on which to anchor your relationship. Public Marriage Proposals Must Die |Tauriq Moosa |December 28, 2014 |DAILY BEAST 

When Hitler became chancellor on Jan. 30, 1933, Hildebrand was confronted with a choice: Would he remain in Nazi Germany? The Catholic Philosopher Who Took on Hitler |John Henry Crosby |December 26, 2014 |DAILY BEAST 

Disney has a choice whether to produce a program with certain fictional characters; the storyline could be re-written or changed. Yep, Korra and Asami Went in the Spirit Portal and Probably Kissed |Melissa Leon |December 25, 2014 |DAILY BEAST 

With some difficulty Jos explained his mother's disclaimer of the title of Senora, and the choice of names she offered to Ramona. Ramona |Helen Hunt Jackson 

Then Mr. Blackbird selected a good many choice tidbits here and there, which he bolted with gusto. The Tale of Grandfather Mole |Arthur Scott Bailey 

Pedantic, unimaginative and presumptuous, Theobald was the logical choice for a Dunce King in 1728. A Letter from Mr. Cibber to Mr. Pope |Colley Cibber 

He apologized for interrupting their tête-à-tête, but said he had no choice, as the saloon was completely full. Bella Donna |Robert Hichens 

The poor artist reconciled himself to go for a time to Brittany, and his choice fell on Concarneau. Bastien Lepage |Fr. Crastre