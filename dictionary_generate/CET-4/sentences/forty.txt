Forty minutes later he says, ‘I think she may have chest injuries now.’ Harry’s Daddy, and Diana’s ‘Murder’: Royal Rumors In a New Play |Tom Sykes |January 4, 2015 |DAILY BEAST 

I was already over forty, had hardly a nickel in my pocket and this was the biggest break in my life. The Story Behind Lee Marvin’s Liberty Valance Smile |Robert Ward |January 3, 2015 |DAILY BEAST 

Almost forty years later, it seems unlikely this will ever be the case. Juiciest ‘Star Wars: The Force Awakens’ Rumors (and Some Debunked Ones) |Rich Goldstein |January 3, 2015 |DAILY BEAST 

Forty-two years after its debut, The Godfather casts a long shadow over American cinema. The Alterna-‘Godfather’: ‘A Most Violent Year’ |Nick Schager |December 30, 2014 |DAILY BEAST 

We were on it for forty minutes of the film, a considerable part of our schedule. Alfred Hitchcock’s Fade to Black: The Great Director’s Final Days |David Freeman |December 13, 2014 |DAILY BEAST 

Botanists have enumerated between forty and fifty varieties of the tobacco plant who class them all among the narcotic poisons. Tobacco; Its History, Varieties, Culture, Manufacture and Commerce |E. R. Billings. 

It succeeds best in a deep rich loam in a climate ranging from forty to fifty degrees of latitude. Tobacco; Its History, Varieties, Culture, Manufacture and Commerce |E. R. Billings. 

After about the forty-fifth year it becomes gradually less; after seventy-five years it is about one-half the amount given. A Manual of Clinical Diagnosis |James Campbell Todd 

It was Wednesday night; over forty men sat down to the house-dinner at the Pandemonium Club. The Pit Town Coronet, Volume I (of 3) |Charles James Wills 

The moon rose on a terrified mob trudging or riding the forty miles of road between Meerut and the Mogul capital. The Red Year |Louis Tracy