Right now we have a president who turns our tragedies into political weapons. My Dad served in WWII — he was a hero, not a loser |Peter Rosenstein |September 10, 2020 |Washington Blade 

The skit compares the Death Star’s destruction to the heinous tragedy that befell America nineteen years ago. Amid geopolitical tensions, ‘Mulan’ is a litmus test for loyalties |rhhackettfortune |September 9, 2020 |Fortune 

The video of Jacob Blake’s shooting feels like a repeat of the same kind of tragedy, showing an officer repeatedly shooting Blake in the back. Violent protests against police brutality in the ’60s and ’90s changed public opinion |German Lopez |August 28, 2020 |Vox 

The pandemic has been an unprecedented event on a truly planetary scale, one that has sadly given people all over the world a unifying human experience through tragedy. Social Distancing From the Stars |Emily Levesque |August 11, 2020 |Quanta Magazine 

The past few months have taught us once again that the greatest tragedies in the world do not affect everyone equally. Let’s lead the return to prosperity by protecting the vulnerable |Rich Lesser |July 15, 2020 |Quartz 

When twelve people are killed by violence, whoever they are, for whatever reason, that is a tragedy and a waste. Trolls and Martyrdom: Je Ne Suis Pas Charlie |Arthur Chu |January 9, 2015 |DAILY BEAST 

It generates tragedy, violence, and a windfall for undertakers. Why Mexicans Are Enraged by Obama’s Big Tuesday Meeting |Ruben Navarrette Jr. |January 6, 2015 |DAILY BEAST 

Yes, publicizing tragedy gets clicks, gets ad revenue, gets notoriety, and can be done for all the wrong reasons. Cover-Ups and Concern Trolls: Actually, It's About Ethics in Suicide Journalism |Arthur Chu |January 3, 2015 |DAILY BEAST 

Does the sending of the message “justify” the tragedy that caused it? Cover-Ups and Concern Trolls: Actually, It's About Ethics in Suicide Journalism |Arthur Chu |January 3, 2015 |DAILY BEAST 

The fate of AirAsia Flight 8501 and the 162 souls on board is a tragedy, but it will not remain a mystery for much longer. Who Will Get AsiaAir 8501’s Black Boxes? |Clive Irving |December 30, 2014 |DAILY BEAST 

Several pioneers familiar with the facts of the tragedy at the time of its occurrence were also present. Among the Sioux |R. J. Creswell 

Happening to walk down the Rue Saint Honoré, he had come upon tragedy. The Joyous Adventures of Aristide Pujol |William J. Locke 

A heaviness as of unguessed tragedy lay upon all three, not only upon Tom. The Wave |Algernon Blackwood 

She had seen little of the tragedy enacted in Meerut; she knew less of its real horrors. The Red Year |Louis Tracy 

She produced the tragedy of Agnes de Castro in her 17th year, which was followed by several others. The Every Day Book of History and Chronology |Joel Munsell