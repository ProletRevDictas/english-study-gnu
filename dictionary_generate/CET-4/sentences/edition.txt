Of the 4,500 recipes in the updated “Joy,” only 50 were unchanged from the earlier edition. Maria Guarnaschelli, influential cookbook editor, dies at 79 |Emily Langer |February 11, 2021 |Washington Post 

Michelle Obama is releasing a new edition of her best-selling 2018 memoir Becoming, geared toward young readers. 'My Story in All Its Messy Glory.' Michelle Obama Is Releasing a New Edition of Becoming for Young Readers |Megan McCluskey |February 3, 2021 |Time 

The arrival will also be joined by a self-charging “Enterprise” edition of the robot and the already announced Spot Arm. Soon Boston Dynamics’ Spot will be remotely opening doors anywhere |Brian Heater |February 2, 2021 |TechCrunch 

To start out, FOS Essentials will use the FOS daily newsletter amplification to get in front of prospective students and Pepsi will be sponsoring some editions of that as well. Why Front Office Sports and Pepsi are taking a B2B approach to Super Bowl marketing |Kayleigh Barber |February 2, 2021 |Digiday 

Great British Bake Off will launch a celebrity edition with James McAvoy and others, and I am HERE for it. The Pandemic Made Instagram an Essential Tool for Restaurants |Amanda Kludt |February 1, 2021 |Eater 

A limited edition export stout known as the Indra Kunindra came to wash it down. Dinner at Nitehawk Cinema: ‘Christmas Vacation’ and a Beer in a Pear Tree |Rich Goldstein |December 12, 2014 |DAILY BEAST 

On the back cover of the first paperback edition we get a glimpse of the media buzz. Living Black & Gay in the ’50s |Alain Mabanckou |December 3, 2014 |DAILY BEAST 

She reportedly also had a book collection worth more than €20 million, including a first edition of Don Quixote from 1605. Adiós to the Diva Duchess |Barbie Latza Nadeau |November 20, 2014 |DAILY BEAST 

If the new edition of Mary Landrieu shows up in the Senate, the Republicans win either way. Didn't Obama Hear Oregon’s Warning Shot on Immigration? |Doug McIntyre |November 14, 2014 |DAILY BEAST 

With only 7,500 sets created, this limited edition 41-DVD box set is available on November 11, 2014 for an SRP of $349.98. How a War-Weary Vet Created ‘The Twilight Zone’ |Rich Goldstein |November 13, 2014 |DAILY BEAST 

In the early sixties a cheap edition appeared, and cheap editions were rare things then. Fifty Years of Railway Life in England, Scotland and Ireland |Joseph Tatlow 

A copy of Tendall's testament sold at Oxford for 20 guineas, supposed to be the only copy of that edition unburned by Tonstall. The Every Day Book of History and Chronology |Joel Munsell 

Transcribers Notes: This ebook has been transcribed from the original print edition, published in 1767. Remarks on a Pamphlet Lately published by the Rev. Mr. Maskelyne, |John Harrison 

At all events, they are not to be found in the second edition of Christabel , nor in any subsequent edition. Notes and Queries, Number 177, March 19, 1853 |Various 

It is the last edition (Paris, 1840), and purports to be "augmente d'un examen critique et des preuves positives," &c. Notes and Queries, Number 177, March 19, 1853 |Various