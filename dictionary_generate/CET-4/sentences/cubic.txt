The two swimmers are chosen based on FINA points, which are calculated using a cubic curve. Out athletes going for gold |Kevin Majoros |July 22, 2021 |Washington Blade 

Within six days, all 600 million to 750 million cubic meters of lake water had vanished, leaving a deep sinkhole filled with fractured ice. Satellites show how a massive lake in Antarctica vanished in days |Carolyn Gramling |July 12, 2021 |Science News 

Thanks to its very low density of fewer than 40 kilograms per cubic meter, the cockpit was ultra-lightweight. Use today’s tech solutions to meet the climate crisis and do it profitably |Walter Thompson |February 12, 2021 |TechCrunch 

Hilbert’s idea of using a line on a cubic surface to solve a ninth-degree polynomial can be extended to lines on these higher-dimensional hypersurfaces. Mathematicians Resurrect Hilbert’s 13th Problem |Stephen Ornes |January 14, 2021 |Quanta Magazine 

The concentration of PM2.5, the smallest particulate matter, is at 153 micrograms per cubic meter. Beijing’s ‘Star Trek’ APEC Summit |Jake Adelstein |November 11, 2014 |DAILY BEAST 

Each year their fields need at least 53 billion cubic meters of water for irrigation. The Aral Sea's Disappearing Act |Anna Nemtsova |October 4, 2014 |DAILY BEAST 

The conflict in Ukraine caused Gazprom to cut production volumes from 496.4 billion cubic meters to 463 billion cubic meters. In Ukraine, Winter Is Coming |Anna Nemtsova |September 23, 2014 |DAILY BEAST 

The daughter had also been exposed and was comatose before she and her mother each received A 250 cubic centimeter transfusion. Infected Ebola Doctor Kent Brantly Is an Endangered Hero |Michael Daly |August 3, 2014 |DAILY BEAST 

She grew increasingly ill despite a 450 cubic centimeter transfusion and became comatose after five days. Infected Ebola Doctor Kent Brantly Is an Endangered Hero |Michael Daly |August 3, 2014 |DAILY BEAST 

The product is then multiplied by the number of cubic centimeters voided in twenty-four hours and divided by 1000. A Manual of Clinical Diagnosis |James Campbell Todd 

Clodd tells us that one cubic inch of rotten stone contains 41 thousand million vegetable skeletons of diatoms. God and my Neighbour |Robert Blatchford 

The normal number of leukocytes varies from 5000 to 10,000 per cubic millimeter of blood. A Manual of Clinical Diagnosis |James Campbell Todd 

An immense bag of linen lined with paper, and containing 23,000 cubic feet, was provided for the occasion. The Every Day Book of History and Chronology |Joel Munsell 

Places on the Alaskan coast, laid bare at high tide, are said to have yielded as much as $12,000 per cubic yard. The Wonder Book of Knowledge |Various