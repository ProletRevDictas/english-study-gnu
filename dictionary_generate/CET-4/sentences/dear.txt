We can still value the fact that, for many years, this person was a dear friend. Politics and conspiracy theories are fracturing relationships. Here’s how to grieve those broken bonds. |Jeff Schrum |February 11, 2021 |Washington Post 

Finding the right college you can afford, dear reader, is within your grasp. Paying for College Can Be Overwhelming. Here's What You Need to Know to Find an Affordable Option |Sean Gregory |February 5, 2021 |Time 

It is with great sadness that we announce the death of our dear father, Captain Sir Tom Moore. Captain Tom Moore, Fundraising Hero of Britain's COVID-19 Lockdown, Dies at 100 |Billy Perrigo |February 2, 2021 |Time 

JoAnn Fields watched the funerals of two dear friends and leaders in the Filipino community in the last week of January. Filipino Residents Have Been Hit Hard by COVID – But No One Knows Just How Hard |Maya Srikrishnan |February 1, 2021 |Voice of San Diego 

Many minutes are spent explaining to dear Aunt Helen how to unmute, telling Uncle Bob that we can't see his head, or asking a cousin to move the yapping puppy farther from the microphone. Miss Manners: Save me from these interminable video ‘parties’ |Judith Martin, Nicholas Martin, Jacobina Martin |February 1, 2021 |Washington Post 

If it should be said these are general remarks, let it be remembered that they are the dear-bought, result of experience. Caleb Williams |William Godwin 

For a flea, my dear-gentlemen may bring that theirselves; but a b——-, that's a stationary, and born of a bed. Evan Harrington, Complete |George Meredith 

So they decided to take it in to the dear-mother and have her explain it to them. In Story-land |Elizabeth Harrison 

But, hark ye, dear-my-soul, make thou no haste; there is room for all. Christmas Tales and Christmas Verse |Eugene Field 

The singer came rushing down a branch, bristled up, blustering, and calling "Dear-r-r-r!" A Bird-Lover in the West |Olive Thorne Miller