Other measures announced Thursday by the league include the recommendation that teams provide all team personnel with KN95 masks. NHL adds game-day rapid testing to coronavirus protocols |Samantha Pell |February 12, 2021 |Washington Post 

Yet straddling the line between public transparency and personnel sensitivity at a place like the New York Times is one of the toughest tasks in media. New York Times bails on intent-neutral standard when discussing epithets |Erik Wemple |February 11, 2021 |Washington Post 

At the end of the season, he balked at Snyder’s plans to take away his authority over trades and personnel decisions and was fired. Marty Schottenheimer, one of the NFL’s winningest coaches, dies at 77 |Matt Schudel |February 9, 2021 |Washington Post 

Hoboken’s contract says records of police discipline will be expunged and removed from an officer’s personnel files after five years. How the Police Bank Millions Through Their Union Contracts |by Andrew Ford, Asbury Park Press, and Agnes Chang, Jeff Kao and Agnel Philip, ProPublica |February 8, 2021 |ProPublica 

Janet Woodcock, the FDA’s acting commissioner, pledged to pursue streamlined processes to get updated products to health-care personnel, saying the FDA would not “create obstacles” to authorizing tools that could be useful. Johnson & Johnson seeks emergency FDA authorization for single-shot coronavirus vaccine |Carolyn Y. Johnson, Laurie McGinley |February 5, 2021 |Washington Post 

Not actual CIA agents, but U.S. government personnel who have worked very closely with the CIA, and who are fans of the show. ‘Archer’ Creator Adam Reed Spills Season 6 Secrets, From Surreal Plotlines to Life Post-ISIS |Marlow Stern |January 8, 2015 |DAILY BEAST 

To the Republic of Korea and United States military personnel stationed in the JSA, it is known as Propaganda Village. Propaganda, Protest, and Poisonous Vipers: The Cinema War in Korea |Rich Goldstein |December 30, 2014 |DAILY BEAST 

Neither individual would go on the record due to the sensitivity of discussing personnel matters. Vaclav Klaus, Libertarian Hero, Has His Wings Clipped by Cato Institute |James Kirchick |December 22, 2014 |DAILY BEAST 

Add to that the per diems paid for the 105 military commission personnel who attend the proceedings, which cost more than $16,000. Prosecutors Have No Idea When 9/11 Mastermind’s Trial Will Start |Tim Mak |December 17, 2014 |DAILY BEAST 

He consistently met the requirements of his job, according to personnel records. Two Texas Regulators Tried to Enforce the Rules. They Were Fired. |David Hasemyer, InsideClimate News |December 9, 2014 |DAILY BEAST 

The state and local group in Virginia is nearly equally divided between school and non-school personnel. Hallowed Heritage: The Life of Virginia |Dorothy M. Torpey 

However, when civilian government and military personnel are combined, government employment surpasses manufacturing employment. Hallowed Heritage: The Life of Virginia |Dorothy M. Torpey 

The number of military personnel in the state is slightly more than twice the number of federal civilian employees. Hallowed Heritage: The Life of Virginia |Dorothy M. Torpey 

During the war both the printing and publishing businesses suffered from shortage of personnel, of metal, and of paper. A History of the Cambridge University Press |S. C. Roberts 

As to personnel, I ask for young and energetic commanders, Byng and Rawlinson, and am turned down. Gallipoli Diary, Volume 2 |Ian Hamilton