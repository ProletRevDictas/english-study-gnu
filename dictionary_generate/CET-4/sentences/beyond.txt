There is a long history of official anti-clericalism in Mexico, but the atmosphere in Tierra Caliente goes far beyond that. Mexico’s Priests Are Marked for Murder |Jason McGahan |January 7, 2015 |DAILY BEAST 

There is a larger reason, beyond the airlines themselves, why Lion Air and 61 other Indonesian airlines are on this black list. Annoying Airport Delays Might Prevent You From Becoming the Next AirAsia 8501 |Clive Irving |January 6, 2015 |DAILY BEAST 

Beyond the huge American flag that hung over the street, the mile-long mass of cops ended. Funeral Protest Is Too Much for NYPD Union Boss |Michael Daly |January 5, 2015 |DAILY BEAST 

They liked what Duke was saying and were willing to look beyond what little they knew of his past. The Louisiana Racists Who Courted Steve Scalise |Jason Berry |January 3, 2015 |DAILY BEAST 

Bottom line is that it will only be a BVR [beyond visual range] airplane. New U.S. Stealth Jet Can’t Fire Its Gun Until 2019 |Dave Majumdar |December 31, 2014 |DAILY BEAST 

It is only just to say, that the officers exhibited a degree of courage far beyond any thing we had expected from them. Blackwood's Edinburgh Magazine, No. CCCXXXIX. January, 1844. Vol. LV. |Various 

To talk German was beyond the sweep of my dizziest ambition, but an Italian runner or porter instantly presented himself. Glances at Europe |Horace Greeley 

Water itself is of course essential to the growth of every plant, but the benefits of Irrigation reach far beyond this. Glances at Europe |Horace Greeley 

The quality of artistic beauty in articulation is very important, beyond the mere accuracy which is ordinarily thought of. Expressive Voice Culture |Jessie Eldridge Southwick 

In a minute Bruce was back with his hat full of water from the creek that whimpered just beyond the willow patch. Raw Gold |Bertrand W. Sinclair