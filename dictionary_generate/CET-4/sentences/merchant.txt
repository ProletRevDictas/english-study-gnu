Over 40 million buyers now regularly use Shop Pay at these merchants and others on Shopify’s platform to complete their purchases. Shopify expands its payment option, Shop Pay, to its merchants on Facebook and Instagram |Sarah Perez |February 9, 2021 |TechCrunch 

Several of these firms also offer a range of payments services for merchants. PayPal is shutting down domestic payments business in India |Manish Singh |February 5, 2021 |TechCrunch 

The company provides a marketplace where third-party merchants have the opportunity to reach customers they might not otherwise have had, Bezos said, and Amazon’s success is built on the ability of those sellers to thrive, too. Amazon CEO Jeff Bezos‘s successor will inherit his challenges |Jay Greene, Cat Zakrzewski |February 4, 2021 |Washington Post 

The purpose was to not mess up your conversion metrics, but rather to have “automated systems to ensure consumers are getting accurate pricing information from our merchants.” Google will suspend merchant sites that show higher prices at checkout |Barry Schwartz |February 4, 2021 |Search Engine Land 

Like Uber Eats, Drizly teams with local merchants in the markets it services. Uber is buying alcohol delivery service Drizly for $1.1B |Brian Heater |February 2, 2021 |TechCrunch 

The Daily Beast spoke to Merchant about the film, dating adventures, and the state of American television. Stephen Merchant Talks ‘Hello Ladies’ movie, the Nicole Kidman Cameo, and Legacy of ‘The Office’ |Marlow Stern |November 22, 2014 |DAILY BEAST 

His father, Hassan, is a carpet merchant with close relations to senior members of the conservative Islamic Coalition Party. Iran’s Blogfather Walks Free After Six Years in Jail |IranWire |November 21, 2014 |DAILY BEAST 

He has a voice not dissimilar in timbre and penetrative ability to the incredibly annoying comedian Stephen Merchant. The Addictive Curse of ‘Let’s Plays’ |Tom Sykes |November 11, 2014 |DAILY BEAST 

She could have auditioned to be the tavern wench or a faerie; instead, she signed on as a merchant, knitting chain-mail bikinis. Best Career Arc Ever: From Burlesque To Bartending |Anne Berry |September 13, 2014 |DAILY BEAST 

Brecht placed his merchant-mother in a dark universe of impossible choices. Brecht's Mercenary Mother Courage Turns 75 |Katie Baker |September 10, 2014 |DAILY BEAST 

They will reach you by the hands of Mr. Mackenzie, a worldly-minded Scotch merchant, but honest as to earthly things. The Pit Town Coronet, Volume I (of 3) |Charles James Wills 

Alila has a cousin married to a Chinese merchant in Manila and some time he is going to visit her. Alila, Our Little Philippine Cousin |Mary Hazelton Wade 

Harris, the silk merchant, stood among these broken and burnt stones and shivered. Three More John Silence Stories |Algernon Blackwood 

Departure on the fourth voyage, accompanied by a merchant-ship bound through Torres Strait. Narrative of a Survey of the Intertropical and Western Coasts of Australia] [Volume 2 of 2] |Phillip Parker King 

A young Englishman, a wine merchant, accompanied us in our journey through this sultry valley and was our cicerone. Fifty Years of Railway Life in England, Scotland and Ireland |Joseph Tatlow