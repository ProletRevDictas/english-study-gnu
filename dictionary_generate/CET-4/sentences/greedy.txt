In an interview you did with 60 Minutes around this time, you write that Lesley Stahl had said that most Americans see big corporations as greedy and selfish. Jeff Immelt Knows He Let You Down (Ep. 452) |Stephen J. Dubner |February 18, 2021 |Freakonomics 

We enjoyed the spread by ourselves, surrounded by our two perpetually greedy hounds and warmed by the thought that, with the help of friends and family and ungodly amounts of Tylenol, we had escaped the worst of this thing. As a food writer with covid, I worried I’d lose my sense of taste. It turned out to be much worse. |Tim Carman |November 29, 2020 |Washington Post 

It’s seen as a bunch of selfish greedy bastards that are just in it to make as much money as possible. Whole Foods’ CEO on how business can be better than just “a bunch of selfish greedy bastards” |McKenna Moore |October 6, 2020 |Fortune 

It all depends on your management style and what you’re happy with and how greedy you really want to be. ‘We’re racing against the clock’: The CEO of the Serum Institute of India on his company’s COVID-19 vaccine campaign |Erika Fry |September 21, 2020 |Fortune 

When stocks take a beating, the pros often jump back in and buy on the cheap, adapting that Warren Buffett-ism of being “greedy when others are fearful.” Buffett’s big bet on Japan sends global stocks higher |Bernhard Warner |August 31, 2020 |Fortune 

The answer may be that animals are greedy: they need a lot of oxygen to grow big and complicated. Why Did It Take So Long For Complex Life To Evolve On Earth? Blame Oxygen. |Matthew R. Francis |November 2, 2014 |DAILY BEAST 

Only the greedy say that greed is good and there is no shaming of the shameless. Too Big to Jail: Confessions of a Goldman Sachs Brat |Michael Daly |June 26, 2014 |DAILY BEAST 

But this soft and greedy subversive organelle is no match for the brilliance of our scientists! The Fake Superbug Cure |Kent Sepkowitz |June 21, 2014 |DAILY BEAST 

We must arrest and return their fugitive slaves with greedy pleasure. From Las Vegas to Georgia, the NRA Has Created a Monster |Cliff Schecter |June 9, 2014 |DAILY BEAST 

The  Silicon Valley tech firms tend to be  every bit as cutthroat and greedy as any capitalist enterprise before it. Silicon Valley’s Giants Are Just Gilded Age Tycoons in Techno-Utopian Clothes |Joel Kotkin |April 25, 2014 |DAILY BEAST 

The mother played her accompaniments and at the same time watched her daughter with greedy admiration and nervous apprehension. The Awakening and Selected Short Stories |Kate Chopin 

Whenever he visited the garden he told everybody that he should never come there again because Grandfather Mole was too greedy. The Tale of Grandfather Mole |Arthur Scott Bailey 

Though this may be accounted for by the fact that he was a greedy reader—of any and every thing which came his way. Dorothy at Skyrie |Evelyn Raymond 

Madame Malmaison knew it also, but the hard look on her greedy face did not soften. The Weight of the Crown |Fred M. White 

With a mind greedy after all manner of information, he had not omitted to inquire closely into ecclesiastical matters. The English Church in the Eighteenth Century |Charles J. Abbey and John H. Overton