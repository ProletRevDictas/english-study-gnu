You might think that the praise for So’s work springs from grief over his dying so young. ‘Afterparties’ is the book of the summer |Kathi Wolfe |August 13, 2021 |Washington Blade 

In fact, even if you do get infected after vaccination, your risks of getting seriously ill, needing to go in the hospital, needing to go in the intensive care unit or dying are reduced even further. Still Unsure About Getting The COVID-19 Vaccine? Start Here. |Kaleigh Rogers |August 11, 2021 |FiveThirtyEight 

It’s a reminder that while most of those who’ve died of the virus have been old, being old is not a prerequisite for dying of the virus. You might not want to base key medical decisions on Marjorie Taylor Greene’s advice |Philip Bump |July 9, 2021 |Washington Post 

In the final stage of this “demographic transition,” Thompson proposed, the fertility rate would settle at just about replacement level with one baby born for every adult dying. How Low Can America’s Birth Rate Go Before It’s A Problem? |Stephanie H. Murray |June 9, 2021 |FiveThirtyEight 

Retiring or dying would still dilute the impact of a justice’s side on the court, but it wouldn’t be so pivotal. 4 ideas for Supreme Court reform |Aaron Blake |April 15, 2021 |Washington Post 

I am fortunate that I have never been deathly ill, but whenever I have the stomach flu, I most certainly feel like I am dying. Why My Norovirus Panic Makes Me Sick |Lizzie Crocker |January 5, 2015 |DAILY BEAST 

Gillingham tells Mary that he wants to make their lives simpler, but it sounds a little like the dying of the light. What Downton’s Fashion Really Means |Katie Baker |January 2, 2015 |DAILY BEAST 

Mills was lying on the sidewalk, dying, right in front of people trained to save him. Red Tape Is Strangling Good Samaritans |Philip K. Howard |December 27, 2014 |DAILY BEAST 

“In a country that once fed the world, children were dying of malnourishment,” writes Ivereigh. How Pope Francis Became the World’s BFF |Jason Berry |December 21, 2014 |DAILY BEAST 

Her son peeked out the window and told me his mother had left Havana for La Lisa to visit a dying relative. The Life and Hard Times Of The Family A Cuban Defector Left Behind |Brin-Jonathan Butler |December 19, 2014 |DAILY BEAST 

You see, I am the city undertaker, and the people are dying here so fast, that I can hardly supply the demand for coffins. The Book of Anecdotes and Budget of Fun; |Various 

The Alcalde was kneeling by his side, gazing sadly and earnestly into the face of the dying man. Blackwood's Edinburgh Magazine, No. CCCXXXIX. January, 1844. Vol. LV. |Various 

The farewell and the mourning are finished by the slaughter of dogs, that the dying man may have forerunners in the other world. The Jesuit Relations and Allied Documents, Vol. II: Acadia, 1612-1614 |Various 

As the music wailed its dying cadences into this fateful silence, Tom met her eyes across the room. The Wave |Algernon Blackwood 

The wish to go to heaven without dying is, as I know, a motive derived from child-life. Children's Ways |James Sully