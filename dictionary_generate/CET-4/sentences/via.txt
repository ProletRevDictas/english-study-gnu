Isolated lesbians learned that there were other women like them via books whose covers aimed to titillate heterosexual men. How Pulp Fiction Saved Literature |Wendy Smith |January 8, 2015 |DAILY BEAST 

As it currently stands, the Via Dolorosa follows the account given in the Gospel of John. Oops! Jesus’ Last Steps Are in the Wrong Place |Candida Moss |January 6, 2015 |DAILY BEAST 

The Via Dolorosa ends at the Church of the Holy Sepulchre, and is marked by nine stations of the cross. Oops! Jesus’ Last Steps Are in the Wrong Place |Candida Moss |January 6, 2015 |DAILY BEAST 

Back in December, and just in time for Hanukkah, J.K. Rowling revealed via Twitter that there were Jewish wizards at Hogwarts. Harry Potter and the Torah of Terror |Candida Moss, Joel Baden |January 4, 2015 |DAILY BEAST 

The police suspect that the other unaccounted for 643,000 bitcoins, were removed from customer accounts via an unknown party. Japanese Bitcoin Heist ‘an Inside Job,’ Not Hackers Alone |Nathalie-Kyoko Stucky, Jake Adelstein |January 1, 2015 |DAILY BEAST 

And next day there was one more revealing incident that helped, yet also hindered him, as he moved along his via dolorosa. The Wave |Algernon Blackwood 

Time is an object, but if Greece came in, preferably via Enos, the problem would be simplified. Gallipoli Diary, Volume I |Ian Hamilton 

Jouett then safely led the Governor's party via a secluded road to Staunton, which became another temporary capital. Hallowed Heritage: The Life of Virginia |Dorothy M. Torpey 

The General Assembly encouraged the establishment of classical schools and academies via revenue secured from lotteries. Hallowed Heritage: The Life of Virginia |Dorothy M. Torpey 

Via, says Rex, meaning the road; communis is common; omnibus to all, meaning thereby—but perchance I weary you? First Plays |A. A. Milne