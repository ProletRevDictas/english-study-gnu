Working through WhatsApp rumor mills, these groups lay siege to highways, stop vans transporting cows to cattle fairs and slaughterhouses — beef is seen as holy by many Hindus — and assault the Muslims and Dalits in those vehicles. American Fringes: How the Media Makes it Worse |Nick Fouriezos |September 7, 2020 |Ozy 

As long as we’re embodied beings, processing the world through our senses, there will be no holier grail than making sure that what those senses register is pleasurable. Solarpunk Is Growing a Gorgeous New World in the Cracks of the Old One |Carin Ism |September 6, 2020 |Singularity Hub 

I began a journey on which I met with holy men and documented varying aspects of my culture. Lafalaise Dion Takes ESSENCE On A Personal Journey |Nandi Howard |September 4, 2020 |Essence.com 

The holy grail of tests may be one that is fast, easy, accurate and inexpensive and that could be used broadly — even by people at home. New coronavirus tests promise to be faster, cheaper and easier |Jack J. Lee |August 31, 2020 |Science News 

The holy grail of front camera design is the under-display front camera. Xiaomi’s “third-generation” under-display camera looks nearly invisible |Ron Amadeo |August 28, 2020 |Ars Technica 

The Via Dolorosa ends at the Church of the Holy Sepulchre, and is marked by nine stations of the cross. Oops! Jesus’ Last Steps Are in the Wrong Place |Candida Moss |January 6, 2015 |DAILY BEAST 

“You were going to work your way into my marriage and you were going to call its new three-way shape holy” the narrator recounts. A Novel Nearly Impossible to Review |Nicholas Mancusi |December 28, 2014 |DAILY BEAST 

Because holy hell was that bland, unfunny, uncomfortable, and just plain confusing. The Biggest Bombs of 2014: ‘Sex Tape,’ Mariah Carey’s Vocals, ‘How I Met Your Mother’ and More |Kevin Fallon |December 19, 2014 |DAILY BEAST 

And he was indicted in Israel last week on charges he plotted to blow up sites holy to Islam. The Strange Case of the Christian Zionist Terrorist |Creede Newton |December 14, 2014 |DAILY BEAST 

Israelis often are amused and appalled by the crazies attracted to the Holy Land, and not only for religious reasons. The Strange Case of the Christian Zionist Terrorist |Creede Newton |December 14, 2014 |DAILY BEAST 

After his death crowds flocked to his grave to touch his holy monument, till the authorities caused the church yard to be shut. The Every Day Book of History and Chronology |Joel Munsell 

His holy book says: There is more joy over one sinner that repenteth than over ninety and nine just men. Blackwood's Edinburgh Magazine, No. CCCXXXIX. January, 1844. Vol. LV. |Various 

Their sin began on Holy Thursday, with so little secrecy and so bad an example, that the affair was beginning to leak out. The Philippine Islands, 1493-1898, Volume XX, 1621-1624 |Various 

All these things shall be for good to the holy, so to the sinners and the ungodly they shall be turned into evil. The Bible, Douay-Rheims Version |Various 

The spread of the holy gospel and uninterrupted preaching went on until the return of the ambassador. The Philippine Islands, 1493-1898, Volume XX, 1621-1624 |Various