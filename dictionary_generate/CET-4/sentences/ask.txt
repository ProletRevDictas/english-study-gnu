As a best practice, position your ask as something that will benefit their readers. Six strategies guaranteed to help you build more backlinks to your website |Izabelle Hundrev |December 10, 2020 |Search Engine Watch 

This ask comes as Senate Majority Leader Mitch McConnell is reportedly “softening” to the idea of including stimulus checks in the next package, Politico reported Tuesday. A last-ditch push to save stimulus checks is gaining traction |Lance Lambert |December 8, 2020 |Fortune 

It seems that isn't an unreasonable ask, but she should own her preference rather than try to pretend that she is just trying to protect the sister-in-law. Carolyn Hax: To summarize, you want to exclude her because she’s disabled? |Carolyn Hax |October 29, 2020 |Washington Post 

“Little did I know how big of an ask it was but, if you want to be at the center of God’s will, you have to be submissive and you have to put yourself out there and I’m so glad I did,” she said. In Senate rematch, Democrats hope for an Espy upset but remain realistic about Mississippi politics |Sarah Fowler |October 23, 2020 |Washington Post 

It’s a tough ask — especially when your team goes from regulation-sized to spanning the globe. What if Your Company Had No Rules? (Bonus Episode) |Maria Konnikova |September 12, 2020 |Freakonomics 

If ISIS “came into a base and killed hundreds of troops, then people would ask a lot more questions.” Pentagon Doesn’t Know How Many People It’s Killed in the ISIS War |Nancy A. Youssef |January 7, 2015 |DAILY BEAST 

And I need to ask why their truth makes me so defensive, as if my truth is the only truth. In 2015, Let’s Try for More Compassion |Gene Robinson |January 4, 2015 |DAILY BEAST 

One is forced to ask, what on earth was Andrew doing hanging out with scantily clad teenagers? Buckingham Palace Disputes Sex Allegations Against Prince ‘Randy Andy’ |Tom Sykes |January 4, 2015 |DAILY BEAST 

“You ask me my motivation,” Marvin says, moving back into his tough guy persona again. The Story Behind Lee Marvin’s Liberty Valance Smile |Robert Ward |January 3, 2015 |DAILY BEAST 

I ask Atefeh and Monir if they see dancing as a form of income in the future, a potential career. Iran’s Becoming a Footloose Nation as Dance Lessons Spread |IranWire |January 2, 2015 |DAILY BEAST 

"There's just one thing I'd like to ask, if you don't mind," said Cynthia, coming suddenly out of a brown study. The Boarded-Up House |Augusta Huiell Seaman 

That it is a reasonable and proper thing to ask our statesmen and politicians: what is going to happen to the world? The Salvaging Of Civilisation |H. G. (Herbert George) Wells 

I ask for half a dozen projectors or so in every school, and for a well-stocked storehouse of films. The Salvaging Of Civilisation |H. G. (Herbert George) Wells 

For it is better that thy children should ask of thee, than that thou look toward the hands of thy children. The Bible, Douay-Rheims Version |Various 

Finally, let me ask the general reader to put aside all prejudice, and give both sides a fair hearing. God and my Neighbour |Robert Blatchford