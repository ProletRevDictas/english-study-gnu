Coronaviruses like SARS and MERS tend to infect deep in the lungs, so the new coronavirus is probably spread mainly by people with symptoms, such as a cough, or during such medical procedures as being intubated. Here’s what we’ve learned in six months of COVID-19 — and what we still don’t know |Erin Garcia de Jesus |June 30, 2020 |Science News 

Wearing any face covering, including bandanas or neck warmers, could at least partially block the cloud of droplets released in a cough, the experiment showed. Why scientists say wearing masks shouldn’t be controversial |Tina Hesman Saey |June 26, 2020 |Science News 

Two days before the ceremony the bride’s 58-year-old father, who had traveled from Spain, felt feverish and developed a runny nose and cough. COVID-19 case clusters offer lessons and warnings for reopening |Helen Thompson |June 18, 2020 |Science News 

Michalak says that previous audiological research has revealed differences between sick and healthy coughs, but the human ear may not be able to distinguish them. No, you can’t hear the difference between sick and healthy coughs |Jonathan Lambert |June 9, 2020 |Science News 

Indirect contact might occur when an infected person uses their hand to cover a cough or a sneeze, then touches an object. Six foot social-distancing will not always be enough for COVID-19 |Tina Hesman Saey |April 23, 2020 |Science News For Students 

Some cough up preposterous jury awards, while others lay bare the egregious failures of the criminal justice system. How One Lawsuit Shows What’s Wrong With America |James Poulos |October 20, 2014 |DAILY BEAST 

Will went on to say doctors believe a “sneeze or some cough” can spread Ebola. Fact-Checking the Sunday Shows: October 19 |PunditFact.com |October 19, 2014 |DAILY BEAST 

It can spread through a sneeze, cough, sharing a beverage or speaking up close with someone who has the disease. Fact-Checking the Sunday Shows: October 19 |PunditFact.com |October 19, 2014 |DAILY BEAST 

Brosseau said her views had nothing to do with Ebola spreading among the public at large through a sneeze or cough. Fact-Checking the Sunday Shows: October 19 |PunditFact.com |October 19, 2014 |DAILY BEAST 

Get a flu shot, wash your hands, and cover your mouth when you cough. Parents’ Ebola Panic Is Taking Over My Clinic |Russell Saunders |October 15, 2014 |DAILY BEAST 

When the quantity is very small there may be no cough, the sputum reaching the larynx by action of the bronchial cilia. A Manual of Clinical Diagnosis |James Campbell Todd 

It is certain that I then had a bad cough nearly always; and this I am sure was what decided the form of his parting gift to me. Fifty Years of Railway Life in England, Scotland and Ireland |Joseph Tatlow 

They then cough it up and use this material they have so oddly prepared in making their nests. Alila, Our Little Philippine Cousin |Mary Hazelton Wade 

If, now, the patient cough or strain as if at stool, the contents of the stomach will usually be forced out through the tube. A Manual of Clinical Diagnosis |James Campbell Todd 

When I shifted my position, he turned the other way quick, and coughed–that pore little gone-in cough of hisn. Alec Lloyd, Cowpuncher |Eleanor Gates