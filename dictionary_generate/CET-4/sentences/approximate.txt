In the absence of live interactions, we must do our best as a society to approximate them. Miss Manners: Practice saying ‘I’ve got it, thanks’ |Judith Martin, Nicholas Martin, Jacobina Martin |February 12, 2021 |Washington Post 

Of the approximate 1,800 fatalities from mid-May to mid-June, 70 percent were people older than 70. 900,000 infected. Nearly 15,000 dead. How the coronavirus tore through D.C., Maryland and Virginia. |Rebecca Tan, Antonio Olivo, John D. Harden |February 5, 2021 |Washington Post 

Some brands pack on features that allow users to more closely approximate the cinematic experience, but if you’re in the HD range, you’re already on your way. Best 2 in 1 laptops: Work hard, play hard with these versatile picks |PopSci Commerce Team |January 27, 2021 |Popular-Science 

I would never have figured out on my own that Braun-Meuser-Case was supposed to approximate “brand-new suitcase.” Style Conversational Week 1419: A smile for the Capitol? |Pat Myers |January 14, 2021 |Washington Post 

Solver Alex Vornsand approximated the size of this region by picking random points in the circle and seeing how many would have resulted in a balanced disk. Can You Cut The Square … Into More Squares? |Zach Wissner-Gross |January 8, 2021 |FiveThirtyEight 

Total approximate retail value of all prizes awarded is $ 3899.97. The Daily Beast Company LLC The New Alphas Sweepstakes Official Rules | |December 9, 2014 |DAILY BEAST 

By 2018, Chinese wealth is expected to approximate the level of US wealth in 1993. World Will Be 40 Percent Richer By 2018 |Credit Suisse |December 20, 2013 |DAILY BEAST 

I figure out the approximate outline, the sequence of subject matter for the chapter, numbering the material. How I Write: Jared Diamond |Noah Charney |November 20, 2013 |DAILY BEAST 

And nobody had reported a missing child of that approximate age. Baby Hope Killer Confesses After 22 Years |Michael Daly |October 13, 2013 |DAILY BEAST 

The red line is the approximate break-even price of the average tar sands producer. The Quixotic Crusade Against the Keystone Pipeline |Megan McArdle |March 1, 2013 |DAILY BEAST 

For accuracy, 500 to 1000 leukocytes must be classified; for approximate results, 200 are sufficient. A Manual of Clinical Diagnosis |James Campbell Todd 

His skin was of so light a yellow color as to approximate to dirty white, and his face was pock-marked from neck to crown. Dope |Sax Rohmer 

They are for the most part straight, and intersect each other at approximate right angles. The Catacombs of Rome |William Henry Withrow 

It is impossible to arrive at even an approximate estimate of the number of victims of the early persecutions. The Catacombs of Rome |William Henry Withrow 

The approximate date is probably about the middle of the fifteenth century. Bell's Cathedrals: A Short Account of Romsey Abbey |Thomas Perkins