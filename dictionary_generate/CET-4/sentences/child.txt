Once a child has one built, they can use an iOS or Android app and program actions via Google’s Blockly coding software. These three robots can teach kids how to code |John Kennedy |September 17, 2020 |Popular-Science 

The Norwegian government provides a big basket of universal benefits, including health care, free college at public universities, parental leave, child care, unemployment insurance, and more. Does Anyone Really Know What Socialism Is? (Ep. 408 Rebroadcast) |Stephen J. Dubner |September 17, 2020 |Freakonomics 

Apple added a “family setup” feature, which lets people link multiple Apple Watches to a single iPhone—a useful option for people with children who may not own a device of their own. Everything announced at Apple’s ‘Time Flies’ event today |rhhackettfortune |September 15, 2020 |Fortune 

The challenge has been compounded in recent months by an increase in the number of children who have entered state care just before or at the time they are cleared for discharge, he said. Still No Answers to Lawmakers’ Questions About Children Stuck in Psychiatric Hospitals |by Duaa Eldeib |September 15, 2020 |ProPublica 

Through its Education Champion Network, the nonprofit organization is investing in initiatives like the Orenda Project, an app that enables digital learning for school-aged children across Pakistan, developed by entrepreneur Haroon Yasin. Malala Yousafzai puts the remote learning struggle in perspective |Michal Lev-Ram, writer |September 15, 2020 |Fortune 

Sands was involved in a scandalous-for-the-time romance with the carpenter and there were rumors she was pregnant with his child. New York’s Most Tragic Ghost Loves Minimalist Swedish Fashion |Nina Strochlic |January 8, 2015 |DAILY BEAST 

In Sweden parents can use those days up until the child turns 12. How Good Dads Can Change the World |Gary Barker, PhD, Michael Kaufman |January 6, 2015 |DAILY BEAST 

The Great Society is a place where every child can find knowledge to enrich his mind and to enlarge his talents. Thank Congress, Not LBJ for Great Society |Julian Zelizer, Scott Porch |January 4, 2015 |DAILY BEAST 

It needs to be said: bigotry in the name of religion is still bigotry; child abuse wrapped in a Bible verse is still child abuse. Dear Leelah, We Will Fight On For You: A Letter to a Dead Trans Teen |Parker Molloy |January 1, 2015 |DAILY BEAST 

At least one child in CAR has been killed or gravely injured per day, and 10,000 have been recruited into militant groups. The Year’s Most Forgotten Humanitarian Crisis |Nina Strochlic |January 1, 2015 |DAILY BEAST 

You would not think it too much to set the whole province in flames so that you could have your way with this wretched child. St. Martin's Summer |Rafael Sabatini 

He shrank, as from some one who inflicted pain as a child, unwittingly, to see what the effect would be. The Wave |Algernon Blackwood 

This is one of the most striking manifestations of the better side of child-nature and deserves a chapter to itself. Children's Ways |James Sully 

The mother's lips could not finish the charge she was about to put upon her innocent child. The Pastor's Fire-side Vol. 3 of 4 |Jane Porter 

In Luke it is said, “And the child grew, and waxed strong in spirit, filled with wisdom.” Solomon and Solomonic Literature |Moncure Daniel Conway