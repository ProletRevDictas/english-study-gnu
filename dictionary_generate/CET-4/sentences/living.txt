His prototype for Goat is Alto Pharmacy, a booming digital health unicorn today that the founders started in his living room. With Goat Capital, Justin Kan and Robin Chan want to keep founding alongside the right teams |Eric Eldon |September 17, 2020 |TechCrunch 

It didn’t take long for the Wrecking Ball of Consequence to come swinging into the Houston Rockets’ living room. Everything Should Be On The Table For The Houston Rockets. Even James Harden’s Future. |Chris Herring (chris.herring@fivethirtyeight.com) |September 14, 2020 |FiveThirtyEight 

This ensures your CTV ads are delivering an experience that is traditionally only found in the living room, watching network television. 5 tips for adding connected TV to your holiday ad strategy |Sponsored Content: SteelHouse |September 14, 2020 |Search Engine Land 

As much as Netflix may be a tree in spirit, in reality, it’s not a living, interconnected organism. What if Your Company Had No Rules? (Bonus Episode) |Maria Konnikova |September 12, 2020 |Freakonomics 

We could raise the federal minimum wage, which hasn’t gone up in over 11 years, or incentivize businesses to pay their employees a living wage. How we can save small business from coronavirus-induced extinction |matthewheimer |September 10, 2020 |Fortune 

Patrick Klugman, the deputy mayor of Paris, said: “We are living our kind of 9/11,” he said. France Kills Charlie Hebdo Murderers |Nico Hines |January 9, 2015 |DAILY BEAST 

Last week I turned 40, a bittersweet occasion because I crossed the line to living longer without my mother than with her. Everyone at This Dinner Party Has Lost Someone |Samantha Levine |January 6, 2015 |DAILY BEAST 

But as an American creating a new brand here, and living the daily life of the souk, he seems to be in a league of his own. The Photographer Who Gave Up Manhattan for Marrakech |Liza Foreman |January 6, 2015 |DAILY BEAST 

A single father, he had been living abroad and returned when his mother was diagnosed with cancer. Everyone at This Dinner Party Has Lost Someone |Samantha Levine |January 6, 2015 |DAILY BEAST 

For those living in poor communities in particular, interactions with police rarely come with good news and a smile. How to Solve the Policing Crisis |Keli Goff |January 5, 2015 |DAILY BEAST 

To be so humbled in the knowledge of any living being, was the vultures of Prometheus to the proud heart of Ripperda. The Pastor's Fire-side Vol. 3 of 4 |Jane Porter 

The living (value £250) is in the gift of trustees, and is now held by the Rev. M. Parker, Vicar. Showell's Dictionary of Birmingham |Thomas T. Harman and Walter Showell 

So far as their thought is still alive these men will come into the discussion of living questions now. The Salvaging Of Civilisation |H. G. (Herbert George) Wells 

If they are still Moderns and alive, I defy you to bury them if you are discussing living questions in a full and honest way. The Salvaging Of Civilisation |H. G. (Herbert George) Wells 

Either they are unavoidable if your living questions are fully discussed, or they are irrelevant and they do not matter. The Salvaging Of Civilisation |H. G. (Herbert George) Wells