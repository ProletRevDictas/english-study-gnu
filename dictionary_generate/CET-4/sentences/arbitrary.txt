A skeptical person will object that the definition of lift as a force acting at a right angle to the airstream is arbitrary. The science behind how an aircraft glides |By Peter Garrison/Flying Mag |September 3, 2020 |Popular-Science 

The 1,000 jobs lost due to arbitrary border restrictions are essential to the 1,000 families that somehow depended on their wages. Border Report: The Lingering Trauma of Family Separation |Maya Srikrishnan |August 31, 2020 |Voice of San Diego 

I looked at that and I say that seems quite arbitrary to me, because you can get coronavirus any time of the day. Full Transcript: Tomi Lahren on ‘The Carlos Watson Show’ |Daniel Malloy |August 31, 2020 |Ozy 

In March, many initially set an arbitrary reopening date of Labor Day 2020. I’m a physician and a CEO. Why I won’t bring my employees back to the office before Labor Day 2021 |matthewheimer |August 26, 2020 |Fortune 

First, a principle of quantum mechanics called the no-cloning theorem says that it’s impossible to copy an arbitrary quantum state, so qubits can’t be duplicated. To live up to the hype, quantum computers must repair their error problems |Emily Conover |June 22, 2020 |Science News 

The truth is that Judd is really just picking an arbitrary number since there is no script. Inside Sony’s ‘Pineapple Express 2 Drama’: Leaked Emails Reveal Fight Over Stoner Comedy Sequel |William Boot |December 21, 2014 |DAILY BEAST 

The problem is that those restrictions—like jumping from a 24- to a 72-hour waiting period—seem fairly arbitrary. Abortion in Missouri Is the Wait of a Lifetime |Justin Glawe |November 12, 2014 |DAILY BEAST 

Pinker is not a self-appointed enforcer of arbitrary rules, and he has little patience for purists, prigs, and pedants. Go Ahead, End With a Preposition: Grammar Rules We All Can Live With |Nick Romeo |November 3, 2014 |DAILY BEAST 

Like Silver, I question the seemingly arbitrary cutoffs and weighting of the data Wang uses in his model. Why Is Nate Silver So Afraid of Sam Wang? |Daniel Altman |October 6, 2014 |DAILY BEAST 

Time and time again, we see women being asked to ace some arbitrary test in order to be deemed model victims. Why We're So Hard on Janay Rice and Celebrity Survivors of Abuse |Amy Zimmerman |September 15, 2014 |DAILY BEAST 

The great dyke which kept out arbitrary power had been broken. The History of England from the Accession of James II. |Thomas Babington Macaulay 

Wherever there is arbitrary rule, there must be necessity, on the part of the dominant classes, superiority be assumed. The Condition, Elevation, Emigration, and Destiny of the Colored People of the United States |Martin R. Delany 

However arbitrary, there are certain policies that regulate all well organized institutions and corporate bodies. The Condition, Elevation, Emigration, and Destiny of the Colored People of the United States |Martin R. Delany 

This, Williston says, is an arbitrary refusal of the court to enforce the contract that the parties made and seems unwarranted. Putnam's Handy Law Book for the Layman |Albert Sidney Bolles 

Depend upon it you will always be properly opposed in such arbitrary measures. Private Letters of Edward Gibbon (1753-1794) Volume 1 (of 2) |Edward Gibbon