It includes nothing about where, how many people it involved, if they were operating outdoors or complying with other guidance. Morning Report: A Century of Single Family Home Supremacy |Voice of San Diego |August 27, 2020 |Voice of San Diego 

Walking your dog seems simple enough—it’s just you, your pup, and the great outdoors. The right way to walk your dog |John Kennedy |August 26, 2020 |Popular-Science 

Online sales for sports and outdoors, kitchen, and especially groceries all saw increases as lockdowns and work-from-home policies led to consumers staying home to cook and exercise. How to prepare your e-commerce ad strategy for an uncertain Q4 |Sponsored Content: Pacvue |August 17, 2020 |Search Engine Land 

You know, that kind of dilution happens to aerosols outdoors. ‘We Have the Power as a Community to Decide’ |Megan Wood |August 7, 2020 |Voice of San Diego 

Just because we’re ordered to stay at home doesn’t mean you can’t go outdoors — especially if you go all the way to the great outdoors. This Weekend: A Sangria Slushie Summer |Fiona Zublin |August 7, 2020 |Ozy 

Neither was old enough to be outdoors with a loaded gun within the city limits. The Stacks: The Judas Priest Teen Suicide Trial |Ivan Solotaroff |June 28, 2014 |DAILY BEAST 

Outdoors, I ran 100m and 200m, and then long-jumped on occasion. ‘Orange Is the New Black’ Star Uzo Aduba on Her Journey From Track Phenom to Crazy Eyes |Marlow Stern |June 11, 2014 |DAILY BEAST 

She compiled a survey for older girls on the outdoors and got almost 2,000 responses in less than two months. Why Are Girl Scout Camps Being Closed? |Alessandra Rafferty |January 12, 2014 |DAILY BEAST 

Like other maskers who have braved the outdoors, he enjoys a turn in the spotlight. The Secret World of Men Who Dress Like Dolls |Nina Strochlic |January 7, 2014 |DAILY BEAST 

But the constant rain and the cold mountain weather usually prevent the students from gathering outdoors to learn. The Afghan Village That Saved Navy SEAL Marcus Luttrell |Sami Yousafzai, Ron Moreau |November 8, 2013 |DAILY BEAST 

Now as he rose to go outdoors with Dorothy he remembered the letter Jim Barlow had brought him. Dorothy at Skyrie |Evelyn Raymond 

Wouldn't hurt the kid a bit—he'd be bigger then, and the outdoors would make him grow like a pig. Cabin Fever |B. M. Bower 

His instincts were all for the great outdoors, and from such the sun brings quick response. Cabin Fever |B. M. Bower 

The last of the logs were glowing red on the hearths, and the air was hot and heavy after the fresh outdoors. The Idyl of Twin Fires |Walter Prichard Eaton 

Yes, her husband thrust her outdoors, half naked, this bitter cold night. The Seven Cardinal Sins: Envy and Indolence |Eugne Sue