This time, the growth is happening even more slowly — we have an even better warning. The new Covid-19 case surge in Europe, explained |Julia Belluz |September 17, 2020 |Vox 

The “wall of smoke” is slowly dispersing, recent images show, and it has now started to move over the rest of the country. West Coast wildfire smoke is visible from outer space |María Paula Rubiano A. |September 16, 2020 |Popular-Science 

As superfast 5G wireless technology slowly rolls out across the country for consumers, Verizon is also planning business applications that might bring in more revenue sooner. Verizon plans to offer indoor 5G networks by year-end |Aaron Pressman |September 16, 2020 |Fortune 

Partial indexing is often connected to a website using JavaScript, but this is slowly changing. How to earn your place in Google’s index in 2020 |Bartosz Góralewicz |September 14, 2020 |Search Engine Land 

It is much easier, I suspect, to imagine that our minds could slowly fill with dehumanizing representations of another group, especially if we are repeatedly bombarded with such representations. Believing in Monsters: David Livingstone Smith on the Subhuman - Facts So Romantic |Eric Schwitzgebel |September 11, 2020 |Nautilus 

Slowly, two were opened up, and in 2010 the regional government opened all four Brogpa villages in a push for tourism. The Himalayas’ Hidden Aryans |Nina Strochlic |January 3, 2015 |DAILY BEAST 

Slowly, slowly, dance classes may cease to be such secret and guilty pleasures in Iran. Iran’s Becoming a Footloose Nation as Dance Lessons Spread |IranWire |January 2, 2015 |DAILY BEAST 

In my eyes she killed those people… exposing a gay person like this is akin to torturing him slowly to death. Sisi Is Persecuting, Prosecuting, and Publicly Shaming Egypt’s Gays |Bel Trew |December 30, 2014 |DAILY BEAST 

Airline pilots are now slowly, too slowly, being given access to flight simulators able to reproduce sudden and unexpected upsets. Did Bad Weather Bring Down AirAsia 8501? |Clive Irving |December 29, 2014 |DAILY BEAST 

“The lies of the government shocked us,” says Fatima, as the tears flow slowly from her eyes and down her cheek. A Sunni-Shia Love Story Imperiled by al Qaeda |Ruth Michaelson |December 26, 2014 |DAILY BEAST 

Sol got up, slowly; took a backward step into the yard; filled his lungs, opened his mouth, made his eyes round. The Bondboy |George W. (George Washington) Ogden 

Again the sallow fingers began to play with the book-covers, passing from one to another, but always slowly and gently. Bella Donna |Robert Hichens 

The lovers got up, with only a silent protest, and walked slowly away somewhere else. The Awakening and Selected Short Stories |Kate Chopin 

He spoke clearly and slowly, well knowing that some among the natives would understand him. The Red Year |Louis Tracy 

The conception of the relation of this institution with them as co-operative makes headway slowly. Readings in Money and Banking |Chester Arthur Phillips