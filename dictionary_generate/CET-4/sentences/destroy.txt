The nova does not destroy the star, unlike a supernova, which marks a star’s death. The number of Milky Way nova explosions per year has been pinned down |Ken Croswell |February 12, 2021 |Science News 

In addition to leveling forests for crops, the Spanish plantation system also began to destroy biodiversity with imported cattle and replace mountains with mines. The problem of environmental racism in Mexico today is rooted in history |Jayson Porter |February 11, 2021 |Washington Post 

The eventual goal is to be able to 3D print bones directly into the cavity of a patient, for scenarios in which a certain portion of bone has been removed or destroyed. This Week’s Awesome Tech Stories From Around the Web (Through February 6) |Singularity Hub Staff |February 6, 2021 |Singularity Hub 

He wants them destroyed lest some accident or malicious act unleash smallpox on the world again. Smallpox used to kill millions of people every year. Here’s how humans beat it. |Kelsey Piper |February 5, 2021 |Vox 

It’s marketing’s mass conservation, with nothing created and nothing destroyed. Celebrities have always done endorsements. Now they sell their fast food orders. |Jameson Rich |February 5, 2021 |Vox 

But at the heart of this “Truther” conspiracy theory is the idea that “someone” wants to destroy Bill Cosby. Phylicia Rashad and the Cult of Cosby Truthers |Stereo Williams |January 8, 2015 |DAILY BEAST 

“The United States had gone to war declaring it must destroy an active weapons of mass destruction program,” the Times reported. Political Memes That Absolutely Must Die in 2015 |Asawin Suebsaeng |January 1, 2015 |DAILY BEAST 

Would he have been careful enough to destroy the odd pieces of jute you've left so messily about? Alfred Hitchcock’s Fade to Black: The Great Director’s Final Days |David Freeman |December 13, 2014 |DAILY BEAST 

Anything less will mean than we are only slightly better as a nation than those who want to destroy us. Why the Muslim World Isn’t Flipping Out Over the CIA Torture Report |Dean Obeidallah |December 12, 2014 |DAILY BEAST 

Instead we must show that America “is different, stronger, and better than those who would destroy us.” Why the Muslim World Isn’t Flipping Out Over the CIA Torture Report |Dean Obeidallah |December 12, 2014 |DAILY BEAST 

O my people, they that call thee blessed, the same deceive thee, and destroy the way of thy steps. The Bible, Douay-Rheims Version |Various 

The cheerful hours of easy labor vary but do not destroy the pursuit of pleasure and of recreation. The Unsolved Riddle of Social Justice |Stephen Leacock 

They destroy ants and spiders and other creeping things, so that Alila's mother never kills them nor drives them away. Alila, Our Little Philippine Cousin |Mary Hazelton Wade 

Thy builders are come: they that destroy thee and make thee waste shall go out of thee. The Bible, Douay-Rheims Version |Various 

Yet the Clarion opposes sweating and tyranny and hypocrisy, and does its best to defeat and to destroy them. God and my Neighbour |Robert Blatchford