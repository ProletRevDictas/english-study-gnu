In movies like Inception, Interstellar, Dunkirk, Memento, and more, he’s been toying with that concept, finding emotion and feeling in the emotionless fabric of the universe. The ancient palindrome that explains Christopher Nolan’s Tenet |Alissa Wilkinson |September 4, 2020 |Vox 

The stories that tend to go viral on Facebook are those that stoke emotion and divisiveness, critics argue. Facebook plans to block users in Australia from sharing news |Claire Zillman, reporter |September 1, 2020 |Fortune 

Rivers and LeBron James had passionately described the emotions the NBA community felt after seeing the video of Blake’s shooting. Athletes, from the NBA to tennis stars, are striking to protest the police shooting of Jacob Blake |kdunn6 |August 27, 2020 |Fortune 

People living in small, relatively isolated communities, such as Himba farmers and herders in southern Africa, often rank facial emotions differently than Westerners do if asked to describe on their own what a facial expression shows, Roberson says. Ancient sculptures hint at universal facial expressions across cultures |Bruce Bower |August 19, 2020 |Science News 

When we see an emotion on the face of another, we feel it ourselves. That chatbot I’ve loved to hate |Tate Ryan-Mosley |August 19, 2020 |MIT Technology Review 

Throughout all the stories of loss and pain with the Chief, there was barely a trace of emotion. The Story Behind Lee Marvin’s Liberty Valance Smile |Robert Ward |January 3, 2015 |DAILY BEAST 

The shared feelings, the bubbling emotion, the awe: she became an experience. Bow Down, Bitches: How Beyoncé Turned an Elevator Brawl Into a Perfect Year |Kevin Fallon |December 31, 2014 |DAILY BEAST 

She suggests mindfulness exercises to help us process the emotion before it triggers a response. Can Self-Help Books Really Make a New You? |Lizzie Crocker |December 29, 2014 |DAILY BEAST 

Even when he opens up, the sentences are wooden, the scenes sucked dry of emotion. The Story of the World’s Greatest Cricket Player |William O’Connor |December 24, 2014 |DAILY BEAST 

He was not a man given to casual affectionate display; the moment was charged with emotion. Alfred Hitchcock’s Fade to Black: The Great Director’s Final Days |David Freeman |December 13, 2014 |DAILY BEAST 

After a minute's pause, while he stood painfully silent, she resumed in great emotion. The Pastor's Fire-side Vol. 3 of 4 |Jane Porter 

But I doubt if he feels any particular emotion himself, when he is piercing you through with his rendering. Music-Study in Germany |Amy Fay 

The medium pitch expresses warmth, emotion, and the heart qualities. Expressive Voice Culture |Jessie Eldridge Southwick 

Her fat red cheeks would quiver with emotion, and be wet with briny tears, over the sorrows of Mr. Trollope's heroines. The Pit Town Coronet, Volume I (of 3) |Charles James Wills 

Even the stern, inflexible commander turned to hide an emotion he would have blushed to betray. Blackwood's Edinburgh Magazine, Volume 60, No. 372, October 1846 |Various