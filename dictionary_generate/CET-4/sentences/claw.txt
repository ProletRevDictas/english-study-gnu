Try not to use a towel for this, as bats’ tiny claws can get caught in the fabric’s loops. How to escort a bat out of your home |John Kennedy |August 12, 2021 |Popular-Science 

At first glance, it appeared to be the scene of a classically gruesome bear mauling—gnashing jaws, torn flesh, crushed bones, the grim outcome of pitting fist against claw. It’s No Fun to Wake a Sleeping Bear |jversteegh |August 11, 2021 |Outside Online 

I curled my hand into a cat’s claw and looked, unsure what I was looking for. I want my daughter to live unapologetically in a world free of gender expectations, including mine |E.J. Levy |May 14, 2021 |Washington Post 

Some have killer claws or jaws, some have camouflage, some taste bad or spew poison. What is Brood X? When do cicadas come out in 2021? Answering your buggiest questions. |Bonnie Berkowitz, Artur Galocha |April 1, 2021 |Washington Post 

It’s apparent in the claw back from a 4-9 start to conference play. Mark Turgeon’s Terrapins might not be great, but they sure are gritty |Barry Svrluga |March 11, 2021 |Washington Post 

My sisters Rosa and Liz called it “the claw,” lovingly at times, and at other times I was not so sure. ‘Tracing the Blue Light’: Read Chapter 1 of Eileen Cronin’s ‘Mermaid’ |Eileen Cronin |April 8, 2014 |DAILY BEAST 

The dragon, now dangling by a claw from the edge of the sand tray, is on the cusp of defeat. The Rise of Superhero Therapy: Comic Books as Psychological Treatment |Alex Suskind |February 17, 2014 |DAILY BEAST 

My personal favorite—by which I mean least favorite—is the still-underrated phenomenon of selfie claw hand. When Not to Take a Selfie |James Poulos |December 11, 2013 |DAILY BEAST 

Are we looking at some sort of formal discrimination – a Jim Claw system? Less is Moo: The Genius of Gary Larson |Tom Doran |March 22, 2013 |DAILY BEAST 

Yes, the agency did try to claw back some of the money it gave out after Katrina, Rita, and Wilma, reports Michael Moynihan. From Katrina To Sandy, FEMA Rumors and Failures Keep Swirling |Michael Moynihan |November 9, 2012 |DAILY BEAST 

In the middle of his singing he felt the cold touch of the Crab's claw on the apple of his throat. Kipling Stories and Poems Every Child Should Know, Book II |Rudyard Kipling 

The woman holds in her claw-like hand a half-empty bottle of cheap red wine. The Real Latin Quarter |F. Berkeley Smith 

She laughed still more when the parrot took a piece of cake in his claw, and ate it, bit by bit, as nicely as she could herself. The Nursery, December 1881, Vol. XXX |Various 

Two of the fingers, or the thumb and finger had been enlarged or grafted into a bone-like semblance of a crab's claw. Valley of the Croen |Lee Tarbell 

A dying man raised one horrible crab claw to me, called out my name! Valley of the Croen |Lee Tarbell