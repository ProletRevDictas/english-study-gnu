Our summers are longer which means that conditions are hotter, they’re drier, and that makes us more susceptible to wildfires. “Unprecedented”: What’s behind the California, Oregon, and Washington wildfires |Umair Irfan |September 11, 2020 |Vox 

Nikola did not address Fortune’s questions about Hindenburg’s specific claims, including questions about whether the truck shown in the 2018 video was moving under its own power, or who Nikola’s battery development partners were by summer 2020. New report claims widespread deception by Nikola Motor and founder Trevor Milton |dzanemorris |September 10, 2020 |Fortune 

Labor Day weekend, the last official weekend of summer, isn’t typically the biggest movie weekend of the year. This is the most important movie weekend of the year |Alissa Wilkinson |September 4, 2020 |Vox 

U-pick typically accounts for 60 percent of business for the 28-year-old farm, and the summer high season has been going well. Pick Your Poison |Nick Mancall-Bitel |September 3, 2020 |Eater 

In the summer of 2019, suppliers were preparing to make components for as many as 75 million handsets. Apple is prepping 75 million 5G iPhones for later this year |radmarya |September 1, 2020 |Fortune 

I actually downloaded the app last summer and was embarrassed because none of my friends seemed to use it. My Week on Jewish Tinder |Emily Shire |January 5, 2015 |DAILY BEAST 

Pitchfork called him a “a rap-obsessed misfit from a summer camp who freestyles poorly” who is “ridiculous without knowing it.” The Cult of Yung Lean: ‘I’m Building An Anarchistic Society From the Ground Up’ |Marlow Stern |January 4, 2015 |DAILY BEAST 

Last summer, I spoke with first black supermodel Beverly Johnson about this for The Root. One Vogue Cover Doesn’t Solve Fashion’s Big Race Problem |Danielle Belton |January 2, 2015 |DAILY BEAST 

Last summer, Louisiana also banned non-legal adoption, with offenders facing a penalty of $5,000 and up to five years in prison. Judge: Rehoming Kids Is Trafficking |Tina Traster |December 30, 2014 |DAILY BEAST 

Miller traces his irreverent and subversive streak to a psychedelic experience during the particularly sweltering summer of 1991. DJ Spooky Wants You To Question Everything You Know About Music, Technology, and Philosophy |Oliver Jones |December 27, 2014 |DAILY BEAST 

Edna did not reveal so much as all this to Madame Ratignolle that summer day when they sat with faces turned to the sea. The Awakening and Selected Short Stories |Kate Chopin 

And this summer it seemed to her that she never would be able to take proper care of her nestful of children. The Tale of Grandfather Mole |Arthur Scott Bailey 

The year before they had spent part of the summer with their grandmother Pontellier in Iberville. The Awakening and Selected Short Stories |Kate Chopin 

Of course, most specimens are probably taken up in the summer when the handsome foliage attracts the eye. How to Know the Ferns |S. Leonard Bastin 

In a general way the fronds are best collected during the summer and autumn, when they will, of course, be well developed. How to Know the Ferns |S. Leonard Bastin