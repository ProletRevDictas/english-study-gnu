Unlike other, more complex algorithms, it allows a user to take any video of a person’s face and use it to animate a photo of someone else’s face with only a few lines of code. Memers are making deepfakes, and things are getting weird |Karen Hao |August 28, 2020 |MIT Technology Review 

It will require apps to ask users for permission to collect and share data using a unique code that identifies their iPhones and iPads. Facebook: Apple privacy changes will muck up online ads |Verne Kopytoff |August 26, 2020 |Fortune 

Postal workers expedite millions of ballots to voters and then bring completed ones to county election offices, where they are tracked using bar codes. As states mull expanding vote by mail, they’re turning to Oregon for advice |Lee Clifford |August 24, 2020 |Fortune 

She was told that because of her ZIP code, she was a high-risk patient and was moved to a quarantine room to deliver her baby. Federal Investigation Finds Hospital Violated Patients’ Rights by Profiling, Separating Native Mothers and Newborns |by Bryant Furlow, New Mexico In Depth |August 22, 2020 |ProPublica 

Because of an error in the computer code, the researchers say, their findings — that humans outperformed a computer game that simulated quantum mechanics — are not valid. A reader asks about coronavirus mutations |Science News Staff |August 10, 2020 |Science News 

Based on a true story” is film code for “this may or may not have happened, but almost certainly not in this way. Wrestler Mark Schultz Hates the ‘Sickening and Insulting Lies’ of ‘Foxcatcher’ |Rich Goldstein |December 31, 2014 |DAILY BEAST 

In Brazil people color code their underwear according to their needs. New Year’s Eve, Babylon Style |Candida Moss |December 31, 2014 |DAILY BEAST 

These addresses were used by whoever carried out the attack to control the malware and can be found in the malware code itself. No, North Korea Didn’t Hack Sony |Marc Rogers |December 24, 2014 |DAILY BEAST 

The source code for the original “Shamoon” malware is widely known to have leaked. No, North Korea Didn’t Hack Sony |Marc Rogers |December 24, 2014 |DAILY BEAST 

Enforcement of U.S Code, Title VII, Chapter 25A “Export Standards for Grapes and Plums” remains fully funded, thank goodness. Congress’ Gift That Keeps on Giving |P. J. O’Rourke |December 20, 2014 |DAILY BEAST 

It is no part of the present essay to attempt to detail the particulars of a code of social legislation. The Unsolved Riddle of Social Justice |Stephen Leacock 

The minimum wage law ought to form, in one fashion or another, a part of the code of every community. The Unsolved Riddle of Social Justice |Stephen Leacock 

Plato, dissatisfied with the laws of his country, wrote out a code of morals and laws which he thought much better. Gospel Philosophy |J. H. Ward 

The objectors consider it to be a most selfish doctrine without any warrant in the civilised code of morality. Third class in Indian railways |Mahatma Gandhi 

Why is a cankered tie indissoluble, notwithstanding the great maxim adopted by the code, Quicquid ligatur dissolubile est? A Philosophical Dictionary, Volume 1 (of 10) |Franois-Marie Arouet (AKA Voltaire)