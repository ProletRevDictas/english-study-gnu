He paused for a moment, furrowed his brow, and said he didn’t know anything about the effort—though he did allude to the “great community” that had supported his 2020 campaign. Dems Convert Jon Ossoff Thirst Into Campaign Cash |Sam Brodey |July 9, 2021 |The Daily Beast 

Fifteen minutes later, a wooden fence ushered us above the valley and an unmistakable profile — heavy brow, thick nose, sturdy chin — protruded from the cliff face. A day’s drive from Chicago, exploring a very different Illinois |Carson Vaughan |February 12, 2021 |Washington Post 

Recent data suggests that crashes often occur in the velocity range covered by impact tests, but that helmets often sustain damage at the brow line, an area that CPSC tests don’t target. The Trek WaveCel Helmet Lawsuit, Explained |Joe Lindsey |February 10, 2021 |Outside Online 

Leading me through the waves by the nose of my board, up to his neck in the ocean, Dillon turned toward the horizon, shielding his brow with his hand. How I Learned to Surf in Middle Age |Tom Vanderbilt |January 5, 2021 |Outside Online 

Gigi grunts and furrows her brow before she ultimately says, “You, Daddy,” handing me the rod to reel in the fish. The happiness and heartbreak of a daughter’s first fishing trip |By Jonathon Klein/Field & Stream |December 30, 2020 |Popular-Science 

He wipes beads of sweat from his brow, and extends his hand out towards the crowd. Revenge of the Rock Nerds: TV on the Radio’s Long Road to ‘Seeds’ |Marlow Stern |December 3, 2014 |DAILY BEAST 

Her pallid young face, brow sweating with fear and pain, yet resolute and stiff with sorrow, makes you want to cry. Relishing Rembrandt’s Blockbuster London Show |Nancy Durrant |October 16, 2014 |DAILY BEAST 

It will come as no surprise to his longtime fans that Chuck Klosterman has contributed to this parade of high-brow hypocrisy. Forget the Wife Beating—Are You Ready for Some Football? |Steve Almond |September 11, 2014 |DAILY BEAST 

I asked why, and he paused for a moment, furrowing his brow and exhaling deeply. Native American Basketball Team in Wyoming Have Hoop Dreams Of Their Own |Robert Silverman |August 31, 2014 |DAILY BEAST 

Instead, Love Hotel seems less a documentary than a high-brow reality television show. Inside Japan's 30,000 Kinky ‘Love Hotels’ |Lizzie Crocker |August 4, 2014 |DAILY BEAST 

E was an Esquire, with pride on his brow; F was a Farmer, and followed the plough. Boys and Girls Bookshelf; a Practical Plan of Character Building, Volume I (of 17) |Various 

In fact, so much of her smooth brow as could be seen under a broad-brimmed straw hat was wrinkled in a decided frown. The Red Year |Louis Tracy 

Old Holmes furrowed his brow and closed one eye, seeking with the other the inspiration of the sky. The Soldier of the Valley |Nelson Lloyd 

Lawrence handed the General the mysterious message and Schofield read it with a darkened brow. The Courier of the Ozarks |Byron A. Dunn 

When he was gone, Monsieur de Tressan flung off his wig, and mopped the perspiration from his brow. St. Martin's Summer |Rafael Sabatini