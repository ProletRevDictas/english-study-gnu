There was a string, dating back to the late oughts, when I rarely missed one. Virtual Apple events just aren’t the same |Adam Lashinsky |October 14, 2020 |Fortune 

When companies do bad things they ought to be held accountable for them. Why Do ‘Progressives’ Want to Ban Uber and AirBnB? |Adam Thierer, Christopher Koopman |December 30, 2014 |DAILY BEAST 

We ought to seek Chinese cooperation in a response to this North Korean act of aggression. Obama Could Hit China to Punish North Korea |Shane Harris, Tim Mak |December 20, 2014 |DAILY BEAST 

Then when we arrive at his flat in Shepherd's Bush following the escape, perhaps there ought to be remnants of the ladder. Alfred Hitchcock’s Fade to Black: The Great Director’s Final Days |David Freeman |December 13, 2014 |DAILY BEAST 

The officers explained that those Sikhs had been lynched to death and that Singh ought to anticipate the same fate for himself. As 30-Year Anniversary of Mass Killings in India Arrives, Sikhs Find Safety in USA |Simran Jeet Singh |October 31, 2014 |DAILY BEAST 

“Novelist good for nothing else,” said Samuel Beckett, and that ought to be taken as a compliment. Nobel Prize Winner Modiano’s Magical Musical Prose About Paris |Pierre Assouline |October 14, 2014 |DAILY BEAST 

And is this a mere fantastic talk, or is this a thing that could be done and that ought to be done? The Salvaging Of Civilisation |H. G. (Herbert George) Wells 

I, therefore, deliver it as a maxim, that whoever desires the character of a proud man ought to conceal his vanity. Pearls of Thought |Maturin M. Ballou 

When we speak against one capital vice, we ought to speak against its opposite; the middle betwixt both is the point for virtue. Pearls of Thought |Maturin M. Ballou 

I really ought to visit my California estates, and I have always wanted to see that part of America. Ancestors |Gertrude Atherton 

But she told Grandfather Mole that it was all right—that she knew a person of his age ought not to go without his breakfast. The Tale of Grandfather Mole |Arthur Scott Bailey