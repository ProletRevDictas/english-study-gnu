Touch also increases levels of serotonin, dopamine, and oxytocin—the hormones related to happiness. The pandemic problem tech can’t solve: Physical touch |Danielle Abril |August 23, 2020 |Fortune 

We hope you’ve been enjoying our wedding gift, the espresso machine, and we wish you many years of happiness together. A guide to giving gifts for postponed and shrunken weddings |Brooke Henderson |August 20, 2020 |Fortune 

Researchers have argued for decades about whether certain facial expressions have evolved to express specific emotions, such as happiness, anger and disgust, regardless of one’s culture. Ancient sculptures hint at universal facial expressions across cultures |Bruce Bower |August 19, 2020 |Science News 

It’s spawned so many other indexes of human well-being — the index of social progress, people talking about gross national happiness. Is Economic Growth the Wrong Goal? (Ep. 429) |Stephen J. Dubner |August 13, 2020 |Freakonomics 

This transition makes even more sense when coupled with the idea of deviating from using GDP as a measure of societal growth, and instead adopting a well-being index based on universal human values like health, community, happiness, and peace. The Global Work Crisis: Automation, the Case Against Jobs, and What to Do About It |Peter Xing |August 6, 2020 |Singularity Hub 

It is the summit of human happiness: the surrender of man to God, of woman to man, of several women to the same man. Houellebecq’s Incendiary Novel Imagines France With a Muslim President |Pierre Assouline |January 9, 2015 |DAILY BEAST 

The health, happiness and well-being of men, children and women improve. How Good Dads Can Change the World |Gary Barker, PhD, Michael Kaufman |January 6, 2015 |DAILY BEAST 

He should be free, filling the world with happiness, love and his fighting spirit. Behind Bars for the Holidays: 11 Political Prisoners We Want to See Free In 2015 |Movements.Org |December 25, 2014 |DAILY BEAST 

But she cautions against depending on getaways for happiness. 9 Ways to Cope With Seasonal Affective Disorder |DailyBurn |December 5, 2014 |DAILY BEAST 

Their bright eyes and wide, happy smiles have spread a feeling of happiness and hope across the art world and beyond. Takashi Murakami’s Art From Disaster |Justin Jones |November 28, 2014 |DAILY BEAST 

Very trim and strong, and confident he looked, with the glow of youth in his cheeks, and the spark of happiness in his gray eyes. The Bondboy |George W. (George Washington) Ogden 

Feeling secure regarding their happiness and welfare, she did not miss them except with an occasional intense longing. The Awakening and Selected Short Stories |Kate Chopin 

How, then, are we to explain this extraordinary discrepancy between human power and resulting human happiness? The Unsolved Riddle of Social Justice |Stephen Leacock 

The vision of the universal happiness seen by the economists has proved a mirage. The Unsolved Riddle of Social Justice |Stephen Leacock 

That news has caused a quite universal happiness to this wretched community. The Philippine Islands, 1493-1898, Volume XX, 1621-1624 |Various