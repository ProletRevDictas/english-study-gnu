A pick hammer sits at the top of the handle to allow users to break ice with a tapping motion, while the spike at the bottom offers more traditional stabbing functionality. Best ice pick: A versatile winter tool for camping and more |PopSci Commerce Team |February 25, 2021 |Popular-Science 

Complete your set with a cutting mat, metal rulers and squares, some sponges, and a rubber or wooden hammer—any other type of hammer can damage the leather. Everything you need to know to start leatherworking |Sandra Gutierrez G. |February 19, 2021 |Popular-Science 

The group, often armed with guns and other weapons such as hammers and baseball bats, regularly rallies on the grounds of the Minnesota Capitol. Americans across the political spectrum fear what the Capitol attack portends |Annie Gowen, Jenna Johnson, Holly Bailey |January 12, 2021 |Washington Post 

There were rules and if someone was acting up in the chat you dropped the hammer. ‘The reality is we have to be professional’: Confessions of an ad exec on working amid the chaos at the Capitol |Kristina Monllos |January 11, 2021 |Digiday 

Papagelis joined forces in Hammer’s Lot with Ken Johnson — “Pinto Ron” — in 1992. ‘Bills Mafia’ waited a generation for a team like this. It’s had to embrace it from afar. |Adam Kilgore |January 7, 2021 |Washington Post 

Next, the GOP should hammer away at how our roads, bridges, and tunnels are crumbling, and push for an infrastructure initiative. Bush, Christie, Romney: Who’ll Be the GOP Class Warrior? |Lloyd Green |December 15, 2014 |DAILY BEAST 

If we enter with hammer in hand, we may leave with merely dust and rubble on our faces. For Rent: Priceless Historic Sites |Elinor Betesh |November 16, 2014 |DAILY BEAST 

In this way, certain cognitive mechanisms can act like a hammer too eager for nails. Why Are Millennials Unfriending Organized Religion? |Vlad Chituc |November 9, 2014 |DAILY BEAST 

The phrase means, “the nail that sticks out always gets hit by a hammer.” Sor Juana: Mexico’s Most Erotic Poet and Its Most Dangerous Nun |Katie Baker |November 8, 2014 |DAILY BEAST 

Another surveillance video, showing the perpetrator with hammer in hand, is here. Is Brooklyn Becoming Unsafe for Gays? It Depends On Which Ones |Jay Michaelson |October 18, 2014 |DAILY BEAST 

The noise of the hammer is always in his ears, and his eye is upon the pattern of the vessel he maketh. The Bible, Douay-Rheims Version |Various 

With a hammer the boy knocked off some of the slats of the small box in which Squinty had made his journey. Squinty the Comical Pig |Richard Barnum 

I suppose the hammer falls back more slowly from the string, and that makes the tone sing longer. Music-Study in Germany |Amy Fay 

He was ready to drop when he reached it, and his heart beat like a hammer against his ribs. Blackwood's Edinburgh Magazine, No. CCCXXXIX. January, 1844. Vol. LV. |Various 

And then the Monitor's deafening hammer sounded again, and after that, silence. Hooded Detective, Volume III No. 2, January, 1942 |Various