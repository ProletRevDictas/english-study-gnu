Gas bubbles rise through puddles of mud, producing goopy popping sounds. Life on Earth may have begun in hostile hot springs |Jack J. Lee |September 24, 2020 |Science News 

That included the mud shack in northeastern South Africa where 4-year-old Ntando Kubheka lived. An Insurance Solution for the World’s Poorest |Daniel Malloy |September 21, 2020 |Ozy 

Allbirds figures that sticking with its best advantage—its good-for-the-planet sheen—represents its best chance of not getting left in the mud. Allbirds is stepping up for the planet—by treading lightly on it |sheilamarikar |September 21, 2020 |Fortune 

Geysers of oil, rock and mud have shot skyward 100 feet, and slopes have collapsed under smoking waterfalls of crude and wastewater. Oil Companies Are Profiting From Illegal Spills. And California Lets Them. |by Janet Wilson, The Desert Sun, and Lylla Younes, ProPublica |September 18, 2020 |ProPublica 

She splashed around in the water, dressed in her conservative Friday best, her grandchildren squealing and throwing mud at one another. Palestinians Are Breaking Lockdown to Go to the Beach |Fiona Zublin |September 18, 2020 |Ozy 

He scrambled outside to find a 25-foot-wide crater just beyond the mud wall surrounding his family compound. The Dangerous Drug-Funded Secret War Between Iran and Pakistan |Umar Farooq |December 29, 2014 |DAILY BEAST 

They described him as clad in black, his face smeared with mud. Killer Eric Frein Held in Murdered Cop’s Cuffs |Michael Daly |October 31, 2014 |DAILY BEAST 

Knee deep in mud, sweat mixing with rain, they forced the Land Rover through the jungle. The Original Ebola Hunter |Abby Haglage |September 14, 2014 |DAILY BEAST 

Sheets of torrential rains pouring down over the Land Rover sent its four wheels plunging into the mud. The Original Ebola Hunter |Abby Haglage |September 14, 2014 |DAILY BEAST 

All around you are farms fields dotted with canted mud structures. Whatever You Do Someone Will Die. A Short Story About Impossible Choices in Iraq |Nathan Bradley Bethea |August 31, 2014 |DAILY BEAST 

Aunt Ri, at her best estate, had never possessed a room which had the expression of this poor little mud hut of Ramona's. Ramona |Helen Hunt Jackson 

In some parts of Korea the houses were built of stout timbers, the chinks covered with woven cane and plastered with mud. Our Little Korean Cousin |H. Lee M. Pike 

The sun was palely shining upon dry, clean pavements and upon roads juicy with black mud. Hilda Lessways |Arnold Bennett 

Frank leaped ashore and pushed the boat off, while Mayne held her by jamming the leeward oar into the mud. The Red Year |Louis Tracy 

A cannon-ball crashed through the mud wall and bounded across the enclosure. The Red Year |Louis Tracy