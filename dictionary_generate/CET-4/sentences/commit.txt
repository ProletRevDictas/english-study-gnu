They set up camp in the first quarter and shot 19 for 32 from beyond the arc, more than making up for the 12 turnovers they committed. The Wizards’ defense again lets them down in loss at home to Raptors |Ava Wallace |February 11, 2021 |Washington Post 

The Cavaliers committed a season-high 17 turnovers but shot 64 percent in the second half, including 5 for 10 on three-pointers. Virginia pulls away from Georgia Tech, tightens grip on first place in ACC |Gene Wang |February 11, 2021 |Washington Post 

“We would also like to receive input from the Housing Commission, which committed to a study on the viability of a vacancy tax two years ago,” she said. Vacancy Tax Study Is Giving City Officials Déjà Vu |Lisa Halverstadt and Andrew Keatts |February 10, 2021 |Voice of San Diego 

Later that year, Kennedy committed to play basketball at Loyola of Chicago. Remember Loyola? This Team Is Even Better Than The One That Made The Final Four. |Jake Lourim |February 10, 2021 |FiveThirtyEight 

It is widely expected that it will happen for the 2021 season, although Goodell did not commit last week to that. NFL begins an uncertain offseason, with questions about vaccines, the salary cap and more |Mark Maske |February 9, 2021 |Washington Post 

“What some people fail to realize is that it is not just fraternities that commit sexual assault,” he suggests. Fraternities in a Post-UVA World |Samantha Allen |December 12, 2014 |DAILY BEAST 

Seevakumaran uploaded six videos to YouTube on March 17, just hours before he would threaten his roommate and commit suicide. School Shooters Love This Pickup Artist Website |Brandy Zadrozny |December 5, 2014 |DAILY BEAST 

Jackson's story is unique, but only in how long he was made to suffer for a crime he didn't commit. For Ricky Jackson, a Just Verdict—But 39 Years Too Late |Cliff Schecter |November 26, 2014 |DAILY BEAST 

Third, Republicans should commit to compassion in action rather than compassion in appearance. How a GOP Senate Can Help the Poor |Veronique de Rugy |November 23, 2014 |DAILY BEAST 

Voters fill out their name, address, phone number and sign a pledge that they will “commit to vote.” The Democrats’ Simple Midterm Weapon |Ben Jacobs |November 4, 2014 |DAILY BEAST 

With him they were soon on the intimate terms of shipboard—terms that commit one to nothing in the future when land is reached. Bella Donna |Robert Hichens 

She knew that the man's honor, his respect for his race and their struggle had brought him to commit the sacrifice. The Homesteader |Oscar Micheaux 

Amongst the prisoners were several provincial governors, one of whom attempted to commit suicide. The Philippine Islands |John Foreman 

If you fight in your boots, we must all do the same, and for myself—well, I have not come here to commit suicide. St. Martin's Summer |Rafael Sabatini 

But it does not follow that because we neither hate nor blame a criminal we should allow him to commit crime. God and my Neighbour |Robert Blatchford