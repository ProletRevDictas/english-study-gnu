There was a lot of prison fiction from movies and books to mine. ‘Archer’ Creator Adam Reed Spills Season 6 Secrets, From Surreal Plotlines to Life Post-ISIS |Marlow Stern |January 8, 2015 |DAILY BEAST 

I gave a reading last week with someone who had taken a class of mine. Daphne Merkin on Lena Dunham, Book Criticism, and Self-Examination |Mindy Farabee |December 26, 2014 |DAILY BEAST 

I wanted to be anonymous, as some of these people were friends of mine. I Was Gang Raped at a UVA Frat 30 Years Ago, and No One Did Anything |Liz Seccuro |December 16, 2014 |DAILY BEAST 

It reminds me of an uncle of mine who said the London Blitz was irritating. Why Can’t Movies Capture Genius? |Clive Irving |December 14, 2014 |DAILY BEAST 

I learn by the third day to tell the nurse privately to make mine mostly orange juice. Alfred Hitchcock’s Fade to Black: The Great Director’s Final Days |David Freeman |December 13, 2014 |DAILY BEAST 

We know one thing—the men that killed Rutter are the ones that held us up, and got off with that money of mine. Raw Gold |Bertrand W. Sinclair 

And it is small consolation to me to note that most people's minds seem to be no better done than mine. The Salvaging Of Civilisation |H. G. (Herbert George) Wells 

When a besieged city suspects a mine, do not the inhabitants dig underground, and meet their enemy at his work? The Pastor's Fire-side Vol. 3 of 4 |Jane Porter 

The second cable quotes mine of last night wherein I ask leave to call for the East Lancs. Gallipoli Diary, Volume I |Ian Hamilton 

And hence the reader can notice the fundamental difference between all other methods and mine. Assimilative Memory |Marcus Dwight Larrowe (AKA Prof. A. Loisette)