She quits cold turkey — “It took four days,” she says — after meeting her husband Kevin Hunter. Wendy Williams seems like the perfect Lifetime movie subject. So why is it so unsettling to watch? |Bethonie Butler |January 31, 2021 |Washington Post 

Naturally, you want to share this workload with your husband, but requesting he quit something he has done for years was going to be met with understandable resistance. Sharing the workload at home when one child has special needs |Meghan Leahy |January 27, 2021 |Washington Post 

A few people quit high school to do the show and then got their GEDs, and we were aware that getting this job was changing their life trajectory. How ‘Freaks and Geeks’ went from misfit dramedy to cult classic, as told by its cast and creators: ‘People just like it so much that it thrusts itself from the grave’ |Sonia Rao |January 27, 2021 |Washington Post 

For the family, if Peter quit flying, it meant he’d be home more to help with the kids. The Climate Crisis Is Worse Than You Can Imagine. Here’s What Happens If You Try. |by Elizabeth Weil |January 25, 2021 |ProPublica 

Despite the threats and harassment not one Parler employee has quit. House Oversight Committee chairwoman requests FBI probe of Parler, including its role in Capitol siege |Tom Hamburger, Craig Timberg |January 22, 2021 |Washington Post 

Park employees helped John quit tobacco by way of a butts-proof glass enclosure, a drastic change in diet, and regular exercise. Zebra Finches, Dolphins, Elephants, and More Animals Under the Influence |Bill Schulz |December 31, 2014 |DAILY BEAST 

But they had not quit and here they now were as the Emerald Society Pipes and Drums came into the Garden. Cop Families Boo De Blasio at NYPD Graduation |Michael Daly |December 30, 2014 |DAILY BEAST 

Army B-squad players who fail to make it onto the varsity team after a year or two usually quit football. A West Point MVP Who Never Played a Down |Nicolaus Mills |December 13, 2014 |DAILY BEAST 

On Tuesday, two senior Kremlin officials, Vladimir Avdeyenko and Boris Rapoport, quit their jobs. Recession? Devaluation? Inflation? Putin Tells Russia Stay the Course. |Anna Nemtsova |December 4, 2014 |DAILY BEAST 

His breath became so strained that he was forced to quit his job as a horticulturalist for the parks department. Before Eric Garner, There Was Michael Stewart: The Tragic Story of the Real-Life Radio Raheem |Marlow Stern |December 4, 2014 |DAILY BEAST 

Hain't I kep' in doors uv a nite, an quit chawn tobacker and smokin' segars just to please her? The Book of Anecdotes and Budget of Fun; |Various 

And knowing that bunch as well as I do, I don't think they'll lift the plunder and quit the country till they can go together. Raw Gold |Bertrand W. Sinclair 

The seller may safely give a quit-claim deed for he thereby sells only whatever interest he may have. Putnam's Handy Law Book for the Layman |Albert Sidney Bolles 

King Edward refused to believe the evidence of his senses, and obstinately refused to quit the field. King Robert the Bruce |A. F. Murison 

That you and Jim don't mention the sale to anybody, and keep on runnin' the place—for wages—until I'm ready for you to quit. Scattergood Baines |Clarence Budington Kelland