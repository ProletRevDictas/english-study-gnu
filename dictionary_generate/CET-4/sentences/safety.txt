The unidentified employee asked whether the rating of the system’s risks should be raised, which may have prompted a more thorough safety review. Boeing crashes were the “horrific culmination” of multiple mistakes, House report says |kdunn6 |September 16, 2020 |Fortune 

While the active pharmaceutical ingredient remains the same, excipients may be different, and even seemingly slight differences can significantly impact patient safety. The ‘inactive' ingredients in your pills could harm you |By Yelena Ionova/The Conversation |September 15, 2020 |Popular-Science 

Customers are given about 48 hours’ notice, at the most, that their power will be cut — known as a public safety power shutoff. Environment Report: State Throws Cold Water on Pricing Scheme |MacKenzie Elmer |September 14, 2020 |Voice of San Diego 

Milton claimed the parts had been taken out of the truck for safety reasons. Nikola stock craters after chairman fails to rebut fraud allegations |Timothy B. Lee |September 11, 2020 |Ars Technica 

He said this was especially true for vaccines, which are tested on healthy people, and therefore have a higher safety threshold for adverse side effects than medicines that are designed to treat people who are already ill. COVID-19 vaccine still on track for later this year despite trial pause, AstraZeneca CEO says |Jeremy Kahn |September 10, 2020 |Fortune 

A passing off-duty school safety officer named Fred Lucas said that he had been told the man was a drug dealer. Shot Down During the NYPD Slowdown |Michael Daly |January 7, 2015 |DAILY BEAST 

“This is the only place in the souk you can buy safety pins,” he said. The Photographer Who Gave Up Manhattan for Marrakech |Liza Foreman |January 6, 2015 |DAILY BEAST 

The accident rate in Asia has marred what was in 2014 a banner year for aviation safety. Annoying Airport Delays Might Prevent You From Becoming the Next AirAsia 8501 |Clive Irving |January 6, 2015 |DAILY BEAST 

Not for the benefit of the harasser, of course, but for your own safety. Cover-Ups and Concern Trolls: Actually, It's About Ethics in Suicide Journalism |Arthur Chu |January 3, 2015 |DAILY BEAST 

There was virtually no government oversight of safety and operational standards. Who Will Get AsiaAir 8501’s Black Boxes? |Clive Irving |December 30, 2014 |DAILY BEAST 

To Berthier, if to any one, Bonaparte entrusted his secret designs, for he knew that he could do so in safety. Napoleon's Marshals |R. P. Dunn-Pattison 

These residents then killed the parish priest, and without arms fled for safety to the mountain ravines. The Philippine Islands |John Foreman 

Juan de Messa lost his head, and ran down stairs, thinking that his safety lay there. The Philippine Islands, 1493-1898, Volume XX, 1621-1624 |Various 

In short, in a few minutes, he might have the safety of his father, and the preservation of Europe in his hand. The Pastor's Fire-side Vol. 3 of 4 |Jane Porter 

In that instant I felt fingers tighten on my arm, tighten till they bit into the flesh, and I was pulled back into safety. Uncanny Tales |Various