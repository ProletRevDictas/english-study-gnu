In khao kan jin, a northern Thai dish, pork blood is mixed with rice and steamed in a banana leaf. Blood is a respected ingredient around the world, but less so in the U.S. A new book aims to change that. |Mayukh Sen |February 26, 2021 |Washington Post 

Case numbers remained low at Rice, with no single day seeing more than six reported cases. How 5 universities tried to handle COVID-19 on campus |Betsy Ladyzhets |February 23, 2021 |Science News 

Meadow Grill, in the same area, offers a surprising variety of goods, including breakfast burritos and rice bowls. The Ultimate Yosemite National Park Travel Guide |Shawnté Salabert |February 22, 2021 |Outside Online 

Since 1977, when he started importing rice and sugar to his home state of Kano, Nigeria, and selling them at a lucrative markup, his business empire has expanded across industries and borders. The New Face of Entrepreneurship? |Joshua Eferighe |February 21, 2021 |Ozy 

Jahmal Usen, and his home-cooked meals of red-tinged jollof rice and comforting spaghetti stir-fry, are an important part of it. Feeding a New Nigeria |Nelson C.J. |January 22, 2021 |Eater 

The book details his confrontations with neoconservatives, and his alliance with Condoleezza Rice. The Best Memoirs of 2014 |William O’Connor |December 9, 2014 |DAILY BEAST 

Rice had received this video in discovery during his criminal case, but it had not been aired publicly, as had the first video. The $44 Million Teflon Don of the NFL |Mike Barnicle |November 30, 2014 |DAILY BEAST 

At that meeting, Rice told the Commissioner that he had hit Mrs. Rice in the elevator. The $44 Million Teflon Don of the NFL |Mike Barnicle |November 30, 2014 |DAILY BEAST 

A call made to police beforehand described Rice as “a guy with a pistol” on a swing set, but said it was “probably fake.” The 14 Teens Killed by Cops Since Michael Brown |Nina Strochlic |November 25, 2014 |DAILY BEAST 

And the Constitution, written on goatskin not rice paper, might be as our country was under Jefferson. Why We Can’t Quit Calling Presidents ‘Kings’ |Kevin Bleyer |November 22, 2014 |DAILY BEAST 

In his youngest days, when his mother used to regulate his food, she would stuff him full of rice. Our Little Korean Cousin |H. Lee M. Pike 

Likewise your Majesty will have shelter for his vessels, and a foothold in that country, which abounds with meat and rice. The Philippine Islands, 1493-1898, Volume XX, 1621-1624 |Various 

This gift of rice was especially pleasing to the traveller, as no dish is held in higher honour in Korea. Our Little Korean Cousin |H. Lee M. Pike 

After this he awoke much refreshed, and having obtained some rice from the native chief, ate a little with relish. Hunting the Lions |R.M. Ballantyne 

Mr. Rice, the owner of the plantation, was a hot Southern sympathizer, but he did not relish his present company. The Courier of the Ozarks |Byron A. Dunn