This stainless steel dishwasher on wheels goes where you need it to go. Small dishwashers that fit in almost any kitchen |PopSci Commerce Team |August 25, 2020 |Popular-Science 

Each jar was topped with a steel cone into which dogs could put their muzzles. Viral scents? Dogs sniff out coronavirus in human sweat |Sharon Oosthoek |August 19, 2020 |Science News For Students 

Brazil and South Korea aggressively expanded steel production. Unmade in America |Tate Ryan-Mosley |August 14, 2020 |MIT Technology Review 

The steel columns that supported the upper-deck overhang and coverings obstructed views of the action. MLB’s Newest Ballpark Is A Shift Away From Retro-Era Stadiums |Travis Sawchik |July 16, 2020 |FiveThirtyEight 

With a laser cutter, they cut some of the patterns into stainless steel and tested them. Shape-shifting cuts give shoes a better grip |Carolyn Wilke |July 14, 2020 |Science News For Students 

As a major source for steel during World War II, Sheffield was a frequent target of bombing raids. The Greatest Rock Voice of All Time Belonged to Joe Cocker |Ted Gioia |December 23, 2014 |DAILY BEAST 

We made big things like steel for ships and tractors and turbines for hydroelectric plants. Christmas Is the New Subprime |Doug McIntyre |December 9, 2014 |DAILY BEAST 

Then, from a pocket inside his camouflage top, he pulled a hidden stainless steel flask. I Shot Bin Laden |Elliot Ackerman |November 16, 2014 |DAILY BEAST 

Now the lead breacher explained how he cut through the steel doors bin Laden used to seal himself into the compound at night. I Shot Bin Laden |Elliot Ackerman |November 16, 2014 |DAILY BEAST 

We sat in rows of grey steel fold out chairs that faced a model of the compound in Abbottabad. I Shot Bin Laden |Elliot Ackerman |November 16, 2014 |DAILY BEAST 

A leather swordbelt, gold-embroidered at the edges, carried a long steel-halted rapier in a leather scabbard chaped with steel. St. Martin's Summer |Rafael Sabatini 

His nose was hooked and rather large, his eyes were blue, bright as steel, and set a trifle wide. St. Martin's Summer |Rafael Sabatini 

A colossal steam "traveller" had ceaselessly carried great blocks of stone and long steel girders from point to point. The Pit Town Coronet, Volume I (of 3) |Charles James Wills 

He saw a large building, in front of which were long, slender strips of shining steel. Squinty the Comical Pig |Richard Barnum 

Woe to the man that first did teach the cursed steel to bite in his own flesh, and make way to the living spirit. Pearls of Thought |Maturin M. Ballou