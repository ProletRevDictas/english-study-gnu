The feature, which has been around for years, allows Alexa users to combine multiple tasks into a single voice command of their choosing. Amazon makes Alexa Routines shareable |Sarah Perez |September 17, 2020 |TechCrunch 

Second, the department plans to overhaul the process and chain of command for simultaneous search warrants. The policing reforms in the Breonna Taylor settlement, explained |Fabiola Cineas |September 17, 2020 |Vox 

The formidable DJI RoboMaster S1 will accept commands from a remote, or via a simple coding system called Scratch. These three robots can teach kids how to code |John Kennedy |September 17, 2020 |Popular-Science 

You could have it switch all your smart home devices off with a single command. Get around your phone more quickly than you already are |David Nield |September 16, 2020 |Popular-Science 

Bashagha, who sought to rein in militias after fighting between armed groups rocked Tripoli in 2018, angered al-Sarraj by stating publicly that the civilian police under his command would protect demonstrators. Can Libya’s Fragile Peace Survive Fresh Cracks? |Charu Kasturi |September 15, 2020 |Ozy 

Certainly, she seems to command near-total devotion among her clients. How Taryn Toomey’s ‘The Class’ Became New York’s Latest Fitness Craze |Lizzie Crocker |January 9, 2015 |DAILY BEAST 

You expect soldiers of all ranks to understand the need to respect the chain of command, regardless of personal feelings. We Need Our Police to Be Better Than This |Nick Gillespie |December 31, 2014 |DAILY BEAST 

The seemingly endless ranks snapped to attention on command and thousands of white gloves rose in salute. Choking Back Tears, Thousands of Cops Honor Fallen Officer Ramos |Michael Daly |December 28, 2014 |DAILY BEAST 

Perhaps the most interesting and indeed relevant of this is the C2 (or Command and Control) addresses found in the malware. No, North Korea Didn’t Hack Sony |Marc Rogers |December 24, 2014 |DAILY BEAST 

In the event, the enemy did plenty—far more than SHAEF, or for that matter the German high command, imagined possible. Hitler’s Hail Mary |James A. Warren |December 20, 2014 |DAILY BEAST 

One of the simplest of these childish tricks is the invention of an excuse for not instantly obeying a command, as "Come here!" Children's Ways |James Sully 

Like every other Spanish general in supreme command abroad, Polavieja had his enemies in Spain. The Philippine Islands |John Foreman 

Thanks to Berthier's admirable system, Bonaparte was kept in touch with every part of his command. Napoleon's Marshals |R. P. Dunn-Pattison 

They were never refused, for their recipients looked upon them much in the light of a royal command. The Pit Town Coronet, Volume I (of 3) |Charles James Wills 

The General in command of the station was a feeble old man, suffering from senile decay. The Red Year |Louis Tracy