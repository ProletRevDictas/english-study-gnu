South Dakota is the more tourist-friendly, slightly better weather, historic Mount Rushmore spot. Full Transcript: Tomi Lahren on ‘The Carlos Watson Show’ |Daniel Malloy |August 31, 2020 |Ozy 

Amsterdam has an economy which is highly dependent on tourists. Is Economic Growth the Wrong Goal? (Ep. 429) |Stephen J. Dubner |August 13, 2020 |Freakonomics 

Travelers, however, are eligible to have the tax refunded on goods they take home with them, which is what agencies such as Planet specialize in facilitating, giving it a clear view of how much tourists are buying. Travel spending has vanished across Europe |Marc Bain |July 27, 2020 |Quartz 

In June, when peak season is typically getting underway, tourist spending was still 98% below its level last year based on Planet’s data. Travel spending has vanished across Europe |Marc Bain |July 27, 2020 |Quartz 

Besides feeling safe, tourists may also be lured to local destinations given international travel currently comes with complicated quarantine protocols by governments in different countries. Short trips and staycations will bring India’s crushed tourism industry back to life |Niharika Sharma |July 9, 2020 |Quartz 

Its graceful hotels and beautiful restaurants are totally dependent on the tourist trade. Is Putin Turning to Terrorism in Ukraine? |Anna Nemtsova |January 6, 2015 |DAILY BEAST 

It is the only tourist center Ukraine has left on the Black Sea, since Russia annexed Crimea last spring. Is Putin Turning to Terrorism in Ukraine? |Anna Nemtsova |January 6, 2015 |DAILY BEAST 

For the Brogpas, transforming into a tourist attraction may offer their community a way to generate much-needed income. The Himalayas’ Hidden Aryans |Nina Strochlic |January 3, 2015 |DAILY BEAST 

The two islands are now tourist sites for visitors from Taiwan and mainland China. The World’s Toughest Political Quiz |Jeff Greenfield |December 31, 2014 |DAILY BEAST 

Not all Israeli gay propaganda is pinkwashing—a lot of it is good, old-fashioned PR to attract gay tourist dollars to Tel Aviv. How Canadian Oilmen Pinkwash the Keystone Pipeline |Jay Michaelson |December 28, 2014 |DAILY BEAST 

The tourist climbing tna, or Vesuvius' rugged side, puffs on though they perchance have long since ceased to smoke. Tobacco; Its History, Varieties, Culture, Manufacture and Commerce |E. R. Billings. 

They have developed fishing and agriculture, and have brought the tourist into districts little visited before. Fifty Years of Railway Life in England, Scotland and Ireland |Joseph Tatlow 

Mr. Frank Cook had insisted upon our being the guests of his firm on their tourist steamer Amasis. Fifty Years of Railway Life in England, Scotland and Ireland |Joseph Tatlow 

My father knew him well, often talked to me about him, and told me stories of the excursion and tourist trade in its early days. Fifty Years of Railway Life in England, Scotland and Ireland |Joseph Tatlow 

This was the first small beginning of that great tourist business which now encircles the habitable globe. Fifty Years of Railway Life in England, Scotland and Ireland |Joseph Tatlow