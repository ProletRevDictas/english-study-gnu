Ogalo, who hosts the podcast “In Your Twentys,” wasn’t used to having deep conversations on Tinder. What’s sexy in a pandemic? Caution. |Lisa Bonos |February 12, 2021 |Washington Post 

It goes a long way, and I wanted to say thank you from the deepest of my heart. Ryan Newman returns to Daytona 500, with no memory of 2020 crash and ‘therefore no fear’ |Cindy Boren |February 11, 2021 |Washington Post 

These specialists are the link between deep knowledge of Schneider Electric’s audiences and account optimization. Case study: Schneider Electric’s digital transformation through centralized search |Evan Kent and Kimberly Dutcher |February 11, 2021 |Search Engine Watch 

I do have actual friends, and I find that I would rather spend my limited free time having real conversations with them about our deeper and more private feelings. Carolyn Hax: No time for small talk? That’s building a big wall. |Carolyn Hax |February 11, 2021 |Washington Post 

Both players had plenty of good looks, hitting three shots from deep apiece. Maryland misses a chance to boost its NCAA tournament hopes with a loss to No. 4 Ohio State |Emily Giambalvo |February 9, 2021 |Washington Post 

Deep, situational, and emotional jokes based on what is relevant and has a POINT! Trolls and Martyrdom: Je Ne Suis Pas Charlie |Arthur Chu |January 9, 2015 |DAILY BEAST 

The lascivious sex predator is out; the deep-pocketed caped crusader is most definitely in. Sleazy Billionaire’s Double Life Featured Beach Parties With Stephen Hawking |M.L. Nestel |January 8, 2015 |DAILY BEAST 

Fumbleroooohski…'” (39) “'Look at me, ungh, splitting my own seam, oohh… going deep. ‘A Gronking to Remember’ Speed Read: 8 Naughtiest Bits |Emily Shire |January 7, 2015 |DAILY BEAST 

There was deep brown flesh, and bronze flesh, and pallid white flesh, and flesh turned red from the hot sun. Powerful Congressman Writes About ‘Fleshy Breasts’ |Asawin Suebsaeng |January 7, 2015 |DAILY BEAST 

As he drove me back to the logging road, Frank told me about the area in his deep voice. The 7-Year-Old Plane Crash Survivor’s Brutal Journey Through the Woods |James Higdon |January 7, 2015 |DAILY BEAST 

He remembered something—the cherished pose of being a man plunged fathoms-deep in business. St. Martin's Summer |Rafael Sabatini 

At present, Louis was too self-absorbed by the struggles within him, to look deep into what was passing around him. The Pastor's Fire-side Vol. 3 of 4 |Jane Porter 

It succeeds best in a deep rich loam in a climate ranging from forty to fifty degrees of latitude. Tobacco; Its History, Varieties, Culture, Manufacture and Commerce |E. R. Billings. 

And then what could be more deep and poetic than Liszt's transcriptions of Schubert's and Wagner's songs? Music-Study in Germany |Amy Fay 

The next moment a pistol was fired at their head, and a deep groan shewed it had taken too true an aim. The Pastor's Fire-side Vol. 3 of 4 |Jane Porter