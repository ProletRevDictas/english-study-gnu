She told Recode that while she thinks Nextdoor can be a useful tool, she’s concerned about the way it handles content that includes misinformation and hateful posts from neighbors. How the Covid-19 pandemic broke Nextdoor |Rebecca Heilweil |February 9, 2021 |Vox 

The domain registrar, Epik, warned that the site would get kicked offline after a flood of complaints about hateful, threatening content. TheDonald’s owner speaks out on why he finally pulled plug on hate-filled site |Craig Timberg, Drew Harwell |February 5, 2021 |Washington Post 

That may make hateful ideas seem more widespread than they are. How to fight online hate before it leads to violence |Kathiann Kowalski |February 4, 2021 |Science News For Students 

Posts that are hateful or controversial or play into preconceived biases tend to gain more likes, comments and shares than those that are thoughtful, lengthy or nuanced. Big Tech's Business Model Is a Threat to Democracy. Here's How to Build a Fairer Digital Future |Billy Perrigo |January 22, 2021 |Time 

Aaron received thousands of letters every day as he chased down Babe Ruth’s career home run record in the 1970s, and much of it was hateful. Hank Aaron left an indelible mark on MLB players: 'He was not going to let anything distract him’ |Matt Bonesteel |January 22, 2021 |Washington Post 

There is no such thing as speech so hateful or offensive it somehow “justifies” or “legitimizes” the use of violence. Trolls and Martyrdom: Je Ne Suis Pas Charlie |Arthur Chu |January 9, 2015 |DAILY BEAST 

If people could only renounce their hateful ideas, they could learn to love one another. COEXIST’s Bonehead Bumper-Sticker Politics |Michael Schulson |December 21, 2014 |DAILY BEAST 

Would her embrace recognize the decade of hateful touch I had lived through? Patted Down by India’s Hugging Saint |Daniel Genis |July 20, 2014 |DAILY BEAST 

“The pro-gay marriage people need to accept that even though we disagree, we are not hateful,” said Brown. The Coming Gay Marriage Witch Hunt |David Freedlander |June 19, 2014 |DAILY BEAST 

He says, “Why have the Gods made me love a hateful woman” over the body of their dead son. Game of Thrones’ Lena Headey on Cersei Lannister’s Future and That Controversial Rape Scene |Marlow Stern |June 17, 2014 |DAILY BEAST 

I guess you are growing up, 17 laughed unconscious Dinah; its hateful and horrid to grow up; I never shall. Tessa Wadsworth's Discipline |Jennie M. Drinkwater 

Pete, after spewing the last hateful molecule away, reversed his tiny fibre engines, and began to draw in. Old Friends Are the Best |Jack Sharkey 

Deception, so hateful to her truthful soul, she was compelled to carry on even against her trusting husband. The Pit Town Coronet, Volume II (of 3) |Charles James Wills 

Oh, if only something, someone would drive away the hateful presence which was following him, surrounding him! The Everlasting Arms |Joseph Hocking 

If those who are going to go in for our hateful business only knew what it really was they would sooner be chambermaids. Camille (La Dame aux Camilias) |Alexandre Dumas, fils