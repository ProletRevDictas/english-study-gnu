The lack of access has been a sore point within the SEC for the past decade. How Tesla led the way for Chinese rival Xpeng’s $1.5 billion IPO |eamonbarrett |August 28, 2020 |Fortune 

The man had a fever, cough, sore throat and headache for three days in March. A Hong Kong man got the new coronavirus twice |Erin Garcia de Jesus |August 26, 2020 |Science News For Students 

The first time the man was infected, he had a fever, cough, sore throat and headache for three days. A man in Hong Kong is the first confirmed case of coronavirus reinfection |Erin Garcia de Jesus |August 24, 2020 |Science News 

The volunteers who got it developed sore arms and other side effects that wouldn’t give away that they had not received the coronavirus vaccine. New COVID-19 vaccines show promise in people |Tina Hesman Saey |July 24, 2020 |Science News For Students 

The meningococcal vaccine is safe and was used as a comparison group instead of a placebo so that volunteers got sore arms and other side effects that wouldn’t give away that they were in a comparison group. COVID-19 vaccines by Oxford, CanSino and Pfizer all trigger immune responses |Tina Hesman Saey |July 21, 2020 |Science News 

But for the real Mark Schultz, whom Tatum plays in the film Foxcatcher, it has become a sore point. Wrestler Mark Schultz Hates the ‘Sickening and Insulting Lies’ of ‘Foxcatcher’ |Rich Goldstein |December 31, 2014 |DAILY BEAST 

In the wake of the verdicts in Ferguson and New York City, many of us are still sore with emotion. The Stacks: A Chicken Dinner That Mends Your Heart |Pete Dexter |December 7, 2014 |DAILY BEAST 

Few sore losers could wield sharp words quite like Leon Trotsky, especially when talking about Joseph Stalin. Kotkin Biography Reveals Stalin's Evil Pragmatism |William O’Connor |November 30, 2014 |DAILY BEAST 

It's always been the same: Tim Stoddard has a sore arm and they believe him. Will the Real Jim Palmer Please Stand Up |Tom Boswell |September 27, 2014 |DAILY BEAST 

Before that final game, the Birds suggested that Palmer take a cortisone shot in his shoulder, which, that week, had become sore. Will the Real Jim Palmer Please Stand Up |Tom Boswell |September 27, 2014 |DAILY BEAST 

The Father had been in sore straits of mind, as month after month had passed without tidings of his "blessed child." Ramona |Helen Hunt Jackson 

She is a model of the Brisk—the little Brisk that was sore exposed that day at Navarino. Kipling Stories and Poems Every Child Should Know, Book II |Rudyard Kipling 

Leo flushed, and began again with a sore throat and a bad temper. Kipling Stories and Poems Every Child Should Know, Book II |Rudyard Kipling 

Jess, whose heart was still sore from the blow she had received at Mr. Closewicks grocery, thought this was very kind of Griff. The Girls of Central High on the Stage |Gertrude W. Morrison 

And he taketh with him Peter and James and John, and began to be greatly amazed, and sore troubled. His Last Week |William E. Barton