Personally, I’m happy for conversations that move things forward in helping us be ever-safer with online data usage and storage. This decade’s most important marketing question: What data rights do advertisers possess? |Kirk Williams |September 17, 2020 |Search Engine Land 

Lorne Michaels, SNL’s producer, told Vulture in an interview that Carrey initiated the conversation about playing the part. Trump contradicts CDC director on vaccine; Biden says Americans shouldn’t trust Trump |Colby Itkowitz, Felicia Sonmez, John Wagner |September 16, 2020 |Washington Post 

They also were instructed to edit the beginning and ending of each snippet to differentiate the posts slightly, according to the notes from the recorded conversation with a participant. Pro-Trump youth group enlists teens in secretive campaign likened to a ‘troll farm,’ prompting rebuke by Facebook and Twitter |Isaac Stanley-Becker |September 15, 2020 |Washington Post 

It became a very emotional experience for me, these conversations. The Last Bar Standing? |Eugene Robinson |September 15, 2020 |Ozy 

While standing in line for food I struck up a conversation with the man in front of me. Making Poly Less of a Pain in the Ass |Eugene Robinson |September 14, 2020 |Ozy 

For many years afterward it was a never-ending topic of conversation, and is more or less talked of even to this day. New York’s Most Tragic Ghost Loves Minimalist Swedish Fashion |Nina Strochlic |January 8, 2015 |DAILY BEAST 

Yet, for god knows what reason, his name is never brought up in the “Great American Filmmaker” conversation. Oscars 2015: The Daily Beast’s Picks, From Scarlett Johansson to ‘Boyhood’ |Marlow Stern |January 6, 2015 |DAILY BEAST 

Even though we were running late, Scott was jovial and candid in his conversation. Remembering ESPN’s Sly, Cocky, and Cool Anchor Stuart Scott |Stereo Williams |January 4, 2015 |DAILY BEAST 

A successful trend-maker might be able to steer a conversation, but virality remains extremely difficult to predict. China’s Internet Is Freer Than You Think |Brendon Hong |December 27, 2014 |DAILY BEAST 

In conversation, her ideas emerge at a roiling boil that often takes on a momentum of its own. Daphne Merkin on Lena Dunham, Book Criticism, and Self-Examination |Mindy Farabee |December 26, 2014 |DAILY BEAST 

A desultory conversation on politics, in which neither took the slightest interest, was a safe neutral ground. The Pit Town Coronet, Volume I (of 3) |Charles James Wills 

Cousin George's position is such a happy one, that conversation is to him a thing superfluous. Physiology of The Opera |John H. Swaby (AKA "Scrici") 

He insisted upon my staying a while, and we had the most amusing and entertaining conversation imaginable. Music-Study in Germany |Amy Fay 

It was the conversation of every circle; and discussed according to the dispositions, or views of the speakers. The Pastor's Fire-side Vol. 3 of 4 |Jane Porter 

During this conversation Harry's right hand was resting beneath his jacket, grasping the butt of his revolver. The Courier of the Ozarks |Byron A. Dunn