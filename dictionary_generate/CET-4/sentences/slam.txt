That moment endeared me to the fierce front woman and former slam poet. Tarriona “Tank” Ball’s Playlist is Pure Friend Goals |Brande Victorian |February 19, 2021 |Essence.com 

Members of the Texas Rangers once chastised him for turning a 3-0 pitch into a grand slam in a game the Padres were winning handily — in other words, for not holding back. The Fernando Tatis Jr. deal is huge, but so are the Padres’ ambition and baseball’s expectations |Chelsea Janes |February 18, 2021 |Washington Post 

At-home tech, however, isn’t always a slam dunk, so brands have to be keenly aware of the marketplace and be agile when it comes to what they are providing. L’Oréal eyes at-home tech market to accelerate its DTC plan |Seb Joseph |January 19, 2021 |Digiday 

Beal gave Bryant the critique, and one play later, the guard found Bryant under the basket for the slam. Thomas Bryant is making the leap by controlling his emotions. Westbrook and Beal help, too. |Ava Wallace |January 6, 2021 |Washington Post 

He also made his first All-Star Game, where he won the slam dunk contest, which featured a team format for the first time. The greatest moments of John Wall’s decade with the Wizards |Scott Allen |December 3, 2020 |Washington Post 

That may be, but experts say that “similarities” to other attacks is hardly a slam dunk. FBI Won’t Stop Blaming North Korea for Sony Hack -- Despite New Evidence |Shane Harris |December 30, 2014 |DAILY BEAST 

The bar also claims that it hosted the first-ever poetry slam 28 years ago. The Bars That Made America Great |Nina Strochlic |December 28, 2014 |DAILY BEAST 

And the information that the FBI has presented so far strikes many experts as hardly a slam dunk against Pyongyang. Cyberwar on North Korea Could Be Illegal |Shane Harris |December 23, 2014 |DAILY BEAST 

Apparently, 2016 is not shaping up as a Democratic slam dunk. 2016 Is No Democratic Slam Dunk |Lloyd Green |December 1, 2014 |DAILY BEAST 

If the President is seeking a way to court Republicans, alcohol is a slam-drunk. The Booze That Saved America |Kevin Bleyer |November 8, 2014 |DAILY BEAST 

She glanced uneasily at Gwynne and fancied she could hear him slam the lid of his breeding upon a supercilious sputter. Ancestors |Gertrude Atherton 

Expectantly I follow his movements; I recognize the vigorous slam of the door and the click of the spring lock. Prison Memoirs of an Anarchist |Alexander Berkman 

Something in the way she said it made it sound like a little slam on the boss, and of course I wasn't going to stand for that. The Wreckers |Francis Lynde 

Into the hut bounded Frank, and the door went to with a slam. Frank Merriwell's Bravery |Burt L. Standish 

After eating the supper Madge prepared for him, he shuffled out, permitting the kitchen door to slam behind him. The Missing Formula |Mildred A. Wirt, AKA Ann Wirt