The flame is supposed to travel through all of Japan’s 47 prefectures before arriving in Tokyo for the July 23 Opening Ceremonies. Shhh: No cheering or shouting at Tokyo Olympic torch relay. But clapping allowed. |Simon Denyer |February 25, 2021 |Washington Post 

Shortly after the TV series completed airing, the manga ended an 11-year-streak held by One Piece to become Japan’s best-selling manga of the year in 2019, according to the Oricon chart. Everything to Know About Demon Slayer: The Manga, TV Series and Record-Breaking Film |Kat Moon |February 24, 2021 |Time 

The senator singles out Japan as a place that could buy more American goods, and points to Malaysia and Vietnam as having labor forces that could produce these goods at competitive prices. Tom Cotton’s big plan to “beat China,” explained |Alex Ward |February 19, 2021 |Vox 

Former prime minister Shinzo Abe, who was the driving force behind Japan’s Games, resigned last year because of poor health. Female Olympian to head Tokyo Games after predecessor’s ouster for sexist remarks |Simon Denyer |February 18, 2021 |Washington Post 

The 84-year-old Kawabuchi played soccer for Japan in the 1964 Olympics and now heads the athletes’ village for the Games. Japan Olympics chief who said women talk too much will resign over remarks, reports say |Simon Denyer, Julia Mio Inuma |February 11, 2021 |Washington Post 

In Japan, one woman said she liked the experience of marrying herself as an exercise in pampering. Why Singles Should Say ‘I Don’t’ to The Self-Marriage Movement |Tim Teeman |December 30, 2014 |DAILY BEAST 

It happened on Glee and in Sex and The City, and now in Japan women can marry themselves. Why Singles Should Say ‘I Don’t’ to The Self-Marriage Movement |Tim Teeman |December 30, 2014 |DAILY BEAST 

When the U.S. went to war with Japan, American movies disappeared. Propaganda, Protest, and Poisonous Vipers: The Cinema War in Korea |Rich Goldstein |December 30, 2014 |DAILY BEAST 

What started as a confectionary is now the oldest know restaurant in Japan. Inside The World’s 10 Oldest Restaurants |Justin Jones |December 20, 2014 |DAILY BEAST 

Further, unlike leagues in Japan, South Korea or Mexico, there are no foreign players. Is Major League Baseball Ready For Cuba’s Players? |Ben Jacobs |December 19, 2014 |DAILY BEAST 

I shipped for a voyage to Japan and China, and spent several more years trying to penetrate the forbidden fastnesses of Tibet. The Boarded-Up House |Augusta Huiell Seaman 

Fujiyama, the noted volcano of Japan, is twelve thousand three hundred and sixty-five feet high. Assimilative Memory |Marcus Dwight Larrowe (AKA Prof. A. Loisette) 

The embassy for Japan—with a gift, which shall not seem an acknowledgment—you say, could not be sent off last year, which is well. The Philippine Islands, 1493-1898, Volume XX, 1621-1624 |Various 

The trench mortars—bomb guns they call them—will be ready in Japan in two and a half months' time. Gallipoli Diary, Volume I |Ian Hamilton 

Now we're saddled with about thirty thousand of them, and more coming on every steamer from Honolulu and Japan. Ancestors |Gertrude Atherton