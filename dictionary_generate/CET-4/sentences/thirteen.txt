Only about twelve or thirteen minutes had passed since she left Judi, though adrenaline made it feel like hours. The Women Who Fought to Defend Their Homes Against ISIS |Gayle Tzemach Lemmon |February 22, 2021 |Time 

She began her career as a model at the age of thirteen, modeling for Dolce & Gabbana and Dior. ‘Spectre’ Casts 50-Year-Old Bond Girl For 007 to Do Sex To |Amy Zimmerman |December 5, 2014 |DAILY BEAST 

At thirteen years old, de Forest speaks with the confidence and the knowledge of someone much older. Blessed or Cursed? Child Prodigies Reveal All |Justin Jones |November 17, 2014 |DAILY BEAST 

Last year, I went to India with my daughter who was thirteen at the time to meet some artisans. Q&A With Designer Rachel Roy |Cynthia Allum |November 3, 2014 |DAILY BEAST 

For him to get killed in action thirteen years after the war began. Beast Fiction: They Killed Our Brothers |J.F. Quinn |September 14, 2014 |DAILY BEAST 

Thirteen years after 9/11, the World Trade Center is undergoing a massive transformation. The Daily Beast’s Best Longreads, Sept 8-14 |John Boot |September 14, 2014 |DAILY BEAST 

Fatigue he never knew, and on one occasion he was said to have spent thirteen days and nights in the saddle. Napoleon's Marshals |R. P. Dunn-Pattison 

Tombs had two horses shot under him, and thirteen out of fifty men in his battery were killed or wounded. The Red Year |Louis Tracy 

Congress declared the authority of England over the thirteen colonies abolished. The Every Day Book of History and Chronology |Joel Munsell 

At the end of 1881 there were 93,776 children in the borough between the ages of three and thirteen. Showell's Dictionary of Birmingham |Thomas T. Harman and Walter Showell 

Colonel Guitar reported his loss in the battle as thirteen killed and fifty-five wounded. The Courier of the Ozarks |Byron A. Dunn