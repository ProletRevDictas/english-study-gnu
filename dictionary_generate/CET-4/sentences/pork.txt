Feast on heaping plates of slow-smoked pulled pork, sausages, ribs, and chicken in the charming courtyard of Atlantic Brewing Company’s Town Hill location. The Ultimate Acadia National Park Travel Guide |Virginia M. Wright |February 8, 2021 |Outside Online 

Chicken feet, pork bones, scallions and ginger inform the hot bath, good to the last slurp. Tom Sietsema’s 8 favorite places to eat right now |Tom Sietsema |January 26, 2021 |Washington Post 

Today, China consumes 28% of the world’s meat, including half of all pork. How China Could Change the World by Taking Meat Off the Menu |Charlie Campbell/Shanghai |January 22, 2021 |Time 

The Rath Packing Company, a family-owned meat company, opens a pork processing plant in Waterloo on the banks of the Cedar River. How the History of Waterloo, Iowa, Explains How Meatpacking Plants Became Hotbeds of COVID-19 |by Bernice Yeung and Michael Grabell |December 21, 2020 |ProPublica 

It also noted that the closure would disrupt the country’s pork supply. As COVID-19 Ravaged This Iowa City, Officials Discovered Meatpacking Executives Were the Ones in Charge |by Michael Grabell and Bernice Yeung |December 21, 2020 |ProPublica 

Roll the pork over the stuffing, like a jelly roll, until the seam is facing down and the fat back is on top. Make Carla Hall’s Roasted Pork Loin With Cranberries |Carla Hall |December 24, 2014 |DAILY BEAST 

While the pork is resting, heat a large, heavy-bottomed pan over medium-high heat. Make Carla Hall’s Roasted Pork Loin With Cranberries |Carla Hall |December 24, 2014 |DAILY BEAST 

Lay the butterflied pork loin on the cutting board with the fat cap facing down. Make Carla Hall’s Roasted Pork Loin With Cranberries |Carla Hall |December 24, 2014 |DAILY BEAST 

Veselka layered its latke with pork goulash, and Toloache added beef short rib chorizo. I Ate Potato Pancakes Til I Plotzed |Emily Shire |December 17, 2014 |DAILY BEAST 

Hitchcock's going on about English pork butchers and how best to prepare pork cracklings. Alfred Hitchcock’s Fade to Black: The Great Director’s Final Days |David Freeman |December 13, 2014 |DAILY BEAST 

Infection in man occurs from ingestion of insufficiently cooked pork, which contains encysted embryos. A Manual of Clinical Diagnosis |James Campbell Todd 

Perhaps she doesnt care for pork and potatoes, my friend, laughed the lady, eyeing Mr. Chumley whimsically. The Girls of Central High on the Stage |Gertrude W. Morrison 

He was a fat man—eating roast pork, and apple-sauce, and mashed potatoes, and bread. Love's Pilgrimage |Upton Sinclair 

Pork maun hae risen in price this last twa-three days, for I'm telt it was gaun cheap enough then. The Underworld |James C. Welsh 

I says to myself, if a body can get anything they pray for, why don't Deacon Winn get back the money he lost on pork? Adventures of Huckleberry Finn, Complete |Mark Twain (Samuel Clemens)