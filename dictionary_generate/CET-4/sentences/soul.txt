The Senate likes to call itself “the world’s greatest deliberative body,” and the filibuster, at least in myth, is the soul of that deliberation. The definitive case for ending the filibuster |Ezra Klein |October 1, 2020 |Vox 

Likewise, when celebrity chef Marcus Samuelsson, of Ethiopian and Swedish descent, opened soul food restaurant Red Rooster in Harlem, New York, in 2010, some criticized it for contributing to gentrification by catering to outsiders. The Stages of Gentrification, as Told by Restaurant Openings |Vince Dixon |September 30, 2020 |Eater 

All the same, Stone Locals is a compelling depiction of the soul of the sport. 3 New Documentaries to Stream This Fall |Erin Berger |September 21, 2020 |Outside Online 

As a Belarusian citizen, my heart hurts, and my soul is tired. A Proud Belarusian’s Heartbreak |Tracy Moran |August 18, 2020 |Ozy 

We started with a major open space idea as we sculpted our plan, and it has now rightfully emerged as the soul of the project. Public Benefits Define Midway Proposal |Frank Wolden, Aruna Doddapaneni and Colin Parent |July 21, 2020 |Voice of San Diego 

Education controls the transmission of values and molds the spirit before dominating the soul. Houellebecq’s Incendiary Novel Imagines France With a Muslim President |Pierre Assouline |January 9, 2015 |DAILY BEAST 

But damn, the music is catchy—a neo-soul aural assault of horns, electro swirls, yelps, funky basslines, and harmonized vocals. The 14 Best Songs of 2014: Bobby Shmurda, Future Islands, Drake, and More |Marlow Stern |December 31, 2014 |DAILY BEAST 

Ragtime, blues, country, jazz, soul, and rock and roll were all pioneered or inspired by black artists. The Cultural Crimes of Iggy Azalea |Amy Zimmerman |December 29, 2014 |DAILY BEAST 

The gathering of the thousands of cops had been a soul-stirring sight. Choking Back Tears, Thousands of Cops Honor Fallen Officer Ramos |Michael Daly |December 28, 2014 |DAILY BEAST 

You mix up English working-class gruffness with African-American soul from the Deep South. The Greatest Rock Voice of All Time Belonged to Joe Cocker |Ted Gioia |December 23, 2014 |DAILY BEAST 

But the liberal soul deviseth liberal things, and by liberal things shall he stand. The Life and Most Surprising Adventures of Robinson Crusoe, of York, Mariner (1801) |Daniel Defoe 

Words are often everywhere as the minute-hands of the soul, more important than even the hour-hands of action. Pearls of Thought |Maturin M. Ballou 

Monotheism is strictly inconsistent with the supremacy of “merits” which is the very soul of Oriental religion. Solomon and Solomonic Literature |Moncure Daniel Conway 

Some critics feel that, despite much that is desirable in her work, the soul is lacking in the women she paints. Women in the fine arts, from the Seventh Century B.C. to the Twentieth Century A.D. |Clara Erskine Clement 

David says, that his soul was full of trouble, and his life drew near unto the grave. The Life and Most Surprising Adventures of Robinson Crusoe, of York, Mariner (1801) |Daniel Defoe