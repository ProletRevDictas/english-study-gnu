Voters with language access needs may require an interpreter to help them with their ballot. A Guide to In-Person Voting vs. Mail-In Voting |by Cynthia Gordy Giwa |October 22, 2020 |ProPublica 

Become an interpreterIf you speak another language, it’s possible you may be able to enroll as an interpreter at the polls. Five ways to help democracy even if you can’t vote |Sandra Gutierrez G. |October 21, 2020 |Popular-Science 

Some voters may require an interpreter to help them with their ballot. Your Guide to Voting in Illinois |by ProPublica |October 8, 2020 |ProPublica 

While interpreters the world over are likely to have faced similar difficulties, the live broadcast provided an especially stark contrast with Japan, where a recent debate for the leadership of the ruling party was an infinitely more staid affair. Election live updates: Debate commission says it will change structure to ensure more ‘orderly discussion’ |John Wagner, Felicia Sonmez, Amy B Wang |September 30, 2020 |Washington Post 

That change came after officers failed to provide an interpreter to a deaf woman who was wrongfully arrested after her mother assaulted her. Will America’s Police Finally Listen to Deaf People? |Charu Kasturi |September 30, 2020 |Ozy 

Shadman started as a laborer, but within a year became an interpreter because he could speak English. Special Forces’ $77M ‘Hustler’ Hits Back |Kevin Maurer |December 8, 2014 |DAILY BEAST 

Our Pashto interpreter explained how he had pretended to be a Pakistani policeman when interested crowds approached the compound. I Shot Bin Laden |Elliot Ackerman |November 16, 2014 |DAILY BEAST 

The grunt takes a hard look at our interpreter, rotates his M16 and opens the vehicle door, motioning for us to get out. Heart of Darkness: Into Afghanistan’s Taliban Valley |Matt Trevithick, Daniel Seckman |November 15, 2014 |DAILY BEAST 

But he continued to study English every day and eventually was accepted as an interpreter. Obama Went to War to Save Them, But They Can’t Get U.S. Visas |Christine van den Toorn, Sherizaan Minwalla |September 28, 2014 |DAILY BEAST 

You guard the pontoon bridge with a squad of Iraqi Army soldiers and a single interpreter. Whatever You Do Someone Will Die. A Short Story About Impossible Choices in Iraq |Nathan Bradley Bethea |August 31, 2014 |DAILY BEAST 

Long before reason found the answer, instinct—swift, merciless interpreter—told him plainly. The Wave |Algernon Blackwood 

The Patriarch went to him, and, with the help of an interpreter, did for him what pertained to his office as a good Pastor. The Jesuit Relations and Allied Documents, Vol. II: Acadia, 1612-1614 |Various 

Several American officers were present on the occasion, accompanied by a Spanish half-caste who acted as their interpreter. The Philippine Islands |John Foreman 

Lowell was frankly worried as he sped away from the agency with Plenty Buffalo and the interpreter. Mystery Ranch |Arthur Chapman 

"Plenty Buffalo wants to know if you noticed all the pony tracks," said the interpreter. Mystery Ranch |Arthur Chapman