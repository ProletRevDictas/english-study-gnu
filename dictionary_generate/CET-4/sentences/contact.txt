Vox contacted Fischer’s office about why and when the system was deactivated but has not received a response. The policing reforms in the Breonna Taylor settlement, explained |Fabiola Cineas |September 17, 2020 |Vox 

Know when your state begins mailing ballots, track your ballot and contact your local election office if you haven’t received yours. ProPublica’s Pandemic Guide to Making Sure Your Vote Counts |by Susie Armitage |September 16, 2020 |ProPublica 

The last major Venus orbiter was ESA’s Venus Express, which studied Venus for eight years until engineers lost contact with it, likely because it ran out of fuel. We need to go to Venus as soon as possible |Neel Patel |September 16, 2020 |MIT Technology Review 

You’ll typically get options to jump straight into your playlists with a music app, or get quick links to your favorite contacts on a messaging app, to name a couple examples. Get around your phone more quickly than you already are |David Nield |September 16, 2020 |Popular-Science 

It’s not clear whether Quinn is still in contact with state elections officials. No Democrats Allowed: A Conservative Lawyer Holds Secret Voter Fraud Meetings With State Election Officials |by Mike Spies, Jake Pearson and Jessica Huseman |September 15, 2020 |ProPublica 

Ney said McDonnell needs to “keep a stiff lip” and stay in close contact with family members. Abramoff’s Advice for Virginia’s New Jailhouse Guv |Tim Mak, Jackie Kucinich |January 7, 2015 |DAILY BEAST 

The spokesman also said that Ambassador King “did not view the movie and did not have any contact directly with Sony.” Exclusive: Sony Emails Say Studio Exec Picked Kim Jong-Un as the Villain of ‘The Interview’ |William Boot |December 19, 2014 |DAILY BEAST 

In fact, he was in contact with Lansky prior to converging from the hillside onto the streets of Havana. Will Hyman Roth Return to Havana With Normalized Relations? |John L. Smith |December 18, 2014 |DAILY BEAST 

She had been my point of contact as I was trying to get up there. A Whisky Connoisseur Remembers That First Sip of The Macallan | |December 10, 2014 |DAILY BEAST 

“Keeping in contact with those people, and mechanically getting them to the trial, those are the hard parts,” Risner said. Drug Smuggler Sues U.S. Over Dog Bite |Caitlin Dickson |December 10, 2014 |DAILY BEAST 

Before Ulm he nearly ruined Napoleon's combination by failing to get in contact with the enemy. Napoleon's Marshals |R. P. Dunn-Pattison 

It is sometimes indented, with its convex side in contact with the periphery of the cell. A Manual of Clinical Diagnosis |James Campbell Todd 

Other forms of contact rapidly oxidized and went out of business. The Recent Revolution in Organ Building |George Laing Miller 

In fact, it was to be expected of almost any man who happened to be thrown in contact with Lyn Rowan for any length of time. Raw Gold |Bertrand W. Sinclair 

There was no engine stopped on account of this accident; but I shall never let the fire come in contact again with the cast iron. Life of Richard Trevithick, Volume II (of 2) |Francis Trevithick