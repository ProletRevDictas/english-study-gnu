In December, regulators in Massachusetts accused Robinhood of using gamification — such as colorful confetti that appeared on-screen after deposits and trades — to “incentivize continuous and repeated engagement with the application.” The GameStop stock situation isn’t about populism. It’s about whether the market is ‘real.’ |Mikhail Klimentov |February 1, 2021 |Washington Post 

Now, in office for a little over three weeks, she is facing a second round of calls for her resignation after a string of reports revealed her repeated endorsements of political violence and extremism. Rep. Marjorie Taylor Greene’s endorsement of conspiracy theories, violence sparks calls for her resignation — again |Reis Thebault |January 27, 2021 |Washington Post 

It’s specifically designed to be reusable for repeated exploration of the moon, and it’s the only one of the three contenders with a horizontal crew cabin. NASA Will Soon Choose One of These 3 Landers to Go Back to the Moon |Vanessa Bates Ramirez |January 14, 2021 |Singularity Hub 

The repeated talk of problem passengers in Row 12 led the captain to mistakenly head toward 12,000 feet, not a higher altitude given by air traffic control to keep planes safely apart. Sneezed on, cussed at, ignored: Airline workers battle mask resistance with scant government backup |Michael Laris |January 1, 2021 |Washington Post 

Typically people get better and better in repeated breath holds, in part because their spleens are squeezing more oxygen-carrying red blood cells into circulation. How Does Your Brain Respond When You Hold Your Breath? |Alex Hutchinson |November 25, 2020 |Outside Online 

And yet Dempsey and others have repeatedly said ISIS cannot be defeated militarily. Pentagon Doesn’t Know How Many People It’s Killed in the ISIS War |Nancy A. Youssef |January 7, 2015 |DAILY BEAST 

Speaking of the literature you love, the Bloomsbury writers crop up in your collection repeatedly. Daphne Merkin on Lena Dunham, Book Criticism, and Self-Examination |Mindy Farabee |December 26, 2014 |DAILY BEAST 

Over the course of the year, Klaus would repeatedly, through word and deed, demonstrate his sympathies with Putin. Vaclav Klaus, Libertarian Hero, Has His Wings Clipped by Cato Institute |James Kirchick |December 22, 2014 |DAILY BEAST 

Images of the hotel crop up repeatedly in his paintings, sometimes plagued by bats or monsters. ‘All Good Cretins Go to Heaven’: Dee Dee Ramone’s Twisted Punk Paintings |Melissa Leon |December 15, 2014 |DAILY BEAST 

In it, Kraven the Hunter tracks down Spider-Man, shoots him repeatedly, and leaves him for dead, buried underground. Exclusive: Sony Hack Reveals Studio's Detailed Plans For Another ‘Spider-Man’ Reboot |William Boot |December 13, 2014 |DAILY BEAST 

But I hope at least to play to him a few times, and what is more important, to hear him play repeatedly. Music-Study in Germany |Amy Fay 

Lieutenant Colonel Buell, in command at Independence, although repeatedly warned, allowed himself to be surprised. The Courier of the Ozarks |Byron A. Dunn 

She has since been repeatedly employed as a nurse to Small-pox patients, without experiencing any ill consequences. An Inquiry into the Causes and Effects of the Variolae Vaccinae |Edward Jenner 

He was repeatedly a member of the Kentucky legislature, and served two years in congress. The Every Day Book of History and Chronology |Joel Munsell 

Mr. Osborne was repeatedly warned to leave the country, but he paid no attention to the warnings. The Courier of the Ozarks |Byron A. Dunn