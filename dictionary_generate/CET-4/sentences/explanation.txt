They may be accurate, or there may be a reasonable explanation for everything. I can’t trust my new co-worker, and I have a list of reasons. How do I tell management? |Karla Miller |February 11, 2021 |Washington Post 

Now, there could be several explanations for the absences this year. Why Big Tech sat out the Super Bowl |Aaron Pressman |February 8, 2021 |Fortune 

McIlroy told reporters the PGA Tour was informed Monday that a course volunteer acknowledged stepping on his ball while looking for it in the rough, providing an explanation of how the ball might have become embedded after taking a bounce. Rory McIlroy, Patrick Reed situations now appear very different with new information from PGA Tour |Des Bieler |February 4, 2021 |Washington Post 

Bill Murray is Bill Murray — the best possible explanation for why he was nominated for “On the Rocks,” but not co-star Rashida Jones. Golden Globe nominations 2021: ‘The Crown’ leads TV nominations while ‘Mank’ dominates movies |Emily Yahr, Bethonie Butler, Sonia Rao |February 3, 2021 |Washington Post 

In explanation, administrators have pointed to the nation’s inability to contain the coronavirus pandemic, but another factor may have been outspoken advocacy from teachers’ associations, which have pushed for remote learning from the start. Virginia’s largest school systems vow to reopen classrooms for all by March |Hannah Natanson |February 3, 2021 |Washington Post 

Have you looked around the American Dental Association website for an explanation of how fluoridation actually works? Anti-Fluoriders Are The OG Anti-Vaxxers |Michael Schulson |July 27, 2016 |DAILY BEAST 

This was later repurposed in Europe as an explanation for racial superiority, and the term “Aryan” came to define a white race. The Himalayas’ Hidden Aryans |Nina Strochlic |January 3, 2015 |DAILY BEAST 

That explanation is believable…but increasingly less so when you hear Jay talk about the nature of his relationship with Adnan. The Deal With Serial’s Jay? He’s Pissed Off, Mucks Up Our Timeline |Emily Shire |December 31, 2014 |DAILY BEAST 

Pakistani security officials offer a more nuanced explanation. The Dangerous Drug-Funded Secret War Between Iran and Pakistan |Umar Farooq |December 29, 2014 |DAILY BEAST 

The deacon said he is demanding an explanation from Williams. Exposed: The Gay-Bashing Pastor’s Same-Sex Assault |M.L. Nestel |December 21, 2014 |DAILY BEAST 

"There is no more war," Brion translated for Ulv, realizing that the Disan had understood nothing of the explanation. Sense of Obligation |Henry Maxwell Dempsey (AKA Harry Harrison) 

And yet, the acknowledged state of things here is a grave fact which challenges inquiry and demands explanation. Glances at Europe |Horace Greeley 

The explanation of his mysterious earlier moods offered itself with a clarity that was ghastly. The Wave |Algernon Blackwood 

If you were to have a frank explanation with her, Blanche would very soon throw Jim Crow out of the window. Confidence |Henry James 

"But it was n't a lie," Punch would begin, charging into a laboured explanation that landed him more hopelessly in the mire. Kipling Stories and Poems Every Child Should Know, Book II |Rudyard Kipling