Certainly, none; because every circumstance of Mr. Hastings's proceedings was systematical, and perfectly well known at Oude. The Works of the Right Honourable Edmund Burke, Vol. XI. (of 12) |Edmund Burke 

Scarcely any thing is involved in more systematical obscurity, than the rights of our ancestors, when they arrived in America. Novanglus, and Massachusettensis |John Adams 

Can your Lordships believe that this can be any other than a systematical, deliberate fraud, grossly conducted? The Works of the Right Honourable Edmund Burke, Vol. X. (of 12) |Edmund Burke 

The result wished for can be obtained by a systematical arrangement of the granulation of powder. Gunnery in 1858 |William Greener 

The preposterous notions of a systematical man who does not know the world, tire the patience of a man who does. The PG Edition of Chesterfield's Letters to His Son |The Earl of Chesterfield