Today’s two major parties have framed political competition since the middle of the 19th century — since the Republicans rose from the rubble of the Whigs. Will Senate Republicans allow their louts to rule the party? |George Will |February 12, 2021 |Washington Post 

Maybe in another century I would have been a witch and burned at the stake. Maria Guarnaschelli, influential cookbook editor, dies at 79 |Emily Langer |February 11, 2021 |Washington Post 

Later in the century mathematicians proved two other key facts that tied together scissors congruence and tiling. Undergraduates Hunt for Special Tetrahedra That Fit Together |Kevin Hartnett |February 9, 2021 |Quanta Magazine 

More than 125 countries, representing more than two-thirds of global emissions, have either adopted or are discussing a goal of reaching net zero emissions by the middle of the century, Höhne said. Countries must ramp up climate pledges by 80 percent to hit key Paris target, study finds |Brady Dennis |February 9, 2021 |Washington Post 

In China, as early as the 15th century, healthy people deliberately breathed smallpox scabs through their noses and contracted a milder version of the disease. Smallpox used to kill millions of people every year. Here’s how humans beat it. |Kelsey Piper |February 5, 2021 |Vox 

For more than a century, Americans have been fretting about these sorts of ghosts. Anti-Fluoriders Are The OG Anti-Vaxxers |Michael Schulson |July 27, 2016 |DAILY BEAST 

In the 21st century women are earning their equality every step of the way… including the bedroom. Career-Minded Women Turn to Male Escorts For No-Strings Fun and (Maybe) Sex |Aurora Snow |January 3, 2015 |DAILY BEAST 

Finally free of Japanese interference, Korea elected its first autonomous government in almost half a century. Propaganda, Protest, and Poisonous Vipers: The Cinema War in Korea |Rich Goldstein |December 30, 2014 |DAILY BEAST 

Warm milk mixed with a spoonful of fireplace ashes seemed to also be popular among 19th century England. History's Craziest Hangover Cures |Justin Jones |December 30, 2014 |DAILY BEAST 

The goal is to create a literary anatomy of the last century—or, to be precise, from 1900 to 2014. The 2014 Novel of the Year |Nathaniel Rich |December 29, 2014 |DAILY BEAST 

El Imparcial maintained that he was worthy of being honoured as a 19th century conquering hero. The Philippine Islands |John Foreman 

It is a lofty and richly-decorated pile of the fourteenth century; and tells of the labours and the wealth of a foreign land. Blackwood's Edinburgh Magazine, Volume 60, No. 372, October 1846 |Various 

We live in an age that is at best about a century and a half old—the age of machinery and power. The Unsolved Riddle of Social Justice |Stephen Leacock 

John of Damascus, an important Greek theologian of the eighth century, often cited by Thomas. The Mediaeval Mind (Volume II of II) |Henry Osborn Taylor 

During the last century these were recast, and addition made to the peal, which now consists of twelve. Showell's Dictionary of Birmingham |Thomas T. Harman and Walter Showell