According to a poll we conducted in partnership with the market research firm Ipsos in early May, basketball is one of the sports with the highest share of “major” or “casual” fans identifying as Democrats. Why A Strike For Racial Justice Started With The Milwaukee Bucks And The NBA |Neil Paine (neil.paine@fivethirtyeight.com) |August 27, 2020 |FiveThirtyEight 

If you take even a casual interest in cryptocurrency markets, there’s a good chance you’ve heard the term DeFi, which is short for decentralized finance. Crypto soars again as traders embrace ‘DeFi’ and ‘yield farming’—but some see echoes of the 2017 bubble |Jeff |August 25, 2020 |Fortune 

Outfitters like Columbia and Patagonia have long made casual frocks meant for traveling or lounging at camp. In Praise of the Adventure Dress |Alison Van Houten |August 22, 2020 |Outside Online 

Modern dress—slacks, dress shirts, the uniform of casual businessmen. The first murder |Katie McLean |August 19, 2020 |MIT Technology Review 

That was so odd when that came across the timeline in such a casual way. The Suns Are On Fire, The Sixers Are Reeling, And Other Lessons From The NBA Bubble |Chris Herring (chris.herring@fivethirtyeight.com) |August 13, 2020 |FiveThirtyEight 

After four or five months of casual interaction, they realized they both had lost a young parent to cancer. Everyone at This Dinner Party Has Lost Someone |Samantha Levine |January 6, 2015 |DAILY BEAST 

And it was such casual racism by people in the halls of power. Ava DuVernay on ‘Selma,’ the Racist Sony Emails, and Making Golden Globes History |Marlow Stern |December 15, 2014 |DAILY BEAST 

Oh, the heaven and hell wrought by the casual use of a pronoun. Owning Up to Possession’s Downside |Samantha Harvey |December 14, 2014 |DAILY BEAST 

He was not a man given to casual affectionate display; the moment was charged with emotion. Alfred Hitchcock’s Fade to Black: The Great Director’s Final Days |David Freeman |December 13, 2014 |DAILY BEAST 

They “hook up” in a manner that makes the casual sex of the 1960s seem like an arranged marriage in Oman. Up to a Point: They Made Me Write About Lena Dunham |P. J. O’Rourke |December 13, 2014 |DAILY BEAST 

"I was just going to ask you if you all came through together," he observed, in a casual tone. Raw Gold |Bertrand W. Sinclair 

The intensity of this drama, however, being interior, caused little outward disturbance that casual onlookers need have noticed. The Wave |Algernon Blackwood 

It was painfully evident to the most casual observer, that she had died of absolute starvation. The World Before Them |Susanna Moodie 

There must be something therefore in the bow, as well as in the violin, more than meets the eye of a casual observer. Violins and Violin Makers |Joseph Pearce 

A casual search of the bar and back room revealed both nearly empty, a natural condition just before dawn. Fee of the Frontier |Horace Brown Fyfe