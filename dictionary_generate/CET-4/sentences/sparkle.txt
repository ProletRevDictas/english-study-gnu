This was just a dream, a sparkle in our eyes 10 years ago, that we would be able to sequence this many genomes. New Fish Data Reveal How Evolutionary Bursts Create Species |Elena Renken |December 1, 2020 |Quanta Magazine 

Drawn by the sparkle and pizzazz, West Virginians made the Hollywood a huge success. Valuation: Casino stocks got clobbered by COVID-19. This one is now an attractive bet |matthewheimer |October 18, 2020 |Fortune 

Despite the sparkle of the tech economy, there is no guarantee that the new jobs that might arrive will match that standard. After the boom: Canada’s oil capital faces an uncertain future |kdunn6 |September 21, 2020 |Fortune 

The fun part and being with their peers is where we’re seeing the sparkle come back in their eyes. The Learning Curve: One School District Stayed Open – and Didn’t Have Problems |Will Huntsberry |August 13, 2020 |Voice of San Diego 

The crown jewel of India’s banking system is losing its sparkle. India’s once squeaky-clean HDFC Bank is now facing “strategic failure” |Prathamesh Mulye |August 4, 2020 |Quartz 

Sitting in that plastic pitcher, she said, the water seemed to sparkle. Are Water Filters B.S.? |Michael Schulson |August 19, 2014 |DAILY BEAST 

Travelers are in danger of losing the “sparkle in their eyes” if they know too much in advance. Do Michelin Stars Still Matter? |Brandon Presser |May 8, 2014 |DAILY BEAST 

Sascha Hertli, chief executive of Rococo Dessous, discovered the missing sparkle while a consultant in oil-rich Qatar. Lingerie for the 1 Percent |CNBC |August 6, 2013 |DAILY BEAST 

I got back in shape when I moved to Los Angeles, and with that went most of my Roger Sterling sparkle. The Return of the Power Paunch |Sean Macaulay |May 1, 2013 |DAILY BEAST 

But he took the “sparkle” just before driving Shrimpton down a narrow winding mountain road. Speed Read: 11 Juiciest Bits From Philip Norman’s Biography of Mick Jagger |The Daily Beast |October 1, 2012 |DAILY BEAST 

It warmed the heart of Marcelle, too, and made her cheeks glow and her eyes sparkle—and added a rosier color to her lips. The Real Latin Quarter |F. Berkeley Smith 

It certainly does not sparkle now, but it must have come of a witty stock, and have boasted a mirth-provoking pedigree. A Cursory History of Swearing |Julian Sharman 

The sunshine was hot, the snow was brilliantly white, and seemed to sparkle as if covered with diamonds. Rudy and Babette |Hans Christian Andersen 

Through the broad leaves of the trees showed the night sky, pale with moonlight and the sparkle of the stars. The Rake's Progress |Marjorie Bowen 

But here the sparkle in the wine had died, leaving the cup that had brimmed flat and dull and only half full after all. The Relief of Mafeking |Filson Young