Our last game we were coming off Lewis & Clark, and that was a tough game. For this college football team, covid means the season starts in February — with Senior Day |Glynn A. Hill |February 11, 2021 |Washington Post 

This is a way for animals to break down tough plant material like grass. How do you build a centaur? |Bethany Brookshire |February 10, 2021 |Science News For Students 

He spent the entire ride explaining how he was tough on the gloves while he skied but rewaterproofed them as often as once a week during the season. The Best Ski Gloves Are the Simplest Ones |Joe Jackson |February 10, 2021 |Outside Online 

If not, gathering about 14,000 signatures in 120 days will be very tough. Morning Report: The Deal With the Jen Campbell Recall |Voice of San Diego |February 9, 2021 |Voice of San Diego 

I learned some very tough lessons during some very tough times. Republican Rep. Nancy Mace on the Capitol insurrection: ‘We need to rebuild our party’ |KK Ottesen |February 9, 2021 |Washington Post 

His flesh is sagging a bit, but he is still trim and looks lean, sinewy and tough. The Story Behind Lee Marvin’s Liberty Valance Smile |Robert Ward |January 3, 2015 |DAILY BEAST 

“You ask me my motivation,” Marvin says, moving back into his tough guy persona again. The Story Behind Lee Marvin’s Liberty Valance Smile |Robert Ward |January 3, 2015 |DAILY BEAST 

After a bunch of tough talk, this round of the hacker-on-hacker fight nevered materialized. The Attack on the Hidden Internet |Marc Rogers |December 29, 2014 |DAILY BEAST 

Although tough environmental controls were put in place in 2000, enforcement has been haphazard. Goodbye, Bahamas. Hello, Havana! |Clive Irving |December 18, 2014 |DAILY BEAST 

It was also an occasion for voluptuary displays of tough-mindedness. The Media's Pro-Torture Cheerleaders |Jedediah Purdy |December 10, 2014 |DAILY BEAST 

But this paper was a very tough, fibrous substance, and would resist quite a heavy blow as well as keep out the cold. Our Little Korean Cousin |H. Lee M. Pike 

"Tough—but most of us have been there, one time or another," Goodell observed sympathetically; and with that the subject rested. Raw Gold |Bertrand W. Sinclair 

You know that I come of tough fiber—of that old Creole race of Pontelliers that dry up and finally blow away. The Awakening and Selected Short Stories |Kate Chopin 

Another tough-looking man ran out of the building and jumped into the red car. Motor Matt's "Century" Run |Stanley R. Matthews 

But it was tough on Clip to run into a relative and find him passing smoke-signals along for that prince of rascals, Dangerfield. Motor Matt's "Century" Run |Stanley R. Matthews