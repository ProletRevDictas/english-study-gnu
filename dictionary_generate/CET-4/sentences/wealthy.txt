It’s still really, really, really expensive to send people into space, and those who are not bankrolled by a government or a wealthy company have to pay their own way. When will we see ordinary people going into space? |Neel Patel |September 9, 2020 |MIT Technology Review 

The more cowries were worn, the wealthier the wearer was believed to be. Lafalaise Dion Takes ESSENCE On A Personal Journey |Nandi Howard |September 4, 2020 |Essence.com 

Riding on the company’s soaring stock price, Musk is now wealthier than Mark Zuckerberg. Can Neuralink take a ride on Elon Musk’s reputation? |Lucinda Shen |September 1, 2020 |Fortune 

Another wide-open race can be found in Kennedy’s old 4th Congressional District, which stretches from wealthy Boston suburbs to working-class cities along the Rhode Island border. Today’s Elections In Massachusetts Are Another Big Test For The Progressive Movement |Nathaniel Rakich (nathaniel.rakich@fivethirtyeight.com) |September 1, 2020 |FiveThirtyEight 

India’s retail industry has become a racetrack for two of the world’s wealthiest men—and one might well be close to the finish line. Ambani versus Bezos: Who’s winning the battle for India’s $700 billion retail industry |Niharika Sharma |August 31, 2020 |Quartz 

A hundred ultra-wealthy liberal and conservative donors have taken over the political system. The 100 Rich People Who Run America |Mark McKinnon |January 5, 2015 |DAILY BEAST 

Little did I know that Lee had actually been born into a wealthy family. The Story Behind Lee Marvin’s Liberty Valance Smile |Robert Ward |January 3, 2015 |DAILY BEAST 

In March, police arrested a group of wealthy businessmen and government officials who were about to dine on illegal tiger meat. China’s Internet Is Freer Than You Think |Brendon Hong |December 27, 2014 |DAILY BEAST 

Not to those in power, not to the cruel and inhumane, not to the wealthy. Jesus Wasn’t Born Rich. Think About It. |Gene Robinson |December 25, 2014 |DAILY BEAST 

No surprise then that aside from wealthy coastal suburbs, the Democratic base has shrunk to the urban cores and college towns. Time to Bring Back the Truman Democrats |Joel Kotkin |December 21, 2014 |DAILY BEAST 

The tattered outcast dozes on his bench while the chariot of the wealthy is drawn by. The Unsolved Riddle of Social Justice |Stephen Leacock 

In the courts of princes and wealthy natives the vessels and tubes are lavishly adorned with precious metals. Tobacco; Its History, Varieties, Culture, Manufacture and Commerce |E. R. Billings. 

And my people shall sit in the beauty of peace, and in the tabernacles of confidence, and in wealthy rest. The Bible, Douay-Rheims Version |Various 

And the wealthy class by no means monopolized the bay with their yachts and luxurious launches. Ancestors |Gertrude Atherton 

James Pulteney, a wealthy English baron, died; whose income was $250,000 per annum. The Every Day Book of History and Chronology |Joel Munsell