Since then, the group has lost one-third of its value as its total market cap shrank to $792 billion. Despite Warren Buffett’s selloff, bank stocks look like great buys in this market |Shawn Tully |August 18, 2020 |Fortune 

Sometimes this is conveyed by declarations in all caps about how amazing the universe is. ‘The End of Everything’ explores the ways the universe could perish |Emily Conover |August 4, 2020 |Science News 

We found that smaller companies have been suffering, with the total market cap of companies worth $1-10 billion falling by 10% so far this year. Why Big Tech is riding high while the US stock market tanks |Dan Kopf |July 24, 2020 |Quartz 

People have lost their jobs and are left with no or limited sources of income, companies specifically small and medium cap businesses are shutting down. The impact of Coronavirus on digital marketing and effective solutions |Birbahadur Kathayat |July 23, 2020 |Search Engine Watch 

That might mean setting a price cap on what a landlord can charge or limiting the amount the rent can be raised. Why Rent Control Doesn’t Work (Ep. 373 Rebroadcast) |Stephen J. Dubner |March 12, 2020 |Freakonomics 

Each CAP, also known as an “orbit,” consists on four aircraft. Exclusive: U.S. Drone Fleet at ‘Breaking Point,’ Air Force Says |Dave Majumdar |January 5, 2015 |DAILY BEAST 

In fact, Clark fell back first from her blows, losing his cap, tie, and badge in the melee. Dr. King Goes to Hollywood: The Flawed History of ‘Selma’ |Gary May |January 2, 2015 |DAILY BEAST 

The cap devices on thousands of identical hats glinted in the late morning sun along with the shields worn by each of the cops. Choking Back Tears, Thousands of Cops Honor Fallen Officer Ramos |Michael Daly |December 28, 2014 |DAILY BEAST 

Lay the butterflied pork loin on the cutting board with the fat cap facing down. Make Carla Hall’s Roasted Pork Loin With Cranberries |Carla Hall |December 24, 2014 |DAILY BEAST 

Available at Amazon Vince Camuto Moto Baseball Cap, $34 Baseball caps may be useful on the road, but they scream “tourist!” The Daily Beast’s 2014 Holiday Gift Guide: For the Anthony Bourdain in Your Life |Allison McNearney |November 29, 2014 |DAILY BEAST 

At once cover the mouth of the tube with a filter-paper cap moistened with saturated aqueous solution of silver nitrate (1:1). A Manual of Clinical Diagnosis |James Campbell Todd 

With horror she had heard her brother addressed by a disreputable costermonger in a mangy fur cap, as "Old pal." The Pit Town Coronet, Volume I (of 3) |Charles James Wills 

"It's like that out here on the Riviera," said Jane, shaking her head so gloomily that the ruffled cap wobbled. Rosemary in Search of a Father |C. N. Williamson 

But his head was too hot to wear a thinking cap, and no story would come at his half-hearted call. Rosemary in Search of a Father |C. N. Williamson 

The more enthusiastic among the audience, male and female, also sport the red cap of liberty. The Pit Town Coronet, Volume I (of 3) |Charles James Wills