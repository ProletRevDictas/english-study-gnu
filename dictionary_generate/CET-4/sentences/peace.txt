California counties can start dealing with mail-in ballots 29 days before the election, but even if your state waits longer or doesn’t tally them until election day, it may still help your peace of mind to get it in early. Why you should vote as early as possible (and how to do it) |John Kennedy |September 17, 2020 |Popular-Science 

Since 2013, police officers have issued 83 tickets against people for speech that supposedly breaches the public peace. Morning Report: Punished for Pissing Off Police |Voice of San Diego |September 10, 2020 |Voice of San Diego 

The government claims they’re necessary for keeping the peace, particularly in areas like Kashmir, where there are regular outbreaks of violence. Podcast: How a 135-year-old law lets India shutdown the internet |Anthony Green |September 2, 2020 |MIT Technology Review 

The first two of those for the obvious purpose of keeping the peace. How Pseudoscientists Get Away With It - Facts So Romantic |Stuart Firestein |August 28, 2020 |Nautilus 

Make sure you have peace of mind at home and on the go with renters coverage from American Family Insurance. The Big Moment: Master the Move-in |Joshua Eferighe |August 25, 2020 |Ozy 

Domestically, the prime minister maintains the dubious line that he is the only man who can keep the still-fragile peace. Cambodia’s Smoke-and-Mirrors Democracy |David Shaftel |January 9, 2015 |DAILY BEAST 

They called for peace, reconciliation, and the safe return of Father Gregorio. Mexico’s Priests Are Marked for Murder |Jason McGahan |January 7, 2015 |DAILY BEAST 

The question is will we see regime changes in both Hamas and Israel that embrace a lasting peace? In the Middle East, the Two-State Solution Is Dead |Dean Obeidallah |January 2, 2015 |DAILY BEAST 

But without any peace talks on the horizon, everyone is now left to their own devices. In the Middle East, the Two-State Solution Is Dead |Dean Obeidallah |January 2, 2015 |DAILY BEAST 

At Christianity Today, Peter Chin claims Christians should preach peace instead of bogging down in the particulars of race. No Gods, No Cops, No Masters |James Poulos |January 1, 2015 |DAILY BEAST 

My son,” said Grabantak one evening to Chingatok, “if we are henceforth to live in peace, why not unite and become one nation? The Giant of the North |R.M. Ballantyne 

Impressed by the lugubrious scene, Aguinaldo yielded, and the next day peace negotiations were opened. The Philippine Islands |John Foreman 

Above all, he was amazed to hear me talk of a mercenary standing army in the midst of peace and among a free people. Gulliver's Travels |Jonathan Swift 

And it was no light task, then, for six hundred men to keep the peace on a thousand miles of frontier. Raw Gold |Bertrand W. Sinclair 

O death, how bitter is the remembrance of thee to a man that hath peace in his possessions! The Bible, Douay-Rheims Version |Various