Unfortunately, this change alone can only mitigate the danger of misleading health information, but does little to actually stop it. Facebook tries to clean up Groups with new policies |Sarah Perez |September 17, 2020 |TechCrunch 

Geologists are also warning of the danger of destructive debris flows. California wildfires may give way to massive mudslides |Ula Chrobak |September 17, 2020 |Popular-Science 

Halloween is still a long way off, but as summer ends, some Outside editors are getting in the spirit early with a new HBO horror series, an unsettling novel about the dangers of technology, and a book all about death rituals. Everything Our Editors Loved in August |The Editors |September 10, 2020 |Outside Online 

“We need another scale or some totally different way of warning of a danger,” says Masters. Slow, meandering hurricanes are often more dangerous—and they’re getting more common |Greta Moran |September 9, 2020 |Popular-Science 

Without privacy, we run the danger that someone will build The Ring and destroy society by ruling us all. Book recommendations from Fortune’s 40 under 40 in tech |Rachel King |September 4, 2020 |Fortune 

Kickstarter is one start-up platform that seems to have realized the danger. Design Your Own Dinosaur: The Era of Custom DNA |Justin Jones |January 8, 2015 |DAILY BEAST 

What had been the greatest asset of the paperback revolution,” observes Rabinowitz, “became its greatest danger. How Pulp Fiction Saved Literature |Wendy Smith |January 8, 2015 |DAILY BEAST 

He remained as hopeful as ever that he would himself join the NYPD, whatever the danger. In The Shadow of Murdered Cops |Michael Daly |December 26, 2014 |DAILY BEAST 

They work in a world filled with a sense—real or imagined—of danger lurking around each corner and every hallway. Any Outrage Out There for Ramos and Liu, Protesters? |Mike Barnicle |December 22, 2014 |DAILY BEAST 

The actions of North Korea this week should also send a clear message about the danger of this regime. The Sony Hack and America’s Craven Capitulation To Terror |David Keyes |December 19, 2014 |DAILY BEAST 

But the greatest danger I ever underwent in that kingdom was from a monkey, who belonged to one of the clerks of the kitchen. Gulliver's Travels |Jonathan Swift 

In particular the Governor of Adinskoy offered us a guard of fifty men to the next station, if we apprehended any danger. The Life and Most Surprising Adventures of Robinson Crusoe, of York, Mariner (1801) |Daniel Defoe 

Worst danger zone, the open sea, now traversed, but on land not yet out of the wood. Gallipoli Diary, Volume I |Ian Hamilton 

We got off our horses and stooped over the man, forgetting for the moment that danger might lurk in the surrounding thicket. Raw Gold |Bertrand W. Sinclair 

They soon retired, however, as the Fort was in danger of being attacked from another side. The Philippine Islands |John Foreman