Residue from fiber extraction is sent to farmers for mulching. How a Wasteland Shrub Is Becoming the Next Big Thing in Fashion |Daniel Malloy |August 28, 2020 |Ozy 

Such subseasonal forecasts can save farmers both time and money, since they don’t need to pay for irrigation when it’s not needed. Improved three-week weather forecasts could save lives from disaster |Alexandra Witze |August 27, 2020 |Science News 

I felt even more ridiculous doing the farmer’s walk exercise, where you’re just walking around, a kettlebell in each hand. The Gym-Free Pandemic Workout: Kettlebells, Indian Clubs, Sandbags, Oh My! |Eugene Robinson |August 25, 2020 |Ozy 

You could make clothes from it without dye and suppliers were often small, organic farmers. Scientists Gene-Hack Cotton Plants to Make Them Every Color of the Rainbow |Jason Dorrier |August 11, 2020 |Singularity Hub 

So, after the war, farmers were producing more food than was necessary. How the Supermarket Helped America Win the Cold War (Ep. 386 Rebroadcast) |Stephen J. Dubner |August 6, 2020 |Freakonomics 

“They are innocent of the charges leveled against them,” a statement issued by Farmer, who also represents the accused, said. Beaten By His Church for Being Gay |Justin Jones |December 16, 2014 |DAILY BEAST 

In Season 4, he abandons his “Ricktatorship,” assuming the role of farmer. The Walking Dead’s Luke Skywalker: Rick Grimes Is the Perfect Modern-Day Mythical Hero |Regina Lizik |October 28, 2014 |DAILY BEAST 

Humanitarian Paul Farmer and World Bank Leader Jim Yong Kim captured the problem in a Washington Post article last week. Why New York’s Ebola Case Will Hurt Infected Patients Everywhere |Abby Haglage |October 24, 2014 |DAILY BEAST 

Is playing ass-kicking Rick more fun than playing Farmer Rick? Andrew Lincoln Wants Rick to End With Johnny Cash and the Sunset |Melissa Leon |October 14, 2014 |DAILY BEAST 

One local farmer muttered to me it was like living next door to an airport—but one with a difference. U.S. Planes are Blowing the Hell out of ISIS at Kobani, But … |Jamie Dettmer |October 9, 2014 |DAILY BEAST 

E was an Esquire, with pride on his brow; F was a Farmer, and followed the plough. Boys and Girls Bookshelf; a Practical Plan of Character Building, Volume I (of 17) |Various 

And if he was worried about Farmer Green's cat, why didn't he dig a hole for himself at once, and get out of harm's way? The Tale of Grandfather Mole |Arthur Scott Bailey 

At that Farmer Green's cat began to run up and down between the rows of vegetables. The Tale of Grandfather Mole |Arthur Scott Bailey 

Farmer Green's cat had never liked Mr. Crow, for no particular reason. The Tale of Grandfather Mole |Arthur Scott Bailey 

The farmer told him it was six miles; "but," he added, "you must ride sharp, or you will get a wet jacket before you reach it." The Book of Anecdotes and Budget of Fun; |Various