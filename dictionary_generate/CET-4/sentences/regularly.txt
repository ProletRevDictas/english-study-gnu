Adams began regularly picking up garbage in June, but his daily walks became a ritual more than a decade ago as part of a rigorous exercise routine. The ‘garbage guy’ walks 12 miles a day around D.C. picking up trash: ‘I’ll pick up pretty much anything.’ |Sydney Page |February 11, 2021 |Washington Post 

So, if your Valentine is someone you’ve been seeing safely and regularly during the pandemic, you can pretty much act as if they’re your live-in partner. How to celebrate Valentine’s Day in a pandemic |Sara Kiley Watson |February 10, 2021 |Popular-Science 

However, for this content-centric subscriber retention strategy to work, a streamer needs to be able to regularly refresh its programming library. Future of TV Briefing: Streaming services count on content to keep subscribers acquired in 2020 |Tim Peterson |February 10, 2021 |Digiday 

Over the course of the year, Quinnipiac University regularly asked Americans how concerned they were about they or their families contracting the virus. What you’re saying when you say that covering the coronavirus is partisan |Philip Bump |February 9, 2021 |Washington Post 

Worst of all, he regularly wakes up my husband in the middle of the night just to announce that he needs to use the restroom. Miss Manners: Family member’s manners have gone to the dogs |Judith Martin, Nicholas Martin, Jacobina Martin |February 9, 2021 |Washington Post 

Someone who regularly rebels against the most socially sanctioned night of the year? The Refuseniks Hiding From ‘Happy New Year’ |Lizzie Crocker |December 31, 2014 |DAILY BEAST 

She also tracks his deteriorating health through the harrowing videos of the captives regularly released by the Nusra Front. A Sunni-Shia Love Story Imperiled by al Qaeda |Ruth Michaelson |December 26, 2014 |DAILY BEAST 

He did not plead guilty, and has regularly filed petitions in an effort to prove his innocence. Behind Bars for the Holidays: 11 Political Prisoners We Want to See Free In 2015 |Movements.Org |December 25, 2014 |DAILY BEAST 

I was briefly scared into eating regularly, but all too soon, the fears fade and my old habits return. You’re Never ‘Cured’ of an Eating Disorder |Carrie Arnold |December 20, 2014 |DAILY BEAST 

The family contends that Muataz was regularly abused and further threatened with rape. In Jerusalem Home Demolitions, the Biblical Justice of Revenge |Creede Newton |November 25, 2014 |DAILY BEAST 

The announcements of the meets in this and adjoining counties appear regularly in the Midland Counties' Herald. Showell's Dictionary of Birmingham |Thomas T. Harman and Walter Showell 

Sir Walter Scott smoked in his carriage, and regularly after dinner, loving both pipes and cigars. Tobacco; Its History, Varieties, Culture, Manufacture and Commerce |E. R. Billings. 

The engine is now working regularly twelve strokes per minute, with 60 lbs. Life of Richard Trevithick, Volume II (of 2) |Francis Trevithick 

Clean your tube regularly, and your amber mouthpiece with a feather dipped in spirits of lavender. Tobacco; Its History, Varieties, Culture, Manufacture and Commerce |E. R. Billings. 

Congress resolved to establish the bank of North America, being the first regularly established bank in the country. The Every Day Book of History and Chronology |Joel Munsell