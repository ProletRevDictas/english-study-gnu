Agencies need to be able to quickly justify why performance has changed and what steps can be taken to address these fluctuations — positive or negative. Solving the agency search intelligence gap |Ian O’Rourke and Stephen Davis |February 9, 2021 |Search Engine Watch 

Attorney Frank Crivelli, who said he negotiated contracts on behalf of police unions in at least 40 towns, said the dangers and challenges of police work justify the price in New Jersey. How the Police Bank Millions Through Their Union Contracts |by Andrew Ford, Asbury Park Press, and Agnes Chang, Jeff Kao and Agnel Philip, ProPublica |February 8, 2021 |ProPublica 

I planned to explore questions about journalistic ethics and whether the ends of getting a scoop that might change history and save lives can ever justify lying to a source. Seeing the Pentagon Papers in a New Light |by Stephen Engelberg |February 3, 2021 |ProPublica 

Why America is 'flying blind' to the coronavirus mutations racing across the globeStates cannot point to hard data to justify aggressive measures to contain the spread of dangerous mutations. U.S. response to coronavirus variants emphasizes masks and vaccines instead of lockdowns |Fenit Nirappil, Brittany Shammas |February 2, 2021 |Washington Post 

GameStop’s share price, which approached $500 at one point, had clearly become untethered from the financial metrics that traditionally justify rising prices. The GameStop stock craze is about a populist uprising against Wall Street. But it’s more complicated than that. |David J. Lynch |February 1, 2021 |Washington Post 

Does the sending of the message “justify” the tragedy that caused it? Cover-Ups and Concern Trolls: Actually, It's About Ethics in Suicide Journalism |Arthur Chu |January 3, 2015 |DAILY BEAST 

No more allowing people to justify their bigotry by spouting a cherry-picked Bible verse. Dear Leelah, We Will Fight On For You: A Letter to a Dead Trans Teen |Parker Molloy |January 1, 2015 |DAILY BEAST 

This story was used by some third-century North African Christians to justify the practice of women performing baptisms. First Anglican Woman Bishop A Return to Christian Roots |Candida Moss |December 18, 2014 |DAILY BEAST 

Like many other Pakistani Taliban, Jamal has his own horror stories to tell, which he believes can justify any bloody retribution. Pakistani School Killers Want to Strike the U.S. |Sami Yousafzai, Christopher Dickey |December 17, 2014 |DAILY BEAST 

With women put in front of the public to justify staying with bad men, we see these justifications in full bloom. Why Didn’t Camille Dump Bill Cosby? |Amanda Marcotte |December 17, 2014 |DAILY BEAST 

But this is quite enough to justify the inconsiderable expense which the experiment I urge would involve. Glances at Europe |Horace Greeley 

He may be a lessee, agent, or having such possession and control as would justify him in thus acting. Putnam's Handy Law Book for the Layman |Albert Sidney Bolles 

Put me in remembrance, and let us plead together: tell if thou hast any thing to justify thyself. The Bible, Douay-Rheims Version |Various 

Does the experience of the last ten years justify the country in placing confidence, on such a point, in a Whig Ministry? Blackwood's Edinburgh Magazine, No. CCCXXXIX. January, 1844. Vol. LV. |Various 

Should the alliance between the two professions be questioned, the following case will justify our assertion. The Book of Anecdotes and Budget of Fun; |Various