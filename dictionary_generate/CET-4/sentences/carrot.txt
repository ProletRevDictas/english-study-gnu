Filling ingredients can vary as long as they are sturdy enough to withstand boiling or frying, so a vegetarian option could include a napa cabbage mixture with carrots and shiitake mushrooms, or even chopped clear vermicelli noodles. Homemade dumplings bring me closer to family this Lunar New Year, even from far away |Marian Liu |February 5, 2021 |Washington Post 

For instance, Sebastian, or “Seb” for short, is a fan of belly rubs and carrot sticks. The latest thing on Zoom meetings: A live goat |Sydney Page |February 5, 2021 |Washington Post 

Yet China has won influence not by wielding sticks but by deftly distributing carrots. The U.S. and China Are Battling for Influence in Latin America, and the Pandemic Has Raised the Stakes |Charlie Campell/Beijing |February 4, 2021 |Time 

A great way to begin homemade soup is with mirepoix — a mixture of chopped onion, celery and carrots usually, although other root veggies can go in as well. Hints From Heloise: Before buying a diamond, learn about the four C’s |Heloise Heloise |February 4, 2021 |Washington Post 

The promise of comprehensive federal privacy law has been dangled like a carrot before consumer watchdogs and privacy advocates for at least a decade. Cheat sheet: What to expect in state and federal privacy regulation in 2021 |Kate Kaye |February 1, 2021 |Digiday 

Assad has been offered safe passage to a third country as a carrot for handing over power. U.S.: Assad’s ‘Machinery of Death’ Worst Since the Nazis |Josh Rogin |July 7, 2014 |DAILY BEAST 

His friends were safely known by the Polish versions of Toothy, Hoppy, Conky, Baldy, Whitey, Carrot Top and Chopper. The Week in Death: Irving Milchberg, the Teenage Gunrunner of the Warsaw Ghetto |The Telegraph |March 1, 2014 |DAILY BEAST 

Allergies—Mix one part cucumber juice with one part beet root juice and three parts carrot juice. Use These 15 Home Remedies Based On Ayurveda To Cure Menstrual Cramps, Hangovers, and Indigestion |Ari Meisel |January 21, 2014 |DAILY BEAST 

Its apple-carrot muffin is a moist and comforting treat that tastes like your grandmother baked it especially for you. The Sweet Side of New York City |Emily Chang |December 13, 2013 |DAILY BEAST 

I had never craved a carrot before, or fantasized about raiding an apple tree in a nearby garden. My Week At An Austrian Fat Camp |Owen Matthews |October 27, 2013 |DAILY BEAST 

"I have time before dark to make Benny's cart," observed Henry, biting a crisp, sweet carrot. The Box-Car Children |Gertrude Chandler Warner 

Put it into a stewpan with an onion, a carrot, a piece of celery; cover with water and boil slowly for fifty minutes. Dressed Game and Poultry la Mode |Harriet A. de Salis 

But while they stopped and waited for him to proceed, Bumper chewed away at his carrot until it was all gone. Bumper, The White Rabbit |George Ethelbert Walsh 

Who'd have thought Sam Lacey's carrot-top could be made over into that? The Opened Shutters |Clara Louise Burnham 

Marjorie had a horse made of a carrot, which looked like a very frisky steed, indeed. Marjorie's Busy Days |Carolyn Wells