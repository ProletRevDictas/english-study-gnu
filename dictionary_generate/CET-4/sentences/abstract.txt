In a pursuit whose meaning and purpose is abstract at the best of times, that’s not nothing. Stop Counting Your Running Mileage |Alex Hutchinson |August 28, 2020 |Outside Online 

“Our models can validate thousands of unseen candidates in seconds,” the study’s authors wrote in the abstract to their paper, which appears in the Monthly Notices of the Royal Astronomical Society. 50 new planets, including one as big as Neptune, are identified using A.I. |rhhackettfortune |August 26, 2020 |Fortune 

It also makes it more real and concrete, rather than abstract. Hurricanes have names. Some climate experts say heat waves should, too |Jack J. Lee |August 14, 2020 |Science News 

The same applies in fields of biology dealing with more abstract concepts of the individual — entities that emerge as distinct patterns within larger schemes of behavior or activity. What Is an Individual? Biology Seeks Clues in Information Theory. |Jordana Cepelewicz |July 16, 2020 |Quanta Magazine 

The Ising model represents one of the simplest places in this abstract “theory space,” and so serves as a proving ground for developing novel tools for exploring uncharted territory. The Cartoon Picture of Magnets That Has Transformed Science |Charlie Wood |June 24, 2020 |Quanta Magazine 

These matters are not mere threats to abstract constitutional principles. Politicians Only Love Journalists When They're Dead |Luke O’Neil |January 8, 2015 |DAILY BEAST 

Do you think that as we get older our thoughts shift to the more abstract, the music, than the definite, the lyrics? Belle & Sebastian Aren’t So Shy Anymore |James Joiner |January 7, 2015 |DAILY BEAST 

To listeners, Adnan is a real human while Jay remains an abstract figure. The Deal With Serial’s Jay? He’s Pissed Off, Mucks Up Our Timeline |Emily Shire |December 31, 2014 |DAILY BEAST 

In the mindset of the Coexist camp, those abstract beliefs have become twisted things, wrapped up with hate. COEXIST’s Bonehead Bumper-Sticker Politics |Michael Schulson |December 21, 2014 |DAILY BEAST 

“There will be flashbacks to that day, but I think it will be a reasonably abstract performance,” Berger said. The My Lai Massacre Inspires an Opera |Asawin Suebsaeng |December 17, 2014 |DAILY BEAST 

This work is now lost, and we know it only by the abstract given by Photius in the passage quoted. Blackwood's Edinburgh Magazine, No. CCCXXXIX. January, 1844. Vol. LV. |Various 

If you are thinking of making an Abstract of a particular book, awaken the utmost interest in regard to it before you begin. Assimilative Memory |Marcus Dwight Larrowe (AKA Prof. A. Loisette) 

Any other work of which an Abstract is published will serve the student as well as the above. Assimilative Memory |Marcus Dwight Larrowe (AKA Prof. A. Loisette) 

Three things are required: To learn how to abstract; To make one, at least, such abstract; and To learn it when made. Assimilative Memory |Marcus Dwight Larrowe (AKA Prof. A. Loisette) 

He never made any attempt to learn the abstract science of war, and until stirred by danger his character seemed to slumber. Napoleon's Marshals |R. P. Dunn-Pattison