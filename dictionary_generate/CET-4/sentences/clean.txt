It takes me the better part of an hour to load the dishwasher, hand-wash piles of other dishes, clean the counters and cooktop, and tidy the floor. Carolyn Hax: How to improve on ‘I cook, you clean’ without stirring the pot |Carolyn Hax |February 12, 2021 |Washington Post 

The player walks over to clean it up and walks away from Doyle. Jaguars’ Urban Meyer hires strength coach accused of mistreating players while at Iowa |Matt Bonesteel |February 11, 2021 |Washington Post 

Marty Schottenheimer, one of the NFL’s winningest coaches, dies at 77“It is our duty and our responsibility to present a clean case file to the prosecutor,” Capt. As police continue probe of Britt Reid crash, 5-year-old remains in critical condition |Rick Maese |February 9, 2021 |Washington Post 

He said that he’d isolate, then get a coronavirus test before coming into my home, showing up with a clean bill of health. Love, Loneliness, and the Chicken in My Freezer |Elazar Sontag |February 9, 2021 |Eater 

After that, they say, it’s safe to rehang feeders — if you’re diligent about cleaning them. Your dirty bird feeder could be spreading disease |Melissa Hart |February 9, 2021 |Washington Post 

With Charlie Hebdo, “you really have a clean case here,” Shearer said. Harry Shearer on The Dangerous Business of Satire |Lloyd Grove |January 8, 2015 |DAILY BEAST 

This is a guy who has his son-in-law clean his eyeglasses, for crying out loud. Will Chris Christie Regret His Cowboy Hug? |Matt Lewis |January 5, 2015 |DAILY BEAST 

Millions of dollars in renovation later the building is gorgeous—Clean, well-kept, organized. His First Day Out Of Jail After 40 Years: Adjusting To Life Outside |Justin Rohrlich |January 3, 2015 |DAILY BEAST 

“Clean as a whistle,” says a senior investigator involved in the case. Exclusive: Inside a Cop-Killer’s Final Hours |Michael Daly |December 31, 2014 |DAILY BEAST 

Clean-shaven and balding, Saleem is in his forties and walks with a limp. The Dangerous Drug-Funded Secret War Between Iran and Pakistan |Umar Farooq |December 29, 2014 |DAILY BEAST 

He shall give his mind to finish the glazing, and his watching to make clean the furnace. The Bible, Douay-Rheims Version |Various 

And I will turn my hand to thee, and I will clean purge away thy dross, and I will take away all thy tin. The Bible, Douay-Rheims Version |Various 

A groom is a chap, that a gentleman keeps to clean his 'osses, and be blown up, when things go wrong. The Book of Anecdotes and Budget of Fun; |Various 

The sun was palely shining upon dry, clean pavements and upon roads juicy with black mud. Hilda Lessways |Arnold Bennett 

Who could have believed that only a fortnight ago these same figures were clean as new pins; smart and well-liking! Gallipoli Diary, Volume I |Ian Hamilton