There needs to be a policy that mandates acoustic mitigation in the marine environment. Underwater Noise Pollution Is Disrupting Ocean Life—But We Can Fix It |Aryn Baker |February 5, 2021 |Time 

You can find out more about its marine science program here. Octopuses Find New Hunting Buddies - Issue 95: Escape |Brandon Keim |January 28, 2021 |Nautilus 

Sharks and rays play an important role as predators in marine ecosystems, as well as being a vital source of sustenance for many people. Sharks and rays are far less abundant in the world’s oceans than 50 years ago |Kate Baggaley |January 27, 2021 |Popular-Science 

We could create the largest marine protected area in the world. Can people protect as much space as nature needs? |Jonathan Lambert |January 21, 2021 |Science News For Students 

Another key question is whether the solar-powered water jets meant to clean off the oyster cages when they rise to the surface will be strong enough to remove marine organisms that may attach. Solar-powered barge could take oyster farming deeper into Chesapeake |Christine Condon |January 11, 2021 |Washington Post 

The Navy and Marine Corps versions of the F-35 have differing configurations and rely on an external gun pod. New U.S. Stealth Jet Can’t Fire Its Gun Until 2019 |Dave Majumdar |December 31, 2014 |DAILY BEAST 

And, thanks to a transparent hull, exploring the deep and spotting rare marine life is practically a cinch. The Most Exciting New Hotels, Restaurants, and Submarines of 2014 |Charlie Gilbert |December 29, 2014 |DAILY BEAST 

Among the scores of bystanders watching their small town turn into war zone was a Marine veteran who was close with Stone. Hunt for Iraq Vet After Killing Spree |M.L. Nestel |December 16, 2014 |DAILY BEAST 

The Marine reservist then went after his ex-wife, Nicole Hill Stone. Hunt for Iraq Vet After Killing Spree |M.L. Nestel |December 16, 2014 |DAILY BEAST 

Stone, according to Marine officials, served eight years in the U.S. Marine Corps Reserve. Hunt for Iraq Vet After Killing Spree |M.L. Nestel |December 16, 2014 |DAILY BEAST 

John Wilson, a celebrated landscape and marine painter, died at Folkstone, aged 81. The Every Day Book of History and Chronology |Joel Munsell 

It was unreasonable to expect those makers of marine steam-engines to report that Trevithick knew better than they did. Life of Richard Trevithick, Volume II (of 2) |Francis Trevithick 

REEF k, in latitude 14 degrees 47 minutes, has a dry sand upon it: its sub-marine extent was not ascertained. Narrative of a Survey of the Intertropical and Western Coasts of Australia] [Volume 2 of 2] |Phillip Parker King 

Those modern marine engines use about the same steam pressure and expand about in the same proportion. Life of Richard Trevithick, Volume II (of 2) |Francis Trevithick 

The effect of tidal movement in nurturing marine life is very great. Outlines of the Earth's History |Nathaniel Southgate Shaler