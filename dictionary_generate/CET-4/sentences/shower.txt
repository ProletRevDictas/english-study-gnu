VOSD contributor and guy-who-thinks-a-lot-about-bodily-fluids Randy Dotinga reports that sewage analysis is sophisticated enough to detect one infected person out of 100,000 who are flushing toilets and taking showers. Morning Report: Hotel Workers Want Their Jobs Back |Voice of San Diego |September 8, 2020 |Voice of San Diego 

In Mumbai, a phone vendor even marked the occasion with showers of confetti and cheerleaders. Apple’s iPhone sales have lagged in India for years. It’s only now unleashing its branding firepower |Grady McGregor |September 6, 2020 |Fortune 

This leave-on treatment absorbs quickly into your scalp, so you can continue on your day after a shower and tangibly feel the nourishing effects of its subtle ingredients such as chamomile and lavender. Scalp scrubs that banish scaly patches and build-up |PopSci Commerce Team |September 4, 2020 |Popular-Science 

We have a queen bed, a stove, an oven, a refrigerator and freezer, and even a flush toilet and shower on board. 5 Portable Grills for All Your Outdoor Cooking Needs |Amy Marturana Winderl |September 2, 2020 |Outside Online 

Then there’s virtual wedding showers and postponed celebrations. A guide to giving gifts for postponed and shrunken weddings |Brooke Henderson |August 20, 2020 |Fortune 

“Wait…” Suddenly a huge, graceful black marlin leaps out of the water, sending a shower of water ten feet high. The Story Behind Lee Marvin’s Liberty Valance Smile |Robert Ward |January 3, 2015 |DAILY BEAST 

I was so relieved, until I thought about my dirty pantyhose hanging on the shower at home. My Love Letter to the Stetson |Mark McKinnon |December 24, 2014 |DAILY BEAST 

His explanation only diminishes the irresistible excitement we feel while watching Tony Perkins peer at Janet Leigh in her shower. Alfred Hitchcock’s Fade to Black: The Great Director’s Final Days |David Freeman |December 13, 2014 |DAILY BEAST 

One measly year later, Pam woke to find a naked Ewing grinning at her in the shower. ‘The Walking Dead’ Fans Demand: Bring Back Beth! |Melissa Leon |December 11, 2014 |DAILY BEAST 

They go into the bathroom, and he asks her to sit by him in the shower. School Shooters Love This Pickup Artist Website |Brandy Zadrozny |December 5, 2014 |DAILY BEAST 

First a shower of shells dropping all along the lower ridges and out over the surface of the Bay. Gallipoli Diary, Volume I |Ian Hamilton 

The eye admireth at the beauty of the whiteness thereof, and the heart is astonished at the shower thereof. The Bible, Douay-Rheims Version |Various 

Then a shower of dirt flew into their faces and both Jolly Robin and his wife tumbled over backward. The Tale of Grandfather Mole |Arthur Scott Bailey 

Sick, trembling, her beautiful face humble and tearful enough now, she bent it on his shoulder in a shower of bitter tears. Elster's Folly |Mrs. Henry Wood 

The shells came from Asia and Achi Baba:—in a fiery shower, they fell upon the lines of our front trenches. Gallipoli Diary, Volume I |Ian Hamilton