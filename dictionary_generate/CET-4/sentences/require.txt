Students from lower-risk areas were required to arrive one week before classes began. Even the most cautious schools are seeing outbreaks |Sy Mukherjee |September 17, 2020 |Fortune 

That would still take two decades, and require a lot of workers and money. Suppressing fires has failed. Here’s what California needs to do instead. |James Temple |September 17, 2020 |MIT Technology Review 

It has been revised to require two flips to insert the edge. A New Algorithm for Graph Crossings, Hiding in Plain Sight |Stephanie DeMarco |September 15, 2020 |Quanta Magazine 

The company wasn’t required to keep workers on the payroll or stop returning money to shareholders. The Big Corporate Rescue and the America That’s Too Small to Save |by Lydia DePillis, Justin Elliott and Paul Kiel |September 12, 2020 |ProPublica 

These apps may not charge a maximum APR higher than 36%, including costs or fees and fees, or require payment in full in 60 days or less. Apple revises App Store rules to permit game streaming apps, clarify in-app purchases and more |Sarah Perez |September 11, 2020 |TechCrunch 

House rules require an absolute majority of members voting to choose a speaker. Kamikaze Congress Prepares to Strike Boehner |Ben Jacobs |January 6, 2015 |DAILY BEAST 

Limbaugh makes comments like this because his right-wing fans require a non–stop diet of race-baiting red meat. Rush Limbaugh’s Fear of a Black James Bond |Dean Obeidallah |December 29, 2014 |DAILY BEAST 

That makes New York the ninth state to require such coverage. The Insurance Company Promised a Gender Reassignment. Then They Made a Mistake. |James Joiner |December 29, 2014 |DAILY BEAST 

Chickens require significantly less land, water, and energy than all other meat options except farmed salmon. The History of the Chicken: How This Humble Bird Saved Humanity |William O’Connor |December 27, 2014 |DAILY BEAST 

Bohac said the bill does not require anyone to say “Merry Christmas” if they are not up for it. A Field General in the War on Christmas |David Freedlander |December 24, 2014 |DAILY BEAST 

The events which succeeded this fortunate capture are too well known to require more than a very brief recapitulation. Blackwood's Edinburgh Magazine, No. CCCXXXIX. January, 1844. Vol. LV. |Various 

While the test is somewhat tedious, all the manipulations are simple and require no apparatus but flasks, test-tubes, and funnels. A Manual of Clinical Diagnosis |James Campbell Todd 

We followed the upland past the end of the Stone till we found a slope that didn't require wings for descent. Raw Gold |Bertrand W. Sinclair 

Are you quite sure,” asked the missionary pointedly, “that you are supplied with everything else that you require? Hunting the Lions |R.M. Ballantyne 

The terms law and equity are frequently used in the law books and require explanation. Putnam's Handy Law Book for the Layman |Albert Sidney Bolles