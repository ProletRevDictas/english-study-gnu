The benefits for advertisers who reduce friction in the consumer journey are numerous, and they include the following outcomes. The race to frictionless consumer journeys is expanding beyond marketplaces |acuityads |September 10, 2020 |Digiday 

Governments are also reducing the frictions, fees, and charges that bog down the system. Smart stimulus: Cash as code |Claire Beatty |September 9, 2020 |MIT Technology Review 

There is only pressure and friction on the surface of the wing—also on the rest of the aircraft, but for the purposes of thinking about gliding, it’s enough to consider only the wing. The science behind how an aircraft glides |By Peter Garrison/Flying Mag |September 3, 2020 |Popular-Science 

Using a computer model, the team then calculated the friction each pattern would produce on ice, vinyl or hardwood floors. Shape-shifting cuts give shoes a better grip |Carolyn Wilke |July 14, 2020 |Science News For Students 

That reduces friction and lets the molecules slide past each other more easily. Astronauts may be able to make cement with their own pee |Lisa Grossman |June 16, 2020 |Science News For Students 

Higher shipping costs mean additional friction for companies working in the Canadian oil sands. Why the Keystone XL Pipeline May Not Be Built |Robert Bryce |November 19, 2014 |DAILY BEAST 

But as Garfield on television gained in popularity, the Peanuts connection became a source of friction. Garfield Television: The Cat Who Saved Primetime Cartoons |Rich Goldstein |November 5, 2014 |DAILY BEAST 

The friction between Israelis and Palestinians is more than 60 years old, with the UN in the middle. War of Words Between Israel and UN Continues |Betsy Pisik |August 10, 2014 |DAILY BEAST 

Due to heavy friction with the producers, the project fell through. ‘Guardians of the Galaxy’ Filmmaker James Gunn on His Glorious Space Opera and Rise to the A-List |Marlow Stern |August 3, 2014 |DAILY BEAST 

The media is going to want a horse race, and it is going to create friction between the base and her record. Can This Ornery Socialist Spoil the Clinton Coronation? |David Freedlander |July 2, 2014 |DAILY BEAST 

The controlling leaders being out of gear the machine did not run smoothly: there was nothing but friction and tension. Napoleon's Marshals |R. P. Dunn-Pattison 

There were usually six joints or sources of friction, between the key and the pallet. The Recent Revolution in Organ Building |George Laing Miller 

This, of course, increased the friction and necessitated the use of a still stronger spring. The Recent Revolution in Organ Building |George Laing Miller 

This of itself, without the friction, or load of water, is far more duty than ever was done before by an engine. Life of Richard Trevithick, Volume II (of 2) |Francis Trevithick 

Meanwhile great friction arose between the general and his new commander-in-chief. Napoleon's Marshals |R. P. Dunn-Pattison