In an air conditioner or refrigerator, you would then have to quickly remove this heat while keeping the alloy in its pliable, low-entropy phase. The Shape-Shifting Squeeze Coolers |Marcus Woo |August 24, 2020 |Quanta Magazine 

The best elastocaloric materials so far are shape memory alloys. The Shape-Shifting Squeeze Coolers |Marcus Woo |August 24, 2020 |Quanta Magazine 

When an external force pushes on the alloy while it’s in its rigid phase, the metal transitions to its pliable, lower-entropy phase. The Shape-Shifting Squeeze Coolers |Marcus Woo |August 24, 2020 |Quanta Magazine 

Once the force is removed, the alloy returns to its rigid, higher-entropy phase. The Shape-Shifting Squeeze Coolers |Marcus Woo |August 24, 2020 |Quanta Magazine 

Led by Jeehwan Kim, an associate professor of mechanical engineering, the researchers borrowed from principles of metallurgy to fabricate each memristor from alloys of silver and copper, along with silicon. Brain on a chip |Katie McLean |August 19, 2020 |MIT Technology Review 

In 2012, Li allegedly supplied the Iranians with 20,000 kilos of steel pipe and 1,300 aluminum alloy tubes. Tehran’s Chinese Missile Man |Jefferson Morley |June 9, 2014 |DAILY BEAST 

The same is now true with Barack Obama but on steroids, with the added alloy of race inserted into the mix. The Obama Haters Book Club: The Canon Swells |John Avlon |October 26, 2012 |DAILY BEAST 

The zirconium alloy will react with water to produce hydrogen and oxide, but it also produces heat that has to be removed. Japan Nuclear Crisis: What Is a Full Meltdown? |Josh Dzieza |March 15, 2011 |DAILY BEAST 

It shows a simple mind to acknowledge at court the smallest alloy of common blood, and not to set up for a nobleman. The 'Characters' of Jean de La Bruyre |Jean de La Bruyre 

The buckets, Fig. 3, are formed of some easily melted, but not too soft metal alloy which can be cast in plaster molds. The Boy Mechanic, Book 2 |Various 

An alloy is a mixture or medley, anything allowed is according to law, and hallow is the same word as holy. Archaic England |Harold Bayley 

A good deal of Vanadium alloy is used, and this is made in America. A Journey Through France in War Time |Joseph G. Butler, Jr. 

Of the two latter they formed an alloy, and made tools of the bronze. Mexico |Charles Reginald Enock