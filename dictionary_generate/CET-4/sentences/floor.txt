Finally, installing data centers on the ocean floor is, surprisingly, much faster than building them on land. Microsoft Had a Crazy Idea to Put Servers Under Water—and It Totally Worked |Vanessa Bates Ramirez |September 17, 2020 |Singularity Hub 

At home, many kids have more freedom to lie on the floor, pace around, move to another room or take breaks as needed. Healthy screen time is one challenge of distance learning |Kathryn Hulick |September 11, 2020 |Science News For Students 

The guy is swatting layups every other time down the floor, it seems. We Could Have Watched Raptors-Celtics Game 6 All Day |Chris Herring (chris.herring@fivethirtyeight.com) |September 10, 2020 |FiveThirtyEight 

I think we’ve really reset our floor for what monthly success and targets look like. ‘We’ve really reset our floor’: How The Atlantic gained 300,000 new subscribers in the past 12 months |Max Willens |September 10, 2020 |Digiday 

Westbrook’s poor shooting and inability to space the floor can grind Houston’s offense to a halt. The Rockets’ New Starting Lineup Is Just Scratching The Surface Of Its Potential |Michael Pina |September 8, 2020 |FiveThirtyEight 

It was a Senate floor soap opera over none other than a soap-opera producer. U.S. Embassies Have Always Been for Sale |William O’Connor |January 2, 2015 |DAILY BEAST 

When I saw the fire in the restaurant, I ran down to the floor below, where I was trapped between flames above and below. ‘We’re Going to Die’: Survivors Recount Greek Ferry Fire Horror |Barbie Latza Nadeau |December 29, 2014 |DAILY BEAST 

Twin girls, Greta and Grace, run around the floor in circles, wearing pink playsuits with tiny pink wings attached. A Sunni-Shia Love Story Imperiled by al Qaeda |Ruth Michaelson |December 26, 2014 |DAILY BEAST 

Instead, most of the suffering species ate insects on the forest floor. Mistletoe is the Vampire of Plants |Helen Thompson |December 21, 2014 |DAILY BEAST 

Its beautifully aged wooden exterior houses traditional floor seating and beautiful gardens typical of the area. Inside The World’s 10 Oldest Restaurants |Justin Jones |December 20, 2014 |DAILY BEAST 

She looked so sweet when she said it, standing and smiling there in the middle of the floor, the door-way making a frame for her. Music-Study in Germany |Amy Fay 

The inner ends of the burrows were enlarged with a depression in the floor, where the eggs were laid. Summer Birds From the Yucatan Peninsula |Erwin E. Klaas 

Her feet felt rooted to the floor in the wonder and doubt of this strange occurrence. The Bondboy |George W. (George Washington) Ogden 

Last night I saw Jean Baptiste lying prone upon the floor, and knew that she had beaten him down to it, and he had not resisted. The Homesteader |Oscar Micheaux 

Several able speakers had made long addresses in support of the bill when one Mr. Morrisett, from Monroe, took the floor. The Book of Anecdotes and Budget of Fun; |Various