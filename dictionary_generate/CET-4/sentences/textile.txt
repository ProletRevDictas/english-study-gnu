They created a series of headpieces out of everyday hardware, rope, and other textiles. Transformer unveils this year’s ‘Queer Threads’ exhibits |Parker Purifoy |October 22, 2020 |Washington Blade 

Founded in 2018, Reflect Innovation, a Joy Ventures portfolio company, has developed a portable interactive textile product that utilizes biofeedback to help people become aware of their emotional state and experience moments of calm. 5 companies that want to track your emotions |jakemeth |August 22, 2020 |Fortune 

Reflect founder Noga Sapir combined her passions for neuroscience and textile design to create this tool—a contrast to the many tech solutions on the market that feature hard surfaces and screens. 5 companies that want to track your emotions |jakemeth |August 22, 2020 |Fortune 

Certainly, previous declines in American manufacturing, such as the waves of textile and steel layoffs in the 1980s, could be linked more or less directly to gains in developing countries. Unmade in America |Tate Ryan-Mosley |August 14, 2020 |MIT Technology Review 

In recent weeks, several firms, including Aditya Birla Group’s Grasim Industries, Mumbai-based textile company Ruby Mills and suitings brand Donear, among others, have launched fabrics that promise to keep clothes virus-free. Indian textile makers are trying to survive Covid-19 by launching “anti-virus” fabrics |Hiren Mansukhani |July 7, 2020 |Quartz 

After World War I, unions began their losing and lethal battle with textile owners across the South. Glenn Beck Is Now Selling Hipster Clothes. Really. |Ana Marie Cox |December 20, 2014 |DAILY BEAST 

Vibrant color surfaced in textile and in beading and embroidery. How World Wars Made Females More Androgynous |Liza Foreman |July 22, 2014 |DAILY BEAST 

One artistic embrace of the textile comes with a specific political message. Shining a Spotlight on Mexico’s Iconic Textile—the Rebozo |Liza Foreman |June 16, 2014 |DAILY BEAST 

Japan had very rich tradition of clothing appreciation for wearing clothes and for textile. Comme Des Garçons, Kenzo, and More Japanese Designers at Paris Fashion Week |Liza Foreman |March 4, 2014 |DAILY BEAST 

Our goal is to bring global awareness to a Peruvian textile industry. Put Down That Cashmere. There’s a New Luxury Wool in Town |Ann Binlot |December 2, 2013 |DAILY BEAST 

The essential point in which it differs from any other known mineral consists in its being at once fibrous and textile. Asbestos |Robert H. Jones 

The textile industry in Virginia includes the spinning and processing of yarn and the weaving and finishing of material. Hallowed Heritage: The Life of Virginia |Dorothy M. Torpey 

There is no main staple of human food which is not grown; there is no material of textile industry which is not produced. Encyclopaedia Britannica, 11th Edition, Volume 4, Part 3 |Various 

On the afternoon of the following day we visited the textile museum. A Journey Through France in War Time |Joseph G. Butler, Jr. 

And the textile industry is well represented, as is brewing and distilling. Mexico |Charles Reginald Enock