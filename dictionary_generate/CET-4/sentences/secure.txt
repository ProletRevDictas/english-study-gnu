He said he did so to secure more personalized attention during his pre-fight camp. As Kamaru Usman edges toward UFC greatness, his former teammate wants to stop him |Glynn A. Hill |February 12, 2021 |Washington Post 

The irony is as India prepares a bill to ban bitcoin in India, the world is turning to our massive technical talent in India to secure and safeguard the bitcoin network. Jack Dorsey and Jay Z invest 500 BTC to make Bitcoin ‘internet’s currency’ |Manish Singh |February 12, 2021 |TechCrunch 

Surveillance video then shows Pence, along with his security detail and members of his family and staff, being evacuated down a Capitol stairway to a secure location away from the building. House impeachment managers emphasize the danger to Pence and other top officials in harrowing retelling of Jan. 6 attack |Amy Gardner, Karoun Demirjian, Felicia Sonmez, Paul Kane |February 11, 2021 |Washington Post 

They took advantage, securing the franchise’s second Super Bowl title and its first in 18 years. Buccaneers defeat Chiefs in Super Bowl LV, 31-9, as Tom Brady claims his seventh championship |Mark Maske |February 8, 2021 |Washington Post 

Its path to monopoly may have seemed organic to most, but the tactics the company used to secure such dominance are now under scrutiny. The future of Google and what it means for search |Pete Eckersley |February 5, 2021 |Search Engine Watch 

Their leader, Njie, still going by “Dave” during the operation, would stay a safe distance away until the State House was secure. The Shadowy U.S. Veteran Who Tried to Overthrow a Country |Jacob Siegel |January 6, 2015 |DAILY BEAST 

I need to resist my urge to talk them into my truth, just so I can feel more comfortable and secure. In 2015, Let’s Try for More Compassion |Gene Robinson |January 4, 2015 |DAILY BEAST 

If someone wants to ensure a direct and secure connection, no entity, whether a hotel or otherwise, should be able to block it. How ‘Ethical’ Hotel Chain Marriott Gouges Guests in the Name of Wi-Fi Security |Kyle Chayka |December 31, 2014 |DAILY BEAST 

However, we have just had a necessary wake-up call that all is not as secure as we believed. A Gift to the Jihadis: The Unseen Airport Security Threat |Clive Irving |December 27, 2014 |DAILY BEAST 

I want to bring this to your kind attention with the hope that you will help me secure an unbiased resolution. An American Marine in Iran’s Prisons Goes on Hunger Strike |IranWire |December 18, 2014 |DAILY BEAST 

Polavieja, as everybody knew, was the chosen executive of the friars, whose only care was to secure their own position. The Philippine Islands |John Foreman 

Nearly half the regiment ran to secure their picketed horses, armed themselves in hot haste, and galloped to the gaol. The Red Year |Louis Tracy 

The whole aim is to secure the development of character by the expression of the highest elements of character. Expressive Voice Culture |Jessie Eldridge Southwick 

Even genius, however, needs direction and adjustment to secure the most perfect and reliable results. Expressive Voice Culture |Jessie Eldridge Southwick 

Feeling secure regarding their happiness and welfare, she did not miss them except with an occasional intense longing. The Awakening and Selected Short Stories |Kate Chopin