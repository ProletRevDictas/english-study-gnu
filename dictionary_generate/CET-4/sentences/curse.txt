If there really was a curse, he should have been one of its first victims. What the mummy’s curse reveals about your brain |Kathryn Hulick |January 14, 2021 |Science News For Students 

The Jazz will use wings and forwards to set ball screens and confuse defenders, but the lion’s share of Mitchell’s attack involves a partnership with Gobert that’s both a gift and a curse. Can Donovan Mitchell Reach His Potential In The Bubble? |Michael Pina |August 3, 2020 |FiveThirtyEight 

Your already-simmering emotions leap into overdrive, and you lay on the horn and shout curses no one can hear. Cars Will Soon Be Able to Sense and React to Your Emotions |Vanessa Bates Ramirez |July 29, 2020 |Singularity Hub 

With their curse lifted, the Red Sox just kept winning over the next decade and a half. Nomar Garciaparra’s Full Career Wasn’t Enough For Cooperstown — But It Was Still Damn Good |Neil Paine (neil.paine@fivethirtyeight.com) |July 28, 2020 |FiveThirtyEight 

These mummies come complete with mazes and hieroglyphs and maybe a curse or two. Let’s learn about mummies |Bethany Brookshire |July 8, 2020 |Science News For Students 

A curse-filled half hour that saw my blood boil as my filing deadline ticked further into the past. J.K. Rowling Pens the Greatest Horror Story Ever: Dolores Umbridge Was Real |Kevin Fallon |October 31, 2014 |DAILY BEAST 

However, these “potty-mouthed princesses” curse like proverbial sailors to prove a point. Marcel the Shell Returns, Potty-Mouthed Princesses, and More Viral Videos |Alex Chancey |October 25, 2014 |DAILY BEAST 

His memory is encyclopedic--a curse for a man who feels persecuted. Will the Real Jim Palmer Please Stand Up |Tom Boswell |September 27, 2014 |DAILY BEAST 

For much of our political history, the “third term” curse was non-existent. Political Mythbusting: Third Term’s the Charm |Jeff Greenfield |August 24, 2014 |DAILY BEAST 

As it is, whatever worries will keep the next Democratic nominee up at night, that “third term curse” should not be one of them. Political Mythbusting: Third Term’s the Charm |Jeff Greenfield |August 24, 2014 |DAILY BEAST 

Seen thus poverty became rather a blessing than a curse, or at least a dispensation prescribing the proper lot of man. The Unsolved Riddle of Social Justice |Stephen Leacock 

A child, under exactly similar circumstances as far as its knowledge goes, cannot very well curse God and die. Kipling Stories and Poems Every Child Should Know, Book II |Rudyard Kipling 

He was given no reply save a muttered curse, a command to hold his tongue, and an angry tug at his tied arms. The Red Year |Louis Tracy 

And then he walked about the room, reflecting on the curse of his life—his besetting sin—irresolution. Elster's Folly |Mrs. Henry Wood 

The Jesuit expatiated on the curse of heaven, which now manifested itself on the head of the Duke in every relation of his life. The Pastor's Fire-side Vol. 3 of 4 |Jane Porter