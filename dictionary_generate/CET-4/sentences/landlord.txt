She had stopped at the counter to find out which courtroom she needed to go to for hearing about the money she still owed her former landlord. The stimulus relieved short-term pain, but eviction’s impact is a long haul |Kyle Swenson |February 8, 2021 |Washington Post 

By summer, the taproom and restaurant, its landlord and the Capitol Riverfront Business Improvement District had closed a parking lane on Half Street SE, filling the space with 17 picnic tables. ‘Streateries’ are thriving, but only in some parts of D.C. Now Bowser wants to expand them. |Michael Brice-Saddler, Fritz Hahn |February 1, 2021 |Washington Post 

The city has more workers than it can house in its own buildings, so it has long leased office space from private landlords. How a Volunteer Helped Get the City Into Its Biggest Real Estate Debacle |Lisa Halverstadt |January 29, 2021 |Voice of San Diego 

On Monday, Newsom and state leaders pledged to pay off 80 percent of the unpaid rent if landlords agree to forgive the remaining 20 percent. San Diegans Are Drowning in Water Debt During COVID-19 |MacKenzie Elmer |January 27, 2021 |Voice of San Diego 

Under the new enforcement policy, the city will work with landlords of persistent violators to bring them into compliance. North County Report: Carlsbad to Step Up Restaurant Enforcement |Kayla Jimenez |January 21, 2021 |Voice of San Diego 

To make matters worse, Kromah says his landlord has given him an eviction notice. This Man Lost 35 Relatives to Ebola and His Community Wants Him Gone |Wade C.L. Williams |October 30, 2014 |DAILY BEAST 

And people searching to take over a lease can select “landlord approved” apartments to streamline the process. From a Broken Lease, a Dream NYC Home |Lizzie Crocker |September 17, 2014 |DAILY BEAST 

Noor was in the summer camp workshops the day the landlord predator was discovered and ejected. The Young Girls Escaping the ISIS War |Chandra Kellison |September 16, 2014 |DAILY BEAST 

“I was warned by my landlord to leave his house before someone burns it down because of my gay work and sexuality,” he says. Uganda Gays Face New Wave of Fear Under Anti-Gay Bill |Caelainn Hogan |February 24, 2014 |DAILY BEAST 

One landlord even paid somebody to hurl a Molotov cocktail into an apartment just to smoke out tenants and jack up rents. The Celebrity You've Never Heard Of |Moral Courage |February 17, 2014 |DAILY BEAST 

Aristide washed and powdered Jean himself, the landlord lounging by, pipe in mouth, administering suggestions. The Joyous Adventures of Aristide Pujol |William J. Locke 

The landlord handed him his bill—'Two weeks board at five dollars—ten dollars.' The Book of Anecdotes and Budget of Fun; |Various 

"In a moment, sir," answered the landlord respectfully, and he turned again to the Parisian. St. Martin's Summer |Rafael Sabatini 

Before the distressed landlord could utter a word, the stranger had wheeled about again to face Garnache. St. Martin's Summer |Rafael Sabatini 

The landlord, an unstoppable gramophone of garrulity, entering by the street-door and bearing down upon him, put him to flight. The Joyous Adventures of Aristide Pujol |William J. Locke