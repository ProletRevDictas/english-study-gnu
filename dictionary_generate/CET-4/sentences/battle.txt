Creating a vaccine that is effective at least 80 percent of the time, which is the threshold the Goalkeepers model uses, is only part of the battle. A coronavirus vaccine will save more lives if we share it widely |Kat Eschner |September 17, 2020 |Popular-Science 

Both Lavrov and Pompeo used their visits to Cyprus to underscore the broader proxy battle at play. Butterfly Effect: The Next U.S.-Russia Conflict Theater Could be the Mediterranean |Charu Kasturi |September 17, 2020 |Ozy 

So, more than a few echoes of the Cold War and the battle between capitalism and socialism. Does Anyone Really Know What Socialism Is? (Ep. 408 Rebroadcast) |Stephen J. Dubner |September 17, 2020 |Freakonomics 

Creating the drug is only “part of the battle,” said Biden, who likened effective distribution to a complex military operation. Biden questions whether a vaccine approved by Trump would be safe |Sean Sullivan |September 16, 2020 |Washington Post 

Apple last week filed a countersuit to stop the game maker from using its own payment system for Fortnite, escalating one of the most closely watched legal battles in the tech sector. Apple says Epic is acting as ‘a saboteur, not a martyr’ in app store challenge |radmarya |September 16, 2020 |Fortune 

The U.S. military just unveiled a new effort to get Iraqis ready to battle ISIS. Pentagon Insider on New Plan to Fight ISIS: ‘Of Course It’s Not Enough’ |Nancy A. Youssef |January 6, 2015 |DAILY BEAST 

First, as opposition to gay marriage collapses, American anti-LGBT activists will slow their battle against it. ‘Only God’ Can Stop Gay Marriage |Tim Mak |January 6, 2015 |DAILY BEAST 

It is the steady accretion of detail that may yet be the most damaging factor in the battle for British hearts and minds. From Playboy Prince to Dirty Old Man? |Tom Sykes |January 5, 2015 |DAILY BEAST 

The audio message featured the words, “the real battle in Lebanon is yet to begin.” A Sunni-Shia Love Story Imperiled by al Qaeda |Ruth Michaelson |December 26, 2014 |DAILY BEAST 

The Daily Beast spoke to a jubilant League on Tuesday about the behind-the-scenes battle to get The Interview to movie theaters. The Inside Story of How Sony’s ‘The Interview’ Finally Made It to Theaters |Marlow Stern |December 23, 2014 |DAILY BEAST 

A Yankee, whose face had been mauled in a pot-house brawl, assured General Jackson that he had received his scars in battle. The Book of Anecdotes and Budget of Fun; |Various 

After the battle of the Pyramids he fell sick, and before the Syrian expedition, applied to return to France. Napoleon's Marshals |R. P. Dunn-Pattison 

He was a distinguished warrior under Francis I, mortally wounded at the battle of Marignan. The Every Day Book of History and Chronology |Joel Munsell 

It was under his auspices that the battle of Lepanto was fought, in which the Turks were so signally defeated. The Every Day Book of History and Chronology |Joel Munsell 

Again, she was present at the battle of Silan, where her heroic example of courage infused new life into her brother rebels. The Philippine Islands |John Foreman