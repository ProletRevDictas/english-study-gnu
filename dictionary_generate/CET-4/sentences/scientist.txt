Once the experiment is done, the platform sends a report to the scientists with the results. IBM has built a new drug-making lab entirely in the cloud |Karen Hao |August 28, 2020 |MIT Technology Review 

Just as the covid-19 pandemic was taking off, a global network of scientists began mapping the DNA of urban microbes and using AI to look for patterns. The scientists who swab subways for coronavirus |Tate Ryan-Mosley |August 27, 2020 |MIT Technology Review 

Newman is what scientists call a “supertaster,” and he is not alone in hating broccoli. Science of ‘Seinfeld’ |LGBTQ-Editor |June 15, 2020 |No Straight News 

For them, becoming a scientist was something completely out of my reach. This scientist wants to know how racial discrimination gets ‘under the skin’ |Esther Landhuis |March 18, 2020 |Science News For Students 

The scientists don’t know for sure exactly when each person began shedding the virus. Coronavirus most contagious before and right after symptoms emerge |Tina Hesman Saey |March 17, 2020 |Science News For Students 

My friend the political scientist Tom Schaller said all this back in 2008, in his book Whistling Past Dixie. Dems, It’s Time to Dump Dixie |Michael Tomasky |December 8, 2014 |DAILY BEAST 

Theda Skocpol, the esteemed Harvard social scientist, agrees with Cohen that they will set up the exchanges. Will GOP Govs Really Rescue Obamacare? |Michael Tomasky |November 12, 2014 |DAILY BEAST 

Darwin was a British Scientist who developed the theory of evolution and natural selection. ‘Gods of Suburbia’: Dina Goldstein’s Arresting Photo Series on Religion vs. Consumerism |Dina Goldstein |November 8, 2014 |DAILY BEAST 

A CDC scientist says he and his colleagues hid research regarding the MMR vaccine and autism. Anti-Vaxxers Have a New Hero |Russell Saunders |September 1, 2014 |DAILY BEAST 

For me, as a scientist, it all began in 1953 when I first tried scuba. ‘Mission Blue’ Warning: The Ocean Is Not Too Big to Fail |Sylvia A. Earle |August 15, 2014 |DAILY BEAST 

No Jewish historian nor scientist mentioned the rending of the veil of the temple, nor the rising of the saints from the dead. God and my Neighbour |Robert Blatchford 

“You will have to ask some scientist who has gone into the matter more deeply than I have,” Jessie said demurely. The Campfire Girls of Roselawn |Margaret Penrose 

The world-famous scientist, Herbert Spencer, says, "The universe had its origin in the unknown source of things." Gospel Philosophy |J. H. Ward 

This has attained to the scientist, and to many non-scientists, the level of a self-evident proposition. Man And His Ancestor |Charles Morris 

He applies practically in his work those laws which the scientist furnishes him with theoretically. The Painter in Oil |Daniel Burleigh Parkhurst