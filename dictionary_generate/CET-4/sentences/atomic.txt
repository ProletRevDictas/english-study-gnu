Many types of qubits are made from bits of metal or other material deposited on a surface, resulting in slight differences between qubits on an atomic level. ‘Designer molecules’ could create tailor-made quantum devices |Emily Conover |February 9, 2021 |Science News 

At this atomic level, the mechanics of quantum physics also come into play, with some seriously intriguing results. This ‘Quantum Brain’ Would Mimic Our Own to Speed Up AI |Shelly Fan |February 9, 2021 |Singularity Hub 

Part of that comes from the need to closely examine every relevant molecule, studying its chemical composition and interactions as well as its physical structure at the atomic level. Nanome raises $3 million to help scientists get up close with molecular structures in VR |Sophie Burkholder |February 9, 2021 |TechCrunch 

Instead, these market forces create opportunities for publishers to innovate by investing in owning the relationship with their users and looking to tap the “atomic” power unleashed when splitting first-party audience data from onsite inventory. Splitting the atom: Decoupling audience from inventory unleashes power of pubs |Trevor Grigoruk |February 9, 2021 |Digiday 

In 1911, at the University of Manchester in England, he deduced the existence of the atomic nucleus in analyzing results of experiments by his assistants Hans Geiger and Ernest Marsden. Top 10 science anniversaries to celebrate in 2021 |Tom Siegfried |February 3, 2021 |Science News 

Following this line of reasoning to its logical conclusion, the way to achieve world peace is to give everyone atomic bombs. Santa Fails One More Time |P. J. O’Rourke |December 27, 2014 |DAILY BEAST 

Transcripts from hearings held by the Atomic Energy Commission in 1954 have recently been declassified and studied by scholars. I Saw Nuclear Armageddon Sitting on My Desk |Clive Irving |November 10, 2014 |DAILY BEAST 

Producing one H-bomb would have diverted enough resources to produce 80 atomic warheads. I Saw Nuclear Armageddon Sitting on My Desk |Clive Irving |November 10, 2014 |DAILY BEAST 

In 1957 the U.S. Army first fielded artillery able to fire shells with atomic warheads. I Saw Nuclear Armageddon Sitting on My Desk |Clive Irving |November 10, 2014 |DAILY BEAST 

One of these critics was William Borden, executive director of the congressional joint committee on atomic energy. I Saw Nuclear Armageddon Sitting on My Desk |Clive Irving |November 10, 2014 |DAILY BEAST 

We only know that under certain conditions the old atomic associations break up, and new ones are formed. Outlines of the Earth's History |Nathaniel Southgate Shaler 

You have atomic weapons you intend using against your enemy—against the Eastern Empire? Restricted Tool |Malcolm B. Morehart 

Factories a long way under ground, behind the Soviet lines, factories that had once made atomic projectiles, now almost forgotten. Second Variety |Philip Kindred Dick 

Then the author tells us of the atomic hypothesis of the formation of the Great World. The Mirror of Literature, Amusement, and Instruction, No. 358 |Various 

Obviously this electric time impulsor is a machine in the nature of an atomic integrator. The Day Time Stopped Moving |Bradner Buckner