The closest thing Rutgers has to a bad loss is a stumble at Michigan State. NCAA tournament bracketology: After 30 years, Rutgers will get its chance at March Madness |Patrick Stevens |February 9, 2021 |Washington Post 

The metrics love Illinois, but it is a 9-5 team with a few stumbles at home. NCAA tournament bracketology: The bubble might be worse than ever this year. No, really. |Patrick Stevens |January 20, 2021 |Washington Post 

Leaders in California and elsewhere have made similar stumbles. Everyone failed on Covid-19 |German Lopez |December 18, 2020 |Vox 

Naturally, Big Tech’s retrenchment has heightened the debate over whether its leaders are vastly overpriced and long overdue for a steep slide, or simply suffering a temporary stumble. Will tech stocks stumble or slide? What the fundamentals tell us |Shawn Tully |September 16, 2020 |Fortune 

Jesse Marx and Lisa Halverstadt broke the news of Thompson’s departure and reviewed the list of stumbles that led to it. Morning Report: With Building Folly, City Real Estate Director Out |Voice of San Diego |August 4, 2020 |Voice of San Diego 

At 5:00 a.m. the clubs get going properly; the Forbes stumble down from their loggias, grinning and swaying tipsily. Russia’s Gold Digger Academy |Peter Pomerantsev |November 11, 2014 |DAILY BEAST 

A party that cannot make these decisions openly and confidently will stumble in 2016. What Republicans Need Right Now Is a Good Internal Fight |James Poulos |November 6, 2014 |DAILY BEAST 

Meanwhile, the labor unions and liberal groups that nominally backed Cuomo could not be more thrilled to see him stumble. Andrew Cuomo Can't Ignore It Now: He's Weak Even at Home |David Freedlander |September 10, 2014 |DAILY BEAST 

I stumble through interviews for my job at NY1, memories flooding back. Gaza, You're No Good For My Marriage |Josh Robin |August 9, 2014 |DAILY BEAST 

At the end of the fourth season premiere, Arya and The Hound stumble upon a tavern in the woods. Game of Thrones’ 8 Most Gruesome Deaths: From The Mountain’s Exploding Head Kill to Rat Torture |Marlow Stern |June 4, 2014 |DAILY BEAST 

You never know when you are going to stumble upon a jewel in the most out-of-the-way corner. Music-Study in Germany |Amy Fay 

And very many of them shall stumble and fall, and shall be broken in pieces, and shall be snared, and taken. The Bible, Douay-Rheims Version |Various 

And he shall turn his face to the empire of his own land, and he shall stumble, and fall, ans shall not be found. The Bible, Douay-Rheims Version |Various 

I feel feverish: my feet drag heavily, and I stumble against the railing. Prison Memoirs of an Anarchist |Alexander Berkman 

These four years through which I may—must stumble along with my hands tied, are a fair example. Ancestors |Gertrude Atherton