Still, no one likes to deal with a sore arm and an annoying headache for an entire day, so you might be tempted to take preventative painkillers before getting the vaccine. How to prepare for getting the COVID-19 vaccine |Tara Santora |February 8, 2021 |Popular-Science 

Adding to the city’s budget headaches is a ballooning annual pension payment that the city and its mayor have little control over. Gloria Eagerly Flagged Budget Issues But Is Less Eager to ID Solutions |Lisa Halverstadt |February 4, 2021 |Voice of San Diego 

The founders also left to their deputies future and ongoing antitrust headaches, as evidenced by Pichai being called to testify before Congress numerous times since the founders’ departure. With Bezos out as Amazon CEO, Zuckerberg is the last man standing |Elizabeth Dwoskin |February 3, 2021 |Washington Post 

Despite all the headaches that come with it, homeownership is still the American dream for many. Divvy Homes secures $110M Series C to help renters become homeowners |Mary Ann Azevedo |February 2, 2021 |TechCrunch 

Tiny screens require you to strain your eyes to make out fine details, so you’ll most likely end up with a headache, another common and annoying symptom of gaming sickness. Video games can cause motion sickness—here’s how to fight it |Sandra Gutierrez G. |February 2, 2021 |Popular-Science 

Within days of the first symptom, a headache, the patient was fighting for his life. The Daily Beast’s Best Longreads, Dec 8-14, 2014 |William Boot |December 13, 2014 |DAILY BEAST 

Why is such a simple countermeasure against headache and disease still so unpopular? Scroguard, Galactic Cap, and More: Will Anyone Wear Normal Condoms In the Future? |Samantha Allen |November 10, 2014 |DAILY BEAST 

Severe belly pains, a headache, and fever were the only conclusions Breman could draw. The Original Ebola Hunter |Abby Haglage |September 14, 2014 |DAILY BEAST 

For novice and fitness-enthusiasts alike, the Amiigo's intelligent pattern recognition alleviates a major headache at the gym. Amiigo Tracker Pairs Data With Deadlifts |Gregory Ferenstein |August 4, 2014 |DAILY BEAST 

His headache cured, the man went home and two months later, he was given a clean bill of health. Doctors Say Motörhead Is So Hardcore, They Could Make Your Brain Bleed |Brandy Zadrozny |July 3, 2014 |DAILY BEAST 

When the family returned from the court house, Orlean had retired at once, complaining of a headache. The Homesteader |Oscar Micheaux 

Another frequent symptom is repeated and constant headache, which, in the present series of cases, existed in 41.1 per cent. A Statistical Inquiry Into the Nature and Treatment of Epilepsy |Alexander Hughes Bennett 

A tightness took him about the heart, and behind his eyes that pulse of red darkness presaged the beginning of a violent headache. We're Friends, Now |Henry Hasse 

The fumes of the tobacco were carried by the air into the house, and brought back Ruth's sick headache. Ruth |Elizabeth Cleghorn Gaskell 

To the credit of Germany, I must say there was not a shadow of a headache the next morning. The Works of Robert Louis Stevenson - Swanston Edition Vol. 25 (of 25) |Robert Louis Stevenson