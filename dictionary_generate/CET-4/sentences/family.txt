Odds are you've spent more time with your family this year than ever before, which is great. Three Family-Friendly Adventures to Try This Fall |Outside Editors |September 17, 2020 |Outside Online 

In a statement, HHS said Caputo would be on leave for the next 60 days to “focus on his health and the well-being of his family.” Trump contradicts CDC director on vaccine; Biden says Americans shouldn’t trust Trump |Colby Itkowitz, Felicia Sonmez, John Wagner |September 16, 2020 |Washington Post 

Conversely, Recon’s campers are designed with families and small groups in mind. Can’t Afford a Sprinter? Get a Tiny Van Instead. |Emily Pennington |September 16, 2020 |Outside Online 

Coronavirus fears are also tied to how people plan to cast their ballots, with nearly 6 in 10 voters who are worried about a family member becoming infected saying they plan to vote early, compared with about 2 in 10 of those who are less worried. Post-ABC Wisconsin poll shows Biden holding narrow edge over Trump |Dan Balz, Emily Guskin |September 16, 2020 |Washington Post 

Biden spoke at the top of a roundtable discussion in Tampa with veterans and military families. Trump, in town hall, says he wouldn’t have done anything differently on pandemic |Colby Itkowitz, Josh Dawsey, Felicia Sonmez, John Wagner |September 16, 2020 |Washington Post 

Everywhere I go, ‘Hey Cartman, you must like Family Guy, right?’ Trolls and Martyrdom: Je Ne Suis Pas Charlie |Arthur Chu |January 9, 2015 |DAILY BEAST 

The third suspect, an 18-year-old named Hamyd Mourad, who turned himself in, is part of the same extended family. France Mourns—and Hunts |Nico Hines, Christopher Dickey |January 8, 2015 |DAILY BEAST 

Saved from the public gallows, Weeks was virtually exiled from the city, and wound up in Mississippi, where he raised a family. New York’s Most Tragic Ghost Loves Minimalist Swedish Fashion |Nina Strochlic |January 8, 2015 |DAILY BEAST 

A spokesman for Lewisham council said last year that it would be forced to act if the family returned to Britain. Britain May Spy on Preschoolers Searching for Potential Jihadis |Nico Hines |January 7, 2015 |DAILY BEAST 

Three on-the-record stories from a family: a mother and her daughters who came from Phoenix. I Tried to Warn You About Sleazy Billionaire Jeffrey Epstein in 2003 |Vicky Ward |January 7, 2015 |DAILY BEAST 

And that was that if he and his wife were to ever live together again and be happy, the family were to be kept out of it. The Homesteader |Oscar Micheaux 

“This house must have been the hotel of some distinguished family, Baron; it is nobly proportioned,” said David Arden. Checkmate |Joseph Sheridan Le Fanu 

What course was taken to supply that assembly when any noble family became extinct? Gulliver's Travels |Jonathan Swift 

The pig family did not know when Squinty would be taken away from them, and all they could do was to wait. Squinty the Comical Pig |Richard Barnum 

Mrs. Jolly Robin had often wished—when she was trying to feed a rapidly-growing family—that she could hunt forp. The Tale of Grandfather Mole |Arthur Scott Bailey