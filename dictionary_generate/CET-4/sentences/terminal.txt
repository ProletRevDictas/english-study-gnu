Its plans to build an LNG-export terminal in Louisiana took a blow at the end of March 2020, when Shell, its 50-50 partner in the project, decided to pull out, citing adverse market conditions. Rick Perry’s Ukrainian Dream |by Simon Shuster, TIME, and Ilya Marritz, WNYC |September 10, 2020 |ProPublica 

A few months later, the company signed a deal with Shell, the Dutch energy giant, to jointly develop the terminal at an estimated cost of about $11 billion. Rick Perry’s Ukrainian Dream |by Simon Shuster, TIME, and Ilya Marritz, WNYC |September 10, 2020 |ProPublica 

Coronavirus infections were linked to a beluga whale that experienced lung disease and terminal liver failure, and Pacific harbor seals that died from pneumonia in 2000. Everything we know—and don’t know—about human-to-animal COVID transmission |jakemeth |September 4, 2020 |Fortune 

This week, the company and several big name partners, including UPS and Penske, announced plans for an autonomous freight network of self-driving trucks, digitally mapped routes, terminals, and a central operations system to monitor the lot. TuSimple’s Robot Big Rigs Will Automate Freight Coast to Coast |Jason Dorrier |July 5, 2020 |Singularity Hub 

With the rise of flies as proven test subjects, when Rogulja became curious about terminal sleep deprivation, it again seemed like a plausible thing to study. Why Sleep Deprivation Kills |Veronique Greenwood |June 4, 2020 |Quanta Magazine 

But millions of rules result in perpetual error, and, as a terminal side effect, make leadership and accomplishment illegal. Red Tape Is Strangling Good Samaritans |Philip K. Howard |December 27, 2014 |DAILY BEAST 

I told them the story about how Delta helped her propose to me in the middle of the international terminal here at Delta. How A Company’s Support of Gay Employees Helped One of Them To Come Out | |December 24, 2014 |DAILY BEAST 

Someone without a terminal illness can go through years and years of terrible suffering. U.K. Courts Grant Mother Right to End Her 12-Year-Old Disabled Daughter’s Life |Elizabeth Picciuto |November 4, 2014 |DAILY BEAST 

“Someone with a terminal illness will die soon anyway,” he said. U.K. Courts Grant Mother Right to End Her 12-Year-Old Disabled Daughter’s Life |Elizabeth Picciuto |November 4, 2014 |DAILY BEAST 

Most of the 38 alleged victims were gravely ill or suffered terminal conditions. Nurse Nasty Suspected of Killing 38 People in Italy |Barbie Latza Nadeau |October 15, 2014 |DAILY BEAST 

He presented the railway case with great ability, and his views were accepted on the important terminal question. Fifty Years of Railway Life in England, Scotland and Ireland |Joseph Tatlow 

The terminal C is held to the metal covering of the fixture, while the end D is held to one of the wires. The Boy Mechanic, Book 2 |Various 

The carbon plates should be connected together and then connected to a binding post which forms the positive terminal of the cell. The Boy Mechanic, Book 2 |Various 

The single railroad passing through Fredericksburg had no coast terminal. Historic Fredericksburg |John T. Goolrick 

A terminal moraine, a mile and a half in depth, separates it from the sea. Over the Rocky Mountains to Alaska |Charles Warren Stoddard