Evans got up, wrapped a handkerchief around his hand and resumed shouting orders. Wreckage of long-lost WW II ship, sunken with its Native American skipper and half its crew, identified |Michael Ruane |April 2, 2021 |Washington Post 

She’s working from her home in Brooklyn and she’s reaching for a handkerchief. Samira Nasr, a fashion first at Harper’s Bazaar: ‘I just want to bring more people with me to the party’ |Robin Givhan |February 19, 2021 |Washington Post 

Being dapper is all about attention to detail, like sporting a perfectly tucked handkerchief in your suit pocket. The Daily Beast’s 2014 Holiday Gift Guide: For the Don Draper in Your Life |Allison McNearney |November 29, 2014 |DAILY BEAST 

Waving a silk cloth, he declared, “Gentlemen, I will have this land just as surely as I now have this handkerchief.” Washington’s Wheeler-Dealer Patriotism |William O’Connor |October 31, 2014 |DAILY BEAST 

Avisar, who sports a green handkerchief and a face streaked with green paint, is from the city of Holon, near Tel Aviv. A Camp Away From Terror: Where Israeli and Palestinian Kids Find Common Ground |Nina Strochlic |August 4, 2014 |DAILY BEAST 

Tessie rose, unrolled her scented handkerchief, and taking a bit of gum from a knot in the hem, placed it in her mouth. Read ‘The King in Yellow,’ the ‘True Detective’ Reference That’s the Key to the Show |Robert W. Chambers |February 20, 2014 |DAILY BEAST 

A small lace handkerchief lay on the keys, and I turned away, choking. Read ‘The King in Yellow,’ the ‘True Detective’ Reference That’s the Key to the Show |Robert W. Chambers |February 20, 2014 |DAILY BEAST 

She folded them tightly in a handkerchief, and thrust the parcel as far as her arm could reach between the mattress and the bed. Checkmate |Joseph Sheridan Le Fanu 

He did not think of the matter again till just as he was getting into bed, when he noticed a red stain upon his handkerchief. Uncanny Tales |Various 

He passed his handkerchief for the last time over face and head, and resumed his wig. St. Martin's Summer |Rafael Sabatini 

The handkerchief glimmered on the counter, more white than anything else in that grey dusk. Hilda Lessways |Arnold Bennett 

With hands nervously working within her muff, she suddenly missed the handkerchief which she had placed there. Hilda Lessways |Arnold Bennett