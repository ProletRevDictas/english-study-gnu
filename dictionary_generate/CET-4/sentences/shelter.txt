Milan Nicole Sherry, co-director of House of Tulip, told the Washington Blade on July 27 during an interview at her Uptown New Orleans home that she expects the shelter will open in the city next spring or summer. New Orleans shelter to be ‘forever home’ for homeless trans people |Michael K. Lavers |August 27, 2020 |Washington Blade 

Normally, most of the thousands of evacuees would be directed to traditional emergency shelters in convention centers and school gyms. Covid-19 is pushing Hurricane Laura evacuees to hotels instead of shelters |Tim McDonnell |August 27, 2020 |Quartz 

It’s especially important to look out for these behaviors if you’ve just adopted a shelter animal. How to help your pet with separation anxiety |Sara Kiley Watson |August 26, 2020 |Popular-Science 

This speaks directly to notions from Maslow’s hierarchy of needs, where work largely addresses psychological and safety needs such as shelter, food, and financial well-being. The Global Work Crisis: Automation, the Case Against Jobs, and What to Do About It |Peter Xing |August 6, 2020 |Singularity Hub 

This bureaucratic hurdle exists on top of the many challenges migrants are facing in the midst of the pandemic, including a shortage of shelter beds and a lack of food and jobs. Morning Report: With Building Folly, City Real Estate Director Out |Voice of San Diego |August 4, 2020 |Voice of San Diego 

The grim instability of shelter life is hardly a recipe for success under the best of circumstances. His First Day Out Of Jail After 40 Years: Adjusting To Life Outside |Justin Rohrlich |January 3, 2015 |DAILY BEAST 

Some of them are trying to find places where they might be able to shelter if it comes to this. Has the Kurdish Victory at Sinjar Turned the Tide of ISIS War? |Niqash |December 27, 2014 |DAILY BEAST 

Small rooms off its graffiti-covered foyer provide shelter from the thick rain that can unexpectedly, and vengefully, hit. The Congo's Forgotten Colonial Getaway |Nina Strochlic |December 18, 2014 |DAILY BEAST 

They sacrifice their shelter to contain the walkers—and Judith gets her first action scene! The Walking Dead’s Midseason Finale Shocker: A Cherished Character Meets a Grisly End |Melissa Leon |December 1, 2014 |DAILY BEAST 

After weeks or months in the line only a wound can offer him the comfort of safety, shelter, and a bed. Blood in the Sand: When James Jones Wrote a Grunt’s View of D-Day |James Jones |November 15, 2014 |DAILY BEAST 

In their shelter, Brion and Ulv crouched low and wondered why the attack didn't come. Sense of Obligation |Henry Maxwell Dempsey (AKA Harry Harrison) 

But one day when we marched beneath the blazing sun, we met a storm and found no shelter. The Soldier of the Valley |Nelson Lloyd 

I feel proud and happy to shelter beneath my roof any of our valued and brave allies. Blackwood's Edinburgh Magazine, Volume 60, No. 372, October 1846 |Various 

This man by hard, manual labor makes only enough to pay for humble shelter and plain food. The Unsolved Riddle of Social Justice |Stephen Leacock 

He lived for some time as a bandit, robbing the subjects of the King of Gath, who had given him shelter. God and my Neighbour |Robert Blatchford