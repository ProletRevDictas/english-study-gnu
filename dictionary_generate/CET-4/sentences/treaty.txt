This convention is the most widely ratified treaty on the planet, and it states that children have an inherent right to life and a supportive environment in which to grow. It’s about time adults start rising up against climate change |Alexandria Villaseñor |October 21, 2020 |Popular-Science 

The current December 2020 cutoff is written into the Brexit deal between the UK and EU, which is now an international treaty. The EU and the UK still haven’t reached a post-Brexit agreement. What’s next? |Jen Kirby |October 16, 2020 |Vox 

All male citizens aged 18 to 28 are required to serve in the military for about two years to guard against North Korea, which maintains one of the world’s largest standing armies and has never signed a peace treaty with South Korea. South Korean boy band BTS built an army. Now, they might get to defer military service |kdunn6 |September 4, 2020 |Fortune 

That could be something along the lines of the International Atomic Energy Agency, which inspects nuclear sites and polices treaties. The “staged rollout” of gene-modified babies could start with sickle-cell disease |Amy Nordrum |September 3, 2020 |MIT Technology Review 

I was hoping that a more general treaty change was coming down the track. The Prime Minister Who Cried Brexit (Ep. 392) |Stephen J. Dubner |October 10, 2019 |Freakonomics 

Start out with that so you understand what happened, beginning with the Treaty of Westphalia. McCain’s 13 Favorite Soldiers |Sandra McElwaine |November 11, 2014 |DAILY BEAST 

It is one of two countries (along with Egypt) to have a peace treaty with the Jewish state. Israel Could Get Dragged Into ISIS’s War, Obama Admin Warns |Eli Lake |June 27, 2014 |DAILY BEAST 

As a result, men from both sides, who were all sporting painful erections, gathered together to negotiate a peace treaty. Japanese Women Tell Their Men They Have to Choose Between Love and War |Jake Adelstein, Angela Erika Kubo |May 14, 2014 |DAILY BEAST 

Al-Huthaili is a dual citizen of the U.S. and Saudi Arabia and the two countries are not bound by a bilateral extradition treaty. Saudi Beauty Says She Robbed Banks for Her Mafia Lover |Caitlin Dickson |May 10, 2014 |DAILY BEAST 

Russian surveillance planes already fly over America, thanks to a long-standing treaty. Pentagon Moves to Block Russian Spy Plane in American Skies |Eli Lake |April 18, 2014 |DAILY BEAST 

Here and there roving parties appeared, but having no recognized leaders, their existence did not invalidate the treaty. The Philippine Islands |John Foreman 

The result of this mission was eminently successful; a special treaty was drawn up and Spain sold Louisiana to France. Napoleon's Marshals |R. P. Dunn-Pattison 

Some were inquisitive enough to ask, Has a treaty been signed or a trick been played upon the rebels? The Philippine Islands |John Foreman 

Among other questions to be agreed upon and embodied in the treaty was the future of the Philippines. The Philippine Islands |John Foreman 

In faith whereof, we, the respective Plenipotentiaries, have signed this treaty and have hereunto affixed our seals. The Philippine Islands |John Foreman