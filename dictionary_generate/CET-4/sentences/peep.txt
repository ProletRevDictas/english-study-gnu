Instead, Mayor Todd Gloria took over in December, and his first 100 days passed without a peep about the project. Politics Report: Hotel Union Leader to Lead Union Leaders |Scott Lewis and Andrew Keatts |March 27, 2021 |Voice of San Diego 

Nadal didn’t utter a peep but took his seat, changed his shirt and sent a clutch of rackets off the court for restringing. Rafael Nadal toppled by Stefanos Tsitsipas at Australian Open |Liz Clarke |February 17, 2021 |Washington Post 

Marijuana advanced toward final passage with not even a peep from Republicans, a small number of whom might wind up supporting it. Virginia General Assembly poised for historic votes to legalize marijuana and end death penalty |Gregory S. Schneider, Laura Vozzella |February 4, 2021 |Washington Post 

Currant-studded Chelsea buns sparkle under a sugary crust, and mincemeat filling peeps from the slashed tops of Eccles cakes. Take a culinary journey with these new travel-inspired cookbooks |Jen Rose Smith |January 22, 2021 |Washington Post 

Then again, the younger crowd might prefer older peeps don’t download the app anyway. Half of Americans oppose Trump’s potential ban of TikTok |Lance Lambert |August 21, 2020 |Fortune 

This film elicited complaints from some on the left, but not a peep from the right. Rush Limbaugh’s Fear of a Black James Bond |Dean Obeidallah |December 29, 2014 |DAILY BEAST 

Not a peep from LaPierre on this extended assault on citizens of Ferguson, at least that I can find. Why Isn't the NRA Defending Ferguson’s Blacks? |Cliff Schecter |August 19, 2014 |DAILY BEAST 

This open secrecy served its purpose, says Peep Ehasalu, communications manager for the hotel. The KGB Welcomes You to Estonia’s Hotel Viru. Please Mind the Hidden Bugs |Nina Strochlic |July 31, 2014 |DAILY BEAST 

Ernst responded by accusing Braley of sexism because his ad, which featured a baby bird not making a peep, had a “chick” in it. The Bruce Braley-Joni Ernst Race Is Iowa’s Ugliest Senate Campaign Ever |Ben Jacobs |July 22, 2014 |DAILY BEAST 

There are 200 mosquitos from New Delhi caught and killed mid-bite, peep-show coins, and artifacts of daily life in North Korea. New York’s Tiniest—and Weirdest—Museum |Nina Strochlic |May 29, 2014 |DAILY BEAST 

No notice being taken of the taps, the unseen visitor, after a short lapse, ventured to open the door and peep in. The Posthumous Papers of the Pickwick Club, v. 2(of 2) |Charles Dickens 

No doubt, even in these choking circumstances, childish feeling may now and again peep out. Children's Ways |James Sully 

Yuh want t' keep your eye glued t' that peep-glass in the mornin', and not overlook no motions. Raw Gold |Bertrand W. Sinclair 

Every hundred yards or so I had a close peep at the ground in front through de Lisle's periscope. Gallipoli Diary, Volume I |Ian Hamilton 

"I was watching you through my little peep-hole—as you saw," he began, with a pleasant smile, advancing to shake hands. Three More John Silence Stories |Algernon Blackwood