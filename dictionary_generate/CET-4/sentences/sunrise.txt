As farm workers in Karabutak, a small village in the foothills of the southern Ural Mountains in Russia, took to their fields around sunrise on the morning of April 24, 1967, they witnessed a remarkable celestial sight in the clear spring skies. The Man Who Fell From Space |Sean Braswell |November 27, 2020 |Ozy 

We should still dip, by around sunrise, into the mid-40s to low 50s downtown. D.C.-area forecast: Comfortable warmth continues for days under generally sunny skies |A. Camden Walker |November 6, 2020 |Washington Post 

For very early risers with a clear view of the eastern horizon, you may glimpse Mercury and Venus paired about an hour before sunrise. D.C.-area forecast: Beautifully dry and mild weather into next week |David Streit |November 5, 2020 |Washington Post 

Moving at five miles-per-second, the ISS orbits the Earth every 90 minutes, allowing its six-person crew to watch 16 sunrises and sunsets every day. Humans have lived on the ISS for 20 years—here are the coolest discoveries we’ve made |María Paula Rubiano A. |November 3, 2020 |Popular-Science 

If your soul burns for quiet shimmering sunrises in the marshlands, learn the patterns of migrating waterfowl. The Beginner's Guide to Hunting |Ian Fohrman |October 30, 2020 |Outside Online 

Much like the Taj Mahal, Revel opened in classically gaudy Atlantic City style in April 2012—with a sunrise Champagne toast. I Watched a Casino Kill Itself: The Awful Last Nights of Atlantic City’s Taj Mahal |Olivia Nuzzi |December 8, 2014 |DAILY BEAST 

I am watching the sunrise from the 39th floor of the Trump Taj Mahal in Atlantic City, New Jersey. I Watched a Casino Kill Itself: The Awful Last Nights of Atlantic City’s Taj Mahal |Olivia Nuzzi |December 8, 2014 |DAILY BEAST 

“It turns out that going for a walk in the morning after sunrise can be especially effective,” says Dr. Rohan. 9 Ways to Cope With Seasonal Affective Disorder |DailyBurn |December 5, 2014 |DAILY BEAST 

Since this hearing was taking place during Ramadan, many of the Muslims had been fasting since sunrise. When Bigotry Comes to Your Hometown |Dean Obeidallah |July 11, 2014 |DAILY BEAST 

Oh, and it was commonly considered a morning drink—quite the sunrise pick-me-up. The Rise and Fall…and Rise Again of the Old-Fashioned |Allison McNearney |June 14, 2014 |DAILY BEAST 

The sky was alight from zenith to horizon, the Nile aflame with sunrise, by the time the letter was written. The Wave |Algernon Blackwood 

About sunrise the troop left camp in a body, later spreading fanwise over the prairies. Raw Gold |Bertrand W. Sinclair 

Yesterday morning, about an hour before sunrise, a bright meteor was seen in the south-west. Journal of a Voyage to Brazil |Maria Graham 

Sometimes he loitered alone, from sunrise to sunset, in the dreary and rugged wilderness which surrounds the Escurial. The History of England from the Accession of James II. |Thomas Babington Macaulay 

Nevertheless, I awaited the sunrise this time with great anxiety, when we continued our journey. A Woman's Journey Round the World |Ida Pfeiffer