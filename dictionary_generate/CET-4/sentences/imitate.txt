It really felt like a melding of my two worlds—life was imitating art there for sure, and it felt special. Inside ‘I Am Woman’: A new biopic tells the story of Helen Reddy and her famous song |radmarya |September 10, 2020 |Fortune 

Though Cobham-Hervey is Australian, just like the singer, imitating Reddy’s speech patterns wasn’t the easiest task. Inside ‘I Am Woman’: A new biopic tells the story of Helen Reddy and her famous song |radmarya |September 10, 2020 |Fortune 

Others contain materials that imitate part of a disease-causing agent. Scientists Say: Vaccine |Bethany Brookshire |August 31, 2020 |Science News For Students 

They imitate the ways in which science works and make claims as if they were scientists because even they recognize the power of a scientific approach. How Pseudoscientists Get Away With It - Facts So Romantic |Stuart Firestein |August 28, 2020 |Nautilus 

Frauds imitate the ways science works because even they recognize the power of a scientific approach. How Pseudoscientists Get Away With It - Facts So Romantic |Stuart Firestein |August 28, 2020 |Nautilus 

We are now to get angry simply when whites happily imitate something that minorities do. You Can’t ‘Steal’ a Culture: In Defense of Cultural Appropriation |John McWhorter |July 15, 2014 |DAILY BEAST 

The idea that when we imitate something we are seeking to replace it rather than join it is weak. You Can’t ‘Steal’ a Culture: In Defense of Cultural Appropriation |John McWhorter |July 15, 2014 |DAILY BEAST 

The point of art,” writes Eagleton, channeling the Romantics, “is not to imitate life but to transform it. Do We Need to Be Told How to Read? |William Giraldi |June 6, 2013 |DAILY BEAST 

My first reaction upon finishing it was to imitate the unsinkable Ursula and begin all over again. Life After Life |Malcolm Jones |April 7, 2013 |DAILY BEAST 

Then, between puffs of cigarette smoke, she began to imitate a mutual friend. Amanda Bynes, Lindsay Yenter & More Celebrities’ Week in Hell |Anna Klassen |March 16, 2013 |DAILY BEAST 

Whatever the species, it is well to imitate the natural conditions as much as possible in the way of soil. How to Know the Ferns |S. Leonard Bastin 

Let us imitate the example of the Great Powers; they cannot exist alone, however strong and great they may be. The Philippine Islands |John Foreman 

Inasmuch as the community is small, and all its inhabitants need the governor and are watching him, they will try to imitate him. The Philippine Islands, 1493-1898, Volume XX, 1621-1624 |Various 

She had taken great pains with the table, trying to imitate Mrs. Perkins's, and the imitation was rather satisfactory to herself. The Cromptons |Mary J. Holmes 

Her character became insensibly molded to their forms, and she was inspired with restless enthusiasm to imitate their deeds. Madame Roland, Makers of History |John S. C. Abbott