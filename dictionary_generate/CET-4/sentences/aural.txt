The “Re-re-re” riff they come up with—springing off their nickname for their sister as well as the first syllable of the song’s title—is the perfectly chosen accessory, the aural equivalent of the handbag that completes the look. Jennifer Hudson Makes a Dazzling Aretha Franklin in the Satisfying and Potent Respect |Stephanie Zacharek |August 12, 2021 |Time 

While I haven’t had the opportunity to test out the maximum power of multiples via PartyBoost, pairing with a friend’s JBL Flip 5 took less than a minute and I appreciated the opportunity for aural improvement. JBL Charge 5 review: A rugged, portable Bluetooth speaker with battery to spare |Billy Cadden |June 15, 2021 |Popular-Science 

The aural accompaniment to a Wolf Trap stroll is one of several versions of Reid’s real-life soundtrack, whose other editions can be heard in New York’s Central Park, Los Angeles’s Griffith Park and additional sylvan locations. At Wolf Trap, this walk in the woods comes with its own soundtrack |Mark Jenkins |April 22, 2021 |Washington Post 

By keeping conversations and other aural disturbances from traveling and bouncing around the room, these tactics also make it easier for all sorts of students and workers to focus. Designing spaces with marginalized people in mind makes them better for everyone |Eleanor Cummins |March 2, 2021 |Popular-Science 

But damn, the music is catchy—a neo-soul aural assault of horns, electro swirls, yelps, funky basslines, and harmonized vocals. The 14 Best Songs of 2014: Bobby Shmurda, Future Islands, Drake, and More |Marlow Stern |December 31, 2014 |DAILY BEAST 

If your ears are tired of slick auto-tuned vocals, pick up this disk for an aural detox. The Best Albums of 2014 |Ted Gioia |December 13, 2014 |DAILY BEAST 

Tonya Jone Miller, who describes herself as an “aural courtesan,” has run the Bay City Blues sex-line for nine years. Sex, Suicide, and Homework: The Secret World of the Telephone Hotline |Tim Teeman |November 20, 2014 |DAILY BEAST 

How a Dutch social-media star and her strange aural stimuli helped combat my chronic insomnia. YouTube’s Sleep Whisperers Are A Sexy Way To Combat Insomnia |Lizzie Crocker |May 3, 2014 |DAILY BEAST 

The biggest problem with lifestyle-driven music criticism is that it poisons our aural culture. Music Criticism Has Degenerated Into Lifestyle Reporting |Ted Gioia |March 18, 2014 |DAILY BEAST 

The results were not: the nerve was directly stimulated, producing aural and visual hallucinations. The Impossible Voyage Home |Floyd L. Wallace 

The sense of hearing is extremely acute, and the tympanum is large, although externally there is no aural development. Natural History of the Mammalia of India and Ceylon |Robert A. Sterndale 

The aural deformities that fall under the head of stigmata, or have been classed as such, affect all portions of the external ear. Degeneracy |Eugene S. Talbot 

The chief use of an ear probe in aural work was to instil liquids into the ear. Surgical Instruments in Greek and Roman Times |John Stewart Milne 

Auricular tubes adapted to be applied to the ears and concealed by the hair, and other forms of aural instruments, were devised. Inventions in the Century |William Henry Doolittle