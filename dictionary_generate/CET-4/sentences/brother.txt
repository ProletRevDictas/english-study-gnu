These are the fathers, husbands, sons and brothers of the communities that these elected leaders are claiming to save. While We’re Rethinking Policing, It’s Time to End Gang Injunctions |Jamie Wilson |September 15, 2020 |Voice of San Diego 

The third generation owner of Shenanigans has had both her father and brother die of the virus. The Last Bar Standing? |Eugene Robinson |September 15, 2020 |Ozy 

Right before Beethoven composed the Fifth Symphony, he wrote to his brothers that his oncoming deafness had “brought me to the verge of despair.” Beethoven’s 5th Symphony is a lesson in finding hope in adversity |Charlie Harding |September 11, 2020 |Vox 

A hospice nurse checked in on Johnson the first two days she was home, said Lavalais and his brother, Michael. Sent Home to Die |by Annie Waldman and Joshua Kaplan |September 2, 2020 |ProPublica 

Tuesday passed, and just as I was beginning to accept that the meeting wouldn’t happen, I got a text from Yves Guillemot, Ubisoft’s CEO and one of the founding brothers of the company, telling me when and where to meet him tomorrow. ‘The Dream Architects’: Inside the making of gaming’s biggest franchises |Rachel King |September 1, 2020 |Fortune 

I did love him like a big brother, and miss him all the time. The Story Behind Lee Marvin’s Liberty Valance Smile |Robert Ward |January 3, 2015 |DAILY BEAST 

He knew I loved him like a big brother, and I knew the feeling was mutual. The Story Behind Lee Marvin’s Liberty Valance Smile |Robert Ward |January 3, 2015 |DAILY BEAST 

The brother of a girl who made her debut in New Orleans society was shaking his fists in excitement. The Louisiana Racists Who Courted Steve Scalise |Jason Berry |January 3, 2015 |DAILY BEAST 

Among the other graduates was Officer Kevin Lynch, brother and son of police officers. Cop Families Boo De Blasio at NYPD Graduation |Michael Daly |December 30, 2014 |DAILY BEAST 

A 2008 Pakistani raid near Turbat turned up Abdolhamid Rigi, the brother of Abdelmalek Rigi. The Dangerous Drug-Funded Secret War Between Iran and Pakistan |Umar Farooq |December 29, 2014 |DAILY BEAST 

After her marriage to Eugène Manet she came under the influence of his famous brother, Édouard. Women in the fine arts, from the Seventh Century B.C. to the Twentieth Century A.D. |Clara Erskine Clement 

Again, she was present at the battle of Silan, where her heroic example of courage infused new life into her brother rebels. The Philippine Islands |John Foreman 

Father, mother, sister, and brother all played and worked together with rare combination of sympathetic gifts. Kipling Stories and Poems Every Child Should Know, Book II |Rudyard Kipling 

Victor was the younger son and brother—a tete montee, with a temper which invited violence and a will which no ax could break. The Awakening and Selected Short Stories |Kate Chopin 

It was father against son, brother against brother, neighbor against neighbor. The Courier of the Ozarks |Byron A. Dunn