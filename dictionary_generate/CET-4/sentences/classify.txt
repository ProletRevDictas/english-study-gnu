It’s certainly arguable that California’s law requiring Uber and Lyft drivers be classified as employees is a bad law with unintended consequences. The next global tech giant may come from India |Adam Lashinsky |August 24, 2020 |Fortune 

That ballot measure would exempt app-based drivers from having to comply with AB 5, the state law limiting when employers can classify their workers as independent contractors. Sacramento Report: San Diego Bills Survive Bad Day for Housing Proposals |Andrew Keatts and Sara Libby |August 21, 2020 |Voice of San Diego 

For now, we’re classifying it as a living project and welcoming spin-offs. The Minecraft Institvte of Technology |Tate Ryan-Mosley |August 19, 2020 |MIT Technology Review 

In the next step, you need to classify your user base into specific groups, each of which will be targeted for unique marketing campaigns that cater to its needs. How to use in-market audiences for better search campaigns |Harikrishna Kundariya |August 18, 2020 |Search Engine Watch 

Both ride-hailing services threatened to temporarily shut down their operations in California after a judge declined to carve out an exemption for them from AB5, the new state law that would require the companies to classify drivers as employees. It really looks like Uber and Lyft might suspend operations in California |Michelle Cheng |August 15, 2020 |Quartz 

Or perhaps we instinctively classify Cubans as a people accustomed to being told what it is they need and want. Castro's Hipster Apologists Want to Keep Cuba ‘Authentically’ Poor |Michael Moynihan |December 18, 2014 |DAILY BEAST 

While some may classify that inactivity as laziness or indifference, Brown suggests the contrary. Deepwater Horizon: Life Drowning in Oil |Samuel Fragoso |November 2, 2014 |DAILY BEAST 

If the doctor is biased, he may still classify it as a disorder that can lead to legal repercussions. Coming Out Kinky to Your Doctor, in Black and Blue |Heather Boerner |October 25, 2014 |DAILY BEAST 

The end goal is to classify what happens when the memory goes awry. DARPA’s $40 Million Plan to Save Soldiers’ Brains |Dr. Anand Veeravagu, MD, Michael Zhang |July 15, 2014 |DAILY BEAST 

There is a debate among experts about how to classify and define the effects of a lithium-ion battery meltdown. Flight 370 Was Carrying 440 Pounds of Dangerous Batteries |Clive Irving |May 1, 2014 |DAILY BEAST 

We would classify these two departments in this way, though in the highest dramatic work elements of both phases are combined. Expressive Voice Culture |Jessie Eldridge Southwick 

Classify each leukocyte seen, and calculate what percentage each variety is of the whole number classified. A Manual of Clinical Diagnosis |James Campbell Todd 

Apple-growers classify apples into three different kinds, each consisting of a great many separate varieties. The Wonder Book of Knowledge |Various 

Among them he found so many curious and various specimens that he was induced not only to draw but also to classify them. Frederick Chopin as a Man and Musician |Frederick Niecks 

It will be an useful task if I attempt to classify the evidence on each side, and to draw an inference therefrom. Ancient Faiths And Modern |Thomas Inman