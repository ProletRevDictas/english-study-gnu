That process took days, involved fax machines, and, needless to say, offered no scratch-and-win prizes. Robinhood’s next adventure: Stealing market share from the rich |Jeff |December 3, 2020 |Fortune 

The steps were needed to avoid risking “needless suffering and death,” he said. Los Angeles’s new COVID lockdown bans most walking, driving and use of public transport |Claire Zillman, reporter |December 3, 2020 |Fortune 

Decoupling also implies needless fear of head-to-head competition, which we simply have to face and in which we continue to enjoy significant advantage. China: Charting a Course Between Conflict and Accommodation |Tracy Moran |October 13, 2020 |Ozy 

It just seems to be a silly and needless risk given what has happened in the last 48 hours with the president. Candidates, debate organizers push ahead with in-person events despite public health concerns |Michael Scherer |October 4, 2020 |Washington Post 

As a health care consumer in America, you are far more likely to be steered toward needless tests and procedures than away from them. My cancer might be back—and I wonder if unnecessary radiation caused it in the first place |jakemeth |September 22, 2020 |Fortune 

His Oxford shirts and matching boxers are, needless to say, woven. The Photographer Who Gave Up Manhattan for Marrakech |Liza Foreman |January 6, 2015 |DAILY BEAST 

The airline industry objects that sometimes these deployable recorders can pop out without cause, spreading needless alarm. Red Tape and Black Boxes: Why We Keep ‘Losing’ Airliners in 2014 |Clive Irving |December 29, 2014 |DAILY BEAST 

Needless to say, Juxiao was thrilled to see them and gave each of them a lot of love and affection. ‘Sexual’ Barbershop Quartet, a Panda Family Reunion, and More Viral Videos |The Daily Beast Video |December 14, 2014 |DAILY BEAST 

Needless to say, this does not enamor her to the powers that be in Cameroon. The Straight Hero of Cameroon’s Gays |Jay Michaelson |December 10, 2014 |DAILY BEAST 

Needless to say, Howie disagrees with that characterization. South Dakota's Bizarre Four-Way (Senate Election, That Is) |Ben Jacobs |October 15, 2014 |DAILY BEAST 

Because if that was to atone for man's sin, it was needless, as God could have forgiven man without Himself suffering. God and my Neighbour |Robert Blatchford 

Needless to say, the Worcestershire baronet had returned to his ancestral acres a sadder but a wiser man. The Pit Town Coronet, Volume I (of 3) |Charles James Wills 

In writing K. I try to convey the truth in terms which will neither give him needless anxiety or undue confidence. Gallipoli Diary, Volume I |Ian Hamilton 

Mr. Pontellier had a vague suspicion of it which he thought it needless to mention at that late day. The Awakening and Selected Short Stories |Kate Chopin 

The needless insertions of little words in many of the 15th-century MSS. Chaucer's Works, Volume 1 (of 7) -- Romaunt of the Rose; Minor Poems |Geoffrey Chaucer