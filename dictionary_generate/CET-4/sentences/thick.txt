Fans generally believe that the ideal oat porridge should be thick enough to offer some resistance, but smooth enough to go down easily. In Pursuit of the Perfect Bowl of Porridge |Clarissa Wei |September 11, 2020 |Eater 

We’re moving away from the convention period of the race and into the thick of the election. Election Update: Polls Are Good For Biden Pretty Much Everywhere — Except Florida |Geoffrey Skelley (geoffrey.skelley@abc.com) |September 9, 2020 |FiveThirtyEight 

Then we woke up with its thick, hot smoke upon us and realized it was smothering our lives. The Climate Crisis Is Happening Right Now. Just Look at California’s Weekend. |by Elizabeth Weil |September 9, 2020 |ProPublica 

The wheels are 200 millimeters thick, and can maintain great speed and a smooth ride over almost any city street or sidewalk. The best scooters for a smooth commute or cruise |PopSci Commerce Team |September 3, 2020 |Popular-Science 

A slimmer knife will be more comfortable to carry than a thicker one. Three Questions to Ask Yourself Before Buying a Knife |Wes Siler |September 3, 2020 |Outside Online 

But the people from Valley Stream had such a thick New York accent that was all around me. Coffee Talk with Fred Armisen: On ‘Portlandia,’ Meeting Obama, and Taylor Swift’s Greatness |Marlow Stern |January 7, 2015 |DAILY BEAST 

His chin rested on the thick plastic collar buckled around his neck. Dungeons and Genital Clamps: Inside a Legendary BDSM Chateau |Ian Frisch |December 20, 2014 |DAILY BEAST 

At the highest navigable point of the Congo River, thick jungle creates an impenetrable wall of green around a large island. The Congo's Forgotten Colonial Getaway |Nina Strochlic |December 18, 2014 |DAILY BEAST 

Small rooms off its graffiti-covered foyer provide shelter from the thick rain that can unexpectedly, and vengefully, hit. The Congo's Forgotten Colonial Getaway |Nina Strochlic |December 18, 2014 |DAILY BEAST 

The Barclays Center where the Duke and Duchess will be seated would have stood in thick of where the pivotal action transpired. The British Royals Reinvade Brooklyn: William and Kate Come Watch Basketball on Historic Battle Site |Justin Jones |December 6, 2014 |DAILY BEAST 

We stumbled along, close up, for the thick-piled clouds still hung their light-obscuring banners over the sky. Raw Gold |Bertrand W. Sinclair 

The eyebrows were low and thick, the upper lip was sensitive, quivering sometimes as she talked, but the lower was firm and full. Ancestors |Gertrude Atherton 

Cystin crystals are colorless, highly refractive, rather thick, hexagonal plates with well-defined edges. A Manual of Clinical Diagnosis |James Campbell Todd 

In a voice thick with the torturing rage of impotence he gave the order upon which the grim Parisian insisted. St. Martin's Summer |Rafael Sabatini 

She locks the door behind them, and throws the key among the nettles that grew in a thick grove at her right. Checkmate |Joseph Sheridan Le Fanu