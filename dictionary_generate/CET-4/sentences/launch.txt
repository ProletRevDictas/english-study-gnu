GQ is looking to deepen its commerce revenue stream with the launch of its new e-commerce store, The GQ Shop, on Tuesday. ‘It’s worth testing’: GQ is moving from recommending products to selling its own |Kayleigh Barber |August 25, 2020 |Digiday 

Its Good & Gather grocery line has become a billion-dollar brand less than a year after its launch. Target just had its best quarter ever thanks to pandemic bulk buying |Phil Wahba |August 19, 2020 |Fortune 

Similarly, there aren’t any branded content tags available for use at launch, unlike within the main Instagram feed, where influencers can signal they have been paid by an advertiser to promote a product. ‘Building up a community first’: Instagram Reels has little on offer for advertisers — for now |Lara O'Reilly |August 6, 2020 |Digiday 

Sportico, a sports business publication planned by Penske Media, has moved its launch date up by three months, before it had even hired any writers. ‘Off the field business’: Sports is still shaky but sports business publications see a lucrative play |Kayleigh Barber |August 3, 2020 |Digiday 

Women’s lifestyle title Marie Claire is getting into the sampling business with the launch of Beauty Drawer, opening Monday, 3 Aug, 9. ‘No brainer’: Marie Claire launches sampling business to boost revenue and data practice |Lucinda Southern |August 3, 2020 |Digiday 

An arrow appears indicating the direction you will launch your ball. Lost For Thousands of Strokes: 'Desert Golfing' Is 'Angry Birds' as Modern Art |Alec Kubas-Meyer |January 2, 2015 |DAILY BEAST 

With those words was a promise to launch the first group of passengers in the coming year. You Were Wrong About Miley & Bitcoin: 2014’s Failed Predictions |Nina Strochlic |December 31, 2014 |DAILY BEAST 

And, with Coca-Cola announcing the launch of a new milk product, the beverage could be back in our hands before we know it. History's Craziest Hangover Cures |Justin Jones |December 30, 2014 |DAILY BEAST 

He argues persuasively that the decision to launch the attack was completely contrary to reason and good military judgment. Hitler’s Hail Mary |James A. Warren |December 20, 2014 |DAILY BEAST 

Instead, they saw music videos as a launch pad for a whole new artistic movement: virality. OK Go Is Helping Redefine the Music Video For the Internet Age |Lauren Schwartzberg |December 15, 2014 |DAILY BEAST 

The launch was about twenty feet long with a small cabin and a fresh coat of brown paint. Ancestors |Gertrude Atherton 

There I deal direct with the San Francisco buyers—and in this launch; it serves me very well as an office. Ancestors |Gertrude Atherton 

He lost his head as the lower gates swung open, and broke the rule of the river by pushing out in front of a launch. Uncanny Tales |Various 

The launch was already under way, and young Cargill trying to avoid it better, thrust with his boat-hook at the side of the lock. Uncanny Tales |Various 

Gwynne turned with a start and found that Isabel had run her launch up to a little pier. Ancestors |Gertrude Atherton