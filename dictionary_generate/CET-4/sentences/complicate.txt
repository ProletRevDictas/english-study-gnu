Planning has been complicated because of uncertainty over the timing of FDA approval and the different dosage and storage requirements of prospective vaccines. Top health official says states need about $6 billion from Congress to distribute coronavirus vaccine |Lena H. Sun |September 16, 2020 |Washington Post 

This is all complicated by the fact that we’re still figuring out how best to combat the pandemic. Why Coming Up With Effective Interventions To Address COVID-19 Is So Hard |Neil Lewis Jr. (nlewisjr@cornell.edu) |September 14, 2020 |FiveThirtyEight 

Putting purpose into law will simplify, not complicate, the running of businesses by aligning what the law wants them to do with the reason why they are created. 50 years later, Milton Friedman’s shareholder doctrine is dead |jakemeth |September 13, 2020 |Fortune 

She believes statements by the city clerk and City Attorney Mara Elliott ahead of the primary declaring that a two-thirds vote would be required to pass the measure could complicate matters for Measure C supporters. Politics Report: Forged Footnote 15 |Scott Lewis and Andrew Keatts |September 12, 2020 |Voice of San Diego 

Rather, most of the officials trying to complicate the voting process are Republicans. The Latest On Republican Efforts To Make It Harder To Vote |Perry Bacon Jr. (perry.bacon@fivethirtyeight.com) |September 9, 2020 |FiveThirtyEight 

But there was money on the line, which tends to complicate things. COEXIST’s Bonehead Bumper-Sticker Politics |Michael Schulson |December 21, 2014 |DAILY BEAST 

The ambivalence is reflected in U.S. policy, which often has served to complicate aid delivery in conflict zones. Why Humanitarians Talk to ISIS |Joshua Hersh |October 24, 2014 |DAILY BEAST 

To complicate matters further, the only people who seemed to have any desire to go after Booker were of the conspiracy-theory ilk. The Ugly Truth About Cory Booker, New Jersey’s Golden Boy |Olivia Nuzzi |October 20, 2014 |DAILY BEAST 

The attacks on the Khorasan Group also complicate U.S. efforts to partner with the more moderate opposition. Al Qaeda Plotters in Syria ‘Went Dark,’ U.S. Spies Say |Eli Lake |September 24, 2014 |DAILY BEAST 

Two menacing Fargo-esque figures show up to complicate things. Viral Video of the Day: A Little 'Lebowski' Influence in Kahlua Ad |Alex Chancey |September 4, 2014 |DAILY BEAST 

It would be a mistake to still further complicate matters at this junction, he thought. The Weight of the Crown |Fred M. White 

The existence of the theocratic element served further to complicate the machinery of government at Yedo. Continental Monthly, Vol. 4, No 3, September 1863 |Various 

Further to complicate the difficulty, Hercules found that as soon as he struck off one head, two others sprang up in its place. Stories of Old Greece and Rome |Emilie Kip Baker 

Not to complicate the question, however, I have excluded all cases of nystagmus from the following investigation. Schweigger on Squint |C. Schweigger 

Every fresh change, especially the introduction of war-chariots or of horse-soldiers, would further complicate the acies. Battles of English History |H. B. (Hereford Brooke) George