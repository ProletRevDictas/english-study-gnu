Elizabeth exclaimed and she expertly piloted her truck through the puddle again, leaving a roiling wake in the water. Speed at their fingertips: Remote-control racing takes off in the District |Joe Heim |July 2, 2021 |Washington Post 

You have to watch the power gauge to nail the timing on your shot, which the narrator will confirm by exclaiming “Nice shot!” ‘Mario Golf: Super Rush’ brings the putting green to your living room |Shannon Liao |June 24, 2021 |Washington Post 

We both held on tight, the otherworldly buzz of a thousand cicadas in our ears, as we took turns exclaiming how good it was to see each other. I'm Grateful for the Hugs I Can Now Share–and Haunted by the Ones I Can't |Nicole Chung |June 24, 2021 |Time 

After taking the electric truck for a spin, he exclaimed, “This sucker’s quick” to news cameras. This Electric Ford Is Climate-Change Ready |Wes Siler |May 20, 2021 |Outside Online 

He would loudly exclaim, “I don’t know what I’m doing,” followed soon after by, “Er, checkmate.” ‘The Double Life of Bob Dylan’ is the definitive account of a shape-shifting genius’s early years |David Kirby |May 20, 2021 |Washington Post 

The idea of black Bond caused Limbaugh to exclaim on his show last week that Bond was “white and Scottish, period.” Rush Limbaugh’s Fear of a Black James Bond |Dean Obeidallah |December 29, 2014 |DAILY BEAST 

Almost unanimously they would exclaim, “We need young people and fresh ideas in government!” There’s No Better Test for Millennials than the American City |Michael Tubbs |April 19, 2014 |DAILY BEAST 

With the spoken word, we use our tone, inflection and volume to question, exclaim and convey our feelings. The Rise and Fall of the Infamous SarcMark |Keith Houston |September 24, 2013 |DAILY BEAST 

They give him a sandwich, begin driving away, and the man slips back into his Walt Jr. voice to exclaim: "Have an A-1 day!" ‘Homeless’ Man Does Amazing ‘Breaking Bad’ Impressions—But Is It a Viral Stunt? |Brian Ries |September 24, 2013 |DAILY BEAST 

So for Warren to exclaim that Rwandans have “figured out a way for people to live together in reconciliation” is, at best, naïve. Rick Warren’s Troubling Africa Mission |Jay Michaelson |September 14, 2013 |DAILY BEAST 

In case any reader should hastily exclaim, “What a ridiculous question; there can be only one southward!” The Giant of the North |R.M. Ballantyne 

A butcher's boy, running against a gentleman with his tray, made him exclaim, "The deuce take the tray!" The Book of Anecdotes and Budget of Fun; |Various 

The sight of the awful carnage affected even the warworn Marshal, and made him exclaim, "What a massacre!" Napoleon's Marshals |R. P. Dunn-Pattison 

Ambiguity abounds everywhere and confounds everything; we are obliged at every word to exclaim, What do you mean? A Philosophical Dictionary, Volume 1 (of 10) |Franois-Marie Arouet (AKA Voltaire) 

I exclaim: How agreeable must it be to dwell underneath119 such a pure sky and in such a delightful abode! The 'Characters' of Jean de La Bruyre |Jean de La Bruyre