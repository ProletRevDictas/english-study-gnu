Rage, Bob Woodward’s newest book, was already at the top of Amazon’s bestseller list days before Americans learned about its explosive contents. Bob Woodward’s book proves Americans still want to read about Trump |Karen Ho |September 11, 2020 |Quartz 

That’s according to an explosive new whistleblower complaint released by the Democrat-led House Intelligence Committee on Wednesday afternoon. What to make of the DHS whistleblower’s shocking complaint |Alex Ward |September 11, 2020 |Vox 

Netflix saw explosive subscriber growth in its first two quarters of this year, mainly attributed to the coronavirus pandemic. Eyeing new subscribers, Netflix makes ‘Stranger Things,’ ‘Love Is Blind,’ and more available for free |Aric Jenkins |August 31, 2020 |Fortune 

When an idle shipment of dangerous explosives burst in Beirut, there were Carrds. The internet of protest is being built on single-page websites |Tanya Basu |August 27, 2020 |MIT Technology Review 

Still others have proposed that space became magnetized before all this, during cosmic inflation — the explosive expansion of space that purportedly jump-started the Big Bang itself. The Hidden Magnetic Universe Begins to Come Into View |Natalie Wolchover |July 2, 2020 |Quanta Magazine 

Some of the most explosive opportunities could be based around things that the Western world seems reluctant to adopt. Silicon Valley Sets Its Sights on Africa |Christian Borys |December 22, 2014 |DAILY BEAST 

And it would let governors show leadership in explosive, highly-publicized cases. Dear GOP: Fix the Damn Justice System! |Jonathan Alter |December 7, 2014 |DAILY BEAST 

As grown-ups without turbo-charged explosive arrows, we can only change the rules through politics. The Hunger Games Economy |Jedediah Purdy |November 29, 2014 |DAILY BEAST 

A Shiite stronghold wrapped in a Sunni explosive belt—not exactly a picture of stability. The Nuclear Deal That Iran’s Regime Fears Most |Djavad Khadem |November 22, 2014 |DAILY BEAST 

That incident was due to an “equipment” problem (a steel tube ruptured)—resulting in explosive vapors being released and ignited. Oil Tankers Leaking into Seattle’s Water |Bill Conroy |October 13, 2014 |DAILY BEAST 

But he marred it all by a temper so ungovernable that in Paris there was current a byword, "Explosive as Garnache." St. Martin's Summer |Rafael Sabatini 

With an explosive hiss, gray jets of live steam erupted from pipes around the edge of the room. Hooded Detective, Volume III No. 2, January, 1942 |Various 

Anything made of iron and containing high explosive and detonator will be welcome. Gallipoli Diary, Volume I |Ian Hamilton 

The gasoline used also gave off a gas of highly explosive character and one very likely to escape from leaky tanks or joints. The Wonder Book of Knowledge |Various 

It is often difficult in serious situations to take these explosive monosyllables seriously. Frdric Mistral |Charles Alfred Downer