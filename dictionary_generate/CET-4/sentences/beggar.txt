Better to be a beggar in freedom,” he cried out, “than to be forced into compromises against my conscience. The Catholic Philosopher Who Took on Hitler |John Henry Crosby |December 26, 2014 |DAILY BEAST 

Rich man, poor man, beggar man, thief, doctor, lawyer, Indian chief, and all that. Joseph Campbell on the Roots of Halloween |Joseph Campbell |October 31, 2014 |DAILY BEAST 

In an interview, Liang said, “Air should be the most valueless commodity, free to breathe for any vagrant or beggar.” The Chinese Can’t Catch Their Breath |Brendon Hong |May 5, 2014 |DAILY BEAST 

The landays in I Am the Beggar of the World are sung only when men are absent. Beauty and Subversion in the Secret Poems of Afghan Women |Daniel Bosch |April 6, 2014 |DAILY BEAST 

I am the Beggar of the World is a book of poems, war reportage, and photographs. Beauty and Subversion in the Secret Poems of Afghan Women |Daniel Bosch |April 6, 2014 |DAILY BEAST 

He's a lucky beggar, Reginald, a very lucky beggar, and Warrender's daughter is more than he deserves. The Pit Town Coronet, Volume I (of 3) |Charles James Wills 

A beggar asking alms under the character of a poor scholar, a gentleman put the question, Quomodo vales? The Book of Anecdotes and Budget of Fun; |Various 

Valence sent a woman, disguised as a beggar, to spy out the position; but Bruce saw through the dodge, and the spy confessed. King Robert the Bruce |A. F. Murison 

If God put a beggar on horseback, would the horse be blamable for galloping to Monte Carlo? God and my Neighbour |Robert Blatchford 

And on the same authority we find that there is the ghost of dirt, for the ghost of the old beggar-man was "dirty." Second Edition of A Discovery Concerning Ghosts |George Cruikshank