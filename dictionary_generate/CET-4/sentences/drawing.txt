Featuring heavyweight drawing paper suitable for pen and ink, pencil, charcoal, and markers, the Travelogue notebook is a great choice for artists. Notable notebooks for writing and drawing |PopSci Commerce Team |September 17, 2020 |Popular-Science 

I particularly like the Artist face, featuring drawings by Geoff McFetridge, and the strange Typography face that lets you mess around with a whole bunch of font options. Apple Watch Series 6 first impressions: A stretchy addition looks great |Aaron Pressman |September 17, 2020 |Fortune 

This set offers a variety of lights and darks to make for easy writing, shading, and drawing. Excellent pencils for school, work, and beyond |PopSci Commerce Team |September 16, 2020 |Popular-Science 

What they belatedly recognized in October was that a flip that brings you closer to being able to add a new edge also brings the graph closer to resembling one of the good drawings they’d already identified. A New Algorithm for Graph Crossings, Hiding in Plain Sight |Stephanie DeMarco |September 15, 2020 |Quanta Magazine 

For example, the text about Alfred Binet came with a drawing of a race car driver. Top 10 tips on how to study smarter, not longer |Kathiann Kowalski |September 9, 2020 |Science News For Students 

Our animators are very excited to be drawing the innards of a human being. ‘Archer’ Creator Adam Reed Spills Season 6 Secrets, From Surreal Plotlines to Life Post-ISIS |Marlow Stern |January 8, 2015 |DAILY BEAST 

Like drawing tattoos, sewing earmuffs, or fashioning model airplanes from old chip bags? How a ‘Real Housewife’ Survives Prison: ‘I Don’t See [Teresa Giudice] Having a Cakewalk Here’ |Michael Howard |January 6, 2015 |DAILY BEAST 

By drawing boundaries against wrongful conduct, law provides a protective zone of freedom within those boundaries. Red Tape Is Strangling Good Samaritans |Philip K. Howard |December 27, 2014 |DAILY BEAST 

Now, the recent actions of ISIS and Boko Haram are drawing attention to the role of human trafficking. ISIS, Boko Haram, and the Growing Role of Human Trafficking in 21st Century Terrorism |Louise I. Shelley |December 26, 2014 |DAILY BEAST 

A drawing of three skulls along with a message in English appeared on computer screens at the targeted firms. Inside the ‘Surprisingly Great’ North Korean Hacker Hotel |Michael Daly |December 20, 2014 |DAILY BEAST 

In the drawing-room things went on much as they always do in country drawing-rooms in the hot weather. The Pit Town Coronet, Volume I (of 3) |Charles James Wills 

She began the study of drawing at the age of thirty, and her first attempt in oils was made seven years later. Women in the fine arts, from the Seventh Century B.C. to the Twentieth Century A.D. |Clara Erskine Clement 

She also practises etching, pen-and-ink drawing, as well as crayon and water-color sketching. Women in the fine arts, from the Seventh Century B.C. to the Twentieth Century A.D. |Clara Erskine Clement 

This habit and the fact that she cares more for color than for drawing are the usual criticisms of her pictures. Women in the fine arts, from the Seventh Century B.C. to the Twentieth Century A.D. |Clara Erskine Clement 

When they rose from table and went back into her little drawing-room, she left her daughter alone for awhile with Bernard. Confidence |Henry James