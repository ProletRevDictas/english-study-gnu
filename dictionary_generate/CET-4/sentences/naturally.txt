That genetic lockup may be key to being able to naturally control the virus. In a first, a person’s immune system fought HIV — and won |Tina Hesman Saey |August 26, 2020 |Science News 

Alexander Salerno, physician, Newark, New JerseyIn the case of a vaccine, the drug teaches your body how to create antibodies naturally so it can fight the illness long term. This New COVID-19 Treatment Could Also Bend Antitrust Norms |Charu Kasturi |August 19, 2020 |Ozy 

By making precise cuts to DNA, geneticists insert or remove naturally occurring genes associated with specific traits. Biotechnology Could Change the Cattle Industry. Will It Succeed? |Dyllan Furness |August 16, 2020 |Singularity Hub 

That means writing a blog post with a link to your website embedded naturally and contextually. Nine mistakes to avoid when contacting websites for backlinks |Raj Dosanjh |July 29, 2020 |Search Engine Watch 

Here’s a primer on 10 ways climate varies naturally, and how each compares with what’s happening now. How Earth’s Climate Changes Naturally (and Why Things Are Different Now) |Howard Lee |July 21, 2020 |Quanta Magazine 

That means that Champagne is fermented a second time in the bottle when sealed closed, which naturally produces the bubbles. Champagne: You’re Drinking It All Wrong |Kayleigh Kulp |December 20, 2014 |DAILY BEAST 

Were they innocent victims or did they conduct themselves in a manner that would naturally lead to their demise? The Post-Brown and Garner Question: Who ‘Deserves’ to Die? |Goldie Taylor |December 9, 2014 |DAILY BEAST 

The issue of low-skilled immigration naturally provokes intense nativist sentiments. The Case for More Low-Skill Immigration |Veronique de Rugy |December 7, 2014 |DAILY BEAST 

We, on the other hand, on the police side, will naturally gear up to deal with any potential contingency that might occur. After No Indictment for Eric Garner Killer, Is NYC the Next Ferguson? |Jacob Siegel |December 3, 2014 |DAILY BEAST 

Which naturally begs the question, where is Kent drawing from? ‘The Babadook’ Is the Best (and Most Sincere) Horror Movie of the Year |Samuel Fragoso |November 30, 2014 |DAILY BEAST 

The unhappy applicant was naturally obliged to temporarily retire from the game, at all events for that night. The Pit Town Coronet, Volume I (of 3) |Charles James Wills 

Naturally the conversation fell on the all-absorbing topic of the day and the object of his mission. The Philippine Islands |John Foreman 

They had been permitted to sit up till after the ice-cream, which naturally marked the limit of human indulgence. The Awakening and Selected Short Stories |Kate Chopin 

Naturally he did not take such pains to dodge Grandfather Mole—after the talk they had had. The Tale of Grandfather Mole |Arthur Scott Bailey 

Winifred, naturally a high-spirited and lively girl, soon recovered from the fright of that fateful Sunday evening. The Red Year |Louis Tracy