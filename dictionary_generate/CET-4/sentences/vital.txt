All of them know how vital it is to have a top quarterback, like Rivera and Hurney had with Newton in Carolina and Mayhew had with Stafford when he was the Lions’ general manager. Finding the right QB is the first — and most crucial — task for Washington’s new front office |Les Carpenter |February 4, 2021 |Washington Post 

The tests could be vital tools in the country’s fight against the virus — especially in the months before most Americans are vaccinated. A fast, at-home coronavirus test will be available to Americans this year |William Wan |February 1, 2021 |Washington Post 

Sand dredging is vital for construction because it’s an essential ingredient in concrete. Why India Needs a Blood Sand Awakening |Tracy Moran |February 1, 2021 |Ozy 

Utilizing outdoor space is vital in order to keep everyone safe and to ensure that state regulations are abided by. Backyard party essentials: Outdoor Super Bowl party ideas for 2021 |PopSci Commerce Team |January 30, 2021 |Popular-Science 

It’s vital to respect it, to be humble in the face of it, and to start adjusting to the democratization of the markets. How the GameStop Trading Surge Will Transform Wall Street |Zachary Karabell |January 29, 2021 |Time 

In the classic skillset of piloting, mental acuity, and its coordination with hand and foot movements, is equally vital. Flight 8501 Poses Question: Are Modern Jets Too Automated to Fly? |Clive Irving |January 4, 2015 |DAILY BEAST 

His ups and downs professionally outside of the World Cup are a vital a part of his story in the book. Tim Howard’s Wall of Intensity |William O’Connor |December 22, 2014 |DAILY BEAST 

Vital Voices in 2013 took over funds from the Women In The World foundation which originated at The Daily Beast. Joe Biden: ‘I’ll Kill Your Son’ |Olivia Nuzzi |December 12, 2014 |DAILY BEAST 

Some organizations, like amfAR, provide vital funding for scientists who bring innovative ideas to the HIV/AIDS research field. How You Can Help Make a More LGBT-Friendly World | |December 12, 2014 |DAILY BEAST 

“The influence of the oak maturation casks on the final character of The Macallan is vital,” says MacPherson. How Much Do Whisky Casks Really Affect Taste? | |December 10, 2014 |DAILY BEAST 

To make the effort of articulation a vital impulse in response to a mental concept,—this is the object sought. Expressive Voice Culture |Jessie Eldridge Southwick 

But this theory of a merciful, and loving Heavenly Father is vital to the Christian religion. God and my Neighbour |Robert Blatchford 

According to Metchnikoff, this property of leukocytes resides entirely within themselves, depending upon their own vital activity. A Manual of Clinical Diagnosis |James Campbell Todd 

If the hunter venture to come close to such a monster, and his dagger fail to pierce the vital spot, there is no help for him. Alila, Our Little Philippine Cousin |Mary Hazelton Wade 

These lofty strategical questions must not make me forget an equally vital munitions message just to hand. Gallipoli Diary, Volume I |Ian Hamilton