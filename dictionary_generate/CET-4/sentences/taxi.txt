While transporting people remains the main source of business for a majority of the motorcycle taxi firms, deliveries of food and other online goods is “the goal for many of these companies,” Osamuyi says. Can Motorbike Taxis Fix Africa’s E-commerce Problem? |Charu Kasturi |February 10, 2021 |Ozy 

The company had a partnership with Volkswagen that was supposed to lead to Aurora's technology being incorporated into Volkswagen's vehicles, with plans to launch a self-driving taxi service. Toyota partners with startup Aurora to develop self-driving taxis |Timothy B. Lee |February 9, 2021 |Ars Technica 

After one off-site meeting, she alleges, he offered to share a taxi with her, dropping her off on his way home. She reported sexual harassment by a former supervisor — and was fired soon after |Samantha Schmidt |February 8, 2021 |Washington Post 

He’s still waiting for the taxi voucher that he’ll be provided to go to and from the vaccine site, so when the notification pops up, Phillips hopes that he’ll be ready. Getting vaccinated is hard. It’s even harder without the internet. |Eileen Guo |February 3, 2021 |MIT Technology Review 

The Capitals recalled goaltender Craig Anderson to the active roster and added prospect Connor McMichael to the taxi squad Thursday after it was clear Ovechkin, Kuznetsov, Orlov and Samsonov would be absent. Capitals’ Ilya Samsonov tests positive for coronavirus; Ovechkin, two others remain off the ice |Samantha Pell |January 21, 2021 |Washington Post 

On Friday, a 26-year-old woman ordered a taxi in Delhi using the Uber app. Uber Finally Gets Banned—in Delhi |Olivia Nuzzi |December 9, 2014 |DAILY BEAST 

Why call a taxi when you can hail a Lyft to pick up visiting family and friends? One of a Kind Gifts Are Only a Neighbor Away |Lawrence Ferber |December 8, 2014 |DAILY BEAST 

He helped her to the waiting taxi and back into the apartment. ‘I Saved My Friend From Bill Cosby’ |Lloyd Grove |December 3, 2014 |DAILY BEAST 

He declined to award £30 to Miss Manners for her taxi journey but awarded her £10 travel expenses. How A British Aristocrat Used Big Game Hunter’s Sperm To Get Pregnant Without His Permission |Tom Sykes |December 2, 2014 |DAILY BEAST 

This meant that Palestinian taxi drivers had to drive through the Israeli settlement of Bet El. The Radicals Who Slaughtered a Synagogue |Creede Newton |November 19, 2014 |DAILY BEAST 

Late one night, when taxi-cabs were scarce, he found that his quickest way to reach home would be by means of one of the tubes. Uncanny Tales |Various 

The day dawned with a steady drizzle of rain, and, after a poor attempt at breakfast, I scoured the neighbourhood for a taxi. Punch, or the London Charivari, Volume 158, April 28, 1920 |Various 

He was a young man, and he wore some sort of a uniform—that of a chauffeur, taxi driver, or something of the sort. Hooded Detective, Volume III No. 2, January, 1942 |Various 

Rounding a corner, Black Hood sighted a taxi cab cruising along. Hooded Detective, Volume III No. 2, January, 1942 |Various 

"I'm in a big hurry to get to a masquerade," Black Hood said as he opened the door of the taxi. Hooded Detective, Volume III No. 2, January, 1942 |Various