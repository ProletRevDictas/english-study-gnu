Jennifer follows her passions and takes us readers and cooks to places we might not go to if she didn't lead us there. Blood is a respected ingredient around the world, but less so in the U.S. A new book aims to change that. |Mayukh Sen |February 26, 2021 |Washington Post 

Mirsani’s sister, Ilhama Safarova, was among the contributing cooks. This new Azerbaijani bakery offers stories as good as its pastries |Tom Sietsema |February 26, 2021 |Washington Post 

Colleagues have become very patient about me cooking while on calls with them, and my boss always politely offers to call back at a more convenient time when met with the boys chanting “go away, go away” at him. Broken windows, ‘cuddling breaks’ and interrupted video calls: Parents share realities of juggling work while homeschooling kids |Jessica Davies |February 26, 2021 |Digiday 

Portland’s Lex Grant has cooked in the NBA bubble and for Oprah Winfrey. The Snow Didn’t Stop. Neither Did Chef Lex Grant. |Brooke Jackson-Glidden |February 25, 2021 |Eater 

Some people find it helpful to label things with the date they were purchased or cooked. A third of all food in the U.S. gets wasted. Fixing that could help fight climate change. |Sarah Kaplan |February 25, 2021 |Washington Post 

Cook, stirring often, for 10 minutes or until the sugar is completely dissolved and the mixture is smooth. Make ‘The Chew’s’ Carla Hall’s Sticky Toffee Pudding |Carla Hall |December 28, 2014 |DAILY BEAST 

Continue to cook until the sauce has reduced by three quarters. Make Carla Hall’s Roasted Pork Loin With Cranberries |Carla Hall |December 24, 2014 |DAILY BEAST 

In his response, Cook spoke sensitively about the very real danger present in the general pickup community. School Shooters Love This Pickup Artist Website |Brandy Zadrozny |December 5, 2014 |DAILY BEAST 

“It's insane to see what the extreme version of that type of helpless anger combined with mental illness can create,” Cook wrote. School Shooters Love This Pickup Artist Website |Brandy Zadrozny |December 5, 2014 |DAILY BEAST 

An older white woman, stopped Cook to ask, in strong New York accent, “Oh no, did they let him off?” ‘They Let Him Off?’ Scenes from NYC in Disbelief |Jacob Siegel |December 4, 2014 |DAILY BEAST 

She did not need a great cook-book; She knew how much and what it took To make things good and sweet and light. Boys and Girls Bookshelf; a Practical Plan of Character Building, Volume I (of 17) |Various 

The camp grew still, except for the rough and ready cook pottering about the fire, boiling buffalo-meat and mixing biscuit-dough. Raw Gold |Bertrand W. Sinclair 

It is to be feared that like the sauce of sauces in the hands of the inexperienced cook, the result is more than doubtful. The Pit Town Coronet, Volume I (of 3) |Charles James Wills 

The voice of duty called her to the kitchen, where her cook patiently awaited her inevitable, and always painful, audience. The Pit Town Coronet, Volume I (of 3) |Charles James Wills 

They stood outside the window and the cook passed them their coffee and a roll, which they drank and ate from the window-sill. The Awakening and Selected Short Stories |Kate Chopin