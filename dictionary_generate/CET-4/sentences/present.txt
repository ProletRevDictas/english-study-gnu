We’re pleased to be present in so many relationships this year, even more so than before. How Hallmark is handling pandemic Valentine’s Day |Luke Winkie |February 12, 2021 |Vox 

Republicans, by many accounts, aren’t even considering the arguments being presented in the trial. How Has The Nation Changed Since The Insurrection At The Capitol? |Micah Cohen (micah.cohen@fivethirtyeight.com) |February 11, 2021 |FiveThirtyEight 

We should request that staff conduct a feasibility study and to present the results of that study to this committee by the end of the year. Vacancy Tax Study Is Giving City Officials Déjà Vu |Lisa Halverstadt and Andrew Keatts |February 10, 2021 |Voice of San Diego 

In five years of Insights puzzles, we’ve tried to present questions that lived up to the column’s name. Zen and the Art of Puzzle Solving |Pradeep Mutalik |February 10, 2021 |Quanta Magazine 

He was a tough guy, but also admired and respected by his players and most in the media, present company included. Marty Schottenheimer’s legacy can be measured in teams’ regret over letting him go |Leonard Shapiro |February 9, 2021 |Washington Post 

In his view, a writer has only one duty: to be present in his books. Houellebecq’s Incendiary Novel Imagines France With a Muslim President |Pierre Assouline |January 9, 2015 |DAILY BEAST 

Angelina Jolie was able to seemingly glide into the Vatican on Thursday to present her new film ‘Unbroken.’ Pope Francis Has the Pleasure of Meeting Angelina Jolie for a Few Seconds |Barbie Latza Nadeau |January 8, 2015 |DAILY BEAST 

Disordered eating is also linked to higher rates of depression and anxiety, both in the present and in the future. How Skinny Is Too Skinny? Israel Bans ‘Underweight’ Models |Carrie Arnold |January 8, 2015 |DAILY BEAST 

In the middle of all of that past suffering and present-day conflict, this Cosby bomb was dropped. Phylicia Rashad and the Cult of Cosby Truthers |Stereo Williams |January 8, 2015 |DAILY BEAST 

The account goes some way in showing just how present the Quds and other forces are in Iraq at this point in time. What an Iranian Funeral Tells Us About the Wars in Iraq |IranWire |January 6, 2015 |DAILY BEAST 

But Mrs. Dodd, the present vicar's wife, retained the precious prerogative of choosing the book to be read at the monthly Dorcas. The Pit Town Coronet, Volume I (of 3) |Charles James Wills 

The Rev. Alonzo Barnard, seventy-one years of age, accompanied by his daughter, was present. Among the Sioux |R. J. Creswell 

Several pioneers familiar with the facts of the tragedy at the time of its occurrence were also present. Among the Sioux |R. J. Creswell 

Bacteria, when present in great numbers, give a uniform cloud which cannot be removed by ordinary filtration. A Manual of Clinical Diagnosis |James Campbell Todd 

At present, Louis was too self-absorbed by the struggles within him, to look deep into what was passing around him. The Pastor's Fire-side Vol. 3 of 4 |Jane Porter