There’s an unlimited number of possible things we can say, of sentence structures, but not anything can be a sentence structure. Talking Is Throwing Fictional Worlds at One Another - Issue 89: The Dark Side |Kevin Berger |September 9, 2020 |Nautilus 

We have to come to terms with the fact that recognizing sentences written by humans is no longer a trivial task. Welcome to the Next Level of Bullshit - Issue 89: The Dark Side |Raphaël Millière |September 9, 2020 |Nautilus 

You can even set how many sentences you want in your summary. Read, watch, and listen to things faster than ever before |David Nield |September 9, 2020 |Popular-Science 

Simple enough, but you can glean much information from that sentence. Can you expose the truth in these two riddles? |Claire Maldarelli |August 26, 2020 |Popular-Science 

It does not help anyone to have communities where people feel like living there is a death sentence. Uncharted Power’s Jessica O. Matthews has a plan to revive America’s crumbling infrastructure |Brooke Henderson |August 23, 2020 |Fortune 

As this list shows, punishments typically run to a short-ish jail sentence and/or a moderately hefty fine. In Defense of Blasphemy |Michael Tomasky |January 9, 2015 |DAILY BEAST 

Real Housewives of New Jersey star Teresa Giudice turned herself in to serve a 15-month sentence for bankruptcy fraud. How a ‘Real Housewife’ Survives Prison: ‘I Don’t See [Teresa Giudice] Having a Cakewalk Here’ |Michael Howard |January 6, 2015 |DAILY BEAST 

That Huckabee is mentioned in the same sentence with other aspiring conservative governors, especially Bobby Jindal, is laughable. Why This Liberal Hearts Huckabee |Sally Kohn |January 6, 2015 |DAILY BEAST 

Brown had been serving a life sentence; McCollum had been on Death Row. How the U.S. Justice System Screws Prisoners with Disabilities |Elizabeth Picciuto |December 16, 2014 |DAILY BEAST 

Had he been competently represented, the jury might well have failed to concur on a death sentence. How the U.S. Justice System Screws Prisoners with Disabilities |Elizabeth Picciuto |December 16, 2014 |DAILY BEAST 

Before he could finish the sentence the Hole-keeper said snappishly, "Well, drop out again—quick!" Davy and The Goblin |Charles E. Carryl 

Each sentence came as if torn piecemeal from his unwilling tongue; short, jerky phrases, conceived in pain and delivered in agony. Raw Gold |Bertrand W. Sinclair 

Sentence of fine and imprisonment passed upon lord Bacon in the house of peers for bribery. The Every Day Book of History and Chronology |Joel Munsell 

John Wilkes released from the tower by the memorable sentence of chief justice Pratt. The Every Day Book of History and Chronology |Joel Munsell 

It seeks the shortest phrase or sentence and adds successively all the modifiers, making no omissions. Assimilative Memory |Marcus Dwight Larrowe (AKA Prof. A. Loisette)