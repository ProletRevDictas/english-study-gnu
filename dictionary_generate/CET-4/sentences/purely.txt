It also provides cover to House GOP members to say they are voting to support Greene purely to keep Democrats from abusing their power as the majority. The Marjorie Taylor Greene committee removal vote, explained |Gabby Birenbaum |February 5, 2021 |Vox 

When you think of content as being purely acquisitional, you become blinded by the drug that is acquisition. Taking your SEO content beyond the acquisition |Mordy Oberstein |February 2, 2021 |Search Engine Watch 

This isn’t to say the US would use AI for purely ethical purposes. China Wants to Be the World’s AI Superpower. Does It Have What It Takes? |Vanessa Bates Ramirez |January 17, 2021 |Singularity Hub 

Sure, the best conferences still win on great speakers and actionable takeaways, but following an in-person playbook for online events falls short of leveraging what a purely digital medium offers. Why we’re creating a new content experience for SMX |Henry Powderly |January 13, 2021 |Search Engine Land 

The Tesla CEO agreed to receive no salary or bonuses in exchange for a purely performance-based compensation plan. Tesla founder Elon Musk surpasses Jeff Bezos as world’s richest human |Michael J. Coren |January 7, 2021 |Quartz 

Indeed, as an almost purely advisory firm, Lazard is (appropriately) barely affected by the Dodd-Frank reforms. Antonio Weiss Is Not Part of the Problem |Steve Rattner |January 7, 2015 |DAILY BEAST 

Traditionally, popular history is almost purely driven by narrative. Thank Congress, Not LBJ for Great Society |Julian Zelizer, Scott Porch |January 4, 2015 |DAILY BEAST 

The Samaritan guidelines are written around the assumption that suicide is a purely irrational act, an act spurred by illness. Cover-Ups and Concern Trolls: Actually, It's About Ethics in Suicide Journalism |Arthur Chu |January 3, 2015 |DAILY BEAST 

Purely by chance, Anna Coren had landed in Sydney just as the chocolate shop siege began. CNN's Overnight Sydney Star |Lloyd Grove |December 16, 2014 |DAILY BEAST 

For starters, from a purely practical, all-hands-on-deck position, I say if you can do the job, you should keep the job. Justice Ginsburg Shouldn’t Quit Just Yet |Kevin Bleyer |December 1, 2014 |DAILY BEAST 

Here, as in so many of these childish admirations, we have to do not with a purely æsthetic perception. Children's Ways |James Sully 

In the preceding chapter an examination has been made of the purely mechanical side of the era of machine production. The Unsolved Riddle of Social Justice |Stephen Leacock 

True, she had taken a lively interest in all her brother's curates, but it was always a professional interest and purely Platonic. The Pit Town Coronet, Volume I (of 3) |Charles James Wills 

His ambition is a purely selfish one, while mine is distinctly benevolent. The Pit Town Coronet, Volume I (of 3) |Charles James Wills 

Our existing poverty is purely a problem in the direction and distribution of human effort. The Unsolved Riddle of Social Justice |Stephen Leacock