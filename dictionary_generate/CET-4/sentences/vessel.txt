The best vessel for taking a salad to go is a mason jar, says Cavuto. How to Make Salad You'll Actually Want to Eat |AC Shilton |August 26, 2020 |Outside Online 

The only gene-edited specimens would be the surrogate sires, which act like vessels in which the elite sperm travel. Biotechnology Could Change the Cattle Industry. Will It Succeed? |Dyllan Furness |August 16, 2020 |Singularity Hub 

Then they connected each lung to a large vein in the neck of a live pig, so that its blood flowed through the vessels. This Week’s Awesome Tech Stories From Around the Web (Through July 18) |Singularity Hub Staff |July 18, 2020 |Singularity Hub 

The XENON1T detector, located deep underground at the Gran Sasso National Laboratory in Italy, searched for interactions of dark matter particles within a large vessel filled with liquid xenon, running from 2016 to 2018. An unexpected result from a dark matter experiment may signal new particles |Emily Conover |June 17, 2020 |Science News 

Studies have shown that the coronavirus can infect pericytes, cells that wrap around blood vessels and help control flow. The way the coronavirus messes with smell hints at how it affects the brain |Laura Sanders |June 12, 2020 |Science News 

In CDC-speak, the problem is filed under the vessel sanitation program (VSP). A Doctor Explains Why Cruise Ships Should Be Banned |Kent Sepkowitz |November 19, 2014 |DAILY BEAST 

It turns out that a rising tide lifts all boats, including the rather leaky vessel carrying Kansas Republicans. Kansas Republicans Pat Roberts and Sam Brownback Lifted by Rising Tide |Ben Jacobs |November 5, 2014 |DAILY BEAST 

The tests in the study assumed that the ship would displace about 9690-tons; the Zumwalt is a 15,500-ton vessel. Can the Navy's $12 Billion Stealth Destroyer Stay Afloat? |Dave Majumdar |October 22, 2014 |DAILY BEAST 

Local mechanics pitched in to help mend the craft, but weeks into setting off the repairs wore thin and the vessel sprung a leak. Victor Mooney’s Epic Adventure for His Dead Brother |Justin Jones |October 19, 2014 |DAILY BEAST 

Within a matter of hours, the vessel that Mooney had crafted began to sink. Victor Mooney’s Epic Adventure for His Dead Brother |Justin Jones |October 19, 2014 |DAILY BEAST 

This vessel, loaded with supplies, went ashore and was lost; and one hundred and twenty Japanese and three Dutchmen were drowned. The Philippine Islands, 1493-1898, Volume XX, 1621-1624 |Various 

The noise of the hammer is always in his ears, and his eye is upon the pattern of the vessel he maketh. The Bible, Douay-Rheims Version |Various 

The vessel escaped miraculously, with sails torn by shots from three Dutch vessels, which they took for one of their own. The Philippine Islands, 1493-1898, Volume XX, 1621-1624 |Various 

At one fell swoop on the field of Jena, the famed military monarchy of the great Frederick fell in pieces like a potter's vessel. Napoleon's Marshals |R. P. Dunn-Pattison 

The American losses were seven men wounded, none killed, and only slight damage to one vessel. The Philippine Islands |John Foreman