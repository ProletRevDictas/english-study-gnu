Unfortunately, the path to the cave was flooded when we arrived, but it wasn’t our only reason to visit. A day’s drive from Chicago, exploring a very different Illinois |Carson Vaughan |February 12, 2021 |Washington Post 

Intelligent use of Google Trends means that you can probe these areas to arrive at insights that let you capitalize on what’s trending with your target audience. A comprehensive guide on using Google Trends for keyword research |Aayush Gupta |February 12, 2021 |Search Engine Watch 

Fans hit the river, arriving on paddle boards, Jet Skis and a floating tiki hut, early Wednesday morning according to WTSP-TV. Tampa Bay Buccaneers celebrate Super Bowl LV victory in boat parade |Cindy Boren, Glynn A. Hill |February 10, 2021 |Washington Post 

All players and staff must show seven consecutive coronavirus tests before arriving in Indianapolis for the NCAA tournament. Big Ten moves men’s basketball tournament to Indianapolis |Emily Giambalvo |February 9, 2021 |Washington Post 

Police say the driver called cousins for help, and they soon arrived in a Chevrolet Traverse. As police continue probe of Britt Reid crash, 5-year-old remains in critical condition |Rick Maese |February 9, 2021 |Washington Post 

Her travel clique has been known to arrive at an airport, bags packed, passport-in-hand, within hours of spotting a deal. ‘We Out Here’: Inside the New Black Travel Movement |Charlise Ferguson |January 4, 2015 |DAILY BEAST 

At the music studio, Brinsley would arrive by train or bus to break into the music scene. Alleged Cop Killer’s Blood-Soaked Screenplay |M.L. Nestel |December 24, 2014 |DAILY BEAST 

He said Jay was anxious and wondering why it was taking so long for the police to arrive. Adnan Killed Her! No, Jay Did It! Serial’s Uncertain, True-to-Reality End |Emily Shire |December 18, 2014 |DAILY BEAST 

Few series arrive with the buzz of Aaron Sorkin's HBO drama. 'The Newsroom' Ended As It Began: Weird, Controversial, and Noble |Kevin Fallon |December 15, 2014 |DAILY BEAST 

I arrive at twelve-twenty-five and the secretaries are in a tizzy. Alfred Hitchcock’s Fade to Black: The Great Director’s Final Days |David Freeman |December 13, 2014 |DAILY BEAST 

They did not arrive in time, so that some of the vessels had sailed, three of which were captured by the enemy. The Philippine Islands, 1493-1898, Volume XX, 1621-1624 |Various 

Peu aprs nostre arrive, i'escrivy l'estat auquel nous avons retrouv ceste Eglise et Colonie naissante. The Jesuit Relations and Allied Documents, Vol. II: Acadia, 1612-1614 |Various 

It was near the hour, by the time they got there, when David Arden would arrive from his northern point of departure. Checkmate |Joseph Sheridan Le Fanu 

They were sure that the white troops in Meerut would soon arrive and put an end to the prevalent anarchy. The Red Year |Louis Tracy 

Alila was not two hours old before friends began to arrive to see him. Alila, Our Little Philippine Cousin |Mary Hazelton Wade