Despite being smaller than Earth’s moon, and despite temperatures hovering around -390 degrees Fahrenheit, Pluto has a complex atmosphere composed of nitrogen laced with methane and carbon monoxide and filled with an unexpected blue haze. These Images Expose the Dark Side of the Solar System - Issue 89: The Dark Side |Corey S. Powell |August 26, 2020 |Nautilus 

In the new study, Kwok-Yung Yuen, a microbiologist at the University of Hong Kong, and colleagues analyzed most of the genetic blueprint, composed of RNA, of each of the patient’s coronavirus infections and looked for differences. A man in Hong Kong is the first confirmed case of coronavirus reinfection |Erin Garcia de Jesus |August 24, 2020 |Science News 

The goal hasn’t been to get cheaper but to get better composed, that’s ongoing and will continue. ‘It’s less dire than it seemed to be’: How The Wall Street Journal’s digital ads business has weathered the downturn |Lucinda Southern |August 20, 2020 |Digiday 

Yet if you examine very large, massive galaxies you find that they tend to be composed of older stars, suggesting that they’ve already sat around in their dotage for a very long time. The Universe Has Made Almost All the Stars It Will Ever Make - Issue 89: The Dark Side |Caleb Scharf |August 19, 2020 |Nautilus 

His commentaries comparing Galileo’s time to today’s are weaved into an engagingly composed and pleasantly readable account. A new Galileo biography draws parallels to today’s science denialism |Tom Siegfried |August 11, 2020 |Science News 

He then enlisted the help of New York City musician Carlo Nicolau to compose the music. The Government Is Using Subliminal Songs To Scare Immigrants |Caitlin Dickson |July 12, 2014 |DAILY BEAST 

“One can no more write good English than one can compose good music by merely keeping to the rules,” he wrote. Will Jargon Be the Death of the English Language? |The Telegraph |March 30, 2014 |DAILY BEAST 

Perhaps compose a fiction about hard-wired fictionalizing, a fiction that reminds readers of their synaptic deceptions. The Brain Man: What is E. L. Doctorow Up to? |Tom LeClair |January 13, 2014 |DAILY BEAST 

But Google grumble grumble makes me compose new messages in a tiny window, you say. Gmail Priority Inbox Freakout: Calm Down, Guys, Opting Out Is Easy |Winston Ross |July 23, 2013 |DAILY BEAST 

After defense explained that it did not consider the document intelligence, Lind compose herself and let defense proceed. Is Judge Denise Lind Bradley Manning’s Biggest Enemy? |Alexa O'Brien |July 19, 2013 |DAILY BEAST 

He placed the paper on the table, and, ere he read a syllable, he laboured to compose himself. Blackwood's Edinburgh Magazine, No. CCCXXXIX. January, 1844. Vol. LV. |Various 

We suddenly realized that after all the greatness and strength of a nation is made up of the men and women who compose it. The Unsolved Riddle of Social Justice |Stephen Leacock 

The act of the Covenanting Society is complex, and is the aggregate of the actings of all who compose it. The Ordinance of Covenanting |John Cunningham 

Controve, compose or invent tunes, foule fayle, fail miserably. Chaucer's Works, Volume 1 (of 7) -- Romaunt of the Rose; Minor Poems |Geoffrey Chaucer 

About this time he began to compose, although his attempts were merely improvisations. The Life &amp; Letters of Peter Ilich Tchaikovsky |Modeste Tchaikovsky