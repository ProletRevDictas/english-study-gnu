Goodell and Sills said at the outset that they hoped to use the season to “contribute knowledge and insights that will aid the country’s pandemic response.” The NFL’s pandemic response was a striking success — and a genuine public service |Sally Jenkins |February 10, 2021 |Washington Post 

Ensuring your mask isn’t too big from the outset will help you avoid having to constantly pull your mask back up over your nose and prevent gaps from forming at the sides and bottom. How to wear a face mask for maximum protection |Erin Fennessy |February 2, 2021 |Popular-Science 

At the outset of the pandemic, hospitals faced a crippling lack of supplies. Capt. Tom Moore dies after covid diagnosis. The 100-year-old raised millions for Britain's NHS. |Jennifer Hassan, William Booth |February 2, 2021 |Washington Post 

At the outset, this is a 137% increase over the revised healthcare estimates of budget 2020-21. India’s healthcare budget sees an improvement, but not as much as the government claims |Manavi Kapur |February 1, 2021 |Quartz 

At the outset — in 2010 — almost every state signed up for one of the two testing consortia. What you need to know about standardized testing |Valerie Strauss |February 1, 2021 |Washington Post 

But the real money, Washington believed, lay in the western land investments he had made at the outset of the war. Washington’s Wheeler-Dealer Patriotism |William O’Connor |October 31, 2014 |DAILY BEAST 

Bill Clinton courted Mixner and his expertise at the outset of his run for president. Gay Activist David Mixner: I Mercy Killed 8 People |Tim Teeman |October 29, 2014 |DAILY BEAST 

Things seem fine at the outset, as they always do—the children are fun and adorable, the husband and wife happy and carefree. ‘Force Majeure’ and the Swedish Family Vacation From Hell |Alex Suskind |October 27, 2014 |DAILY BEAST 

At the outset, only a handful of the victims spoke out about what had happened to them. Jennifer Lawrence’s Furious, Perfect Response to Nude Photo Leak: “It Is a Sex Crime” |Kevin Fallon |October 7, 2014 |DAILY BEAST 

Is it day to day, game to game, or do they prepare a master plan from the outset. Tony La Russa Explains How To Make It To The World Series |Dave Pottruck |October 4, 2014 |DAILY BEAST 

Since we are to learn by thinking we must at the outset learn the definition of the three Laws of Thinking. Assimilative Memory |Marcus Dwight Larrowe (AKA Prof. A. Loisette) 

Here, however, is the place where the water trouble will first arise, which will have to be provided for at the outset. Asbestos |Robert H. Jones 

From the very outset of his career in Spain he showed a lack of strategic insight and a want of rapidity of movement. Napoleon's Marshals |R. P. Dunn-Pattison 

Thereupon began a procedure identical to that which had characterized the outset of every successful case of the Chief Inspector. Dope |Sax Rohmer 

Organization was Aguinaldoʼs peculiar talent, without the exercise of which the movement would have failed at the outset. The Philippine Islands |John Foreman