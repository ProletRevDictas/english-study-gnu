Bannon is the group’s president and is described as working 80 hours a week for the group with no salary. Federal Prosecutors Have Steve Bannon’s Murky Nonprofit in Their Sights |by Yeganeh Torbati |August 24, 2020 |ProPublica 

The company cut its dividend for the second quarter by 38 percent, suspended more than $1 billion of development projects and temporarily reduced staff salaries by as much as 30 percent. America’s Largest Shopping Mall Owner Gets a New Tenant: Itself |Daniel Malloy |August 20, 2020 |Ozy 

Because all of our big sports, the average salaries are in the millions of dollars. The Economics of Sports Gambling (Ep. 388 Rebroadcast) |Stephen J. Dubner |August 20, 2020 |Freakonomics 

The researchers surmised that the company would have to raise salaries by a third in order to compete with simple messaging of social impact. Why a year later, the Business Roundtable’s updated statement of purpose is more relevant than ever |jakemeth |August 19, 2020 |Fortune 

Under the scheme, the UK government pays workers up to 80% of their salary for a limited period of time, allowing companies to retain them without paying them—though companies were allowed to top up the government money. Furloughed Brits got paid not to work—but two-thirds of them worked anyway |Cassie Werber |August 18, 2020 |Quartz 

They will still receive a salary if something is to happen to Ziad, but she is trying to make sure she saves as much as possible. A Sunni-Shia Love Story Imperiled by al Qaeda |Ruth Michaelson |December 26, 2014 |DAILY BEAST 

Yes, lawyers bill by the hour but are paid an annual salary—plus bonuses. How Amazon Became Santa’s Sweatshop |Sally Kohn |December 11, 2014 |DAILY BEAST 

Clinkscales is still a cop, and made more than $100,000 in salary and overtime last year. Chicago’s Cops Don’t Even Get Investigated for Shooting People in the Back |Justin Glawe |December 5, 2014 |DAILY BEAST 

Still, he admitted—without disclosing his salary—that he wondered whether the paychecks were too good to last. The Godfather of Right-Wing Radio |Caitlin Dickson |November 23, 2014 |DAILY BEAST 

He looks like a man of the woods, but works at The Nerdery, programming for a healthy salary and benefits. How Straight World Stole ‘Gay’: The Last Gasp of the ‘Lumbersexual’ |Tim Teeman |November 12, 2014 |DAILY BEAST 

With this object in view, he has been continually paid his salary from the judicial expenses. The Philippine Islands, 1493-1898, Volume XX, 1621-1624 |Various 

Sir Peter Edlin, it seems, has been doing treble the amount of work for a two-third's salary. Punch, or the London Charivari, Volume 107, November 3, 1894 |Various 

Thimbletoes doesn't fancy that, you know, because the Prime Minister has all the honey he wants, by way of a salary. Davy and The Goblin |Charles E. Carryl 

Your Majesty assigns him no salary, for it seems to be your intention to have him attend to that duty with his salary as fiscal. The Philippine Islands, 1493-1898, Volume XX, 1621-1624 |Various 

Twenty dollars a month is the salary, and school keeps for six months, so I shall earn the large sum of $120 a year. The Soldier of the Valley |Nelson Lloyd