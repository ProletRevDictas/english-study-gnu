I told the reporter that this chatbot, no matter how clever, can only disorient and disappoint. That chatbot I’ve loved to hate |Tate Ryan-Mosley |August 19, 2020 |MIT Technology Review 

However, the no-commitment model would also seem to represent special risks if Canoo’s products disappoint or consumers prove flighty. Electric-vehicle startup Canoo to go public, joining the wave of companies chasing Tesla’s success |dzanemorris |August 18, 2020 |Fortune 

Proponents of the measure tried to get the Council to put similar reforms on the 2016 and 2018 ballot, but were disappointed each time. Morning Report: Lilac Hills Is Back (Again) |Voice of San Diego |June 24, 2020 |Voice of San Diego 

He said he might have been content with a smaller cut that showed a commitment, while others would have been disappointed with anything less than the full number. Despite Recent Budget Vote, Reformers See SDPD Cuts as Inevitable |Andrew Keatts |June 23, 2020 |Voice of San Diego 

Pai’s parents, both doctors, were initially disappointed that he chose law over medicine — but they’ve since come around. Can You Hear Me Now? (Ep. 406) |Stephen J. Dubner |February 20, 2020 |Freakonomics 

Regardless of whom President Obama picks to be his next Attorney General, he is bound to disappoint key segments of his coalition. Obama's No-Win Attorney General Decision |Tim Mak |October 10, 2014 |DAILY BEAST 

And, Scott is never one to disappoint with his runway productions. Miley Cyrus Channels Her Bad Year Into Rave-Kid Art |Justin Jones |September 11, 2014 |DAILY BEAST 

Just like her memoir, Lee Grant does not disappoint when it comes to candor in an interview with The Daily Beast. The Unsinkable Lee Grant Sets the Record Straight |William O’Connor |July 23, 2014 |DAILY BEAST 

The rapper doesn't disappoint in this feel good comedy, which chronicles a day in the life of a South Side Chicago barbershop. 9 New Movies to Stream on Netflix This June |The Daily Beast |June 9, 2014 |DAILY BEAST 

So far the signs are that the turnout will disappoint the army. Islamists Boycott Egypt’s Constitutional Vote |Jamie Dettmer |January 14, 2014 |DAILY BEAST 

Sir George was p. 138not present, something had happened, for he was not the man to disappoint his friends without grave cause. Fifty Years of Railway Life in England, Scotland and Ireland |Joseph Tatlow 

Baxter, I don't want to disappoint you, but I have reluctantly come to the conclusion that you are one of the mob. First Plays |A. A. Milne 

Something had happened to disappoint and annoy them—that much he could gather from their gestures and impassioned speech. The Red Year |Louis Tracy 

I am a traitor to my oath, for I now know I shall never disappoint Eva's faith in me. The Circular Study |Anna Katharine Green 

Yes,” said Algitha, “my mother has had a lot of troublesome children to worry and disappoint her. The Daughters of Danaus |Mona Caird