I’m proud to teach at a school where we’ve taken the concerns and views of our parents seriously and better yet, taken action. Our Public Schools Have a Customer Service Problem |Thomas Courtney |September 10, 2020 |Voice of San Diego 

I was fully convinced that we were all doomed to be outsiders for our entire lives, and secretly, I was kind of proud of that. ‘The Dream Architects’: Inside the making of gaming’s biggest franchises |Rachel King |September 1, 2020 |Fortune 

For performance management, we decided to combine it with our goals and ask employees to tell us what three to five things they are most proud of, and what they need more or less of from their manager. 5 of the best tips for navigating the pandemic at work from chief HR officers |Michal Lev-Ram, writer |August 31, 2020 |Fortune 

What stuck out to me, too, was how proud it seemed like the former players were of the current players for taking this stand. What Happened In The NBA This Week? |Sara Ziegler (sara.ziegler@fivethirtyeight.com) |August 28, 2020 |FiveThirtyEight 

He was proud of what he believed in, he stood up for it, and he fought for it. Full Transcript: Sean Spicer on ‘The Carlos Watson Show’ |Daniel Malloy |August 26, 2020 |Ozy 

I had wanted to give him something, something to make him proud. The Story Behind Lee Marvin’s Liberty Valance Smile |Robert Ward |January 3, 2015 |DAILY BEAST 

But that makes the Ismael brothers no less proud of the resistance that they and other fellow fighters have put up. The Brothers Who Ambushed ISIS |Mohammed A. Salih |December 27, 2014 |DAILY BEAST 

She added: “NBC News is proud to have David in the important anchor chair of ‘Meet the Press.’ ” David Gregory's 'Meet the Press' Eviction Exposed in Washingtonian Takedown |Lloyd Grove |December 23, 2014 |DAILY BEAST 

We proud skeptics would rather trust the demonstrable facts than the alleged truth. On Torture, Chuck Johnson & Sondheim |James Poulos |December 13, 2014 |DAILY BEAST 

Hitchcock is very proud of his kitchen; he's comfortable here. Alfred Hitchcock’s Fade to Black: The Great Director’s Final Days |David Freeman |December 13, 2014 |DAILY BEAST 

I, therefore, deliver it as a maxim, that whoever desires the character of a proud man ought to conceal his vanity. Pearls of Thought |Maturin M. Ballou 

You never cared—you were too proud to care; and when I spoke to you about my fault, you did n't even know what I meant. Confidence |Henry James 

To be so humbled in the knowledge of any living being, was the vultures of Prometheus to the proud heart of Ripperda. The Pastor's Fire-side Vol. 3 of 4 |Jane Porter 

You may imagine the effect this missive produced upon the proud, high-minded doctor of divinity. Elster's Folly |Mrs. Henry Wood 

"I'm not proud," replied Davy, provoked at being mixed up with Gobobbles in this way. Davy and The Goblin |Charles E. Carryl