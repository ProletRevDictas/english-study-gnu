For starters there are the 100,000 trillion photons arriving every second at every square centimeter of Earth’s dayside surface, after racing here from the outer envelope of a natural giant thermonuclear reactor we call the sun. The Universe Has Made Almost All the Stars It Will Ever Make - Issue 89: The Dark Side |Caleb Scharf |August 19, 2020 |Nautilus 

It makes the same prediction for spaces of every dimension — that in covering, say, 12-dimensional space using 12-dimensional “square” tiles, you will end up with at least two tiles that abut each other exactly. Computer Search Settles 90-Year-Old Math Problem |Kevin Hartnett |August 19, 2020 |Quanta Magazine 

Treating arid land with LNC costs $2 to $5 per square meter, far more than many farmers can afford. A Norwegian Startup Is Turning Dry Deserts Into Fertile Cropland |Vanessa Bates Ramirez |August 19, 2020 |Singularity Hub 

Most of these were “gridworlds”—literally two-dimensional grids with objects in some squares. DeepMind’s Newest AI Programs Itself to Make All the Right Decisions |Jason Dorrier |July 26, 2020 |Singularity Hub 

The median sales price of homes in Cheesman Park is $350,000 with an average of $401 per square foot. Denver: Where the Queer Community is a Mile High |LGBTQ-Editor |July 14, 2020 |No Straight News 

Some longtime local acquaintances are struggling to square the man they know with the ugly associations. No. 3 Republican Admits Talking to White Supremacist Conference |Tim Mak |December 30, 2014 |DAILY BEAST 

This last trip it had felt as if there were more cameras around Havana than Times Square. The Life and Hard Times Of The Family A Cuban Defector Left Behind |Brin-Jonathan Butler |December 19, 2014 |DAILY BEAST 

Records describe this house as 6,916 square feet, with six bedrooms and eight bathrooms. The Luxury Homes That Torture and Your Tax Dollars Built |Michael Daly |December 12, 2014 |DAILY BEAST 

The international media had been waiting on Wenceslas Square since early afternoon. How Havel Inspired the Velvet Revolution |Michael Zantovsky |December 6, 2014 |DAILY BEAST 

In the afternoon, about a thousand people marched in protest through the largest Prague square, with police nowhere in sight. How Havel Inspired the Velvet Revolution |Michael Zantovsky |December 6, 2014 |DAILY BEAST 

Nowhere can be found a region capable of supporting a larger population to the square mile than Lombardy. Glances at Europe |Horace Greeley 

For years his lordship was seldom seen in London, the great house in Grosvenor Square was never opened. The Pit Town Coronet, Volume I (of 3) |Charles James Wills 

She opened the door of a square room with large roses on the white wall-paper, and fine old mahogany furniture. Ancestors |Gertrude Atherton 

I would not just then have traded off that steamboat for several square miles of snow-capped sublimity. Glances at Europe |Horace Greeley 

The building, which has five storeys, stands on three sides of a square courtyard, and faces into Edmund Street. Showell's Dictionary of Birmingham |Thomas T. Harman and Walter Showell