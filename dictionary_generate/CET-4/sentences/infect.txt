The question of how the virus probably jumped from bats to infect humans has been a mystery since the start of the outbreak. WHO team in Wuhan dismisses lab leak theory, continues hunt for intermediary coronavirus host |Gerry Shih |February 9, 2021 |Washington Post 

If a nursing-home patient was infected at his home but died in a hospital, it wasn’t counted. What to make of New York’s revised nursing-home coronavirus numbers |Philip Bump |February 9, 2021 |Washington Post 

People may continue to get infected with the virus, but the infections are not as severe. How coronavirus variants may drive reinfection and shape vaccination efforts |Erin Garcia de Jesus |February 5, 2021 |Science News 

It has infected more than 100 million others, and new variants threaten another surge in cases even as vaccines have begun to roll out. Smallpox used to kill millions of people every year. Here’s how humans beat it. |Kelsey Piper |February 5, 2021 |Vox 

You just don’t know if they’re infected, so, as difficult as that is, at least this time around, just lay low and cool it. Super spreader Sunday? Experts worry Super Bowl could trigger coronavirus explosion |Brittany Shammas, Fenit Nirappil, Mark Maske |February 5, 2021 |Washington Post 

While the bats are infected, they shed large quantities of virus that can infect other animals. Bats’ Link to Ebola Finally Solved |Carrie Arnold |November 12, 2014 |DAILY BEAST 

Ebola Reston, it seemed, could infect humans, but never became symptomatic. Already Deadly in Africa, Could Ebola Hit America Next? |Scott Bixby |April 5, 2014 |DAILY BEAST 

They had to infect the perfectly adequate data with the totally improbable idea of a 400-year-old heirloom elk antler tool. Incontrovertible Evidence Proves the First Americans Came From Asia |Doug Peacock |March 27, 2014 |DAILY BEAST 

But what I think is that this constant wound, that this constant sore, does infect all of our foreign policy. Pro-Israel Group Attacks New York Times' Iran Correspondent |Ali Gharib |August 5, 2013 |DAILY BEAST 

Have you wondered how the U.S. managed to infect Iranian computers? Obama Leaks for Votes |David Frum |June 2, 2012 |DAILY BEAST 

A very light attack of any of these diseases in one child may infect another fatally. Essays In Pastoral Medicine |Austin Malley 

One child coming down with scarlet fever, measles, or whooping cough can infect twenty others at an afternoon party. The Mother and Her Child |William S. Sadler 

Spores of parasitic fungi enter the cracks, germinate and infect the heartwood. Our National Forests |Richard H. Douai Boerker 

Because the milk doth grow sour in the stomach, where evil humours are bred, and infect the breath. The Works of Aristotle the Famous Philosopher |Anonymous 

It was necessary to infect them in the mass so that as individuals they might infect others with the fever to buy bonds. Harper's Pictorial Library of the World War, Volume XII |Various