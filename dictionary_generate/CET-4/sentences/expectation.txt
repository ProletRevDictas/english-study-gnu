Still, the very presence of paramedics at sites where forced entry warrants are to be used could suggest the expectation that someone will be harmed. The policing reforms in the Breonna Taylor settlement, explained |Fabiola Cineas |September 17, 2020 |Vox 

“This removal was about an IG who in my mind was increasingly falling short of expectations,” he added. State Department offers murky analysis of job performance survey to justify inspector general’s firing |Karoun Demirjian, Carol Morello |September 17, 2020 |Washington Post 

She was a student who would drive up performance and expectations and build community. When a Calculus Class Abruptly Became Ceramics at Lincoln High |Scott Lewis |September 16, 2020 |Voice of San Diego 

One reason for that is unlike in 2000, “these companies actually have revenues and earnings, they’re not just trading on fumes or expectations,” CFRA Research’s Sam Stovall tells Fortune. 3 ways tech stocks resemble the 2000 bubble—and one way they don’t |Anne Sraders |September 16, 2020 |Fortune 

Few groups in stock market history have been accorded such great expectations. Will tech stocks stumble or slide? What the fundamentals tell us |Shawn Tully |September 16, 2020 |Fortune 

The loss of this “expectation” game began his decline and ultimate withdrawal from the race. The World’s Toughest Political Quiz |Jeff Greenfield |December 31, 2014 |DAILY BEAST 

I had a couple bluntly tell me that they hated it here, with the silent expectation that I would too. Freshman Year Sucks—and That’s OK |Eleanor Hyun |November 12, 2014 |DAILY BEAST 

Their expectation was that her petition to have her passport returned might be honored. Let’s Free Stacey Addison, The Oregon Woman Jailed at the Ends of the Earth |Christopher Dickey |October 30, 2014 |DAILY BEAST 

Now, there are no cliffhangers, just an expectation from the audience that the blood will flow. Sex, Blood and Maroon 5: Pop Culture’s Wounds Run Deep |Lizzie Crocker |October 3, 2014 |DAILY BEAST 

Every day, I drove from my flat in Mayfair to Abbey Road in joyous expectation of what magic I would be participating in that day. When Gary Wright Met George Harrison: Dream Weaver, John and Yoko, and More |Gary Wright |September 29, 2014 |DAILY BEAST 

Ollie saw someone standing before it, bending slightly forward in the pose of expectation. The Bondboy |George W. (George Washington) Ogden 

He obeyed without remark, though with an unsteady voice, as he uttered communications he knew were so hostile to her expectation. The Pastor's Fire-side Vol. 3 of 4 |Jane Porter 

Upon such expectation, Allcraft stood—upon such props suffered his aching soul to rest. Blackwood's Edinburgh Magazine, No. CCCXXXIX. January, 1844. Vol. LV. |Various 

I knew this was a city of noble and beautiful structures, but the reality surpasses my expectation. Glances at Europe |Horace Greeley 

This expectation of high dividends, I need hardly say, has not been realised, and the Act in this respect has been a dead letter. Fifty Years of Railway Life in England, Scotland and Ireland |Joseph Tatlow