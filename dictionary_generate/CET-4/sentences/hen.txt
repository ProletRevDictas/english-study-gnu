All the eggs produced by hens sexed with this technology are sold under the Respeggt brand, free from chick culling. Why the US egg industry is still killing 300 million chicks a year |Tove K. Danovich |April 12, 2021 |Vox 

That is not the case with egg-laying hens, which have been bred to put all their energy toward laying. Why the US egg industry is still killing 300 million chicks a year |Tove K. Danovich |April 12, 2021 |Vox 

In chickens, the hen controls whether the resulting chick is male or female. Why the US egg industry is still killing 300 million chicks a year |Tove K. Danovich |April 12, 2021 |Vox 

File this under, “Sure, I’ll watch your hens,” said the fox. The Outré Art of Pegging |Eugene Robinson |December 14, 2020 |Ozy 

You can look at the number of hens laying eggs in this country. What Happens When Everyone Stays Home to Eat? (Ep. 412) |Stephen J. Dubner |April 9, 2020 |Freakonomics 

He even claims that hen partridges conceive just by smelling the scent of males. Why Aristotle Deserves A Posthumous Nobel |Nick Romeo |October 18, 2014 |DAILY BEAST 

“[W]hen the going got tough, his economic team picked Wall Street,” Warren said. Warren and Christie Are the Anti-Hillarys |Sally Kohn |October 14, 2014 |DAILY BEAST 

He eventually brings his wife and children over, and later he manages a hen and rabbit farm. Nothing Was Banal About Eichmann’s Evil, Says a Scathing New Biography |Michael Signer |October 11, 2014 |DAILY BEAST 

He described her then as “a mother hen who took care of everyone.” Joni Ernst's Big Pivot: From Pig Castrator to Iowa Nice |Ben Jacobs |August 11, 2014 |DAILY BEAST 

“[W]hen a novelist finds an audience, even a small one … the relation is based on recognition, not misunderstanding,” he writes. In Defense of Jonathan Franzen |Michelle Goldberg |September 26, 2013 |DAILY BEAST 

Out of the darkening sky rang the twanging call of a night-hawk, and the cluck of a dozing hen sounded from the foliage overhead. The Soldier of the Valley |Nelson Lloyd 

What can be prettier than a brood of chickens with a good motherly hen, like the one in this picture! The Nursery, July 1873, Vol. XIV. No. 1 |Various 

“Well, Hen knows how to kill snakes, but maybe she is a poor judge of character,” laughed Amy. The Campfire Girls of Roselawn |Margaret Penrose 

The storm hath passed;I hear the birds rejoice; the hen,Returned into the road again,Her cheerful notes repeats. The Poems of Giacomo Leopardi |Giacomo Leopardi 

Their manner of talking has been compared to the clucking of a hen, and by the Dutch to the "gobbling of a turkeycock." Man And His Ancestor |Charles Morris