Physicists don’t usually deal with objects of different sizes at the same time. How Mathematical ‘Hocus-Pocus’ Saved Particle Physics |Charlie Wood |September 17, 2020 |Quanta Magazine 

Powerful gusts not only blow objects around, they also churn up massive waves called storm surges. Soggy coastal soils? Here’s why ecologists love them |Alison Pearce Stevens |September 17, 2020 |Science News For Students 

When you only look at things close-up, your eyes don’t get practice focusing on distant objects. Healthy screen time is one challenge of distance learning |Kathryn Hulick |September 11, 2020 |Science News For Students 

GPT-3’s word embeddings are not perceptually grounded in the world, which explains why it often struggles to consistently answer common-sense questions about visual and physical features of familiar objects. Welcome to the Next Level of Bullshit - Issue 89: The Dark Side |Raphaël Millière |September 9, 2020 |Nautilus 

It’s not yet clear what this object is and what its purpose might be. China says it has launched and landed a reusable spacecraft |Neel Patel |September 8, 2020 |MIT Technology Review 

But by far the most interesting object, which held enormous fascination for me, sat high up on the top shelf. My Love Letter to the Stetson |Mark McKinnon |December 24, 2014 |DAILY BEAST 

And who better to do that with than the actress who is playing the object of said (alleged) lesbian affection in the flick? Inside the Lifetime Whitney Houston Movie’s Lesbian Lover Storyline |Kevin Fallon |December 16, 2014 |DAILY BEAST 

If they run off with somebody else, we say they were stolen—as if they are an object or a commodity. Owning Up to Possession’s Downside |Samantha Harvey |December 14, 2014 |DAILY BEAST 

He can barely speak the titles, but manages to let Viridiana and That Obscure Object of Desire pass from his lips. Alfred Hitchcock’s Fade to Black: The Great Director’s Final Days |David Freeman |December 13, 2014 |DAILY BEAST 

The show, Bell Hooks argued in Black Looks: Race and Representation, “represents wom[e]n as the object of a phallocentric gaze.” Science-Fiction TV Finds a New Muse: Feminism |David Levesley |November 29, 2014 |DAILY BEAST 

The thought seemed to produce the dreaded object, for next moment a large hummock appeared right ahead. The Giant of the North |R.M. Ballantyne 

Naturally the conversation fell on the all-absorbing topic of the day and the object of his mission. The Philippine Islands |John Foreman 

To make the effort of articulation a vital impulse in response to a mental concept,—this is the object sought. Expressive Voice Culture |Jessie Eldridge Southwick 

The object of this practise is to attain facility in manipulating the elements while maintaining the smooth quality of the tone. Expressive Voice Culture |Jessie Eldridge Southwick 

Every time he is dressed, or sees his mother dress, he has an object-lesson in symmetrical arrangement. Children's Ways |James Sully