The meat was almost blackened by the time it absorbed the smoke, and while the skin was crisp, it gave way between my teeth. Until I Can Go Back to My Favorite Restaurant, This Jerk Paste Is the Next Best Thing |Elazar Sontag |September 25, 2020 |Eater 

With a more extensive root system, plants can absorb more nutrients and pump more exudates into the soil to recruit more helpful microbes that can make more indole-3-acetic acid. Junk Food Is Bad For Plants, Too - Issue 90: Something Green |Anne Bikl&#233; & David R. Montgomery |September 23, 2020 |Nautilus 

A few amphibians don’t bother with lungs and instead absorb oxygen through their skin. Scientists Say: Amphibian |Bethany Brookshire |September 21, 2020 |Science News For Students 

Even with an increase in volume, the system has space to absorb more passengers and still perform well. Planning to fly for the holidays? 6 things to know before you book |matthewheimer |September 19, 2020 |Fortune 

I wanted to learn as much as I could about the trail and, as an educator, he was happy to give me as much information as I could absorb. Inside an FKT Attempt on the Appalachian Trail |Martin Fritz Huber |September 3, 2020 |Outside Online 

Similar reinforced plinths were developed by the Getty museums in Los Angeles to absorb the seismic movements there. Florence Preps ‘David’ for the Big One |Barbie Latza Nadeau |December 25, 2014 |DAILY BEAST 

In the book, you say “Absorb youth and you will be absorbed by youth.” George Clinton on Industry ‘Mobsters’ and How Nobody Wants to Listen to a Crackhead |Curtis Stephen |November 19, 2014 |DAILY BEAST 

But the details of this massacre have been especially difficult to absorb. Anatomy of a Mexican Student Massacre |Jason McGahan |October 8, 2014 |DAILY BEAST 

It gave me license to pore over raw tape, again and again, to absorb the subtle clues of human behavior. We Interrupt This Broadcast: How a TV Producer Learned to Write Fiction |George Lerner |September 9, 2014 |DAILY BEAST 

“I welcome China to Africa because Africa is big enough to absorb China,” he said. The American Elite Embraces a New Africa at D.C. Summit |Eleanor Clift |August 6, 2014 |DAILY BEAST 

Never smoke when the pores are open: they absorb, and you are unfit for decent society. Tobacco; Its History, Varieties, Culture, Manufacture and Commerce |E. R. Billings. 

It was with much amazement that they watched Henrietta absorb sandwiches, cake, eggs, and fruit. The Campfire Girls of Roselawn |Margaret Penrose 

Here one can be alive and absorb something of the earth-forces that never get within touching distance in the cities. Three More John Silence Stories |Algernon Blackwood 

The recent researches of Brustlein have shown that lime does cause the organic matters to absorb ammonia from its salts. Elements of Agricultural Chemistry |Thomas Anderson 

Should it still be too moist to be sown, it must be again turned over, and mixed with some dry substance to absorb the moisture. Elements of Agricultural Chemistry |Thomas Anderson