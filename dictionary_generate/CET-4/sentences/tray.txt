Inkjet printers also tend to print more slowly, be louder, and hold less paper in their trays than lasers. Best all-in-one printer: Upgrade your home office with these multitasking machines |Carsen Joenk |February 8, 2021 |Popular-Science 

Metal trays designed for surgical instruments double as bedside tables. Inside a Rhode Island field hospital, preparing for the worst of the pandemic |Lenny Bernstein |December 26, 2020 |Washington Post 

A bigger issue is that rural hospitals are often small with few employees, but Pfizer sends its packages in trays with 975 doses. Rural America may miss out on early COVID-19 vaccines |Tara Santora |December 18, 2020 |Popular-Science 

Perhaps you can suggest that until your husband can display good manners, you will be dining elsewhere — even if that means in the bedroom with a tray. Miss Manners: Husband’s rude eating habits driving wife up the wall |Judith Martin, Nicholas Martin, Jacobina Martin |December 18, 2020 |Washington Post 

Rather than needing to be watered, the plants actually sit in grow trays with their roots extending into shallow troughs of nutrient-rich water. Europe’s Biggest Vertical Farm Will Be Powered by Wind and Planted by Robots |Vanessa Bates Ramirez |December 11, 2020 |Singularity Hub 

Disturbed by these suppositions and deciding not to tell my wife, I made the tea and took the tray to the bedroom. Knocking on Heaven's Door: True Stories of Unexplained, Uncanny Experiences at the Hour of Death |Patricia Pearson |August 11, 2014 |DAILY BEAST 

A doorman at the Ritz-Carlton took to serving him carrots on a silver tray. Central Park’s Carriages Saved This Horse |Michael Daly |April 24, 2014 |DAILY BEAST 

At another mortuary, “a very stout matron” has frozen to the metal refrigerator tray and must be chipped from it. Death Became Her: Molly Lefebure’s Wartime Years of Murder and Suicide |Tim Teeman |April 2, 2014 |DAILY BEAST 

Willie signals to the stewardess that he needs some help with his tray. Stacks: Hitting the Note with the Allman Brothers Band |Grover Lewis |March 15, 2014 |DAILY BEAST 

The dragon, now dangling by a claw from the edge of the sand tray, is on the cusp of defeat. The Rise of Superhero Therapy: Comic Books as Psychological Treatment |Alex Suskind |February 17, 2014 |DAILY BEAST 

It is only necessary to have a zinc, or a galvanized tray on which to stand the glass in an inverted position. How to Know the Ferns |S. Leonard Bastin 

Broken crocks should be strewn upon the tray, and on to this is heaped peaty soil mixed with sand. How to Know the Ferns |S. Leonard Bastin 

A light-colored mulatto boy, in dress coat and bearing a diminutive silver tray for the reception of cards, admitted them. The Awakening and Selected Short Stories |Kate Chopin 

The air was heavy with the perfume of frankincense which smouldered in a brass vessel set upon a tray. Dope |Sax Rohmer 

The boy retired and returned after a moment, bringing the tiny silver tray, which was covered with ladies' visiting cards. The Awakening and Selected Short Stories |Kate Chopin