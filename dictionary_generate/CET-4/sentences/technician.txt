The contract was so costly, Seide said, because it required technicians to build a search tool to sort through millions of emails archived on USAGM’s servers — even before the lawyers could get to work. Former Voice of America overseer hired two law firms to $4 million no-bid contracts. |Paul Farhi |January 25, 2021 |Washington Post 

Before taking the bench, they are required to undergo fewer hours of training than the Palmetto State requires of its barbers and hair salon technicians. South Carolina’s Governor Addresses Magistrate Judge Controversy by Urging Changes |by Joseph Cranney, The Post and Courier |January 15, 2021 |ProPublica 

This is a perfect opportunity for donors, public and private sectors, investors, and technicians to collaborate and create a positive outcome for all parties. Climate disasters are inevitable. We need to do more than just wait to clean up the damage |Jake Meth |January 12, 2021 |Fortune 

These technicians often move up the career ladder by obtaining other degrees. COVID-19 testing scientists are the unsung heroes of the pandemic |By Rodney E. Rohde/The Conversation |December 16, 2020 |Popular-Science 

Meanwhile, wait times for results are stretching for weeks, forcing lab technicians to work around the clock. The state of COVID-19 testing in the US |Tara Santora |December 9, 2020 |Popular-Science 

The technician on the other end is probably in a laboratory a thousand miles away. Alfred Hitchcock’s Fade to Black: The Great Director’s Final Days |David Freeman |December 13, 2014 |DAILY BEAST 

Sisler is described as a “Medical/Surgical instrument technician” in his own court declaration. Patients Screwed in Spine Surgery ‘Scam’ |The Center for Investigative Reporting |November 3, 2014 |DAILY BEAST 

Landon Wilson, a U.S. Navy cryptologic technician, returned from Afghanistan in 2013 to a promotion for good work. Yes to LGB, No to T: The Pentagon Still Has a Transgender Ban |Tim Mak |October 21, 2014 |DAILY BEAST 

Others who had become infected included a technician from the lab that had been returning mistaken results. How Bureaucrats Let Ebola Spread to Nigeria |Michael Daly |August 14, 2014 |DAILY BEAST 

For a decade, Maier worked as a senior technician with Hughes Aircraft in California. The Hop-Crazy Master Brewer |Nina Strochlic |June 9, 2014 |DAILY BEAST 

In other words, the technician is the man who invents or preserves labels to be pasted on the intuitive practices of his art. Seeing Things at Night |Heywood Broun 

It is superfluous to make fun of the fact that the technician writes on his visiting cards: Stud. Criminal Psychology |Hans Gross 

Twenty steps down the corridor, a Negro technician was focusing a sharp lens on page three of Arriba for April 27, 1938. The Five Arrows |Allan Chase 

It is recommended that only your authorized Zenith television technician make repairs or adjustments inside the receiver. Zenith Television Receiver Operating Manual |Zenith Radio Corporation 

The senior technician's eyes were still on the bright needle points. The Syndic |C.M. Kornbluth