Having no electric charge, the neutron was the ideal bullet to shoot into an atom, able to penetrate the nucleus and destabilize it. How understanding nature made the atomic bomb inevitable |Tom Siegfried |August 6, 2020 |Science News 

In an atom, the protons and neutrons hang out in the center, or nucleus. Scientists Say: Atom |Bethany Brookshire |July 20, 2020 |Science News For Students 

The familiar protons and neutrons that make up the atomic nucleus are examples of baryons. CERN: Physicists Report the Discovery of Unique New Particle |Harry Cliff |July 15, 2020 |Singularity Hub 

Those energy-producing organelles are inherited from a mother and have their own DNA, distinct from the genetic information — from both parents — that’s stored in a cell’s nucleus. A bacterial toxin enables the first mitochondrial gene editor |Jack J. Lee |July 13, 2020 |Science News 

Alternatively, the detector might contain a minute amount of tritium, a radioactive form of hydrogen with two neutrons in its nucleus. An unexpected result from a dark matter experiment may signal new particles |Emily Conover |June 17, 2020 |Science News 

Deuterium is an isotope of hydrogen containing a proton and neutron in its nucleus, while normal hydrogen has only a proton. Are Comets the Origin of Earth’s Oceans? |Matthew R. Francis |December 14, 2014 |DAILY BEAST 

A July 1884 New York Times article called her “the nucleus and center of the whole organization of crime in New York City.” Meet 'The Queen of Thieves' Marm Mandelbaum, New York City's First Mob Boss |J. North Conway |September 7, 2014 |DAILY BEAST 

To win you have to start winning and these very attractive candidates could help form a nucleus to rebuild the Party. The GOP’s Long, Hard Road in California |Stuart Stevens |May 8, 2014 |DAILY BEAST 

The Russian people should form the state at the center, “a nucleus around which other peoples are gathered.” Putin’s Dream of Empire Doesn’t Stop at Crimea, Or Even Ukraine |Oleg Shynkarenko |March 23, 2014 |DAILY BEAST 

To me, the nucleus of Peter Parker is him being left behind by his parents. Marc Webb Takes Us Inside ‘The Amazing Spider-Man 2’ and Discusses His Rise to the A-List |Marlow Stern |March 15, 2014 |DAILY BEAST 

Any epithelial cell may be so granular from degenerative changes that the nucleus is obscured. A Manual of Clinical Diagnosis |James Campbell Todd 

Each contains one irregular nucleus or several small, rounded nuclei. A Manual of Clinical Diagnosis |James Campbell Todd 

Occasionally the nucleus is irregular in shape, "clover-leaf" forms being not infrequent. A Manual of Clinical Diagnosis |James Campbell Todd 

Wright's stain gives the nucleus a deep purple color and the cytoplasm a pale robin's-egg blue in typical cells. A Manual of Clinical Diagnosis |James Campbell Todd 

Each contains a single round or oval nucleus, often located eccentrically. A Manual of Clinical Diagnosis |James Campbell Todd