Hold them to keep your hands warm, place them at the bottom of your bag to heat your feet, or stuff them into a chest pocket to warm your core. How to stay warm while sleeping in the frigid outdoors |Alisha McDarris |February 12, 2021 |Popular-Science 

Video showed the two shoving one another and Strickland accused Ujiri of hitting him “in the face and chest with both fists” while ignoring orders to stop trying to get to the floor. Raptors president, sheriff’s deputy drop lawsuits over shoving incident at NBA Finals |Cindy Boren |February 12, 2021 |Washington Post 

If I get too warm, I take it off, pack it into its chest pocket—it squishes down to the size of an apple—and stuff it into my shorts pocket. 5 Pieces of Gear That Help Me Stay Active During Winter |Jakob Schiller |February 11, 2021 |Outside Online 

Every single one of these photos makes the center of my chest clench as I remember how I felt eating these things. Six Months of Soft Food Set Me Free |Meghan McCarron |February 2, 2021 |Eater 

The paper analyzes training loads for leg press, chest press, and pulldowns. The Data Behind a Once-a-Week Strength Routine |Alex Hutchinson |February 2, 2021 |Outside Online 

At St. Barnabas Hospital, Pellerano was listed in stable condition with wounds to his chest and arm. Shot Down During the NYPD Slowdown |Michael Daly |January 7, 2015 |DAILY BEAST 

Forty minutes later he says, ‘I think she may have chest injuries now.’ Harry’s Daddy, and Diana’s ‘Murder’: Royal Rumors In a New Play |Tom Sykes |January 4, 2015 |DAILY BEAST 

Couple guided Stella as she crawled and dipped her chest to pick up each magnet. Dungeons and Genital Clamps: Inside a Legendary BDSM Chateau |Ian Frisch |December 20, 2014 |DAILY BEAST 

I received many bruises on my collarbones, neck, chest, and shoulders. Beaten By His Church for Being Gay |Justin Jones |December 16, 2014 |DAILY BEAST 

Once a month he attaches a device to his chest, clamps metal bracelets on his wrists, and hooks the whole thing up to a telephone. Alfred Hitchcock’s Fade to Black: The Great Director’s Final Days |David Freeman |December 13, 2014 |DAILY BEAST 

He vowed he was taking even better care of himself than usual, but his chest is bad again. Ancestors |Gertrude Atherton 

He continued active till his 35th year, when he began to decline, and died of water in the chest. The Every Day Book of History and Chronology |Joel Munsell 

He held the sabre lower, but the point was kept unwaveringly at the chest of his enemy; his teeth were set. The Pit Town Coronet, Volume I (of 3) |Charles James Wills 

One baas was bigger than the other, and on his chest and on his body were pictures of birds, and beasts, and strange things. Uncanny Tales |Various 

On his chest was a great inkoos with one eye covered, and on his back a hut with trees growing straight up into the air from it. Uncanny Tales |Various