Rereading is like looking at the answer to a puzzle, rather than doing it yourself, he says. Top 10 tips on how to study smarter, not longer |Kathiann Kowalski |September 9, 2020 |Science News For Students 

This cocktail hour interactive exhibition changes each year, but last year it involved attendees competing against one another in a cookie-cutter-themed puzzle challenge designed by artist Zak Kitnick. How nonprofits are catering to millennials and rethinking the charity gala for younger generations |Rachel King |September 7, 2020 |Fortune 

At first, chronicling daily symptoms was like starting a brand-new puzzle. Everything You Need to Know About Period Tracking |Christine Yu |September 6, 2020 |Outside Online 

He’s given us something to contemplate, a puzzle we must return to more than once to begin to apprehend. The ancient palindrome that explains Christopher Nolan’s Tenet |Alissa Wilkinson |September 4, 2020 |Vox 

While that was the intent of the puzzle, it wasn’t explicitly stated. Can You Reach The Beach? |Zach Wissner-Gross |August 7, 2020 |FiveThirtyEight 

For passengers, beyond the statistics lies a puzzle that has persisted for years. Flying Coach Is the New Hell: How Airlines Engineer You Out of Room |Clive Irving |November 25, 2014 |DAILY BEAST 

Working on his own piece of the intelligence puzzle, Stasio racked up impressive victories. How the NSA Became a Killing Machine |Shane Harris |November 9, 2014 |DAILY BEAST 

When he looks at the neatly compiled jigsaw puzzle of his life, however, he feels empty, deeply dissatisfied. Is ‘Satisfaction’ a Love Story That’s Too Real About Sex and Marriage? |David Masciotra |September 19, 2014 |DAILY BEAST 

But their action just proved another confounding piece of this negligent puzzle. Westgate's Chilling Security Video Reveals Shopping Mall Bloodbath |Nina Strochlic |September 15, 2014 |DAILY BEAST 

Without access to the site, reporters, social media, and governments put together pieces of the puzzle. MH17 Is the World’s First Open-Source Air Crash Investigation |Clive Irving |July 22, 2014 |DAILY BEAST 

He was an Englishman and came to the valley by chance and settled here, and to his dying day he was a puzzle to the people. The Soldier of the Valley |Nelson Lloyd 

The chief puzzle of the problem is that nothing turns out as we were told it would turn out. Gallipoli Diary, Volume I |Ian Hamilton 

Something seemed to puzzle him, for he was frowning, but by and by the old cynical smile came back. The Soldier of the Valley |Nelson Lloyd 

It did not puzzle the Colonel at all to know where the sperrit came from, and he did not like the child the less because of it. The Cromptons |Mary J. Holmes 

I can read a chapter to father in the Bible, but the hard names sadly puzzle me. The World Before Them |Susanna Moodie