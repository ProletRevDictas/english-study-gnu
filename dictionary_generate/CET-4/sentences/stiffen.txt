When muscles contract and stiffen up, they can put pressure on nearby nerves. Best massage chair: Take relaxation to a new level right in your living room |Irena Collaku |August 13, 2021 |Popular-Science 

From Charlottesville to Capitol Hill, folks bracing for the worst — workers and residents who lived through violence and bloodshed — stiffened and waited for the metal manifestations of so much American hatred to be removed. Fences aren’t freedom and statues aren’t history |Petula Dvorak |July 12, 2021 |Washington Post 

Our skin sags, our hair goes gray, joints stiffen and creak—all signs that our components—that is, proteins and other biomolecules—aren’t what they used to be. Genetic tricks of the longest-lived animals |Ars Staff |May 31, 2021 |Ars Technica 

GOP-backed bills in various statehouses aim to ban ballot drop boxes, limit voting periods, restrict absentee voting or stiffen requirements for voter identification. More than 100 corporate executives hold call to discuss halting donations and investments to fight controversial voting bills |Todd Frankel |April 11, 2021 |Washington Post 

In December, Congress rewrote aviation safety laws in an effort to stiffen the regulator’s oversight of the company. Airlines ground some 737 Max jets after Boeing discloses electrical problem |Ian Duncan |April 9, 2021 |Washington Post 

The embrace caused Susskind to stiffen up “like an unwilling virgin,” he later said. Nikita Khrushchev, Talk Show Guest |Stephen Battaglio |November 20, 2010 |DAILY BEAST 

She would break down to some degree, and then stiffen back up. The Copycat Killer Mom |Harriet McLeod |August 17, 2010 |DAILY BEAST 

Black Hood took two steps toward her and saw her gun wrist stiffen. Hooded Detective, Volume III No. 2, January, 1942 |Various 

Jessie saw the dark eyes blaze and the stern face of the countess stiffen with fury. The Weight of the Crown |Fred M. White 

Beardsley saw her stiffen; there was a change across her face, a struggle beneath the eyes. We're Friends, Now |Henry Hasse 

From where he sat watching—curious to see what Cash would do—Bud saw him flinch and stiffen as a man does under pain. Cabin Fever |B. M. Bower 

There is also in large bridges wind-bracing to stiffen the structure against horizontal forces. Encyclopaedia Britannica, 11th Edition, Volume 4, Part 3 |Various