Most people believe the idiom “time flies when you’re having fun,” and research has, indeed, shown that when time seems to pass by quickly, people assume the task must have been engaging and enjoyable. Why Vacations Feel Like They’re Over Before They Even Start |LGBTQ-Editor |July 12, 2021 |No Straight News 

The commercial, which advertises the brand’s seltzer lemonade, runs with the “when life gives you lemons” idiom, riffing off 2020 being a “lemon of a year.” The 8 best Super Bowl commercials, from an ‘Edward Scissorhands’ sequel to Michael B. Jordan’s Alexa |Sonia Rao, Maura Judkis |February 8, 2021 |Washington Post 

First of all, remember that idioms or colloquialisms may make sense in one place but not in another, even if the same language is spoken. Six must-know international SEO tips to expand business |Edward Coram James |June 3, 2020 |Search Engine Watch 

Later she observed that one of the most skilled in this idiom was the journalist Dorothy Parker. Tallulah Bankhead: Gay, Drunk and Liberated in an Era of Excess Art |Judith Mackrell |January 25, 2014 |DAILY BEAST 

Are some jobs, to use the standard idiom, “inherently governmental?” Who Should Kill? Looking for Answers in Erik Prince’s Memoir |Brian Castner |November 22, 2013 |DAILY BEAST 

Is ‘idiom’ enough to defend to the modern reader sentences like this? This Week’s Hot Reads: Oct. 15, 2013 |Nicholas Mancusi, Thomas Flynn |October 15, 2013 |DAILY BEAST 

Additionally impressive is that an Australian can write so convincingly in the idiom of a country so different from her own. This Week’s Hot Reads: September 9, 2013 |Nicholas Mancusi |September 9, 2013 |DAILY BEAST 

Yet he seemed interested only in recasting GOP concepts in his own idiom. Obama’s Speech Took Ideas From the GOP and Rhetoric From Madison Avenue |Lee Siegel |January 28, 2012 |DAILY BEAST 

His musical idiom was growing richer, and music had become to him what poetry had been at Votinsk. The Life &amp; Letters of Peter Ilich Tchaikovsky |Modeste Tchaikovsky 

Lange thinks these lines corrupt; but I believe the idiom is correct. Chaucer's Works, Volume 1 (of 7) -- Romaunt of the Rose; Minor Poems |Geoffrey Chaucer 

For the hospitality of England can scarcely be translated with full flavor into any other idiom. McClure's Magazine, Vol. 1, No. 2, July, 1893 |Various 

The occasional use of the imperfect is almost his only Gaelic idiom. Angling Sketches |Andrew Lang 

Accent, idiom, vocabulary give a new turn to the ancient speech. American Sketches |Charles Whibley