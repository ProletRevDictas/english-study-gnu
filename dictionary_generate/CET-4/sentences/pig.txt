The meat companies’ employees, many of them immigrants and refugees, slice pig bellies or cut up chicken carcasses in close quarters. After Hundreds of Meatpacking Workers Died From COVID-19, Congress Wants Answers |by Bernice Yeung and Michael Grabell |February 4, 2021 |ProPublica 

Growing up on a farm in an Amish part of rural Pennsylvania, he woke up before dawn to care for pigs and cows. Who is Intel’s new CEO, Pat Gelsinger |Aaron Pressman |January 13, 2021 |Fortune 

It’s a painting of a warty pig, an animal still found on Sulawesi, that was rendered on the cave’s back wall at least 45,500 years ago, researchers report January 13 in Science Advances. One of the oldest known cave paintings has been found in Indonesia |Bruce Bower |January 13, 2021 |Science News 

I talked to a woman who nursed an injured baby pig back to health. Stories from a Lost Year |Emily VanDerWerff |December 31, 2020 |Vox 

I’m also guessing most people haven’t found themselves sitting in the back seat of a car, in charge of making sure a massive raw pig didn’t roll onto the floor. At the end of an isolating year, even the embarrassing, frustrating, weird parts of family gatherings feel missed |Theresa Vargas |December 26, 2020 |Washington Post 

He has said he believes Al Sharpton is a “race pimp” and a pig. How James Woods Became Obama’s Biggest Twitter Troll |Asawin Suebsaeng |December 31, 2014 |DAILY BEAST 

Elle magazine shot an editorial in September, one picture revealing a teacup pig sitting pretty by a mini Tyler Alexandra bag. Handbags: The More You Pay, The Smaller They Shrink |Elizabeth Landers |December 29, 2014 |DAILY BEAST 

Unfortunately, neither of our teams had pinpointed the pig's burial site. Knowing Where the Bodies Are Buried: An Excerpt From 'Lives in Ruins' |Marilyn Johnson |November 14, 2014 |DAILY BEAST 

Bats that had once lived deep in the forest were now eking out a living on mango trees and near pig farms. Bats’ Link to Ebola Finally Solved |Carrie Arnold |November 12, 2014 |DAILY BEAST 

In 2014, it appears, the key to winning in a swing state is to avoid talking about issues and emphasize pig castration. Tea Party Firebrand Wins Big in Iowa |Ben Jacobs |November 5, 2014 |DAILY BEAST 

Squinty could look out, but the slats were as close together as those in a chicken coop, and the little pig could not get out. Squinty the Comical Pig |Richard Barnum 

"I don't know whether I am going to like this or not--this coming to live in town," thought the little pig. Squinty the Comical Pig |Richard Barnum 

The pig family did not know when Squinty would be taken away from them, and all they could do was to wait. Squinty the Comical Pig |Richard Barnum 

Several times after this the boy and his sisters came to look down into the pig pen. Squinty the Comical Pig |Richard Barnum 

But just wishing never made anyone larger or taller, not even a pig, and Squinty stayed the same size. Squinty the Comical Pig |Richard Barnum