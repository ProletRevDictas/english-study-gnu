He said the proceeds of the sale would benefit the foundation’s philanthropic efforts in the region. Morning Report: Redistricting Issues Could Get Lost in Translation |Voice of San Diego |February 10, 2021 |Voice of San Diego 

Since then, Ford has given away tens of millions of masks and is now turning proceeds from its health-care products toward manufacturing the clear respirators and air filtration kits, Baumbick said. Ford’s next pandemic mission: Clear N95 masks and low-cost air filters |Hannah Denham |February 9, 2021 |Washington Post 

You don’t make money inside of a community and not use some of the proceeds to give back to those in need. Let’s Talk About Kevin |Eugene Robinson |January 23, 2021 |Ozy 

EVgo has raised about $575 million in proceeds through the business combination, including a $400 million in private investment in public equity, or PIPE. EVgo to go public via SPAC in bid to power EV charging expansion |Kirsten Korosec |January 22, 2021 |TechCrunch 

Some 30% to 40% of those proceeds, says CEO Arik Shtilman, will go toward mergers and acquisitions. Why investors are excited to see the Plaid and Visa merger die |Lucinda Shen |January 13, 2021 |Fortune 

What does Bondi mean that clerks now should “determine how to proceed”? The Back Alley, Low Blow-Ridden Fight to Stop Gay Marriage in Florida Is Finally Over |Jay Michaelson |January 5, 2015 |DAILY BEAST 

It was done after we had received a binding legal opinion from Justice and approval from the White House to proceed. CIA Interrogation Chief: ‘Rectal Feeding,’ Broken Limbs Are News to Me |Kimberly Dozier |December 11, 2014 |DAILY BEAST 

So any discussions have to proceed with all three nations on board. Should Obama Take North Korea’s Bait? |Gordon G. Chang |October 21, 2014 |DAILY BEAST 

But there is broad agreement between Washington and Ankara as to how the fight against ISIS should proceed, he said. Exclusive: Turkey OK’s American Drones to Fight ISIS |Eli Lake, Josh Rogin |October 16, 2014 |DAILY BEAST 

They do not proceed from a genuine affective and sexual complementarity. The Vatican's Same-Sex Synod: The Bishops Hear About Reality. Do They Listen? |Barbie Latza Nadeau |October 12, 2014 |DAILY BEAST 

That the inconstancy of such notices, in cases equally important, proves they did not proceed from any such agent. The Life and Most Surprising Adventures of Robinson Crusoe, of York, Mariner (1801) |Daniel Defoe 

Still, monsieur, I am willing to proceed upon the lines which would appear to be more agreeable to yourself. St. Martin's Summer |Rafael Sabatini 

With the announcement of the thirty-six directors, it was possible to proceed to the active opening of the institutions. Readings in Money and Banking |Chester Arthur Phillips 

We now proceed to learn the eighteen kings intermediate between William II. Assimilative Memory |Marcus Dwight Larrowe (AKA Prof. A. Loisette) 

They will proceed, at once, to their offices and lodge their names and serve under their present chiefs. The Philippine Islands |John Foreman