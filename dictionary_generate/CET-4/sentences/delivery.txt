To combat the declines in their traditional businesses, Uber continued its push into consumer delivery, while Lyft announced a push into business-to-business logistics. Will ride-hailing profits ever come? |Alex Wilhelm |February 12, 2021 |TechCrunch 

You pick anything you want — you don’t have to cook it, order it, wait two hours, or pay a delivery fee — you just take a plate and step up. We’re all fantasizing about post-covid dining now: “I just want someone to spill a beer on me” |Emily Heil |February 12, 2021 |Washington Post 

It makes sense as e-commerce and on demand delivery isn’t a flash in the pan fad, per Cavaluzzo. ‘Order booze from your phone’: Why brands like Guinness, Babe Wine see advertising value on Drizly |Kimeko McCoy |February 11, 2021 |Digiday 

Others, such as meal kits and wine delivery, have gotten more traction than usual. Brands court customers with different tactics this Covid-tinged Valentine’s Day |Erika Wheless |February 11, 2021 |Digiday 

Grocery delivery services charge a fee — usually around $10 per $100 spent, on average. Hints From Heloise: Grocery delivery has its pros and cons |Heloise Heloise |February 11, 2021 |Washington Post 

Should capability delivery experience additional changes, this estimate will be revised appropriately. Pentagon Misfires in Stealth Jet Scandal |Dave Majumdar |January 8, 2015 |DAILY BEAST 

It also helps pilots orientate themselves during weapons delivery passes. Newest U.S. Stealth Fighter ‘10 Years Behind’ Older Jets |Dave Majumdar |December 26, 2014 |DAILY BEAST 

And this week it was Mister Ham, General Delivery, United States. The Stacks: Sell the Overcoat, Keep the Dignity |Paul Hemphill |December 22, 2014 |DAILY BEAST 

Fewer women are shackled during labor and delivery (PDF), though this still occurs. The GOP’s Hidden Ban on Prison Abortions |Harold Pollack |December 13, 2014 |DAILY BEAST 

A powdered form of the measles vaccine could make delivery safer and easier around the world. Powdered Measles Vaccine Could Be Huge for Developing World |Kent Sepkowitz |December 2, 2014 |DAILY BEAST 

A delivery of a policy therefore, to an insurance broker, would be a delivery to his principal. Putnam's Handy Law Book for the Layman |Albert Sidney Bolles 

If the offeree sent a telegram, then he would be obliged to prove the delivery of the dispatch. Putnam's Handy Law Book for the Layman |Albert Sidney Bolles 

There must be an actual delivery by him, and though a deed may be completed in every other respect, it is not an effective deed. Putnam's Handy Law Book for the Layman |Albert Sidney Bolles 

Suppose a deed were mailed to the grantee, or handed to another person to deliver to the grantee, this would be a good delivery. Putnam's Handy Law Book for the Layman |Albert Sidney Bolles 

The contract becomes complete when the policy is put in the mail, postage prepaid, for delivery in due course to the insured. Putnam's Handy Law Book for the Layman |Albert Sidney Bolles