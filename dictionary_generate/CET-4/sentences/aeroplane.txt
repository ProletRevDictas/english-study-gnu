The region is located near North America and has seen the disappearance of countless aeroplanes and ships. Bermuda Triangle mystery resolved? |noreply@blogger.com (Unknown) |September 27, 2021 |TechCrunch 

I do not recall what sort of aeroplane Mr. Hughes had at the time; however, it was quite comfortable, as I recall. Alfred Hitchcock’s Fade to Black: The Great Director’s Final Days |David Freeman |December 13, 2014 |DAILY BEAST 

An aeroplane had reported that the Goeben had come into the Narrows, presumably to fire over the Peninsula with her big guns. Gallipoli Diary, Volume I |Ian Hamilton 

Presently the aeroplane came into sight again and was greeted with a sudden roar of cheering. Uncanny Tales |Various 

The aeroplane had been hoisted to its elevated position by means of a stout rope passing through a pulley at the top of the pole. Motor Matt's "Century" Run |Stanley R. Matthews 

So far as can be seen by aeroplane scouting, this ridge is still unoccupied; certainly it is unentrenched. Gallipoli Diary, Volume 2 |Ian Hamilton 

A novelty for a window display is made of a model aeroplane flying by its own power. The Boy Mechanic, Book 2 |Various