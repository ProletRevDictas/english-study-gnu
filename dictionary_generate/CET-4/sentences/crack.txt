It began a crack down on the conspiracy group in July, when it banned thousands of accounts that had been spreading baseless BS which Twitter said had “the potential to lead to offline harm”. Twitter tightens account security for political candidates ahead of US election |Natasha Lomas |September 17, 2020 |TechCrunch 

It also introduced rules meant to crack down on the spread of misinformation through these more private networks. Facebook tries to clean up Groups with new policies |Sarah Perez |September 17, 2020 |TechCrunch 

The separation of phenomena by length, as quantified by the renormalization group, has allowed scientists to move gradually from big to small over the centuries, rather than cracking all scales at once. How Mathematical ‘Hocus-Pocus’ Saved Particle Physics |Charlie Wood |September 17, 2020 |Quanta Magazine 

By midmonth, the state had recorded possibly the hottest temperature ever measured on earth — 130 degrees in Death Valley — and an otherworldly storm of lightning had cracked open the sky. Climate Change Will Force a New American Migration |by Abrahm Lustgarten, photography by Meridith Kohut |September 15, 2020 |ProPublica 

So in order to crack the live-streaming commerce market in the west, he said there had to be a strong, trusted point of view to stand out. ‘Our goal is to become a massive marketplace’: NTWRK is bringing livestream commerce to a younger generation |Kayleigh Barber |September 14, 2020 |Digiday 

We see a system that will indict a 20-year-old for selling crack but not a police officer for choking the life out of a citizen. Bobby Shmurda and Rap’s Ultimate Hoop Dream |Rawiya Kameir |December 23, 2014 |DAILY BEAST 

“The crack baby myth is being recapitulated in terms of NAS,” Sunderlin said. States Slap Pregnant Women With Harsher Jail Sentences |Emily Shire |December 12, 2014 |DAILY BEAST 

The night before he bought a lot of crack-cocaine on credit with no way to pay, intending to kill himself after smoking. A Million Ways to Die in Prison |Daniel Genis |December 8, 2014 |DAILY BEAST 

Of course, nobody could have foreseen that the floor would begin to crack. I Watched a Casino Kill Itself: The Awful Last Nights of Atlantic City’s Taj Mahal |Olivia Nuzzi |December 8, 2014 |DAILY BEAST 

The door opened a crack and for a second I was tempted to give in again. Hell Hath No Fury Like Valerie Trierweiler, the French President’s Ex |Lizzie Crocker |November 28, 2014 |DAILY BEAST 

This was a hard nut to crack, if his past were not to be ruthlessly severed from Angel's by a word. Rosemary in Search of a Father |C. N. Williamson 

There was no fight in his men; they ran like a pack of frightened coyotes at the first crack of a gun. The Courier of the Ozarks |Byron A. Dunn 

Here, said Toby, as the young Jew placed some fragments of food and a bottle upon the table, Success to the crack! Oliver Twist, Vol. II (of 3) |Charles Dickens 

There is always something doing there, and I opened the door a crack to hear what was under discussion. The Soldier of the Valley |Nelson Lloyd 

Should the coating crack at the knee or elbow joints, it is merely necessary to retouch it slightly at those places. The Book of Anecdotes and Budget of Fun; |Various