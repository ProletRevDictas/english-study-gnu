Rather than fretting about infinites, they should have focused on connecting tiny with huge. How Mathematical ‘Hocus-Pocus’ Saved Particle Physics |Charlie Wood |September 17, 2020 |Quanta Magazine 

To their astonishment, they found that the huge difference in the mantled clones was the result of a single, tiny epigenetic change. The Environmental Headache in Your Shampoo - Issue 90: Something Green |Anastasia Bendebury & Michael Shilo DeLay |September 16, 2020 |Nautilus 

The “Top Stories” SERP feature, however, was a huge benefit to using an AMP for any news agency with a website, and it’s easy to understand why. Google ranking factors to change search in 2021: Core Web Vitals, E-A-T, or AMP? |Aleh Barysevich |September 16, 2020 |Search Engine Watch 

First, Apple was supplying a base of huge, though slowly growing profits, and Google and Facebook provided earnings that were both increasingly big, and racing ahead. Will tech stocks stumble or slide? What the fundamentals tell us |Shawn Tully |September 16, 2020 |Fortune 

Being in the Midwest, “there are just a limited number of funds and angel investors, which have huge concentrations on the coast,” says Candice Matthews Brackeen, founder and CEO of Cincinnati-based Lightship Capital. Cincinnati’s Secret Sauce to Help Minority Businesses Succeed |Nick Fouriezos |September 15, 2020 |Ozy 

Music is a huge part of the tone of Black Dynamite overall—going back to the original 2009 movie on which the series is based. ‘Black Dynamite’ Presents Police Brutality: The Musical |Stereo Williams |January 9, 2015 |DAILY BEAST 

And, as Gow adds wryly from his own personal experience, “To a huge extent they achieved that aim very well.” ‘Nazi Cows’ Tried to Kill British Farmer |Tom Sykes |January 6, 2015 |DAILY BEAST 

Last March they gave Airbus a huge piece of new business, ordering 169 A320s and 65 of the slightly larger A321. Annoying Airport Delays Might Prevent You From Becoming the Next AirAsia 8501 |Clive Irving |January 6, 2015 |DAILY BEAST 

In doing so he exposed the failure of other airlines in the region to see the huge pent-up demand for cheap travel. Annoying Airport Delays Might Prevent You From Becoming the Next AirAsia 8501 |Clive Irving |January 6, 2015 |DAILY BEAST 

Beyond the huge American flag that hung over the street, the mile-long mass of cops ended. Funeral Protest Is Too Much for NYPD Union Boss |Michael Daly |January 5, 2015 |DAILY BEAST 

Only the petrol tins they took for water right and left of their pathway up the cliff; huge diamonds in the evening sun. Gallipoli Diary, Volume I |Ian Hamilton 

Two huge steam engines had snorted and puffed for three whole years. The Pit Town Coronet, Volume I (of 3) |Charles James Wills 

Well, the pudding moment arrived, and a huge slice almost obscured from sight the plate before us. The Book of Anecdotes and Budget of Fun; |Various 

Nothing doubtful or "reputed" ever arrived in the huge packing-cases consigned to Walls End Castle. The Pit Town Coronet, Volume I (of 3) |Charles James Wills 

Thus among the huge mass of accumulated commodities the simplest wants would go unsatisfied. The Unsolved Riddle of Social Justice |Stephen Leacock