Uneven pavement played havoc with plated dishes and craft cocktails. Shaw’s HalfSmoke is so cool, it’s hot |Evan Caplan |February 5, 2021 |Washington Blade 

Trudging along empty pavement to the nursing home where she works in Northeast Washington, she struggled to shake the feeling of being alone — and exposed. 900,000 infected. Nearly 15,000 dead. How the coronavirus tore through D.C., Maryland and Virginia. |Rebecca Tan, Antonio Olivo, John D. Harden |February 5, 2021 |Washington Post 

The pavement is solid underfoot, the steps at 77 Mass Ave mere feet away, yet I can’t go inside, so the whole place feels like a dream. First-year fall, off campus |Katie McLean |December 18, 2020 |MIT Technology Review 

Depending on how the surface is sealed, the cleaner could break down the oil in the pavement. Need to remove sap from asphalt? Get out the hand sanitizer. |Jeanne Huber |November 30, 2020 |Washington Post 

As you would expect, it was far superior than the van once we left the pavement. Sprinter vs. Teardrop Trailer: What's Best for Camping? |Will Taylor |November 28, 2020 |Outside Online 

Blood spatters on the pavement seem to confirm that he was moving toward Wilson when the instantly fatal shot was fired. 90 Seconds of Fury in Ferguson Are the Key to Making Peace in America |Michael Daly |November 26, 2014 |DAILY BEAST 

This year, however, Democratic dominance at pounding the pavement might finally be challenged. Fight Over the Ground Game in Iowa Could Swing the Senate |Ben Jacobs |August 13, 2014 |DAILY BEAST 

Democrats have long dominated at pounding the pavement in the Hawkeye State. Fight Over the Ground Game in Iowa Could Swing the Senate |Ben Jacobs |August 13, 2014 |DAILY BEAST 

“For some unknown reason, the vehicle traveled off the pavement at a gradual angle,” the troopers wrote. The Cops Who Found Out the Truth About GM's Deadly Cars—in 2006 |Michael Daly |July 17, 2014 |DAILY BEAST 

Buzzfeed assures me that deadbeats camped out on the pavement in big cities are hipsters. Why Do We Hate Hipsters So F'ing Much? |Ted Gioia |July 13, 2014 |DAILY BEAST 

After that he lit a cigarette and smoked feverishly, walking up and down the pavement. The Joyous Adventures of Aristide Pujol |William J. Locke 

One day, on Ludgate hill, a porter passing him was nearly pushed off the pavement by an unintentional motion of the doctor. The Book of Anecdotes and Budget of Fun; |Various 

There were many knots of men under the broad roof over the pavement, but in spite of the ubiquitous saloon no drunkenness. Ancestors |Gertrude Atherton 

Why must I and my child walk on this hot pavement, while they repose on velvet cushions and revel in all luxury? Madame Roland, Makers of History |John S. C. Abbott 

The pavement is of rough cobble-stones, and swarms of dogs and children crowded the way everywhere. British Highways And Byways From A Motor Car |Thomas D. Murphy