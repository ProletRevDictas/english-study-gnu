The US and the UK have taken different approaches to helping workers through the Covid-19 pandemic. Despite wildly different approaches, the US and UK are seeing identical declines in hours worked |Dan Kopf |September 17, 2020 |Quartz 

From Washington to Berlin, officials are scrambling to find the best ways to help workers. Job markets in the US and Europe are surprisingly similar |Dan Kopf |September 16, 2020 |Quartz 

The pandemic has made Britain’s economy even more British, as workers born in the EU walk away. European workers are fleeing the UK economy during the pandemic |John Detrixhe |September 16, 2020 |Quartz 

That means there are generally enough workers for three to four queens to each lead off a swarm of workers and create new colonies. Quacks and toots help young honeybee queens avoid deadly duels |Sharon Oosthoek |September 14, 2020 |Science News For Students 

Unsurprisingly, workers at TransDigm’s companies often feel expendable. The Big Corporate Rescue and the America That’s Too Small to Save |by Lydia DePillis, Justin Elliott and Paul Kiel |September 12, 2020 |ProPublica 

Farrell issued a ticket to an 18-year-old shipyard worker for speeding and an improper exhaust mechanism, according to the TP. The Louisiana Racists Who Courted Steve Scalise |Jason Berry |January 3, 2015 |DAILY BEAST 

He expected European capitalism to evolve spontaneously into a market socialism of worker-owned cooperatives. American Democracy Under Threat for 250 Years |Jedediah Purdy |December 28, 2014 |DAILY BEAST 

She is incapable of responding to kindness and enquiry, even very gentle flirting on the part of a co-worker. Grief: The Real Monster in The Babadook |Tim Teeman |December 19, 2014 |DAILY BEAST 

Wolf concurs that the conceit of the show seems to have everyone but the sex worker in mind. To Catch a Sex Worker: A&E’s Awful, Exploitative Ambush Show |Samantha Allen |December 19, 2014 |DAILY BEAST 

“We thought it must be the children playing some game,” a worker at the school told Reuters. Taliban: We Slaughtered 100+ Kids Because Their Parents Helped America |Sami Yousafzai |December 16, 2014 |DAILY BEAST 

Each seems satisfied with the way his own branch is getting on: Winter is the quicker worker. Gallipoli Diary, Volume I |Ian Hamilton 

Not more than one adult worker in ten—so at least it might with confidence be estimated—is employed on necessary things. The Unsolved Riddle of Social Justice |Stephen Leacock 

As he was a great worker all his life, “Busy,” or “ Benjamin Ceased” would significantly express his death-date. Assimilative Memory |Marcus Dwight Larrowe (AKA Prof. A. Loisette) 

A strenuous worker, Mr. Johnstone, like most men who have no hobby, did not long survive his retirement from active business life. Fifty Years of Railway Life in England, Scotland and Ireland |Joseph Tatlow 

No true worker, be he digger, or divine, blends real work with either smoking or drinking. Tobacco; Its History, Varieties, Culture, Manufacture and Commerce |E. R. Billings.