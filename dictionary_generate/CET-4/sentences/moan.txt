Indeed, asking him to say who he would like to take part in season 3 merits an anguished moan. Is Glenn Shephard, the ‘Below Deck Sailing Yacht’ Captain, the Nicest Guy on Reality TV? |Tim Teeman |May 14, 2021 |The Daily Beast 

In the privacy of her office, behind a closed door, she allowed herself to sob, letting out a deep moan. The Child Care Industry Was Collapsing. Mrs. Jackie Bet Everything on an Impossible Dream to Save It. |by Lizzie Presser |May 1, 2021 |ProPublica 

Students moan and growl and shriek and yawp, as if exorcising demons in a ritualistic ceremony. How Taryn Toomey’s ‘The Class’ Became New York’s Latest Fitness Craze |Lizzie Crocker |January 9, 2015 |DAILY BEAST 

“I always find it weird when actors moan about things like this,” he says. New ‘Late Late Show’ Host James Corden Would Like to Manage Your Expectations |Kevin Fallon |December 18, 2014 |DAILY BEAST 

As much as customers love to moan about small, uncomfortable seats, the demand for them is higher than ever. Solution to Seat Rage: No More Reclining |Will Doig |September 4, 2014 |DAILY BEAST 

And the best his Republican opponents can do is moan about Benghazi. Carter’s Belated Triumph |Peter Beinart |May 28, 2013 |DAILY BEAST 

But, generally speaking, businesses scream and moan, react and innovate, and wind up in a better place. Don’t Cheap Out, Big Biz! |Daniel Gross |February 13, 2013 |DAILY BEAST 

The gnarled hands shut up into clenched fists, and the feeble voice trailed off in an agonized moan. Raw Gold |Bertrand W. Sinclair 

The old owl no longer hooted, and the water-oaks had ceased to moan as they bent their heads. The Awakening and Selected Short Stories |Kate Chopin 

Then a moan, then a howl and a shriek arose which reached from group to group, from house to house, from square to forest. A Lost Hero |Elizabeth Stuart Phelps Ward and Herbert D. Ward 

With a low moan her head sunk upon the old man's knee, and she shook and trembled with violent emotion. The World Before Them |Susanna Moodie 

"I refuse," she answered, her arms falling, her voice a low moan of the most utter despair. They Looked and Loved |Mrs. Alex McVeigh Miller