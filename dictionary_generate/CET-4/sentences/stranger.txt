Estrada, who previously led legal and policy operations at Bird and government relations at Lyft, is no stranger to playing ball with regulatory agencies. Hear from Lyft, Cruise, Nuro and Aurora about the road ahead for driverless vehicles |Megan Rose Dickey |August 28, 2020 |TechCrunch 

From what career you choose to what sandwich you want for lunch, we care about what our friends, families, and complete strangers think—otherwise, Yelp wouldn’t exist. This Is How Your Brain Responds to Social Influence |Shelly Fan |August 25, 2020 |Singularity Hub 

You brush against strangers in the street and see what reaction your touch evokes. The first murder |Katie McLean |August 19, 2020 |MIT Technology Review 

A total stranger had lost access to his bitcoin private keys—and wanted Stay’s help getting his $300,000 back. This Week’s Awesome Tech Stories From Around the Web (Through August 8) |Singularity Hub Staff |August 8, 2020 |Singularity Hub 

In the first study, singles went on a blind date with a stranger and reported how things went. Your Romantic Ideals Don’t Predict Who Your Future Partner Will Be - Issue 88: Love & Sex |Alice Fleerackers |August 5, 2020 |Nautilus 

When I first arrived at Duke, hooking up with a stranger seemed like a way to shed my inhibitions. Random Hook-Ups or Dry Spells: Why Millennials Flunk College Dating |Ellie Schaack |January 1, 2015 |DAILY BEAST 

And his pitiless beliefs would be no stranger to the political discourse of today. How Dickens and Scrooge Saved Christmas |Clive Irving |December 22, 2014 |DAILY BEAST 

The rate of partner violence dwarfs the number  of women who experience sexual assault from a stranger (7%). The Hidden Link Between Women and War |Leith Greenslade |December 3, 2014 |DAILY BEAST 

“The social convention of not talking to a stranger was fairly rigid at the time,” Weber told me. The Secret World of Pickup Artist Julien Blanc |Brandy Zadrozny |December 1, 2014 |DAILY BEAST 

As Europe closes its shores to immigrants and refugees, the pope asks for welcome of the stranger fleeing war. Pope Bids Refugees to EU ‘Bienvenido’; Europe Says ‘Non’ |Candida Moss |November 30, 2014 |DAILY BEAST 

Before Ripperda could unclasp his lips to reply, the stranger had opened the door, and passed through it like a gliding shadow. The Pastor's Fire-side Vol. 3 of 4 |Jane Porter 

None other would dare to show herself unveiled to a stranger, and a white man at that. The Red Year |Louis Tracy 

Weimar being such a "kleines Nest (little nest)," as Liszt calls it, every stranger is immediately remarked. Music-Study in Germany |Amy Fay 

The stranger was approaching the front entrance, Hedges was wheeling off to the back; but the former turned and spoke. Elster's Folly |Mrs. Henry Wood 

"I read a notice of his marriage in the public papers," continued the stranger, whose eyes were fixed on Hedges. Elster's Folly |Mrs. Henry Wood