Plus, Chick-fil-A is testing a new sandwich, and more news to start your dayThe Travis Scott Meal has been a godsend for McDonald’s, which is probably relying on its popularity to distract from the other issues the company is dealing with right now. McDonald’s Teaches Employees Travis Scott Catchphrases Because of TikTok Trend |Jaya Saxena |September 17, 2020 |Eater 

Sitting in the gorgeously appointed and wonderfully shaded patio, there was plenty of room to socially distance, and the ideal spot to taste mid-Atlantic staples like their soft shell sandwich. We owe it to places like the Tabard Inn |Brock Thompson |September 11, 2020 |Washington Blade 

Have a slice of pizza or homemade sandwich at Lakeshore Convenience, 655 Blue Star Highway. Saugatuck-Douglas is open and better than ever |Bill Malcolm |August 15, 2020 |Washington Blade 

Mikko treated me to a salmon and poached egg sandwich followed by pancakes with a raspberry sauce. You know what Dupont needed? A Nordic restaurant |Brock Thompson |August 12, 2020 |Washington Blade 

Early in a trip, treat yourself with real food, like a ham sandwich, an avocado, or an apple. How to Plan a Successful Backpacking Trip in 7 Steps |Andrew Skurka |July 8, 2020 |Outside Online 

Myers had been out on bail in a gun case, but his family claimed he was unarmed and holding only a sandwich in his hand. The 14 Teens Killed by Cops Since Michael Brown |Nina Strochlic |November 25, 2014 |DAILY BEAST 

While the President chomped on his tuna fish sandwich, the Blackhawk pilot explained the details of his crash. I Shot Bin Laden |Elliot Ackerman |November 16, 2014 |DAILY BEAST 

The President continued to chomp on his sandwich, and now I was sweating. I Shot Bin Laden |Elliot Ackerman |November 16, 2014 |DAILY BEAST 

But merit aside, you can indict a ham sandwich if it's Republican in the most liberal hotbed of Texas: Travis County. Peak 'Oops': Explaining the Perry Indictment |Mark McKinnon |August 17, 2014 |DAILY BEAST 

The problem for Perry is that a Travis County jury can also find a Republican ham sandwich guilty. Peak 'Oops': Explaining the Perry Indictment |Mark McKinnon |August 17, 2014 |DAILY BEAST 

Edward Montague, earl of Sandwich, drowned in the confusion of the battle of Southwold bay. The Every Day Book of History and Chronology |Joel Munsell 

"But—but—oh, tell me," and then he became silent and looked away, raising the sandwich to his mouth mechanically. The Homesteader |Oscar Micheaux 

Louis of France, at the invitation of the rebel English barons, crossed the channel with 680 sail, and landed at Sandwich. The Every Day Book of History and Chronology |Joel Munsell 

A Sandwich Islander appreciates this when he salutes a British crew in terms compounded of oaths and ribaldry. A Cursory History of Swearing |Julian Sharman 

So having sent for my wife, she and I to my Lady Sandwich, and after a short visit away home. Diary of Samuel Pepys, Complete |Samuel Pepys