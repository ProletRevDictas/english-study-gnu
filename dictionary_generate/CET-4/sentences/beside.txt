Selma becomes a biopic in which the hero shines while those who worked beside him are overlooked or relegated to the sidelines. Dr. King Goes to Hollywood: The Flawed History of ‘Selma’ |Gary May |January 2, 2015 |DAILY BEAST 

Dehydrated and feeling weary, Marino lay down beside another migrant under a tree and fell asleep. Drug Smuggler Sues U.S. Over Dog Bite |Caitlin Dickson |December 10, 2014 |DAILY BEAST 

But the flaws and peccadilloes of Renaissance artists like Michelangelo pale beside the misdeeds of patrons and pontiffs. Great Renaissance Art Thrived Amid Filth |Nick Romeo |December 3, 2014 |DAILY BEAST 

The artist came down and stood beside his patron to assess things. Great Renaissance Art Thrived Amid Filth |Nick Romeo |December 3, 2014 |DAILY BEAST 

So they laid her on a bier, and all seven of them sat down beside it and wept and wept for three whole days. In New Brothers Grimm 'Snow White', The Prince Doesn't Save Her |The Brothers Grimm |November 30, 2014 |DAILY BEAST 

Beside her was a box of bonbons, which she held out at intervals to Madame Ratignolle. The Awakening and Selected Short Stories |Kate Chopin 

There were other children beside, and two nurse-maids followed, looking disagreeable and resigned. The Awakening and Selected Short Stories |Kate Chopin 

That they may know thee, as we also have known thee, that there is no God beside thee, O Lord. The Bible, Douay-Rheims Version |Various 

None of the other scholars were asked, and when I entered the room there were only three persons in it beside Liszt. Music-Study in Germany |Amy Fay 

Just as it disappeared from view he caught a glimpse of a charming little girl, peeping out of a latticed window beside the door. Davy and The Goblin |Charles E. Carryl