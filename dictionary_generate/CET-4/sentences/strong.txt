Hoppe had planned to enroll at San Diego State, but the pull of Europe became too strong. Matthew Hoppe was a little-known American soccer player — until he reached the Bundesliga |Steven Goff |February 11, 2021 |Washington Post 

It stands to reason, then, that stronger, more connected and more trusting communities would have more success weathering the pandemic. Researchers identify social factors inoculating some communities against coronavirus |Christopher Ingraham |February 11, 2021 |Washington Post 

“We are still very far from a strong labor market whose benefits are broadly shared,” Powell said. Fed chair: Unemployment rate was closer to 10 percent, not 6.3 percent, in January |Rachel Siegel |February 10, 2021 |Washington Post 

Direct-drive models usually cost less, but chippers with a clutch-drive often can handle tougher waste thanks to their stronger engines. Wood chippers to keep your property looking great |PopSci Commerce Team |February 9, 2021 |Popular-Science 

Each death is a tragedy that breaks our hearts and demands strong, urgent action. Rep. Ron Wright has died after battle with covid-19 |Paulina Firozi, David Weigel |February 8, 2021 |Washington Post 

Despite the strong language, however, the neither the JPO nor Lockheed could dispute a single fact in either Daily Beast report. Pentagon Misfires in Stealth Jet Scandal |Dave Majumdar |January 8, 2015 |DAILY BEAST 

She fills her characters up—strong women beating back against a sexist system—with so much heart. Oscars 2015: The Daily Beast’s Picks, From Scarlett Johansson to ‘Boyhood’ |Marlow Stern |January 6, 2015 |DAILY BEAST 

In a romantic relationship, facing humiliation or awkwardness is a strong possibility. Random Hook-Ups or Dry Spells: Why Millennials Flunk College Dating |Ellie Schaack |January 1, 2015 |DAILY BEAST 

The strong ties he would cultivate with America were first instilled by his American mother. The Real-Life ‘Downton’ Millionairesses Who Changed Britain |Tim Teeman |December 31, 2014 |DAILY BEAST 

The area is 98 percent white, and the Klan has a strong foothold even to this very day. What Would Happen if I Got in White Cop’s Face? |Goldie Taylor |December 30, 2014 |DAILY BEAST 

She skilfully manages the side-lights, and by this means produces strong effects. Women in the fine arts, from the Seventh Century B.C. to the Twentieth Century A.D. |Clara Erskine Clement 

The garrison of the town and fortress was nearly three thousand strong. Blackwood's Edinburgh Magazine, No. CCCXXXIX. January, 1844. Vol. LV. |Various 

The well-known "cock and bull" stories of small children are inspired by this love of strong effect. Children's Ways |James Sully 

Those in whom the impulse is strong and dominant are perhaps those who in later years make the good society actors. Children's Ways |James Sully 

In Luke it is said, “And the child grew, and waxed strong in spirit, filled with wisdom.” Solomon and Solomonic Literature |Moncure Daniel Conway