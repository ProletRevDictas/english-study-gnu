The Democrats have also tried to steer that discontent in their direction — especially Vermont Senator Bernie Sanders, a long-time registered Independent, who was for a time a leading candidate for the Democratic presidential nomination. Does Anyone Really Know What Socialism Is? (Ep. 408 Rebroadcast) |Stephen J. Dubner |September 17, 2020 |Freakonomics 

The announcement that Fraser would indeed attain the long-elusive corner office of a big Wall Street bank was widely celebrated—and kicked off widespread speculation about the most likely next candidates to follow her lead. ‘It has to have an impact’: What Citi’s new CEO means for other women on Wall Street |Maria Aspan |September 16, 2020 |Fortune 

Even that proposal was one that Biden, as a candidate who doesn’t hold elective office, obviously had no power to actually implement. Trump’s incoherent defense of his coronavirus response |Aaron Blake |September 16, 2020 |Washington Post 

Biden holds a similar 22-point advantage on which candidate is more honest and trustworthy, 56 percent to 34 percent. Post-ABC poll and others suggest Minnesota has shifted since 2016, but by how much? |Scott Clement, Dan Balz |September 16, 2020 |Washington Post 

Sinopharm has tested both of these vaccines in the UAE, but it is unclear if the UAE granted emergency approval to one or both candidates. China’s controversial emergency-use program for COVID vaccines is going global |Grady McGregor |September 16, 2020 |Fortune 

Already, 10 Republicans have declared they will vote for an alternative candidate and more seemed poised to join. Kamikaze Congress Prepares to Strike Boehner |Ben Jacobs |January 6, 2015 |DAILY BEAST 

First, they allow Paul to siphon off attention from whichever potential candidate is making news. Rand Paul’s Passive-Aggressive Trolling Campaign |Olivia Nuzzi |January 6, 2015 |DAILY BEAST 

A Republican candidate hoping to win red state support could find a worse team to root for than one from Dallas. Will Chris Christie Regret His Cowboy Hug? |Matt Lewis |January 5, 2015 |DAILY BEAST 

Having regional appeal is one thing; simply being a regional candidate is another. Can Huckabee Convert the GOP’s Moneymen? |Lloyd Green |January 4, 2015 |DAILY BEAST 

But he should not be judged by his wavering as a presidential candidate. Mario Cuomo, a Frustrating Hero to Democrats, Is Dead at 82 |Eleanor Clift |January 2, 2015 |DAILY BEAST 

The Colonel left, and in a few days the election coming off, each candidate made his appearance at the critical German polls. The Book of Anecdotes and Budget of Fun; |Various 

In admitting a member, if no form of election has been prescribed, each candidate must be elected separately. Putnam's Handy Law Book for the Layman |Albert Sidney Bolles 

This was the point of compass revealed by the astrologer as most favourable to the young candidate for manly honours. Our Little Korean Cousin |H. Lee M. Pike 

The Liberal candidate wanted to address the colliers in one of the Lanarkshire towns; but his meeting was very poorly attended. Punch, or the London Charivari, Vol. 109, July 27, 1895 |Various 

Though he frankly acknowledged that he preferred the Electoral Prince to any other candidate, he professed. The History of England from the Accession of James II. |Thomas Babington Macaulay