I have no worries about slinging it around camp or strapping it to the roof rack if space is tight in the car. The Matador SEG42 Offers Unrivaled Gear Organization |Graham Averill |August 25, 2020 |Outside Online 

It helps that EU lawmakers have already agreed to a giant stimulus spending package for the trading bloc, so that worry is off the table. Investors continue to push global stocks into record territory |Bernhard Warner |August 24, 2020 |Fortune 

McMansions are back, in a big way, given the trend toward remote work — and thus the need for office space at home, and no worries about long commutes. Sunday Magazine: A World in Need |Daniel Malloy |August 16, 2020 |Ozy 

The worry for influencers is whether their audiences would follow them over to Triller if they switched. WTF is Triller? |Seb Joseph |August 14, 2020 |Digiday 

Then I could cheerfully traipse from my backyard to a neighbor’s driveway and then on to a dark corner booth somewhere with no worries. Every Decision Is A Risk. Every Risk Is A Decision. |Maggie Koerth (maggie.koerth-baker@fivethirtyeight.com) |July 21, 2020 |FiveThirtyEight 

Still, I worry that a simple traffic stop could have tragic consequences. What Would Happen if I Got in White Cop’s Face? |Goldie Taylor |December 30, 2014 |DAILY BEAST 

And the authorities also worry that the December fires are just the beginning. Italy’s Terror on the Tracks |Barbie Latza Nadeau |December 28, 2014 |DAILY BEAST 

But in the days ahead he, his brother, and the others will be back in the street while their families worry at home. Choking Back Tears, Thousands of Cops Honor Fallen Officer Ramos |Michael Daly |December 28, 2014 |DAILY BEAST 

I wish this was the last time I had to worry about hunger and bombs. Has the Kurdish Victory at Sinjar Turned the Tide of ISIS War? |Niqash |December 27, 2014 |DAILY BEAST 

This is a well-documented phenomenon which does not worry specialists. Uh Oh: Ebola Vaccine Trials Stop |Leigh Cowart |December 19, 2014 |DAILY BEAST 

In sheer nervousness, Hilda also dropped to her knees on the hearthrug, and began to worry the fire with the poker. Hilda Lessways |Arnold Bennett 

His only worry at the time lay in the dark sky above and the blue-white stabs of lightning that promised an electrical storm. Hooded Detective, Volume III No. 2, January, 1942 |Various 

No, there was nothing to worry about as long as that relentless hunter of criminals known as the Black Hood kept off their tail. Hooded Detective, Volume III No. 2, January, 1942 |Various 

I should worry if Burd has a dozen maiden aunts,” observed Amy scornfully, “and they all knitted him red wristlets! The Campfire Girls of Roselawn |Margaret Penrose 

Matt began to appreciate the difficulties ahead of him and to worry a little about the outcome. Motor Matt's "Century" Run |Stanley R. Matthews