Inspectors, who are registered nurses, review a nursing home’s compliance history, observe operations, interview residents and staff and examine medical records. Confirmed Nursing Home Complaints Plummet During Pandemic |Jared Whitlock |August 25, 2020 |Voice of San Diego 

She told investigators she overheard a nurse say she’d been quarantined because of her home ZIP code. Federal Investigation Finds Hospital Violated Patients’ Rights by Profiling, Separating Native Mothers and Newborns |by Bryant Furlow, New Mexico In Depth |August 22, 2020 |ProPublica 

If the researchers spot unusual or worrying patterns, the patients are invited to speak with a nurse. Machines can spot mental health issues—if you hand over your personal data |Bobbie Johnson |August 13, 2020 |MIT Technology Review 

Perhaps they are college students on spring break, or hospital nurses, or people who touch their face all the time. Population immunity is slowing down the pandemic in parts of the US |David Rotman |August 11, 2020 |MIT Technology Review 

According to reports, doctors, nurses, and teachers will be given the shot first when enough supplies of the vaccine are ready in October, and it could reach the general public by January. Russia says it has a covid vaccine called “Sputnik-V” |Antonio Regalado |August 11, 2020 |MIT Technology Review 

But Olds did more than build Nurse-Family Partnership; he did the rigorous evaluation to prove it would work. Can the U.S. Government Go Moneyball? |Peter Orszag, Jim Nussle |December 23, 2014 |DAILY BEAST 

Consider Nurse-Family Partnership, one of the best examples of evidence in action. Can the U.S. Government Go Moneyball? |Peter Orszag, Jim Nussle |December 23, 2014 |DAILY BEAST 

At the hospital, I was told to wait, and was given some tea by a nurse. I Was Gang Raped at a UVA Frat 30 Years Ago, and No One Did Anything |Liz Seccuro |December 16, 2014 |DAILY BEAST 

I learn by the third day to tell the nurse privately to make mine mostly orange juice. Alfred Hitchcock’s Fade to Black: The Great Director’s Final Days |David Freeman |December 13, 2014 |DAILY BEAST 

The substitute nurse says to him in a stage whisper, “You know, the doctor says no vodka.” Alfred Hitchcock’s Fade to Black: The Great Director’s Final Days |David Freeman |December 13, 2014 |DAILY BEAST 

There were other children beside, and two nurse-maids followed, looking disagreeable and resigned. The Awakening and Selected Short Stories |Kate Chopin 

And sure enough when Sunday came, and the pencil was restored to him, he promptly showed nurse his picture. Children's Ways |James Sully 

Jean clung to his English nurse, who played the fascinating game of pretending to eat his hand. The Joyous Adventures of Aristide Pujol |William J. Locke 

A small boy of three years and nine months on receiving from his nurse the familiar order, "Come here!" Children's Ways |James Sully 

Here he can inspect what he sees, say the reflection of the face of his mother or nurse, and compare it at once with the original. Children's Ways |James Sully