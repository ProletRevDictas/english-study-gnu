Thrilling enables small mom-and-pop shops to sell online, reach new customers and geographies, and generate critical new sources of revenue, especially important during a pandemic. The CEO striving to make vintage, secondhand clothing as popular as fast fashion |Rachel King |September 6, 2020 |Fortune 

For many investors, the coronavirus has effectively taken geography out of the equation when it comes to vetting new opportunities. 3 views on the future of geographic-focused funds |Natasha Mascarenhas |September 4, 2020 |TechCrunch 

They care because this issue confronts all families, but they understand the burden is exacerbated by issues of race, income, or geography. Millennials and Gen Zers need better childcare. Can either Biden or Trump deliver? |matthewheimer |August 29, 2020 |Fortune 

At the same time, people are spending more time communicating online, where geography is practically irrelevant. The pandemic is changing the geography of LinkedIn connections |Dan Kopf |August 27, 2020 |Quartz 

Between March 1 and August 1, 2020, virtual schooling saw a 238% spike in searches, many of which were specific to geography, with state or school districts comprising most virtual school searches. Back to school looks very different this year |Christi Olson |August 26, 2020 |Search Engine Land 

If Huckabee runs, the hurdles he faced the last time out, namely geography and money, would still be there. Can Huckabee Convert the GOP’s Moneymen? |Lloyd Green |January 4, 2015 |DAILY BEAST 

These trends will affect everything from geography to culture and politics. Trustafarians Want to Tell You How to Live |Joel Kotkin |October 31, 2014 |DAILY BEAST 

Much of the appeal of the Bay Area is a result of happy coincidence of history and geography. Battle of the Upstarts: Houston vs. San Francisco Bay |Joel Kotkin |October 5, 2014 |DAILY BEAST 

Still, for some, ideology can trump everything — even local geography. A GOP Senate? Why It Won't Come Easy |Lloyd Green |September 1, 2014 |DAILY BEAST 

Even in the smallest of Indian locales, the divisions of geography and nationalism are played out on a micro scale. The Real India Revealed |Rafia Zakaria |August 6, 2014 |DAILY BEAST 

The Italian trip was discussed, and considerable ignorance of geography was, as is usual, manifested by all present. The Pit Town Coronet, Volume I (of 3) |Charles James Wills 

At this spot, according to our works on geography, the Atlantic Ocean changes its name and assumes that of the Pacific. A Woman's Journey Round the World |Ida Pfeiffer 

In this way, though at first his efforts will be very awkward, he will soon come to know the general geography of the heavens. Outlines of the Earth's History |Nathaniel Southgate Shaler 

The instruction ranges from history and geography to practical seamanship, with all the intermediate scientific subjects. The Philippine Islands |John Foreman 

His writings are very numerous, and highly valuable for the purposes of navigation and geography. The Every Day Book of History and Chronology |Joel Munsell