Placed in drinking water, fluoride can serve people who otherwise have poor access to dental care. Anti-Fluoriders Are The OG Anti-Vaxxers |Michael Schulson |July 27, 2016 |DAILY BEAST 

If someone wants to ensure a direct and secure connection, no entity, whether a hotel or otherwise, should be able to block it. How ‘Ethical’ Hotel Chain Marriott Gouges Guests in the Name of Wi-Fi Security |Kyle Chayka |December 31, 2014 |DAILY BEAST 

Larson, as usual, instills gravitas and agency in an otherwise underwritten character. Brie Larson’s Hollywood Transformation |Marlow Stern |December 29, 2014 |DAILY BEAST 

Millennials—rich or otherwise—have been notoriously uninterested in politics. When Will We See a #Millennial Congress? |Linda Killian |December 26, 2014 |DAILY BEAST 

Otherwise, we will be but celebrating an empty holiday, missing its true meaning altogether. Jesus Wasn’t Born Rich. Think About It. |Gene Robinson |December 25, 2014 |DAILY BEAST 

The Majesty on high has a colony and a people on earth, which otherwise is under the supremacy of the Evil One. Solomon and Solomonic Literature |Moncure Daniel Conway 

That is the only point in which one sees Liszt's sense of his own greatness; otherwise his manner is remarkably unassuming. Music-Study in Germany |Amy Fay 

Rather late that evening we administered extreme unction to him, for otherwise he was sufficiently prepared for it. The Jesuit Relations and Allied Documents, Vol. II: Acadia, 1612-1614 |Various 

It rather annoyed her than otherwise, but her husband was pleased, and that was enough for Georgie. The Pit Town Coronet, Volume I (of 3) |Charles James Wills 

Otherwise, a child's box of tin soldiers sent by post would have been just the thing for the Dardanelles landing! Gallipoli Diary, Volume I |Ian Hamilton