Some said the structure resembled the lair of a Hollywood supervillain—or a Death Star from another galaxy. Apple’s ‘most ambitious’ new store is a departure from its signature design |claychandler |September 8, 2020 |Fortune 

It contains genetically-engineered DNA molecules called plasmids that resemble those of the coronavirus. More than manufacturing: India’s homegrown COVID vaccines could transform its pharma industry |Naomi Xu Elegant |September 6, 2020 |Fortune 

The device revealed on Friday resembled a small keychain affixed to an oversized penny. Elon Musk shows off Neuralink brain implant technology in a living pig |jonathanvanian2015 |August 29, 2020 |Fortune 

If the professional behaviors of police officers resemble at all the professional behaviors of doctors, then the answer is no. Why some senior officers are making it harder for police departments to fight racism |matthewheimer |August 26, 2020 |Fortune 

Unlike American company Moderna’s vaccine, which prompts an immune response using Covid-19 messenger RNA, the Oxford vaccine is made from a virus genetically engineered to resemble coronavirus. An Indian Company Is Gearing Up to Make Millions of Doses of a $3 Covid-19 Vaccine |Vanessa Bates Ramirez |August 12, 2020 |Singularity Hub 

A tense commute to work in Houston will start to resemble a tense commute in Boston or New York City. Will Texas Stay Texan? |David Fontana |December 29, 2014 |DAILY BEAST 

What it did not resemble was any other bookstore in the nation. The Bookstore That Bewitched Mick Jagger, John Lennon, and Greta Garbo |Felice Picano |December 16, 2014 |DAILY BEAST 

Black Alice and Strix have origin stories that more closely resemble the archetypal comic heroes. Gail Simone’s Bisexual Catman and the ‘Secret Six’ |Rich Goldstein |December 6, 2014 |DAILY BEAST 

And this is where the plague outbreak does resemble Ebola—as a grim reminder of the consequences of our global interconnectedness. Bubonic Plague Is Back (but It Never Really Left) |Kent Sepkowitz |November 27, 2014 |DAILY BEAST 

After a few hundred years, these voices start to resemble doomsday cultists—the end is often heralded but never delivered. Why Are Millennials Unfriending Organized Religion? |Vlad Chituc |November 9, 2014 |DAILY BEAST 

The changes in hemoglobin and red cells resemble those of a moderate symptomatic anemia, with rather low color-index. A Manual of Clinical Diagnosis |James Campbell Todd 

The ova closely resemble those of Tnia saginata, but are a little smaller (Fig. 101). A Manual of Clinical Diagnosis |James Campbell Todd 

Others again put the varnish on and rub it off in places to resemble the wear of age. Violins and Violin Makers |Joseph Pearce 

Improved stove pipings are now being manufactured in the States which in appearance exactly resemble cast-iron. Asbestos |Robert H. Jones 

These resemble corporations in some ways, and this is the reason for calling them quasi corporations. Putnam's Handy Law Book for the Layman |Albert Sidney Bolles