That’s not yet anywhere near the last peak of 6,500 — but it’s a significant rise from 200 in early July. The new Covid-19 case surge in Europe, explained |Julia Belluz |September 17, 2020 |Vox 

This research took place before the pandemic and the rise in distance learning. Healthy screen time is one challenge of distance learning |Kathryn Hulick |September 11, 2020 |Science News For Students 

“We just had a record quarter across almost every important metric in our business” thanks to the rise in digitization, he says. Why PayPal’s Dan Schulman gave workers pay increases, without the market requiring it |Alan Murray |September 9, 2020 |Fortune 

With in-person learning at school still curtailed in many areas, it makes sense that this would factor into an even greater rise in part-time work, simply out of necessity. The Easy Part Of The Economic Recovery Might Be Over |Amelia Thomson-DeVeaux |September 4, 2020 |FiveThirtyEight 

Hurricanes and sea level rise inundate their coastal communities. India’s megacities aren’t prepared for a wave of climate migrants |Manavi Kapur |September 3, 2020 |Quartz 

Being something of a political cipher may have helped Revels rise to prominence. The Black Man Who Replaced Jefferson Davis in the Senate |Philip Dray |January 7, 2015 |DAILY BEAST 

We tend to think not, but the rise of King, Kennedy, and Lincoln was unlikely, too. No Gods, No Cops, No Masters |James Poulos |January 1, 2015 |DAILY BEAST 

The rapid rise of the sharing economy is changing the way people around the world commute, shop, vacation, and borrow. Why Do ‘Progressives’ Want to Ban Uber and AirBnB? |Adam Thierer, Christopher Koopman |December 30, 2014 |DAILY BEAST 

Most importantly, they were all deleted long before that percentage could rise any higher. The Attack on the Hidden Internet |Marc Rogers |December 29, 2014 |DAILY BEAST 

He was also swept about in the music of D.C., a scene which gave rise to such acts as Fugazi and Thievery Corporation. DJ Spooky Wants You To Question Everything You Know About Music, Technology, and Philosophy |Oliver Jones |December 27, 2014 |DAILY BEAST 

See how those distant peaks rise serenely over the southern horizon! Blackwood's Edinburgh Magazine, Volume 60, No. 372, October 1846 |Various 

But he was ignorant of that part of the horrid tale; and the Duke, in a milder voice, bade him rise. The Pastor's Fire-side Vol. 3 of 4 |Jane Porter 

That bunch of cottonwoods with the new-made grave close by the dead horses seemed to rise up between us, and I became speechless. Raw Gold |Bertrand W. Sinclair 

The overture is over, the curtain is about to rise on the drama of Georgie's married life. The Pit Town Coronet, Volume I (of 3) |Charles James Wills 

It had its counterpart on the political side in the rise of representative democratic government. The Unsolved Riddle of Social Justice |Stephen Leacock