Other vaccines will almost certainly come to the market that only require one shot — Johnson & Johnson’s one-dose vaccine is already working through the last stage of federal approval. Good news about America’s Covid-19 vaccine campaign |German Lopez |February 12, 2021 |Vox 

After a broader play about the group of enslaved people, who became known as the GU272, fell through, Short-Colomb said, she and Lab officials went to the university administration and got approval for her project. An innovative Georgetown lab looks to theater to quell political fires |Peter Marks |February 12, 2021 |Washington Post 

City News Service reports that any recommendations proposed by the South County Environmental Justice Task Force to clean up the Tijuana River Valley would first need approval by the supervisors. Morning Report: Vacancy Tax Déjà Vu |Voice of San Diego |February 11, 2021 |Voice of San Diego 

No one can remove someone’s liver without approval for medical purposes. New technology can get inside your head. Are you ready? |Laura Sanders |February 11, 2021 |Science News For Students 

Much like a liver can’t be taken out of a body without approval for medical purposes, neural data shouldn’t be removed either. Can privacy coexist with technology that reads and changes brain activity? |Laura Sanders |February 11, 2021 |Science News 

“The process of getting the approval is too slow and is too cumbersome,” Rogers said. ISIS Fight Has a Spy Shortage, Intel Chair Says |Kimberly Dozier |January 2, 2015 |DAILY BEAST 

They would have to get court approval to re-home their children. Judge: Rehoming Kids Is Trafficking |Tina Traster |December 30, 2014 |DAILY BEAST 

It was done after we had received a binding legal opinion from Justice and approval from the White House to proceed. CIA Interrogation Chief: ‘Rectal Feeding,’ Broken Limbs Are News to Me |Kimberly Dozier |December 11, 2014 |DAILY BEAST 

Vreeland believes that in the end, his grandmother put her subtle seal of approval on his lifestyle. From Fashion Player to Photographer Monk |Nina Strochlic |December 3, 2014 |DAILY BEAST 

He tried to contact a Health Republic doctor to overrule the nurse and get approval for payment. My Insurance Company Killed Me, Despite Obamacare |Malcolm MacDougall |November 24, 2014 |DAILY BEAST 

At the store he would never have given in, but he was not accustomed to hearing so loud a murmur of approval greet the opposition. The Soldier of the Valley |Nelson Lloyd 

She pressed her hands tighter upon her bosom; her eyes sparkled with an odd approval of that brisk deed. St. Martin's Summer |Rafael Sabatini 

I shall be glad if your Majesty is satisfied and pleased with it, as in all I desire to win your approval. The Philippine Islands, 1493-1898, Volume XX, 1621-1624 |Various 

The socialist reads such criticism as the above with impatient approval. The Unsolved Riddle of Social Justice |Stephen Leacock 

On September 17, the famous "Suffolk Resolves" were laid before the deputies for their approval. The Eve of the Revolution |Carl Becker