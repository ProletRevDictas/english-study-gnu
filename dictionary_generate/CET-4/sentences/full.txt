To put it rather uncharitably, the USPHS practiced a major dental experiment on a city full of unconsenting subjects. Anti-Fluoriders Are The OG Anti-Vaxxers |Michael Schulson |July 27, 2016 |DAILY BEAST 

Could the (thus far) timid trembling give way to a full-on, grand mal seizure? 26 Earthquakes Later, Fracking’s Smoking Gun Is in Texas |James Joiner |January 7, 2015 |DAILY BEAST 

So we know that boring down to the bedrock and pumping it full of fluid can cause earthquakes. 26 Earthquakes Later, Fracking’s Smoking Gun Is in Texas |James Joiner |January 7, 2015 |DAILY BEAST 

It used to carry livestock but sailed its final voyage with a hold full of Syrian men, women, and children. Ghost Ships of the Mediterranean |Barbie Latza Nadeau |January 6, 2015 |DAILY BEAST 

Whether he gets his full due in popular culture remains to be seen. Ed Brooke: The Senate's Civil Rights Pioneer and Prophet of a Post-Racial America |John Avlon |January 4, 2015 |DAILY BEAST 

It ended on a complaint that she was 'tired rather and spending my time at full length on a deck-chair in the garden.' The Wave |Algernon Blackwood 

As long as may be necessary, Sam,” replied Mr. Pickwick, “you have my full permission to remain. The Posthumous Papers of the Pickwick Club, v. 2(of 2) |Charles Dickens 

It is full of poetic feeling, and the flesh tints are unusually natural. Women in the fine arts, from the Seventh Century B.C. to the Twentieth Century A.D. |Clara Erskine Clement 

A small book, bound in full purple calf, lay half hidden in a nest of fine tissue paper on the dressing-table. Hilda Lessways |Arnold Bennett 

One would not have wanted her white neck a mite less full or her beautiful arms more slender. The Awakening and Selected Short Stories |Kate Chopin