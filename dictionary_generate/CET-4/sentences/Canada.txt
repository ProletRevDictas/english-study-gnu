By the end of 2020, Canada bought up 338 million doses, enough to inoculate their population four times over. Global inequity in COVID-19 vaccination is more than a moral problem |Jonathan Lambert |February 26, 2021 |Science News 

Her last competition of any sort was a junior event in Canada in March 2020, so this marks the longest stretch she has gone without competing since she was 4. For competition-starved U.S. gymnasts, Winter Cup will measure Olympic readiness |Liz Clarke |February 26, 2021 |Washington Post 

Crickets, which are high in protein and other vital nutrients, were already being farmed successfully in Canada for both human and animal consumption. They're Healthy. They're Sustainable. So Why Don't Humans Eat More Bugs? |Aryn Baker |February 26, 2021 |Time 

“When I watch that last bit of the interview, in light of what we now know, I would love to take those words back,” Silver told Canada’s Sportsnet, in comments published Thursday. Adam Silver says he regrets comments about Finals incident involving Raptors’ Masai Ujiri |Des Bieler |February 26, 2021 |Washington Post 

To this day I make sure there are a couple of icy cold cans of Diet Coke in the fridge — even if I’m coming home from Canada in December. Homecomings used to feel special. But that was before we spent all our time at home. |Liz Langley |February 25, 2021 |Washington Post 

Yes, we do typically do better than Europe (and Canada, too, which is frequently awful on this score). How the PC Police Threaten Free Speech |Nick Gillespie |January 9, 2015 |DAILY BEAST 

The same picture emerges from middle class men in the U.S., Canada, and the Nordic countries. How Good Dads Can Change the World |Gary Barker, PhD, Michael Kaufman |January 6, 2015 |DAILY BEAST 

In fact, Mexico buys and sells more US goods than any other country on the planet except for Canada. Why Mexicans Are Enraged by Obama’s Big Tuesday Meeting |Ruben Navarrette Jr. |January 6, 2015 |DAILY BEAST 

That gays (and other liberals) should choose Canadian oil because Canada “has no laws prohibiting LGBT lifestyle.” How Canadian Oilmen Pinkwash the Keystone Pipeline |Jay Michaelson |December 28, 2014 |DAILY BEAST 

As southern California turns to desert western Canada could follow Oregon and Washington states as a contender in top class wines. Beer Countries vs. Wine Countries |Clive Irving |December 7, 2014 |DAILY BEAST 

Our talk ranged from the Panhandle to the Canada line, while our horses jogged steadily southward. Raw Gold |Bertrand W. Sinclair 

The conclusion is reached that, despite these drawbacks, the Jesuit mission in Canada has made a hopeful beginning. The Jesuit Relations and Allied Documents, Vol. II: Acadia, 1612-1614 |Various 

From Canada on the north, to Texas on the south, the hot winds had laid the land seemingly bare. The Homesteader |Oscar Micheaux 

This particular kind, I am told, is at the present time only to be found in Canada and some parts of the States. Asbestos |Robert H. Jones 

Metal buttons or pistons located on the toe piece of the pedal-board were introduced by the ingenious Casavant of Canada. The Recent Revolution in Organ Building |George Laing Miller