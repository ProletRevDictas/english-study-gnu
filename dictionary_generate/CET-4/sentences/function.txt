Not every website has one, and even the ones that do have very surface-level functions. How to drive digital innovation necessary during the pandemic |Nick Chasinov |September 16, 2020 |Search Engine Watch 

In part, this was a function of some remaining uncertainty about how the virus spread most effectively. Parsing Trump’s baffling, head-slapping comments on mask-wearing |Philip Bump |September 16, 2020 |Washington Post 

Excipients are critical materials and serve a broad variety of functions. The ‘inactive' ingredients in your pills could harm you |By Yelena Ionova/The Conversation |September 15, 2020 |Popular-Science 

For example, shipping, and social listening sound like siloed functions, but actually they’re closely related. Deep Dive: How the Summer of 2020 forced brand marketing to change for the better |jim cooper |September 14, 2020 |Digiday 

That’s because daily exercise not only helps kids stay physically and emotionally healthy, it also boosts cognitive function. A 15-Week Exercise Plan for Kids and Families |Krista Langlois |September 12, 2020 |Outside Online 

But the copper performs another important function: working as a catalyst in the distillation process. When It Comes to Great Whisky, The Size of Your Still Matters | |December 9, 2014 |DAILY BEAST 

The iPad was an even bigger hit, especially as it had a new function that allowed him to play the drawing back. The Many Lives of Artist David Hockney |William O’Connor |November 23, 2014 |DAILY BEAST 

Openness might be a function of sexuality and gender, as well. Coming Out Kinky to Your Doctor, in Black and Blue |Heather Boerner |October 25, 2014 |DAILY BEAST 

Entitled “Please Go Home,” the parody stars Daniel Franzese, best known as the “too gay to function” Damian from Mean Girls. Anime Hologram Pop Stars, Return of ‘Fresh Prince’ Carlton, and More Viral Videos |Alex Chancey |October 12, 2014 |DAILY BEAST 

Sex is a basic human function; a physiological drive we cannot ignore. Christian Right-Wingers Love Porn: New Studies Suggest the Bible Belt Has A Kinky Side |Aurora Snow |October 11, 2014 |DAILY BEAST 

To prevent intruders or extruders from withdrawing his mind from the text, he exercises the Inhibitory function of the Attention. Assimilative Memory |Marcus Dwight Larrowe (AKA Prof. A. Loisette) 

Little girls perhaps represent the attractive function of adornment: they like to be thought pretty. Children's Ways |James Sully 

This is hardly a function—parties even in the big political country-houses are more or less informal. Ancestors |Gertrude Atherton 

No definite proof of this position has, however, as yet been adduced, and the function of the compound is entirely unknown. Elements of Agricultural Chemistry |Thomas Anderson 

After the formal proclamation was issued the function terminated with a banquet given to 200 insurgent notabilities. The Philippine Islands |John Foreman