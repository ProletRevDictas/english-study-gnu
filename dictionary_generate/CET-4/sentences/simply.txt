When a user receives a shared routine, they simply click the URL while on the mobile device where they have the Alexa app installed. Amazon makes Alexa Routines shareable |Sarah Perez |September 17, 2020 |TechCrunch 

We’ll have to see if he can rise even higher as the season goes on, or if he simply regresses back to his reduced form of the previous few seasons. Aaron Rodgers Is Playing Like Aaron Rodgers Again |Neil Paine (neil.paine@fivethirtyeight.com) |September 16, 2020 |FiveThirtyEight 

In all of these cases, each individual doesn’t have to decide whether they will cooperate or defect, they simply have to play their part in the greater whole. The Dark Side of Smart - Facts So Romantic |Diana Fleischman |September 15, 2020 |Nautilus 

The term may sound old to some, but today, grandparents are vibrant, active, and they simply aren’t monolithic. Debbie Allen’s Grandmother Love Doubled |Joi-Marie McKenzie |September 11, 2020 |Essence.com 

Some of the updates simply put to writing how Apple’s rules apply to technologies it’s introducing with its new mobile operating system, iOS 14, due out later this fall. Apple revises App Store rules to permit game streaming apps, clarify in-app purchases and more |Sarah Perez |September 11, 2020 |TechCrunch 

Carla points out how meaningful it can be to have people in your life who simply understand what you're going through. Everyone at This Dinner Party Has Lost Someone |Samantha Levine |January 6, 2015 |DAILY BEAST 

This simply is not an option for ACC to source indeterminately. Exclusive: U.S. Drone Fleet at ‘Breaking Point,’ Air Force Says |Dave Majumdar |January 5, 2015 |DAILY BEAST 

Conway goes on to list a series of other coincidences that he suggests are not simply explained. Harry’s Daddy, and Diana’s ‘Murder’: Royal Rumors In a New Play |Tom Sykes |January 4, 2015 |DAILY BEAST 

Having regional appeal is one thing; simply being a regional candidate is another. Can Huckabee Convert the GOP’s Moneymen? |Lloyd Green |January 4, 2015 |DAILY BEAST 

But this year, instead of simply voting against Boehner on Tuesday, at least two members of the group are vying to replace him. The YOLO Caucus' New Cry for Attention |Ben Jacobs |January 4, 2015 |DAILY BEAST 

He is simply hearing every tone, knowing exactly what effect he wishes to produce and how to do it. Music-Study in Germany |Amy Fay 

Of Liszt the first part of this is not true, for if he strikes a wrong note it is simply because he chooses to be careless. Music-Study in Germany |Amy Fay 

And though I never suspected it at the time, I have no doubt that he pocketed the money and simply destroyed the letters. The Boarded-Up House |Augusta Huiell Seaman 

Sherwood often wavers between him and Kullak, and Deppe would like to teach Sherwood if he could, simply out of interest for him. Music-Study in Germany |Amy Fay 

The morning we started was one of those perfect autumnal days when it is a delight simply to live. Music-Study in Germany |Amy Fay