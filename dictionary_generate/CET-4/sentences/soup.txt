We have been lied to about what is and is not healthy, and there’s no hard evidence that putting a can of mushroom soup in your casserole every once in a while is going to result in serious physical harm. The Joy of Cooking Other People’s ‘Secret Family Recipes’ |Amy McCarthy |September 11, 2020 |Eater 

The pandemic in many ways has already showcased the strength of the sector, with consumers stockpiling candy and chocolate along with their canned soup and peanut butter. How scary will a COVID-19 Halloween be for candy companies? |Beth Kowitt |August 29, 2020 |Fortune 

There’s no telling whether Davis, like Rapoport, will make regular appearances in test kitchen videos, but if she does, fans can expect to see her cooking up soup and fish, her two favorite types of dishes, according to her CNN interview. Bon Appétit Announces Dawn Davis, Publishing Heavyweight, as New Editor-in-Chief |Elazar Sontag |August 27, 2020 |Eater 

Microorganisms chemically modify the metal, setting it free from the surrounding rock and allowing it to dissolve in a microbial soup, from which the metal can be isolated and purified. We’re Using Microbes to Clean Up Toxic Electronic Waste. Here’s How |Sebastien Farnaud |August 20, 2020 |Singularity Hub 

If we had switched gravity off at the beginning of the universe, it would still be a big, boiling hot soup of fundamental particles. The Physicist Who Slayed Gravity’s Ghosts |Thomas Lewton |August 18, 2020 |Quanta Magazine 

There was also the grapefruit diet, the cabbage soup diet, and the cookie diet. Why Your New Year’s Diet Will Fail |Carrie Arnold |December 30, 2014 |DAILY BEAST 

By nightfall, I had showered, eaten some soup that a friend brought me, and I slept in my room for 12 solid hours. I Was Gang Raped at a UVA Frat 30 Years Ago, and No One Did Anything |Liz Seccuro |December 16, 2014 |DAILY BEAST 

Once inside, he removes a cup of broccoli-cheddar soup from his bag and goes to heat it up. The Revival of Kieran Culkin: A Reluctant Star Seizes the Spotlight |Marlow Stern |October 23, 2014 |DAILY BEAST 

Tear Soup: A Recipe for Healing after Lossby Pat Schwiebert. Book Bag: Reading Your Way Out Of Grief |Anna Whiston-Donaldson |October 16, 2014 |DAILY BEAST 

The shards of one can be used to cut someone (never mind the constant availability of soup-can tops). Prisoners Get Cultural Fix with 8-Tracks and Bootleg Cassettes |Daniel Genis |August 18, 2014 |DAILY BEAST 

She had forced herself to eat most of her soup, and now she was picking the flaky bits of a court bouillon with her fork. The Awakening and Selected Short Stories |Kate Chopin 

"There were a good many," replied Edna, who was eating her soup with evident satisfaction. The Awakening and Selected Short Stories |Kate Chopin 

Mr. Pontellier, who was observant about such things, noticed it, as he served the soup and handed it to the boy in waiting. The Awakening and Selected Short Stories |Kate Chopin 

Had put on her Sunday gown, and had nothing to do now but hold up her head high, and sup her soup out of a silver spoon. The World Before Them |Susanna Moodie 

Meanwhile, the judicious establishment of free soup kitchens in the streets alleviated the necessities of the mob. Napoleon's Marshals |R. P. Dunn-Pattison