The team reasoned this urge might stem from natural instincts to hunt, or from a need for cats to supplement their diet. Meatier meals and more playtime might reduce cats’ toll on wildlife |Jonathan Lambert |February 11, 2021 |Science News 

For so long, I thought, “I want a seat at the table,” but now I have this urge to knock over the table. This Revolutionary Chef Wants to Topple the Table |Shaan Merchant |January 30, 2021 |Ozy 

The downside is you won’t be able to look at an untended tree again without seeing a wayward branch and feeling the urge to fix it. It’s tree-pruning season. Here’s how to do it without resorting to ‘crape murder.’ |Adrian Higgins |January 27, 2021 |Washington Post 

You have to control the urge to panic and maintain some sense of inner peace. How to survive three days in the wild |By Keith McCafferty/Field & Stream |January 26, 2021 |Popular-Science 

He felt an urge to return to the District for the first time after watching the inauguration on television Wednesday night. The fortress around downtown D.C. is being dismantled. But heightened security may remain. |Emily Davies, Michael Brice-Saddler, Peter Hermann |January 21, 2021 |Washington Post 

I need to resist my urge to talk them into my truth, just so I can feel more comfortable and secure. In 2015, Let’s Try for More Compassion |Gene Robinson |January 4, 2015 |DAILY BEAST 

For instance, how do you balance honesty with any protective urge? Daphne Merkin on Lena Dunham, Book Criticism, and Self-Examination |Mindy Farabee |December 26, 2014 |DAILY BEAST 

And not that anyone would know I have a self-protective urge, but I do have one. Daphne Merkin on Lena Dunham, Book Criticism, and Self-Examination |Mindy Farabee |December 26, 2014 |DAILY BEAST 

On the other hand, I have an equally strong urge to tell secrets. Daphne Merkin on Lena Dunham, Book Criticism, and Self-Examination |Mindy Farabee |December 26, 2014 |DAILY BEAST 

But not even the threat of death can suppress the urge to live vicariously through Jack Dawson and James Bond. North Korea’s Secret Movie Bootleggers: How Western Films Make It Into the Hermit Kingdom |Lizzie Crocker |December 22, 2014 |DAILY BEAST 

And I have given this tedious detail to urge and embolden others to remonstrate against it. Glances at Europe |Horace Greeley 

In excusing the Freedom of your Satyr, you urge that it galls no body, because nobody minds it enough to be mended by it. A Letter from Mr. Cibber to Mr. Pope |Colley Cibber 

But this is quite enough to justify the inconsiderable expense which the experiment I urge would involve. Glances at Europe |Horace Greeley 

The judicious father saw that it would be utterly unavailing to urge the suit, and the matter was dropped. Madame Roland, Makers of History |John S. C. Abbott 

At first the girl declined to follow advice which instinctively she distrusted, and Sir Lucien was too clever to urge it upon her. Dope |Sax Rohmer