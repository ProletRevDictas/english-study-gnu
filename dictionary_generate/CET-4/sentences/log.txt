These logs don’t contain the content the spyware extracted, like chats or emails—NSO insists it never sees specific intelligence—but do include metadata such as a list of all the phones the spyware tried to infect and their locations at the time. Inside NSO, Israel’s billion-dollar spyware giant |Tate Ryan-Mosley |August 19, 2020 |MIT Technology Review 

If accounts conflict, NSO can demand logs that reveal targets. The man who built a spyware empire says it’s time to come out of the shadows |Bobbie Johnson |August 19, 2020 |MIT Technology Review 

Another reason to use a log scale is that it allows scientists to show data easily. Explainer: What are logarithms and exponents? |Bethany Brookshire |August 12, 2020 |Science News For Students 

Filebeat will be needed to interpret your logs before you send them to Elasticsearch. How SEOs can create a free server log dashboard to better understand incoming traffic to your website |Jean-Christophe Chouinard |August 3, 2020 |Search Engine Land 

This approach harnesses the power of Elasticsearch to help create powerful visualizations using your log files. How SEOs can create a free server log dashboard to better understand incoming traffic to your website |Jean-Christophe Chouinard |August 3, 2020 |Search Engine Land 

Turn off the TV, put down your phone, and log off the computer. Why Your New Year’s Diet Will Fail |Carrie Arnold |December 30, 2014 |DAILY BEAST 

The young man weaves through clusters of bamboo and cuts a diagonal slash into a tree, positioning a hollow log at the end. The Congo's Forgotten Colonial Getaway |Nina Strochlic |December 18, 2014 |DAILY BEAST 

Official Donetsk Republic business was log-jammed because the high command had only one stamp for documents and identity papers. East Ukraine: Back in the USSR |Jamie Dettmer |November 19, 2014 |DAILY BEAST 

One morning a few years ago, the editor left his apartment to find an ax stuck into a log on his doorstep. The Kremlin Is Killing Echo of Moscow, Russia’s Last Independent Radio Station |Anna Nemtsova |November 7, 2014 |DAILY BEAST 

The Spires gather data every time somebody uses them; they log each “product.” Font of Invention | |September 18, 2014 |DAILY BEAST 

You see, I stuck to him like a log to a root, but for the first week or so 'twant no use—not a bit. The Book of Anecdotes and Budget of Fun; |Various 

Neither of us spoke again, and at length the squat log buildings of Pend d' Oreille loomed ahead of us in the night. Raw Gold |Bertrand W. Sinclair 

“There are no sahib-log in the town,” he said, for Malcolm deemed it advisable to begin by a question on that score. The Red Year |Louis Tracy 

In the centre was a log-house, larger and more pretentious than many log-houses which he had seen in the South. The Cromptons |Mary J. Holmes 

Instead, he came to me and lifted to my knee one of those ponderous feet of his, and tried to pull me from my log. The Soldier of the Valley |Nelson Lloyd