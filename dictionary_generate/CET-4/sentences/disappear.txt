Students leave Sullivan for forced marriages, or their families move, or they simply disappear from the roster. At a Chicago high school, helping refugee students navigate American life |Martha Toll |August 27, 2021 |Washington Post 

During that time, a flood of reports from female Afghan journalists began to emerge, in which they detailed their experiences watching their freedom disappear overnight and relinquished to Taliban militants and newly imposed Sharia law. ‘Last Chance to Leave’: The Fight to Get One Woman Out of Afghanistan |Diana Falzone |August 26, 2021 |The Daily Beast 

If MJ, for instance, forgets his secret identity, their bond will disappear. Every Single Detail of the Spider-Man: No Way Home Trailer, Explained |Eliana Dockterman |August 24, 2021 |Time 

Once that’s done, you’ll see a small thumbnail down in the lower left-hand corner—tap the Edit button next to the thumbnail before it disappears to start tweaking and annotating your picture. How to edit screenshots on any device |David Nield |August 24, 2021 |Popular-Science 

We know that people are disappearing—not to be heard from again. Operation Afghanistan Rescue |aheard |August 21, 2021 |Outside Online 

It is wild that something that would seem to be so scandalous would just disappear from the press. Inside the Lifetime Whitney Houston Movie’s Lesbian Lover Storyline |Kevin Fallon |December 16, 2014 |DAILY BEAST 

Ultimately, all it took was the mere mention of a lawyer for the perpetrator to delete the accounts and disappear completely. A Female Writer’s New Milestone: Her First Death Threat |Annie Gaus |December 1, 2014 |DAILY BEAST 

The CAP paper estimates that if current trends continue unabated, overtime pay will disappear entirely by 2026. It’s Always Black Friday for Clerks |Michael Tomasky |November 28, 2014 |DAILY BEAST 

The brash, engaged, occasionally self-centered ex-lawmaker seemed to retreat inward and practically disappear. Hagel Takes a Bullet for Obama: Inside the Defense Secretary’s Sudden Firing |Shane Harris, Tim Mak |November 24, 2014 |DAILY BEAST 

Because this food source could abruptly disappear at any time, cutworm moths cannot be counted on to replace pine nuts. What It Takes to Kill a Grizzly Bear |Doug Peacock |November 23, 2014 |DAILY BEAST 

The color does not disappear upon boiling, which excludes diacetic acid. A Manual of Clinical Diagnosis |James Campbell Todd 

The hills disappear some miles above this city, and henceforward to the sea all is flat and tame as a marsh. Glances at Europe |Horace Greeley 

Quinin causes them rapidly to disappear from the peripheral blood, and few or none may be found after its administration. A Manual of Clinical Diagnosis |James Campbell Todd 

As I receded, the watery veil would disappear, and as I approached it would again take form. Music-Study in Germany |Amy Fay 

Sometimes a trenchful of the enemy would fire a volley and half of them disappear through gullies leading to other cover. The Philippine Islands |John Foreman