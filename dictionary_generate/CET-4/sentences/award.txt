That might be just what the 72-year-old award show needs to stave off irrelevance. The virtual Emmys could give a much-needed jolt to the fading awards show |Adam Epstein |September 17, 2020 |Quartz 

In recent years, you have publicly criticized the industry, especially in relation to the awards and rankings that have come to dominate it. Christian Puglisi Is Closing His Influential Copenhagen Restaurants. COVID Is Only Partly to Blame |Rafael Tonon |September 17, 2020 |Eater 

Weber regularly contributes to major newspapers and magazines, such as National Geographic, GEO, and Die Zeit, and has won a number of awards for his writing. What the Meadow Teaches Us - Issue 90: Something Green |Andreas Weber |September 16, 2020 |Nautilus 

What is clear, however, is that when the foundation does grant awards again, the process will likely be quite different. The Mess That Is the 2020 James Beard Awards, Explained |Elazar Sontag |September 11, 2020 |Eater 

Her total numbers probably don’t support winning the award, but other than Dangerfield, it’s hard to point to a rookie who has had a greater impact on a playoff-bound team. Which Players Have A Shot At Becoming WNBA Rookie Of The Year? |Howard Megdal |September 11, 2020 |FiveThirtyEight 

But I had won the British Award, Best Foreign Actor, so I went. The Story Behind Lee Marvin’s Liberty Valance Smile |Robert Ward |January 3, 2015 |DAILY BEAST 

From Ann Coulter on Ebola to evangelicals on climate change, 2014 was full of award-worthy science denialism. 2014: Revenge of the Creationists |Karl W. Giberson |December 27, 2014 |DAILY BEAST 

The award surprised Lewis, but it also struck the right note. A West Point MVP Who Never Played a Down |Nicolaus Mills |December 13, 2014 |DAILY BEAST 

His photography has won more than a hundred awards, including the prestigious Alfred Eisenstaedt Award for Magazine Photography. The Best Coffee Table Books of 2014 |Robert Birnbaum |December 13, 2014 |DAILY BEAST 

Near the end of my time with Hitchcock, the American Film Institute is preparing to honor him with their Life Achievement Award. Alfred Hitchcock’s Fade to Black: The Great Director’s Final Days |David Freeman |December 13, 2014 |DAILY BEAST 

I issued my award, with the usual result that while each party was fairly well pleased neither was altogether satisfied. Fifty Years of Railway Life in England, Scotland and Ireland |Joseph Tatlow 

These men composed a self-constituted tribunal to award life or instant death to those brought before them. Madame Roland, Makers of History |John S. C. Abbott 

Thus, even the discovery which made chemistry a science, has attached to it in their award this feeble appendage. Decline of Science in England |Charles Babbage 

The world is usually just, and it will ultimately award the tokens of its approbation to those who deserve success. Thoughts on Educational Topics and Institutions |George S. Boutwell 

To the party present in the afternoon the magistrate shall award the suit. The Two Great Republics: Rome and the United States |James Hamilton Lewis