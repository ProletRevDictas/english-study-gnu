Kids that are growing up today, they just aren’t tolerating anything and I do love that. Late-night TV sensation Amber Ruffin and her sister co-wrote a book about racism. And, yes, it’s hilarious. |Hau Chu |February 8, 2021 |Washington Post 

He then said he could not use it because, as he has gotten older, his stomach cannot tolerate hot, spicy food. Miss Manners: She’s ‘best’ at giving gifts but stinks at receiving them |Judith Martin, Nicholas Martin, Jacobina Martin |February 4, 2021 |Washington Post 

We must affirm that this type of behavior is not now, will not be tomorrow and will never be tolerated in the United States Congress. House to vote on removing GOP’s Marjorie Taylor Greene from her committees |Felicia Sonmez, John Wagner, Colby Itkowitz |February 4, 2021 |Washington Post 

For years, defense officials have said that they do not tolerate extremism in the ranks — a point that senior defense officials repeated Tuesday. 12 members of the National Guard removed from inauguration duty |Dan Lamothe, Alex Horton, Paul Sonne |January 19, 2021 |Washington Post 

However, we needed to fly over people to get feedback about how people would tolerate the minimized boom. The NASA Engineer Who’s a Mathematician at Heart |Susan D'Agostino |January 19, 2021 |Quanta Magazine 

“The US cannot tolerate the idea of any rival economic entity,” Stone writes. Oliver Stone’s Latest Dictator Suckup |James Kirchick |January 5, 2015 |DAILY BEAST 

He also wrote, “Torture is not a thing that we can tolerate.” The Luxury Homes That Torture and Your Tax Dollars Built |Michael Daly |December 12, 2014 |DAILY BEAST 

Revisions went back and forth for weeks before Caro finally signed off on versions he could tolerate. ‘The Power Broker’ Turns 40: How Robert Caro Wrote a Masterpiece |Scott Porch |September 16, 2014 |DAILY BEAST 

Should she leave her husband and endure loneliness or tolerate his dalliance and keep a companion for old age? Ian McEwan's New Novel Keeps Life at Arm's Length |Nick Romeo |September 11, 2014 |DAILY BEAST 

Why tolerate toxicity in a powerful sphere of modern life that has the potential to—and does—benefit so many? Zelda Williams Is the Latest to Leave Twitter Because of Ugly Attacks |Tauriq Moosa |August 14, 2014 |DAILY BEAST 

I cannot believe that a good God would create or tolerate a Devil, nor that he would allow the Devil to tempt man. God and my Neighbour |Robert Blatchford 

Her left knee was supported on pillows, and the bed-clothes were raised away from it, for it could tolerate no weight whatever. Hilda Lessways |Arnold Bennett 

But if the "great public" will only tolerate one as a pupil long enough, eventually, one must succeed. Music-Study in Germany |Amy Fay 

An ear accustomed to the fine tone of a good violin will not now tolerate a bad piano-forte. Violins and Violin Makers |Joseph Pearce 

But I would tolerate, welcome, indeed, plead for a stiff protective duty upon foreign goods. Third class in Indian railways |Mahatma Gandhi