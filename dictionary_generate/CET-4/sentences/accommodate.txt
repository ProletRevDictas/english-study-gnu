As the pandemic has worn on, many publishers looked for other ways to win client budgets while accommodating this short-term thinking. ‘Nothing quite like being forced’: Publishers whip up quicker, cheaper ad products for advertisers |Max Willens |August 27, 2020 |Digiday 

Thompson had said the deal would save money over the long term, but also suggested that the property would need $15 million to potentially accommodate another 245 employees and make other capital improvements, including asbestos remediation. The Deal Before the 101 Ash St. Debacle Helps Explain How We Got Here |Lisa Halverstadt and Jesse Marx |August 24, 2020 |Voice of San Diego 

One carrier, though, said that while water and treats and cold beverages are appreciated, that the best thing a customer can do is provide a bigger mailbox—one that can be reached from the truck and accommodate all mail and parcels. U.S. Postal Service carriers reveal how you can make their day better |Jeff |August 18, 2020 |Fortune 

Jones said he feels bad he’s not going to be able to accommodate those parents. Rural Districts Still Lack Devices, Internet Access as School Year Draws Near |Kayla Jimenez |August 11, 2020 |Voice of San Diego 

Rapid urbanisation and an increasing population compel India to accommodate more vehicles on the roads, which indirectly means more crude oil imports and carbon emissions. How an Indian flight could run on biofuel—but India cannot |Monika Mandal |August 11, 2020 |Quartz 

Oxygen levels will be decreased to accommodate fewer people. The Sistine Chapel Gets Mood Lighting |Barbie Latza Nadeau |November 2, 2014 |DAILY BEAST 

It will also take into account outside temperatures and even accommodate for people with fevers. The Sistine Chapel Gets Mood Lighting |Barbie Latza Nadeau |November 2, 2014 |DAILY BEAST 

“You can host a sit-down diner for 140, and the house can accommodate 700 people at a party,” Davenport says proudly. 'Lord Fraud' Gets Out of Jail, Back Into Orgies |Tom Sykes |August 26, 2014 |DAILY BEAST 

If a fan has a Spanish or Japanese accent, George will switch languages to accommodate them. Why We're Obsessed With George Takei |Jennifer M. Kroot |August 20, 2014 |DAILY BEAST 

She's happy to accommodate vegetarians, vegans, and those intolerant of gluten. The Ultimate Southern Cheeseburger Created in South Carolina |Jane & Michael Stern |August 10, 2014 |DAILY BEAST 

If the paper is about twelve by eighteen inches this will accommodate moderate examples of most of the fronds. How to Know the Ferns |S. Leonard Bastin 

The principal room or "hall" will accommodate about 1,000 persons, the remaining portion of the premises being let off in offices. Showell's Dictionary of Birmingham |Thomas T. Harman and Walter Showell 

Youre a-going to accommodate us, and wots to prevent my standing treat for a pint or so, in return? Oliver Twist, Vol. II (of 3) |Charles Dickens 

I eventually came to one of the largest, where by considerable shifting they managed to accommodate my car. British Highways And Byways From A Motor Car |Thomas D. Murphy 

The round table, if large enough to accommodate many guests, has too large a diameter each way for easy conversation. The Ladies' Book of Etiquette, and Manual of Politeness |Florence Hartley