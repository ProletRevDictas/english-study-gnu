He was confident without being cocky, combative only at moments of righteous indignation, safe and steady as a senatorial policymaker, reliable as a pilot. Another look at John Glenn through a heroic lens |Francis French |November 20, 2020 |Washington Post 

We could see a light but steady northeasterly breeze build during the predawn hours. D.C.-area forecast: Mainly clear skies plus warmer temperatures today and tomorrow |A. Camden Walker |November 20, 2020 |Washington Post 

The Seattle Seahawks and their quarterback, Russell Wilson, steadied themselves and moved into first place in the NFC West. Kyler Murray runs out of miracles as Seahawks hang on to take over first place in NFC West |Mark Maske |November 20, 2020 |Washington Post 

Much like the contrast between Paul and Westbrook, Holiday’s game is far steadier than Bledsoe’s. The Upsides And Downsides Of The NBA’s Five Biggest Trades So Far |Chris Herring (chris.herring@fivethirtyeight.com) |November 19, 2020 |FiveThirtyEight 

Seoul’s two three-star restaurants Gaon and La Yeon maintain their status for the fifth year running, while the city’s two-star restaurant count holds steady at seven. Michelin Announces 2021 Stars for Seoul |Monica Burton |November 19, 2020 |Eater 

Slow at first, then steadily, a stream of liquid drips off the incision. The Congo's Forgotten Colonial Getaway |Nina Strochlic |December 18, 2014 |DAILY BEAST 

And ultimately this creates steadily eroding trust among voters for not just politics but the institutions of government. Just What We Needed: More Campaign Spending |Mark McKinnon |December 8, 2014 |DAILY BEAST 

It was reaffirmed in 2012, and popular support has been steadily building an ever—healthier majority for marriage equality. America’s First Post-Gay Governor |David Freedlander |October 24, 2014 |DAILY BEAST 

Jeep steadily gave up a market it had created to rivals, particularly Toyota and Range Rover. Nationalism on Four Wheels |Clive Irving |October 18, 2014 |DAILY BEAST 

Incumbent Democrat Kay Hagan is steadily but narrowly leading GOP challenger Thom Tillis. Who Are the Judicial Activists Now? |Michael Tomasky |October 7, 2014 |DAILY BEAST 

She kept her eyes fixed steadily on his, saying what followed gently, calmly, yet as though another woman spoke the words. The Wave |Algernon Blackwood 

Our talk ranged from the Panhandle to the Canada line, while our horses jogged steadily southward. Raw Gold |Bertrand W. Sinclair 

The result of the restoration of trade, banking, and credit to earlier and more normal conditions has been steadily apparent. Readings in Money and Banking |Chester Arthur Phillips 

I should not be surprised if he returned to business in a day or two, and settled steadily to work for the remainder of his life. Blackwood's Edinburgh Magazine, No. CCCXXXIX. January, 1844. Vol. LV. |Various 

But he was quite right in carping at her, for fortune, like other fickle jades, is more likely to be true if steadily abused. The Pit Town Coronet, Volume I (of 3) |Charles James Wills