When Mulan returns to the imperial court, the emperor offers her prizes and promotions, but Mulan asks only for a steed — in some translations a horse, in others a donkey — so that she can go back home to her family. The history of Mulan, from a 6th-century ballad to the live-action Disney movie |Constance Grady |September 4, 2020 |Vox 

Nevertheless, this complicated shape allowed the three researchers to access the rich theory of translation surfaces. Mathematicians Report New Discovery About the Dodecahedron |Erica Klarreich |August 31, 2020 |Quanta Magazine 

“The conditions are being formed in the country in which a tech business cannot function,” the letter reads, according to a translation from Radio Free Europe. Belarus’s crackdown could squash Minsk’s budding tech sector |Nicolás Rivero |August 20, 2020 |Quartz 

At face value, it might look like its services are used to improve translation quality, but in reality they are also used to build other products, including products connected to national security work. How China surveils the world |Tate Ryan-Mosley |August 19, 2020 |MIT Technology Review 

For instance, it has a strategic agreement with Alibaba Cloud to embed its translation services in the company’s technology. How China surveils the world |Tate Ryan-Mosley |August 19, 2020 |MIT Technology Review 

One is, of course, inevitably up against the problem of translation when speaking of names. Does Pope Francis Believe Christians and Muslims Worship the Same God? |Jay Parini |December 7, 2014 |DAILY BEAST 

The translation agency only advertises for girls with “no complexes”: code for being prepared to bed the client. Russia’s Gold Digger Academy |Peter Pomerantsev |November 11, 2014 |DAILY BEAST 

A look at the new translation of the works of Sor Juana Inés de la Cruz. Sor Juana: Mexico’s Most Erotic Poet and Its Most Dangerous Nun |Katie Baker |November 8, 2014 |DAILY BEAST 

Translation: What plays to the base is not what plays to the middle. Is Caring About Climate Change an Essential Part of LGBT Identity? |Jay Michaelson |September 21, 2014 |DAILY BEAST 

CNN did the helpful translation for us, though: Duck Dynasty was simply too profitable for A&E to halt filming. Phil Robertson’s Despicable AIDS Argument Should Be the Last Straw |Kevin Fallon |September 16, 2014 |DAILY BEAST 

In the following words, the vowels have no figure value, hence in translation are never counted. Assimilative Memory |Marcus Dwight Larrowe (AKA Prof. A. Loisette) 

Christopher Smart, an English poet and miscellaneous writer, died; known by a popular translation of Horace. The Every Day Book of History and Chronology |Joel Munsell 

His sayings come to us through several hands, and through more than one translation. God and my Neighbour |Robert Blatchford 

Messrs. Thomas Nelson and Sons have generously co-operated in permitting the use of the best translation. His Last Week |William E. Barton 

The translation should be compared with the original F. text, as given below it. Chaucer's Works, Volume 1 (of 7) -- Romaunt of the Rose; Minor Poems |Geoffrey Chaucer