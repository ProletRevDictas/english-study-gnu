Special projects editor Elizabeth Quill explores the implications of Einstein’s general theory of relativity, which was considered shocking in the early 1900s. Should corporations get access to our brains? |Nancy Shute |February 7, 2021 |Science News 

Einstein presented his general theory of relativity at the end of 1915 in a series of lectures in Berlin. Einstein’s theory of general relativity unveiled a dynamic and bizarre cosmos |Elizabeth Quill |February 3, 2021 |Science News 

To which I answer that you forget the lessons of Gulliver’s Travels and the relativity of size. Time Flows Toward Order - Issue 93: Forerunners |Julian Barbour |December 2, 2020 |Nautilus 

Besso never gave up on luring Einstein back to Machian relativity. When Einstein Tilted at Windmills - Issue 93: Forerunners |Amanda Gefter |November 18, 2020 |Nautilus 

In 1907, Albert Einstein realized that his brand-new theory of relativity must render faster-than-light communication impossible. Quantum Tunnels Show How Particles Can Break the Speed of Light |Natalie Wolchover |October 20, 2020 |Quanta Magazine 

More clumsily, fireworks stand in for the Big Bang and a potato and peas are invoked to explain relativity. Why Can’t Movies Capture Genius? |Clive Irving |December 14, 2014 |DAILY BEAST 

For that reason, researchers have developed a framework to describe quantum theory in combination with general relativity. Black Holes Exist. So Does Bad Science |Matthew R. Francis |September 28, 2014 |DAILY BEAST 

The equations of general relativity unambiguously predict event horizons forming if mass is sufficiently concentrated. Black Holes Exist. So Does Bad Science |Matthew R. Francis |September 28, 2014 |DAILY BEAST 

Bob Dylan makes the theory of relativity worth caring about at all: he is a seer. Bob Dylan: Why We Can Never Know Him |Elizabeth Wurtzel |November 9, 2013 |DAILY BEAST 

To doubt it sounds to biologists as absurd as denying relativity does to physicists. Ask the Big Questions |Deepak Chopra, Leonard Mlodinow |October 26, 2011 |DAILY BEAST 

On Spencer's own theory of relativity a cause only exists in relation to an effect. Theism or Atheism |Chapman Cohen 

This, however, would be an error—the sort of error that the theory of relativity avoids. The Analysis of Mind |Bertrand Russell 

Why is not man satisfied with the relativity which so obstinately clings to his existence? An Interpretation of Rudolf Eucken's Philosophy |W. Tudor Jones 

At first glance it would appear as if, at last, one had here to do with a direct relativity between cost and volume of business. Railroads: Rates and Regulations |William Z. Ripley 

But this does not mean mere relativity as between directly competing commodities or places. Railroads: Rates and Regulations |William Z. Ripley