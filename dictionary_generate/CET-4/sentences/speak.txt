Other researchers decoded speech from the brain signals of a paralyzed man who is unable to speak. New technology can get inside your head. Are you ready? |Laura Sanders |February 11, 2021 |Science News For Students 

Every time she talks to a caller the connection feels real, even if it’s with someone she’ll never speak to again. The loneliness of an interrupted adolescence |Ellen McCarthy |February 11, 2021 |Washington Post 

Over the years, I’ve spoken with some helmet companies that are wary of adding a rotational-energy standard to the test, because there are disagreements over methodology. The Trek WaveCel Helmet Lawsuit, Explained |Joe Lindsey |February 10, 2021 |Outside Online 

While Somali and Oromo may not be among the most-spoken languages in the state – or even the county – there are many residents who speak those languages in San Diego, home to the largest populations of those groups in the state. African Communities Warn Language Issues Could Shut Them Out of Redistricting |Maya Srikrishnan |February 10, 2021 |Voice of San Diego 

“I was confused,” says the doctor, who requested anonymity because his employer will not allow him to speak to the media. India is betting on glitchy software to inoculate 300 million people by August |Lindsay Muscato |February 10, 2021 |MIT Technology Review 

So we do demand justice and we do speak up and make demands. Mexico’s Priests Are Marked for Murder |Jason McGahan |January 7, 2015 |DAILY BEAST 

Or has the see and hear and speak-no-evil stance of the Republican House persuaded him that he is in the clear? The Price of Steve Scalise’s Silence |Jason Berry |January 7, 2015 |DAILY BEAST 

“We met the smuggler in the train station; he came to speak with us about the services he provided,” Yazbek says. Ghost Ships of the Mediterranean |Barbie Latza Nadeau |January 6, 2015 |DAILY BEAST 

He disagrees, though, and says it is the duty of every person—men, especially—to speak up on this issue. Can Hip-Hop Prevent Honor Killings? |Moral Courage |December 30, 2014 |DAILY BEAST 

They were born in 51 countries and speak 59 foreign languages, but they seemed bound by a single purpose and resolve. Cop Families Boo De Blasio at NYPD Graduation |Michael Daly |December 30, 2014 |DAILY BEAST 

“Perhaps you do not speak my language,” she said in Urdu, the tongue most frequently heard in Upper India. The Red Year |Louis Tracy 

Now first we shall want our pupil to understand, speak, read and write the mother tongue well. The Salvaging Of Civilisation |H. G. (Herbert George) Wells 

To be wiser than other men is to be honester than they; and strength of mind is only courage to see and speak the truth. Pearls of Thought |Maturin M. Ballou 

Since this is a law of vibration, it is unscientific to speak of giving an overtone, for all tones contain overtones. Expressive Voice Culture |Jessie Eldridge Southwick 

When we speak against one capital vice, we ought to speak against its opposite; the middle betwixt both is the point for virtue. Pearls of Thought |Maturin M. Ballou