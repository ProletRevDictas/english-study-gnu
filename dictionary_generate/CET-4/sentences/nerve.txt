For example, a 2017 study demonstrated that nerve signals could be used for precise control of prosthetic limbs. Neurologists aren’t so sure about Elon Musk’s Neuralink brain implant startup |dzanemorris |August 31, 2020 |Fortune 

For example, at least half a dozen studies have found that stimulating the limb area acupoint can suppress systemic inflammation, partially through the vagus nerve. We Need New, Safer Ways to Treat Pain. Could Electroacupuncture Be One? |Shelly Fan |August 18, 2020 |Singularity Hub 

From remembering facts or conversations to improving musical or athletic skills, learning alters connections between nerve cells called synapses. Brain Scientists Haven’t Been Able To Find Major Differences Between Women’s And Men’s Brains, Despite Over A Century Of Searching |LGBTQ-Editor |August 13, 2020 |No Straight News 

The toxin, known by the initials TTX, stops nerve cells from sending signals that tell muscles to move. Toxic germs on its skin make this newt deadly |Erin Garcia de Jesus |June 23, 2020 |Science News For Students 

Contradictory evidence means that no one knows whether SARS-CoV-2 can infect nerve cells in the brain directly, and if so, whether the virus’s route to the brain can sometimes start in the nose. The way the coronavirus messes with smell hints at how it affects the brain |Laura Sanders |June 12, 2020 |Science News 

Few have the nerve to call him and he's usually pleased when an old friend does. Alfred Hitchcock’s Fade to Black: The Great Director’s Final Days |David Freeman |December 13, 2014 |DAILY BEAST 

Then he lost his nerve and decided to live after all, and I called for help. A Million Ways to Die in Prison |Daniel Genis |December 8, 2014 |DAILY BEAST 

“But bringing it to New York was a bit nerve-wracking,” he admits. This Fashion World Darling Is Homeless |Erica Wagner |December 2, 2014 |DAILY BEAST 

For anyone—not just a fan—the first time performing on camera can be nerve wracking. Sotheby’s for Sex: The Problem with Auctioning Off Sex with A Porn Star |Aurora Snow |November 15, 2014 |DAILY BEAST 

She was a perky redhead of about 30 with lively blue eyes, a petite figure, and lots of nerve. Gay Talese on Charlie Manson’s Home on the Range |Gay Talese |October 31, 2014 |DAILY BEAST 

It was Carmena, every nerve of her loyal nature on the alert to baffle this pursuer of Alessandro and Ramona. Ramona |Helen Hunt Jackson 

To add point to this success, he knew that the victor of Montebello was straining every nerve to gain this very prize. Napoleon's Marshals |R. P. Dunn-Pattison 

I hadn't the nerve to stand there and tell her she'd never see her father again this side of the pearly gates. Raw Gold |Bertrand W. Sinclair 

It was a hair-raising problem, too, and called for every ounce of nerve and every particle of skill the boy possessed. Motor Matt's "Century" Run |Stanley R. Matthews 

This time it was really Mr. Bills, and Mrs. Biggs went out to meet him, while Eloise felt every nerve quiver with dread. The Cromptons |Mary J. Holmes