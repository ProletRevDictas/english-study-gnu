Police in riot gear—and others camouflaged in plain clothes—made nearly 300 arrests, citing the majority for “unlawful assembly.” Hong Kong’s citywide COVID-19 testing has become a barometer of public trust |eamonbarrett |September 9, 2020 |Fortune 

They have to pay employees to fill out the forms, but they could have someone doing that for $15 an hour, so it’s probably just a mass assembly line. The Loan Company That Sued Thousands of Low-Income Latinos During the Pandemic |by Kiah Collier, Ren Larson and Perla Trevizo |August 31, 2020 |ProPublica 

Over time, Shih argues, outsourcing has cannibalized not only the assembly line jobs we associate with the factory floor, but the whole chain of intellectual effort that makes those jobs possible. Unmade in America |Tate Ryan-Mosley |August 14, 2020 |MIT Technology Review 

Every aspect of project management, systems engineering, risk management, and logistics of the machine assembly must perform together with the precision of a Swiss watch. Construction of the World’s Biggest Nuclear Fusion Plant Just Started in France |Edd Gent |August 3, 2020 |Singularity Hub 

The economy is changing at a rate of speed that looks like something that I’m sure people who didn’t have electrification, and they got electricity, and then we had assembly lines, felt. Fed Up (Ep. 390) |Stephen J. Dubner |September 26, 2019 |Freakonomics 

His son, Yaqoob Bizenjo, served as a member of the National Assembly until 2013. The Dangerous Drug-Funded Secret War Between Iran and Pakistan |Umar Farooq |December 29, 2014 |DAILY BEAST 

The FSLN-controlled legislative assembly approved the mega-project under a cloud of secrecy in a record seven days. China’s Nicaragua Canal Could Spark a New Central America Revolution |Nina Lakhani |November 30, 2014 |DAILY BEAST 

At break time, the entire assembly line would run over to play the machines that were ready to be shipped out. ‘Asteroids’ & The Dawn of the Gamer Age |David Owen |November 29, 2014 |DAILY BEAST 

Call your state senators, your assembly members, your mayors, and your city councils. A Navy Vet’s Case for Gun Control |Shawn VanDiver |November 23, 2014 |DAILY BEAST 

A representative assembly, John Adams wrote in 1776, “should be in miniature an exact portrait of the people at large.” Is It Time to Take a Chance on Random Representatives? |Michael Schulson |November 8, 2014 |DAILY BEAST 

What course was taken to supply that assembly when any noble family became extinct? Gulliver's Travels |Jonathan Swift 

Opening of the national assembly of France, after the abdication of Louis Philippe. The Every Day Book of History and Chronology |Joel Munsell 

Struck with surprise, the dead silence of profound awe, for an instant stilled the whole assembly. The Pastor's Fire-side Vol. 3 of 4 |Jane Porter 

"We yield obedience to the act granting duties," declared the Massachusetts Assembly. The Eve of the Revolution |Carl Becker 

Fresh Commissioners came from the Assembly, and it was only their fortunate recall to Paris that saved the general from arrest. Napoleon's Marshals |R. P. Dunn-Pattison