Daryl Groom, a former winemaker with Penfolds in Australia and Geyser Peak in California, is perhaps Naked Wines’s best-known winemaker, marketing his DRG line of wines through the platform. Online wine sales continue to grow, but can they — or should they — replace local shops? |Dave McIntyre |February 26, 2021 |Washington Post 

It’s large and fairly heavy, though, so it works best for those with medium- or large-sized desks rather and a dedicated workspace, rather than hot desks or cafe tables. Best ergonomic keyboards for hand and wrist pain |PopSci Commerce Team |February 26, 2021 |Popular-Science 

Researchers have found that open and honest communication works best. Digital self-harm: What to do when kids cyberbully themselves |Juli Fraga |February 26, 2021 |Washington Post 

Online consumer intelligence and social media listening platform Brandwatch has been acquired by Cision, best known for its media monitoring and media contact database services, for $450 million, in a combined cash and shares deal. Brandwatch is acquired by Cision for $450M, creating a PR, marketing and social listening giant |Mike Butcher |February 26, 2021 |TechCrunch 

So it might be worth picking a desk with multiple features just to see what works best for you. Best home office desk: Standing desks, office tables, and more |PopSci Commerce Team |February 25, 2021 |Popular-Science 

The best comparison here for an American audience is, well, Internet stuff. Trolls and Martyrdom: Je Ne Suis Pas Charlie |Arthur Chu |January 9, 2015 |DAILY BEAST 

Strangio is at his best when exposing what appears to be a flourishing civil society in Cambodia. Cambodia’s Smoke-and-Mirrors Democracy |David Shaftel |January 9, 2015 |DAILY BEAST 

Her style, much like her diminutive nickname, is best described as “Hamptons twee”—preppy and peppy. How Taryn Toomey’s ‘The Class’ Became New York’s Latest Fitness Craze |Lizzie Crocker |January 9, 2015 |DAILY BEAST 

Perhaps on his own nowadays, Epstein is trying his best to webmaster over a dozen URLs. Sleazy Billionaire’s Double Life Featured Beach Parties With Stephen Hawking |M.L. Nestel |January 8, 2015 |DAILY BEAST 

Scalise spoke briefly, adding little of substance, saying that the people back home know him best. The Price of Steve Scalise’s Silence |Jason Berry |January 7, 2015 |DAILY BEAST 

We resolved to do our best to merit the good opinion which we thus supposed them to entertain of us. Blackwood's Edinburgh Magazine, No. CCCXXXIX. January, 1844. Vol. LV. |Various 

He is perplexed and hindered by the lack of soldiers, but is doing his best with his small forces. The Philippine Islands, 1493-1898, Volume XX, 1621-1624 |Various 

If Mac had been alone he would have made the post by sundown, for the Mounted Police rode picked horses, the best money could buy. Raw Gold |Bertrand W. Sinclair 

Her best-known works are portrait busts, which are numerous. Women in the fine arts, from the Seventh Century B.C. to the Twentieth Century A.D. |Clara Erskine Clement 

But the Mexicans were not the people to give up their best province so easily. Blackwood's Edinburgh Magazine, No. CCCXXXIX. January, 1844. Vol. LV. |Various