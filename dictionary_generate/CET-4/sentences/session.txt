No other teams were in the gym during the sessions, though Clark said occasionally non-Patrick Henry students would join in. School Sports Became ‘Clubs’ Amid the Pandemic – Now Two Coaches Are Out |Ashly McGlone |September 17, 2020 |Voice of San Diego 

Earlier this year, boxing superstar Mike Tyson shared astonishing video clips from one of his recent sparring sessions. Mike Tyson’s comeback is a counterpunch to age discrimination |Lila MacLellan |September 16, 2020 |Quartz 

I felt I had just gone to a group therapy session, but had one too many beers to properly take part. The Last Bar Standing? |Eugene Robinson |September 15, 2020 |Ozy 

I don’t think the governor – certainly he wouldn’t call a special session for that conversation, but if we do have a special session, that needs to be a part of it. Sacramento Report: Bipartisan Support for a Special Session |Sara Libby |September 11, 2020 |Voice of San Diego 

SitterStream is a new startup providing 30- to 90-minute virtual babysitting and tutoring sessions. The best back-to-school benefits companies are offering their employees |ehinchliffe |September 10, 2020 |Fortune 

These are eight of the most interesting laws passed in the second session of the 113th Congress. Nazis, Sunscreen, and Sea Gull Eggs: Congress in 2014 Was Hella Productive |Ben Jacobs |December 29, 2014 |DAILY BEAST 

Her services include a makeup session for a night out for $50. Inside A Finishing School for Transwomen |Sharon Adarlo |December 27, 2014 |DAILY BEAST 

On piano was legendary session man Billy Preston, who co-wrote and recorded the original version one year before Cocker's. Joe Cocker's Deep Live Cuts |Asawin Suebsaeng |December 22, 2014 |DAILY BEAST 

Couple removed the weights and continued the session further. Dungeons and Genital Clamps: Inside a Legendary BDSM Chateau |Ian Frisch |December 20, 2014 |DAILY BEAST 

But when the 114th Congress gavels into session in January, GOP speechwriters are going to need some new material. Harvard’s Conservative Cabal Takes Congress |Patricia Murphy |December 17, 2014 |DAILY BEAST 

It will be a busy session; and I want to see if I can't become a useful public man. Elster's Folly |Mrs. Henry Wood 

Important orders for new books are now in course of execution, the volumes being due early in this year's session. Report of the Chief Librarian for the Year 1924-25 |General Assembly Library (New Zealand) 

Three to four examples thoroughly studied are quite sufficient for one session or sitting. Assimilative Memory |Marcus Dwight Larrowe (AKA Prof. A. Loisette) 

He was one of the lords of session in Scotland, and a philosophical writer of considerable learning, but of peculiar notions. The Every Day Book of History and Chronology |Joel Munsell 

Policeman: The prisoner's three brothers were transported last session, and his mother and father are now in Clerkenwell. The Book of Anecdotes and Budget of Fun; |Various