Second, the recent surge looks relatively large compared to the spring spike — but in reality, it’s probably smaller. The new Covid-19 case surge in Europe, explained |Julia Belluz |September 17, 2020 |Vox 

In the 1940s, trailblazing physicists stumbled upon the next layer of reality. How Mathematical ‘Hocus-Pocus’ Saved Particle Physics |Charlie Wood |September 17, 2020 |Quanta Magazine 

It’s still a ways off, but if companies like Wildtype can make their vision a reality, people, animals, and the planet will all be better off for it. This Startup Is Growing Sushi-Grade Salmon From Cells in a Lab |Vanessa Bates Ramirez |September 16, 2020 |Singularity Hub 

The reality, though, can flower into all kinds of weirdness. Making Poly Less of a Pain in the Ass |Eugene Robinson |September 14, 2020 |Ozy 

In many ways, it feels like Americans increasingly live in two different realities. Political rhetoric may impact your hurricane preparedness |Ula Chrobak |September 11, 2020 |Popular-Science 

But if Democrats are faced with the reality of a glut of qualified candidates, Republicans are assembling more of a fantasy team. The Golden State Preps for the ‘Red Wedding’ of Senate Races |David Freedlander |January 9, 2015 |DAILY BEAST 

That is a reality that still eats at Grace Castro and Yvonne Lozoya. An Informant, a Missing American, and Juarez’s House of Death: Inside the 12-Year Cold Case of David Castro |Bill Conroy |January 6, 2015 |DAILY BEAST 

His hero, Bruce Springsteen, is a gazillionaire, but he still manages to come across as a regular guy, so perception is reality. Will Chris Christie Regret His Cowboy Hug? |Matt Lewis |January 5, 2015 |DAILY BEAST 

He was a dreamer, an idealist, grounded in the reality he observed around him. Mario Cuomo, Always Moving Us Toward the Light |Mike Barnicle |January 4, 2015 |DAILY BEAST 

I mean, the reality of it was, I had to go out and get on a horse, and ride in, shoot the gun — how hard was that, right? The Story Behind Lee Marvin’s Liberty Valance Smile |Robert Ward |January 3, 2015 |DAILY BEAST 

The intensity of his sensations seemed inexplicable, unless some reality, some truth, lay behind them. The Wave |Algernon Blackwood 

With less intelligent children traces of this tendency to take pictorial representation for reality may appear as late as four. Children's Ways |James Sully 

Thus they become accustomed to act as christians, to become so in reality in his time. The Jesuit Relations and Allied Documents, Vol. II: Acadia, 1612-1614 |Various 

Isaacson thought what the world would say, and suddenly he knew the reality of his affection for Nigel. Bella Donna |Robert Hichens 

In reality he was annoyed at having old Monsieur Farival, who considered himself the better sailor of the two. The Awakening and Selected Short Stories |Kate Chopin