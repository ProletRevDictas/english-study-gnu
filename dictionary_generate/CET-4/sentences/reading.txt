Separately, 84% of the CFOs surveyed by Deloitte for their quarterly survey, coming out later today, say the stock market is overvalued…the second highest reading in the survey’s history. Inclusion in the Dow does not guarantee a bump to your share price |Alan Murray |August 27, 2020 |Fortune 

In the past few years Jensen’s gallery has branched out, hosting public speakings, book readings, musical performances and sit-down meals. Can the Arts Save Rural America From the Recession? |Charu Kasturi |August 16, 2020 |Ozy 

The product blends readings from thousands of ground stations, weather balloons and satellites, and it uses weather models to intelligently fill in the blanks. Global Wave Discovery Ends 220-Year Search |Charlie Wood |August 13, 2020 |Quanta Magazine 

In order to pass, the bill must be approved in two more Knesset readings. Israel lawmakers move to ban conversion therapy |Kaela Roeder |August 12, 2020 |Washington Blade 

From a more diverse required reading list to structural reforms, students are increasingly stepping up to force change, with groups such as Diversify Our Narrative organizing online. Is School Out Forever? |Daniel Malloy |August 9, 2020 |Ozy 

It's cheesy and ludicrous and, therefore, delightful; it's the reading equivalent of hate-watching. ‘A Gronking to Remember’ Speed Read: 8 Naughtiest Bits |Emily Shire |January 7, 2015 |DAILY BEAST 

Once I began reading, I realized A Gronking to Remember was a masturbatory tribute to the New England Patriots. ‘A Gronking to Remember’ Speed Read: 8 Naughtiest Bits |Emily Shire |January 7, 2015 |DAILY BEAST 

I gave a reading last week with someone who had taken a class of mine. Daphne Merkin on Lena Dunham, Book Criticism, and Self-Examination |Mindy Farabee |December 26, 2014 |DAILY BEAST 

My choice is that it should be required reading by those who run the city of Fort Lauderdale, Florida. How Dickens and Scrooge Saved Christmas |Clive Irving |December 22, 2014 |DAILY BEAST 

Corden has actually been attached to Into the Woods since the first reading of the screenplay two-and-a-half years ago. New ‘Late Late Show’ Host James Corden Would Like to Manage Your Expectations |Kevin Fallon |December 18, 2014 |DAILY BEAST 

The lady in black was reading her morning devotions on the porch of a neighboring bathhouse. The Awakening and Selected Short Stories |Kate Chopin 

I do not care very much how you censor or select the reading and talking and thinking of the schoolboy or schoolgirl. The Salvaging Of Civilisation |H. G. (Herbert George) Wells 

While Louis was reading these dispatches, he received a summons from Elizabeth, to attend her immediately. The Pastor's Fire-side Vol. 3 of 4 |Jane Porter 

Lawrence and Dan were passing a newspaper office, before which a large crowd had gathered, reading the war bulletins. The Courier of the Ozarks |Byron A. Dunn 

This reading secures scarcely anything more than a succession of sights to the eye or sounds to the ear. Assimilative Memory |Marcus Dwight Larrowe (AKA Prof. A. Loisette)