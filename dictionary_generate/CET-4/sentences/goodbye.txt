Check out this guide to help you find the best hand warmers for your needs, and say goodbye to tripling up on gloves just to walk to the grocery store. Best hand warmers: Block the chill during your favorite winter activities |PopSci Commerce Team |February 10, 2021 |Popular-Science 

Fifty-three years ago, Paul Grisham put on his heavy snow gear and said goodbye to Antarctica after spending 13 months there as a meteorologist with the Navy at one of the most remote stations on Earth. This man mistakenly left his wallet in Antarctica. Some 53 years later, he got it back. |Cathy Free |February 9, 2021 |Washington Post 

The publisher said goodbye to Google Analytics in favor of a homegrown alternative called Longboat and hasn’t looked back. ‘We had to take full ownership of data’: Why Denmark’s biggest news site cut reliance on Google’s tech |Seb Joseph |January 27, 2021 |Digiday 

Say goodbye to showing up with wet hair, and get ready to start feeling cool and confident whenever you leave the house. The best hair dryer: Get a salon-worthy blowout at home |Carsen Joenk |January 22, 2021 |Popular-Science 

When her brother was dying, she flew home to say goodbye, but owing to travel delays, she arrived too late. In the animal kingdom, rituals that connect, renew and heal |Barbara King |January 22, 2021 |Washington Post 

Just a few short years ago, I sat down at my computer, and I typed out a similar goodbye letter. Dear Leelah, We Will Fight On For You: A Letter to a Dead Trans Teen |Parker Molloy |January 1, 2015 |DAILY BEAST 

Say goodbye to fighting grandma over whether to watch The Good Wife or The Walking Dead come Christmas Night. New Innovations Let You Watch TV Anywhere You Go | |December 8, 2014 |DAILY BEAST 

The rest of the episode follows Carrie spreading the gospel of her indignance over the thoughtless goodbye. Confessions of a Rom-Com Writer: Liz Tuccillo Talks ‘Sex and the City,’ ‘Take Care,’ and More |Kevin Fallon |December 5, 2014 |DAILY BEAST 

Never Can Say Goodbye also addresses what happens when the romantic notion dissipates. Writers to New York: I Wish I Knew How to Quit You |Molly Hannon |December 1, 2014 |DAILY BEAST 

Those images you took of people saying goodbye to the bodies of their loved ones are particularly haunting. The Photojournalist Who Stared Down Ebola |Abby Haglage |November 8, 2014 |DAILY BEAST 

De Reb laughs an' sez, 'kin' o' you sir,' an' he waves goodbye ter de crowd an' dey carried him off a laughin' fit ter kill. Slave Narratives: a Folk History of Slavery in the United States |Various 

As we came on board we saw our ambassador, Mr. Van Dyke, tell some of his friends goodbye and wish them Godspeed. Ways of War and Peace |Delia Austrian 

This same determination and courage came to the women when they told their husbands and sweethearts goodbye. Ways of War and Peace |Delia Austrian 

When all is over, perhaps you will have the honesty to come and tell me which was right—you or I. Goodbye. When Valmond Came to Pontiac, Complete |Gilbert Parker 

He thought his uncle looked ill when he said goodbye, and the old man spoke despondently of the failure his life had been. Masterpieces of Mystery, Vol. 1 (of 4) |Various