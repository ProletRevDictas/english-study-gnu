That month, Turkey’s finance minister Berat Albayrak—Erdogan’s son-in-law—resigned, and Erdogan sacked the head of the central bank. The 'Badass Chief of Staff' of Turkey's Opposition Faces Years in Jail After Challenging Erdogan's Power. She's Not Backing Down |Joseph Hincks / Istanbul |February 24, 2021 |Time 

Better to use the cell phone network from the Turkish company Turkcell, even if it meant Turkey could hear every move they made. The Women Who Fought to Defend Their Homes Against ISIS |Gayle Tzemach Lemmon |February 22, 2021 |Time 

Though Turkey tries to tweet-shame America on the Capitol attack, its independent journalists have faced detentions and criminal investigations for reporting on the government’s management of the pandemic. Save the Sarcasm for Other Democracies. America Is Fine |Debasish Roy Chowdhury |January 11, 2021 |Time 

Dream center Elizabeth Williams, playing in Turkey, took part in a virtual Warnock for Georgia event last month. WNBA players helped oust Kelly Loeffler from the Senate. Will she last in the league? |Candace Buckner |January 7, 2021 |Washington Post 

In Turkey, the government’s health ministry said that an interim analysis of late-stage trials showed that the vaccine was 91% effective. Data delays aren’t slowing the global rollout of a Chinese COVID-19 vaccine |Grady McGregor |January 4, 2021 |Fortune 

Meanwhile two kids were taken from their mother when she flew back to the UK from Turkey. Britain May Spy on Preschoolers Searching for Potential Jihadis |Nico Hines |January 7, 2015 |DAILY BEAST 

After two nights in detention, he was scheduled to be deported back to Turkey on Monday. Pope-Shooter Ali Agca’s Very Weird Vatican Visit |Barbie Latza Nadeau |December 29, 2014 |DAILY BEAST 

In Turkey, crime groups in border areas are exploiting the labor of Syrian male refugees who cannot find legitimate employment. ISIS, Boko Haram, and the Growing Role of Human Trafficking in 21st Century Terrorism |Louise I. Shelley |December 26, 2014 |DAILY BEAST 

She is also head of the Sabancı Foundation, which conducts female-empowerment programs for women in rural Turkey. The Women Battling an Islamist Strongman |Christina Asquith |December 22, 2014 |DAILY BEAST 

Turkey has had more than a decade of economic boom, and is now the sixth-most-visited tourist destination in the world. The Women Battling an Islamist Strongman |Christina Asquith |December 22, 2014 |DAILY BEAST 

Elderly matrons—and in Turkey every lady is an elderly matron in her fortieth year—are passionately devoted to this enjoyment. Tobacco; Its History, Varieties, Culture, Manufacture and Commerce |E. R. Billings. 

Edmund Chishull, an English traveler, died; author of a book of travels in Turkey. The Every Day Book of History and Chronology |Joel Munsell 

We have only to keep our grip firm and fast; Turkey will die of exhaustion trying to do what she can't do; drive us into the sea! Gallipoli Diary, Volume I |Ian Hamilton 

Turkey tobacco ranges in color from brown to light yellow, the latter being the most in demand. Tobacco; Its History, Varieties, Culture, Manufacture and Commerce |E. R. Billings. 

In the meantime he was completely infatuated, and trotted about after Sarah like an old turkey cock. Skipper Worse |Alexander Lange Kielland