The shift toward culture fights driven by conservative media has real implications for the evolution and expectations of that group of Americans, including an erosion in the democratic system the GOP was built to leverage. What happens to an entrenched two-party system when one party undermines the system? |Philip Bump |February 12, 2021 |Washington Post 

By the time the fight finishes, Irene has backed out of the elevator, staring. What floor? |Sophia Nguyen |February 12, 2021 |Washington Post 

That's one possible explanation for why so many species have evolved various defensive structures to protect them from damage during a fight. Caged heat: Mesquite bugs battle in a plastic cup—for science! |Jennifer Ouellette |February 11, 2021 |Ars Technica 

With the Washington Wizards back at Capital One Arena for the first time in a week Wednesday, one of the things Coach Scott Brooks hoped his team would carry from its up-and-down trip was the importance of fight. The Wizards’ defense again lets them down in loss at home to Raptors |Ava Wallace |February 11, 2021 |Washington Post 

In the nation’s capital, teachers and students returned to school buildings last week after a drawn-out fight over how and when classrooms would reopen. Chicago reaches deal with teachers to reopen school buildings |Moriah Balingit |February 10, 2021 |Washington Post 

Gay marriage was the hot-button fight on the left and right. Jeb Bush’s Unseen Anti-Gay Marriage Emails |Jackie Kucinich |January 9, 2015 |DAILY BEAST 

Iraq may have been an irregular fight, but it had major moments. Pentagon Doesn’t Know How Many People It’s Killed in the ISIS War |Nancy A. Youssef |January 7, 2015 |DAILY BEAST 

The U.S. military is finally starting to train Iraqi troops to fight ISIS in restive Anbar province. Pentagon Insider on New Plan to Fight ISIS: ‘Of Course It’s Not Enough’ |Nancy A. Youssef |January 6, 2015 |DAILY BEAST 

She says she will have to fight in “other ways” to get her client freed. Amal Clooney vs. Egypt’s Courts |Christopher Dickey |January 4, 2015 |DAILY BEAST 

Starting in the 1970s, then MPAA president Jack Valenti began what was to become a decades-long fight against the quota system. Propaganda, Protest, and Poisonous Vipers: The Cinema War in Korea |Rich Goldstein |December 30, 2014 |DAILY BEAST 

It was more like the boarding of a ship than any land fight I had ever seen or imagined. Blackwood's Edinburgh Magazine, No. CCCXXXIX. January, 1844. Vol. LV. |Various 

The fight lasted two days, and only two men out of the five hundred escaped with their lives. Blackwood's Edinburgh Magazine, No. CCCXXXIX. January, 1844. Vol. LV. |Various 

But they soon fell out, for Murat had the audacity to try and make these patriots fight instead of merely seeking plunder. Napoleon's Marshals |R. P. Dunn-Pattison 

It was found afterwards that the rebels meant to fight the two British forces in detail before they could effect a junction. The Red Year |Louis Tracy 

Ike had read the "Herald," with all about "the great prize fight" in it, and had become entirely carried away with it. The Book of Anecdotes and Budget of Fun; |Various