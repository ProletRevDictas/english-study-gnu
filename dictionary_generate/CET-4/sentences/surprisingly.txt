In the United States, especially as infection rates continue to rise, it’s not surprising that teachers are afraid to return to the classroom. Schools are not spreading covid-19. This new data explains why. |Emily Oster |November 20, 2020 |Washington Post 

The stark message from the premier public health agency may not seem surprising given the dire state of the country. CDC Thanksgiving guidance: No traveling, no outside-household members |Beth Mole |November 19, 2020 |Ars Technica 

So it’s not surprising that mercury has made its way to the deepest reaches of the ocean as well. Researchers found signs of human pollution in animals living six miles beneath the sea |Kate Baggaley |November 19, 2020 |Popular-Science 

With a brilliant mathematical mind engaged in unraveling the deepest questions in theoretical physics, it is not surprising that many of Pauli’s dreams had geometric elements and abstract symbols. The Synchronicity of Wolfgang Pauli and Carl Jung - Issue 93: Forerunners |Paul Halpern |November 18, 2020 |Nautilus 

Takeout meals for members have turned out to be a surprising success. Things are shipshape at the Army and Navy Club on Farragut Square in D.C. |John Kelly |November 18, 2020 |Washington Post 

But Obamacare picked itself up and dusted itself off surprisingly well. You Were Wrong About Miley & Bitcoin: 2014’s Failed Predictions |Nina Strochlic |December 31, 2014 |DAILY BEAST 

Not surprisingly, many middle and working class voters, particularly whites, have deserted the Democrats in increasing numbers. Time to Bring Back the Truman Democrats |Joel Kotkin |December 21, 2014 |DAILY BEAST 

Surprisingly Great Hotel - Clean, Tasteful.... and North Korean! Inside the ‘Surprisingly Great’ North Korean Hacker Hotel |Michael Daly |December 20, 2014 |DAILY BEAST 

Not surprisingly, rates for recovery vary enormously, from as low as three percent to upwards of 75 percent. You’re Never ‘Cured’ of an Eating Disorder |Carrie Arnold |December 20, 2014 |DAILY BEAST 

That was challenging physically for me but actually doing Liz and being able to access her is surprisingly is pretty easy. The Zany Shades of Nick Kroll |Abby Haglage |December 15, 2014 |DAILY BEAST 

The adventure had surprisingly followed upon the discovery that Alicia had been quite wrong. Hilda Lessways |Arnold Bennett 

But he was so surprisingly dexterous with his lips, and feet too, when he was in his cabin that I suppose I put them down to that. Uncanny Tales |Various 

She preceded him along a passage and then, taking a door on the left, found herself surprisingly in the shop, behind a counter. Hilda Lessways |Arnold Bennett 

Generally speaking, the hotel accommodations in the provincial towns throughout England and Scotland are surprisingly good. British Highways And Byways From A Motor Car |Thomas D. Murphy 

Three Americans were killed, seventeen others wounded and four deserters were surprisingly found aboard the "Chesapeake." Hallowed Heritage: The Life of Virginia |Dorothy M. Torpey