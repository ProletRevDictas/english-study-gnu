So it was me, the taxi driver, and a man with a broken arm moving all these priceless cars out of the barn. Telling men’s stories through their cars |Daniel Bentley |October 19, 2020 |Fortune 

A great option for a rustic, farmhouse transitional style media stand, this unit comes in several colorways and includes sliding barn style doors, two side doors with metal mesh, and a distressed pine finish. The best TV and media stands for your home |PopSci Commerce Team |October 16, 2020 |Popular-Science 

Right now, the barn is relatively simple and open with an astroturf floor, but as the season progresses, Roginson plans to add propane heaters and three vinyl walls on the north, south, and west sides of the structure to insulate from the cold. A Looming Menace for Restaurants: Winter Is Coming |Brenna Houck |October 15, 2020 |Eater 

Several of these measures are sound ideas, but unfortunately, two of its latest efforts once again amount to waiting until the horse has made it halfway around the world before you shut the barn door. Facebook to pause all political advertising—after the election |Kate Cox |October 8, 2020 |Ars Technica 

The store, the barn, and the other structures were consumed by the fire Monday night. A California mountain community loses its heart |Mike Silverstein |September 10, 2020 |Washington Blade 

He was born in a barn to penniless parents who were part of a people under occupation. Jesus Wasn’t Born Rich. Think About It. |Gene Robinson |December 25, 2014 |DAILY BEAST 

From the roof of the barn is a long loop of rope, through this the turkey is suspended by its legs. Confessions of a Turkey Killer |Tom Sykes |November 26, 2014 |DAILY BEAST 

There is an ancestral homestead, but it has a meth lab in the barn. Book Bag: Gritty Stories From the Real Montana |Carrie La Seur |October 2, 2014 |DAILY BEAST 

I read that you recorded it at a converted barn in Devon, UK. La Roux Discusses New Album ‘Trouble in Paradise,’ the 5-Year Gap, and Embracing Her Androgyny |Marlow Stern |July 6, 2014 |DAILY BEAST 

Also happy to get some fresh air (and a huge, aromatic cigar at a safe distance from the barn). What Did TJ Mean By “Pursuit of Happiness,” Anyway? |P. J. O’Rourke |June 8, 2014 |DAILY BEAST 

He went blindly down the street, turned at the corner and sought a quiet place, a livery barn. The Homesteader |Oscar Micheaux 

Then, kindly and gently, the boy took Squinty over to the place where the corn crib was built on to the barn. Squinty the Comical Pig |Richard Barnum 

She knew there would be plenty of rope in the Norwood barn or the garage for their need in erecting the aerials. The Campfire Girls of Roselawn |Margaret Penrose 

If we go against 'em now, it'll be all same goin' blindfolded into a barn t' pick out the best hoss. Raw Gold |Bertrand W. Sinclair 

A man driving a number of cattle to Boston, one of his cows went into a barn-yard, where there stood a young lad. The Book of Anecdotes and Budget of Fun; |Various