Maryland hasn’t played the same opponent on back-to-back nights since January 1949 at Miami. In schedule shuffle, Maryland will host Nebraska on back-to-back days next week |Emily Giambalvo |February 12, 2021 |Washington Post 

The game at Texas Southern was thrown together Friday, about 30 hours before tip-off, when Texas Southern also had an opponent suddenly cancel because of the virus. A rare disease, a covid diagnosis, a painful decision: The death of basketball coach Lew Hill |Dave Sheinin |February 11, 2021 |Washington Post 

At just 19, he’s accustomed to being hunted by every opponent and still finding a way to make you remember him. An NBA experiment lets draft prospects skip college, stay home and get paid to play |Michael Lee |February 11, 2021 |Washington Post 

First off, California may have been Democratic-leaning in 2003, but it has a notably deeper shade of blue today, which will make it more challenging for Newsom’s opponents to engineer his ouster. California’s Gavin Newsom Will Likely Face A Recall Election — But He’ll Probably Survive It |Geoffrey Skelley (geoffrey.skelley@abc.com) |February 10, 2021 |FiveThirtyEight 

Campbell said she simply doesn’t buy into vacation rental opponents’ thinking that banning them altogether is possible. Morning Report: The Deal With the Jen Campbell Recall |Voice of San Diego |February 9, 2021 |Voice of San Diego 

Despite the scandal, Grimm beat his Democratic opponent by 18 points in November. 2014 Was a Delectably Good Year for Sleaze |Patricia Murphy |December 30, 2014 |DAILY BEAST 

One would be hard-pressed to find an earlier opponent of the Nazis than Dietrich von Hildebrand. The Catholic Philosopher Who Took on Hitler |John Henry Crosby |December 26, 2014 |DAILY BEAST 

Mired in ideological warfare, America faces her most formidable opponent yet— herself. The Post-Brown and Garner Question: Who ‘Deserves’ to Die? |Goldie Taylor |December 9, 2014 |DAILY BEAST 

This opponent is like no other Rodgers has to face on the gridiron. Aaron Rodgers Takes Aim at Congo’s ‘Blood Minerals’ War |John Prendergast |December 3, 2014 |DAILY BEAST 

Perhaps the greatest irony remains that civil rights titan Caesar Chavez was a lifelong opponent of illegal immigration. The Liberal Case Against Illegal Immigration |Doug McIntyre |November 25, 2014 |DAILY BEAST 

He realized, as his mother had realized a little while before, that in Garnache they had an opponent who took no chances. St. Martin's Summer |Rafael Sabatini 

In a trial in the King's Bench, Mr. Erskine, counsel for the defendant, was charged by his opponent with traveling out of his way. The Book of Anecdotes and Budget of Fun; |Various 

Scarcely had he smoothed the way with one opponent than another sprung up in an unexpected quarter. Life of Richard Trevithick, Volume II (of 2) |Francis Trevithick 

As for the slipperiness of the ground, my opponent will run no greater risks than I. I am not the only impatient one. St. Martin's Summer |Rafael Sabatini 

Augereau at first confined himself to parrying, but at last, being wounded, he thrust out and killed his opponent. Napoleon's Marshals |R. P. Dunn-Pattison