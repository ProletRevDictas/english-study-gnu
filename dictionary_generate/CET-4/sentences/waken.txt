Drink, and go to sleep—sleep so sound that you waken only when Moukir and Nakir, the death angels, sift soul from body. God Wills It! |William Stearns Davis 

The servant, who carried her breakfast, reported her asleep, and the careful mother would not let them waken her. Alone |Marion Harland 

Humphrey and Pablo both set off, and then Edward went to waken the boy, still lying on the bed. The Children of the New Forest |Captain Marryat 

A conscience that had overslept itself began to stir and waken. Quin |Alice Hegan Rice 

"Death must come to all," some one would waken up to murmur. Auld Licht Idylls |J. M. Barrie