They have been studying a roughly 7-million-year-old fossil. American crocs seem to descend from kin that crossed the Atlantic |Carolyn Wilke |August 25, 2020 |Science News For Students 

These changes equate to a roughly 6 and 9 percent jump in body mass in males and females, respectively, or about a one kilogram increase on average, the team reports July 31 in the Biological Journal of the Linnean Society. Culling dingoes with poison may be making them bigger |Jake Buehler |August 19, 2020 |Science News 

The home-improvement chain said that domestic same-store sales rose 25% in the quarter that ended in early August, roughly twice as much as Wall Street expected. Home Depot sales jump 25% as homebound shoppers spruce up their houses |Phil Wahba |August 18, 2020 |Fortune 

Petra Nova was meant to cut the unit’s carbon footprint by about a third—roughly the equivalent of taking 300,000 cars off the road each year. The business model for carbon capture is broken |Tim McDonnell |August 13, 2020 |Quartz 

Lord’s group extracted a complete set of nuclear DNA, which is inherited from both parents, from a roughly 18,530-year-old woolly rhino bone. Climate change, not hunters, may have killed off woolly rhinos |Bruce Bower |August 13, 2020 |Science News 

The U.S. only plans to train roughly 3,000 Iraqi troops in the first year. Pentagon Insider on New Plan to Fight ISIS: ‘Of Course It’s Not Enough’ |Nancy A. Youssef |January 6, 2015 |DAILY BEAST 

These (roughly) $2,500 ceremonies are supposedly about encouraging “positive feelings” on the part of the single brides. Why Singles Should Say ‘I Don’t’ to The Self-Marriage Movement |Tim Teeman |December 30, 2014 |DAILY BEAST 

It is a multimillion-dollar business in which roughly 15 million fowl die a year. The History of the Chicken: How This Humble Bird Saved Humanity |William O’Connor |December 27, 2014 |DAILY BEAST 

By the end of the construction period, the number of deaths had reached roughly twenty percent of the workforce. Riding Thailand’s WWII Death Railway |Liza Foreman |December 21, 2014 |DAILY BEAST 

Since 1987, there have been roughly 1,300 cases filed under the blasphemy laws, according to varied reports. Disco Mullah Blasphemy Row Highlights Pakistan’s Hypocrisy |Shaheen Pasha |December 21, 2014 |DAILY BEAST 

"We will go to the Hotel de l'Europe, if you press it;" and away the cabriolet joggled over the roughly paved street. Blackwood's Edinburgh Magazine, Volume 60, No. 372, October 1846 |Various 

He stepped to the girl, and roughly raised her chin with his hand so that she was forced to look him in the face. St. Martin's Summer |Rafael Sabatini 

A few have the eye and the artistic impulse needed for picturing, roughly at least, the look of an object. Children's Ways |James Sully 

Marceau, indignant at being rebuked by a young staff officer, roughly asked, "And who are you?" Napoleon's Marshals |R. P. Dunn-Pattison 

You could use some force to prevent him, you could not kill him, or put out his eyes, or treat him roughly. Putnam's Handy Law Book for the Layman |Albert Sidney Bolles