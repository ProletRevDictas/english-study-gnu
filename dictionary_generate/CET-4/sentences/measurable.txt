It is relatively easy to find measurable increases in decoding ability after phonics instruction. Is there really a ‘science of reading’ that tells us exactly how to teach kids to read? |Valerie Strauss |January 26, 2021 |Washington Post 

Your clients are investing in your services to see measurable results and a positive return on investment. Five reasons why clients are leaving your agency |Service Provider Pro (SPP.co) |January 12, 2021 |Search Engine Watch 

Express forecastForecast in detailWe’ll pass the midpoint of January this week, and it’s likely that we’ll have done so without any measurable snow or extreme cold. D.C.-area forecast: A mid-January week that’s not too cold and mostly free of snow and rain |Jason Samenow |January 11, 2021 |Washington Post 

The investment fund model also would help build precedent for expecting measurable returns from research—something that is seldom emphasized with grants or other forms of government spending. Why the U.S. needs a national climate investment fund |matthewheimer |January 3, 2021 |Fortune 

The trail further thickened with two Notre Dame timeouts and one officiating review, making the whole thing seem measurable by sundial. Alabama advances to the national title game with an artful offensive performance |Chuck Culpepper, Des Bieler |January 2, 2021 |Washington Post 

Direct funds away from practices, policies, and programs that consistently fail to achieve measurable outcomes. Can the U.S. Government Go Moneyball? |Peter Orszag, Jim Nussle |December 23, 2014 |DAILY BEAST 

And to be sure, classrooms are seeing measurable improvements under Common Core Standards. Why Voters Love Common Core |Harold Ford Jr. |November 28, 2014 |DAILY BEAST 

The astronomers found that of the 93 quasars in the sample, 19 exhibited a measurable amount of polarization. The Black Hole Tango |Matthew R. Francis |November 24, 2014 |DAILY BEAST 

The Sarah Palin Channel, by contrast, had a barely measurable 36,000 visitors in September, according to Quantcast. Stuck in the Lamestream: Sarah Palin TV Barely Registers on the Web |Lloyd Grove |November 7, 2014 |DAILY BEAST 

These movements sometimes occur despite no measurable blood flow to the brain. Real Life Lazarus: When Patients Rise From the Dead |Sandeep Jauhar |August 21, 2014 |DAILY BEAST 

The effect ought to be great enough to be measurable by astronomers in the course of a thousand years. Outlines of the Earth's History |Nathaniel Southgate Shaler 

Was it possible that at last he was actually within measurable distance of the solution of the mystery? The Doctor of Pimlico |William Le Queux 

It is characteristic that most of these actions take place at slow, very frequently easily measurable, rates of speed. The Elements of Qualitative Chemical Analysis, vol. 1, parts 1 and 2. |Julius Stieglitz 

Perhaps the greatest amount of variation in the skull is in features which are not readily measurable by the usual physical means. Speciation in the Kangaroo Rat, Dipodomys ordii |Henry W. Setzer 

Hence the mighty spirit of Islam, measurable only by a soul capacity which has never ceased to expand and develop. Islam Her Moral And Spiritual Value |Arthur Glyn Leonard