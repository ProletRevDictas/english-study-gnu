Following Szutowicz’s arrest, many protesters have been displaying rainbow flags across the city, and are subsequently being arrested for those actions. Poland’s LGBTQ activists confront growing crackdown |Kaela Roeder |August 14, 2020 |Washington Blade 

All the colors in the rainbow—and your t-shirt drawer, as it so happens. Scientists Gene-Hack Cotton Plants to Make Them Every Color of the Rainbow |Jason Dorrier |August 11, 2020 |Singularity Hub 

They gave away over 200 hundreds of rainbow masks in each spot. They Gave Away Rainbow Masks In “LGBT Free Zones” In Poland |LGBTQ-Editor |May 26, 2020 |No Straight News 

That’s why, when you see a double rainbow, the secondary arc is much dimmer and its colors are flipped. Explainer: Rainbows, fogbows and their eerie cousins |Matthew Cappucci |May 1, 2020 |Science News For Students 

I noticed when I went over to the chemistry department that a lot of the professors had stickers on their doors with a rainbow or triangle that said that they were LGBTQ allies. Weight lifting is this planetary scientist’s pastime |Bryn Nelson |March 10, 2020 |Science News For Students 

Meanwhile, big dollar advertising campaigns have taken an explicit rainbow-hued slant. Corporations Are No Longer Silent on LGBT Issues | |December 24, 2014 |DAILY BEAST 

She is smiling, a pink-striped hat on her head and a mini rainbow lollipop sticking out of her mouth. Even Grade School Kids Are Protesting the Garner Killing Now |Caitlin Dickson |December 6, 2014 |DAILY BEAST 

Here and there, sparingly, one of the dolls might be purple or green: “Rainbow Piets,” they call them. Dutch Try to Save Santa’s Slave |Nadette De Visser |December 2, 2014 |DAILY BEAST 

David Bowie, Ronnie Wood, and Marianne Faithfull were regulars at the Rainbow Room, a restaurant on the fifth floor. Barbara Hulanicki, Queen of Fast Fashion |Lizzie Crocker |October 15, 2014 |DAILY BEAST 

Because the marketing of the screening included a rainbow flag and said it was to be the kickoff of “LGBT Awareness Month.” Catholic University’s Harvey Milk Ban Reflects A Church In Transition |Jay Michaelson |October 3, 2014 |DAILY BEAST 

Directly after them came a lot of palace attendants in curious hats and long robes of all colours of the rainbow. Our Little Korean Cousin |H. Lee M. Pike 

The colours of the rainbow shone there in pale tints, and the flaring sunshine could not enter. Alila, Our Little Philippine Cousin |Mary Hazelton Wade 

They glittered and shone with an intensity of colour which surpassed even those of the rainbow. A Woman's Journey Round the World |Ida Pfeiffer 

Nor did the future appear any more in a rainbow glory, since he realised that it would bring renunciation as well as joy. The Life &amp; Letters of Peter Ilich Tchaikovsky |Modeste Tchaikovsky 

As the appearance of the rainbow when it is in a cloud on a rainy day: this was the appearance of the brightness round about. The Bible, Douay-Rheims Version |Various