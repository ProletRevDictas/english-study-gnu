“We’re now beginning to see even vessels that had entered the Mediterranean hang a U-turn,” Lars Jensen, the CEO of Denmark-based SeaIntelligence Consulting, told The Washington Post. Piracy fears mount as ships take long way around Africa to avoid blocked Suez Canal |Sudarsan Raghavan, Antonia Noori Farzan |March 26, 2021 |Washington Post 

The plant is indigenous to the Mediterranean and is part of the same botanical family as carrots and parsley. How to embrace fennel and add its subtle sweetness to your cooking |Aaron Hutcherson |March 26, 2021 |Washington Post 

History’s most celebrated imperial infrastructure remains the sprawling ancient network of Roman roads that bound the far corners of the Mediterranean into a unified empire. China is taking aim at the key to America’s dominant role in the world |Gregory Mitrovich |March 25, 2021 |Washington Post 

Drink this and imagine yourself at a trattoria in Palermo or some small coastal town, breathing in the aromas of the Mediterranean and the wild herbs and scruff of the hillsides. Usher in spring with this delightfully crisp sauvignon blanc that costs just $14 |Dave McIntyre |March 19, 2021 |Washington Post 

He traveled south to the ancient city of Saintes-Maries-de-la-Mer, in the Camargue region on the Mediterranean. An art lover’s Impressionist video trip to Provence and the Riviera |Nancy Nathan |February 5, 2021 |Washington Post 

The city served as a crossroads for African, Mediterranean, and Middle Eastern cultures. Egypt Ain’t The Only Pyramid Show In Town |Nina Strochlic |December 11, 2014 |DAILY BEAST 

“We cannot allow the Mediterranean to become a vast cemetery,” he said. Pope’s Blistering Attack on ‘Haggard’ Europe |Nico Hines |November 26, 2014 |DAILY BEAST 

Europe no longer wants to rescue migrants in the Mediterranean. Britain’s Let-Em-All-Die Policy |Nico Hines, Barbie Latza Nadeau |November 1, 2014 |DAILY BEAST 

Without a dedicated and proactive rescue force, campaigners fear, the death toll in the Mediterranean will skyrocket. Britain’s Let-Em-All-Die Policy |Nico Hines, Barbie Latza Nadeau |November 1, 2014 |DAILY BEAST 

Libyans are by and large charming, charismatic, humorous people with a Mediterranean joie de vivre. It’s Not the USA that Made Libya the Disaster it is Today |Ann Marlowe |August 3, 2014 |DAILY BEAST 

The countries about the eastern part of the Mediterranean Sea and its adjoining waters. Gulliver's Travels |Jonathan Swift 

The view from the sea-side may be somewhat better, but not much—not comparable to that of Genoa from the Mediterranean. Glances at Europe |Horace Greeley 

Africa has one such, though it is north of the centre, and extends to the shores of the Mediterranean and the Atlantic. Outlines of the Earth's History |Nathaniel Southgate Shaler 

In Europe it extended to the Alpine region, but failed to reach the countries bordering on the Mediterranean. Man And His Ancestor |Charles Morris 

And then he went off into a long description of the great Temple of Fortune on the shores of the Mediterranean. The Pit Town Coronet, Volume III (of 3) |Charles James Wills