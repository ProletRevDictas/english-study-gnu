Fill the pot with warm water so the jars are covered by an inch or two, and bring it to a boil. How to can your favorite foods without dying |Rachel Feltman |October 13, 2020 |Popular-Science 

Fill your pot with water until it’s nearly half full, and bring it to a boil. A Family-Friendly Camp Meal That Everyone Can Help With |Wes Siler |October 1, 2020 |Outside Online 

The release says the process should take about two weeks, during which the boil water advisory will remain in place. Brain-eating amoebae are very rare, but warming waters may change that |Kat Eschner |September 29, 2020 |Popular-Science 

Residents of Lake Jackson, where the boy contracted the amoeba after playing at a local water park, are under a boil water advisory. Brain-eating amoebae are very rare, but warming waters may change that |Kat Eschner |September 29, 2020 |Popular-Science 

They are simply a boil on the rectum of humanity and should be treated as such. Tribute to my American heroes |Brody Levesque |September 17, 2020 |Washington Blade 

Bring a large pot of water to a boil and season liberally with salt. Make Carla Hall’s Crispy Shallot Green Bean Casserole |Carla Hall |December 27, 2014 |DAILY BEAST 

In conversation, her ideas emerge at a roiling boil that often takes on a momentum of its own. Daphne Merkin on Lena Dunham, Book Criticism, and Self-Examination |Mindy Farabee |December 26, 2014 |DAILY BEAST 

Bring a large pot of salted water to a boil and add the string beans. The Barefoot Contessa’s Tasty Trip to Paris |Ina Garten |November 27, 2014 |DAILY BEAST 

A curse-filled half hour that saw my blood boil as my filing deadline ticked further into the past. J.K. Rowling Pens the Greatest Horror Story Ever: Dolores Umbridge Was Real |Kevin Fallon |October 31, 2014 |DAILY BEAST 

Businesses are suffering more each day in an area where the rents are extortionate, and the situation could boil over soon. Hong Kong Between Calm and Chaos |Ben Leung |October 3, 2014 |DAILY BEAST 

Still he hung on, drawing himself upward to hook a leg over the very pipe that threatened to boil him alive. Hooded Detective, Volume III No. 2, January, 1942 |Various 

When the Cave-men first learned to boil water, do you think they would think of boiling food? The Later Cave-Men |Katharine Elizabeth Dopp 

Boil the oil and soap together in a pipkin, and then gradually stir in the sand and lemon-juice. The Ladies' Book of Etiquette, and Manual of Politeness |Florence Hartley 

Like a boil, such a horse race as this must burst some day, and it was reaching the acute stage. Scattergood Baines |Clarence Budington Kelland 

Let it boil in the suds for an hour or more, till the lace is clean and white all through. The Ladies' Book of Etiquette, and Manual of Politeness |Florence Hartley