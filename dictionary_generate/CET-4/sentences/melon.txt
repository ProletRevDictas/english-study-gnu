Right when the melons were set to burst from their buds and balloon into juicy orbs, a two-month dry spell hit, and Ochoo’s fledgling watermelons withered. Mixing trees and crops can help both farmers and the climate |Jonathan Lambert |July 14, 2021 |Science News 

How to store peaches, corn, melons and more summer produceCherries should always be stored unwashed in a breathable produce bag in the refrigerator. A guide to stone fruit: How to choose, ripen, store and cook with it |Aaron Hutcherson |July 9, 2021 |Washington Post 

Friday night’s five-course dinner will begin with a melon gazpacho, followed by a roasted Cornish turbot caught by a local fisherman and served alongside locally grown new potatoes, greens and wild garlic pesto. Live updates: G-7 leaders commit to donating 1 billion doses of coronavirus vaccines as summit begins |John Wagner, Colby Itkowitz, Eugene Scott, Felicia Sonmez |June 11, 2021 |Washington Post 

It’s like drinking a bowl of fruit — strawberries, cherries, melon, plus lots of smiles and fun. Keep this Spanish red wine on your counter for easy refills — at the equivalent of under $8 a bottle |Dave McIntyre |April 23, 2021 |Washington Post 

GREAT VALUEThis nice cava lives up to its name, with delicious flavors of red berries and melon, and a fine bead of bubbles. The pandemic bubble may be starting to burst. Celebrate with bubbles all under $20. |Dave McIntyre |April 16, 2021 |Washington Post 

Rhode Island Reds will not eat melon rinds, banana peels, orange skins, pickles, or onions, F.Y.I. What Did TJ Mean By “Pursuit of Happiness,” Anyway? |P. J. O’Rourke |June 8, 2014 |DAILY BEAST 

Do they realize that karela is a bitter melon, popular from China and India to Trinidad and Vietnam? ‘Chopped’: Why I’m Obsessed with Food Network’s Reality Competition Show |Jace Lacob |April 2, 2013 |DAILY BEAST 

The man who wrote about hulking linebackers nibbling melon in the Texas dusk. An Open Letter to Buzz Bissinger |Sean Macaulay |March 27, 2013 |DAILY BEAST 

But how could they bronze that stubby little body, the melon head, the double chin? Richard Ben Cramer Dies: Iconic Writer Had an Unerring Ear for Dialogue |John Avlon |January 8, 2013 |DAILY BEAST 

Rachel “Bunny” Melon wanted John to be the next president so he could “rescue America.” Edwards Staffer Andrew Young Offers Shocking Testimony About His Boss |Diane Dimond |April 25, 2012 |DAILY BEAST 

"Take some melon, Mr. Mudge," said we, as with a sudden bolt we recovered our speech and took another slice ourself. The Book of Anecdotes and Budget of Fun; |Various 

The bread-fruit is somewhat similar in shape to a water-melon, and weighs from four to six pounds. A Woman's Journey Round the World |Ida Pfeiffer 

Neither the pine-apple nor water-melon grow in Teneriffe, but abundance of the latter are brought from Grand Canary. Journal of a Voyage to Brazil |Maria Graham 

We left Parkers melon on his doorstep to chaperon itself, and turned back with him. The Romance of His Life |Mary Cholmondeley 

As we came out we saw across the court that the melon had been taken in, so judged that Parker had returned. The Romance of His Life |Mary Cholmondeley