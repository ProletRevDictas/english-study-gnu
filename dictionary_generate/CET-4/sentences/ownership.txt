We can store the data we get from platforms like Google, and thus have some sort of ownership rights ourselves to it as advertisers. This decade’s most important marketing question: What data rights do advertisers possess? |Kirk Williams |September 17, 2020 |Search Engine Land 

NASA would retain sole ownership of the material upon transfer. NASA will pay for moon rocks excavated by private companies |Neel Patel |September 10, 2020 |MIT Technology Review 

Prices far outpaced inflation, especially in the most competitive markets, putting ownership out of reach for many Americans. Climate change will transform what it means to be a homeowner |Alexandra Ossola |September 8, 2020 |Quartz 

Please note that PCS was sold by its previous ownership in August 2000. Paris Hilton was the original influencer. Now a new documentary lets viewers see past the facade |ehinchliffe |September 7, 2020 |Fortune 

It’s unclear exactly what the costs would be or how Walmart and Microsoft would split ownership. Walmart’s TikTok bid is a risky bet on the direction of US retail |Marc Bain |August 29, 2020 |Quartz 

The NRA objects to doctors asking patients basic questions about gun ownership. The NRA’s Twisted List for Santa |John Feinblatt |December 23, 2014 |DAILY BEAST 

But does this means that animal ownership continues in heaven? Sorry, Internet: Pope Francis Didn't Open Paradise to Pets |Candida Moss |December 14, 2014 |DAILY BEAST 

We fight over their ownership and control, as if reality were a resource as scarce as the water and oil in Mad Max. On Torture, Chuck Johnson & Sondheim |James Poulos |December 13, 2014 |DAILY BEAST 

The rate of violent crime had nearly doubled, so Republicans took ownership of that issue. The GOP and Police Unions: A Love Story |Eleanor Clift |December 12, 2014 |DAILY BEAST 

Yet here we are, fighting over ownership of the idea of freedom. How One Lawsuit Shows What’s Wrong With America |James Poulos |October 20, 2014 |DAILY BEAST 

But after all, land is only one particular case of ownership under the one and the same system. The Unsolved Riddle of Social Justice |Stephen Leacock 

If the finder knows who the owner is or has a reasonable clue to the ownership, which he disregards, he is guilty of larceny. Putnam's Handy Law Book for the Layman |Albert Sidney Bolles 

The private ownership of land is one of the greatest incentives to human effort that the world has ever known. The Unsolved Riddle of Social Justice |Stephen Leacock 

By statute the stock record of ownership is usually made the conclusive test of the right to vote. Putnam's Handy Law Book for the Layman |Albert Sidney Bolles 

Also, ownership of vast limits of growing spruce was necessary to the control of the valley. Scattergood Baines |Clarence Budington Kelland