For his part, Bratton is disappointed but not surprised that the same narrative is already being mapped onto Fry and Spencer. Freaking Out About Age Gaps in Gay Relationships Is Homophobic |Samantha Allen |January 9, 2015 |DAILY BEAST 

So it might be me projecting my desires onto Archer to want to just get away from work for a few weeks. ‘Archer’ Creator Adam Reed Spills Season 6 Secrets, From Surreal Plotlines to Life Post-ISIS |Marlow Stern |January 8, 2015 |DAILY BEAST 

I was sick in street gutters, onto my desk, at dinners with friends. I Tried to Warn You About Sleazy Billionaire Jeffrey Epstein in 2003 |Vicky Ward |January 7, 2015 |DAILY BEAST 

Gang tattoos are still inked onto his face, like scarlet letters. How Good Dads Can Change the World |Gary Barker, PhD, Michael Kaufman |January 6, 2015 |DAILY BEAST 

Where these laser-like missiles are falling out of the sky onto a city and you have to stop each of them from hitting the targets? Coffee Talk with Ethan Hawke: On ‘Boyhood,’ Jennifer Lawrence, and Bill Clinton’s Urinal Exchange |Marlow Stern |December 27, 2014 |DAILY BEAST 

I put down my haid, and was just kinda dragged up the aisle and onto the platform. Alec Lloyd, Cowpuncher |Eleanor Gates 

His untidy hair was rumpled, as if someone had been hanging onto it while in the process of giving him the shiner. Fee of the Frontier |Horace Brown Fyfe 

Either: we cannot give you what you ask, so fall back onto the defensive; or, go ahead, we will give you the means. Gallipoli Diary, Volume I |Ian Hamilton 

But momentum was sufficient to carry Jeff Weedham's roadster out onto the road. Hooded Detective, Volume III No. 2, January, 1942 |Various 

Black Hood let the clutch slap in and the roadster bounded back onto the tarvia drive. Hooded Detective, Volume III No. 2, January, 1942 |Various