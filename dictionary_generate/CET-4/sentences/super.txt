It was a viral meme using the technology that led her to research the possibility—and discover that it was super easy and completely free. Memers are making deepfakes, and things are getting weird |Karen Hao |August 28, 2020 |MIT Technology Review 

Ives believes this year in particular will be what he deems a “super cycle,” with roughly 350 million of the company’s 950 million iPhone users due for an upgrade. As Apple stock tops $500, bulls cite these key reasons it could still go higher |Anne Sraders |August 24, 2020 |Fortune 

It’s super important you understand what’s going on so you can make decisions for the future. Brynne Kennedy could be the first female tech founder to serve in Congress |ehinchliffe |August 24, 2020 |Fortune 

To grasp why beaten-down value stocks look like the place to be in this super-pricey market, just compare the bargain bank stocks to the high-flying S&P 500. Despite Warren Buffett’s selloff, bank stocks look like great buys in this market |Shawn Tully |August 18, 2020 |Fortune 

As value shows it can clear that super-low bar, value stocks will deliver strong gains, just as in past recoveries. The champ’s big comeback: Why beaten-down value stocks are poised to thrive |Shawn Tully |August 18, 2020 |Fortune 

Boys are taught early in life to devalue care, to be hyper-competitive, super-achieving men. How Good Dads Can Change the World |Gary Barker, PhD, Michael Kaufman |January 6, 2015 |DAILY BEAST 

The relationships, and motivations of their chief participants, are as tangled and shady as you expect of the super-rich. The Real-Life ‘Downton’ Millionairesses Who Changed Britain |Tim Teeman |December 31, 2014 |DAILY BEAST 

Further, in the Super Tuesday states of Florida, Texas, and Virginia, Paul is operating at a decided disadvantage. GOP Won’t Forgive Rand for Cop Critique |Lloyd Green |December 23, 2014 |DAILY BEAST 

But Goff called it “insulting” to suggest that he might be running a super PAC for personal gain. ‘Ready for Romney’ Is Amateur Hour |Tim Mak |December 23, 2014 |DAILY BEAST 

Romneyland is abuzz over the new super PAC—and not in a good way. ‘Ready for Romney’ Is Amateur Hour |Tim Mak |December 23, 2014 |DAILY BEAST 

Not only did he provide sub-octave and super-octave couplers freely, but he even added a Swell Sub-quint to Great coupler! The Recent Revolution in Organ Building |George Laing Miller 

This circumstance alone, ought to be strong evidence, even to a skeptic, of its super-human origin. Gospel Philosophy |J. H. Ward 

His eyes were sighting along an instrument of his own devising as if he were aiming some super-gun of a great air cruiser. Astounding Stories, May, 1931 |Various 

Lords not only ought to be gentlefolk, and be fed and waited upon and live in affluent idleness, but super-gentlefolk. Punch, or the London Charivari, Vol. 152, May 16, 1917. |Various 

It was more ferocious than the merely brutal glare of a tiger; it was an intentional malignity, super-beastly and sub-human. Overland |John William De Forest