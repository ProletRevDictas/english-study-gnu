You and your enemy each had 100 soldiers to distribute, any way you liked, to fight at any of the 13 castles. Can You Reach The Summit First? |Zach Wissner-Gross |September 11, 2020 |FiveThirtyEight 

He will stand up to our enemies like Putin and aid our allies. Vote for Biden (duh) |Kevin Naff |September 10, 2020 |Washington Blade 

You and your enemy each have 100 soldiers to distribute, any way you like, to fight at any of the 13 castles. The Fifth Battle For Riddler Nation |Zach Wissner-Gross |September 4, 2020 |FiveThirtyEight 

An app could react more quickly than a human to what an enemy aircraft does, for example. An F-16 pilot took on A.I. in a dogfight. Here’s who won |Aaron Pressman |August 20, 2020 |Fortune 

Rodrigo Duterte of the Philippines even threatened to treat citizens who defied lockdown orders as enemy soldiers and shoot them. Why female leaders are faring better than ‘wartime presidents’ against COVID-19 |matthewheimer |August 20, 2020 |Fortune 

But the enemy of the new emirs is neither the Jew nor the Christian, it is the godless militant defending secularism. Houellebecq’s Incendiary Novel Imagines France With a Muslim President |Pierre Assouline |January 9, 2015 |DAILY BEAST 

In “Steal This Episode,” the filmmaker denounces Homer Simpson as an “enemy of art.” Here’s the Lost Judd Apatow ‘Simpsons’ Episode, Penned by Judd Apatow |Asawin Suebsaeng |January 6, 2015 |DAILY BEAST 

Scott, who died Sunday at 49, could go from evoking a Baptist preacher to quoting Public Enemy. Remembering ESPN’s Sly, Cocky, and Cool Anchor Stuart Scott |Stereo Williams |January 4, 2015 |DAILY BEAST 

“Do not use complaints, courts and lawyers to beat and to silence the enemy,” the NOA activists wrote. Italy’s Terror on the Tracks |Barbie Latza Nadeau |December 28, 2014 |DAILY BEAST 

In the event, the enemy did plenty—far more than SHAEF, or for that matter the German high command, imagined possible. Hitler’s Hail Mary |James A. Warren |December 20, 2014 |DAILY BEAST 

If the "Y" Beach lot press their advantage they may cut off the enemy troops on the toe of the Peninsula. Gallipoli Diary, Volume I |Ian Hamilton 

In this situation we waited the motion of the enemy, without perceiving any advancement they made towards us. The Life and Most Surprising Adventures of Robinson Crusoe, of York, Mariner (1801) |Daniel Defoe 

When a man converses with himself, he is sure that he does not converse with an enemy. The Life and Most Surprising Adventures of Robinson Crusoe, of York, Mariner (1801) |Daniel Defoe 

The enemy were pursued and annoyed by a few hundred of the citizens under Wooster and Arnold; the former was killed. The Every Day Book of History and Chronology |Joel Munsell 

It was he who first said, If thine enemy hunger give him food, if he thirst give him drink. Solomon and Solomonic Literature |Moncure Daniel Conway