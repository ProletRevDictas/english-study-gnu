Next, FiveThirtyEight senior sportswriter Chris Herring joins to talk about what’s ahead for the Houston Rockets now that they’ve crashed out of the NBA playoffs. Reading The Right Amount Into The NFL’s Week 1 |Sarah Shachat |September 15, 2020 |FiveThirtyEight 

Colin Cunliff is a senior policy analyst with the Information Technology and Innovation Foundation. To confront the climate crisis, the US should launch a National Energy Innovation Mission |Amy Nordrum |September 15, 2020 |MIT Technology Review 

Eddy Cue, 55, was named the senior vice president of the unit in 2011. Apple’s leadership evolves ahead of a post-Tim Cook era |radmarya |September 12, 2020 |Fortune 

Jeffrey Sonnenfeld is a senior associate dean and Lester Crown Professor of Management Practice at the Yale School of Management. On COVID-19 vaccines, Big Pharma knows to just say ‘no’ |matthewheimer |September 11, 2020 |Fortune 

For longer-term success, new-country partners will need to train and develop senior business leaders. COVID proves that companies need to reduce their dependence on China |matthewheimer |September 11, 2020 |Fortune 

Stanley Richards, Senior Vice President of the Fortune Society, gave a tour along with a few residents. His First Day Out Of Jail After 40 Years: Adjusting To Life Outside |Justin Rohrlich |January 3, 2015 |DAILY BEAST 

“We look for the qualities that are evocative of V.S.O.P Privilege,” explained Hennessy Senior Vice President Rodney Williams. Streetwear pioneer, visionary entrepreneur, and community mentor Daymond John is honored with Hennessy Privilege Award |Hennessy |January 1, 2015 |DAILY BEAST 

“Clean as a whistle,” says a senior investigator involved in the case. Exclusive: Inside a Cop-Killer’s Final Hours |Michael Daly |December 31, 2014 |DAILY BEAST 

Another senior Air Force official with stealth fighter experience agreed. New U.S. Stealth Jet Can’t Fire Its Gun Until 2019 |Dave Majumdar |December 31, 2014 |DAILY BEAST 

E.J. Graff, senior fellow at the Schuster Institute for Investigative Journalism, is the author of What Is Marriage For? The Real Story Behind the Fight for Marriage Equality |E.J. Graff |December 30, 2014 |DAILY BEAST 

The senior branch of the family being thus extinct the whole of the entailed estate had devolved on me. Uncanny Tales |Various 

“Reduced counsels,” interposed Mr. Weller senior, in an undertone. The Posthumous Papers of the Pickwick Club, v. 2(of 2) |Charles Dickens 

This senior was middle-aged, and passing rich on eighty pounds a year. Fifty Years of Railway Life in England, Scotland and Ireland |Joseph Tatlow 

I remember a senior clerk in the office where I first worked to whom there was a general aversion. Fifty Years of Railway Life in England, Scotland and Ireland |Joseph Tatlow 

He must have had means of his own, as he lived in a way far beyond the reach of even a senior clerk of the first degree. Fifty Years of Railway Life in England, Scotland and Ireland |Joseph Tatlow