The odor of cooking and pipe smoke penetrated the air, and soot from baking bread in the basement damaged books in the Library of Congress, a librarian noted. Troops lodged in the Capitol in 1861. It was a wreck when they left. |Meryl Kornfield, Felicia Sonmez |January 14, 2021 |Washington Post 

She hopes that the books will be cataloged by the end of January, and that she and the other reference librarians can start answering food-related questions and adding to the site shortly thereafter. The Internet’s Most Incredible Collection of Food History Has Been Saved |Dayna Evans |January 12, 2021 |Eater 

Philip Williams, a librarian at a Singapore school, has used games on Roblox, a popular platform for the preteen set, to teach physics concepts. Kids are sick of Zoom too—so their teachers are getting creative |Tanya Basu |December 12, 2020 |MIT Technology Review 

She can describe the real experience of librarians and borrowers who are often overlooked in the copyright debates. As libraries fight for access to e-books, a new copyright champion emerges |Jeff |November 28, 2020 |Fortune 

To help with that task, librarians have compiled a 2020 Election Reading List, with nearly 200 nonfiction and fiction titles for adults, kids, and teens. A reading list for the 2020 presidential election, from the New York Public Library |Rachel King |October 6, 2020 |Fortune 

The mother had left the fields to become a librarian and her love of literature passed on to her three children. For Next AG, Obama Picks a Quiet Fighter With a Heavy Punch |Michael Daly |November 8, 2014 |DAILY BEAST 

Marjorie Wilkes Huntley was a New Age feminist, a widow, and a librarian. Wonder Woman’s Creation Story Is Wilder Than You Could Ever Imagine |Tom Arnold-Forster |November 3, 2014 |DAILY BEAST 

How about the man who created her—a lover of women who lived with a wife, his lover, their children, and a wayward librarian? Wonder Woman’s Creation Story Is Wilder Than You Could Ever Imagine |Tom Arnold-Forster |November 3, 2014 |DAILY BEAST 

On stage, Amber spoofed Sarah Palin in a topknot and librarian glasses, yanking a toy gun and stuffed moose from her skirt. Best Career Arc Ever: From Burlesque To Bartending |Anne Berry |September 13, 2014 |DAILY BEAST 

Aloof and bookish, Pius XI (Achille Ratti) spent years as a Vatican librarian before becoming a diplomat and cardinal. How the Catholic Church Got in Bed with Mussolini |Jason Berry |February 5, 2014 |DAILY BEAST 

Because it's so much pleasanter, in a small library like this, to poke about by one's self—with the help of the librarian. Summer |Edith Wharton 

The Marchesa Baroli, at Turin, provided for his maintenance, by engaging him as her secretary and librarian. My Ten Years' Imprisonment |Silvio Pellico 

The librarian told me I'd never find a copy, and this was on top of a pile of trash in a second-hand shop right here in this town. A Hoosier Chronicle |Meredith Nicholson 

She had been trusted by the librarian to take good care of it. Space Prison |Tom Godwin 

In any case, she may ask the librarian to advise her which biography to read first. The Canadian Girl at Work |Marjory MacMurchy