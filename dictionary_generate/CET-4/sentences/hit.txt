Return to the car and restart it, and the radio is now magically on channel 2, tormenting you with the lamest, er, greatest popular hits of our day. Ford’s electric Mustang Mach-E is an important leap into the future |Dan Carney |February 12, 2021 |Popular-Science 

Still, it’s unlikely that Popeyes’ pescatarian offering will come close to matching its big-clucking-deal hit. Popeyes’ new fish sandwich is a muted sequel to the chicken sandwich blockbuster of 2019 |Emily Heil |February 12, 2021 |Washington Post 

Leonard said the NBA was putting “money over health,” a direct hit on the league’s values. The NBA’s week of controversies show how hard life is outside of the bubble |Ben Golliver |February 12, 2021 |Washington Post 

The Driver twists, slamming the hit man’s head against one wall, then another. What floor? |Sophia Nguyen |February 12, 2021 |Washington Post 

Where Starz has seen more of an impact, however, is when it premieres a new season or spin-off of its hit series “Power.” Future of TV Briefing: Streaming services count on content to keep subscribers acquired in 2020 |Tim Peterson |February 10, 2021 |Digiday 

The most recent issue contains detailed instructions for building car bombs, and the magazine frequently draws up hit-lists. U.S. Spies See Al Qaeda Fingerprints on Paris Massacre |Shane Harris, Nancy A. Youssef |January 8, 2015 |DAILY BEAST 

The big slug happened to hit the suspect in the street, passing through his arm and then striking Police Officer Andrew Dossi. Shot Down During the NYPD Slowdown |Michael Daly |January 7, 2015 |DAILY BEAST 

I was friends with her drummer from Sleater-Kinney, and I met Carrie, and we just hit it off. Coffee Talk with Fred Armisen: On ‘Portlandia,’ Meeting Obama, and Taylor Swift’s Greatness |Marlow Stern |January 7, 2015 |DAILY BEAST 

We hit it off amazingly well, and started a real friendship. The Story Behind Lee Marvin’s Liberty Valance Smile |Robert Ward |January 3, 2015 |DAILY BEAST 

They were the machine gun bullets coming from the ambush when my company got hit. The Story Behind Lee Marvin’s Liberty Valance Smile |Robert Ward |January 3, 2015 |DAILY BEAST 

Many of us had been hit by the balls, but a bruise or a graze of the skin was the worst consequence that had ensued. Blackwood's Edinburgh Magazine, No. CCCXXXIX. January, 1844. Vol. LV. |Various 

He shut his fist and hit Butterface a weak but well intended right-hander on the nose. The Giant of the North |R.M. Ballantyne 

One of the men seemed pretty bad, being hit in the head and in the body. Gallipoli Diary, Volume I |Ian Hamilton 

He could hit upon no plan, and he couldn't muster confidence to turn in. Blackwood's Edinburgh Magazine, No. CCCXXXIX. January, 1844. Vol. LV. |Various 

The Frenchman never withdrew his blade; but his very anxiety to make a hit was defeating itself. The Pit Town Coronet, Volume I (of 3) |Charles James Wills