You can use powerful lasers to measure the distance of these objects, like radar or sonar. How to cast a wider net for tracking space junk |Neel Patel |August 5, 2020 |MIT Technology Review 

This Raman spectroscopy shines laser light on a sample, then measures how the light bounces off. Early dinosaurs may have laid soft-shelled eggs |Jack J. Lee |August 3, 2020 |Science News For Students 

SHERLOC, or Scanning Habitable Environments with Raman and Luminescence for Organics and Chemicals, will take similar measurements using an ultraviolet laser. NASA’s Perseverance rover will seek signs of past life on Mars |Lisa Grossman |July 28, 2020 |Science News 

In one incident, a man is accused of aiming a laser pointer at a police helicopter. Years Into Smart Streetlights Program, Council Will Write Surveillance Rules |Jesse Marx |July 9, 2020 |Voice of San Diego 

Carbon, oxygen, nitrogen and other more complex substances must be available for life to both evolve and build radio transmitters or lasers to send signals through space. Self-destructive civilizations may doom our search for alien intelligence |Tom Siegfried |July 6, 2020 |Science News 

Initially, it will be able to carry 1,000-pound satellite-guided bombs or 500-pound laser-guided weapons. New U.S. Stealth Jet Can’t Fire Its Gun Until 2019 |Dave Majumdar |December 31, 2014 |DAILY BEAST 

Where these laser-like missiles are falling out of the sky onto a city and you have to stop each of them from hitting the targets? Coffee Talk with Ethan Hawke: On ‘Boyhood,’ Jennifer Lawrence, and Bill Clinton’s Urinal Exchange |Marlow Stern |December 27, 2014 |DAILY BEAST 

“The laser-wielding robot is a real threat,” Hetflaisz says. Damien Hirst’s Army of Geppettos |Tim Teeman |December 2, 2014 |DAILY BEAST 

Obama is widely believed to tap an ex-physicist who cuts military waste like a laser to become the next secretary of defense. Ashton Carter, the Wonk Who Would Lead the Pentagon |Shane Harris, Tim Mak |December 2, 2014 |DAILY BEAST 

Green laser pointers shined in the faces of men in uniforms looking down from the roof of the National Palace. Mexican Protesters Look to Start a New Revolution |Jason McGahan |November 21, 2014 |DAILY BEAST 

Then, as though finding an error, he halted its operation and swung the laser-head back away from the work piece. Where I Wasn't Going |Walt Richmond 

Casually, without even looking at the Security man, he had somehow centered the laser directly on him. Where I Wasn't Going |Walt Richmond 

The theory behind the heat projector was simply an extension of the laser theory, plus a few refinements. The Foreign Hand Tie |Gordon Randall Garrett 

Or, I should say, the thing that is supposed to look like a laser component. The Foreign Hand Tie |Gordon Randall Garrett 

It looked like a keychain laser-pointer, or maybe a novelty light-saber. Makers |Cory Doctorow