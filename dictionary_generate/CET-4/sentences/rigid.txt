Without them, it would not have been apparent that something rigid could fly. The science behind how an aircraft glides |By Peter Garrison/Flying Mag |September 3, 2020 |Popular-Science 

A rigid backplate makes it easier to insert the bladder into a backpack’s hydration sleeve, even when the pack is fully loaded—but that does add some weight. Hydration bladders for outdoor adventures |PopSci Commerce Team |September 1, 2020 |Popular-Science 

Hong Kong Secretary for Food and Health Sophia Chan has said they expect 5 million people to participate in the test, but Lam told reporters she has not set a “rigid target” for participation. Hong Kong’s new mass COVID testing scheme is free and voluntary—and some citizens are suspicious |eamonbarrett |August 26, 2020 |Fortune 

Of course, social media is a rigid place for the testing of different messages and campaigns, but multivariate testing tools have the power to deliver fully customized website experiences for traffic arriving from various social sources. How A/B and multivariate testing can skyrocket your social media conversions |Peter Jobes |July 3, 2020 |Search Engine Watch 

Eggs from the earliest dinosaurs were more like leathery turtle eggs than rigid bird eggs. Fossil discoveries suggest the earliest dinosaurs laid soft-shelled eggs |Jack J. Lee |June 24, 2020 |Science News 

Beyond that, how will China evolve its rigid Internet policy? China’s Internet Is Freer Than You Think |Brendon Hong |December 27, 2014 |DAILY BEAST 

Bound together by mutual distrust, both sides end up lashing themselves to the mast of rigid law. Red Tape Is Strangling Good Samaritans |Philip K. Howard |December 27, 2014 |DAILY BEAST 

Doctors are prohibited from doing what a patient needs by rigid practice guidelines. Red Tape Is Strangling Good Samaritans |Philip K. Howard |December 27, 2014 |DAILY BEAST 

The man behind the desk is a fictional character—a ferocious patriot exposing the limits of rigid ideology. The End of Truthiness: Stephen Colbert’s Sublime Finale |Noel Murray |December 19, 2014 |DAILY BEAST 

Or have gender roles in stories become fewer and more rigid? Gail Simone’s Bisexual Catman and the ‘Secret Six’ |Rich Goldstein |December 6, 2014 |DAILY BEAST 

In all business matters he required a rigid economy though never at the expense of efficiency. Fifty Years of Railway Life in England, Scotland and Ireland |Joseph Tatlow 

Before her pencil he sat rigid and unflinching, as he had faced the cannon's mouth in days gone by. The Awakening and Selected Short Stories |Kate Chopin 

Every movement rigidly prescribed, arms held rigid and sharply bent at the elbows. The Man from Time |Frank Belknap Long 

The baggage is then taken to the Custom-house in a steam-launch for examination, which is not unduly rigid. The Philippine Islands |John Foreman 

Then there was a faint pulsation of the rigid limbs, the white, mean face took on a tinge as if the blood were flowing again. The Weight of the Crown |Fred M. White