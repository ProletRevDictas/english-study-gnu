The cub’s mother was on her way, and Dearie had to make tracks, as animal keepers do not directly interact with adult pandas. Baby panda makes debut — online — at National Zoo |Dana Hedgpeth, Justin Wm. Moyer |January 27, 2021 |Washington Post 

The zoo’s red pandas — a group that includes males Rusty, Clinger, Slash and Shredder — seem like a fun bunch. The National Zoo’s wild naming inspiration, from Billy Joel to the Golden Girls |John Kelly |January 25, 2021 |Washington Post 

Also unclear is whether horse poop actually prevents pandas from feeling cold temporarily, or if it just makes feeling cold less unpleasant, Gracheva says. Giant pandas may roll in horse poop to feel warm |Jonathan Lambert |December 7, 2020 |Science News 

All pandas at the zoo move to China when they are 4 years old as part of the zoo’s breeding agreement with China. Baby panda at National Zoo is named Xiao Qi Ji |Dana Hedgpeth |November 23, 2020 |Washington Post 

Rushing up a tree can save a wild panda from attacks by wild dogs. Pandas use their heads as a kind of extra limb for climbing |Susan Milius |March 13, 2020 |Science News For Students 

Triplet panda cubs born this past July were reunited with their mother, Juxiao, in a Chinese zoo this week. ‘Sexual’ Barbershop Quartet, a Panda Family Reunion, and More Viral Videos |The Daily Beast Video |December 14, 2014 |DAILY BEAST 

The grand prize is a pristine white Fiat Panda 4X4 – with full options. Pope Francis Raffles Off His Swag to Help the Poor |Barbie Latza Nadeau |November 18, 2014 |DAILY BEAST 

Vineberg's were said by police to be Panda, Black List, or Red Bull. A Sax Player, Then a Suspect After Philip Seymour Hoffman’s Final Act |Michael Daly |February 7, 2014 |DAILY BEAST 

Panda-Patrice smothers sweet and sour sauce on his lips and grins at the camera. The Most Offensive Lyrics and WTF Moments From ‘Chinese Food’ |Culture Team |October 15, 2013 |DAILY BEAST 

Panda-Patrice hybrid plays board games with five young girls whilst rapping “Get me broccoli/While I play Monopoly.” The Most Offensive Lyrics and WTF Moments From ‘Chinese Food’ |Culture Team |October 15, 2013 |DAILY BEAST 

I will conclude with the following proverb, which comes in very appropriately: En retudi panda nasti abela macha. Carmen |Prosper Merimee 

The Milinda Panda mentions the royal astrologer as one of the principal functionaries of Menander. Appletons' Popular Science Monthly, July 1899 |Various 

Is it that the fates of battle 'gainst the Kuru house combine, Is it that thy heart's affection unto Panda's sons incline? Maha-bharata |Anonymous 

Panda, pan′da, n. a remarkable animal in the bear section of Carnivores found in the south-east Himalayas. Chambers's Twentieth Century Dictionary (part 3 of 4: N-R) |Various 

This is a very curious animal, which, like the panda and the linsang, at first misled naturalists in assigning it a place. Natural History of the Mammalia of India and Ceylon |Robert A. Sterndale