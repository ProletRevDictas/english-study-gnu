Things most meat eaters like to ignore for the ease and inoffensiveness of picking up a pound of plastic-wrapped chicken breasts on the way home from work. Instagram's Most Fascinating Subculture? Women Hunters. |Rachel Levin |September 8, 2020 |Outside Online 

I soon met more than a dozen women who said that male mechanics would grope them, leer at their breasts and make sexual comments or physical insinuations about sex. Temp Workers Fight Back Against Alleged Sexual Harassment and Say They Face Retaliation for Doing So |by Melissa Sanchez |August 28, 2020 |ProPublica 

Three more women have died from a cancer associated with certain kinds of breast implants—all within a six-month period starting just weeks before those implants were recalled. Three more women have died from cancer linked to Allergan’s recalled breast implants, FDA says |Maria Aspan |August 24, 2020 |Fortune 

The team got similar results with several different Brd4 inhibitors and in mice with breast, colon or lung tumors. These cells slow an immune response. Derailing them could help fight tumors |Esther Landhuis |July 10, 2020 |Science News 

Men with this condition can opt for breast reduction surgery or wear a compression vest. Science of ‘Seinfeld’ |LGBTQ-Editor |June 15, 2020 |No Straight News 

He tore a piece of meat off the breast and stroked her coat while she ate. The Stacks: A Chicken Dinner That Mends Your Heart |Pete Dexter |December 7, 2014 |DAILY BEAST 

My girlfriend, Barbara, came to visit me and exposed her breast through the window. The Unbelievable (True) Story of the World’s Most Infamous Hash Smuggler |Marlow Stern |November 14, 2014 |DAILY BEAST 

Hooters is cleverly asking me to “Give a Hoot” about breast cancer. The Misogynistic Companies Jumping On The Breast Cancer Bandwagon |Emily Shire |October 16, 2014 |DAILY BEAST 

It also sells a number of products that contain chemicals that have been linked to breast cancer. The Misogynistic Companies Jumping On The Breast Cancer Bandwagon |Emily Shire |October 16, 2014 |DAILY BEAST 

Sure, Hooters may have a vested financial interests in breasts—or rather, a very specific type of breast. The Misogynistic Companies Jumping On The Breast Cancer Bandwagon |Emily Shire |October 16, 2014 |DAILY BEAST 

"Here's my authority, yuh blasted runt," he yelled, and jerked his six-shooter to a level with the policeman's breast. Raw Gold |Bertrand W. Sinclair 

It is something which takes side in the child's breast with the reasonable governor and the laws which he or she administers. Children's Ways |James Sully 

Alessandro walked at the horses' heads, his face sunk on his breast, his eyes fixed on the ground. Ramona |Helen Hunt Jackson 

We laid him down gently, folded his arms on his breast, and for a moment held our peace in tribute to his passing. Raw Gold |Bertrand W. Sinclair 

The governor placed a dagger to his breast in order to get him to tell what he knew of his wife. The Philippine Islands, 1493-1898, Volume XX, 1621-1624 |Various