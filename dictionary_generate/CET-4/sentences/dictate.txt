Audiophiles often lean toward passive speakers because they allow the users to try out different amplifiers and DACs and to upgrade components as their taste or budget dictates, all while keeping the same speakers. The best bookshelf speakers fill your room with sound, not clutter |Tony Ware |July 19, 2021 |Popular-Science 

The desire for retribution is understandable when someone has taken an innocent life, but ought, in a civilized society, to be constrained by dictates of justice and mercy, even when the criminal himself has acted inhumanely. This is what Derek Chauvin’s sentence should be |Paul Butler |June 24, 2021 |Washington Post 

Moral clarity would dictate that civil-rights and other civic leaders would speak out against such a senseless act of violence. It’s Time to Hold Protesters Accountable |Ron Christie |December 4, 2014 |DAILY BEAST 

Neither trusts the other, yet cultural norms dictate that everyone remain cordial. Heart of Darkness: Into Afghanistan’s Taliban Valley |Matt Trevithick, Daniel Seckman |November 15, 2014 |DAILY BEAST 

So, in short, everyone knows Leung is a mere puppet with zero power and will read out whatever the communists dictate to him. Beijing/Hong Kong: A Tale of Two Cities as Demonstrations Continue |Ben Leung |October 1, 2014 |DAILY BEAST 

Because the federal government really should dictate all that, right? Now Let’s Replace All the Other Big-Spending Eric Cantors |Nick Gillespie |June 11, 2014 |DAILY BEAST 

How could I forget his dictate to always be proud to be a Jew, even in circumstances when it might not seem to ones advantage? A Jewish Ex-Con Recalls Keeping Kosher with the Faithful in Prison |Daniel Genis |May 11, 2014 |DAILY BEAST 

Even the purest selfishness would dictate a policy of social insurance. The Unsolved Riddle of Social Justice |Stephen Leacock 

Do not let scandal or a mere love of gossip dictate a letter of intelligence. The Ladies' Book of Etiquette, and Manual of Politeness |Florence Hartley 

It is not the desire to deceive, but the desire to please, which will dictate such a course. The Ladies' Book of Etiquette, and Manual of Politeness |Florence Hartley 

France would never again send a Barillon to dictate to the cabinet of England. The History of England from the Accession of James II. |Thomas Babington Macaulay 

It was disputed among the Mussulmans whether it was eternal or God had created it in order to dictate it to Mahomet. A Philosophical Dictionary, Volume 1 (of 10) |Franois-Marie Arouet (AKA Voltaire)