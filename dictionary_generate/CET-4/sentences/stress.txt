These fossils didn’t show the big clusters of dark stress bands. Ancient Lystrosaurus tusks may show the oldest signs of a hibernation-like state |Susan Milius |September 16, 2020 |Science News 

In that case, the MRI is a “ghost” of that prior inflammation and stress, she says. College athletes show signs of possible heart injury after COVID-19 |Aimee Cunningham |September 11, 2020 |Science News 

For one, if you’re a competitive athlete or bodybuilder that regularly puts a lot of stress on your muscles, BCAAs may help your body recover faster so you can train more often. BCAA supplements can enhance your workout, but should you take them? |Amy Schellenbaum |September 10, 2020 |Popular-Science 

Inspiring to read about the hard work, ongoing learning, and level of stress necessary for greatness. Book recommendations from Fortune’s 40 under 40 in tech |Rachel King |September 4, 2020 |Fortune 

Ricardo, upon noticing my incessant micromanagement and stress, said, “As a CEO, you need to be the most incompetent person in the room.” The advice that helped this year’s 40 under 40 find their own path |kdunn6 |September 3, 2020 |Fortune 

Obsessive exercising and inadequate nutrition can, over time, put people at high risk for overuse injuries like stress fractures. How Skinny Is Too Skinny? Israel Bans ‘Underweight’ Models |Carrie Arnold |January 8, 2015 |DAILY BEAST 

Yes, cops are under stress and tension (though their jobs are far less dangerous than normally supposed). We Need Our Police to Be Better Than This |Nick Gillespie |December 31, 2014 |DAILY BEAST 

Nor do these studies address the structural and systematic issues that contribute to obesity, such as poverty and stress. Why Your New Year’s Diet Will Fail |Carrie Arnold |December 30, 2014 |DAILY BEAST 

It also means not having to stress about cleaning out your DVR. Four TV Shows We Can’t Wait to Return In 2015 |DISH |December 22, 2014 |DAILY BEAST 

Moreover, trucks, dust, and boomtown stress are the effects of any large-scale industrial activity. New York’s Conservative Fracking Ban |Jay Michaelson |December 20, 2014 |DAILY BEAST 

Feeling himself irresistibly driven by the sudden stress to some kind of action, he sprang to his feet—and screamed! Three More John Silence Stories |Algernon Blackwood 

This description is only imperfect in this point that sufficient stress is not laid on the words fall off. Violins and Violin Makers |Joseph Pearce 

Although many British actors and musicians were participants in this theater, it often suffered from financial stress. Hallowed Heritage: The Life of Virginia |Dorothy M. Torpey 

They looked over the parapet because that method was more sure and quick, and the stress of the battle was great. Gallipoli Diary, Volume I |Ian Hamilton 

It is not necessary to repeat the outlines of his political attitude during the storm and stress of Wallace's memorable struggle. King Robert the Bruce |A. F. Murison