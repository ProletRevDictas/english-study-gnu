Some of it has to be attributed to the ridiculous offensive environment we have seen in the bubble. Monster Games In The NBA Playoffs Aren’t Automatically Translating To Wins |Jared Dubin |August 26, 2020 |FiveThirtyEight 

In my driveway during lunchtime I felt ridiculous swinging a kettlebell. The Gym-Free Pandemic Workout: Kettlebells, Indian Clubs, Sandbags, Oh My! |Eugene Robinson |August 25, 2020 |Ozy 

We’re coming up with such a ridiculous answer that it really forces us to reinvestigate every single assumption that we made along the way to get there. The Physicist Who Slayed Gravity’s Ghosts |Thomas Lewton |August 18, 2020 |Quanta Magazine 

The telltale sign of these problems is that models claiming to predict past elections extremely well often produce inaccurate — or even ridiculous — answers when applied to elections in which the result is unknown ahead of time. How FiveThirtyEight’s 2020 Presidential Forecast Works — And What’s Different Because Of COVID-19 |Nate Silver (nrsilver@fivethirtyeight.com) |August 12, 2020 |FiveThirtyEight 

Shepard said he’s always dismissed allegations of voter fraud through mail voting as ridiculous, based on the validation steps followed by registrars in California, and by San Diego Registrar Michael Vu in particular. GOP Chair Who Called Mail Voting ‘Fraught With Danger’ Voted by Mail 22 Straight Times |Andrew Keatts |August 10, 2020 |Voice of San Diego 

As it turns out, though, cartoon curmudgeons get the best, most ridiculous lines. ‘Downton Abbey’ Review: A Fire, Some Sex, and Sad, Sad Edith |Kevin Fallon |January 5, 2015 |DAILY BEAST 

Pitchfork called him a “a rap-obsessed misfit from a summer camp who freestyles poorly” who is “ridiculous without knowing it.” The Cult of Yung Lean: ‘I’m Building An Anarchistic Society From the Ground Up’ |Marlow Stern |January 4, 2015 |DAILY BEAST 

It was fearless and raunchy and fun and ridiculous and weird and feminist and powerful. Bow Down, Bitches: How Beyoncé Turned an Elevator Brawl Into a Perfect Year |Kevin Fallon |December 31, 2014 |DAILY BEAST 

Marriage is a bond and a commitment—marrying yourself is ridiculous because you are already married to yourself. Why Singles Should Say ‘I Don’t’ to The Self-Marriage Movement |Tim Teeman |December 30, 2014 |DAILY BEAST 

Are the standards for female beauty in Hollywood ridiculous? Renée Zellweger Got a New Face—and Everyone Had An Opinion About It |Kevin O’Keeffe |December 29, 2014 |DAILY BEAST 

This wasn't at all what he meant to say, and it sounded very ridiculous; but somehow the words wouldn't come straight. Davy and The Goblin |Charles E. Carryl 

I do not know how things are in America but in England there has been a ridiculous attempt to suppress Bolshevik propaganda. The Salvaging Of Civilisation |H. G. (Herbert George) Wells 

This was such a ridiculous idea that Davy threw back his head, and laughed long and loud. Davy and The Goblin |Charles E. Carryl 

In case any reader should hastily exclaim, “What a ridiculous question; there can be only one southward!” The Giant of the North |R.M. Ballantyne 

Her directness had made all possible 'buts' seem ridiculous and futile, and had made the expression of curiosity seem offensive. Hilda Lessways |Arnold Bennett