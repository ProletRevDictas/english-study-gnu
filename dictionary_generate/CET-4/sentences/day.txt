Biden, who has held public events less regularly, has been seen wearing a mask on 28, including on all but five days this month. In 160 words, Trump reveals how little he cares about the pandemic |Philip Bump |September 17, 2020 |Washington Post 

The rules are defined day by day by people with subjective points of view. Christian Puglisi Is Closing His Influential Copenhagen Restaurants. COVID Is Only Partly to Blame |Rafael Tonon |September 17, 2020 |Eater 

When you have 15 people, and the 15 within a couple of days is going to be down to close to zero. Timeline: The 124 times Trump has downplayed the coronavirus threat |Aaron Blake, JM Rieger |September 17, 2020 |Washington Post 

Earlier in the day, Redfield had said wearing a mask was more effective than a vaccine. Trump contradicts CDC director on vaccine; Biden says Americans shouldn’t trust Trump |Colby Itkowitz, Felicia Sonmez, John Wagner |September 16, 2020 |Washington Post 

It’s light enough to wear in the middle of the day here in the muggy South, and dries fast enough that I usually keep it on while I go overboard for a dip. The Gear That Lets Me Enjoy the Last Days of Summer |Graham Averill |September 15, 2020 |Outside Online 

He added: “People say he deserves his day in court… Do we have enough time?” Bill Maher: Hundreds of Millions of Muslims Support Attack on ‘Charlie Hebdo’ |Lloyd Grove |January 8, 2015 |DAILY BEAST 

For many years afterward it was a never-ending topic of conversation, and is more or less talked of even to this day. New York’s Most Tragic Ghost Loves Minimalist Swedish Fashion |Nina Strochlic |January 8, 2015 |DAILY BEAST 

“We talked about the science the whole time the other day,” Krauss told The Daily Beast in a phone interview. Sleazy Billionaire’s Double Life Featured Beach Parties With Stephen Hawking |M.L. Nestel |January 8, 2015 |DAILY BEAST 

Gunshots rang out in Paris this morning on a second day of deadly violence that has stunned the French capital. France Mourns—and Hunts |Nico Hines, Christopher Dickey |January 8, 2015 |DAILY BEAST 

In the middle of all of that past suffering and present-day conflict, this Cosby bomb was dropped. Phylicia Rashad and the Cult of Cosby Truthers |Stereo Williams |January 8, 2015 |DAILY BEAST 

The afternoon was a lovely one—the day was a perfect example of the mellowest mood of autumn. Confidence |Henry James 

Edna did not reveal so much as all this to Madame Ratignolle that summer day when they sat with faces turned to the sea. The Awakening and Selected Short Stories |Kate Chopin 

Each day she resolved, "To-morrow I will tell Felipe;" and when to-morrow came, she put it off again. Ramona |Helen Hunt Jackson 

There are three things a wise man will not trust: the wind, the sunshine of an April day, and woman's plighted faith. Pearls of Thought |Maturin M. Ballou 

The proceedings of the day commenced with divine service, performed by Unitarian and Baptist ministers. Blackwood's Edinburgh Magazine, Volume 60, No. 372, October 1846 |Various