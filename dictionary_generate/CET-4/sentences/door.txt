No one rushes us out the door, but I’m aware the clock is ticking as I push away from the table and prepare to leave what’s been a delightful evening out. Now that’s room service: What it’s like to check into a hotel just for dinner |Tom Sietsema |February 12, 2021 |Washington Post 

Although social media allows us to easily unfriend or unfollow someone, closing the door “on a human relationship that has had some closeness at one point in time” isn’t as simple in real life, Boss said. Politics and conspiracy theories are fracturing relationships. Here’s how to grieve those broken bonds. |Jeff Schrum |February 11, 2021 |Washington Post 

Thanks to some specific, well-used gear, I usually make it out the door. 5 Pieces of Gear That Help Me Stay Active During Winter |Jakob Schiller |February 11, 2021 |Outside Online 

One was arrested after he allegedly pulled his sister’s door off its hinges so he could attack her. Capitol rioters searched for Nancy Pelosi in a way that should make every woman’s skin crawl |Monica Hesse |February 11, 2021 |Washington Post 

So we had to come to this understanding and agreement that if dance can get a foot in the door with a style the IOC wants, then maybe the other dances aren’t as far behind as we feared. How break dancing made the leap from ’80s pop culture to the Olympic stage |Rick Maese |February 9, 2021 |Washington Post 

A guard is manning the door, which is always kept ajar so she can be monitored. Should Teens Have The Right To Die? |Brandy Zadrozny |January 8, 2015 |DAILY BEAST 

The kid from next door drops by and Marvin talks to him about the stunts in his latest film, Death Hunt. The Story Behind Lee Marvin’s Liberty Valance Smile |Robert Ward |January 3, 2015 |DAILY BEAST 

They eye the door anxiously, convinced that at any moment, a Pakistani or Iranian intelligence officer will come barging in. The Dangerous Drug-Funded Secret War Between Iran and Pakistan |Umar Farooq |December 29, 2014 |DAILY BEAST 

Gurley came out of stairwell door as the two cops approached. Protesters Demand Justice For Gurley As Gap Grows Between Cops and NYC |M.L. Nestel |December 28, 2014 |DAILY BEAST 

This courageous act earned him a late-night knock on the door with orders for Serna to vamos from Cuba. Obama’s One Hand Clap With Castro |Doug McIntyre |December 24, 2014 |DAILY BEAST 

Before Ripperda could unclasp his lips to reply, the stranger had opened the door, and passed through it like a gliding shadow. The Pastor's Fire-side Vol. 3 of 4 |Jane Porter 

She looked so sweet when she said it, standing and smiling there in the middle of the floor, the door-way making a frame for her. Music-Study in Germany |Amy Fay 

She walked away toward another door, which was masked with a curtain that she lifted. Confidence |Henry James 

Hilda, trembling at the door, more than half expected Mr. Orgreave to say: "You mean, she's invited herself." Hilda Lessways |Arnold Bennett 

Then the door opened, the portiere was swept aside, and Anselme announced "Monsieur de Garnache." St. Martin's Summer |Rafael Sabatini