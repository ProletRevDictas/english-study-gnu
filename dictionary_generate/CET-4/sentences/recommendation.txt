They’re binging videos on TikTok and YouTube pushed to them by recommendation systems that end up shaping their worldviews. Why kids need special protection from AI’s influence |Karen Hao |September 17, 2020 |MIT Technology Review 

The Centers for Disease Control and Preventions has also compiled a list of coronavirus-related recommendations for election officials, poll workers, and voters, but you can ultimately only control what you do. Why you should vote as early as possible (and how to do it) |John Kennedy |September 17, 2020 |Popular-Science 

Declarations, manifestos, and recommendations are flooding the internet. AI ethics groups are repeating one of society’s classic mistakes |Amy Nordrum |September 14, 2020 |MIT Technology Review 

Here are a selection of book recommendations from this year’s 40 Under 40 in government and politics. Book recommendations from Fortune’s 40 under 40 in government and policy |Rachel King |September 10, 2020 |Fortune 

Here is a selection of book recommendations from this year’s 40 Under 40 in health. Book recommendations from Fortune’s 40 under 40 in health |Rachel King |September 9, 2020 |Fortune 

One of his kinder letters of recommendation warned that his scholarship was “open to the charge of sensationalism.” Wonder Woman’s Creation Story Is Wilder Than You Could Ever Imagine |Tom Arnold-Forster |November 3, 2014 |DAILY BEAST 

The only recommendation I make is that they do it on a big training day. When Is It OK to Cheat? The Pros and Cons of Cheat Days |DailyBurn |July 14, 2014 |DAILY BEAST 

Sadly, that is a recommendation that Russian journalists pursue at risk of death. Meet the Censors, Propagandists and Outright Liars Who Won Putin’s Pulitzers |James Kirchick |May 5, 2014 |DAILY BEAST 

The president, according to a White House statement last week, has decided to accept our recommendation. The NSA Shouldn’t Stockpile Web Glitches |Richard Clarke, Peter Swire |April 18, 2014 |DAILY BEAST 

But there was one fact about him that makes the recommendation remarkable: he was Jewish. The Jews Who Fought for Hitler: ‘We Did Not Help the Germans. We Had a Common Enemy’ |The Telegraph |March 10, 2014 |DAILY BEAST 

Marry, let an honest man lack their assistance, and starving stares him in the face, for want of a recommendation. The Battle of Hexham; |George Colman 

All such vows are widely different from those restraints which have no higher recommendation than human authority. The Ordinance of Covenanting |John Cunningham 

Letters of Recommendation should be truthful, polite, and carefully considered. The Ladies' Book of Etiquette, and Manual of Politeness |Florence Hartley 

I had brought no letters of recommendation to this amiable woman, but she happened to hear of my travels and paid me a visit. A Woman's Journey Round the World |Ida Pfeiffer 

At the fall of the Empire he lost his position, obtaining his new one on the recommendation of the Comte de Serizy. Repertory Of The Comedie Humaine, Complete, A -- Z |Anatole Cerfberr and Jules Franois Christophe