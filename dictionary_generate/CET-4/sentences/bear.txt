While Neumann’s job meant she was involved, Troye was consistently bearing witness not just early in the outbreak, but through its resurgence this summer, when virtually every other First World country had things much more under control. A devastating picture of Trump’s coronavirus response — from a firsthand witness |Aaron Blake |September 17, 2020 |Washington Post 

As wildfires bear down on the West Coast, many have lost their homes or had to evacuate. California wildfires may give way to massive mudslides |Ula Chrobak |September 17, 2020 |Popular-Science 

Bernstein notes that while further study is required, there is evidence that air pollution affects birth outcomes—babies are more like to be born pre-term or at low birth weights—and development. Why fighting climate change is key to America’s health |Erika Fry |September 16, 2020 |Fortune 

Since they’re born in freshwater streams but then migrate to the sea to mature, salmon serve as a link between saltwater and freshwater ecosystems, bringing nutrients from the oceans inland and vice versa. This Startup Is Growing Sushi-Grade Salmon From Cells in a Lab |Vanessa Bates Ramirez |September 16, 2020 |Singularity Hub 

He was born in Scranton and grew up and still resides in Wilmington, a short drive down Interstate 95 from Philadelphia. Trump, in town hall, says he wouldn’t have done anything differently on pandemic |Colby Itkowitz, Josh Dawsey, Felicia Sonmez, John Wagner |September 16, 2020 |Washington Post 

“If Charleston harbor needs improvement, let the commerce of Charleston bear the burden,” he said. Steve Scalise Shows There’s a Fine Line Between Confederate & Southern |Lloyd Green |January 2, 2015 |DAILY BEAST 

He said,  “I am breaking my heart over this story, and cannot bear to finish it.” How Dickens and Scrooge Saved Christmas |Clive Irving |December 22, 2014 |DAILY BEAST 

Once again he accused the West of being unfair to Russia, bringing back his favorite metaphor, the Russian bear. After His Disastrous Annual Press Conference, Putin Needs A Hug |Anna Nemtsova |December 18, 2014 |DAILY BEAST 

Maybe our dear bear should sit quietly, not chase piglets and just eat berries and honey. After His Disastrous Annual Press Conference, Putin Needs A Hug |Anna Nemtsova |December 18, 2014 |DAILY BEAST 

Putin suggested that all the West wanted was to turn the Russian bear into “taxidermy.” After His Disastrous Annual Press Conference, Putin Needs A Hug |Anna Nemtsova |December 18, 2014 |DAILY BEAST 

Many British Ferns evidence a marked tendency to “sport,” and this is a fact which the beginner should always bear in mind. How to Know the Ferns |S. Leonard Bastin 

The bear laughed and joined his companion, and the torpedo thundered away. The Joyous Adventures of Aristide Pujol |William J. Locke 

This may be done by taking the humming tone and bringing to bear upon it a strong pressure of energy. Expressive Voice Culture |Jessie Eldridge Southwick 

The left heel followed like lightning, and the right paw also slipped, letting the bear again fall heavily on the ice below. The Giant of the North |R.M. Ballantyne 

The bear watched him narrowly with its wicked little eyes, though it did not see fit to cease its paw-licking. The Giant of the North |R.M. Ballantyne