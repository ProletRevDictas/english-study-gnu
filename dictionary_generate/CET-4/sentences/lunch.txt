Yu acknowledged that there is a risk of a “backlog” in the kitchen if everyone wants their lunch at the same time, but he said KitchenMate tries to alleviate this issue by allowing people to pre-order their meals in the app. KitchenMate makes it easy to cook fresh meals at work |Anthony Ha |August 27, 2020 |TechCrunch 

The Rotisserie League was named after the restaurant where the founders often ate lunch. The Economics of Sports Gambling (Ep. 388 Rebroadcast) |Stephen J. Dubner |August 20, 2020 |Freakonomics 

Incarcerated military veterans enter the dining hall for lunch at the Cybulski Rehabilitation Center in Enfield, Connecticut. The Hidden Costs of War: Vet Crimes |Nick Fouriezos |August 19, 2020 |Ozy 

It’s kind of folklore and the sort of thing you learn over a lunch table discussion around the common room. New Geometric Perspective Cracks Old Problem About Rectangles |Kevin Hartnett |June 25, 2020 |Quanta Magazine 

A 63-year-old woman and her family walked into a crowded restaurant for lunch, after returning from Wuhan the day before. COVID-19 case clusters offer lessons and warnings for reopening |Helen Thompson |June 18, 2020 |Science News 

According to the USDA, student participation began to fall, with 1.4 million students opting out of the lunch program entirely. The Republican War on Kale |Patricia Murphy |January 7, 2015 |DAILY BEAST 

Two Indonesian airlines, Garuda and Lion Air, have seen Fernandes eat their lunch and are only now responding. Annoying Airport Delays Might Prevent You From Becoming the Next AirAsia 8501 |Clive Irving |January 6, 2015 |DAILY BEAST 

Twenty-one-and-a-half million students participate in free or reduced-price school lunch programs. Congress’ Gift That Keeps on Giving |P. J. O’Rourke |December 20, 2014 |DAILY BEAST 

On one summer lunch hour, Donna Ann Levonuk, 50, lifted a tub of diaper cream priced at $43.98—and then stashed it in her purse. The Insane $11 Billion Scam at Retailers’ Return Desks |M.L. Nestel |December 19, 2014 |DAILY BEAST 

I'm to be at his Universal bungalow at twelve-thirty for lunch, to meet him for the first time, going to see a man about a job. Alfred Hitchcock’s Fade to Black: The Great Director’s Final Days |David Freeman |December 13, 2014 |DAILY BEAST 

HE ordered a lunch which he thought the girl would like, with wine to revive the faculties that he knew must be failing. Rosemary in Search of a Father |C. N. Williamson 

I often recall the farewell lunch we had together at the Restaurant de Paris, in the Escolta. The Philippine Islands |John Foreman 

He paid for the lunch, and tipped the waiters so liberally that they all hoped he would come again often. Rosemary in Search of a Father |C. N. Williamson 

I'd much rather see what is going on than be cooped up below, and after lunch I told Bob I was going up on deck. Uncanny Tales |Various 

At lunch he was the greatest possible fun, bubbling over with jokes and witty sallies. Gallipoli Diary, Volume I |Ian Hamilton