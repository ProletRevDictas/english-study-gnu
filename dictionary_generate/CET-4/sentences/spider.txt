What the redoubled interest in leather goods means for the alternative spider silk that was the company’s original product is unclear. Bolt Threads partners with Adidas, owners of Balenciaga and Gucci, and Stella McCartney on mushroom leather |Jonathan Shieber |October 2, 2020 |TechCrunch 

The major complicating factor here is that spiders are everywhere. Your car is probably full of spiders |PopSci Staff |September 2, 2020 |Popular-Science 

You don’t realize how many spiders are in and around your house right now. Your car is probably full of spiders |PopSci Staff |September 2, 2020 |Popular-Science 

In building those homes, larvaceans remind Katija a bit of spiders. Larvaceans’ underwater ‘snot palaces’ boast elaborate plumbing |Susan Milius |June 15, 2020 |Science News 

She’s a spider expert in Norfolk, England where she works with the British Arachnological Society. Why you’re spotting more wildlife during COVID-19 |Bethany Brookshire |June 8, 2020 |Science News For Students 

Another side of Spider-Man that might be interesting to explore in a reboot is seeing him as an adult. Exclusive: Sony Hack Reveals Studio's Detailed Plans For Another ‘Spider-Man’ Reboot |William Boot |December 13, 2014 |DAILY BEAST 

He then dons a Spider-Man costume and savagely starts attacking criminals. Exclusive: Sony Hack Reveals Studio's Detailed Plans For Another ‘Spider-Man’ Reboot |William Boot |December 13, 2014 |DAILY BEAST 

Then, under the bold headline “Rebooting Spider-Man,” Robinov describes a broad vision for the future of the franchise. Exclusive: Sony Hack Reveals Studio's Detailed Plans For Another ‘Spider-Man’ Reboot |William Boot |December 13, 2014 |DAILY BEAST 

Another angle Robinov suggests as a possibility for Peter Parker/Spider-Man is a franchise reboot tackling Spidey as… an adult. Exclusive: Sony Hack Reveals Studio's Detailed Plans For Another ‘Spider-Man’ Reboot |William Boot |December 13, 2014 |DAILY BEAST 

In it, Kraven the Hunter tracks down Spider-Man, shoots him repeatedly, and leaves him for dead, buried underground. Exclusive: Sony Hack Reveals Studio's Detailed Plans For Another ‘Spider-Man’ Reboot |William Boot |December 13, 2014 |DAILY BEAST 

There were two hard formal-looking couches, with straight backs and spider legs. The Pit Town Coronet, Volume I (of 3) |Charles James Wills 

Suddenly the spaniel leapt up with that feverish, spider-like activity of the toy species and began to bark. Dope |Sax Rohmer 

One of the most interesting of the spider race is the “trap-door” spider which inhabits warm countries all over the earth. The Wonder Book of Knowledge |Various 

The touch of the soft fabric reassured him: it was as soft as though woven of spider's web, and strong as fibres of steel. Astounding Stories, May, 1931 |Various 

The tarantula, like many other members of the spider family, is an expert in the making of burrows. The Wonder Book of Knowledge |Various