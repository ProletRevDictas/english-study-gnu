Digging back into these old cookbooks is an opportunity to eschew the fatphobic, classist expectations that govern what is “good” and “bad” food and just eat the damn fake cheese. The Joy of Cooking Other People’s ‘Secret Family Recipes’ |Amy McCarthy |September 11, 2020 |Eater 

Every public-health measure to limit the spread of a virus “is like a layer of Swiss cheese,” Blaisdell says. Four summer camps show how to limit COVID-19 outbreak |Erin Garcia de Jesus |September 8, 2020 |Science News For Students 

It’s the stacking of “multiple layers of cheese on top of each other that close those holes,” she says. Four summer camps show how to limit COVID-19 outbreak |Erin Garcia de Jesus |September 8, 2020 |Science News For Students 

We want to end your pain and suffering by offering you some options for precise pizza cutters that can cut through even the thickest layer of cheese. Pizza cutters that will get you the slice of your dreams |PopSci Commerce Team |September 2, 2020 |Popular-Science 

“Every public health layer is like a layer of Swiss cheese with a hole in it,” Blaisdell says. How four summer camps in Maine prevented COVID-19 outbreaks |Erin Garcia de Jesus |August 28, 2020 |Science News 

Mixing meat and dairy is a kosher rule-breaker, so they switched the cheese for potatoes. I Ate Potato Pancakes Til I Plotzed |Emily Shire |December 17, 2014 |DAILY BEAST 

Before serving, bake the cheese packages, combine the salad and vinaigrette, and serve. The Barefoot Contessa’s Tasty Trip to Paris |Ina Garten |November 27, 2014 |DAILY BEAST 

Bake the goat cheese packages for 15 to 20 minutes, until golden brown. The Barefoot Contessa’s Tasty Trip to Paris |Ina Garten |November 27, 2014 |DAILY BEAST 

A French green bean salad with warm goat cheese reminds Ina Garten of having lunch in Paris. The Barefoot Contessa’s Tasty Trip to Paris |Ina Garten |November 27, 2014 |DAILY BEAST 

I remember them coming over all adorable with mac and cheese, collard greens, fried chicken. All Eyes on Anjelica Huston: The Legendary Actress on Love, Abuse, and Jack Nicholson |Alex Suskind |November 10, 2014 |DAILY BEAST 

Aristide lived on bread and cheese, and foresaw the time when cheese would be a sinful luxury. The Joyous Adventures of Aristide Pujol |William J. Locke 

A bailee received some cheese and gave a receipt slating that it was to be kept at the owner's risk of loss from water. Putnam's Handy Law Book for the Layman |Albert Sidney Bolles 

Coals!what would he do with coals?Toast his cheese with em, and then come back for more. Oliver Twist, Vol. II (of 3) |Charles Dickens 

The brown loaf was cut by a very excited little hostess into five thick squares; the cheese into four. The Box-Car Children |Gertrude Chandler Warner 

Between the pastry and the dessert, have salad and cheese placed before each guest. The Ladies' Book of Etiquette, and Manual of Politeness |Florence Hartley