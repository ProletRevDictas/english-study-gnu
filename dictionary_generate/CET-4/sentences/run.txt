McSally ran for Senate in 2018 but lost to Democrat Kyrsten Sinema. Election live updates: Trump returns to Wisconsin; Biden to face live audience at town hall |Colby Itkowitz, Felicia Sonmez, John Wagner |September 17, 2020 |Washington Post 

He unsuccessfully ran for the Republican presidential nomination during the 2016 election cycle. Former Wisconsin governor Scott Walker is working with Pence on debate preparations, people familiar with sessions say |Robert Costa |September 17, 2020 |Washington Post 

New York food impresario Tom Colicchio got into all of our living rooms with his 17-season run as the head judge on Top Chef. Tom Colicchio Hopes (and Fears) COVID-19 Will Change the Restaurant Industry |Pallabi Munsi |September 16, 2020 |Ozy 

While Fraser is not the first woman to run one of the country’s top 20 banks—KeyCorp’s Beth Mooney, who retired in May, broke that ceiling—she is the first to run one of Citigroup’s size. ‘It has to have an impact’: What Citi’s new CEO means for other women on Wall Street |Maria Aspan |September 16, 2020 |Fortune 

With a massive influx of Israeli tourism expected now that commercial flights are running, many are expecting the demand for kosher food to grow. The Newest Fusion Cuisine: Kosherati |Fiona Zublin |September 16, 2020 |Ozy 

As this list shows, punishments typically run to a short-ish jail sentence and/or a moderately hefty fine. In Defense of Blasphemy |Michael Tomasky |January 9, 2015 |DAILY BEAST 

Everybody is trapped in an elevator together and tempers run a little hot. ‘Archer’ Creator Adam Reed Spills Season 6 Secrets, From Surreal Plotlines to Life Post-ISIS |Marlow Stern |January 8, 2015 |DAILY BEAST 

Using standard methods, the cost of printing DNA could run upwards of a billion dollars or more, depending on the strand. Design Your Own Dinosaur: The Era of Custom DNA |Justin Jones |January 8, 2015 |DAILY BEAST 

Should lightning strike and Hillary Clinton forgoes a presidential run, Democrats have a nominee in waiting. Sen. Warren’s Main Street Crusade to Pressure Clinton |Eleanor Clift |January 8, 2015 |DAILY BEAST 

The decision not to run the cartoons is motivated by nothing more than fear: either fear of offending or fear of retaliation. Why We Stand With Charlie Hebdo—And You Should Too |John Avlon |January 8, 2015 |DAILY BEAST 

Do not the widow's tears run down the cheek, and her cry against him that causeth them to fall? The Bible, Douay-Rheims Version |Various 

A few, very few, little dots had run back over that green patch—the others had passed down into the world of darkness. Gallipoli Diary, Volume I |Ian Hamilton 

But if what I told him were true, he was still at a loss how a kingdom could run out of its estate like a private person. Gulliver's Travels |Jonathan Swift 

The controlling leaders being out of gear the machine did not run smoothly: there was nothing but friction and tension. Napoleon's Marshals |R. P. Dunn-Pattison 

When these last words of his were interpreted to her, she started, made as if she would run after him, but checked herself. Ramona |Helen Hunt Jackson