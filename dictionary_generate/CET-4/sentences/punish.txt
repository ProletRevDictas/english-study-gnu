Silver said in December that he hoped that players would stand for the anthem, but that he wouldn’t punish players who protested. The NBA’s week of controversies show how hard life is outside of the bubble |Ben Golliver |February 12, 2021 |Washington Post 

Students will not be punished for using single-use plastics on campus, but school officials plan to offer enough alternatives so that students will not need to rely on plastics, Chapple said. George Washington University commits to single-use-plastic ban |Lauren Lumpkin |February 11, 2021 |Washington Post 

To his credit, when he was in a position to financially punish me for saying no, he did not. Outing a Celeb Sex Pest and Mourning Larry Flynt … Sorta |Eugene Robinson |February 11, 2021 |Ozy 

Those videos and other stuff happened before any of us were on the team, and it feels like we’re being punished. Former cheerleaders settle with Washington Football Team as program’s future is in doubt |Beth Reinhard |February 10, 2021 |Washington Post 

Each practice session was planned to the minute, and players were punished if they were late. Marty Schottenheimer, one of the NFL’s winningest coaches, dies at 77 |Matt Schudel |February 9, 2021 |Washington Post 

Instead, it appears that the Obama administration has opted to punish North Korea financially. U.S. Spies Say They Tracked ‘Sony Hackers’ For Years |Shane Harris |January 2, 2015 |DAILY BEAST 

And the law can easily be used as a political tool to punish any disrespect of the state. Disco Mullah Blasphemy Row Highlights Pakistan’s Hypocrisy |Shaheen Pasha |December 21, 2014 |DAILY BEAST 

Did North Korea hack Sony to punish them for a Seth Rogen movie that taunts Kim Jong-un? Sony Hack: A Dictator Move? |Kevin Bleyer |December 14, 2014 |DAILY BEAST 

Asked, if Christie is so terrible, why he would want to punish the people of the Garden State with his presence, Tancredo laughed. The ‘Stop Chris Christie’ Movement Begins. Good Luck With That. |Olivia Nuzzi |November 8, 2014 |DAILY BEAST 

And is it right for us to withhold assistance and punish civilians? U.S. Humanitarian Aid Going to ISIS |Jamie Dettmer |October 20, 2014 |DAILY BEAST 

To punish the habit, a Turk was seized and a pipe transfixed through his nose. Tobacco; Its History, Varieties, Culture, Manufacture and Commerce |E. R. Billings. 

The children of Israel, having been sent out by Jahweh to punish the Midianites, "slew all the males." God and my Neighbour |Robert Blatchford 

The apprehension that God will punish for not making fulfilment to him accompanies equally the oath and the vow. The Ordinance of Covenanting |John Cunningham 

And to punish himself while this reaction lasted, he would seek her out and see that she inflicted the punishment itself. The Wave |Algernon Blackwood 

Oh, madame, I tell you you do but waste time, and you punish me and harass yourself to little purpose. St. Martin's Summer |Rafael Sabatini