The journey through a magical and illogical land to rescue two princesses is sort of a 20th-century Pilgrim’s Progress — and may help you find your purpose too. This Weekend: This Is How You Lounge in Style |Joshua Eferighe |October 10, 2020 |Ozy 

This update on the classic game of capitalism stars everyone’s favorite princess-rescuing plumber, Mario. Classic board games that make great gifts |PopSci Commerce Team |September 30, 2020 |Popular-Science 

She must be a princess, they decide, and that is how they choose to raise her. 7 daring movie adaptations of literary classics |Allegra Frank |August 28, 2020 |Vox 

If a show didn’t happen to have a princess in it, there probably weren’t many females. Does Hollywood Still Have a Princess Problem? (Ep. 394) |Stephen J. Dubner |October 24, 2019 |Freakonomics 

That was the movie that led her, as a teenager, to start rethinking how she used to think about princesses. Does Hollywood Still Have a Princess Problem? (Ep. 394) |Stephen J. Dubner |October 24, 2019 |Freakonomics 

And then that chorus kicks in, and the young lady formerly known as Lizzy Grant transforms into the princess of darkness. The 14 Best Songs of 2014: Bobby Shmurda, Future Islands, Drake, and More |Marlow Stern |December 31, 2014 |DAILY BEAST 

Were it not for them, nobody would have even built the brand new arena where Sarah saw a PRINCESS! Synagogue Slay: When Cops Have to Kill |Michael Daly |December 10, 2014 |DAILY BEAST 

Then he read the golden inscription and saw that she was a princess. In New Brothers Grimm 'Snow White', The Prince Doesn't Save Her |The Brothers Grimm |November 30, 2014 |DAILY BEAST 

Both heroines are women, but they offer a pretty bizarre dichotomy for girls: Ice queen or ditzy princess. Sexism Begins in the Toy Aisle |Nancy Kaffer |November 29, 2014 |DAILY BEAST 

Princess Ariel and Prince Eric walk down the aisle, and are greeted by a stout clergyman who is allegedly too happy to see them. When the Religious Right Attacked ‘The Little Mermaid’ |Asawin Suebsaeng |November 20, 2014 |DAILY BEAST 

The Princess still kept her eyes fixed on Louis, while, in a suppressed and unsteady voice, she answered her governess. The Pastor's Fire-side Vol. 3 of 4 |Jane Porter 

But first he held a whispered colloquy with the Princess, whom he entreated, or persuaded, to re-enter her gorgeous vehicle. The Red Year |Louis Tracy 

Louis listened with pleasure, and dwelt with delight on the interesting Princess and her son. The Pastor's Fire-side Vol. 3 of 4 |Jane Porter 

The Princess was pale and thin; and, though dressed superbly, seemed fitter for her chamber. The Pastor's Fire-side Vol. 3 of 4 |Jane Porter 

Besides, I've read of such things in the Princess Novelettes; only there it's most generally lovers, not 'usbins, nor yet fathers. Rosemary in Search of a Father |C. N. Williamson