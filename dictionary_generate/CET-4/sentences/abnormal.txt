In a phone interview with The Washington Post, she explained why the decisions made in Texas about possible weather extremes were far from abnormal. Texas is a reminder: We’re much more willing to pay for cures than prevention |Philip Bump |February 18, 2021 |Washington Post 

When asexual people are treated as “abnormal” by doctors or therapists, it does them a disservice. Single On Valentine’s Day And Happily So |LGBTQ-Editor |February 13, 2021 |No Straight News 

Recent research suggests the body’s waste removal process relies on sleep to get rid of harmful proteins from the brain, particularly abnormal variants of amyloid. Why Sleep Experts Say It’s Time To Ditch Daylight Saving Time |LGBTQ-Editor |October 29, 2020 |No Straight News 

Within hours, they learned there were 27 cases — seven of them severe — with fever, difficulty breathing and a buildup of abnormal substances in the lungs. Inside the Fall of the CDC |by James Bandler, Patricia Callahan, Sebastian Rotella and Kirsten Berg |October 15, 2020 |ProPublica 

It not only increases the spread of misinformation, … it also groups people together so that their incorrect ideas don’t seem as abnormal as they otherwise would. Empathy is an underrated weapon in fighting vaccine skepticism |jakemeth |October 13, 2020 |Fortune 

The problem, though, is that this advice presumes that death threats are rare and abnormal. Cover-Ups and Concern Trolls: Actually, It's About Ethics in Suicide Journalism |Arthur Chu |January 3, 2015 |DAILY BEAST 

As the system booted up, Beck noticed that an abnormal number of terminals were active. The Intern Who Birthed The KAL007 Conspiracy Theories |Tim Mak |September 8, 2014 |DAILY BEAST 

Robyn was in her senior year of college in January 2008 when she found out she had an abnormal pap smear. The Silent Shame of HPV |Emily Shire |August 29, 2014 |DAILY BEAST 

Erica was 22 when her doctor called her and said she had an abnormal pap smear. The Silent Shame of HPV |Emily Shire |August 29, 2014 |DAILY BEAST 

Neurologist Oliver Sacks says the syndrome helps give the Team USA goalie ‘abnormal quickness.’ Why Tourette’s May Be Tim Howard’s Secret Weapon on the Field |Michael Daly |July 3, 2014 |DAILY BEAST 

They combine the fixing with the staining process, and stain differentially every normal and abnormal structure in the blood. A Manual of Clinical Diagnosis |James Campbell Todd 

The abnormal rise in wages had the bad effect of inducing the natives to leave their pastoral pursuits to flock into the towns. The Philippine Islands |John Foreman 

All these abnormal conditions, as a rule, disappear when the consumption of the poison is arrested. A Statistical Inquiry Into the Nature and Treatment of Epilepsy |Alexander Hughes Bennett 

She was ever looking forward for something to excite or satisfy her abnormal desire for the romantic or the dreadful. The value of a praying mother |Isabel C. Byrum 

But conditions now and then appear which are abnormal to man, but which are normal to some of the lower animals. Man And His Ancestor |Charles Morris