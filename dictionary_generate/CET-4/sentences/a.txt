A timeline for election officials put together by the Department of Homeland Security showed that the process for expanding mail-in voting should have begun in April. Vote by mail: Which states allow absentee voting |Kate Rabinowitz, Brittany Mayes |September 17, 2020 |Washington Post 

A big question in public health law is how far an executive, such as a governor, can go without legislative approval. Courts may reconsider temporary coronavirus restrictions as pandemic drags on |Anne Gearan, Karin Brulliard |September 16, 2020 |Washington Post 

A total of about 26 million doses of vaccine B would be available by the end of the year, most of them in December. Top health official says states need about $6 billion from Congress to distribute coronavirus vaccine |Lena H. Sun |September 16, 2020 |Washington Post 

A number of recent polls at the state level have measured both the presidential election and Senate races. If voters are wary of stating support for Trump in polls, why does he outperform GOP Senate candidates? |Philip Bump |September 16, 2020 |Washington Post 

A waiter touching a plate with a bare hand is indeed not ideal, which is why waiters are generally instructed to wear gloves. Parsing Trump’s baffling, head-slapping comments on mask-wearing |Philip Bump |September 16, 2020 |Washington Post 

ROME — What does it take for a Hollywood A-lister to get a private audience with Pope Francis? Pope Francis Has the Pleasure of Meeting Angelina Jolie for a Few Seconds |Barbie Latza Nadeau |January 8, 2015 |DAILY BEAST 

Yes, Byrd—dead four-and-a-half years now—was a Kleagle in the Ku Klux Klan. Steve Scalise and the Right’s Ridiculous Racial Blame Game |Michael Tomasky |January 2, 2015 |DAILY BEAST 

By the time it concluded with a sing-a-long of “XO,” Beyoncé had done the rare thing. Bow Down, Bitches: How Beyoncé Turned an Elevator Brawl Into a Perfect Year |Kevin Fallon |December 31, 2014 |DAILY BEAST 

In another year, stories about the strange new face of an A-list actress might draw chortles and cackles. Renée Zellweger Got a New Face—and Everyone Had An Opinion About It |Kevin O’Keeffe |December 29, 2014 |DAILY BEAST 

Because Duck Dynasty receives monster TV ratings and Robertson paid by a company (A&E) while making these public statements. Butts, Brawls, and Bill Cosby: The Biggest Celebrity Scandals of 2014 |Kevin Fallon |December 27, 2014 |DAILY BEAST 

Now-a-days it is the bankrupt who flouts, and his too confiding creditors who are jeered and laughed at. Glances at Europe |Horace Greeley 

He felt himself the meanest, vilest thing a-crawl upon this sinful earth, and she—dear God! St. Martin's Summer |Rafael Sabatini 

All that scientific bric-a-brac in the cupboard had far better be thrown away. The Salvaging Of Civilisation |H. G. (Herbert George) Wells 

Urinary sediments may be studied under three heads: A. Unorganized sediments. A Manual of Clinical Diagnosis |James Campbell Todd 

I knowed, a-course, that I could go kick up a fuss when Simpson stopped by his office on his trip back from Goldstone. Alec Lloyd, Cowpuncher |Eleanor Gates