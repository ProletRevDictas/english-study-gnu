On the other, there’s a point when even the most progressive parent squirms when confronted with a moaning, breathless roll in the sheets on a 50-inch screen. Watching edgier TV with your kids during the pandemic? You’re not alone. |Bonnie Miller Rubin |February 11, 2021 |Washington Post 

The Council, under Campbell, Williamson said, was not nearly as progressive as it acted. What’s Behind the Effort to Recall Council President Jen Campbell |Scott Lewis |February 9, 2021 |Voice of San Diego 

He was also involved in the founding of Catalist, the flagship progressive data company. The Secret History of the Shadow Campaign That Saved the 2020 Election |Molly Ball |February 4, 2021 |Time 

Key financial regulatory positions remain unfilled, and progressives oppose some leading candidates. The Return of the Regulators |by Jesse Eisinger |February 2, 2021 |ProPublica 

Today’s progressives are in danger of repeating my generation’s mistakes. Bernie Sanders is often called a liberal. He’d beg to differ. Who is actually a liberal? |Graham Vyse |February 2, 2021 |Washington Post 

Weiss is likely to get confirmed even as Warren and a handful of other progressive Democrats vote no. Sen. Warren’s Main Street Crusade to Pressure Clinton |Eleanor Clift |January 8, 2015 |DAILY BEAST 

But now his politics were offending the progressive sensibilities of the American film industry. How James Woods Became Obama’s Biggest Twitter Troll |Asawin Suebsaeng |December 31, 2014 |DAILY BEAST 

They are afflicted with “progressive spiritual emptiness,” he said, which no amount of academic honors and degrees can fill. Pope Francis Denounces the Vatican Elite’s 'Spiritual Alzheimer’s' |Barbie Latza Nadeau |December 23, 2014 |DAILY BEAST 

Some imagine Senator Elizabeth Warren as the charismatic leader of a progressive version of the “tea party.” Time to Bring Back the Truman Democrats |Joel Kotkin |December 21, 2014 |DAILY BEAST 

Throughout the progressive movement, this sentiment is echoed almost everywhere. Why the Left Loves Warren, But Won’t Swoon for Sanders |David Freedlander |December 19, 2014 |DAILY BEAST 

The act, however, is a progressive piece of legislation and creates new conditions as the result of its own operation. Readings in Money and Banking |Chester Arthur Phillips 

At first geologists were disposed to attribute all the phenomena of mountain-folding to the progressive cooling of the earth. Outlines of the Earth's History |Nathaniel Southgate Shaler 

Science and revelation are therefore progressive, though in somewhat different ways. Gospel Philosophy |J. H. Ward 

In this section we can see how the progressive melting gradually brings the rocky débris into plain view. Outlines of the Earth's History |Nathaniel Southgate Shaler 

I am too enlightened and progressive to feel comfortable in my own country, and that is why I spend so much time in England. The Weight of the Crown |Fred M. White