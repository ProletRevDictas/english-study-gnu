Despite the investment frenzy around Luckin and other coffee businesses, coffee drinking still has a relatively low penetration in China compared to countries like the United States and Germany. Tim Hortons marks two years in China with Tencent investment |Rita Liao |February 26, 2021 |TechCrunch 

Liverpool just beat the second-place team in Germany by a comfortable two-goal margin. This Champions League Round Has Not Been Kind To Barcelona |Chadwick Matlin (chadwick.matlin@fivethirtyeight.com) |February 25, 2021 |FiveThirtyEight 

Germany has been actively recruiting foreign nurses to fill thousands of open positions across the country, and is ready to hire 15,000 new Filipino medical workers. The Philippines is offering to trade nurses for vaccines |Anne Quito |February 24, 2021 |Quartz 

If this pipeline repurposing works, electrolyzers connected to old pipes could ultimately serve green hydrogen to nearly all Germany’s major industries. Cheap renewables could make green hydrogen a practical replacement for fossil fuels |Katie McLean |February 24, 2021 |MIT Technology Review 

It found seven commodities accounted for 72 million hectares of lost forest, an area double the size of Germany. When it comes to deforestation, nothing beats a hamburger |Michael J. Coren |February 13, 2021 |Quartz 

We do see that a few European countries have them on the books: Germany, Poland, Italy, Ireland, a couple more. In Defense of Blasphemy |Michael Tomasky |January 9, 2015 |DAILY BEAST 

In Dresden, Germany, anti-Islam rallies each week draw thousands of demonstrators. Police Hunt for Paris Massacre Suspects |Tracy McNicoll, Christopher Dickey |January 7, 2015 |DAILY BEAST 

Some of them already are in Germany taking language lessons. Ghost Ships of the Mediterranean |Barbie Latza Nadeau |January 6, 2015 |DAILY BEAST 

Most coup members “lived in the diaspora in the United States and Germany,” Faal said. The Shadowy U.S. Veteran Who Tried to Overthrow a Country |Jacob Siegel |January 6, 2015 |DAILY BEAST 

The zoologist at University of Tubingen in Germany gave a bunch of spiders some LSD. Zebra Finches, Dolphins, Elephants, and More Animals Under the Influence |Bill Schulz |December 31, 2014 |DAILY BEAST 

Gottlieb Conrad Pfeffel, one of the best poets of Germany, died. The Every Day Book of History and Chronology |Joel Munsell 

She must be freed through the progress of Liberal ideas in France and Germany—not by her own inherent energies. Glances at Europe |Horace Greeley 

Everybody began talking at once the minute dinner was served, as they always do at table in Germany. Music-Study in Germany |Amy Fay 

When we reached the hotel everybody went in to take a siesta—that "Mittags-Schlaf" which is law in Germany. Music-Study in Germany |Amy Fay 

Of course, the usual international operations for obtaining gold were denied to Germany. Readings in Money and Banking |Chester Arthur Phillips