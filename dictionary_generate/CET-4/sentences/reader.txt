All we can ask of you, the readers, is that you not become accustomed to this grim routine. America Is About to Lose Its 200,000th Life to Coronavirus. How Many More Have to Die? |by Stephen Engelberg |September 14, 2020 |ProPublica 

The Atlantic also made it possible for readers to subscribe via Facebook Instant Articles and Google AMP pages. ‘We’ve really reset our floor’: How The Atlantic gained 300,000 new subscribers in the past 12 months |Max Willens |September 10, 2020 |Digiday 

I should add that it appears to me unlikely that many readers of Gourmet wish to think hard about it. Instagram's Most Fascinating Subculture? Women Hunters. |Rachel Levin |September 8, 2020 |Outside Online 

He either mistakenly or accidentally leaves the reader with the impression that these have been ruled out for good, which is most definitely not the case. Your Guide to the Many Meanings of Quantum Mechanics - Facts So Romantic |Sabine Hossenfelder |September 3, 2020 |Nautilus 

Avid readers in particular have embraced the concept, sharing creative, beautiful digital bookshelves that illustrate their reading journey. Digital gardens let you cultivate your own little bit of the internet |Tanya Basu |September 3, 2020 |MIT Technology Review 

Senhor José remains stationary, but this lengthy series of clauses propels the reader along an unmarked path. The Lost Novel of Nobel-Winner José Saramago |Charles Shafaieh |January 5, 2015 |DAILY BEAST 

You, dear reader and refusenik, will likely be called a cynic or a sad sack by friends. The Refuseniks Hiding From ‘Happy New Year’ |Lizzie Crocker |December 31, 2014 |DAILY BEAST 

The second pitfall is that Tendulkar has given the reader little of what should be a gripping, meaningful story of his life. The Story of the World’s Greatest Cricket Player |William O’Connor |December 24, 2014 |DAILY BEAST 

Yet Lohse is confident that the reader will take his actions as the fruits of selfless moral courage. An Ivy League Frat Boy’s Shallow Repentance |Stefan Beck |November 24, 2014 |DAILY BEAST 

When he gets his hands on a Canon copier, the reader gets a glimpse into the unique fashion in which his mind works. The Many Lives of Artist David Hockney |William O’Connor |November 23, 2014 |DAILY BEAST 

Finally, let me ask the general reader to put aside all prejudice, and give both sides a fair hearing. God and my Neighbour |Robert Blatchford 

The old earl's property, the source of his wealth, as from his title the reader will have shrewdly guessed, was in collieries. The Pit Town Coronet, Volume I (of 3) |Charles James Wills 

The reader is referred to larger works upon urinalysis for details. A Manual of Clinical Diagnosis |James Campbell Todd 

Are you quite sure you have never suffered from this rather common disorder, gentle reader, at least, if you be of the male sex? The Pit Town Coronet, Volume I (of 3) |Charles James Wills 

In case any reader should hastily exclaim, “What a ridiculous question; there can be only one southward!” The Giant of the North |R.M. Ballantyne