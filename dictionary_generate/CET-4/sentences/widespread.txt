The coronavirus pandemic and the resulting widespread shift to remote learning have brought major changes to physical education. Kids are shooting hoops with rolled up socks, but pandemic physical education is not canceled |Kelly Field |February 12, 2021 |Washington Post 

One review concluded that the available evidence doesn’t suggest widespread harm to humans—but added that the evidence is limited. Microplastics are everywhere. Here’s what that means for our health. |Ula Chrobak |February 11, 2021 |Popular-Science 

That’s worrisome to some health experts, who note that other events celebrated with widespread get-togethers, such as Memorial Day, the Fourth of July and Thanksgiving, have been accompanied by a jump in infections. Super spreader Sunday? Experts worry Super Bowl could trigger coronavirus explosion |Brittany Shammas, Fenit Nirappil, Mark Maske |February 5, 2021 |Washington Post 

Verily, a health tech sister company of Google, was touted early on as a potential solution to provide widespread testing across the country. For $149, this Oakland Airport vending machine dispenses covid tests |Rachel Lerman |February 5, 2021 |Washington Post 

The Fed’s ongoing intervention in the market will probably hold down rates, but Blake anticipates they will rise slowly as coronavirus vaccinations become more widespread. Uncertainty surrounding economy holds mortgage rates in place |Kathy Orton |February 4, 2021 |Washington Post 

But the inability to measure progress in the ISIS campaign is widespread. Pentagon Doesn’t Know How Many People It’s Killed in the ISIS War |Nancy A. Youssef |January 7, 2015 |DAILY BEAST 

Where the force generating those threats is a widespread, self-sustaining, and virulent social movement? Cover-Ups and Concern Trolls: Actually, It's About Ethics in Suicide Journalism |Arthur Chu |January 3, 2015 |DAILY BEAST 

Those same studies unsurprisingly reveal widespread distrust of police officers among sex workers. To Catch a Sex Worker: A&E’s Awful, Exploitative Ambush Show |Samantha Allen |December 19, 2014 |DAILY BEAST 

Widespread, popular protests began last week after the local grand jury decision. Eric Garner Protesters Have a Direct Line to City Hall |Jacob Siegel |December 11, 2014 |DAILY BEAST 

It is this very sensitive issue that has galvanized widespread resistance from previously loyal campesinos. China’s Nicaragua Canal Could Spark a New Central America Revolution |Nina Lakhani |November 30, 2014 |DAILY BEAST 

Discontent was so widespread that the new general at once ordered all troops, save some three thousand, to leave the capital. Napoleon's Marshals |R. P. Dunn-Pattison 

Mr. Ward is a man of great talents—his fame is widespread as an orator and man of learning, and needs no encomium from us. The Condition, Elevation, Emigration, and Destiny of the Colored People of the United States |Martin R. Delany 

The commercial motorcycle is said to be gaining widespread favor, and therein lies its greatest future. The Wonder Book of Knowledge |Various 

Scotch intellectual activity is the result of a widespread education which is within the reach of the poorest. Friend Mac Donald |Max O'Rell 

An examination of Elizabethan writings does not conduce to the idea of the term having had a widespread acceptation. A Cursory History of Swearing |Julian Sharman