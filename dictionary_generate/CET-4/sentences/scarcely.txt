Electron is only 59 feet tall and scarcely four feet in diameter, significantly smaller than other rockets going to space today. Peter Beck on Rocket Lab’s public listing debut, space SPACs and the Neutron rocket |Aria Alamalhodaei |August 25, 2021 |TechCrunch 

He observed that in stark contrast to the English way of executing “great things in isolation…there is scarcely an undertaking so small that Americans do not unite for it.” What a Rural Corner of America Can Teach Us All About Community and Resilience |Gigi Georges |August 6, 2021 |Time 

The delta variant plaguing much of the country has been scarcely detected in the District, and as cases among vaccinated people are generally not severe, the city’s hospitals are not overwhelmed by covid-19 patients. D.C. reinstates indoor mask mandate as coronavirus cases rise |Julie Zauzmer, Karina Elwood |July 29, 2021 |Washington Post 

The most glittering sporting event on the planet will be elevated once again by epic personal histories involving bloodshed, poverty and a level of endurance other Olympians could scarcely imagine. The Olympic Refugee Team Was Created to Offer Hope. Some Athletes Are Running Away From It |Vivienne Walt |July 8, 2021 |Time 

People, who inhabit just a handful of Ross’ works, scarcely have the same impact. Why is watching Bob Ross paint so dang soothing? We decided to investigate. |Sara Chodosh |July 2, 2021 |Popular-Science 

This is amazing progress considering that marriage equality in the first state was achieved scarcely more than a decade ago. State of LGBT Rights: Married on Sunday, but Fired on Monday |Gene Robinson |December 14, 2014 |DAILY BEAST 

And he scarcely bothered to hide his chief ambition: to lead his country as prime minister. Boris Johnson’s Churchill Man Crush |Michael F. Bishop |November 22, 2014 |DAILY BEAST 

And as he sees it they were scarcely more qualified than he was to lead a group of green soldiers into battle. Corruption Eats Away at Ukraine Military |Charles McPhedran |October 21, 2014 |DAILY BEAST 

The Internet gives video on demand the kind of universal reach that revival houses could scarcely dream of. David Vs. Goliath in the Age of Video on Demand |Teo Bugbee |September 25, 2014 |DAILY BEAST 

Law is weak in Iraq and scarcely exists apart from violence. Bikers of Baghdad: Sunnis, Shias, Skulls, ‘Harleys,’ and Iraqi Flags |Jacob Siegel |July 20, 2014 |DAILY BEAST 

I assure you, no matter how beautifully we play any piece, the minute Liszt plays it, you would scarcely recognize it! Music-Study in Germany |Amy Fay 

But he walked up and down the room and forced himself to listen, though he could scarcely bear it, I could see. Music-Study in Germany |Amy Fay 

The artist, though emphasizing the latter, can scarcely achieve power in this without also attaining the former. Expressive Voice Culture |Jessie Eldridge Southwick 

This reading secures scarcely anything more than a succession of sights to the eye or sounds to the ear. Assimilative Memory |Marcus Dwight Larrowe (AKA Prof. A. Loisette) 

For my part, I scarcely know what to say; inasmuch as I do not care either to affirm or deny a thing of which I have no proof. The Jesuit Relations and Allied Documents, Vol. II: Acadia, 1612-1614 |Various