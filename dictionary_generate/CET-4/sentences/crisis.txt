Back in July, Tamar experienced a mental health crisis that resulted in her hospitalization. Trina Braxton To David Adefeso: ‘When You Attack One, You Attack Us All’ |Hope Wright |September 17, 2020 |Essence.com 

Desmond could not get support even from Supervisor Kristin Gaspar, who often sees the crisis in the same light he does. Morning Report: The Dreaded Purple Tier |Voice of San Diego |September 16, 2020 |Voice of San Diego 

The coronavirus crisis also disrupted Honda’s esports marketing plans. ‘A credible voice’: Why Honda is doubling down on esports |Lara O'Reilly |September 16, 2020 |Digiday 

The crisis has not spurred a spate of shows shot exclusively over Zoom or captured entirely on device screens. How the future of TV and streaming has – and hasn’t – been reshaped so far by 2020 |Tim Peterson |September 16, 2020 |Digiday 

In terms of suggesting he took the crisis seriously and she did not, that’s a stretch. Trump’s ABC News town hall: Four Pinocchios, over and over again |Glenn Kessler |September 16, 2020 |Washington Post 

What is known is that Peña Nieto bungled his response to the crisis. Why Mexicans Are Enraged by Obama’s Big Tuesday Meeting |Ruben Navarrette Jr. |January 6, 2015 |DAILY BEAST 

Consent is manufactured—like, remember the Ebola crisis from a few weeks ago? How Canadian Oilmen Pinkwash the Keystone Pipeline |Jay Michaelson |December 28, 2014 |DAILY BEAST 

And he said, I know you see this crisis through a very personal lens. The NY Police Union’s Vile War with Mayor De Blasio |Michael Tomasky |December 21, 2014 |DAILY BEAST 

Are you more pessimistic about the overall public education crisis given this current environment? Dr. Howard Fuller's Injustice Education |Campbell Brown |December 21, 2014 |DAILY BEAST 

The housing bubble was at very the center of the financial crisis that birthed Dodd-Frank. How Naive is Elizabeth Warren? |Nick Gillespie |December 18, 2014 |DAILY BEAST 

Then Paterno adroitly brought matters to a crisis in a bold peroration which changed the whole scene. The Philippine Islands |John Foreman 

The Afghan was true to his salt, and their own retainers, who had come with them from Lucknow, remained steadfast at this crisis. The Red Year |Louis Tracy 

Mamma had left the drawing-room, and I was sitting alone; I immediately saw that we had reached a crisis. Confidence |Henry James 

With the end of the moratorium on November 4, it may be said that the crisis produced by the outbreak of war was over. Readings in Money and Banking |Chester Arthur Phillips 

Lombard Street has thus shown that it has fully learnt the only lesson that the external side of the crisis had to teach it. Readings in Money and Banking |Chester Arthur Phillips