The Chiefs halted the haircuts quickly after learning of the positive test result. Chiefs escape further coronavirus exposure after mishap involving barber who tested positive |Mark Maske |February 4, 2021 |Washington Post 

Yet, in many states people can drink in bars, get a haircut, eat inside a restaurant, get a tattoo, get a massage, and do myriad other normal, pleasant, but non-essential activities. We Can't Afford to Wait for COVID-19 Vaccines To Be Rolled Out. Here’s What We Can Do to Curb the Virus Now |Gavin Yamey |January 13, 2021 |Time 

Decisions that used to feel mundane—to get that haircut or not—become much more loaded when your health might hang in the balance. Stay calm under pressure with lessons learned in the world’s most stressful careers |Rob Verger |January 8, 2021 |Popular-Science 

Plus, jobs in service sectors won’t come back until people feel comfortable rescheduling a haircut or booking a hotel room. U.S. economy recoups two-thirds of ground lost in first half of year, but there is still far to go |Rachel Siegel, Andrew Van Dam |October 29, 2020 |Washington Post 

In several 2018 demonstrations, Google’s AI made haircut and restaurant reservations without receptionists realizing they were talking with a non-human. Hey Google … What Movie Should I Watch Today? How AI Can Affect Our Decisions |TaeWoo Kim |October 21, 2020 |Singularity Hub 

You must now wear the same haircut & clothes you sported in junior high…forever. #FixTheInternet: The Hashtag That Beat Back Kim Kardashian’s Butt |Emily Shire |November 14, 2014 |DAILY BEAST 

And he had a cowboy hat that he liked touching, too—he smoothed the brim back like it was a ducktail haircut. The Stacks: Pete Dexter on What It’s Like to Lose the Knack of Having Fun |Pete Dexter |September 20, 2014 |DAILY BEAST 

Just old paperbacks, magazine articles, press clippings, and snapshots of you when you had the Larry Page haircut in 8th grade. Up To a Point: Robber Barons Make Way For Robber Nerds |P. J. O’Rourke |August 9, 2014 |DAILY BEAST 

New York Times columnist Maureen Dowd knew Hillary Clinton would be running for office last year when she saw the new haircut. Study: Voters Want Their Female Politicos to Look Like Ladies |Brandy Zadrozny |May 15, 2014 |DAILY BEAST 

Meet the tiny tot who, armed with a juice box and the "it" haircut, is proving that kids can be stylish, too. The Littlest Fashionista Is Just Plain Stinkin’ Cute |Justin Jones |May 13, 2014 |DAILY BEAST 

Two of them -- Severe Haircut woman and utility belt man -- looked at me from their ergonomic super-chairs. Little Brother |Cory Doctorow 

"I think you should really reconsider your approach to this situation," Severe Haircut woman said. Little Brother |Cory Doctorow 

Severe haircut lady was in the new questioning party, as were three big guys who moved me around like a cut of meat. Little Brother |Cory Doctorow 

It was the guy who had come into the truck and spoken to Severe-Haircut woman when I was chained up in the back. Little Brother |Cory Doctorow 

The men are fond of saying that all they had by way of preparation for the job was four days' drilling and a haircut. The Glory of The Coming |Irvin S. Cobb