Widely regarded as one of the greatest tennis players ever, Williams’s only remaining snag is the Grand Slam record of a troublesome Margaret Court, who played in an era with weaker competition. How Serena Williams Could Finally Break The Grand Slam Record |Amy Lundy |February 10, 2021 |FiveThirtyEight 

I was always playing tennis, and that was my path before it was interrupted. Boris Kodjoe: Teach Racial History |Eugene Robinson |February 2, 2021 |Ozy 

I had a conversation with Andre Agassi a few years ago where he said that he actually never really liked tennis, and I was surprised to hear that. Danica Patrick on Why She Never Loved Racing and Her Post-Track Career |Joshua Eferighe |January 30, 2021 |Ozy 

So they nap wherever they can — on concrete, indoor tennis courts or on carpeted floors. National Guard members allowed back at Capitol after they were banished to a parking garage |Alex Horton |January 22, 2021 |Washington Post 

Although tennis officials have requested that players who repeatedly test negative over their first days in the country be treated leniently, government officials have refused. Australian Open players in quarantine accused of feeding mice in their hotel rooms |Cindy Boren |January 20, 2021 |Washington Post 

According to police, Kory then attacked the victim with an aluminum tennis racket. Meet Your New ‘Hot Mugshot Guy’: Sean Kory, Fox News’ Public Enemy No. 1 |Marlow Stern |November 3, 2014 |DAILY BEAST 

The celebrity of Li is hard to fathom in terms that American fans—tennis savvy or not—can fully comprehend. Tennis Star Li Na Says Goodbye to the Court…and Puts the Sport’s Rise in Asia in Question |Nicholas McCarvel |September 19, 2014 |DAILY BEAST 

Ask any tennis journalist (myself included) or public relations representative if they have a good Li Na story and they do. Tennis Star Li Na Says Goodbye to the Court…and Puts the Sport’s Rise in Asia in Question |Nicholas McCarvel |September 19, 2014 |DAILY BEAST 

I'm sure there will be a huge down fall in the next two to three years for women's tennis in China. Tennis Star Li Na Says Goodbye to the Court…and Puts the Sport’s Rise in Asia in Question |Nicholas McCarvel |September 19, 2014 |DAILY BEAST 

As Liu and Worcester note, Li brought tennis to an entire new population of fans, including Asians living around the globe. Tennis Star Li Na Says Goodbye to the Court…and Puts the Sport’s Rise in Asia in Question |Nicholas McCarvel |September 19, 2014 |DAILY BEAST 

After tea Betty executed a quite deliberate manœuvre to avoid having him for a partner at tennis. Uncanny Tales |Various 

I played lawn tennis in the morning, and after lunch down with Graham to Apia. The Works of Robert Louis Stevenson - Swanston Edition Vol. 25 (of 25) |Robert Louis Stevenson 

As for me, I have to lay aside my lawn tennis, having (as was to be expected) had a smart but eminently brief hemorrhage. The Works of Robert Louis Stevenson - Swanston Edition Vol. 25 (of 25) |Robert Louis Stevenson 

To half a tennis-lawn add two ounces of croquet-mallet and three arches of pergola, and reduce the whole to a fine powder. Punch, or the London Charivari, Vol. 152, May 16, 1917. |Various 

Hal got his hat, and the chums went forth, again in their tennis flannel undress. Uncle Sam's Boys as Lieutenants |H. Irving Hancock