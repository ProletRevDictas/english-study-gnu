Instead, we have to see this sort of behavior in humans through an entirely different lens, that recognizes how important our cultural systems are in determining what we do as men and women, boys and girls. No, Animals Do Not Have Genders - Facts So Romantic |Cailin O'Connor |August 26, 2020 |Nautilus 

That’s a slight downgrade from the similar lens in the S20 Ultra, which had 10X and 100X zooming capabilities. Samsung Note20 Ultra review: Why this big phone works for the COVID era |Aaron Pressman |August 18, 2020 |Fortune 

Companies are having to rethink every aspect of the business through this new lens of rising costs and the on-location experience not being what it was just a few months ago. SEO in the second half of 2020: Five search opportunities to act on now |Jim Yu |August 17, 2020 |Search Engine Watch 

Each card gives the reader a different lens on the election. How We Designed The Look Of Our 2020 Forecast |Anna Wiederkehr (anna.wiederkehr@abc.com) |August 13, 2020 |FiveThirtyEight 

For Americans, that might mean that questions of whether to stay home, wear a mask or to see friends and family without social distancing are filtered through a partisan lens. Republicans And Democrats See COVID-19 Very Differently. Is That Making People Sick? |Amelia Thomson-DeVeaux |July 23, 2020 |FiveThirtyEight 

It reminded me a bit of an alternative take on The Wolf of Wall Street—through the Toni and Candace lens. Coffee Talk with Fred Armisen: On ‘Portlandia,’ Meeting Obama, and Taylor Swift’s Greatness |Marlow Stern |January 7, 2015 |DAILY BEAST 

And he said, I know you see this crisis through a very personal lens. The NY Police Union’s Vile War with Mayor De Blasio |Michael Tomasky |December 21, 2014 |DAILY BEAST 

The camera dollied backward along the length of the tower's staircase while simultaneously its lens zoomed forward. Alfred Hitchcock’s Fade to Black: The Great Director’s Final Days |David Freeman |December 13, 2014 |DAILY BEAST 

I see my personal experiences as a lens to look at something much bigger. Meghan Daum On Tackling The Unspeakable Parts Of Life |David Yaffe |December 6, 2014 |DAILY BEAST 

They, like Klain, deserve to be seen through a historical lens before we rush to judgment. Does Ebola Need An Organization Man? |Nicolaus Mills |October 26, 2014 |DAILY BEAST 

Go carefully over the film with an oil-immersion lens, using a mechanical stage if available. A Manual of Clinical Diagnosis |James Campbell Todd 

Still kneeling, he drew from his waistcoat pocket a powerful lens contained in a washleather bag. Dope |Sax Rohmer 

By far the greatest of these scientific inventions are those which depend upon the lens. Outlines of the Earth's History |Nathaniel Southgate Shaler 

It was a little longer than the usual three cell case, and there was a finely ground lens at the end. Hooded Detective, Volume III No. 2, January, 1942 |Various 

He removed his glasses and wiped them slowly and carefully, polishing each lens with meticulous care. Hooded Detective, Volume III No. 2, January, 1942 |Various