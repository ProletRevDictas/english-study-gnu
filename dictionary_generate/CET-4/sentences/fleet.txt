At first, its strategy of selling carmakers the technology to build their own self-driving fleets looked promising. Last Unicorn Standing: Can This Self-Driving Innovator Survive? |Charu Kasturi |August 27, 2020 |Ozy 

American says that on all planes in its fleet, air is either replaced with outside air, or scrubbed using hospital-grade HEPA filtration, every two-to-four minutes. American Airlines touts a new tool to combat COVID. But does it really make flying safer? |dzanemorris |August 24, 2020 |Fortune 

American Airlines will swiftly begin electrostatically spraying the product across its fleet, including, eventually its regional aircraft, though it is limited to applying the disinfectant in the state of Texas under the current EPA authorization. This product kills COVID for 7 days—and just got EPA approval for some American Airlines planes |Lee Clifford |August 24, 2020 |Fortune 

Instead, they taught a fleet of computers to do it for them. Computer Search Settles 90-Year-Old Math Problem |Kevin Hartnett |August 19, 2020 |Quanta Magazine 

By 3rd century BC, Ostia was transformed into a naval base, so much so that it became the seat of one of the officially appointed commanders of the Roman fleet, who was known as the quaestor Ostiensis. Ostia Antica: Reconstruction and History of The Harbor City of Ancient Rome |Dattatreya Mandal |April 14, 2020 |Realm of History 

But it takes more than just pilots to operate the drone fleet. Exclusive: U.S. Drone Fleet at ‘Breaking Point,’ Air Force Says |Dave Majumdar |January 5, 2015 |DAILY BEAST 

The player starts out with a fleet of three or four ships (depending on the machine), which he operates one at a time. ‘Asteroids’ & The Dawn of the Gamer Age |David Owen |November 29, 2014 |DAILY BEAST 

The company also converts the gas into a liquid fuel that can run vehicles in its fleet. Garbage In, Power Out |The Daily Beast |November 24, 2014 |DAILY BEAST 

Waste Management, the large disposal company, has turned its landfills into a fleet of power producers. Garbage In, Power Out |The Daily Beast |November 24, 2014 |DAILY BEAST 

In 2015, Monster Jam will have a fleet of eight female drivers. The Moms of Monster Jam Drive Trucks, Buck Macho Culture |Eliza Krigman |November 22, 2014 |DAILY BEAST 

The commander of this fleet was an Englishman, according to the agreement between them. The Philippine Islands, 1493-1898, Volume XX, 1621-1624 |Various 

The Dutch fleet attacked Burnt island, in Scotland, but were repulsed. The Every Day Book of History and Chronology |Joel Munsell 

The fleet of the enemy left the place where they last halted, and came in sight day before yesterday in the morning. The Philippine Islands, 1493-1898, Volume XX, 1621-1624 |Various 

Don't you mind, Ike, it come the same day and on the wery same stage as the news of the sinkin' of the Spaynish fleet? The Soldier of the Valley |Nelson Lloyd 

That argument was not the one of least weight in the council in determining that our fleet should not sail against the enemy. The Philippine Islands, 1493-1898, Volume XX, 1621-1624 |Various