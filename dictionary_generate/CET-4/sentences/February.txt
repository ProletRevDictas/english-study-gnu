Amazingly, in February, Americans were feeling incredibly bullish. Can the Dow’s Big Tech makeover finally push the blue chip index above 30,000? |Bernhard Warner |August 25, 2020 |Fortune 

The Capital and ProPublica asked the housing authority in July why officials required Brown to pay the full rent in February after the agency confirmed that she was unemployed. She Was Sued Over Rent She Didn’t Owe. It Took Seven Court Dates to Prove She Was Right. |by Danielle Ohl, Capital Gazette, and Talia Buford and Beena Raghavendran, ProPublica |August 25, 2020 |ProPublica 

Shares of the company now trade 38% below their debut in February even as the S&P 500 climbs to new highs. Casper’s CEO is filing for a blank check company. What does he plan to buy? |Lucinda Shen |August 19, 2020 |Fortune 

A bundle Bloomberg Media and The Information announced in February, for example, has already been unwound. ‘There are so many cool things we could do’: Publisher interest in subscription-driving bundles simmers |Max Willens |August 11, 2020 |Digiday 

Despite assurances throughout January and February that Europe was successfully containing the virus, it had silently taken hold of the continent and officials were beginning to realize that a larger crisis was looming. Inside Europe’s response to the coronavirus outbreak |Laura Margottini |July 16, 2020 |Quartz 

In February, Slovakia will have a referendum on whether marriage should be defined as a union between a man and a woman. ‘Only God’ Can Stop Gay Marriage |Tim Mak |January 6, 2015 |DAILY BEAST 

This past February, another member, Fernando Gonzalez, was sent back to Cuba. Of Cuban Spies, a Baby, and a Filmmaker: The Strange Tale of the Cuban Five |Nina Strochlic |December 28, 2014 |DAILY BEAST 

And back in February, he lost a close pal in Philip Seymour Hoffman. Coffee Talk with Ethan Hawke: On ‘Boyhood,’ Jennifer Lawrence, and Bill Clinton’s Urinal Exchange |Marlow Stern |December 27, 2014 |DAILY BEAST 

By the end of February, as Hitler consolidated his power, his decision was made: He would not—indeed, could not—remain. The Catholic Philosopher Who Took on Hitler |John Henry Crosby |December 26, 2014 |DAILY BEAST 

Political tensions are underlying every move in Nigeria, where, in February, the next presidential candidates will be nominated. The New Face of Boko Haram’s Terror: Teen Girls |Nina Strochlic |December 13, 2014 |DAILY BEAST 

The work was commenced in June, 1882, the memorial stone being laid February 15th, the following year. Showell's Dictionary of Birmingham |Thomas T. Harman and Walter Showell 

At last he arrived at Dieppe, and, after a sojourn there, he put to sea upon the 26th of this same month of February. The Jesuit Relations and Allied Documents, Vol. II: Acadia, 1612-1614 |Various 

On February 18th the French captured the suburb on the left bank of the river, and thus placed the inner town between two fires. Napoleon's Marshals |R. P. Dunn-Pattison 

After the surrender of Comyn and his adherents in February 1303–4, he threw himself heartily into the pursuit of Wallace. King Robert the Bruce |A. F. Murison 

Early in February he was invited for a week's hunting to a house at which Betty Lardner was also a guest. Uncanny Tales |Various