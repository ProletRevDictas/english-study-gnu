Just the hard-on before you shoot unarmed members of the public. 'Babylon' Review: The Dumb Lives of Trigger-Happy Cops |Melissa Leon |January 9, 2015 |DAILY BEAST 

Three on-the-record stories from a family: a mother and her daughters who came from Phoenix. I Tried to Warn You About Sleazy Billionaire Jeffrey Epstein in 2003 |Vicky Ward |January 7, 2015 |DAILY BEAST 

You just travel light with carry-on luggage, go to cities that you love, and get to hang out with all your friends. Coffee Talk with Fred Armisen: On ‘Portlandia,’ Meeting Obama, and Taylor Swift’s Greatness |Marlow Stern |January 7, 2015 |DAILY BEAST 

It was a brick wall that we turned into the on-ramp of a highway. Coffee Talk with Fred Armisen: On ‘Portlandia,’ Meeting Obama, and Taylor Swift’s Greatness |Marlow Stern |January 7, 2015 |DAILY BEAST 

Could the (thus far) timid trembling give way to a full-on, grand mal seizure? 26 Earthquakes Later, Fracking’s Smoking Gun Is in Texas |James Joiner |January 7, 2015 |DAILY BEAST 

I drew back from the rim of Writing-On-the-Stone, that set of whispered phrases echoing in my ears. Raw Gold |Bertrand W. Sinclair 

Kingston-on-Thames is still provincial in appearance, though now the centre of a great growth of modern suburbs. The Portsmouth Road and Its Tributaries |Charles G. Harper 

Besides the districts mentioned, tobacco is grown largely in that of Frankfort-on-the Oder. Tobacco; Its History, Varieties, Culture, Manufacture and Commerce |E. R. Billings. 

Madame Malmaison had always been a little proud of the beauty and grace and sweetness of her fitter-on. The Weight of the Crown |Fred M. White 

Berwick-on-Tweed lies partly in England and partly in Scotland, the river which runs through it forming the boundary line. British Highways And Byways From A Motor Car |Thomas D. Murphy