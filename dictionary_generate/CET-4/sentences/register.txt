Filipinos make up nearly a fifth of registers nurses in California, according to a 2018 Board of Registered Nursing survey. Filipino Residents Have Been Hit Hard by COVID – But No One Knows Just How Hard |Maya Srikrishnan |February 1, 2021 |Voice of San Diego 

She doesn’t have a news anchor voice — one that is trained and practiced at staying at a low register. The sound of a shifting power structure |Robin Givhan |January 13, 2021 |Washington Post 

It can guess basic demographic information like gender, who’s an employee—based on whether they go behind the register, even interactions between employees and customers. Podcast: Attention, shoppers–you’re being tracked |Tate Ryan-Mosley |December 21, 2020 |MIT Technology Review 

Then it wandered into Walmart and found its owner working at the register. Puzzle swap celebrates jigsaw ‘barter culture’ amid pandemic chaos |Justin Wm. Moyer |December 17, 2020 |Washington Post 

Germany’s tech sector has hailed the government’s decision to allow some electronic-only securities—a move that paves the way for “crypto securities” to be entered into a blockchain-based register. Germany prepares to put some financial securities on the blockchain |David Meyer |December 16, 2020 |Fortune 

Annie Lee Cooper, well played by Winfrey, is shown trying but failing to register to vote. Dr. King Goes to Hollywood: The Flawed History of ‘Selma’ |Gary May |January 2, 2015 |DAILY BEAST 

Many dance instructors register their classes at gyms and teach women or men (separately) under the name of aerobics. Iran’s Becoming a Footloose Nation as Dance Lessons Spread |IranWire |January 2, 2015 |DAILY BEAST 

Nor are we told that she lost her job at a local nursing home after she tried to register to vote in 1964. Dr. King Goes to Hollywood: The Flawed History of ‘Selma’ |Gary May |January 2, 2015 |DAILY BEAST 

“They refused to register a case, saying the matter is out of their hands,” he told me. The Dangerous Drug-Funded Secret War Between Iran and Pakistan |Umar Farooq |December 29, 2014 |DAILY BEAST 

Even the valor of tragedy is denied to Daisy, “a woman born with a voice that lacks a tragic register.” Carol Shields’s Tale Of Secondhand Life |Nathaniel Rich |October 26, 2014 |DAILY BEAST 

Girls are just like cats; they all like to mope around the register or the steam radiator in cold weather. The Girls of Central High on the Stage |Gertrude W. Morrison 

He scrutinized the register, and found, to his satisfaction, that a Mr. Bowman of Boston was occupying room 106. Scattergood Baines |Clarence Budington Kelland 

You file some papers, you collate some register, you sign your name or your initials to some documents. The 'Characters' of Jean de La Bruyre |Jean de La Bruyre 

If you have written since, your letter also has miscarried, as is much the rule in this part of the world, unless you register. The Works of Robert Louis Stevenson - Swanston Edition Vol. 25 (of 25) |Robert Louis Stevenson 

Have just had breakfast, written up one letter, register and close this. The Works of Robert Louis Stevenson - Swanston Edition Vol. 25 (of 25) |Robert Louis Stevenson