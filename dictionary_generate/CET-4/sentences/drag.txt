Though the vast majority of stocks––including beaten down financials and airlines––proved resilient over that period, their numbers and strength weren’t nearly sufficient to offset the drag from the falling tech titans. Will tech stocks stumble or slide? What the fundamentals tell us |Shawn Tully |September 16, 2020 |Fortune 

How to vote in your stateBut the president’s ratings overall and on his handling of the coronavirus pandemic are in negative territory and are a drag on his candidacy. Post-ABC Wisconsin poll shows Biden holding narrow edge over Trump |Dan Balz, Emily Guskin |September 16, 2020 |Washington Post 

If the poll result gets dragged out, or, gasp, gets contested, that will add all kinds of volatility to the markets. Is M&A back? Investors hope so, and that’s lifting global stocks |Bernhard Warner |September 15, 2020 |Fortune 

For smaller kids, try dragging a mattress out of the bedroom, propping it against a couch, and encouraging your little ones to scramble to the “peak” at the top. A 15-Week Exercise Plan for Kids and Families |Krista Langlois |September 12, 2020 |Outside Online 

What had never been done was mobilizing drag artists all over the country in service of getting out the vote. Virtual Drag Out the Vote event features ‘Drag Race’ alum |Steph Purifoy |September 11, 2020 |Washington Blade 

This breach is an extraordinary emotional drag on the exhausted population. In One Corner of Syria, Christmas Spirit Somehow Manages to Survive |Peter Schwartzstein |December 25, 2014 |DAILY BEAST 

Is this your first time dressing in drag, or have you ever had an Ed Wood moment? Michael C. Hall on Going Drag for ‘Hedwig and the Angry Inch’ and Exorcising ‘Dexter’ |Marlow Stern |December 4, 2014 |DAILY BEAST 

I went to dinner with Christian and he talked about Guitar Drag and I suddenly realized I was going to write about it. Greil Marcus Talks About Trying to Unlock Rock and Roll in 10 Songs |Allen Barra |November 17, 2014 |DAILY BEAST 

What it did do was drag him down, as though my shot had dropped him into the dunk tank at the state fair. I Shot Bin Laden |Elliot Ackerman |November 16, 2014 |DAILY BEAST 

Nicki Minaj popularized “yaaasssss” with her song “Yasss Bish” and she claims the pronunciation has roots in drag-queen culture. Feminist, Bae, Turnt: Time’s ‘Worst Words’ List Is Sexist and Racist |Samantha Allen |November 13, 2014 |DAILY BEAST 

We, Watsons, are waiting for him to step forward and drag various dark mysteries into the light of day. Punch, or the London Charivari, Volume 107, November 3, 1894 |Various 

Alcee Arobin and Mrs. Highcamp called for her one bright afternoon in Arobin's drag. The Awakening and Selected Short Stories |Kate Chopin 

He found such delight in playing that it was frequently necessary to drag him by force from the instrument. The Life &amp; Letters of Peter Ilich Tchaikovsky |Modeste Tchaikovsky 

Frenziedly she caught at the heavy oaken table, and began to drag it across the room as Garnache had begged her. St. Martin's Summer |Rafael Sabatini 

If you take hold of your dress on both sides, in that way, and drag it up so high, you will be set down as a raw country girl. The Ladies' Book of Etiquette, and Manual of Politeness |Florence Hartley