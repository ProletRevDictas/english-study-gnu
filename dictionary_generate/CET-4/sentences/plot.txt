What followed, prosecutors said, was a criminal plot by his underlings to cyberstalk the couple. Four ex-eBay employees to admit guilt in cyberstalking plot |Verne Kopytoff |September 23, 2020 |Fortune 

Counseling, rehab, a lot of medication, trial and error, quitting drinking, and support from family and friends have essentially kept me here and with the plot. The Accidental Attempted Murder |Eugene Robinson |September 2, 2020 |Ozy 

Over subsequent days, the hacker met with the employee multiple times to hash out the plot, unaware that the FBI was listening in. The FBI broke up a Russian hacker plot to extort millions from Tesla |Aaron Pressman |August 28, 2020 |Fortune 

After rejecting all possible sources of error they could think of, the researchers came up with three explanations that would fit the size and shape of the bump in their data plots. Dark Matter Experiment Finds Unexplained Signal |Natalie Wolchover |June 17, 2020 |Quanta Magazine 

And, if you’ve forgotten, here’s the plot summary, as told by Anya Dubner. Does Hollywood Still Have a Princess Problem? (Ep. 394) |Stephen J. Dubner |October 24, 2019 |Freakonomics 

When communism was a threat, it was construed as a communist plot. Anti-Fluoriders Are The OG Anti-Vaxxers |Michael Schulson |July 27, 2016 |DAILY BEAST 

But his account of a dissident plot involving Gambian expats using U.S. weapons is similar to what Faal told the FBI. The Shadowy U.S. Veteran Who Tried to Overthrow a Country |Jacob Siegel |January 6, 2015 |DAILY BEAST 

Another member of the plot took care of the ammo along with black uniforms, night-vision equipment, and body armor. The Shadowy U.S. Veteran Who Tried to Overthrow a Country |Jacob Siegel |January 6, 2015 |DAILY BEAST 

They were able to purchase weapons and plot attacks on the island without much interference. Of Cuban Spies, a Baby, and a Filmmaker: The Strange Tale of the Cuban Five |Nina Strochlic |December 28, 2014 |DAILY BEAST 

The plot was a string of anecdotes from the senseless shootings of friends that Brinsley knew. Alleged Cop Killer’s Blood-Soaked Screenplay |M.L. Nestel |December 24, 2014 |DAILY BEAST 

It was thanks to the discovery of this plot that the Marshal first got information of his enemies' projected advance. Napoleon's Marshals |R. P. Dunn-Pattison 

But Magellan learned of their wicked plot in time to defeat them, and he punished them as they deserved. Alila, Our Little Philippine Cousin |Mary Hazelton Wade 

While he grieved over the loss of our little one, you conceived a vile plot to 'get even,' Oh, you—liar! The Homesteader |Oscar Micheaux 

It was assuming a great deal to tell a woman that he saw through her plot to disenchant him with a rival. Ancestors |Gertrude Atherton 

The friends so overacted their part, that Jane immediately saw through the plot. Madame Roland, Makers of History |John S. C. Abbott