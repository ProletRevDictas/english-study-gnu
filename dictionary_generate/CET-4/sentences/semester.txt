That we’re not going to wait to play later in the semester when it’s mythically going to just magically get better. For this college football team, covid means the season starts in February — with Senior Day |Glynn A. Hill |February 11, 2021 |Washington Post 

The semester I spent abroad in Paris kind of changed all that and I decided I didn’t want to do pre-med anymore, but I wanted to find a way to get back to Paris. How French Baking Was Brought to Virginia |Eugene Robinson |February 5, 2021 |Ozy 

Although this is not the way we wanted to start this new term, we also know that with the proper commitment to our established safety protocols, we will be able to have a successful and safe semester together. Johns Hopkins suspends in-person classes for undergrads until Friday after virus cases |Lauren Lumpkin |February 4, 2021 |Washington Post 

Some, he said, are delaying the start of the semester or the start of in-person teaching to buy time. Spring term delays: New wave of coronavirus uncertainty slams higher education |Nick Anderson |January 20, 2021 |Washington Post 

Like other schools, Middle Tennessee had moved classes online for the remainder of the semester. A college football coach’s season at war with the coronavirus — and his own school |Kent Babb |January 19, 2021 |Washington Post 

I had been studying abroad in London, and came back to finish the semester at Tufts. Everyone at This Dinner Party Has Lost Someone |Samantha Levine |January 6, 2015 |DAILY BEAST 

The trick, in any case, was repeated semester after semester. Stonewall Jackson, VMI’s Most Embattled Professor |S. C. Gwynne |November 29, 2014 |DAILY BEAST 

According to his suit, Carleton would rotate four new boys into his home every semester. Headmasters Behaving Badly |Emily Shire |November 29, 2014 |DAILY BEAST 

My relationship with foreign cultures began, like many privileged Americans, with an idealistic college semester abroad. How I Got Addicted to Africa (and Wrote a Thriller About It) |Todd Moss |September 9, 2014 |DAILY BEAST 

I will be moving out of my House next semester, if only—quite literally—to save my life. The College Bro’s Burden: Consent and Assault Cast a Shadow on Sexy Times |Amy Zimmerman |August 22, 2014 |DAILY BEAST 

These girls had all arrived at Ardmore several days in advance of the opening of the semester. Ruth Fielding At College |Alice B. Emerson 

This would be sometime between the first of January 1940, and the time you finished the second semester, let us say. Warren Commission (11 of 26): Hearings Vol. XI (of 15) |The President's Commission on the Assassination of President Kennedy 

In most instances he was a man of wealth and high social standing, who looked upon his semester or two as a romantic episode. Lippincott's Magazine of Popular Literature and Science |Various 

Queen's is bad enough, but if I am to descend to a room over the post-office after this semester, I'd—I'd rather die! Molly Brown's Sophomore Days |Nell Speed 

That young man will not fight another round for many a long semester after I have done with him.' Greifenstein |F. Marion Crawford