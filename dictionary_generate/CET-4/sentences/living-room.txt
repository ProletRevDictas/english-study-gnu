There’s just no way to play soccer alone in your living room. Kids are shooting hoops with rolled up socks, but pandemic physical education is not canceled |Kelly Field |February 12, 2021 |Washington Post 

Her daughter and the other kids followed online classes from their own computers in the living room. The stimulus relieved short-term pain, but eviction’s impact is a long haul |Kyle Swenson |February 8, 2021 |Washington Post 

Get a friendly tournament going in your backyard, garage, or living room with professional and affordable equipment. Advanced tables to enhance your ping pong game |PopSci Commerce Team |February 5, 2021 |Popular-Science 

Sitting in my living room, I conducted interviews over Zoom with City Wildlife staffers, volunteers and other animal experts. Here’s the deal with Alice Deal and the middle school named in her honor |John Kelly |February 3, 2021 |Washington Post 

The thermostat tells me it’s 69 degrees in my living room, and I’m as comfortable now as I will be when I take the dogs for a walk after I wrap up this article. The Kora Xenolith Is My Secret Weapon Against the Cold |Wes Siler |February 2, 2021 |Outside Online 

Toomey glides around the room like a Brazilian capoeira dancer. How Taryn Toomey’s ‘The Class’ Became New York’s Latest Fitness Craze |Lizzie Crocker |January 9, 2015 |DAILY BEAST 

Patrick Klugman, the deputy mayor of Paris, said: “We are living our kind of 9/11,” he said. France Kills Charlie Hebdo Murderers |Nico Hines |January 9, 2015 |DAILY BEAST 

Last week I turned 40, a bittersweet occasion because I crossed the line to living longer without my mother than with her. Everyone at This Dinner Party Has Lost Someone |Samantha Levine |January 6, 2015 |DAILY BEAST 

But as an American creating a new brand here, and living the daily life of the souk, he seems to be in a league of his own. The Photographer Who Gave Up Manhattan for Marrakech |Liza Foreman |January 6, 2015 |DAILY BEAST 

A single father, he had been living abroad and returned when his mother was diagnosed with cancer. Everyone at This Dinner Party Has Lost Someone |Samantha Levine |January 6, 2015 |DAILY BEAST 

The big room at King's Warren Parsonage was already fairly well filled. The Pit Town Coronet, Volume I (of 3) |Charles James Wills 

In the drawing-room things went on much as they always do in country drawing-rooms in the hot weather. The Pit Town Coronet, Volume I (of 3) |Charles James Wills 

Kind of a reception-room in there—guess I know a reception-room from a hole in the wall. Boys and Girls Bookshelf; a Practical Plan of Character Building, Volume I (of 17) |Various 

His lordship retired shortly to his study, Hetton and Mr. Haggard betook themselves to the billiard-room. The Pit Town Coronet, Volume I (of 3) |Charles James Wills 

When his lordship retired early, as was his custom, the other men adjourned once more to the billiard-room. The Pit Town Coronet, Volume I (of 3) |Charles James Wills