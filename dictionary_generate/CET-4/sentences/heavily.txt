Walmart may also be able to push e-commerce more heavily on TikTok, and turn it into more of a livestream shopping app. What a Walmart-Microsoft bid for TikTok could mean for e-commerce |Anna Hensel |August 28, 2020 |Digiday 

People who experienced childhood trauma are more likely to smoke or drink heavily, it found. Puberty may reboot the brain and behaviors |Esther Landhuis |August 27, 2020 |Science News For Students 

Similar to learning from our own experiences, this social learning circuit generates a “social prediction error”—one that heavily guides how we learn from others, but surprisingly, also how we learn from ourselves. This Is How Your Brain Responds to Social Influence |Shelly Fan |August 25, 2020 |Singularity Hub 

However, employment fears continue to weigh heavily everywhere. Retail continues to charge back, this time in the U.K. But clothing, fuel sales lag |David Meyer |August 21, 2020 |Fortune 

Of course, these scores aren’t perfect either, as they’re heavily influenced by which bills actually get to the floor and which don’t, but they’re still useful for helping us distinguish conservatives from liberals from moderates. How Can A Senator Vote With Trump Most Of The Time And Still Be A Moderate? |Seth Masket |July 22, 2020 |FiveThirtyEight 

That fantasy, however, is still heavily regimented by all sorts of norms. ‘Empire’ Review: Hip-Hop Musical Chairs with an Insane Soap Opera Twist |Judnick Mayard |January 8, 2015 |DAILY BEAST 

The scene was heavily cordoned off to traffic and anyone not with the police, press, or residents. Police Hunt for Paris Massacre Suspects |Tracy McNicoll, Christopher Dickey |January 7, 2015 |DAILY BEAST 

His later books drew heavily from experiences and people he encountered at the bar, including the cruel captain in The Sea-Wolf. The Bars That Made America Great |Nina Strochlic |December 28, 2014 |DAILY BEAST 

I mean my background weighed heavily, because I was brought up in this orthodox way. Daphne Merkin on Lena Dunham, Book Criticism, and Self-Examination |Mindy Farabee |December 26, 2014 |DAILY BEAST 

But, under the hawkish eye of the media and through a heavily active social media presence, she carried on as usual. Nicki Minaj Bares Her Own Vulnerability on ‘The Pinkprint’ |Rawiya Kameir |December 16, 2014 |DAILY BEAST 

The left heel followed like lightning, and the right paw also slipped, letting the bear again fall heavily on the ice below. The Giant of the North |R.M. Ballantyne 

Even New Zealand Brigade which has been only recently engaged lost heavily and is to some extent demoralised. Gallipoli Diary, Volume I |Ian Hamilton 

One heavily-laden boat was dragged into the stream, and a few officers and men clambered on board. The Red Year |Louis Tracy 

He was flung down heavily, and pinned prone in a corner by one of those bullies who knelt on his spine. St. Martin's Summer |Rafael Sabatini 

But before he reached it there was the shriek of a whistle, a violent shock, and he was hurled heavily into the opposite seat. Uncanny Tales |Various