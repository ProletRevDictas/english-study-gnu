After abandoning his training as a priest, he spent the rest of his life reading voraciously. Did Stalin’s rise to power foretell the butchery that came next? |Robert Service |October 30, 2020 |Washington Post 

Initially, I and another parishioner helped our priest record services ahead of time that would be uploaded on the church’s YouTube page for Sunday morning. How a funeral inspired the pandemic’s hottest hardware |Daniel Bentley |October 18, 2020 |Fortune 

Those days that I had to go to various priests for deliverance and guidance. How Young, Queer Nigerians Use Twitter To Shape Identity And Fight Homophobia |LGBTQ-Editor |October 14, 2020 |No Straight News 

Kolfage has unleashed his growing army of followers on critics and opponents of those projects, including local elected and wildlife refuge officials and a priest. Veteran, War Hero, Defendant, Troll |by Jeremy Schwartz and Perla Trevizo |September 29, 2020 |ProPublica 

He and the other priests at his observatory were particularly adept at reading the clouds for storm intensity and trajectory, and he tried to warn Texans about the incoming threat. Your car is probably full of spiders |PopSci Staff |September 2, 2020 |Popular-Science 

He was then literally slapped around by the high priest, who pulled on his ears in an effort to produce tears. New Year’s Eve, Babylon Style |Candida Moss |December 31, 2014 |DAILY BEAST 

According to Wahlberg, his time in prison, as well as the guidance of a parish priest, helped him turn his life around. Mark Wahlberg’s Pardon Plea: A Look Back At His Troubling, Violent, and Racist Rap Sheet |Marlow Stern |December 7, 2014 |DAILY BEAST 

Amid accusations of infidelity, she told reporters in 1988 that she and the former priest were just fine. Adiós to the Diva Duchess |Barbie Latza Nadeau |November 20, 2014 |DAILY BEAST 

He speaks with the authority of a native of this land as well as the authority of priest. Mexico’s Holy Warrior Against the Cartels |Jason McGahan |November 18, 2014 |DAILY BEAST 

The priest for the Creole ceremony was Father Marcel Saint Jean. Mother Cabrini, Saint of the Green Card |Michael Luongo |November 11, 2014 |DAILY BEAST 

These residents then killed the parish priest, and without arms fled for safety to the mountain ravines. The Philippine Islands |John Foreman 

Simon the high priest, the son of Onias, who in his life propped up the house, and in his days fortified the temple. The Bible, Douay-Rheims Version |Various 

"Without doubt; true demons incarnate," replied the veracious priest. Blackwood's Edinburgh Magazine, Volume 60, No. 372, October 1846 |Various 

He now knew no bounds to his wrath; and he proclaimed it in such a manner, that the terrified priest flew before him. The Pastor's Fire-side Vol. 3 of 4 |Jane Porter 

“My own mother nearly broke her heart because I would not become a priest,” said Aristide. The Joyous Adventures of Aristide Pujol |William J. Locke