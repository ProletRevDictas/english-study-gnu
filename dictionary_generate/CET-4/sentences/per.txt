Added to drinking water at concentrations of around one part per million, fluoride ions stick to dental plaque. Anti-Fluoriders Are The OG Anti-Vaxxers |Michael Schulson |July 27, 2016 |DAILY BEAST 

During an emergency that ratio could be allowed to drop to 8.5 people per orbit. Exclusive: U.S. Drone Fleet at ‘Breaking Point,’ Air Force Says |Dave Majumdar |January 5, 2015 |DAILY BEAST 

Well over a thousand holes in, I average less than four strokes per hole. Lost For Thousands of Strokes: 'Desert Golfing' Is 'Angry Birds' as Modern Art |Alec Kubas-Meyer |January 2, 2015 |DAILY BEAST 

At least one child in CAR has been killed or gravely injured per day, and 10,000 have been recruited into militant groups. The Year’s Most Forgotten Humanitarian Crisis |Nina Strochlic |January 1, 2015 |DAILY BEAST 

Bitcoin began 2013 with a roaring price of $770 per unit, and businesses right and left were converting to the ethereal product. You Were Wrong About Miley & Bitcoin: 2014’s Failed Predictions |Nina Strochlic |December 31, 2014 |DAILY BEAST 

I doubt that thirty persons per day are carried into or brought out of it by all public conveyances whatever. Glances at Europe |Horace Greeley 

The Act permits member banks to accept an amount of bills not exceeding 50 per cent. Readings in Money and Banking |Chester Arthur Phillips 

At this period it brought enormous prices, the finest selling at from fifteen to eighteen shillings per pound. Tobacco; Its History, Varieties, Culture, Manufacture and Commerce |E. R. Billings. 

In 1205 wheat was worth 12 pence per bushel, which was cheap, as there had been some years of famine previous thereto. Showell's Dictionary of Birmingham |Thomas T. Harman and Walter Showell 

We did not talk much about the past at dinner, except—ah me, how bitterly we regretted our 10 per cent. Gallipoli Diary, Volume I |Ian Hamilton