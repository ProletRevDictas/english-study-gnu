Aksyonov has great fun inventing a culturally pureed lexicon for his might-have-been cosmopolitan Russians. This 1979 Novel Predicted Putin’s Invasion Of Crimea |Michael Weiss |May 18, 2014 |DAILY BEAST 

The divide between the haves and the have-nots is so great that the elite never feel these things directly. Boko Loco: A View From Nigeria |Lola Ogunnaike |May 9, 2014 |DAILY BEAST 

Moran's point is a good one, but the truth is that Congress is already dominated by have's and want-to-have's. These Guys Need a Raise? |Patricia Murphy |April 10, 2014 |DAILY BEAST 

It's now a must-have wardrobe staple, similar to that of the t-shirt or a pair of jeans. Diane von Furstenberg Celebrates 40 Years of the Wrap Dress |Erin Cunningham |January 14, 2014 |DAILY BEAST 

Must-have TV channels are out, and cheap work-arounds are in. The Opt-Out Economy |Daniel Gross |December 16, 2013 |DAILY BEAST 

I appeal to yourself, Madam, whether these sublime notions have-any thing consoling in them? Letters To Eugenia |Paul Henri Thiry Holbach 

Ha-ha-have my head sha-a-ved, dress myself up li-like a Turk? Jack Harkaway's Boy Tinker Among The Turks |Bracebridge Hemyng 

He did not-have to teach Mappo very much, for the monkey could already do those things. Mappo, the Merry Monkey |Richard Barnum 

No poorest man on earth would change places with this man-that-might-have-been, for his time draws nigh and his end is perdition. Raemaekers' Cartoons |Louis Raemaekers 

Has time-have my changed fortunes softened her stern determination towards me? The Knight Of Gwynne, Vol. II (of II) |Charles James Lever