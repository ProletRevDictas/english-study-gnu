Etta and Ella get into a jealous row over ownership of certain stories from their past, at a public event. Adrienne Kennedy’s new play, ‘Etta and Ella on the Upper West Side,’ is utterly unique |Peter Marks |January 14, 2021 |Washington Post 

Many suggested that Colin Meloy, the frontman of the Decemberists, might feel a little jealous — as his band created sea shanty-adjacent folk-rock tunes for years. Sea shanties are here to save us |Travis Andrews |January 13, 2021 |Washington Post 

Lunch may be jealous, but the rest of your day will thank you. BLTs Should Be a Breakfast Food |Lesley Suter |October 6, 2020 |Eater 

Like the recently released Mario Lego sets, this is the kind of toy that makes me jealous of kids today. Nintendo’s new RC Mario Kart looks terrific |Brian Heater |October 2, 2020 |TechCrunch 

Airhead also makes a training tube that can handle up to 120 pounds, so check that one out if some of the older kids start getting jealous. The best boating tubes |PopSci Commerce Team |September 3, 2020 |Popular-Science 

His acolytes, some of whom refer to themselves as “health freedom fighters” are undeterred by this sort of jealous shoptalk. Honey Boo Boo, Snake Oil, and Ebola: The Weird World of Young Living Essential Oils |Kent Sepkowitz |December 5, 2014 |DAILY BEAST 

Jealous of her young male friend who was permitted to ride his bike around shirtless, she once ripped off her top, too. Speed Read: Lena Dunham’s Most Shocking Confessions From ‘Not That Kind of Girl’ |Kevin Fallon |September 26, 2014 |DAILY BEAST 

It even makes Cersei jealous to the point where she calls her out on it at the Purple Wedding. Game of Thrones’ Gwendoline Christie on Brienne of Tarth’s Epic S4 Finale Showdown with The Hound |Marlow Stern |June 16, 2014 |DAILY BEAST 

“Rupert was horrible to Colin during the filming, very jealous at this other young person coming up,” says Mitchell. Bring ‘Another Country’ to Broadway: Why a Hit British Classic Needs Its New York Moment |Tom Teodorczuk |June 2, 2014 |DAILY BEAST 

Elle.com published a piece entitled “Why This Photo of Gisele Bündchen Breastfeeding Makes Me Jealous.” Why Do We Love Gisele Bundchen but Hate Gwyneth Paltrow? |Erin Cunningham |May 15, 2014 |DAILY BEAST 

Moreover, Napoleon, so great in many things, was so jealous of his own glory that he could be mean beyond words. Napoleon's Marshals |R. P. Dunn-Pattison 

Gordon, however, had never been a lover, and if Bernard noted Angela's gravity it was not because he felt jealous. Confidence |Henry James 

Why, Lucy has been your only serious competitor this season; I wonder you aren't jealous of each other. The Pit Town Coronet, Volume I (of 3) |Charles James Wills 

He's jealous, for he has never been past Harrisburg; but I've really gone around a little circle. The Soldier of the Valley |Nelson Lloyd 

These latter were jealous of their leader because he belonged to a different nation from themselves. Alila, Our Little Philippine Cousin |Mary Hazelton Wade