Those calls have led some politicians, including Gloria, to urge the state to prioritize vaccinations for school employees who work with young children. Morning Report: Not Much COVID Enforcement Happening Despite Crackdown |Voice of San Diego |February 12, 2021 |Voice of San Diego 

As far as I know, we haven’t rewritten the employee handbook. New York Times bails on intent-neutral standard when discussing epithets |Erik Wemple |February 11, 2021 |Washington Post 

Restrooms and coffee bars are spaced apart, he adds, encouraging employees to walk more. Tim Cook Pivots to Fitness |Michael Roberts |February 10, 2021 |Outside Online 

The Justice Department has asked a judge to dismiss the case against the controllers, who are federal employees, on procedural grounds. Helicopter pilot flying Kobe Bryant didn’t follow his training when flying into disorienting clouds, federal investigators say |Ian Duncan |February 9, 2021 |Washington Post 

An asynchronous working environment is one in which there are no fixed hours for employees. Businesses adopt ‘asynchronous working’ to fight remote-working fatigue and encourage cross-border collaboration |Jessica Davies |February 9, 2021 |Digiday 

Imagine waking up to find a guy who looks like a tech startup employee eating your charred crispy leg. The Red Viper, Zoe Barnes, and the Best Fictional Deaths of 2014 |Melissa Leon |January 1, 2015 |DAILY BEAST 

In some cases, public employee unions even pushed private sector unions to endorse Republicans. How Public Sector Unions Divide the Democrats |Daniel DiSalvo |December 29, 2014 |DAILY BEAST 

Public employee unions are a little-acknowledged driver of this conflict. How Public Sector Unions Divide the Democrats |Daniel DiSalvo |December 29, 2014 |DAILY BEAST 

In January 2014, a lifelong District of Columbia parks employee, Medric Mills, collapsed while walking with his grown daughter. Red Tape Is Strangling Good Samaritans |Philip K. Howard |December 27, 2014 |DAILY BEAST 

It said: “Tonie Tobias, Information Technology, President of GLEN, Gay and Lesbian Employee Network.” How A Company’s Support of Gay Employees Helped One of Them To Come Out | |December 24, 2014 |DAILY BEAST 

A building employee stated earlier today that Girra left the premises less than five minutes before the killing. Hooded Detective, Volume III No. 2, January, 1942 |Various 

A director of a bank is not an employee within the meaning of the acts under consideration. Putnam's Handy Law Book for the Layman |Albert Sidney Bolles 

The fact that a workman furnishes tools and materials, or undertakes to do a specified job will not prevent his being an employee. Putnam's Handy Law Book for the Layman |Albert Sidney Bolles 

An apprentice who is qualifying himself to operate an elevator is an employee within the Minnesota Act. Putnam's Handy Law Book for the Layman |Albert Sidney Bolles 

Thus, one who is employed as a workman in a sawmill on such days as it was in operation for four months was not a casual employee. Putnam's Handy Law Book for the Layman |Albert Sidney Bolles