I asked her how her trainers, born and raised in Iran, have learned how to teach hip-hop. Iran’s Becoming a Footloose Nation as Dance Lessons Spread |IranWire |January 2, 2015 |DAILY BEAST 

But when their students asked them how they could teach civics if they could not vote, they took to the streets. Dr. King Goes to Hollywood: The Flawed History of ‘Selma’ |Gary May |January 2, 2015 |DAILY BEAST 

Many dance instructors register their classes at gyms and teach women or men (separately) under the name of aerobics. Iran’s Becoming a Footloose Nation as Dance Lessons Spread |IranWire |January 2, 2015 |DAILY BEAST 

But we were attempting a deliberate naiveté, a decision to approach these books as if they might have something to teach us. American Democracy Under Threat for 250 Years |Jedediah Purdy |December 28, 2014 |DAILY BEAST 

The mission is to teach any person to use technology for independence and empowerment no matter where they are located. 3-D Printing Is Changing the Future of Prosthetics |Lucy Vernasco |December 10, 2014 |DAILY BEAST 

“It means, my dear, that the Dragoons and the 60th will have to teach these impudent rebels a much-needed lesson,” said her uncle. The Red Year |Louis Tracy 

Woe to the man that first did teach the cursed steel to bite in his own flesh, and make way to the living spirit. Pearls of Thought |Maturin M. Ballou 

Sherwood often wavers between him and Kullak, and Deppe would like to teach Sherwood if he could, simply out of interest for him. Music-Study in Germany |Amy Fay 

I've tried to teach lots of folks; an' sum learns quick, an' some don't never learn; it's jest 's 't strikes 'em. Ramona |Helen Hunt Jackson 

These strange things that Alf has been trying to teach me during the long nights I have learned—I understand. The Giant of the North |R.M. Ballantyne