For the friend who is always outside hiking, camping, skiing, and more, these socks from Smartwool will make a great gift. Socks that make great gifts |PopSci Commerce Team |September 30, 2020 |Popular-Science 

Unlike some oil-rich economies—notably Norway, which socked away its petroleum riches over the years to amass what is now a $1 trillion sovereign wealth fund—Alberta hasn’t saved much for a rainy day. After the boom: Canada’s oil capital faces an uncertain future |kdunn6 |September 21, 2020 |Fortune 

To prove to me that Boston wasn’t very cold, he would wear shorts and he would wear no socks every day, no matter how cold it was. Full Transcript: Tomi Lahren on ‘The Carlos Watson Show’ |Daniel Malloy |August 31, 2020 |Ozy 

He doesn’t know how to make any socks, but he can destroy all that expertise. Unmade in America |Tate Ryan-Mosley |August 14, 2020 |MIT Technology Review 

As Clark put it, “We still have 330 million people in this country, most of whom wear socks, but Walmart couldn’t find anybody who made socks in America.” Unmade in America |Tate Ryan-Mosley |August 14, 2020 |MIT Technology Review 

Two and a half years ago this was just a sock, underwear and a lounge kind of company. The Hot Designer Who Hates Fashion: VK Nagrani Triumphs His Own Way |Tom Teodorczuk |December 1, 2014 |DAILY BEAST 

Based on his sock puppet, I expected him to be a burly bearded giant clad in plaid—basically, a Canadian Paul Bunyan. Canada’s Subversive Sock Puppet: Ed the Sock Isn’t Afraid to Say Anything |Soraya Roberts |November 13, 2014 |DAILY BEAST 

A food court in a suburban mall seemed like a good place to meet Ed the Sock. Canada’s Subversive Sock Puppet: Ed the Sock Isn’t Afraid to Say Anything |Soraya Roberts |November 13, 2014 |DAILY BEAST 

Still, Ed the Sock is more active on Twitter these days, where he has more than 14 thousand followers. Canada’s Subversive Sock Puppet: Ed the Sock Isn’t Afraid to Say Anything |Soraya Roberts |November 13, 2014 |DAILY BEAST 

The duo first met in 1997 when Kerzner was playing Ed the Sock live. Canada’s Subversive Sock Puppet: Ed the Sock Isn’t Afraid to Say Anything |Soraya Roberts |November 13, 2014 |DAILY BEAST 

The boy was hurt; my heart went out to him, for the memory of my own sock-ball and tickley-bender days came back to me. The Soldier of the Valley |Nelson Lloyd 

"I was playin' sock-ball," snuffled the boy, and a solitary tear rolled down his snub nose. The Soldier of the Valley |Nelson Lloyd 

In spite of all they've spiled, I'd be nigh $500 ahead o' the game if I could git out o' camp with what I've got in my sock. Si Klegg, Book 2 (of 6) |John McElroy 

"Fudge on your everlasting knitting," said Sal, snatching the sock from Mary's hands and making the needles fly nimbly. The English Orphans |Mary Jane Holmes 

A service-sock requires three skeins of knitting-yarn for two pairs, with No. 11 steel needles. Handbook of Wool Knitting and Crochet |Anonymous