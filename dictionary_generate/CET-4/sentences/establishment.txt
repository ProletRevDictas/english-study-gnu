He wouldn’t have taken those positions if he didn’t think they were viable as priorities with the party establishment. America needs a democratic revolution |Matthew Yglesias |September 17, 2020 |Vox 

If my employees had more money, I reasoned, they’d have more money to spend at those same establishments. How we can save small business from coronavirus-induced extinction |matthewheimer |September 10, 2020 |Fortune 

They’ve championed a surveillance ordinance and the establishment of a privacy advisory commission that would vet technology and its impacts before being considered by the City Council. Morning Report: A Questionable Stat That’s Guiding Reopening |Voice of San Diego |September 9, 2020 |Voice of San Diego 

Meanwhile, Kennedy’s endorsers include establishment heavyweights such as House Speaker Nancy Pelosi. Today’s Elections In Massachusetts Are Another Big Test For The Progressive Movement |Nathaniel Rakich (nathaniel.rakich@fivethirtyeight.com) |September 1, 2020 |FiveThirtyEight 

You can visit establishments like Axis Nightclub and Level Dining Lounge. Columbus: A Rich History of LGBTQ Diversity |LGBTQ-Editor |August 30, 2020 |No Straight News 

Satirists occupy a perilous position—to skewer dogma and cant, and to antagonize the establishment while needing its protection. Harry Shearer on The Dangerous Business of Satire |Lloyd Grove |January 8, 2015 |DAILY BEAST 

Satirists are reliant ultimately on the very establishment they mock. Harry Shearer on The Dangerous Business of Satire |Lloyd Grove |January 8, 2015 |DAILY BEAST 

Two factors made Hiram Revels especially interesting to the Washington establishment. The Black Man Who Replaced Jefferson Davis in the Senate |Philip Dray |January 7, 2015 |DAILY BEAST 

Ironically, the play deals with the ‘management’ of information by the Establishment. Harry’s Daddy, and Diana’s ‘Murder’: Royal Rumors In a New Play |Tom Sykes |January 4, 2015 |DAILY BEAST 

Before his writing days, London used the Oakland establishment to conduct his studies. The Bars That Made America Great |Nina Strochlic |December 28, 2014 |DAILY BEAST 

Mrs. Wurzel was quite right; they had been supplied, regardless of cost, from Messrs. Rochet and Stole's well-known establishment. The Pit Town Coronet, Volume I (of 3) |Charles James Wills 

We had half a dozen passengers to Ferrara; for the rest of the way, I had this extensive traveling establishment to myself. Glances at Europe |Horace Greeley 

On the establishment of the Empire Berthier, like many another, received the reward for his faithfulness to Napoleon. Napoleon's Marshals |R. P. Dunn-Pattison 

If schooling is a training in expression and communication, college is essentially the establishment of broad convictions. The Salvaging Of Civilisation |H. G. (Herbert George) Wells 

Orlean had secured a position in a ladies' tailoring establishment at five dollars and fifty cents a week, and there he went. The Homesteader |Oscar Micheaux