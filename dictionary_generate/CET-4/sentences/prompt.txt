That prompted grumbling from industry competitors, but Trek’s claims went largely unexamined, and the controversy faded. The Trek WaveCel Helmet Lawsuit, Explained |Joe Lindsey |February 10, 2021 |Outside Online 

This has prompted some to suggest we should instead create a distributed network of smaller quantum computers that can work together to simulate a larger one. Connecting Distant Qubits Just Brought Distributed Quantum Computing Closer |Edd Gent |February 8, 2021 |Singularity Hub 

They are often used as a call-to-action as they might prompt a viewer to read or learn more about a topic by clicking on the internal link, or else ask a viewer to contact you or schedule a visit. Seven enterprise SEO strategies and tactics that really work |Harpreet Munjal |February 8, 2021 |Search Engine Watch 

That belief prompted two teachers unions to call for her removal from the House Education Committee — one of her new committee assignments. House punishes Republican lawmaker who promoted violent conspiracy theories |Taylor Hatmaker |February 5, 2021 |TechCrunch 

Several clusters of cases related to a new viral variant have emerged in his area, prompting schools to delay their start dates. We May Never Eliminate COVID-19. But We Can Learn to Live With It |Jamie Ducharme |February 4, 2021 |Time 

God has to continually prompt and prod him, and puts his brother Aaron at his side to do most of the real leadership. Meet Moses the Swashbuckling Israelite |James Romm |December 14, 2014 |DAILY BEAST 

Fears that the plot was in the final stages helped prompt U.S. airstrikes against al Qaeda positions in Syria. U.S. Clams Up on Xmas Airline Bomb ‘Plot’ |Shane Harris |December 2, 2014 |DAILY BEAST 

For most people, just saying the name Manson is enough to prompt discomfort. Mrs. Manson, Hometown Antihero |Justin Glawe |November 24, 2014 |DAILY BEAST 

These poll numbers again prompt the question: How could Al Gore lose in 2000?! We've Been on the Wrong Track Since 1972 |Dean Obeidallah |November 7, 2014 |DAILY BEAST 

Mamoon and his second wife, Liana, hope it will revive his reputation, and “prompt the reissuing of his books in forty languages.” A Novel About a Novelist ‘Like’ Naipaul |Edward Platt |November 6, 2014 |DAILY BEAST 

Whatever can be done to bring about the prompt and effective use of this new system of bank settlement will be done. Readings in Money and Banking |Chester Arthur Phillips 

It is a notable fact that under the wholly unusual circumstances prevailing, the recovery was so prompt and effective. Readings in Money and Banking |Chester Arthur Phillips 

Perhaps their course is wiser than that which hot impatience would prompt—nay, I believe it is. Glances at Europe |Horace Greeley 

A little practice makes the pupil prompt in dealing with any figures whatever. Assimilative Memory |Marcus Dwight Larrowe (AKA Prof. A. Loisette) 

They have now (I suppose) reaped the harvest thereof, except that of the trees they planted, which are not so prompt in bearing. The Jesuit Relations and Allied Documents, Vol. II: Acadia, 1612-1614 |Various