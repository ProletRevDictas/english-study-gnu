Yet one can almost forget there’s a pandemic going on in many parts of the continent. The new Covid-19 case surge in Europe, explained |Julia Belluz |September 17, 2020 |Vox 

That’s enough time to read the directions repeatedly, forget the paperwork on your kitchen table three times, and still manage to deliver it on your way to the grocery store with weeks to spare. Why you should vote as early as possible (and how to do it) |John Kennedy |September 17, 2020 |Popular-Science 

I think people forget sometimes that most of this legislative activity has been concentrated on public and, more specifically, on police use. Eight case studies on regulating biometric technology show us a path forward |Karen Hao |September 4, 2020 |MIT Technology Review 

After all, if we put the consumer psychology lens in place, we mustn’t forget that people are more prone to evaluate essential and non-essential goods and services for daily use right now, showing more risk-aversion than usual. How would an SEO agency be built today? Part 1: Consumers and trends |Sponsored Content: SEOmonitor |September 4, 2020 |Search Engine Land 

You’re talking about people—many people—who didn’t have computers, forget about laptops. Podcast: How a 135-year-old law lets India shutdown the internet |Anthony Green |September 2, 2020 |MIT Technology Review 

The plan is to stretch it out as long as possible, then probably forget about it, and then suddenly remember it. ‘Archer’ Creator Adam Reed Spills Season 6 Secrets, From Surreal Plotlines to Life Post-ISIS |Marlow Stern |January 8, 2015 |DAILY BEAST 

And for those on the Palestinian right who still dream of driving the Jews into the sea, they too can forget it. In the Middle East, the Two-State Solution Is Dead |Dean Obeidallah |January 2, 2015 |DAILY BEAST 

But for those on the Israeli right who are hoping that this deferred dream will just fade away, they can forget it. In the Middle East, the Two-State Solution Is Dead |Dean Obeidallah |January 2, 2015 |DAILY BEAST 

If we go another year without doing one people will just forget what it was. Deer Tick's John McCauley on Ten Years in Rock and Roll |James Joiner |January 2, 2015 |DAILY BEAST 

Forget those silly “games played with the ball”; they are far “too violent for the body and stamp no character on the mind.” Forget the Resolutions; Try a Few Declarations |Kevin Bleyer |January 1, 2015 |DAILY BEAST 

On the morning after Ramona's disappearance, words had been spoken by each which neither would ever forget. Ramona |Helen Hunt Jackson 

Alone Orlean lay trying vainly to forget something—something that stood like a spectre before her eyes. The Homesteader |Oscar Micheaux 

Forget it not: for there is no returning, and thou shalt do him no good, and shalt hurt thyself. The Bible, Douay-Rheims Version |Various 

She would never forget it; but realizing its gravity, she decided thereupon never to tell it—the dream—to anybody. The Homesteader |Oscar Micheaux 

She would sometimes gather them passionately to her heart; she would sometimes forget them. The Awakening and Selected Short Stories |Kate Chopin