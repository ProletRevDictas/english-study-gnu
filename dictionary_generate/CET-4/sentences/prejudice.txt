They are flesh-and-blood evidence of the ways in which our prejudices and stereotypes hinder the economic stability of the hardest workers and professional advancement of some of this country’s most talented residents. The sound of a shifting power structure |Robin Givhan |January 13, 2021 |Washington Post 

The duke, the lady and the baby-face queen — these characters’ struggles are not framed by slavery or prejudice. How ‘Bridgerton’ flipped the script on ‘The Duke and I’ |Vanessa Riley |January 12, 2021 |Washington Post 

As Hinds and other critics pointed out, the show also explicitly references slavery — so the “fantasy” of this 1813 is still anchored in the reality of systemic prejudice. The debate over Bridgerton and race |Aja Romano |January 7, 2021 |Vox 

For once, death, and the death in life of prejudice, could claim nothing but the skeleton of an old man. Satchel Paige was one of baseball’s best. It didn’t take an announcement to know that. |Thomas M. Boswell |December 18, 2020 |Washington Post 

Quantum mechanics needs no particular interpretation if it is formulated without the preexisting prejudice that nature should exhibit cause-and-effect determinism. Top 10 questions I’d ask an alien from the Galactic Federation |Tom Siegfried |December 9, 2020 |Science News 

I do, however, intend it to sound mean about the reactionary, prejudice-infested place she comes from. Dems, It’s Time to Dump Dixie |Michael Tomasky |December 8, 2014 |DAILY BEAST 

A few days ago, he criticized his home state of Alabama for its entrenched prejudice. Tim Cook: Why ‘I’m Gay’ Isn’t Enough |Tim Teeman |October 30, 2014 |DAILY BEAST 

But the exemption was also born of prejudice and discrimination. Finally, Home Care Workers Start Fighting Back |Monica Potts |October 19, 2014 |DAILY BEAST 

So specious, in fact, that they are increasingly seen to be rationales to cover outdated forms of prejudice. Catholic University’s Harvey Milk Ban Reflects A Church In Transition |Jay Michaelson |October 3, 2014 |DAILY BEAST 

If The Biggest Loser could correct this misconception, it would do a lot to reduce anti-obesity prejudice. ‘The Biggest Loser’ Could Be TV’s Most Important Show Ever |Daniela Drake |September 26, 2014 |DAILY BEAST 

Finally, let me ask the general reader to put aside all prejudice, and give both sides a fair hearing. God and my Neighbour |Robert Blatchford 

Thou fell spirit of pride, prejudice, ignorance, and mauvaise honte! Blackwood's Edinburgh Magazine, Volume 60, No. 372, October 1846 |Various 

It is beyond the comprehension of any man not blinded by superstition, not warped by prejudice and old-time convention. God and my Neighbour |Robert Blatchford 

The last vestige of her prejudice against Indians had melted and gone, in the presence of their simple-hearted friendliness. Ramona |Helen Hunt Jackson 

With Monsieur de Lussigny,” he interposed, “it is a matter of prejudice, not of principle. The Joyous Adventures of Aristide Pujol |William J. Locke