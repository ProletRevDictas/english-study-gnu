Police did not immediately identify the security guard or say whether he worked for a private company contracted by the restaurant chain. Security guard at Chick-fil-A shoots armed robbery suspect in Northeast Washington, police say |Peter Hermann |February 26, 2021 |Washington Post 

Disruptions to one link of that supply chain, say steel manufacturing in Turkey, ripple throughout. Global inequity in COVID-19 vaccination is more than a moral problem |Jonathan Lambert |February 26, 2021 |Science News 

It’s really bringing automation that replaces the factory fixed tooling, supply chains, hundreds of thousands of parts, manual labor and slow iteration speed, with something that I believe is needed for the future on Earth, too. Relativity Space unveils plans for a new, much larger and fully reusable rocket |Darrell Etherington |February 26, 2021 |TechCrunch 

They also saw that even though new C2C retail models, like social commerce, are gaining popularity, the beauty industry’s supply chain hasn’t kept up. Singapore-based Raena gets $9M Series A for its pivot to skincare and beauty-focused social commerce |Catherine Shu |February 26, 2021 |TechCrunch 

Other chains might have edgier marketing or more innovative menus. Where does the new McDonald’s chicken sandwich rank? Turns out, the Arches fall flat. |Emily Heil |February 25, 2021 |Washington Post 

You expect soldiers of all ranks to understand the need to respect the chain of command, regardless of personal feelings. We Need Our Police to Be Better Than This |Nick Gillespie |December 31, 2014 |DAILY BEAST 

Say what you want about Abercrombie & Fitch (A&F), the chain has done something special. Muslims & Jews Unite vs. Abercrombie & Fitch |Dean Obeidallah |December 16, 2014 |DAILY BEAST 

After the scanning takes place, KSM is led down a long corridor flanked by chain-link fences. 9/11 Mastermind Is Afraid of the Ladies |Tim Mak |December 16, 2014 |DAILY BEAST 

Elderly women played Triple Double Diamond and Tiki Magic while they chain-smoked. I Watched a Casino Kill Itself: The Awful Last Nights of Atlantic City’s Taj Mahal |Olivia Nuzzi |December 8, 2014 |DAILY BEAST 

So while mourning the closing of De Robertis, consider that we might someday mourn the bankruptcy of whatever chain replaces it. De Robertis, a New York Great, Bids Farewell |Lizzie Crocker |December 4, 2014 |DAILY BEAST 

She stood, in her young purity, at one end of the chain of years, and Mrs. Chepstow—did she really stand at the other? Bella Donna |Robert Hichens 

Then he closed the spring with a snap, and she let him pass the chain over her hand once more. Rosemary in Search of a Father |C. N. Williamson 

From the ceiling, which was divided into compartments painted in dark red and blue, hung a heavy lamp by a chain of gilded silver. Bella Donna |Robert Hichens 

An amount of slack in the chain caused the balls to knock on passing this roller before entering the pump bottom. Life of Richard Trevithick, Volume II (of 2) |Francis Trevithick 

The large size of the rag-wheel gave the rapidly revolving chain and balls a great speed. Life of Richard Trevithick, Volume II (of 2) |Francis Trevithick