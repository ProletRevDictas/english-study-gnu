Rolled-up socks and laundry baskets have replaced balls and nets. Kids are shooting hoops with rolled up socks, but pandemic physical education is not canceled |Kelly Field |February 12, 2021 |Washington Post 

The team is coming off a 4-12 season and has hired Nick Caserio as its general manger and David Culley as its coach to replace Bill O’Brien, who was fired from both roles in October. J.J. Watt will be released by Houston Texans at his request |Mark Maske |February 12, 2021 |Washington Post 

If you don’t love it, weigh the cost of replacing it against starting over. Replacing pieces of flatware or china can be a challenge. Here’s how to track them down. |Laura Daily |February 11, 2021 |Washington Post 

One way to practice this, she said, is by creating new hope to replace the hope you’ve lost. Politics and conspiracy theories are fracturing relationships. Here’s how to grieve those broken bonds. |Jeff Schrum |February 11, 2021 |Washington Post 

Riders of Metro’s Blue Line will need to seek alternatives starting Saturday as the transit agency shuts the line down for three months to replace platforms at two stations. Two Blue Line Metro stations to close through late May for platform work |Justin George |February 11, 2021 |Washington Post 

He also wants to “replace every existing organism with a better one.” Design Your Own Dinosaur: The Era of Custom DNA |Justin Jones |January 8, 2015 |DAILY BEAST 

But this year, instead of simply voting against Boehner on Tuesday, at least two members of the group are vying to replace him. The YOLO Caucus' New Cry for Attention |Ben Jacobs |January 4, 2015 |DAILY BEAST 

It has grown from a rotten root—striving to replace human judgment with detailed dictates. Red Tape Is Strangling Good Samaritans |Philip K. Howard |December 27, 2014 |DAILY BEAST 

Because this food source could abruptly disappear at any time, cutworm moths cannot be counted on to replace pine nuts. What It Takes to Kill a Grizzly Bear |Doug Peacock |November 23, 2014 |DAILY BEAST 

But Klein offers nothing specific to replace that structurelessness. Naomi Klein’s ‘This Changes Everything’ Will Change Nothing |Michael Signer |November 17, 2014 |DAILY BEAST 

When cool, replace the acid with water, and examine for hemin crystals with two-thirds and one-sixth objectives. A Manual of Clinical Diagnosis |James Campbell Todd 

New and feverish desires for luxuries replace each older want as satisfied. The Unsolved Riddle of Social Justice |Stephen Leacock 

Extra howitzers will be most useful to replace pieces damaged by enemy batteries on the Asiatic side of the Dardanelles. Gallipoli Diary, Volume I |Ian Hamilton 

Another would replace him, and there is little to choose among the men that garrison Condillac. St. Martin's Summer |Rafael Sabatini 

The old servant now and again crept in on soundless feet to replace with a freshly heated bottle of sak the one grown cold. The Dragon Painter |Mary McNeil Fenollosa