More important, to save bee populations, Stamets’s solution has to be widely adopted, a feat he hopes to accomplish by recruiting the efforts of millions of citizen scientists. The Fungal Evangelist Who Would Save the Bees - Issue 90: Something Green |Merlin Sheldrake |September 23, 2020 |Nautilus 

If companies are serious about accomplishing these goals, it’s crucial that they first understand the common reasons that fuel the participation gap so they can tailor initiatives accordingly. How Outdoor Companies Can Back Up Their DEI Pledges |Kai Lightner |September 23, 2020 |Outside Online 

The US has the resources, physical and financial, to accomplish this transformation. The case for a billion Americans |Tim Fernholz |September 19, 2020 |Quartz 

Criminal prosecution and civil litigation accomplish different goals. The policing reforms in the Breonna Taylor settlement, explained |Fabiola Cineas |September 17, 2020 |Vox 

One way to accomplish that is by opening the doors to telemedicine at a global scale. Why the coronavirus pandemic has made 5G more essential than ever |jakemeth |September 7, 2020 |Fortune 

They were able to move forward, accomplish goals, and meet friends. Inside A Finishing School for Transwomen |Sharon Adarlo |December 27, 2014 |DAILY BEAST 

Like I said, in spite of or because of my circumstances, I was able to accomplish my dreams. Tim Howard’s Wall of Intensity |William O’Connor |December 22, 2014 |DAILY BEAST 

And yet, ultimately, the Supreme Court holds the power to uphold or undo what it has taken him years to accomplish. From POTUS to SCOTUS: Obama’s Big Move? |Keli Goff |November 17, 2014 |DAILY BEAST 

It took a special, meticulous kind of person to accomplish the undertaking, someone with brains, patience, and nerves of steel. The High Society Bank Robber of the 1800s |J. North Conway |October 19, 2014 |DAILY BEAST 

Leaving the body consciously is a feat only a fully liberated master with no more karma can accomplish. When Gary Wright Met George Harrison: Dream Weaver, John and Yoko, and More |Gary Wright |September 29, 2014 |DAILY BEAST 

If we can free this State of Yankees, we will accomplish more than your armies down south have. The Courier of the Ozarks |Byron A. Dunn 

Jacob cheated his brother out of the parental blessing, and lied about God, and lied to his father to accomplish his end. God and my Neighbour |Robert Blatchford 

Steam machinery would accomplish more than nine-tenths of all the work, besides saving the expense of all the powder. Life of Richard Trevithick, Volume II (of 2) |Francis Trevithick 

Human desires would eat up the result of ten times the work we now accomplish. The Unsolved Riddle of Social Justice |Stephen Leacock 

That I may accomplish the oath which I swore to your fathers, to give them a land flowing with milk and honey, as it is this day. The Bible, Douay-Rheims Version |Various