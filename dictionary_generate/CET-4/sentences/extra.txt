To avoid unnecessary cleaning or extra touch points, Riggs has cleared the room of some hotel frills. Now that’s room service: What it’s like to check into a hotel just for dinner |Tom Sietsema |February 12, 2021 |Washington Post 

When someone offers you an extra fresh mask they brought, even if you already have one. What’s sexy in a pandemic? Caution. |Lisa Bonos |February 12, 2021 |Washington Post 

A quick, extra hot sear plus a low and slow roast in the oven produces a luxurious steak. This Valentine’s Day, we couldn’t help but wonder, which ‘Sex and the City’ character will you most eat like? |Kari Sonde |February 11, 2021 |Washington Post 

Subscribers can pay more for additional service, but some cannot afford the extra costs of better connectivity and the benefits it provides, such as the ability to stream video conversations through Zoom and other tools. Lacking a Lifeline: How a federal effort to help low-income Americans pay their phone bills failed amid the pandemic |Tony Romm |February 9, 2021 |Washington Post 

February is National Bird-Feeding Month, usually a time for celebrating feathered creatures and providing extra food and water to get them through the winter. Your dirty bird feeder could be spreading disease |Melissa Hart |February 9, 2021 |Washington Post 

But one extra trick would instantly solve the problem of crashes that occur over water. Red Tape and Black Boxes: Why We Keep ‘Losing’ Airliners in 2014 |Clive Irving |December 29, 2014 |DAILY BEAST 

Extra security was also set up along the lines to monitor other signs of potential sabotage. Italy’s Terror on the Tracks |Barbie Latza Nadeau |December 28, 2014 |DAILY BEAST 

Extra dry, for example, is actually sweeter than brut, which is drier than demi-sec, which is somewhat sweet. Champagne: You’re Drinking It All Wrong |Kayleigh Kulp |December 20, 2014 |DAILY BEAST 

The constitutional problem with this ruling, experts say, is that it places an extra burden on women for being pregnant. States Slap Pregnant Women With Harsher Jail Sentences |Emily Shire |December 12, 2014 |DAILY BEAST 

Bershin, who worked with the Ukrainian police before rebels took control of Donetsk, says officers have to be extra vigilant. The Corrupt Cops of Rebel-Held East Ukraine |Kristina Jovanovski |December 11, 2014 |DAILY BEAST 

Of course, newly acquired Ferns will pay for extra attention in the way of watering until they have secured a proper roothold. How to Know the Ferns |S. Leonard Bastin 

Harry was Aunty Rosa's one child, and Punch was the extra boy about the house. Kipling Stories and Poems Every Child Should Know, Book II |Rudyard Kipling 

In the evening the little theatre is illuminated regardless of expense, a fabulous sum being expended on extra lamps. The Pit Town Coronet, Volume I (of 3) |Charles James Wills 

Near noon I found a place where they'd cached two extra horses in the brush on Sage Creek. Raw Gold |Bertrand W. Sinclair 

An extra 50,000 men to feed war-trained units already in the field is another, and very different, and very much better thing. Gallipoli Diary, Volume I |Ian Hamilton