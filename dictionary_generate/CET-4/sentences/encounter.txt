A new opinion paper assesses one way in which we might be able to learn from our encounters with smallpox. Wearing a mask could protect you from COVID-19 in more ways than you think |Kat Eschner |September 10, 2020 |Popular-Science 

Walker’s deputy chief of staff at the time, Jang, said Potts told her just before the encounter that she was expecting to meet with Walker as well as Mallott. The Woman Propositioned by Alaska’s Former Lieutenant Governor Tells Her Story for the First Time |by Kyle Hopkins and Michelle Theriault Boots, Anchorage Daily News |September 10, 2020 |ProPublica 

The most touching things were the encounters I would have … when a kid with so much on the line quietly comes out to you while shaking your hand, passes you a note, and you’re just thinking about what that kid might be up against. Pete Buttigieg Says Dems Did Not Coordinate to Seal Biden’s Primary Win |Nick Fouriezos |September 9, 2020 |Ozy 

Texas Governor Greg Abbott, Texas Senator Ted Cruz and Louisiana Senator John Kennedy, all Republicans, wore masks in encounters with the president in the last week. Trump and his top aides have stopped wearing masks after a brief effort |Rachel Schallom |September 3, 2020 |Fortune 

Several police groups issued a statement ahead of the hearing saying the board would invite “members whose qualifications are inherently based on negative encounters with peace officers.” Sacramento Report: Jones, COVID-19 and the Irony of Remote Voting |Sara Libby and Jesse Marx |August 28, 2020 |Voice of San Diego 

Hatuey replied that he would rather burn and be sent to hell than ever again encounter people as cruel as the Spanish. The Life and Hard Times Of The Family A Cuban Defector Left Behind |Brin-Jonathan Butler |December 19, 2014 |DAILY BEAST 

This is heartfelt music, and after a first encounter you will find yourself coming back for more. The Best Albums of 2014 |Ted Gioia |December 13, 2014 |DAILY BEAST 

Instead, she says other people she has shared the story with described the sexual encounter as rape. The Right's Rape Trolls vs. Lena Dunham |Emily Shire |December 10, 2014 |DAILY BEAST 

Outside of that one encounter, however, Moses is pretty meek. Christian Bale: One Man's Moses Is Another Man's Terrorist |Candida Moss, Joel Baden |December 7, 2014 |DAILY BEAST 

What if healthy sexuality was the framework that young adults used to process every sexual message that they encounter? A Rallying Cry Against the Oversexualization of Our Youth |Darryl Roberts |November 30, 2014 |DAILY BEAST 

All along the highways and by-paths of our literature we encounter much that pertains to this "queen of plants." Tobacco; Its History, Varieties, Culture, Manufacture and Commerce |E. R. Billings. 

If now we turn to the higher aspects of form, such as symmetry and proportion, we encounter a difficulty. Children's Ways |James Sully 

During his mild régime the insurrection increased rapidly, and in one encounter he himself was very near falling a prisoner. The Philippine Islands |John Foreman 

Following the boys, Baptiste entered by the kitchen door to encounter the mother and three daughters preparing the meal. The Homesteader |Oscar Micheaux 

Monsieur Quérin saluted and declared himself enchanted at the encounter. The Joyous Adventures of Aristide Pujol |William J. Locke