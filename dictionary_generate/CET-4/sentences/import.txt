China has fallen behind in its purchases of American agricultural products and energy imports. Jefferies strategist expects China to fulfill its U.S. trade deal commitments for an unexpected reason |Veta Chan |August 27, 2020 |Fortune 

The resulting production slowdown caused bottlenecks for all of sorts of imports. Why the US can’t stock dumbbells fast enough |Jenni Avins |August 26, 2020 |Quartz 

The import reduction is due to three main factors, according to spirits industry veteran Adam Levy. Covid-19 and a trade war are a deadly mix for US liquor imports |Dan Kopf |August 21, 2020 |Quartz 

India’s smart move to tackle this dual problem was to launch a policy which could reduce the crude oil import along with handling the environmental crisis. How an Indian flight could run on biofuel—but India cannot |Monika Mandal |August 11, 2020 |Quartz 

The company also announced new initiatives to automate data imports into Google Ads. Google extends lead forms to YouTube, Discovery campaigns |Ginny Marvin |August 5, 2020 |Search Engine Land 

The import of those words resonated through my entire being. When Gary Wright Met George Harrison: Dream Weaver, John and Yoko, and More |Gary Wright |September 29, 2014 |DAILY BEAST 

CEO Mark Thompson, a solidly-built, 6-foot-2 import from the BBC. Dean Baquet, the NYT’s Executive Editor, on Jill Abramson, Race, Surviving Cancer—and TMZ Envy |Lloyd Grove |September 16, 2014 |DAILY BEAST 

Is it possible that Zima might even make a comeback in the U.S.A, perhaps as some sort of exotic import? In Japan, Zima Haz No Zexual Preference |Jake Adelstein, Angela Erika Kubo |September 13, 2014 |DAILY BEAST 

How the Export-Import Bank” became a target “for Tea Party wrath is a little strange to me. Lobbyist Derangement Syndrome Sweeps DC |James Poulos |August 8, 2014 |DAILY BEAST 

Earlier this year, a Long Island man admitted to trying to import 40,000 piranhas from Hong Kong. The $10 Billion Pet Cheetah and Chimp Industry |Sharon Adarlo |July 20, 2014 |DAILY BEAST 

When you next hear of, or see Philip Wharton, you will understand the import of your own words. The Pastor's Fire-side Vol. 3 of 4 |Jane Porter 

I add nothing to the “Extremes,” import nothing from abroad in regard to them, invent nothing. Assimilative Memory |Marcus Dwight Larrowe (AKA Prof. A. Loisette) 

Porson smoked many bundles of cheroots, which nabobs began to import. Tobacco; Its History, Varieties, Culture, Manufacture and Commerce |E. R. Billings. 

The verb (—) in the Hebrew, when connected with the name of God in different other passages, has the same import. The Ordinance of Covenanting |John Cunningham 

If performed to God, it is, according to the import of the expression confessing to him, to Covenant. The Ordinance of Covenanting |John Cunningham