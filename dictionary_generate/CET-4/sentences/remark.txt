Northam made the remarks at an afternoon news briefing on the coronavirus pandemic. Trump, in town hall, says he wouldn’t have done anything differently on pandemic |Colby Itkowitz, Josh Dawsey, Felicia Sonmez, John Wagner |September 16, 2020 |Washington Post 

In March, she made a similar remark, only this time specifying that all 3,000 SIAs were stopped at the southern border. What to make of the DHS whistleblower’s shocking complaint |Alex Ward |September 11, 2020 |Vox 

In a phone interview, Jang remembered being surprised by the remark. The Woman Propositioned by Alaska’s Former Lieutenant Governor Tells Her Story for the First Time |by Kyle Hopkins and Michelle Theriault Boots, Anchorage Daily News |September 10, 2020 |ProPublica 

Putting aside Drew Brees’s advanced age — and the chemistry fallout from his ill-advised remarks in June — he still ranks as the second-best QB in the league and leads one of the NFL’s best groups of offensive weapons. What To Watch For In An Abnormally Normal 2020 NFL Season |Neil Paine (neil.paine@fivethirtyeight.com) |September 9, 2020 |FiveThirtyEight 

Last summer, when I told friends and family that I’d be rafting 225 miles from Lee’s Ferry to Diamond Creek, Arizona, on the Colorado River, I received all kinds of remarks. The Gear You Need to Bring on a 225-Mile River Trip |Mitch Breton |September 6, 2020 |Outside Online 

The remark comes to mind while reading The Selected Letters of Norman Mailer. Mailer’s Letters Pack a Punch and a Surprising Degree of Sweetness |Ronald K. Fried |December 14, 2014 |DAILY BEAST 

He always claimed that “in England everyone looks as I do, and no one would remark on it.” Alfred Hitchcock’s Fade to Black: The Great Director’s Final Days |David Freeman |December 13, 2014 |DAILY BEAST 

I would have asked for his name, if not for the comment that followed the concerned remark. Mrs. Manson, Hometown Antihero |Justin Glawe |November 24, 2014 |DAILY BEAST 

House Speaker John Boehner condemned the remark, but the damage was done. The Republican Rainbow Coalition Is Real |Tim Mak |November 18, 2014 |DAILY BEAST 

And the second remark came during the Japanese internment conversation. The Fox News Apology Tour |Dean Obeidallah |October 1, 2014 |DAILY BEAST 

"Capital, capital," his lordship would remark with great alacrity, when there was no other way of escape. The Pit Town Coronet, Volume I (of 3) |Charles James Wills 

He obeyed without remark, though with an unsteady voice, as he uttered communications he knew were so hostile to her expectation. The Pastor's Fire-side Vol. 3 of 4 |Jane Porter 

When Grandfather Mole overheard Mrs. Robin making such a remark he would quite likely advise her to "try a smaller one." The Tale of Grandfather Mole |Arthur Scott Bailey 

I made some remark to Masters which led to another from him, and in five minutes' time we were chatting on all sorts of topics. Uncanny Tales |Various 

He made no further remark as they descended the darker section of the stair, and she could think of nothing to say to him. Ancestors |Gertrude Atherton