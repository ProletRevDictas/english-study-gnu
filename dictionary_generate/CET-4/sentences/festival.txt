Another Goldenvoice festival, the country-music-oriented Stagecoach, was also delayed. How the coronavirus outbreak is roiling the film and entertainment industries |Alissa Wilkinson |September 11, 2020 |Vox 

While 2020 has been a veritable festival for the financing vehicle, with over $32 billion raised so far, some venture capitalists that Term Sheet has spoken to have voiced doubts about the structure. A SPAC courts Airbnb |Lucinda Shen |September 3, 2020 |Fortune 

Now the restaurant is in limbo as the town decides whether or not to cancel this year’s pumpkin festival. Pick Your Poison |Nick Mancall-Bitel |September 3, 2020 |Eater 

Held every June, Columbus Pride is a three-day event that includes a parade, festival, and Pride brunch. Columbus: A Rich History of LGBTQ Diversity |LGBTQ-Editor |August 30, 2020 |No Straight News 

The festival awards a Golden Bear for the best film and a series of Silver Bears, which until this year included best actor and best actress honors. Berlin Film Festival will make acting prizes gender neutral next year |radmarya |August 24, 2020 |Fortune 

The ancient Egyptian festival of Wepet Renpet (“opening of the year”) was not just a time of rebirth—it was dedicated to drinking. New Year’s Eve, Babylon Style |Candida Moss |December 31, 2014 |DAILY BEAST 

The only repercussions Iraqi has faced are outside of Egypt, as she lost her place in an upcoming Swiss Film Festival, he added. Sisi Is Persecuting, Prosecuting, and Publicly Shaming Egypt’s Gays |Bel Trew |December 30, 2014 |DAILY BEAST 

Check out a clip from this exclusive interview with SCAD President and Founder Paula Wallace at the 2014 Savannah Film Festival. Who Is Joe Manganiello? Sofia Vergara’s Fiancé on the Value of Hard Work |The Daily Beast Video |December 29, 2014 |DAILY BEAST 

Like Edgar, he remembers a unique time when American rappers came down and performed at the Primer Festival de Rap Cubano. Cuban Hip-Hop Was Born in Alamar |Daniel Levin |December 26, 2014 |DAILY BEAST 

A big cake requires a big festival, and Augustus was happy to comply. One Cake to Rule Them All: How Stollen Stole Our Hearts |Molly Hannon |December 24, 2014 |DAILY BEAST 

Signer Costa was at the Festival in 1829, and he afterwards appeared on the stage at the Royal. Showell's Dictionary of Birmingham |Thomas T. Harman and Walter Showell 

On the 2nd of November I saw a festival of another description—namely, a religious one. A Woman's Journey Round the World |Ida Pfeiffer 

Musical festival in Westminster abbey, in commemoration of the birthday of Handel. The Every Day Book of History and Chronology |Joel Munsell 

The repetition of her festival may possibly point to separate celebrations of the communities of Palatine and Quirinal. The Religion of Ancient Rome |Cyril Bailey 

Of the Feralia of February 21, the culmination of the festival of the kindred dead (Parentalia), we have already spoken. The Religion of Ancient Rome |Cyril Bailey