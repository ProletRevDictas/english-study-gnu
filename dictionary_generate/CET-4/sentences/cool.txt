So, the water cooled in the salt slush was definitely colder than the water cooled in simple ice. Build ice towers with bottled water and ice |Bethany Brookshire |September 16, 2020 |Science News For Students 

According to a Monday blog post from Microsoft, the consistently cool underwater temperatures made it possible to use similar heat-exchange plumbing to the kind found on submarines. Microsoft hails success of its undersea data center experiment—and says it could have implications on dry land, too |David Meyer |September 15, 2020 |Fortune 

We handled some of the work, so you can get your cool java sooner. Great coffee beans for cold brew |PopSci Commerce Team |September 15, 2020 |Popular-Science 

While it seems everyone wants to keep pace with the cool kids, everyone can’t keep pace with the cool kids. Making Poly Less of a Pain in the Ass |Eugene Robinson |September 14, 2020 |Ozy 

We look at the trends, and it’s a reason we started a delivery service before it was cool. Momofuku’s David Chang on the big changes the restaurant industry needs to make to survive |Beth Kowitt |September 14, 2020 |Fortune 

It’s cool because Trenchmouth opened for Green Day in the early ‘90s in Wisconsin. Coffee Talk with Fred Armisen: On ‘Portlandia,’ Meeting Obama, and Taylor Swift’s Greatness |Marlow Stern |January 7, 2015 |DAILY BEAST 

She came to sound check, that was the first time we ever performed it, and it was really cool. Deer Tick's John McCauley on Ten Years in Rock and Roll |James Joiner |January 2, 2015 |DAILY BEAST 

Triton prices the 1000/3 LP at a cool $3.15 million—inclusive of pilot training. The Most Exciting New Hotels, Restaurants, and Submarines of 2014 |Charlie Gilbert |December 29, 2014 |DAILY BEAST 

Allow beans to cool completely then remove to a paper towel-lined plate to dry. Make Carla Hall’s Crispy Shallot Green Bean Casserole |Carla Hall |December 27, 2014 |DAILY BEAST 

The Ismael brothers even make an effort to look cool, if not fashionable, by local standards. The Brothers Who Ambushed ISIS |Mohammed A. Salih |December 27, 2014 |DAILY BEAST 

Things looked anxious for a bit, but by this morning's dawn all are dug in, cool, confident. Gallipoli Diary, Volume I |Ian Hamilton 

It mounted straight as a plume for a little way, until it met the cool air of evening which was beginning to fall. The Bondboy |George W. (George Washington) Ogden 

He has told me that their society produced on him the effect of the cool hands of saints against his cheek. The Joyous Adventures of Aristide Pujol |William J. Locke 

I laved his pain-twisted face with the cool water and let a few drops trickle into his open mouth. Raw Gold |Bertrand W. Sinclair 

As for him, he much preferred the darkness of his cool, damp galleries under the ground. The Tale of Grandfather Mole |Arthur Scott Bailey