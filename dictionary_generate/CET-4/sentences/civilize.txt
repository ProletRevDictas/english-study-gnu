Sometimes it takes just one civilizing meal to make us feel like ourselves again. Daniel Boulud on the essential role of restaurants in recovering from the pandemic |Anne Quito |June 8, 2021 |Quartz 

Alexander Stuart, the third interior secretary, once declared that the United States’ mission was to “civilize or exterminate” native people. Why Senate Republicans fear Deb Haaland |Julian Brave NoiseCat |February 25, 2021 |Washington Post 

An Army veteran looks at the fall of Mosul and recalls his own time there trying to civilize the land with guns and money. Mosul's Civilization and Its Discontents |Michael Carson |June 14, 2014 |DAILY BEAST 

It means destroying Bedouin villages in the place where Jews civilize the desert. Daniel Gordis Has It Backwards |Mark Baker |December 11, 2012 |DAILY BEAST 

Consider it part of the mission to civilize, a necessary part of the process to start solving problems again in Washington. Vote the Bums Out: the Eight Worst Members of Congress |John Avlon |November 3, 2012 |DAILY BEAST 

In the old days, colonial occupiers had what the French called “a mission to civilize.” The O Word: Christopher Dickey on What Occupation Means Today |Christopher Dickey |December 6, 2011 |DAILY BEAST 

A person who had resided some time on the coast of Africa, was asked if he thought it possible to civilize the natives? The Book of Anecdotes and Budget of Fun; |Various 

Nothing but slavery ever partially civilized him, nothing but slavery continued in some form can civilize him further! The Martin Luther King, Jr. Day, 1995, Memorial Issue |Various 

But our object in the country is to civilize and Christianize the Indian tribes among whom we are located. Early Western Travels 1748-1846, Volume XXX |Joel Palmer 

Are they useful men helping to civilize and elevate their less fortunate fellows? The Negro Problem |Booker T. Washington, et al. 

History furnishes us with abundant and specific evidence of his capacity to civilize and Christianize. History of the Negro Race in America From 1619 to 1880. Vol 1 |George W. Williams