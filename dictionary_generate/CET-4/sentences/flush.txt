Instead of flushing waste, the system would filter, clean and recycle waste on site. Climate-friendly farming and Science Moms |Lyndsey Layton |February 2, 2021 |Washington Post 

Celery juice is extremely detoxifying, as it flushes out old toxins and poisons that have built up over time. The Benefits Of Drinking Celery Juice |Charli Penn |February 2, 2021 |Essence.com 

One is by turning on the heating system, which takes in fresh air from outside, and opening windows through which it can be flushed out. How to reduce the risk of covid-19 airborne transmission inside a car | |January 31, 2021 |Washington Post 

People read these facts that I flush out of the record, their eyes bug out, and I think that demonstrates how little we know about these kind of ugly corners of America. The Trailer: Seven questions for the new political year |David Weigel |January 21, 2021 |Washington Post 

The market is flush with options — and so, in theory, are the lower rungs of the 40-man roster. Jon Lester’s reasonable contract leaves the Nationals room to address their remaining needs |Jesse Dougherty |January 20, 2021 |Washington Post 

He was in the bathroom, perhaps trying to flush some pot down the toilet, when a cop burst in. ‘I Can’t Breathe!’ ‘I Can’t Breathe!’ A Moral Indictment of Cop Culture |Michael Daly |December 4, 2014 |DAILY BEAST 

There is, for example, the Seinfeld episode where Jerry, feeling flush with cash, buys his parents a Caddy. Nationalism on Four Wheels |Clive Irving |October 18, 2014 |DAILY BEAST 

Perhaps reflective of his ability to get information, X-2 gave Schwend the codename FLUSH. On the Trail of Nazi Counterfeiters |Dr. Kevin C. Ruffner |September 20, 2014 |DAILY BEAST 

The conservative ranks are so flush with outsiders and fringe groups that they end up tripping over one another. Is Big Money Politics an Overblown Evil? |David Freedlander |August 2, 2014 |DAILY BEAST 

Where now the outdoor café tables are, right there, buses used to pull up and flush their air-brakes. Don Carpenter Was a Novelist Both Lacerating and Forgiving |Louis B. Jones |July 14, 2014 |DAILY BEAST 

The Marchioness observed a brilliant flush shoot over the face of her auditor, as he bowed his head to her last words. The Pastor's Fire-side Vol. 3 of 4 |Jane Porter 

With a flush mounting to his cheeks, and his brows drawn together in perplexity, Garnache surveyed him. St. Martin's Summer |Rafael Sabatini 

This gallantry surprised her ever so little, for a faint flush came into her cheek and the shadow of a smile into her eyes. The Joyous Adventures of Aristide Pujol |William J. Locke 

Everything looked bright on earth, and in the heavens, in the early flush of that lovely June morning. The World Before Them |Susanna Moodie 

Lady Hartledon understood the implication; she felt nettled, and a flush rose to her face. Elster's Folly |Mrs. Henry Wood