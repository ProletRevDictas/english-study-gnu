Watts helped Jagger design the stage sets for the band’s elaborate world tours, but on the road he generally stayed to himself, listening to jazz and drawing sketches of his hotel rooms. Charlie Watts, Rolling Stones drummer and band’s rhythmic mainstay, dies at 80 |Matt Schudel |August 24, 2021 |Washington Post 

Her stoic personality is ready fodder for comedy and sketches. Tschüss: OZY’s Angela Merkel Retrospective |Charu Kasturi |July 29, 2021 |Ozy 

Google posted sketches of “examples of drops and what they could potentially mean.” Google categorizes what organic search traffic drops look like |Barry Schwartz |July 20, 2021 |Search Engine Land 

SNL veteran Kenan Thompson was also nominated for his work on the sketch series, a category in which he seems to have better odds. Emmy nominations 2021: ‘The Crown,’ ‘The Mandalorian,’ ‘WandaVision’ lead with most nods |Sonia Rao, Bethonie Butler |July 13, 2021 |Washington Post 

These help you create some seriously brilliant results, from basic digital sketches to advanced computer-generated artwork. The best apps for iPad Pro connoisseurs: 10 essentials to download right now |empire |June 27, 2021 |Popular-Science 

Then they would go to a hotel afterwards and combine the parts they had remembered in one sketch. The Big Business of Fashion Counterfeits |Lizzie Crocker |December 24, 2014 |DAILY BEAST 

As for the artist, the great Turner canvases, his watercolors and his sketch books are never allowed to speak. Why Can’t Movies Capture Genius? |Clive Irving |December 14, 2014 |DAILY BEAST 

The premise of the sketch was that sex was too spontaneous to be regulated, and the quiz show played that idea to the hilt. How Antioch College Got Rape Right 20 Years Ago |Nicolaus Mills |December 10, 2014 |DAILY BEAST 

Even though it was just a line in a sketch, were you like, “I am saying a line in a sketch on SNL?” How Aidy Bryant Stealthily Became Your Favorite ‘Saturday Night Live’ Star |Kevin Fallon |October 31, 2014 |DAILY BEAST 

Look to “Booty Rap,” easily the best sketch on this season thus far, for proof of that. How Aidy Bryant Stealthily Became Your Favorite ‘Saturday Night Live’ Star |Kevin Fallon |October 31, 2014 |DAILY BEAST 

It is impossible to believe that thus far it is anything but a sketch and intimation of what it will presently be. The Salvaging Of Civilisation |H. G. (Herbert George) Wells 

The magazines sketch us a lively article, the newspapers vignette us, step by step, a royal tour. Blackwood's Edinburgh Magazine, No. CCCXXXIX. January, 1844. Vol. LV. |Various 

I have an old sketch of a back view of three of them taken side by side; you see nothing but billows of fine silky hair. Ancestors |Gertrude Atherton 

They would have made a good study for an artist, had an artist been at hand to sketch them and their surroundings. The World Before Them |Susanna Moodie 

Who is not carried back to good old times as he reads this sketch of Connecticut goin' to meetin' fifty years ago? The Book of Anecdotes and Budget of Fun; |Various