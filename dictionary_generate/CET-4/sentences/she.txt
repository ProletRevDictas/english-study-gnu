And without physical evidence, cases often come down to “he said/she said.” No Wonder Cosby's Keeping Quiet: He Could Still Be Prosecuted |Jay Michaelson |November 23, 2014 |DAILY BEAST 

They would later be dubbed a “bloodthirsty” “lesbian she-wolf pack” and—most famously—“a seething, Sapphic septet.” ‘Out in the Night’ and the Redemption of the ‘Killer Lesbian Gang' |Nina Strochlic |June 21, 2014 |DAILY BEAST 

This is just one voter out of many, naturally, but he/she has enough to say for an army. A Tony Voter on Bryan Cranston’s Overrated LBJ, Neil Patrick Harris’s Underwhelming Hedwig |Michael Musto |June 4, 2014 |DAILY BEAST 

No he-said-she-said, no muffled sounds through the dorm ceiling, no “Maybe he has other issues.” Your Princess Is in Another Castle: Misogyny, Entitlement, and Nerds |Arthur Chu |May 27, 2014 |DAILY BEAST 

On the term “she-male,” GLAAD has issued similar condemnations. RuPaul’s ‘She-Mail’ Exits Drag Race |Parker Molloy |April 14, 2014 |DAILY BEAST 

Sometimes far up in the hills a she-fox would bark, or some too-aged tree of the forest would come down with a booming crash. Love's Pilgrimage |Upton Sinclair 

She was very angry with Crozier, for it was absurd, that look of deprecating homage, that "Hush-she-is-coming" in his eyes. You Never Know Your Luck, Complete |Gilbert Parker 

The males have nearly an equal vehement desire for the female mule, the she-ass, and the mare. Buffon's Natural History. Volume IX (of 10) |Georges Louis Leclerc de Buffon 

A too strong ardour is often attended with sterility; and the female mule is at least as ardent as the she-ass. Buffon's Natural History. Volume IX (of 10) |Georges Louis Leclerc de Buffon 

The he and she-ass, therefore, both incline to sterility by common and also by different qualities. Buffon's Natural History. Volume IX (of 10) |Georges Louis Leclerc de Buffon