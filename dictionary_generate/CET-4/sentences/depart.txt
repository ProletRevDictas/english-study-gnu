While it angered fans to see the comic strip depart the funny pages, the animated version gained serious attention. Aaron McGruder’s ‘The Boondocks’ Returns Without Aaron McGruder |Rich Goldstein |April 21, 2014 |DAILY BEAST 

Then he disappeared by the same door through which I had watched him depart less than sixty seconds before. Read ‘The King in Yellow,’ the ‘True Detective’ Reference That’s the Key to the Show |Robert W. Chambers |February 20, 2014 |DAILY BEAST 

Zeitz efficiently shows how their lives parallel and depart from the larger story of a rapidly changing America in those decades. The Battle over President Lincoln’s Legacy |Tom LeClair |February 8, 2014 |DAILY BEAST 

“When V. Asaro attempted to depart in his car, agent observed him drive into a metal pole,” the papers note. A Goodfellas Sequel: A True-Life Lufthansa Figure Comes to Court |Michael Daly |January 24, 2014 |DAILY BEAST 

So al-Qaeda may well recover in months, not years, after we depart Afghanistan if the pressure on its base in Pakistan dwindles. Al Qaeda’s Next Comeback Could Be Afghanistan And Pakistan |Bruce Riedel |January 13, 2014 |DAILY BEAST 

The memory of him shall not depart away, and his name shall be in request from generation to generation. The Bible, Douay-Rheims Version |Various 

I shall soon depart, and practise no more; and my time will become my own—still my own, by no means yours. Checkmate |Joseph Sheridan Le Fanu 

But the essential problem of to-day is to know how far we are to depart from its principles. The Unsolved Riddle of Social Justice |Stephen Leacock 

Monsieur de Garnache comes alone, and if I so will it alone he shall depart or not at all. St. Martin's Summer |Rafael Sabatini 

At the same instant the landed proprietor rose from his chair, and was about to depart likewise. Blackwood's Edinburgh Magazine, No. CCCXXXIX. January, 1844. Vol. LV. |Various