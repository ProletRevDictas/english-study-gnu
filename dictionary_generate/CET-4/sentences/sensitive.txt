For example biometric data is something that’s seen as highly sensitive under the LGPD, but in the decree could still be shared between bodies. Podcast: COVID-19 is helping turn Brazil into a surveillance state |Anthony Green |September 16, 2020 |MIT Technology Review 

All, I think Melanie would say, because she was able to come up with an answer to every question a skeptical, risk-sensitive investor might have. ‘How I Built This’ host Guy Raz on insights from some of the world’s most famous entrepreneurs |Rachel King |September 15, 2020 |Fortune 

Tests were conducted at Germany’s animal health institute and sensitive areas will now be cordoned off to try to prevent the disease spreading, Kloeckner said. Europe is on high alert after a deadly swine virus emerges in Germany |Bernhard Warner |September 10, 2020 |Fortune 

According to Dhamodharan, Mastercard is sensitive to privacy issues and is building its testing kit to reflect that. Mastercard launches digital currency kit for central banks |Jeff |September 9, 2020 |Fortune 

Some tests may be very sensitive but less specific — in other words, good at picking up the presence of virus when it is there but less good at saying when the virus is not there. Spit vs. Swab? Scientists say new studies support use of ‘saliva tests’ for COVID |Lee Clifford |September 5, 2020 |Fortune 

And the Gävle Goat, apparently a sensitive creature, took the destruction hard. Sweden’s Burning Christmas Goat |Nina Strochlic |December 25, 2014 |DAILY BEAST 

The expo is introduced by Mayor Anne Hildalgo, who describes it as a “sensitive reading of the upheavals in French society.” A History of Paris in 150 Photographs |Sarah Moroz |December 14, 2014 |DAILY BEAST 

But they are also sensitive to pressure and attention from the West. The Straight Hero of Cameroon’s Gays |Jay Michaelson |December 10, 2014 |DAILY BEAST 

It is this very sensitive issue that has galvanized widespread resistance from previously loyal campesinos. China’s Nicaragua Canal Could Spark a New Central America Revolution |Nina Lakhani |November 30, 2014 |DAILY BEAST 

He was highly perceptive and exquisitely sensitive to everything around him. Stonewall Jackson, VMI’s Most Embattled Professor |S. C. Gwynne |November 29, 2014 |DAILY BEAST 

He felt, in his sensitive way, that the two sweet-souled Englishwomen had deepened and sanctified his love for Jean. The Joyous Adventures of Aristide Pujol |William J. Locke 

The eyebrows were low and thick, the upper lip was sensitive, quivering sometimes as she talked, but the lower was firm and full. Ancestors |Gertrude Atherton 

He was beset by his sensitive dislike to mix in other people's affairs, but almost angrily he overcame it. Bella Donna |Robert Hichens 

And here he might have stopped with safety; but his roused, suspicious, sensitive nature, would not suffer him. Blackwood's Edinburgh Magazine, No. CCCXXXIX. January, 1844. Vol. LV. |Various 

It is a sensitive test, and, when positive, is absolute proof of the presence of blood. A Manual of Clinical Diagnosis |James Campbell Todd