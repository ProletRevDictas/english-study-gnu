Nelson has been involved with previous protests related to the coronavirus pandemic. Twisted Sister’s Dee Snider does not approve of anti-maskers using ‘We’re Not Gonna Take It’ |radmarya |September 17, 2020 |Fortune 

This involves a lot of data entry, and most states require growers to submit this information in Metrc. Canix aims to ease cannabis cultivators’ regulatory bookkeeping |Matt Burns |September 17, 2020 |TechCrunch 

Facebook will now attempt to identify groups where an admin is not involved and proactively suggest admin roles to members who may be interested. Facebook tries to clean up Groups with new policies |Sarah Perez |September 17, 2020 |TechCrunch 

The Pentagon is involved with the distribution of vaccines, but civilian health workers will be the ones giving shots. U.S. outlines sweeping plan to provide free COVID-19 vaccines |Rachel Schallom |September 16, 2020 |Fortune 

The 737 Max was grounded March 13, 2019, three days after the second crash involving a safety feature on the plane that malfunctioned and repeatedly sent the planes into a dive toward the ground. Boeing crashes were the “horrific culmination” of multiple mistakes, House report says |kdunn6 |September 16, 2020 |Fortune 

And U.S. lawmakers are pushing to involve China in any retaliation against the Sony hack. Obama Could Hit China to Punish North Korea |Shane Harris, Tim Mak |December 20, 2014 |DAILY BEAST 

He tells the cops and testifies at trial that Adnan threatened to hurt Stephanie or get her involve if he went to police. The Scoop on ‘Serial’: Making Sense of The Nisha Call, Asia's Letters, and Our Obsession |Emily Shire |December 11, 2014 |DAILY BEAST 

Is this in any way going to involve us in getting us in there and getting us tied down there? ‘Argo’ in the Congo: The Ghosts of the Stanleyville Hostage Crisis |Nina Strochlic |November 23, 2014 |DAILY BEAST 

This year, Kalac was arrested for domestic violence in a case that apparently did not involve Coplin. Did the Amber Lynn Coplin Murder Photos Sicken the Creeps of 4Chan? |Michael Daly |November 6, 2014 |DAILY BEAST 

Magic and gaming involve in-depth worlds that feature both male and female characters. Is ‘Magic: The Gathering’ Immune to GamerGate Misogyny? |David Levesley |October 29, 2014 |DAILY BEAST 

There is more of artfulness in the flatteries which appear to involve a calculating intention to say the nice agreeable thing. Children's Ways |James Sully 

But this is quite enough to justify the inconsiderable expense which the experiment I urge would involve. Glances at Europe |Horace Greeley 

Let us remember that "if we suffer tamely a lawless attack upon our liberty, we encourage it, and involve others in our doom!" The Eve of the Revolution |Carl Becker 

The endeavor would not only be utterly unavailing, but would, with certainty, involve them in speedy and retrieveless ruin. Madame Roland, Makers of History |John S. C. Abbott 

This might involve a month and in the meantime the enemy would have time to consolidate his position. Gallipoli Diary, Volume I |Ian Hamilton