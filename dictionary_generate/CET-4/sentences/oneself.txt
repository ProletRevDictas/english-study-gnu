The speaker conjures up centuries of collective sagacity, aligning oneself with an eternal, inarguable good. Politicians Only Love Journalists When They're Dead |Luke O’Neil |January 8, 2015 |DAILY BEAST 

Perhaps there is something too painful about throwing oneself too deep into what went wrong. Janis Joplin’s Kozmic Blues |William O’Connor |November 8, 2014 |DAILY BEAST 

When one plays life that way, one in a way awakes creative vital energies in oneself that otherwise are not available. Joseph Campbell on the Roots of Halloween |Joseph Campbell |October 31, 2014 |DAILY BEAST 

How many details are needed, after all, before one can say to oneself, “This is what I have lost?” Those Kansas City Blues: A Family History |Katie Baker |October 24, 2014 |DAILY BEAST 

And so the reaction seems to be to corral oneself off from disagreement. Pew Study: Americans Are Self-Segregating Amid Proliferating Partisan Media |John Avlon |October 21, 2014 |DAILY BEAST 

The habit of refreshing oneself with a pipe on some elevated spot which commands a fine view, is common to both sexes. Tobacco; Its History, Varieties, Culture, Manufacture and Commerce |E. R. Billings. 

To suddenly discover oneself proficient where failure had been feared increases self esteem and adds to the sum of happiness. Fifty Years of Railway Life in England, Scotland and Ireland |Joseph Tatlow 

It is good, I believe, to be respectable, but much nobler to respect oneself and utter the voice of God. The Pocket R.L.S. |Robert Louis Stevenson 

There is no greater folly than in making oneself disagreeable without any probability of reformation. Beacon Lights of History, Volume I |John Lord 

Whether one does this or that, one finds oneself in pretty much the same position at the end. The Daughters of Danaus |Mona Caird