In the early stages of the coronavirus it wouldn’t have made sense for us to continue to spend on some point of sale material, but there was still a lot of people shopping in a natural retail channels like supermarkets. ‘Retailers are media owners in their own right’: Why e-commerce is driving more of Unilever’s media spend |Seb Joseph |September 9, 2020 |Digiday 

Although Walmart is still by far the nation’s biggest grocer, it has recently lost share to mainstream supermarkets, which have expanded sales at a faster pace during the quarantine. Walmart’s Amazon Prime challenger is launching Sept. 15 |radmarya |September 1, 2020 |Fortune 

In supermarkets, scan-and-go technology has accelerated the mobile checkout experience — enabling staff to be re-deployed in other areas of the store — and also creates a safer experience for both the employee and the customer. Research: Only 25 percent of professionals expect to be working from home long-term |DailyPay |August 17, 2020 |Digiday 

I mean, this is central to the lie, really, of the supermarket as a weapon. How the Supermarket Helped America Win the Cold War (Ep. 386 Rebroadcast) |Stephen J. Dubner |August 6, 2020 |Freakonomics 

The broader answer is that what makes a supermarket a supermarket is the industrial agriculture system that enables the affordability of mass-produced foods. How the Supermarket Helped America Win the Cold War (Ep. 386 Rebroadcast) |Stephen J. Dubner |August 6, 2020 |Freakonomics 

When Chérif got out of prison, he worked at the fish counter of a supermarket. France Mourns—and Hunts |Nico Hines, Christopher Dickey |January 8, 2015 |DAILY BEAST 

No alarms were triggered as she strolled out of the Giant supermarket in Limerick, Pennsylvania, and nobody thought otherwise. The Insane $11 Billion Scam at Retailers’ Return Desks |M.L. Nestel |December 19, 2014 |DAILY BEAST 

Notoriously, Atlantic City did not get its first supermarket until 1996. I Watched a Casino Kill Itself: The Awful Last Nights of Atlantic City’s Taj Mahal |Olivia Nuzzi |December 8, 2014 |DAILY BEAST 

He was captivated by footage of her escape through the hazy entryway of the supermarket, which was obscured by pepper spray. Westgate's Chilling Security Video Reveals Shopping Mall Bloodbath |Nina Strochlic |September 15, 2014 |DAILY BEAST 

Soon, you could find products containing palm oil on every supermarket shelf in the country. Our Taste for Cheap Palm Oil Is Killing Chimpanzees |Carrie Arnold |July 11, 2014 |DAILY BEAST 

A gasoline station and a weed-grown lot would shortly be replaced by a supermarket. Time and Time Again |Henry Beam Piper 

The back door of the supermarket was forced and somebody made off with a variety of groceries. And Then the Town Took Off |Richard Wilson 

Apparently he cashed the check at the little store, or the supermarket, near where you lived there in New Orleans. Warren Commission (5 of 26): Hearings Vol. V (of 15) |The President's Commission on the Assassination of President Kennedy 

That put it early in April, which decreased the weather hazard—a major consideration in even a trip to the Supermarket. A Matter of Proportion |Anne Walker 

Do you know, are you familiar with Hutch's Market, Supermarket? Warren Commission (9 of 26): Hearings Vol. IX (of 15) |The President's Commission on the Assassination of President Kennedy