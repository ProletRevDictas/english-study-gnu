While the DoJ had submitted inquiries to the company, Clover Health downplayed the issue, characterizing it as a regular occurrence in the industry that it did not consider material to its investors. A short seller vs. Chamath Palihapitiya |Lucinda Shen |February 5, 2021 |Fortune 

We can thank some combination of Oklahoma State and the NCAA for delivering the sort of subplot that has been a once-every-few-years occurrence. NCAA tournament bracketology: Oklahoma State will probably make it despite its postseason ban |Patrick Stevens |January 26, 2021 |Washington Post 

In an essay in August, when global covid-19 deaths stood at around 600,000, Bill Gates pointed out that climate change fatalities could reach that level by 2060—but as an annual occurrence. The pandemic taught us how not to deal with climate change |James Temple |January 1, 2021 |MIT Technology Review 

Either we try to piece the world back together as it was before this catastrophic occurrence, or we can use this shared event as the founding moment of a unifying global narrative. These Were the 15 Most-Read Singularity Hub Stories of 2020 |Singularity Hub Staff |December 26, 2020 |Singularity Hub 

It’s a common occurrence that the agency has failed to address. They Made a Revolutionary System to Protect People With Developmental Disabilities. Now It’s Falling Apart. |by Amy Silverman for Arizona Daily Star |December 12, 2020 |ProPublica 

Although often this is considered proof positive of guilt at trial, it is not an uncommon occurrence in false confessions. How the U.S. Justice System Screws Prisoners with Disabilities |Elizabeth Picciuto |December 16, 2014 |DAILY BEAST 

I had to pause for sheep crossing the road, which is a common occurrence when driving through the Highlands of Scotland. A Whisky Connoisseur Remembers That First Sip of The Macallan | |December 10, 2014 |DAILY BEAST 

At one point Schettino admitted that bringing passengers up to the bridge was a common occurrence. The Costa Concordia’s Randy Reckless Captain Takes the Stand |Barbie Latza Nadeau |December 2, 2014 |DAILY BEAST 

On its eastern front, exchanges of fire with Pakistan are a daily occurrence. The Nuclear Deal That Iran’s Regime Fears Most |Djavad Khadem |November 22, 2014 |DAILY BEAST 

According to Monaghan, this was a very regular occurrence among the female soldiers stationed there. Michelle Monaghan on ‘Fort Bliss,’ the Lack of Roles for Women, and ‘True Detective’ Hysteria |Marlow Stern |September 23, 2014 |DAILY BEAST 

Several pioneers familiar with the facts of the tragedy at the time of its occurrence were also present. Among the Sioux |R. J. Creswell 

Her feet felt rooted to the floor in the wonder and doubt of this strange occurrence. The Bondboy |George W. (George Washington) Ogden 

In the outskirts of the city, skirmishes between Spanish troops and rebels were of frequent occurrence. The Philippine Islands |John Foreman 

The formation of a new ministry was not an unusual occurrence in the early years of King George the Third. The Eve of the Revolution |Carl Becker 

She felt sure that the significance of the whole occurrence had lain in her own self-consciousness. The Awakening and Selected Short Stories |Kate Chopin