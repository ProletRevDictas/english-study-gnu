We were eating dinner by kerosene lantern light, as the hydroelectric plant no longer generated electricity, when it sounded. The quest to snare—and save—the world’s largest owl |Jonathan Slaght |August 28, 2020 |Popular-Science 

For some reason, I wanted carrots and peas that night as part of our dinner. Full Transcript: Sean Spicer on ‘The Carlos Watson Show’ |Daniel Malloy |August 26, 2020 |Ozy 

With six wash cycles, you can get your whole dinner plates set in there along with your silverware. Small dishwashers that fit in almost any kitchen |PopSci Commerce Team |August 25, 2020 |Popular-Science 

Energy demand tends to spike at night, when people are cooking dinner and cooling off at bedtime. Environment Report: Real Estate Sellers Aren’t Required to Disclose Sea Level Rise Risk |MacKenzie Elmer |August 24, 2020 |Voice of San Diego 

We fell into a routine of building a morning fire, cooking together, and talking over dinner, sharing our fears and the events of the day. That chatbot I’ve loved to hate |Tate Ryan-Mosley |August 19, 2020 |MIT Technology Review 

Eva Silverman, who co-hosts an Oakland Dinner Party, agrees. Everyone at This Dinner Party Has Lost Someone |Samantha Levine |January 6, 2015 |DAILY BEAST 

Though tissues are present and tears are not uncommon, the Dinner Parties are distinctly not grief counseling or group therapy. Everyone at This Dinner Party Has Lost Someone |Samantha Levine |January 6, 2015 |DAILY BEAST 

Those who come to the Dinner Party are self-selecting; they do want to talk about it. Everyone at This Dinner Party Has Lost Someone |Samantha Levine |January 6, 2015 |DAILY BEAST 

Talking about death is never easy, but with food, comfort, and familiarity, a new kind of dinner party is making it easier. Everyone at This Dinner Party Has Lost Someone |Samantha Levine |January 6, 2015 |DAILY BEAST 

Kyle Dietrich, 36, is a host of one of the DC Dinner Parties. Everyone at This Dinner Party Has Lost Someone |Samantha Levine |January 6, 2015 |DAILY BEAST 

It was Wednesday night; over forty men sat down to the house-dinner at the Pandemonium Club. The Pit Town Coronet, Volume I (of 3) |Charles James Wills 

As usual the dinner was recherché, for the Pandemonium chef enjoyed a world-wide reputation. The Pit Town Coronet, Volume I (of 3) |Charles James Wills 

Dinner occurred in the middle of the day, and about nine in the evening was an informal but copious supper. Hilda Lessways |Arnold Bennett 

Then a nervous, anxious feeling that takes away my appetite, and prevents me from eating my dinner. Music-Study in Germany |Amy Fay 

It is to be feared that the attractions of the house-dinner were not the sole inducement to many of those sitting there. The Pit Town Coronet, Volume I (of 3) |Charles James Wills