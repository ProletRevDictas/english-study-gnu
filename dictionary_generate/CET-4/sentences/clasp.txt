While binder-style clasps add weight and bulk to a planner, they allow you to rearrange pages to suit your needs and are often compatible with customized inserts, like templates designed for project management or specific productivity methods. Best daily planners to boost your productivity |Rowan Lyons |August 21, 2021 |Popular-Science 

The watch crown and clasp have engraved "M" logos, just like Mario's cap. The $2,000 Super Mario smartwatch you’ve always wanted is here |Ron Amadeo |July 14, 2021 |Ars Technica 

Normally of good balance, I found myself reaching for the handrail, unable to clasp it for the refreshments in my hands. San Fran Kisses Its 70,000-Person Toilet Goodbye |Jon Rochmis |August 15, 2014 |DAILY BEAST 

On the pink cotton inside lay a clasp of black onyx, on which was inlaid a curious symbol or letter in gold. Read ‘The King in Yellow,’ the ‘True Detective’ Reference That’s the Key to the Show |Robert W. Chambers |February 20, 2014 |DAILY BEAST 

That metal clasp has DNA attributed to Sollecito (in multiple tests), which should have sealed the conviction. Amanda Knox Faces Another Guilty Verdict |Barbie Latza Nadeau |January 20, 2014 |DAILY BEAST 

When her bra was cut from her body, the assassin or assassins also cut off the tab on which the tiny metal clasp was affixed. Amanda Knox Faces Another Guilty Verdict |Barbie Latza Nadeau |January 20, 2014 |DAILY BEAST 

The tiny metal clasp had traces of DNA attributed to Sollecito. Italian Court Explains Why It Overturned Amanda Knox’s Acquittal |Barbie Latza Nadeau |June 19, 2013 |DAILY BEAST 

She climbed the winding stairs that led to her mother's room, and she paused to clasp her beating heart with both her hands. Honey-Bee |Anatole France 

Felix would be troubled and angry she knew, 138 even at this clasp of an old friends hand. Tessa Wadsworth's Discipline |Jennie M. Drinkwater 

And the two understood each other better by that silent, hearty hand-clasp than they could have done with any number of words. Gold-Seeking on the Dalton Trail |Arthur R. Thompson 

She glanced downwards with averted head, awaiting some outcry of gladness, surrendering herself to the quick clasp of strong arms. Jaffery |William J. Locke 

When she did wrong, she would kneel and clasp her hands, seeming earnestly to ask to be forgiven. Minnie's Pet Monkey |Madeline Leslie