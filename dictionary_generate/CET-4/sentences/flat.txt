Print advertisements for Quaker Rice Cakes from that period show thin, grinning models lying on their flat leotard-covered stomachs to emphasize the lightness of rice cakes. The Rise and Fall of the Rice Cake, America’s One-Time Favorite Health Snack |Brenna Houck |September 17, 2020 |Eater 

Unlike the flat disk that gave rise to the planets in our own Solar System, the system’s disk consists of three misaligned rings. A strange dusty disk could hide a planet betwixt three stars |Paola Rosa-Aquino |September 11, 2020 |Popular-Science 

It uses technology well, including flat-screen monitors that connect to the Internet and Bluetooth headphones. Peloton races to the future, but it’s still no tech company |Adam Lashinsky |September 11, 2020 |Fortune 

The catch included two tiny species with coiled shells about 1 millimeter across, four midsize species with long, conical or urn-shaped shells of about 7 to 11 millimeters, and one species with a flat shell up to 14 millimeters across. Sea butterflies’ shells determine how the snails swim |Maria Temming |September 8, 2020 |Science News 

It combines the functionality of a suitcase and the durability of a duffel, with structured sides and interior pockets for ample gear hauling, a flat-bottom design that enables it to stand, and sturdy, oversize wheels. Our Favorite Gear for Any Type of Traveler |Janna Irons |September 8, 2020 |Outside Online 

Luckily enough I have this dedicated flat that is just along from my house that I go to every day. Belle & Sebastian Aren’t So Shy Anymore |James Joiner |January 7, 2015 |DAILY BEAST 

There were stomachs, taut and flat, but also undulating bellies, soft and bloated from the breakfast buffet. Powerful Congressman Writes About ‘Fleshy Breasts’ |Asawin Suebsaeng |January 7, 2015 |DAILY BEAST 

The program—weirdly—is now under the umbrella of ABC News, and is suffering from flat ratings and an aging demographic. The Bloodiest Media Coups of 2014 |Lloyd Grove |December 22, 2014 |DAILY BEAST 

Another sent back a flat-screen television with a bona fide tombstone within. The Insane $11 Billion Scam at Retailers’ Return Desks |M.L. Nestel |December 19, 2014 |DAILY BEAST 

Hitchcock settled in southern California, leaving behind a flat in London and a country house in Shamley Green. Alfred Hitchcock’s Fade to Black: The Great Director’s Final Days |David Freeman |December 13, 2014 |DAILY BEAST 

Why not have sought out the pure white lime-rocks of the flat country, or the grey granite of the hills? Blackwood's Edinburgh Magazine, Volume 60, No. 372, October 1846 |Various 

Then the roof itself, with its gables and dormer windows, softly folded itself flat down upon the top of the house, out of sight. Davy and The Goblin |Charles E. Carryl 

Instead of writing slander and flat blasphemy, they propose to draw it, and not draw it mild. Blackwood's Edinburgh Magazine, No. CCCXXXIX. January, 1844. Vol. LV. |Various 

There is no other way but fresh blood for it is sheer human nature to feel flat after an effort. Gallipoli Diary, Volume I |Ian Hamilton 

The hills disappear some miles above this city, and henceforward to the sea all is flat and tame as a marsh. Glances at Europe |Horace Greeley