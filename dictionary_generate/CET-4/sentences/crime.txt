Facial recognition, cameras that were set up throughout cities to monitor crime. Podcast: COVID-19 is helping turn Brazil into a surveillance state |Anthony Green |September 16, 2020 |MIT Technology Review 

She could also qualify for a legal status known as a U visa, which is intended for immigrant victims of crime. ICE Deported a Woman Who Accused Guards of Sexual Assault While the Feds Were Still Investigating the Incident |by Lomi Kriel |September 15, 2020 |ProPublica 

Most crimes committed by documented gang members are crimes of poverty. While We’re Rethinking Policing, It’s Time to End Gang Injunctions |Jamie Wilson |September 15, 2020 |Voice of San Diego 

Those results will now be uploaded to an FBI database to be cross-referenced from results from other crimes to potentially identify perpetrators. Morning Report: Downtown Jail Outbreak Goes From Bad to Worse |Voice of San Diego |September 11, 2020 |Voice of San Diego 

In 2020 as in 1998, Disney’s Mulan is a tomboy who disgracefully fails her matchmaker’s marriage test, and in this version, too, Mulan’s deception is a capital crime. The history of Mulan, from a 6th-century ballad to the live-action Disney movie |Constance Grady |September 4, 2020 |Vox 

Did he denounce the involvement of organized crime in the abduction and disappearance of 43 students in the nearby city of Iguala? Mexico’s Priests Are Marked for Murder |Jason McGahan |January 7, 2015 |DAILY BEAST 

But they say its effect on the regular daily operation of organized crime has been negligible. Mexico’s Priests Are Marked for Murder |Jason McGahan |January 7, 2015 |DAILY BEAST 

The anti-crime cops began searching the likely path of flight. Shot Down During the NYPD Slowdown |Michael Daly |January 7, 2015 |DAILY BEAST 

Which is impossible unless people talk publicly rather than letting each crime be its own isolated incident. Cover-Ups and Concern Trolls: Actually, It's About Ethics in Suicide Journalism |Arthur Chu |January 3, 2015 |DAILY BEAST 

Denied parole nine straight times, he insists he is innocent of the crime for which he was convicted. His First Day Out Of Jail After 40 Years: Adjusting To Life Outside |Justin Rohrlich |January 3, 2015 |DAILY BEAST 

He was thrashed at school before the Jews and the hubshi, for the heinous crime of bringing home false reports of progress. Kipling Stories and Poems Every Child Should Know, Book II |Rudyard Kipling 

At that moment the crime and inefficacy of bloodshed, in avenging injuries like his, or any injuries, struck upon his soul. The Pastor's Fire-side Vol. 3 of 4 |Jane Porter 

He knew that the whole fabric of crime was due to the human reading of His "revelation" to man. God and my Neighbour |Robert Blatchford 

Humanity must bench with justice; or punishment itself becomes crime, and degenerates into revenge. The Pastor's Fire-side Vol. 3 of 4 |Jane Porter 

It is therefore true that the field of crime is not fixed, is in truth always changing. Putnam's Handy Law Book for the Layman |Albert Sidney Bolles