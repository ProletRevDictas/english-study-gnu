Another client is an Ohio-based manufacturer of cotton swabs that is replacing the cotton with a synthetic equivalent in order to make nasal testing swabs uncontaminated by the plant fiber’s DNA. Unmade in America |Tate Ryan-Mosley |August 14, 2020 |MIT Technology Review 

The team hopes their new cotton plants might eventually be grown widely and made into clothes, helping to displace the toxic dyes and synthetic materials used by the fashion industry. Scientists Gene-Hack Cotton Plants to Make Them Every Color of the Rainbow |Jason Dorrier |August 11, 2020 |Singularity Hub 

All he needed was a long, two-dimensional chain—natural or synthetic—and the chain should fold itself automatically, similar to how it folds into the double helix inside our bodies. How Fake Viruses Can Help Us Make the Best Possible Vaccines |Shelly Fan |July 7, 2020 |Singularity Hub 

Because the smell was completely synthetic, though, researchers could mess with it. How to make a mouse smell a smell that doesn’t actually exist |Laura Sanders |June 18, 2020 |Science News 

The fully synthetic nature of the Arkansas team’s technology could thus have a leg up on biologically-based approaches. Artificial Kidneys Are a Step Closer With This New Tech |Vanessa Bates Ramirez |June 3, 2020 |Singularity Hub 

One key ingredient of the stuff: Propylene glycol, a synthetic liquid that absorbs water. Europeans Recall Fireball Whiskey Over a Sweetener Also Used in Antifreeze |Tim Mak |October 28, 2014 |DAILY BEAST 

The Illinois legislation does not include bioplastics like PHA, defining the banned microplastics to be of the synthetic type. Your Favorite Facewash Is Hurting Nemo |Alexa C. Kurzius |June 18, 2014 |DAILY BEAST 

But the arguments over drug testing may cease to matter if the synthetic drug epidemic continues. Are We Addicted to Drug Testing? |Abby Haglage |May 27, 2014 |DAILY BEAST 

With more than 300 synthetic cannabinoids in use today, drug-testing companies are struggling to keep up with the new varieties. Are We Addicted to Drug Testing? |Abby Haglage |May 27, 2014 |DAILY BEAST 

A dangerous synthetic drug trend is fueling an epidemic among American youth. Is Synthetic Weed Feuling Yemen’s Terrorism? |Abby Haglage |May 23, 2014 |DAILY BEAST 

Nearly all the translating words given in this section so far are synthetic. Assimilative Memory |Marcus Dwight Larrowe (AKA Prof. A. Loisette) 

Drapes of velvety synthetic, dyed the deep green that Martian colonists like, covered the walls. Fee of the Frontier |Horace Brown Fyfe 

If you want to make a synthetic sapphire, you start with a seed sapphire, and the artificial process builds up on that. Nine Men in Time |Noel Miller Loomis 

Approximately two-thirds of such chemical employees are found in the synthetic fiber field. Hallowed Heritage: The Life of Virginia |Dorothy M. Torpey 

Virginia now has approximately 30% of the total employees in the United States engaged in synthetic fibers. Hallowed Heritage: The Life of Virginia |Dorothy M. Torpey