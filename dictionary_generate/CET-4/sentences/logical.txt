There are also a few logical arguments in favor of going for it. Super Bowl highlights: Bucs celebrate championship, Tom Brady wins MVP |Des Bieler, Mark Maske, Chuck Culpepper |February 8, 2021 |Washington Post 

For them, a model “is just a way of understanding a particular process or a particular question we’re interested in,” Kucharski said, “and working through the logical implications of our assumptions.” The Hard Lessons of Modeling the Coronavirus Pandemic |Jordana Cepelewicz |January 28, 2021 |Quanta Magazine 

In a room with six exits, it seems like the most logical course of action would be for the crowd to divide evenly among all six. Want to Get Out Alive? Follow the Ants - Issue 95: Escape |Conor Myhrvold |January 28, 2021 |Nautilus 

When the Shakepeare Theatre left, the logical choice would have been to stick to more historical work — to look backward. Folger Theatre’s Janet Griffin announces her retirement after decades of devotion to Shakespeare |Peter Marks |January 27, 2021 |Washington Post 

If we take this logical and strategic perspective, we can be more efficient in vaccine delivery and enjoy our normal activities much sooner. Envisioning better health outcomes for all |Jason Sparapani |January 26, 2021 |MIT Technology Review 

Following this line of reasoning to its logical conclusion, the way to achieve world peace is to give everyone atomic bombs. Santa Fails One More Time |P. J. O’Rourke |December 27, 2014 |DAILY BEAST 

Taken to its logical conclusion, the “not me” judgment can lead us to regard other human beings as not human at all! Ferguson, Immigration, and ‘Us Vs. Them’ |Gene Robinson |November 27, 2014 |DAILY BEAST 

If the certainty of the wisdom of uncertainty is itself uncertain, the force of the definition crumbles by logical standards. The Birth of the Novel |Nick Romeo |November 27, 2014 |DAILY BEAST 

The declaration of war against ISIS, then, would seem to be a logical next step. Rand Paul Declares War on ISIS—and Allows Boots on the Ground |Olivia Nuzzi |November 24, 2014 |DAILY BEAST 

If you were emotionalized, you saw him clearly as the Fuhrer, but anyone who was reasonable and logical was seen as the enemy. The Godfather of Right-Wing Radio |Caitlin Dickson |November 23, 2014 |DAILY BEAST 

Pedantic, unimaginative and presumptuous, Theobald was the logical choice for a Dunce King in 1728. A Letter from Mr. Cibber to Mr. Pope |Colley Cibber 

Aunt Ri was a privileged character, but her logical method of questioning was inconvenient. Ramona |Helen Hunt Jackson 

Probably, he did not discover this consciously for himself: the result, in any case, was logical and obvious. The Wave |Algernon Blackwood 

The logical, thorough reasoning behind the content of the Declaration is easily apparent. Hallowed Heritage: The Life of Virginia |Dorothy M. Torpey 

Some of it, I knew, he discarded because it didn't sound logical, but other parts seemed to make an impression on him. Hooded Detective, Volume III No. 2, January, 1942 |Various