Israeli media have reported on company’s links to shell companies and byzantine deals. The man who built a spyware empire says it’s time to come out of the shadows |Bobbie Johnson |August 19, 2020 |MIT Technology Review 

Then they begin to burn hydrogen in a shell surrounding the center. Explainer: Stars and their families |Ken Croswell |August 18, 2020 |Science News For Students 

An empty shell of a man stumbled into a ramshackle Pentecostal church at 3585 Middlefield Road looking to be filled with the Holy Spirit. When Jesus Freaks Go Bad |Eugene Robinson |August 6, 2020 |Ozy 

I just know everybody has shell shock from the crisis learning that we did when we closed schools, and everybody had to start online. North County Report: School Reopening Tensions Are High in Oceanside |Kayla Jimenez |July 22, 2020 |Voice of San Diego 

The virus can infect human cells, and delivers DNA instructions for making the coronavirus’ spike protein — the knobby protein studding the virus’s outer shell. COVID-19 vaccines by Oxford, CanSino and Pfizer all trigger immune responses |Tina Hesman Saey |July 21, 2020 |Science News 

Their friends noticed, and asked Sabrine to talk to him to bring him out of his shell a little. A Sunni-Shia Love Story Imperiled by al Qaeda |Ruth Michaelson |December 26, 2014 |DAILY BEAST 

Another said just the act of spending money on self-improvement made him determined to break out of his shell. The Secret World of Pickup Artist Julien Blanc |Brandy Zadrozny |December 1, 2014 |DAILY BEAST 

If you tend to inhale your food without realizing how much you consumed, opt for nibbles like in-shell pistachio nuts. 12 Thanksgiving Weight Loss Tips That Actually Work |DailyBurn |November 27, 2014 |DAILY BEAST 

Then came the day Mustafa, along with two others, was killed by a mortar shell. Drawing on the Memories of Syrian Women |Masha Hamilton |November 26, 2014 |DAILY BEAST 

Yet five years later, the news operation has vanished and TRN is now a shell of its former self. The Godfather of Right-Wing Radio |Caitlin Dickson |November 23, 2014 |DAILY BEAST 

Off flew the shell, seven miles it flew; over the Turkish Army from one sea into another. Gallipoli Diary, Volume I |Ian Hamilton 

He conducts this ceremony with the greatest solemnity, occasionally pronouncing these incantatory words, "Plate or shell, sah?" Physiology of The Opera |John H. Swaby (AKA "Scrici") 

Fancy that enormous shell dropping suddenly out of the blue on to a ship's deck swarming with troops! Gallipoli Diary, Volume I |Ian Hamilton 

From pre-natal days I was destined for the railway service, as an oyster to its shell. Fifty Years of Railway Life in England, Scotland and Ireland |Joseph Tatlow 

If one could languish through life in the shell of a mere beauty that life would be a good deal simpler proposition than it is. Ancestors |Gertrude Atherton