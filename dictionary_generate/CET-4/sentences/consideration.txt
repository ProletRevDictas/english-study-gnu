Thoughts during her tenure of leaving gave way to other considerations, she said. Former Pence aide says she will vote for Biden because of Trump’s ‘flat out disregard for human life’ during pandemic |Josh Dawsey |September 17, 2020 |Washington Post 

Participants who responded to their error by giving it more consideration were able to do better on the test after making their mistake. A secret of science: Mistakes boost understanding |Rachel Kehoe |September 10, 2020 |Science News For Students 

Given her own rapid rise and position in leadership, Sykes has thrust herself into consideration as a future governor or Senate candidate for the Democrats. Can This Millennial Power Broker Lead an Ohio Comeback for Democrats? |Nick Fouriezos |September 10, 2020 |Ozy 

Both of these considerations can help explain how there could be moral error and disagreement even if the ground of ethical truth is, so to speak, right under our noses. The Universe Knows Right from Wrong - Issue 89: The Dark Side |Philip Goff |September 9, 2020 |Nautilus 

If style and finish are considerations in your under bed storage needs, this set of two boxes will meet the occasion. The best under-bed storage solutions |PopSci Commerce Team |September 9, 2020 |Popular-Science 

There were even heartfelt pleas for consideration, before—what? How to Survive the New Year ‘Gympocalypse’ |Tim Teeman |January 6, 2015 |DAILY BEAST 

But The Pinkprint gives little consideration to the gulf between her various musical selves. Nicki Minaj Bares Her Own Vulnerability on ‘The Pinkprint’ |Rawiya Kameir |December 16, 2014 |DAILY BEAST 

Prizes may not be exchanged for cash or any other consideration. The Daily Beast Company LLC The New Alphas Sweepstakes Official Rules | |December 9, 2014 |DAILY BEAST 

But there were moments of expected genius on it worthy of Grammy consideration. 10 Biggest Grammy Award Snubs and Surprises: Meghan Trainor, Miley Cyrus & More |Kevin Fallon |December 5, 2014 |DAILY BEAST 

As I leave,  I mention in parting how Culkin deserves some serious Tony Awards consideration for his performance. The Revival of Kieran Culkin: A Reluctant Star Seizes the Spotlight |Marlow Stern |October 23, 2014 |DAILY BEAST 

It therefore took under favorable consideration the question of a voluntary clearing system. Readings in Money and Banking |Chester Arthur Phillips 

In connection with this comes the consideration of slides and the finer modulations of tone-color, movement, and cadence. Expressive Voice Culture |Jessie Eldridge Southwick 

Much more attention should be given than is ordinarily devoted to the consideration of rhythm. Expressive Voice Culture |Jessie Eldridge Southwick 

Hitherto, I have not given the subject much consideration, but I turn over a new leaf from the date of this adventure. Glances at Europe |Horace Greeley 

He had not taken his nerves into consideration for the simple reason that he had never known that he possessed any. Uncanny Tales |Various