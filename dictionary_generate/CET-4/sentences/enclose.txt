She told ProPublica and the Tribune in a telephone interview that officers encouraged detainees to sign up for anti-anxiety pills because they oversee the dispensing of medication at night and have access to an enclosed off-camera area. ICE Deported a Woman Who Accused Guards of Sexual Assault While the Feds Were Still Investigating the Incident |by Lomi Kriel |September 15, 2020 |ProPublica 

Human bones were found buried beneath these platforms and in the walls, including a stillborn foetus enclosed in a brick. An Ancient Site with Human Skulls on Display - Issue 89: The Dark Side |Jo Marchant |September 2, 2020 |Nautilus 

You wanted to figure out the shape that enclosed the largest area possible. Can You Cover The Globe? |Zach Wissner-Gross |August 28, 2020 |FiveThirtyEight 

Both Perry and Mynchenberg stress that enclosed areas, like a dog park or fenced-in yard, are most ideal for off-leash sessions. The right way to walk your dog |John Kennedy |August 26, 2020 |Popular-Science 

In a Tuesday statement, the labor ministry said masks would need to become de rigueur in companies’ enclosed and shared spaces, except where there is just one person working alone. As COVID-19 rates rise, France will require most people to wear masks at work |David Meyer |August 18, 2020 |Fortune 

The foundational concepts of philosophy enclose the logos, and reason, within a sort of 'closure.' Derrida’s ‘Of Grammatology’ and the Birth of Deconstruction |Benoît Peeters |December 21, 2012 |DAILY BEAST 

This might have inspired Wright to enclose part of his Anna Karenina inside a theater, as if a Chekhov play is being mounted. ‘The Trial’ & More Top Film Adaptations of Literary Classics (VIDEO) |Jimmy So |November 24, 2012 |DAILY BEAST 

Tall iron gates between the arches enclose the graves, which are marked with massive sarcophagi of Scotch granite. British Highways And Byways From A Motor Car |Thomas D. Murphy 

If the enquiry refers to matters interesting only to yourself, enclose a postage-stamp for the reply. The Ladies' Book of Etiquette, and Manual of Politeness |Florence Hartley 

Enclose the invitation in a white envelope, and tie it with white satin ribbon. The Ladies' Book of Etiquette, and Manual of Politeness |Florence Hartley 

How shall we enclose the power of such majesty in one dwelling-place? A Philosophical Dictionary, Volume 1 (of 10) |Franois-Marie Arouet (AKA Voltaire) 

The tire on its steel core is taken to the mold room and placed in a steel box or mold, shaped to exactly enclose it. The Wonder Book of Knowledge |Various