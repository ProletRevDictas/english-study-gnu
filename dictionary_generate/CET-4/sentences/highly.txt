As this pandemic shows, we live in a highly networked world, and it’s unlikely that borders can remain shut indefinitely. A coronavirus vaccine will save more lives if we share it widely |Kat Eschner |September 17, 2020 |Popular-Science 

In 2017, when the first interstellar object was detected in our solar system, a highly respected Harvard astrophysicist suggested it might be a probe that was sent by aliens. What We Really Know About Life in Outer Space |Outside Editors |September 17, 2020 |Outside Online 

We’ve gathered selections of high quality, highly durable, excellently pigmented options to tackle any job. Excellent pencils for school, work, and beyond |PopSci Commerce Team |September 16, 2020 |Popular-Science 

That’s a highly suspect claim, given the slow rollout of testing here. Trump’s incoherent defense of his coronavirus response |Aaron Blake |September 16, 2020 |Washington Post 

On October 5, Fortune is launching an online learning platform and community called Fortune Connect that will target mid-tier, vp and senior manager executives in a highly monetized way. Fortune Connect is bringing its conference business to a larger audience, with a higher price tag |Kayleigh Barber |September 15, 2020 |Digiday 

Such is her burgeoning popularity Toomey is looking to employ more instructors to lead her highly personalized exercise classes. How Taryn Toomey’s ‘The Class’ Became New York’s Latest Fitness Craze |Lizzie Crocker |January 9, 2015 |DAILY BEAST 

Asian-Americans may vote for Democrats now, but they are a highly persuadable—and growing—part of the electorate. Asian-Americans Are The New Florida |Tim Mak |January 8, 2015 |DAILY BEAST 

It was Orlando vs. Justin in an Ibiza melee with two highly unlikely opposing parties. The Bloom-Bieber Brawl We Didn’t Know We Needed |Kevin O’Keeffe |December 29, 2014 |DAILY BEAST 

On May 9, which Moscow commemorates as World War II “Victory Day,” Klaus paid a highly visible visit to the Russian Embassy. Vaclav Klaus, Libertarian Hero, Has His Wings Clipped by Cato Institute |James Kirchick |December 22, 2014 |DAILY BEAST 

Depending on the producer, Champagne can also be highly cloyingly sweet, buttery, or round, or mineral. Champagne: You’re Drinking It All Wrong |Kayleigh Kulp |December 20, 2014 |DAILY BEAST 

Under the one-sixth they appear as slender, highly refractive fibers with double contour and, often, curled or split ends. A Manual of Clinical Diagnosis |James Campbell Todd 

We prefer the American volume of Hochelaga to the Canadian one, although both are highly interesting. Blackwood's Edinburgh Magazine, Volume 60, No. 372, October 1846 |Various 

This has a warm though a thin soil, which must be highly favorable to the Vine to induce so exclusive a devotion to it. Glances at Europe |Horace Greeley 

Thomas Cooper, an English prelate, died; highly commended for his great learning and eloquence. The Every Day Book of History and Chronology |Joel Munsell 

It took twenty Eskimos to hold it when allowed full play, and even these it jerked about in a manner that highly diverted them. The Giant of the North |R.M. Ballantyne