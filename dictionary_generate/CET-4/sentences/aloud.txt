With the exception of the last I found them quite straightforward and manageable, but that’s not to say I wasn’t urging Mono on aloud. ‘Little Nightmares II’: A hypnotic, dark fairy tale |Christopher Byrd |February 9, 2021 |Washington Post 

An officer would summon each firefighter into his office and then read numeric codes aloud. Call box of duty: Cops remember the call boxes that kept them connected |John Kelly |January 30, 2021 |Washington Post 

Yet after the fight, after finally saying aloud what he’d been thinking for almost 15 years, Peter felt better. The Climate Crisis Is Worse Than You Can Imagine. Here’s What Happens If You Try. |by Elizabeth Weil |January 25, 2021 |ProPublica 

According to the Constitution, Pence will read aloud electoral college votes cast in each state in December. Pence seeks rejection of lawsuit that aimed to expand his power to overturn the election |Rosalind Helderman, John Wagner |January 1, 2021 |Washington Post 

He wondered aloud at the board meeting whether the port’s streetlights system and another project on the table at the time — a system of interactive kiosks — could be joined together to better understand how port visitors were moving around. The Port of San Diego Had Its Own Smart Streetlight Program |Jesse Marx |December 21, 2020 |Voice of San Diego 

Fossella declined to run again, but in the years since he has mused aloud about challenging Grimm. Will Dirty Pol Vito Fossella Replace Dirty Pol Michael Grimm? |David Freedlander |December 31, 2014 |DAILY BEAST 

Giorgio read aloud what they had chosen as their class motto. Cop Families Boo De Blasio at NYPD Graduation |Michael Daly |December 30, 2014 |DAILY BEAST 

Then I read aloud from something that captures the Holiday Spirit. Congress’ Gift That Keeps on Giving |P. J. O’Rourke |December 20, 2014 |DAILY BEAST 

Red letters scrawled underneath seem to be Ramone thinking aloud: “I wonder but I think so.” ‘All Good Cretins Go to Heaven’: Dee Dee Ramone’s Twisted Punk Paintings |Melissa Leon |December 15, 2014 |DAILY BEAST 

At the end of his prayer, the grand mufti whispered aloud: “May God accept it.” Does Pope Francis Believe Christians and Muslims Worship the Same God? |Jay Parini |December 7, 2014 |DAILY BEAST 

It was such a magnificent sum that Sol did not feel like taking the familiarity with it of mentioning it aloud. The Bondboy |George W. (George Washington) Ogden 

At Felipe's cry, the women waiting in the hall hurried in, wailing aloud as their first glance showed them all was over. Ramona |Helen Hunt Jackson 

"The worst of it is that the children will grow up away from me," thought Mamma; but she did not say it aloud. Kipling Stories and Poems Every Child Should Know, Book II |Rudyard Kipling 

She groaned aloud, and her tears flowed faster: Alessandro was making the baby's coffin. Ramona |Helen Hunt Jackson 

"They said at Ritz's that he was one of the young millionaires, well known already in America," the fat woman reflected aloud. Rosemary in Search of a Father |C. N. Williamson