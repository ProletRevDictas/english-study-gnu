Completing the entire course takes about 11 hours—yes, quizzes are included. Want to know how to WFH better? There’s a class for that |Michal Lev-Ram, writer |September 22, 2020 |Fortune 

Sky, for example, launched its “Fanzone” online viewing experience that lets people watch sports and play quizzes together with family and friends. ‘We are permanently in beta’: European sports broadcasting is still in a coronavirus-forced state of reinvention |Lara O'Reilly |September 15, 2020 |Digiday 

Rummel says it provides free distance learning activities like multiplication quizzes and poetry tutorials for preschool through grade 12. Need to entertain your kids? These educational apps may help |Michal Lev-Ram, writer |September 13, 2020 |Fortune 

The company will now require all Revel users, including long-time users of the service, to take a 21-question safety training quiz and watch an instructional video before they can start their first ride. Electric moped startup Revel returns to New York with helmet selfie, other in-app safety features |Kirsten Korosec |August 27, 2020 |TechCrunch 

In March, the Polish government launched a Minecraft server which packages quizzes and other educational activities, with each student allotted a plot of virtual land to construct buildings. Is School Out Forever? |Daniel Malloy |August 9, 2020 |Ozy 

The premise of the sketch was that sex was too spontaneous to be regulated, and the quiz show played that idea to the hilt. How Antioch College Got Rape Right 20 Years Ago |Nicolaus Mills |December 10, 2014 |DAILY BEAST 

Whenever I take a clickbait quiz to determine which of The Avengers I would be, I always game the questions to aim for the Hulk. Model Minority Rage: Why the Hulk Should Be an Asian Guy |Arthur Chu |July 18, 2014 |DAILY BEAST 

Pop quiz: How many of the top 15 highest-U.S.-grossing movies of all time—adjusted for inflation—star comic-book characters? Hollywood Declares 2014 the Year of the Bible |Andrew Romano |January 9, 2014 |DAILY BEAST 

Take the quiz below and see if you can match the spouse to her post-scandal statement. The Good Wives Club: Who Said It? |Eliza Shapiro, Sam Schlinkert |July 25, 2013 |DAILY BEAST 

I scored a 50 percent on that little quiz, which I have since learned is proof that I live under a boulder. My Bitcoin (Mis)adventure |Winston Ross |April 4, 2013 |DAILY BEAST 

So whilst we was eatin' breakfast I begins t' quiz, an', one way an' another, lets on I wanted t' see that Injun scout. Raw Gold |Bertrand W. Sinclair 

Robinson enrolled him in his police and it was the fashion openly to quiz, and secretly respect him. It Is Never Too Late to Mend |Charles Reade 

Hardly had they taken their places when Napoleon began to quiz Betsy on the fondness of the English for "rosbif and plum pudding." Napoleon's Young Neighbor |Helen Leah Reed 

The Emperor continued to tease and quiz, pulling Betsy's ear or her dress, and always managing to escape being caught. Napoleon's Young Neighbor |Helen Leah Reed 

One day at the meet this young man said to Captain Bolton, "Let us quiz the old fellow." Yorkshire Oddities, Incidents and Strange Events |S. Baring-Gould