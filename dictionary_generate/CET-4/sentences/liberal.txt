She endorsed Biden in April — after Sanders, with whom she had been fighting for the liberal vote, did so — and was widely reported to have been on Biden’s short list as a possible running mate. Trump, in town hall, says he wouldn’t have done anything differently on pandemic |Colby Itkowitz, Josh Dawsey, Felicia Sonmez, John Wagner |September 16, 2020 |Washington Post 

San Diego’s new policy seems to be a little less liberal, according to a document obtained by Voice of San Diego. The Learning Curve: San Diego Unified Is Terrified of Kids Opting Out |Will Huntsberry |September 10, 2020 |Voice of San Diego 

That’s two of the court’s remaining four liberal justices in their 80s. Vote for Biden (duh) |Kevin Naff |September 10, 2020 |Washington Blade 

It’s a little bit more conservative, but you still have plenty of liberal thought here. Full Transcript: Tomi Lahren on ‘The Carlos Watson Show’ |Daniel Malloy |August 31, 2020 |Ozy 

For the first time this century, a liberal Democratic candidate has a real shot at the seat, according to forecastsin the deep-red state. Trending News For Our Community |Tanya Christian |August 28, 2020 |Essence.com 

Obviously, the first obligation of all liberal democratic governments is to enforce the rule of law. Ayaan Hirsi Ali: Our Duty Is to Keep Charlie Hebdo Alive |Ayaan Hirsi Ali |January 8, 2015 |DAILY BEAST 

To be a liberal, you have to stand up for liberal principles. Bill Maher: Hundreds of Millions of Muslims Support Attack on ‘Charlie Hebdo’ |Lloyd Grove |January 8, 2015 |DAILY BEAST 

A hundred ultra-wealthy liberal and conservative donors have taken over the political system. The 100 Rich People Who Run America |Mark McKinnon |January 5, 2015 |DAILY BEAST 

Liberal Democrats like to blow their bugles about how all the big money in politics comes from rich Republicans. The 100 Rich People Who Run America |Mark McKinnon |January 5, 2015 |DAILY BEAST 

The election of 1964 produced the most liberal Congress since the Democratic landslide of 1936. Thank Congress, Not LBJ for Great Society |Julian Zelizer, Scott Porch |January 4, 2015 |DAILY BEAST 

But the liberal soul deviseth liberal things, and by liberal things shall he stand. The Life and Most Surprising Adventures of Robinson Crusoe, of York, Mariner (1801) |Daniel Defoe 

She must be freed through the progress of Liberal ideas in France and Germany—not by her own inherent energies. Glances at Europe |Horace Greeley 

Then he suddenly went in for politics and announced himself an uncompromising Liberal. Ancestors |Gertrude Atherton 

Joseph, with his liberal ideas, had attempted to free the people from clerical thraldom. Napoleon's Marshals |R. P. Dunn-Pattison 

A woman with such capabilities would be wasted in the rle of a mere countess—but as the wife of an aspiring Liberal statesman! Ancestors |Gertrude Atherton