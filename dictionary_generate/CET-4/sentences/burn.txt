Fuel spilled by a tanker burns in the Cuyahoga River on August 25th. 51 Years Later, the Cuyahoga River Burns Again |Wes Siler |August 28, 2020 |Outside Online 

There are just so many reasons not to pick up the drip torch and start a prescribed burn even though it’s the safe, smart thing to do. They Know How to Prevent Megafires. Why Won’t Anybody Listen? |by Elizabeth Weil |August 28, 2020 |ProPublica 

Burn bosses in California can more easily be held liable than their peers in some other states if the wind comes up and their burn goes awry. They Know How to Prevent Megafires. Why Won’t Anybody Listen? |by Elizabeth Weil |August 28, 2020 |ProPublica 

California, of course, uses aircraft—it has both its own fleet and can employ contractors—to mount full-court presses on fires, but the practice is certainly not limited to combating burns in the Golden State. How aerial firefighters battle blazes from the skies |Rob Verger |August 27, 2020 |Popular-Science 

The battery, she alleged, exploded and left her with severe burns. Amazon’s days of dodging liability for its marketplace could be numbered |Marc Bain |August 14, 2020 |Quartz 

Hatuey replied that he would rather burn and be sent to hell than ever again encounter people as cruel as the Spanish. The Life and Hard Times Of The Family A Cuban Defector Left Behind |Brin-Jonathan Butler |December 19, 2014 |DAILY BEAST 

Are sociopathic animals in while sociopathic people burn in hell? Sorry, Internet: Pope Francis Didn't Open Paradise to Pets |Candida Moss |December 14, 2014 |DAILY BEAST 

Related: Infographic: How Much Exercise It Takes to Burn Off Thanksgiving Dinner 6. 12 Thanksgiving Weight Loss Tips That Actually Work |DailyBurn |November 27, 2014 |DAILY BEAST 

Soon, though, voices from off camera begin shouting for retribution, not justice, chanting “Burn this b**** down.” Michael Brown’s Stepfather Tells Crowd, ‘Burn This Bitch Down’ |Jack Holmes, The Daily Beast Video |November 25, 2014 |DAILY BEAST 

In order to get the ghosts to glow, we had to do what was called a double burn. Garfield Television: The Cat Who Saved Primetime Cartoons |Rich Goldstein |November 5, 2014 |DAILY BEAST 

Truth is a torch, but one of enormous size; so that we slink past it in rather a blinking fashion for fear it should burn us. Pearls of Thought |Maturin M. Ballou 

On the thirteenth of the same month they bound to the stake, in order to burn alive, a man who had two religious in his house. The Philippine Islands, 1493-1898, Volume XX, 1621-1624 |Various 

He couldn't sell them; he couldn't burn them; he was even compelled to insure them, to his intense disgust. The Pit Town Coronet, Volume I (of 3) |Charles James Wills 

They used to declare that every unbaptised baby would go to Hell and burn for ever in fire and brimstone. God and my Neighbour |Robert Blatchford 

And it shall devour the mountains, and burn the wilderness, and consume all that is green as with fire. The Bible, Douay-Rheims Version |Various