No matter how this tournament ends, Durant deserves a hearty salute. Other NBA Superstars Ditched the Olympics. Kevin Durant Showed Up, and He's Keeping Team USA on Track for the Gold |Sean Gregory/Tokyo |August 3, 2021 |Time 

The first few minutes of play offered an even better salute, however, with Denmark’s Yussuf Poulson scoring 1 minute, 39 seconds into the match and setting off roars from the 25,000 fans that Eriksen may well have heard in his hospital room. Christian Eriksen will have a heart starter implanted after collapsing during Euro game |Cindy Boren |June 17, 2021 |Washington Post 

The nation stopped to give a Memorial Day salute and to honor the Americans who sacrificed their lives for our country this week. Vaccine reluctance makes no sense in a nation of patriots |Petula Dvorak |May 31, 2021 |Washington Post 

One of the more enduring items in the company’s collection is a cotton T-shirt with the word “AMATEUR” confidently emblazoned across the chest—a salute to runners who are doing it “for the love.” Tracksmith Made Running Culture Something You Can Buy |Martin Fritz Huber |January 11, 2021 |Outside Online 

This salute to extra pandemic pounds features Big Lotto and is a party anthem that will make you crank up the volume and turn up in your own free time. Stay Trippy With This O.G.-Laced Playlist Featuring Juicy J |cmurray |November 27, 2020 |Essence.com 

The seemingly endless ranks snapped to attention on command and thousands of white gloves rose in salute. Choking Back Tears, Thousands of Cops Honor Fallen Officer Ramos |Michael Daly |December 28, 2014 |DAILY BEAST 

We salute a venerable lineage of strong women, big hair, and drama. Ariana Grande, This Is How to Be a Diva |Tim Teeman |October 21, 2014 |DAILY BEAST 

Rather than salute with the coffee cup in his hand, President Obama hands the cup to an aide standing on the steps behind him. Obama, the Coffee Salute, and the Dementia on the Right |Sally Kohn |September 25, 2014 |DAILY BEAST 

General Grant issued a general order that “every battery bearing upon the enemy” fire in salute. Atlanta’s Fall Foretold The End Of Civil War Bloodshed |Marc Wortman |September 1, 2014 |DAILY BEAST 

As eight of her novels are republished, we salute a doyenne of literary fiction whose work juxtaposes tragedy and comedy. The Rediscovered Genius of Muriel Spark |Lucy Scholes |July 29, 2014 |DAILY BEAST 

Of silence before them that salute thee: of looking upon a harlot: and of turning away thy face from thy kinsman. The Bible, Douay-Rheims Version |Various 

MacRae's heels clicked together and his right hand went up in the stiff military salute. Raw Gold |Bertrand W. Sinclair 

A full General landing to inspect overseas is entitled to a salute of 17 guns—well, I got my dues. Gallipoli Diary, Volume I |Ian Hamilton 

As he stepped forward to salute her, she presented her cheek to him, and suddenly stabbed him dead at her feet. The Pit Town Coronet, Volume I (of 3) |Charles James Wills 

"Gentlemen, I have the honour to salute you," said Monsieur de Kerguel with a profound obeisance. The Pit Town Coronet, Volume I (of 3) |Charles James Wills