You might object that the approach creates only superficial mimicry — the way some alchemists claimed to have “made gold” by applying some surface treatment to another metal that induced chemical reactions to gave the metal a golden sheen. Alchemy Arrives in a Burst of Light |Philip Ball |September 30, 2020 |Quanta Magazine 

Popular critiques of millennials, in the mid-2000s and beyond, ranged from “They’re too entitled” to “Technology and social media are making them superficial.” Why Sex Is No Longer a Teen Commodity |Joshua Eferighe |September 21, 2020 |Ozy 

He also suffered a superficial wound to his left thigh and a graze wound to his forehead. Prosecutors charge teen in fatal shooting of two protestors and wounding of a third in Kenosha |kdunn6 |August 28, 2020 |Fortune 

The government could easily make superficial efforts to address public backlash against visible data collection without really touching the core of the Ministry of Public Security’s national operations, says Wang, of Human Rights Watch. Inside China’s unexpected quest to protect data privacy |Tate Ryan-Mosley |August 19, 2020 |MIT Technology Review 

“They have a superficial understanding of the marketplace,” he added. ‘You need to fix the entire line’: Publishers’ sales and revenue teams struggle with entrenched diversity problem |Max Willens |July 10, 2020 |Digiday 

Her Miss America win transcended mere superficial beauty standards. Why Was Bess Myerson the First and Last Jewish Miss America? |Emily Shire |January 7, 2015 |DAILY BEAST 

But it will take more than superficial solidarity to dismantle those structures and the ideologies that birthed them. Jamie Foxx: Get Over the Black ‘Annie’ |Stereo Williams |December 20, 2014 |DAILY BEAST 

It is loathed by some critics who find it patronizing, silly, and superficial. 'The Newsroom' Ended As It Began: Weird, Controversial, and Noble |Kevin Fallon |December 15, 2014 |DAILY BEAST 

The implication is that she might even have assisted her husband inflicting his superficial wounds. Did Joran Van Der Sloot Fake His Prison Shanking? |Andrea Zarate, Barbie Latza Nadeau |November 5, 2014 |DAILY BEAST 

Every Bentley is made to order, which essentially means anything “superficial” is possible. Behind the Wheel of the Bespoke Bentley |Zoe Settle |October 27, 2014 |DAILY BEAST 

"It has a superficial, glittering prettiness, without—" I hesitated to find the word I wanted. Three More John Silence Stories |Algernon Blackwood 

This design reveals a stumbling-block that superficial people fall over. Life of Richard Trevithick, Volume II (of 2) |Francis Trevithick 

So strong were prejudices in favour of superficial morality in even that licentious age! The Portsmouth Road and Its Tributaries |Charles G. Harper 

A little or superficial knowledge may incline a man's mind to atheism; but depth in philosophy bringeth him back to religion. Gospel Philosophy |J. H. Ward 

In touching truly deep and serious things Mistral is often superficial, and passes them off with a commonplace. Frdric Mistral |Charles Alfred Downer