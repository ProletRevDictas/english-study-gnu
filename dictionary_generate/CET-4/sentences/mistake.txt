You’ve got to have the ability to block out past mistakes, or even the future. College basketball’s kingdoms have gone haywire — in case you just started paying attention |Chuck Culpepper |February 12, 2021 |Washington Post 

What’s more, not every speaker may be fluent, meaning that a child hearing more languages might also hear mistakes. Learning a second language early might have ripple effects throughout your life |Rahul Rao |February 9, 2021 |Popular-Science 

She tried following the instructions but made so many mistakes that “it made my whole screen just look red,” she said. Samaira Mehta shows other kids the fun of coding by inventing board games |Gina Rich |February 9, 2021 |Washington Post 

UNC-Miami postponed after two Tar Heels are shown celebrating Duke win without masks“Every little mistake we made, they just took advantage of it,” Wiggins said, referencing Ohio State’s 21 points off turnovers. Maryland misses a chance to boost its NCAA tournament hopes with a loss to No. 4 Ohio State |Emily Giambalvo |February 9, 2021 |Washington Post 

On Sunday, she updated her accounts to show that she had started the process of trying to remove what is presumably one of the worst mistakes of her life. She used Gorilla Glue as hairspray. After 15 washes and a trip to the ER, it still won’t budge. |Lateshia Beachum |February 8, 2021 |Washington Post 

This time it would be the biggest mistake for the Western press to repeat that—absolutely the biggest mistake. Ayaan Hirsi Ali: Our Duty Is to Keep Charlie Hebdo Alive |Ayaan Hirsi Ali |January 8, 2015 |DAILY BEAST 

Scalise has called the talk, which he delivered in a hotel outside New Orleans, “a mistake I regret.” The Louisiana Racists Who Courted Steve Scalise |Jason Berry |January 3, 2015 |DAILY BEAST 

Scalise offered his contrition that he had made a mistake and apologized for appearing before a group some 12 years ago. Reverend Jeremiah Wright Was Worse Than Scalise |Ron Christie |January 2, 2015 |DAILY BEAST 

Make no mistake: The technology exists—has existed for a long while—to stop this from happening. Red Tape and Black Boxes: Why We Keep ‘Losing’ Airliners in 2014 |Clive Irving |December 29, 2014 |DAILY BEAST 

I made the mistake of promising one group of guys they could ask me anything if they answered my questions. The Craziest Date Night for Single Jews, Where Mistletoe Is Ditched for Shots |Emily Shire |December 26, 2014 |DAILY BEAST 

Instead of giving you a chance to say, "He has made a mistake," he forced you to say, "He has shown how to get out of a mistake." Music-Study in Germany |Amy Fay 

My mother now tells me that she knew of this mistake, an error of the New York paper in copying the item from a Southern journal. The Boarded-Up House |Augusta Huiell Seaman 

I must make no mistake, and blunder into a national type of features, all wrong; if I make your mask, it must do us credit. Checkmate |Joseph Sheridan Le Fanu 

They never knew how it got there, but thinking it was by mistake, Glavis took it into the house and spread it out. The Homesteader |Oscar Micheaux 

"Yes, there has been a mistake," she said peevishly, turning in with him to a small room they used as a breakfast-room. Elster's Folly |Mrs. Henry Wood