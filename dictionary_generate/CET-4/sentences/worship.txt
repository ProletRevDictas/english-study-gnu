Newsom settled the issue after county supervisors asked the state to discount SDSU’s high infection rate from the county total to avoid reverting back to restrictions on businesses and worship centers. Morning Report: Lincoln Abruptly Canceled AP Class |Voice of San Diego |September 17, 2020 |Voice of San Diego 

Indoor shopping centers could still operate at 25 percent capacity, but restaurants, houses of worship, museums and gyms would have to move all operations outdoors. Morning Report: The Dreaded Purple Tier |Voice of San Diego |September 16, 2020 |Voice of San Diego 

It was one of the strictest in the world, with the government confining people to their homes, suspending transport networks, and closing almost all businesses, offices, and places of worship. India tried to tame COVID-19 by sacrificing its economy. It got the worst of both worlds instead |Grady McGregor |September 1, 2020 |Fortune 

Outside of movie theaters and places of worship, one of the first spaces to open as states started rolling back stay-at-home orders back in mid-May were … gyms. Clean, Lean, Mean Machines: 5 Super COVID-Safe Gyms |Joshua Eferighe |August 28, 2020 |Ozy 

From giving at your place of worship to volunteering at your local food bank to making sure that when you get takeout it’s not from a big chain, neighborhood acts make a difference. Sunday Magazine: A World in Need |Daniel Malloy |August 16, 2020 |Ozy 

Some people worship money, some people worship power, and lots of people worship themselves. Santa Fails One More Time |P. J. O’Rourke |December 27, 2014 |DAILY BEAST 

Are we actually taking into account what some people worship? Santa Fails One More Time |P. J. O’Rourke |December 27, 2014 |DAILY BEAST 

His constant worship of his wife stands in stark contrast to scandals of the domestic nature in other sports. The Story of the World’s Greatest Cricket Player |William O’Connor |December 24, 2014 |DAILY BEAST 

A long-running argument exists over whether Christians and Muslims worship the same God. Does Pope Francis Believe Christians and Muslims Worship the Same God? |Jay Parini |December 7, 2014 |DAILY BEAST 

The worship of some of these deities appears to have included orgiastic rituals: music, wine, sex. Meet Krampus, the Seriously Bad Santa |Jay Michaelson |December 5, 2014 |DAILY BEAST 

Her exquisite, frail beauty held a strength that mocked the worship in his eyes and voice. The Wave |Algernon Blackwood 

To swear by his name is not to do sacrifice; and is therefore to perform another part of his worship. The Ordinance of Covenanting |John Cunningham 

In addition to the great chief temple, there were many smaller places of worship, with bell and tablet houses. Our Little Korean Cousin |H. Lee M. Pike 

Perhaps he is a little conscious of his charm; if so, it is hardly his fault, for hero-worship has been his lot from boyhood. First Plays |A. A. Milne 

It is a part of religious worship, and claims that solemnity of mind that is due to every religious service. The Ordinance of Covenanting |John Cunningham