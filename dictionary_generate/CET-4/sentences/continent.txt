Still, prices across the continent are likely to be pressured amid excess supply, said Rupert Claxton, meat director at consultant Gira. Europe is on high alert after a deadly swine virus emerges in Germany |Bernhard Warner |September 10, 2020 |Fortune 

This is thought to be largely due to belts all along the northern parts of the continents where the snow has been melting much earlier due to climate change. Slow, meandering hurricanes are often more dangerous—and they’re getting more common |Greta Moran |September 9, 2020 |Popular-Science 

Many of the continent’s highlights are in the Northern Territory, an area left unscathed during last season’s wildfires. 27 Epic Trips to Start Planning Now |The Editors |September 8, 2020 |Outside Online 

These hordes can cross continents, eating through crops along the way. A single chemical may draw lonely locusts into a hungry swarm |Jonathan Lambert |September 7, 2020 |Science News For Students 

Projects like ReGen villages, the so-called “Tesla of ecovillages,” now partnering with municipalities on four continents. Solarpunk Is Growing a Gorgeous New World in the Cracks of the Old One |Carin Ism |September 6, 2020 |Singularity Hub 

Opponents of Muslims and immigrants across the continent are claiming vindication in the aftermath of the Charlie Hebdo attack. Europe’s Islam Haters Say We Told You So |Barbie Latza Nadeau |January 8, 2015 |DAILY BEAST 

World War II is still a long way off, but the seeds of conflict are already being sown on the continent. What Downton’s Fashion Really Means |Katie Baker |January 2, 2015 |DAILY BEAST 

It can be hard to wrap your head around the problems facing the continent because they might seem ancient to us. Silicon Valley Sets Its Sights on Africa |Christian Borys |December 22, 2014 |DAILY BEAST 

But this is often seen as little more than a way of trying to defame Edward III on the continent. The Sex Life of King Richard III's Randy Great Great Great Grandfather |Tom Sykes |December 4, 2014 |DAILY BEAST 

Gurira says she approaches her work with a dual-continent mindset. Walking Dead’s Danai Gurira Vs. Boko Haram |Kristi York Wooten |November 30, 2014 |DAILY BEAST 

For good or ill, the torrent of rebellion was suffered to break loose, and it soon engulfed a continent. The Red Year |Louis Tracy 

There are some other trees planted, and many small, thrifty forests, such as I had hardly seen before on the Continent. Glances at Europe |Horace Greeley 

These form one of the many island groups that hang like a fringe or festoon on the skirt of the continent of Asia. Alila, Our Little Philippine Cousin |Mary Hazelton Wade 

Thence they have spread all over the continent of South America, and have proved of more real value to it than its mines. Journal of a Voyage to Brazil |Maria Graham 

It is extensively used in the manufacture of cigars, and on the continent it frequently realizes as much as 5s. Tobacco; Its History, Varieties, Culture, Manufacture and Commerce |E. R. Billings.