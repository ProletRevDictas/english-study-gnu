My heart goes out to the unsung IT heroes at the Capitol tonight. Decrypted: How bad was the US Capitol breach for cybersecurity? |Zack Whittaker |January 7, 2021 |TechCrunch 

Feel free to follow us on Twitter tonight for full coverage up and down the ballot. What You Need To Know About Today’s Elections In Kansas, Michigan And Missouri |Nathaniel Rakich (nathaniel.rakich@fivethirtyeight.com) |August 4, 2020 |FiveThirtyEight 

Higher courts, including the Supreme Court had refused to intercede, and the stay was to expire tonight. The Back Alley, Low Blow-Ridden Fight to Stop Gay Marriage in Florida Is Finally Over |Jay Michaelson |January 5, 2015 |DAILY BEAST 

“Either this or stay home and jerk off,” said one guy when I asked why he came tonight. The Craziest Date Night for Single Jews, Where Mistletoe Is Ditched for Shots |Emily Shire |December 26, 2014 |DAILY BEAST 

In both of these latter cases, their eyes show more focus than fun, like tonight is a job. The Craziest Date Night for Single Jews, Where Mistletoe Is Ditched for Shots |Emily Shire |December 26, 2014 |DAILY BEAST 

There's blood on many hands tonight…That blood on the hands starts at City Hall in the Office of the Mayor. Justice League Vigil for Slain NYPD Officers Asks Whose Life Matters |Olivia Nuzzi |December 22, 2014 |DAILY BEAST 

I finally come across the only thing open tonight at the Taj: a deli called GO, the sort of place you might find in Penn Station. I Watched a Casino Kill Itself: The Awful Last Nights of Atlantic City’s Taj Mahal |Olivia Nuzzi |December 8, 2014 |DAILY BEAST 

Let your orders for preparation go round tonight, so that your knaves may be ready to set out betimes to-morrow. St. Martin's Summer |Rafael Sabatini 

Murphy was kill-crazy, and tonight the Monitor rifle in his hands had made him feel like a god. Hooded Detective, Volume III No. 2, January, 1942 |Various 

She was a strong-souled, high-spirited girl, but tonight hope seemed extinguished in her breast. St. Martin's Summer |Rafael Sabatini 

If Kip didn't stop his arguing, she wouldn't vouch for him at this meeting tonight at the Weedham home. Hooded Detective, Volume III No. 2, January, 1942 |Various 

"There were one hundred thousand dollars worth of unset diamonds in that store tonight," Black Hood said. Hooded Detective, Volume III No. 2, January, 1942 |Various