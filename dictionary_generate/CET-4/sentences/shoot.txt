We spoke with Egan about where the green shoots are and how publishers’ relationships with platforms are shifting. ‘We’re getting more used to the uncertainty’: BBC Global News chief on ad-funded news |Lucinda Southern |September 10, 2020 |Digiday 

Today, green shoots of community intelligence can be seen all around us. The race for a COVID-19 vaccine shows the power of ‘community intelligence’ |matthewheimer |September 9, 2020 |Fortune 

He has also adapted to the growing demands of his clients, whether they want to be subject of their own styled shoot or simply geek out on photography. Couples spend thousands on a wedding photographer for that perfect shot |Rachel King |September 6, 2020 |Fortune 

It’s so popular that couples carve out time in the itinerary to make a shoot happen, often with the help of drones. Couples spend thousands on a wedding photographer for that perfect shot |Rachel King |September 6, 2020 |Fortune 

His answers were so direct that the planned shoot was over in about half an hour, Ebersole says. ‘House of Cardin’ aims to be more than a fashion documentary |radmarya |August 27, 2020 |Fortune 

Just the hard-on before you shoot unarmed members of the public. 'Babylon' Review: The Dumb Lives of Trigger-Happy Cops |Melissa Leon |January 9, 2015 |DAILY BEAST 

I mean, the reality of it was, I had to go out and get on a horse, and ride in, shoot the gun — how hard was that, right? The Story Behind Lee Marvin’s Liberty Valance Smile |Robert Ward |January 3, 2015 |DAILY BEAST 

But even when the jet will be able to shoot its gun, the F-35 barely carries enough ammunition to make the weapon useful. New U.S. Stealth Jet Can’t Fire Its Gun Until 2019 |Dave Majumdar |December 31, 2014 |DAILY BEAST 

The brand logo turned out to feature a graceful archer on horseback, in a Tatar national costume, poised to shoot his arrow. Rebranding The Land of Mongol Warriors & Ivan The Terrible |Anna Nemtsova |December 25, 2014 |DAILY BEAST 

At the beginning of the video and before the call to kill police, you can hear what sounds like, “arms up, shoot back!” The Monsters Who Screamed for Dead Cops |Jacob Siegel |December 23, 2014 |DAILY BEAST 

I was right, so it seems, about getting ashore before the enemy could see to shoot out to sea. Gallipoli Diary, Volume I |Ian Hamilton 

That was a considerable sensible commandment of yourn, always to shoot the foremost of the Mexicans when they attacked. Blackwood's Edinburgh Magazine, No. CCCXXXIX. January, 1844. Vol. LV. |Various 

"But I don't see what you can shoot with it," said Davy, feeling that he was somehow getting the worst of the argument. Davy and The Goblin |Charles E. Carryl 

Thereafter he ran away from school twice, having been seized with a romantic and irresistible desire to see and shoot a lion! Hunting the Lions |R.M. Ballantyne 

The sentinels seemed much alarmed, and drew up their carbines as if to shoot. The Courier of the Ozarks |Byron A. Dunn