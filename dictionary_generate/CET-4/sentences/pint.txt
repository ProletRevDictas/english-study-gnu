How to achieve fluffy, icy perfection, with some help from pastry chef Michelle PolzineCome summer, my freezer is stocked with pints of ice cream. This Summer, Learn to Make a Perfect Granita |Emma Wartzman |July 1, 2021 |Eater 

It’s squeezing into the Grill from Ipanema one night to cheer and drink caipirinhas with Brazil fans, and leaving work early a couple days later for pints and a chorus of “Three Lions” with the England fans at the Queen Vic pub. Where to watch Euro 2020 and Copa America in the D.C. area |Fritz Hahn |June 11, 2021 |Washington Post 

A portion of the pints were distributed to Jeni’s brick-and-mortar stores — which reportedly sold out of the flavor, after people lined up before opening hours in the light of a clear blue morning — but most were reserved for online ordering. Why’d Dolly Parton Come in Here and Crash Jeni’s Website Like That? |Jenny G. Zhang |April 9, 2021 |Eater 

He speaks cornily about fostering “team spirit” and his love of going for a pint with colleagues on a Friday, prepandemic. Can Public Transit Survive the Pandemic? London's New Transport Commissioner Wants You to Believe It Can |Ciara Nugent/London |April 2, 2021 |Time 

This sampler includes four delicious ice-cream flavors, plus a pint of ready-to-bake Chocolate Chunk Cookie Dough. From the Cut: 33 Valentine’s Day Gifts for the Foodie in Your Life |The Cut Staff |February 8, 2021 |Eater 

That year, on August 10, the first woman stepped in for a pint. The Bars That Made America Great |Nina Strochlic |December 28, 2014 |DAILY BEAST 

I order a pint a Fula Farmacia, Casa Bruja's 4.7 percent Blond Ale. House of the Witch: The Renegade Craft Brewers of Panama |Jeff Campagna |November 30, 2014 |DAILY BEAST 

As President, the Father of the Constitution James Madison drank a pint a day. The Booze That Saved America |Kevin Bleyer |November 8, 2014 |DAILY BEAST 

This continues until an impossibly huge amount of ice cream is compressed into the pint. Dr. Mike’s Makes the Best Ice Cream on Earth |Jane & Michael Stern |July 27, 2014 |DAILY BEAST 

Franklin might have been describing James Madison, father of the Constitution, who drank a pint of whiskey every day. Life, Liberty, and the Founding Fathers’ Pursuit of Hoppiness |Kevin Bleyer |July 4, 2014 |DAILY BEAST 

Youre a-going to accommodate us, and wots to prevent my standing treat for a pint or so, in return? Oliver Twist, Vol. II (of 3) |Charles Dickens 

We had each of us a fresh cocoa-nut with a hole bored in it, containing at least a pint of clear, sweet-tasting water. A Woman's Journey Round the World |Ida Pfeiffer 

A pint of meal and a pint of plaster to each rod, is a good mixture to sow in. Tobacco; Its History, Varieties, Culture, Manufacture and Commerce |E. R. Billings. 

One wine-glass of the solution, added to half a pint of tepid water, is sufficient for each application. The Ladies' Book of Etiquette, and Manual of Politeness |Florence Hartley 

One wine-glassful of this solution, added to half a pint of tepid water, is sufficient for each application. The Ladies' Book of Etiquette, and Manual of Politeness |Florence Hartley