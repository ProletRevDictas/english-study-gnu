What Mulan doesn’t know is that her greatest strengths are the very characteristics she’s been urged to hide — her intellect, her quick thinking, her ingenuity — and in the end they’re key to saving her country. Toward a queer Disney canon |Emily VanDerWerff |September 4, 2020 |Vox 

Links and knots are topological characteristics, in that they don’t change when vortices are stretched, compressed or otherwise deformed. An Unexpected Twist Lights Up the Secrets of Turbulence |David H. Freedman |September 3, 2020 |Quanta Magazine 

Sub-6GHz 5G has much better signal characteristics and has a shot of a wide rollout. Qualcomm is bringing 5G to everyone with cheap 5G Snapdragon chips |Ron Amadeo |September 3, 2020 |Ars Technica 

The rule is more of the same, allowing shelter staff to judge the physical characteristics of those seeking services to decide who is sufficiently male or sufficiently female. Trump administration targets homeless trans Americans |Jennifer Wexton |September 2, 2020 |Washington Blade 

Because different types of neurons have their characteristic ways of spiking—that is, the “shape” of their spikes are diverse—the chip can also be configured to detect the particular spikes you’re looking for. Neuralink’s Wildly Anticipated New Brain Implant: the Hype vs. the Science |Shelly Fan |September 1, 2020 |Singularity Hub 

She said the defining characteristic of her husband was his sensitivity. The Life and Hard Times Of The Family A Cuban Defector Left Behind |Brin-Jonathan Butler |December 19, 2014 |DAILY BEAST 

He wears his characteristic white shalwar kameez with a tattered gray waistcoat. Heart of Darkness: Into Afghanistan’s Taliban Valley |Matt Trevithick, Daniel Seckman |November 15, 2014 |DAILY BEAST 

Most have been straightforward cases where the child came in with the characteristic rash. Predator Doctors Take Advantage of Patients With ‘Chronic Lyme’ Scam |Russell Saunders |September 19, 2014 |DAILY BEAST 

Any person or group can be prejudiced against another group, for any reason and based on any characteristic. What We Need Are Anti-Racists |Gene Robinson |August 24, 2014 |DAILY BEAST 

It's remarkable, too, that during his delivery, Lou showed no significant signs of slurred speech, often so characteristic of ALS. The Stacks: The Day Lou Gehrig Delivered Baseball’s Gettysburg Address |Ray Robinson |July 4, 2014 |DAILY BEAST 

Fibrinous casts are characteristic of fibrinous bronchitis, but may also be found in diphtheria of the smaller bronchi. A Manual of Clinical Diagnosis |James Campbell Todd 

In this case, I suspect, there was co-operant a strongly marked childish characteristic, the love of producing an effect. Children's Ways |James Sully 

A characteristic which distinguished them and which impressed Mrs. Pontellier most forcibly was their entire absence of prudery. The Awakening and Selected Short Stories |Kate Chopin 

Most characteristic is the presence of Curschmann's spirals, Charcot-Leyden crystals, and eosinophilic leukocytes. A Manual of Clinical Diagnosis |James Campbell Todd 

When pneumonia occurs during the course of a chronic bronchitis, the characteristic rusty red sputum may not appear. A Manual of Clinical Diagnosis |James Campbell Todd