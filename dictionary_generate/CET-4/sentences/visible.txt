“One thing we’re seeing is that people are working longer and harder because they feel less visible,” said Rayl. ‘Integrators’ and ‘separators’: How managers are helping the two types of remote workers survive the pandemic |Jen Wieczner |September 16, 2020 |Fortune 

How visible they are has nothing to do with how strong you are or how hard you’re working. How to get a six-pack (or even an eight-pack) |Sara Chodosh |September 4, 2020 |Popular-Science 

In describing a pig that had two Neuralink implants, Musk said his team wanted to prove that a living creature can have multiple devices implanted without them being visible. Elon Musk shows off Neuralink brain implant technology in a living pig |jonathanvanian2015 |August 29, 2020 |Fortune 

When we move teams to remote, what we first do is make the work visible without constantly narrating it. ‘It can take on a panopticon effect’: Slack’s presenteeism problem grows with no end in sight for remote work |Lucinda Southern |August 28, 2020 |Digiday 

Both of its visible planets are unlike anything seen in our solar system. A weird cousin of our solar system is caught on camera |Lisa Grossman |August 26, 2020 |Science News For Students 

But he elected instead to have a very visible affair with a music-hall star. The Real-Life ‘Downton’ Millionairesses Who Changed Britain |Tim Teeman |December 31, 2014 |DAILY BEAST 

On May 9, which Moscow commemorates as World War II “Victory Day,” Klaus paid a highly visible visit to the Russian Embassy. Vaclav Klaus, Libertarian Hero, Has His Wings Clipped by Cato Institute |James Kirchick |December 22, 2014 |DAILY BEAST 

A sign peeking out from the heavy forest is barely visible on the trip back. The Congo's Forgotten Colonial Getaway |Nina Strochlic |December 18, 2014 |DAILY BEAST 

“There has been no visible progress of oil removal,” he said in the April 2012 inspection report. Two Texas Regulators Tried to Enforce the Rules. They Were Fired. |David Hasemyer, InsideClimate News |December 9, 2014 |DAILY BEAST 

One visible trend is the re-use of components to meet different mission needs. How China Will Track—and Kill—America’s Newest Stealth Jets |Bill Sweetman |December 2, 2014 |DAILY BEAST 

Genoa has but recently and partially felt the new impulse, yet even here the march of improvement is visible. Glances at Europe |Horace Greeley 

A lateen sail was visible in the direction of Cat Island, and others to the south seemed almost motionless in the far distance. The Awakening and Selected Short Stories |Kate Chopin 

He saw with evident pleasure the outward and visible signs of the old earl's immense wealth. The Pit Town Coronet, Volume I (of 3) |Charles James Wills 

In the metal of the tenor several coins are visible, one being a Spanish dollar of 1742. Showell's Dictionary of Birmingham |Thomas T. Harman and Walter Showell 

There was still visible on it the stain where he had wiped his hand, and this stain seemed certainly blood. Uncanny Tales |Various