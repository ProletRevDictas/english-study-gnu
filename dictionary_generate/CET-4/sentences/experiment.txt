IBM hopes that a platform like RoboRXN could dramatically speed up that process by predicting the recipes for compounds and automating experiments. IBM has built a new drug-making lab entirely in the cloud |Karen Hao |August 28, 2020 |MIT Technology Review 

The hope there is for improved sensitivity in searches for dark matter or experiments that might reveal some long-sought flaws in our standard model of particle physics. Cosmic rays could pose a problem for future quantum computers |Neel Patel |August 26, 2020 |MIT Technology Review 

The experiment represents early progress toward the possible development of an ultra-secure communications network beamed from space. U.S. vies for leadership in quantum and A.I. with $1 billion bet |rhhackettfortune |August 26, 2020 |Fortune 

The new experiment represents, however, the first time scientists have applied machine learning to “validation,” a further step toward confirming results that involves additional statistical calculation. 50 new planets, including one as big as Neptune, are identified using A.I. |rhhackettfortune |August 26, 2020 |Fortune 

At first, the sites amounted to experiments on the outer edges of the crypto universe, but in 2020 they have started to attract real money. Crypto soars again as traders embrace ‘DeFi’ and ‘yield farming’—but some see echoes of the 2017 bubble |Jeff |August 25, 2020 |Fortune 

To put it rather uncharitably, the USPHS practiced a major dental experiment on a city full of unconsenting subjects. Anti-Fluoriders Are The OG Anti-Vaxxers |Michael Schulson |July 27, 2016 |DAILY BEAST 

If the noble experiment of American democracy is to mean anything, it is fidelity to the principle of freedom. The Sony Hack and America’s Craven Capitulation To Terror |David Keyes |December 19, 2014 |DAILY BEAST 

A classroom experiment seeks to demonstrate what it looks like. What Is Privilege? |The Daily Beast Video |December 11, 2014 |DAILY BEAST 

This video, courtesy of BuzzFeed, tries a bit of an experiment to get some answers. The Bottled Water Taste Test |The Daily Beast Video |December 1, 2014 |DAILY BEAST 

In the fall of 1992, Booker became a vegetarian “as an experiment,” he said, “and I was surprised by how much my body took to it.” Talking Tofurky With Newly Vegan Cory Booker |Vlad Chituc |November 26, 2014 |DAILY BEAST 

With Bacon, experientia does not always mean observation; and may mean either experience or experiment. The Mediaeval Mind (Volume II of II) |Henry Osborn Taylor 

I made the experiment two years ago, and all my experience since has corroborated the conclusion then arrived at. Assimilative Memory |Marcus Dwight Larrowe (AKA Prof. A. Loisette) 

But this is quite enough to justify the inconsiderable expense which the experiment I urge would involve. Glances at Europe |Horace Greeley 

He commenced to experiment in electro-pneumatics in the year 1860, and early in 1861 communicated his discoveries to Mr. Barker. The Recent Revolution in Organ Building |George Laing Miller 

Readers will doubtless be familiar with the well-known experiment illustrating this point. The Recent Revolution in Organ Building |George Laing Miller