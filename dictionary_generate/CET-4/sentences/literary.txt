Steve Erickson is the author of 10 novels including Shadowbahn, Zeroville, These Dreams of You and Our Ecstatic Days—as well as two works of literary non-fiction about politics and culture—that have been translated into 11 languages. In Science Fiction, We Are Never Home - Issue 95: Escape |Steve Erickson |February 10, 2021 |Nautilus 

There, he determined to relaunch his stalled literary career. ‘The Queen’s Gambit’ is a bestseller, but its author, Walter Tevis, was hardly a one-hit wonder |Michael Dirda |February 3, 2021 |Washington Post 

We could also use a literary culture that nurtures more writers the way it has Duchovny. David Duchovny wants to be taken seriously as a novelist. His new book makes a good case. |Mark Athitakis |February 1, 2021 |Washington Post 

Her debut poetry volume is expected in September, her first picture book is on the way, and she has several other literary projects cooking. Amanda Gorman will bring poetry—and politics—to the Super Bowl |Kristen Bellstrom |January 28, 2021 |Fortune 

Much as I value serious literary fiction, I find reading it to be exceptionally draining. When I find fiction too draining, I turn to books about books. They can be as thrilling as a whodunit. |Michael Dirda |January 27, 2021 |Washington Post 

From this attitude he draws a singular comic and literary power. Houellebecq’s Incendiary Novel Imagines France With a Muslim President |Pierre Assouline |January 9, 2015 |DAILY BEAST 

Lacey Noonan's A Gronking to Remember makes 50 Shades of Grey look like Madame Bovary in terms of its literary sophistication. ‘A Gronking to Remember’ Speed Read: 8 Naughtiest Bits |Emily Shire |January 7, 2015 |DAILY BEAST 

The goal is to create a literary anatomy of the last century—or, to be precise, from 1900 to 2014. The 2014 Novel of the Year |Nathaniel Rich |December 29, 2014 |DAILY BEAST 

To reclaim it, he had to move beyond established conventions about how a literary career should be conducted. A Year In The Life of The Canterbury Tales’ Storied Beginnings |Wendy Smith |December 25, 2014 |DAILY BEAST 

A new book from Mallory Ortberg imagines what literary legends including King Lear and Jane Eyre would have texted. What Would Jane Eyre Sext? |Jennie Yabroff |December 23, 2014 |DAILY BEAST 

Louis Petit de Bachaumon died; a native of Paris, known as the author of several literary works. The Every Day Book of History and Chronology |Joel Munsell 

It was strenuously opposed by all possible means, governmental, legislative, and literary. Tobacco; Its History, Varieties, Culture, Manufacture and Commerce |E. R. Billings. 

Samuel Badcock, an English divine and writer, died; admired as a pulpit orator and a man of literary talent. The Every Day Book of History and Chronology |Joel Munsell 

She was a woman of great intellectual endowment, with highly cultivated literary tastes. Fifty Years of Railway Life in England, Scotland and Ireland |Joseph Tatlow 

Robert Harley, earl of Oxford, died; an English statesman and literary character. The Every Day Book of History and Chronology |Joel Munsell