Then I glanced at the second book and woefully added it to the reject pile. How to take an overnight trip with your two-wheeled vehicle |Melanie D.G. Kaplan |March 26, 2021 |Washington Post 

“I firmly and wholeheartedly reject the allegations,” Hawking said from a Cambridge Hospital. The Other Side of Stephen Hawking: Strippers, Aliens, and Disturbing Abuse Claims |Marlow Stern |November 6, 2014 |DAILY BEAST 

As Assaf put it, “this is one way to reject extremism and make it so the people are not afraid.” Middle East Goes Monty Python on ISIS |Dean Obeidallah |October 29, 2014 |DAILY BEAST 

And I was wondering how you combat that impulse to reject the young? Martin Amis Talks About Nazis, Novels, and Cute Babies |Ronald K. Fried |October 9, 2014 |DAILY BEAST 

His Mormon faith was no reason to reject his candidacy, he argued. It’s Official: Religion Doesn’t Make You More Moral |Elizabeth Picciuto |September 23, 2014 |DAILY BEAST 

I reject angrily authority that exists without my respect. Brecht's Mercenary Mother Courage Turns 75 |Katie Baker |September 10, 2014 |DAILY BEAST 

If you use it wisely, it may be Ulysses' hauberk; if you reject it, the shirt of Nessus were a cooler winding-sheet! The Pastor's Fire-side Vol. 3 of 4 |Jane Porter 

No man would reject the words of God if he knew that God spoke those words. God and my Neighbour |Robert Blatchford 

Again, a principal cannot accept part of an agent's act and reject the remainder. Putnam's Handy Law Book for the Layman |Albert Sidney Bolles 

If eager to get the most possible, she would reject the gift of money and claim her dower rights. Putnam's Handy Law Book for the Layman |Albert Sidney Bolles 

Early stages of great grief reject comfort, but they long, with intense longing, for sympathy. The Ladies' Book of Etiquette, and Manual of Politeness |Florence Hartley