They are speeding up trading on financial markets, making and losing fortunes in micro-seconds. Algorithms Workers Can’t See Are Increasingly Pulling the Management Strings |Tom Barratt |August 28, 2020 |Singularity Hub 

Patrick Howell O’Neill profiles Israeli spyware company NSO, which has quietly built up its fortune by helping governments around the world snoop on people. A world divided into “cans” and “cannots” |Katie McLean |August 19, 2020 |MIT Technology Review 

Today’s cannabis industry is a constantly shifting landscape of legal developments, policy changes, company fortunes, and product innovations. The best cannabis industry newsletters, sites, and podcasts |Jenni Avins |August 13, 2020 |Quartz 

Unlike other rich women of her time, Scripps worked for her fortune instead of marrying or inheriting it, McClain said. Suffragette City: San Diego’s Crucial Role in Getting Women the Vote |Randy Dotinga |August 6, 2020 |Voice of San Diego 

Backlinks are one way to get this boost without spending a fortune. Nine mistakes to avoid when contacting websites for backlinks |Raj Dosanjh |July 29, 2020 |Search Engine Watch 

The building used to be an all-girls school, and when it was initially purchased by Fortune it was dilapidated. His First Day Out Of Jail After 40 Years: Adjusting To Life Outside |Justin Rohrlich |January 3, 2015 |DAILY BEAST 

Stanley Richards, Senior Vice President of the Fortune Society, gave a tour along with a few residents. His First Day Out Of Jail After 40 Years: Adjusting To Life Outside |Justin Rohrlich |January 3, 2015 |DAILY BEAST 

That good fortune meant CNN had the only TV correspondent on the scene. CNN's Overnight Sydney Star |Lloyd Grove |December 16, 2014 |DAILY BEAST 

Hitchcock had the historical good fortune to have worked from silent films through television. Alfred Hitchcock’s Fade to Black: The Great Director’s Final Days |David Freeman |December 13, 2014 |DAILY BEAST 

“I suppose she'll want a fortune as well,” he says, looking at me as if I were Liv Ullmann's agent. Alfred Hitchcock’s Fade to Black: The Great Director’s Final Days |David Freeman |December 13, 2014 |DAILY BEAST 

Sleek finds it far harder work than fortune-making; but he pursues his Will-o'-the-Wisp with untiring energy. The Pit Town Coronet, Volume I (of 3) |Charles James Wills 

The fortune was proving quite as large as he had expected, and not even an inquest had been held upon the dead man. Uncanny Tales |Various 

But "the cards never forgive," and as a rule Dame Fortune is relentless to the reckless player. The Pit Town Coronet, Volume I (of 3) |Charles James Wills 

Aristide always regarded the fortune of the moment as if it would last forever. The Joyous Adventures of Aristide Pujol |William J. Locke 

The fact that her fortune was vaguely threatened did not cause her anxiety: she scarcely realized it. Hilda Lessways |Arnold Bennett