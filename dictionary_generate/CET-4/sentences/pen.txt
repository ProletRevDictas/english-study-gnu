Pilot’s fountain pens are some of the smoothest writing, most dependable brands—and for a good reason. No-fuss fountain pens that make your note taking and letter writing more elegant |PopSci Commerce Team |September 15, 2020 |Popular-Science 

The best pens stay fluid after many uses, dry fast, and write smoothly. Add some verve to your life with these colorful pens |PopSci Commerce Team |September 11, 2020 |Popular-Science 

In March 2007,Oralgaisha Omarshanova, who uses the pen name Oralgaisha Zhabagtaikyzy, wrote about ethnic clashes in rival regions of Almaty, Kazakhstan’s financial capital. These 10 journalists are missing, and COVID is impeding investigations |lbelanger225 |September 1, 2020 |Fortune 

To create the pen, you had several vertical posts, around which you were wrapping a sheet of fabric. Can You Cover The Globe? |Zach Wissner-Gross |August 28, 2020 |FiveThirtyEight 

Pop the S pen when the phone is already unlocked and you get a choice of eight apps including simple ones like notes and more complex ones like augmented-reality doodling on live videos. Samsung Note20 Ultra review: Why this big phone works for the COVID era |Aaron Pressman |August 18, 2020 |Fortune 

Storey said Wright often returned applications “dripping in red pen.” Two Texas Regulators Tried to Enforce the Rules. They Were Fired. |David Hasemyer, InsideClimate News |December 9, 2014 |DAILY BEAST 

One industry executive complained that Wright returned unapproved applications “dripping in red pen.” Two Texas Regulators Tried to Enforce the Rules. They Were Fired. |David Hasemyer, InsideClimate News |December 9, 2014 |DAILY BEAST 

So, I had a pen and a pencil and started scribbling and drawing, and I felt good about it. The Renegade: Robert Downey Sr. on His Classic Films, Son’s Battle with Drugs, and Bill Cosby |Marlow Stern |November 26, 2014 |DAILY BEAST 

Artists would use pen names and scrawl them across subway cars, walls, shops, and office buildings. Catch Him If You Can: Reliving Banksy’s New York Invasion |Alex Suskind |November 14, 2014 |DAILY BEAST 

When you are firing out at night, the red tracers go out into the blackness as if you were drawing with a light pen. War Is Hell and Such Good Fun |Don Gomez |November 11, 2014 |DAILY BEAST 

Squinty, several times, looked at the hole under the pen, by which he had once gotten out. Squinty the Comical Pig |Richard Barnum 

Several times after this the boy and his sisters came to look down into the pig pen. Squinty the Comical Pig |Richard Barnum 

She also practises etching, pen-and-ink drawing, as well as crayon and water-color sketching. Women in the fine arts, from the Seventh Century B.C. to the Twentieth Century A.D. |Clara Erskine Clement 

And right after that, some nice sour milk would come splashing down into the trough of the pen. Squinty the Comical Pig |Richard Barnum 

Then the boy lifted out the comical little pig, and Squinty found himself inside a large box, very much like the pen at home. Squinty the Comical Pig |Richard Barnum