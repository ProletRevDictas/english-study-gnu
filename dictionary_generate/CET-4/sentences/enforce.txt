Those traits make the Aerospace goggles particularly useful now while my local mountain, Mount Ashland in Oregon, is strictly enforcing a mask rule in lift lines. These Goggles Let You Ski Fog-Free |Joe Jackson |February 12, 2021 |Outside Online 

The NBA has a rule that players must stand for the anthem, but it has declined to enforce it in recent years. The Dallas Mavericks will resume playing the national anthem before games |Timothy Bella |February 10, 2021 |Washington Post 

As one lawyer essentially put it, there’s what the law says is required, and then what the SEC ever bothers to enforce. PSA: Most aggregate VC trend data is garbage |Danny Crichton |February 9, 2021 |TechCrunch 

The task of distributing vaccines was in many ways less complicated for the District than for any state — the city has only one health department and could make and enforce centralized decisions. Confusion and chaos: Inside the vaccine rollout in D.C., Maryland and Virginia |Julie Zauzmer, Gregory S. Schneider, Erin Cox |February 9, 2021 |Washington Post 

All these manual actions do are enforce the existing policies and guidelines Google has already published within its help documents. Google publishes new manual actions aimed at Google News and Discover penalties |Barry Schwartz |February 8, 2021 |Search Engine Land 

Obviously, the first obligation of all liberal democratic governments is to enforce the rule of law. Ayaan Hirsi Ali: Our Duty Is to Keep Charlie Hebdo Alive |Ayaan Hirsi Ali |January 8, 2015 |DAILY BEAST 

At any rate, policy can enforce equal rights and foster equal opportunity. No Gods, No Cops, No Masters |James Poulos |January 1, 2015 |DAILY BEAST 

“Given the number of events which take place across the UK it is likely to be difficult to enforce,” said Rupert Sutton. To Stop ISIS, Britain Is Set to Stop Free Speech |Nico Hines |November 25, 2014 |DAILY BEAST 

People often forget that the National Panhellenic council used to enforce racial segregation by means of strict codes and laws. Stepford Sororities: The Pressures of USC’s Greek Life |Maya Richard Craven |November 17, 2014 |DAILY BEAST 

So the CIW has looked to the marketplace to enforce these guarantees. We're All Living on The Supermarket Plantation |Lewis Beale |November 8, 2014 |DAILY BEAST 

Consequently the Chinese (or Ming) emperor sent a large army to enforce his demand for the amount of money due him. Our Little Korean Cousin |H. Lee M. Pike 

Or at least he would merely have expressed his wish that I should take the name, without going so far as to enforce it. First Plays |A. A. Milne 

The old dog stuck to her like a burr, and she had not the heart to take up a stick to enforce obedience. The World Before Them |Susanna Moodie 

His duties were to enforce the continental system and to keep a stern eye on Prussia. Napoleon's Marshals |R. P. Dunn-Pattison 

If he can get anything, he cannot claim it under his contract for he has broken it and therefore a court could not enforce it. Putnam's Handy Law Book for the Layman |Albert Sidney Bolles