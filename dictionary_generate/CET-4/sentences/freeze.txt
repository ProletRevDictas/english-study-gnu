Most or all spots except right near the water should see a freeze. PM Update: Clear and cold tonight, then it’s a bit warmer Thursday |Ian Livingston |December 2, 2020 |Washington Post 

The system was among the first in the country to announce service cuts and a hiring freeze in hopes of conserving cash. Transit system service cuts proposed in Congress’s backyard elicit calls for more funding |Justin George, Lori Aratani, Meagan Flynn |December 2, 2020 |Washington Post 

Patchy frost and freeze spots are possible — bring in those outdoor plants if you’ve potted them! D.C.-area forecast: Raindrops end this morning, then it gradually clears with breezy conditions |A. Camden Walker |October 30, 2020 |Washington Post 

The visa freeze is due to expire at the end of the year, and people such as Bowman have vowed to keep their doors open once a new season starts, probably next year. Can Small-Town America Survive Pandemic’s Hit to Minor League Baseball? |Charu Kasturi |September 14, 2020 |Ozy 

BBC Global News, the commercial, international arm of the BBC, has, like other publishers, weathered sudden double-digit percentage drops in digital ad revenue over the last six months from coronavirus-induced spending freezes. ‘We’re getting more used to the uncertainty’: BBC Global News chief on ad-funded news |Lucinda Southern |September 10, 2020 |Digiday 

The quandary of whether to freeze eggs or not could become irrelevant overnight. Men Will Someday Have Kids Without Women |Samantha Allen |January 3, 2015 |DAILY BEAST 

But with the outbreak of hostilities in mid-2011, all festivities were thrust into the deep freeze. In One Corner of Syria, Christmas Spirit Somehow Manages to Survive |Peter Schwartzstein |December 25, 2014 |DAILY BEAST 

To get the product from manufacturer to arm, the product is lyophilized (a fancy word for freeze dried). Powdered Measles Vaccine Could Be Huge for Developing World |Kent Sepkowitz |December 2, 2014 |DAILY BEAST 

MAKE IT AHEAD: Assemble the pot pies completely, cover tightly, and refrigerate for up to a day or freeze for up to a month. Make These Barefoot Contessa Chicken Pot Pies |Ina Garten |November 29, 2014 |DAILY BEAST 

Some time passes, another performer tests positive, another temporary production freeze. Risky Business or None of Your Business? Gay XXX Films and the Condom Question |Aurora Snow |November 1, 2014 |DAILY BEAST 

I have paid to have a fire kept up in the furnace for a week so that the pipes would not freeze. The Girls of Central High on the Stage |Gertrude W. Morrison 

Cold words freeze people, and hot words scorch them, and bitter words make them bitter, and wrathful words make them wrathful. The Ladies' Book of Etiquette, and Manual of Politeness |Florence Hartley 

When the soil pipe from a water-closet is exposed in cold weather it may freeze up or be clogged by urinary deposits. Essays In Pastoral Medicine |Austin Malley 

The newcomer would import an element of caste and class which would freeze mother and daughter to the bones. You Never Know Your Luck, Complete |Gilbert Parker 

Even while she gazed there crept over her a sensation of deadly fear and dread, that seemed to freeze the very blood in her veins. They Looked and Loved |Mrs. Alex McVeigh Miller