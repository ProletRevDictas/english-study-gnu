Instead, they can turn to a variety of websites that let people loan out their cryptocurrency, often for high rates of interest. Crypto soars again as traders embrace ‘DeFi’ and ‘yield farming’—but some see echoes of the 2017 bubble |Jeff |August 25, 2020 |Fortune 

The company is also letting consumers apply the 50% bonus they receive when spending rewards points for travel to a variety of other items under a “Pay Yourself Back” program. Chase’s Sapphire card created a millennial ‘cult.’ Can it last through COVID? |Jeff |August 24, 2020 |Fortune 

This is a system that assigns trustworthiness scores to individuals, companies, organizations, and governments on the basis of a variety of behaviors. The Chinese and U.S. Internets are drifting apart. Why that’s bad for the whole world |jakemeth |August 21, 2020 |Fortune 

This year’s Digiday Media Awards Europe honor a wide variety of publishers, media brands, advertisers and technology companies. Sky News, Hearst UK and RT are Digiday Media Awards Europe winners |Digiday Awards |August 21, 2020 |Digiday 

Canoo says this modular approach will allow the company to serve a variety of market segments at reduced cost. Electric-vehicle startup Canoo to go public, joining the wave of companies chasing Tesla’s success |dzanemorris |August 18, 2020 |Fortune 

An escort who goes by the name of “Tommy” has experienced a wide variety of female clients. Career-Minded Women Turn to Male Escorts For No-Strings Fun and (Maybe) Sex |Aurora Snow |January 3, 2015 |DAILY BEAST 

A variety of systems were in place across countries like the United Kingdom, France, and Australia. Propaganda, Protest, and Poisonous Vipers: The Cinema War in Korea |Rich Goldstein |December 30, 2014 |DAILY BEAST 

But this approach can be troublesome for a variety of reasons. The Craziest Date Night for Single Jews, Where Mistletoe Is Ditched for Shots |Emily Shire |December 26, 2014 |DAILY BEAST 

Klaus espouses inflammatory views on a variety of subjects, some of which Cato happily embraced. Vaclav Klaus, Libertarian Hero, Has His Wings Clipped by Cato Institute |James Kirchick |December 22, 2014 |DAILY BEAST 

In this day and age, this “help” comes in a variety of forms, from creative writing courses to ghost writers. Meet Zoella—The Newbie Author Whose Book Sales Topped J.K. Rowling |Lucy Scholes |December 11, 2014 |DAILY BEAST 

Selections for practice should be chosen which contain much variety of thought and feeling and are smooth in movement. Expressive Voice Culture |Jessie Eldridge Southwick 

Adequate conception of the extent, the variety, the excellence of the works of Art here heaped together is impossible. Glances at Europe |Horace Greeley 

She apparently prefers to paint single figures of women and young girls, but her works include a variety of subjects. Women in the fine arts, from the Seventh Century B.C. to the Twentieth Century A.D. |Clara Erskine Clement 

"Immer eine bunte Reihe machen (Always have a little variety)," said he. Music-Study in Germany |Amy Fay 

He distinguished himself in early youth by the variety of studies which he accomplished. The Every Day Book of History and Chronology |Joel Munsell