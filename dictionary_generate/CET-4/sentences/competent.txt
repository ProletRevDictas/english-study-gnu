Beyond diagnosing what went wrong, I asked Wright what the federal government could have done differently and if he thinks a competent administration with a serious plan might’ve saved hundreds of thousands of lives. “We did the worst job in the world”: Lawrence Wright on America’s botched Covid-19 response |Sean Illing |February 9, 2021 |Vox 

Yes, I think what he is gong for is a sort of I’m-more-competent-and-would-have-gotten-all-this-done angle, as opposed to the who-cares-about-the-pandemic-just-open approach. Politics Report: School of Faulconer |Scott Lewis |February 6, 2021 |Voice of San Diego 

My hope is that we’ll get another big emergency package and, finally, a competent vaccine rollout, and that we’ll be mostly back to normal by the summer. What happens after the GameStop bubble bursts? |Peter Kafka |February 2, 2021 |Vox 

They were fully formed humans before motherhood and competent as mothers, and those with grown children continue to be independent folks accomplishing much. It’s time to end the TV trope of the needy empty-nest mom |Michele Weldon |February 2, 2021 |Washington Post 

A competent travel adviser would never send you into harm’s way. How to find a travel adviser amid the pandemic |Christopher Elliott |January 27, 2021 |Washington Post 

What they found was that most people preferred to work with the lovable fool rather than the competent jerk. The Case Against In-Your-Face Atheism |Steve Neumann |January 4, 2015 |DAILY BEAST 

After all, unlike the other vaccines currently in the pipeline, this one is alive and replication-competent. Uh Oh: Ebola Vaccine Trials Stop |Leigh Cowart |December 19, 2014 |DAILY BEAST 

As his later wartime record would show, Jackson was extremely competent in the many skills required of a commanding general. Stonewall Jackson, VMI’s Most Embattled Professor |S. C. Gwynne |November 29, 2014 |DAILY BEAST 

But he had a personal fortune—he spent $1.5 million of his own money on the race—and a competent, if uninspired, message. How House Dems Lost Their Last Southern White Guy |James Richardson |November 9, 2014 |DAILY BEAST 

“Either of the MiG types must be honored, especially in the hands of a competent pilot,” the retired pilot said. U.S. Fighter Jocks Pray The ‘ISIS Air Force’ Rumors Are True |Dave Majumdar |October 21, 2014 |DAILY BEAST 

Unfortunately, I had studied so little at that time, that I don't feel as if I were competent to judge him. Music-Study in Germany |Amy Fay 

Sometimes the tracing down may have been done by some advanced pupil or competent assistant. Antonio Stradivari |Horace William Petherick 

Judging from the rapid progress he made in a short time, this teacher must have been thoroughly competent. The Life &amp; Letters of Peter Ilich Tchaikovsky |Modeste Tchaikovsky 

That persons competent to judge of their merit would in after years pronounce them of priceless value. The World Before Them |Susanna Moodie 

The things these fellows produce are all read and checked by competent General Staff Officers. Gallipoli Diary, Volume I |Ian Hamilton