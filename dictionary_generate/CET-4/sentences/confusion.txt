Part of the confusion has to do with the fact that the current phase in Europe — despite how bad it looks according to case numbers alone — has a different dynamic from the first phase. The new Covid-19 case surge in Europe, explained |Julia Belluz |September 17, 2020 |Vox 

Instead, there is a lot of uncertainty and confusion about what is next. Malala Yousafzai tells the business community: Education is the best way to guard against future crises |Michal Lev-Ram, writer |September 15, 2020 |Fortune 

Part of the confusion around sealing any deal is the number of interested parties that TikTok’s parent company must satisfy. TikTok pushing forward with deal to meet looming deadline |Verne Kopytoff |September 11, 2020 |Fortune 

A shortage of poll workers could mean “long lines, mass confusion and miscounted ballots.” Facebook launches poll worker recruitment push in the News Feed |Taylor Hatmaker |September 11, 2020 |TechCrunch 

However, since it might create confusion to have one jersey playing for both sides, they agree that the residents of two towns will combine forces to play against the third town’s residents. Can You Reach The Summit First? |Zach Wissner-Gross |September 11, 2020 |FiveThirtyEight 

Despite its ranking at the bottom of most international development indexes, the conflict is shrouded by confusion. The Year’s Most Forgotten Humanitarian Crisis |Nina Strochlic |January 1, 2015 |DAILY BEAST 

Bohac vowed to that when he came back next year there would be no confusion about any Christmas tree or Santa aprons. A Field General in the War on Christmas |David Freedlander |December 24, 2014 |DAILY BEAST 

There was confusion because our fathers were no longer going to work. The Congo's Forgotten Colonial Getaway |Nina Strochlic |December 18, 2014 |DAILY BEAST 

Putin may very well be the last optimist left in the country, which is facing a time of confusion and disappointment. After His Disastrous Annual Press Conference, Putin Needs A Hug |Anna Nemtsova |December 18, 2014 |DAILY BEAST 

Her own muddled feelings of confusion, shame, and fear are what make the essay great and what make the essay her story. The Right's Rape Trolls vs. Lena Dunham |Emily Shire |December 10, 2014 |DAILY BEAST 

In the darkness and confusion I did not distinguish the face of the man who rendered me this assistance. Blackwood's Edinburgh Magazine, No. CCCXXXIX. January, 1844. Vol. LV. |Various 

Quickly seeing his confusion, they laughed long and heartily among themselves. The Homesteader |Oscar Micheaux 

Scratches and flaws in the glass of slide or cover are likewise a common source of confusion to beginners. A Manual of Clinical Diagnosis |James Campbell Todd 

Then our people were sure they were captured, and there was nothing but cries and confusion. The Jesuit Relations and Allied Documents, Vol. II: Acadia, 1612-1614 |Various 

And the strength of Pharao shall be to your confusion, and the confidence of the shadow of Egypt to your shame. The Bible, Douay-Rheims Version |Various