The city, the state, the whole land, were ready to rise and tremble before the Pallid Mask. Read ‘The King in Yellow,’ the ‘True Detective’ Reference That’s the Key to the Show |Robert W. Chambers |February 20, 2014 |DAILY BEAST 

He began to read, raising his eyebrows with a puzzled, whimsical air, which made me tremble with suppressed anger. Read ‘The King in Yellow,’ the ‘True Detective’ Reference That’s the Key to the Show |Robert W. Chambers |February 20, 2014 |DAILY BEAST 

But it was also the kind of determination that made tyrants tremble, even in the closing days of her time as prime minister. British Prime Minister Margaret Thatcher’s Lasting Legacy |Christopher Dickey |April 8, 2013 |DAILY BEAST 

Who is this woman going toe-to-toe with Wintour, when all others appear to tremble, and who excels because of it? Vogue Creative Director Grace Coddington’s Memoir Offers Few Revelations |Robin Givhan |November 20, 2012 |DAILY BEAST 

I just tremble when I think about how tremendous this moment is. Before the Rapture |Bryan Curtis |May 19, 2011 |DAILY BEAST 

She looked both as she permitted her full red mouth to tremble and his arms to take sudden possession of her. Ancestors |Gertrude Atherton 

Softly tremble in the delicate blue mist and the azure spirals from his old Virginia clay—the domes of a sea-bathed city. Tobacco; Its History, Varieties, Culture, Manufacture and Commerce |E. R. Billings. 

From time to time I felt my companion's arm tremble convulsively, as if he shivered from head to feet. Camille (La Dame aux Camilias) |Alexandre Dumas, fils 

At seven o'clock, a horrible din makes you start up in bed and tremble from head to foot. Friend Mac Donald |Max O'Rell 

Aunt Harriet began to tremble, and Sara Lee went over and put her young arms about her. The Amazing Interlude |Mary Roberts Rinehart