Kelsey Lindsey, associate editorIn 2016, I was shamed by a buddy for wearing a running vest during a race with aid stations. The Gear Our Editors Loved in September |The Editors |October 4, 2020 |Outside Online 

Yes, $199 is a lot for a vest, but the Approach is worth it because it’s the only one you’ll need this winter. 7 of Our Favorite Men's and Women's Fall Layers |Jakob Schiller |October 3, 2020 |Outside Online 

If you’re an employee at a chain, it doesn’t matter what logo is on your vest—your job is not assured. What restaurants need from the 2020 election |ehinchliffe |September 30, 2020 |Fortune 

Simply slip on the vest as you go about your usual activities—whether that’s a casual walk in the park or an intense cardio session—and you’ll torch more calories, while building up endurance. The best weighted vests for your next tough workout |PopSci Commerce Team |September 16, 2020 |Popular-Science 

We rounded up some of the best weighted vests on the market. The best weighted vests for your next tough workout |PopSci Commerce Team |September 16, 2020 |Popular-Science 

He wore white gloves, a dignified long black coat, and matching pants and vest, and he carried a dark walking stick. The Black Man Who Replaced Jefferson Davis in the Senate |Philip Dray |January 7, 2015 |DAILY BEAST 

Other officials told reporters that searchers also spotted a life vest and baggage in the water. Wreckage, Bodies of AirAsia Crash Found |Lennox Samuels |December 30, 2014 |DAILY BEAST 

One is reported to have blown himself up, along with many victims, but detonating a suicide vest. Taliban: We Slaughtered 100+ Kids Because Their Parents Helped America |Sami Yousafzai |December 16, 2014 |DAILY BEAST 

In the most recent image posted by her mother, Kristina is wearing a black vest over her skinny shoulders. Is 9-Year-Old Russian Model Kristina Pimenova Too Sexualized? |Anna Nemtsova |December 12, 2014 |DAILY BEAST 

The news reports quoted him as saying he wore the vest to celebrate Sunday Mass in Apatzingán. Mexico’s Holy Warrior Against the Cartels |Jason McGahan |November 18, 2014 |DAILY BEAST 

In a short time the pockets were all as full as they could hold—coat, vest, and trousers. The Floating Light of the Goodwin Sands |R.M. Ballantyne 

Ward sat back in his swivel chair, hooked his thumbs into the arm holes of his vest and beamed. Hooded Detective, Volume III No. 2, January, 1942 |Various 

He took one up, eased a stray safety match from his vest pocket, flicked it with his fingernail, and lit up. Hooded Detective, Volume III No. 2, January, 1942 |Various 

Ghurri-Wurri wore tattered white baggy trousers, vest and cloak, a turban and black goggles. Fifty Contemporary One-Act Plays |Various 

It is of carmine silk damask with gold thread, and the inner vest is of white lawn. Fifty Contemporary One-Act Plays |Various