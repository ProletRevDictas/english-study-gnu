This comes just a couple of months after Amazon started selling auto insurance in India. Amazon’s fintech push is a play to dig further into India |Ananya Bhattacharya |September 17, 2020 |Quartz 

Google said on Thursday that it recently implemented a new policy to stop auto-complete search queries from popping up if they seem to support a candidate or contain misinformation about voting or the election. Tech's election-season survival plan: transparency |Ashley Gold |September 11, 2020 |Axios 

Google rather not show a suggestion in auto-complete than show an inaccurate suggestion. Google now uses BERT to match stories with fact checks |Barry Schwartz |September 10, 2020 |Search Engine Land 

Another auto entry — Science News, September 12, 1970The recent week-long clean air car race from Massachusetts to California provided a shotgun approach to development of low-emission or nonpolluting vehicle engines. 50 years ago, scientists were trying to develop a low-emission car |Erin Garcia de Jesus |September 1, 2020 |Science News 

For a retailer or auto manufacturer, the trigger would be if a certain percentage of stores or dealerships are closed. ‘Kind of squishy’: Advertisers lobby to add pandemic clauses to TV upfront deals |Tim Peterson |September 1, 2020 |Digiday 

Even his signature instrument, Auto-Tune, has become as accepted an ingredient in hip-hop as the drum machine. Future Makes Us Rethink Everything We Thought We Knew About Rap Artists |Luke Hopping |December 15, 2014 |DAILY BEAST 

If your ears are tired of slick auto-tuned vocals, pick up this disk for an aural detox. The Best Albums of 2014 |Ted Gioia |December 13, 2014 |DAILY BEAST 

I know that Detroit is losing market share in auto sales, but how did they let the Motown sound slip out of their hands? The Best Albums of 2014 |Ted Gioia |December 13, 2014 |DAILY BEAST 

A small fire in an auto parts store created explosions that quickly got out of control. Raging Protesters Set Ferguson on Fire |Justin Glawe |November 25, 2014 |DAILY BEAST 

In third person, Grand Theft Auto V was like a really elaborate action figure play set. I Felt Like Showering After the First-Person Sex in ‘Grand Theft Auto’ |Alec Kubas-Meyer |November 22, 2014 |DAILY BEAST 

At this hour of the afternoon, before the return rush of the auto-commuters from the city, the road was almost empty. The Campfire Girls of Roselawn |Margaret Penrose 

He had her arm—held it close, as they passed through the station and crossed the walkway to where an inclosed auto stood. The Homesteader |Oscar Micheaux 

There were warm robes and blankets in the Belding auto and in the sightseeing machine that Mr. Purcell had sent. The Girls of Central High on the Stage |Gertrude W. Morrison 

We had two auto-cars for our party of eight, in case the added four joined the two couples who had passage secured on the Ryndam. Ways of War and Peace |Delia Austrian 

There were two large auto-stages in waiting, and Ruth and Helen followed the crowd of girls briskly getting aboard the buses. Ruth Fielding At College |Alice B. Emerson