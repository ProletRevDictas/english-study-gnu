Some moderates hope Pelosi will come around and make a deal -- particular now that the Problem Solvers Caucus has laid out a bipartisan bluepring. Trump moves closer to Pelosi in economic aid talks, and House speaker must decide next move |Rachael Bade, Erica Werner |September 17, 2020 |Washington Post 

In the weeks before a trial in the case, lawyers for Bluestone filed documents detailing a draft deal worked out separately with the state’s Department of Environmental Protection. This Billionaire Governor’s Coal Company Might Get a Big Break From His Own Regulators |by Ken Ward Jr. |September 17, 2020 |ProPublica 

They called the state action “a self-dealing administrative order” and said the proposed penalties “are insufficient to deter future violations, leaving a realistic prospect of continued noncompliance.” This Billionaire Governor’s Coal Company Might Get a Big Break From His Own Regulators |by Ken Ward Jr. |September 17, 2020 |ProPublica 

Asked whether advertisers are required to commit to spend a minimum amount of money to advertise on NBCU’s properties in order to access the program, the NBCU spokesperson said each deal is different and negotiated on an individual basis. NBCUniversal tests new measurement program to prove it can push product sales for advertisers |Tim Peterson |September 17, 2020 |Digiday 

So when the governor calls in the National Guard, perhaps on behalf of a mayor to respond to a particular situation, you’re there to fulfill the vision of that municipality dealing with whatever emergency effort it is. Mobilizing the National Guard Doesn’t Mean Your State Is Under Martial Law. Usually. |by Logan Jaffe |September 17, 2020 |ProPublica 

“Personally, I deal with manners of righteousness and God,” he says. New York’s Most Tragic Ghost Loves Minimalist Swedish Fashion |Nina Strochlic |January 8, 2015 |DAILY BEAST 

Speculation raged that Duke agreed not to run as part of the deal, though it was never proven. The Price of Steve Scalise’s Silence |Jason Berry |January 7, 2015 |DAILY BEAST 

He later accepted a plea deal that put him behind bars for 25 years. An Informant, a Missing American, and Juarez’s House of Death: Inside the 12-Year Cold Case of David Castro |Bill Conroy |January 6, 2015 |DAILY BEAST 

It's not necessarily a deal-breaker, but it is kind of a top priority. My Week on Jewish Tinder |Emily Shire |January 5, 2015 |DAILY BEAST 

In the wee hours of Christmas morning, a flight deal was shared in an exclusive Facebook group for urban travelers. ‘We Out Here’: Inside the New Black Travel Movement |Charlise Ferguson |January 4, 2015 |DAILY BEAST 

They are very urgent questions; our sons and daughters will have to begin to deal with them from the moment they leave college. The Salvaging Of Civilisation |H. G. (Herbert George) Wells 

The patriarchal decree of the government was a good deal of a joke on the plains, anyway—except when you were caught defying it! Raw Gold |Bertrand W. Sinclair 

She and her younger sister, Janet, had quarreled a good deal through force of unfortunate habit. The Awakening and Selected Short Stories |Kate Chopin 

In practice we find a good deal of technical study comes into the college stage. The Salvaging Of Civilisation |H. G. (Herbert George) Wells 

Bernard sat thinking for a long time; at first with a good deal of mortification—at last with a good deal of bitterness. Confidence |Henry James