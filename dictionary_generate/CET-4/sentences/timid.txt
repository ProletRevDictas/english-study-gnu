We exchanged timid greetings while the rest of them settled down. Locked up in the Land of Liberty: Part I |Yariel Valdés González |July 7, 2021 |Washington Blade 

Colonial hasn’t fully changed that, but I think we’re moving away from a timid response. Could the ransomware crisis force action against Russia? |Patrick Howell O'Neill |May 21, 2021 |MIT Technology Review 

“At first, I was a little bit timid because I wasn’t sure how they would react,” JJ said. After the shooting, a boy gave flowers to workers at King Soopers stores near the attack |Sydney Page |March 25, 2021 |Washington Post 

We’re not going to make the same mistake we made after the last economic downturn, when Congress did too little to help the nation rebound … we’re not going to be timid in the face of big challenges. Senate revamps unemployment benefits and rejects minimum-wage increase as chamber debates $1.9 trillion stimulus |Erica Werner, Jeff Stein, Tony Romm |March 5, 2021 |Washington Post 

One night after her program, the typically timid Evie perked up in her chair at the dinner table, excited to share some news. Why the Equality Act Matters For Our Family—And For Many Others Across America |Marie Newman and Evie Newman |March 3, 2021 |Time 

Could the (thus far) timid trembling give way to a full-on, grand mal seizure? 26 Earthquakes Later, Fracking’s Smoking Gun Is in Texas |James Joiner |January 7, 2015 |DAILY BEAST 

He largely agreed with Lieberman but considered Netanyahu too timid towards the Palestinians. Goodbye to Israel’s Lousy Government (Let’s Hope the Next One Isn’t Worse) |Alon Ben-Meir |December 4, 2014 |DAILY BEAST 

But when I arrive at the entrance, the timid gatekeeper tells me—without explanation—that I can no longer speak with him. Pablo Escobar’s Private Prison Is Now Run by Monks for Senior Citizens |Jeff Campagna |June 7, 2014 |DAILY BEAST 

Even the most timid and shaky of the puppies looks determined and confident in slo-mo. ‘The Puppy Bowl’: The Super Bowl’s Fiercest Rival |Lori-Lee Emshey |February 2, 2014 |DAILY BEAST 

SO THAT HIS PLACE SHALL NEVER BE WITH THOSE COLD AND TIMID SOULS WHO NEITHER KNOW VICTORY NOR DEFEAT. Miley Cyrus's Smartest Tattoo |James Poulos |September 28, 2013 |DAILY BEAST 

If it were not for the cowardly fear of being thought timid, there would be more care used in such matters. Glances at Europe |Horace Greeley 

The public eye, ever watchful and timid, waits scarcely for the show of danger to take alarm and withdraw its favour. Blackwood's Edinburgh Magazine, No. CCCXXXIX. January, 1844. Vol. LV. |Various 

One retrograde or timid step would open the eyes of men, and bring down ruin on the Pantamorphica. Blackwood's Edinburgh Magazine, No. CCCXXXIX. January, 1844. Vol. LV. |Various 

Impudent and reckless us he had been all his life, he was now more timid and nervous than an hysterical girl. Blackwood's Edinburgh Magazine, No. CCCXXXIX. January, 1844. Vol. LV. |Various 

She smiled back at him, a pale, timid smile, like a gleam of sunshine from a wintry sky. St. Martin's Summer |Rafael Sabatini