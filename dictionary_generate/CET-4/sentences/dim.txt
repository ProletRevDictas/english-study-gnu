Soon we had the entire garden to ourselves — we encountered fewer than a dozen people the entire weekend — and as the light dimmed behind an overcast sky, the three of us perched on a ledge near the trail’s end. A day’s drive from Chicago, exploring a very different Illinois |Carson Vaughan |February 12, 2021 |Washington Post 

It’s during this dim period, when the stars still mingle with the moon in the sky but the sun announces that it’s on its way, that I’ve maintained a tenuous hold on the outside world through surfing. The Spark After the Darkness |Bonnie Tsui |February 6, 2021 |Outside Online 

This is Delaware, home of presidents and seekers of information about Cantonese dim sum to be served during the Super Bowl. The 2021 Super Bowl food map is a deep dive into America’s weird culinary underbelly |Matt Bonesteel |February 2, 2021 |Washington Post 

On a per-36-minutes and per-100-possessions basis, Randle is diming up his teammates more than he ever previously had. The Only Thing Keeping The Knicks Offense Afloat Is Julius Randle’s Turn As A Point Guard |Jared Dubin |January 28, 2021 |FiveThirtyEight 

When researchers look for far-off planets, they do so by watching stars and noting when planets pass in front of them, measuring the resulting dim in brightness. These 6 exoplanets somehow orbit their star in perfect rhythm |Charlie Wood |January 27, 2021 |Popular-Science 

Justin gazed out from the dim interior as more than 300 police motorcycles from dozens of jurisdictions rumbled past. Choking Back Tears, Thousands of Cops Honor Fallen Officer Ramos |Michael Daly |December 28, 2014 |DAILY BEAST 

The essential fault lies not with the stars around him, however dim, but with himself. Before Ditching His Top Aides, Obama Should Look in the Mirror |Leslie H. Gelb |November 2, 2014 |DAILY BEAST 

Below, the thick marshland and dim lights created a scene that Breman describes as “Joseph Conrad territory.” The Original Ebola Hunter |Abby Haglage |September 14, 2014 |DAILY BEAST 

In a dim backroom of a mud hut in Save, 82-year-old Teresa Nyirabutunda sits propped upright in bed by her daughter, Francine. After the Genocide, Rwanda’s Widows Aging Alone |Nina Strochlic |August 31, 2014 |DAILY BEAST 

My concerns about the study do not dim my admiration for the fund itself or the work that it does. How to Tell When a Scientific Study Is Total B.S. |Russell Saunders |August 22, 2014 |DAILY BEAST 

She opened the letter by the flickering firelight, which was stronger on the hearthrug than the light of the dim November day. Hilda Lessways |Arnold Bennett 

Dr. Ashton walked out of the chapel, and Val stood for a few moments where he was, looking up and down in the dim light. Elster's Folly |Mrs. Henry Wood 

Any moment, if he looked up, he would meet eyes—eyes that gazed with dim yet definite recognition into his own across the night. The Wave |Algernon Blackwood 

The contrast between the open street and the enclosed stuffiness of the dim and crowded interior was overwhelming. Hilda Lessways |Arnold Bennett 

By the dim light of the campfire they saw what they supposed were the sleeping forms of their enemies. The Courier of the Ozarks |Byron A. Dunn