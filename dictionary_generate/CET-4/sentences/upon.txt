Completed in 1953 and composed with standard line breaks and punctuation, the book was completely ignored upon submission. The Lost Novel of Nobel-Winner José Saramago |Charles Shafaieh |January 5, 2015 |DAILY BEAST 

Once upon a time, a girl named Onika Maraj dreamed of being an actress. Nicki Minaj: High School Actress |Alex Chancey, The Daily Beast Video |December 30, 2014 |DAILY BEAST 

Excited, Shaheen wasted no time and began interviewing surgeons, deciding upon Dr. Curtis Crane in Greenbrae, California. The Insurance Company Promised a Gender Reassignment. Then They Made a Mistake. |James Joiner |December 29, 2014 |DAILY BEAST 

The poet apparently collapsed in the street upon his departure from “The Horse” and died not long after. The Bars That Made America Great |Nina Strochlic |December 28, 2014 |DAILY BEAST 

“I happened upon yak butter tea, a traditional high-energy food eaten by Tibetans,” Asprey says. Bulletproof Coffee and the Case for Butter as a Health Food |DailyBurn |December 27, 2014 |DAILY BEAST 

It is thinner than that of chronic bronchitis, and upon standing separates into three layers of pus, mucus, and frothy serum. A Manual of Clinical Diagnosis |James Campbell Todd 

Other things being equal, the volume of voice used measures the value that the mind puts upon the thought. Expressive Voice Culture |Jessie Eldridge Southwick 

He was too drowsy to hold the thought more than a moment in his mind, much less to reflect upon it. The Wave |Algernon Blackwood 

In the year of misery, of agony and suffering in general he had endured, he had settled upon one theory. The Homesteader |Oscar Micheaux 

He turned his eyes upon her; but no sympathy was in their beams; no belief in the semblance of her tears. The Pastor's Fire-side Vol. 3 of 4 |Jane Porter