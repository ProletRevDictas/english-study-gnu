From the outset, Perry’s focus on Ukraine had puzzled his colleagues in government, who say that he took a personal interest in the country’s affairs. Rick Perry’s Ukrainian Dream |by Simon Shuster, TIME, and Ilya Marritz, WNYC |September 10, 2020 |ProPublica 

Despite much drum-beating on the topic, much of the public is still unaware that seasonal flu is a serious affair. The COVID-19 pandemic is about to collide with flu season. Here’s what to expect. |Sara Chodosh |August 26, 2020 |Popular-Science 

In her new role, Delaine Prado will report to Google’s SVP of global affairs, Kent Walker, an influential lawyer who has played a key role in guiding the company’s political and legal response to the numerous challenges it is facing. Google names Halimah DeLaine Prado as new general counsel |Jeff |August 25, 2020 |Fortune 

There is overwhelming public support for making cocktails to go permanent, says Mike Whatley, vice president of state and local affairs for the National Restaurant Association. Number of states allowing to-go cocktails has surged from 2 to 33 during coronavirus |Lee Clifford |August 24, 2020 |Fortune 

Days earlier, he had admitted to being questioned in a federal investigation related to the 2016 presidential election, blaming the whole affair on “the deep state.” The year’s hottest e-commerce stock is up more than 1,500%. Its founder cashed out before the rally |Bernhard Warner |August 18, 2020 |Fortune 

But he elected instead to have a very visible affair with a music-hall star. The Real-Life ‘Downton’ Millionairesses Who Changed Britain |Tim Teeman |December 31, 2014 |DAILY BEAST 

It is what I (sometimes) find endearing about the whole affair. The Craziest Date Night for Single Jews, Where Mistletoe Is Ditched for Shots |Emily Shire |December 26, 2014 |DAILY BEAST 

Literature in the 14th century, Strohm points out, was an intimate, interactive affair. A Year In The Life of The Canterbury Tales’ Storied Beginnings |Wendy Smith |December 25, 2014 |DAILY BEAST 

For Kirke it was being paid to pretend to play the oboe that heightened her affair with classical music. ‘Mozart in the Jungle’: Inside Amazon’s Brave New World of Sex, Drugs, and Classical Music |Kevin Fallon |December 23, 2014 |DAILY BEAST 

Dinner was a baroque affair, on the beach, a warm breeze gently blowing. Canada ♥ Cuba Just Got Complicated |Shinan Govani |December 22, 2014 |DAILY BEAST 

And now, monsieur, if you will communicate to me the nature of your affair, you shall find me entirely at your service. St. Martin's Summer |Rafael Sabatini 

Their sin began on Holy Thursday, with so little secrecy and so bad an example, that the affair was beginning to leak out. The Philippine Islands, 1493-1898, Volume XX, 1621-1624 |Various 

He was a boy of eighteen, aching over his first love affair; and she was divinely mothering him. The Wave |Algernon Blackwood 

This information was balm to Louis, as it seemed to promise a peaceful termination to so threatening an affair. The Pastor's Fire-side Vol. 3 of 4 |Jane Porter 

And it might be a good idea for you to give your men a gentle hint to keep their mouths closed about this affair—all of it. Raw Gold |Bertrand W. Sinclair