It's ok to be sad your favourite person didn't go through, but please remember it's not my fault. ‘I’m a Real Person With Feelings’: Fans Bully ‘Great British Bake Off’ Contestant for Making It to Finals |Jaya Saxena |November 20, 2020 |Eater 

Safety aside, there was something sad about looking at vehicles of adventure, when adventure feels so out of reach. You have until Nov. 22 to visit these six Smithsonian museums. Here’s what to expect. |Kelsey Ables |November 19, 2020 |Washington Post 

“The longer Senate Republicans are playing this sad game is the longer they are denying families much needed relief from the covid health and economic crisis,” Schumer said. Democrats allege GOP refusal to accept election results is imperiling U.S. coronavirus response as cases, deaths spike |Erica Werner |November 12, 2020 |Washington Post 

The new HD webcam gets access to Apple’s latest image signal processor, which should hopefully make it look better than its current sad state. Follow along with Apple’s last 2020 product announcement event |Stan Horaczek |November 10, 2020 |Popular-Science 

After an awkward conversation and a sad quantity of spilled wine, Darcy and Elle are both happy to go home alone and never meet again. ‘Written in the Stars’ is a ‘Pride and Prejudice’ reboot that’s charming, effervescent and entirely itself |Ellen Morton |November 10, 2020 |Washington Post 

Sadly, it appears the American press often doesn't need any outside help when it comes to censoring themselves. Politicians Only Love Journalists When They're Dead |Luke O’Neil |January 8, 2015 |DAILY BEAST 

Having finally seen Selma on November 17, I must report, sadly, that I do not share the enthusiasm the film has generated so far. Dr. King Goes to Hollywood: The Flawed History of ‘Selma’ |Gary May |January 2, 2015 |DAILY BEAST 

Sadly, the world will never see the realization of those skills. Dear Leelah, We Will Fight On For You: A Letter to a Dead Trans Teen |Parker Molloy |January 1, 2015 |DAILY BEAST 

Sadly, this choice between growth and climate change may not be necessary. Time to Bring Back the Truman Democrats |Joel Kotkin |December 21, 2014 |DAILY BEAST 

Sadly, laws throughout the Middle East—from North Africa to the Gulf—limit the rights of religious minorities and non-believers. What It’s Like to Be an Atheist in Palestine |Waleed al-Husseini, Movements.Org |December 8, 2014 |DAILY BEAST 

The Alcalde was kneeling by his side, gazing sadly and earnestly into the face of the dying man. Blackwood's Edinburgh Magazine, No. CCCXXXIX. January, 1844. Vol. LV. |Various 

The autumn winds had come, and with them the autumn rain, that washes sadly away the last sweet traces of summer. Bella Donna |Robert Hichens 

"They look ever on woe;" and he laid the baby back on Ramona's breast, and stood gazing sadly at her. Ramona |Helen Hunt Jackson 

So the twain walked on very lovingly together, Dorothy gazing sadly and fondly at every well-known object in her path. The World Before Them |Susanna Moodie 

"The angels don't whisper such blessed dreams to me," returned Dorothy, sadly. The World Before Them |Susanna Moodie