The fashionable lady, my neighbour, rose also, with graceful reserve. Read ‘The King in Yellow,’ the ‘True Detective’ Reference That’s the Key to the Show |Robert W. Chambers |February 20, 2014 |DAILY BEAST 

And she was a regular visitor and supporter of her neighbour in the Oxfordshire countryside, David Cameron. Rebekah Brooks Takes The Stand At Phone Hacking Trial |Peter Jukes, Nico Hines |February 20, 2014 |DAILY BEAST 

Her sister – and neighbour – Lolita Miraflorez, also lost her husband, as did dozens of other women. Typhoon Haiyan: The Philippine Village that Lost Its Men |The Telegraph |November 17, 2013 |DAILY BEAST 

I wasn't paying any attention to him, therefore, when suddenly my left-hand neighbour touched my arm. Music-Study in Germany |Amy Fay 

Turn not away thy face from thy neighbour, and of taking away a portion and not restoring. The Bible, Douay-Rheims Version |Various 

Cast the beam from thine eye before noticing the mote in that of thy neighbour. Solomon and Solomonic Literature |Moncure Daniel Conway 

Every one shall be amazed at his neighbour, their countenances shall be as faces burnt. The Bible, Douay-Rheims Version |Various 

They not only do their studying aloud, but they talk very loud, as if each one were trying to make more noise than his neighbour. Our Little Korean Cousin |H. Lee M. Pike