By April 2009, Obama was preparing to disclose those images. The Detainee Abuse Photos Obama Didn’t Want You To See |Noah Shachtman, Tim Mak |December 15, 2014 |DAILY BEAST 

All reportedly agreed on the need to disclose the matter the following day. How the Reagan White House Bungled Its Response to Iran-Contra Revelations |Malcolm Byrne |November 3, 2014 |DAILY BEAST 

Super PACs, unlike politically active nonprofits, must disclose their donors to the FEC in regular filings. Mystery Man Buys Kentucky for the GOP |Center for Public Integrity |October 29, 2014 |DAILY BEAST 

Some residents took to the secret-sharing app Whisper to disclose their biggest fears. ‘I’m Flipping Out’: Dallas Residents’ Worst Ebola Fears |The Daily Beast |October 15, 2014 |DAILY BEAST 

The groups, nonprofits exempt from paying taxes, are not required to disclose their donors in Kansas and most other states. Millions in Dark Money Has Taken Over the Airwaves in Kansas |Center for Public Integrity |October 9, 2014 |DAILY BEAST 

There are others who disclose a special susceptibility to the more simple effects of pathos. Children's Ways |James Sully 

A bill and note broker who does not disclose the principal's name is liable like other agents as a principal. Putnam's Handy Law Book for the Layman |Albert Sidney Bolles 

By the help of Heaven or earth, she would find out this secret that he refused to disclose to her. Elster's Folly |Mrs. Henry Wood 

On the 28th of March 1642 he was sent to the Tower for having failed to disclose to parliament the Kentish petition. Encyclopaedia Britannica, 11th Edition, Volume 4, Part 3 |Various 

Sometimes the breeze would fan aside a leaf cluster to disclose a jocund skull secured to the bark behind. Menotah |Ernest G. Henham