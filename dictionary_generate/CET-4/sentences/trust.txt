Hire the best people, people you trust, people whose judgment you trust. What if Your Company Had No Rules? (Bonus Episode) |Maria Konnikova |September 12, 2020 |Freakonomics 

Public schools have lost parent trust on this issue despite their continued lip service, and charter schools know it. Our Public Schools Have a Customer Service Problem |Thomas Courtney |September 10, 2020 |Voice of San Diego 

We asked leaders from the two companies about their high-trust, inclusive workplace cultures and how they’ve responded to the coronavirus crisis. How the Best Workplaces in Manufacturing have risen to the COVID-19 challenge |lbelanger225 |September 10, 2020 |Fortune 

Twenty-six times, I’ve been trusted to take the game-winning shot — and missed. A secret of science: Mistakes boost understanding |Rachel Kehoe |September 10, 2020 |Science News For Students 

Typically, each user in such a system needs to be directly wired to the other or connected via trusted nodes, which can make large networks costly and increase the number of windows for hackers to exploit. A New Breakthrough Just Brought City-Wide Quantum Communication Into Reach |Edd Gent |September 7, 2020 |Singularity Hub 

We proud skeptics would rather trust the demonstrable facts than the alleged truth. On Torture, Chuck Johnson & Sondheim |James Poulos |December 13, 2014 |DAILY BEAST 

And ultimately this creates steadily eroding trust among voters for not just politics but the institutions of government. Just What We Needed: More Campaign Spending |Mark McKinnon |December 8, 2014 |DAILY BEAST 

Others have taken the stage to tell women to just work harder and trust in karma. Tech’s Male ‘Feminists’ Aren’t Helping |Cate Huston, Karen Catlin |December 8, 2014 |DAILY BEAST 

If she wants voters to believe and trust in her, she must court favor with the local pastor, Jeremiah. The Good Wife’s Religion Politics: Voters Have No Faith in Alicia's Atheism |Regina Lizik |November 24, 2014 |DAILY BEAST 

In order for a reunion to happen, it would take a high level of trust, musically, on everything that happened. Wyclef Jean Talks Lauryn Hill, the Yele Haiti Controversy, and Chris Christie |Marlow Stern |November 20, 2014 |DAILY BEAST 

If you throw away this chance, you will both richly deserve to be hanged, as I sincerely trust you will be. The Posthumous Papers of the Pickwick Club, v. 2(of 2) |Charles Dickens 

There are three things a wise man will not trust: the wind, the sunshine of an April day, and woman's plighted faith. Pearls of Thought |Maturin M. Ballou 

He must trust to his human merits, and not miracles, for his Sonship is of no value in this conflict. Solomon and Solomonic Literature |Moncure Daniel Conway 

As if unwilling to trust himself longer in dangerous companionship, he went up to town with Thomas Carr. Elster's Folly |Mrs. Henry Wood 

I would not trust their removal to any other hand, and so, the panel comes out without a shake. Checkmate |Joseph Sheridan Le Fanu