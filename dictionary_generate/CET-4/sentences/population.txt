Now, Covid-19 demographics are changing again — shifting back into older populations. The new Covid-19 case surge in Europe, explained |Julia Belluz |September 17, 2020 |Vox 

According to the UN, about 40 percent of the world’s population lives within 60 miles of an ocean. Microsoft Had a Crazy Idea to Put Servers Under Water—and It Totally Worked |Vanessa Bates Ramirez |September 17, 2020 |Singularity Hub 

The federal government will release a dashboard to help states map these populations, the playbook said. Top health official says states need about $6 billion from Congress to distribute coronavirus vaccine |Lena H. Sun |September 16, 2020 |Washington Post 

The United States would still be responsible for 11 percent of global deaths, despite constituting only about 4 percent of the world’s population. Trump blames blue states for the coronavirus death toll — but most recent deaths have been in red states |Philip Bump |September 16, 2020 |Washington Post 

They are actually in a situation where they could afford to wait for the outcome of ongoing phase 3 trials, understand the safety and efficacy properly, without compromising their population. China has quietly vaccinated more than 100,000 people for Covid-19 before completing safety trials |Lili Pike |September 11, 2020 |Vox 

Even other men of color considered Revels a curious figure, for Mississippi had never had a large free black population. The Black Man Who Replaced Jefferson Davis in the Senate |Philip Dray |January 7, 2015 |DAILY BEAST 

As of 2013, Jews make up 1.8 to 2.2 percent of the adult U.S. population. Why Was Bess Myerson the First and Last Jewish Miss America? |Emily Shire |January 7, 2015 |DAILY BEAST 

Furthermore, mixed race children are the fastest growing population in the country. Obama Is Right on Race. The Media Is Wrong. |Keli Goff |December 29, 2014 |DAILY BEAST 

Veterans are a small minority of the population, as well, serving the greater whole. A Veteran’s View: NYC Cold War Between Cops and City Hall |Matt Gallagher |December 29, 2014 |DAILY BEAST 

For the first time in American history, rural America has been losing population. Will Texas Stay Texan? |David Fontana |December 29, 2014 |DAILY BEAST 

Nowhere can be found a region capable of supporting a larger population to the square mile than Lombardy. Glances at Europe |Horace Greeley 

This again is inexact, since there are no precise figures of population that cover the period. The Unsolved Riddle of Social Justice |Stephen Leacock 

Massed on the plateau above the mule-path, the whole population of the village stood to watch them down the steep descent. Rosemary in Search of a Father |C. N. Williamson 

In a population of angels a socialistic commonwealth would work to perfection. The Unsolved Riddle of Social Justice |Stephen Leacock 

Never had the black population of the city listened to or witnessed a more eloquent appeal. The Homesteader |Oscar Micheaux