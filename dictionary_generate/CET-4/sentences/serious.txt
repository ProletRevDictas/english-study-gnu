The pulps brought new readers to serious fiction, making it less intimidating with alluring art and low prices. How Pulp Fiction Saved Literature |Wendy Smith |January 8, 2015 |DAILY BEAST 

I like the idea of Jon Hamm… There have been discussions—though I'm not sure how serious they've been. ‘Archer’ Creator Adam Reed Spills Season 6 Secrets, From Surreal Plotlines to Life Post-ISIS |Marlow Stern |January 8, 2015 |DAILY BEAST 

Whatever the reason, and however absurd their beliefs may seem, American evangelicals are deadly serious. The Evangelical Apocalypse Is All Your Fault |Jay Michaelson |January 4, 2015 |DAILY BEAST 

He said it was okay, that he had been busy too… busy fighting serious intestinal problems. The Story Behind Lee Marvin’s Liberty Valance Smile |Robert Ward |January 3, 2015 |DAILY BEAST 

The “nature of the crime” was too serious to release him, they said. His First Day Out Of Jail After 40 Years: Adjusting To Life Outside |Justin Rohrlich |January 3, 2015 |DAILY BEAST 

Joe looked at her with a smile, his face still solemn and serious for all its youth and the fires of new-lit hope behind his eyes. The Bondboy |George W. (George Washington) Ogden 

It is the dramatic impulse of childhood endeavouring to bring life into the dulness of the serious hours. Children's Ways |James Sully 

Between South and North, the probabilities of a serious, and no very distant rupture, are strong and manifest. Blackwood's Edinburgh Magazine, Volume 60, No. 372, October 1846 |Various 

Never then was there a moment in which there was greater need for sane and serious thought. The Unsolved Riddle of Social Justice |Stephen Leacock 

Though the amount played for is serious, a good deal of rather bald conversation and chaff goes on. The Pit Town Coronet, Volume I (of 3) |Charles James Wills