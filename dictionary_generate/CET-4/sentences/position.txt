The scanner is light and super easy to use, and you can even put it away in a vertical position to make room for drawing when you’re done. Must-have art supplies to let your inner creative shine |Sandra Gutierrez G. |February 11, 2021 |Popular-Science 

Ironically, it is elderly men in senior positions who often have a reputation for talking endlessly during meetings and resenting any challenge to their authority, especially from women, experts say. Japan Olympics chief who said women talk too much will resign over remarks, reports say |Simon Denyer, Julia Mio Inuma |February 11, 2021 |Washington Post 

If I were actually applying for a position, the system would compare my scores with those of employees already working in that job. Auditors are testing hiring algorithms for bias, but there’s no easy fix |Amy Nordrum |February 11, 2021 |MIT Technology Review 

We are disgusted and outraged that someone in a position of power and trust would use it for these means. Lincoln Project’s avowed ignorance of Weaver texts undercut by leaked communications |Chris Johnson |February 9, 2021 |Washington Blade 

It’s more that he’s in a better position to do the things that make him such a great player. Joel Embiid Changed His Offseason Conditioning. Now He’s Playing Like An MVP. |Yaron Weitzman |February 9, 2021 |FiveThirtyEight 

Satirists occupy a perilous position—to skewer dogma and cant, and to antagonize the establishment while needing its protection. Harry Shearer on The Dangerous Business of Satire |Lloyd Grove |January 8, 2015 |DAILY BEAST 

You have to acknowledge your age and position in life, for me quite a lot of those emotionally fueled songs were hormone songs. Belle & Sebastian Aren’t So Shy Anymore |James Joiner |January 7, 2015 |DAILY BEAST 

If the ball goes off the screen, it teleports back to the starting position. Lost For Thousands of Strokes: 'Desert Golfing' Is 'Angry Birds' as Modern Art |Alec Kubas-Meyer |January 2, 2015 |DAILY BEAST 

In a 2009 interview, Church apostle Dallin H. Oaks held that the Church “does not have a position” on that point. Your Husband Is Definitely Gay: TLC’s Painful Portrait of Mormonism |Samantha Allen |January 1, 2015 |DAILY BEAST 

And by the time an airplane was in the water, its exact position would be known. Red Tape and Black Boxes: Why We Keep ‘Losing’ Airliners in 2014 |Clive Irving |December 29, 2014 |DAILY BEAST 

In this position, the line of cavalry formed the chord of the arc described by the river, and occupied by us. Blackwood's Edinburgh Magazine, No. CCCXXXIX. January, 1844. Vol. LV. |Various 

Polavieja, as everybody knew, was the chosen executive of the friars, whose only care was to secure their own position. The Philippine Islands |John Foreman 

Cousin George's position is such a happy one, that conversation is to him a thing superfluous. Physiology of The Opera |John H. Swaby (AKA "Scrici") 

It is only necessary to have a zinc, or a galvanized tray on which to stand the glass in an inverted position. How to Know the Ferns |S. Leonard Bastin 

The case may be kept in a light position, and when once under way it will rarely need any additional water. How to Know the Ferns |S. Leonard Bastin