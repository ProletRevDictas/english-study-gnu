Kate concludes that Jeanette may be a creep but that she would be an innocent one. ‘Cruel Summer’ finale reveals the truth about its biggest mystery. We think. |Bethonie Butler |June 16, 2021 |Washington Post 

The nurse returns with, of all things, “Sabbath’s Theater,” Philip Roth’s sexually explicit work about an aging, suicidal creep. In ‘The Living Sea of Waking Dreams,’ last-ditch medical interventions are their own horror story |Jake Cline |May 31, 2021 |Washington Post 

Between the labor-intensive work and that superficial creep factor, making miniatures can be solitary or even ostracizing. Why 2020 was the year of miniatures |Eleanor Cummins |January 1, 2021 |Popular-Science 

They must slow the creep of mortality until the last possible moment, at which point they abruptly pivot from healers to consolers. Emma Glass’s ‘Rest and Be Thankful’ powerfully describes what it means to be a health-care worker |Pete Tosiello |December 2, 2020 |Washington Post 

We can start by refusing to make or use any more digital shackles, and by refusing to let their creep extend one inch—or one use case—further. Covid-19 has led to a worrisome uptick in the use of electronic ankle monitors |Amy Nordrum |October 8, 2020 |MIT Technology Review 

Another acquaintance described Seevakumaran as “a creep,” who would “constantly hit on women.” School Shooters Love This Pickup Artist Website |Brandy Zadrozny |December 5, 2014 |DAILY BEAST 

It distorts more and more every day of the month, every year, due to the slow effects of fault creep. Silicon Valley Mansions, Swallowed Alive |Geoff Manaugh |November 8, 2014 |DAILY BEAST 

But generational differences soon creep in, threatening to pull the two couples apart. Oscar Season Kicks Off in Toronto: Channing Tatum, Kristen Stewart, and More Court Awards Glory |Marlow Stern |September 14, 2014 |DAILY BEAST 

Of course my very first words to that creep had been, “Which way to the mechanical sharks?” My Time on the Set of 'Jaws,' or How to Get a Photo of a Frickin' Mechanical Shark |Tom Shales |August 17, 2014 |DAILY BEAST 

As the price of gas continues to creep up, it is helpful to find ways to reduce fuel costs. Testing Automatic Link, the FitBit for Your Car |Jamie Todd Rubin |July 8, 2014 |DAILY BEAST 

He listened to tales of the Igorrotes, who live in huts like beehives and creep into them like insects. Alila, Our Little Philippine Cousin |Mary Hazelton Wade 

It was some minutes before the children dared to creep out of the bushes again. The Box-Car Children |Gertrude Chandler Warner 

There, in broad daylight, I saw Maloney deliberately creep closer to the fire and heap the wood on. Three More John Silence Stories |Algernon Blackwood 

Don't let the idea creep into your head, that I am going to give you a dull and sleepy essay on music. Mike Marble |Uncle Frank 

I am too sore and bruised to be thankful; I feel, sometimes, as if I could creep into a dark corner and cry my heart out. Tessa Wadsworth's Discipline |Jennie M. Drinkwater