This video covers route finding, uphill-skinning tips, and kick turns. Uphill Ski Touring for Beginners |Outside Editors |February 5, 2021 |Outside Online 

Nike says the new kicks are geared towards everyday wear, from going to the grocery store or on a casual walk with friends. Nike’s lace-free sneakers offer a perfect fit you simply step into |Claire Maldarelli |February 2, 2021 |Popular-Science 

On average, according to a mathematical analysis, the fast start lasted 827 meters, and the finishing kick started with 410 meters left. The Physiology of the Finishing Kick |Alex Hutchinson |January 26, 2021 |Outside Online 

I don’t know who gets the bigger kick out of it, the players or the Presidents. 93-Year-Old Announcer Charlie Brotman Just Served in His 16th Inauguration. Here's Why He's Still Honored to Do the Job |Melissa August |January 20, 2021 |Time 

An oddity of Saban’s run at Alabama is that the Tide led the FBS in missed kicks from 2007 to 2019. What To Watch For In The College Football National Championship |Richard Johnson |January 11, 2021 |FiveThirtyEight 

When fathers hold and play with their children, oxytocin and prolactin kick in, priming them for bonding. How Good Dads Can Change the World |Gary Barker, PhD, Michael Kaufman |January 6, 2015 |DAILY BEAST 

Eventually, Weirich had to kick out her jacuzzi and plants from her sunroom, where she now holds court. Inside A Finishing School for Transwomen |Sharon Adarlo |December 27, 2014 |DAILY BEAST 

Ramone, who turned to religion while trying to kick drugs, would probably approve (and laugh a little, too). ‘All Good Cretins Go to Heaven’: Dee Dee Ramone’s Twisted Punk Paintings |Melissa Leon |December 15, 2014 |DAILY BEAST 

Keith Green finds Ramone at the Chelsea, trying to kick heroin for good. ‘All Good Cretins Go to Heaven’: Dee Dee Ramone’s Twisted Punk Paintings |Melissa Leon |December 15, 2014 |DAILY BEAST 

It not only had a kick-ass part, it was the first Israeli movie featuring a transgender character in a leading role. Trans in the Holy Land: ‘Marzipan Flowers,’ Tal Kallai, and the Shattering of Israel’s LGBT Taboos |Itay Hod |November 4, 2014 |DAILY BEAST 

The latter trod on the toes of the former, whereupon the former threatened to "kick out of the cabin" the latter. The Book of Anecdotes and Budget of Fun; |Various 

And with that the host gave him such a kick as sent him howling into the street, amidst the roars of the company. The Book of Anecdotes and Budget of Fun; |Various 

I knowed, a-course, that I could go kick up a fuss when Simpson stopped by his office on his trip back from Goldstone. Alec Lloyd, Cowpuncher |Eleanor Gates 

Next morning the hero of Wagram, lame from the effect of a kick from his horse, was summoned before the Emperor. Napoleon's Marshals |R. P. Dunn-Pattison 

And I should kick the bottom out of dis crate just because you don't like the looks of somebody behind us! Hooded Detective, Volume III No. 2, January, 1942 |Various