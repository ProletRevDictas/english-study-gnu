While I was sympathetic, it makes no sense to stick our head in the sand. Patients are getting infected with covid-19 in the hospital. It happened to one of mine and killed him. |Manoj Jain |January 2, 2021 |Washington Post 

I completely get it and am sympathetic to my son's position. Carolyn Hax: He’s a great guy, except for the bigotry |Carolyn Hax |December 11, 2020 |Washington Post 

But, Young wanted to post the email in hopes that it would still convince people to shop early, and to be sympathetic about any delays. DTC brands are preparing for nightmare holiday shipping delays and out of stocks |Anna Hensel |December 4, 2020 |Digiday 

An issue I am deeply sympathetic with is that many authors don’t have access to affordable healthcare during this pandemic. As libraries fight for access to e-books, a new copyright champion emerges |Jeff |November 28, 2020 |Fortune 

It’s hard to imagine that Giuliani would similarly have been unable to either convince a Fox News host that his claims were legitimate or otherwise avoid being put in a position of tension with a sympathetic host. What line did Sidney Powell cross that Rudy Giuliani didn’t? |Philip Bump |November 23, 2020 |Washington Post 

At this point, the female rapper comes across as fairly sympathetic—that is, until Azealia Banks grabs the mic. Solange Smacks Jay Z, Legolas Slaps Bieber, and the Biggest Celebrity Feuds of the Year |Amy Zimmerman |December 24, 2014 |DAILY BEAST 

Goebbels, in fact, would be seen as the leader most sympathetic to the modernists. Top Nazis And Their Complicated Relationship With Artists |William O’Connor |November 30, 2014 |DAILY BEAST 

That is not to say the students who submit to the elitism and racism promoted by the USC Greek system are wholly sympathetic. Stepford Sororities: The Pressures of USC’s Greek Life |Maya Richard Craven |November 17, 2014 |DAILY BEAST 

Some Bush Administration officials were sympathetic , but told them that under existing law it would be very difficult. Bringing El Salvador Nun Killers to Justice |Raymond Bonner |November 10, 2014 |DAILY BEAST 

He says the owner of the house in which he lives has not been sympathetic to him. This Man Lost 35 Relatives to Ebola and His Community Wants Him Gone |Wade C.L. Williams |October 30, 2014 |DAILY BEAST 

Father, mother, sister, and brother all played and worked together with rare combination of sympathetic gifts. Kipling Stories and Poems Every Child Should Know, Book II |Rudyard Kipling 

The temptation to pour his financial troubles into the sympathetic ears of these two dear women he resisted. The Joyous Adventures of Aristide Pujol |William J. Locke 

"And the first thing you did with your liberty was to come to Europe," said Miss Thangue, with a sympathetic smile. Ancestors |Gertrude Atherton 

Nigel found him an excellent fellow, the most sympathetic and energetic man of Eastern blood whom he had ever encountered. Bella Donna |Robert Hichens 

If a merry dance is produced by the agile bow, its sympathetic tones at once excite a corresponding feeling. Violins and Violin Makers |Joseph Pearce