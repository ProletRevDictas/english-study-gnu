The increase is manageable, but logistically challenging given social distancing requirements, says Lithander. UK Universities Predicted a COVID-19 Crash. They Got the Opposite |Fiona Zublin |September 17, 2020 |Ozy 

All regions except the Northeast experienced modest increases in seroprevalence over the course of the summer. Blood donations show that the United States is still nowhere near herd immunity |Jonathan Lambert |September 17, 2020 |Science News 

Historically, the less-expensive systems sell more units, driving an increase in revenue from games. Sony debuts new PlayStation for the holidays. Here’s how much it costs |Verne Kopytoff |September 17, 2020 |Fortune 

There was a marked increase when Brazil hosted the football World Cup in 2014 and followed by the Olympics in 2016. Podcast: COVID-19 is helping turn Brazil into a surveillance state |Anthony Green |September 16, 2020 |MIT Technology Review 

The unprecedented shift to remote work and online learning, combined with a dramatic increase in movie streaming, videoconferencing, and social media traffic, has led to significant spikes in internet use. How to drive digital innovation necessary during the pandemic |Nick Chasinov |September 16, 2020 |Search Engine Watch 

Industry experts claim an increase in awareness amongst men when it comes to styles, design, and price regarding their underwear. Would You Pay $100 For a 50 Cent Bulge? Men’s Undies Get Expensive |James Joiner |December 23, 2014 |DAILY BEAST 

That indicated a relatively rapid increase in methane, followed by an equally fast decrease. Methane on Mars: Life or Just Gas? |Matthew R. Francis |December 17, 2014 |DAILY BEAST 

In Kentucky the bourbon distillers have had to increase their payrolls by 77 per cent in two years to meet the demand. Beer Countries vs. Wine Countries |Clive Irving |December 7, 2014 |DAILY BEAST 

In exchange for the increase in sales, the dispensaries pay the service, not the consumer. Days Are Numbered for Nestdrop, LA’s ‘Uber for Weed’ |Justin Hampton |December 6, 2014 |DAILY BEAST 

Both give estimates of how many U.S. workers would benefit from an increase in the minimum wage. Staving Off a Democratic Civil War |Michael Tomasky |December 2, 2014 |DAILY BEAST 

The occasion should be seized also to increase the balances of depositors who carry unprofitable accounts. Readings in Money and Banking |Chester Arthur Phillips 

An increase is also noted in the uric-acid diathesis and in diseases accompanied by respiratory insufficiency. A Manual of Clinical Diagnosis |James Campbell Todd 

Simple constipation and diseases of the large intestine alone do not increase the amount of indican. A Manual of Clinical Diagnosis |James Campbell Todd 

Instinctively he tried to hide both pain and anger—it could only increase this distance that was already there. The Wave |Algernon Blackwood 

The ability to sustain the tone for a long time will increase, and with it the power of the muscles exercised. Expressive Voice Culture |Jessie Eldridge Southwick