“From everything I’ve seen so far, and from the people who have reached out to me, yes, I’m positive that was Clint,” said the former coach, Jason Strunk, when reached by telephone Monday. Sheriff’s deputy who tackled Super Bowl field invader was a ‘tough as nails’ high school QB |Des Bieler |February 9, 2021 |Washington Post 

One allowed access to a telephone used for checking in and non-emergency communication with headquarters. Call box of duty: Cops remember the call boxes that kept them connected |John Kelly |January 30, 2021 |Washington Post 

However, during a telephone conversation, the agency refused to confirm the mission took place in Mexico—citing the protection of “sources and methods.” Lunik: Inside the CIA’s audacious plot to steal a Soviet satellite |Bobbie Johnson |January 28, 2021 |MIT Technology Review 

Just as a century ago we embraced the telephone, we embraced Facebook. 'We Need a Fundamental Reset.' Shoshana Zuboff on Building an Internet That Lets Democracy Flourish |Billy Perrigo |January 22, 2021 |Time 

Feliciano said in a brief telephone interview Thursday evening. Virginia Democrats seek to remove member of state’s new redistricting commission as it convenes for first meeting |Gregory S. Schneider |January 22, 2021 |Washington Post 

After my first trip to his place in Tucson we called one another on the telephone. The Story Behind Lee Marvin’s Liberty Valance Smile |Robert Ward |January 3, 2015 |DAILY BEAST 

Once a month he attaches a device to his chest, clamps metal bracelets on his wrists, and hooks the whole thing up to a telephone. Alfred Hitchcock’s Fade to Black: The Great Director’s Final Days |David Freeman |December 13, 2014 |DAILY BEAST 

He gives a long telephone interview to nine reporters around the country. Alfred Hitchcock’s Fade to Black: The Great Director’s Final Days |David Freeman |December 13, 2014 |DAILY BEAST 

“I grew up with Special Forces,” Shadman said during a telephone interview Wednesday. Special Forces’ $77M ‘Hustler’ Hits Back |Kevin Maurer |December 8, 2014 |DAILY BEAST 

Since 2013, his telecoms company Beijing Xinwei has been awarded several telephone and internet licenses. China’s Nicaragua Canal Could Spark a New Central America Revolution |Nina Lakhani |November 30, 2014 |DAILY BEAST 

New proposals regarding telephone charges are expected as soon as the Select Committee has reported. Punch, or the London Charivari, Volume 158, April 28, 1920 |Various 

It contained a writing-table (upon which was a telephone and a pile of old newspapers), a cabinet, and two chairs. Dope |Sax Rohmer 

At the back, near a stand that racked a number of grease guns, he saw a second telephone fixed to the wall. Hooded Detective, Volume III No. 2, January, 1942 |Various 

As they left the tower and the ugly old woman, they heard the latter calling a number into the telephone receiver. The Campfire Girls of Roselawn |Margaret Penrose 

If Burkey knew the Eye's telephone number he apparently kept it in his head. Hooded Detective, Volume III No. 2, January, 1942 |Various