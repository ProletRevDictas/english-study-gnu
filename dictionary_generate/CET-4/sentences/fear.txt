Others voice fear that their taxes would be raised under Biden. Election live updates: Trump returns to Wisconsin; Biden to face live audience at town hall |Colby Itkowitz, Felicia Sonmez, John Wagner |September 17, 2020 |Washington Post 

There’s a palpable fear among Democrats that voters will blame them on Election Day should they appear to be putting their own re-elections ahead of what’s good for Americans. Trump moves closer to Pelosi in economic aid talks, and House speaker must decide next move |Rachael Bade, Erica Werner |September 17, 2020 |Washington Post 

She was asked by senior Pence aides, she said, to help on an op-ed for the Wall Street Journal that minimized the fears of a second coronavirus wave and touted the administration’s work on the virus as a success story. Former Pence aide says she will vote for Biden because of Trump’s ‘flat out disregard for human life’ during pandemic |Josh Dawsey |September 17, 2020 |Washington Post 

Cleopatra and Toliver conquer obstacles and fears under Bell’s tutelage and draw connections between the trail and life. Discovering the Joy of Bikepacking in the Backcountry |Outside Editors |September 17, 2020 |Outside Online 

That might seem tangential to solving your back pain, but the truth is that a large part of overcoming that discomfort is about overcoming the fear of being in pain. The best thing for back pain is actually more movement |Sara Chodosh |September 16, 2020 |Popular-Science 

Before anti-vaxxers, there were anti-fluoriders: a group who spread fear about the anti-tooth decay agent added to drinking water. Anti-Fluoriders Are The OG Anti-Vaxxers |Michael Schulson |July 27, 2016 |DAILY BEAST 

The fear of violence should not determine what one does or does not say. Trolls and Martyrdom: Je Ne Suis Pas Charlie |Arthur Chu |January 9, 2015 |DAILY BEAST 

Stephanie Giorgio, a classical musician, credits The Class for helping her cope with anxiety, focus, fear, and self-doubt. How Taryn Toomey’s ‘The Class’ Became New York’s Latest Fitness Craze |Lizzie Crocker |January 9, 2015 |DAILY BEAST 

The choice between freedom and fear is not difficult when seen with perspective. Why We Stand With Charlie Hebdo—And You Should Too |John Avlon |January 8, 2015 |DAILY BEAST 

The decision not to run the cartoons is motivated by nothing more than fear: either fear of offending or fear of retaliation. Why We Stand With Charlie Hebdo—And You Should Too |John Avlon |January 8, 2015 |DAILY BEAST 

Truth is a torch, but one of enormous size; so that we slink past it in rather a blinking fashion for fear it should burn us. Pearls of Thought |Maturin M. Ballou 

To others the fierce desire for social justice obliterates all fear of a general catastrophe. The Unsolved Riddle of Social Justice |Stephen Leacock 

Her heart fluttered violently with fear as she saw that he stepped out after her, and walked by her side toward the house. Checkmate |Joseph Sheridan Le Fanu 

And for fear of being ill spoken of weep bitterly for a day, and then comfort thyself in thy sadness. The Bible, Douay-Rheims Version |Various 

The water suggested the fear that he must be nearing the open sea, and he became supernaturally grave. The Giant of the North |R.M. Ballantyne