The exterior duvet is machine washable, but you’ll want to hand-wash the interior blanket. Best weighted blanket: Sleep like a baby with our comfy bedding picks |PopSci Commerce Team |February 12, 2021 |Popular-Science 

Images of the shell’s interior revealed two holes that had been chipped into spiral layers just beneath the opening, likely to hold the mouthpiece in place. Humans made a horn out of a conch shell about 18,000 years ago |Bruce Bower |February 10, 2021 |Science News 

The interior is lined with fleece that contains heat coils that can stay active for up to five hours. Best hand warmers: Block the chill during your favorite winter activities |PopSci Commerce Team |February 10, 2021 |Popular-Science 

As mentioned earlier, toaster ovens reach high temps through high wattage, but this process takes further delicate engineering and is facilitated by using multiple heating elements and placing them in specific areas of the oven’s interior. Best toaster oven: Save counter space and time with our toaster oven picks |Julian Cubilllos |February 5, 2021 |Popular-Science 

These hues, often rich and dark, evoke shadowy interiors and midnight skies. In the galleries: Exploring the tension between physical and digital art |Mark Jenkins |February 5, 2021 |Washington Post 

The interior video shows the gunman firing the shot through the window. Shot Down During the NYPD Slowdown |Michael Daly |January 7, 2015 |DAILY BEAST 

One of its top officials is the current minister of the interior in Baghad. What an Iranian Funeral Tells Us About the Wars in Iraq |IranWire |January 6, 2015 |DAILY BEAST 

Justin gazed out from the dim interior as more than 300 police motorcycles from dozens of jurisdictions rumbled past. Choking Back Tears, Thousands of Cops Honor Fallen Officer Ramos |Michael Daly |December 28, 2014 |DAILY BEAST 

Isaacs recently returned from the New Mexico desert after shooting interior scenes for a new TV mini-series called Dig. After The Fall: Introducing The Anti-Villain |Rich Goldstein |December 21, 2014 |DAILY BEAST 

“We know the outbreak is still flaming strongly in western Sierra Leone and some parts of the interior of Guinea,” said Nabarro. Jail Threats for Sierra Leone Ebola Victims’ Families |Abby Haglage |December 10, 2014 |DAILY BEAST 

A reproduction of Mrs. Charmington herself decorated the interior of the omnibuses. The Pit Town Coronet, Volume I (of 3) |Charles James Wills 

I don't know much about the interior arrangements of Kullak's conservatory, because I only went to his own class. Music-Study in Germany |Amy Fay 

The contrast between the open street and the enclosed stuffiness of the dim and crowded interior was overwhelming. Hilda Lessways |Arnold Bennett 

The intensity of this drama, however, being interior, caused little outward disturbance that casual onlookers need have noticed. The Wave |Algernon Blackwood 

We give an engraving of a kind of pipe used by the natives of interior Africa. Tobacco; Its History, Varieties, Culture, Manufacture and Commerce |E. R. Billings.