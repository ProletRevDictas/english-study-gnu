Yet faced with language and literacy barriers, immigration fears, or a lack of outreach from local and state governments, some of the most vulnerable communities have become fertile ground for vaccine misinformation, advocates say. Among Latino immigrants, false vaccine claims are spreading as fast as the virus |Teo Armus |February 11, 2021 |Washington Post 

The Maryland General Assembly is poised to create a state-mandated system to compensate people who are wrongly convicted and imprisoned, removing barriers to getting paid that exonerees have faced for decades. Maryland moves toward clear plan for paying people who were wrongly convicted |Ovetta Wiggins |February 10, 2021 |Washington Post 

The jargon and acronyms used can confuse even most political observers, let alone those with language barriers. Morning Report: Redistricting Issues Could Get Lost in Translation |Voice of San Diego |February 10, 2021 |Voice of San Diego 

Our position and framework is the commission should make plans to meet the needs of both sizeable groups and additionally smaller language groups where there is organizing happening to take part, but language will be a barrier. African Communities Warn Language Issues Could Shut Them Out of Redistricting |Maya Srikrishnan |February 10, 2021 |Voice of San Diego 

Instead, it is the lack of accessible or alternative resources that creates a barrier. Ford’s next pandemic mission: Clear N95 masks and low-cost air filters |Hannah Denham |February 9, 2021 |Washington Post 

I was out, maybe in the Great Barrier Reef catching black marlin. The Story Behind Lee Marvin’s Liberty Valance Smile |Robert Ward |January 3, 2015 |DAILY BEAST 

Like other barrier-breakers before him, Colfer suffered his share of doubters. Chris Colfer on Writing, Acting, and the Pain of Being A Pop Culture Trailblazer |Oliver Jones |December 15, 2014 |DAILY BEAST 

Language was no barrier; just about every tongue on the planet was babbling away, caught up in the elaborate mystique of a cult. Sherlock Holmes Vs. Jack the Ripper |Clive Irving |November 16, 2014 |DAILY BEAST 

It was called, rather dramatically, breaking the sound barrier. Virgin Galactic’s Flight Path to Disaster: A Clash of High Risk and Hyperbole |Clive Irving |November 1, 2014 |DAILY BEAST 

He saw a chain barrier covered with PVC piping that the Jeep had apparently struck and damaged before becoming stuck. Manhunt for a Cop-Hating Pennsylvania ‘Survivalist’ |Michael Daly |September 17, 2014 |DAILY BEAST 

He was fired at by the sepoys, of course, but horse and man escaped untouched and the low barrier was leaped without effort. The Red Year |Louis Tracy 

But scorn is far more volcanic than glacial and a poor barrier between sex and judgment. Ancestors |Gertrude Atherton 

The barrier between them lowered perceptibly again, and Tom felt a momentary return of the confidence he had lost. The Wave |Algernon Blackwood 

The talk between them rarely touched reality, as though a barrier deadened their very voices. The Wave |Algernon Blackwood 

Guilelessly, the old man, in a few words, had swept away the barrier Mary and I had raised between us. The Soldier of the Valley |Nelson Lloyd