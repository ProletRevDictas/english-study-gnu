Indeed, when the first asylums were created, the poor and the mad were locked up together with the rest of society’s outcasts. How colonialism and capitalism helped place a stigma on mental illness |Balaji Ravichandran |February 12, 2021 |Washington Post 

A House committee took the proposal further this week, replacing Hogan’s $750 payments to 400,000 poor families with a three-year anti-poverty plan that would increase the Earned Income Tax Credit. Maryland House Democrats push to expand eligibility for covid relief |Ovetta Wiggins, Erin Cox |February 11, 2021 |Washington Post 

The wealthier wards had kids returning to class at twice the rate as the poorest ward in the city, according to city data. The mid-pandemic return to school is totally weird for kids. And possibly lonely, too. |Petula Dvorak |February 11, 2021 |Washington Post 

Chairs that offer active sitting will strengthen your core while offering support for your lower back, while chairs that have poor support can cause spinal alignment issues and lead to a potentially serious back injury. Best desk chair for any home office |PopSci Commerce Team |February 11, 2021 |Popular-Science 

Their conclusion, summarized in a statement and short report, is that the crash stemmed from the pilot executing “poor decision making” as well as experiencing spatial disorientation. This surprisingly common flight issue contributed to Kobe Bryant’s helicopter crash |Rob Verger |February 11, 2021 |Popular-Science 

Placed in drinking water, fluoride can serve people who otherwise have poor access to dental care. Anti-Fluoriders Are The OG Anti-Vaxxers |Michael Schulson |July 27, 2016 |DAILY BEAST 

If so, he has his silence -- on top of poor judgment -- to blame. The Price of Steve Scalise’s Silence |Jason Berry |January 7, 2015 |DAILY BEAST 

But most likely it was linked to the way priests identify with the poor in the face of government and criminal abuses. Mexico’s Priests Are Marked for Murder |Jason McGahan |January 7, 2015 |DAILY BEAST 

What they actually mean by that is, you know, he actually knows some people that are poor. Why This Liberal Hearts Huckabee |Sally Kohn |January 6, 2015 |DAILY BEAST 

For those living in poor communities in particular, interactions with police rarely come with good news and a smile. How to Solve the Policing Crisis |Keli Goff |January 5, 2015 |DAILY BEAST 

Poor Squinty ran and tried to hide under the straw, for he knew the boy was talking about him. Squinty the Comical Pig |Richard Barnum 

He did believe you, more or less, and what you said fell in with his own impressions—strange impressions that they were, poor man! Confidence |Henry James 

In withdrawing aside sorrow remaineth: and the substance of the poor is according to his heart. The Bible, Douay-Rheims Version |Various 

The poor must look to the brightness of a future world for the consolation that they were denied in this. The Unsolved Riddle of Social Justice |Stephen Leacock 

Y was a Youth, that did not love school; Z was a Zany, a poor harmless fool. Boys and Girls Bookshelf; a Practical Plan of Character Building, Volume I (of 17) |Various