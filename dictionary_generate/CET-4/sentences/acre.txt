Amid the fundraising and growing the business in a pandemic, Menker was intensely involved in efforts to combat the locust swarm that has devastated hundreds of thousands of acres of crops and pasture land in Africa, including her native Ethiopia. Sara Menker, CEO of Gro Intelligence, Believes Big Data Can Save Our Climate and Food Supply |Eben Shapiro |February 21, 2021 |Time 

Public agencies had occupied thousands of acres intended to return Native people to their ancestral lands, paying little or no compensation for decades as the sites were used for military bases, game preserves, schools and other purposes. The Government Promised to Return Ancestral Hawaiian Land, Then Never Finished the Job |by Rob Perez, Honolulu Star-Advertiser, and Agnel Philip, ProPublica |December 19, 2020 |ProPublica 

So they have to plant corn, beans, or hay in those acres, and the yields have to come up to the county averages. Finding the ground truth about crop yields |Katie McLean |December 18, 2020 |MIT Technology Review 

Achieving it could significantly increase Russia’s prosperity and power in the process, through the opening of tens of millions of acres of land and a flourishing new agricultural economy. The Big Thaw: How Russia Could Dominate a Warming World |by Abrahm Lustgarten, photography by Sergey Ponomarev |December 16, 2020 |ProPublica 

Nuns, and the nearby fires it merged with, went on to burn over 56,000 acres and destroyed more than 1,500 structures. The Long-Lasting Mental Health Effects of Wildfires |Jane C. Hu |December 3, 2020 |Outside Online 

“They say they treat the dogs like family,” Schill said, referring to the Green Acre website, which has been taken down. Arizona Opens Investigation Into Deadly Doggie Disneyland |Caitlin Dickson |June 25, 2014 |DAILY BEAST 

One woman said her dog came back from Green Acre with unexplained chemical burns. Arizona Opens Investigation Into Deadly Doggie Disneyland |Caitlin Dickson |June 25, 2014 |DAILY BEAST 

Ingram first reached out to MaLeisa and Todd Hughes, the owners of Green Acre Dog Boarders, about two months ago. Arizona Opens Investigation Into Deadly Doggie Disneyland |Caitlin Dickson |June 25, 2014 |DAILY BEAST 

Yet, as these families would eventually discover, there were 28 dogs at Green Acre at the time of the deaths. Arizona Opens Investigation Into Deadly Doggie Disneyland |Caitlin Dickson |June 25, 2014 |DAILY BEAST 

His attempt to boost farm wages, called the Agricultural Adjustment Act, supposedly "plowed under" every fourth acre. When America Said No to War |Marc Wortman |September 10, 2013 |DAILY BEAST 

During the siege of Acre he commanded the covering force, and pushed reconnaissances far and wide. Napoleon's Marshals |R. P. Dunn-Pattison 

Nearly every acre I have seen is susceptible of cultivation, and of course either cultivated, built upon, or devoted to wood. Glances at Europe |Horace Greeley 

It has been shown by Chevandrier, that an acre of land under beech wood accumulates annually about 1650 lb. Elements of Agricultural Chemistry |Thomas Anderson 

Now, the column of air resting upon an acre of land contains only about 15,500 lb. Elements of Agricultural Chemistry |Thomas Anderson 

Why, child alive, I raked the hay together on three whole six-acre fields! Dorothy at Skyrie |Evelyn Raymond