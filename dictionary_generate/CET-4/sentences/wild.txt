Still, in an ozone-polluted environment in the wild, tobacco hawkmoths would have to be close enough to a tobacco flower to see it to learn its altered scent, and Knaden isn’t sure how often that will occur. This moth may outsmart smog by learning to like pollution-altered aromas |Carmen Drahl |September 11, 2020 |Science News 

The machine learning adds just enough of a wild card on top of the scripted tracks to give each user a unique mix. Create your own moody quarantine music with Google’s AI |Karen Hao |September 4, 2020 |MIT Technology Review 

That’s a wild claim until you consider that the series’ spin-off has been attracting more viewers than would-be franchise contenders like “Real Housewives” and “Below Deck.” As AT&T considers downsizing its media business, whither WarnerMedia? |Tim Peterson |September 2, 2020 |Digiday 

When people put eggs in the wild, there’s no antibiotic, so daughters die. Genetically modified mosquitoes have been OK’d for a first U.S. test flight |Susan Milius |August 22, 2020 |Science News 

Tesla, the pioneer of the category, has experienced wild stock growth over the past 12 months, culminating in the expectation that it will join the S&P 500. Electric-vehicle startup Canoo to go public, joining the wave of companies chasing Tesla’s success |dzanemorris |August 18, 2020 |Fortune 

He has wild swings between trying not to care about Lana and the baby, and being completely obsessed by it. ‘Archer’ Creator Adam Reed Spills Season 6 Secrets, From Surreal Plotlines to Life Post-ISIS |Marlow Stern |January 8, 2015 |DAILY BEAST 

The sound of birds, quail, even doe, make a wild grid of noise. The Story Behind Lee Marvin’s Liberty Valance Smile |Robert Ward |January 3, 2015 |DAILY BEAST 

Exactly when the transition to modern domestic creature took place, for a bird that is wild to this day, is controversial. The History of the Chicken: How This Humble Bird Saved Humanity |William O’Connor |December 27, 2014 |DAILY BEAST 

It is wild that something that would seem to be so scandalous would just disappear from the press. Inside the Lifetime Whitney Houston Movie’s Lesbian Lover Storyline |Kevin Fallon |December 16, 2014 |DAILY BEAST 

Everyone who saw Beasts of the Southern Wild knows Wallis is a unique talent, but still, no one saw this coming. The Golden Globes Sobers Up (Sort Of): Years of Ridicule and Bribery Rumors Scares HFPA Straight |Marlow Stern |December 11, 2014 |DAILY BEAST 

They are so rich in harmony, so weird, so wild, that when you hear them you are like a sea-weed cast upon the bosom of the ocean. Music-Study in Germany |Amy Fay 

The white men served their smoking cannon with a wild energy that, for a time, made the gallant nine equal to a thousand. The Red Year |Louis Tracy 

A cricket-match was in progress, but the bowling and batting were extremely wild, thanks to The Warren strong beer. The Pit Town Coronet, Volume I (of 3) |Charles James Wills 

People are busy ballooning or driving; shooting like stars along railroads; or migrating like swallows or wild-geese. Blackwood's Edinburgh Magazine, No. CCCXXXIX. January, 1844. Vol. LV. |Various 

Suddenly he shot a disturbing glance at Tressan's face, and the corner of his wild-cat mustachios twitched. St. Martin's Summer |Rafael Sabatini