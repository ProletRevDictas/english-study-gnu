Later on, astronauts looked at the pellets after one, two and three years. Clumps of bacteria could spread life between planets |Paola Rosa-Aquino |August 27, 2020 |Popular-Science 

Being an astronaut is the ultimate idea of looking into what surrounds us by going there. The Physicist Who Slayed Gravity’s Ghosts |Thomas Lewton |August 18, 2020 |Quanta Magazine 

Though we may say that astronauts are weightless in space, what they’re actually experiencing is microgravity. Scientists Say: Microgravity |Bethany Brookshire |July 27, 2020 |Science News For Students 

About 30 nonphysicians learned first aid from a software system designed to help astronauts. What will astronauts need to survive the dangerous journey to Mars? |Maria Temming |July 15, 2020 |Science News 

Other simulated Mars missions suggest that astronauts isolated together could develop an us-versus-them mentality that would lead the crew to stop listening to mission control, which could be dangerous on a long mission. Two new books explore Mars — and what it means to be human |Lisa Grossman |July 15, 2020 |Science News 

On the second planet, they encounter a marooned astronaut named Dr. Mann, and a fistfight ensues. Neil deGrasse Tyson Breaks Down ‘Interstellar’: Black Holes, Time Dilations, and Massive Waves |Marlow Stern |November 11, 2014 |DAILY BEAST 

Her long-term boyfriend Dave Clark is head of ‘astronaut relations’ for Virgin Galactic. Princess Beatrice Gives Up Her Virgin Galactic Dream |Tom Sykes |November 3, 2014 |DAILY BEAST 

I asked a former NASA astronaut, who cannot be quoted on the record, to look at photographs of the debris. Clues From SpaceShipTwo’s Wreckage: Did the Crew Compartment Fail? |Clive Irving |November 2, 2014 |DAILY BEAST 

“The astronaut phenomenon is destroying the Iraqi army,” one officer, Kadhim al-Shammari, told us. Iraqi Soldiers Bribe Officers So They Don't Have to Fight ISIS |Niqash |October 8, 2014 |DAILY BEAST 

BAGHDAD, Iraq — The Iraqi army is suffering badly from what locals describe as the “astronaut phenomenon.” Iraqi Soldiers Bribe Officers So They Don't Have to Fight ISIS |Niqash |October 8, 2014 |DAILY BEAST 

Of course, ground control was to be used only if the astronaut failed to ignite the retro-rockets himself. Egocentric Orbit |John Cory 

The astronaut fell victim to a psychological stress that was unforeseen. Ten From Infinity |Paul W. Fairman 

The parachute opened and the daring astronaut drifted towards the sea. Make Mine Homogenized |Rick Raphael 

It would not surprise me to find that most of those languages have survived and that our distressed astronaut knows them all. Anything You Can Do |Gordon Randall Garrett 

It would not surprise me to find that most of these languages have survived and that our distressed astronaut knows them all. Anything You Can Do ... |Gordon Randall Garrett