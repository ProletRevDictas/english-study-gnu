Fat-free or skim milkEveryone knows milk is an excellent source of calcium that will keep your bones in tip-top shape. 10 Ways to Stay Hydrated (That Aren’t Water) |DailyBurn |July 25, 2014 |DAILY BEAST 

And good for Bruno, bless his heart, who is truly the skim milk of pop music. Your Super Bowl Etiquette Guide From Food to Clothes to What Not to Say |Kelly Williams Brown |February 1, 2014 |DAILY BEAST 

And the iTunes story makes music—arguably the most mysterious, magical art form—as accessible and ubiquitous as skim milk. From Bieber to the Beatles, How the iTunes Store Brooklynized Music |Justin Moyer |May 5, 2013 |DAILY BEAST 

Skim off most of the fat with a spoon: just dip in, get a spoonful of fat, and remove. Your Friday Gadget Chef Recipe: Two Day Soup |Megan McArdle |November 9, 2012 |DAILY BEAST 

Drinks at the dinner table are restricted to “water and skim milk.” 12 Juicy Bits From Michelle Obama’s Garden Book |Alex Klein |May 31, 2012 |DAILY BEAST 

All summer we have had three calves that came to the orchard fence twice a day to get their ration of skim milk and feeding flour. The Red Cow and Her Friends |Peter McArthur 

When the birds are done, skim off all grease, add the juice of a lemon, and serve hot. Dressed Game and Poultry la Mode |Harriet A. de Salis 

You don't suppose, do you, I've had time since Tuesday to read all this through and skim off the cream?' Wandering Heath |Sir Arthur Thomas Quiller-Couch 

In our own little book we have been compelled to skim lightly, and, in many places, to pass over subjects of great interest. The Ocean and its Wonders |R.M. Ballantyne 

Now, I have made a calculation, and I am satisfied that Mrs. Skim can not possibly make much profit out of me. Harper's New Monthly Magazine, No. VII, December 1850, Vol. II |Various