To be fair, this is one of the least-insulting comparisons Watters could have made. Getting to Know the ‘Beyoncé Voter’ |Kelly Williams Brown |July 7, 2014 |DAILY BEAST 

The Mosel region is arguably the most storied, least-understood wine region in the world. Germany’s Wine Revolution Is Just Getting Started |Jordan Salcito |April 26, 2014 |DAILY BEAST 

He has one of the least-lined faces of anyone I have ever met. BP, Putin, and the Power of Oil |Tim Teeman |March 9, 2014 |DAILY BEAST 

The New York/New Jersey/Long Island metropolitan region is the fourth-least-equal U.S. metro region, according to one study. Can Bill de Blasio Fix New York’s Income Inequality? |David Freedlander |September 18, 2013 |DAILY BEAST 

When you're among the least-prepared students in the class, you're likely to struggle. Affirmative Action: Who Does it Help, Who Does it Hurt? |Megan McArdle |June 24, 2013 |DAILY BEAST 

I assure you I am very sorry for critics all round,they are the least-regarded and worst-rewarded of all the literary community. The Sorrows of Satan |Marie Corelli 

These eight little volumes will always remain Borrow's least-read books. George Borrow and His Circle |Clement King Shorter 

On Alaska alone, the least-known section of the Pacific coast, there is a bibliographical list of four thousand. Vikings of the Pacific |Agnes C. Laut 

So far Maui had the luck which so commonly attends the youngest and least-considered child in folklore and mythology. Modern Mythology |Andrew Lang 

Least-wise, he would rot in a sewer near a busy bus stop replete with all the dronings of archaic feet. The Land of Look Behind |Paul Cameron Brown