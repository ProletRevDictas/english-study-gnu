The hospital effectively ended the legal proceedings by filing thousands of notices with Shelby County General Sessions Court stating that the defendants’ balances were now zero. What Happens After a Debt Collection Machine Grinds to a Halt |by Wendi C. Thomas, MLK50: Justice Through Journalism |October 2, 2020 |ProPublica 

“What I’m focused on is ensuring we’re picking the right areas of enterprise to focus on,” she said, citing training, remote assist, and 3D visualization as the three primary use cases the company is zeroing in on. Magic Leap’s Peggy Johnson: Becoming CEO of a pivoting business doesn’t mean jumping off the ‘glass cliff’ |lbelanger225 |October 1, 2020 |Fortune 

The plaintiffs in Texas, which include 18 states with Republican governors or attorneys general, claim this zeroed-out mandate is unconstitutional. 7 big cases the Supreme Court will hear in its new term, explained |Ian Millhiser |October 1, 2020 |Vox 

You’ve got to connect with those people who are at ground zero and know what they’re saying is really happening. Erin Brockovich has given up on the federal government saving the environment |Nicole Goodkind |September 25, 2020 |Fortune 

“The industry dynamics have changed,” said Antonios Drossos, a Finland-based telecoms consultant whose firm Rewheel has analyzed EU zero-rating offers over recent years. The EU’s top court just closed a major loophole in Europe’s net-neutrality rules |David Meyer |September 15, 2020 |Fortune 

Not quite, but at one point the temperature registered 29 below zero, with 21 inches of snow. Speed Read: The Juiciest Bits From the History of ‘Purple Rain’ |Jennie Yabroff |January 1, 2015 |DAILY BEAST 

The longer someone stays well, the lower their chance of relapsing, although that possibility never becomes zero. You’re Never ‘Cured’ of an Eating Disorder |Carrie Arnold |December 20, 2014 |DAILY BEAST 

Is it True that “Gays vs. Traditionalists are a Zero-Sum Game”? Do LGBTs Owe Christians an Olive Branch? Try The Other Way Around |Jay Michaelson |December 14, 2014 |DAILY BEAST 

Islamic State brought “peace, autonomy, zero corruption, low crime-rate,” he Tweeted last month. The Scared Widdle Kitty of ISIS |Jacob Siegel |December 12, 2014 |DAILY BEAST 

During the recession net immigration to the U.S. from Mexico fell to zero or less. The Case for More Low-Skill Immigration |Veronique de Rugy |December 7, 2014 |DAILY BEAST 

But the day he planned to start was very cold—the mercury stood twenty-seven below zero. The Homesteader |Oscar Micheaux 

Of course, at roulette, some number or zero itself is bound to come up every time, but number twenty-seven was invariably unlucky. The Pit Town Coronet, Volume III (of 3) |Charles James Wills 

The words whispered in the pastry-cook's ear cooled his hot fit of courage down to zero. An Episode Under the Terror |Honore de Balzac 

At last, one evening, he walked up to a table and said to the croupier, "When was zero up last?" You Never Know Your Luck, Complete |Gilbert Parker 

One day last week, when the mercury was sulking at zero, three lambs arrived on the place. The Red Cow and Her Friends |Peter McArthur