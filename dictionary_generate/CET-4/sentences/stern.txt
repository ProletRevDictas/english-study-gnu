Only hours after tug boats had initially wrenched the stern free, some news outlets reported that strong winds had blown it back to its stuck position across the 205-meter southern section of the canal. How the Giant Boat Blocking the Suez Canal Was Freed: Dredgers, Tugboats, and a Full Moon |Joseph Hincks |March 29, 2021 |Time 

That disruption appears to have caused the ship to become wedged sideways across the canal, with its bow pressed against the eastern wall and its stern wedged into the canal’s western wall. How did a ship get stuck in the Suez Canal? |Miriam Berger, Júlia Ledur, Adam Taylor |March 26, 2021 |Washington Post 

Stern said he and fellow legislators wanted public access to all agency documents restored. Are California Oil Companies Complying With the Law? Even Regulators Often Don’t Know. |by Janet Wilson, The Desert Sun |March 22, 2021 |ProPublica 

They may not even need a host star at all, Stern writes, and could exist on wandering planets ejected from their systems. Extraterrestrial Life Could Be Hiding in Our Galaxy’s Interior Ocean Worlds |Jason Dorrier |March 21, 2021 |Singularity Hub 

In his report, Stern suggests interior ocean worlds have several advantages over exterior ocean worlds, and therefore, if they’re common, it’s far less likely we are alone in the universe—but also, it might be a lot harder to prove the case. Extraterrestrial Life Could Be Hiding in Our Galaxy’s Interior Ocean Worlds |Jason Dorrier |March 21, 2021 |Singularity Hub 

He opens up to Marlow Stern about music, Hollywood, and more. Idris Elba on Eric Garner, ‘Mi Mandela,’ and Selling Weed to Dave Chappelle |Marlow Stern |December 6, 2014 |DAILY BEAST 

Buress went on The Howard Stern Show to talk about the Cosby story. How the World Turned on Bill Cosby: A Day-by-Day Account |Scott Porch |December 1, 2014 |DAILY BEAST 

But at the end of the day, as a governor, you have to be stern and there are decisions you have to make. Wyclef Jean Talks Lauryn Hill, the Yele Haiti Controversy, and Chris Christie |Marlow Stern |November 20, 2014 |DAILY BEAST 

I like to end columns with a potential policy fix, some kind of suggested action, or at least a stern finger-wagging. GOP States’ Hitlist: Abortion, Unions & Hillary |Nancy Kaffer |November 18, 2014 |DAILY BEAST 

After a stern media backlash, Dunham decided to pay her opening acts and, predictably, all was forgiven. Will White Feminists Finally Dump Lena Dunham? |Samantha Allen |November 4, 2014 |DAILY BEAST 

He had meted out stern justice to his own son, when he had banished big Reginald to South America; but he had his virtues. The Pit Town Coronet, Volume I (of 3) |Charles James Wills 

Her stern was towards him, and all he saw of her was the ironical legend, “Cure your Corns.” The Joyous Adventures of Aristide Pujol |William J. Locke 

As it came near, it proved to be the clock, with a sail hoisted, and the Goblin sitting complacently in the stern. Davy and The Goblin |Charles E. Carryl 

As he read, a look of surprise came over his face, and then his countenance grew stern and grim. The Courier of the Ozarks |Byron A. Dunn 

As Louis spoke with the stern calmness of a divorced heart, Wharton became other than he had ever seen him. The Pastor's Fire-side Vol. 3 of 4 |Jane Porter