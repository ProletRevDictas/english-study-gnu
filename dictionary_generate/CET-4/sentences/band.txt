The Solo bands are available as an option on the new Series 6 models or for purchase separately, at $50 for the rubber version and $100 for the braided band. Apple Watch Series 6 first impressions: A stretchy addition looks great |Aaron Pressman |September 17, 2020 |Fortune 

One way to explain the difference is that the zones of wide dark bands indicate torpor, and animals with milder winters didn’t need to enter that state. Ancient Lystrosaurus tusks may show the oldest signs of a hibernation-like state |Susan Milius |September 16, 2020 |Science News 

It’s more like a jazz band, one where all the musicians know the rules and how to play, but nothing is scripted or planned. What if Your Company Had No Rules? (Bonus Episode) |Maria Konnikova |September 12, 2020 |Freakonomics 

There is always a mix of music with a live band and a DJ, a full open bar, creative catering, and photo booths, plus an afterparty. How nonprofits are catering to millennials and rethinking the charity gala for younger generations |Rachel King |September 7, 2020 |Fortune 

An online concert held by the band in June was the world’s biggest paid online music event, drawing more than 750,000 viewers, the Yonhap News Agency reported. South Korean boy band BTS built an army. Now, they might get to defer military service |kdunn6 |September 4, 2020 |Fortune 

He plays an aging punk rocker and I play the drummer from his old band. Coffee Talk with Fred Armisen: On ‘Portlandia,’ Meeting Obama, and Taylor Swift’s Greatness |Marlow Stern |January 7, 2015 |DAILY BEAST 

The band turned back around, raising a lively tune to signal life would go on. Funeral Protest Is Too Much for NYPD Union Boss |Michael Daly |January 5, 2015 |DAILY BEAST 

The band was still on its way back as De Blasio and his wife departed. Funeral Protest Is Too Much for NYPD Union Boss |Michael Daly |January 5, 2015 |DAILY BEAST 

The last band I was in was kind of a Sonic Youth rip-off band, and I thought that that was my calling. Deer Tick's John McCauley on Ten Years in Rock and Roll |James Joiner |January 2, 2015 |DAILY BEAST 

Every other band I had been in had been pretty loud, you could never hear the vocals. Deer Tick's John McCauley on Ten Years in Rock and Roll |James Joiner |January 2, 2015 |DAILY BEAST 

His little band was almost immediately surrounded by the enemy. Blackwood's Edinburgh Magazine, No. CCCXXXIX. January, 1844. Vol. LV. |Various 

Conny stepped smilingly forward, and proceeded to affix the band around the vicar's massive throat. The Pit Town Coronet, Volume I (of 3) |Charles James Wills 

Roulard had played the trumpet in the regimental band in which Aristide had played the kettle drum. The Joyous Adventures of Aristide Pujol |William J. Locke 

There was a band playing down at Klein's hotel, and the strains reached them faintly, tempered by the distance. The Awakening and Selected Short Stories |Kate Chopin 

On this the royal band of music would strike up its liveliest airs, and a great bell would toll its evening warning. Our Little Korean Cousin |H. Lee M. Pike