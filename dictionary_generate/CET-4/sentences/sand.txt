Strong tidal currents there have since dumped several square miles worth of sand into the bay behind Fire Island, creating wide, shallow flats perfect for trying out the new platform. This motorized kayak can drive itself |By Nate Matthews/Outdoor Life |August 28, 2020 |Popular-Science 

Others are insisting the oil sands will remain the economic heart of the region for decades to come. The oil sands triggered a heated global debate on our energy needs. Now, they could be a sign of what’s to come |kdunn6 |August 20, 2020 |Fortune 

The coating allows moisture to stick and absorb to the sand. A Norwegian Startup Is Turning Dry Deserts Into Fertile Cropland |Vanessa Bates Ramirez |August 19, 2020 |Singularity Hub 

So we cross-registered in the animation department at the Massachusetts College of Art and Design twice in our sophomore year, learning things like hand drawing with a light table, experimental sand animation, and digital animation. Crafting our path |Tate Ryan-Mosley |August 19, 2020 |MIT Technology Review 

Throw a grain of sand in the machinery and we have an outage like yesterday. Google Search bug caused by issue with its ‘indexing systems’ |Barry Schwartz |August 11, 2020 |Search Engine Land 

There is only sand, a white ball, and a flag indicating the hole. Lost For Thousands of Strokes: 'Desert Golfing' Is 'Angry Birds' as Modern Art |Alec Kubas-Meyer |January 2, 2015 |DAILY BEAST 

When the game starts, there is only sand, a white ball, a flag indicating hole 1, and a “0” at the top of the screen. Lost For Thousands of Strokes: 'Desert Golfing' Is 'Angry Birds' as Modern Art |Alec Kubas-Meyer |January 2, 2015 |DAILY BEAST 

And there, the sand castle builder and tag player who loved her aunt more than science would be buried. 11 Children Shot in Milwaukee, One in Her Grandpa's Lap |Michael Daly |November 12, 2014 |DAILY BEAST 

Mr. Wynd said the shrinking process includes filling the head with hot sand and boiling it with herbs. Dodo Bones and Kylie’s Poo: Inside London’s Strangest New Museum |Liza Foreman |November 11, 2014 |DAILY BEAST 

The family had grown up dirt-poor, sharecropping the 20,000 acres of cotton that stretched out below Sand Mountain. Those Kansas City Blues: A Family History |Katie Baker |October 24, 2014 |DAILY BEAST 

Broken crocks should be strewn upon the tray, and on to this is heaped peaty soil mixed with sand. How to Know the Ferns |S. Leonard Bastin 

You speak with about as little reflection as we might expect from one of those children down there playing in the sand. The Awakening and Selected Short Stories |Kate Chopin 

Edna looked at her feet, and noticed the sand and slime between her brown toes. The Awakening and Selected Short Stories |Kate Chopin 

The little faces shone like polished bronze; they held their hands out, their bare feet pattered in the sand. The Wave |Algernon Blackwood 

Though built upon the sand, they still endured, and would continue to endure. The Wave |Algernon Blackwood