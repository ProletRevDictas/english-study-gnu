These sesame blossoms are inspired by that classic, as well as Syrian barazek, which you can find in many Middle Eastern bakeries, especially ones specializing in western Mediterranean cuisine. Tahini stands in for peanut butter in these nostalgic sesame blossoms |Kathryn Pauline |December 2, 2020 |Washington Post 

The right mix can help a cactus blossom, a peperomia propagate, or philodendron grow long enough to vine up a trellis. Indoor potting mixes for a thriving houseplant jungle |PopSci Commerce Team |September 24, 2020 |Popular-Science 

Within two or three weeks, the stalks swell into a multitude of meadow-grasses and blossoms as tall as my waist, fragrant and enveloping. What the Meadow Teaches Us - Issue 90: Something Green |Andreas Weber |September 16, 2020 |Nautilus 

Usually, when the moth smells the unaltered floral fragrance, it flies upwind and uses its long, skinny mouthparts to probe the tube the way that it would a blossom. This moth may outsmart smog by learning to like pollution-altered aromas |Carmen Drahl |September 11, 2020 |Science News 

Drones that blow pollen-laden bubbles onto blossoms could someday help farmers pollinate their crops. Bubble-blowing drones may one day aid artificial pollination |Maria Temming |June 22, 2020 |Science News 

Mayim Bialik and the cast of Blossom Charming family moment, or the beginning of a “Very Special Episode” of Blossom? The Most WTF Covers of ‘Baby, It’s Cold Outside,’ Everyone’s Favorite Date-Rape Holiday Classic |Kevin Fallon |November 19, 2014 |DAILY BEAST 

My finger burned when it touched the blossom of lead embedded in the ceramic armor. I Shot Bin Laden |Elliot Ackerman |November 16, 2014 |DAILY BEAST 

What Newman brought to the screen, what allowed him to blossom, was his ability to make Hud and Harper and Fast Eddie so familiar. The Stacks: The Eyes of Winter: Paul Newman at 70 |Peter Richmond |October 11, 2014 |DAILY BEAST 

An orange blossom of flame exploded on our screens as a new reality dawned. The Resilient City: New York After 9/11 |John Avlon |September 11, 2014 |DAILY BEAST 

Sinatra left for Tokyo in April 1962, perfect for cherry-blossom time. Frank Sinatra and the Birth of the Jet Set |William Stadiem |August 2, 2014 |DAILY BEAST 

When they shall rush in unto Jacob, Israel shall blossom and bud, and they shall fill the face of the world with seed. The Bible, Douay-Rheims Version |Various 

Her head, set off by her dainty white gown, suggested a rich, rare blossom. The Awakening and Selected Short Stories |Kate Chopin 

It will not long pause anywhere, and it easily leaves each blossom for a better. The Unsolved Riddle of Social Justice |Stephen Leacock 

He had lost the dream that Tony but tended a blossom, the fruit of which would come sweetly to his plucking afterwards. The Wave |Algernon Blackwood 

The leaf is small and light green, and it is quite (p. 384) a showy plant when in blossom. Tobacco; Its History, Varieties, Culture, Manufacture and Commerce |E. R. Billings.