There is a new and increasing level of acrimony, specifically directed at administrators. For Election Administrators, Death Threats Have Become Part of the Job |by Jessica Huseman |August 21, 2020 |ProPublica 

Those opposing same-sex marriage are on their heels, and increasingly unwilling or unable to make a stand against it. ‘Only God’ Can Stop Gay Marriage |Tim Mak |January 6, 2015 |DAILY BEAST 

Community policing is expensive and, in an era of budget cuts, increasingly rare. How to Solve the Policing Crisis |Keli Goff |January 5, 2015 |DAILY BEAST 

Though conversational and often witty, his meandering phrases become increasingly unpredictable as they develop. The Lost Novel of Nobel-Winner José Saramago |Charles Shafaieh |January 5, 2015 |DAILY BEAST 

And increasingly smart navigation aids in the cockpit brought far greater precision and efficiency to route planning. Flight 8501 Poses Question: Are Modern Jets Too Automated to Fly? |Clive Irving |January 4, 2015 |DAILY BEAST 

As the months passed and she began to cast the film, I became increasingly excited. Dr. King Goes to Hollywood: The Flawed History of ‘Selma’ |Gary May |January 2, 2015 |DAILY BEAST 

As a center for tourists, Inverness is increasingly popular and motor cars are very common. British Highways And Byways From A Motor Car |Thomas D. Murphy 

He had grown weary of an easy-going life, and the desire to start afresh made itself increasingly felt. The Life &amp; Letters of Peter Ilich Tchaikovsky |Modeste Tchaikovsky 

This rule has been revived in America, and appears to be increasingly relied on in bridge-designing. Encyclopaedia Britannica, 11th Edition, Volume 4, Part 3 |Various 

In spite of the superb sincerity of his indifference, he found it increasingly difficult to ignore his wife. The Creators |May Sinclair 

The towns grew increasingly jealous of extending their privileges, as these became valuable. The Influence and Development of English Gilds |Francis Aiden Hibbert