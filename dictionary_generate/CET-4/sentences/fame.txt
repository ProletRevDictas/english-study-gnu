Old comments from Justin Timberlake, Spears’s ex-boyfriend at the height of her fame, were among the many that surged back into public view. Britney Spears’s conservatorship is back in court — and back in the public eye |Ashley Fetters |February 11, 2021 |Washington Post 

Brown, 40, exploded into Internet fame last week when she posted her saga to TikTok. She used Gorilla Glue as hairspray. After 15 washes and a trip to the ER, it still won’t budge. |Lateshia Beachum |February 8, 2021 |Washington Post 

Travis Scott has been able to straddle the line between social media popularity and traditional fame better than virtually any celebrity of the moment, which doubtlessly contributed to the overall success of the McDonald’s campaign. Celebrities have always done endorsements. Now they sell their fast food orders. |Jameson Rich |February 5, 2021 |Vox 

Humiliated and disappointed by Spanish governance, he decided to seek his own wealth and fame in piracy. The Buccaneers embody Tampa’s love of pirates. Is that a problem? |Jamie Goodall |February 5, 2021 |Washington Post 

As Times critic-at-large Wesley Morris points out in the episode, Spears rose to fame during the Bill Clinton-Monica Lewinsky scandal, when young women’s sexual desires were being discussed in public at once frankly, pruriently and scornfully. Britney Spears and the trauma of being young, female and famous in the ’90s |Ashley Fetters |February 5, 2021 |Washington Post 

Her post-crown fame, though, only further begs the question: Why has there not been another Jewish Miss America since 1945? Why Was Bess Myerson the First and Last Jewish Miss America? |Emily Shire |January 7, 2015 |DAILY BEAST 

Tim Russert and I are driving back to the Albany airport after taking our kids to the baseball Hall of Fame in Cooperstown. Mario Cuomo, Always Moving Us Toward the Light |Mike Barnicle |January 4, 2015 |DAILY BEAST 

Any restaurant with a sustained fame ends up becoming a set, of sorts, and on that front, Sotto Sotto cinched it. The Fiery Death of Sotto Sotto, Toronto’s Celebrity Hotspot |Shinan Govani |December 30, 2014 |DAILY BEAST 

We might have thought The Comeback was about a desperate actress's shameless struggle for fame. ‘The Comeback’ Finale: Give Lisa Kudrow All of the Awards |Kevin Fallon |December 29, 2014 |DAILY BEAST 

She is using this technique, which generations of African-Americans have used for survival, for fame and profit. The Cultural Crimes of Iggy Azalea |Amy Zimmerman |December 29, 2014 |DAILY BEAST 

It was by popularizing tobacco in France that he gained a lasting fame. Tobacco; Its History, Varieties, Culture, Manufacture and Commerce |E. R. Billings. 

He achieved his highest fame from his connection with the revision of the statutes of New York. The Every Day Book of History and Chronology |Joel Munsell 

That first 'pinch' was its own priceless reward, far above present appreciation or future fame. Tobacco; Its History, Varieties, Culture, Manufacture and Commerce |E. R. Billings. 

We have heard the fame thereof, our hands grow feeble: anguish hath taken hold of us, as a woman in labour. The Bible, Douay-Rheims Version |Various 

As these lines are not in the original, the writer may have taken them from Chaucer's Hous of Fame, ll. Chaucer's Works, Volume 1 (of 7) -- Romaunt of the Rose; Minor Poems |Geoffrey Chaucer