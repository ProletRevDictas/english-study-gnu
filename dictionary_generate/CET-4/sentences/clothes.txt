Growing up as a teen in the 1960s, she had yearned to wear the same clothes her girlfriends wore. Inside A Finishing School for Transwomen |Sharon Adarlo |December 27, 2014 |DAILY BEAST 

Her daughter, Elaina, 24, a trained costume designer and makeup artist, helps out by sewing clothes. Inside A Finishing School for Transwomen |Sharon Adarlo |December 27, 2014 |DAILY BEAST 

BEIRUT—It is December, but Sabrine Omar is still wearing her summer clothes. A Sunni-Shia Love Story Imperiled by al Qaeda |Ruth Michaelson |December 26, 2014 |DAILY BEAST 

They dye their hair and alter their clothes, but not enough to attract attention from authorities. North Korea’s Secret Movie Bootleggers: How Western Films Make It Into the Hermit Kingdom |Lizzie Crocker |December 22, 2014 |DAILY BEAST 

In St. Louis, I believe, for a change of clothes and to go to a nightclub. Alfred Hitchcock’s Fade to Black: The Great Director’s Final Days |David Freeman |December 13, 2014 |DAILY BEAST 

At this same time they seized in Nangasaqui a servant of the father provincial, Matheo Couros, who was washing his clothes. The Philippine Islands, 1493-1898, Volume XX, 1621-1624 |Various 

Not a dollar did he possess—not even did he have a suit of clothes any more, and wore every day his corduroys. The Homesteader |Oscar Micheaux 

For six days Mamma wept at intervals, and showed the woman in black all Punch's clothes—a liberty which Punch resented. Kipling Stories and Poems Every Child Should Know, Book II |Rudyard Kipling 

Little boys when decking themselves out with tall hat and monstrously big clothes seem to be trying to put on an alarming aspect. Children's Ways |James Sully 

The door opened and a plain clothes detective entered the office. The Joyous Adventures of Aristide Pujol |William J. Locke