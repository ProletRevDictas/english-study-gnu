The victims ranged in age from 1 to 93 years old, were of all races and came from various backgrounds. Traffic counts fell during the coronavirus pandemic, but road fatalities still increased |Luz Lazo |February 12, 2021 |Washington Post 

Prosecutors allege Caldwell used his military and law enforcement background to plan violence — including possible snipers and weapons stashed on a boat along the Potomac River — weeks ahead of the Capitol insurrection. Former FBI official, a Navy veteran, is ‘key figure’ in Jan. 6 riot, prosecutors allege |Rachel Weiner, Spencer Hsu |February 11, 2021 |Washington Post 

He explains that, because of his background in chemistry, he was aware that any chemical compond can be broken down with the right solvent. Tessica Brown Successfully Removes Gorilla Glue Via Surgery |Jasmine Grant |February 11, 2021 |Essence.com 

The algorithm disregards irrelevant data, like types of backgrounds and model’s skin tones. Tech-savvy fashion forecasters already know what you’ll be wearing in two years |Rachael Zisk |February 10, 2021 |Popular-Science 

It’s not often that people are able to snap a picture of an urban cougar, but no mountain lion photo may be more iconic than the 2013 image of a male cougar, prowling at night with the Hollywood sign in the background. Four wild animals that are thriving in cities |By Ryan Chelius/Outdoor Life |February 9, 2021 |Popular-Science 

“We saw his background and he was a Bronx guy and we started breaking the case,” Boyce says. Shot Down During the NYPD Slowdown |Michael Daly |January 7, 2015 |DAILY BEAST 

The following page details a tribute gag the Simpsons team inserted into the background of a scene. Here’s the Lost Judd Apatow ‘Simpsons’ Episode, Penned by Judd Apatow |Asawin Suebsaeng |January 6, 2015 |DAILY BEAST 

But there's a ton of value for me in my background and my history, and losing it would be a shame. My Week on Jewish Tinder |Emily Shire |January 5, 2015 |DAILY BEAST 

Desert Golfing is the gaming equivalent of putting TV on in the background. Lost For Thousands of Strokes: 'Desert Golfing' Is 'Angry Birds' as Modern Art |Alec Kubas-Meyer |January 2, 2015 |DAILY BEAST 

But, but … there was a token black girl in the background, Target cried in its defense! One Vogue Cover Doesn’t Solve Fashion’s Big Race Problem |Danielle Belton |January 2, 2015 |DAILY BEAST 

There is always in the background of my mind dread lest help should reach the enemy before we have done with Sedd-el-Bahr. Gallipoli Diary, Volume I |Ian Hamilton 

But she was young enough and pretty enough to pay little heed to pose or background. The Red Year |Louis Tracy 

Against the blue background of the sky, green hill-tops trace an undulant line. Bastien Lepage |Fr. Crastre 

There was an exclamation of horror in the background, and Monsieur de Gaubert thrust himself forward. St. Martin's Summer |Rafael Sabatini 

The banks of the river are flat, and fringed with underwood and young trees; the background is formed by ranges of hills. A Woman's Journey Round the World |Ida Pfeiffer