The air quality in Portland has become the worst in the world — with Seattle, Los Angeles and Denver also ranking up there with notoriously polluted places like Delhi and Shanghai. Air quality in American West among the worst in the world |Jennifer A. Kingson |September 17, 2020 |Axios 

She noted that with the ongoing wildfires, there are certain communities who can’t shelter in place or evacuate their neighborhoods to escape the polluted air. Why fighting climate change is key to America’s health |Erika Fry |September 16, 2020 |Fortune 

The campaign said the ads will air in local markets in key states with early voting, including North Carolina, Florida, Georgia, Michigan, Minnesota, Wisconsin, Arizona and Pennsylvania. Trump, in town hall, says he wouldn’t have done anything differently on pandemic |Colby Itkowitz, Josh Dawsey, Felicia Sonmez, John Wagner |September 16, 2020 |Washington Post 

The original ad starring Boyega, 28, aired last year and was called “The London Gent.” John Boyega steps down from Jo Malone perfume ambassador role after being replaced in Chinese ad |radmarya |September 15, 2020 |Fortune 

I watched as towering plumes of smoke billowed from distant hills in all directions and air tankers crisscrossed the skies. Climate Change Will Force a New American Migration |by Abrahm Lustgarten, photography by Meridith Kohut |September 15, 2020 |ProPublica 

And Air Force assessors are the first to say such imaging never tells the whole story. Pentagon Doesn’t Know How Many People It’s Killed in the ISIS War |Nancy A. Youssef |January 7, 2015 |DAILY BEAST 

Sprawled on chaise lounges with their knees high in the air and their legs spread wide. Powerful Congressman Writes About ‘Fleshy Breasts’ |Asawin Suebsaeng |January 7, 2015 |DAILY BEAST 

Indeed, Lion Air, with 45 percent of the domestic Indonesian airline market, has swallowed the Fernandes formula whole. Annoying Airport Delays Might Prevent You From Becoming the Next AirAsia 8501 |Clive Irving |January 6, 2015 |DAILY BEAST 

There is a larger reason, beyond the airlines themselves, why Lion Air and 61 other Indonesian airlines are on this black list. Annoying Airport Delays Might Prevent You From Becoming the Next AirAsia 8501 |Clive Irving |January 6, 2015 |DAILY BEAST 

The Pentagon said Faal served in the Air Force for seven years, during which time he became a U.S. citizen. The Shadowy U.S. Veteran Who Tried to Overthrow a Country |Jacob Siegel |January 6, 2015 |DAILY BEAST 

Bells were pealing and tolling in all directions, and the air was filled with the sound of distant shouts and cries. Davy and The Goblin |Charles E. Carryl 

It is to be remembered, however, that a few of these bacteria may reach the sputum from the upper air-passages. A Manual of Clinical Diagnosis |James Campbell Todd 

We had now approached closely to the foot of the mountain-ranges, and their lofty summits were high above us in mid-air. Blackwood's Edinburgh Magazine, Volume 60, No. 372, October 1846 |Various 

He stood, with the air of a hero, both arms extended towards the amazed pair of lovers. The Joyous Adventures of Aristide Pujol |William J. Locke 

Their method of curing the leaves was to air-dry them and then packing them until wanted for use. Tobacco; Its History, Varieties, Culture, Manufacture and Commerce |E. R. Billings.