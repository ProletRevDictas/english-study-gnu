For every 50 degrees Centigrade, you change the chemical reaction rate by a factor of two. Japan Nuclear Crisis: What Is a Full Meltdown? |Josh Dzieza |March 15, 2011 |DAILY BEAST 
A Calorie is the amount of heat required to raise the temperature of one kilogram of water from zero to one degree Centigrade. A Civic Biology |George William Hunter 
A centigrade thermometer, sunk two and a half inches in the earth, at noon, marked 56. The Desert World |Arthur Mangin 
I don't think I'd care to walk into a hydrogen atmosphere at three hundred Centigrade. The Bramble Bush |Gordon Randall Garrett 
All I did was wait until the temperature got above three fifty-seven Centigrade—above the boiling point of mercury. The Bramble Bush |Gordon Randall Garrett 
Slightly disappointed that there were only five degrees of frost (Centigrade) I returned to the lounge. Once a Week |Alan Alexander Milne