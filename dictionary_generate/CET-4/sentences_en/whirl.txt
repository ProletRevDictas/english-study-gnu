Viewers are surrounded by a whirl of images in which it’s impossible to distinguish real from unreal. This NFT Painting Is a Work of Art - Issue 104: Harmony |Arthur I. Miller |August 18, 2021 |Nautilus 
It was a giant fire whirl with winds over 100 miles per hour. What’s Fueling Today’s Extreme Fires - Issue 104: Harmony |Dan Falk |August 4, 2021 |Nautilus 
I think within the next 10 years, we ought to be able to predict where fire whirls are likely to form. What’s Fueling Today’s Extreme Fires - Issue 104: Harmony |Dan Falk |August 4, 2021 |Nautilus 
Cambage is a whirl of drop-steps, shoulder shoves and jump hooks, her 6-foot-8 frame affording her whatever space she requires. A’ja Wilson had a rebirth, Liz Cambage is healthy, and Las Vegas is among the WNBA favorites |Robert O'Connell |May 13, 2021 |Washington Post 
It all makes sense in the whirl of Ide’s fate-driven universe. Joe Ide’s IQ series continues with the idiosyncratic marvel ‘Smoke’ |Maureen Corrigan |February 26, 2021 |Washington Post 
The dying are deceived by the chemical whirl of “a dying brain.” Eben Alexander Has a GPS for Heaven |Patricia Pearson |October 8, 2014 |DAILY BEAST 
I never lifted a brush before, I never mixed a paint, so I gave it a whirl. Dubya’s Portraits of Tony Blair and Vladimir Putin Are Just as Genius as You Hoped |Ann Binlot |April 4, 2014 |DAILY BEAST 
A whirl of activity on and off the slopes, Kathy heads the local chapter of Disabled Sports, Eastern Sierra region. Homefront Veterans: Skiing With Wounded Warriors |John Kael Weston |February 17, 2014 |DAILY BEAST 
Ted Widmer on the whirl of celebrity and policy that dance across the pages. The Man with the President’s Ear, Arthur Schlesinger Jr. and JFK |Ted Widmer |October 27, 2013 |DAILY BEAST 
Sajed fearlessly slapped on the rollerblades to give them a whirl, and a skater was born. If You Build It, They Will Skate |Maysoon Zayid |July 17, 2013 |DAILY BEAST 
Then dawn flung itself impetuously across the hills, and the naked rim of the canyon took form in a shifting whirl of smoke. Raw Gold |Bertrand W. Sinclair 
They are, however, much less energetic, and often of greater size than the hurricane whirl. Outlines of the Earth's History |Nathaniel Southgate Shaler 
They could revel in the rugged measures of ‘Marmion,’ in the whirl and clatter of the ‘Last Minstrel.’ A Cursory History of Swearing |Julian Sharman 
Fragments were spun off the whirl of people, bits of BSG uniforms torn off their wearers and tossed like confetti. The Great Potlatch Riots |Allen Kim Lang 
Her life at this time was a whirl of excitement—excitement of the keenest order—namely, trying on. With Edged Tools |Henry Seton Merriman