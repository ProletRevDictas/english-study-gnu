Early-decision and early-action programs are used by many of the most popular and toughest-to-get-into colleges in the country. College Early Admission Is Tougher Than Ever |Steve Cohen |December 19, 2011 |DAILY BEAST 
At times like these you must be careful not to brim over with elation-into-crashing-despair. What the Stars Predict for Your Week |Starsky + Cox |August 20, 2011 |DAILY BEAST 
It presents itself to us as an effective corroboration of the so well-known phenomenon of talking-yourself-into-it. Criminal Psychology |Hans Gross 
Breton peasants are represented playing on Breton pipes in the Entry-into-Jerusalem scene. How France Built Her Cathedrals |Elizabeth Boyle O'Reilly 
"I w-w-was b-b-b-blown i-i-i-into t-t-t-the air," he replied, smiling sweetly. Fanny Goes to War |Pat Beauchamp 
No sooner had they disappeared than Siegfried came into-393- the wood, armed for the hunt. Operas Every Child Should Know |Mary Schell Hoke Bacon 
Bess went to put on her bonnet, and when she camc into-the parlour George backed into the fireplace with astonishment. Rogues and Vagabonds |George R. Sims