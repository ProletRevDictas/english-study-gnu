The community decided to lean into the moniker, referring to their football team as the Arabian Knights. Inside One Combat Vet's Journey From Defending His Country to Storming the Capitol |W.J. Hennigan/Washington |July 9, 2021 |Time 
As the Arabian Peninsula flourished, the area became the center of cultural development. When Saudi Arabia Ruled the World |Emily Wilson |October 31, 2014 |DAILY BEAST 
A Saudi Arabian television ad for Viagra shows a man struggling to push a straw through the lid of his beverage. Laughter Will Be the Legacy of Viagra |Samantha Allen |October 2, 2014 |DAILY BEAST 
The Saudi Arabian government paid for her tuition in addition to a $1,800 stipend for personal expenses. Saudi Beauty Says She Robbed Banks for Her Mafia Lover |Caitlin Dickson |May 10, 2014 |DAILY BEAST 
So was Nasir al Wuhayshi, the Yemeni firebrand who runs al Qaeda in the Arabian Peninsula. How Al Qaeda Escaped Afghanistan and Lived to Fight Another Day |Yaniv Barzilai |March 16, 2014 |DAILY BEAST 
Dusty paths lined with flickering orange lanterns led us to our final camp, a slice of dramatic Arabian luxury. On Foot in the High Atlas Mountains of Morocco |Joanna Eede |January 22, 2014 |DAILY BEAST 
The apocryphal gospels contain many, and some are preserved by Persian and Arabian poets. Solomon and Solomonic Literature |Moncure Daniel Conway 
To the girls, that jolting ride was like an adventure straight from the Arabian Nights. The Outdoor Girls in the Saddle |Laura Lee Hope 
A short distance from the station lay a true Arabian sand desert, but which was fortunately not of very great extent. A Woman's Journey Round the World |Ida Pfeiffer 
Thus the Phoenicians and the Amorites belong to the first stage of the second great Arabian migration. Encyclopaedia Britannica, 11th Edition, Volume 5, Slice 2 |Various 
I was warned secretly by a strange Arabian woman, who required of me solemn oath not to reveal her. God Wills It! |William Stearns Davis