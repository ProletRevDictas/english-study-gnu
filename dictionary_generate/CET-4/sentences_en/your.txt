These pathetic folks need to accept that “jazz has replaced classical music as the dreaded incarnation of eat-your-broccoli art.” What’s With This Uncool Surge in Jazz Bashing? |Ted Gioia |November 2, 2014 |DAILY BEAST 
Dessert is a slice of melt-in-your-mouth treacle tart with a dollop of perfectly tart clotted cream. Join The Mile High (Dining) Club |Allison McNearney |September 26, 2014 |DAILY BEAST 
He was like my old man with that angry, in-your-face rhetoric. This Man Is The Future of Westboro Baptist Church |Caitlin Dickson |March 24, 2014 |DAILY BEAST 
He continues that "the gays I know are not the flamboyant shove-it-in-your face type of people." "Why I Called Lindsey Graham 'Ambiguously Gay'" |Olivia Nuzzi |March 14, 2014 |DAILY BEAST 
Most of the posts—featuring kind, thoughtful, restores-your-faith-in-humanity type of statements—align with the lofty statement. Ashton Kutcher Is Investing in the Anonymous Confessional App Secret |Abby Haglage |March 14, 2014 |DAILY BEAST 
Stretch-your-necks, wags and grind hunters, supplied Jerry, now sufficiently aroused to join in the conversation. Marjorie Dean College Freshman |Pauline Lester 
He had done rough work in Central Asia, and had seen rather more help-your-self fighting than most men of his years. Soldier Stories |Rudyard Kipling 
Sat Bhai has many members, and perhaps before they jolly-well-cut-your-throat they may give you just a chance for life. Kim |Rudyard Kipling 
Opposite is a little, hold-your-own school-mistressy young person in pince-nez. Sea and Sardinia |D. H. Lawrence 
Such was the difference between the costume and arms of Master Rend-your-Soul and that of his servant. A Romance of the West Indies |Eugne Sue