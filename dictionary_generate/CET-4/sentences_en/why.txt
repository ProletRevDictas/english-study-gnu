I mean, even Ron Fournier of National Journal, usually devoted to the pox-on-both-houses, why-can't-Obama-lead? Numbers That Speak for Themselves |Michael Tomasky |October 1, 2013 |DAILY BEAST 
DB: Why/how does Credit Suisse feel like a good fit for you? Industry Trailblazer to Aspiring Women Pros: Stand Up and Be Counted |Daily Beast Promotions |November 14, 2011 |DAILY BEAST 
Ferguson reverses those emphases—focusing solely on this historic meltdown, the who/what/where/why/how. Inside Job: Hard Lessons Through Pretty Pictures |Randall Lane |October 15, 2010 |DAILY BEAST 
Till now one with sudden hiss: "But-good Christ-just look-why, the roof's leaning—!" The Lord of the Sea |M. P. Shiel 
And what is it that brings disaster on those who employ the phalanx?Why the phalanx fails. The Histories of Polybius, Vol. II (of 2) |Polybius 
It needed a beefy person with fat legs and a large amount of inexplicable dignity, a regular God-knows-why loftiness. The O'Ruddy |Stephen Crane 
Weren't they as good as gold this morning, Emily?and yesterday!why they never murmured, as good as gold they were. The White Peacock |D. H. (David Herbert) Lawrence 
Like other sharp children, Why-Why was always asking metaphysical conundrums. In the Wrong Paradise |Andrew Lang