For years, the top complaint about the product was that people lost the little red straw that came with each can. 20 ingenious uses for WD-40 |By Bill Heavey/Field & Stream |February 11, 2021 |Popular-Science 
They’re like high-end veggie straws you get in the snack aisle. 5 Delicious Snacks You Can Forage in the City |Vanessa Hua |February 7, 2021 |Outside Online 
For a surface with boundary, such as a straw with its two boundary circles, each cut must begin and end on a boundary. Topology 101: The Hole Truth |David S. Richeson |January 26, 2021 |Quanta Magazine 
She was a tough act to follow, and the guy who drew the short straw was Garth Brooks. Inauguration Musical Performances Are Tricky. But Lady Gaga, Jennifer Lopez and Garth Brooks Did Exactly What We Needed Them to Do |Stephanie Zacharek |January 20, 2021 |Time 
Such defiance from the tech giants was a final straw for Museveni. Authoritarians have a love-hate relationship with social media |Yinka Adegoke |January 16, 2021 |Quartz 
Most of it is taken up by a graphic inviting the visitor to participate in the 2016 online presidential straw poll. Today’s GOP: Still Cool With Racist Pandering? |Michael Tomasky |January 7, 2015 |DAILY BEAST 
Exactly one month after the first straw goat was erected in Gävle, it was mysteriously burned to a crisp. Sweden’s Burning Christmas Goat |Nina Strochlic |December 25, 2014 |DAILY BEAST 
I thanked him, sat down on the sofa, and sipped it through the straw. I Was Gang Raped at a UVA Frat 30 Years Ago, and No One Did Anything |Liz Seccuro |December 16, 2014 |DAILY BEAST 
Hana seeks refuge from the buzzing lights of Otome Road in a nearby café and makes another swirl with her straw. The Japanese Women Who Love Gay Anime |Brandon Presser |December 6, 2014 |DAILY BEAST 
She slowly moves her straw through the whipped cream in her designer latte and looks up. The Japanese Women Who Love Gay Anime |Brandon Presser |December 6, 2014 |DAILY BEAST 
Poor Squinty ran and tried to hide under the straw, for he knew the boy was talking about him. Squinty the Comical Pig |Richard Barnum 
He reached up for her big, rough straw hat that hung on a peg outside the door, and put it on her head. The Awakening and Selected Short Stories |Kate Chopin 
In fact, so much of her smooth brow as could be seen under a broad-brimmed straw hat was wrinkled in a decided frown. The Red Year |Louis Tracy 
He put on his big straw hat, and taking his umbrella from the stand in the hall, followed the lady in black, never overtaking her. The Awakening and Selected Short Stories |Kate Chopin 
The clear, straw-colored fluid which is left after separation of the coagulum is called blood-serum. A Manual of Clinical Diagnosis |James Campbell Todd