The cheers that went up when a fireman was taken from a window, placed on a gurney and rolled away. Sept. 11, 2001: An ordinary work day, then surreal scenes of dread and death |David Maraniss |September 10, 2021 |Washington Post 
A fireman came in on a stretcher, an older man, wearing the uniform of a unit from Jersey City across the river. Sept. 11, 2001: An ordinary work day, then surreal scenes of dread and death |David Maraniss |September 10, 2021 |Washington Post 
He didn’t have typical kid dreams—of growing up to become a fireman, doctor, or movie star. He Robbed a Taco Joint With a Toy Water Gun for $264. He Got Life in Prison. |Kate Briquelet |May 31, 2021 |The Daily Beast 
Eventually, he leaves the dog in the care of another fireman and we go back, discussing the fire and the dog. Eight Year Old Rides to a Blaze With His Firefighter Grandfather |Michael Daly |January 19, 2014 |DAILY BEAST 
In fact, he would survive it, working as a volunteer fireman in London during the blitz. Marco Roth’s Book Bag: The Anti-Memoir Memoir |Marco Roth |October 1, 2013 |DAILY BEAST 
The backup-man stops stroller traffic on the sidewalk, another fireman is road guard. With the Fireman of Brooklyn’s Company 224 as They Observe the Fallen |Maurice Emerson Decaul |September 12, 2013 |DAILY BEAST 
My cousin Al is a Navy veteran, a former EMT, and now a fireman in New York City. The Marine and His Cousin the Firefighter |Maurice Emerson Decaul |September 11, 2013 |DAILY BEAST 
He wanted to go into the Army and then be a police officer and then be a fireman. Curse the Media in Newtown for Doing Too Little, Too Late on Guns |Michael Daly |December 20, 2012 |DAILY BEAST 
The engineer and fireman stood leaning out over the closed lower half of the engine-room door. Uncle Sam's Boys as Lieutenants |H. Irving Hancock 
Again the fireman reflected, for there was nothing hasty about this excellent creature. The Works of Robert Louis Stevenson, Volume XXI |Robert Louis Stevenson 
Under Mr. Van Britt's directions the engineer and fireman of the pilot engine brought tools and the break was repaired. The Wreckers |Francis Lynde 
The fireman—'e's a real 'andsome man—I can tike to that sort myself. Sue, A Little Heroine |L. T. Meade 
But the driver and fireman were beyond the range of their skill. The Story of the Cambrian |C. P. Gasquoine