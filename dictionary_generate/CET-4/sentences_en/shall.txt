Who knew that “we shall overcome” meant “we, the few, shall book covers every decade or so, maybe, sometimes, if we are in style.” One Vogue Cover Doesn’t Solve Fashion’s Big Race Problem |Danielle Belton |January 2, 2015 |DAILY BEAST 
Blessed are the peacemakers, for they shall be called the children of God. Cop Families Boo De Blasio at NYPD Graduation |Michael Daly |December 30, 2014 |DAILY BEAST 
But time and history will render an unambiguous verdict on this matter, as Rubio shall soon see. Rubio’s Embargo Anger Plays to the Past |Michael Tomasky |December 19, 2014 |DAILY BEAST 
But alas, a snub is yet another of the many indignities Valerie Cherish shall endure. 15 Enraging Golden Globe TV Snubs and Surprises: Amy Poehler, 'Mad Men' & More |Kevin Fallon |December 11, 2014 |DAILY BEAST 
It demands only that judges “shall hold their Offices during good Behaviour.” Justice Ginsburg Shouldn’t Quit Just Yet |Kevin Bleyer |December 1, 2014 |DAILY BEAST 
A wise man hateth not the commandments and justices, and he shall not be dashed in pieces as a ship in a storm. The Bible, Douay-Rheims Version |Various 
He that seeketh the law, shall be filled with it: and he that dealeth deceitfully, shall meet with a stumblingblock therein. The Bible, Douay-Rheims Version |Various 
It seems very strange that I shall actually know Liszt at last, after hearing of him so many years. Music-Study in Germany |Amy Fay 
Now first we shall want our pupil to understand, speak, read and write the mother tongue well. The Salvaging Of Civilisation |H. G. (Herbert George) Wells 
We shall recover again some or all of the steadfastness and dignity of the old religious life. The Salvaging Of Civilisation |H. G. (Herbert George) Wells