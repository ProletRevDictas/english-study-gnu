They began the process the same way, by removing sericin from silk fibers. Silk can be molded into strong medical implants |Sid Perkins |April 2, 2020 |Science News For Students 
Doctors like silk for a variety of reasons, says David Kaplan. Silk can be molded into strong medical implants |Sid Perkins |April 2, 2020 |Science News For Students 
All that water adds a lot of weight and volume to the silk materials as they’re stored. Silk can be molded into strong medical implants |Sid Perkins |April 2, 2020 |Science News For Students 
They’re actually the caterpillars of the domestic silk moth, Bombyx mori. Silk can be molded into strong medical implants |Sid Perkins |April 2, 2020 |Science News For Students 
The type of silk that’s most useful to people comes from what are often called silkworms. Silk can be molded into strong medical implants |Sid Perkins |April 2, 2020 |Science News For Students 
Poking out of the shiny gold pages is a “distinctive silk marker”—also gold—which “complements the color of the leather.” Rand Paul’s Many Leather-Bound Books |Olivia Nuzzi |November 27, 2014 |DAILY BEAST 
Sometimes I wear my silk pyjamas when I am going for a walk in the mornings, does that make me eccentric? The Death of the English Eccentric |Tom Sykes |November 25, 2014 |DAILY BEAST 
Behind their silk hats loom shadows of their immigrant forbears. The Magazine That Made—and Unmade—Politicians |Anthony Haden-Guest |November 2, 2014 |DAILY BEAST 
Waving a silk cloth, he declared, “Gentlemen, I will have this land just as surely as I now have this handkerchief.” Washington’s Wheeler-Dealer Patriotism |William O’Connor |October 31, 2014 |DAILY BEAST 
Undercover detective Gustav Frank sold Mandelbaum several bolts of stolen silk that had been secretly marked. Meet 'The Queen of Thieves' Marm Mandelbaum, New York City's First Mob Boss |J. North Conway |September 7, 2014 |DAILY BEAST 
Q was a Queen, who wore a silk slip; R was a Robber, and wanted a whip. Boys and Girls Bookshelf; a Practical Plan of Character Building, Volume I (of 17) |Various 
He urged the growing of mulberry trees and the propagation of silk worms, as being of more value than tobacco. Tobacco; Its History, Varieties, Culture, Manufacture and Commerce |E. R. Billings. 
She had been properly dressed for the occasion in black tulle and black silk tights. The Awakening and Selected Short Stories |Kate Chopin 
A chair covered with red silk, borne on the shoulders of sixteen chair-men, passed up to the temple. Our Little Korean Cousin |H. Lee M. Pike 
Instead of a cloth, on each table was a sheet of fine glazed paper which had the appearance of oiled silk. Our Little Korean Cousin |H. Lee M. Pike