A fourth suspect, a 26-year-old woman named Hayat Boumeddiene, remains at large. France Kills Charlie Hebdo Murderers |Nico Hines |January 9, 2015 |DAILY BEAST 
So, Islamized teaching sends girls back home for marriage and housework, and remains exclusively for boys. Houellebecq’s Incendiary Novel Imagines France With a Muslim President |Pierre Assouline |January 9, 2015 |DAILY BEAST 
But he, like many people using dating apps whatever their sexual identity, remains stoutly positive. Grindr’s Trans Dating Problem |David Levesley |January 9, 2015 |DAILY BEAST 
The eating disorder field remains divided over the potential efficacy of such measures. How Skinny Is Too Skinny? Israel Bans ‘Underweight’ Models |Carrie Arnold |January 8, 2015 |DAILY BEAST 
But how much they have regained or how durable their hold is remains unclear. Pentagon Doesn’t Know How Many People It’s Killed in the ISIS War |Nancy A. Youssef |January 7, 2015 |DAILY BEAST 
The plant as a whole remains green until late in the autumn. How to Know the Ferns |S. Leonard Bastin 
The interest of the story is now at an end; but much yet remains before the conclusion. Blackwood's Edinburgh Magazine, No. CCCXXXIX. January, 1844. Vol. LV. |Various 
They speak of a certain Norumbega and give the names of cities and strongholds of which to-day no trace or even report remains. The Jesuit Relations and Allied Documents, Vol. II: Acadia, 1612-1614 |Various 
The plan, however, remains in use to this day in many mines, and is frequently spoken of under the name of quartz-crusher. Life of Richard Trevithick, Volume II (of 2) |Francis Trevithick 
And yet there still remains a superstitious belief in prayer, and most surprising are some of its manifestations. God and my Neighbour |Robert Blatchford