The video shows the back screen keeping up with the front screen as the user navigates around in a few apps. New Xiaomi smartphone has an extra screen in… uh, the camera bump? |Ron Amadeo |February 12, 2021 |Ars Technica 
A team member put the cup onto the display screen lying on the table and a concept was born. Ford’s electric Mustang Mach-E is an important leap into the future |Dan Carney |February 12, 2021 |Popular-Science 
On screen, an elevator can force people to get close or, when the doors open, to separate suddenly. What floor? |Sophia Nguyen |February 12, 2021 |Washington Post 
I posted screen shots of this over here, here is how they compare. Google Search dark theme mode expands but search ads are hard to distinguish |Barry Schwartz |February 11, 2021 |Search Engine Land 
More than ever, the little screen is where things are happening. The loneliness of an interrupted adolescence |Ellen McCarthy |February 11, 2021 |Washington Post 
The garrulous assistant to a fading screen siren in Clouds of Sils Maria. Oscars 2015: The Daily Beast’s Picks, From Scarlett Johansson to ‘Boyhood’ |Marlow Stern |January 6, 2015 |DAILY BEAST 
“JSwipe is currently under heavy load,” flashed across the screen, one night as a friend and I looked at it. My Week on Jewish Tinder |Emily Shire |January 5, 2015 |DAILY BEAST 
A sad-faced orange Star of David flashed across the iPhone screen as we swiped left on “James” (not his real name). My Week on Jewish Tinder |Emily Shire |January 5, 2015 |DAILY BEAST 
Her name was Courtney, and she was a fashion editor for magazines like Photoplay, Screenland, Silver Screen. The Story Behind Lee Marvin’s Liberty Valance Smile |Robert Ward |January 3, 2015 |DAILY BEAST 
A more rugged version of American masculinity is hard to find on screen. The Story Behind Lee Marvin’s Liberty Valance Smile |Robert Ward |January 3, 2015 |DAILY BEAST 
Among the Perpendicular additions to the church last named may be noted a very beautiful oaken rood-screen. Encyclopaedia Britannica, 11th Edition, Volume 3, Slice 4 |Various 
The fire had been heaped over with earth—to screen it from prying eyes, I suppose, while the good work went on. Raw Gold |Bertrand W. Sinclair 
Now, he chose a small table in a corner of the balcony, close to the glass screen. Rosemary in Search of a Father |C. N. Williamson 
According to a weekly paper not only is Constance Binney a famous screen star, but she is also a first-class ukelele player. Punch, or the London Charivari, Volume 158, April 28, 1920 |Various 
It was ten minutes before she raised her hand and pointed to a wilted but still effective screen. Ancestors |Gertrude Atherton