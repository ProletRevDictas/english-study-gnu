Black and white were now mixing sufficiently to make a Klan member weep. How Rock and Roll Killed Jim Crow |Dennis McNally |October 26, 2014 |DAILY BEAST 
She did not weep on cue in public when Monteith died, or seek sympathy. Why Does Everyone Hate Lea Michele? |Tim Teeman |October 9, 2014 |DAILY BEAST 
“For all the victims of the mindless wars, in every age, humanity needs to weep,” he said. Pope Francis vs. The Warmongers |Barbie Latza Nadeau |September 13, 2014 |DAILY BEAST 
World leaders, and we, should look on them, be sickened, weep, and maybe finally learn. To Truly Shame Putin, Show Us the Bodies of MH17 |Tim Teeman |July 22, 2014 |DAILY BEAST 
“A typical parent would weep in this situation, but they showed no emotion,” he remembers in the documentary. ‘Love Child’ Game Over: Internet Addicts Let Their Baby Starve to Death |Nina Strochlic |July 21, 2014 |DAILY BEAST 
And for fear of being ill spoken of weep bitterly for a day, and then comfort thyself in thy sadness. The Bible, Douay-Rheims Version |Various 
Behold they that see shall cry without, the angels of peace shall weep bitterly. The Bible, Douay-Rheims Version |Various 
Lyn was no chicken-hearted weakling, to sit down and weep unavailingly in time of peril. Raw Gold |Bertrand W. Sinclair 
Pau longed to weep with her, and in his efforts to console, he addressed her with rather vague remarks. Honey-Bee |Anatole France 
Oliver felt stunned and stupified by the unexpected intelligence; he could not weep, or speak, or rest. Oliver Twist, Vol. II (of 3) |Charles Dickens