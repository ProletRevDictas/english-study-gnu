This topological understanding of Euler’s formula — in which the shapes were rubber-like and not rigid — was first presented in an article by Johann Listing in 1861. Topology 101: The Hole Truth |David S. Richeson |January 26, 2021 |Quanta Magazine 
Different variations of Baffins and Sorels—think rubber bottom, synthetic top—boots are popular, good with moisture, and more lightweight, but generally not as effective in extreme cold as the bunny boots. Dress like an Alaskan to weather the winter cold |By Tyler Freel/Outdoor Life |January 20, 2021 |Popular-Science 
They have Clay and Harper, a rubber arm fit to swing between the majors and minors. The Nationals’ bullpen doesn’t need a makeover. But it could use another arm or two. |Jesse Dougherty |January 14, 2021 |Washington Post 
Once you pour your batter into the pan, smooth its top with a rubber spatula. Four tips for bake-off worthy cakes |By Saveur |January 12, 2021 |Popular-Science 
Lightly grease a rubber spatula with oil and use it to transfer the marshmallow mixture to the greased pan, spreading it evenly but quickly, as it will start to set. Squishy, sweet homemade marshmallows will elevate your s’mores and hot chocolate |Daniela Galarza |January 12, 2021 |Washington Post 
These villages used to harvest rubber, cacao, palm oil, and coffee beans. The Congo's Forgotten Colonial Getaway |Nina Strochlic |December 18, 2014 |DAILY BEAST 
He spent three days in a rubber room wearing a plastic smock before returning. A Million Ways to Die in Prison |Daniel Genis |December 8, 2014 |DAILY BEAST 
With a rubber spatula, stir in the chocolate and cranberries until the dough is well mixed. Make These Barefoot Contessa Salty Oatmeal Chocolate Chunk Cookies |Ina Garten |November 28, 2014 |DAILY BEAST 
Soon his coffers were overflowing with revenue from rubber, palm oil, and ivory. ‘Argo’ in the Congo: The Ghosts of the Stanleyville Hostage Crisis |Nina Strochlic |November 23, 2014 |DAILY BEAST 
I pressed the dime-sized rubber button on my vest, which was linked to my radio. I Shot Bin Laden |Elliot Ackerman |November 16, 2014 |DAILY BEAST 
A special quality of glove made of asbestos cloth, lined with rubber, is supplied for electric light work. Asbestos |Robert H. Jones 
The garden walks were damp, and Edna called to the maid to bring out her rubber sandals. The Awakening and Selected Short Stories |Kate Chopin 
Mackintoshes, vulcanized india-rubber, gutta-percha, and gossamer dust-coats unknown then. The Portsmouth Road and Its Tributaries |Charles G. Harper 
But "son" had rebounded from the impact like a rubber ball, or the best trained gymnast of his school, as he was. Dorothy at Skyrie |Evelyn Raymond 
To-day they wore light covert coats over their canvas and rubber. Ancestors |Gertrude Atherton