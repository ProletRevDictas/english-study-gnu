As Democrats mutter privately that their Senate majority is sinking beneath the waves, their leadership has sent out an SOS. The Only Way for Democrats to Win |Jonathan Alter |October 24, 2014 |DAILY BEAST 
But when you do so, make sure to mutter the appropriate things about poverty, “the empire,” and the scourge of “neoliberalism.” The Stupidest Hugo Chávez Hagiographies From the Yanquis Who Loved Him |Michael Moynihan |March 7, 2013 |DAILY BEAST 
The theory here seems to be that to mutter about the jews off the record would be perfectly fine. Who's Afraid of 'the Israel Lobby'? |David Frum |February 2, 2013 |DAILY BEAST 
In Germany, die Mutter has been known to graduate from a promising career to welfare to stay with her children. The Book for Angry Moms |Eric Pape |March 29, 2010 |DAILY BEAST 
He fired with an accuracy of aim that won him an admiring mutter, although to miss would have been almost as noteworthy. Ancestors |Gertrude Atherton 
"It's not Joan I've killed at any rate," I heard him mutter as she turned and opened her eyes and smiled faintly up in his face. Three More John Silence Stories |Algernon Blackwood 
For all that, an occasional mutter came unheeded to his ears, the closed curtains preserving articulate sounds like room walls. Cabin Fever |B. M. Bower 
But Richard would cross himself and mutter prayers, calling on every saint to fight against the assailing devils. God Wills It! |William Stearns Davis 
And he in turn told a story that made men cross themselves and mutter their Glorias. God Wills It! |William Stearns Davis