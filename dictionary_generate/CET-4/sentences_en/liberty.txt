As the public school we are not at liberty to provide that information. The Learning Curve: San Diego Unified Is Terrified of Kids Opting Out |Will Huntsberry |September 10, 2020 |Voice of San Diego 
Women and men who believed so fiercely in the promise of equality, liberty, and justice for all. Why Kamala Harris’s Big Night was bittersweet |Claire Zillman, reporter |August 20, 2020 |Fortune 
The pandemic has accelerated this attrition of individual liberties. Covid-19 and the geopolitics of American decline |Katie McLean |August 19, 2020 |MIT Technology Review 
The challenge has always been to balance our national security needs while protecting civil liberties. Congressional Dems: New Surveillance Bill Strikes Right Balance |Jesse Marx |June 22, 2020 |Voice of San Diego 
Minority groups have, for the most part, gone along with restrictions on their liberty for the sake of the health of all. Epidemics Have Often Led To Discrimination Against Minorities – This Time Is No Different |LGBTQ-Editor |June 9, 2020 |No Straight News 
At this point Marvin gives his Liberty Valance smile, the kind that makes you wish you could disintegrate in front of him. The Story Behind Lee Marvin’s Liberty Valance Smile |Robert Ward |January 3, 2015 |DAILY BEAST 
Finally, Van Cleef and Martin realize Liberty is going too far. The Story Behind Lee Marvin’s Liberty Valance Smile |Robert Ward |January 3, 2015 |DAILY BEAST 
The first day of Liberty, I was hanging around waiting for Ford to come in. The Story Behind Lee Marvin’s Liberty Valance Smile |Robert Ward |January 3, 2015 |DAILY BEAST 
But Liberty is always dipping his shoulder, whirling around. The Story Behind Lee Marvin’s Liberty Valance Smile |Robert Ward |January 3, 2015 |DAILY BEAST 
We are looking forward to working closely with this champion of liberty. Vaclav Klaus, Libertarian Hero, Has His Wings Clipped by Cato Institute |James Kirchick |December 22, 2014 |DAILY BEAST 
Whether advocates and orators had liberty to plead in causes, manifestly known to be unjust, vexatious, or oppressive? Gulliver's Travels |Jonathan Swift 
He worketh under correction, and seeketh to rest: let his hands be idle, and he seeketh liberty. The Bible, Douay-Rheims Version |Various 
It is a further refinement when the staunch little lover of liberty sets about "easing" the pressure of commands. Children's Ways |James Sully 
He walked on, and thought of the rapt liberty of the soul in the sweet serenities of beautiful solitude. The Pastor's Fire-side Vol. 3 of 4 |Jane Porter 
"And the first thing you did with your liberty was to come to Europe," said Miss Thangue, with a sympathetic smile. Ancestors |Gertrude Atherton