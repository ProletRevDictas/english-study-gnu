If saccharide reminds you of sugar, it’s because sugars are saccharides. Scientists Say: Carbohydrate |Bethany Brookshire |August 24, 2020 |Science News For Students 
Leaves, for instance, get their green from chlorophyll — the same chemical that helps them make sugar from sunlight. Let’s learn about colors |Bethany Brookshire |June 11, 2020 |Science News For Students 
That let them identify proteins, sugars and metals in the slime. This tube worm’s glowing slime may help sustain its own shine |Carolyn Wilke |June 5, 2020 |Science News For Students 
That yeast breaks down glucose, a simple sugar, to make beer’s alcohol and carbon dioxide. Planets with hydrogen skies could harbor life |Lisa Grossman |June 2, 2020 |Science News For Students 
They received pollen and a sugar water solution, but no pesticide. Pesticides can have long-term impact on bumblebee learning |Alison Pearce Stevens |May 18, 2020 |Science News For Students 
Alcohol and sugar, even in moderate amounts, are not only sinful but poisonous. How Taryn Toomey’s ‘The Class’ Became New York’s Latest Fitness Craze |Lizzie Crocker |January 9, 2015 |DAILY BEAST 
Cook, stirring often, for 10 minutes or until the sugar is completely dissolved and the mixture is smooth. Make ‘The Chew’s’ Carla Hall’s Sticky Toffee Pudding |Carla Hall |December 28, 2014 |DAILY BEAST 
In the bowl of an electric mixer, cream the butter and brown sugar until light and fluffy. Make ‘The Chew’s’ Carla Hall’s Sticky Toffee Pudding |Carla Hall |December 28, 2014 |DAILY BEAST 
Remove from heat and stir in the walnuts, rum, powdered sugar, and salt until fully incorporated. Carla Hall’s Christmas Day Treat: Rum Balls |Carla Hall |December 25, 2014 |DAILY BEAST 
Other versions are coated in marzipan, or dusted in powder sugar. One Cake to Rule Them All: How Stollen Stole Our Hearts |Molly Hannon |December 24, 2014 |DAILY BEAST 
His chief duty on the sugar plantation is to keep the monkeys out of the cane. Alila, Our Little Philippine Cousin |Mary Hazelton Wade 
Besides, there is always a bunch of bananas hanging inside the house, and he has sugar-cane in abundance. Alila, Our Little Philippine Cousin |Mary Hazelton Wade 
Until a few months ago, the boy has lived on a sugar plantation owned by a rich Tagal planter. Alila, Our Little Philippine Cousin |Mary Hazelton Wade 
Receiving small encouragement in England, he applied to sugar-cane planters to give his engines a trial in the West Indies. Life of Richard Trevithick, Volume II (of 2) |Francis Trevithick 
Spain is at war with North America, and now offers us this sugar-plum to draw us to her side to defend her against invasion. The Philippine Islands |John Foreman