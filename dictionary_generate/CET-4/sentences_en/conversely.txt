Conversely, she noted that some African Americans are hostile to Muslims who own liquor stores in their communities. Michael Brown, Gaza, and Muslim Americans |Dean Obeidallah |August 20, 2014 |DAILY BEAST 
Conversely, the way we get the Donald Sterlings of tomorrow is by shutting up the Mark Cubans of today. Thank You, Mark Cuban, for Speaking Up |Joshua DuBois |May 23, 2014 |DAILY BEAST 
Conversely, Cersei never consents on the show, and cries and objects throughout the scene. The Abused Wives of Westeros: A Song of Feminism in ‘Game of Thrones’ |Amy Zimmerman |April 30, 2014 |DAILY BEAST 
Conversely, birth control prescriptions were 31 percent lower. AIDS Patients Flock to Obamacare |Julie Appleby |April 9, 2014 |DAILY BEAST 
Conversely, the Divergent film is self-consciously, proudly progressive. Sex Won’t Kill Young Adult Heroines: ‘Divergent’ and Rape Culture |Amy Zimmerman |March 28, 2014 |DAILY BEAST 
Can we say, conversely, that it consists wholly of such accuracy of response? The Analysis of Mind |Bertrand Russell 
Conversely it will be less in places where sunshine and heat are very abundant during the summer. Dwarf Fruit Trees |F. A. Waugh 
Conversely, when the midwife is rewarded with that which seems valuable it turns out worthless. The Science of Fairy Tales |Edwin Sidney Hartland 
So Jeffryes would sometimes spend the whole evening trying to transmit a single message, or, conversely, trying to receive one. The Home of the Blizzard |Douglas Mawson 
He has proof of the constructive power of righteousness, and conversely he learns the destructive power of sin. The Minister and the Boy |Allan Hoben