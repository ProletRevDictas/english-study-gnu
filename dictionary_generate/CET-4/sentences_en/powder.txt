It’s filled with an emery powder, which can sharpen needles. Hints From Heloise: Grocery delivery has its pros and cons |Heloise Heloise |February 11, 2021 |Washington Post 
Toner cartridges use a powder and precision technology to accurately and sharply print out text documents. Best all-in-one printer: Upgrade your home office with these multitasking machines |Carsen Joenk |February 8, 2021 |Popular-Science 
Plus, metal powders also can present a “breathing hazard,” Gunduz points out. This huge Xerox printer can create metal parts for the US Navy |Rob Verger |February 4, 2021 |Popular-Science 
Next, glazes are mixed, requiring a number of powders, sands, and clays. How Traditional Korean Tableware Is Made for Michelin-Starred Restaurants |Eater Video |January 27, 2021 |Eater 
The new catalyst is a powder made of cheap ingredients, such as iron. A new catalyst turns greenhouse gas into jet fuel |Maria Temming |January 27, 2021 |Science News For Students 
Place the flour, baking soda, baking powder, and salt on parchment or wax paper. Make ‘The Chew’s’ Carla Hall’s Sticky Toffee Pudding |Carla Hall |December 28, 2014 |DAILY BEAST 
Other versions are coated in marzipan, or dusted in powder sugar. One Cake to Rule Them All: How Stollen Stole Our Hearts |Molly Hannon |December 24, 2014 |DAILY BEAST 
This powder can be shipped anywhere and then reconstituted—just add water, as if it were instant coffee. Powdered Measles Vaccine Could Be Huge for Developing World |Kent Sepkowitz |December 2, 2014 |DAILY BEAST 
So the new inhaled powder measles vaccine may in a few years turn out to be an easier way to protect kids from measles. Powdered Measles Vaccine Could Be Huge for Developing World |Kent Sepkowitz |December 2, 2014 |DAILY BEAST 
They say that the Israelis framed him in order to light the powder keg of religious war over the al-Aqsa compound. In Jerusalem Home Demolitions, the Biblical Justice of Revenge |Creede Newton |November 25, 2014 |DAILY BEAST 
The law went into operation in England imposing a tax on wearing hair powder. The Every Day Book of History and Chronology |Joel Munsell 
The girl began to hum, as she powdered her nose with a white glove, lying in a powder box. Rosemary in Search of a Father |C. N. Williamson 
Steam machinery would accomplish more than nine-tenths of all the work, besides saving the expense of all the powder. Life of Richard Trevithick, Volume II (of 2) |Francis Trevithick 
When first seen it is said to have had a pasty consistency, but on exposure to the air it dried and crumbled into powder. Asbestos |Robert H. Jones 
The poorest people reduce it to powder by manual labour, in the same way as they grind corn preparatory to baking it into cakes. Life of Richard Trevithick, Volume II (of 2) |Francis Trevithick