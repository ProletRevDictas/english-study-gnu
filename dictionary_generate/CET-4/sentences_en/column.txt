In the figure below, the column on the left is a list of all pages. How to teach an old blog new SEO tricks |Tom Pick |August 27, 2020 |Search Engine Watch 
In a recent newspaper column, Josefowitz wrote about the pandemic affording free time to tackle procrastinated tasks. No Visitors Leading to Despair and Isolation in Senior Care Homes |Jared Whitlock |July 28, 2020 |Voice of San Diego 
When placed on the column, each ring slid down to its correct position, if possible. Can The Hare Beat The Tortoise? |Zach Wissner-Gross |July 17, 2020 |FiveThirtyEight 
A pressure sensor on a tag attached to a shark’s fin recorded the animal’s swimming depth at one-second intervals as the shark moved up and down in the water column. Random Search Wired Into Animals May Help Them Hunt |Liam Drew |June 11, 2020 |Quanta Magazine 
At the end of my five-day experiment, I created a spreadsheet of my results, with each group getting its own column. Rock Candy Science 2: No such thing as too much sugar |Bethany Brookshire |April 30, 2020 |Science News For Students 
Sometimes a column has the economy and rhythm of a short story. The Best Columns of 2014 |John Avlon, Errol Louis |December 31, 2014 |DAILY BEAST 
Later that night, that same black-and-red banner would be seen again—in the column of marchers chanting for dead cops. The Monsters Who Screamed for Dead Cops |Jacob Siegel |December 23, 2014 |DAILY BEAST 
He branded it a fifth-column invasion into popular culture, normalizing radical, even communist ambitions. Glenn Beck Is Now Selling Hipster Clothes. Really. |Ana Marie Cox |December 20, 2014 |DAILY BEAST 
My editor called and said, “Do a column on this Lena Dunham flap!” Up to a Point: They Made Me Write About Lena Dunham |P. J. O’Rourke |December 13, 2014 |DAILY BEAST 
His sign was the last one people saw as the column of marchers passed them, it read, “Am I next?” ‘They Let Him Off?’ Scenes from NYC in Disbelief |Jacob Siegel |December 4, 2014 |DAILY BEAST 
My two eyes haven't quite the same focal length and this often puts me out of the straight with a column of figures. The Salvaging Of Civilisation |H. G. (Herbert George) Wells 
The very first chords which Mademoiselle Reisz struck upon the piano sent a keen tremor down Mrs. Pontellier's spinal column. The Awakening and Selected Short Stories |Kate Chopin 
He leaned against that same stone column, thinking, searching in his mind, feeling acutely. The Wave |Algernon Blackwood 
Track of the count may be kept by placing a mark for each leukocyte in its appropriate column, ruled upon paper. A Manual of Clinical Diagnosis |James Campbell Todd 
The last thing—against the skyline—a little column of French soldiers of the line charging back upwards towards the lost redoubt. Gallipoli Diary, Volume I |Ian Hamilton