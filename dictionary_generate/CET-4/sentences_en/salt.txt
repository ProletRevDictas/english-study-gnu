Along with the expected water and salts, the geysers contain organic compounds. These Images Expose the Dark Side of the Solar System - Issue 89: The Dark Side |Corey S. Powell |August 26, 2020 |Nautilus 
The salts found on the surface are important in helping to maintain liquid water within an environment like Ceres. The dwarf planet Ceres might be home to an underground ocean of water |Neel Patel |August 11, 2020 |MIT Technology Review 
He also learned how salt marshes can protect inland communities by buffering those waves. For teens, big problems may lead to meaningful research |Carolyn Wilke |July 28, 2020 |Science News For Students 
For some tastes, such as salt, scientists can isolate the taste alone, dropping salty water right into the mouth. Can we taste fat? The brain thinks so |Bethany Brookshire |July 24, 2020 |Science News For Students 
The team also tested the device with artificial sweat that included water and salts. Working up a sweat may one day power up a device |Carolyn Wilke |June 29, 2020 |Science News For Students 
Place the flour, baking soda, baking powder, and salt on parchment or wax paper. Make ‘The Chew’s’ Carla Hall’s Sticky Toffee Pudding |Carla Hall |December 28, 2014 |DAILY BEAST 
Whisk in the half and half and season to taste with salt and pepper. Make Carla Hall’s Crispy Shallot Green Bean Casserole |Carla Hall |December 27, 2014 |DAILY BEAST 
Bring a large pot of water to a boil and season liberally with salt. Make Carla Hall’s Crispy Shallot Green Bean Casserole |Carla Hall |December 27, 2014 |DAILY BEAST 
Remove from heat and stir in the walnuts, rum, powdered sugar, and salt until fully incorporated. Carla Hall’s Christmas Day Treat: Rum Balls |Carla Hall |December 25, 2014 |DAILY BEAST 
Rub the loin with olive oil, and season with salt and pepper. Make Carla Hall’s Roasted Pork Loin With Cranberries |Carla Hall |December 24, 2014 |DAILY BEAST 
The Afghan was true to his salt, and their own retainers, who had come with them from Lucknow, remained steadfast at this crisis. The Red Year |Louis Tracy 
Robert Fitzgerald received a patent in England for making salt water fresh. The Every Day Book of History and Chronology |Joel Munsell 
The men arrived in very bad condition, and many of them blinded with the salt water which had dashed into their eyes. The Philippine Islands, 1493-1898, Volume XX, 1621-1624 |Various 
He shall pour frost as salt upon the earth: and when it freezeth, it shall become like the tops of thistles. The Bible, Douay-Rheims Version |Various 
How still it was, with only the voice of the sea whispering through the reeds that grew in the salt-water pools! The Awakening and Selected Short Stories |Kate Chopin