The player whose card has a higher rank wins the turn and places both cards on the bottom of their pile. The Fifth Battle For Riddler Nation |Zach Wissner-Gross |September 4, 2020 |FiveThirtyEight 
At tables spread out around a room, citizen scientists of all ages and all backgrounds inspected piles of scat. Are coyotes moving into your neighborhood? |Kathryn Hulick |September 3, 2020 |Science News For Students 
They’ll slice apples and cheese with aplomb, but if you use one to try and cut up a pile of cardboard, it’ll be dull by the end of that task. Three Questions to Ask Yourself Before Buying a Knife |Wes Siler |September 3, 2020 |Outside Online 
Right now, those piles present a fire risk and are costly to manage. California and the Forest Service have a plan to prevent future catastrophic fires |Ula Chrobak |August 27, 2020 |Popular-Science 
It’s not like after 13 grains, it moves from a collection to a pile. What Is an Individual? Biology Seeks Clues in Information Theory. |Jordana Cepelewicz |July 16, 2020 |Quanta Magazine 
What you see is a massive, well-intentioned, legal junk pile. Red Tape Is Strangling Good Samaritans |Philip K. Howard |December 27, 2014 |DAILY BEAST 
The correspondent does a stand-up next to a burning pile of heroin and gets a taste of its effect. BBC Reporter Gets High On The Job |Jack Holmes, The Daily Beast Video |December 23, 2014 |DAILY BEAST 
Hitchcock leans toward me in a conspiratorial, almost lascivious, way and says, “Let's pile on the menace.” Alfred Hitchcock’s Fade to Black: The Great Director’s Final Days |David Freeman |December 13, 2014 |DAILY BEAST 
Pre-sizing eliminates the opportunity to pile those taters too high. 12 Thanksgiving Weight Loss Tips That Actually Work |DailyBurn |November 27, 2014 |DAILY BEAST 
Inside a box I could see a pile of whips, chains, ball gags, and hoods. Whip It: Secrets of a Dominatrix |Justin Jones |November 25, 2014 |DAILY BEAST 
It is a lofty and richly-decorated pile of the fourteenth century; and tells of the labours and the wealth of a foreign land. Blackwood's Edinburgh Magazine, Volume 60, No. 372, October 1846 |Various 
Again the young fellow repeats his fatal "Banco," as he stakes a fresh pile of notes handed to him by the obsequious Jew. The Pit Town Coronet, Volume I (of 3) |Charles James Wills 
They soon had a large pile heaped up in the middle of the road which led through the forest. The Nursery, July 1873, Vol. XIV. No. 1 |Various 
A-course, Mrs. Bridger got a nice little pile of money fer it, and paid Curry the balance she owed him. Alec Lloyd, Cowpuncher |Eleanor Gates 
Even Konnel had a small pile before him, although he seemed to be losing some of Lilac's attention to Meadows. Fee of the Frontier |Horace Brown Fyfe