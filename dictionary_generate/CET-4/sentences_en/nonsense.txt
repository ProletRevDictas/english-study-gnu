Turns out that many of the revelations you have wagered your lives on were transparent nonsense, despite your exhaustive research involving Internet memes. Gene Weingarten: I come bearing good news, QAnon. Meet WAnon, the new Mr. Right. |Gene Weingarten |February 11, 2021 |Washington Post 
This is why nonsense advice like “think positive” usually fails. 7 Wellness Strategies to Build Resilience |Brad Stulberg |January 22, 2021 |Outside Online 
The question is why Fox News continues to pay him to sow division and nonsense on the highest-rated show on cable news. Newt Gingrich’s blatant hypocrisy |Erik Wemple |January 11, 2021 |Washington Post 
Instead they air fictions about defunding the police or insulting nonsense about Warnock hating America. A disgusting GOP attack ad shows what’s really at stake in Georgia |Greg Sargent |December 18, 2020 |Washington Post 
I’m not worried about the “microchip tracking devices” nonsense. A doctor on 9 things that could go wrong with the new vaccines |F. Perry Wilson |December 11, 2020 |Vox 
The academic, historic, and geopolitical nonsense that Khomeinism equals Iran has lasted long enough. The Nuclear Deal That Iran’s Regime Fears Most |Djavad Khadem |November 22, 2014 |DAILY BEAST 
But the author of The Beauty Myth has become a nonsense-garbling conspiracy theorist. From ISIS to Ebola, What Has Made Naomi Wolf So Paranoid? |Michael Moynihan |October 11, 2014 |DAILY BEAST 
In those early days, no-nonsense Anna was a full partner in the business. ‘The Harness Maker’s Dream:’ The Unlikely Ranch King of Texas |Nick Kotz |September 20, 2014 |DAILY BEAST 
A country that is serious about health reform would not take a limited, valued resource and waste it on nonsense like this. Why Your Doctor Feels Like a 'Beaten Dog' |Daniela Drake |September 11, 2014 |DAILY BEAST 
You can either consume this plasticky PR nonsense and keep watching, or you can walk away. How Could the NFL Not Know About the Brutal Second Ray Rice Tape? |Robert Silverman |September 8, 2014 |DAILY BEAST 
"Look here, old man, this superstitious nonsense is becoming an obsession to you," it said one fine April morning. Uncanny Tales |Various 
If you do not give up thinking and take to nonsense and novels, I shall be called to take you through a nervous fever. Tessa Wadsworth's Discipline |Jennie M. Drinkwater 
I don't want to hear any more of that nonsense, nor to have you, Dorothy, go searching for the place. Dorothy at Skyrie |Evelyn Raymond 
"If he would only read our books, and enter into poetry and delight in it; but it is all nonsense to him," said Ethel. The Daisy Chain |Charlotte Yonge 
If you were an only son, it might be your duty to stay; being one of many, 'tis nonsense to make a rout about parting with you. The Daisy Chain |Charlotte Yonge