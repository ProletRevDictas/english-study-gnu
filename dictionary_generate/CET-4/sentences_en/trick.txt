The biggest trick to digitization right now is meeting customers where they are. How to drive digital innovation necessary during the pandemic |Nick Chasinov |September 16, 2020 |Search Engine Watch 
If you were good at finding differences between two pictures as a kid, this will probably do the trick for you in most cases. How to earn your place in Google’s index in 2020 |Bartosz Góralewicz |September 14, 2020 |Search Engine Land 
The trick is, when you get something right the first time, you haven't learned anything. How I learned to tell stories on purpose in the new Frog Fractions |Ars Staff |September 11, 2020 |Ars Technica 
The trick is being able to get featured snippets is using a structured process. How to become a master of featured snippets |Mark Webster |September 3, 2020 |Search Engine Watch 
An old drug can learn new tricks during the coronavirus pandemic. WHO says common steroids can slash death risk for the sickest coronavirus patients |Sy Mukherjee |September 2, 2020 |Fortune 
They can be ingested sporadically or used as a mixer throughout the night (though a can of Sprite seems to be the latest trick). History's Craziest Hangover Cures |Justin Jones |December 30, 2014 |DAILY BEAST 
But one extra trick would instantly solve the problem of crashes that occur over water. Red Tape and Black Boxes: Why We Keep ‘Losing’ Airliners in 2014 |Clive Irving |December 29, 2014 |DAILY BEAST 
The trick has been to create nonstops from cities like Boston that were under-served. Goodbye, Bahamas. Hello, Havana! |Clive Irving |December 18, 2014 |DAILY BEAST 
And just last May Glee aired “Old Dog, New Trick,” the first episode scripted by Colfer. Chris Colfer on Writing, Acting, and the Pain of Being A Pop Culture Trailblazer |Oliver Jones |December 15, 2014 |DAILY BEAST 
The trick is to be able to recognize the right one when it comes along. Alfred Hitchcock’s Fade to Black: The Great Director’s Final Days |David Freeman |December 13, 2014 |DAILY BEAST 
He thrust his tiny tuft of beard between his teeth—a trick he had when perplexed or thoughtful. St. Martin's Summer |Rafael Sabatini 
I could have sworn I heard a cry, and one of my men spoke in a tone that assured me my imagination had not been playing a trick. Raw Gold |Bertrand W. Sinclair 
Some were inquisitive enough to ask, Has a treaty been signed or a trick been played upon the rebels? The Philippine Islands |John Foreman 
And the finger he pointed at the girl quivered with the rage that filled him at this trick they had thought to put upon him. St. Martin's Summer |Rafael Sabatini 
Nothing was out of the ordinary except that the Professor developed an odd trick of continually glancing at his right hand. Uncanny Tales |Various