Since the 1950s, fluoride has adapted itself to the prevailing concerns of the time. Anti-Fluoriders Are The OG Anti-Vaxxers |Michael Schulson |July 27, 2016 |DAILY BEAST 
“Gronkowski” itself never manages to sound more erotic than the name of a hearty Polish stew or a D-list WWE performer. ‘A Gronking to Remember’ Speed Read: 8 Naughtiest Bits |Emily Shire |January 7, 2015 |DAILY BEAST 
While public interest in Ebola continues to dwindle, the epidemic itself continues to soar. The Race for the Ebola Vaccine |Abby Haglage |January 7, 2015 |DAILY BEAST 
Finding the shop is a trip in itself and an introduction to a slice of history. The Photographer Who Gave Up Manhattan for Marrakech |Liza Foreman |January 6, 2015 |DAILY BEAST 
That apparently includes some members of the management of the airport itself and some air traffic controllers. Annoying Airport Delays Might Prevent You From Becoming the Next AirAsia 8501 |Clive Irving |January 6, 2015 |DAILY BEAST 
This mania for correction shows itself too in relation to the authorities themselves. Children's Ways |James Sully 
This is one of the most striking manifestations of the better side of child-nature and deserves a chapter to itself. Children's Ways |James Sully 
The well-worn aphorism of the Frenchman, “History repeats itself,” was about to assert itself. Checkmate |Joseph Sheridan Le Fanu 
Besides this fundamental or primary vibration, the movement divides itself into segments, or sections, of the entire length. Expressive Voice Culture |Jessie Eldridge Southwick 
I've never had time to write home about it, for I felt that it required a dissertation in itself to do it justice. Music-Study in Germany |Amy Fay