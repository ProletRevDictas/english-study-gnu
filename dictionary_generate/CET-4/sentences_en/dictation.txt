Bought by Microsoft in April, Nuance boasts accurate dictation, including in web apps such as Gmail. AI: The New Teacher’s Pet |Toyloy Brown III |July 27, 2021 |Ozy 
With the brand new "Slide to Type" feature, it's by far the fastest way to type on a watch when dictation isn't applicable or reliable enough. Maker of keyboard apps for the blind sues Apple, claiming anticompetitive behavior |Reed Albergotti |March 18, 2021 |Washington Post 
The new tablet sort of has Siri, albeit only in the form of voice dictation, not the full voice assistant. Apple iPad 3 Debut: Inside the New Features | |March 7, 2012 |DAILY BEAST 
The novelist who can't read uses dictation to continue to write books. Oliver Sacks on The Mind's Eye and Neurological Afflictions |Jamie Holmes |November 13, 2010 |DAILY BEAST 
During so long drawn out a suspense I tried to ease the tension by dictation. Gallipoli Diary, Volume I |Ian Hamilton 
If the dictation of his will as a law in reference to the service had been sufficient, he would not otherwise have enjoined it. The Ordinance of Covenanting |John Cunningham 
This I am writing by dictation, and really think it is an art I can manage to acquire. The Works of Robert Louis Stevenson - Swanston Edition Vol. 25 (of 25) |Robert Louis Stevenson 
At last he spoke in a low tone to the avocat, who quickly began writing at his dictation. When Valmond Came to Pontiac, Complete |Gilbert Parker 
The result was that the bank resisted, and refused the required acquiescence in the dictation of the treasury. Select Speeches of Daniel Webster |Daniel Webster