If you want to put in a floor, sew a quilt, knit a scarf or paint a wall, you will use algebra to find out how much material you need. Scientists Say: Algebra |Bethany Brookshire |January 4, 2021 |Science News For Students 
Or, and this seems just as likely, computing may splinter into a bizarre quilt of radical chips, all stitched together to make the most of each depending on the situation. The Trillion-Transistor Chip That Just Left a Supercomputer in the Dust |Jason Dorrier |November 22, 2020 |Singularity Hub 
Brooke fought with the administration at the all-girls school they attended to ensure he didn’t have to wear the quilt uniform or a dress at graduation. Vote Brooke Pinto for Ward 2 Council |John Guggenmos |October 6, 2020 |Washington Blade 
Efforts to map reopening around the country end up looking like patchwork quilts. 10 facts about school reopenings in the Covid-19 pandemic |Anna North |October 1, 2020 |Vox 
A sample from a cotton quilt with batting also worked very well in the tests as did four layers of silk. Science offers recipes for homemade coronavirus masks |Kathiann Kowalski |May 14, 2020 |Science News For Students 
Historical photographs on view in the exhibition show a crazy quilt of painting and sculpture. The Most Wanted Warhol: A Scandal at the 1964 World’s Fair |Jessica Dawson |April 25, 2014 |DAILY BEAST 
The final product is an appropriately “salmon” colored quilt. They’re Done With YouTube. Now Cats Storm the Art-World |Justin Jones |January 25, 2014 |DAILY BEAST 
Like a  patchwork quilt we can cherry pick from the holiday the parts that nurture our well worn hearts. Christmas Misfits Unite |Dr. Michelle K. London |December 24, 2013 |DAILY BEAST 
On another wall was hung a gaily colored quilt made by Harris. Scarsdale Doctor Killer Jean Harris Found Redemption in Prison |Michael Daly |December 31, 2012 |DAILY BEAST 
The only problem is that I almost never see a new quilt that seems worthy of being so taken. Self-Protection |Blake Gopnik |July 11, 2012 |DAILY BEAST 
Zeal had undressed, extended himself on the bed, and covered his body with an eider-down quilt. Ancestors |Gertrude Atherton 
His grandfather submitted, and Gwynne dropped his arm and rearranged the quilt over his cousin's body. Ancestors |Gertrude Atherton 
Our attention was attracted to three or four windows that looked much like the crazy-quilt work that used to be in fashion. British Highways And Byways From A Motor Car |Thomas D. Murphy 
He sat down on the side of the cheap, iron bedstead, and emptied his pockets on the top quilt. Cabin Fever |B. M. Bower 
Above, in the lower lofts, every conceivable human oddity was assembled in a sort of mercantile crazy quilt. The Woman Gives |Owen Johnson