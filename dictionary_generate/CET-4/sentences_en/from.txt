The bulk of the premiere actually put an unexpected spin on the ripped-from-the-headlines story. ‘Newsroom’ Premiere: Aaron Sorkin Puts CNN on Blast Over the Boston Bombing |Kevin Fallon |November 10, 2014 |DAILY BEAST 
But its title is a misnomer: The far-from-renegade Gay is a very good feminist. Roxane Gay: Not Such a 'Bad Feminist' After All |Lizzie Crocker |August 12, 2014 |DAILY BEAST 
Says my wife, returning from the shower be-robed, towel-turbaned, and still smelling faintly of not-made-from-concentrate. The Ridiculousness of Father's Day |P. J. O’Rourke |June 15, 2014 |DAILY BEAST 
But a new crop of famous-from-birth models are trying to make it on their own…and they deserve to be taken seriously. The Famous Parents Modeling Club |Erin Cunningham |May 28, 2014 |DAILY BEAST 
Michael Jackson's back-from-the-dead moonwalk stunned viewers at the Billboard Awards. Michael Jackson's Crazy Billboard Awards Performance and More Hologram Wins and Fails (VIDEO) |The Daily Beast |May 19, 2014 |DAILY BEAST 
They have a nodding-from-a-motor-acquaintance with it but I like a real handshake-friendship with it. Patchwork |Anna Balmer Myers 
Once more we found ourselves in the far-from-delectable town of Cape François. Hurricane Hurry |W.H.G. Kingston 
It was no good trying some tricky approach; his best bet was the straight-from-the-shoulder bit. Greylorn |John Keith Laumer 
Now the fugitive-from-labor clause must be interpreted in part by the light of the Purpose of the Constitution. The Trial of Theodore Parker |Theodore Parker 
And again came that scent of cigar smoke-from the old saturated leather. The Forsyte Saga, Volume III. |John Galsworthy