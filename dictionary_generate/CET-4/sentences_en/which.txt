So I've worked out a plan by-which you can examine the invention and test its profits without risking one penny. Astounding Stories of Super-Science February 1930 |Various 
His declaration means that he believes in "That-which-is-above-Things." Dynamic Thought |William Walker Atkinson 
By the way, I wonder if I ought to tell him about the silver which-not. Berry And Co. |Dornford Yates 
We just cant afford to have our goods floating around every-which-way right in the start. Motor Boat Boys' River Chase |Louis Arundel 
We went into another field—behind us and before us, and every which-a-way we looked, we seen a rhinusorus. Bransford of Rainbow Range |Eugene Manlove Rhodes