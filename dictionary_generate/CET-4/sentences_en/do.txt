When they thought about Lewis, what struck the players most was that he never acted like a do-gooder. A West Point MVP Who Never Played a Down |Nicolaus Mills |December 13, 2014 |DAILY BEAST 
First on the to-do list, the profiling exercises to help the Western masses understand the nature of the wretched beast. ISIS and BS |Amal Ghandour |October 15, 2014 |DAILY BEAST 
If someone wants to dismiss this as do-goodism, fine, but it has real world effects. Confronting George Clooney’s Critics on South Sudan |John Avlon |October 7, 2014 |DAILY BEAST 
Think of it as the Jersey Shore exception, where you can act like a brutish goon and the first bust is essentially a do-over. Ray Rice Should Have Remembered His 'Kindness' Anti-Bullying Wristband |Michael Daly |September 10, 2014 |DAILY BEAST 
She tried the direct, how-do-you-do handshake approach, but was blocked by a burly aide-de-camp. Andrew Cuomo Can't Ignore It Now: He's Weak Even at Home |David Freedlander |September 10, 2014 |DAILY BEAST 
The ne'er-do-well blew, like seed before the wind, to distant places, but mankind at large stayed at home. The Unsolved Riddle of Social Justice |Stephen Leacock 
With time this land had mounted to great values and the holders had been made well-to-do thereby. The Homesteader |Oscar Micheaux 
His parents were of the well-to-do farming class, occupied from one year's end to the other with the work of the fields. Bastien Lepage |Fr. Crastre 
“But it certainly was a great to-do,” murmured Jessie, as she tried to see what the boys were doing. The Campfire Girls of Roselawn |Margaret Penrose 
Widder Morse wants to ape these well-to-do folks that live tother end o Whiffle Street. The Girls of Central High on the Stage |Gertrude W. Morrison