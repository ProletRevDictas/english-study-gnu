When twelve people are killed by violence, whoever they are, for whatever reason, that is a tragedy and a waste. Trolls and Martyrdom: Je Ne Suis Pas Charlie |Arthur Chu |January 9, 2015 |DAILY BEAST 
These addresses were used by whoever carried out the attack to control the malware and can be found in the malware code itself. No, North Korea Didn’t Hack Sony |Marc Rogers |December 24, 2014 |DAILY BEAST 
The mass dump suggests that whoever did this, their primary motivation was to embarrass Sony Pictures. No, North Korea Didn’t Hack Sony |Marc Rogers |December 24, 2014 |DAILY BEAST 
The real hackers—whoever they may prove to be—had pulled off a feat: they ruined a Hollywood fete. Sony Hack: A Dictator Move? |Kevin Bleyer |December 14, 2014 |DAILY BEAST 
Whoever it is that Lebanese officials now have in custody, they clearly think she is valuable and worth publicizing. The ISIS Wife Swap Mystery |Jacob Siegel |December 3, 2014 |DAILY BEAST 
I, therefore, deliver it as a maxim, that whoever desires the character of a proud man ought to conceal his vanity. Pearls of Thought |Maturin M. Ballou 
Whoever succeeded in getting the ring on his stick won the game, and carried the prize home as a sign of victory. Our Little Korean Cousin |H. Lee M. Pike 
You cannot all at once eradicate the deep-rooted customs and habits of any people, whoever they may be. The Jesuit Relations and Allied Documents, Vol. II: Acadia, 1612-1614 |Various 
Suffice it to say that whoever boasts of an excellent violin should match it with a superior bow. Violins and Violin Makers |Joseph Pearce 
Whoever denied the former proposition was called a tool of the Court. The History of England from the Accession of James II. |Thomas Babington Macaulay