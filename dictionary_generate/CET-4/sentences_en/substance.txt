A judge in that case has yet to rule on most of the substance of Amazon’s lawsuit. Oracle loses appeal in $10 billion Pentagon cloud-computing contract dispute |radmarya |September 2, 2020 |Fortune 
This is a substance that can give a person immunity from a disease. Scientists Say: Vaccine |Bethany Brookshire |August 31, 2020 |Science News For Students 
To keep a wormhole’s throat from collapsing, some substance with negative mass must prop it open. Could ripples in spacetime point to wormholes? |Emily Conover |August 24, 2020 |Science News For Students 
We believe the substance and process for this important park planning policy needs more attention and significant changes. New Plan for City Parks Misses the Point |Deborah Sharpe, Howard Greenstein and Jeff Harkness |July 24, 2020 |Voice of San Diego 
These foreign invaders contain substances the body doesn’t recognize. Explainer: What are Antibodies? |Avery Elizabeth Hurt |July 24, 2020 |Science News For Students 
Scalise spoke briefly, adding little of substance, saying that the people back home know him best. The Price of Steve Scalise’s Silence |Jason Berry |January 7, 2015 |DAILY BEAST 
“Any time you put a foreign substance into anybody you have the potential for an adverse event,” Geisbert reminds. Uh Oh: Ebola Vaccine Trials Stop |Leigh Cowart |December 19, 2014 |DAILY BEAST 
Those with a slightly sleazier bent have dredged up reports of his weight gain, substance abuse, and arrest. D’Angelo’s ‘Black Messiah’ Was Worth Waiting 15 Years For |James Joiner |December 16, 2014 |DAILY BEAST 
South Carolina and Alabama courts have already ruled late-pregnancy substance abuse can be considered a form of child abuse. States Slap Pregnant Women With Harsher Jail Sentences |Emily Shire |December 12, 2014 |DAILY BEAST 
Things got even more serious when Cosby moved on to the subject of substance abuse and children. When Bill Cosby N-Bombed the Congressional Black Caucus |Asawin Suebsaeng |December 2, 2014 |DAILY BEAST 
In withdrawing aside sorrow remaineth: and the substance of the poor is according to his heart. The Bible, Douay-Rheims Version |Various 
Its backbone should be the study of biology and its substance should be the threshing out of the burning questions of our day. The Salvaging Of Civilisation |H. G. (Herbert George) Wells 
The nature both of this substance and the antecedent substance from which it is derived is not known. A Manual of Clinical Diagnosis |James Campbell Todd 
But this paper was a very tough, fibrous substance, and would resist quite a heavy blow as well as keep out the cold. Our Little Korean Cousin |H. Lee M. Pike 
There has always been a mystery connected with this remarkable substance. Tobacco; Its History, Varieties, Culture, Manufacture and Commerce |E. R. Billings.