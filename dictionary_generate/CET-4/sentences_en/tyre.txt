Last year I met a bomb victim, Ali Muorad, a good-natured twentysomething native of Tyre in southern Lebanon. Clinton's Cluster Bomb Hypocrisy |Lionel Beehner |April 16, 2011 |DAILY BEAST 
The king of Tyre, who affected to be like to God, shall fall under the like sentence with Lucifer. The Bible, Douay-Rheims Version |Various 
The king of Egypt shall be overthrown, and his kingdom wasted: it shall be given to Nabuchodonosor for his service against Tyre. The Bible, Douay-Rheims Version |Various 
And I will send a fire upon the wall of Tyre, and it shall devour the houses thereof. The Bible, Douay-Rheims Version |Various 
And Tyre hath built herself a strong hold, and heaped together silver as earth, and gold as the mire of the streets. The Bible, Douay-Rheims Version |Various 
Emath also in the borders thereof, and Tyre, and Sidon: for they have taken to themselves to be exceeding wise. The Bible, Douay-Rheims Version |Various