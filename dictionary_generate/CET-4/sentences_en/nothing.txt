In the meantime, he should just accept that the holdup has nothing to do with his politics. Conservative Curt Says His Politics, Not His Pitching, Kept Him Out of the Hall of Fame |Ben Jacobs |January 9, 2015 |DAILY BEAST 
The decision not to run the cartoons is motivated by nothing more than fear: either fear of offending or fear of retaliation. Why We Stand With Charlie Hebdo—And You Should Too |John Avlon |January 8, 2015 |DAILY BEAST 
It has nothing to do with the regulatory job he is nominated for. Sen. Warren’s Main Street Crusade to Pressure Clinton |Eleanor Clift |January 8, 2015 |DAILY BEAST 
“There is a heavy security presence but nothing has changed,” agrees Father Javier. Mexico’s Priests Are Marked for Murder |Jason McGahan |January 7, 2015 |DAILY BEAST 
Nothing made Groucho funnier than having this Margaret Dumont around not understanding the jokes. Patton Oswalt on Fighting Conservatives With Satire |William O’Connor |January 6, 2015 |DAILY BEAST 
The vision—it had been an instantaneous flash after all and nothing more—had left his mind completely for the time. The Wave |Algernon Blackwood 
We should have to admit that the new law does little or nothing to relieve such a situation. Readings in Money and Banking |Chester Arthur Phillips 
Nothing remarkable occurred in our march through this country. The Life and Most Surprising Adventures of Robinson Crusoe, of York, Mariner (1801) |Daniel Defoe 
It was very warm, and for a while they did nothing but exchange remarks about the heat, the sun, the glare. The Awakening and Selected Short Stories |Kate Chopin 
"There is no more war," Brion translated for Ulv, realizing that the Disan had understood nothing of the explanation. Sense of Obligation |Henry Maxwell Dempsey (AKA Harry Harrison)