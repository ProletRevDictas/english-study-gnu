Otis says he was wearing a tan jacket similar to one described by witnesses. His First Day Out Of Jail After 40 Years: Adjusting To Life Outside |Justin Rohrlich |January 3, 2015 |DAILY BEAST 
Little ricochets of dust kicked into the face of a tall man in a tan shalwar kameez and prayer cap. I Shot Bin Laden |Elliot Ackerman |November 16, 2014 |DAILY BEAST 
Judging by the pictures of President Truong Tan Sang and Obama, Vietnam is showing some affection back. Beijing’s ‘Star Trek’ APEC Summit |Jake Adelstein |November 11, 2014 |DAILY BEAST 
During the season, he regularly takes a dugout seat where he can work on his tan. Will the Real Jim Palmer Please Stand Up |Tom Boswell |September 27, 2014 |DAILY BEAST 
The philosopher once complained about young men whose desire for learning resembled their desire for a sun tan. The Ivy League Provides the Best Trade Schools Around |Nick Romeo |August 17, 2014 |DAILY BEAST 
She wore soiled Burberry, high-legged tan boots, and a peaked cap of distinctly military appearance. Dope |Sax Rohmer 
The corporal, rather chalky-looking under his tan, stepped forward and laid a hand on MacRae's shoulder. Raw Gold |Bertrand W. Sinclair 
Woollen rags are mixed with cotton which has no manurial value, and the skin refuse from tan-works contains much lime. Elements of Agricultural Chemistry |Thomas Anderson 
I recognized the reddish tan that comes from facing a hot wind on the top of a moving boxcar. Nine Men in Time |Noel Miller Loomis 
He was as well groomed as usual, but he was unmistakably pale beneath his new coat of tan. Ancestors |Gertrude Atherton