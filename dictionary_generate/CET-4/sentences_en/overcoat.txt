“Maybe you need a good overcoat for Christmas,” Mister Ham was saying. The Stacks: Sell the Overcoat, Keep the Dignity |Paul Hemphill |December 22, 2014 |DAILY BEAST 
In his left overcoat pocket was the front-page logotype of the paper, and in his left pocket were 10 or 12 halftones. The Stacks: H.L. Mencken on the 1904 Baltimore Fire |H.L. Mencken |October 4, 2014 |DAILY BEAST 
A boxy white overcoat with a plunging neckline was a highlight, as was as a sand-colored crepe dress with an equally low neckline. Proenza Schouler Spring/Summer 2014: Serenity Now |Isabel Wilkinson |September 11, 2013 |DAILY BEAST 
I have an overcoat I bought in Jermyn Street in London three years ago. Lev Grossman’s Weird Phobia: ‘How I Write’ Interview |Noah Charney |June 8, 2012 |DAILY BEAST 
That night, he heads out into the bitter cold in a suit and overcoat to make his rounds of a sleepy neighborhood down the road. Mitt Romney’s British Mormon Ancestors |Mike Giglio |January 31, 2012 |DAILY BEAST 
Mindful of the military nature of the occasion he appeared in his old army overcoat, in spite of the heat. The Soldier of the Valley |Nelson Lloyd 
Then Edwin was bending over it, with his ear close to her ear, and the sleeve of his overcoat touching her sleeve. Hilda Lessways |Arnold Bennett 
He fumbled under his overcoat for loose silver, drew out a handful and paid off the taximan. Dope |Sax Rohmer 
At last I gave up, and by a sudden thought arose and pulled on my overcoat, and got my hat. The Soldier of the Valley |Nelson Lloyd 
There was nothing in the pockets of the overcoat, but inside the hat he found pasted the initials L. P. Dope |Sax Rohmer