To his critics, he explained—sometimes at painful length—his reasoning against it. Jeb Bush’s Unseen Anti-Gay Marriage Emails |Jackie Kucinich |January 9, 2015 |DAILY BEAST 
The motives were most always harmless, and only sometimes ethically questionable. Design Your Own Dinosaur: The Era of Custom DNA |Justin Jones |January 8, 2015 |DAILY BEAST 
Freedom of speech, then, is sometimes not worth the trouble that comes with it. Politicians Only Love Journalists When They're Dead |Luke O’Neil |January 8, 2015 |DAILY BEAST 
It upsets me because I used to really, and still do sometimes, love the articles Salon writes. Patton Oswalt on Fighting Conservatives With Satire |William O’Connor |January 6, 2015 |DAILY BEAST 
Sometimes, a tech glitch means you are prevented from looking at other users. My Week on Jewish Tinder |Emily Shire |January 5, 2015 |DAILY BEAST 
Sometimes it comes in literal sobriety, sometimes in derisive travesti, sometimes in tragic aggravation. Checkmate |Joseph Sheridan Le Fanu 
Sometimes in the case of large plants, cones have been known to occur on the tips of the branches of the Marsh Horsetail. How to Know the Ferns |S. Leonard Bastin 
The sailors sometimes use it to fry their meat, for want of butter, and find it agreeable enough. The Life and Most Surprising Adventures of Robinson Crusoe, of York, Mariner (1801) |Daniel Defoe 
It, or a similar bacillus, is sometimes found in the sputum of gangrene of the lung. A Manual of Clinical Diagnosis |James Campbell Todd 
Sometimes the stems are quite bare; on other occasions they are partly branched; in any case the branches are short. How to Know the Ferns |S. Leonard Bastin