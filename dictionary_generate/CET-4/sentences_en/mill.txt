The “doctorate” Duke claims is from an anti-Semitic Ukranian “diploma mill” as described by the State Department. The Price of Steve Scalise’s Silence |Jason Berry |January 7, 2015 |DAILY BEAST 
The language school did not focus on providing instruction but instead was a visa mill. ISIS, Boko Haram, and the Growing Role of Human Trafficking in 21st Century Terrorism |Louise I. Shelley |December 26, 2014 |DAILY BEAST 
Not even Radio Bemba (Cuban slang for the rumor mill) had picked up the signal. The Life and Hard Times Of The Family A Cuban Defector Left Behind |Brin-Jonathan Butler |December 19, 2014 |DAILY BEAST 
Did you know that you can purchase and mill 80 percent receivers without a license? What Are We Protecting with Gun Laws? |Jessica Solce |November 13, 2014 |DAILY BEAST 
Kids ranging from 17 down to elementary-school age mill about, waiting to be called by the court. The Border Kid Crisis Hits the Courts |Caitlin Dickson |September 19, 2014 |DAILY BEAST 
But men we had known and trails we had followed furnished us plenty of grist for the conversational mill. Raw Gold |Bertrand W. Sinclair 
Sounds rotten, but that's their style; and you've been through the mill at home enough to know what it is to be knifed socially. Raw Gold |Bertrand W. Sinclair 
Along in the afternoon Porter's force was located near Moore's Mill, about four miles distant. The Courier of the Ozarks |Byron A. Dunn 
The first steam rolling mill, with the exception of the one at Soho, was put up at Bradley ironworks. Showell's Dictionary of Birmingham |Thomas T. Harman and Walter Showell 
They heard how in the early spring in the meadow by the mill-dam Tim and I had stopped our ploughs to draw lots and he had lost. The Soldier of the Valley |Nelson Lloyd