As with any item related to your sleep, you’ll want to make the right investment—and that includes a bedside lamp. Bedside table lamps to brighten your sleep space |PopSci Commerce Team |September 15, 2020 |Popular-Science 
Though the output of a single device is tiny, a group of them could charge a phone or light a lamp, Yao says. Will bacterial ‘wires’ one day power your phone? |Alison Pearce Stevens |September 2, 2020 |Science News For Students 
The Dundee lab is planning to repeat the study with many more subjects and a well-filtered lamp as soon as they secure funding for it, says Nardell. Can far-UVC light reduce the spread of COVID-19 indoors? |Erika Fry |August 31, 2020 |Fortune 
At this temperature, the surface glows brightly in infrared, like the lamp on night-vision goggles. These Images Expose the Dark Side of the Solar System - Issue 89: The Dark Side |Corey S. Powell |August 26, 2020 |Nautilus 
One model did double-duty as a lamp, another as a bookshelf. Ikea promises ‘democratic’ design. Has its Virgil Abloh collaboration lived up? |claychandler |August 25, 2020 |Fortune 
Another Dem who has been just a heart-beat (or a lamp bash) away from being president. Why 2016’s Hopefuls Are Hopeless |P. J. O’Rourke |November 22, 2014 |DAILY BEAST 
“When Tibor died we did a retrospective of MCo., and the lamp was the last thing you saw,” she says. The Singular Artist of New Yorkistan |Lizzie Crocker |November 14, 2014 |DAILY BEAST 
That he ends up not lighting a lamp but tangled in the cobwebs is one of the truths of this valuable book. Slaves In A Family's Past Haunt The Present |Michael Signer |August 28, 2014 |DAILY BEAST 
Struggling with the only battery-operated lamp, McDaniel glanced at Boyah. Fighting Ebola With Nothing but Hope |Abby Haglage |August 27, 2014 |DAILY BEAST 
Maybe the cleric can rub his own magic lamp, and ask it to explain the concept known as brain drain. Iran Cleric: Jews Use Sorcery to Spy |IranWire |July 5, 2014 |DAILY BEAST 
He came to the top of the stairs with a lamp in his hand, and wanted to know what the rumpus was about. The Bondboy |George W. (George Washington) Ogden 
Every light in the hall was ablaze; every lamp turned as high as it could be without smoking the chimney or threatening explosion. The Awakening and Selected Short Stories |Kate Chopin 
Now he blew out the lamp and stood over me in the half-light, holding out a hand. The Soldier of the Valley |Nelson Lloyd 
Martini appeared from his little anti-room, with a lamp in his hand, as the prison clock struck ten. The Pastor's Fire-side Vol. 3 of 4 |Jane Porter 
She sat in a distant corner of the formal room discreetly lit by a shaded lamp. The Joyous Adventures of Aristide Pujol |William J. Locke