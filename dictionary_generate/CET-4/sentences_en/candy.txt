Self-consumption has always been a massive part of the holiday and may end up salvaging sales for candy makers this year. How scary will a COVID-19 Halloween be for candy companies? |Beth Kowitt |August 29, 2020 |Fortune 
She added that if trick-or-treating is lower than expected, Hershey will focus on the “Treat for Me and Candy Bowl occasion”—industry-speak for people buying candy that they will end up eating themselves. How scary will a COVID-19 Halloween be for candy companies? |Beth Kowitt |August 29, 2020 |Fortune 
The holiday is basically an “excuse to buy candy,” says Dickerson. How scary will a COVID-19 Halloween be for candy companies? |Beth Kowitt |August 29, 2020 |Fortune 
The inside shop is full of Nordic products, from sodas to mustards to mints and candies. You know what Dupont needed? A Nordic restaurant |Brock Thompson |August 12, 2020 |Washington Blade 
It is a nontoxic substance often used in candy to bind ingredients together. Science of ‘Seinfeld’ |LGBTQ-Editor |June 15, 2020 |No Straight News 
In fact, that candy store is heavy industry, with all the mess that entails. New York’s Conservative Fracking Ban |Jay Michaelson |December 20, 2014 |DAILY BEAST 
Another returned a printer box stuffed with a candy-filled piñata. The Insane $11 Billion Scam at Retailers’ Return Desks |M.L. Nestel |December 19, 2014 |DAILY BEAST 
These medications will not continue to work when we need them if they are handed out like candy. Without Education, Antibiotic Resistance Will Be Our Greatest Health Crisis |Russell Saunders |December 19, 2014 |DAILY BEAST 
Meanwhile younger, lighter colors evoke citrus and tree fruits, candy sugars and vanilla toffee. Why Natural Color Is So Crucial To Understanding A Whisky’s Flavors | |December 10, 2014 |DAILY BEAST 
Not just a candy factory but a candy store, and everything in it free. ‘Asteroids’ & The Dawn of the Gamer Age |David Owen |November 29, 2014 |DAILY BEAST 
Clarence's papa said the candy had better be eaten by monkeys than by boys; but I doubt whether Clarence was of that opinion. The Nursery, July 1873, Vol. XIV. No. 1 |Various 
He fed the monkeys with candy, and laughed to see them hang by their tails while they took it from his hand. The Nursery, July 1873, Vol. XIV. No. 1 |Various 
It is extremely soluble in water, and can be obtained in large transparent prismatic crystals, as in common sugar-candy. Elements of Agricultural Chemistry |Thomas Anderson 
Four miles before reaching Candy, we came to the river Mahavilaganga, which is spanned by a masterly bridge of one arch. A Woman's Journey Round the World |Ida Pfeiffer 
In my opinion, the situation of Candy is most beautiful, but many affirm that it is too near the mountains, and lies in a pit. A Woman's Journey Round the World |Ida Pfeiffer