Corea, whose nickname evolved from an aunt who called him “Cheeky,” began to study the piano at age 4. Chick Corea, versatile pianist who made jazz eclectic and electric, dies at 79 |Matt Schudel |February 11, 2021 |Washington Post 
My aunt is insisting that I wear a tuxedo, which, to me, is a very odd request. Miss Manners: Declining help from the unmasked |Judith Martin, Nicholas Martin, Jacobina Martin |February 8, 2021 |Washington Post 
When he was talking about his uncle Gus in the barbecue, his aunt Bunny falling down the steps, his mom with the shoe. Jo Koy on Representation and How the Military ‘Created a Race’ |Joshua Eferighe |February 5, 2021 |Ozy 
As StVil did as a child, Gabrielle leaves her family behind to live with her uncle, aunt and cousins. Author’s immigrant story inspires debut novel for kids |Mary Quattlebaum |February 2, 2021 |Washington Post 
Her aunt, a nurse in western New York, just had her first appointment canceled when her county ran out of doses. For younger generation, securing vaccine appointments for parents can be ‘full-time job’ |Travis Andrews |February 2, 2021 |Washington Post 
Sure, your cubicle mate, neighbor, and aunt all own a Fitbit or JawBone fitness tracker. Nothing Says I Love You Like Data |The Daily Beast |December 8, 2014 |DAILY BEAST 
Melchior is the forerunner of the aunt who always gave me socks. Keep Christmas Commercialized! |P. J. O’Rourke |December 6, 2014 |DAILY BEAST 
And there, the sand castle builder and tag player who loved her aunt more than science would be buried. 11 Children Shot in Milwaukee, One in Her Grandpa's Lap |Michael Daly |November 12, 2014 |DAILY BEAST 
Another aunt recalled something Laylah had said to her when they went trick-or-treating on Halloween just a few days before. 11 Children Shot in Milwaukee, One in Her Grandpa's Lap |Michael Daly |November 12, 2014 |DAILY BEAST 
When Juana was 8, her father abandoned the family and the girl moved to Mexico City to live with her aunt. Sor Juana: Mexico’s Most Erotic Poet and Its Most Dangerous Nun |Katie Baker |November 8, 2014 |DAILY BEAST 
You see, I'd always thought of him as the boy whom Great-aunt Lucia described having seen. The Boarded-Up House |Augusta Huiell Seaman 
I don't care, it ain't nice, and I wonder aunt brought us to such a place. The Book of Anecdotes and Budget of Fun; |Various 
The landlady had related the tragic history of the dead mother and the invalid aunt. The Joyous Adventures of Aristide Pujol |William J. Locke 
One old aunt in particular visited him twice a year, and stayed six months each time. The Book of Anecdotes and Budget of Fun; |Various 
Aunt Ri was looking forward to the rest with great anticipation; she was heartily tired of being on the move. Ramona |Helen Hunt Jackson