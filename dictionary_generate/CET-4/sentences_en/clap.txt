The pocket shape “dramatically improves the clap by trapping more air and creating a stronger jet.” Butterflies use jet propulsion for quick getaways |Alison Pearce Stevens |March 15, 2021 |Science News For Students 
Researchers thought the wing clap likely formed a pocket of air that shoots out like a jet. Butterflies use jet propulsion for quick getaways |Alison Pearce Stevens |March 15, 2021 |Science News For Students 
Previous research had suggested that a butterfly’s overhead wing clap forces the insect forward. Butterflies use jet propulsion for quick getaways |Alison Pearce Stevens |March 15, 2021 |Science News For Students 
Researchers thought they knew how the clap worked, he notes. Butterflies use jet propulsion for quick getaways |Alison Pearce Stevens |March 15, 2021 |Science News For Students 
He once joked to The Washington Post that it was “a little bit like saying you have the clap.” Hugh Newell Jacobsen, award-winning modernist architect, dies at 91 |Kathy Orton |March 4, 2021 |Washington Post 
Which is why you should: “Clap along, if you feel like a room without a roof.” Forget the Resolutions; Try a Few Declarations |Kevin Bleyer |January 1, 2015 |DAILY BEAST 
The music drifted through the rain and the woman started to clap her hands and dance. The Stacks: How Leonard Chess Helped Make Muddy Waters |Alex Belth |August 2, 2014 |DAILY BEAST 
Like, clap-my-hands-together-in-schoolgirl-like-glee over the moon to see it. ‘Orange Is the New Black’ Season 2: The Finest, Funniest, and Most Terrifying Moments of Eps. 1-6 |Kevin Fallon, Marlow Stern |June 12, 2014 |DAILY BEAST 
No one applauded–rare on a night when hands tend to clap after every cough and sneeze. Obama’s 34 Words That Matter Most |Joshua DuBois |February 2, 2014 |DAILY BEAST 
Everyone stood up to clap in his honor, including Mao himself. How to Hide a Famine with Ping-Pong |Nicholas Griffin |January 9, 2014 |DAILY BEAST 
"We have a grandfather in Greenfield," spoke up the youngest child before his sister could clap her hand over his mouth. The Box-Car Children |Gertrude Chandler Warner 
But ten thousand saw Musa's hand clap to hilt, and Iftikhar's lance half fall to rest. God Wills It! |William Stearns Davis 
Viscount Melbourne expressed himself to the effect that the Earl of Ripon's motion came like a thunder-clap upon him. The History of England in Three Volumes, Vol.III. |E. Farr and E. H. Nolan 
You had a very fair clap-trap against us, as we happened to be master manufacturers, in saying that we wanted to reduce wages. The History of England in Three Volumes, Vol.III. |E. Farr and E. H. Nolan 
You can't clap a new head on to old shoulders without upsetting circulation and equilibrium. Gallipoli Diary, Volume 2 |Ian Hamilton