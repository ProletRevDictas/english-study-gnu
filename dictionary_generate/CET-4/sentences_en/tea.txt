Terroir labels are also becoming more common for products like coffee, tea and craft beer, says Miguel Gómez, an economist at Cornell University who studies food marketing and distribution. How does a crop’s environment shape a food’s smell and taste? |Carolyn Beans |September 10, 2020 |Science News 
I happen to like watching Doctor Who, but if that’s not your cup of tea, that’s fine with me. The Universe Knows Right from Wrong - Issue 89: The Dark Side |Philip Goff |September 9, 2020 |Nautilus 
For all its economic and diplomatic might, though, China has its vulnerabilities — even with tea. The Rise of China’s Tea Diplomacy |Pallabi Munsi |September 9, 2020 |Ozy 
More recently, though, China has decided to upscale its tea outreach. The Rise of China’s Tea Diplomacy |Pallabi Munsi |September 9, 2020 |Ozy 
His team claimed he had ingested poison, probably through some tea he had drunk. Trump has long wanted to kill a Russia-Germany natural gas pipeline. Navalny’s poisoning could do it for him |David Meyer |September 8, 2020 |Fortune 
The smell of grilled meat mixes with the exotic wafts of cinnamon tea served with a mush of sweet brown dessert. The Photographer Who Gave Up Manhattan for Marrakech |Liza Foreman |January 6, 2015 |DAILY BEAST 
A year before he had similarly arrived with news of the Boston Tea Party. The Bars That Made America Great |Nina Strochlic |December 28, 2014 |DAILY BEAST 
Senseless bureaucracy is part of what spawned the Tea Party. Red Tape Is Strangling Good Samaritans |Philip K. Howard |December 27, 2014 |DAILY BEAST 
“I happened upon yak butter tea, a traditional high-energy food eaten by Tibetans,” Asprey says. Bulletproof Coffee and the Case for Butter as a Health Food |DailyBurn |December 27, 2014 |DAILY BEAST 
Adults prepare food and drink dark sweet tea on the doorsteps of their homes as they watch their children playing. The Brothers Who Ambushed ISIS |Mohammed A. Salih |December 27, 2014 |DAILY BEAST 
Being quieted by the Captain with a draught of cold tea, and made to sit down, the examination of the book proceeded. The Giant of the North |R.M. Ballantyne 
Afterwards we saw you once or twice at tea at the Ritz, and you took off your hat, so you must have remembered then. Rosemary in Search of a Father |C. N. Williamson 
Janet might have said before leaving: "Tea had better not wait too long--Hilda has to be down at Clayhanger's at half-past six." Hilda Lessways |Arnold Bennett 
The tea was all laid on tables in the garden, and the sausages were cooking over a fire made on the grounds. Music-Study in Germany |Amy Fay 
A trim maid then brought in the tea equipage, and Georgie did the honours with her usual unaffected grace. The Pit Town Coronet, Volume I (of 3) |Charles James Wills