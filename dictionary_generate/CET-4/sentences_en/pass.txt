San Francisco was the first city to pass one in 2006; since then, 14 other cities and three states have followed suit. Christie Blames Parents for Bad Economy |Monica Potts |January 3, 2015 |DAILY BEAST 
Congress is attempting to pass the buck on federal funding for education. The ‘No Child’ Rewrite Threatens Your Kids’ Future |Jonah Edelman |January 3, 2015 |DAILY BEAST 
“They just walk around, they ride in their patrol cars, and they just pass by,” he said. Ground Zero of the NYPD Slowdown |Batya Ungar-Sargon |January 1, 2015 |DAILY BEAST 
Typically, aircraft will work in pairs where the flight lead will make an initial pass to mark a target with rockets. New U.S. Stealth Jet Can’t Fire Its Gun Until 2019 |Dave Majumdar |December 31, 2014 |DAILY BEAST 
He goes into some detail into what it took to persuade voters to pass marriage equality at the ballot box in four states in 2012. The Real Story Behind the Fight for Marriage Equality |E.J. Graff |December 30, 2014 |DAILY BEAST 
Let the thought of self pass in, and the beauty of great action is gone, like the bloom from a soiled flower. Pearls of Thought |Maturin M. Ballou 
The riches of the unjust shall be dried up like a river, and shall pass away with a noise like a great thunder in rain. The Bible, Douay-Rheims Version |Various 
But men, through neglecting the rules of health, pass quickly to old age, and die before reaching that term. The Mediaeval Mind (Volume II of II) |Henry Osborn Taylor 
Madame and myself had just been regretting that we should have to pass the evening in this miserable hole of a town. Blackwood's Edinburgh Magazine, Volume 60, No. 372, October 1846 |Various 
He shall pass into strange countries: for he shall try good and evil among men. The Bible, Douay-Rheims Version |Various