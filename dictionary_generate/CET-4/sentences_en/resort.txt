You don’t have a huge demand of people who live in the Bahamas who also want to stay in a resort in the Bahamas. Airbnb CEO: The pandemic will force us to see more of the world, not less |Verne Kopytoff |September 7, 2020 |Fortune 
Bikers flood the area after ski resorts close and before temperatures spike. The Recreation Economy Isn't As Resilient As We Thought |Heather Hansman |August 29, 2020 |Outside Online 
He saw going to court as a final resort for patching up the law’s inadequacies, not a principal tool for establishing it in the first place. Inside China’s unexpected quest to protect data privacy |Tate Ryan-Mosley |August 19, 2020 |MIT Technology Review 
There were 1,471 votes cast out of 1,731 registered voters in the resort town. Rehoboth’s U-turn: Stan Mills elected mayor |Staff reports |August 9, 2020 |Washington Blade 
One thing I think the government should have done more of is to just become the payer of first resort. How to Prevent Another Great Depression (Ep. 421) |Stephen J. Dubner |June 11, 2020 |Freakonomics 
So filmmakers usually resort to a plot device to compensate for this absence. Why Can’t Movies Capture Genius? |Clive Irving |December 14, 2014 |DAILY BEAST 
The two scientific stories resort to the equivalent of Mathematics for Dummies andPhysics for Dummies. Why Can’t Movies Capture Genius? |Clive Irving |December 14, 2014 |DAILY BEAST 
Winter Resort operators are harnessing an unlikely source to power their operations: the sun. Solar Powered Ski Lift |The Daily Beast |November 24, 2014 |DAILY BEAST 
Berkshire East ski resort near the Vermont border, which has 44 trails, has taken this power-production drive a step further. Solar Powered Ski Lift |The Daily Beast |November 24, 2014 |DAILY BEAST 
For aesthetic reasons, ski resort operators try to limit the noise and infrastructure associated with producing power. Solar Powered Ski Lift |The Daily Beast |November 24, 2014 |DAILY BEAST 
Nevertheless, when once issued, they made unnecessary any resort to additional Bank of England notes. Readings in Money and Banking |Chester Arthur Phillips 
We must keep to the text and not resort to any foreign matter to help the feeble memory. Assimilative Memory |Marcus Dwight Larrowe (AKA Prof. A. Loisette) 
This method, too, once used in addition to what has been done by the pupil, will make a further resort to it unnecessary. Assimilative Memory |Marcus Dwight Larrowe (AKA Prof. A. Loisette) 
Then again amateurs may resort to the old French makers, some old English and the Tyrolean, which may be had cheaper still. Violins and Violin Makers |Joseph Pearce 
It is especially a winter resort, although the hotels keep open during the year. British Highways And Byways From A Motor Car |Thomas D. Murphy