In August, Trump filed a lawsuit to have his name removed from the casino and from the nearby, since-closed Trump Plaza. I Watched a Casino Kill Itself: The Awful Last Nights of Atlantic City’s Taj Mahal |Olivia Nuzzi |December 8, 2014 |DAILY BEAST 
One, of course, is the long-since established State of Israel. U.N. Adds New Name: "State of Palestine" |Ali Gharib |December 20, 2012 |DAILY BEAST 
But U.S. governments of both parties long-since opposed settlements, and the speech was far from combative. "Is John Kerry Good For Israel?" |Ali Gharib |December 17, 2012 |DAILY BEAST 
Obviously the 16 words [the since-discredited claim that Saddam tried to buy uranium] were bogus. Bush's Biggest Critics Gun for Obama |Lloyd Grove |October 28, 2010 |DAILY BEAST 
Aunt Gretchen has long-since lost the smooth silhouette for which the Nicholas women are noted. "And That's How It Was, Officer" |Ralph Sholto 
She drew Esmay after her down the draughty passage that led to the offices of the long-since-deserted dwelling-house. The Doomsman |Van Tassel Sutphen 
The long-since dismantled Abbey of Lanercost had its origin in a tragedy. Bygone Cumberland and Westmorland |Daniel Scott 
How can we reasonably expect that the road back to our long-since forsaken God is to be smooth, pleasant, velvet-covered. The Prodigal Returns |Lilian Staveley 
Since-355- thou must become as mortals are, and the slave of man, I will guard thee from all but the brave. Operas Every Child Should Know |Mary Schell Hoke Bacon