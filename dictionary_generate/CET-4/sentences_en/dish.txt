KitchenMate delivers new Meal-Pods once or twice a week, and teams can influence what gets delivered by voting on the dishes that they want. KitchenMate makes it easy to cook fresh meals at work |Anthony Ha |August 27, 2020 |TechCrunch 
No more long nights or gross sponges, trying to avoid dirty dishes in your sink. Small dishwashers that fit in almost any kitchen |PopSci Commerce Team |August 25, 2020 |Popular-Science 
If we have a hankering for a dish of pasta loaded with shaved truffles, we know where to go. Investors continue to push global stocks into record territory |Bernhard Warner |August 24, 2020 |Fortune 
Then there are others who can’t so much as wash the dishes or make a sales call without first devising an optimal plan of attack. A former student of “growth mindset” scholar Carol Dweck has identified a new mindset for success |Lila MacLellan |July 27, 2020 |Quartz 
A mouse is not a person, and brain cells in a dish do not make a brain. What will astronauts need to survive the dangerous journey to Mars? |Maria Temming |July 15, 2020 |Science News 
Caen was pitching and I was crouched behind the dish, catching. Mario Cuomo, Always Moving Us Toward the Light |Mike Barnicle |January 4, 2015 |DAILY BEAST 
Combine the beans and onion sauce in a 9x9-inch casserole dish and bake for 20 to 25 minutes. Make Carla Hall’s Crispy Shallot Green Bean Casserole |Carla Hall |December 27, 2014 |DAILY BEAST 
DISH is the first and only provider to offer the Netflix app. Four TV Shows We Can’t Wait to Return In 2015 |DISH |December 22, 2014 |DAILY BEAST 
DISH delivers a one-of-a-kind entertainment experience to every room of your home, wirelessly. Four TV Shows We Can’t Wait to Return In 2015 |DISH |December 22, 2014 |DAILY BEAST 
Add to that the DISH Anywhere app, and you have instant access to the program guide and the ability to record shows on the go. Four TV Shows We Can’t Wait to Return In 2015 |DISH |December 22, 2014 |DAILY BEAST 
A dish of toads of the largest and most repulsive variety used to be offered one by one to the big man's relatives and guests. The Pit Town Coronet, Volume I (of 3) |Charles James Wills 
This gift of rice was especially pleasing to the traveller, as no dish is held in higher honour in Korea. Our Little Korean Cousin |H. Lee M. Pike 
"Perry Thomas guessed he was an embezzler," said Tim, putting the last dish in the cupboard and sitting down to his pipe. The Soldier of the Valley |Nelson Lloyd 
When our nosegay is ready, we lay the cone with the flowers very carefully in a dish of water. The Nursery, July 1873, Vol. XIV. No. 1 |Various 
Your electro-plated butter-dish, or whatever it's going to be, will be simply flung back at you. First Plays |A. A. Milne