The line right before this is “With little tin horns and little toy drums.” The Most Confusing Christmas Music Lyrics Explained (VIDEO) |Kevin Fallon |December 24, 2014 |DAILY BEAST 
The raw materials— tin, tantalum, tungsten and gold—were dubbed “conflict minerals.” Aaron Rodgers Takes Aim at Congo’s ‘Blood Minerals’ War |John Prendergast |December 3, 2014 |DAILY BEAST 
One and all, they come shaking their tin cups at election time then run like the wind when a critical vote comes up. How the Lame Democrats Blew It |Goldie Taylor |November 5, 2014 |DAILY BEAST 
The more than 50,000 who reside in West Point live mostly in shacks made of zinc with rusted tin roofs. Meet the Liberian Girls Beating Ebola |Abby Haglage |October 29, 2014 |DAILY BEAST 
But his lumbering lurch toward the Ted Cruz tin-foil-hat convention should instead be an object lesson for Republicans to come. Will the GOP Get the Message in Kansas? |Ana Marie Cox |October 24, 2014 |DAILY BEAST 
And I will turn my hand to thee, and I will clean purge away thy dross, and I will take away all thy tin. The Bible, Douay-Rheims Version |Various 
Otherwise, a child's box of tin soldiers sent by post would have been just the thing for the Dardanelles landing! Gallipoli Diary, Volume I |Ian Hamilton 
He watched the man put some bread and milk in a tin pan, and set it down on the floor of the basket. Squinty the Comical Pig |Richard Barnum 
All the bamboo clappers, cocoanut shells, tin pans, and red flags that could be found were seized and put into use. Alila, Our Little Philippine Cousin |Mary Hazelton Wade 
Another manner of punishment consists in making them wear a tin mask, which is fastened with a lock behind. A Woman's Journey Round the World |Ida Pfeiffer