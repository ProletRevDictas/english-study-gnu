Germs like the flu virus can also hitch a ride on dust and other airborne particles that we shed. Dust can infect animals with flu, raising coronavirus concerns |Erin Garcia de Jesus |August 24, 2020 |Science News For Students 
If you’re a crime junkie, clear your schedule, because this one is going to take you on a ride. This Weekend: You’ll Never Bathe the Same Way Again |Joshua Eferighe |August 21, 2020 |Ozy 
Uber may also try to push more drivers to work for both its rides and food delivery services to reduce costs, Shmulik said. Will Uber and Lyft shut down in California? |Danielle Abril |August 18, 2020 |Fortune 
Along for the ride are the real people whose lives and livelihoods are, to some degree, linked to it. TikTok made him famous. Now he’s imagining a world without it |Abby Ohlheiser |August 14, 2020 |MIT Technology Review 
Meanwhile, ride demand has been rebounding from the sharp drop-off that accompanied the start of lockdowns across the US. Lyft still aims to be profitable by the end of next year |Michelle Cheng |August 13, 2020 |Quartz 
I told them it was back where I parked my car, so they offered me a ride. The 7-Year-Old Plane Crash Survivor’s Brutal Journey Through the Woods |James Higdon |January 7, 2015 |DAILY BEAST 
I mean, the reality of it was, I had to go out and get on a horse, and ride in, shoot the gun — how hard was that, right? The Story Behind Lee Marvin’s Liberty Valance Smile |Robert Ward |January 3, 2015 |DAILY BEAST 
“They just walk around, they ride in their patrol cars, and they just pass by,” he said. Ground Zero of the NYPD Slowdown |Batya Ungar-Sargon |January 1, 2015 |DAILY BEAST 
In “Sleigh Ride,” the narrator is painting a scene so perfect that it could be featured on an iconic Currier and Ives print. The Most Confusing Christmas Music Lyrics Explained (VIDEO) |Kevin Fallon |December 24, 2014 |DAILY BEAST 
My bike ride that mid-October day starts like so many others. You’re Never ‘Cured’ of an Eating Disorder |Carrie Arnold |December 20, 2014 |DAILY BEAST 
Possibly, he would not shy at such monstrosities after twenty miles of a lathering ride. The Red Year |Louis Tracy 
The other day an excursion was arranged to Sondershausen, a town about three hours' ride from Weimar in the cars. Music-Study in Germany |Amy Fay 
The truth is, it is not safe to trot down such mountains and hardly to ride down them at all. Glances at Europe |Horace Greeley 
The farmer told him it was six miles; "but," he added, "you must ride sharp, or you will get a wet jacket before you reach it." The Book of Anecdotes and Budget of Fun; |Various 
Coppy, in a tone of too-hastily-assumed authority, had told her over night that she must not ride out by the river. Kipling Stories and Poems Every Child Should Know, Book II |Rudyard Kipling