Citizens, perhaps, need to feel like they can communicate something to science. Anti-Fluoriders Are The OG Anti-Vaxxers |Michael Schulson |July 27, 2016 |DAILY BEAST 
Perhaps on his own nowadays, Epstein is trying his best to webmaster over a dozen URLs. Sleazy Billionaire’s Double Life Featured Beach Parties With Stephen Hawking |M.L. Nestel |January 8, 2015 |DAILY BEAST 
Democrats would be mistaken to underestimate Mike Huckabee, perhaps the strongest Republican presidential contender. Why This Liberal Hearts Huckabee |Sally Kohn |January 6, 2015 |DAILY BEAST 
Perhaps, as Dwight Garner wrote, Steinberg just needed an idea for a book. Was ‘The Book of Mormon’ a Great American Novel? |Stefan Beck |January 4, 2015 |DAILY BEAST 
Perhaps it always seems that way at the time, but surely we face our fair share right now. In 2015, Let’s Try for More Compassion |Gene Robinson |January 4, 2015 |DAILY BEAST 
“Perhaps you do not speak my language,” she said in Urdu, the tongue most frequently heard in Upper India. The Red Year |Louis Tracy 
There is, perhaps, in this childish suffering often something more than the sense of being homeless and outcast. Children's Ways |James Sully 
Those in whom the impulse is strong and dominant are perhaps those who in later years make the good society actors. Children's Ways |James Sully 
The sensation she had communicated to him then she communicated again, this time perhaps more strongly. Bella Donna |Robert Hichens 
When he gets quite large the boy will get tired of having him for a pet, and perhaps bring him back. Squinty the Comical Pig |Richard Barnum