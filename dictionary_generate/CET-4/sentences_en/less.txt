Submission is less a novel of ideas than a political book, and of the most subversive kind. Houellebecq’s Incendiary Novel Imagines France With a Muslim President |Pierre Assouline |January 9, 2015 |DAILY BEAST 
Back in New York, the slow pace and inward focus of her yoga practice was less fulfilling. How Taryn Toomey’s ‘The Class’ Became New York’s Latest Fitness Craze |Lizzie Crocker |January 9, 2015 |DAILY BEAST 
For many years afterward it was a never-ending topic of conversation, and is more or less talked of even to this day. New York’s Most Tragic Ghost Loves Minimalist Swedish Fashion |Nina Strochlic |January 8, 2015 |DAILY BEAST 
No one wants to align with less freedom at a time like this. Politicians Only Love Journalists When They're Dead |Luke O’Neil |January 8, 2015 |DAILY BEAST 
The pulps brought new readers to serious fiction, making it less intimidating with alluring art and low prices. How Pulp Fiction Saved Literature |Wendy Smith |January 8, 2015 |DAILY BEAST 
In less than ten minutes, the bivouac was broken up, and our little army on the march. Blackwood's Edinburgh Magazine, No. CCCXXXIX. January, 1844. Vol. LV. |Various 
He was too drowsy to hold the thought more than a moment in his mind, much less to reflect upon it. The Wave |Algernon Blackwood 
He did believe you, more or less, and what you said fell in with his own impressions—strange impressions that they were, poor man! Confidence |Henry James 
One would not have wanted her white neck a mite less full or her beautiful arms more slender. The Awakening and Selected Short Stories |Kate Chopin 
With childlike confidence he follows the advice of some more or less honest dealer. The Pit Town Coronet, Volume I (of 3) |Charles James Wills