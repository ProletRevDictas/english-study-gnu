The force that inspires defiant videos and top-of-their-lungs screeching tweens is, in fact, a media-shy 43-year old Swedish… man. Taylor Swift’s Secret Music Man: Max Martin, Elusive Hitmaker |Debra A. Klein |October 17, 2014 |DAILY BEAST 
There were housing projects, and some truly derelict hotels where the very-down-on-their-luck lived. How New York Could Get More Affordable Housing |Megan McArdle |March 13, 2013 |DAILY BEAST 
Issa is in the Republicans-lost-their-way camp, saying his party spent way too much in the Bush years. The GOP's New Top Cop |Howard Kurtz |November 27, 2010 |DAILY BEAST 
One daring exception to this lineup of standees-by-their men was the second wife of Newt Gingrich. Why Republicans Are Better at Affairs |Gail Sheehy |May 7, 2009 |DAILY BEAST 
I call that one: The Canadians-Are-Indeed-Nicer-and-Their-Side-of-the-Falls-More-Spectacular-Too One. My Four Weddings |Kara Swisher |November 10, 2008 |DAILY BEAST 
Again Rome had to gasp for breath, and again the two were fiercely locked-their corded arms as tense as serpents. A Cumberland Vendetta |John Fox, Jr. 
We may, however, settle it that Mr. Plan-others-their-work could put all the harvest he ever had in his waistcoat pocket! Broken Bread |Thomas Champness 
Such was the education of the Spartans with regard to one of the greatest of their-kings. The Social Contract &amp; Discourses |Jean-Jacques Rousseau 
Oak, hickory and beech—clean, vast, in-their-prime forest-men—with thorn and dogwood growing between. Child and Country |Will Levington Comfort 
In our desperate state, anything seemed fair in love or war with such hard, worth-their-weight-in-gold people. Everyman's Land |C. N. Williamson and A. M. Williamson