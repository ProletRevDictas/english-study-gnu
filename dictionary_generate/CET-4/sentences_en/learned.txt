That may be about to change though, thanks to a new approach that blends pre-learned skills on the fly to tackle new challenges. New Deep Learning Method Helps Robots Become Jacks-of-all-Trades |Edd Gent |December 14, 2020 |Singularity Hub 
Isolated lesbians learned that there were other women like them via books whose covers aimed to titillate heterosexual men. How Pulp Fiction Saved Literature |Wendy Smith |January 8, 2015 |DAILY BEAST 
The police learned that Kemp worked in a grocery on Decatur Avenue. Shot Down During the NYPD Slowdown |Michael Daly |January 7, 2015 |DAILY BEAST 
“Then I learned he can't spell and is a manager at a CPK,” she said. My Week on Jewish Tinder |Emily Shire |January 5, 2015 |DAILY BEAST 
His first language was Russian, then he learned Swedish, but chooses to perform in monosyllabic broken English. The Cult of Yung Lean: ‘I’m Building An Anarchistic Society From the Ground Up’ |Marlow Stern |January 4, 2015 |DAILY BEAST 
I asked her how her trainers, born and raised in Iran, have learned how to teach hip-hop. Iran’s Becoming a Footloose Nation as Dance Lessons Spread |IranWire |January 2, 2015 |DAILY BEAST 
He was rector of the university of Ilfeldt 40 years, and published several learned works. The Every Day Book of History and Chronology |Joel Munsell 
Decide about it, ye that are learned in the ethnographic distinctions of our race—but heaven defend us from the Bourbonnaises! Blackwood's Edinburgh Magazine, Volume 60, No. 372, October 1846 |Various 
He was learned, benevolent and pious, and author of several religious works. The Every Day Book of History and Chronology |Joel Munsell 
It was not an exalted niche to fill in life, but at least she had learned to fill it to perfection, and her ambitions were modest. Ancestors |Gertrude Atherton 
He was the author of many learned theological works and controversial publications. The Every Day Book of History and Chronology |Joel Munsell