Without it, they say, the disease would surely kill her within two years. Should Teens Have The Right To Die? |Brandy Zadrozny |January 8, 2015 |DAILY BEAST 
Within a few swipes, I was already feeling that burst of romantic optimism you need the first day of the (Christian) new year. My Week on Jewish Tinder |Emily Shire |January 5, 2015 |DAILY BEAST 
Her travel clique has been known to arrive at an airport, bags packed, passport-in-hand, within hours of spotting a deal. ‘We Out Here’: Inside the New Black Travel Movement |Charlise Ferguson |January 4, 2015 |DAILY BEAST 
Within minutes, it seems, of the disclosures of these tragic events, large numbers of people chose a side and stuck to it. In 2015, Let’s Try for More Compassion |Gene Robinson |January 4, 2015 |DAILY BEAST 
Whatever happened overtook them both within a minute or so of that altitude change request, and they were never heard from again. Flight 8501 Poses Question: Are Modern Jets Too Automated to Fly? |Clive Irving |January 4, 2015 |DAILY BEAST 
You need but will, and it is done; but if you relax your efforts, you will be ruined; for ruin and recovery are both from within. Pearls of Thought |Maturin M. Ballou 
Within the past thirty years civilization has rapidly taken possession of this lovely region. Among the Sioux |R. J. Creswell 
At present, Louis was too self-absorbed by the struggles within him, to look deep into what was passing around him. The Pastor's Fire-side Vol. 3 of 4 |Jane Porter 
The Senora Moreno's heart broke within her, when those words passed her lips to her adored Felipe. Ramona |Helen Hunt Jackson 
General Santa Anna is within a mile of us with fifteen hundred men. Blackwood's Edinburgh Magazine, No. CCCXXXIX. January, 1844. Vol. LV. |Various