About a dozen attendees, socially distanced across the backyard, listened. Election live updates: Trump returns to Wisconsin; Biden to face live audience at town hall |Colby Itkowitz, Felicia Sonmez, John Wagner |September 17, 2020 |Washington Post 
The real skill is pattern recognition over time of who is actually useful for good information — knowing who to listen to and for what. Startup founders must overcome information overload |Walter Thompson |September 17, 2020 |TechCrunch 
Instead, I took to spending whole days in the wetland, watching and listening. How to hunt for star-nosed moles (and their holes) |Kenneth Catania |September 15, 2020 |Popular-Science 
As always, thanks for the feedback and thanks for listening. What if Your Company Had No Rules? (Bonus Episode) |Maria Konnikova |September 12, 2020 |Freakonomics 
Even Spotify only shows you speed options when you’re listening to podcasts. Read, watch, and listen to things faster than ever before |David Nield |September 9, 2020 |Popular-Science 
But if you listen to our leaders, they weren't the real targets here. Politicians Only Love Journalists When They're Dead |Luke O’Neil |January 8, 2015 |DAILY BEAST 
What an amazing thing to be able to listen to any music you want, a whole world of bands. Belle & Sebastian Aren’t So Shy Anymore |James Joiner |January 7, 2015 |DAILY BEAST 
One of the rites of passage for every young political reporter is to listen to the elders tell stories about campaigns past. The World’s Toughest Political Quiz |Jeff Greenfield |December 31, 2014 |DAILY BEAST 
But then I thought about the feedback I get from fans, yes we do listen to you, and thought why not? Porn Stars on the Year in Porn: Drone Erotica, Belle Knox, and Wild Sex |Aurora Snow |December 27, 2014 |DAILY BEAST 
Why would they listen to the radio when they can see the outside world? North Korea’s Secret Movie Bootleggers: How Western Films Make It Into the Hermit Kingdom |Lizzie Crocker |December 22, 2014 |DAILY BEAST 
But he walked up and down the room and forced himself to listen, though he could scarcely bear it, I could see. Music-Study in Germany |Amy Fay 
Shopkeepers ran out of their shops, housewives craned over their balconies to listen to him. The Joyous Adventures of Aristide Pujol |William J. Locke 
I am always astonished, amazed and delighted afresh, and even as I listen I can hardly believe that the man can play so! Music-Study in Germany |Amy Fay 
No one would listen to him but old Monsieur Farival, who went into convulsions over the droll story. The Awakening and Selected Short Stories |Kate Chopin 
I knew, further, that Sunday could not be a day of rest for her, for of all his people she would have to listen to his preaching. The Soldier of the Valley |Nelson Lloyd