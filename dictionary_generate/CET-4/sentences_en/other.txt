Fairylands is a name that conjures up a dreamy, other-worldly place, somewhere to escape the cares of a busy life. How John Lennon Rediscovered His Music in Bermuda |The Telegraph |November 3, 2013 |DAILY BEAST 
She means well, and she's trying, clearly, to be some more real-world version of her other-worldly sister. Is Pippa Middleton Just Very Badly Dressed? |Tom Sykes |September 27, 2013 |DAILY BEAST 
This can lead to an other-than-honorable discharge, a bad-conduct discharge, or a dishonorable discharge. From PTSD to Prison: Why Veterans Become Criminals |Matthew Wolfe |July 28, 2013 |DAILY BEAST 
And with all this change, so curiously parallel to that of the Other-world, goes too the same inevitable change in ideals. David's Book Club: The Souls of Black Folk |David Frum |May 5, 2013 |DAILY BEAST 
We would talk off and on, just checking-up-on-each-other kind of thing. Lennay Marie Kekua and Manti Te’o: A Scandal Explained |Andrew Carter |January 17, 2013 |DAILY BEAST 
His name was Lefty something-or-other, and he was about the sneakiest stool the department had. Hooded Detective, Volume III No. 2, January, 1942 |Various 
Rodney is thoroughly and comfortably this-worldly; Michael is—other-worldly! Jane Journeys On |Ruth Comfort Mitchell 
In conclusion, we must consider an offshoot of other-world ideas—the belief in the transmigration of souls. Elements of Folk Psychology |Wilhelm Wundt 
As a matter of fact, the retributive idea is far from being implicated with other-world hopes. Elements of Folk Psychology |Wilhelm Wundt 
Intellectualism may not always be so clearly other-worldly as Plato shows himself to be in this passage. The Behavior of Crowds |Everett Dean Martin