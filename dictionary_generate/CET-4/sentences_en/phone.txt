I reached him by phone in the Chicago suburb where he lives with his younger brother and parents. My mother and her friends couldn’t get coronavirus vaccine appointments, so they turned to a stranger for help. He’s 13. |Greg Harris |February 12, 2021 |Washington Post 
And, if you prefer a human, associates are available by phone. Replacing pieces of flatware or china can be a challenge. Here’s how to track them down. |Laura Daily |February 11, 2021 |Washington Post 
He takes any early work phone calls on foot, while exploring new areas around the city. The ‘garbage guy’ walks 12 miles a day around D.C. picking up trash: ‘I’ll pick up pretty much anything.’ |Sydney Page |February 11, 2021 |Washington Post 
That hasn’t been enough to drive Kramon away from her phone. The loneliness of an interrupted adolescence |Ellen McCarthy |February 11, 2021 |Washington Post 
I was at the counter picking over the rice when my phone finally buzzed with a text. Love, Loneliness, and the Chicken in My Freezer |Elazar Sontag |February 9, 2021 |Eater 
“We talked about the science the whole time the other day,” Krauss told The Daily Beast in a phone interview. Sleazy Billionaire’s Double Life Featured Beach Parties With Stephen Hawking |M.L. Nestel |January 8, 2015 |DAILY BEAST 
He added, "I have not had this many phone calls since the shutdown of the government, truthfully." Democrats Accidentally Save Boehner From Republican Coup |Ben Jacobs, Jackie Kucinich |January 6, 2015 |DAILY BEAST 
Now it can't open on my phone due to what appears to be software incompatibility. My Week on Jewish Tinder |Emily Shire |January 5, 2015 |DAILY BEAST 
Eventually Morrow was released with no money, vehicle, or phone. Are Police Stealing People’s Property? |Joan Blades, Matt Kibbe |January 2, 2015 |DAILY BEAST 
My wife was talking to her on the phone, and I just kinda found the courage to ask her. Deer Tick's John McCauley on Ten Years in Rock and Roll |James Joiner |January 2, 2015 |DAILY BEAST 
He crossed to the wall phone and gently removed the receiver from its hook and held it to his ear. Hooded Detective, Volume III No. 2, January, 1942 |Various 
The phone call wouldn't take long and he'd be coming back any minute now. The Man from Time |Frank Belknap Long 
From further back, through an open doorway, a girl's voice was shrieking for the police over the phone. Hooded Detective, Volume III No. 2, January, 1942 |Various 
And Lamb said a four-letter word after he had hung up and laughed out loud in the phone booth. Hooded Detective, Volume III No. 2, January, 1942 |Various 
When a phone jangled down the corridor, his eyes bugged right at the door. Hooded Detective, Volume III No. 2, January, 1942 |Various