In short, he moved closer to Pelosi’s position after a monthlong showdown. Trump moves closer to Pelosi in economic aid talks, and House speaker must decide next move |Rachael Bade, Erica Werner |September 17, 2020 |Washington Post 
Antebellum, the film, has its roots in a short story the duo wrote in October 2017. ‘Antebellum’ tackles the past head on in an effort to ‘move forward’ |radmarya |September 17, 2020 |Fortune 
When a QED calculation leads to an infinite sum, cut it short. How Mathematical ‘Hocus-Pocus’ Saved Particle Physics |Charlie Wood |September 17, 2020 |Quanta Magazine 
Rapid short-term cooling following the Chicxulub impact at the Cretaceous–Paleogene boundary. Dawn of the Heliocene - Issue 90: Something Green |Summer Praetorius |September 16, 2020 |Nautilus 
For best results, cut a short section of FireCord and remove the red strand. This essential survival tool can save your life 10 different ways |By Tim MacWelch/Outdoor Life |September 15, 2020 |Popular-Science 
As this list shows, punishments typically run to a short-ish jail sentence and/or a moderately hefty fine. In Defense of Blasphemy |Michael Tomasky |January 9, 2015 |DAILY BEAST 
The precision it took to craft such a cohesive, wholly compelling work over 12 years is nothing short of remarkable. Oscars 2015: The Daily Beast’s Picks, From Scarlett Johansson to ‘Boyhood’ |Marlow Stern |January 6, 2015 |DAILY BEAST 
In short, fatherhood gets little attention in policy debates. How Good Dads Can Change the World |Gary Barker, PhD, Michael Kaufman |January 6, 2015 |DAILY BEAST 
But the qualities Mario Cuomo brought to public life—compassion, integrity, commitment to principle—remain in short supply today. President Cuomo Would’ve Been a Lion |Jonathan Alter |January 2, 2015 |DAILY BEAST 
In short, we found ways to couch messages of failure or inadequacy. Random Hook-Ups or Dry Spells: Why Millennials Flunk College Dating |Ellie Schaack |January 1, 2015 |DAILY BEAST 
Sometimes the stems are quite bare; on other occasions they are partly branched; in any case the branches are short. How to Know the Ferns |S. Leonard Bastin 
Many of their cannon balls that fell far short of us, were collected and returned to them with powerful effect. Blackwood's Edinburgh Magazine, No. CCCXXXIX. January, 1844. Vol. LV. |Various 
He was tall and of familiar figure, and the firelight was playing in the tossed curls of his short, fair hair. The Bondboy |George W. (George Washington) Ogden 
They are ovoid in shape, and lie in pairs, end to end, often forming short chains. A Manual of Clinical Diagnosis |James Campbell Todd 
And since he was a very fast runner—for short distances—he met Grandfather Mole just as the old chap was crawling up the bank. The Tale of Grandfather Mole |Arthur Scott Bailey