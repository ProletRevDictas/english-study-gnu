We have thousands of users who identify themselves as transgendered and they are welcome members of the Grindr community. Grindr’s Trans Dating Problem |David Levesley |January 9, 2015 |DAILY BEAST 
Grindr introduced the feature themselves in October the same year and called it ‘tribes.’ Grindr’s Trans Dating Problem |David Levesley |January 9, 2015 |DAILY BEAST 
Sadly, it appears the American press often doesn't need any outside help when it comes to censoring themselves. Politicians Only Love Journalists When They're Dead |Luke O’Neil |January 8, 2015 |DAILY BEAST 
Parents are talking about it, schools are talking about it, even kids themselves are talking about it. How Skinny Is Too Skinny? Israel Bans ‘Underweight’ Models |Carrie Arnold |January 8, 2015 |DAILY BEAST 
As for the federal authorities, they have made themselves available but the clergy have not requested special protection. Mexico’s Priests Are Marked for Murder |Jason McGahan |January 7, 2015 |DAILY BEAST 
This mania for correction shows itself too in relation to the authorities themselves. Children's Ways |James Sully 
All our intelligent students will insist upon learning what they can of these discussions and forming opinions for themselves. The Salvaging Of Civilisation |H. G. (Herbert George) Wells 
Her eyes, for a moment, fixed themselves with a horrid conviction of a wide and nameless treachery. The Pastor's Fire-side Vol. 3 of 4 |Jane Porter 
His lordship retired shortly to his study, Hetton and Mr. Haggard betook themselves to the billiard-room. The Pit Town Coronet, Volume I (of 3) |Charles James Wills 
All the operations of her brain related themselves somehow to to-morrow afternoon. Hilda Lessways |Arnold Bennett