研究人员在6月25日的《科学进步》杂志上报道，这种新酵母每升处理过的玉米秸秆能够产生100克以上的乙醇，其效率与使用玉米粒制造生物燃料的标准工艺相当。一种经过调整的酵母可以从玉米秸秆和收获的其他残渣中提取乙醇| Nikk Ogasa | 2021年7月7日|科学新闻
就像我总是根据背包能装多少公升来思考背包一样，因为我可以很容易地想象一个Nalgene水瓶是什么样子，我可以想象一个背包里装40或60个水瓶。户外事物，但通过不熟悉的测量|布伦丹·伦纳德| 2021年6月22日|户外在线
他形容自己独自一人在山里的时光——没有手电筒，背包里只有一公升水和一根电源棒——是可怕的。一个徒步旅行者迷路了，走投无路。一个有着不同寻常爱好的陌生人救了他|悉尼页| 2021年4月22日|华盛顿邮报
在海平面上煮沸一公升水需要一分钟，但炉子也提供了良好的热量调节，这样你就不会烧咸肉了。断电时如何保持舒适| Wes Siler | 2021年3月5日|户外在线
一公升的污水携带着每个共用下水道系统的人排放到厕所的病毒残留物，提供了数千人，甚至数百万人健康状况的读数。快速传播的冠状病毒变种正在美国下水道中出现|斯蒂芬妮·阿内特| 2021年2月8日|麻省理工技术评论
它是在20世纪80年代装瓶的，他花了34000日元（约合5470美元）买了半升的瓶子。中国正在用虎骨酿造葡萄酒| Brendon Hong | 2014年7月22日| DAILY BEAST
格雷西收拾好她的惠灵顿靴子、长袖衬衫和裤子，带着一个5升的阿勒姆比克蒸馏器飞往加拉加斯。2014年7月8日，印第安纳州的琼斯遇到了一位大胆的杜松子酒酿酒师安·宾洛特
专制只是俄罗斯人的一个坏习惯，就像一天抽三包烟，喝一升伏特加。俄罗斯历史站在我们这一边：普京肯定会自杀| P.J.O'Rourke | 2014年5月11日|每日野兽
一瓶6升的1992年“尖叫的鹰”葡萄酒创下了单瓶葡萄酒最高价格的世界纪录。是的，女人可以酿出好酒| Jordan Salcito | 2014年3月22日| DAILY BEAST
此外，一升利乐包装的重量不到一磅。认真对待盒装酒：它不再只适合流浪汉和青少年| Jordan Salcito | 2014年3月15日| DAILY BEAST
这种材料被彻底浸渍，并用乙醚装入10升的瓶子中。有毒常春藤植物的一些成分：（Rhus toxidendron）| William Anderson Syme
因此，一公升中给定物质的克数除以其分子量表示其浓度。定性化学分析的要素，第一卷，第一部分和第二部分|朱利叶斯·斯蒂格利茨
这种表示浓度的形式在许多细节上优于摩尔/升形式。定性化学分析的要素，第一卷，第一部分和第二部分|朱利叶斯·斯蒂格利茨
在这种溶液中，银离子的浓度降低到每升8E24克离子。定性化学分析的要素，第一卷，第一部分和第二部分|朱利叶斯·斯蒂格利茨
里面只有一个半升的瓶子，蜡封的，里面有一种深红棕色的糖浆。猎人巡逻队|亨利·比姆·派珀和约翰·J·麦奎尔