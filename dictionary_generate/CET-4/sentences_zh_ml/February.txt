令人惊讶的是，在二月份，美国人感到难以置信的乐观。道指的重大科技改造能否最终推动蓝筹股指数突破30000点|伯恩哈德·华纳| 2020年8月25日|财富
7月，在房屋委员会确认布朗失业后，首都和公共机构询问房屋委员会，为什么官员要求布朗在2月份支付全额租金。她因未欠房租而被起诉。花了七个法庭日期证明她是对的|Danielle Ohl，首都公报，以及Talia Buford和Beena Raghavendran，ProPublica | 2020年8月25日| ProPublica
尽管标准普尔500指数攀升至新高，但该公司股价目前仍较2月份的首次公开发行价格低38%。卡斯珀的首席执行官正在申请一家空白支票公司。他打算买什么|沈露辛达| 2020年8月19日|财富
例如，彭博社媒体与2月份公布的信息捆绑已经解除我们可以做很多很酷的事情：出版商对订阅驱动捆绑包的兴趣正在酝酿| Max Willens | 2020年8月11日| Digiday
尽管整个1月和2月都保证欧洲正在成功遏制病毒，但它已经悄悄地控制了欧洲大陆，官员们开始意识到一场更大的危机正在逼近。欧洲内部对冠状病毒爆发的反应|劳拉·马戈蒂尼| 2020年7月16日|石英
今年2月，斯洛伐克将举行全民公决，决定是否将婚姻定义为男女之间的结合只有上帝才能阻止同性恋婚姻| Tim Mak | 2015年1月6日|每日野兽
今年2月，另一名成员费尔南多·冈萨雷斯被送回古巴。关于古巴间谍、婴儿和电影制作人：古巴五人的奇怪故事|尼娜·斯特罗奇利奇| 2014年12月28日|每日野兽
二月份，他失去了一个亲密的朋友菲利普·西摩·霍夫曼。与伊桑·霍克（Ethan Hawke）的咖啡谈话：詹妮弗·劳伦斯（Jennifer Lawrence）和比尔·克林顿（Bill Clinton）的小便器交换《马洛·斯特恩》（Marlow Stern）2014年12月27日《每日野兽》（DAILY BEAST）
到了2月底，希特勒巩固了他的权力，他做出了决定：他不会留下来。与希特勒较量的天主教哲学家|约翰·亨利·克罗斯比| 2014年12月26日|每日野兽
尼日利亚的每一个举动背后都隐藏着政治紧张，明年2月，下一任总统候选人将被提名。博科圣地恐怖的新面孔：少女|尼娜·斯特罗奇利奇| 2014年12月13日|每日野兽
这项工作于1882年6月开始，纪念石于次年2月15日铺设。肖厄尔伯明翰词典|托马斯·T·哈曼和沃尔特·肖厄尔
最后，他到达了迪耶普，在那里逗留了一段时间后，于2月26日出海。《耶稣会关系与同盟文献》，第二卷：阿卡迪亚，1612-1614
2月18日，法国人占领了河左岸的郊区，从而将内城置于两次大火之间。拿破仑元帅R.P.邓恩·帕蒂森
1303-1304年2月科门及其追随者投降后，他全身心地投入到对华莱士的追捕中。罗伯特国王布鲁斯·A·F·穆里森
二月初，他应邀到贝蒂·拉德纳（Betty Lardner）也是客人的一所房子里打猎一周。离奇的故事|各种各样