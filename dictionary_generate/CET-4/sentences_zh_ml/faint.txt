你仍然可以看到它们经过，但它们几乎都很微弱。卫星巨型星座有可能永远毁掉天文学|尼尔·帕特尔| 2020年9月2日|麻省理工学院技术评论
在侧面，一团稀薄的预混合火焰看起来像一缕微弱的火焰。四种类型的火焰合力形成了这一怪异的“蓝色漩涡”|艾米莉·科诺弗| 2020年8月12日|科学新闻
研究人员已经开始使用LOFAR进行粗略的法拉第旋转测量，但是望远镜很难分辨出极其微弱的信号。隐藏的磁性宇宙开始出现| Natalie Wolchover | 2020年7月2日| Quanta杂志
通过训练计算机识别这些微弱的隆隆声，科学家们不仅能够识别地震背后可能的罪魁祸首，还能够追踪这些神秘的群体如何在空间和时间上通过复杂的断层网络传播。机器学习帮助揭开加利福尼亚地震群的神秘面纱|卡罗琳·格拉姆林| 2020年6月18日|科学新闻
当微弱的光线照射到另一面时，它已经被分割成不同的颜色。解说员：彩虹、雾弓及其怪异的表亲| Matthew Cappucci | 2020年5月1日|学生科学新闻
她的声音刺耳，回答完问题后，她停顿了一下，好像要晕倒似的。危险冠军朱莉娅·柯林斯的大脑感觉像糊状物|苏杰·库马尔| 2014年11月20日|每日野兽
我看到了他曾经是一个凶猛的保护者的微弱而甜美的光芒。没有人会输给癌症|杜什卡·萨帕塔| 2014年10月8日|每日野兽
但这一次，我可以通过滔滔不绝的话语清楚地听到歇斯底里的轻微嘎嘎声，这表明某个地方的螺丝松动了。书堆：我认识的感恩之死|埃德·麦克拉纳汉| 2014年8月30日|每日野兽
大约一个小时后，他听到冰箱里传来微弱的敲击声，于是打开了门。好莱坞大小丑罗宾·威廉姆斯于2014年8月12日《每日野兽》去世，享年63岁
到处都是一本好书，但不是一本给胆小鬼看的书。盖世太保仍然为邪恶设置了门槛|詹姆斯·A·沃伦| 2014年7月13日|每日野兽
昏暗的烛光在一个沉重的镀金檐口上闪烁着，这座檐口也持续着暴力。将死|约瑟夫·谢里丹·勒法努
这是很难描述的——有点严厉，有点狂野，有点强调野蛮的窥视。波浪|阿尔杰农·布莱克伍德
他好奇地拱起眉毛看着我，嘴角隐隐流露出敌意。山谷战士|纳尔逊·劳埃德
淋巴细胞的细胞质一般为罗宾蛋蓝；大的单核细胞可能有淡淡的蓝色。临床诊断手册|詹姆斯·坎贝尔·托德
Wright氏染色使这些细胞在病情温和时呈淡蓝色，严重时呈深蓝色。临床诊断手册|詹姆斯·坎贝尔·托德