它了解每个设备的独特行为、其操作环境的怪癖以及如何与其他设备交互以防止恶意和异常使用，同时提供分析以提高性能。前NSA员工提供的近地点基础设施安全解决方案进入公共测试版| Ron Miller | 2020年9月17日| TechCrunch
如果你在旧环境中踏水，你真的会在新环境中挣扎“我们在招聘记者”：Insider Inc.在新加坡推出第三个全球新闻中心（Lucinda Southern，2020年9月17日，Digiday
对于分离主义者来说，这是一个非常艰难的生活环境集成商和分离者：管理者如何帮助这两类远程工作者在流感大流行中生存| Jen Wieczner | 2020年9月16日|财富
其他的名字已经浮出水面来描述我们是如何改造我们的环境的。第90期：绿色的东西|夏季总督府| 2020年9月16日|鹦鹉螺
美国宇航局科学家发起的一项名为“长寿命原位太阳系探索”的计划，要求建造能够承受金星恶劣环境长达60天的电子设备和硬件。我们需要尽快去金星|尼尔·帕特尔| 2020年9月16日|麻省理工技术评论
一般来说，遗传因素本身并不能导致饮食失调，布利克指出，环境仍然起着作用。多瘦就是太瘦？以色列禁止“体重不足”的模特| Carrie Arnold | 2015年1月8日| DAILY BEAST
在一个时间就是金钱的环境中，与一名护送人员搭讪可能是明智之举。有事业心的女性转向男性护卫，享受无条件的乐趣和（可能）性生活|极光雪| 2015年1月3日|每日野兽
这并不反映人们对更好的环境缺乏兴趣。是时候让杜鲁门民主党人回归了|乔尔·科特金| 2014年12月21日|每日野兽
在目前的环境下，你是否对整个公共教育危机更加悲观？霍华德·富勒博士的不公正教育|坎贝尔·布朗| 2014年12月21日|每日野兽
一位教育专家谈论种族关系、政治环境以及可以做些什么来改善现状。霍华德·富勒博士的不公正教育|坎贝尔·布朗| 2014年12月21日|每日野兽
但在大多数情况下，即使是工业和捐赠也无力抵挡习俗的惯性和环境的沉重负担。社会正义的未解之谜|斯蒂芬·利科克
幻象本身就是那种使人超越环境的神圣不满的结果。社会正义的未解之谜|斯蒂芬·利科克
人类如果受到环境的青睐，很容易每二十五年就翻一番。社会正义的未解之谜|斯蒂芬·利科克
这是他的故乡，一个完全适合他独特才能的环境。革命前夕|卡尔·贝克尔
环境改变了他的本性：环境由他本性之外的力量的作用组成。上帝与我的邻居罗伯特·布拉奇福德