“这是我们的制度，允许总统行使他的赦免权，”他说。弗吉尼亚州联邦检察官接近比尔·巴尔下台|雷切尔·韦纳| 2021年1月5日|华盛顿邮报|
尽管关键的证词对他不利，德怀尔仍然声称自己无罪，甚至要求时任总统罗纳德·里根赦免。R.Budd Dwyer | Fiona Zublin | 2021年1月4日| Ozy的民谣
正如圣地亚哥和加利福尼亚州共和党前主席罗恩·尼林（Ron Nehring）所说，他不应该得到赦免。政治报道：雪莉·韦伯的鞋子将填补|斯科特·刘易斯| 2020年12月26日|圣地亚哥之声
她后来被判犯有反人类罪，并于2018年获得总统赦免。为什么认为女性有能力……犯下可怕的暴行很重要| LGBTQ编辑| 2020年11月21日|没有直接的新闻
这个问题可能会出现，也可能不会出现，但这需要对赦免权的范围进行法律分析。巴雷特确认听证会第三天：巴雷特拒绝说明将移民子女与父母分开是否是错误的|德里克·霍金斯、金升民、安·马里莫、卡隆·德米尔吉安| 2020年10月14日|华盛顿邮报
沃尔伯格于11月26日向马萨诸塞州州长递交了赦免申请。洛杉矶警察局基金会：马克·沃尔伯格将成为一名优秀的后备警察|阿萨温·苏伯桑| 2014年12月9日|每日野兽
我们获得了当时的卡尔扎伊总统前所未有的赦免，古尔纳兹获释。我们在阿富汗没有结束|金伯利杂色| 2014年12月5日|每日野兽
政府应该请求艾伦·图灵的亲属原谅他们对他如此恶劣的对待！查尔斯在泰温·兰尼斯特（Tywin Lannister）的S5回归（一部“权力游戏”电影）和性感的彼得·丁克拉奇（Peter Dinklage）|马洛·斯特恩| 2014年11月18日|每日野兽（DAILY BEAST）上跳舞
“这里没有任何牛排，请原谅这个双关语，”琴说。冒险摄影师Jimmy Chin:挑战理性、身体和创造性|奥利弗·琼斯| 2014年10月6日|每日野兽
对于我们所有的罪，愿使宽恕成为可能的力量原谅我们，原谅我们，使赎罪成为可能。犹太人和非犹太人需要为美国和以色列的罪行忏悔|拉比迈克尔·勒纳| 2014年9月24日|每日野兽
当父亲结束时，他刺伤了他的妻子，告诉她忏悔自己的罪行，并向上帝忏悔，上帝会原谅她。菲律宾群岛，1493-1898年，第二十卷，1621-1624年
如果陛下接受我的回答，请原谅我回答的时间比问题的时间长。菲律宾群岛，1493-1898年，第二十卷，1621-1624年
非常感谢您继续给予我帮助，并请原谅我经常打扰您。理查德·特雷维希克生平，第二卷（共2卷）|弗朗西斯·特雷维希克
如果一个人伤害了另一个人，赦免权应该属于受伤的人。上帝与我的邻居罗伯特·布拉奇福德
窗帘上的灰尘，请原谅我暗示了这样一件事，把我的喉咙烤得干裂。《觉醒》与短篇小说选集《凯特·肖邦》