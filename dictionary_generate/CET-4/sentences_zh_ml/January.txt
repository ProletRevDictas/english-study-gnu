今年1月，当合作正式开始时，这位朋友向他的新导演赠送了原创歌曲和提纲《老魂记》通过Zoom | Patrick Folliard | 2021年2月26日| Washington Blade创造亲密交流
记者汤姆·舍伍德（Tom Sherwood）首先在推特上发布了布列斯纳汉辞职的消息，布列斯纳汉是在1月份提交的。哥伦比亚特区彩票执行董事于2021年2月25日（星期五）辞职《华盛顿邮报》
尽管冠状病毒感染率有所下降，但1月份疲弱的就业数据似乎显示复苏放缓。最低工资辩论中缺失的部分|科琳·杜迪| 2021年2月25日|华盛顿邮报
自1月份达到峰值以来，这些关键指标一直朝着积极的方向发展。新的研究表明加州冠状病毒变种更容易传播| Joel Achenbach，Carolyn Y.Johnson | 2021年2月24日|华盛顿邮报
港口正在等待联邦政府对他们在一月份提交的申请的回复。一个环保的新公路项目？（提示：它在海里）|麦肯齐·埃尔默| 2021年2月24日|圣地亚哥之声
然而，在以色列，一项新法律于1月1日生效，禁止使用体重不足的车型。多瘦就是太瘦？以色列禁止“体重不足”的模特| Carrie Arnold | 2015年1月8日| DAILY BEAST
切里夫于2005年1月在巴黎被捕，当时他正准备与一名名叫Thamer Bouchnak的男子一起登上一架飞往大马士革的飞机。法国哀悼和狩猎| Nico Hines，Christopher Dickey | 2015年1月8日|每日野兽
安德鲁仍计划于1月21日代表英国政府飞往瑞士达沃斯参加世界经济论坛。从花花公子王子到肮脏的老人|汤姆·赛克斯| 2015年1月5日|每日野兽
她的重点将放在1965年1月至3月的三个月内，这三个月催生了《投票权法案》。金博士前往好莱坞：《塞尔玛》有缺陷的历史|加里·梅| 2015年1月2日|每日野兽报
一月份，上诉听证会将决定他是否有资格获得定罪后救济。和连续剧杰伊的交易？他生气了，弄乱了我们的时间表|艾米丽郡| 2014年12月31日|每日野兽
他准许她继续留在家里，直到她见习的那一年；但那一年必须到来，它将从明年1月开始。《牧师的火面》第3卷，共4卷|简·波特
1813年1月10日，传来了普鲁士人实际上已经投敌的消息。拿破仑元帅R.P.邓恩·帕蒂森
纽霍尔山曾经是最受欢迎的地点，1817年1月22日在那里举行了第一次会议。肖厄尔伯明翰词典|托马斯·T·哈曼和沃尔特·肖厄尔
1611年1月26日，我们在最不利的季节从迪耶普启航。《耶稣会关系与同盟文献》，第二卷：阿卡迪亚，1612-1614
1820年1月，他恢复了元帅的bton和其他荣誉，进入了政治领域。拿破仑元帅R.P.邓恩·帕蒂森