11月的发行不仅会受到奥巴马读者的欢迎，也会受到书商和其他出版商的欢迎，他们预计，对“应许之地”的巨大需求将提高每个人的销量。巴拉克·奥巴马期待已久的回忆录的第一卷终于有了发行日期，《瑞秋·金》2020年9月17日《财富》
新加坡中心本身将以程序化的方式开始销售广告，但业内人士预计，这些全球广告活动将更多地采用本地变体“我们在招聘记者”：Insider Inc.在新加坡推出第三个全球新闻中心（Lucinda Southern，2020年9月17日，Digiday
无论您所在的行业如何，您都应该寻找预测和满足客户需求的方法。如何在流感大流行期间推动必要的数字创新| Nick Chasinov | 2020年9月16日|搜索引擎观察
威斯康星州的最新一轮调查备受期待，此前共和党全国代表大会主要关注法律和秩序信息，布莱克枪击案和随后的抗议活动也随之而来。美国广播公司威斯康星州调查显示，拜登以微弱优势击败特朗普、丹·巴尔兹、艾米莉·古斯金（2020年9月16日）《华盛顿邮报》
为了与活动的“时光飞逝”主题保持一致，正如人们普遍预期的那样，苹果推出了两款新款smartwatch。《财富》杂志于2020年9月15日在苹果公司的“时光飞逝”活动上公布了一切
这些见解和发现有助于百事可乐预测不断变化的消费者前景，而不是作出反应。配料创新科学| | 2014年12月15日|每日野兽
一位国防部官员说：“我们预计，当我们第一次开始训练时，我们不会有那么多（新兵）开始训练。”。美国甚至还没有开始训练叛军对抗伊斯兰国| Tim Mak | 2014年11月25日| DAILY BEAST
预计未来会有更多的土库曼人和其他人加入战斗并不难。伊朗政权最担心的核协议|贾瓦德·哈德姆| 2014年11月22日|每日野兽
官员们解释说，这些锡克教徒已经被私刑处死，辛格应该为自己预测同样的命运。随着印度大屠杀30周年纪念日的到来，锡克教徒在美国找到了安全| Simran Jeet Singh | 2014年10月31日| DAILY BEAST
但凶手显然没有预料到接下来会有什么骚动。墨西哥谋杀案的第一夫人正在逃亡|迈克尔·戴利| 2014年10月29日|每日野兽|
路易太阳穴的脉搏剧烈跳动；然而，他决定不去预料，而是让沃顿自己解释。《牧师的火面》第3卷，共4卷|简·波特
我没想到会有一次愉快的爱尔兰之旅，但现实比我想象的更痛苦。欧洲概览|霍勒斯·格里利
另一方面，他的脚与被切断的动脉相距甚远，因此他们期待着受到屈辱。加里波利日记，第一卷|伊恩·汉密尔顿
当着你的面讲述的任何轶事，千万不要预料到其中的要点或笑话。女士礼仪手册和礼貌手册|弗洛伦斯·哈特利
但这件事就连他那乐观的精神也几乎不敢预料。英国自詹姆斯二世登基以来的历史|麦考莱