至于海尼克，他说他的左肩“几乎百分之百。”四分卫泰勒·海尼克与华盛顿足球队签订了两年的合同。|山姆·福蒂尔| 2021年2月10日|华盛顿邮报|
这位人士说：“我非常肯定，这一比例实际上不到半个百分点。”。媒体简报：媒体公司的DE&I后续行动未能实现| Tim Peterson | 2021年2月4日| Digiday
如果第三方获得了15%到30%的订单，那么这就超过了实际工作人员的利润。避免使用大型送货应用程序的餐厅必须变得富有创意|克里斯汀·霍利| 2021年2月1日| Eater
在这些股票中，维珍银河（VirginGalactic）的“短期浮动百分比”约为70%。随着维珍银河在游戏停止狂热中被席卷，它又回到了飞行状态| Eric Berger | 2021年2月1日| Ars Technica
如果是的话，大约35%到40%的成年人将永远得不到驾驶执照。关于标准化测试，你需要知道什么？|瓦莱丽·施特劳斯| 2021年2月1日|华盛顿邮报
36%的人赞成，38%的人反对。个人电脑警察如何威胁言论自由|尼克·吉莱斯皮| 2015年1月9日|每日野兽
历史上，国会议员的连任率为95%。国会难以忍受的白色|迪安·奥贝达拉| 2015年1月8日|每日野兽
第114届国会议员中80%为白人，80%为男性，92%为基督教徒。国会难以忍受的白色|迪安·奥贝达拉| 2015年1月8日|每日野兽
通过化疗，她的医生给她至少80%的生存机会。青少年应该有死亡的权利吗|布兰迪·扎德罗兹尼| 2015年1月8日|每日野兽
在众议院19%的席位和参议院20%的席位上，本届国会将比以往任何时候都欢迎更多的女性。国会难以忍受的白色|迪安·奥贝达拉| 2015年1月8日|每日野兽
公众希望看到侏儒，其中50%的人现在从事某种形式的演艺事业。大卫·兰纳克，侏儒乔治·S·哈尼
政府在土地上浇水的成本从50%增加到私营企业的两倍（掌声）。第二届全国自然保护大会会议记录
我们百分之八十多的人直接或间接地依靠商业条件生活。第二届全国自然保护大会会议记录
在这片辽阔的土地上，27%被森林占据，这一比例几乎与德国的森林面积相同。第二届全国自然保护大会会议记录
这使得海军造船厂的雇员工资不可避免地减少了20%。美国工会主义的历史|塞利格·帕尔曼