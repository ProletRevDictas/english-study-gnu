不可避免地，会有更多的出版物紧随其后，尽管有传言说现在正计划保持强势。成人分类广告的重要性| Hazlitt | 2014年9月6日| DAILY BEAST
唐宁街一位发言人否认了任何“危机会谈”，但否认了谣言本身。性丑闻席卷唐宁街10号，野性媒体回击| Peter Jukes | 2013年6月2日| DAILY BEAST
关于过去事件的传闻可能使他参观了一种新型蒸汽机的工作场地。理查德·特雷维希克生平，第二卷（共2卷）|弗朗西斯·特雷维希克
在咖啡馆里人们谈论马斯格雷夫；但他将被求婚的谣言很快就消失了。英国自詹姆斯二世登基以来的历史|托马斯·巴宾顿·麦考利
他们对这位女仆一无所知，只知道她在奥尔良获胜的传闻。雅克·托尔内布罗希的《梅里故事》——法国阿纳托利
谣言在他周围传开了，在他自己的世界里，使他获得了一个令人羡慕和卑鄙的地位。纳博布·阿尔方斯·道代特酒店
关于西方，有一个强烈的谣言说我们采取了有利于我们的行动：但无论如何，我们是安全的，而且可能是成功的。爱德华·吉本的私人信件（1753-1794）第1卷（共2卷）|爱德华·吉本