他从Cremonese小提琴制造商那里了解到，那里很少有虫害，这使他怀疑乐器制造商已经开发出了严密防范虫害的配方。你可能喜欢斯特拉迪瓦里小提琴，因为虫子讨厌它们|萨拉·乔多什| 2021年8月18日|科普
有时，他每天花三到四个小时在练习小提琴或浏览互联网上的招聘广告时暂停工作。《失业保险欺诈如何在大流行期间爆发》| Cezary Podkul | 2021年7月26日| ProPublica
我不能像弹钢琴一样演奏它们，我需要像拉小提琴一样演奏它们，在那里我是在哄骗，而不是推动它们发出声音。当我们与他人和谐相处时，我们会更加自我——第104期：和谐|凯文·伯杰| 2021年7月21日|鹦鹉螺
《小提琴协奏曲》，加上他为小提琴和管弦乐队创作的两部浪漫曲，都是在新冠疫情结束后创作的。米多里的职业生涯始于转瞬即逝的一刻。它已经演变成了一个持久的遗产|Michael Andor Brodeur | 2021年5月13日|华盛顿邮报
就在那时，一位14岁的小提琴神童在伦纳德·伯恩斯坦（Leonard Bernstein）华丽而苛刻的“小夜曲”（Serenade）中的一段特别火辣的音乐中，在作曲家的指挥棒下，切掉了不是一根，而是两根E弦。米多里的职业生涯始于转瞬即逝的一刻。它已经演变成了一个持久的遗产|Michael Andor Brodeur | 2021年5月13日|华盛顿邮报
如果恒星质量黑洞是小提琴，那么IMBH就是低音提琴。黑洞的金发姑娘| Matthew R.Francis | 2014年8月24日|每日野兽
民主党人将强调小提琴的故事，这些故事也将存在。为什么奥巴马医改能在2014年帮助民主党人？迈克尔·托马斯基（Michael Tomasky）2013年12月5日《每日野兽》
新闻发布室的一顶帽子是用黑色天鹅绒设计的，头盖骨顶上有一把小提琴。安娜·皮亚吉的“帽子学”展览在米兰开幕|莉莎·福尔曼| 2013年9月23日|每日野兽
在右翼，周二出现了一种普遍嘲笑的观点，认为俄罗斯正在像拉廉价小提琴一样演奏奥巴马。奥巴马只是改变了他在叙利亚问题上的运气吗|Michael Tomasky | 2013年9月11日|每日野兽
这位扮演全能美国总统的女性喜欢她的私人小提琴。玛格丽特·撒切尔如何改变英国政治| Tunku Varadarajan | 2013年4月8日|每日野兽
弗鲁莱因·费希特纳已经离开，但第一小提琴手演奏了门德尔松著名的小提琴协奏曲。德国音乐研究|艾米·费伊
他在小提琴上就像李斯特在钢琴上一样，是唯一值得与他相提并论的艺术家。德国音乐研究|艾米·费伊
奥特林一接触到他的小提琴，我就发现他是一位卓越的艺术家，这立刻激发了我的灵感。德国音乐研究|艾米·费伊
小提琴是一种乐器，虽然体积小，原价微不足道，但价格却非常昂贵。小提琴和小提琴制作者|约瑟夫·皮尔斯
人声和小提琴是两种最悦耳、最富有表现力、最有力的单一乐器。小提琴和小提琴制作者|约瑟夫·皮尔斯