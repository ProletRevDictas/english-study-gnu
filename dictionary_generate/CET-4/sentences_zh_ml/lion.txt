这次旅行从内罗毕国家公园开始，在那里犀牛、长颈鹿和狮子在开阔的草原上漫步。27次史诗般的旅行将立即开始规划|编辑| 2020年9月8日|外部在线
罗宾和他的朋友们渴望一个他们被邀请进入主流社会的世界，同时仍然能够成为他们超级酷的自己，当理查德国王在电影结尾归来时，他们就能做到这一点，他不仅是一头狮子，而且是一个真正的盟友。走向怪异的迪斯尼佳能|艾米莉·范德沃夫| 2020年9月4日| Vox
他用它来回答一个关于狮子在圈养或野外繁殖是否有困难的问题。一头19岁的狮子如何在18个月内生下35只幼崽|汤姆·麦克纳马拉| 2020年8月27日|科普
生物学家记录了许多物种的杀婴行为，包括狮子和许多非人灵长类动物，如叶猴和黑猩猩。仅仅因为它是自然的并不意味着它是好的-第89期：黑暗面|大卫·P·巴拉什| 2020年8月19日|鹦鹉螺
你也可能对潜在的危险变得高度警惕——就像众所周知的潜伏在我们祖先大草原高草中的食人狮子。真的有“孤独流行病”吗？（第407页）|斯蒂芬·J·杜布纳| 2020年2月27日|畸形经济学
事实上，拥有印尼国内航空市场45%份额的狮航已经完全吞并了费尔南德斯公式。恼人的机场延误可能会妨碍您成为下一个亚航8501 |克莱夫·欧文| 2015年1月6日|每日野兽
除了航空公司本身，狮子航空和其他61家印尼航空公司被列入黑名单还有一个更大的原因。恼人的机场延误可能会妨碍您成为下一个亚航8501 |克莱夫·欧文| 2015年1月6日|每日野兽
印尼两家航空公司Garuda和Lion Air看到费尔南德斯吃午餐，现在才做出回应。恼人的机场延误可能会妨碍您成为下一个亚航8501 |克莱夫·欧文| 2015年1月6日|每日野兽
狮子航空的机长离开了他的新手副驾驶去着陆，直到他意识到自己遇到了麻烦。恼人的机场延误可能会妨碍您成为下一个亚航8501 |克莱夫·欧文| 2015年1月6日|每日野兽
这架飞机归印尼廉价航空公司狮航所有。谁将获得亚航8501的黑匣子|克莱夫·欧文| 2014年12月30日|每日野兽
此后，他两次逃学，被一种浪漫而不可抗拒的欲望抓住，想看到并射杀一头狮子！猎狮| R.M.巴兰坦
狮子的力量是巨大的，因为它的下巴、肩膀和前臂周围有大量的肌肉。猎狮| R.M.巴兰坦
牛犊和熊要吃草，它们的幼崽要一起休息，狮子要像牛一样吃草
到目前为止，他还没有成功地实现射杀狮子的伟大愿望。猎狮| R.M.巴兰坦
当饥饿的狮子在寻找猎物时，看到任何动物都会让它开始跟踪猎物。猎狮| R.M.巴兰坦