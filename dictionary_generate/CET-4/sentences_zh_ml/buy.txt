随着这些平台继续围绕广告购买实现流程自动化，购买活动正在向海外发展长期人手不足：一位机构高管对在线广告新冠病毒增长成本的自白| Seb Joseph | 2021年2月11日| Digiday
上个月，高盛（Goldman Sachs）将其股票列为一笔好买卖，预计该公司今年将反弹，赶上市场上表现较好的其他石油巨头。石油公司在2020年的亏损令人震惊。那是在政府关注气候变化之前|英国《华盛顿邮报》2021年2月4日
第二，提及诸如“价格”或“折扣”等关键词的术语是购买意向的强烈信号，你应该将它们捆绑在一起。谷歌广告新功能：谷歌广告活动的五大技巧|马丁·罗梅里奥| 2021年2月3日|搜索引擎观察
很多这些，你知道，所谓的模因股票，你知道，在社交媒体上迅速传播，人们都在加入罗比尼奥，他们有很多网上购买活动。埃隆·马斯克打破了俱乐部会所的限制，粉丝们涌向YouTube，他转而采访罗比尼奥CEO |迈克·布彻| 2021年2月1日| TechCrunch
根据联邦选举委员会的报告，她的竞选团队为一家名为“邻里研究与媒体”的公司支付了65.6万美元的广告费、民意调查费、媒体购买费和其他服务费。卡农无根据理论的推动者、众议员马乔里·泰勒·格林如何在共和党关键人物迈克尔·克拉尼什、赖斯·蒂鲍尔特、斯蒂芬妮·麦克鲁门的支持下崛起| 2021年1月30日|华盛顿邮报|
为什么“他们”仅仅因为他20年前试图买东西就想压垮他？Phylicia Rashad和Cosby Truthers的崇拜| Stereo Williams | 2015年1月8日|每日野兽
“这是市场上唯一可以买到安全别针的地方，”他说。为马拉喀什放弃曼哈顿的摄影师|莉莎·福尔曼| 2015年1月6日|每日野兽
“在便利设施和商店里，你可以买到你需要的东西，”他说，这要容易得多。为马拉喀什放弃曼哈顿的摄影师|莉莎·福尔曼| 2015年1月6日|每日野兽
例如，百思买在其客户忠诚度计划“奖励区”中拥有4000多万会员。百思买在亚马逊的回击|威廉·奥康纳| 2014年12月27日|每日野兽
百思买陷入了技术创新的危险世界。百思买在亚马逊的回击|威廉·奥康纳| 2014年12月27日|每日野兽
如果麦克一个人的话，他会在日落前赶到邮局的，因为骑警骑着精选的马，这是最划算的钱。原金|伯特兰·W·辛克莱
“给你即将成为妻子的妻子买点东西，”他对他的侄子说，一边把折叠好的纸递给他。《Pit Town Coronet》，第一卷（共3卷）|查尔斯·詹姆斯·威尔斯
亚历山德罗付出了艰辛的努力，为那些希望以四分之一的价格购买贝尼托和马车的人提供了文明的回答。拉蒙娜|海伦·亨特·杰克逊
他们说，它非常古老，如果你能找到合适的人买下它，它值很多钱。拉蒙娜|海伦·亨特·杰克逊
为了取代她的位置，我们必须先贷款，然后才能出售和购买。货币和银行读物|切斯特·阿瑟·菲利普斯