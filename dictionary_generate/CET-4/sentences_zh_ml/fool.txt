他向我俯下身，低声说：“这些绝地头脑的把戏只对弱智的傻瓜有效。”小丑多久笑一次|尤金·罗宾逊| 2021年1月7日|奥齐
当别人不知道的时候，假装你知道的更好，假装这不是你的错，假装你是一个神圣的使者而其他人都是傻瓜，这是他对罪的一个很好的定义。还记得我的朋友巴里·洛佩兹|鲍勃·沙科奇| 2021年1月5日|在户外在线
当经纪人下了赌注，一些人发了财，一些人愚弄自己，其他人为自己辩护时，克莱恩和数百万其他医疗工作者只是祈祷明天会有足够的供给。“我们这些不死的人将要戒烟”：一大群病人，不断减少的医疗用品和失去希望的护士，J·大卫·麦克斯旺著，2020年12月30日，ProPublica
遇到一个以“亚宾傻瓜”而闻名的家伙，我不会吓唬任何看起来像盗版滑头的人，而在他的最新作品《高谭》中，丹兹尼不会让任何人感到安全。不给糖就捣蛋：你们和食尸鬼的万圣节前夜播放列表| cmurray | 2020年10月30日| Essence.com
另一个是害怕出丑和看起来很愚蠢。你可能误用了5个心理学术语（第334集重播）|斯蒂芬·J·杜布纳| 2020年1月9日|畸形经济学
他们发现，大多数人更喜欢和可爱的傻瓜一起工作，而不是和能干的混蛋一起工作。反对无神论的案例|史蒂夫·诺依曼| 2015年1月4日|每日野兽
你必须冒这个险，并且有看起来像个十足的傻瓜的危险。闯入百老汇的英国人|蒂姆·蒂曼| 2014年12月7日|每日野兽
这也许不会愚弄一个知识渊博的威士忌饮用者，但那些渴望得到帕皮的人中有多少是知识渊博的威士忌饮用者呢？帕皮·范·温克尔的崇拜|埃里克·费尔滕| 2014年12月3日|每日野兽
我的原话是允许“世界上任何有互联网接入的傻瓜”自由修改网站上的任何页面。你可以查阅：维基百科故事|沃尔特·艾萨克森| 2014年10月19日|每日野兽
也可能是因为这个傻瓜在十字路口中间闯红灯。阿曼达·拜恩斯的火车失事又回来了，在一次新的酒后驾车被捕之后，凯文·法伦于2014年9月29日在《每日野兽》杂志上
醉酒的热度是愚人的绊脚石，它会削弱体力，造成伤害。《圣经》，杜埃·莱姆斯版本，多种多样
Y是一个不爱上学的年轻人；Z是一个滑稽的、可怜的、无害的傻瓜。男孩和女孩书架；《性格塑造实用计划》，第一卷（共17卷）|
我还没有傻到把我宝贵的小憩置于危险之中，就在我如此迫切地需要它们的时候。布莱克伍德的爱丁堡杂志，第60卷，第372期，1846年10月
大卫认为农夫是个傻瓜，于是继续骑着马，欣赏着没有被一朵云遮住的蓝天。《奇闻轶事》和《趣味预算》|各种各样的
这位身披桂冠的天文学家步履蹒跚地走着，这又是一个傻瓜的例子，他的钱很快就分崩离析了。《奇闻轶事》和《趣味预算》|各种各样的