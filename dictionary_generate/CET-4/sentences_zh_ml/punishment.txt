我是个傻瓜，这让我摆脱了和妈妈一起受罚的命运。情景喜剧皇后提奇娜·阿诺德为了避免被打屁股而变得滑稽起来|帕拉比·蒙西| 2020年9月10日|奥兹
如果巴里奥斯确实赚了公司利润的一部分，或者继续赚，并且在为戈麦斯工作时没有披露，他可能会受到额外的惩罚。Barrios没有透露他在担任市政厅工作前的收入| Andrew Keatts和Jesse Marx | 2020年8月31日|圣地亚哥之声|
在接受詹努利的认罪协议时，戈顿说，在这种情况下，刑期是“足够的，但不超过必要的惩罚”。洛里·洛林在法官接受《财富》杂志2020年8月21日《大学贿赂丑闻》中的认罪协议后被判入狱两个月
这种方法只能导致对突出的异常值进行惩罚，以获得一致的平均值。英国考试的失败提醒我们，算法无法修复损坏的系统| Karen Hao | 2020年8月20日|麻省理工技术评论
从表面上看，他们是为了惩罚新疆对穆斯林维吾尔人令人震惊的侵犯人权行为。新冠病毒-19与美国衰落的地缘政治|凯蒂·麦克莱恩| 2020年8月19日|麻省理工技术评论
在《每日野兽》中，我们不能容忍的体罚行为是约瑟夫抓住他的耳朵并“使劲拉”。婴儿耶稣是一个神圣的恐怖分子吗|Candida Moss | 2014年12月21日|每日野兽
当基督徒和印度教徒被指控在巴基斯坦侮辱伊斯兰教时，惩罚是严厉的。迪斯科毛拉亵渎神明的争吵凸显了巴基斯坦的虚伪| Shaheen Pasha | 2014年12月21日| DAILY BEAST
尽管如此，有些人仍会放纵，即使有受到惩罚的风险。因同性恋被教会殴打|贾斯汀·琼斯| 2014年12月16日|每日野兽
在20世纪80年代，你们的社区允许数十万人死亡，因为你们认为艾滋病是神圣的惩罚。LGBT是否欠基督徒一个橄榄枝？试试另一种方法|杰伊·迈克尔森| 2014年12月14日|每日野兽
在他生命结束时，老师们对他进行体罚的记忆十分清晰。阿尔弗雷德·希区柯克的《褪色为黑色：伟大导演的最后日子》|大卫·弗里曼| 2014年12月13日|每日野兽报
惩罚游行的第二天是星期天，这一事实使纪律有所放松。红色的一年|路易斯·特蕾西
自我惩罚的故事证实了这些冲动将矫正视为一件非常恰当的事情的证据。儿童的方式|詹姆斯·萨利
但一旦奥地利被处置，普鲁士和俄罗斯就因秘密或公开援助而受到惩罚。拿破仑元帅R.P.邓恩·帕蒂森
惩罚仍然来自那些我们想避开的人。布莱克伍德的爱丁堡杂志，第CCCXXXIX号。1844年1月。第LV卷|各种各样的
罪有应得的是他，而不是受苦受难的人，是他犯错的父亲强加给他的灾难。布莱克伍德的爱丁堡杂志，第CCCXXXIX号。1844年1月。第LV卷|各种各样的