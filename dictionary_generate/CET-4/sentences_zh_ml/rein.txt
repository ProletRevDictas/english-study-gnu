西雅图—亚马逊创始人杰夫·贝佐斯（Jeff Bezos）将辞去这家电子商务巨头的首席执行官一职，将权力移交给该公司长期的云计算老板安迪·贾西（Andy Jassy）。杰夫·贝佐斯（Jeff Bezos）辞去亚马逊首席执行官职务，过渡到执行主席职位|杰伊·格林、托尼·罗姆| 2021年2月4日|华盛顿邮报|
2019年，谷歌创始人拉里·佩奇（Larry Page）和谢尔盖·布林（Sergey Brin）辞去了Alphabet首席执行官和总裁的职务，将控制权交给了孙达尔·皮凯（Sundar Pichai），他是一位有15年历史的值得信赖的副手。随着贝佐斯成为亚马逊首席执行官，扎克伯格是最后一个站在那里的人。|伊丽莎白·德沃斯金| 2021年2月3日|华盛顿邮报|
市长托德·格洛里亚（Todd Gloria）上周四在市政厅接掌大权，他已经发誓要找到资金让庇护所一直开放到新年。会议中心的冠状病毒爆发是不可避免的|丽莎·哈尔沃斯塔特| 2020年12月17日|圣地亚哥之声|
市长托德·格洛里亚（Todd Gloria）周四在市政厅上任，成为首位有色人种人士和LGBT社区的成员，当选为该市最高职位。晨报：新任市长、议会接管|圣地亚哥之声| 2020年12月11日|圣地亚哥之声
当他在2018年掌权时，这家1470连锁店将成为电子商务参与者作为一个优先事项。流行病小狗推动Petco盈利，促使公开招股书《菲尔·瓦哈巴2020年12月4日财富》
在父亲去世后掌权的金正恩几乎让张伯伯可以自由处理与北京的关系。朝鲜为何释放两名美国人|戈登·G·张| 2014年11月9日|每日野兽
退休后，弗兰克有意识地试图削减开支并加以控制。理查德·福特巧妙的生存主义指南：弗兰克·巴斯科姆的回归|汤姆·莱克莱尔| 2014年11月4日|每日野兽
具有讽刺意味的是，国会在越战后控制总统使用武力的努力中也承认了这一点。谁来决定我们什么时候开战|Deborah Pearlstein | 2014年9月21日|每日野兽
有些时候，我们确实采取了非常疯狂的，我们必须控制它。Mindy项目的居民怪人Ike Barinholtz | Kevin Fallon | 2014年9月16日| DAILY BEAST
他组织了一次跨党派的努力来控制国家安全局（National Security Agency），该机构在众议院获得通过的几票之内。大企业与共和党自由主义者之争| David Boaz | 2014年6月12日| DAILY BEAST
他骑得很轻松，缰绳放松，一看到这些白脸，他就挥舞着那只松开的手。红色的一年|路易斯·特蕾西
勒住缰绳是不可能的，下去几乎肯定会使马和人丧命。猎狮| R.M.巴兰坦
缰绳拴在一根从动物鼻子里抽出的分裂藤条上。Alila，我们的菲律宾小表妹玛丽·哈泽尔顿·韦德
他们没有勒住缰绳，继续向布西鲁贡日推进，越过赛河，靠近邦尼村。红色的一年|路易斯·特蕾西
在白杨林不远处，皮耶根拉住缰绳，举起一只手。原金|伯特兰·W·辛克莱