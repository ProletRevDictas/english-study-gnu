他们是邪恶核心的典型代表，因此，需要以轻蔑的态度对待他们。向我的美国英雄致敬|布罗迪·列维斯克| 2020年9月17日|华盛顿刀片
实行成人宵禁，不允许穿颜色的衣服，在公共场所乱丢垃圾或大声喧哗可能导致藐视法庭。当我们重新思考治安时，是时候结束帮派禁令了|杰米·威尔逊| 2020年9月15日|圣地亚哥之声|
3月份，他的律师辩称，MTS因未能遵守她的传票而应被判藐视法庭，而市检察官办公室因未能提供协助而应受到制裁。晨报：3台人体摄像头，无镜头|圣地亚哥之声| 2020年7月22日|圣地亚哥之声
假设你和某人有关系，你不是轻蔑者，而是轻蔑接受者。新冠病毒-19对城市（和婚姻）意味着什么？（第410页）| Stephen J.Dubner | 2020年3月26日|畸形经济学
你会认为最好是蔑视你不认识的人。新冠病毒-19对城市（和婚姻）意味着什么？（第410页）| Stephen J.Dubner | 2020年3月26日|畸形经济学
他说：“国际上对她的行为有很多恐惧和蔑视，国内很少。”。Sisi正在迫害、起诉并公开羞辱埃及同性恋者| Bel Trew | 2014年12月30日| DAILY BEAST
阿拉斯泰尔·西姆的下巴像融化的蜡烛蜡，咆哮像一只走投无路的猫，眼睛因轻蔑而冰冷。狄更斯和史克鲁奇如何拯救圣诞节|克莱夫·欧文| 2014年12月22日|每日野兽
他们的声明表明，他们蔑视这些承诺。Facebook Prince清洗新共和国：在一本100年前的杂志《劳埃德·格罗夫》（Lloyd Grove）2014年12月5日《每日野兽》（DAILY BEAST）被摧毁的过程中
特里尔韦勒声称，社会党人奥朗德蔑视穷人，据说他称穷人为“无牙者”。法国总统前利齐·克罗克（Lizzie Crocker）2014年11月28日《每日野兽》（DAILY BEAST）的瓦莱丽·特里尔韦勒（Valerie Trierweiler）没有愤怒
总统和他的政党对白人工人阶级选民的蔑视已经昭然若揭。随着移民行动，奥巴马和福利党再次罢工|劳埃德·格林| 2014年11月24日|每日野兽
只有这样，我们才能与他成为朋友，使我们超越恶人的嫉妒和蔑视。《鲁滨逊漂流记》的生活和最令人惊讶的冒险故事，纽约，水手（1801）|丹尼尔·笛福
她的眼睛里闪耀着胜利的光芒，然而她对她颤抖的父亲的态度却轻蔑地弯起了嘴唇。红色的一年|路易斯·特蕾西
如果他割伤了她，他会表现出比那顶死板的帽子还要少的轻蔑。寻找父亲的迷迭香| C.N.威廉姆森
他在最后两个字里加入了一种难以形容的半笑半藐视的声音。贝拉·唐娜|罗伯特·希金斯
由于受到大众的奉承，他升空了一个小时，但由于历史的蔑视，他从此陷入了默默无闻的境地。布莱克伍德的爱丁堡杂志，第CCCXXXIX号。1844年1月。第LV卷|各种各样的