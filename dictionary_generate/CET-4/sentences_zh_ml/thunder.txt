人们甚至知道，地震仪能够探测到尘暴在寒冷的沙漠世界中旋转的微弱雷声。火星上的隆隆声增加了地下岩浆流动的希望|罗宾·乔治·安德鲁斯| 2021年2月1日|量子杂志
他担心，如果工作人员减缓加泰罗尼亚大火的速度，他们可能会导致它形成一个积雨云——一个由火、雷和风组成的猛烈云，就像在小溪大火上形成的云一样。火灾建模的复杂数学告诉我们关于加利福尼亚森林的未来|艾米·诺德鲁姆| 2021年1月18日|麻省理工技术评论
当然，绿巨人很强壮，雷神可以控制雷声，奇迹船长可以飞行并发射能量束。在观看WandaVision | Eliana Dockterman | 2021年1月14日|时间之前，您需要了解的一切
至于雷雨的可能性，我们不能排除在较重的雨室中有雷声。华盛顿地区周三至周四的暴雨和局部洪水预报|杰弗里·哈尔弗逊，杰森·萨莫诺| 2020年11月10日|华盛顿邮报
不幸的是，明天会下雨，有时甚至会有一两声雷声。华盛顿特区-地区预报：下雨前又一天天气温暖宜人|马特·罗杰斯| 2020年11月10日|《华盛顿邮报》
我继承了阿诺德家族的Thunder ThighsTM，这是我小时候经常受到嘲笑和困扰的原因。你永远无法“治愈”进食障碍|卡莉·阿诺德| 2014年12月20日|每日野兽
小心你的背利亚姆·尼森，凯文·科斯特纳来抢你的老主唱雷霆了！2014年最大的炸弹：性录影带、玛丽亚·凯莉的演唱、我是如何认识你母亲等|凯文·法伦| 2014年12月19日|每日野兽
齐尔奇，与《Showtime》的另一部热气腾腾的性爱剧《风流韵事》相比，这部剧抢走了它的风头。15愤怒的金球奖电视冷落和惊喜：艾米·波勒，《广告狂人》及更多|凯文·法伦| 2014年12月11日|每日野兽
当我第二次到达并迈出最后一步时，雷声大雨倾盆而下。Philippe Petit在世贸中心走钢丝时的关注时刻| Anthony Haden Guest | 2014年8月8日|每日野兽
然后，照明弹照亮了天空，建筑物开始从防护边缘的致命雷声中震动。以色列将加沙送回石器时代的运动|杰西·罗森菲尔德| 2014年7月29日|每日野兽
不义人的财物，必像江河一样干涸，又像大雨中的大雷声，喧哗而去。《圣经》，杜埃·莱姆斯版本，多种多样
山顶上布满了雷鸣云，混浊的空气中弥漫着令人窒息的西罗科火山。《牧师的火面》第3卷，共4卷|简·波特
雷云的威胁在他童年的梦中逼近；这场安静的外部表演背后隐藏着灾难。波浪|阿尔杰农·布莱克伍德
有一天，山上远处的一声轰鸣唤起了所有人的希望，但却被雷声击碎。拿破仑元帅R.P.邓恩·帕蒂森
就在国内这片雷云爆炸后的几天里，吉尔伯特和多萝西被单独扔到了一起。他们面前的世界|苏珊娜·穆迪