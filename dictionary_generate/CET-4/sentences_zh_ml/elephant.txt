然而，鲸目动物和大象可以禁食更长的时间，“这不知何故告诉我们，它们一定找到了不同的方法在饥饿期间为大脑提供能量，”希勒说。由于失去了基因，生命往往进化得更加复杂。| Viviane Callier | 2020年9月1日| Quanta杂志
死去的大象的獠牙仍然存在，到目前为止，没有其他物种在类似情况下死亡。科学家们对博茨瓦纳数百头大象神秘死亡的原因有一些理论| Vicky Boult | 2020年7月7日|石英
这项新的研究重新开启了关于大象是否真的会醉醺醺地大嚼马鲁拉水果的长期争论。为什么大象和犰狳很容易喝醉|苏珊·米利厄斯| 2020年6月4日|学生科学新闻
贾尼亚克说，关于大象在吃过熟水果后行为异常的描述至少可以追溯到1875年。为什么大象和犰狳很容易喝醉|苏珊·米利厄斯| 2020年6月4日|学生科学新闻
前几届政府，无论是民主党还是共和党，都没有真正将中国视为房间里的大象。总统和你想的一样重要吗？（第404页）|斯蒂芬·J·杜布纳| 2020年2月6日|畸形经济学
大象马塞尔带着读者踏上了他的人生旅程，讲述了他充满旅行和冒险的记忆。《每日野兽》2014年节日礼物指南：献给你生命中的蓝色常春藤| Allison McNearney | 2014年11月29日|每日野兽
这段在赞比亚拍摄的视频显示，一头年幼的大象被14头狮子袭击。小象与14头狮子搏斗并幸存下来|Alex Chancey，《每日野兽》视频| 2014年11月13日|每日野兽
到1998年，他们的明星大象肯尼病了，马戏团的兽医命令他不要表演。马戏团如何获得社会良知|贾斯汀·琼斯| 2014年11月7日|每日野兽
被公众视为怪人的身体中温文尔雅、博学的灵魂是《象人》核心的反差。《象人》的真实故事|拉塞尔·桑德斯| 2014年11月3日|每日野兽
想在新的“Giopi:2014使命多数”中作为爱国大象与“税务者”和“送泥者”作战吗？共和党奇怪的反Dem电子游戏，由兄弟会男孩大象Asawin Suebsaeng主演，2014年8月27日《每日野兽》
当时被我们猎人杀死的母象是一头相对较小的象。猎狮| R.M.巴兰坦
大象径直向前跑，穿过了整个队伍，但没有走近任何人。猎狮| R.M.巴兰坦
大象没有跟随，但猎人们发现了她的退路，并毫不迟疑地跟随和攻击她。猎狮| R.M.巴兰坦
但他很快就克服了恐惧，忙着喂大象，爸爸不得不哄他离开。《托儿所》，1873年7月，第十四卷。第一|各种
突然，大象大声吹喇叭，好像有人在刺激他。吉卜林的故事和诗每个孩子都应该知道，第二册|鲁迪亚德·吉卜林