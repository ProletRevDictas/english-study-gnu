根据最高法院的说法，妇女享有的平等保护没有宪法对种族歧视的保护那么有力。金斯伯格为女性赢得的法律胜利也为LGBTQ社区带来了里程碑式的反歧视裁决| LGBTQ编辑| 2020年9月24日|没有直接新闻
在某些时候，我们负担不起一些员工，因为我们没有从客户那里获得资金“我们到底想搞到多少？”：一位机构高管因冠状病毒而未付款的自白|克里斯蒂娜·蒙洛斯| 2020年9月24日|迪吉德
最先进的人工智能技术需要大量的计算资源，这越来越只有最富有的公司才能负担得起。OpenAI让微软独家访问其GPT-3语言模型| Niall Firth | 2020年9月23日|麻省理工技术评论
他的家人负担不起把祖母送到有物理治疗等服务的养老院。他想修复美国农村破碎的养老院。现在，纳税人可能要为7600万美元买单|由Max Blau为《乔治亚州健康新闻》| 2020年9月22日|出版
有一部分公众能买得起，这类人想知道产品来自哪里。Allbirds正以轻快的步伐为地球挺进| sheilamarikar | 2020年9月21日|财富|
讽刺的是，随着经济的发展，现在谁能真正承担得起假装自己动手做的费用呢？格伦·贝克现在在卖时髦的衣服。真的|安娜·玛丽·考克斯| 2014年12月20日|每日野兽
他的继父离开了母亲，他们再也买不起他们住的房子了。因同性恋被教会殴打|贾斯汀·琼斯| 2014年12月16日|每日野兽
就美元和美分而言，美国短期内可能有能力支付残疾和食品券。布什、克里斯蒂、罗姆尼：谁将成为共和党的阶级战士|劳埃德·格林| 2014年12月15日|每日野兽
它代表了一杯好的苏格兰威士忌所能给我们的生活带来的舒适和小小的奢侈品。一位威士忌鉴赏家记得2014年12月10日每日野兽第一口麦卡伦威士忌
卖掉多余的东西后，我看到我的邻居对它的气味感到惊讶，并喃喃地说他希望自己能买得起。《监狱里的一百万种死亡方式》|丹尼尔·吉尼斯| 2014年12月8日|每日野兽》
我怀疑现代社会是否有能力继续这样做；它当然负担不起将其广泛推广。拯救文明| H.G.（赫伯特·乔治）威尔斯
这里和那里都留下了一些顶部没有被折断的植物，以便它们能够为下一年提供种子。烟叶它的历史、品种、文化、制造和商业——E.R.比林斯。
我们的炮手投入的炮火超出了他们的承受能力，几乎没有什么可以铺路的。加里波利日记，第一卷|伊恩·汉密尔顿
这种感觉在圣贝纳迪诺并不常见，因此他们不能忽视这样一个引人注目的场合。拉蒙娜|海伦·亨特·杰克逊
在某些情况下，在侧面或正面设置一扇彩色玻璃窗，但只有有钱人才能买得起这种奢侈品。我们的韩国小表弟| H.李M.派克