这一决定是该公司重新专注于核心品牌的目标的一部分，这一努力包括抛弃像圣诞树商店这样的超重行李，并修复其财务状况。为什么阿迪达斯最终将锐步出售|菲尔·瓦哈巴| 2020年12月14日|财富
西南航空公司称，飞行员、乘务员、行李搬运工和其他工作人员周四接到通知，他们可能会休假。西南航空公司警告说，近13%的员工面临裁员风险，因为他们在2020年12月4日《财富》杂志（Fortune）上因旅行而受到处罚
他说，当消防队员赶到时，他们发现一名美铁员工坐在一辆“用来运送行李”的高尔夫球车上，被火车撞了。2020年12月3日，美国铁路公司员工在联合车站乘坐高尔夫球车被火车撞倒| Dana Hedgpeth |华盛顿邮报
明确地说，韦斯特布鲁克把自己的行李和自己的巨额合同带到了华盛顿。奇才队用约翰·沃尔换下拉塞尔·韦斯特布鲁克是正确的选择，但风险重重|本·戈利弗| 2020年12月3日|华盛顿邮报|
航空旅行需要严格遵守行李重量限制，即使在考虑到超重对身体的影响之前，往返于工作和家庭之间的背包和笔记本电脑包的容量也是有限的。最佳无线耳机：需要考虑的五件事：PopSCI商务团队（2020年10月30日）流行科学
其他官员告诉记者，搜救人员还在水中发现了一件救生衣和一件行李。2014年12月30日，亚航失事飞机残骸、尸体被发现| Lennox Samuels | DAILY BEAST
他们把精神包袱带回家，这与几乎所有其他工作都不一样。抗议者们对拉莫斯和刘翔有什么愤怒吗|迈克·巴尼科尔| 2014年12月22日|每日野兽
基萨说：“我开始写作时，父母就分手了，这给我带来了很多情感负担。”。基萨的制作：从海军神枪手到选美皇后再到流行天后|马洛·斯特恩| 2014年10月20日|每日野兽
作为男人和女人都有各种各样的包袱。约瑟夫·戈登·莱维特（Joseph Gordon Levitt）关于他为什么要探索“女权主义”这个词和在线厌女症的文章|马洛·斯特恩| 2014年9月22日|每日野兽
查尔斯身上的许多包袱是众所周知的。想象查尔斯王子成为国王让全英国都希望他们能像苏格兰一样离开|克莱夫·欧文| 2014年9月17日|每日野兽
嗯，如果你真的丢了行李或者想买一个已经标记好的行李箱，如果我不是你要找的人，那就惨了。《奇闻轶事》和《趣味预算》|各种各样的
虽然军队在通过葡萄牙撤退的过程中士气低落，但他从未丢失过一支枪或一辆行李车。拿破仑元帅R.P.邓恩·帕蒂森
苏尔特的军队到达时没有大炮或行李，只是一个武装的乌合之众，尼伊的士兵嘲笑那些混乱的营。拿破仑元帅R.P.邓恩·帕蒂森
在她的行李上了马车后，立即开车到她家，并确保那里的一切都准备好让她感到舒适。女士礼仪手册和礼貌手册|弗洛伦斯·哈特利
罗伯特占领了比兰和里沃克斯的修道院，并分配了英军营地的战利品和国王的行李。罗伯特国王布鲁斯·A·F·穆里森