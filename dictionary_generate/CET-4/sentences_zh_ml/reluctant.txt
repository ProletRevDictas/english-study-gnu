政府可能更不愿意依赖其他国家的战略物资，如口罩、药品或电脑芯片。在20万亿美元的流行病救济支出之后，仍然没有通货膨胀的迹象。发生了什么事|伯恩哈德·华纳| 2020年8月25日|财富
由于未来法庭之争的不确定性，官员们不愿向债券市场的投资者推销。晨报：101 Ash St.交易前的交易|圣地亚哥之声| 2020年8月24日|圣地亚哥之声
伦敦金融城的律师们相信他错了，但未来法庭斗争的不确定性意味着伦敦金融城不愿在债券市场向投资者推销自己的产品。101 Ash St.崩溃前的交易有助于解释我们是如何来到这里的| Lisa Halverstadt和Jesse Marx | 2020年8月24日|圣地亚哥之声|
最近几周病例的上升被归咎于社交聚会和旅行者，但官员们不愿意求助于在3月和4月疫情最初高峰期实施的严格封锁。欧洲正处于一个转折点，因为新冠病毒病例激增，脆弱的政府感受到了财富的热度|伯恩哈德·华纳| 2020年8月20日|
其他出版商不愿提供帮助，要么因为他们有自己的订阅要推广，要么他们不确定Scroll是否会成功。托尼·海尔拯救新闻业的探险队﻿ |Steven Perlberg | 2020年7月27日| Digiday
一些最具爆炸性的机会可能是基于西方世界似乎不愿采纳的东西。硅谷将目光投向非洲|克里斯蒂安·鲍里斯| 2014年12月22日|每日野兽
我不愿意问太具体的问题，因为我觉得我会自找麻烦！克里斯托弗·诺兰·安切特：《星际》、《本·阿弗莱克的蝙蝠侠》和《人类的未来》|马洛·斯特恩| 2014年11月10日|《每日野兽》
但美国官员仍然不愿承认这一点。间谍警告白宫：不要打击叙利亚的基地组织| Shane Harris，Jamie Dettmer | 2014年11月7日| DAILY BEAST
如果今年的选票上的候选人不愿意和桑德斯一起竞选，他们也不会羞于接受他的钱。伯尼·桑德斯（Bernie Sanders）向我们展示了社会党竞选总统的方式|大卫·弗里德兰德| 2014年11月3日|每日野兽报
由安德鲁·林肯扮演的里克·格里姆斯开始了这部系列剧，既不情愿又痛苦。《行尸走肉》中的卢克·天行者：里克·格里姆斯是现代神话中的完美英雄|里贾娜·利兹克| 2014年10月28日|每日野兽
用当时的卑鄙手段，很难从勉强生存的土地上榨取生计。社会正义的未解之谜|斯蒂芬·利科克
当终场哨声从发动机中响起时，他才勉强从她怀里挣脱出来。《Pit Town Coronet》，第一卷（共3卷）|查尔斯·詹姆斯·威尔斯
人们不愿意把这种政治服从与一种更肮脏的义务联系起来。罗伯特国王布鲁斯·A·F·穆里森
这名妇女似乎很不愿意接受这一提议，并以各种借口辩解。他们面前的世界|苏珊娜·穆迪
法庭更不愿意承认醉酒是犯罪行为的借口。普特南为门外汉准备的简易法律书|阿尔伯特·西德尼·博尔斯