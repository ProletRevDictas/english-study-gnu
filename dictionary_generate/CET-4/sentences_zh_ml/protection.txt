有精装本和软装本两种，都能为内饰提供坚固的保护。著名的写作和绘画笔记本| PopSci商业团队| 2020年9月17日|科普
Siddharth Narayan说，沿海湿地对于大风暴期间的防洪至关重要。潮湿的海岸土壤？这就是为什么生态学家喜欢它们|艾莉森·皮尔斯·史蒂文斯| 2020年9月17日|学生科学新闻
还有另外一个，监视器，数据保护法是如何实施的。播客：新冠病毒-19正在帮助巴西成为一个监测国家|安东尼·格林| 2020年9月16日|麻省理工技术评论
它将包括第二轮针对小企业的工资支票保护计划贷款、学校资金以及针对企业的病毒相关责任保护。国会缺乏更多经济刺激的紧迫性| Alayna Treene | 2020年9月11日| Axios
该公司还表示，它还有排名保护措施，以确保声称提前获胜的报道不会出现在搜索结果中。谷歌、Facebook和Twitter计划如何处理有关2020年总统选举结果的错误信息| Danielle Abril | 2020年9月10日|财富
讽刺作家占据着危险的地位，他们歪曲教条和废话，在需要保护的同时对抗建制派。哈利·希勒论讽刺的危险事业|劳埃德·格罗夫| 2015年1月8日|每日野兽
至于联邦当局，他们已经准备好了，但神职人员没有要求特别保护。墨西哥牧师被判谋杀罪|杰森·麦加汉| 2015年1月7日|每日野兽
但事实是，他们所坚持的平等保护并不是现实。如果我撞到白人警察的脸会怎么样|Goldie Taylor | 2014年12月30日|每日野兽
只要温斯顿仍能将球抛向50码远的外场，并将球传给一个连胜的广角接球手，这种保护将持续多久。Jameis Winston和其他大学体育明星一样，清除了强奸罪| Robert Silverman | 2014年12月22日| DAILY BEAST
预算为《1970年马匹保护法》提供了69.7万美元。国会不断赠送的礼物| P.J.O'Rourke | 2014年12月20日| DAILY BEAST
他们会被赶出去吗？就在这一天，当圣母玛利亚刚刚似乎答应帮助和保护她时，他们会被赶出去吗？拉蒙娜|海伦·亨特·杰克逊
否则，这些薄壁的房子对熟睡的旅行者来说就没有什么保护了。我们的韩国小表弟| H.李M.派克
在城市一侧，狭窄的小巷、高大的房屋和坚固的宫殿为围攻者提供了安全的保护。红色的一年|路易斯·特蕾西
他的表妹逗她开心，刺激她，这是汤姆所不能提供的；她向他寻求保护，靠在他身上。波浪|阿尔杰农·布莱克伍德
除了伊诺斯港，那里没有保护船只免受潜艇攻击，而伊诺斯港只有一英寻深。加里波利日记，第一卷|伊恩·汉密尔顿