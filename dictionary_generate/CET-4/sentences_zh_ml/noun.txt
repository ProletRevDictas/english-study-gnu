他自己的文章总是清晰明了，他的散文紧紧地建立在名词和动词的基础上《女王的游戏》是一本畅销书，但它的作者沃尔特·特维斯（Walter Tevis）几乎不是一个一炮走红的奇迹。|迈克尔·迪尔达| 2021年2月3日|华盛顿邮报|
实体是事物、人、地方或概念，可以用名词或名称来表示。NLP和AI如何革新SEO友好内容[帮助你的五种工具]| May Habib | 2020年12月29日|搜索引擎观察
他当时说，雅虎99%的搜索查询中都有一个名词。谷歌搜索通过地图中的忙碌、双工和AR深入“现实世界”|格雷格·斯特林| 2020年10月16日|搜索引擎之地
这种专有名词的嵌套有助于使高等数学不仅对局外人难以理解，而且对试图从一个子领域读入另一个子领域的在职数学家也难以理解。为什么数学家应该停止用彼此的名字命名事物-第89期：黑暗面|劳拉·鲍尔| 2020年9月2日|鹦鹉螺
按照当时的社会科学用法，米德除了在语言学意义上使用性别一词外，从未在其他任何方面使用过性别一词，例如可以归类为阴性、阳性或中性的名词。性别就是你对它的理解-第88期：爱与性|查尔斯·金| 2020年8月5日|鹦鹉螺
当使用专有名词时，可能会与普通名词混淆。Yo | Dale Eisinger的禅宗| 2014年8月2日|每日野兽
名词“mechanicals”指的是对创作和表演作品的任何物理复制，即“罐装音乐”。范戴克·帕克斯讲述了歌曲作者在数字时代是如何陷入困境的|范戴克·帕克斯| 2014年6月4日|每日野兽
“大萧条”作为一个专有名词，直到20世纪50年代才开始流行，那是在大萧条结束很久之后。我们的统治思想是否定|尤金·林登| 2014年5月12日|每日野兽
当我关掉收音机时，我听到的最后一个单词一定是名词，而不是动词、形容词或介词。A.J.雅各布斯：我如何写作|诺亚·查尼| 2013年5月29日|每日野兽
《星际迷航》科幻小说系列于1966年在电视上播出《星际迷航》中的傻瓜：准备好我们的初级读物《走进黑暗》| Sujay Kumar | 2013年5月14日| DAILY BEAST
正如贝尔所建议的那样，我找不到将它作为一个集体名词的权威。乔叟作品，第1卷（共7卷）——玫瑰的浪漫；小诗|杰弗里·乔叟
这常常成为一个抽象的女性名词，与法语中的-e结尾对应；米斯特拉语中的armée是isPg 56 armado。Frdric Mistral |查尔斯·阿尔弗雷德·唐纳
另一方面，当听到“一个黑人”这个词时，大脑就不会构建任何形象；它一直等到修饰的名词说出。中文：作文与文学| W.F.（威廉·富兰克林）韦伯斯特
在平衡句中，一个部分与另一部分是平衡的，-一个名词和一个名词，一个形容词和一个形容词，短语和短语。中文：作文与文学| W.F.（威廉·富兰克林）韦伯斯特
“钟摆”和“强度”这两个词是他第一次使用的，也是他第一次用流体作为名词。文字告诉我们的故事|伊丽莎白·奥尼尔