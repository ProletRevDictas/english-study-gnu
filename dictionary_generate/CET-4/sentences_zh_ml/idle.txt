事实上，情况越来越糟，包括福特和通用汽车在内的许多原始设备制造商都不得不轮班甚至整个工厂。硅芯片短缺正导致汽车制造商的工厂闲置|乔纳森M.吉特林| 2021年2月4日| Ars Technica
弗吉尼亚州官员表示，在冠状病毒大流行期间，他们暂时暂停停用闲置的E-ZPass帐户。弗吉尼亚州暂停停用闲置的E-ZPass账户，以防疫情爆发。莫耶| 2021年2月3日|华盛顿邮报
他们在一些科室工作过度，而在另一些科室则处于闲置状态。尽管在某些情况下，由于冠状病毒的爆发，他们的患者数量有所增加，但他们大体上遭受了巨大的经济损失。如果医院愿意倾听，新冠病毒可以给我们一些教训| Annalisa Merelli | 2021年1月28日|石英
官员们希望这个临时系统能让医院外闲置的救护车腾出空间，因为他们的病人不能入院。洛杉矶患者的氧气不足，因为新冠肺炎住院率在全国范围内创下历史新高| Fenit Nirappil，William Wan | 2021年1月5日|华盛顿邮报|
一个罕见的例外出现在3月流感大流行初期，当时肯塔基州州长安迪·贝希尔命令一家服装退货仓库在那里爆发疫情后闲置。亚马逊关闭新泽西仓库，因新冠病毒-19病例激增|雷切尔·金| 2020年12月21日|财富|
夏天，电梯闲置时，它会向当地社区输送果汁。太阳能滑雪升降机|每日野兽| 2014年11月24日|每日野兽
中年的、身体不好的普京坐在那里无所事事，沉默着，他的梦想和对未来的希望都被摧毁了。柏林墙的倒塌如何使普京变得激进|玛莎·格森| 2014年11月9日|每日野兽
任何即将发布的苹果新产品都会让科技网站发出震耳欲聋的无聊猜测。业余特技演员、iPhone 6和更多病毒式视频|杰克·霍姆斯| 2014年8月30日|每日野兽
其余的警察坐在星巴克前的马背上闲坐着观看。迈克尔·布朗抗议走向全国时的纽约团结|吉迪恩·雷斯尼克| 2014年8月15日|每日野兽
那些无所事事的富人带着他们的制服司机从比亚里茨开车过来。这是海明威的《潘普洛纳》还是《很多公牛》|克莱夫·欧文| 2014年7月13日|每日野兽
我知道这是个无聊的问题；智者和发霉的哲学家都说后悔是愚蠢的。原金|伯特兰·W·辛克莱
他在管教之下工作，寻求安息。愿他的手闲散，寻求自由。《圣经》，杜埃·莱姆斯版本，多种多样
整个营地一片沉寂；有些人在睡觉，那些观看的人没有心情闲聊。布莱克伍德的爱丁堡杂志，第CCCXXXIX号。1844年1月。第LV卷|各种各样的
虽然要塞在国内遭到破坏，但他们并不是无所事事，他们正准备从国外攻打要塞。《牧师的火面》第3卷，共4卷|简·波特
下层阶级又懒又懒，他们愿意为任何通过炫耀吸引他们的君主服务。拿破仑元帅R.P.邓恩·帕蒂森