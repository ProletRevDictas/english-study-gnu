由于无法获得银行融资，该公司的创始人认为他们唯一的选择是以过高的利率向硬通货贷款人借款。机遇区尚未充分发挥其潜力，但不要将其冲销。| jakemeth | 2020年9月16日|财富|
美联储倾向于小幅通胀，因为这给了央行更大的空间来削减或提高短期利率。美联储将短期利率维持在接近零的水平|李·克利福德| 2020年9月16日|财富
实际总数可能更高，因为许多银行在不披露计划的情况下裁员。令人难以置信地萎缩的银行业今年正面临近乎创纪录的失业，《伯纳德·华纳》《财富》（Fortune）杂志，2020年9月16日
当然，弗雷泽将于2月份成为华尔街一家大型银行的首位女性首席执行官。为什么世界上为数不多的女性银行首席执行官之一决定辞职|克莱尔·齐尔曼，记者| 2020年9月16日|财富
Russak Aminach加入第8团队的决定与她之前领导一家可追溯到1902年的传统银行的角色截然不同。她是世界上为数不多的女性银行首席执行官之一。现在，她正在成立一个金融科技风险投资集团，克莱尔·齐尔曼，记者，2020年9月15日，《财富》
一个他们无法在银行兑现以支付他们的公寓的人。《Vogue》的一个封面并不能解决时尚界的重大种族问题| Danielle Belton | 2015年1月2日| DAILY BEAST
一艘拖船高高地停在河岸上，被高高的草丛遮住，附近的水面上悬挂着一个破碎的石油钻井平台。刚果被遗忘的殖民地逃亡|尼娜·斯特罗奇利奇| 2014年12月18日|每日野兽
但就在陡峭的河岸上，灌木丛中有一个开口。刚果被遗忘的殖民地逃亡|尼娜·斯特罗奇利奇| 2014年12月18日|每日野兽
他对央行不理解这一点感到惊讶。克里米亚如何摧毁俄罗斯经济|安娜·涅姆佐娃| 2014年12月17日|每日野兽
在他生命的这一点上，丹顿的银行账户里有足够多的不义之财，足以影响他对这些东西的某种兴趣。尼克·丹顿所说的福音对于Gawker创始人来说下一步是什么|劳埃德·格罗夫| 2014年12月14日|每日野兽
在对票据进行分类时，必须能够容易地区分本银行的票据和其他储备银行的票据。货币和银行读物|切斯特·阿瑟·菲利普斯
到目前为止，波士顿银行从这家银行获得的利益比这一地区的其他银行多。货币和银行读物|切斯特·阿瑟·菲利普斯
生长在每一个水面上和河岸上的野草，应在所有草之前拔除。《圣经》，杜埃·莱姆斯版本，多种多样
大多数银行在储备银行保留的休眠账户，或许表明了它们对此的态度。货币和银行读物|切斯特·阿瑟·菲利普斯
在储备银行，他们可以作为常备权利借款，而不是作为可能被切断的恩惠借款。货币和银行读物|切斯特·阿瑟·菲利普斯