通过狗的气味检测，用气味材料筛查结直肠癌。医生现在会嗅你的气味-第95期：逃跑|丽娜·泽尔多维奇| 2021年2月3日|鹦鹉螺
它充满了我的头脑，那低沉的声音，像从一个油腻的大桶里冒出的浓烟，或是一股令人讨厌的腐烂气味。阅读《黄色的国王》，这是《真正的侦探》这部剧的关键参考资料|罗伯特·W·钱伯斯| 2014年2月20日|每日野兽》
它在余烬中卷曲扭曲，仿佛它是一个活物；一股烟，一股刺鼻的气味，然后就消失了。《Pit Town Coronet》，第一卷（共3卷）|查尔斯·詹姆斯·威尔斯
尽管上面覆盖着芳香植物，但还是有一股难闻的气味。卡米尔（卡米利亚夫人）|亚历山大大仲马
科利姆树散发出一种非常强烈的气味，类似于洋葱的气味，这表明它的位置在远处。女人的环球之旅|艾达·普费弗
他回避的独特之处，夹杂着某种异国情调，就像一个深受太阳喜爱的国家的遥远的香水味。查尔斯·波德莱尔，他的一生
它有百里香的气味，微溶于水，但极易溶于酒精、乙醚和碱性溶液。大英百科全书，第11版，第5卷，第2卷，各种