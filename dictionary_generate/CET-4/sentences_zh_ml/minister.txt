菅直人经常被提及为安倍的众多可能继任者之一，特别是在最近几个月首相缩减公开露面并面临一系列涉及亲友的政治丑闻之际。草莓采摘者之子如何成为日本下任首相最有可能的选择|克莱钱德勒| 2020年9月3日|财富|
计划于12月19日在德里的红堡举行大规模的反政府抗议活动，这是一座历史性的纪念碑，总理通常在独立日向全国发表电视讲话。播客：一项135年的法律如何让印度关闭互联网|安东尼·格林| 2020年9月2日|麻省理工技术评论
这项以医疗为由给予豁免的政策得到了他的工业、科学和技术部长的支持。澳大利亚不会是最后一个与强制性冠状病毒疫苗搏斗的国家，奥利维亚·戈德希尔（Olivia Goldhill）将于2020年8月20日（Quartz）
当首相说，“没有哪个国家会在地下找到1730亿桶石油，然后把它们留在那里，”他没有错。油砂引发了关于我们能源需求的激烈全球辩论。现在，它们可能是未来命运的一个标志——2020年8月20日
今年7月，梅的前任外交部长鲍里斯·约翰逊（Boris Johnson）以及在此之前的伦敦市长接替了梅的总理职务。呼吁英国退出的首相（第392页）|斯蒂芬·J·杜布纳| 2019年10月10日|畸形经济学|
在国内，首相坚持一条可疑的路线，即他是唯一能够维持仍然脆弱的和平的人。柬埔寨的烟雾与镜子民主|大卫·沙夫特尔| 2015年1月9日|每日野兽
其最高官员之一是巴格哈德现任内政部长。伊朗葬礼向我们讲述了伊拉克战争的故事|伊朗战争| 2015年1月6日|每日野兽
即使是那些想在种族和治安的仇恨中事奉的基督徒也没有抓住重点。没有神，没有警察，没有主人|詹姆斯·普洛斯| 2015年1月1日|每日野兽
他说：“我已经与我国外交部长进行了协调，因此我们将向其他提供援助的国家借款。”。亚航QZ8501航班的假定坠机事件与MH370 | Lennox Samuels | 2014年12月29日| DAILY BEAST完全不同
哈米什·马歇尔本人曾是哈珀总理的工作人员。加拿大石油商如何粉饰Keystone管道|杰伊·迈克尔森| 2014年12月28日|每日野兽
“但是，”弗拉特兰的首相开始了一个难题，“谁将是最伟大的酋长？北方的巨人R.M.巴兰坦。”
无论是王子还是牧师，他都声称厌恶和鄙视一切神秘、优雅和诡计。格列佛游记|乔纳森·斯威夫特
她的幽默之一是将牧师的儿子与守寡的圣日耳曼女王的侄女结合在一起。《牧师的火边》第3卷，共4页，简·波特
牧师的目光始终停留在某一点上，把他所统治的国家提升到人间最伟大的顶峰。《牧师的火面》第3卷，共4页，简·波特
所需要的只是对内阁部长略知一二，并略知一二。Punch，或《伦敦查里瓦里》（the London Charivari），第107卷，1894年11月3日