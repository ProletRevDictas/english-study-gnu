这是我的逃避，但也是我联系和建立友谊的桥梁。2021年2月7日，摩城（Motown）总统埃塞•哈伯特马里亚姆（Eben Shapiro）带领传奇品牌度过大流行
从卡罗尔和哈利身上，泰奇纳了解到，我们都有能力在人生的任何年龄或阶段创造和建立新的友谊和依恋。一只“钱坑”狗，一个生病的主人和一个大问题|黛安·科尔| 2021年2月5日|华盛顿邮报|
一封信，甚至只是一次谈话，说明友谊对你有多重要，那将是多么可爱。礼仪小姐：对不起，接待员不会记得你的名字|朱迪思·马丁、尼古拉斯·马丁、雅各比娜·马丁| 2021年2月5日|华盛顿邮报|
就埃里克而言，他似乎对未来与阿曼达的接触感兴趣，但只是在友谊的基础上。日期实验室： 对立没有吸引，但也没有排斥。| Rich Juzwiak | 2021年2月4日|华盛顿邮报
正如《萤火虫巷》一样，这部新小说挖掘了友谊的力量。关于Netflix《萤火虫巷》背后的书，你知道些什么？安娜贝尔·古特曼时间2021年2月3日
他们的友谊始于克劳斯，他是克利夫兰凯斯韦斯特大学物理系主任，寻找爱泼斯坦。下流亿万富翁的双重生活与斯蒂芬·霍金的海滩派对| M.L.雀巢| 2015年1月8日|每日野兽
我们相处得非常好，开始了真正的友谊。李·马文的自由帷幔微笑背后的故事|罗伯特·沃德| 2015年1月3日|每日野兽
他们牢固的友谊是首映式中最动人的细节之一，但这也让布兰森陷入了棘手的困境。唐顿时尚的真正含义|凯蒂·贝克| 2015年1月2日|每日野兽
甚至有幽默、友谊和爱情的时刻，也有宗教或缺乏宗教的时刻。”《行尸走肉》节目主持人斯科特·金普尔（Scott Gimple）取笑《未来更黑暗、更怪异的时代》|梅丽莎·莱昂| 2014年12月2日|每日野兽报
一款时尚的小型Scocha友谊手镯是表达你对你的挚爱的新方式。《每日野兽》2014年节日礼物指南：献给你生命中的蓝色常春藤| Allison McNearney | 2014年11月29日|每日野兽
当路易向友谊和天堂发出热烈的呼吁时，沃顿的面颊上泛着炽热的红晕。《牧师的火面》第3卷，共4卷|简·波特
他们之间产生了一种友谊，多年来这种友谊是理想化的，而不是受损的。阿里斯蒂德·普约尔|威廉·J·洛克的欢乐冒险
对汤姆和我来说，这是一见钟情的友谊，在最后的分手到来之前，没有任何东西能扰乱它的进程。英格兰、苏格兰和爱尔兰50年的铁路生活|约瑟夫·塔洛
我们的友谊是密切而亲密的，这种友谊是在青春的温暖中形成的，只有坟墓才能融化。英格兰、苏格兰和爱尔兰50年的铁路生活|约瑟夫·塔洛
我们的品味和习惯非常相似，但我们之间的差异足以使我们的友谊增添情趣。英格兰、苏格兰和爱尔兰50年的铁路生活|约瑟夫·塔洛