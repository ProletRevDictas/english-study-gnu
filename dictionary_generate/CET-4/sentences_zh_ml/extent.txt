他说，需要更多的分析来确定超速驾驶在多大程度上导致了与速度有关的车祸的增加，但去年的数据显示了一个令人不安的趋势。在冠状病毒大流行期间，交通量下降，但道路死亡人数仍在增加。|卢兹·拉佐| 2021年2月12日|华盛顿邮报
在海洋中，生物体一生都会在体内积累这些碎片，有时会达到有害或致命的程度，反过来，我们吃的海鲜中也会出现微塑料。微塑料无处不在。这对我们的健康意味着什么|Ula Chrobak | 2021年2月11日|科普
大多数学生可能要等到秋天才会看到这些变化，当时该校表示，预计将“尽可能地”重新开放。乔治华盛顿大学承诺禁止一次性使用塑料|劳伦·隆普金| 2021年2月11日|华盛顿邮报|
在某种程度上，双方都有一种错觉，如果你取消了230条款，那将神奇地解决他们所有的问题。26个单词创造了互联网。要怎样才能挽救它|斯蒂芬·恩格尔伯格| 2021年2月9日|出版
俄勒冈州众议院农业和自然资源委员会负责人布拉德·维特（Brad Witt）表示：“作为一个州，我们必须尽可能确保维持一个公正、平衡和公平的税收体系。”。“我们有深陷困境的县”：俄勒冈州立法者试图扭转让社区损失数十亿美元的木材税减免政策，该政策由俄勒冈州/俄勒冈州直播节目Rob Davis和俄勒冈州公共广播节目Tony Schick于2021年2月5日提出
我父亲是一名水手，在我的整个童年时期，他有一半时间在海上，在某种程度上，我也有一份类似的工作。Belle&Sebastian不再那么害羞了| James Joiner | 2015年1月7日| DAILY BEAST
而且，正如Gow从自己的亲身经历中挖苦地补充道，“在很大程度上，他们很好地实现了这个目标。”“纳粹奶牛”试图杀死英国农民汤姆·赛克斯（Tom Sykes）（2015年1月6日）（DAILY BEAST）
这就是火焰最盛时期的范围，以及第二天媒体上的数字。多伦多名人热点索托·索托的火热死亡| Shinan Govani | 2014年12月30日|每日野兽
我不认为它会达到它所能达到的程度，但我们的办公室并没有感到震惊。老迈克尔·布朗的洗礼和弗格森的火洗礼|贾斯汀·格拉维| 2014年11月27日|每日野兽
大陪审团听到的证词在多大程度上得到了法医证据的证实或反驳？弗格森的大陪审团买下了达伦·威尔逊的故事《保罗·坎波斯》2014年11月25日《每日野兽》
但是，我们正在进入的这片平原是多么壮丽啊：它是一片辽阔的土地。布莱克伍德的爱丁堡杂志，第60卷，第372期，1846年10月
不可能对堆积在一起的艺术作品的范围、多样性和卓越性有充分的概念。欧洲概览|霍勒斯·格里利
烟草是一种生长旺盛的植物，其耐热性和抗旱性远远超过大多数植物（第018页）。烟叶它的历史、品种、文化、制造和商业——E.R.比林斯。
我很高兴地说，至少在某种程度上，M'Bongo和他的整个法庭现在都穿上了衣服。《Pit Town Coronet》，第一卷（共3卷）|查尔斯·詹姆斯·威尔斯
事实上，长期的征服让意大利人民士气低落到了可怕的程度。欧洲概览|霍勒斯·格里利