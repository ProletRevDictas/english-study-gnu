希腊的回应是派遣自己的船只跟踪土耳其的船只。蝴蝶效应：下一个美俄冲突战区可能是地中海|卡斯图里| 2020年9月17日|奥齐
在量子现实的早期，巴戈特将科学比作一艘在“表象之海”上来回航行的船，从经验现实的岩石海岸到形而上学现实的沙滩。《量子力学的多种含义指南——如此浪漫的事实》| Sabine Hossenfelder | 2020年9月3日| Nautilus
在美洲，最近发生了一系列针对墨西哥坎佩切湾石油平台和船只的袭击事件。海上海盗活动死而复生了吗|Eromo Egbejule | 2020年8月25日| Ozy
尤其是在DTC的世界里，无论出于何种原因，媒体买家似乎都是掌舵者，在战略上做出了许多决策美元的数额不值得精神上的损失：一位媒体买家在疫情期间保持业绩的压力下的自白《克里斯蒂娜·蒙洛斯》2020年8月25日《Digiday》
一个模块将宇航员从轨道上的一艘更大的飞船运送到月球表面。以下是美国宇航局的下一个月球着陆器的样子|亚伦·普雷斯曼| 2020年8月21日|财富
船长发出弃船命令后，150人乘坐电子武器降下的救生艇逃生“我们要死了”：幸存者讲述希腊渡轮火灾恐怖|芭比·拉萨·纳多| 2014年12月29日|每日野兽
亚当·罗杰斯的《书呆子游船》（Nerd Cruise），连线了800名游船上的书呆子教给我的关于生命、宇宙和浮潜的知识。《每日野兽》最佳长读物，2014年12月22-28日|威廉·布特| 2014年12月28日|每日野兽
船上只有一个浴室，没有淋浴和床。在充斥着欧洲难民的走私网络内部| Barbie Latza Nadeau | 2014年12月15日| DAILY BEAST
北极探险队进行了两年后，他们被迫在西伯利亚以北一千英里处弃船。2014年最佳非小说类书籍|威廉·奥康纳| 2014年12月14日|每日野兽
预计这款设备的发货日期是2014年12月，这正是说“再见，永远吸烟”的最佳时机。没有什么比数据《每日野兽》2014年12月8日《每日野兽》更能说明我爱你
智慧人不恨诫命和公义，也不会像船在暴风雨中被摔成碎片。《圣经》，杜埃·莱姆斯版本，多种多样
总统坐在一把椅子上，椅子上坐着朝圣者，他们乘坐的是五月花号。布莱克伍德的爱丁堡杂志，第60卷，第372期，1846年10月
这更像是上船，而不是我所见过或想象过的任何陆地战斗。布莱克伍德的爱丁堡杂志，第CCCXXXIX号。1844年1月。第LV卷|各种各样的
我的命令应该在一个没有受伤的军官或男子被送回船上之前执行。加里波利日记，第一卷|伊恩·汉密尔顿
真想不到那个巨大的炮弹突然出乎意料地掉到了一艘满载士兵的甲板上！加里波利日记，第一卷|伊恩·汉密尔顿