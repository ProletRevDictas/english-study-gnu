美国海军第五舰队驻扎在这个小王国，因此他们都有保持友好的军事和经济理由。特朗普宣布巴林将与以色列实现关系正常化|亚历克斯·沃德| 2020年9月11日| Vox
通过分析来自动物王国的数百个基因组，西班牙和英国的研究人员表明，令人震惊的基因缺失遍布生命之树。由于失去了基因，生命往往进化得更加复杂。| Viviane Callier | 2020年9月1日| Quanta杂志
认识到基因丢失对整个动物王国的进化非常重要，这为研究打开了新的大门。由于失去了基因，生命往往进化得更加复杂。| Viviane Callier | 2020年9月1日| Quanta杂志
被捕食者消化后存活下来是很罕见的，但在动物界并非闻所未闻。水甲虫在被青蛙吃掉和排泄后可以存活|乔纳森·兰伯特| 2020年8月3日|科学新闻
从亚马逊丛林中的大树到室内植物，再到海洋中的海藻，绿色是主宰植物王国的颜色。为什么植物是绿色的？减少光合作用中的噪音|Rodrigo Pérez Ortega | 2020年7月30日| Quanta杂志
但要赞扬王国的仁慈：一次只能使用50根睫毛。为亵渎辩护|迈克尔·托马斯基| 2015年1月9日|每日野兽
英国、法国和澳大利亚等国都建立了各种制度。宣传、抗议和毒蛇：朝鲜电影战争| Rich Goldstein | 2014年12月30日|每日野兽
愤怒的伊朗官员指责美国和英国支持武装分子，而巴基斯坦则无动于衷。危险药物资助的伊朗和巴基斯坦之间的秘密战争| Umar Farooq | 2014年12月29日| DAILY BEAST
这位年轻的约旦飞行员来自约旦一个著名的军人家庭，他的叔叔是一位退休少将。伊斯兰国击落了一架战斗机吗|Jamie Dettmer，Christopher Dickey | 2014年12月24日|每日野兽
联邦调查局和总统可能会声称，隐士王国应该为史上最引人注目的网络漏洞负责。不，朝鲜没有入侵索尼|马克·罗杰斯| 2014年12月24日|每日野兽
但我在那个王国经历过的最大的危险来自一只猴子，它属于厨房的一个职员。《格列佛游记》|乔纳森·斯威夫特
但是，如果我告诉他的是真的，他仍然不知道一个王国怎么会像一个私人一样耗尽它的财产。《格列佛游记》|乔纳森·斯威夫特
你要使国分裂，使以法莲成为悖逆的国掌权。《圣经》，杜埃·莱姆斯版本，多种多样
奖品是盘子，利润将用于修复王国的避风港。《每日历史与年表》乔尔·蒙塞尔
虽然多数人以某种方式退出，但有一个活跃的少数人希望纳纳人建立一个独立的王国。红色的一年|路易斯·特蕾西