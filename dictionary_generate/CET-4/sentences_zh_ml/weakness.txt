库珀作为编剧和导演的初出茅庐有一些严重的弱点，但这些弱点被库珀和嘎嘎之间的电化反应所掩盖。《金库：最佳女主角》版|布莱恩·T·卡尼| 2020年8月20日|华盛顿刀刃
许多人（如果不是所有人的话）可能都有非常特殊的基因脆弱性，比如他们的免疫系统的弱点，直到一种特定的病原体穿过他们的路径时才会被注意到。我们的基因可能解释了新冠病毒-19和其他感染的严重性| Monique Brouillette | 2020年7月27日| Quanta杂志
了解品牌健康状况的细节将有助于您了解品牌的优势和劣势，并帮助您决定未来的相关行动。七个品牌健康指标及其衡量方法| Aleh Barysevich | 2020年7月24日|搜索引擎观察
侧通道攻击不是利用底层密码系统中的弱点，而是以软件和硬件实现的方式进行攻击。中国向不可破解的量子通信迈出了新的一步| Edd Gent | 2020年6月15日|奇点中心
然而，波兰确实发现了“有趣”的观点，即降雨可能导致地面的弱点，从而导致熔岩喷发。雨水是否使基拉韦厄火山的熔岩形成速度过快|Megan Sever | 2020年5月15日|学生科学新闻
事实上，机场安全专家长期以来一直担心这一弱点。给圣战者的礼物：看不见的机场安全威胁|克莱夫·欧文| 2014年12月27日|每日野兽
柯尼格为她似乎视为软弱的表现而道歉。阿德南杀了她！不，是杰干的！连续剧的不确定、真实结局|艾米丽郡| 2014年12月18日|每日野兽
她变得极不受欢迎，并被广泛指责为国王随着年龄增长而日益虚弱的罪魁祸首。理查德三世国王的兰迪曾曾曾祖父的性生活|汤姆·赛克斯| 2014年12月4日|每日野兽
像现在这样的情况很可能会加剧他的弱点。以色列犹太教堂大屠杀后：新的起义|Michael Tomasky | 2014年11月19日|每日野兽
是的，2014年是共和党的一大胜利，但奥巴马在中期选举中表现出异常软弱的想法是不准确的。奥巴马：有史以来最跛脚的鸭子|乔纳森·奥尔特| 2014年11月6日|每日野兽
如此屈服于这些阴谋，从来没有表现出比这更愚蠢、更应受谴责的弱点。埃尔斯特的愚蠢|亨利·伍德夫人
我相信，用法律的话来说，他是在逃避一个次要问题。自信|亨利·詹姆斯
奥尔克拉夫特退缩了，因为每一个音节都显示出说话者的实际实力——他自己的依赖性和彻底的弱点。布莱克伍德的爱丁堡杂志，第CCCXXXIX号。1844年1月。第LV卷|各种各样的
尽管他们操作巧妙，但清洁并不是他们的弱点。烟叶它的历史、品种、文化、制造和商业——E.R.比林斯。
同样，当他只爬了一段路时，他可能会晕眩，或者因为虚弱而失去控制。Alila，我们的菲律宾小表妹玛丽·哈泽尔顿·韦德