例如，罗德里格斯说，医务人员进行的体检不充分，因为他们将听诊器放在两到三层衣服上。古巴医生在冰上拘留期间感染了冠状病毒|亚里尔·巴尔德斯·冈萨雷斯| 2020年9月9日|华盛顿
心脏监护仪每秒读取多次数据，而常规体检的信息每年可能只有一到两次。迪斯科舞厅、喇叭裤、大头发……还有尖端的人工智能|杰里米·卡恩| 2020年9月8日|财富
这意味着他的访问将包括一个扩展的、以问题为中心的历史和考试。一名医生到自己的雇主那里做了新冠病毒-19抗体测试。它花了10984美元|Marshall Allen | 2020年9月5日| ProPublica
这是对我们人类的某种审视，而不是真正的犯罪本身，这可能有点像麦克古芬。“人们想要相信”：爱情欺诈如何围绕一个浪漫的骗子建立一个吸引人的家庭| Alissa Wilkinson | 2020年9月4日| Vox
有人告诉她，她的高考成绩已经获得了南京邮电大学英语系的录取。《中国保护数据隐私的意外探索》泰特·瑞安·莫斯利（Tate Ryan Mosley）2020年8月19日《麻省理工学院技术评论》
对围绕《星球大战》经典中最新作品的一些谣言的审查。最有趣的《星球大战：原力觉醒》谣言（以及一些被揭穿的谣言）| Rich Goldstein | 2015年1月3日| DAILY BEAST
对美国及其电影在大韩民国的复杂历史的考察。宣传、抗议和毒蛇：朝鲜电影战争| Rich Goldstein | 2014年12月30日|每日野兽
所有这些都需要严格的自我审视：当你可以成为任何你想成为的人时，你想成为什么？男人需要更好的男权运动|南希·卡弗| 2014年12月16日|每日野兽
通过电话进行心电图检查，然后送医生检查。阿尔弗雷德·希区柯克的《褪色为黑色：伟大导演的最后日子》|大卫·弗里曼| 2014年12月13日|每日野兽报
如果证人真的目睹了如此可怕的罪行，证词将在盘问的坩埚中保存下来。弗格森的秘密陪审团中没有阴谋|保罗·卡伦| 2014年11月17日|每日野兽
然而，作为一项规则，即使是在极端品种的情况下，对样本的仔细检查将使其能够被识别。如何认识蕨类植物Leonard Bastin
船长喝了一口凉茶，让他安静下来，让他坐下，然后开始检查这本书。北方巨人| R.M.巴兰坦
事实上，在马尔科姆第一次疯狂地检查房子时，他没有看到躺在那里的几十具尸体。红色的一年|路易斯·特蕾西
这是一次筛选证据和检查外表的巨大训练。拯救文明| H.G.（赫伯特·乔治）威尔斯
这比死记硬背然后忍受竞争性考试的折磨要简单得多。潘趣，或《伦敦查里瓦里》，第107卷，1894年11月3日