今年夏天，加利福尼亚、俄勒冈州和华盛顿州也极度干旱，各州大部分地区处于“严重干旱”状态，一些地区达到“极端干旱”。“前所未有”：加利福尼亚、俄勒冈州和华盛顿的野火背后是什么
我们刚刚度过了一个潮湿的冬天，这意味着水库和土壤不像过去干旱时那样干燥。晨报：MTS执法负责人离开|圣地亚哥之声| 2020年7月28日|圣地亚哥之声
寒冷的夏天、干旱、饥荒和瘟疫摧毁了世界各地的社会。地球气候如何自然变化（以及为什么现在情况不同）| Howard Lee | 2020年7月21日| Quanta杂志
这是联合国同意的防止灾难性气候变化的数量——就像吞没整个沿海城市的海洋，非常非常严重的野火和无法忍受的干旱。环境报告：SDG&E和Sempra的最新权力斗争| MacKenzie Elmer | 2020年6月29日|圣地亚哥之声
因此，研究小组只在干旱期间进入深舱。《在深洞穴中猎取恐龙的挑战》|约翰·皮克雷尔| 2020年5月19日|学生科学新闻
它的标题是保护你们的森林免遭破坏，保护你们的国家免遭洪水和干旱。塑造和颠覆政客的杂志| Anthony Haden Guest | 2014年11月2日| DAILY BEAST
其次，博洛格帮助开发了更高产、更耐旱的水稻品种，这些品种在亚洲得到了广泛的适应。成长股|每日野兽| 2014年10月17日|每日野兽
从加利福尼亚的干旱到ENIAC的女性，《每日野兽》本周从网络上挑选了最好的新闻。《每日野兽》最佳长读物，2014年9月22-28日|约翰·布特| 2014年9月28日|每日野兽
干旱现在正在夺走加利福尼亚州已有百年历史的农场。《每日野兽》最佳长读物，2014年9月22-28日|约翰·布特| 2014年9月28日|每日野兽
应对干旱和边缘土壤是一场持续的斗争。”马具制造商的梦想：“不太可能的德克萨斯牧场之王”| Nick Kotz | 2014年9月20日| DAILY BEAST
烟草是一种生长旺盛的植物，其耐热性和抗旱性远远超过大多数植物（第018页）。烟叶它的历史、品种、文化、制造和商业——E.R.比林斯。
这个新国家的情况每况愈下，如果这个季节再次发生旱灾，最坏的情况就来了。宅基地主人奥斯卡·米切奥
有一天，她听到一个男人说，“如果发生旱灾，我们将在冬天结束之前用我们的股票支付魔鬼的代价。”拉蒙娜|海伦·亨特·杰克逊
在这一点上，我们有一个典型的例子，那就是在长期干旱后产生雨水的水族馆仪式。古罗马的宗教|西里尔·贝利
有乾旱临到她的水，水就必乾涸。因为这是偶像之地，他们以奇事夸耀。《圣经》，杜埃·莱姆斯版本，多种多样