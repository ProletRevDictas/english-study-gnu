最好的加热毛毯将提供安全，可定制的热量和完全的舒适感，没有可辨别的电线，只有一种毛绒，颓废温暖的感觉。最佳加热毛毯：与这些电毯捆绑在一起| PopSci商业团队| 2021年2月11日|科普
我们建议您随身带上这个，或者带上一两个一次性的暖手器，以防您在那些轻快的清晨需要更多的温暖。最佳暖手器：在你最喜欢的冬季活动中阻挡寒冷| PopSci商业团队| 2021年2月10日|科普
虽然耐用性和保暖性是关键属性，但我最欣赏秋线的灵巧。最好的滑雪手套是最简单的|乔·杰克逊| 2021年2月10日|户外在线|
当你工作时，寻找额外的温暖如果你的工作带你外出或在非加热区域，你需要用最好的寒冷天气装备保护自己免受恶劣天气的影响。最佳保暖背心：用合适的冬季装备抵御寒冷| PopSci商务团队| 2021年2月9日|科普
当天晚些时候，云层可能会增加，使我们失去一些阳光的温暖。华盛顿地区预报：今天天气转暖，有微风。周六晚上下雪的几率上升到周日|A.卡姆登·沃克| 2021年2月5日|华盛顿邮报
香气随着温度的升高而增加，而寒冷会使它们变得迟钝。香槟：你喝错了| Kayleigh Kulp | 2014年12月20日| DAILY BEAST
当莎拉和她的姐姐及母亲前往温暖的家时，示威继续进行。犹太教堂屠杀：当警察必须杀死时| Michael Daly | 2014年12月10日| DAILY BEAST
即使是她最极端的言论，在表达时也带着微笑，散发出友好的小镇邻居的温暖。乔尼·恩斯特的得梅因注册Diss是不是毁了她的“爱荷华尼斯”|Ben Jacobs | 2014年10月25日|每日野兽
你从舞蹈中获得的温暖和灵魂，如果有人阻止你这样做？Nigel Lythgoe谈如何拯救真人秀电视节目《小镇上》和《断背舞厅》|凯文·法伦| 2014年10月22日|每日野兽
我把雨披裹在身上取暖，在寂静的黑暗中等待。哥伦比亚萨满精神之旅| Chris Allbritton | 2014年8月24日|每日野兽
中等音高表达温暖、情感和内心品质。富有表现力的声音文化|杰西·埃尔德里奇·索斯威克
对微小事物的感觉可能包含着一种年轻人个人同情的温暖。儿童的方式|詹姆斯·萨利
安妮小姐和蔼可亲地笑了笑，没有想到他的困惑，被他南方的温暖逗乐了。阿里斯蒂德·普约尔|威廉·J·洛克的欢乐冒险
我们的友谊是密切而亲密的，这种友谊是在青春的温暖中形成的，只有坟墓才能融化。英格兰、苏格兰和爱尔兰50年的铁路生活|约瑟夫·塔洛
英国炉边的宁静舒适和发自肺腑的温暖一定值得欣赏。欧洲概览|霍勒斯·格里利