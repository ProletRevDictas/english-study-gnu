官方的说法是艺术和自然和谐相处，但事实上，艺术和景观都试图超越自然，改造自然，掌握自然。在流感大流行期间，雕塑公园是观赏艺术的绝佳方式。这就是为什么有些人比其他人更好|塞巴斯蒂安·斯密| 2021年2月11日|华盛顿邮报
鲁芬在绝望中寻求真理讲述中的幽默和谐，但这并不总是容易的。深夜电视巨星Amber Ruffin和她的妹妹共同写了一本关于种族主义的书。而且，是的，这很搞笑|Hau Chu | 2021年2月8日|华盛顿邮报
这就把这个决定留给了第二任丈夫和妻子，并警告说，如果他们重视家庭和谐，他们将不会接受他们没有获得的头衔。礼仪小姐：停止试图取悦虐待父母|朱迪思·马丁、尼古拉斯·马丁、雅各比娜·马丁| 2021年1月27日|华盛顿邮报|
我们已经看到世界上的一些地区，邻居们彼此和谐相处，但总有发生这种情况的危险。人类越来越善于发动战争——第94期：进化|史蒂夫·保尔森| 2021年1月6日|鹦鹉螺
我们的实验表明，美国人发现那些遵守保护他人感情或保持群体和谐的人更热情、更有能力、更真实。我们如何鼓励人们戴口罩-为了他人| Shai Davidai | 2020年12月24日| Vox
我必须在和他演奏和声的同时演奏旋律。堆栈：约翰·科尔特兰的强大音乐探索|纳特·亨托夫| 2014年10月18日|每日野兽
当她还是个小婴儿的时候，她的母亲会唱她的摇篮曲，当她开始和声地唱这些摇篮曲时，她会“完全被吓坏了”关于贝斯歌手梅根·特莱纳的《仇恨者》和她的两极分化（不太可能）头号热门歌曲《马洛·斯特恩》2014年10月7日《每日野兽》
低质量黑洞与自身和谐地“歌唱”，尽管是用闪光代替声音。黑洞的金发姑娘| Matthew R.Francis | 2014年8月24日|每日野兽
很快，封面版本就被《骨头暴徒-和谐》、《毒品》和《对机器的愤怒》等剧目录制下来。“F*ck the Police”一词的简史| Rich Goldstein | 2014年8月23日| DAILY BEAST
他想要和平与和谐，在这方面，他只是另一位对帝国统一感兴趣的罗马统治者。策划尼西亚三世可能是教皇方济各的杰作| Candida Moss | 2014年6月8日| DAILY BEAST
它们是如此的和谐，如此的怪异，如此的狂野，以至于当你听到它们的时候，你就像一朵海草撒在海洋的胸前。德国音乐研究|艾米·费伊
很久以后，在除了天才儿童以外的所有儿童中，和谐的奥秘开始呈现出明确的形式和意义。儿童的方式|詹姆斯·萨利
房间里出现了严重的不和谐，不久前至少有一种表面上的和谐。波浪|阿尔杰农·布莱克伍德
她对家庭和睦的一瞥，使她既不后悔，也不渴望。《觉醒》与短篇小说选集《凯特·肖邦》
与基本法治相一致的是，一名成员一旦被宣告无罪，就不能因同一罪行再次受审。普特南为门外汉准备的简易法律书|阿尔伯特·西德尼·博尔斯