几何学和数论之间的联系给了数学家们一个机会，但他们必须努力加以利用。经过几十年的计算机搜索，四面体解终于被证明了。| Kevin Hartnett | 2021年2月2日| Quanta杂志
一些基本几何图形是相同的，而一些主要方面已经被润色，特别是角色模型和背景几何图形。Goldeneye 007丢失的Xbox 360 remaster已作为全场快跑泄露| Sam Machkovech | 2021年2月1日| Ars Technica
在这里，目标的几何形状是相关的，因为现在连续的箭头落在同一个环内的概率不是零。你能找到神秘的数字吗|扎克·维斯纳·格罗斯| 2021年1月15日|第五届38
人们对这些曲面的几何学知之甚少，但在过去的几十年里，数学家们已经能够证明在某些情况下超曲面总是有直线的。数学家复活希尔伯特的第13个问题|斯蒂芬·奥恩斯| 2021年1月14日|量子杂志
然而，乐高积木的这些基本部分是一些实体，它们的波包，原则上，在普朗克尺度的连续几何概念开始失去意义之前，你可以把它们打包成你想要的小区域。测量自然构成要素的突破——如此浪漫的事实| Subodh Patil | 2021年1月8日| Nautilus
穿过邓恩公园附近的市中心，敏锐的观测者可以看到一个隐藏的扭曲几何的游乐场。硅谷大厦，被活活吞没|杰夫·马诺| 2014年11月8日|每日野兽
一切都变成了一种针对IDF几何和抛物线的计算。塔利班战斗季节开始时在阿富汗躲避火箭弹| Nick Willard | 2014年5月14日| DAILY BEAST
他把他的论文写成了《关于微分几何新发展的物理学几何微扰理论》一书。这就是当你教机器自然选择的力量时所发生的事情|詹姆斯·巴拉特| 2014年2月1日|每日野兽
如果你懂几何学，我想你在科学上会懂很多。Michel Gondry和Noam Chomsky在干什么|Jimmy So | 2013年11月20日|每日野兽
我认为在几何学中，如果不是凭直觉的话，也可以凭你的感性得出非常准确的结论。Michel Gondry和Noam Chomsky在干什么|Jimmy So | 2013年11月20日|每日野兽
哲学与几何学同步，那些观察自然的人也以深奥的计算为荣。历史的灯塔之光，第一卷|约翰·洛德
在文法学校也教音乐和几何学，这些使儿童时期的普通教育得以完成。罗马人的私生活|哈罗德·惠斯通·约翰斯顿
当句子的其余部分加上“努力学习我的几何课”时，整个句子必须重新构建。中文：作文与文学| W.F.（威廉·富兰克林）韦伯斯特
人们认为几何学是从埃及传入希腊的，天文学和算术是从Phœnicia传入希腊的。斯特拉博地理，第三卷（共3卷）|斯特拉博
他在绘画中看到了一种抽象的几何学，这种几何学存在着硬朗的形式。现代绘画史，第1卷（共4卷）|理查德·穆瑟