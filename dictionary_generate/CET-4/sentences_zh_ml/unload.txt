造成预计降雪的风暴与在加利福尼亚州山区卸下高达9英尺雪的风暴相同，并将于周日席卷平原和中西部地区。周日，冬季暴风雪将华盛顿地区覆盖在中雪中，周一可能还会有更多暴风雪。|杰森·萨莫诺，韦斯·容克| 2021年1月31日|华盛顿邮报
这是关于他如何让男孩触摸一次未卸下的枪，以满足他的好奇心，以及他如何看到他的儿子逐渐了解警察工作的复杂现实。蔡斯·杨的成长经历使他成为一位“疯狂的不同寻常”的领导人，华盛顿已经开始追随《华盛顿邮报》|山姆·福蒂尔| 2021年1月1日|
巨大的铝制集装箱从飞机上卸下，装上小车，小车将集装箱拉到输入区域的几条线中的一条线上。一名临时工在联邦快递没有修复已知的危险后在工作中死亡。罚款：7000美元|Wendi C.Thomas著，MLK50：通过新闻业实现正义| 2020年12月23日| ProPublica
也就是说，信托结构意味着Grayscale必须将信托中股票的优先购买权出售给富有的投资者，他们可以在一年后在公开市场上出售这些股票$20000比特币为Grayscale难以置信的一年（杰夫，2020年12月16日）财富封顶
据一些报道，作为一名年轻的博物馆员工，她帮助从德国卸下或记录了早期交付的奖杯艺术品。俄罗斯文化生活的大夫人伊琳娜·安东诺娃于2020年12月4日在《华盛顿邮报》去世，享年98岁
身为财政部长的盖特纳终于在《压力测试》中脱颖而出。快速阅读：蒂莫西·盖特纳的新回忆录《威廉·奥康纳》2014年5月13日《每日野兽》中最有趣的部分
考试结束后，他们经常邀请申请者卸下他们可能承受的任何精神负担。在测谎仪测试中，将成为边境巡逻人员的特工承认犯罪|安德鲁·贝克尔| 2013年4月4日|每日野兽报
当罗姆尼的助手们在克里斯·克里斯蒂身上卸下重担时，奥巴马在摇摆州（尤其是俄亥俄州）继续保持领先地位。总统候选人在最后一个竞选周末闪电战摇摆州|霍华德·库尔茨| 2012年11月4日|每日野兽
我有一种感觉，佩林真的很想在米特身上卸下担子，为纽特竞选。佩林与纽特|霍华德·库尔茨| 2012年2月7日|每日野兽
这就是为什么看到前犹他州州长乔恩·亨茨曼（Jon Huntsman）因其对边缘球队的吸引力而被对手淘汰，令人耳目一新。共和党人|马克·麦金农| 2011年8月22日|每日野兽
那天早上，我们有三次不得不卸下骡子，把包裹从障碍物上拿过去，然后再往另一边装货。人类的摇篮| W.A.维格拉姆
爱丽丝回到她的新同伴身边，爱德华和巴勃罗继续卸货。新森林的孩子们|马里亚特船长
这是我们第一次体验la porteuse，我们等着她弯腰驼背卸货。加勒比花园，v。1/2 |艾达·梅·希尔·斯塔尔
商人们开始卸货，以便出售或交换货物。《一千零一夜》——不为人知
卸下货物，把所有东西都放在安全的地方，这不是很愉快吗！南部联盟女孩日记|莎拉·玛根·道森