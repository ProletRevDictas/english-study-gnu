大多数人相信“玩得开心，时间过得真快”这句成语。事实上，研究表明，当时间似乎过得很快时，人们会认为这项任务一定很有趣。为什么假期还没开始就感觉结束了| LGBTQ编辑| 2021年7月12日|没有直接的消息
该广告宣传该品牌的萨尔茨柠檬水，用“当生活给你柠檬”的成语，将2020年称为“一年柠檬”。8个最佳超级碗广告，从《剪刀手爱德华》续集到迈克尔·B·乔丹的《亚历山大·索尼娅·拉奥，毛拉·朱基斯》2021年2月8日《华盛顿邮报》
首先，记住习语或俗语可能在一个地方有意义，但在另一个地方却没有意义，即使说的是同一种语言。拓展业务的六大国际搜索引擎优化秘诀|爱德华·科拉姆·詹姆斯| 2020年6月3日|搜索引擎观察
后来，她注意到，在这个成语中最熟练的人之一是记者多萝西·帕克。Tallulah Bankhead:在一个过度艺术的时代里，同性恋、醉酒和解放| Judith Mackrell | 2014年1月25日|每日野兽
用标准的成语“天生的政府”来说，是不是有些工作该杀谁？在埃里克·普林斯的回忆录《布赖恩·卡斯纳》2013年11月22日《每日野兽》中寻找答案
“成语”足以为现代读者辩护吗？本周热门读物：2013年10月15日|尼古拉斯·曼库西、托马斯·弗林| 2013年10月15日|每日野兽
此外，令人印象深刻的是，澳大利亚人能够用一个与她自己国家如此不同的国家的成语如此令人信服地写作。本周热门读物：2013年9月9日|尼古拉斯·曼库西| 2013年9月9日|每日野兽
然而，他似乎只对用自己的习惯用语重铸共和党的概念感兴趣。奥巴马的演讲借鉴了共和党的观点和麦迪逊大道（Lee Siegel）2012年1月28日《每日野兽》（DAILY BEAST）的言论
他的音乐习惯越来越丰富，音乐对他来说就像在沃廷斯克的诗歌一样。生活与健康；彼得·伊里奇·柴可夫斯基的信
兰格认为这些路线是腐败的；但我相信这个成语是正确的。乔叟作品，第1卷（共7卷）——玫瑰的浪漫；小诗|杰弗里·乔叟
因为英国的热情好客很难被翻译成任何其他的成语。麦克卢尔杂志，第1卷，第2期，1893年7月|各种
偶尔使用“不完美”几乎是他唯一的盖尔语成语。钓鱼素描|安德鲁·朗
口音、成语、词汇给古代语言带来了新的变化。美国素描|查尔斯·惠布里