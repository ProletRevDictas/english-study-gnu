在选举前回家而不帮助家庭和小企业是不合情理的，我认为人们深刻认识到，现在，在这一点上，不做点什么就不可能回家。特朗普在经济援助谈判中向佩洛西靠拢，众议院议长必须决定下一步行动|雷切尔·巴德、埃里卡·沃纳| 2020年9月17日|华盛顿邮报|
在众议院流传的另一个想法是通过个人法案，如新企业贷款或失业救助，只是为了向美国人表明或提醒众议院民主党人正在试图提供帮助。为什么众议院民主党有充分的理由担心没有冠状病毒救济协议| Amber Phillips | 2020年9月17日|华盛顿邮报
有争议的全部限制现在已经放松，但对商业和集会的各种限制仍然有效。随着疫情的蔓延，法院可能会重新考虑暂时限制冠状病毒的措施。| Anne Gearan，Karin Brulliard | 2020年9月16日|华盛顿邮报
在冠状病毒大流行导致企业倒闭和失业率飙升之前，总统肯定会吹嘘他担任总统的头三年的经济状况。特朗普的ABC新闻市政厅：四只皮诺奇，一遍又一遍|格伦·凯斯勒| 2020年9月16日|华盛顿邮报
该计划呼吁加快获得重建资金，免除对市政当局的救灾贷款，并支持当地企业。拜登访问佛罗里达州，民主党人担心他在该州的地位。|肖恩·沙利文| 2020年9月15日|华盛顿邮报|
我有点困惑，他对我关于他的商业活动的问题做出了多么友好的回答。我试着提醒你关于2003年的下流亿万富翁杰弗里·爱泼斯坦|维姬·沃德| 2015年1月7日|每日野兽|
这名记者在周二早上敲响了威尔金斯家的门，但既没有得到回答，也没有收到一支猎枪的商业目的。7岁的飞机失事幸存者穿越森林的残酷旅程|詹姆斯·希顿| 2015年1月7日|每日野兽
“竞争当然存在，但我认为只要有需求，每个人都有足够的生意，”他说。地中海鬼船|芭比拉萨·纳多| 2015年1月6日|每日野兽
去年3月，他们向空中客车公司提供了一笔巨大的新业务，订购了169架A320和65架稍大的A321。恼人的机场延误可能会妨碍您成为下一个亚航8501 |克莱夫·欧文| 2015年1月6日|每日野兽
他们一起徒步跨过国际桥梁进入华雷斯开展业务。一名告密者、一名失踪的美国人和华雷斯的死亡之家：在大卫·卡斯特罗12年的冷酷案件中|比尔·康罗伊| 2015年1月6日|每日野兽|
他想起了一件事，那就是作为一个深陷商业的人所珍爱的姿态。圣马丁之夏|拉斐尔·萨巴蒂尼
法哈多给国王的一封信（1621年12月10日）涉及各种行政和商业事务。菲律宾群岛，1493-1898年，第二十卷，1621-1624年
斯泰西·马克斯先生的父亲命中注定让他从事马车制造业。潘趣，或《伦敦查里瓦里》，第107卷，1894年11月3日
一场由杀戮引发的射程战，以及某种商业交易几乎使他们破产。原金|伯特兰·W·辛克莱
但是他无法忍受这种想法，带着极度的不耐烦，他匆匆忙忙地完成了上午的工作。《牧师的火面》第3卷，共4卷|简·波特