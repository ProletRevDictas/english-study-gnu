在一场致命的大流行期间，餐馆老板不应该做出这样的选择。难道政府就要眼睁睁地看着餐饮业消亡吗|Elazar Sontag | 2020年8月28日|食客
它提到了2007年致命的大米火灾，火灾起因是一棵梧桐树的树枝击中了费尔布鲁克附近的电线。监督机构警告：SDG&E的树木修剪计划可能加剧野火| MacKenzie Elmer | 2020年8月24日|圣地亚哥之声
至少有一项现实世界的实地研究支持这种观点，即可传播疫苗在根除野生动物致命疾病方面既安全又有效。野生动物疫苗能预防人类大流行吗|Rodrigo Pérez Ortega | 2020年8月24日| Quanta杂志
这种粉末无味无味，可以混合成肉块，散落在整个风景区，作为野狗抢夺的致命诱饵。用毒药扑杀野狗可能会让它们变大|杰克·布勒| 2020年8月19日|科学新闻
在2016年的一项研究中，Pedersen及其同事在《公共科学图书馆·病原体》杂志上报道说，八只猫中有六只在接受药物治疗后从致命的猫冠状病毒感染中康复。两种针对猫的冠状病毒药物如何帮助人类对抗新冠病毒-19 | Erin Garcia de Jesus | 2020年8月11日|科学新闻
这位喜剧演员对法国一家讽刺杂志的致命攻击作出回应，重申了他最近对伊斯兰信仰的批评。比尔·梅尔：数亿穆斯林支持对“查理·赫布多”的袭击；劳埃德·格罗夫2015年1月8日《每日野兽》
今天上午，巴黎发生了第二天的枪声，致命的暴力袭击震惊了法国首都。法国哀悼和狩猎| Nico Hines，Christopher Dickey | 2015年1月8日|每日野兽
拂晓后不久，又爆发了致命的暴力。法国哀悼和狩猎| Nico Hines，Christopher Dickey | 2015年1月8日|每日野兽
无论原因是什么，无论他们的信仰看起来多么荒谬，美国福音派教徒都是极其严肃的。福音启示录全是你的错|杰伊·迈克尔森| 2015年1月4日|每日野兽
他们现在声称，阿曼达实际上挥舞着刀子，受到了致命的打击，她的刑期增加到28年半。阿曼达·诺克斯：母亲的痴迷|尼娜·达恩顿| 2014年11月26日|每日野兽
蛇有一种魅力，还有一种比蛇更致命的魅力，谁没有感觉到呢？布莱克伍德的爱丁堡杂志，第60卷，第372期，1846年10月
这一次，骑兵们并没有单独迎接他们，但大炮在他们的脸上喷出了致命的炮弹。奥扎克一家的信使拜伦·A·邓恩
我希望维琪看穿她；她对杰克有如此大的影响力，以及如此致命的嘲笑力量。祖先|格特鲁德·阿瑟顿
几周延长成几个月，假期来临；但就在假期前，害群之马陷入了致命的罪恶之中。吉卜林的故事和诗每个孩子都应该知道，第二册|鲁迪亚德·吉卜林
节约是一种基本美德，浪费是一种致命的罪恶。英格兰、苏格兰和爱尔兰50年的铁路生活|约瑟夫·塔洛