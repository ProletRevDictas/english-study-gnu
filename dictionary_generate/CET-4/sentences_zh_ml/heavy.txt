在更重、更强大的机器上，好的轮胎更为重要，因为它们可以让您获得更多的亚视动力。让您的亚视更加坚固的六种方法|泰勒·弗里尔/户外生活| 2020年9月17日|科普
请注意，这是一件重的物品，重约47磅。为您的家庭办公室准备好的文件柜| PopSci商务团队| 2020年9月17日|科普
光滑的装订和圆角使本笔记本在大量使用后保持坚固。著名的写作和绘画笔记本| PopSci商业团队| 2020年9月17日|科普
科技股总是很有分量，但当投资者出价上涨时，这种分量会显著增加。科技股在三个方面与2000年泡沫相似，但在一个方面与之不同| Anne Sraders | 2020年9月16日|财富
她支持全民医疗保险和绿色新政，这意味着一个郊区密集、倾向于民主党的州可以在华盛顿有更多的自由派代表。特朗普在市政厅表示，他不会在流感大流行问题上做任何不同的事情。|科尔比·伊特科维茨、乔什·道西、费利西亚·桑梅兹、约翰·瓦格纳| 2020年9月16日|华盛顿邮报
关于痴迷足球的丈夫和沮丧的妻子的陈词滥调是相当严厉的《快速阅读：8个最淘气的比特》| Emily Shire | 2015年1月7日|每日野兽报|
“有大量的安全人员在场，但一切都没有改变，”哈维尔神父表示同意。墨西哥牧师被判谋杀罪|杰森·麦加汉| 2015年1月7日|每日野兽
奔跑的机器发出沉重的脚步声。如何度过新的一年“Gympocalypse”| Tim Teeman | 2015年1月6日| DAILY BEAST
一天晚上，我和一位朋友看了一眼屏幕，屏幕上闪现着“JSwipe当前负载很重”。我的犹太火药周|艾米丽郡| 2015年1月5日|每日野兽
在那之前，我只是一只笨重的狗，一队人。李·马文的自由帷幔微笑背后的故事|罗伯特·沃德| 2015年1月3日|每日野兽
警察们看上去呆滞沉重，好像再也不会有人犯罪了，好像他们已经知道了这一点。贝拉·唐娜|罗伯特·希金斯
嗡嗡声：袋式烟斗中最大的一根管子，发出沉闷沉重的音调。《格列佛游记》|乔纳森·斯威夫特
亨特·韦斯顿尽管损失惨重，但他明天将继续前进，这将转移你的压力。加里波利日记，第一卷|伊恩·汉密尔顿
整个下午，激烈的枪战仍在继续，叛军损失惨重，而西班牙人则损失了一名士兵。菲律宾群岛|约翰·福尔曼
他脸上的强烈怨恨已经消失了，但却有一种巨大的责备——一种沉重的、无助的、吸引人的责备。自信|亨利·詹姆斯