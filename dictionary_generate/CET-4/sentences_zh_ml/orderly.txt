这座城市并不惊慌，它只是以一种有序的方式展开。自动驾驶汽车是一个危险的话题——第92期：前沿|安东尼·汤森| 2020年10月21日|鹦鹉螺
昨晚的辩论表明，应在其余辩论的形式上增加额外的结构，以确保对这些问题进行更有序的讨论。总统辩论破裂|妮可·古德金德| 2020年10月15日|财富
他在第一次总统辩论中的好战表现可能并没有让希望制定更有序政策的商界人士感到宽慰。2020年大选的商业需求|杰弗里·科尔文| 2020年9月30日|财富
因此，按字母顺序排列的书架更有序，熵更小。变形挤压冷却器| Marcus Woo | 2020年8月24日| Quanta杂志
说明这一点的另一种方式是，xy平面“参数化”或有序地收集数线上的所有点对。新几何透视法破解了关于矩形的老问题| Kevin Hartnett | 2020年6月25日| Quanta杂志
抗议者在很大程度上进行了合作，使游行有序和平进行。埃里克·加纳抗议：“就像越南一样”|艾比·哈格拉格、凯特琳·迪克森、雅各布·西格尔、克里斯·奥尔布里顿| 2014年12月5日|每日野兽报
人们从墨西哥来，大多是20多岁的年轻人，或多或少都是井然有序的。小心你的愿望：以下是没有非法移民的加州的样子|小鲁本·纳瓦雷特| 2014年9月18日|每日野兽
是的，我认为一个有序和有节制的移民政策是合适的。是时候把自由女神送回法国了吗|Gene Robinson | 2014年7月20日|每日野兽
按照全球标准，这是一个相当平静、安静、有序的中等规模场所，在大多数日子里都运转良好。大城市生来肮脏|威尔·多伊格| 2014年7月13日|每日野兽
延误将导致检查站交通混乱和混乱，阻碍有序的时间表，并使脾气急躁。伊拉克叛乱分子散布谎言说他们在萨达姆的审判中杀害了法官|迈克尔·牛顿| 2014年6月28日|每日野兽报|
在我看来，这种有序守法自我的建立似乎暗示着，存在着促使秩序的冲动。儿童的方式|詹姆斯·萨利
不仅要求他们以适当有序的方式做事，而且人们必须以应有的尊重对待他们。儿童的方式|詹姆斯·萨利
我们必须记住，他的日常生活，家里井然有序，有助于让他记住形式的规律性。儿童的方式|詹姆斯·萨利
多布森张大了嘴，敲了敲桌子上的小铃铛，勤务兵从外面的房间走了进来。原金|伯特兰·W·辛克莱
经过一段不确定的等待时间后，勤务兵叫作“戈登·麦克雷”，宗教裁判所开始了。原金|伯特兰·W·辛克莱