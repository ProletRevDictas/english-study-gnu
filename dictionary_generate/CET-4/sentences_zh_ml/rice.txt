在泰国北部的一道菜考干津（khao kan jin）中，猪血与米饭混合，在香蕉叶中蒸制。血液在全世界都是受人尊敬的成分，但在美国就不那么受人尊敬了。一本新书旨在改变这种状况|Mayukh Sen | 2021年2月26日|华盛顿邮报
赖斯的病例数仍然很低，没有一天报告的病例超过6例。5所大学如何在校园内应对新冠病毒-19 | Betsy Ladyzhets | 2021年2月23日|科学新闻
同一地区的Meadow Grill提供种类繁多的商品，包括早餐玉米饼和饭碗。约塞米蒂国家公园终极旅游指南|肖恩特·萨拉伯特| 2021年2月22日|户外在线
1977年，当他开始向家乡尼日利亚的卡诺州进口大米和糖，并以利润丰厚的价格出售时，他的商业帝国已经跨越行业和国界扩张。企业家精神的新面貌|约书亚·埃弗里希| 2021年2月21日|奥齐
贾马尔·乌森（Jahmal Usen）和他自制的红色乔洛夫米饭和令人安慰的意大利面炒菜是其中的重要组成部分。2021年1月22日喂养一只新的尼日利亚食客
这本书详细描述了他与新保守主义者的对抗，以及他与康多莉扎·赖斯的联盟。2014年最佳回忆录|威廉·奥康纳| 2014年12月9日|每日野兽
赖斯在刑事案件期间在《发现》杂志上收到了这段视频，但与第一段视频一样，这段视频并没有公开播放。价值4400万美元的美国橄榄球联盟特氟龙堂|迈克·巴尼科尔| 2014年11月30日|每日野兽
在那次会议上，赖斯告诉专员，他在电梯里撞了赖斯夫人。价值4400万美元的美国橄榄球联盟特氟龙堂|迈克·巴尼科尔| 2014年11月30日|每日野兽
事先打电话给警方，称赖斯在秋千上是“一个拿着手枪的家伙”，但称这“可能是假的”。自迈克尔·布朗|尼娜·斯特罗奇利奇| 2014年11月25日|每日野兽报|以来被警察杀害的14名青少年
而写在山羊皮而不是宣纸上的宪法，可能就像我们的国家在杰斐逊统治时期一样。为什么我们不能停止称总统为“国王”|凯文·布莱耶| 2014年11月22日|每日野兽
在他最年轻的时候，当他母亲经常给他喂食时，她会给他塞满米饭。我们的韩国小表弟| H.李M.派克
同样地，陛下也将为他的船只提供庇护所，并在那个盛产肉类和大米的国家建立立足点。菲律宾群岛，1493-1898年，第二十卷，1621-1624年
这种大米的礼物对旅行者来说尤其令人高兴，因为在韩国，没有一道菜能获得更高的荣誉。我们的韩国小表弟| H.李M.派克
在这之后，他醒了过来，神清气爽，从当地酋长那里得到了一些米饭，吃得津津有味。猎狮| R.M.巴兰坦
种植园的主人赖斯先生是一位热情的南方同情者，但他并不喜欢现在的公司。奥扎克一家的信使拜伦·A·邓恩