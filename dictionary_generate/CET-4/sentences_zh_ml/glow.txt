把它们挂在你的窗户、庭院、花园或床架上，你会享受到温暖、温馨的阳光。闪烁的灯光瞬间让你的家充满欢乐| PopSci商业团队| 2020年8月27日|科普
当来自太阳的带电粒子被木星的磁场俘获，然后以紫外线的形式倾泻到大气中时，就会产生这种辉光。这些图像揭示了太阳系的黑暗面——第89期：黑暗面| Corey S.Powell | 2020年8月26日| Nautilus
在家里，你准备睡觉，仍然沐浴在美丽夜晚的光辉中。第一起谋杀案|凯蒂·麦克莱恩| 2020年8月19日|麻省理工技术评论
他是一个团队的成员，该团队从气泡中发现了一种新的可见光。发现：可见光下的银河系巨大气泡|艾米莉·科诺弗| 2020年7月16日|学生科学新闻
当他们对来自灯丝的数据应用去模糊算法时，他们立刻看到了同步辐射的辉光。隐藏的磁性宇宙开始出现| Natalie Wolchover | 2020年7月2日| Quanta杂志
发光：里克·詹姆斯里克·詹姆斯·大卫·里兹的自传（中庭图书）从哪里开始？2014年最佳回忆录|威廉·奥康纳| 2014年12月9日|每日野兽
当然，在她的梦幻庄园里，他们会把你的牙齿漂白得很白，让你容光焕发，麦当娜还会教你令人信服的英国口音彼得·潘还活着评论：再多的掌声也无法让它复活|凯文·法伦| 2014年12月5日|每日野兽
但愿它以家庭之爱的温暖光芒开始和结束。如何活着度过感恩节| Lizzie Crocker | 2014年11月26日|每日野兽
“他说，‘照镜子，看看你脸上的光芒，’”据《每日新闻》报道，艾莉森回忆起科斯比说过的话。比尔·科斯比（Bill Cosby）的一长串原告名单（到目前为止）：1965-2004年间18名据称的性侵犯受害者|马洛·斯特恩| 2014年11月24日|每日野兽
为了让鬼魂发光，我们不得不进行所谓的双重燃烧。加菲尔德电视台：《拯救黄金时间卡通片的猫》《里奇·戈尔茨坦》《2014年11月5日》《每日野兽》
烧焦的横梁和发黑的墙壁在大量残骸的阴燃中显得呆滞憔悴。红色的一年|路易斯·特蕾西
于是夜幕降临，黑暗笼罩着他，只有壁炉上燃烧着的木头发出的红光让他松了一口气。圣马丁之夏|拉斐尔·萨巴蒂尼
当一小片闪光划破黑暗时，我们身后的一个声音尖锐地喊道：“举起手来！”原金|伯特兰·W·辛克莱
他看上去非常苗条、强壮、自信，两颊泛着青春的光芒，灰色的眼睛里闪烁着幸福的火花。邦德男孩|乔治·W.（乔治·华盛顿）奥格登
红光是如此的灿烂，以至于整条河都被它染成了一片亮光。《耶稣会关系与同盟文献》，第二卷：阿卡迪亚，1612-1614