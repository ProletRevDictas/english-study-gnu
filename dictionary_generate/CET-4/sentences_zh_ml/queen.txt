“他们对待我们就像对待国王和王后一样，”一位参加set访问的成员说。反对关注金球奖的案例| Alissa Wilkinson | 2021年2月26日| Vox
这些白色皱纹的小啮齿动物，原产于东非，生活在地下殖民地，在女王的管理下，有着严格的角色和复杂的社会等级制度。和人类一样，裸鼹鼠也有地方口音——拉胡尔·拉奥（Rahul Rao）——2021年2月25日——科普
一旦一个新的女王出现，殖民地的声音再次开始匹配。独特的方言有助于裸鼹鼠分辨敌人和朋友|乔纳森·兰伯特| 2021年2月24日|学生科学新闻
唉，女王从来就不喜欢工作皇室的“一只脚进一只脚出”的概念。哈里王子和梅根失去了他们的赞助人，不会以“王室工作人员”的身份回来。|威廉·布斯，卡拉·亚当| 2021年2月19日|华盛顿邮报|
最近的一场比赛是网球女王塞丽娜·威廉姆斯与她的继承人大阪直美的较量。瑟琳娜·威廉姆斯（Serena Williams）和内奥米·大阪（Naomi Osaka）有着短暂但难忘的历史。|辛迪·博伦| 2021年2月17日|华盛顿邮报
这位在位的嘻哈天后在十几岁的时候就踏上了舞台。Nicki Minaj：高中女演员| Alex Chancey，《每日野兽》视频| 2014年12月30日|每日野兽
上一次如此大规模的袭击是在2001年，当时52名男子在尼罗河上的一家浮动迪斯科舞厅皇后船上被捕。Sisi正在迫害、起诉并公开羞辱埃及同性恋者| Bel Trew | 2014年12月30日| DAILY BEAST
当它们入侵新领地时，种群数量很低，而且女王的交配选择也很有限。猫鼬、猫鼬和蚂蚁，天哪！为什么有些动物在家里一直交配？|海伦·汤普森| 2014年12月29日|每日野兽
2008年，即使是女王也认为应该在白金汉宫授予他大英帝国勋章。有史以来最伟大的摇滚歌手是乔·科克（Joe Cocker）|泰德·乔亚（Ted Gioia）| 2014年12月23日|每日野兽（DAILY BEAST）
然而，一位皇宫内部人士今天向《每日野兽报》坚称，女王不会退位。女王会在圣诞节退位吗|汤姆·赛克斯| 2014年12月17日|每日野兽
在我们文学的道路上，我们遇到了许多与这位“植物女王”有关的东西；它的历史、品种、文化、制造和商业——E.R.比林斯。
我的脑海里浮现出一种幻想，我会用这种乐器演奏一首英国曲子来款待国王和王后。《格列佛游记》|乔纳森·斯威夫特
Q是一位女王，她穿着一条丝绸的便条；他是个强盗，想要一根鞭子。男孩和女孩书架；《性格塑造实用计划》，第一卷（共17卷）|
她的幽默之一是把牧师的儿子和守寡的圣日耳曼女王的侄女结合起来。《牧师的火面》第3卷，共4卷|简·波特
当夜幕降临大地时，伊丽莎白女王似乎觉得是时候发泄她的愤怒了。加里波利日记，第一卷|伊恩·汉密尔顿