通过直言不讳并采取行动，首席执行官们可以让当选官员知道，在民主权利受到侵犯时，商界不会袖手旁观。为什么首席执行官必须在民主和选举诚信方面采取行动，以及他们如何做到这一点|马修海默| 2020年8月27日|财富
以色列人权倡导者说，香港反政府武装的出口应该在2019年合法停止，那时反民主镇压急剧增加。以色列电话窃听公司面临法庭对香港销售的斗争帕特里克·奥尼尔2020年8月25日麻省理工学院技术评论
它当然很受欢迎，至少是“民主的”。宜家承诺“民主的”设计。它与维吉尔·阿布洛的合作成功了吗|克莱钱德勒| 2020年8月25日|财富
这次关闭一直持续到1月份，这是民主世界有史以来最长的一次互联网断电。印度如何成为互联网关闭的世界领导者|凯蒂·麦克莱恩| 2020年8月19日|麻省理工技术评论
无论是从赛马的角度，还是从更广泛的民主规范和价值观的角度，都值得思考这些类别。特朗普和共和党官员破坏选举进程的五种方式——小佩里·培根（Perry Bacon Jr.）。bacon@fivethirtyeight.com)2020年8月11日第五届第三十八届
显然，所有自由民主政府的首要义务是实施法治。阿亚安·赫西·阿里：我们的职责是让查理·赫多活着|阿亚安·赫西·阿里| 2015年1月8日|每日野兽
甚至可以说更民主的众议院也只有10%的黑人议员。国会难以忍受的白色|迪安·奥贝达拉| 2015年1月8日|每日野兽
到2012年，民主党总统巴拉克·奥巴马拥有亚裔美国人的选票，以47个百分点的优势获胜。亚裔美国人是新的佛罗里达州| Tim Mak | 2015年1月8日|每日野兽
无论是共和党还是民主党，都没有采取任何行动来始终如一地针对亚裔美国选民。亚裔美国人是新的佛罗里达州| Tim Mak | 2015年1月8日|每日野兽
但共和党和民主党已经努力扭转这一趋势。亚裔美国人是新的佛罗里达州| Tim Mak | 2015年1月8日|每日野兽
在代议制民主政府的崛起中，它在政治方面也有其对应者。社会正义的未解之谜|斯蒂芬·利科克
他这样做是为了表明他对皇室的不满，以及他对民主平等的偏好。同化记忆| Marcus Dwight Larrowe（又名A.Loisette教授）
第一步确实是民主的一步，引起了极大的兴奋。英格兰、苏格兰和爱尔兰50年的铁路生活|约瑟夫·塔洛
威尼斯革命，以及在法国将军安杰鲁的领导下成立的民主政府。《每日历史与年表》乔尔·蒙塞尔
记住，在当今世界，选举民主控制只覆盖了这一领域的一部分。社会正义的未解之谜|斯蒂芬·利科克