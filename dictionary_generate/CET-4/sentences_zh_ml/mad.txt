当他过去给警方打电话时，他们没有回应，或回应“疯狂迟到”。纽约警察局减速的原点| Batya Ungar Sargon | 2015年1月1日| DAILY BEAST
从正义的愤怒到虚假的愤怒，2014年我们为之疯狂的一切，以及愤怒如何夺走了我们的生命。《每日野兽》最佳长读物，2014年12月15-21日|威廉·布特| 2014年12月21日|每日野兽
房子在阿米莉亚和塞缪尔周围腐朽，他们的世界变窄了，变得疯狂，无法与人相处。悲伤：Babadook中的真正怪物| Tim Teeman | 2014年12月19日|每日野兽
我们为他们的所有权和控制权而战，就好像现实是一种资源，就像《疯狂的马克斯》中的水和油一样稀缺。查克·约翰逊和桑德海姆《詹姆斯·普洛斯》2014年12月13日《每日野兽》
今年的震撼：没有艾米·波勒，没有《广告狂人》，还有对处女和变性人的爱。15愤怒的金球奖电视冷落和惊喜：艾米·波勒，《广告狂人》及更多|凯文·法伦| 2014年12月11日|每日野兽
然后她赢了，欣喜若狂，兴奋不已，但喜悦并没有持续多久。寻找父亲的迷迭香| C.N.威廉姆森
人们会看到，被活动的能量弄得发疯的人类，正在追逐贪得无厌的逃离幻影。社会正义的未解之谜|斯蒂芬·利科克
艾琳已经三次去火车上接你了，这次她肯定是疯了。宅基地主人奥斯卡·米切奥
你疯狂的职业生涯通常在人群和五彩纸屑的自由搏斗中结束。阿里斯蒂德·普约尔|威廉·J·洛克的欢乐冒险
有些人会冷静地面对疯牛，对蟑螂或蜈蚣感到厌恶。猎狮| R.M.巴兰坦