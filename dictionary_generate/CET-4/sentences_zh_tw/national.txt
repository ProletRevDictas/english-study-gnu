每年的首演之夜，在他的指導下，管絃樂隊都會在歌劇開始前演奏國歌。馬克·庫班的想法是正確的：是時候重新思考我們如何使用國歌了《約翰·范斯坦》《2021年2月12日》《華盛頓郵報》
馬克·庫班並不是第一個試圖停止演奏國歌的人。他還指出，觀看運動員在國歌中跪下尋找自己的聲音並引起人們對他們激情的關注是一件令人鼓舞的事情。賽前國歌——儘管充滿了激烈的矛盾——仍然有一些東西可以提供給《巴里·斯維盧加》《2021年2月11日》《華盛頓郵報》
72歲的首相菅直義喜首先告訴議會，他“不熟悉這些言論”，反對黨發出噓聲，然後說這些言論“不利於國家利益”，但聲稱森喜朗是否辭職不取決於他。據《華盛頓郵報》報道，日本奧運會負責人西蒙·丹耶（Simon Denyer，Julia Mio Inuma）於2021年2月11日辭職，他說女性說話太多
NCAA錦標賽以4月3日的四強和4月5日的全國冠軍賽結束。十大男籃將男子籃球錦標賽移至印第安納波利斯|艾米莉·賈姆巴沃| 2021年2月9日|華盛頓郵報
許多跟隨林堡進入國家銀團的主持人都沒有那麼熟練，也沒有那麼受歡迎。拉什·林堡病了。保守的談話廣播業也是如此|Paul Farhi | 2021年2月9日|華盛頓郵報
該計劃遭到公民自由組織的譴責，並受到全國校長協會的質疑。英國可能監視學齡前兒童尋找潛在的聖戰者尼科·海因斯2015年1月7日《每日野獸》
林肯和艾森豪威爾的肖像被從共和黨全國委員會的辦公室移走。Ed Brooke：參議院民權先驅和後種族主義美國的先知| John Avlon | 2015年1月4日|每日野獸
他能否在國家舞臺上做到這一點是一個懸而未決的問題。哈克比能把共和黨的有錢人兌換成現金嗎|勞埃德·格林| 2015年1月4日|每日野獸
在我國和全球歷史上，我們目前面臨著許多問題。2015年，讓我們嘗試更多的同情心| Gene Robinson | 2015年1月4日| DAILY BEAST
但最後一位以明確的宗教方式行使古代個人權威的國家人物是羅伯特·F·肯尼迪。沒有神，沒有警察，沒有主人|詹姆斯·普洛斯| 2015年1月1日|每日野獸
他長期居住在弗吉尼亞州，是《國家情報員》和其他報紙的通訊員。《每日曆史與年表》喬爾·蒙塞爾
他開始相信養老金和國家保險等東西。社會正義的未解之謎|斯蒂芬·利科克
我一定不要搞錯了，大錯特錯成了一個民族類型的特徵，全錯了；如果我給你做面具，那一定是我們的功勞。將死|約瑟夫·謝里丹·勒法努
路易·菲利普退位後，法國國民議會開幕。《每日曆史與年表》喬爾·蒙塞爾
每個州的法院在涉及國家性質的所有問題上也必須遵循聯邦法院的裁決。普特南為門外漢準備的簡易法律書|阿爾伯特·西德尼·博爾斯