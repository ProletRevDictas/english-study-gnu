西爾弗在12月表示，他希望球員們能夠代表國歌，但他不會懲罰抗議的球員。NBA的爭議周顯示出泡沫之外的生活是多麼艱難|本·戈利弗| 2021年2月12日|華盛頓郵報|
查普爾說，學生在校園內使用一次性塑料不會受到懲罰，但學校官員計劃提供足夠的替代品，使學生不必依賴塑料。喬治華盛頓大學承諾禁止一次性使用塑料|勞倫·隆普金| 2021年2月11日|華盛頓郵報
值得稱讚的是，當他因為我說“不”而在經濟上懲罰我時，他沒有。出遊名人性害蟲和哀悼拉里·弗林特……索塔|尤金·羅賓遜| 2021年2月11日|奧茲
那些視頻和其他事情發生在我們任何人加入球隊之前，感覺我們正在受到懲罰。《華盛頓郵報》2021年2月10日《貝絲·萊因哈德》，前啦啦隊隊長與華盛頓足球隊達成和解，因為該計劃的未來尚不明朗
每節練習課都是按分鐘計劃的，如果球員遲到，他們將受到懲罰。橄欖球聯盟獲勝教練之一馬蒂·肖特海默於2021年2月9日在《華盛頓郵報》去世，享年77歲
相反，奧巴馬政府似乎選擇在財政上懲罰朝鮮。美國間諜說，他們追蹤“索尼黑客”多年了。| Shane Harris | 2015年1月2日| DAILY BEAST
法律可以很容易地作為一種政治工具來懲罰任何對國家的不尊重。迪斯科毛拉褻瀆神明的爭吵凸顯了巴基斯坦的虛偽| Shaheen Pasha | 2014年12月21日| DAILY BEAST
朝鮮有沒有因為賽斯·羅根（Seth Rogen）的一部嘲弄金正恩的電影而入侵索尼以懲罰他們？索尼黑客：獨裁者的舉動|Kevin Bleyer | 2014年12月14日|每日野獸
當被問到，如果克里斯蒂這麼可怕，他為什麼要用他的存在來懲罰花園州的人民時，坦克雷多笑了。“阻止克里斯克里斯蒂”運動開始了。祝你好運|奧利維亞·努齊| 2014年11月8日|每日野獸
我們停止援助和懲罰平民是否正確？美國人道主義援助前往伊斯蘭國|傑米·德特默| 2014年10月20日|每日野獸
為了懲罰這個習慣，一個土耳其人被抓住，一根菸鬥穿過他的鼻子。菸葉它的歷史、品種、文化、製造和商業——E.R.比林斯。
雅威派以色列人去懲罰米甸人，“殺死了所有的男人。”上帝和我的鄰居羅伯特·布拉奇福德
在誓言和誓言的同時，人們擔心上帝會因為沒有實現而懲罰他。約翰·坎寧安立約條例
為了在這種反應持續的時候懲罰自己，他會去找她，看看她是自己造成懲罰的。波浪|阿爾傑農·布萊克伍德
哦，夫人，我告訴你，你這樣做只是浪費時間，而你懲罰我，騷擾自己卻毫無意義。聖馬丁之夏|拉斐爾·薩巴蒂尼