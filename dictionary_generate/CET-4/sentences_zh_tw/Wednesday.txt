週三，全國城市聯盟（National Urban League）、全國有色人種協進會（NAACP）和全國行動網絡（National Action Network）的領導人参加了一場虛擬新聞發佈會，敦促國會通過該法案。眾議員卡倫·巴斯（Karen Bass）在國會重新提出喬治·弗洛伊德（George Floyd）警務法案| Shani Parrish | 2021年2月26日| Essence.com
在週三的比賽中，只有勇士隊、爵士隊、開拓者隊和快船隊的5人單位比賽時間至少為100分鐘，這進一步證明了國王籃球隊的發展趨勢是正確的。德阿龍·福克斯不是全明星，但他已經取得了飛躍|詹姆斯·L·傑克遜| 2021年2月25日|第五屆38
官員週三表示，萬豪酒店背後的家族已向霍華德大學捐贈2000萬美元，用於創建一個酒店領導力中心。萬豪家族向霍華德大學酒店領導力中心捐贈2000萬美元|勞倫·隆普金| 2021年2月25日|華盛頓郵報
主管內森·弗萊徹（Nathan Fletcher）在該縣週三的新聞發佈會上說，這項努力應該會導致學校安全地重新開放，聖迭戈統一規定4月12日重新開放。晨報：綠色的藍色高速公路|聖地亞哥之聲| 2021年2月25日|聖地亞哥之聲
據《華盛頓郵報》數據顯示，截至週三，近4500萬美國人至少接受了一次雙劑量方案的注射由你決定：鼓勵冠狀病毒疫苗接種的廣告活動正在進行中|丹·戴蒙德| 2021年2月25日|華盛頓郵報
這位漫畫家，更出名的名字叫查布，在週三被槍殺。法國殺死查理·赫布多殺人犯|尼科·海因斯| 2015年1月9日|每日野獸
週三晚些時候，法國當局報告說，穆拉德已向警方自首，而兩兄弟仍逍遙法外。警方追捕巴黎大屠殺嫌疑人|特雷西·麥克尼科爾，克里斯托弗·迪基| 2015年1月7日|每日野獸
但到週三晚上，該地區幾乎沒有組織抗議或隨機騷亂。聖路易斯槍擊案是反弗格森的|賈斯汀·格拉維| 2014年12月25日|每日野獸
星期三下午能和哈姆先生在一起真是太難過了。書堆：出售大衣，保持尊嚴|保羅·亨菲爾| 2014年12月22日|每日野獸
週三，美國戰機瞄準了他在城市東側行駛的一輛汽車。伊拉克庫爾德人找回了他們的狀態，結束了對辛賈爾山的圍困|傑米·德特梅爾| 2014年12月20日|每日野獸
那是星期三晚上；40多人坐下來在混亂俱樂部共進晚餐。《Pit Town Coronet》，第一卷（共3卷）|查爾斯·詹姆斯·威爾斯
儘管這是本月的第二個星期四，她還是選擇了第一個星期三指定的部分。《Pit Town Coronet》，第一卷（共3卷）|查爾斯·詹姆斯·威爾斯
G先生和夫人——請L先生和夫人的陪伴在3月8日星期三——點鐘吃飯。女士禮儀手冊和禮貌手冊|弗洛倫斯·哈特利
霍華德夫婦向加勒特夫人致意，並愉快地接受她星期三的盛情邀請。女士禮儀手冊和禮貌手冊|弗洛倫斯·哈特利
看來星期三是她的生日，除了在古羅馬營地吃晚餐外，她什麼也吃不上。雛菊鏈|夏洛特·楊