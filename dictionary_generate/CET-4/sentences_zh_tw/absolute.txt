對於一個迫切需要入學的學區和學校來說，佐拉·威廉姆斯（Zora Williams）絕對是一份天賦，她渴望反駁野心勃勃的學生應該去北方或特許學校求學的說法。2020年9月16日，林肯高中的一門微積分課突然變成了陶瓷，聖迭戈之聲
有一個內置的“絕對初學者”程序，對於我們這些在過去六個月左右一直坐著不動的人來說，它可能會派上用場。蘋果剛剛發佈了新的iPad、iPad Air和蘋果手錶系列6 |斯坦·霍拉澤克| 2020年9月15日|科普
讓密爾沃基心神不寧的是首發控球后衛埃裡克·布萊索的射門能力——這是一個系列賽中的絕對殺手，而防守正是依靠這一點。雄鹿隊穩操勝券，創造了錯誤的歷史|克里斯·赫林（Chris Herring）。herring@fivethirtyeight.com)2020年9月9日第五屆第三十八屆
他說，亞馬遜“在過去一年中意外喚醒的次數減少了50%，”但沒有透露任何絕對數字。亞馬遜的人工智能語音項目得到Facebook、杜比和Garmin的幫助| jonathanvanian2015 | 2020年9月9日|財富
高盛（Goldman Sachs）表示，從絕對值來看，情況比這更糟。為什麼上週科技股的大幅拋售會讓投資者提防《財富》| Bernhard Warner | 2020年9月8日|
他們將以謙卑的毅力和絕對的信念面對壓迫。墨西哥牧師被判謀殺罪|傑森·麥加漢| 2015年1月7日|每日野獸
眾議院的規定要求投票選出一位發言人的議員必須獲得絕對多數。神風隊國會準備打擊博納|本·雅各布斯| 2015年1月6日|每日野獸
沒有屍體，沒有人能絕對肯定地說卡斯特羅是否死了，即使所有跡象都指向那個方向。一名告密者、一名失蹤的美國人和華雷斯的死亡之家：在大衛·卡斯特羅12年的冷酷案件中|比爾·康羅伊| 2015年1月6日|每日野獸|
而這首歌是絕對的天才和完全的普遍性。是的，我喜歡聖誕音樂。別笑了|Michael Tomasky | 2014年12月24日|每日野獸
你必須冒這個險，並且有看起來像個十足的傻瓜的危險。闖入百老匯的英國人|蒂姆·蒂曼| 2014年12月7日|每日野獸
你完全控制著其中的一個人。思想的珍珠|成熟M.巴盧
婚姻就像蛋黃醬，要麼是巨大的成功，要麼是徹底的失敗。《Pit Town Coronet》，第一卷（共3卷）|查爾斯·詹姆斯·威爾斯
他受到了噓聲和嘲笑，但他以絕對冷靜的態度重新組織了部隊並遏制了敵人。拿破崙元帥R.P.鄧恩·帕蒂森
很明顯，任何品種的絕對增加都可能伴隨著相對減少。臨床診斷手冊|詹姆斯·坎貝爾·託德
實際數量的增加是絕對增加；僅百分比增加，相對增加。臨床診斷手冊|詹姆斯·坎貝爾·託德