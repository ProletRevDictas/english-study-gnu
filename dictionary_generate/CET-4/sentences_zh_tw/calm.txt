由於本季我們中的許多人都是家庭成員，這裡有一些方法可以讓你在自己的空間裡保持冷靜、冷靜和沉著。8夏季必備的禦寒用品|喬伊·奈斯比特| 2020年7月4日|奧茲
我們正在加倍努力，保持海灘和當地企業的開放，讓人們感受到平靜的海水，但今年7月4日的週末看起來會有點不同。安全第一今年7月4日|李·安·威爾金森| 2020年7月4日|華盛頓
幾天後，他讓我劃過一些小急流，告訴我把船對準哪裡，給我冷靜的指示，讓我調整一下左右的軌跡。我花在漂流大峽谷上的那個令人大開眼界的月份|布倫丹·倫納德| 2020年6月30日|在網上之外
這是一種遊戲，但它主要是一種讓自己冷靜下來的方式，因為實際上沒有任何輸球或競爭的方式。本週末：用彩虹的顏色看你的世界|菲奧娜·祖布林| 2020年6月26日|奧茲
健康、積極——如果不是奇怪和盲目的話——的內容已經成為治癒我們焦慮的一種香膏，使它成為一種偉大的溝通方式，一種填補空白並提供床單面具和酸麵包無法提供的平靜感的自我照顧形式。混沌傳播：新冠病毒19正在改寫我們的文化聯繫規則|梅根·勞斯| 2020年6月11日|搜索引擎觀察
政府繼續呼籲人們保持冷靜，同時警告人們保持警惕。法國瘋狂恐怖襲擊浪潮《克里斯托弗·迪基》2014年12月24日《每日野獸》
警察局長安德魯·西皮奧內（Andrew Scipione）要求居民保持冷靜，因為擔心發生動亂。悉尼聖戰圍城以槍戰結束|考特尼·薩布拉曼尼安、倫諾克斯·薩繆爾斯、克里斯·奧爾布里頓| 2014年12月15日|每日野獸
這部戲劇震撼了通常平靜的悉尼，以其悠閒的氛圍和放鬆的人群而聞名。悉尼聖戰圍城以槍戰結束|考特尼·薩布拉曼尼安、倫諾克斯·薩繆爾斯、克里斯·奧爾布里頓| 2014年12月15日|每日野獸
他以一種我欣賞的冷靜、直率的態度，把這個話題提到希區柯克身上。阿爾弗雷德·希區柯克的《褪色為黑色：偉大導演的最後日子》|大衛·弗里曼| 2014年12月13日|每日野獸報
一位醫生來到他的家，給他注射可的松以緩解他膝蓋的關節炎疼痛。阿爾弗雷德·希區柯克的《褪色為黑色：偉大導演的最後日子》|大衛·弗里曼| 2014年12月13日|每日野獸報
我們認為，這種危險的平靜可能預示著暴風雨的來臨，我們不允許它使我們陷入安全之中。布萊克伍德的愛丁堡雜誌，第CCCXXXIX號。1844年1月。第LV卷|各種各樣的
他的面容表情平靜而沉著，他的眼睛帶著希望和懇求的神色仰望天空。布萊克伍德的愛丁堡雜誌，第CCCXXXIX號。1844年1月。第LV卷|各種各樣的
天氣很平靜，他繞著我劃了好幾圈，觀察著我的窗戶和保護窗戶的鐵絲網。《格列佛遊記》|喬納森·斯威夫特
她用平靜的手指重新整理著她的文件，而他自己的手指則幾乎被壓抑的慾望所束縛。波浪|阿爾傑農·布萊克伍德
風箏高飛，小船在平靜而閃閃發光的海面上疾馳而過。北方巨人| R.M.巴蘭坦