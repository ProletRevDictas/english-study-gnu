他總是把絲帶敲下來，我總是把它插進去。與吉姆·克萊默|尤金·羅賓遜| 2021年2月1日|奧茲一起殺人
展示防水布、標記絲帶或您的火焰橙色狩獵背心。《如何在野外生存三天》|基思·麥卡弗蒂/菲爾德與溪流| 2021年1月26日|科普
1986年，這家辦公超市在馬薩諸塞州的布萊頓開設了第一家店鋪。不到一年前，創始人湯姆·斯特姆伯格（Tom Stemberg）就有了這一想法。在7月4日的週末，湯姆·斯特姆伯格（Tom Stemberg）努力為自己的打字機找到一條替換色帶。最佳辦公椅：使用我們精選的辦公傢俱，舒適、高效| PopSci商務團隊| 2021年1月11日|科普
“在這裡將是徹底的破壞，”索倫指著緞帶和橋之間的樹林說。生物學家說，一座更寬的美國軍團橋將摧毀重要的研究地點《華盛頓郵報》，即2020年12月11日的Katherine Shaver
把織物做成一棵樹，配上絲帶，真的有助於形成對比。《華盛頓郵報》比爾特莫爾|弗吉尼亞布朗| 2020年12月10日|的專家稱，提升假日裝飾的七種方法
他是2012年“白絲帶革命”的活生生的象徵之一，總是穿著黑色，苗條，剃著鬍子，幾乎是個和尚。假期坐牢：我們希望2015年11名政治犯獲釋| Movements.Org | 2014年12月25日| DAILY BEAST
在不同的時候，我們談論過榮譽——希區柯克被授予榮譽區，並在翻領上戴著絲帶。阿爾弗雷德·希區柯克的《褪色為黑色：偉大導演的最後日子》|大衛·弗里曼| 2014年12月13日|每日野獸報
但這是一個真正的鑼，它帶有一條漂亮的絲帶和一封女王的信。2014年11月10日，克萊夫·歐文，《每日野獸》，我在桌上看到了核世界末日
但是世貿中心的綵帶來自9/11襲擊，在第一架飛機被擊中後，Baugh趕往現場。總統和拖車司機邁克爾·戴利2014年9月25日《每日野獸》
作為一名廚師，大學畢業後，我和其他隊員一起喝了巴布斯特藍絲帶。酒鬼，有啤酒給你| Jordan Salcito | 2014年4月5日| DAILY BEAST
騎士團的徽章是一條緞帶，上面有黑色、白色和黃色的條紋，這個裝置有點像冰柱。《每日曆史與年表》喬爾·蒙塞爾
亨利埃塔穿著一條幹淨的便條和她擁有的最漂亮的髮帶。羅斯草坪的篝火女孩|瑪格麗特·彭羅斯
對於年輕女士來說，在家裡，絲帶或天鵝絨是做頭飾最合適的材料。女士禮儀手冊和禮貌手冊|弗洛倫斯·哈特利
她要來了，我的孩子們，我的孩子們，就像湯米在擔任法國大使絲帶史達徹時所說的那樣。第一部戲劇| A.A.米爾恩
面紗滑落了，很容易被誤認為是一條緞帶，限制了頭部底部的隊列。祖先|格特魯德·阿瑟頓