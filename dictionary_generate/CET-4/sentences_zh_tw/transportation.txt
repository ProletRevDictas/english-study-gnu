投票站是一個會議中心，提供了多個投票地點，那裡的交通是免費的。對於選舉管理者來說，死亡威脅已經成為工作的一部分——傑西卡·胡斯曼（Jessica Huseman）於2020年8月21日（ProPublica）
我們親自接觸車手和司機，分享更多關於為什麼會發生這種情況，你能做些什麼，並提供一些交通選擇。Lyft計劃在午夜暫停加利福尼亞州的騎行活動，除非法院作出裁決《財富》雜誌2020年8月20日的裁決
我需要回家，但意識到公共交通已經關閉，所以我開始步行去叫出租車。明斯克的苦難|尤金·羅賓遜| 2020年8月18日|奧齊
然而，女議員芭芭拉·布萊（Barbara Bry）與SANDAG執行董事哈桑·伊克拉塔（Hasan Ikhrata）的建議保持了一定的距離，即讓其他交通方式與駕駛一樣具有競爭力。晨報：SDPD表示將停止煽動性語言門票|聖地亞哥之聲| 2020年8月17日|聖地亞哥之聲
當我想到對可持續發展、能源、交通、住房的進步觀點時，我們美國人會想到你們——也許不是阿姆斯特丹本身，但肯定是你們所在地區，比我們更像一個榜樣。經濟增長是錯誤的目標嗎？（第429頁）| Stephen J.Dubner | 2020年8月13日|畸形經濟學
從瀝青砂中提取石油成本很高，如果把運輸成本算在首位，成本更高。週日實況調查節目：1月4日| PunditFact.com | 2015年1月5日|每日野獸
但有了管道，運輸成本下降，產量也會更高。週日實況調查節目：1月4日| PunditFact.com | 2015年1月5日|每日野獸
交通服務和其他類似服務是共享經濟的縮影。其中一件禮物只有一個鄰居在外| Lawrence Ferber | 2014年12月8日| DAILY BEAST
當然，紅衣軍團在交通、補給和訓練方面佔了上風。英國皇室成員萊因維德·布魯克林：威廉和凱特來到歷史遺址觀看籃球比賽|賈斯汀·瓊斯| 2014年12月6日|每日野獸
到週二傍晚，共乘出租車的城際交通系統已經關閉。屠殺猶太教堂的激進分子| Creede Newton | 2014年11月19日| DAILY BEAST
鐵路軌道和橋樑被拆除；一些地區幾乎沒有交通設施。神聖遺產：弗吉尼亞的生活|多蘿西·M·託佩
製造業工人的第六大僱主是運輸設備行業。神聖遺產：弗吉尼亞的生活|多蘿西·M·託佩
這類工人中有一半是鐵路和水運工人。神聖遺產：弗吉尼亞的生活|多蘿西·M·託佩
任何地區的經濟活動在很大程度上取決於其交通設施。神聖遺產：弗吉尼亞的生活|多蘿西·M·託佩
他走到了交通大樓寬闊的地面上。令人震驚的故事，1931年5月|各種各樣