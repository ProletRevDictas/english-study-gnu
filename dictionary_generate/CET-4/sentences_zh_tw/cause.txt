一些積極分子致力於這兩項事業，而另一些人則認為他們的工作在意識形態上具有同情心。“廢除冰”如何幫助廢奴主義思想進入主流| Nicole Narea | 2020年7月9日| Vox
這個詞來自於這些球員給其他人帶來的悲傷。Minecraft的話：你的孩子在說什麼|Minrose Straussman | 2020年7月8日| Z之後的一切
她提出，“公平就是要找到你所看到的模式的根本原因，而不是停留在公司通過分析所能看到的”上，“而是，為什麼背景是這樣的。”公司現在可以用三種方法來解決種族主義這一重大公共健康問題|安妮·斯拉德| 2020年7月7日|財富雜誌
例如，嚴重擺動的車輪可能會損壞輪轂中的軸承，從而導致其不規則地旋轉。使用這些支持修復的技巧讓你的舊自行車恢復生機|斯坦·霍拉切克| 2020年7月7日|科普
瘧疾是一種由寄生蟲引起的傳染病，但它不會傳染，因為你不會僅僅通過與受感染者接觸或在周圍感染瘧疾。“傳染性”與“傳染性”：區別可能很重要|約翰·凱利| 2020年7月5日| Z之後的一切
但科斯比·特魯瑟斯（Cosby Truthers）將他們的原則應用於錯誤的事業。Phylicia Rashad和Cosby Truthers的崇拜| Stereo Williams | 2015年1月8日|每日野獸
所以我們知道，鑽入基岩並將其充滿流體會導致地震。26次地震之後，水力壓裂的冒煙槍在德克薩斯州|詹姆斯·喬納| 2015年1月7日|每日野獸報
現在的天氣不應該導致商業客機墜毀。惱人的機場延誤可能會妨礙您成為下一個亞航8501 |克萊夫·歐文| 2015年1月6日|每日野獸
如果Dudesmash是我們繼續做的事情，這將是重要的一年，因為我們去年沒有做過。Deer Tick的約翰·麥考利《搖滾十年》|詹姆斯·喬伊納| 2015年1月2日|每日野獸
米歇爾·奧巴馬（Michelle Obama）在推特上發佈了一個標籤，不知何故引起了憤怒。絕對必須在2015年消亡的政治模因| Asawin Suebsaeng | 2015年1月1日| DAILY BEAST
他們是否曾在不同時期為同一原因辯護或反對同一原因，並引用先例證明相反的觀點？《格列佛遊記》|喬納森·斯威夫特
路易斯和德帕蒂諾斯在沒有任何已知的冒犯原因的情況下，默許了彼此的厭惡。《牧師的火面》第3卷，共4卷|簡·波特
當他們在沒有任何抵抗手段的情況下將110艘船隻帶入這些海域時，我們有理由感到震驚。菲律賓群島，1493-1898年，第二十卷，1621-1624年
如果我們要按照表達方式進行真正的教育，我們必須從表達的“內容”或“原因”開始。富有表現力的聲音文化|傑西·埃爾德里奇·索斯威克
行動的迅速和在戰場上從未被打敗的自信是穆拉特成功的原因。拿破崙元帥R.P.鄧恩·帕蒂森