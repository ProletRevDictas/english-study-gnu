事實證明，北歐寒冷的氣候不適合甘蔗種植，因此這種植物在氣候較為溫和的南部和歐洲南部海岸附近的島嶼生根。一家初創公司最終找到了食品科學的聖盃之一——健康的糖替代品嗎|Jonathan Shieber | 2021年2月26日| TechCrunch
這種蚊子已經在該國南部建立起來，但這種疾病的爆發仍然很罕見，因為只有在夏季溫度非常高時才能傳播。寒冷的冬天意味著夏天蟲子更少嗎|菲利普·基弗| 2021年2月25日|科普
當阿澤瑪和她的部隊，包括十九名安然度過這一天事件的士兵回到他們在南部前線的陣地時，夜晚和涼爽已經到來。為保衛家園抵抗伊斯蘭國而戰的婦女|蓋爾·澤馬克·萊蒙| 2021年2月22日|時間
如果你想在約塞米蒂的南大門附近閒逛，巴斯湖距離瓦沃納僅半小時車程，提供了許多娛樂機會，包括極好的溫水釣魚。約塞米蒂國家公園終極旅遊指南|肖恩特·薩拉伯特| 2021年2月22日|戶外在線
在任何活動持續的地方都可能有一層冰和一層雪，儘管馬里蘭州南部可能有十分之一英寸的冰。PM更新：由於週期性的冬季混合光持續存在，夜間天氣很滑。| Ian Livingston | 2021年2月19日|華盛頓郵報
在阿富汗，有人試圖奪回南部的赫爾曼德省。五角大樓不知道在ISIS戰爭中有多少人喪生|南希A.優素福| 2015年1月7日|每日野獸|
雖然南方並不自動等同於新邦聯，但有時這種區別很容易消失。Steve Scalise展示了邦聯和南方之間的細微差別|勞埃德·格林| 2015年1月2日|每日野獸
然而，南部和南部聯盟之間的界線往往會變得模糊。Steve Scalise展示了邦聯和南方之間的細微差別|勞埃德·格林| 2015年1月2日|每日野獸
相反，民主黨最初是南方黨。Steve Scalise展示了邦聯和南方之間的細微差別|勞埃德·格林| 2015年1月2日|每日野獸
我們看到南方種族隔離主義者幾乎每天都在威脅他的生命和他的家人。金博士前往好萊塢：《塞爾瑪》有缺陷的歷史|加里·梅| 2015年1月2日|每日野獸報
看看那些遙遠的山峰如何安詳地聳立在南部地平線上！布萊克伍德的愛丁堡雜誌，第60卷，第372期，1846年10月
我母親現在告訴我，她知道這個錯誤，紐約報紙從一份南方雜誌上抄襲了這個項目。封閉的房子| Augusta Huiell Seaman
在整個田納西州南部，幾乎找不到一個比他們更知足、更懶散、更壞的家庭。拉蒙娜|海倫·亨特·傑克遜
安妮小姐和藹可親地笑了笑，沒有想到他的困惑，被他南方的溫暖逗樂了。阿里斯蒂德·普約爾|威廉·J·洛克的歡樂冒險
在過去的三年裡，我一直在朱迪思盆地，而南方服裝還沒有開始進入那裡。原金|伯特蘭·W·辛克萊