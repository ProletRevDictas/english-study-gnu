例如，Tide利用亞馬遜Alexa Skills平臺上的營銷，從採購流程中刪除了一個步驟。如何在流感大流行期間推動必要的數字創新| Nick Chasinov | 2020年9月16日|搜索引擎觀察
因此，谷歌正在採取一系列措施，使自己成為消費者在線或離線搜索產品的場所。谷歌通過新的購物功能提高“附近”產品庫存的可見性|格雷格·斯特林| 2020年9月16日|搜索引擎土地
然後，他們採取措施得到可靠來源的證實，而可靠來源確實證實了這一點。政治報道：偽造腳註15 |斯科特·劉易斯和安德魯·基阿特斯| 2020年9月12日|聖地亞哥之聲
這通常被視為國會的職權範圍，自3月和4月的第一批援助以來，國會一直沒有采取重大的新措施來幫助陷入困境的美國人。《大企業拯救和太小而無法拯救的美國》| Lydia DePillis、Justin Elliott和Paul Kiel | 2020年9月12日| ProPublica
另一方面，他們正在國內採取許多措施來支撐該行業一場真正的艱苦戰鬥：為什麼中國將奮力反擊美國對華為的攻擊|陳維塔| 2020年9月10日|財富|
但布魯克與新左派及其激進時尚的理念格格不入。Ed Brooke：參議院民權先驅和後種族主義美國的先知| John Avlon | 2015年1月4日|每日野獸
在21世紀，女性在生活的每一步都獲得了平等待遇……包括臥室。有事業心的女性轉向男性護衛，享受無條件的樂趣和（可能）性生活|極光雪| 2015年1月3日|每日野獸
一個循序漸進的計劃，打破你對各種技術的沉迷。自助書籍真的能讓你煥然一新嗎|Lizzie Crocker | 2014年12月29日|每日野獸
當她試圖進去時，火車已經開動了，她的身體被壓碎了。乘坐泰國二戰死亡鐵路|莉莎·福爾曼| 2014年12月21日|每日野獸
在那之前，我們將一起走樓梯，一步一步地走。你永遠無法“治癒”進食障礙|卡莉·阿諾德| 2014年12月20日|每日野獸
新娘當選人衝到他跟前，於是他們都走到腳燈前。歌劇生理學|約翰·H·斯瓦比（又名“斯克裡奇”）
但是，當汽車轟隆隆地落下時，它被擠到了臺階上；司機做了一個憂鬱的手勢，拒絕了她的信號。希爾達·萊斯韋斯|阿諾德·貝內特
索爾慢慢地站了起來；向院子裡退了一步；充滿了他的肺，張開了嘴，使他的眼睛轉了個圈。邦德男孩|喬治·W.（喬治·華盛頓）奧格登
她變得有點胖了，但這似乎絲毫沒有減損她每一步、每一個姿勢、每一個姿勢的優雅。《覺醒》與短篇小說選集《凱特·肖邦》
我的腳步聲將使你的心跳動；我一看，你就會啞口無言一個小時。將死|約瑟夫·謝里丹·勒法努