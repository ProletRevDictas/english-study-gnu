標誌著他祖國邊緣的手風琴鐵絲網每次他看到時都會觸發維利的邊境牢房開火，反過來，開火可能會引發與那個地方相關的絕望。邊界牆如何欺騙人類大腦和心理|傑西卡·瓦普納| 2020年10月2日|科普
在他的第五交響曲的第一樂章中，貝多芬在希望和絕望之間展開了一場鬥爭。貝多芬的第五交響曲是在逆境中尋找希望的一課|查理·哈丁| 2020年9月11日| Vox
我開始思考，我的工作是確保“氣候一代”的人們不會被絕望的海洋吞沒。Z一代受到氣候變化的“創傷”，他們是應對氣候變化的關鍵|馬修海默| 2020年8月19日|財富
當涉及到在線聲譽管理時，這種差異可能會產生一種絕望的感覺，但這隻意味著你需要更加主動地獲得客戶的評論。在線聲譽管理：成功的七個步驟| Aleh Barysevich | 2020年6月3日|搜索引擎觀察
美國在絕望和年久失修的狀態下進入2020年競選的最後幾個月。隨著明尼阿波利斯的燃燒，特朗普的總統任期正陷入更深的危機。然而，他仍有可能再次當選| LGBTQ編輯| 2020年6月2日|沒有直接新聞
紅衣主教說：“光明戰勝黑暗，希望戰勝絕望，恩典戰勝罪惡，愛戰勝仇恨，生命戰勝死亡。”請不要死拯救被謀殺警察的瘋狂戰鬥|邁克爾·戴利| 2014年12月22日|每日野獸
在密蘇里州郊區，當警官達倫·威爾遜被告知他可以自由行走時，三個月的絕望被點燃。憤怒的抗議者放火焚燒弗格森|賈斯汀·格拉維| 2014年11月25日|每日野獸報
在這一點上，可能只有憤怒和絕望。Ferguson抗議者騷擾黑人警察，呼籲Darren Wilson死亡| Justin Glawe | 2014年11月21日| DAILY BEAST
我高度懷疑，任何尚未處於絕望狀態的人都會將戰爭視為無神的解藥。狐狸洞裡只有無神論者|邁克爾·卡森| 2014年10月5日|每日野獸
未來的城市化不需要在農村絕望和城市絕望之間做出選擇。歡迎來到億萬人貧民窟| Joel Kotkin | 2014年8月25日|每日野獸
戴維極其困惑地讀了兩三遍，然後絕望地放棄了。戴維與妖精|查爾斯·E·卡里爾
普魯德洪在屈辱和絕望中，過著幾乎完全孤獨的生活。從公元前七世紀到公元二十世紀的女性美術作品|克拉拉·厄斯金·克萊門特
落下的露珠和呼嘯的風並沒有把他從孤獨絕望的床上扶起來。《牧師的火面》第3卷，共4卷|簡·波特
但是，34年的港口是如此的好，以至於他兩次撤銷了，令他不幸的兄弟和合夥人感到憤怒和絕望。《Pit Town Coronet》，第一卷（共3卷）|查爾斯·詹姆斯·威爾斯
當他們越來越低地下山時，她的痛苦和不安變得尖銳起來，達到了極度絕望的地步。希爾達·萊斯韋斯|阿諾德·貝內特