與此同時，全國範圍內行駛的英里數減少了15%。在冠狀病毒大流行期間，交通量下降，但道路死亡人數仍在增加。|盧茲·拉佐| 2021年2月12日|華盛頓郵報
快節奏播放列表讓我保持動力，每英里至少提高15秒的配速。5件裝備幫助我在冬季保持活力|雅各布·席勒| 2021年2月11日|戶外在線
她特別鼓勵那些住在一英里半學校建築裡的人考慮步行去上課。亞歷山大市公立學校確定重新開放的日期；阿靈頓拒絕效仿|漢娜·納坦森| 2021年2月5日|華盛頓郵報|
他跑了100英里，150英里，為他所支持的不同組織帶來關注。鮑里斯·科喬：教種族歷史|尤金·羅賓遜| 2021年2月2日|奧茲
以每小時65英里的速度，你可以在三秒內跑完100碼。為普通汽車添加行車燈的最佳方法| Wes Siler | 2021年1月26日|戶外在線
然後她設法在陰暗多雨的樹林中掙扎了一英里。7歲的飛機失事倖存者穿越森林的殘酷旅程|詹姆斯·希頓| 2015年1月7日|每日野獸
除了懸掛在街道上的巨大的美國國旗，一英里長的警察隊伍結束了。紐約警察局工會老闆邁克爾·戴利2015年1月5日《每日野獸》的葬禮抗議太多了
每一個似乎都有一英里高，整個飛行都是一個無法逾越的障礙。你永遠無法“治癒”進食障礙|卡莉·阿諾德| 2014年12月20日|每日野獸
所以我回家了，我們住在四分之一英里外，我騎上自行車回來，他在甜甜圈店裡。喬·拜登：《我要殺了你的兒子》|奧利維亞·努齊| 2014年12月12日|《每日野獸》
事件發生在距加納事件僅一英里的地方。在埃裡克·加納之前，有《邁克爾·斯圖爾特：現實生活電臺Raheem | Marlow Stern | 2014年12月4日|每日野獸》的悲劇故事
當你們吞下一英里又一英里令人眩暈的道路時，你們需要向右或向左看什麼？阿里斯蒂德·普約爾|威廉·J·洛克的歡樂冒險
聖安娜將軍帶著一千五百人在離我們不到一英里的地方。布萊克伍德的愛丁堡雜誌，第CCCXXXIX號。1844年1月。第LV卷|各種各樣的
沒有任何一個地區能比倫巴第更能支撐一平方英里的人口。歐洲概覽|霍勒斯·格里利
當布魯諾跑回來時，他們已經走了一英里，從他的舉止可以看出他有消息要告訴大家。奧扎克一家的信使拜倫·A·鄧恩
隊伍慢慢地向東面四分之三英里的河邊走去。紅色的一年|路易斯·特蕾西