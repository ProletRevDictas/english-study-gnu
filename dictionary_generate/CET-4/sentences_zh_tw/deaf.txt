如果很明顯，他們只是在那裡聽自己或少數特權人士發言，那麼他們對所代表的人民的關切充耳不聞也可能是真的。《財富》雜誌《政府與政策》中40歲以下40人的推薦書《雷切爾·金》2020年9月10日《財富》
它能顯示人臉，這樣聾啞人或聽力障礙的人能更好地理解佩戴者所說的話。蘋果公司為《財富》雜誌2020年9月9日的凡爾納·科皮托夫（Verne Kopytoff）員工開發了特殊的口罩
蘋果公司與總部設在華盛頓的加拉德特大學（Gallaudet University）合作，選擇使用哪種清晰的口罩。加拉德特大學專門教育聾啞和重聽學生。蘋果公司為《財富》雜誌2020年9月9日的凡爾納·科皮托夫（Verne Kopytoff）員工開發了特殊的口罩
馬斯克說，該公司希望在因脊髓損傷而癱瘓的患者身上測試這項技術，有朝一日能恢復聾啞人或盲人的聽力和視力。神經科醫生對埃隆·馬斯克的Neuralink大腦植入初創公司《財富》雜誌（2020年8月31日）不太確定
如果你問機構高管和員工營銷人員是如何管理這項任務的，他們會告訴你，這是一種平衡行為，因為營銷人員不想顯得“音盲”但他們也不想像最初的迴應廣告那樣在信息傳遞上做得過火。“隱式而非顯式”：廣告商不再想討論冠狀病毒（Kristina Monllos，2020年7月20日，Digiday）
在他看來，他們可能擅長政策，但“在政治上卻充耳不聞”。古巴將如何在皮奧里亞比賽|埃莉諾·克里夫特| 2014年12月21日|每日野獸
鮑曼聲稱，她將此事告訴了她的代理人和律師，但她的指控置若罔聞。比爾·科斯比（Bill Cosby）的一長串原告名單（到目前為止）：1965-2004年間18名據稱的性侵犯受害者|馬洛·斯特恩| 2014年11月24日|每日野獸
一部新的真人秀系列劇突出了人們將在多大程度上給迷戀者留下深刻印象，從假裝失聰到偷竊《我瘋狂的愛》揭示了人們為愛所說的最瘋狂的謊言|凱文·法倫| 2014年11月18日|每日野獸報
吉爾瑪是一名26歲的哈佛法學院畢業生，她又盲又聾。TEDx會談存在殘疾問題，但這位令人難以置信的年輕女性正在努力改變這一狀況| Nina Strochlic | 2014年11月5日| DAILY BEAST
聾啞人和重聽人想要觀看，卻無法接觸到談話內容，這太荒謬了。TEDx會談存在殘疾問題，但這位令人難以置信的年輕女性正在努力改變這一狀況| Nina Strochlic | 2014年11月5日| DAILY BEAST
到那日，聾子必聽見書上的話，瞎子的眼睛必從黑暗黑暗中看見。《聖經》，杜埃·萊姆斯版本，多種多樣
S夫人承認這是真的，但同時表示，他最好記住我們並非都是聾子。德國音樂研究|艾米·費伊
其他人則對壟斷的不公正感到反感，這些話落在耳朵上，如果可以的話，他們會充耳不聞。女士禮儀手冊和禮貌手冊|弗洛倫斯·哈特利
她決心知道上帝對她的旨意；對這樣的人，上帝從不充耳不聞。祈禱母親的價值|伊莎貝爾C.拜倫
所以格蕾絲小姐沒有改變她的服裝，她對帕芬先生的教會權威充耳不聞。《Pit Town Coronet》，第二卷（共3卷）|查爾斯·詹姆斯·威爾斯