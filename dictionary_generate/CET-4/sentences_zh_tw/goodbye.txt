看看這本指南，幫助你找到最適合你需要的暖手器，並告別僅僅為了步行去雜貨店而戴上三倍的手套。最佳暖手器：在你最喜歡的冬季活動中阻擋寒冷| PopSci商業團隊| 2021年2月10日|科普
五十三年前，保羅·格里沙姆（Paul Grisham）穿上他的大雪裝備，在南極度過了13個月的時間，他作為一名氣象學家在地球上最偏遠的一個氣象站的海軍服役，並向南極告別。這個人把錢包忘在南極洲了。大約53年後，他找回了它|Cathy Free | 2021年2月9日|華盛頓郵報
出版商告別了谷歌分析，轉而選擇了一種國產的替代產品，名為Longboat，但沒有回頭看“我們必須擁有數據的全部所有權”：為什麼丹麥最大的新聞網站不再依賴谷歌的技術（Seb Joseph）2021年1月27日（Digiday）
告別溼漉漉的頭髮，準備好離開家時開始感到涼爽和自信。最好的吹風機：在家裡享受一次適合沙龍的吹風| Carsen Joenk | 2021年1月22日|科普
當她哥哥去世時，她飛回家道別，但由於旅行延誤，她來的太晚了。在動物王國，連接、更新和治癒的儀式|芭芭拉·金| 2021年1月22日|華盛頓郵報
就在幾年前，我坐在電腦前，打出了一封類似的告別信。親愛的Leelah，我們將為你繼續戰鬥：致一名已死的變性青少年的信| Parker Molloy | 2015年1月1日| DAILY BEAST
說再見吧，為了看這位好妻子還是行屍走肉來到聖誕夜而和奶奶吵架。新發明讓你隨時隨地看電視| | 2014年12月8日|每日野獸
這一集的其餘部分講述了嘉莉對這一輕率的告別表示憤慨的故事。一位Rom Com作家的自白：Liz Tuccillo談論“性與城市”、“保重”等等| Kevin Fallon | 2014年12月5日| DAILY BEAST
《永不說再見》也能說明當浪漫的想法消失時會發生什麼。紐約作家：我希望我知道如何離開你|莫莉·漢農| 2014年12月1日|每日野獸
你拍的那些人們告別親人屍體的照片特別令人難忘。盯著埃博拉的攝影記者| Abby Haglage | 2014年11月8日| DAILY BEAST
德雷布笑了一聲，謝謝你，先生，他向人群揮手告別，德雷布帶著他離開了一個歡笑的地方。奴隸敘事：美國奴隸制度的民間史
當我們上船時，我們看到我們的大使範戴克先生向他的一些朋友道別，並祝他們一路平安。戰爭與和平之路|迪莉婭奧地利人
當她們向丈夫和愛人道別時，同樣的決心和勇氣也降臨到她們身上。戰爭與和平之路|迪莉婭奧地利人
當一切結束時，也許你會誠實地來告訴我你對還是我對。再見。當瓦爾蒙德來到龐蒂亞克，吉爾伯特·帕克
當他說再見的時候，他覺得他的叔叔看起來病了，老人沮喪地說他的生活失敗了。《神祕的傑作》，第1卷（共4卷）|各種