外出就餐的樂趣之一是與員工的互動，即使是在遠處。現在是客房服務：入住酒店只是為了吃飯的感覺|湯姆·西塞馬| 2021年2月12日|華盛頓郵報|
此外，聯盟正在尋求限制球隊在路上的社交互動。NHL將比賽日快速檢測添加到冠狀病毒協議中| Samantha Pell | 2021年2月12日|華盛頓郵報
董事會會議的重要性正在迅速增加，以促進作為一個組織的一致和重要的互動。早期創業公司Zoom董事會會議的最佳實踐|沃爾特·湯普森| 2021年2月12日| TechCrunch
流行病既是人類行為的產物，也是生物學的產物，因為病毒通過社會互動傳播。研究人員確定了一些社區接種冠狀病毒的社會因素|克里斯托弗·英格拉漢姆| 2021年2月11日|華盛頓郵報
你可以找到它們，跑和躲，儘管與第一場遊戲相比，怪物和玩家之間的互動更多小噩夢讓我每時每刻都害怕。我喜歡它|Elise Favis | 2021年2月9日|華盛頓郵報
經過四五個月的隨意交流後，他們意識到他們都因為癌症失去了一位年輕的父母。參加晚宴的每個人都失去了一個人|薩曼莎·萊文| 2015年1月6日|每日野獸
有時，大型動態的所有錯誤都會在一次小的交互中捕獲。聖路易斯公羊隊參加弗格森之戰|莎莉·科恩| 2014年12月1日|每日野獸
否則，我們會在道德上侵蝕環境，使之成為一種首先讓與他人交流變得如此困難的類型。嘿，令人毛骨悚然，“讚美”也是騷擾| Tauriq Moosa | 2014年11月5日| DAILY BEAST
我認為我們能做的第一件顯而易見的事情就是把每個警察的身體攝像頭都錄下來，你可以說是車內攝像頭。《弗格森的謊言和後果》|馬特·劉易斯| 2014年10月25日|每日野獸》
儘管如此，在海上呆了近一個月後，我想他們還是渴望充電，準備與外界交流。賽道內最大的比賽|莉齊·克羅克| 2014年10月11日|每日野獸
形而上學者就思想和物質的相互作用進行了無休止的爭論。心理分析|伯特蘭·羅素
討論物質和精神事物之間的相互作用，以及在一個有固定原因的世界中自由的可能性。生命之書：第一卷心靈與身體；第二卷愛與社會|厄普頓辛克萊
研究人類進步的學生可能會對思想和制度之間的互動留下越來越深刻的印象。科佩拉蒂的倫理學|詹姆斯·海登·塔夫茨
因此，這裡的每一個元素也相互加強；生命的所有因素都在不斷地相互作用。民間心理學要素|威廉·馮特
我們看到語言和思想相互作用的複雜過程實際上發生在我們的眼睛下。語言|愛德華·薩皮爾