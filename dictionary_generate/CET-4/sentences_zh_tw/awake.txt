她會回家把這件事告訴她現在的丈夫，晚上躺在床上不睡覺，想著這件事不要等待完美：獨角獸創始人和投資者的四大創業祕訣| Beth Kowitt | 2020年9月6日| Fortune
從理論上講，覺醒就是對社會和種族正義問題的覺醒。美國邊緣成為主流| Nick Fouriezos | 2020年9月6日| Ozy
約翰遜進來時睡著了，但醫生很容易把她叫醒。安妮·沃爾德曼和約書亞·卡普蘭於2020年9月2日被送回家送死
雖然還不知道盧卡申科在白俄羅斯被這一消息驚醒時會有什麼樣的反應，但選舉結果讓人難以置信，以至於這個國家爆發了。明斯克的苦難|尤金·羅賓遜| 2020年8月18日|奧齊
每當那隻老鼠想休息時，科學家們就旋轉桌子，輕輕推醒兩隻老鼠，有時把它們推入水中。為什麼睡眠剝奪會導致死亡| Veronique Greenwood | 2020年6月4日| Quanta雜誌
現在半睡半醒，我們需要所有我們能得到的幫助來了解我們的處境。250年來美國民主受到威脅| Jedediah Purdy | 2014年12月28日|每日野獸
我經常上11-7班，所以不得不保持清醒，儘管大多數晚上其他人都在睡覺。詹姆斯·帕特森（James Patterson）在《燃燒的書》視頻《威廉·奧康納》（William O'Connor）2014年11月25日《每日野獸》（DAILY BEAST）中演繹了《華氏451度》
我和我的搭檔布蘭登在黎明時分醒來，乘獨木舟在路易絲湖乳藍色的冰水上航行。我們的氣候戰爭歸零地之旅| Darren Aronofsky | 2014年9月19日| DAILY BEAST
有多少次你的故事讓我夜不能寐，像一個在黑暗中的孩子，想知道附近潛伏著什麼怪物？斯蒂芬·金《墜落》和我父親的詩歌《克里斯托弗·迪基》2014年9月14日《每日野獸》
但這一刻向他暗示，如果他是清醒的，那麼他的現實真的比他想象的更加陌生和危險。村上春樹的怪異奇妙世界|馬爾科姆·瓊斯| 2014年8月15日|每日野獸
嫩咖啡館在他的椅子上睡著了；搬運工走了；只有哨兵在崗位上保持清醒。歐洲概覽|霍勒斯·格里利
他發現他醒著，就坐在他身邊，像一個託兒所女傭一樣認真地拍著他入睡。阿里斯蒂德·普約爾|威廉·J·洛克的歡樂冒險
或者更確切地說，我想我只是半睡半醒；但是你似乎很容易就打開了那扇門，這讓我大吃一驚。離奇的故事|各種各樣
起初她睡得很輕，半醒半醒，昏昏欲睡地注意著周圍的事情。《覺醒》與短篇小說選集《凱特·肖邦》
她醒著躺在床上寫著一封信，與她第二天寫的信完全不同。《覺醒》與短篇小說選集《凱特·肖邦》