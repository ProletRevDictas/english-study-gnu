切斯特納特說，由於狼獾的出生巢穴依賴降雪，它們是一種非常容易受到氣候影響的物種，而氣候變暖正在縮小它們的生存棲息地。雷尼爾山一個世紀以來第一個狼獾媽媽的誕生是該物種捲土重來的標誌| Hannah Seo | 2020年8月28日|科普
再加上選舉、人口普查、刺激計劃檢查以及對送貨上門的新依賴，郵件可能比以往任何時候都更為重要。關於當地郵局運營的信息相互矛盾，很難獲得| Ashly McGlone和Kate Nucci | 2020年8月27日|聖地亞哥之聲
我們還可以看到更多的人依賴Facebook和Twitter的社交信號。GPT-3搜索引擎技術的商品化對谷歌和SEO意味著什麼| Manick Bhan | 2020年8月21日|搜索引擎觀察
前推特產品經理埃文·賴瑟（Evan Reiser）說，騙子們正在利用遠程工作者對數字工具日益增長的依賴性，他現在是“異常”的負責人。不要點擊縮放邀請|Rhackettfortune | 2020年8月19日| Fortune
加利福尼亞大學能源經濟學家伯克利（Severin Borenstein）是獨立系統運營商管理委員會的負責人。他說，國家對可再生能源的日益依賴“肯定”在停電中發揮了作用。以下是如何防止加州電網在高溫下發生屈曲|詹姆斯·坦普爾| 2020年8月18日|麻省理工技術評論
基督教催生了最初在埃及的修道院運動，該運動堅持和平、自力更生、教育和慈善。Karen Armstrong的新規則：宗教不應對暴力負責| Patricia Pearson | 2014年10月29日| DAILY BEAST
佩蒂說，有人曾經告訴他，他們理解他對視力、觸覺甚至嗅覺的依賴。Philippe Petit在世貿中心走鋼絲時的關注時刻| Anthony Haden Guest | 2014年8月8日|每日野獸
我對舊電視節目最大的不滿是他們對可預測公式的依賴。搖搖欲墜的音樂產業可以從電視上學到的五個教訓| Ted Gioia | 2014年8月3日| DAILY BEAST
“奧創時代”還將解決我們對技術的依賴和對技術可能走向何方的恐懼。”《復仇者：奧特倫時代》揭幕：小羅伯特·唐尼及其同事在漫畫大會上的演講|安娜莉莎·薩維奇| 2014年7月27日|每日野獸
儘管如此，懸念小說的作者對事實的依賴和執著程度各不相同。寫小說：即使是虛構也需要研究|雷德利·皮爾森| 2014年7月16日|每日野獸
如果一所房子本身分裂，又不能免受其成員的鋪張浪費和掠奪，那麼它還能依靠什麼呢？布萊克伍德的愛丁堡雜誌，第CCCXXXIX號。1844年1月。第LV卷|各種各樣的
隨著我們越來越多地瞭解自然法則，我們越來越不依賴祈禱的效果。上帝與我的鄰居羅伯特·布拉奇福德
我們相信，在現代，這種巧妙的政策，加上明顯的自力更生，幾乎沒有實例。布萊克伍德的愛丁堡雜誌，第CCCXXXIX號。1844年1月。第LV卷|各種各樣的
事實上，個別案例表明，實際上缺乏自力更生能力。兒童和青少年道德犯罪特別委員會的報告| Oswald Chettle Mazengarb等人。
時至今日，在Servia，流行的咒罵表達了對自然力量的依賴和依賴。胡利安·沙曼（Julian Sharman）的粗俗咒罵史