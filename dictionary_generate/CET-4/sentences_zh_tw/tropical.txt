隨著勞拉的軌跡向南移動，遠離佛羅里達州，熱帶風暴馬可似乎正在進入它身後的墨西哥灣。2020年8月的極端天氣背後是什麼？氣候變化與厄運|卡羅琳·格拉姆林| 2020年8月27日|科學新聞
紅十字會等組織正開始利用季節性天氣預報來制定應對天氣災害的策略，比如在熱帶氣旋可能襲擊某個地區時，確定應急物資的運送地點。改進的三週天氣預報可以從災難中拯救生命| Alexandra Witze | 2020年8月27日|科學新聞
這種空氣循環擾動的影響可以在全球範圍內感受到，抑制了大西洋上空的風，否則可能會將熱帶風暴分開。對2020年大西洋颶風季節的預測變得更糟了| Maria Temming | 2020年8月7日|科學新聞
過度砍伐森林意味著熱帶森林現在產生的溫室氣體比它們吸收的要多。科學家說：森林砍伐|伯大尼布魯克郡| 2020年8月3日|學生科學新聞
一項新的分析發現，減少熱帶森林砍伐和限制野生動物貿易可能是在流行病爆發之前阻止它們的經濟有效的方法。為了防止下一次大流行，我們可能需要減少砍伐樹木|喬納森·蘭伯特| 2020年7月23日|科學新聞
墨西哥中部梅斯基特樹上的槲寄生與大量熱帶鳥類有關。槲寄生是植物的吸血鬼|海倫·湯普森| 2014年12月21日|每日野獸
利比里亞軍隊廢棄的兵營就在熱帶叢林的另一邊。一名感染埃博拉的利比里亞兒童的生活|莎拉·克羅| 2014年11月5日|每日野獸
一場熱帶風暴，在雷達上清晰可見數天，是罪魁禍首。尼泊爾最致命的雪崩是完全可以避免的|迪克·多沃斯| 2014年10月20日|每日野獸|
當我結束與穆尼的通話時，他正在監測熱帶風暴岡薩洛正在演變成一場等級颶風。維克多·穆尼為死去的兄弟賈斯汀·瓊斯（Justin Jones）所作的史詩般的冒險故事2014年10月19日《每日野獸》
從內心深處，仰望熱帶的天空就像是透過某種泥土教堂的圓頂凝視。是什麼造就了墨西哥最神祕的海灘|布蘭登出版社| 2014年10月14日|每日野獸報
藍斑霸王：熱帶王鳥。尤卡坦半島的夏季鳥類| Erwin E.Klaas
這條優雅的飛魚像一隻美麗的白鳥，掠過蔚藍壯麗的熱帶海洋。上帝與我的鄰居羅伯特·布拉奇福德
古巴的土壤肥沃，島上生長的熱帶植物和水果種類繁多。菸葉它的歷史、品種、文化、製造和商業——E.R.比林斯。
我曾經處於熱帶的平靜中，在嘗試了這兩種天氣之後，我真的更喜歡微風和雷雨。巴西航行日誌|瑪麗亞·格雷厄姆
幸運的是，颶風僅限於熱帶地區的一小部分地區。地球歷史綱要|納撒尼爾·索斯蓋特·謝勒