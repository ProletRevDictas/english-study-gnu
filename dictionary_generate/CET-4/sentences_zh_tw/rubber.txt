這種對歐拉公式的拓撲理解——在歐拉公式中，形狀是橡膠狀的，而不是剛性的——是在1861年約翰·列斯汀（Johann Listing）的一篇文章中首次提出的。拓撲學101：黑洞真相|大衛·S·裡奇森| 2021年1月26日|量子雜誌
Baffins和Sorels的不同變體認為橡膠底、合成頂靴很受歡迎，具有良好的吸溼性，而且更輕，但通常在極端寒冷的天氣下不如兔子靴有效。穿得像一個阿拉斯加人來抵禦冬天的寒冷|泰勒·弗里爾/戶外生活| 2021年1月20日|科普
他們有克雷和哈珀，橡膠手臂適合在大聯盟和小聯盟之間搖擺。國民隊的牛棚不需要改造。但它可能需要另外一兩隻手臂|傑西·多爾蒂| 2021年1月14日|華盛頓郵報
將麵糊倒入平底鍋後，用橡膠抹刀將其頂部抹平。烤出有價值蛋糕的四個技巧|薩維爾| 2021年1月12日|科普
在橡膠抹刀上輕輕塗上潤滑油，然後用它將棉花糖混合物轉移到塗了潤滑油的平底鍋中，均勻但快速地攤鋪，因為棉花糖混合物將開始凝固。黏糊糊的、甜甜的自制棉花糖會提升你的口味和熱巧克力| Daniela Galarza | 2021年1月12日|華盛頓郵報|
這些村莊過去常常收穫橡膠、可可、棕櫚油和咖啡豆。剛果被遺忘的殖民地逃亡|尼娜·斯特羅奇利奇| 2014年12月18日|每日野獸
他穿著塑料罩衫在一間橡膠房裡呆了三天才回來。《監獄裡的一百萬種死亡方式》|丹尼爾·吉尼斯| 2014年12月8日|每日野獸》
用橡膠抹刀將巧克力和蔓越莓攪拌至麵糰充分混合。製作這些赤腳伯爵鹹燕麥巧克力塊餅乾| Ina Garten | 2014年11月28日| DAILY BEAST
很快，他的金庫裡就堆滿了橡膠、棕櫚油和象牙的收入剛果的阿爾戈：斯坦利維爾人質危機的幽靈|尼娜·斯特羅奇利奇| 2014年11月23日|每日野獸
我按下背心上一角硬幣大小的橡膠按鈕，它與我的收音機相連。我射殺了本拉登|埃利奧特·阿克曼| 2014年11月16日|每日野獸|
為電燈作業提供一種特殊質量的手套，由石棉布製成，內襯橡膠。石棉|羅伯特·H·瓊斯
花園裡的人行道很潮溼，埃德娜叫女僕拿出她的橡膠涼鞋。《覺醒》與短篇小說選集《凱特·肖邦》
Mackintoshes、硫化印度橡膠、杜仲膠和薄紗防塵衣當時還不為人知。朴茨茅斯公路及其支流|查爾斯G.哈珀
但是“兒子”像一個橡皮球一樣從衝擊中反彈，或者像他那樣是學校裡訓練最好的體操運動員。多蘿西在斯凱里|伊芙琳·雷蒙德
今天，他們在帆布和橡膠上穿了一件薄薄的大衣。祖先|格特魯德·阿瑟頓