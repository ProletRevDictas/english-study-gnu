當我們其餘的人待在家裡隔離時，有影響力的人會在游泳池的花車上休息，與粉絲見面和打招呼，一起聚會。蒂克托克之家如何自毀|麗貝卡·詹寧斯| 2020年10月1日| Vox
這座位於奧扎克山脈的夢幻小屋的兩邊是香農戴爾州立森林，靠近這條河上漂流旅行的一個很好的中途點。每個州我們最喜歡的Hipcamp | Alison Van Houten | 2020年10月1日|戶外在線
魁北克市拉瓦爾大學的海洋學家阿希姆·蘭德爾霍夫（Achim Randelhoff）及其同事在巴芬灣部署了自動潛水浮標，可以測量水下的光合活性和藻類濃度。被困在冰下的喜光藻類在黑暗的北極冬季生長|喬納森·蘭伯特| 2020年9月25日|科學新聞
下面列出了一些我最喜歡的水上裝備，它們可以幫助我享受剩下的炎熱日子，無論是去峽谷探險還是和孩子們一起悠閒地漂流。讓我享受夏日最後幾天的裝備| Graham Averill | 2020年9月15日|戶外在線
如果你覺得不需要速度，Super Mable會成為一個很棒的游泳池或離岸漂浮物。最佳划船管| PopSci商業團隊| 2020年9月3日|科普
與此同時，愛潑斯坦試圖利用他的慈善項目讓他重回巔峰。下流億萬富翁的雙重生活與斯蒂芬·霍金的海灘派對| M.L.雀巢| 2015年1月8日|每日野獸
你可以想走多遠就走多遠，也可以在水面上漂浮。D'Angelo的《黑色彌賽亞》值得為《詹姆斯·喬納》（James Joiner）2014年12月16日《每日野獸》（DAILY BEAST）等上15年
他以一種我欣賞的冷靜、直率的態度，把這個話題提到希區柯克身上。阿爾弗雷德·希區柯克的《褪色為黑色：偉大導演的最後日子》|大衛·弗里曼| 2014年12月13日|每日野獸報
他們死的時間還不夠長，還不能漂浮，但遲早會到來的。無論你做什麼，總有人會死。關於伊拉克不可能的選擇的短篇故事|內森·布拉德利·貝希亞| 2014年8月31日|每日野獸
但神奇的是，它們必須漂浮在離我們如此之遠的天空中，它們美麗的光芒將永遠照耀著我們。比利·克里斯托在艾美獎上對羅賓·威廉姆斯的致敬是完美的|凱文·法倫| 2014年8月26日|每日野獸|
人們經常希望確定因體積太小而無法使尿比重計漂浮的液體的比重。臨床診斷手冊|詹姆斯·坎貝爾·託德
雲煤變得越來越淡，變成紫色；現在，它們在灰燼中飄向冰冷的藍色。山谷戰士|納爾遜·勞埃德
一個巨大的浮標出現了，描繪了石器時代和原始人，每一個細節都經過了博物館的仔細研究。真正的拉丁四分之一| F.Berkeley Smith
它們慢慢地在黑夜中飄浮，看起來像仙女之手的作品。女人的環球之旅|艾達·普費弗
植物的種子被包裹在通常很密的外殼中，因為它們漂浮在水面上，所以可以被獨立地運送到很遠的地方。地球歷史綱要|納撒尼爾·索斯蓋特·謝勒