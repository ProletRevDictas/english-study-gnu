一位熟悉彭斯準備工作的人士表示，幾次與辯論相關的會議定於週四舉行。知情人士稱，前威斯康星州州長斯科特·沃克（Scott Walker）正與彭斯（Pence）合作準備辯論。|羅伯特·科斯塔| 2020年9月17日|華盛頓郵報|
很明顯，麥當勞想要迎合顧客，特別是在流感大流行期間，很多麥當勞員工可能已經熟悉特拉維斯·斯科特了。麥當勞教授員工Travis Scott流行語，因為TikTok Trend | Jaya Saxena | 2020年9月17日| Eater
據知情人士透露，畢爾巴鄂銀行（Banco Bilbao Vizcaya Argentaria SA）、德國商業銀行（Commerzbank AG）、匯豐控股有限公司（HSBC Holdings Plc）、荷蘭國際集團（ING Groep NV）和法國興業銀行（Societe Generale SA）正在處理這筆交易。大眾汽車是最新一家利用紅熱的綠色債券市場為其電動汽車雄心壯志（Bernhard Warner，2020年9月16日）財富（Fortune）融資的汽車製造商
兩名國會助手和一名熟悉會議情況的獨立官員證實了拜登與民主黨參議員的會面，這是《政治》雜誌首次報道的。特朗普在市政廳表示，他不會在流感大流行問題上做任何不同的事情。|科爾比·伊特科維茨、喬什·道西、費利西亞·桑梅茲、約翰·瓦格納| 2020年9月16日|華盛頓郵報
據一位不願透露姓名的知情人士透露，雲數據軟件製造商雪花（Snowflake）的首次公開發行（IPO）定價為每股120美元，高於上市價。斯諾弗雷克的IPO變得更熱鬧了一點——凡爾納·科皮托夫（Verne Kopytoff）2020年9月15日《財富》（Fortune）
Lalo說，他向他的冰上管理員報告了綁架事件，一位熟悉此案的前聯邦探員證實了這一點。一名告密者、一名失蹤的美國人和華雷斯的死亡之家：在大衛·卡斯特羅12年的冷酷案件中|比爾·康羅伊| 2015年1月6日|每日野獸|
這是自1975年以來，在遠離外界後，他感到熟悉的為數不多的事情之一。他40年後出獄的第一天：適應戶外生活| Justin Rohrlich | 2015年1月3日| DAILY BEAST
當《唐頓莊園》週日晚上回歸時，它的時尚迷們都在享受一種熟悉的待遇。唐頓時尚的真正含義|凱蒂·貝克| 2015年1月2日|每日野獸
在俱樂部的中間，有一種海米什語（意第緒語，意為熟悉的、古老的學校）的性質。單身猶太人最瘋狂的約會之夜，槲寄生被丟棄拍照| Emily Shire | 2014年12月26日| DAILY BEAST
不過，除了淫穢之外，這個球還為陶、拉沃、公園和夢酒店等時尚場所增添了一種熟悉的樸實無華。單身猶太人最瘋狂的約會之夜，槲寄生被丟棄拍照| Emily Shire | 2014年12月26日| DAILY BEAST
幾位熟悉悲劇發生時的事實的先驅者也在場。在蘇族人中| R.J.克雷斯韋爾
他個子很高，身材也很熟悉，火光在他金黃色的短髮的捲曲中閃爍。邦德男孩|喬治·W.（喬治·華盛頓）奧格登
他向旁邊瞥了一眼，看到一張非常漂亮、黝黑的臉，看上去有點眼熟。尋找父親的迷迭香| C.N.威廉姆森
這是一座安全的老房子嗎？童年時代已經過去了，房子裡到處都是友好和熟悉的面孔？將死|約瑟夫·謝里丹·勒法努
一個三歲零九個月大的小男孩從他的護士那裡收到了熟悉的命令，“過來！”孩子們的方式|詹姆斯·薩利