事實上，Troye並不是唯一一個從內部角度公開反對政府應對冠狀病毒的人。特朗普冠狀病毒反應的毀滅性畫面——來自第一手證人亞倫·布萊克（2020年9月17日）《華盛頓郵報》
USPS內部審計發現，在2020年初選後期，超過100萬張選票被郵寄給選民，其中包括選舉後收到的數百張選票。ProPublica的《確保你的選票有價值的大流行指南》| Susie Armitage | 2020年9月16日| ProPublica
評估內部搜索的有效性，注意搜索後如何查找和組織內容。如何在流感大流行期間推動必要的數字創新| Nick Chasinov | 2020年9月16日|搜索引擎觀察
聲明說，其他文件被扣留，以保護董事會的內部審議程序。“掩蓋”：眾議院民主黨人傳喚NLRB拒絕在Ian MacDougall於2020年9月15日進行的道德調查中分享的文件
為了讓用戶參與進來，請確保製作高質量的副本，並提供大量的視覺效果和內部鏈接。8大谷歌排名因素-搜索引擎優化指南|贊助內容：搜索引擎優化動力套件| 2020年9月15日|搜索引擎領域
將溫度降至325°F，繼續烹飪，直到溫度計上顯示內部溫度為140°F。製作卡拉大廳的烤豬排配小紅莓|卡拉大廳| 2014年12月24日|每日野獸
其結果是一個不向外部替代品開放，也沒有內部創新激勵的體系。您當地的學校不必太差勁| Michael S.Roth | 2014年12月17日| DAILY BEAST
一個世俗的警察國家，擅長壓制內部挑戰。週日實況調查節目：12月14日| PunditFact.com | 2014年12月14日|每日野獸
磨砂玻璃放在食物中會導致內出血，而通過煮沸濃縮的尼古丁會導致心臟病發作。《監獄裡的一百萬種死亡方式》|丹尼爾·吉尼斯| 2014年12月8日|每日野獸》
比這些數字重要得多的是一種沒有統計數據的內部動態。啤酒國家vs.葡萄酒國家|克萊夫·歐文| 2014年12月7日|每日野獸
在內部壓力下，他的鬍鬚豎立著，臉變得通紅。邦德男孩|喬治·W.（喬治·華盛頓）奧格登
當時，他的一臺高壓噴氣式發動機在斯塔福德郡工作，帶有圓柱形鍋爐和內管。理查德·特雷維希克生平，第二卷（共2卷）|弗朗西斯·特雷維希克
鍋爐為特雷維希克圓柱形，內管，全部由熟鐵製成。理查德·特雷維希克生平，第二卷（共2卷）|弗朗西斯·特雷維希克
鍋爐是圓柱形的，由熟鐵製成，帶有內部消防管和外部磚煙道；《理查德·特雷維希克的一生》第二卷（共2卷）弗朗西斯·特雷維希克
說著這些話，這位善良的女士心煩意亂地指著碗櫃，由於內部痙攣而抽搐了一下。《霧都孤兒》，第二卷（共3卷）|查爾斯·狄更斯