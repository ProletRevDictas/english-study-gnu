未被起訴的前eBay首席執行官Devin Wenig去年發短信給一位同事，表達了對這些不光彩報道的擔憂和憤怒。四名前eBay員工承認網絡跟蹤陰謀罪名| Verne Kopytoff | 2020年9月23日|財富
過去十年中最成功的涓涓流運動不是由恐懼或憤怒激發的，而是由興奮激發的。氣候行動：恐懼並沒有激勵人們，所以讓我們讓他們興奮起來|馬修海默| 2020年9月22日|財富
馬丁·路德·金為民權而鬥爭的動機既有對不公正的憤怒，也有對愛情的熱愛。哲學和心理學都同意——對不戴面具的人大喊大叫是行不通的| LGBTQ編輯| 2020年9月21日|沒有直接新聞
此外，如果你的家人缺乏特權，有必要談談如何處理歧視帶來的憤怒。如何與孩子們談論冠狀病毒和其他壞消息| Sara Kiley Watson | 2020年8月6日|科普
解散基金運動的一個憤怒來源是，福克納提議增加SDPD的支出，儘管該市的預算因疫情而縮減。政治報道：假期租金？就像過去一樣|斯科特·劉易斯和安德魯·基阿特斯| 2020年7月4日|聖地亞哥之聲
如果克里斯蒂不是一個有憤怒管理問題的總統候選人，那麼這一事件甚至可能不會被列入名單。2014年對於卑鄙的帕特里夏·墨菲（Patricia Murphy）2014年12月30日《每日野獸》（DAILY BEAST）來說是一個令人愉快的好年頭
他們既不因她明顯的憤怒而感到不安，也不認為她有威脅性。如果我撞到白人警察的臉會怎麼樣|Goldie Taylor | 2014年12月30日|每日野獸
作家兼心理學家安德里亞·勃蘭特（Andrea Brandt）寫道：我們大多數人與憤怒有著不健康的關係。自助書籍真的能讓你煥然一新嗎|Lizzie Crocker | 2014年12月29日|每日野獸
但她並沒有通過一位公關人員悄悄地發表聲明，而是將自己的憤怒發洩到了四面八方。詹妮弗·勞倫斯的正義之怒說出了我們想說的一切|凱文·奧基夫| 2014年12月29日|每日野獸
憤怒通常表現為另一種自我毀滅但更為社會接受的感覺或行為，如焦慮。自助書籍真的能讓你煥然一新嗎|Lizzie Crocker | 2014年12月29日|每日野獸
他本能地試圖隱藏痛苦和憤怒，這隻會增加已經存在的距離。波浪|阿爾傑農·布萊克伍德
然後，她把她的憤怒從她；也要消除她迄今為止對他的傲慢和蔑視。聖馬丁之夏|拉斐爾·薩巴蒂尼
如果他繼續忘恩負義和殘暴，說我的憤怒是無止境的，我的心都碎了，他會把我砸死的。布萊克伍德的愛丁堡雜誌，第CCCXXXIX號。1844年1月。第LV卷|各種各樣的
所有這些脾氣和憤怒的表現都是我在其他許多信中向陛下指出的。菲律賓群島，1493-1898年，第二十卷，1621-1624年
有一段時間，大衛叔叔氣得心煩意亂，我想他快要打他了。將死|約瑟夫·謝里丹·勒法努