此時，交易後活動對谷歌來說是非常肥沃的土壤。本地搜索的新時代已經到來：谷歌的本地信任包| Justin Sanger | 2020年9月18日|搜索引擎土地
他們擊中的是世界上理論上最肥沃的死衚衕。本週《環球網》（至9月12日）精彩科技故事|奇點中心員工| 2020年9月12日|奇點中心
該應用程序將向他們顯示您過去、當前和預測的時間段、生育窗口和PM的日期。您需要了解的有關週期跟蹤的所有信息| Christine Yu | 2020年9月6日|外部在線
南非有60萬人感染，已成為疫苗的肥沃試驗場。這些國家沒有在等待美國、中國或英國的科維德疫苗，Claire Zillman，記者，2020年8月26日的財富。
SpaceX的融資正值一個肥沃的時期，對特斯拉姐妹公司和更廣泛的資本市場來說都是如此。埃隆·馬斯克（Elon Musk）的SpaceX在其最新基金《Verne Kopytoff》2020年8月18日《財富》（Fortune）中獲得了極高的估值
第八十九屆國會可能是他夢想議程上廣泛爭議項目的肥沃土壤。感謝國會，而不是LBJ《偉大社會》|朱利安·澤利澤，斯科特·波奇| 2015年1月4日|每日野獸報
目前，並不是每個婦女都足夠年輕、有生育能力或健康，能夠用自己的卵子或子宮生孩子。男人總有一天會有沒有女人的孩子|薩曼莎·艾倫| 2015年1月3日|每日野獸
土地肥沃，有沖積或未固結的土壤。Ester Elchies，威士忌建造的莊園| | 2014年12月10日|每日野獸
有些地方乾旱，幾乎貧瘠，另一些地方綠色肥沃馬具製造商的夢想：“不太可能的德克薩斯牧場之王”| Nick Kotz | 2014年9月20日| DAILY BEAST
即使是最豐富的想象力也無法在黑暗中召喚出比現在更好的怪物。逃離伊斯蘭國戰爭的年輕女孩|錢德拉·凱利森| 2014年9月16日|每日野獸
在某些莖上，可育的球果出現，孢子在6月左右成熟，然後枯萎。如何認識蕨類植物Leonard Bastin
聖安東尼奧·德貝克斯位於一個肥沃且灌溉良好的山谷中，從薩拉多河向西延伸。布萊克伍德的愛丁堡雜誌，第CCCXXXIX號。1844年1月。第LV卷|各種各樣的
窮人的居住地沒有意大利那麼悲慘，但不等於瑞士肥沃的土地。歐洲概覽|霍勒斯·格里利
在大多數情況下，該國地勢平坦，肥沃，雖然種植了相當多的小麥，但大部分用於放牧。歐洲概覽|霍勒斯·格里利
它不像比利時中部那樣水平，也不像比利時中部那樣完美，但總體上肥沃，前景廣闊。歐洲概覽|霍勒斯·格里利