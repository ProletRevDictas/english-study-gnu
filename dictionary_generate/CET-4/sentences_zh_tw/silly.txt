例如，聲稱耳朵突出的人天生意志薄弱，這顯然是愚蠢的。性別就是你對它的理解-第88期：愛與性|查爾斯·金| 2020年8月5日|鸚鵡螺
好吧，早在1世紀初，“傻”的最初含義是“受祝福的”，或者更準確地說是“精神上受祝福的”。“傻”是一個積極的詞還是一個消極的詞|坎迪斯·布拉德利| 2020年7月24日| Z之後的一切
這聽起來可能有點傻，但在我們的文化中，人們常常認為科學家或數學家一定是被關在自己工作的房間裡的瘋子。他的領域缺乏多樣性困擾著這位數學家| Esther Landhuis | 2020年4月14日|學生科學新聞
有時我的朋友問一些他們擔心很愚蠢的問題。舉重是這位行星科學家的消遣|布萊恩·納爾遜| 2020年3月10日|學生科學新聞
我懷疑，當我們回首100年，甚至50年後，我們會驚訝地發現，目前我們心中珍視的許多愚蠢想法實際上是錯誤的。你可能誤用了5個心理學術語（第334集重播）|斯蒂芬·J·杜布納| 2020年1月9日|畸形經濟學
忘掉那些愚蠢的“玩球遊戲”；他們“對身體來說太暴力了，在思想上也沒有個性”；嘗試一些聲明|凱文·布萊耶| 2015年1月1日|每日野獸
除了接觸國際觀眾之外，離開奧茲還有另一個好處——不再愚蠢地侵犯她的隱私。CNN《一夜悉尼之星》|勞埃德·格羅夫| 2014年12月16日|每日野獸報
一些評論家討厭它，認為它傲慢、愚蠢和膚淺《新聞編輯室》一開始就結束了：怪異、有爭議、高貴|凱文·法倫| 2014年12月15日|每日野獸報
所以，祝這個愚蠢的時尚經典20歲生日快樂。20歲生日快樂，Liz Hurley的安全別針連衣裙| Tim Teeman | 2014年12月12日| DAILY BEAST
這是性感的，愚蠢的，在那些相對溫和的時代聳人聽聞。20歲生日快樂，Liz Hurley的安全別針連衣裙| Tim Teeman | 2014年12月12日| DAILY BEAST
如果他能更認真地對待她，那真是太遺憾了，他娶了她，因為她太傻了！自信|亨利·詹姆斯
“我想知道為什麼聰明人總是選擇愚蠢的妻子，”她故意補充道，玩弄著韁繩。泰莎·沃茲沃思的紀律|珍妮·M·德林克沃特
我只是坐在那裡，幾乎傻到把鉛錘敲了，然後看著地毯上的一朵大玫瑰。亞歷克·勞埃德，牛仔|埃莉諾·蓋茨
穿著極端時尚的愚蠢錯誤往往不僅犧牲了良好的品味，也犧牲了健康。女士禮儀手冊和禮貌手冊|弗洛倫斯·哈特利
想想看，像這樣一個愚蠢的錢包裡不小心裝了一百美元！舞臺上的中央高中女生|格特魯德·W·莫里森