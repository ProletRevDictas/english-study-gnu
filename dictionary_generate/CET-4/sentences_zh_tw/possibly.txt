然而，如果你發現自己並不十分關心成為最大的自己，那麼這場鬥爭的解決方案很可能是完全放棄我是如何根據世界上最著名的企業家雷切爾·金（Rachel King）2020年9月15日《財富》雜誌（Fortune）的見解打造這個“主持人蓋伊·拉茲”（host Guy Raz）的
“我們可能正處於人類歷史上最孤獨的時刻，”切斯基宣稱。Airbnb首席執行官：流感大流行將迫使我們看到更多的世界，而不是更少的《凡爾納·科皮托夫》《財富》雜誌（2020年9月7日）
有一天，我們在談論這是怎麼可能發生的，要想成為一個騙子，把所有的故事都講清楚，需要付出多大的努力。“人們想要相信”：愛情欺詐如何圍繞一個浪漫的騙子建立一個吸引人的家庭| Alissa Wilkinson | 2020年9月4日| Vox
積極的一面超過了消極的一面，但是你不可能是第一個並且認為這會對你很順利。譚·法蘭西深陷種族主義，當他幾乎退出《怪眼》時，尤金·羅賓遜（Eugene Robinson）於2020年9月3日（Ozy）
看來很多客戶可能都在進行某種形式的內部鬥爭，以使預算得到批准看似不停：媒體買家不斷要求重新規劃和重組活動|克里斯蒂娜·蒙洛斯| 2020年9月2日|迪吉迪
與此同時，在佛羅里達州，布什被關於同性戀婚姻是否可能進入陽光州的問題淹沒。傑布·布什看不見的反同性戀婚姻電子郵件|傑基·庫奇尼奇| 2015年1月9日|每日野獸
如果他支持為1988年去世的黑人法官命名一家郵局，斯卡利塞的選民中有誰會在意呢？史蒂夫·斯卡利斯沉默的代價|傑森·貝里| 2015年1月7日|每日野獸
幾小時內，數千名伊朗人在社交媒體上向外交部長提出挑戰，詢問這可能是怎麼回事。假期坐牢：我們希望2015年11名政治犯獲釋| Movements.Org | 2014年12月25日| DAILY BEAST
好萊塢可能會擔心朝鮮的潛伏細胞會炸燬放映反諾克電影的電影院。平壤洗牌：好萊塢對索尼黑客行為極度恐慌|詹姆斯·普洛斯| 2014年12月19日|每日野獸
他們的獎賞是：這可能是新千年最令人憤怒的系列結局。2014年最大的炸彈：性錄影帶、瑪麗亞·凱莉的演唱、我是如何認識你母親等|凱文·法倫| 2014年12月19日|每日野獸
戴維穿過門道，發現自己置身於一個可以想象得到的最奇怪的小鄉村。戴維與妖精|查爾斯·E·卡里爾
梅耶少校很可能早在1802年就受到普魯德洪的影響，可能是在那之前。從公元前七世紀到公元二十世紀的女性美術作品|克拉拉·厄斯金·克萊門特
艾麗斯·雅頓，你對這個人的小小夢想，以及一條可能向你飛馳而來的解脫之路。將死|約瑟夫·謝里丹·勒法努
也許，在騎了20英里的摩托後，他不會對這種怪事感到羞怯。紅色的一年|路易斯·特蕾西
他不可能懷疑或質疑，羞恥感淹沒了他，直到他覺得自己是活著的最卑鄙的人。波浪|阿爾傑農·布萊克伍德