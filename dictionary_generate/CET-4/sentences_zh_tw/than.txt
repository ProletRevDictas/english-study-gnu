然而，正如羅伯特·沃德發現的那樣，馬文在現實生活中表現出的超凡男子氣概令人驚訝。李·馬文的自由帷幔微笑背後的故事|羅伯特·沃德| 2015年1月3日|每日野獸
幾年前，我的小兒子，比箭還直的兒子在兩個不同的司法管轄區被截獲和逮捕。如果我撞到白人警察的臉會怎麼樣|Goldie Taylor | 2014年12月30日|每日野獸
他是一個超凡脫俗的人物，在這場競選中，他顯得出奇的高大。前普羅維登斯市長兼前獄警巴迪·齊安奇的救贖之旅破產了|大衛·弗裡德蘭德| 2014年11月4日|每日野獸
巴拉克·奧巴馬（Barack Obama）將面臨比平時更艱難的幾個月。嘿，國會：投票表決這場該死的戰爭|邁克爾·托馬斯基| 2014年9月30日|每日野獸
金姆在嘲笑她職業生涯賴以建立的整個價值體系，以及她自己不那麼美好的過去。金·卡戴珊不再是笑柄了|艾米·齊默爾曼| 2014年8月14日|每日野獸
傑克在那次旅行中對《聖經》的歷史和英雄們的瞭解可能比他以前所有的歲月都要多。馬克·吐溫，傳記，1835-1910，全集|阿爾伯特·畢格羅·潘恩
“萊特兄弟在二十世紀早期發明了比空氣輕的飛船，”他說。失落的戰艦|羅伯特·摩爾·威廉姆斯
魯格爾告訴他，這是平衡時刻，是超光速運動的峰值。空間的色彩|馬裡恩·齊默·布拉德利
如果競爭對手在同等規模的業務中支付低於整車的價格，那麼他將不幸地處於不利地位。鐵路：費率和法規| William Z.Ripley
總的來說，直到十九世紀，在使用比空氣重的機器飛行方面才取得任何真正的進展。飛機傳奇|勞倫斯·亞德·史密斯