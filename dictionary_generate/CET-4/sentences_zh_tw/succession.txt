上週，美國因主要與地面系統有關的問題而取消了一系列發射嘗試。SpaceX，諾斯羅普試圖通過週五晚上的嘗試打破發射“小精靈詛咒”| Eric Berger | 2020年10月2日| Ars Technica
這是對美國民主的最大考驗——對克斯特尤科夫來說也是如此。克斯特尤科夫的兩位前任在弗拉基米爾·普京不高興的謠言中相繼去世。《星期日》雜誌：決策者|丹尼爾·馬洛伊| 2020年9月13日|奧齊
巴西時裝零售商達菲蒂（Dafiti）一瘸一拐地走過了一連串的低谷。歐洲最受炒作的創業孵化器：火箭互聯網|傑里米·卡恩| 2020年9月1日|財富
一系列氣候案件目前正在美國法院審理。卡馬拉·哈里斯（Kamala Harris）堅持與石油公司決一死戰的十年|邁克爾·J·科倫| 2020年8月14日|石英
這可能會繼續發生，使我們面臨一系列新的、潛在致命的疾病。如何應對新冠病毒-19造成的社會距離|希拉·穆魯尼·埃爾德雷德| 2020年3月23日|學生科學新聞
這場混戰以200英鎊的賭注開始，緊接著是連續幾次較小的賭注。女王會在聖誕節退位嗎|湯姆·賽克斯| 2014年12月17日|每日野獸
接著，他先後創作了《眩暈》（1958年）、《西北偏北》（1959年）、《精神病》（1960年）和《鳥類》（1963年）。阿爾弗雷德·希區柯克的《褪色為黑色：偉大導演的最後日子》|大衛·弗里曼| 2014年12月13日|每日野獸報
他說：“起初我在一張帆布床上，後來又在一系列的住宿中。”。《每日野獸》2014年10月8日《使他的城鎮陷入醜聞的牧師》|勞埃德·格羅夫|
20世紀的兩種現象，接二連三地發生，是罪魁禍首。蘇格蘭的“是”運動和蘇格蘭平等的神話|諾亞·考德威爾| 2014年9月18日|每日野獸
在接任了一批灰色總統後，TR以其不自覺的精力、理想主義和決心為辦公室注入了活力。從廣場協議到新政：TR和FDR的重疊政治身份|約翰·阿夫隆| 2014年9月9日|每日野獸
然後在李斯特音樂廳待上幾個小時，演奏一系列協奏曲、幻想曲和各種各樣的精彩作品。德國音樂研究|艾米·費伊
這種閱讀只會使眼睛看到一連串的景象，耳朵聽到一連串的聲音。同化記憶| Marcus Dwight Larrowe（又名A.Loisette教授）
受傷和憤怒的動物發出一連串的惡魔般的尖叫。《Pit Town Coronet》，第一卷（共3卷）|查爾斯·詹姆斯·威爾斯
這一嘗試失敗了，緊接著是一連串雜亂無章的猛攻和傳球，令人眼花繚亂。《Pit Town Coronet》，第一卷（共3卷）|查爾斯·詹姆斯·威爾斯
如果你能連續演奏八遍而不漏一個音符，我會很滿意的。德國音樂研究|艾米·費伊