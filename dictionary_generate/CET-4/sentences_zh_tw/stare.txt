一隻蟬甚至落在我的嘴上，冷冷地盯著我的眼睛。我的名字是馬克斯，我有一個蟬的問題|凱文·安布羅斯| 2021年5月28日|華盛頓郵報|
索托馬約爾寫道：“法院對凝視判決的尊重已經下降到多麼低的程度。”。最高法院對被判處無期徒刑的未成年人的裁決|羅伯特·巴恩斯| 2021年4月22日|華盛頓郵報
在他們中間站著一名身穿紅色貝雷帽的黎巴嫩武裝部隊士兵，手持突擊步槍，目不轉睛地盯著他們。受基地組織威脅的遜尼派-什葉派愛情故事|露絲·邁克爾森| 2014年12月26日|每日野獸
然後他安靜了下來，而我茫然地盯著他，直到他補充道：“鏡頭永遠不能移動。”阿爾弗雷德·希區柯克的《褪黑：偉大導演的最後日子》|大衛·弗里曼| 2014年12月13日|每日野獸報
在史蒂文·尤金·華盛頓的案例中，僅僅是一個茫然的凝視就讓他成為警方子彈的目標。比埃裡克·加納更糟糕：殺害自閉症男子和小女孩的警察|艾米莉·夏爾| 2014年12月4日|每日野獸
據警察說，這名少年拒絕放下他的刀，用“100碼的凝視”固定住了他們，然後朝他們走去。自邁克爾·布朗|尼娜·斯特羅奇利奇| 2014年11月25日|每日野獸報|以來被警察殺害的14名青少年
“見鬼，是的，”他笑著說，目光銳利，藍眼睛。我與自由主義色情明星詹姆斯·迪恩的奇異之夜|艾米莉·夏爾| 2014年11月12日|每日野獸
布蘭奇坐在那裡，目光有點激動，但天真無邪；她的眼睛盯著薇薇安太太。自信|亨利·詹姆斯
在車站，搬運工領班呆呆地盯著布拉德肖，搖了搖頭，接受了他們的詢問。希爾達·萊斯韋斯|阿諾德·貝內特
在其他人當中，一位神甫三次把叉子舉到嘴邊，三次放下叉子，帶著驚訝的熱切目光。《奇聞軼事》和《趣味預算》|各種各樣的
可憐的孩子不明白托馬斯老師為什麼這麼盯著她，她發出了一聲長長的、無休止的叫聲。山谷戰士|納爾遜·勞埃德
傑西狠狠地瞪了她一眼，堅定地搖了搖頭，不讓她的朋友告訴她。羅斯草坪的篝火女孩|瑪格麗特·彭羅斯