狙擊手在屋頂和窗戶上監視著，林肯乘坐馬車穿過街道前往國會大廈時，由步兵和騎兵守衛著。林肯的第一次就職典禮遭到綁架、殺害和民兵的威脅|邁克爾·魯昂| 2021年1月15日|華盛頓郵報|
安德魯·傑克遜和他的繼任者馬丁·範布倫一起開始了坐馬車去參加就職典禮的潮流，這在他之後的整個歷史上都很常見。一次與眾不同的就職典禮|羅克珊·羅伯茨| 2021年1月13日|華盛頓郵報
然而，當我閉上眼睛時，我看不到我那雙黑黑的手緊握著馬車的座位，也看不到我在紳士的陪伴下拿著一杯拉塔菲亞酒。《布里奇頓》是如何將《公爵與我》的劇本翻過來的？凡妮莎·萊利？2021年1月12日？華盛頓郵報
愛德華·瓦倫丁（Edward Valentine）在附近的一個馬車房裡有自己的工作室，在他死後，馬車房被重新安置在博物館的地面上。里士滿雕塑家愛德華·瓦倫丁（Edward Valentine）創造了許多雕像，這些雕像定義了迷失事業的神話。現在，他家的博物館正面臨著遺產問題|Gregory S.Schneider | 2021年1月2日|華盛頓郵報
越野滑雪場遍佈數英里整潔的公路。冬季友好國家公園附近7個寧靜的空中巴士站| Megan Michelson | 2020年12月11日|戶外在線
在一個交通工具只有馬車、自行車或拖拉機的地方，免提只是一種額外的福利。瘋狂的中世紀薩克島|莉莎·福爾曼| 2014年10月4日|每日野獸
或者是一匹馬和一輛馬車，就像當年一個年輕人駕著一輛馬車，穿著粗花呢西裝，戴著帽子，仰望星空。瘋狂的中世紀薩克島|莉莎·福爾曼| 2014年10月4日|每日野獸
曼德爾鮑姆的迴應是打了弗蘭克的鼻子，把他從馬車上撞了下來。見見“盜賊女王”Marm Mandelbaum，紐約市首任暴徒頭目| J.North Conway | 2014年9月7日|每日野獸
見見羅傑，一位馬車伕從屠宰中救出的馬。《最佳野獸》，4月21日至4月27日| | 2014年4月27日|每日野獸
早在2009年，他就在一封給市議會議員的信中表達了對運輸爭議的看法。中央公園的馬車救了這匹馬|邁克爾·戴利| 2014年4月24日|每日野獸
他回答說他沒有異議，只要她不在車廂裡放上他完全厭惡的帶子盒。《奇聞軼事》和《趣味預算》|各種各樣的
一位紳士在馬車停下來之前下了車，從欄杆和站臺之間摔了下來。埃爾斯特的愚蠢|亨利·伍德夫人
她不這麼認為：“她說這些話的時候，為什麼要不厭其煩地從車廂的窗戶向外看我呢？”？布萊克伍德的愛丁堡雜誌，第60卷，第372期，1846年10月
“上帝保佑你，小姐，”老人用一種尖銳而沙啞的年齡聲音喊道，他緊靠著馬車的窗戶。《Pit Town Coronet》，第一卷（共3卷）|查爾斯·詹姆斯·威爾斯
李斯特和他的有頭銜的朋友們自己乘坐頭等車廂旅行。德國音樂研究|艾米·費伊