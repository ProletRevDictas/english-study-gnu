雖然這些幻覺可能看起來是一種休閒行為，但你不一定要打破這種習慣。當你做白日夢時，你的大腦會發生什麼？莫莉·格利克？2020年9月23日？科普
居住在德國科隆的莎拉·波拉拉（Sarah Poralla）於2018年10月和2019年9月前往聖地亞哥工作和休閒，並表示她已提出兩項退款申請。港口結算公司將向機場租車客戶發送退款| Ashly McGlone | 2020年9月22日|聖地亞哥之聲
與度假和休閒相關的旅行被認為是不必要的。特拉華州於2020年9月22日再次被列入華盛頓特區“高風險”州名單
這不僅能讓你在選舉前的空閒時間投票，而且還能幫助你所在的社區。為什麼你應該儘早投票（以及如何投票）|約翰·肯尼迪| 2020年9月17日|科普
許多室內休閒場所，包括餐館、酒吧和劇院，仍然被州長庫莫禁止經營。為什麼飲料初創企業聯合蘇打（United Sodas）正在測試一項新的戶外戰略| Gabriela Barkho | 2020年7月27日| Digiday
但在2007年，CDC在21次休閒旅行中報告了疫情，其中一次是在QE-II上。一位醫生解釋了為什麼應該禁止遊輪|肯特·塞普科維茨| 2014年11月19日|每日野獸
為《迷失在河上》工作的音樂家們沒有這樣的空閒時間。Elvis Costello、Marcus Mumford和其他人眾包了Dylan的專輯《馬爾科姆·瓊斯》| 2014年11月16日《每日野獸》
假設居住在華盛頓特區的國會議員是成年人，他們也可以在閒暇時被用石頭砸死。既然華盛頓特區已經將大麻合法化，國會會不會被石頭砸死|Abby Haglage | 2014年11月5日|每日野獸
他們穿著昂貴的休閒服，在花式俱樂部閒逛。《玩家、蓋茨和迪斯科的毀滅：反動憤怒的根源》|朱棣文| 2014年10月16日|每日野獸
越來越多的女性將馬球作為一項休閒運動，她就是其中的一員。打破馬球的草地天花板|克利·戈夫| 2014年8月20日|每日野獸
文士的智慧，是在閒暇的時候來的。行動不多的，必得智慧。《聖經》，杜埃·萊姆斯版本，多種多樣
對另一些人來說，生活不過是一種愚蠢的消遣，通過模仿活動和業餘愛好來掩蓋它的無用性。社會正義的未解之謎|斯蒂芬·利科克
到了代辦處，我卸下了搬運工，坐下來等著開門，有足夠的時間思考。歐洲概覽|霍勒斯·格里利
我以前沒有這樣做過，因為我沒有足夠的時間來檢查它們，或者在賽季允許的時間間隔內檢查它們。菲律賓群島，1493-1898年，第二十卷，1621-1624年
1770年，他擔任巴黎治安官，閒暇時致力於慈善事業，直到革命把他壓倒。《每日曆史與年表》喬爾·蒙塞爾