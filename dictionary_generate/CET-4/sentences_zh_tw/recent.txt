在最近一次來自芝加哥郊區的電話中，她在流感大流行期間一直獨自生活，她聽起來很失敗。我母親和她的朋友無法預約冠狀病毒疫苗，所以他們向陌生人求助。他13歲|格雷格·哈里斯| 2021年2月12日|華盛頓郵報
在最近輸給拜仁慕尼黑後，他特意尋找超級巨星羅伯特·勒萬多夫斯基，並索要他的球衣。馬修·霍普（Matthew Hoppe）是一名鮮為人知的美國足球運動員，直到2021年2月11日加盟德甲《史蒂文·戈夫》（Steven Goff）和《華盛頓郵報》（Washington Post）
因此，我們詢問了《科學新聞》的讀者對最近神經技術進步的看法。隱私能與閱讀和改變大腦活動的技術共存嗎|勞拉·桑德斯| 2021年2月11日|科學新聞
最近的國家委員會會議預演了一些翻譯問題可能導致的問題。非洲社區警告說，語言問題可能使他們無法重新劃分選區，2021年2月10日，聖迭戈之聲
最近的一項GitHub研究表明，自去年4月以來，開源項目的創建量增加了25%。維權開發者沃爾特·湯普森的崛起2021年2月9日TechCrunch
事實上，在最近對其國際用戶的一項研究中，這是大多數人的最低優先級。Grindr的變性問題| David Levesley | 2015年1月9日|每日野獸
這位喜劇演員對法國一家諷刺雜誌的致命攻擊作出迴應，重申了他最近對伊斯蘭信仰的批評。比爾·梅爾：數億穆斯林支持對“查理·赫布多”的襲擊；勞埃德·格羅夫2015年1月8日《每日野獸》
最新一期包含了製造汽車炸彈的詳細說明，該雜誌經常列出襲擊名單。美國間諜在巴黎大屠殺中發現基地組織的指紋| Shane Harris，Nancy A.Youssef | 2015年1月8日|每日野獸
最近幾周，在網上大肆攻擊警察已被定為刑事犯罪。政客們只愛死後的記者|盧克·奧尼爾| 2015年1月8日|每日野獸報
最近的地震活動在里氏震級上達到了3.6級的高點。26次地震之後，水力壓裂的冒煙槍在德克薩斯州|詹姆斯·喬納| 2015年1月7日|每日野獸報
毫無疑問，他們現在後悔了；但是，你知道，他們對最近的苦難記憶猶新。匹克威克俱樂部的遺書，v。查爾斯·狄更斯
菲律賓公眾看到了西班牙政府最近作出的讓步。菲律賓群島|約翰·福爾曼
最近的事件為向全世界介紹韓國及其人民做了大量工作。我們的韓國小表弟| H.李M.派克
此外，這項檢查與我無關，因為我既不是最老的審計員，也不是最近的審計員。菲律賓群島，1493-1898年，第二十卷，1621-1624年
在這次盤點中，下落不明的人數是22人，其中有幾人是最近才進入圖書館的。1924-25年總圖書館長的報告|大會圖書館（新西蘭）