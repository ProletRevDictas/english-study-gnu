在冠狀病毒大流行之後，正如今年春天的初選所表明的那樣，這一數字肯定會增加。郵寄投票的最大威脅不是安全，而是政治|馬修海默| 2020年8月24日|財富|
我在2016年大選後對報紙報道進行的另一項研究發現，約三分之一的新聞報道和專欄文章認為，克林頓之所以失敗，是因為她對身份政治的關注。克林頓的失利為拜登鋪平了道路|塞思·梅克特| 2020年8月20日|第五屆38
這是根據國家經濟研究局（National Bureau of Economic Research）5月份發表的一份工作報告得出的結論，該報告研究了在雙子塔遭到襲擊後，退伍軍人在海外部署後，暴力犯罪行為是如何增加的。戰爭的隱藏成本：獸醫犯罪| Nick Fouriezos | 2020年8月19日| Ozy
美國在流感大流行後地位的下降正在加速過去五年中出現的兩種全球政治趨勢。新冠病毒-19與美國衰落的地緣政治|凱蒂·麥克萊恩| 2020年8月19日|麻省理工技術評論
隨著新冠疫情的爆發，各行業的供應鏈正在經歷前所未有的全球混亂。新冠疫情關閉了機場、海港，阻礙了全球貨物和人員的流動。非洲正在通過一個由美國支持的研究中心解決其供應鏈赤字問題，該研究中心位於加納（Yinka Adegoke）2020年7月27日
這就是為什麼《每日野獸》與查理·赫布多站在一起，並在襲擊發生後出版了他們備受爭議的封面。為什麼我們支持Charlie Hebdo而你也應該支持| John Avlon | 2015年1月8日| DAILY BEAST
在這場騷亂之後，《紐約郵報》報道說，警方已經停止了維持治安。紐約警察局減速的地面零點| Batya Ungar Sargon | 2015年1月1日| DAILY BEAST
然而，我們剛剛得到了一個必要的警鐘：一切並不像我們所認為的那樣安全。給聖戰者的禮物：看不見的機場安全威脅|克萊夫·歐文| 2014年12月27日|每日野獸
這個新自由的國家在獨立後努力維持秩序，但不幸的是，它毫無準備。剛果被遺忘的殖民地逃亡|尼娜·斯特羅奇利奇| 2014年12月18日|每日野獸
在弗格森和紐約市的判決之後，我們中的許多人仍然情緒激動。《堆疊：修復心臟的雞肉晚餐》|皮特·德克斯特| 2014年12月7日|每日野獸》
這位先生想知道他是否做了一個夢，他應該在早上醒來。埃爾斯特的愚蠢|亨利·伍德夫人
過了一會兒，我們也離開了郵局，跟隨著領班的馬車和騎乘的護衛隊，身後塵土飛揚。原金|伯特蘭·W·辛克萊
隨著令人印象深刻的進步繼續，狂歡者們停止了狂歡，跟隨著阿里斯蒂德。阿里斯蒂德·普約爾|威廉·J·洛克的歡樂冒險
女人，你是個睡得很熟的人。醒醒，照顧好你的孩子，我會給你們倆一頓豐盛的早餐。他們面前的世界|蘇珊娜·穆迪
他的憤怒再次觸動了我的欽佩；但我在他吵醒整個營地之前就把他弄走了。還有三個約翰·默斯的故事|阿爾傑農·布萊克伍德