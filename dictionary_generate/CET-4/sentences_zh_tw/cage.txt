在後來的一項實驗中，他們發現，當給予動物選擇時，它們會主動避開籠子裡那些在進入籠子時會觸發神經元激活的區域。你為什麼感到孤獨？神經科學正開始尋找答案|Amy Nordrum | 2020年9月4日|麻省理工學院技術評論
研究小組還分析了在新籠子裡呆的前後應激激素皮質醇的血液水平。青春期可以修復生命早期艱苦生活後大腦的應激反應| Esther Landhuis | 2020年8月28日|科學新聞
十週後，每隻猴子都和它的媽媽一起被轉移到一個陌生的籠子裡。青春期可以修復生命早期艱苦生活後大腦的應激反應| Esther Landhuis | 2020年8月28日|科學新聞
這些血液是在他們被關在新籠子裡之前、期間和之後採集的。一點壓力可能有助於年輕人建立韌性| Esther Landhuis | 2020年8月27日|學生科學新聞
大約在同一時間，精神病學家塞薩爾·阿戈斯蒂尼（Cesar Agostini）把狗關在裝有鈴鐺的籠子裡，每當它們試圖躺下睡覺時，鈴鐺就會發出可怕的叮噹聲。20世紀20年代，日本的研究人員用佈滿釘子的籠子做了類似的事情。為什麼睡眠剝奪會導致死亡| Veronique Greenwood | 2020年6月4日| Quanta雜誌
但你想知道，當你走進格拉特福德的牢房或者更確切地說是牢籠時，即使是神智正常的人也如何保持頭腦清醒。就連科赫兄弟和喬治·索羅斯也可以在《蒂娜·布朗》2014年11月10日《每日野獸》上達成一致
它是空的，門打開了，也許鳥已經飛了，或者籠子在等待它的下一個居民。索爾·胡安娜：墨西哥最性感的詩人和最危險的修女|凱蒂·貝克| 2014年11月8日|每日野獸
事實上，2000年，尼古拉斯·凱奇（Nicolas Cage）擁有的一部9.0級動作漫畫《第一號》（No.1）被人從他的房子裡偷走。2014年10月14日《每日野獸》紐約漫畫展上，漫畫書的聖盃藏在一個樸素的地方
在美國，約翰·凱奇（John Cage）的學生、藝術家艾倫·卡普羅（Allan Kaprow）於1957年提出了“正在發生”一詞。激進煽動者和社區領袖奧托·穆爾的生活和藝術|安東尼·哈登·蓋斯特| 2014年9月22日|每日野獸
當我第一次聽說這項運動時，我認為這是一場“無障礙”的籠式比賽，幾乎什麼都有。耶穌說擊倒你：在“戰鬥教會”中，基督徒毆打你的鄰居布萊恩·斯托克爾2014年9月16日《每日野獸》
於是海蒂把雞放在籠子裡，用羊毛蓋住，每天喂幾次，直到它認識了她。《託兒所》，1873年7月，第十四卷。第一|各種
“牢籠”僅僅是一種“嚴格監護”的安排，但對於女性來說，很少有必要這樣做。羅伯特國王布魯斯·A·F·穆裡森
籠子中間豎起了一道堅固的路障，路障後面坐著主人，面色蒼白，但目中無人。潘趣酒，或《倫敦查裡瓦里》，第158卷，1920年4月28日，各種
幕間休息時，舞臺上放著一個長長的、可移動的籠子，裡面有十隻走來走去的野獸。真正的拉丁四分之一| F.Berkeley Smith
籠子被衝進了船裡，嘩啦一聲摔碎了。古德溫沙灘的浮光| R.M.巴蘭坦