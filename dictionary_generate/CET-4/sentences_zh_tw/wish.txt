在你們共同開始新生活之際，向你們倆致以我們的愛和最美好的祝願。如何在冠狀病毒大流行期間拒絕婚禮邀請|布魯克·亨德森| 2020年8月23日|財富
雖然各國領導人都希望首先保護自己的人民，但對這一流行病的反應必須是集體的。世衛組織總幹事警告世界：不要成為新冠病毒“疫苗民族主義”的犧牲品
聖迭戈市議會成員有一份願望清單，上面寫著他們希望用6200萬美元做些什麼。該市在向下一個電力供應商麥肯齊·埃爾默（MacKenzie Elmer）索要數百萬美元方面走的很好，2020年8月7日，聖地亞哥之聲（Voice of San Diego）
我們從用戶那裡得到了很多令人鼓舞的評論，他們的願望已經包含在我們的路線圖中。如何將您的客戶反饋轉化為您產品的驅動力| Maria Kazakova | 2020年6月18日|搜索引擎手錶
現在，需要注意的是，元機器人標籤在任何時候都可能被忽略，但大多數爬蟲都尊重網站管理員的意願。對SEO至關重要的八個HTML元素| Aleh Barysevich | 2020年5月5日|搜索引擎觀察
是的，我們的價值觀包括容忍那些想取笑宗教的人。阿亞安·赫西·阿里：我們的職責是讓查理·赫多活著|阿亞安·赫西·阿里| 2015年1月8日|每日野獸
我希望我是一個年輕的卡羅爾·金，在布里爾大廈工作。Belle&Sebastian不再那麼害羞了| James Joiner | 2015年1月7日| DAILY BEAST
最終，2015年可能是美國反LGBT倡導者希望跳過的一年只有上帝才能阻止同性戀婚姻| Tim Mak | 2015年1月6日|每日野獸
在這一點上，馬文給出了他的自由帷幔微笑，那種讓你希望你能在他面前解體的微笑。李·馬文的自由帷幔微笑背後的故事|羅伯特·沃德| 2015年1月3日|每日野獸
儘管如此，我還是希望DuVernay給了我們更多關於那些不太出名的人的信息，除了他們一起吃飯的場景。金博士前往好萊塢：《塞爾瑪》有缺陷的歷史|加里·梅| 2015年1月2日|每日野獸報
當伊瑪用這枚戒指壓地球時，地球的天才阿拉瑪蒂響應了他的願望和命令。所羅門與所羅門文學
我的意思是，很好，我母親病了，不想吃東西，因為如果她吃了，她就什麼也吃不下了。尋找父親的迷迭香| C.N.威廉姆森
然而，在第一年年底，她放棄了這一特權，因為她不想接受禮物的條件。從公元前七世紀到公元二十世紀的女性美術作品|克拉拉·厄斯金·克萊門特
那時，我有多少次希望和我親愛的格盧姆達爾奇在一起，到目前為止，一個小時的時間把我和他分開了。《格列佛遊記》|喬納森·斯威夫特
當絕望的願望穿過他的靈魂時，鐵也跟著進來了，但沒有消失。《牧師的火面》第3卷，共4卷|簡·波特