當談到心愛的產品，我們“非常善於合理化，以減少這種不適，同時仍然消費我們想要消費的產品，”約翰說。抵制MyPillow：產品如何引發身份危機| Elizabeth Chang | 2021年2月12日|華盛頓郵報
他說，他不是雙重面具的支持者，因為雙重面具會消耗更多的面具，也會導致更多的空氣洩漏。CDC說，口罩應該更合身，或者可以摺疊起來以防冠狀病毒變異，Lena H.Sun，Fenit Nirappil，2021年2月11日，華盛頓郵報
時事通訊通過讓讀者養成每天早上閱讀《泰晤士報》內容的習慣，支持《泰晤士報》訂閱驅動的業務。《紐約時報》的目標是將時事通訊讀者轉變為付費訂戶，因為《晨間時事通訊》將於2021年2月10日（Sara Guaglione）在Digiday發行，發行量超過10億份
意圖信號行為——當用戶搜索、點擊、消費和重複時可以觀察到——是數字廣告生態系統的生命線，出版商必須尊重這一點。分裂原子：將觀眾與庫存分離釋放酒吧的力量| Trevor Grigoruk | 2021年2月9日| Digiday
如果聖地亞哥的飲用水成本不夠高，就要考慮去除水的成本。《環境報告：去除水的高成本》|麥肯齊·埃爾默| 2021年2月8日|聖地亞哥之聲
法國生產的香檳仍有一半以上是由法國人消費的。香檳：你喝錯了| Kayleigh Kulp | 2014年12月20日| DAILY BEAST
隨著技術適應反映我們消費媒體的方式，家庭也適應了技術。狂歡觀看是新的結合時間|每日野獸| 2014年12月10日|每日野獸
這正是我們現在消費電視的方式：我們最喜歡的節目，無論何時何地，只要我們想要。新發明讓你隨時隨地看電視| | 2014年12月8日|每日野獸
新的創新繼續使無線點播電視成為我們家庭消費電視的新方式。新發明讓你隨時隨地看電視| | 2014年12月8日|每日野獸
由於擔心“食品純度”，你的飲食營養不平衡。Orthorexia：當健康飲食成為一種困擾時| DailyBurn | 2014年10月25日| DAILY BEAST
它必吞滅眾山，焚燒曠野，燒滅一切青綠的，好像用火焚燒一樣。《聖經》，杜埃·萊姆斯版本，多種多樣
阿里斯蒂德站在那裡閒聊，直到市長邀請他坐到餐桌旁，喝點飲料。阿里斯蒂德·普約爾|威廉·J·洛克的歡樂冒險
她認為偶像會吃掉他們，因為單身漢烹飪從來不是為單身漢殘疾人準備的。山谷戰士|納爾遜·勞埃德
火也從上頭髮出，要燒山和樹林，照所吩咐的行。《聖經》，杜埃·萊姆斯版本，多種多樣
所以我說，我要在曠野將我的忿怒倒在他們身上，將他們滅絕。《聖經》，杜埃·萊姆斯版本，多種多樣