該醫院向謝爾比縣法院提交了數千份通知，聲稱被告的餘額現在為零，從而有效地結束了法律訴訟。債務催收機器停止運轉後會發生什麼？|作者：Wendi C.Thomas，MLK50：通過新聞業實現正義| 2020年10月2日| ProPublica
“我關注的是確保我們選擇了正確的企業領域來關注，”她說，並將培訓、遠程協助和三維可視化作為公司關注的三個主要用例。魔幻飛躍的佩吉·約翰遜：成為一家旋轉業務的首席執行官並不意味著跳出“玻璃懸崖”——《愛爾蘭人》——《財富》雜誌，2020年10月1日
德克薩斯州的原告包括18個州的共和黨州長或總檢察長，他們聲稱這項歸零的授權是違憲的。伊恩·米爾希瑟（Ian Millhiser）於2020年10月1日解釋說，最高法院將在新的任期內審理7起重大案件
你必須與那些身處世貿中心的人建立聯繫，知道他們所說的是什麼。艾琳·布羅科維奇已經放棄了聯邦政府拯救環境的計劃|妮可·古德金德| 2020年9月25日|財富|
“行業動態已經發生了變化，”芬蘭電信顧問安東尼奧斯·德羅索斯（Antonios Drossos）表示，其Rewheel公司近年來分析了歐盟的零評級提議。歐盟最高法院剛剛填補了歐洲網絡中立規則的一個重大漏洞，即《財富》（Fortune）
不完全是這樣，但有一次氣溫降到零下29度，雪厚21英寸。速讀：紫雨歷史上最有趣的片段|珍妮·亞布羅夫| 2015年1月1日|每日野獸
一個人康復的時間越長，復發的機率就越低，儘管這種可能性永遠不會為零。你永遠無法“治癒”進食障礙|卡莉·阿諾德| 2014年12月20日|每日野獸
“同性戀與傳統主義者是一場零和遊戲”是真的嗎？LGBT是否欠基督徒一個橄欖枝？試試另一種方法|傑伊·邁克爾森| 2014年12月14日|每日野獸
伊斯蘭國帶來了“和平、自治、零腐敗、低犯罪率”，他上個月在推特上寫道。伊希斯嚇壞了的Widdle Kitty |雅各布·西格爾| 2014年12月12日|每日野獸
在經濟衰退期間，從墨西哥到美國的淨移民下降到零或更少。更多低技能移民的案例| Veronique de Rugy | 2014年12月7日| DAILY BEAST
但他計劃出發的那天非常冷，水銀柱在零下27度。宅基地主人奧斯卡·米切奧
當然，在輪盤賭中，每次都會出現一些數字或零本身，但數字27總是不走運的。《Pit Town Coronet》，第三卷（共3卷）|查爾斯·詹姆斯·威爾斯
糕點廚師耳邊低語著的話使他那熾熱的勇氣降到了零。恐怖事件下的一集|榮譽巴爾扎克
最後，有一天晚上，他走到一張桌子前，對蹲著的人說：“上一次零號是什麼時候？”吉爾伯特·帕克，你永遠不會知道你的運氣
上週的一天，當水銀柱在零度時，三隻小羊來到了這個地方。紅母牛和她的朋友彼得·麥克阿瑟