與分子和抗原診斷試驗不同，FDA警告說，基於血液的抗體試驗不是為了診斷活動性感染，而是為了揭示以前是否有人感染過病毒。《新冠病毒-19的魅力：一個自稱的重罪犯如何說服當選官員試圖幫助他從疫情中獲利》| Vianna Davila、Jeremy Schwartz和Lexi Churchill著| 2020年9月25日| ProPublica
為了確保你在這個秋天能進入這些階段並保持活力，你可能需要一些基本的東西來提升你的家庭或健身房鍛鍊計劃。5款適合您今秋日常鍛鍊的健身產品| Jasmine Grant | 2020年9月25日| Essence.com
與政治活躍教會有聯繫的反對派團體——他們被指責是最近病毒死灰復燃的罪魁禍首——正計劃在未來幾周在首都舉行一系列大規模抗議活動。韓國的宗教右翼破壞了對抗新冠肺炎的鬥爭|菲奧娜·祖布林| 2020年9月24日|奧齊
最初為數不多的優秀餐飲合作伙伴已攀升至100多名活躍會員和承諾會員。這兩家餐廳想要一個零碳食品系統。這會發生嗎|Bobbie Johnson | 2020年9月24日|麻省理工學院技術評論
2018年，Damer在新西蘭一個活躍的地熱區開了一家店，以通常的主題——地獄之門——命名，以驗證這一假設。地球上的生命可能在充滿敵意的溫泉中開始|傑克·J·李| 2020年9月24日|科學新聞
《泰晤士報》報道說：“美國已經開戰，宣佈必須銷燬一項正在進行的大規模殺傷性武器計劃。”。絕對必須在2015年消亡的政治模因| Asawin Suebsaeng | 2015年1月1日| DAILY BEAST
幾乎立刻，另一個活躍在抗議活動中的組織稱為正義聯盟告密者。De Blasio和紐約市抗議者手上沒有血跡| Jacob Siegel | 2014年12月22日| DAILY BEAST
沿著這條河，一個活躍的貿易中心的殘垣斷壁被大自然所取代。剛果被遺忘的殖民地逃亡|尼娜·斯特羅奇利奇| 2014年12月18日|每日野獸
但是，在媒體鷹派的目光下，通過高度活躍的社交媒體，她照常行事。Nicki Minaj在《針印》上暴露了自己的弱點| Rawiya Kameir | 2014年12月16日| DAILY BEAST
過去兩年來，女性成員參與了大屠殺，但從未扮演過如此積極的角色。博科聖地恐怖的新面孔：少女|尼娜·斯特羅奇利奇| 2014年12月13日|每日野獸
我們必須先有母題，然後是適應和調整表達的技術，並在活性劑中開發設施。富有表現力的聲音文化|傑西·埃爾德里奇·索斯威克
隨著三十六名董事的宣佈，這些機構得以積極開業。貨幣和銀行讀物|切斯特·阿瑟·菲利普斯
他成為衛斯理先生的助手之一，並積極為教會服務。《每日曆史與年表》喬爾·蒙塞爾
與這兩種死記硬背學習方法相反的是我的方法，它在每對單詞之間注入了一個積極的過程。同化記憶| Marcus Dwight Larrowe（又名A.Loisette教授）
他繼續運動，直到35歲時開始衰弱，死於胸腔積水。《每日曆史與年表》喬爾·蒙塞爾