他自己的文章總是清晰明瞭，他的散文緊緊地建立在名詞和動詞的基礎上《女王的遊戲》是一本暢銷書，但它的作者沃爾特·特維斯（Walter Tevis）幾乎不是一個一炮走紅的奇蹟。|邁克爾·迪爾達| 2021年2月3日|華盛頓郵報|
實體是事物、人、地方或概念，可以用名詞或名稱來表示。NLP和AI如何革新SEO友好內容[幫助你的五種工具]| May Habib | 2020年12月29日|搜索引擎觀察
他當時說，雅虎99%的搜索查詢中都有一個名詞。谷歌搜索通過地圖中的忙碌、雙工和AR深入“現實世界”|格雷格·斯特林| 2020年10月16日|搜索引擎之地
這種專有名詞的嵌套有助於使高等數學不僅對局外人難以理解，而且對試圖從一個子領域讀入另一個子領域的在職數學家也難以理解。為什麼數學家應該停止用彼此的名字命名事物-第89期：黑暗面|勞拉·鮑爾| 2020年9月2日|鸚鵡螺
按照當時的社會科學用法，米德除了在語言學意義上使用性別一詞外，從未在其他任何方面使用過性別一詞，例如可以歸類為陰性、陽性或中性的名詞。性別就是你對它的理解-第88期：愛與性|查爾斯·金| 2020年8月5日|鸚鵡螺
當使用專有名詞時，可能會與普通名詞混淆。Yo | Dale Eisinger的禪宗| 2014年8月2日|每日野獸
名詞“mechanicals”指的是對創作和表演作品的任何物理複製，即“罐裝音樂”。範戴克·帕克斯講述了歌曲作者在數字時代是如何陷入困境的|範戴克·帕克斯| 2014年6月4日|每日野獸
“大蕭條”作為一個專有名詞，直到20世紀50年代才開始流行，那是在大蕭條結束很久之後。我們的統治思想是否定|尤金·林登| 2014年5月12日|每日野獸
當我關掉收音機時，我聽到的最後一個單詞一定是名詞，而不是動詞、形容詞或介詞。A.J.雅各布斯：我如何寫作|諾亞·查尼| 2013年5月29日|每日野獸
《星際迷航》科幻小說系列於1966年在電視上播出《星際迷航》中的傻瓜：準備好我們的初級讀物《走進黑暗》| Sujay Kumar | 2013年5月14日| DAILY BEAST
正如貝爾所建議的那樣，我找不到將它作為一個集體名詞的權威。喬叟作品，第1卷（共7卷）——玫瑰的浪漫；小詩|傑弗裡·喬叟
這常常成為一個抽象的女性名詞，與法語中的-e結尾對應；米斯特拉語中的armée是isPg 56 armado。Frdric Mistral |查爾斯·阿爾弗雷德·唐納
另一方面，當聽到“一個黑人”這個詞時，大腦就不會構建任何形象；它一直等到修飾的名詞說出。中文：作文與文學| W.F.（威廉·富蘭克林）韋伯斯特
在平衡句中，一個部分與另一部分是平衡的，-一個名詞和一個名詞，一個形容詞和一個形容詞，短語和短語。中文：作文與文學| W.F.（威廉·富蘭克林）韋伯斯特
“鐘擺”和“強度”這兩個詞是他第一次使用的，也是他第一次用流體作為名詞。文字告訴我們的故事|伊麗莎白·奧尼爾