餐館老闆別無選擇，只能繼續營業，他們的員工冒著重病或死亡的風險領取薪水。難道政府就要眼睜睜地看著餐飲業消亡嗎|Elazar Sontag | 2020年8月28日|食客
Verily的其他努力包括一項消滅蚊子傳播疾病的計劃和幾個州的新冠病毒-19檢測計劃。Alphabet確實計劃利用大數據幫助僱主預測醫療保險成本| Rachel Schallom | 2020年8月25日| Fortune
在一組約3000名患者中，研究人員比較了在不同疾病階段接受高水平和低水平（或滴度）抗體血漿的患者的死亡率。新冠病毒-19血漿治療可能是安全的，但我們不知道它們是否有效| Tina Hesman Saey | 2020年8月25日|科學新聞
這些人患有呼吸系統疾病，沒有人談論它。《未知力量》的傑西卡·馬修斯（Jessica O.Matthews）有一項計劃，旨在振興美國搖搖欲墜的基礎設施《布魯克·亨德森》（Brooke Henderson）2020年8月23日《財富》（Fortune）
他們還可以將病毒傳播給其他人，包括教育工作者、學校工作人員和家庭成員，這些人僅僅是成年人就有更高的罹患嚴重疾病的風險。當科學還沒有答案時|南希·舒特| 2020年8月23日|科學新聞
他兩次戰勝了自己的疾病，寫下了自己與疾病的鬥爭，並在健康每況愈下的情況下繼續廣播。記住ESPN狡猾、自大、冷靜的主播斯圖爾特·斯科特|立體威廉姆斯| 2015年1月4日|每日野獸
撒瑪利亞指南是圍繞自殺是一種純粹的非理性行為這一假設編寫的，這種行為是由疾病引起的。掩蓋和關注巨魔：事實上，這是關於自殺新聞中的道德問題| Arthur Chu | 2015年1月3日| DAILY BEAST
很少有關於他精神疾病的報道討論鉛中毒是他精神惡化的可能原因。摔跤手馬克·舒爾茨痛恨《獵狐人》|裡奇·戈爾茨坦| 2014年12月31日|每日野獸報|的“令人作嘔的侮辱性謊言”
謀殺、自殺、疾病、老年：這些死亡困擾著我們所有人，但在監獄裡，它們以更便宜的價格收留我們。《監獄裡的一百萬種死亡方式》|丹尼爾·吉尼斯| 2014年12月8日|每日野獸》
庫克寫道：“看到那種無助的憤怒加上精神疾病的極端版本會產生什麼，真是瘋了。”。校園槍擊者喜歡這個皮卡藝術家網站|布蘭迪·扎德羅茲尼| 2014年12月5日|每日野獸
在大約18個月的辦公室工作後，我得了一場長期的重病，離開工作崗位將近半年。英格蘭、蘇格蘭和愛爾蘭50年的鐵路生活|約瑟夫·塔洛
仍然有成千上萬的人由於意外或疾病、年齡或虛弱而無法維持生活。社會正義的未解之謎|斯蒂芬·利科克
這是寫在明顯的焦慮，主要的主題是他的女兒的疾病。布萊克伍德的愛丁堡雜誌，第CCCXXXIX號。1844年1月。第LV卷|各種各樣的
歐內斯克利夫先生對他在非洲海岸的疾病非常熟悉，醫生非常喜歡他。雛菊鏈|夏洛特·楊
科門聽說布魯斯得了重病，就與莫佈雷和布萊欽一起，以一種更強大的力量與他對抗。羅伯特國王布魯斯·A·F·穆裡森