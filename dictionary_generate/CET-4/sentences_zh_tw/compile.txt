正如我們在2018年所做的那樣，FiveThirtyEight為今年參眾兩院和州長的每一位主要政黨候選人收集了大量信息，包括性別信息。競選公職的女性比以往任何時候都多。但他們是否贏得了初選|梅雷迪斯·康羅伊| 2020年9月2日|第五屆38
香港人擔心他們的數據也會被北京彙編。香港新的大規模科考試驗方案是自由自願的，一些公民是可疑的。埃蒙巴雷特（2020年8月26日）。
根據綠街顧問在流感大流行前收集的數據，西蒙在一週內購買了兩家全國連鎖店，估計在自己的房產中擁有約400家分店。美國最大的購物中心擁有者獲得了一個新租戶：它自己| Daniel Malloy | 2020年8月20日| Ozy
該系統最初由巴爾的摩市、美國衛生部長辦公室和紐約衛生部用於編制衛生統計數據。所有的機會可能都是在比林斯的幫助下獲得的。打孔|泰特·瑞安·莫斯利| 2020年8月19日|麻省理工技術評論
為此，該團隊期待著彙編一套完整的3D數據集和工作流程，供其他研究人員和學生使用，以便在涉及龐貝城宏偉建築的未來項目上進行更加協調一致的努力。羅馬多莫斯（住宅）：建築與重建| Dattareya Mandal | 2020年4月8日|歷史領域
在101件物品中篩選出無數的文物來編纂紐約的歷史已經足夠有挑戰性了。我們過去都有玫瑰花蕾|山姆·羅伯茨| 2014年10月15日|每日野獸
一位發言人甚至工作到深夜，以彙編有關和平隊安全的統計數據。和平隊的可怕祕密| Tim Mak | 2014年8月16日|每日野獸
事實上，沃默斯最終將在長達12頁的編年史中編撰一份傑克·倫敦撥款清單。鮑勃·迪倫的《達芬奇密碼》披露|克里斯·弗朗切斯卡尼| 2014年5月18日|每日野獸
我們將在聖誕節前為他編一本厚厚的讀物。克林頓、布什和奧巴馬關於他們如何發表國情諮文的演講稿撰寫人|克里斯·裡巴克| 2014年1月27日|每日野獸
但我敢打賭，當我明年12月編撰我的十大排行榜時，《真偵探》仍將獨佔鰲頭《真實偵探》評論：你必須觀看HBO的革命犯罪經典劇《安德魯·羅馬諾》2014年1月11日《每日野獸》
為了用充分的證據來支持這些說法，我必須編纂一本書，其篇幅是本書的四倍。上帝與我的鄰居羅伯特·布拉奇福德
不完全是這樣，如果我可以從這些比較速度的表格中判斷的話，這些表格是我根據自己的經驗記憶編制的。魔鬼詞典|安布羅斯·比爾斯
從來沒有人試圖編寫硒有機化合物的參考書目。2-甲基-4-硒喹唑酮、2-苯基苯並硒唑及其衍生物的合成
因此，編撰這樣一部作品所需的大量勞動和費用可能會形成一些想法。智者、化學家和偉大醫生的祕密|威廉·K·大衛
為什麼一個私人醫生的耐心工作不編一本大人物穩定名字的字典呢？該死|亨利·路易斯·門肯