手柄頂部有一把鎬，用戶可以通過輕敲動作破冰，而底部的釘子則提供了更傳統的刺傷功能。最佳冰鎬：露營和其他活動的多功能冬季工具| PopSci商業團隊| 2021年2月25日|科普
用切割墊、金屬尺和方格、一些海綿和橡膠或木錘來完成你的套裝。任何其他類型的錘子都可能損壞皮革。開始製革所需瞭解的一切|桑德拉·古鐵雷斯G.| 2021年2月19日|科普
該組織經常攜帶槍支和其他武器，如錘子和棒球棒，定期在明尼蘇達州國會大廈舉行集會。不同政治派別的美國人都擔心國會大廈襲擊預示著什麼|安妮·戈文、詹娜·約翰遜、霍莉·貝利| 2021年1月12日|華盛頓郵報|
這裡有規矩，如果有人在聊天中出了差錯，你就把錘子扔了現實是我們必須專業：一位廣告執行官在國會大廈混亂中工作的自白|克里斯蒂娜·蒙洛斯| 2021年1月11日|迪吉德
1992年，帕帕傑利斯與肯·約翰遜（Ken Johnson）“平託·羅恩”（Pinto Ron）一起加入了哈默的陣營比爾·黑手黨（Bills Mafia）等了整整一代人才成立這樣的團隊。它不得不從遠處擁抱它|亞當·基爾戈爾| 2021年1月7日|華盛頓郵報
下一步，共和黨應該努力研究我們的道路、橋樑和隧道是如何崩潰的，並推動基礎設施建設計劃。布什、克里斯蒂、羅姆尼：誰將成為共和黨的階級戰士|勞埃德·格林| 2014年12月15日|每日野獸
如果我們手裡拿著錘子進去，離開時臉上可能只有塵土和瓦礫。出租：無價古蹟|埃莉諾·貝特什| 2014年11月16日|每日野獸
通過這種方式，某些認知機制可能會像錘子一樣，對釘子過於渴望。為什麼千禧一代對有組織的宗教不友好|Vlad Chituc | 2014年11月9日|每日野獸
這句話的意思是，“伸出的釘子總是被錘子砸到。”Sor Juana：墨西哥最性感的詩人和最危險的修女| Katie Baker | 2014年11月8日| DAILY BEAST
另一段監控錄像顯示凶手手裡拿著錘子。布魯克林對同性戀者來說變得不安全了嗎？這取決於哪一個|傑伊·邁克爾森| 2014年10月18日|每日野獸
錘子的響聲常在他耳中，他的眼睛注視著他所造器皿的形狀。《聖經》，杜埃·萊姆斯版本，多種多樣
男孩用錘子敲掉了小盒子的一些板條，斯金蒂就是在這個盒子裡旅行的。滑稽豬眯眼|理查德·巴納姆
我想錘子從琴絃上落下的速度會更慢，這會使音調唱得更長。德國音樂研究|艾米·費伊
當他到達那裡時，他已經準備好倒下了，他的心臟像錘子一樣在肋骨上跳動。布萊克伍德的愛丁堡雜誌，第CCCXXXIX號。1844年1月。第LV卷|各種各樣的
接著，班長震耳欲聾的錘子又響了起來，然後是沉默。戴兜帽的偵探，第三卷第2期，1942年1月|各種