還有一些菜根本不適合放在最上面的架子上，比如一整隻烤雞。不要害怕肉雞| Elazar Sontag | 2021年2月11日|食客
我在夜裡醒來，不停地想這隻烤雞可能會給我的生活帶來什麼。愛、孤獨和我冰箱裡的雞| Elazar Sontag | 2021年2月9日| Eater
將您的荷蘭烤箱放在這款優雅的三角架上，為您提供完美的烤雞或豐盛的燉牛肉。來自削減：33件情人節禮物送給你生活中的美食家|削減工作人員| 2021年2月8日|食客
當你烤一大盤蔬菜時，很容易將剩菜重新加熱或添加到穀物碗中。這5個食譜以烤剩的蔬菜為特色，證明你應該經常多做一些| Kari Sonde | 2021年2月8日|華盛頓郵報|
現在他在這裡聽到，真的是第一次聽到，這個星球，他兒子未來的家，即將被烤焦。氣候危機比你想象的還要嚴重。下面是如果你嘗試的話會發生什麼|伊麗莎白·威爾（Elizabeth Weil）| 2021年1月25日| ProPublica
聚集在那裡晒太陽的保姆、罌粟花、祖母和祖父。有權勢的國會議員寫“豐滿的乳房”| Asawin Suebsaeng | 2015年1月7日|每日野獸
將烤肉從鍋中取出，靜置至少15分鐘。製作卡拉大廳的烤豬排配小紅莓|卡拉大廳| 2014年12月24日|每日野獸
當我狼吞虎嚥地吃下烤鴨準備告別晚餐時，一位中國餐館老闆跑到窗前，聽到對面傳來槍聲。沒有電影明星，沒有紅地毯，但淡季戛納仍然是魔術|莉莎·福爾曼| 2014年9月15日|每日野獸
隨著時間的推移，我學會了把整個火鍋拆開，把加熱線圈裝在一個烤牛肉罐上，再在罐上打上一個完整的孔。《監獄美食家的故事：我是如何在監獄裡學會烹飪的》| Daniel Genis | 2014年6月21日| DAILY BEAST
我告訴他，它有西海岸最好的燉肉，但事實證明，它只出現在晚餐菜單上。邁克爾·黑斯廷斯的生命渴望|傑克·格雷| 2014年6月14日|每日野獸
或者，如果我根本不釣魚，而是通過支付我在鋸木廠工作所得工資的一部分來獲得烤魚，那該怎麼辦？社會正義的未解之謎|斯蒂芬·利科克
他們正要生火，烤一些肉當晚餐，這時聽到了一聲悽慘的叫聲。Alila，我們的菲律賓小表妹瑪麗·哈澤爾頓·韋德
為了向英國人致意，英國人贈送了烤牛肉，烤得很少。巴西航行日誌|瑪麗亞·格雷厄姆
他是個胖子，吃烤豬肉、蘋果醬、土豆泥和麵包。愛的朝聖|厄普頓辛克萊
鼻子裡有一個烤蘋果，還有一條絲帶——一條藍色的絲帶，一條粉紅色的絲帶裝飾著他那華麗的小尾巴。這個女人給了歐文·約翰遜