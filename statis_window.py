from tkinter import *
from tkinter import ttk
import os

class Window(Frame):
    def __init__(self, master=None):
        Frame.__init__(self, master)
        self.master = master
def bool(a):return True if a=='True'else False
root = Tk()
app = Window(root)

# set window title and icon
root.wm_title("統計數據")
icon=PhotoImage(file='icon.png')
root.tk.call('wm','iconphoto',root._w,icon)

# set window size and position
w,h=1280,600
root.geometry("%dx%d+%d+%d"%(w,h,(root.winfo_screenwidth()-w)/2,(root.winfo_screenheight()-h)/2))

# set status of gird positions
Grid.columnconfigure(root,0,weight=1)
Grid.rowconfigure(root,1,weight=1)

# set treeview
ti=Label(root,text="Time",font=("宋体",24))
ti.grid(row=0,column=0,columnspan=1,sticky="NSEW")
style_head = ttk.Style()
style_head.configure("Treeview",font=("宋体", 10))
style_head.configure("Treeview.Heading",font=("宋体", 15))
ta=ttk.Treeview(root)
sc=Scrollbar(root,command=ta.yview)
sc.grid(row=1,column=1,sticky='NSEW')
sc2=Scrollbar(root,orient='horizontal',command=ta.xview)
sc2.grid(row=2,column=0,columnspan=1,sticky='NSEW')
ta.configure(yscrollcommand=sc.set,xscrollcommand=sc2.set)
ta['columns']=('ids','time','word','bool','error','style','table')
texts=['ID','時間','詞彙','是否','錯案','訓練類型','詞表']
ta.column("#0",width=0,stretch=NO)
for a in ta['columns']:ta.column(a,width=80)
ta.heading("#0",text="")
for a in range(len(ta['columns'])):ta.heading(ta['columns'][a],text=texts[a])
ta.grid(row=1,column=0,columnspan=1,sticky='NSEW')

# set treeview 2
ti2=Label(root,text="Spell",font=("宋体",24))
ti2.grid(row=0,column=2,columnspan=1,sticky="NSEW")
ta2=ttk.Treeview(root)
sc2=Scrollbar(root,command=ta2.yview)
sc2.grid(row=1,column=3,sticky='NSEW')
sc22=Scrollbar(root,orient='horizontal',command=ta2.xview)
sc22.grid(row=2,column=2,columnspan=1,sticky='NSEW')
ta2.configure(yscrollcommand=sc2.set,xscrollcommand=sc22.set)
ta2['columns']=('ids','word','sum_num','right','error','rate')
texts=['ID','詞彙','訓練次數','正確數','錯誤數','正確率']
ta2.column("#0",width=0,stretch=NO)
for a in ta2['columns']:ta2.column(a,width=80)
ta2.heading("#0",text="")
for a in range(len(ta2['columns'])):ta2.heading(ta2['columns'][a],text=texts[a])
ta2.grid(row=1,column=2,columnspan=1,sticky='NSEW')

# set ta and tas
l=[]
for a in os.walk(pa:='dynamic_logging'):
    for b in a[2]:
        if b[0]=='.':continue
        f=open('%s/%s'%(pa,b));t=[a.split(' ')for a in f.read().split('\n')];f.close()
        n=0
        for c in t:
            c.insert(0,str(n+1))
            c[1]='%sT%s'%('-'.join(b.split('-')[:-1]),c[1])
            c[4]=c[4]if c[4]!='None'else''
            c.append(b.split('-')[-1:][0].split('.txt')[0])
            l.append(c)
            ta.insert(parent='',index='end',iid=n,text='',values=tuple(c))
            n+=1
#'ID','詞彙','訓練次數','正確數','錯誤數','正確率'
def de(a,b):return[z[b]for z in a]
d=[]
for a in l:
    if a[2]not in de(d,0):d.append([a[2],1,1 if bool(a[3])else 0,0 if bool(a[3])else 1,0])
    else:
        for z in range(len(d)):
            if d[z][0]==a[2]:
                d[z][1]+=1
                if bool(a[3]):d[z][2]+=1
                else:d[z][3]+=1
for a in range(len(d)):d[a][4]='{}%'.format(str(d[a][2]/d[a][1]*100))
for a in range(len(d)):
    for b in range(len(d[a])):
        d[a][b]=str(d[a][b])
n=0
for a in range(len(d)):
    d[a].insert(0,str(n+1))
    ta2.insert(parent='',index='end',iid=n,text='',values=tuple(d[a]))
    n+=1

# show window
root.mainloop()
